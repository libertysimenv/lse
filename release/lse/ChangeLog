Changes from LSE_bundle-1.0p3 to LSE_bundle-2.0
===============================================
- All modules upgraded

Changes from LSE_bundle-1.0p2 to LSE_bundle-1.0p3
============================================

- emulib-1.0      was upgraded to emulib-1.0p1
- lse-1.0p1       was upgraded to lse-1.0p2


Changes from emulib-1.0 to emulib-1.0p1
============================================

- Compilation errors due to bad lvalues in gcc 4 fixed.
- IA64 emulator failure under valgrind due to uninitialized PFS register fixed.

Changes from lse-1.0p1 to lse-1.0p2
============================================

- Compilation errors due to bad lvalues in gcc 4 fixed.

- Class not found exception in lss that seems to have appeared 
  at about Fedora Core 5 fixed.

- Support for Java 1.5

Changes from LSE_bundle-1.0p1 to LSE_bundle-1.0p2
============================================

- lse-1.0          was upgraded to lse-1.0p1
- corelib-1.0      was upgraded to corelib-1.0p1
- visualizer-1.0p1 was upgraded to visualizer-1.0p2


Changes from lse-1.0 to lse-1.0p1
============================================

- Large specifications used to overflow the Java stack during type
  inference.  This bug has been fixed.

- Large specifications used to overflow Python stack on write.  This
  manifested itself as a Python error with no message.  This bug has
  been fixed.

- Spaces at end of C pre-processor directives caused mess errors in
  the tokenizer.  This bug has been fixed.

- Syntax errors in codepoints used to report the error and then abort
  build immediately.  Now compilation continues and aborts after all
  analysis.

- LSE_signal_not worked improperly.  This has been fixed.

- LSE_port_query to a port alias on a port carrying data caused a
  syntax error at C compile time.  This has been fixed.

- LSE_port_query to a port alias on an unconnected port caused Python
  errors.  This has been fixed.

- LSE_port_query to an unconnected port caused a syntax error at C
  compile time.  This has been fixed.

- realmain.c was missing \n\ at line 246.  This has been fixed.

- LSE_check_ports_incrementally reported errors when handlers before
  first dynamic section.  This has been fixed

- Rebuild of framework was not triggered by changes in instance's
  start_of_timestep/end_of_timestep user function emptiness.  This has
  been fixed.

- Port resolution events on ports of adapters caused C compile error.
  This has been fixed.

- Runtimeability of LSE_show_port_statuses_changes was not happening
  (if declared runtimeable, always ran).  This has been fixed.

- Port queries resolving before dynamic sections didn't fire receiving
  code blocks.  This has been fixed.

- Port queries feeding handlers weren't rescheduling the handlers.
  This has been fixed.

- Top-level data collectors were not getting default emulator
  instances.  This has been fixed.


Changes from corelib-1.0 to corelib-1.0p1
============================================

- combiner - Add a parameter to control whether or not the combine
  userpoint gets the output port ack status.  Previously this data was
  available to the user point.  In the new module, it is parametrically
  controlled, but the default is to NOT provide the ack to the
  userpoint.  This improves scheduling when using this module.
  
- converter - The converter module can now also change the dynamic id
  in addition to the data

- delay - The delay module now has a drop_func user point

- gate - The gate module has a new parameter which controls whether or
  not the gate_control user point should wait for data before being
  invoked.  Previously the module always waited for data.  Now the
  module can be customized so it does not wait for data.  The default
  behavior is the same as before.

- pipe - The pipe module was not properly deleting from the middle of the
  pipe if told to do so by drop_func.  This has been fixed.

- pass_acks_to_enable - The delay, pipe, and mqueue modules have a
  parameter which affects their default control semantics.  If this
  parameter is set to FALSE, the enable signal is calculated from the
  data signal.  If TRUE, the default, the behavior is the same as
  before.



Changes from visualizer-1.0p1 to visualizer-1.0p2
============================================

- Fixed a race condition in the execution management threads, so
  that RPC errors should be much more unlikely to occur.
	
- Implemented a global preferences system to maintain application
  preferences for window geometries and compilation/execution options.

- Added execution options to set the time quantum for timestep and a
  button to run until a certain cycle of execution.



Changes from tomasulodlx-1.0p1 to tomasulodlx-1.0p2
============================================

- Updated to work with corelib-1.0p1

