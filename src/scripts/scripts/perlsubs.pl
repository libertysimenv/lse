# /* 
#  * Copyright (c) 2000-2002 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Useful Perl subroutines
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This file contains useful Perl subroutines that other Perl scripts
#  * in the Liberty tools use.
#  *
#  */

#################### Calling the system routines ######################

# Call system but ignore the return value
sub dosys() {
    $prog = $0; $prog=~s+.*/++;
    print "$prog==> " . join(' ',@_) . "\n";
    system @_;
}

# Call system and die if return value non-zero
sub dosysfail() {
    $prog = $0; $prog=~s+.*/++;
    print "$prog==> " . join(' ',@_) . "\n";
    system (@_) == 0 || die ("\nFailed in " . join(' ',@_) . "\n"); 
}


# Call command in back-ticks and return output
# note: $? is correctly set afterwards... quite a miracle
sub dosysret() {
    $prog = $0; $prog=~s+.*/++;
    $t = "";
    print "$prog==> " . join(' ',@_) . "\n";
           # add error processing as above
           $pid = open(KID_TO_READ, "-|");
           if ($pid) {   # parent
               while (<KID_TO_READ>) {
                   $t .= $_;
               }
               close(KID_TO_READ); 
           } else { 
               exec (@_) || die "$0: can't exec program: $!\n";
               # NOTREACHED
           }
    return $t;
}

############## Translate relative paths to absolute ###################

sub getfullpath() {
    $work = $_[0];
    $given = $_[1];    
    if ($given eq ".") {
	return $work;
    }
    elsif (!($given =~ /^\//)) {
	$given = "$work/$given";
	while (($given =~ /\/\.\//) || ($given =~ /\/\.\.\//)) {
	    $given =~ s+/\./+/+;
	    $given =~ s+/[^/]*/\.\./+/+;
	}
    }
    $given =~ s+/$++;
    return $given;
}

######### generate appropriate quotes for calling a sub-program #######
######### shouldn't be too necessary with the dosys stuff .... ########

sub putquotes() {
    $s = $_[0];
    if ($s =~ /\'/) { 
        # if it has a single quote it must have been double quoted before
	$s =~ s/\\/\\\\/g;
	$s =~ s/"/\\"/g;
	$s =~ s/\$/\\\$/g;
	$s =~ s/`/\\`/g;
        return "\"" . $s . "\"";
    }
    elsif ($s =~ /[^0-9A-Za-z_.\-:\/]/) {
        # if it has anything not normally in a parameter, single quote it
	return "\'" . $s . "\'";
    }
    elsif ($s eq "") {
	return "''";
    } 
    return $s; # otherwise leave it alone
}

################## Create a directory and die if you can't #########

sub createdirifnotthere() {
    $prog = $0; $prog=~s+.*/++;
    $dirname = $_[0];
    if (! -d $dirname) {
	die ("'" . $dirname . "' exists but is not a directory.\n"
	     . "Please remove it.\n")
	    if (-e $dirname);
        print "$prog==> creating directory " . $dirname . "\n";
	mkdir($dirname, 0777) 
	    || die ("Unable to create " . $dirname . " subdirectory.\n");
    }
}
