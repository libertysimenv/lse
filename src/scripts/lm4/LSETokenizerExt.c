/* 
 * Copyright (c) 2003-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Python extension module which implements critical-path parts of the
 *  tokenizer...
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file contains a Python extension model which implements parts
 * of the tokenizer.  We are doing this in C because the
 * tokenizer is really, really useful for generating code, but is also
 * really, really slow when written in Python.
 *
 */

#include "Python.h"
#ifndef PyMODINIT_FUNC
#define PyMODINIT_FUNC void
#endif
#include <string.h>
#ifndef __GNUC__
#define __inline__
#endif
#if (PYTHON_API_VERSION < 1013)
  typedef int Py_ssize_t;
#endif

static PyObject *newline, *emptystring, *emptydictionary,
  *LSEtk_TokenizeException,
  *LSEtk_TokenEmpty,
  *LSEtk_TokenComment,
  *LSEtk_TokenIdentifier,
  *LSEtk_TokenNumber,
  *LSEtk_TokenM4Quote,
  *LSEtk_TokenQuote,
  *LSEtk_TokenOther,
  *LSEtk_TokenRaw,
  *LSEtk_TokenPList,
  *LSEtk_TokenSBList,
  *LSEtk_TokenCBList,
  *LSEtk_TokenPreProc,
  *LSEtk_TokenTopLevel,
  *LSEtk_find_id,
  *LSEtk_invoke_id,
  *_LSEtk_BitBucket,
  *_LSEtk_FileWrapper;

static char *whitespace;

/* NOTE: these need to be kept up to date with Python! */
#define LSEtk_NoArgs       1
#define LSEtk_NoWhiteSpace 2
#define LSEtk_EatLine      4
#define LSEtk_KeepLeandWS  8
#define LSEtk_RequireArgs  16

/*******************************************************************
 *  Input streamers....
 *******************************************************************/

typedef struct {
  int future_ptr;
  PyObject *future;
} LSEtke_streamer_t;

static void LSEtke_streamer_destructor(void *sp) {
  LSEtke_streamer_t *streamer = (LSEtke_streamer_t *)sp;
  Py_XDECREF(streamer->future);
  PyMem_Free(streamer);
}

static __inline__ void 
LSEtke_streamer_dumpState_int(LSEtke_streamer_t *streamer) {
  PyObject *p;
  fprintf(stderr,"%d ",streamer->future_ptr);
  p = PyList_GetSlice(streamer->future,streamer->future_ptr,
		      PyList_Size(streamer->future));
  PyObject_Print(p,stderr,0);
  Py_DECREF(p);
  fprintf(stderr,"\n");
}

static __inline__ void
LSEtke_streamer_prepend_str_int(LSEtke_streamer_t *streamer, 
				PyObject *newsource)
{  
  /* put new source at front */
  if (PyString_Check(newsource)) {
    PySequence_DelSlice(streamer->future,0,streamer->future_ptr);
    PyList_Insert(streamer->future,0,newsource); 
  } else { /* must be a list of strings */
    PyList_SetSlice(streamer->future,0,streamer->future_ptr,newsource);
  }
  streamer->future_ptr = 0;
}

static __inline__ void
LSEtke_streamer_prepend_int(LSEtke_streamer_t *streamer, PyObject *newsource)
{  
  /* put new source at front */
  if (PyString_Check(newsource)) {
    PySequence_DelSlice(streamer->future,0,streamer->future_ptr);
    PyList_Insert(streamer->future,0,newsource); 
  } else { /* must be a list of things to be converted to strings */
    PyObject *tObj;
    int i,l;
    l = PyList_GET_SIZE(newsource);
    tObj = PyList_New(l);
    for (i=0;i<l;i++) {
      PyList_SET_ITEM(tObj,i,PyObject_Str(PyList_GET_ITEM(newsource,i)));
    }
    PyList_SetSlice(streamer->future,0,streamer->future_ptr,tObj);
    Py_DECREF(tObj);
  }
  streamer->future_ptr = 0;
}

static __inline__ LSEtke_streamer_t *
LSEtke_streamer_create_int(PyObject *source)
{
  LSEtke_streamer_t *streamer;

  streamer = (LSEtke_streamer_t *)PyMem_Malloc(sizeof(LSEtke_streamer_t));
  streamer->future = PyList_New(0);
  streamer->future_ptr = 0;

  LSEtke_streamer_prepend_str_int(streamer,source);
  return streamer;
}

static PyObject *
LSEtke_streamer_create(PyObject *self, PyObject *args)
{
  LSEtke_streamer_t *streamer;
  PyObject *source;

  if (!PyArg_ParseTuple(args, "O",&source)) return NULL;

  streamer = LSEtke_streamer_create_int(source);

  return PyCObject_FromVoidPtr(streamer, LSEtke_streamer_destructor);
}

static PyObject *
LSEtke_streamer_prepend(PyObject *self, PyObject *args)
{
  void *streamer;
  PyObject *newsource, *streamerObj;

  if (!PyArg_ParseTuple(args, "OO",&streamerObj,&newsource)) return NULL;
  streamer = PyCObject_AsVoidPtr(streamerObj);

  LSEtke_streamer_prepend_int(streamer,newsource);
  Py_INCREF(Py_None);
  return Py_None;
}

static __inline__ int
LSEtke_streamer_notEmpty_int(LSEtke_streamer_t *streamer) 
{
  return (PyList_GET_SIZE(streamer->future)!= streamer->future_ptr);
}

static PyObject *
LSEtke_streamer_notEmpty(PyObject *self, PyObject *args)
{
  LSEtke_streamer_t *streamer;
  PyObject *streamerObj;

  if (!PyArg_ParseTuple(args, "O",&streamerObj)) return NULL;
  streamer = (LSEtke_streamer_t *)PyCObject_AsVoidPtr(streamerObj);

  return PyInt_FromLong(LSEtke_streamer_notEmpty_int(streamer));
}

/* Fill up to a newline that is not preceeded by a backslash... */

static __inline__ PyObject *
LSEtke_streamer_getLine_int(LSEtke_streamer_t *streamer,
			    char **rets, Py_ssize_t *retslen)
{
  PyObject *ts;
  int fp, mfp;
  char *s,*s2;
  Py_ssize_t size, msize;

  /* discover size */
  mfp = PyList_GET_SIZE(streamer->future);
  fp = streamer->future_ptr;
  msize = 0;

  while (fp != mfp) {
    if (PyString_AsStringAndSize(PyList_GET_ITEM(streamer->future,fp),
				 &s,&size)<0) {
      /* NOTE: this function returns -1 on error, not 0 like most! */
      fprintf(stderr,"arg\n");
      return NULL;
    }
    msize += size;
    fp++;
    if ((s2 = rindex(s,'\n')))
      if (s2 == s || *(s2-1) != '\\') break;
  }
  mfp = fp;

  if (fp == streamer->future_ptr+1) { /* special case for just one line... */
    ts = PyList_GET_ITEM(streamer->future,streamer->future_ptr);
    PyString_AsStringAndSize(ts, rets, retslen);
    Py_INCREF(ts);
    streamer->future_ptr = fp;
    return ts;
  }

  /* allocate it */
  ts = PyString_FromStringAndSize(NULL,msize);
  *rets = s2 = PyString_AsString(ts);
  *retslen = msize;

  /* now fill it */
  fp = streamer->future_ptr;
  while (fp != mfp) {
    PyString_AsStringAndSize(PyList_GET_ITEM(streamer->future,fp),
			     &s, &size);
    strncpy(s2, s, size);
    s2 += size;
    fp ++;
  }
  streamer->future_ptr = fp;
  return ts;
}

static PyObject *
LSEtke_streamer_getLine(PyObject *self, PyObject *args)
{
  LSEtke_streamer_t *streamer;
  PyObject *streamerObj, *rs, *rv;
  Py_ssize_t slen;
  char *s;

  if (!PyArg_ParseTuple(args, "O",&streamerObj)) return NULL;
  streamer = (LSEtke_streamer_t *)PyCObject_AsVoidPtr(streamerObj);

  rs = LSEtke_streamer_getLine_int(streamer,&s,&slen);
  if (!rs) return NULL;
  rv = PyTuple_New(2);
  PyTuple_SET_ITEM(rv,0,rs);   /* steals reference */
  PyTuple_SET_ITEM(rv,1,PyInt_FromLong(slen));  /* steals reference */
  return rv;
}

static PyObject *
LSEtke_streamer_dumpState(PyObject *self, PyObject *args)
{
  LSEtke_streamer_t *streamer;
  PyObject *streamerObj;

  if (!PyArg_ParseTuple(args, "O",&streamerObj)) return NULL;
  streamer = (LSEtke_streamer_t *)PyCObject_AsVoidPtr(streamerObj);

  LSEtke_streamer_dumpState_int(streamer);
  Py_INCREF(Py_None);
  return Py_None;
}

/***********************************************************************
 *  Actual parsing routines 
 ***********************************************************************/

#define MY_DECREF(x) Py_DECREF(x)
#define MY_INCREF(x) Py_INCREF(x)
#define MY_XDECREF(x) Py_XDECREF(x)
#define MY_XINCREF(x) Py_XINCREF(x)

typedef struct LSEtke_tkinfo_s {
  int doM4Quotes;
  int doCQuotes;
  int doM4Comments;
  int doCComments;
  int doMatchBraces;
  int doMatchParens;
  int doDirectives;
  int skipping;
  char *startchars;
  char *endchars;
  char *m4lcomm;
  char *m4lquote;
  int m4lcommlen;
  int m4lquotelen;
  char *m4rcomm;
  char *m4rquote;
  int m4rcommlen;
  int m4rquotelen;
  int expandLevel;
  int debugMode;

  /* curparent's contents must conform to the sequence protocol... */
  PyObject *curparent_contents;
  int curparent_type;
  PyObject *curparent_file;

  PyObject *tinst;
  LSEtke_streamer_t *streamer;
} LSEtke_tkinfo_t;

#define LSEtke_GET_INT_FIELD(rv,o,f) \
  { PyObject *tObj;                          \
    tObj = PyObject_GetAttrString(o,f);      \
    rv = PyInt_AS_LONG(tObj);                \
    MY_DECREF(tObj);                         \
  }

#define LSEtke_SET_INT_FIELD(o,f,nv) \
  { PyObject *tObj = PyInt_FromLong(nv);     \
    PyObject_SetAttrString(o,f,tObj);        \
    MY_DECREF(tObj);                         \
  }

#define LSEtke_GET_STR_FIELD(rv,o,f) \
  { PyObject *tObj;                          \
    tObj = PyObject_GetAttrString(o,f);      \
    rv = PyString_AS_STRING(tObj);           \
    MY_DECREF(tObj);                         \
  }

static int
LSEtke_fill_tkinfo(LSEtke_tkinfo_t *tkinfop, int force) {
  PyObject *flagsObj, *tObj2;
  int changed;

  if (!(flagsObj = PyObject_GetAttrString(tkinfop->tinst,"flags"))) return 0;

  if (!force) {
    LSEtke_GET_INT_FIELD(changed,flagsObj,"changed");
    if (!changed) {
      MY_DECREF(flagsObj);
      return 1;
    }
  }
  LSEtke_GET_INT_FIELD(tkinfop->doM4Quotes, flagsObj, "doM4Quotes");
  LSEtke_GET_INT_FIELD(tkinfop->doCQuotes, flagsObj, "doCQuotes");
  LSEtke_GET_INT_FIELD(tkinfop->doM4Comments, flagsObj, "doM4Comments");
  LSEtke_GET_INT_FIELD(tkinfop->doCComments, flagsObj, "doCComments");
  LSEtke_GET_INT_FIELD(tkinfop->doMatchBraces, flagsObj, "doMatchBraces");
  LSEtke_GET_INT_FIELD(tkinfop->doMatchParens, flagsObj, "doMatchParens");
  LSEtke_GET_INT_FIELD(tkinfop->doDirectives, flagsObj, "doDirectives");
  LSEtke_GET_INT_FIELD(tkinfop->skipping, flagsObj, "ppSkip");

  LSEtke_GET_STR_FIELD(tkinfop->startchars, flagsObj, "startchars");
  LSEtke_GET_STR_FIELD(tkinfop->endchars, flagsObj, "endchars");
  LSEtke_GET_STR_FIELD(tkinfop->m4lcomm, flagsObj, "m4LeftComment");
  LSEtke_GET_STR_FIELD(tkinfop->m4lquote, flagsObj, "m4LeftQuote");
  LSEtke_GET_STR_FIELD(tkinfop->m4rcomm, flagsObj, "m4RightComment");
  LSEtke_GET_STR_FIELD(tkinfop->m4rquote, flagsObj, "m4RightQuote");

  tkinfop->m4lcommlen = strlen(tkinfop->m4lcomm);
  tkinfop->m4lquotelen = strlen(tkinfop->m4lquote);
  tkinfop->m4rcommlen = strlen(tkinfop->m4rcomm);
  tkinfop->m4rquotelen = strlen(tkinfop->m4rquote);

  tObj2 = PyObject_CallMethod(flagsObj, "clearChanged", "");
  MY_DECREF(tObj2);
  MY_DECREF(flagsObj);
  return 1;
}

static __inline__ PyObject *
LSEtke_set_curparent(LSEtke_tkinfo_t *tkinfo, PyObject *curparent)
{
  PyObject *p;
  p = PyObject_GetAttrString(curparent,"contents");
  MY_DECREF(p);
  tkinfo->curparent_contents = p;
  tkinfo->curparent_type = (PyObject_IsInstance(p,_LSEtk_FileWrapper)?0:
			    (PyList_Check(p)?1:
			     PyObject_IsInstance(p,_LSEtk_BitBucket)?2:3));
  if (tkinfo->curparent_type == 0) {
    tkinfo->curparent_file = PyObject_GetAttrString(p, "fileobject");
  }
  return curparent;
}

static __inline__ int
LSEtke_curparent_append(LSEtke_tkinfo_t *tkinfo, PyObject *ni) {
  int rval;

  /* parents can only be lists, files, or bit-buckets */
  switch (tkinfo->curparent_type) {
  case 0: /* file wrapper */
    rval = PyFile_WriteObject(ni, tkinfo->curparent_file, Py_PRINT_RAW);
    break;
  case 1: /* list */
    rval = PyList_Append(tkinfo->curparent_contents, ni);
  case 2: /* bit bucket */
    rval = 0;
    break;
  default: /* unknown */
    {
      PyObject *nl;
      int l = PySequence_Size(tkinfo->curparent_contents);
      nl = PyList_New(1);
      PyList_SET_ITEM(nl,0,ni);
      rval = PySequence_SetSlice(tkinfo->curparent_contents,l,l,nl);
      MY_DECREF(nl);
    }
    break;
  }
  return rval;
}

static __inline__ int
LSEtke_curparent_extend(LSEtke_tkinfo_t *tkinfo, PyObject *ni) {
  int l,i,rval;
  PyObject *p;

  /* parents can only be lists, files, or bit-buckets */
  switch (tkinfo->curparent_type) {
  case 0: /* file wrapper */
    p = PySequence_Fast(ni,NULL);
    if (!p) return 1;
    l = PySequence_Fast_GET_SIZE(p);
    rval = 0;
    for (i=0;i<l;i++) {
      rval = PyFile_WriteObject(PySequence_Fast_GET_ITEM(p,i), 
				tkinfo->curparent_file, Py_PRINT_RAW);
      if (rval) {
	MY_XDECREF(p);
	return rval;
      }
    }
    break;
  case 1: /* list */
    l = PyList_GET_SIZE(tkinfo->curparent_contents);
    rval = PyList_SetSlice(tkinfo->curparent_contents, l,l,ni);
  case 2: /* bit bucket */
    rval = 0;
    break;
  default: /* unknown */
    l = PySequence_Size(tkinfo->curparent_contents);
    rval = PySequence_SetSlice(tkinfo->curparent_contents,l,l,ni);
    break;
  }
  return rval;
}

#define RAISEEXC(s,v) \
{ PyObject *teObj, *earglist; \
  earglist = Py_BuildValue("(sO)",s,v); \
  teObj = PyObject_CallObject(LSEtk_TokenizeException, earglist); \
  PyErr_SetObject(LSEtk_TokenizeException, teObj); \
  MY_DECREF(teObj); \
  MY_DECREF(earglist); \
}

#define RAISEEXCO(s,v) \
{ PyObject *teObj, *earglist; \
  earglist = Py_BuildValue("(NO)",s,v); \
  teObj = PyObject_CallObject(LSEtk_TokenizeException, earglist);	\
  PyErr_SetObject(LSEtk_TokenizeException, teObj); \
  MY_DECREF(teObj); \
  MY_DECREF(earglist); \
}

#define RAISEEXC2(s,v,l) \
{ PyObject *teObj, *earglist; \
  earglist = Py_BuildValue("(sOi)",s,v,l); \
  teObj = PyObject_CallObject(LSEtk_TokenizeException,earglist); \
  PyErr_SetObject(LSEtk_TokenizeException, teObj); \
  MY_DECREF(teObj); \
  MY_DECREF(earglist); \
}

static __inline__ PyObject *
CREATETOK(PyObject *tt, PyObject *cont)
{ // NOTE: old-style classes ONLY
  PyObject *targlist = Py_BuildValue("(O)",cont);
  PyObject *ptok = PyInstance_New(tt,targlist,emptydictionary);
  MY_DECREF(targlist);
  return ptok;
}

/****
 * Update the line number/line number slip counters...
 ****/

static __inline__ int
update_lineno(PyObject *tinst, int amt)
{
  PyObject *iss, *lcount;
  int val;
  iss = PyObject_GetAttrString(tinst,"inputSourceStack");
  lcount = PyList_GetItem(iss,PyList_Size(iss)-1);
  if (!lcount) return 0;
  MY_DECREF(iss);
  val = PyInt_AS_LONG(PyList_GET_ITEM(lcount,1));
  if (val > amt) {
    val -= amt;
    PyList_SetItem(lcount,1,PyInt_FromLong(val));
  } else {
    val = -val;
    val += amt + PyInt_AS_LONG(PyList_GET_ITEM(lcount,0));
    PyList_SetItem(lcount,0,PyInt_FromLong(val));
    PyList_SetItem(lcount,1,PyInt_FromLong(0));
  }
  return 1;
}

static __inline__ int
get_lineno(PyObject *tinst)
{
  PyObject *iss, *lcount;
  iss = PyObject_GetAttrString(tinst,"inputSourceStack");
  lcount = PyList_GetItem(iss,PyList_Size(iss)-1);
  if (!lcount) return -1;
  MY_DECREF(iss);
  return PyInt_AS_LONG(PyList_GET_ITEM(lcount,0));
}

static __inline__ int
count_newlines(PyObject *tObj) {
  PyObject *tObj2;
  int cnt;

  tObj2 = PyObject_CallMethod(tObj, "count", "O", newline);
  cnt = PyInt_AS_LONG(tObj2);
  MY_DECREF(tObj2);
  return cnt;
}

/****
 * Handle an m4 quote (which may be nested)
 ****/
static __inline__ char *
handle_m4_quote(LSEtke_tkinfo_t *tkinfo, char *s, PyObject **sObj) 
{
  int qdepth;
  Py_ssize_t slen;
  char *qend, *qbeg, *pos2;
  PyObject *skeep, *ptokObj, *tObj;
  
  pos2 = s = s + tkinfo->m4lquotelen;
  qdepth = 1;
  qend = strstr(pos2,tkinfo->m4rquote);
  qbeg = strstr(pos2,tkinfo->m4lquote);

  skeep = PyList_New(0);

  /* find the matching quote, taking into account nesting.... */
  while (1) {
    if (qend && (!qbeg || qend < qbeg)) {
      /* eat a quote end */
      qdepth = qdepth - 1;
      if (!qdepth) break;
      pos2 = qend + tkinfo->m4rquotelen;
      qend = strstr(pos2,tkinfo->m4rquote);
    } else if (qbeg && (!qend || qbeg < qend)) {
      /* eat a quote begining */
      qdepth++;
      pos2 = qbeg + tkinfo->m4lquotelen;
      qbeg = strstr(pos2,tkinfo->m4lquote);
    } else {
      /* hmm... we have run out of quotes in our text and
       * there's still an open beginning!  So, let's get
       * some more quotes...
       * we also know that both qend and qbeg are NULL
       *
       * We need to remember what's in the quote, then remove the old line
       * (in that order because of refcounts!), then set up the new line
       */
      tObj = PyString_FromString(s);
      PyList_Append(skeep,tObj);
      MY_DECREF(tObj);
      MY_DECREF(*sObj);
      *sObj = LSEtke_streamer_getLine_int(tkinfo->streamer,&s,&slen);
      if (!*sObj) {
	MY_DECREF(skeep);
	return NULL;
      }
      if (!slen) {
	RAISEEXC("Unterminated m4 quote",tkinfo->tinst);
	MY_DECREF(skeep);
	return NULL;
      }
      pos2 = s;
      qend = strstr(pos2,tkinfo->m4rquote);
      qbeg = strstr(pos2,tkinfo->m4lquote);
    }
  } /* while (1) */
  
  tObj = PyString_FromStringAndSize(s,qend-s);
  PyList_Append(skeep,tObj);
  MY_DECREF(tObj);

  /* skeep is destroyed as a side effect because we use 'N' */
  tObj = PyObject_CallMethod(emptystring,"join","N",skeep);
  if (!tkinfo->skipping) {
    ptokObj = CREATETOK(LSEtk_TokenM4Quote,tObj);
    if (!ptokObj) {
      MY_DECREF(tObj);
      return NULL;
    }
    LSEtke_curparent_append(tkinfo,ptokObj);
    MY_DECREF(ptokObj);
  }
  if (!update_lineno(tkinfo->tinst, count_newlines(tObj))) {
    MY_DECREF(tObj);
    return NULL;
  }
  MY_DECREF(tObj);
  s = qend+tkinfo->m4rquotelen;
  return s;
}

/****
 * Handle an m4 comment 
 ****/
static __inline__ char *
handle_m4_comment(LSEtke_tkinfo_t *tkinfo, char *s, PyObject **sObj) 
{
  Py_ssize_t slen;
  char *pos2, *mind, *npos;
  PyObject *skeep, *ptokObj, *tObj;
  
  pos2 = s + tkinfo->m4lcommlen;
  skeep = PyList_New(0);

  /* find the matching quote, taking into account nesting.... */
  while (1) {
    mind = strstr(pos2,tkinfo->m4rcomm);
    if (mind) break;
    tObj = PyString_FromString(s);
    PyList_Append(skeep,tObj);
    MY_DECREF(tObj);
    MY_DECREF(*sObj);
    *sObj = LSEtke_streamer_getLine_int(tkinfo->streamer,&s,&slen);
    if (!*sObj) {
      MY_DECREF(skeep);
      return NULL;
    }
    if (!slen) {
      RAISEEXC("Unterminated m4 comment",tkinfo->tinst);
      MY_DECREF(skeep);
      return NULL;
    }
    pos2 = s;
  } /* while (1) */
  
  npos = mind + tkinfo->m4rcommlen;
  tObj = PyString_FromStringAndSize(s,npos-s);
  PyList_Append(skeep,tObj);
  MY_DECREF(tObj);

  /* skeep is destroyed as a side effect because we use 'N' */
  tObj = PyObject_CallMethod(emptystring,"join","N",skeep);
  if (!tkinfo->skipping) {
    ptokObj = CREATETOK(LSEtk_TokenComment,tObj);
    if (!ptokObj) {
      MY_DECREF(tObj);
      return NULL;
    }
    LSEtke_curparent_append(tkinfo,ptokObj);
    MY_DECREF(ptokObj);
  }
  if (!update_lineno(tkinfo->tinst, count_newlines(tObj))) {
    MY_DECREF(tObj);
    return NULL;
  }
  MY_DECREF(tObj);
  return npos;
}

/****
 * Handle a C quote 
 ****/
static __inline__ char *
handle_C_quote(LSEtke_tkinfo_t *tkinfo, 
	       char *s,
	       PyObject **sObj,
	       char tchar)
{
  char *mind, *npos, *pos2;
  PyObject *skeep, *ptokObj, *tObj;
  int olcount;
  Py_ssize_t slen;

  olcount = get_lineno(tkinfo->tinst);
  pos2 = s + 1;
  skeep = PyList_New(0);

  while (1) {

    mind = index(pos2,tchar);
    if (mind) {
      if (mind > pos2 && *(mind-1) == '\\') {
	pos2 = mind + 1;
	continue;
      } else break;
    }
    /* remember string */
    tObj = PyString_FromString(s);
    PyList_Append(skeep,tObj);
    MY_DECREF(tObj);
    MY_DECREF(*sObj);
    *sObj = LSEtke_streamer_getLine_int(tkinfo->streamer,&s,&slen);
    if (!*sObj) {
      MY_DECREF(skeep);
      return NULL;
    }
    if (!slen) {
      RAISEEXC2("Unterminated string",tkinfo->tinst,olcount);
      MY_DECREF(skeep);
      return NULL;
    }
    pos2 = s;
  } /* while (1) */
 
  npos = mind + 1;
  tObj = PyString_FromStringAndSize(s,npos - s);
  PyList_Append(skeep,tObj);
  MY_DECREF(tObj);

  /* skeep is destroyed as a side effect because we use 'N' */
  tObj = PyObject_CallMethod(emptystring,"join","N",skeep);
  if (!tkinfo->skipping) {
    ptokObj = CREATETOK(LSEtk_TokenQuote,tObj);
    if (!ptokObj) {
      MY_DECREF(tObj);
      return NULL;
    }
    LSEtke_curparent_append(tkinfo,ptokObj);
    MY_DECREF(ptokObj);
  }

  if (!update_lineno(tkinfo->tinst, count_newlines(tObj))) {
    MY_DECREF(tObj);
    return NULL;
  }

  MY_DECREF(tObj);
  return npos;
}

/****
 * Handle a C comment beginner
 ****/
static __inline__ char * 
handle_C_comment(LSEtke_tkinfo_t *tkinfo, 
		 char *s,
		 PyObject **sObj,
		 int *ctype)
{
  char *pos2, *mind, *npos;
  Py_ssize_t slen;
  PyObject *skeep, *ptokObj, *tObj;

  if (*(s+1)=='*') { /* C comment */
    pos2 = s + 2;
    skeep = PyList_New(0);

    while (1) {
      mind = strstr(pos2,"*/");
      if (mind) break;
      tObj = PyString_FromString(s);
      PyList_Append(skeep,tObj);
      MY_DECREF(tObj);
      MY_DECREF(*sObj);
      *sObj = LSEtke_streamer_getLine_int(tkinfo->streamer,&s,&slen);
      if (!*sObj) {
	MY_DECREF(skeep);
	return NULL;
      }
      if (!slen) {
	RAISEEXC("Unterminated /*",tkinfo->tinst);
	MY_DECREF(skeep);
	return NULL;
      }
      pos2 = s;
    } /* while (1) */

    npos = mind + 2;

    tObj = PyString_FromStringAndSize(s,npos - s);
    PyList_Append(skeep,tObj);
    MY_DECREF(tObj);

    /* skeep is destroyed as a side effect because we use 'N' */
    tObj = PyObject_CallMethod(emptystring,"join","N",skeep);
    if (!tkinfo->skipping) {
      ptokObj = CREATETOK(LSEtk_TokenComment,tObj);
      if (!ptokObj) {
	MY_DECREF(tObj);
	return NULL;
      }
      LSEtke_curparent_append(tkinfo,ptokObj);
      MY_DECREF(ptokObj);
    }
    if (!update_lineno(tkinfo->tinst, count_newlines(tObj))) {
      MY_DECREF(tObj);
      return NULL;
    }
    *ctype = 0;

    MY_DECREF(tObj);
    return npos;
  } /* C comments */

  else if ((*(s+1)) == '/') { /* CPP comments */
    
    pos2 = index(s,'\n');
    if (!pos2) pos2 = s + strlen(s);
    else {
      update_lineno(tkinfo->tinst, 1);
      pos2++;
    }
    
    tObj = PyString_FromStringAndSize(s,pos2 - s);
    if (!tkinfo->skipping) {
      ptokObj = CREATETOK(LSEtk_TokenComment,tObj);
      if (!ptokObj) {
	MY_DECREF(tObj);
	return NULL;
      }
      LSEtke_curparent_append(tkinfo,ptokObj);
      MY_DECREF(ptokObj);
    }
    MY_DECREF(tObj);
    *ctype = 1;
    return pos2;
  } 

  else { /* just a / by itself... */
    if (!tkinfo->skipping) { 
      tObj = PyString_FromStringAndSize(s,1);
      ptokObj = CREATETOK(LSEtk_TokenOther,tObj);
      if (!ptokObj) {
	MY_DECREF(tObj);
	return NULL;
      }
      LSEtke_curparent_append(tkinfo,ptokObj);
      MY_DECREF(ptokObj);
      MY_DECREF(tObj);
    }
    s++;
    *ctype = 2;
    return s;
  }
} /* handle_C_comment */

static __inline__ int
get_divno(PyObject *tinst) 
{
  PyObject *tObj;
  int rval;
  tObj = PyObject_GetAttrString(tinst,"currDivertNo");
  rval = PyInt_AS_LONG(tObj);
  MY_DECREF(tObj);
  return rval;
}

/***********************************************************************
 * And the big boy routine to tokenize a string/list of strings....
 ************************************************************************/

#define SET_curparent(np) { \
  curparent = LSEtke_set_curparent(&tkinfo,np); \
}

static PyObject *
LSEtke_tokenize_string_int(PyObject *self, PyObject *args) 
{
  /* input parameters */
  PyObject *sObj, *dpathObj, *curparent;
  int gathering, inpreproc;

  char terminator, c;
  PyObject *idtok, *olcount, *thistok, *idarglist;
  PyObject *tObj, *ptokObj, *envStack;
  LSEtke_streamer_t *streamer;

  LSEtke_tkinfo_t tkinfo;
  int savedivno;
  Py_ssize_t slen;
  char *s;  /* NOTE: points to a string that should be considered immutable */

  /* 
    def _LSEtk_tokenize_string_int(tinst, s, dpath, curparent, gathering=0,
                                   inpreproc=0):
    # tinst is the tokenizer instance
    # s is the input string
    # dpath is the dictionary list
    # curparent is the token to add input to
    # gathering is flag indicating that we're gathering arguments
    # inpreproc is flag indicating that we're in the preprocessor
  */

  gathering = 0; inpreproc = 0;

  if (!PyArg_ParseTuple(args, "OOOO|ii", 
			&tkinfo.tinst, &sObj, &dpathObj, &curparent, 
			&gathering, &inpreproc)) return NULL; /* borrowed! */

  /****************************
   * I will maintain references on....
   *
   * curparent,
   * olcount,
   * sObj (after creating the streamer)
   * envStack
   * idtok
   * idarglist
   **********/

  MY_INCREF(curparent);

  olcount = Py_None;  MY_INCREF(olcount);

  streamer = LSEtke_streamer_create_int(sObj);
  sObj = NULL; /* in case of error exit before we can set it */
  idarglist = NULL;
  idtok = NULL;
  terminator = 0;

  /* sentinel value 
   * for envStack, we do not transfer our references...
   */
  envStack = PyList_New(1);
  PyList_SET_ITEM(envStack,0,
		  Py_BuildValue("(iOiOi)",0,curparent,0,
				curparent,get_divno(tkinfo.tinst)));

  
  /* Set up to run... */

  LSEtke_fill_tkinfo(&tkinfo,1);
  tkinfo.streamer = streamer;
  LSEtke_GET_INT_FIELD(tkinfo.expandLevel, tkinfo.tinst, "expandLevel");

  /***************** 
   * Safe to do error exits after this point; do not move
   * anything from above it below it!!!
   ********************/

  LSEtke_set_curparent(&tkinfo,curparent);

  /* sObj is now the string s; we keep the reference so we do not lose
   * the buffer from under us!
   */
  sObj = LSEtke_streamer_getLine_int(streamer,&s,&slen);
  if (!sObj) goto error;

  /********************** THE MAIN LOOP *************************/

  /* skipping logic:
   *    while we are skipping, we do not evaluate anything but pre-processor
   *    directives.  However, we still need to do quote and comment matching
   *    and it is not a bad idea to move forward by whole tokens...
   */

  while (*s || LSEtke_streamer_notEmpty_int(streamer)) { 

    /* invariant: idtok == None */

    while ((c = *s)) {

      /* search for various things.....  break once idtok found or
       * we pop back to having an idtok
       */
            
      /*
       * start with m4 quotes and comments, as they can override anything
       */

      if (tkinfo.doM4Quotes && 
	  !strncmp(s,tkinfo.m4lquote,tkinfo.m4lquotelen)) {
	s = handle_m4_quote(&tkinfo,s,&sObj);
	if (!s) goto error;

      } /* m4 left quote handling */

      else if (tkinfo.doM4Comments && 
	       !strncmp(s,tkinfo.m4lcomm,tkinfo.m4lcommlen)) {
	s = handle_m4_comment(&tkinfo,s,&sObj);
	if (!s) goto error;

      } /* m4 left comment handling */

      /*
       * line continuation has to go here so that we do not mess up
       * line continuations in pre-processor directives
       *
       * but it is kind of weird, so I'm going to turn it off for now..
       */
      
      else if (c == '\\' && (tkinfo.doCComments || tkinfo.doDirectives) &&
	       *(s+1) == '\n') {
	update_lineno(tkinfo.tinst,1);
	if (!tkinfo.skipping) { /* turn continuation into a space */
	  tObj = PyString_FromStringAndSize(s,2);     /* +tObj */
	  ptokObj = CREATETOK(LSEtk_TokenEmpty,tObj); /* +ptokObj, +tObj */
	  MY_DECREF(tObj);
	  if (!ptokObj) goto error;
	  LSEtke_curparent_append(&tkinfo,ptokObj); /* +ptokObj */
	  MY_DECREF(ptokObj);
	  /* exit with ptokObj +1, tObj +1 */
	}
	s+= 2;
      }
      
      /*
       * now look for terminators; putting it here allows whitespace
       * to be a terminator....
       */
      else if (c == terminator) {
	PyObject *thistok;
	int dummy;

	s++;

	if (terminator == '\n') {
	  update_lineno(tkinfo.tinst,1);
	  inpreproc = 0;
	}
	if (tkinfo.skipping) continue;

	/* cannot just save curparent and use as thistok because
	 * curparent might have got diverted to something else!
	 */
	tObj = PyList_GET_ITEM(envStack,
			       PyList_GET_SIZE(envStack)-1); /* borrowed */
	MY_DECREF(curparent); 
	MY_DECREF(olcount);   
	PyArg_Parse(tObj,"(iObOiO)", &dummy,
		    &curparent, &terminator, &thistok,  /* borrowed */
		    &savedivno, &olcount);	
	MY_INCREF(curparent);
	MY_INCREF(olcount);
	MY_INCREF(thistok);
	PySequence_DelItem(envStack,-1); /* -thistok, -curparent, -olcount */

	LSEtke_set_curparent(&tkinfo,curparent);

	/* put token in old diversion */
	LSEtke_curparent_append(&tkinfo,thistok); /* +thistok */

	if (!gathering) {
	  int newdivno = get_divno(tkinfo.tinst);
	  if (savedivno != newdivno) {
	    PyObject *divlist = PyObject_GetAttrString(tkinfo.tinst,
						       "divertList");
	    MY_DECREF(curparent);
	    SET_curparent(PyList_GetItem(divlist,newdivno));
	    MY_DECREF(divlist);
	    MY_INCREF(curparent);
	  }
	}
	else {
	  tObj = PyList_GET_ITEM(envStack,PyList_GET_SIZE(envStack)-1);
	  if (PyInt_AS_LONG(PyTuple_GET_ITEM(tObj,0)) == 2) {
	    /* we have reached an id push */
	    MY_XDECREF(idarglist);
	    idarglist = PyObject_GetAttrString(thistok,"contents"); /* +1 */
	    gathering = 0;
	    MY_XDECREF(idtok);
	    /* safe to treat as list because we are gathering, therefore
	     * curparent must be a list
	     */
	    idtok = PyList_GetItem(tkinfo.curparent_contents,0); 
	    MY_INCREF(idtok);
	    MY_DECREF(thistok);

	    break; /* head for the macro expansion */
	  }

	}
	MY_DECREF(thistok);

      } /* if (c == terminator) */

      /*
       * whitespace is now good to look for
       */
      else if (isspace(c)) {
	size_t len, len2;
	len = strspn(s,whitespace);
	if (terminator && isspace(terminator)) {
	  char *s2;
	  s2 = index(s,terminator);
	  if (s2) {
	    len2 = s2-s;
	    if (len2 < len) len = len2;
	  }
	}
	tObj = PyString_FromStringAndSize(s,len);

	update_lineno(tkinfo.tinst,count_newlines(tObj));

	if (!tkinfo.skipping) { 
	  ptokObj = CREATETOK(LSEtk_TokenEmpty,tObj);
	  if (!ptokObj) {
	    MY_DECREF(tObj);
	    goto error;
	  }
	  LSEtke_curparent_append(&tkinfo,ptokObj);
	  MY_DECREF(ptokObj);
	}
	  
	MY_DECREF(tObj);
	s+= len;
      }
      /*
       * start of id....
       */
      else if (isalpha(c) || c=='_' || c=='@') {
	PyObject *identry, *targList;
	
	int len = strspn(s+1,"abcdefghijklmnopqrstuvwxyz"
			 "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_")+1;

	if (tkinfo.skipping) {
	  s += len;
	  continue;
	}
	tObj = PyString_FromStringAndSize(s,len);
	MY_XDECREF(idtok);
	idtok = CREATETOK(LSEtk_TokenIdentifier,tObj);
	MY_DECREF(tObj);

	targList = Py_BuildValue("(OO)",tObj, dpathObj);
	identry = PyEval_CallObject(LSEtk_find_id,targList);
	MY_DECREF(targList);
	if (!identry) goto error;

	if (identry != Py_None) {
	  int entryflags;

	  s += len;
	  MY_XDECREF(idarglist);
	  idarglist = NULL;
	  
	  tkinfo.expandLevel ++;

	  {
	    int debugmode;
	    LSEtke_GET_INT_FIELD(debugmode, tkinfo.tinst, "debugMode");
	    if (debugmode) {
	      PyObject *debugFile = PyObject_GetAttrString(tkinfo.tinst,
							   "debugFile");
	      fprintf(PyFile_AsFile(debugFile),"-%d- %s ...\n",
		      tkinfo.expandLevel, PyString_AsString(tObj));
	      MY_DECREF(debugFile);
	    }
	  }

	  /* transfer current curparent into stack; but increment ptokObj,
	   * which will become new curparent
	   *
	   * also transfer identry into it....
	   */
	  tObj = Py_BuildValue("(iNiOiN)",2,curparent,get_divno(tkinfo.tinst),
	  		       idtok,gathering,identry);
	  PyList_Append(envStack,tObj);
	  MY_DECREF(tObj);

	  tObj = PyList_New(1);
	  PyList_SET_ITEM(tObj,0,idtok);
	  MY_INCREF(idtok);
	  SET_curparent(CREATETOK(LSEtk_TokenTopLevel,tObj));
	  MY_DECREF(tObj);

	  entryflags = PyInt_AS_LONG(PyTuple_GET_ITEM(identry,1));

	  if (entryflags & LSEtk_EatLine) {
	    char *pos2;

	    pos2 = strstr(s,"\n");
	    if (!pos2) {
	      PyObject *tObj2;

	      tObj = PyString_FromString(s);
	      tObj2 = CREATETOK(LSEtk_TokenRaw,tObj);
	      MY_DECREF(tObj);
	      MY_XDECREF(idarglist);
	      idarglist = PyList_New(1);
	      PyList_SET_ITEM(idarglist,0,tObj2);
	      s = s + strlen(s);
	    } else {
	      PyObject *tObj2;

	      tObj = PyString_FromStringAndSize(s,(pos2-s)+1);
	      tObj2 = CREATETOK(LSEtk_TokenRaw,tObj);
	      MY_DECREF(tObj);
	      MY_XDECREF(idarglist);
	      idarglist = PyList_New(1);
	      PyList_SET_ITEM(idarglist,0,tObj2);
	      s = pos2 + 1;
	    }
	    
	    tObj = CREATETOK(LSEtk_TokenPList,idarglist);
	    LSEtke_curparent_append(&tkinfo,tObj);
	    MY_DECREF(tObj);
	    update_lineno(tkinfo.tinst, 1);
	    gathering = 0;

	  } else if ( (entryflags & LSEtk_NoArgs) || \
		      ((entryflags & LSEtk_NoWhiteSpace) && \
		       strspn(s,whitespace))) {
	    /* handle 0-argument macros and macros not allowing
	     * whitespace before arguments which actually have
	     * whitespace
	     */
	    gathering = 0;
	  } else {
	    /* we are searching for arguments, so create a
	     * new parent token and continue
	     */
	    gathering = 1;
	  }

	  break; /* go to token handling */

	} else { /* skip over id and do not expand */
	  if (c=='@') {
	    /* this case is so that we can insert internal
	     * macros without them merging with previous
	     *  identifiers.  One place we use this is
	     * when popping file names
	     */
	    if (!tkinfo.skipping) {
	      tObj = PyString_FromStringAndSize(s,1);
	      ptokObj = CREATETOK(LSEtk_TokenOther,tObj);
	      if (!ptokObj) {
		MY_DECREF(tObj);
		goto error;
	      }
	      LSEtke_curparent_append(&tkinfo,ptokObj);
	      MY_DECREF(ptokObj);
	      MY_DECREF(tObj);
	    }
	    MY_DECREF(idtok);
	    idtok = NULL;
	    s++;
	  } else {
	    if (!tkinfo.skipping) {
	      LSEtke_curparent_append(&tkinfo,idtok);
	    }
	    MY_DECREF(idtok);
	    idtok = NULL;
	    s += len;
	  }
	} /* else skip over id */
      } /* identifier */

      /*
       * start of number
       *     match ([0-9]([eE][+-]|[\w.])*)
       */
      else if (isdigit(c)) {
	char *sp;
	sp = s;

	while (++sp) {
	  c = *sp;
	  if (c=='e'||c=='E') {
	    if (*(sp+1)=='+' || *(sp+1)=='-') {
	      sp++;
	    }
	    continue;
	  }
	  if (isalnum(c) || c=='_') continue;
	  break;
	}

	tObj = PyString_FromStringAndSize(s,sp-s);
	if (!tkinfo.skipping) { 
	  ptokObj = CREATETOK(LSEtk_TokenNumber,tObj);
	  if (!ptokObj) {
	    MY_DECREF(tObj);
	    goto error;
	  }
	  LSEtke_curparent_append(&tkinfo,ptokObj);
	  MY_DECREF(ptokObj);
	}
	MY_DECREF(tObj);
	s=sp;
      }

      /*
       * C comments and quotes
       */

      else if (c=='"' && tkinfo.doCQuotes) {
	s = handle_C_quote(&tkinfo,s,&sObj,'"');
	if (!s) goto error;
      } 

      else if (c == '\'' && tkinfo.doCQuotes) {
	s = handle_C_quote(&tkinfo,s,&sObj,'\'');
	if (!s) goto error;
      }

      else if (c == '/' && tkinfo.doCComments) {
	int ctype;
	s = handle_C_comment(&tkinfo,s,&sObj,&ctype);
	if (!s) goto error;
      } /* possible C comments */

      /*
       * preprocessor
       */

      else if (c == '#' && tkinfo.doDirectives && ! inpreproc) {
	char *ibeg = s++;
	char *iend;
	PyObject *identry, *targList;
	PyObject *tname = NULL;

	do {
	  c = *(++ibeg);
	} while (isspace(c) && c != '\n'); /* skip over the whitespace */
	iend = ibeg;
	if (isalpha(c) || c=='_') {
	  do {
	    c = *(++iend);
	  } while (isalnum(c) || c=='_'); /* get to end of character... */


	  tname = PyString_FromString("#");
	  PyString_ConcatAndDel(&tname,
				PyString_FromStringAndSize(ibeg,iend-ibeg));

	  targList = Py_BuildValue("(OO)",tname, dpathObj);
	  identry = PyEval_CallObject(LSEtk_find_id,targList);
	  MY_DECREF(targList);
	  if (!identry) goto error;
	  if (identry == Py_None) {
	    identry=NULL;
	    MY_DECREF(Py_None);
	  }
	} else {
	  identry = NULL;
	}

	if (!identry) {
	  tObj = PyList_New(0);
	  ptokObj = CREATETOK(LSEtk_TokenPreProc,tObj);
	  MY_DECREF(tObj);

	  if (!tkinfo.skipping) {

	    tObj = Py_BuildValue("(iNbOiO)",
				 1,curparent,terminator,ptokObj,
				 get_divno(tkinfo.tinst),
				 olcount);
	    PyList_Append(envStack,tObj);
	    MY_DECREF(tObj);
	    terminator = '\n';
	    
	    /* TODO: olcount = copy.deepcopy(tinst.inputSourceStack)
	    */

	    SET_curparent(ptokObj);

	    inpreproc = 1;
	  }
	} else {

	  PyObject *argstring;
	  char *pos2;

	  MY_XDECREF(idtok);
	  idtok = CREATETOK(LSEtk_TokenIdentifier,tname);

	  s = iend;

	  MY_XDECREF(idarglist);
	  idarglist = PyList_New(1);

	  /* find the true end of line... */
	  pos2 = s;
	  while ((*pos2 != '\n' || *(pos2-1) == '\\') && *pos2) pos2++;

	  argstring = PyString_FromStringAndSize(s,pos2-s);

	  PyList_SET_ITEM(idarglist,0,CREATETOK(LSEtk_TokenRaw,argstring));
	  
	  tkinfo.expandLevel ++;
	  
	  {
	    int debugmode;
	    LSEtke_GET_INT_FIELD(debugmode, tkinfo.tinst, "debugMode");
	    if (debugmode) {
	      PyObject *debugFile = PyObject_GetAttrString(tkinfo.tinst,
							   "debugFile");
	      fprintf(PyFile_AsFile(debugFile),"-%d- %s ...\n",
		      tkinfo.expandLevel, PyString_AsString(tname));
	      MY_DECREF(debugFile);
	    }
	  }

	  tObj = Py_BuildValue("(iNiOiN)",
			       2,curparent,
			       get_divno(tkinfo.tinst),
			       idtok, gathering, identry);
	  PyList_Append(envStack,tObj);
	  MY_DECREF(tObj);

	  tObj = PyList_New(2);
	  SET_curparent(CREATETOK(LSEtk_TokenTopLevel,tObj));
	  PyList_SET_ITEM(tObj,0,idtok);
	  MY_INCREF(idtok);
	  PyList_SET_ITEM(tObj,1,CREATETOK(LSEtk_TokenPList,idarglist));
	  MY_DECREF(tObj);
	  
	  MY_DECREF(tname);
	  update_lineno(tkinfo.tinst,count_newlines(argstring)+1);
	  MY_DECREF(argstring);

	  gathering = 0;

	  if (*pos2) s = pos2 + 1;
	  else s = pos2;

	  break;
             
	}
      } /* preprocessor */

      /*
       * start characters
       */
      else if ((index(tkinfo.startchars,c) ||
		(c == '(' && gathering)) && !tkinfo.skipping) {
	PyObject *tclass;
	char newterm;
	switch (c) {
	case '(': newterm = ')'; tclass = LSEtk_TokenPList; break;
	case '[': newterm = ']'; tclass = LSEtk_TokenSBList; break;
	case '{': newterm = '}'; tclass = LSEtk_TokenCBList; break;
	default: newterm = 0; tclass = NULL; break;
	}
	tObj = PyList_New(0);
	ptokObj = CREATETOK(tclass,tObj);
	MY_DECREF(tObj);
	/* transfer current curparent into stack; but increment ptokObj,
	 * which will become new curparent
	 */
	tObj = Py_BuildValue("(iNbOiO)",
			     1,curparent,terminator,ptokObj,
			     get_divno(tkinfo.tinst),
			     olcount);
	PyList_Append(envStack,tObj);
	MY_DECREF(tObj);
	terminator = newterm;
	SET_curparent(ptokObj);
	
	s++;
	/* TODO:
                olcount = copy.deepcopy(tinst.inputSourceStack)
                #sys.stderr.write("In %d %s\n" % (len(envStack),s[pos:pos+5]))
	*/
      }

      /*
       * bad terminators
       */
      else if (index(tkinfo.endchars,c) && !tkinfo.skipping) {
	if (terminator) {
	  RAISEEXCO(PyString_FromFormat("Extra '%c' when expecting a "
				      "'%c' ",c,terminator),tkinfo.tinst);
	  goto error;
	} else {
	  RAISEEXCO(PyString_FromFormat("Extra '%c'",c),tkinfo.tinst);
	  goto error;
	}
      }

      /*
       * other
       */
       else {
	 if (!tkinfo.skipping) { 
	   tObj = PyString_FromStringAndSize(s,1);
	   ptokObj = CREATETOK(LSEtk_TokenOther,tObj);
	   if (!ptokObj) {
	     MY_DECREF(tObj);
	     goto error;
	   }
	   LSEtke_curparent_append(&tkinfo,ptokObj);
	   MY_DECREF(ptokObj);
	   MY_DECREF(tObj);
	 }
	 s++;
       } /* and we've now covered all the character cases */

    } /* while (c = *s) */

    /* ##### INNER WHILE LOOP (pos < slen) NOW OVER */

    if (!*s) { /* while loop else condition: we ran out of this string */

      MY_DECREF(sObj);
      sObj = LSEtke_streamer_getLine_int(streamer,&s,&slen);
      if (!sObj) goto error;
    } /* ran out of string */

    if (gathering) {
      /*
       * anything allowed between an id and an argument list should be
       * found in this section, which is just C comments and whitespace
       */
      while (( c = *s)) {

	/*
	 * whitespace
	 */

	if (isspace(c)) {
	  size_t len;
	  len = strspn(s,whitespace);
	  tObj = PyString_FromStringAndSize(s,len);
	  
	  update_lineno(tkinfo.tinst,count_newlines(tObj));
	  
	  if (!tkinfo.skipping) { 
	    ptokObj = CREATETOK(LSEtk_TokenEmpty,tObj);
	    if (!ptokObj) {
	      MY_DECREF(tObj);
	      goto error;
	    }
	    LSEtke_curparent_append(&tkinfo,ptokObj);
	    MY_DECREF(ptokObj);
	  }
	  
	  MY_DECREF(tObj);
	  s+= len;
	  continue;
	}

	/* 
	 * open parenthesis... push the environment, set idtok to None
	 * and go back to the top loop....
	 */

	if (c == '(') {
	  PyObject *tclass = LSEtk_TokenPList;
	  char newterm = ')';
	  tObj = PyList_New(0);
	  ptokObj = CREATETOK(tclass,tObj);
	  MY_DECREF(tObj);
	  /* transfer current curparent into stack; but increment ptokObj,
	   * which will become new curparent
	   */
	  tObj = Py_BuildValue("(iNbOiO)",
			       1,curparent,terminator,ptokObj,
			       get_divno(tkinfo.tinst),
			       olcount);
	  PyList_Append(envStack,tObj);
	  MY_DECREF(tObj);
	  terminator = newterm;
	  SET_curparent(ptokObj);
	  MY_XDECREF(idtok);
	  idtok = NULL;

	  s++;
	  /* TODO:
	     olcount = copy.deepcopy(tinst.inputSourceStack)
	  */
	  break;  /* important... want to go back through outer while loop */
	}

	/*
	 * C comments
	 */

	if (c == '/' && tkinfo.doCComments) {
	  int ctype;
	  s = handle_C_comment(&tkinfo,s,&sObj,&ctype);
	  if (!s) goto error;
	  if (ctype != 2) continue;
	  else break;
	} /* possible C comments */
	
	/* anything else, get out of here.... */

	break;

      } /* while... */
    } /* if gathering */

    if (idtok) { /* finally, expand an id... */

      int entryflags;
      PyObject *identry;

      /* safe because a new diversion cannot have happened between 
       * the time we set curparent to the id's tree 
       */
      thistok = curparent;
      { 
	int dummy;
	tObj = PyList_GET_ITEM(envStack,PyList_GET_SIZE(envStack)-1);
	MY_DECREF(idtok);
	PyArg_Parse(tObj,"(iOiOiO)", &dummy,
		    &curparent, &savedivno, &idtok,
		    &gathering, &identry);
	MY_INCREF(curparent);
	MY_INCREF(identry);
	MY_INCREF(idtok);
	SET_curparent(curparent);
      }
      PySequence_DelItem(envStack,-1); /* -curparent, -idtok, -identry */

      /*  did we not get args when we needed them? */
      
      entryflags = PyInt_AS_LONG(PyTuple_GET_ITEM(identry,1));
      if ((entryflags & LSEtk_RequireArgs) && !idarglist) {

	/* actually put the id on as an id with whatever was there.... */
	tObj = PyObject_GetAttrString(thistok,"contents");

#ifdef OLD
	PySequence_SetSlice(tkinfo.curparent_contents,
			    PySequence_Length(tkinfo.curparent_contents),
			    PySequence_Length(tkinfo.curparent_contents),
			    tObj);
#else
	LSEtke_curparent_extend(&tkinfo,tObj);
#endif
	MY_DECREF(tObj);

	/*
	 * we will not have changed diversion, so there is nothing
         * to do for that...
	 */
	MY_DECREF(thistok); /* former curparent ref now gone */
	MY_DECREF(idtok);
	idtok = NULL;
	tkinfo.expandLevel--;

	/* (no need to put s back into the future) */

      } else {
	PyObject *wslist, *targList, *rval;

	/* if we did not get any arguments, we had better put back
	 * the whitespace!  We also need to "uncount" any newlines
	 * in it.... 
	 */
	if (!idarglist) {
	  tObj = PyObject_GetAttrString(thistok,"contents");
	  wslist = PyList_GetSlice(tObj,1,PyList_GET_SIZE(tObj));
	  MY_DECREF(tObj);
	} else {
	  wslist = PyList_New(0);
	}

	if (!idarglist) {
	  idarglist = Py_None; 
	  MY_INCREF(Py_None);
	}

	/* now do the invocation... */

	/* need to make tinst.expandLevel visible.... */
	LSEtke_SET_INT_FIELD(tkinfo.tinst, "expandLevel", tkinfo.expandLevel);
	
	tObj = PyObject_GetAttrString(idtok,"contents");

	targList = Py_BuildValue("(OOOOOOO)",tkinfo.tinst, tObj, identry,
				 idarglist, dpathObj, thistok, envStack);
#ifdef HMM
	fprintf(stderr,"========");
	PyObject_Print(tkinfo.tinst,stderr,0);
	PyObject_Print(tObj,stderr,0);
	PyObject_Print(identry,stderr,0);
	PyObject_Print(idarglist,stderr,0);
	PyObject_Print(thistok,stderr,0);
	PyObject_Print(envStack,stderr,0);
	fprintf(stderr,"==\n");
#endif

	rval = PyEval_CallObject(LSEtk_invoke_id,targList);
	MY_DECREF(targList);
	MY_DECREF(tObj);
	if (!rval) {
	  MY_DECREF(thistok); /* curparent ref now gone */
	  MY_DECREF(identry);
	  MY_DECREF(wslist);
	  goto error;
	}

	tkinfo.expandLevel--;

	/* ... handle diversion changes */
	if (!gathering){
	  int newdivno = get_divno(tkinfo.tinst);
	  if (savedivno != newdivno) {
	    PyObject *divlist = PyObject_GetAttrString(tkinfo.tinst,
						       "divertList");
	    MY_DECREF(curparent);
	    SET_curparent(PyList_GetItem(divlist,newdivno));
	    MY_DECREF(divlist);
	    MY_INCREF(curparent);
	  }
	}

	/* ... and splice in .... */
	if (PyString_Check(rval)) { /* insert text */
	  if (PyString_Size(rval)) { /* not empty */

	    /* put everything on the line after the id and args
	     * back into the future... */
	    if (*s) {
	      tObj = PyString_FromString(s);
	      LSEtke_streamer_prepend_int(streamer,tObj);
	      MY_DECREF(tObj);
	    }

	    /* and whitespace after the id (if there were no args) */
	    LSEtke_streamer_prepend_int(streamer,wslist);

	    /* TODO:
                        # fix up the line numbers..
                        tinst.inputSourceStack[-1][1] += (
                            reduce(lambda x,y:x+str(y).count("\n"),wslist,0)+
                            rval.count("\n"))
	    */

	    /* and finally put in the return value */
	    LSEtke_streamer_prepend_int(streamer,rval);

	    /* and get a whole line to continue with.... */
	    MY_DECREF(sObj);
	    sObj = LSEtke_streamer_getLine_int(streamer,&s,&slen);
	    if (!sObj) {
	      MY_DECREF(thistok); /* curparent ref now gone */
	      MY_DECREF(identry);
	      MY_DECREF(wslist);
	      MY_DECREF(rval);
	      goto error;
	    }
	  } else {
	    /* put away the whitespace if there is some... */
	    if (PyList_GET_SIZE(wslist)) {
	      LSEtke_curparent_extend(&tkinfo,wslist);
	    }
	    /* leave s alone */
	  }

	} else { /* a list of tokens */

	  /* TODO: ugh... a real mess.... */
	  if (rval != Py_None) {
	      int l = PySequence_Size(tkinfo.curparent_contents);
	      PySequence_SetSlice(tkinfo.curparent_contents,l,l,rval);
	      if (PyList_GET_SIZE(wslist)) {
		l += PyList_GET_SIZE(rval);
		PySequence_SetSlice(tkinfo.curparent_contents,l,l,wslist);
	      }
	  } else {
	      int l = PySequence_Size(tkinfo.curparent_contents);
	      tObj = PyObject_GetAttrString(thistok,"contents");
	      PySequence_SetSlice(tkinfo.curparent_contents,l,l,tObj);
	      if (PyList_GET_SIZE(wslist)) {
		l += PyList_GET_SIZE(tObj);
		PySequence_SetSlice(tkinfo.curparent_contents,l,l,wslist);
	      }
	      MY_DECREF(tObj);
	  }

	} /* a list of tokens */

	LSEtke_fill_tkinfo(&tkinfo,0);

	MY_DECREF(thistok); /* curparent ref now gone */
	MY_DECREF(identry);
	MY_DECREF(wslist);
	MY_DECREF(rval);

	MY_DECREF(idtok);
	idtok = NULL;

      } /* else no arguments */
    } /* if (idtok) */
  } /* outer while: if not done */

  /* and look out for a left-over terminator */
  
  if (terminator) {
    if (terminator != '\n') {
      /* if there is still a terminator needed, that is bad
       * restore the input source stack to what it was...
       */
      /* TODO: tinst.inputSourceStack = olcount */
      RAISEEXCO(PyString_FromFormat("Mismatched '%c'",
				    terminator==']' ? '[' : 
				    terminator=='[' ? ']' : 
				    terminator=='}' ? '{' :
				    terminator=='{' ? '}' :
				    terminator==')' ? '(' :
				    '(' ),tkinfo.tinst);
      goto error;
    } else {
      PySequence_DelItem(envStack,-1); /* get a leftover # out of the way */
    }
  }

  tObj = PyList_GetItem(envStack,PyList_Size(envStack)-1);
  MY_DECREF(curparent); /* curparent ref now gone */
  { PyObject *dummy2;
    int *dummy;
    if (!PyArg_Parse(tObj,"(iObOi)", &dummy,
		&curparent, &terminator, &dummy2,
		&savedivno)) return NULL;
  }

  tObj = PyObject_GetAttrString(tkinfo.tinst,"divertList");
  PyList_SetItem(tObj,savedivno,curparent);
  MY_INCREF(curparent);
  MY_DECREF(tObj);

  /* need to make tinst.expandLevel visible.... */
  LSEtke_SET_INT_FIELD(tkinfo.tinst, "expandLevel", tkinfo.expandLevel);

  MY_XDECREF(sObj);
  /* Py_XDECREF(curparent); -- stolen from stack... */
  MY_XDECREF(olcount);
  MY_XDECREF(envStack);
  MY_XDECREF(idtok);
  MY_XDECREF(idarglist);
  LSEtke_streamer_destructor(streamer);

  MY_INCREF(Py_None);
  return Py_None;

 error:
  /* need to make tinst.expandLevel visible.... */
  LSEtke_SET_INT_FIELD(tkinfo.tinst, "expandLevel", tkinfo.expandLevel);
  LSEtke_streamer_destructor(streamer);
  MY_XDECREF(sObj);
  MY_XDECREF(curparent);
  MY_XDECREF(olcount);
  MY_XDECREF(envStack);
  MY_XDECREF(idtok);
  MY_XDECREF(idarglist);
  return NULL;
}

static PyObject *
initializer(PyObject *self, PyObject *args) 
{
  if (!PyArg_ParseTuple(args, "OOOOOOOOOOOOOOOOsOO", 
			&LSEtk_TokenizeException,
			&LSEtk_TokenEmpty,
			&LSEtk_TokenComment,
			&LSEtk_TokenIdentifier,
			&LSEtk_TokenNumber,
			&LSEtk_TokenM4Quote,
			&LSEtk_TokenQuote,
			&LSEtk_TokenOther,
			&LSEtk_TokenRaw,
			&LSEtk_TokenPList,
			&LSEtk_TokenSBList,
			&LSEtk_TokenCBList,
			&LSEtk_TokenPreProc,
			&LSEtk_TokenTopLevel,
			&_LSEtk_FileWrapper,
			&_LSEtk_BitBucket,
			&whitespace,
			&LSEtk_find_id,
			&LSEtk_invoke_id))
    return NULL; /* borrowed, but not going anywhere! */

  Py_INCREF(Py_None);
  return Py_None;
}

/************** Method table and initializer ********************/

static PyMethodDef LSETokenizerExtMethods[] = {

  /* initialization */

  { "LSEtke_init", initializer, METH_VARARGS, "Callback initialization" },

  /* input streamer code */

  { "LSEtke_streamer_create", LSEtke_streamer_create,
    METH_VARARGS, "Create an input streamer" },
  { "LSEtke_streamer_prepend", LSEtke_streamer_prepend,
    METH_VARARGS, "Prepend input to streamer" },
  { "LSEtke_streamer_notEmpty", LSEtke_streamer_notEmpty,
    METH_VARARGS, "Streamer is not empty" },
  { "LSEtke_streamer_getLine", LSEtke_streamer_getLine,
    METH_VARARGS, "Get a line" },
  { "LSEtke_streamer_dumpState", LSEtke_streamer_dumpState,
    METH_VARARGS, "Dump current state" },

  /* tokenizer */

  { "LSEtke_tokenize_string_int", LSEtke_tokenize_string_int, 
    METH_VARARGS, "The magic internal function...." },

  { NULL, NULL, 0, NULL }   /* Sentinel */
};

PyMODINIT_FUNC 
initLSETokenizerExt(void) {
  (void) Py_InitModule("LSETokenizerExt", LSETokenizerExtMethods);
  newline = PyString_FromString("\n");
  emptystring = PyString_FromString("");
  emptydictionary = PyDict_New();
}


