#! /bin/sh
# The next line is a null operation in the shell.  We do this to find
# python in the user's path, even if #! doesn't work.
"""cat" </dev/null >/dev/null
eval '(exit $?0)' && eval 'exec python -O $0 ${1+"$@"}'
& eval 'exec python -O $0 $argv:q'
"""
# /* 
#  * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Tokenizing and macro substitution script
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This script is a Python script which breaks strings or files into
#  * tokens, performing macro substitution as it does so.  Eventually it
#  * will become a substitute for m4 or perhaps the C pre-processor, but
#  * we are not there yet!
#  *
#  * "Tokenizer" is perhaps not the best name for what happens here; it is
#  * a combination of a tokenizer with a very simple grammar for (),[],{} and
#  * C-pre-processor starts.  What is returned is a sort of parse tree;
#  * matched parenthesis/braces have their contents as children.
#  *
#  * This parse tree concept helps explain the main difference between the
#  * macro processing and the normal m4 processing.  m4 macros expand to
#  * text, which is then re-parsed.  The "token" macros expand to parse trees
#  * which are not re-parsed.  Both behaviors are supported; each macro
#  * definition states what it is to do...
#  *
#  * The parse trees are a bit funny; the parent links are not created
#  * with valid values to start with.  A macro can ask for its arguments
#  * to be properly parented w.r.t. each other and the parent holding
#  * the id token and the parameter list.  To get anything above that, it
#  * must ask the tokenizer to reparent the current diversion....
#  *
#  * IMPORTANT CHANGE: parent links, besides costing something to maintain
#  *                   can mess up the naive Python garbage collector that
#  *                   was the only option in 1.5.2 and is still a seeming
#  *                   option in 2.0 and higher.  As a result, we do not
#  *                   maintain parent links.  Instead, we pass a piece of
#  *                   the parsing stack into called tokenizer macros.
#  *
#  * Another big difference between this and m4 is that m4 always outputs
#  * text.  Here we want parse trees as output.  The two are equivalent in
#  * non-interactive modes; it's just a matter of when the text is turned
#  * into a string....  The way we're going to deal with this is by having
#  * a special top-level token which is a file; when you add tokens to it,
#  * they are output.  This causes us to not have to do any special logic
#  * to output to a file.  There is a difference from m4: matched (), [], {}
#  * will not be output immediately; they will wait for the closing side
#  * before they are output.
#  *
#  * Note that the m4 diversion concept is supported and works with parse
#  * trees; you can divert and create a new parse tree buffer...  But there
#  * is an ugly complication: you really should only switch to the new buffer
#  * at the top level.  If you do so within a matched list, the diversion
#  * does not take effect until after the matched list finishes...  The
#  * matched list itself stays in the previous diversion.
#  *
#  * While diversion redirects output, another concept must be used to have
#  * behavior like #if, which must skip input.  To deal with this, there is
#  * a "skipping" variable.... when it is set, tokens are not added and
#  * list grouping characters become "Other" tokens (so we do not have
#  * problems with mismatches between them...)  Macros to end skipping
#  * must take no arguments or be "#" macros...
#  *
#  * Input side vs. output tokens:
#  *    Input token is the token to add element to.
#  *    Output token (diversion) is the token to add elements to when elements
#  *       are going to output.
#  *
#  *    Tokens go to output unless we are looking for parenthesis....
#  *
#  * Data details:
#  *
#  * Identifier dictionary entry format:
#  * {
#  *   "name" : [( macrotype, macroflags, macrotype-specific args), ...]
#  *   ...
#  * }
#  *
#  * The list is necessary because definitions can be "stacked".
#  *
#  * Specific args:
#  *   C macro: not done yet
#  *   m4 macro: ( macrotype, macroflags, definition)),
#  *   tok macro: ( macrotype, macroflags, func, extraargs)
#  *
#  * Differences from m4:
#  *
#   not supported:
#     - changeword builtin (actually might be easier than in m4 itself!)
#     - dumpdef builtin (wouldn't be too bad)
#     - eval builtin's radix and width parameters (non-octal/hex/decimal hard)
#     - format builtin (ugly to re-implement C?)
#     - m4wrap builtin
#     - traceon, traceoff builtins (ugly)
#
#     - -f/--freeze-state (no frozen files)
#     - -R/--reload-state (no frozen files)
#     - -s/--synclines   (really, really, really hard to implement)
#     - -G/--traditional (why do the old?)
#     - -W/--word-regexp (not unless changeword is implemented...)
#     - -E/--fatal-warnings (would not be hard, just tedious)
#     - -l/--arglength      (debug related)
#     - -e/--interactive    (not true for tokenizer, why bother?)
#     - -L/--nesting-limit  (would not be hard)
#     - -Q/--quiet/--silent (not hard, just tedious)
#     - -t/--trace          (ugly, part of tracing)
#
#   other things:
#     - usage message is missing completely
#     - number tokens are recognized as in C (i.e. with 0x, floats, ULL)
#     - files are read completely into a string for processing as they are
#       included, so it is not generally a good idea to include from a pipe...
#     - eval's boolean operators do return non-zero instead of 1 for true
#       (first non-zero operator for ||, last operator for &&)
#     - eval does not support 0r or 0b notation
#     - some esoteric regular expression stuff might not work due to
#       differences in m4's regexp and Python's regexp module (which is
#       deprecated, by the way
#     - maketemp generates different names (template@pid.unique#)
#     - debugmode is always either "on" or "off"; values do not matter
#     - diversions inside parenthesis do *not* split the parenthesis between
#       the previous and following diversion...
#     - do( blah ) where blah->`' turn into do(), not do(  )
#     - it seems likely that if you set the m4 quotes or comments to the
#       parenthesis characters, different bad things will happen than normally
#       happen in m4.
#
#  */

import string, re, sys, types, copy, pdb, os, os.path, getopt, cStringIO, \
       code, tempfile

try:
    from LSETokenizerExt import *
    _LSEtk_extension_present = 1
except ImportError:
    #sys.stderr.write("Unable to load LSE tokenizer extensions; "
    #                 "are you using ls-python?\n")
    _LSEtk_extension_present = 0
    #sys.exit(1)

## yes, these are deprecated, but they match the m4 syntax!
#try:
#    import warnings
#    warnings.filterwarnings("ignore", ".* regex .*",
#                            DeprecationWarning, ".*",
#                            append=1)
#    warnings.filterwarnings("ignore", ".* regsub .*",
#                            DeprecationWarning, ".*",
#                            append=1)
#except:
#    pass
#import regex, regsub

LSEtk_flag_MatchBraces = 1
LSEtk_flag_MatchParens = 2
LSEtk_flag_CQuotes = 4
LSEtk_flag_M4Quotes = 8
LSEtk_flag_CComments = 16
LSEtk_flag_M4Comments = 32
LSEtk_flag_Directives = 64

LSEtk_flag_LSEBuiltins = 128
LSEtk_flag_M4Builtins = 256
LSEtk_flag_UseM4Pref = 512

LSEtk_flag_FatalWarnings = 1024

LSEtk_flag_COnly = LSEtk_flag_MatchBraces | LSEtk_flag_CQuotes | \
                   LSEtk_flag_MatchParens | LSEtk_flag_CComments | \
                   LSEtk_flag_Directives | LSEtk_flag_LSEBuiltins
LSEtk_flag_M4Only = LSEtk_flag_M4Quotes | LSEtk_flag_M4Comments | \
                    LSEtk_flag_M4Builtins
LSEtk_flag_All = LSEtk_flag_COnly | LSEtk_flag_M4Only

# macrotypes
LSEtk_m4_macro = 1    # works like m4; inserts text
LSEtk_Tok_macro = 2   # inserts parse trees

# macroflags
LSEtk_NoArgs        = 1  # macro has no arguments, do not look for them
LSEtk_NoWhiteSpace  = 2  # cannot have whitespace between macro name and '('
LSEtk_EatLine       = 4  # macro eats remaining characters from line
LSEtk_KeepLeadWS    = 8  # whitespace before arguments should not be removed
LSEtk_RequireArgs   = 16 # if macro has no arguments, do not invoke it

#_LSEtk_extension_present = 0

#######################################################################
#                              CLASSES
#######################################################################

#
# The Exception class
#    able to perform input file tracing, which is is main advantage
#
class LSEtk_TokenizeException(Exception):
    def __init__(self,reason,tinst=None,line=None):
        Exception.__init__(self,"Hoping this works")
        #sys.stderr.write("%s %s %s %s\n" %
        #                 (reason,tinst,tinst.inputSourceStack, line))
        self.reason = reason
        if tinst:
            self.inputTrace = map(lambda x:[x[2],x[0],x[3]],
                                  tinst.inputSourceStack)
            if line != None:
                self.inputTrace[-1][1]=line
            self.inputTrace = filter(lambda x:x[0],self.inputTrace)
            if not len(self.inputTrace):
                self.inputTrace  = [["in unknown source","??",0]]
        else: self.inputTrace = [["in unknown source","??",0]]
        self.inputTrace.reverse() # so it is in printing order...

    def __str__(self):
        def trace2str(y):
            if y[2]:
                return " %s" % y[0]
            else:
                return " at line %s %s" % (y[1],y[0])
    
        return self.reason + string.join(map(trace2str,self.inputTrace),"")

#
# Exit class
#
class LSEtk_Exit(StandardError):
    
    def __init__(self, exitcode):
        self.exitcode = exitcode

#
# class for wrapping file objects to make them look like lists....
#
class _LSEtk_FileWrapper:
    def __init__(self, fobj):
        self.fileobject = fobj

    def append(self, obj):
        self.fileobject.write(str(obj))

    def extend(self, olist):
        for o in olist:
            self.fileobject.write(str(o))

    def __len__(self):
        return 0

    def __getitem__(self,ro):
        raise IndexError

    def __delitem__(self,ro):
        raise IndexError

    def __getslice__(self,l,r):
        return []
    
    def __delslice__(self,l,r):
        pass
    
    def __setslice__(self,l,r,slist):
        for s in slist:
            self.fileobject.write(str(s))
    
#
# class for throwing away output
#
class _LSEtk_BitBucket:
    def __init__(self):
        pass
    
    def append(self, obj):
        pass

    def extend(self, olist):
        pass

    def __len__(self):
        return 0

    def __getitem__(self,ro):
        raise IndexError

    def __delitem__(self,ro):
        raise IndexError

    def __getslice__(self,l,r):
        return []
    
    def __delslice__(self,l,r):
        pass
    
    def __setslice__(self,l,r,s):
        pass

    def write(self,str):
        pass

    def close(self):
        pass
        
################### token classes #########################
#  If later parsers want to use the same kind of system, they
#  can just add additional subclasses

#
# base class for tokens
#
class LSEtk_Token: # should not instantiate directly....

    def __init__(self, contents):
        #self.parent = None
        self.begWhite = ""
        self.contents = contents

    def __str__(self):
        raise LSEtk_TokenizeException("Token does not have __str__ method")

    def canonicalStr(self):
        raise LSEtk_TokenizeException("Token does not have canonicalStr "
                                      "method")

    def debug(self,level=0,stream=sys.stderr):
        raise LSEtk_TokenizeException("Token does not have debug method")

    def writeDebugSpaces(self,level=0,stream=sys.stderr):
        stream.write(" " * (3 * level))

    def toWordList(self):
        raise LSEtk_TokenizeException("Token does not have toWordList method")

def _LSEtk_listReduce(x,y):
    y.toStringList(x)
    return x

#
# an atomic token
#
class LSEtk_Token_atomic(LSEtk_Token):
    def __str__(self):
        return self.begWhite + self.contents

    def toStringList(self,sl):
        sl.extend([ self.begWhite, self.contents])
        return sl
    
    def __cmp__(self, other):
        # check for sub-classing/comparison against non-tokens
        if type(other) is not types.InstanceType: return 1
        cv = cmp(self.__class__,other.__class__)
        if cv: return cv
        # we do NOT compare whitespace
        return cmp(self.contents, other.contents)
    
    def canonicalStr(self):
        return " " + self.contents

    def debug(self,level=0,stream=sys.stderr):
        self.writeDebugSpaces(level,stream)
        stream.write("%s:%s\n" % (self.tlabel,repr(self.contents)))

    def toWordList(self, wl=[]):
        return [ self.contents ]

#
# C-pre-processor directive
#
class LSEtk_Token_compound(LSEtk_Token):
    def __str__(self):
        return string.join(reduce(_LSEtk_listReduce,self.contents,
                                  [self.begWhite]),"")

    def toStringList(self,sl):
        sl.append(self.begWhite)
        return reduce(_LSEtk_listReduce,self.contents,sl)
        
    def __cmp__(self, other):
        # check for sub-classing/comparison against non-tokens
        if type(other) is not types.InstanceType: return 1
        cv = cmp(self.__class__,other.__class__)
        if cv: return cv
        # we do NOT compare whitespace
        cv = cmp(len(self.contents),len(other.contents))
        if cv: return cv
        for c in range(0,len(self.contents)):
            cv = cmp(self.contents[c],other.contents[c])
            if cv: return cv
        return 0
    
    def debug(self,level=0,stream=sys.stderr):
        self.writeDebugSpaces(level,stream)
        stream.write(self.tlabel + "\n")
        for c in self.contents:
            c.debug(level+1)

    def toWordList(self):
        return reduce(lambda x,y:x+y.toWordList(),self.contents,[])

#
# a token made up of a list of other tokens
#
class LSEtk_Token_list(LSEtk_Token):

    def __str__(self):
        sl = [self.begWhite,self.groupchars[0:1]]
        reduce(_LSEtk_listReduce,self.contents,sl)
        sl.append(self.groupchars[1:2])
        return string.join(sl,"")

    def toStringList(self,sl):
        sl.extend([self.begWhite,self.groupchars[0:1]])
        reduce(_LSEtk_listReduce,self.contents,sl)
        sl.append(self.groupchars[1:2])
        return sl
    
    def __cmp__(self, other):
        # check for sub-classing/comparison against non-tokens
        if type(other) is not types.InstanceType: return 1
        cv = cmp(self.__class__,other.__class__)
        if cv: return cv
        # we do NOT compare whitespace
        cv = cmp(len(self.contents),len(other.contents))
        if cv: return cv
        for c in range(0,len(self.contents)):
            cv = cmp(self.contents[c],other.contents[c])
            if cv: return cv
        return 0
    
    def debug(self,level=0,stream=sys.stderr):
        self.writeDebugSpaces(level,stream)
        if self.groupchars[0:1]:
            stream.write(self.tlabel + self.groupchars[0:1]+"\n")
            for c in self.contents:
                c.debug(level+1)
            self.writeDebugSpaces(level,stream)
            stream.write(self.groupchars[1:2]+"\n")
        else:
            stream.write(self.tlabel+"\n")
            for c in self.contents:
                c.debug(level+1)

    def toWordList(self):
        if self.groupchars[0:1]: start = [ self.groupchars[0:1] ]
        else: start = []
        if self.groupchars[1:2]: end = [ self.groupchars[1:2] ]
        else: end = []
        return start + \
               reduce(lambda x,y:x+y.toWordList(),self.contents,[]) + \
               end

#
# a token that holds only whitespace
#
class LSEtk_TokenEmpty(LSEtk_Token_atomic):
    tlabel = "EMPTY"
    tid = "Empty"
    def toWordList(self):
        return [ ]

#
# a token that holds comments
#
class LSEtk_TokenComment(LSEtk_Token_atomic):
    tlabel = "COMMENT"
    tid = "Empty"
    def toWordList(self):
        return [ ]

#
# an identifier
#
class LSEtk_TokenIdentifier(LSEtk_Token_atomic):
    tlabel = "ID"
    tid = "Identifier"

#
# A C-pre-processor number
#
class LSEtk_TokenNumber(LSEtk_Token_atomic):
    tlabel = "NUMBER"
    tid = "Number"

#
# m4 quoted string
#
class LSEtk_TokenM4Quote(LSEtk_Token_atomic):
    tlabel = "M4QUOTE"
    tid = "M4Quote"

#
# C quoted string (single or double quotes)
#
class LSEtk_TokenQuote(LSEtk_Token_atomic):
    tlabel = "QUOTE"
    tid = "Quote"

#
# Other atomic token
#
class LSEtk_TokenOther(LSEtk_Token_atomic):
    tlabel = "OTHER"
    tid = "Other"

#
# Raw, untokenized strings
#
class LSEtk_TokenRaw(LSEtk_Token_atomic):
    tlabel = "RAW"
    tid = "Raw"

#
# List enclosed in parenthesis
#
class LSEtk_TokenPList(LSEtk_Token_list):
    tlabel = ""
    groupchars = "()"
    tid = "Plist"
    
#
# List enclosed in square brackets
#
class LSEtk_TokenSBList(LSEtk_Token_list):
    tlabel = ""
    groupchars = "[]"
    tid = "SBList"

#
# List enclosed in curly braces
#
class LSEtk_TokenCBList(LSEtk_Token_list):
    tlabel = ""
    groupchars = "{}"
    tid = "CBList"

#
# C-pre-processor directive
#
class LSEtk_TokenPreProc(LSEtk_Token_list):
    tlabel = "PREPROC"
    groupchars = "#\n"
    tid = "PreProc"

    def toWordList(self):
        return [ "#" ] + \
               reduce(lambda x,y:x+y.toWordList(),self.contents,[])

    def debug(self,level=0,stream=sys.stderr):
        self.writeDebugSpaces(level,stream)
        stream.write(self.tlabel +"\n")
        for c in self.contents:
            c.debug(level+1)

#
# Top-level token
#
class LSEtk_TokenTopLevel(LSEtk_Token_list):
    tlabel = "TOP"
    groupchars = ""
    tid = "TopLevel"

########## Tokenizer... data structures for a tokenizing run ###########

_LSEtk_NullDiversion = LSEtk_TokenTopLevel(_LSEtk_BitBucket())

class LSEtk_TokFlags:
    def __init__(self, flags):
        self.__dict__["flags"] = flags
        self.__dict__["doMatchBraces"] = flags & LSEtk_flag_MatchBraces
        self.__dict__["doMatchParens"] = flags & LSEtk_flag_MatchParens
        self.__dict__["doCQuotes"] = flags & LSEtk_flag_CQuotes
        self.__dict__["doM4Quotes"] = flags & LSEtk_flag_M4Quotes
        self.__dict__["doCComments"] = flags & LSEtk_flag_CComments
        self.__dict__["doM4Comments"] = flags & LSEtk_flag_M4Comments
        self.__dict__["doDirectives"] = flags & LSEtk_flag_Directives
        self.__dict__["fatalWarnings"] = flags & LSEtk_flag_FatalWarnings
        self.__dict__["changed"] = 0
        self.updateChars()
        
    def __setattr__ (self, name, value):
        self.__dict__[name] = value
        self.__dict__["changed"] = 1
        if name in ["doMatchBraces", "doMatchParens" ]:
            self.updateChars()

    def clearChanged(self):
        self.__dict__["changed"] = 0

    def updateChars(self):
        if self.doMatchBraces:
            if self.doMatchParens:
                startchars = "([{"
                endchars = "}])"
            else:
                startchars = "[{"
                endchars = "}]"
        else:
            if self.doMatchParens:
                startchars = "("
                endchars = ")"
            else:
                startchars = ""
                endchars = ""
        self.__dict__["startchars"] = startchars
        self.__dict__["endchars"] = endchars
    
#
# The basic tokenizer class
#
class LSEtk_Tokenizer:

    # userdlist = user dictionary list to be prepended on all work calls
    def __init__(self, flags, userdlist=None, env = None):
        if userdlist is None: userdlist=[]
        self.idMap = {}
        self.pythonEnv = { "LSE_thisTokenizer" : self }
        self.debugMode = 0
        self.flags = LSEtk_TokFlags(flags)
        self.debugFile = sys.__stderr__
        
        self.userDlist = userdlist = userdlist * 1
        self.sysval = 0
        
        # set up standard environment
        bids = {}
        def copier(y,d=bids): d[y[0]] = y[1] * 1
        
        if (flags & LSEtk_flag_M4Builtins):
            if flags & LSEtk_flag_UseM4Pref:
                filter(copier, _LSEtk_PrefixedM4Builtins.items())
            else:
                filter(copier, _LSEtk_m4Builtins.items())
        if (flags & LSEtk_flag_LSEBuiltins):
            filter(copier, _LSEtk_TokBuiltins.items())

        filter(copier, _LSEtk_VitalBuiltins.items())
        userdlist.append(bids)
            
        self.clearEnv()
        self.clearDiversions()
        self.incPath = []
        self.env = env
        
    # Clear environment
    def clearEnv(self):
        self.flags.m4LeftQuote = "`"  #` for emacs
        self.flags.m4RightQuote = "'"
        self.flags.m4LeftComment = "#"
        self.flags.m4RightComment = "\n"
        self.flags.clearChanged()
        
    def clearDiversions(self):
        # we will be one off on all diversions so the bit bucket can always
        # be on the list...
        self.currDivertNo = 0
        self.divertList = [_LSEtk_NullDiversion,None]
        self.ppStack = []
        self.flags.ppSkip = 0
        self.flags.clearChanged()
        
    # Input source stack handling
    
    def pushInputSource(self,iname):
        noline = 0
        if type(iname)==types.TupleType:
            (name,line) = iname
            if line is None:
                line = 1
                noline = 1
        else:
            name = iname
            line = 1
        self.inputSourceStack.append([line, 0, name, noline])

    def popInputSource(self):
        lnor = self.inputSourceStack[-1][0]
        del self.inputSourceStack[-1]
        try: # may have emptied stack
            self.inputSourceStack[-1][1] = self.inputSourceStack[-1][1] - lnor
        except: pass

    def getLineNo(self):
        return self.inputSourceStack[-1]

    def startInput(self, dlist=[], oobj=None):
        if oobj is not None:
            funnyfile = _LSEtk_FileWrapper(oobj)
            tlobj = LSEtk_TokenTopLevel(funnyfile)
        else:
            tlobj = LSEtk_TokenTopLevel([])
        
        self.divertList[1] = tlobj
        self.inputSourceStack = []
        self.currDivertNo = 1
        self.dlisttouse = dlist + self.userDlist 
        self.expandLevel = 0
        return tlobj
        
    # Process some input
    #   iobj = input object (string or file)
    #   iname = input object name
    #   dlist = dictionary list
    #   oobj = output object (None or file)
    #
    def processInput(self, iobj, iname, dlist=[], oobj=None):
        tlobj = self.startInput(dlist, oobj)
        self.inputSourceStack = []
        self.pushInputSource(iname)
        
        if type(iobj) is types.StringType:
            LSEtke_tokenize_string_int(self, iobj, self.dlisttouse, tlobj)
        else:
            LSEtke_tokenize_string_int(self, iobj.readlines(),
                                       self.dlisttouse, tlobj)
            
        if self.flags.ppSkip:
            raise LSEtk_TokenizeException("Unmatched #LSE endif",tinst)

        self.popInputSource()
        LSEtk_cleanup_token(tlobj)
        return tlobj

    # Process some input
    #   iobj = input object (string or file)
    #   iname = input object name
    #   dlist = dictionary list
    #   oobj = output object (None or file)
    #
    def continueInput(self, iobj, iname, clearout=0):
        tlobj = self.divertList[self.currDivertNo]
        if clearout:
            try:
                del tlobj.contents[:]
            except TypeError: # must have been a string...
                tlobj.contents = ""
            
        if iname is not None: self.pushInputSource(iname)
        
        if type(iobj) is types.StringType:
            LSEtke_tokenize_string_int(self, iobj, self.dlisttouse, tlobj)
        else:
            LSEtke_tokenize_string_int(self, iobj.readlines(),
                                       self.dlisttouse, tlobj)

        if iname is not None: self.popInputSource()
        return tlobj

    #
    #  and a file-like interface for the tokenizer...
    #
    def open(self,name,lineno=1):
        self.pushInputSource((name,lineno))
        
    def write(self,str):
         LSEtke_tokenize_string_int(self, str, self.dlisttouse,
                                    self.divertList[self.currDivertNo])

    def close(self):
        self.popInputSource()
    
    # finish processing input -- part of normal m4 behavior
    
    def finishDiversions(self):
        if self.flags.ppSkip:
            raise LSEtk_TokenizeException("Unmatched #LSE endif",tinst)

        # and undivert.... run with empty dictionaries so that nothing
        # expands at all...
        self.currDivertNo = 1
        d = self.divertList[1]
        for n in range(2,len(self.divertList)):
            d.contents.extend(self.divertList[n].contents)
        del self.divertList[2:]
        LSEtk_cleanup_token(d)
        return d

    def getCurrentDictionaries(self):
        return self.dlisttouse
    
##################### substitution stuff ########################

#
# LSEtk_find_id
#   discover whether the id is a macro to be expanded and return
#   the expansion information
#
def _LSEtk_find_id(mname, dlist):
    for d in dlist:
        dentry = d.get(mname)
        if dentry:
            return dentry[-1] # get end of list
    return None

#
# _LSEtk_invoke_id
#  actually set up parameters and invoke a macro
#
def _LSEtk_invoke_id(tinst, name, identry, arglist, dpath, parent,
                     envStack):
    if arglist is None: # has no arguments
        narglist = []
    else: # has arguments
        # We will split out the argument handling into separate loops
        # for each whitespace case:
        # 1) build tokens - collapse whitespace/comments into tokens, possibly
        #    eating leading whitespace/comments.  Does *NOT* parent tokens
        #    tokens properly w.r.t the rest of the tree; if a macro wishs
        # 2) build strings - collapse only whitespace, possibly eating
        #    leading whitespace/comments

        eatleadspace = not (identry[1] & LSEtk_KeepLeadWS)

        # case 1: tokens; eat all whitespace and comments unless keeping
        if identry[0] == LSEtk_Tok_macro:
            
            # puts whitespace and comments where they belong.  Reparent what
            # we have so later on we can do reparenting at only a single level
            #
            # the idea is to have arguments encased in toplevels, but the
            # commas still there and narglist consisting of only the toplevels;
            # the reason we still need the plist to contain the commas is that
            # the function may return None and we want to put the original
            # contents (or at least those with the extra TopLevel) onto the
            # output stream.
            LSEtk_cleanup_token(parent)
            
            # look at TopLevel->PList
            try:
                plist = parent.contents[1]
            except IndexError:
                parent.debug()
                raise
            narglist=[]
            currarg = []
            npcontents = []
            
            for a in plist.contents:

                # handle whitespace
                if eatleadspace and not currarg:
                    if isinstance(a,LSEtk_TokenEmpty): continue
                    if a.begWhite: a.begWhite=""

                # look for commas...
                if a.contents == ',' and isinstance(a,LSEtk_TokenOther):
                    nt = LSEtk_TokenTopLevel(currarg)
                    #nt.parent = plist
                    npcontents.append(nt)
                    npcontents.append(a)
                    #reduce(_LSEtk_reparenter,currarg,nt)
                    narglist.append(nt)
                    currarg = []
                else:
                    currarg.append(a)
            nt = LSEtk_TokenTopLevel(currarg)
            #nt.parent = plist
            npcontents.append(nt)
            #reduce(_LSEtk_reparenter,currarg,nt)
            narglist.append(nt)
            plist.contents = npcontents
            
        # case 2: strings; eat whitespace but not comments
        else:

            narglist=[]
            currarg = []
            for a in arglist:

                if isinstance(a,LSEtk_TokenEmpty): # must be true whitespace
                    # drop leading
                    if eatleadspace and not currarg: continue
                    # keep the rest
                    currarg.append(a)
                    continue
                
                if a.begWhite and eatleadspace and not currarg:
                    a.begWhite=""

                # look for commas...
                if a.contents == ',' and isinstance(a,LSEtk_TokenOther):
                    narglist.append(string.join(reduce(_LSEtk_listReduce,
                                                       currarg,[]),
                                                ""))
                    currarg = []
                else: currarg.append(a)
                
            narglist.append(string.join(reduce(_LSEtk_listReduce,currarg,[]),
                                        ""))

    # Report arguments
    
    if tinst.debugMode:
        astring = _LSEtk_m4quote_args(tinst,narglist)
        tinst.debugFile.write("-%d- %s(%s) -> ???\n" % (tinst.expandLevel,
                                                        name,astring))

    # And invoke
    
    macro = identry[2]
    if type(macro)==types.StringType:
        rval = _LSEtk_expand_m4_defn(tinst,macro,name,narglist)
    elif type(macro)==types.FunctionType:
        #sys.stderr.write("I %s %s\n\t%s\n" % (name,envStack,
        #                 map(lambda x:str(x[3]),envStack)))
        rval = macro((tinst,dpath,parent,map(lambda x:x[3],envStack)),
                     [name] + narglist, identry[3],
                     tinst.env)
    else: rval = macro # hopefully this will make sense!

    # Now report return value
    
    if tinst.debugMode:
        tinst.debugFile.write("-%d- %s(...) ->" % (tinst.expandLevel,name))
        if type(rval) is types.StringType:
            tinst.debugFile.write("%s%s%s\n" % (tinst.flags.m4LeftQuote,
                                                str(rval),
                                                tinst.flags.m4RightQuote))
        elif type(rval) is types.ListType:
            tinst.debugFile.write("%s\n" % repr(rval))
            for r in rval:
                if type(r) is types.StringType:
                    tinst.debugFile.write("%s%s%s\n" %
                                          (tinst.flags.m4LeftQuote,
                                           str(r),
                                           tinst.flags.m4RightQuote))
                else:
                    tinst.debugFile.write("%s\n" % repr(r))
        elif isinstance(rval,LSEtk_Token):
            rval.debug(0,tinst.debugFile)
        else: tinst.debugFile.write("\n")

    # and process the return value
    
    if (type(rval) is types.StringType):
        return rval
    elif (type(rval) is types.ListType):
        return rval
    elif isinstance(rval,LSEtk_Token):
        if isinstance(rval,LSEtk_TokenTopLevel):
            return rval.contents
        else: return [ rval ]
    else: return None

##########
#  _LSEtk_expand_m4_defn(s,name,alist)
#    expand the m4 definition

_ArgPattern=re.compile(r'(\$[0-9]+|\$@|\$#|\$\*)')


def _LSEtk_m4quote_args(tinst,alist):
    def quote_arg(x,lq=tinst.flags.m4LeftQuote,rq=tinst.flags.m4RightQuote):
        return lq + str(x) + rq
    
    if (alist != []):
        astring = reduce(lambda x,y:x+","+y,map(quote_arg,alist))
    else: astring = quote_arg("")
    return astring

def _LSEtk_expand_m4_defn(tinst,s,name,alist):
    pos = 0
    m=_ArgPattern.search(s,pos)
    while pos < len(s) and m:
        mstring = m.group()

        if (mstring=='$#'):
            astring = repr(len(alist))
        elif (mstring=='$*'):
            if (alist != []):
                astring = reduce(lambda x,y:x+","+y,alist)
            else: astring = ""
        elif (mstring=='$@'):
            astring = _LSEtk_m4quote_args(tinst,alist)
        elif (mstring=='$0'):
            astring = name
        else: # numeric
            aindex=string.atoi(mstring[1:])
            if (aindex > 0 and aindex <= len(alist)):
                astring = alist[aindex-1]
            else:
                astring=""

        s=s[:m.start()]+astring+s[m.end():]
        pos = m.end() - len(mstring) + len(astring)
        m=_ArgPattern.search(s,pos)
    return s

########################## lexing stuff ############################

_LSEtk_CCommentPattern=re.compile('(/\*)')
_LSEtk_CCommentEndPattern=re.compile('(\*/)')
_LSEtk_CPPCommentPattern=re.compile('(//)')
_LSEtk_CPPCommentEndPattern=re.compile('($)',re.MULTILINE)
_LSEtk_LineContPattern=re.compile(r'(\\$)',re.MULTILINE)
_LSEtk_LineEndPattern=re.compile('($)',re.MULTILINE)
_LSEtk_WhitePattern=re.compile('(\s+)')
_LSEtk_IDPattern=re.compile('([A-Za-z_@]\w*)')
_LSEtk_WhiteIDPattern=re.compile('[ \t]*([A-Za-z_]\w*)')
# we use the same pattern as gcc's preprocessing token for numbers;
# it is easier, and I think it matches ANSI C, which is fine here, since
# we are not overly concerned with the number being valid
_LSEtk_NumberPattern = re.compile('([0-9]([eE][+-]|[\w.])*)')

# Set up the ID translation stuff....
# this will not work with Unicode, but I do not care...
_LSEtk_IDTableL = [None] * 256
for r in (string.letters + "_" + "@"):
    _LSEtk_IDTableL[ord(r)] = 1
_LSEtk_IDFirstTable = tuple(_LSEtk_IDTableL)

_LSEtk_Otherside={ '[': ']',
            ']': '[',
            ')': '(',
            '(': ')',
            '{': '}',
            '}': '{',
            }

def _LSEtk_find_real_line_end(s,p):
    found = 0
    pos2 = p
    while (1):
        m = _LSEtk_LineEndPattern.search(s,pos2)
        if m: # found line end
            pos2=m.end()
            if (s[pos2-1]=='\\'): # was continuator
                pos2 = pos2+1
            else:
                return pos2
        else: # EOF?
            return len(s)-p

def _LSEtk_handle_quote(tinst,curparent,skipping,s,pos,tchar,streamer):
    olcount = tinst.getLineNo()[0]
    skeep = []
    pos2 = pos + 1
    slen = len(s)
    
    while (1):
        mind = string.find(s,tchar,pos2)
        if (mind>=0):
            if (mind and s[mind-1] == '\\'):
                pos2 = mind+1
                continue
            break
        skeep.append(s[pos:])
        (s,slen) = LSEtke_streamer_getLine(streamer)
        if not s:
            raise LSEtk_TokenizeException("Unterminated string",
                                          tinst,olcount)
        pos2 = pos = 0
    npos = mind + 1
    skeep.append(s[pos:npos])
    ccont = string.join(skeep,"")
    if not skipping:
        curparent.contents.append(LSEtk_TokenQuote(ccont))
    pos = npos
    _LSEtk_update_lineno(tinst,string.count(ccont,"\n"))
    return (s,pos)

def handle_C_comment(tinst, curparent, skipping, s, pos, streamer):
    if s[pos+1:pos+2] == "*":
        skeep = []
        pos2 = pos+2
        while (1): 
            mind = string.find(s,"*/",pos2)
            if mind >= 0: break
            skeep.append(s[pos:])
            (s,slen) = LSEtke_streamer_getLine(streamer)
            if not s:
                raise LSEtk_TokenizeException("Unterminated /* ",tinst)
            pos2 = pos = 0
        npos = mind + 2
        skeep.append(s[pos:npos])
        ccont = string.join(skeep,"")
        if not skipping:
            curparent.contents.append(LSEtk_TokenComment(ccont))
        pos = npos
        _LSEtk_update_lineno(tinst,string.count(ccont,"\n"))
        ctype = 0
        
    elif (s[pos+1:pos+2]== "/"): # CPP comment begin

        pos2 = string.find(s,"\n",pos)
        if (pos2 < 0):
            ccont = s[pos:]
            pos = slen
        else:
            ccont = s[pos:pos2+1]
            pos = pos2+1
            _LSEtk_update_lineno(tinst,1)
        if not skipping:
            curparent.contents.append(LSEtk_TokenComment(ccont))
        ctype = 1

    else:
        if not skipping:
            ptok = LSEtk_TokenOther('/')
            curparent.contents.append(ptok)
        pos = pos + 1
        ctype = 2

    return (s,pos,ctype)
    
def _LSEtk_update_lineno(tinst, amt):
    lcount = tinst.inputSourceStack[-1]
    if (lcount[1] > amt):
        lcount[1] = lcount[1] - amt
    else:
        lcount[0] = lcount[0] + (amt-lcount[1])
        lcount[1] = 0

if not _LSEtk_extension_present:
    #sys.stderr.write("No extensions; line-based\n")
    class InputStreamerDummy:
        def __init__(self):
            pass
        
    # takes strings or lists
    def LSEtke_streamer_create(source):
        self = InputStreamerDummy()
        self.future_ptr = 0
        self.future = []
        LSEtke_streamer_prepend_str(self,source)
        return self
    
    def LSEtke_streamer_prepend_str(self, newsource):

        # put new source at front
        if type(newsource) is types.StringType:
            self.future[0:self.future_ptr] = [ newsource ]
        else: # list of strings...
            self.future[0:self.future_ptr] = newsource
        self.future_ptr = 0

    def LSEtke_streamer_prepend(self, newsource):

        # put new source at front
        if type(newsource) is types.StringType:
            self.future[0:self.future_ptr] = [ newsource ]
        else: # list of things to be made strings
            self.future[0:self.future_ptr] = map(lambda x:str(x),newsource)
        self.future_ptr = 0

    def LSEtke_streamer_notEmpty(self):
        return len(self.future)!=self.future_ptr

    def LSEtke_streamer_getLine(self):
        ts = ""
        while len(self.future)!=self.future_ptr:
            s = self.future[self.future_ptr]
            ts = ts + s
            self.future_ptr = self.future_ptr + 1
            if (s.find("\n")>=0):
                return (ts, len(ts))
        return (ts,len(ts))
    
    def LSEtke_streamer_dumpState(self):
        sys.stderr.write("%d %s\n" % (self.future_ptr,
                                      self.future[self.future_ptr:]))

else:
    #sys.stderr.write("Extensions\n")
    pass

if not _LSEtk_extension_present:
  def LSEtke_tokenize_string_int(tinst, s, dpath, curparent, gathering=0,
                               inpreproc=0):
    # tinst is the tokenizer instance
    # s is the input string
    # dpath is the dictionary list
    # curparent is the token to add input to
    # gathering is flag indicating that we're gathering arguments
    # inpreproc is flag indicating that we're in the preprocessor
    
    olcount = None
    terminator = None
    idtok = None
    
    doM4Quotes = tinst.flags.doM4Quotes
    doCQuotes = tinst.flags.doCQuotes
    doM4Comments = tinst.flags.doM4Comments
    doCComments = tinst.flags.doCComments
    doMatchBraces = tinst.flags.doMatchBraces
    doMatchParens = tinst.flags.doMatchParens
    doDirectives = tinst.flags.doDirectives
    skipping = tinst.flags.ppSkip
    startchars = tinst.flags.startchars
    endchars = tinst.flags.endchars
    m4lcomm = tinst.flags.m4LeftComment
    m4lquote = tinst.flags.m4LeftQuote
    m4lcommlen = len(m4lcomm)
    m4lquotelen = len(m4lquote)
    m4rcomm = tinst.flags.m4RightComment
    m4rquote = tinst.flags.m4RightQuote
    m4rcommlen = len(m4rcomm)
    m4rquotelen = len(m4rquote)
    
    whitespace = string.whitespace
    digits = string.digits
    
    tclass = { "(" : (LSEtk_TokenPList,')'),
               "[" : (LSEtk_TokenSBList,']'),
               "{" : (LSEtk_TokenCBList,'}') }
    
    # sentinel value
    envStack = [(0,curparent,None,curparent,tinst.currDivertNo)]

    streamer = LSEtke_streamer_create(s)
                
    # skipping logic:
    #    while we are skipping, we do not evaluate anything but pre-processor
    #    directives.  However, we still need to do quote and comment matching
    #    and it is not a bad idea to move forward by whole tokens...
    pos = 0
    (s,slen) = LSEtke_streamer_getLine(streamer)
    
    while (pos < slen or LSEtke_streamer_notEmpty(streamer)):
        
        # invariant: idtok == None
        
        while (pos < slen):

            #
            # search for various things.....  break once idtok found or
            # we pop back to having an idtok
            #
            
            c = s[pos]

            #
            # start with m4 quotes and comments, as they can override anything
            #

            if (doM4Quotes and s[pos:pos+m4lquotelen]==m4lquote):
                
                # find the matched end of the quote
                pos2 = pos = pos+m4lquotelen
                qdepth = 1
                qend = string.find(s,m4rquote,pos2)
                qbeg = string.find(s,m4lquote,pos2)

                skeep = []
                while (1):
                    if qend>=0 and (qbeg<0 or qend < qbeg):
                        # eat a quote end
                        qdepth = qdepth-1
                        if not qdepth: break
                        pos2 = qend + m4rquotelen
                        qend = string.find(s,m4rquote,pos2)
                    elif qbeg>=0 and (qend<0 or qbeg < qend):
                        # eat a quote beginning
                        qdepth = qdepth+1
                        pos2 = qbeg + m4lquotelen
                        qbeg = string.find(s,m4lquote,pos2)
                    else:
                        # hmm... we have run out of quotes in our text and
                        # there's still an open beginning!  So, let's get
                        # some more quotes...
                        # we also know that both qend and qbeg are < 0
                        skeep.append(s[pos:])
                        (s,slen) = LSEtke_streamer_getLine(streamer)
                        if not s:
                            raise LSEtk_TokenizeException(
                                "Unterminated m4 quote", tinst)
                        pos2 = pos = 0
                        qbeg = string.find(s,m4lquote)
                        qend = string.find(s,m4rquote)

                skeep.append(s[pos:qend])
                ccont = string.join(skeep,"")
                if not skipping:
                    ptok=LSEtk_TokenM4Quote(ccont)
                    curparent.contents.append(ptok)
                pos = qend + m4rquotelen
                _LSEtk_update_lineno(tinst,string.count(ccont,"\n"))

            elif (doM4Comments and s[pos:pos+m4lcommlen]==m4lcomm):
                skeep = []
                pos2 = pos+m4lcommlen
                while (1):
                    mind = string.find(s,m4rcomm,pos2)
                    if (mind>=0): break
                    skeep.append(s[pos:])
                    (s,slen) = LSEtke_streamer_getLine(streamer)
                    if not s:
                        raise LSEtk_TokenizeException(
                            "Unterminated m4 comment", tinst)
                    pos2 = pos = 0
                npos = mind + m4rcommlen
                skeep.append(s[pos:npos])
                ccont = string.join(skeep,"")
                if not skipping:
                    ptok=LSEtk_TokenComment(ccont)
                    curparent.contents.append(ptok)
                pos = npos
                _LSEtk_update_lineno(tinst,string.count(ccont,"\n"))
                
            #
            # line continuation has to go here so that we do not mess up
            # line continuations in pre-processor directives
            #
            # but it is kind of weird, so I'm going to turn it off for now..

            elif c == '\\' and (doCComments or doDirectives) and \
                 s[pos+1:pos+2]=="\n":
                _LSEtk_update_lineno(tinst,1)
                if not skipping:
                    # turn continuation into a space
                    curparent.contents.append(LSEtk_TokenEmpty(s[pos:pos+2]))
                pos = pos+2

            #
            # now look for terminators; putting it here allows whitespace
            # to be a terminator....
            #
            
            elif c == terminator:
                pos = pos + 1
                if terminator == "\n":
                    _LSEtk_update_lineno(tinst,1)
                    inpreproc = 0
                if skipping: continue

                #sys.stderr.write("Out %d %s\n" % (len(envStack),s[pos:]))

                # cannot just save curparent and use as thistok because
                # curparent might have got diverted to something else!
                (dummy,curparent, terminator, thistok, savedivno,
                 olcount) = envStack[-1]
                del envStack[-1]
                
                # put token in old diversion
                curparent.contents.append(thistok)

                if not gathering:
                    if savedivno != tinst.currDivertNo:
                        curparent = tinst.divertList[tinst.currDivertNo]

                elif envStack[-1][0]==2: # we have reached an id push
                    idarglist = thistok.contents
                    gathering = 0
                    idtok = curparent.contents[0]
                    break # head for the macro expansion....
                    
            #
            # whitespace is now good to look for
            #
            elif c in whitespace:
		if terminator and terminator in whitespace:
		   spos = pos
		   while c in whitespace and c != terminator and pos < slen:
		       pos = pos + 1
                       if(pos < slen):
                           c = s[pos]
		   ws = s[spos:pos]
		   if ws:
		       _LSEtk_update_lineno(tinst,string.count(ws,"\n"))
		       if not skipping:
                           curparent.contents.append(LSEtk_TokenEmpty(ws))
		else:
                    m = _LSEtk_WhitePattern.match(s,pos)
                    _LSEtk_update_lineno(tinst,string.count(m.group(),"\n"))
                    if not skipping:
                        curparent.contents.append(LSEtk_TokenEmpty(m.group()))
                    pos = m.end()

            #
            # start of id....
            #
            elif _LSEtk_IDFirstTable[ord(c)]:
                m = _LSEtk_IDPattern.match(s,pos)
                if (skipping):
                    pos = m.end()
                    continue
                
                idtok = LSEtk_TokenIdentifier(m.group())
                identry = _LSEtk_find_id(idtok.contents,dpath)
                if identry:

                    pos = m.end()
                    
                    idarglist = None

                    tinst.expandLevel = tinst.expandLevel+1
                    if tinst.debugMode:
                        tinst.debugFile.write("-%d- %s ...\n" % \
                                         (tinst.expandLevel, idtok.contents))

                    envStack.append((2,curparent,tinst.currDivertNo,
                                     idtok, gathering, identry))
                    curparent = LSEtk_TokenTopLevel([idtok])
                    
                    if identry[1] & LSEtk_EatLine:
                        # eat the rest of the line (almost very easy!)
                        pos2 = string.find(s,"\n",pos)
                        if (pos2 < 0):
                            idarglist = [LSEtk_TokenRaw(s[pos:])]
                            pos = slen
                        else:
                            idarglist = [LSEtk_TokenRaw(s[pos:pos2+1])]
                            pos = pos2+1
                        curparent.contents.append(LSEtk_TokenPList(idarglist))
                        _LSEtk_update_lineno(tinst,1)
                        gathering = 0
                    elif (identry[1] & LSEtk_NoArgs) or \
                         ((identry[1] & LSEtk_NoWhiteSpace) and \
                          _LSEtk_WhitePattern.match(s,m.end())):
                        # handle 0-argument macros and macros not allowing
                        # whitespace before arguments which actually have
                        # whitespace
                        gathering = 0
                    else:
                        # we are searching for arguments, so create a
                        # new parent token and continue
                        gathering = 1

                    break # go to token handling...
                    
                else: # skip over id and do not expand
                    if c == "@":
                        # this case is so that we can insert internal
                        # macros without them merging with previous
                        # identifiers.  One place we use this is
                        # when popping file names
                        if not skipping:
                            curparent.contents.append(LSEtk_TokenOther(c))
                        pos = pos + 1
                        idtok = None
                    else:
                        if not skipping: curparent.contents.append(idtok)
                        idtok = None
                        pos = m.end()

            #
            # start of number
            #
            elif c in digits:
                m = _LSEtk_NumberPattern.match(s,pos)
                if not skipping:
                    ptok = LSEtk_TokenNumber(s[pos:m.end()])
                    curparent.contents.append(ptok)
                pos = m.end()

            #
            # C comments and quotes
            #


            elif c == '"' and doCQuotes:
                (s,pos) = _LSEtk_handle_quote(tinst,curparent,skipping,
                                              s,pos,'"',streamer)
                slen = len(s)

            elif c == "'" and doCQuotes:
                (s,pos) = _LSEtk_handle_quote(tinst,curparent,skipping,
                                              s,pos,"'",streamer)

            elif c == "/" and doCComments:
                (s,pos,ctype) = handle_C_comment(tinst,curparent,skipping,s,
                                                 pos,streamer)
                                    
            #
            # preprocessor
            #
            elif c == "#" and doDirectives and not inpreproc:
                pos = pos + 1
                m = _LSEtk_WhiteIDPattern.match(s,pos)
                if m: identry = _LSEtk_find_id('#' + m.group(1),dpath)
                else: identry = None

                if not identry:
                    ptok = LSEtk_TokenPreProc([])

                    if not skipping:
                        envStack.append((1,curparent,terminator,ptok,
                                         tinst.currDivertNo, olcount))
                        terminator = "\n"
                        olcount = copy.deepcopy(tinst.inputSourceStack)
                        curparent = ptok
                        inpreproc = 1
                else:
                    idtok = LSEtk_TokenIdentifier('#'+m.group(1))
                    pos = m.end()
                    pos2 = _LSEtk_find_real_line_end(s,pos)
                    argstring = s[pos:pos2]
                    idarglist = [LSEtk_TokenRaw(argstring)]

                    tinst.expandLevel = tinst.expandLevel+1
                    if tinst.debugMode:
                        tinst.debugFile.write("-%d- %s ...\n" % \
                                         (tinst.expandLevel,
                                          idtok.contents))

                    envStack.append((2,curparent,tinst.currDivertNo,
                                     idtok, gathering, identry))
                    curparent = LSEtk_TokenTopLevel([idtok,
                                                     LSEtk_TokenPList(\
                        idarglist)])
                    _LSEtk_update_lineno(tinst,string.count(argstring,"\n")+1)
                    gathering = 0
                    pos = pos2+1
                    break
                    
            #
            # start characters
            #
            elif c in startchars and not skipping:
                tk = tclass[c]
                ptok = tk[0]([])
                pos = pos + 1
                envStack.append((1,curparent,terminator,ptok,
                                 tinst.currDivertNo, olcount))
                terminator = tk[1]
                olcount = copy.deepcopy(tinst.inputSourceStack)
                curparent = ptok
                #sys.stderr.write("In %d %s\n" % (len(envStack),s[pos:pos+5]))

            #
            # start characters
            #
            elif c == '(' and gathering and not skipping:
                ptok = LSEtk_TokenPList([])
                pos = pos + 1
                envStack.append((1,curparent,terminator,ptok,
                                 tinst.currDivertNo, olcount))
                terminator = ')'
                olcount = copy.deepcopy(tinst.inputSourceStack)
                curparent = ptok

            #
            # bad terminators
            #
            elif c in endchars and not skipping:
                if terminator:
                    raise LSEtk_TokenizeException("Extra %s when expecting a "
                                                  "%s " % (repr(c),
                                                           repr(terminator)),
                                                  tinst)
                else:
                    raise LSEtk_TokenizeException("Extra '%s'" % c,
                                                  tinst)

            #
            # other
            #
            else:
                if not skipping:
                    ptok = LSEtk_TokenOther(c)
                    curparent.contents.append(ptok)
                pos = pos + 1


        ##### INNER WHILE LOOP (pos < slen) NOW OVER
        else:
            pos = 0
            (s,slen) = LSEtke_streamer_getLine(streamer)
                
        #
        # anything allowed between an id and an argument list should be
        # found in this section; these are just C comments and whitespace
        # repeated....
        #
        if gathering:
            while(pos < slen):

                c = s[pos]

                #
                # whitespace
                #
                if c in whitespace:
                    m = _LSEtk_WhitePattern.match(s,pos)
                    _LSEtk_update_lineno(tinst,string.count(m.group(),"\n"))
                    if not skipping:
                        curparent.contents.append(LSEtk_TokenEmpty(m.group()))
                    pos = m.end()
                    continue

                # open parenthesis... push the environment, set idtok to None
                # and go back to the top loop....

                if c == '(':
                    ptok = LSEtk_TokenPList([])
                    pos = pos + 1
                    envStack.append((1,curparent,terminator,ptok,
                                     tinst.currDivertNo, olcount))
                    terminator = ')'
                    olcount = copy.deepcopy(tinst.inputSourceStack)
                    curparent = ptok
                    idtok = None
                    break
                
                #
                # C comments
                #
                
                if c == "/" and doCComments:

                    (s, pos, ctype) = handle_C_comment(tinst,curparent,
                                                       skipping,
                                                       s,pos,streamer)
                    
                    
                    if ctype != 2: continue
                    else: break

                # anything else, get out of here....

                break
        
        if idtok:  # finally, expand an id...

            # safe because a new diversion cannot have happened between
            # the time we set curparent to the id's tree
            thistok = curparent 
            (dummy,curparent,savedivno,idtok,gathering,identry) = envStack[-1]
            del envStack[-1]

            # did we not get args when we needed them?
            
            if (identry[1] & LSEtk_RequireArgs) and idarglist is None:
                # actually put the id on as an id with whatever was there....
                curparent.contents.extend(thistok.contents)
                # we will not have changed diversion, so there is nothing
                # to do for that...
                idtok = None
                tinst.expandLevel = tinst.expandLevel-1

                # (no need to put s back into the future)
            else:
                # if we did not get any arguments, we had better put back
                # the whitespace!  We also need to "uncount" any newlines
                # in it....
                if idarglist is None:
                    wslist = thistok.contents[1:]
                else: wslist = []

                tinst.divertList[savedivno] = curparent
                #thistok.parent = curparent

                # now do the invocation...
                rval = _LSEtk_invoke_id(tinst, idtok.contents, identry,
                                        idarglist, dpath, thistok, envStack)
                    
                tinst.expandLevel = tinst.expandLevel-1
                    
                # ... handle diversion changes
                if not gathering:
                    if savedivno != tinst.currDivertNo:
                        curparent = tinst.divertList[tinst.currDivertNo]
                
                # ... and splice in ...
                if type(rval) is types.StringType: # insert text
                    if rval: # not empty...
                        # put everything on the line after the id and args
                        # back into the future...
                        if (pos < slen):
                            LSEtke_streamer_prepend(streamer,s[pos:])
                            
                        # and whitespace after the id (if there were no args)
                        LSEtke_streamer_prepend(streamer,wslist)

                        # fix up the line numbers..
                        tinst.inputSourceStack[-1][1] += (
                            reduce(lambda x,y:x+str(y).count("\n"),wslist,0)+
                            rval.count("\n"))

                        # and finally put in the return value
                        LSEtke_streamer_prepend(streamer,rval)

                        # and get a whole line to continue with....
                        pos = 0
                        (s, slen) = LSEtke_streamer_getLine(streamer)
                        
                    else:
                        if wslist: curparent.contents.extend(wslist)
                        # leave (s/pos) alone
                else: # a list of tokens
                    # (leave s/pos alone)
                    if rval is not None:
                        curparent.contents.extend(rval)
                        if wslist: curparent.contents.extend(wslist)
                    else:
                        curparent.contents.extend(thistok.contents)
                        if wslist: curparent.contents.extend(wslist)
                    
                if tinst.flags.changed:
                    doM4Quotes = tinst.flags.doM4Quotes
                    doCQuotes = tinst.flags.doCQuotes
                    doM4Comments = tinst.flags.doM4Comments
                    doCComments = tinst.flags.doCComments
                    doMatchBraces = tinst.flags.doMatchBraces
                    doMatchParens = tinst.flags.doMatchParens
                    doDirectives = tinst.flags.doDirectives
                    startchars = tinst.flags.startchars
                    endchars = tinst.flags.endchars
                    skipping = tinst.flags.ppSkip
                    m4lcomm = tinst.flags.m4LeftComment
                    m4lcommlen = len(m4lcomm)
                    m4lquote = tinst.flags.m4LeftQuote
                    m4lquotelen = len(m4lquote)
                    m4rcomm = tinst.flags.m4RightComment
                    m4rquote = tinst.flags.m4RightQuote
                    m4rcommlen = len(m4rcomm)
                    m4rquotelen = len(m4rquote)
                    tinst.flags.clearChanged()
                    
                idtok = None # we've used up the id token            

    ### end of outer while loop
        
    if (terminator):
        if terminator != '\n':
            # if there is still a terminator needed, that is bad
            # restore the input source stack to what it was...
            tinst.inputSourceStack = olcount
            raise LSEtk_TokenizeException("Mismatched '%s'" % 
                                          _LSEtk_Otherside[terminator],
                                          tinst)
        else: del envStack[-1]
    
    (dummy,curparent, terminator, dummy,savedivno) = envStack[-1]
    
    tinst.divertList[savedivno] = curparent
        
    return

#########################################################################
#   Hooks for built-in tokenizer functions to use
#########################################################################

if _LSEtk_extension_present:
    LSEtke_init(LSEtk_TokenizeException,
                LSEtk_TokenEmpty,
                LSEtk_TokenComment,
                LSEtk_TokenIdentifier,
                LSEtk_TokenNumber,
                LSEtk_TokenM4Quote,
                LSEtk_TokenQuote,
                LSEtk_TokenOther,
                LSEtk_TokenRaw,
                LSEtk_TokenPList,
                LSEtk_TokenSBList,
                LSEtk_TokenCBList,
                LSEtk_TokenPreProc,
                LSEtk_TokenTopLevel,
                _LSEtk_FileWrapper,
                _LSEtk_BitBucket,
                string.whitespace,
                _LSEtk_find_id,
                _LSEtk_invoke_id
                )

#########################################################################
#   Other useful methods
#########################################################################

#
# reduction function for strings
#
def LSEtk_reducestr(x,y):
    return x+str(y)

#
# Reparent a token's descendants
#
def LSEtk_reparent_hier(t):
    if type(t.contents) is types.ListType:
        reduce(_LSEtk_reparenter_hier,t.contents,t)
    return t

def _LSEtk_reparenter_hier(parent,node):
    # clever... we use the reduction mechanism to pass along the invariant!
    #node.parent = parent
    if type(node.contents) is types.ListType:
        reduce(_LSEtk_reparenter_hier,node.contents,node)
    return parent
    
def _LSEtk_reparenter(parent,node):
    # clever... we use the reduction mechanism to pass along the invariant!
    #node.parent = parent
    return parent

#
# Unparent a token's descendants so that stupid garbage collectors work
# better.
#
def LSEtk_unparent_hier(t):
    #t.parent = None
    if type(t.contents) is types.ListType:
        reduce(_LSEtk_unparenter_hier,t.contents,t)
    return t

def _LSEtk_unparenter_hier(parent,node):
    # clever... we use the reduction mechanism to pass along the invariant!
    #node.parent = None
    if type(node.contents) is types.ListType:
        reduce(_LSEtk_unparenter_hier,node.contents,node)
    return parent
    
#
#
# Reparent and fix the whitespace for a token and its decendants...
#
def LSEtk_cleanup_token(tok):
    if type(tok.contents) is types.ListType:
        loopcarry = reduce(_LSEtk_cureducer,tok.contents,([],[],tok))
        if loopcarry[1]:
            loopcarry[0].append(LSEtk_TokenEmpty(string.join(loopcarry[1],"")))
        tok.contents = loopcarry[0]

def _LSEtk_cureducer(loopcarry,tok):
    # loopcarry = (new list, whitespace, parent)
    nl = loopcarry[0]
    ws = loopcarry[1]
    
    if isinstance(tok,LSEtk_TokenComment) \
       or isinstance(tok,LSEtk_TokenEmpty):
        
        ws.append(str(tok))

    else:
        
        ws.append(tok.begWhite)
        tok.begWhite = string.join(ws,"")
        del ws[:]
        nl.append(tok)
        #tok.parent = loopcarry[2]

        if type(tok.contents) is types.ListType:
            loopcarry2 = reduce(_LSEtk_cureducer,tok.contents,([],[],tok))
            if loopcarry2[1]:
                loopcarry2[0].append(\
                    LSEtk_TokenEmpty(string.join(loopcarry2[1],"")))
            tok.contents = loopcarry2[0]
        
    return loopcarry


#
# Reparent a list of tokens to a particular parent
#
def LSEtk_reparent_to(tlist,parent):
    if type(tlist) is types.ListType:
        for n in tlist:
            if type(n) is not types.StringType:
                #n.parent = parent
                pass
    return tlist

#
# look for a file along some search paths....
#
def LSEtk_find_file(paths,name):
    if (os.path.isfile(name)): return open(name)
    for p in paths:
        s = os.path.join(p,name)
        if (os.path.isfile(s)): return open(s)
    raise Exception("ugh")  # will be caught by functions using it.

#################### End of tokenizing code ############################

########################################################################
#
#     Standard LSE macros
#
########################################################################

def _LSEtk_set(tinfo,args,extraargs,env):
    if len(args)!=3:
        raise LSEtk_TokenizeException("LSEtk_set must be called "
                                      "with 2 arguments", tinst)
    cstring = ("tinfo[0].%s = %s" % (str(args[1]), str(args[2])))
    #sys.stderr.write(cstring + "\n")
    try:
        exec cstring
    except:
        raise LSEtk_TokenizeException("Unable to set tokenizer attribute "
                                      "'%s'" % str(args[1]), tinst)
    return []

def _LSEtk_push_if(tinfo,args,extraargs,env):
    tinfo[0].pushInputSource(("in file %s" % repr(args[1]),1))
    return []

def _LSEtk_pop_if(tinfo,args,extraargs,env):
    tinfo[0].popInputSource()
    return []

# if/else/endif....
#  the idea here is that we change the diversion on the "false" path
#  and leave it on the "true" path.... so:
#   if cond = true:
#       at if: push status 1, do not divert
#       at else: change status to 0, note diversion, divert to -1
#       at endif: divert to previous if status = 0, pop
#   if cond = false:
#       at if: push status to 0, note diversion, divert to -1
#       at else: change status to 1, divert to previous
#       at endif: divert to previous if status = 0, pop
def _LSEtk_pp_if(tinst,argstring,dpath):
    if len(tinst.ppStack):
        pstat = tinst.ppStack[-1][0] and tinst.ppStack[-1][1]
    else: pstat = 1
    if pstat:
        tinst.pushInputSource(("arguments of #LSE if",None))
        nval = LSEtk_TokenTopLevel([])
        LSEtke_tokenize_string_int(tinst,argstring,dpath,nval,1,1)
        tinst.popInputSource()
        try:
            ns = str(nval)
            if not ns: raise Exception("blah")

            ns=re.sub('!',' not ',ns)
            ns=re.sub('\|\|',' or ',ns)
            ns=re.sub('&&',' and ',ns)
            cond = eval(ns,{},{})
        except:
            raise LSEtk_TokenizeException("Unable to evaluate expression ("
                                          + argstring + ")=" + str(nval) +
                                          " in #LSE if ",tinst)
        if cond:
            tinst.ppStack.append((1,1,None,None,None))
        else:
            tinst.ppStack.append((1,0,tinst.currDivertNo,
                                  tinst.divertList[tinst.currDivertNo],
                                  dpath * 1))
            dpath[0:len(dpath)] = [ _LSEtk_TokReduced ]
            tinst.flags.ppSkip = 1
    else:
        tinst.ppStack.append((0,0,None,None,None))
    return []

def _LSEtk_pp_else(tinst,argstring,dpath):
    if not len(tinst.ppStack):
        raise LSEtk_TokenizeException("Unmatched #LSE else",tinst)
    entry = tinst.ppStack[-1]
    if entry[0]: # if this was a live if....
        if entry[1]: # in a recursively true branch....
            tinst.ppStack[-1] = (1,0,tinst.currDivertNo,
                                 tinst.divertList[tinst.currDivertNo],
                                 dpath * 1)
            dpath[0:len(dpath)] = [ _LSEtk_TokReduced ]
            tinst.flags.ppSkip = 1
        else: 
            dpath[0:len(dpath)] = entry[4]
            tinst.ppStack[-1] = (1,1,None,None,None)
            tinst.flags.ppSkip = 0
    return []

def _LSEtk_pp_endif(tinst,argstring,dpath):
    if not len(tinst.ppStack):
        raise LSEtk_TokenizeException("Unmatched #LSE endif",tinst)
    entry = tinst.ppStack[-1]
    del tinst.ppStack[-1]
    if entry[0]: # if this was a live if
        if not entry[1]: # do undivert
            dpath[0:len(dpath)] = entry[4]
            tinst.flags.ppSkip = 0
    return []

def _LSEtk_pp_include(tinst,argstring,dpath):
    # parse argumentsg
    argstring = string.strip(argstring)
    
    tinst.pushInputSource(("arguments of #LSE include",None))
    nval = LSEtk_TokenTopLevel([])
    LSEtke_tokenize_string_int(tinst,argstring,dpath,nval,1,1)
    tinst.popInputSource()

    if len(nval.contents)!=1 or \
       not isinstance(nval.contents[0],LSEtk_TokenQuote):
            raise LSEtk_TokenizeException("Unable to parse filename "
                                          "in #LSE include",tinst)
    fname = nval.contents[0].contents[1:-1]
    try:
        f=LSEtk_find_file(tinst.incPath,fname)
    except:
        raise LSEtk_TokenizeException("Unable to open file %s" %
                                      repr(fname), tinst)
    
    flines = [ "_LSEtk_push_if(%s)" % fname ]
    flines.extend(f.readlines())
    flines.append("@_LSEtk_pop_if()")
    f.close()
    return string.join(flines,"")

_LSEtk_ppDict = {
    'if' : (1,_LSEtk_pp_if),
    'else' : (1,_LSEtk_pp_else),
    'endif' : (1,_LSEtk_pp_endif),
    'include' : (0,_LSEtk_pp_include),
    }

def _LSEtk_pp_handler(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    dpath = tinfo[1]
    if len(args)>1:
        argstring = str(args[1])
        m = _LSEtk_WhiteIDPattern.match(argstring)
    else: m = None
    if not m:
        raise LSEtk_TokenizeException("No directive following #LSE", tinst)
    
    dname = m.group(1)
    argstring = argstring[m.end():]
    dentry = _LSEtk_ppDict.get(dname)
    if not dentry:
        raise LSEtk_TokenizeException("Unknown directive '%s' "
                                      "following #LSE" % (dname), tinst)
    
    if dentry[0] or not len(tinst.ppStack) or \
       (tinst.ppStack[-1][0] and tinst.ppStack[-1][1]):
        
        return dentry[1](tinst,argstring,dpath)
    
    else: return []
    
_LSEtk_TokReduced = {
    "#LSE" : [(LSEtk_Tok_macro,0,_LSEtk_pp_handler,None)],
    }

_LSEtk_TokBuiltins = {
    "LSEtk_set" : [(LSEtk_Tok_macro,0,_LSEtk_set,None)],
    "#LSE" : [(LSEtk_Tok_macro,0,_LSEtk_pp_handler,None)],
    }

_LSEtk_VitalBuiltins = {
    "@_LSEtk_pop_if" : [(LSEtk_m4_macro,0,_LSEtk_pop_if,None)], 
    "_LSEtk_push_if" : [(LSEtk_m4_macro,0,_LSEtk_push_if,None)], 
    }

##########################################################################
#
# Builtin m4 macros
#
##########################################################################

def _oldre2newre(s):
    # this macro converts from emacs-style m4 regular expressions to
    # the Perl-style python regular
    # not handled: \< \> \= \s from emacs
    # not avoided: \A \Z {}? (?...) from Perl
    i = 0
    l = len(s)
    ns = ""
    inbracket=0
    while i < l:
        if inbracket:
            cs = s[i:i+2]
            c = cs[0]
            if cs == r'\]':
                ns = ns + cs
                i = i + 1
            else:
                ns = ns + c
                if c == ']': inbracket=0
        else:
            c = s[i]
            if c == "\\":
                cs = s[i:i+2]
                d = cs[1]
                if d in "{}()|":
                    ns = ns + d
                else:
                    ns = ns + cs
                i = i + 1
            elif c == '[':
                ns = ns + c
                inbracket = 1
            elif c in "{}()|":
                ns = ns + "\\" + c
            elif c == '\n': ns = ns + "\\n"
            elif c == '\t': ns = ns + "\\t"
            elif c == '\r': ns = ns + "\\r"
            else:
                ns = ns + c
        i = i + 1
    return ns


def _LSEtk_m4___file__(tinfo,args,extraargs,env):
    if len(args)>1:
        sys.stderr.write("Warning: Excess arguments to builtin __file__ ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    return tinfo[0].getLineNo()[2]

def _LSEtk_m4___line__(tinfo,args,extraargs,env):
    if len(args)>1:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin __file__ ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    return tinfo[0].getLineNo()[0]

def _LSEtk_m4_builtin(tinfo,args,extraargs,env):
    if len(args): mname = args[1]
    else: mname = ""
    try:
        entry = _LSEtk_m4Builtins[mname][-1]
    except:
        raise LSEtk_TokenizerError("Undefined name %s\n" % mname,tinfo[0])
    return entry[2](tinfo,[mname]+args[1:],extraargs,env)

def _LSEtk_m4_changecom(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    if len(args)==1:
        tinst.flags.doM4Comment = 0
    elif len(args)==2:
        tinst.flags.doM4Comment = 1
        if args[1]: tinst.flags.m4LeftComment = args[1]
        else: tinst.flags.m4LeftComment = "#"
        tinst.flags.m4RightComment = "\n"
    else:
        tinst.flags.doM4Comment = 1
        if args[1]: tinst.flags.m4LeftComment = args[1]
        else: tinst.flags.m4LeftComment = "#"
        if args[2]: tinst.flags.m4RightComment = args[2]
        else: tinst.flags.m4RightComment = "\n"
        if len(args)>3:
            sys.stderr.write("Warning: Excess arguments to builtin "
                             "changecom ignored\n")
            if tinfo[0].flags.fatalWarnings:
                raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    return ""

def _LSEtk_m4_changequote(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    if len(args)==1:
        tinst.flags.doM4Quote = 0
    elif len(args)==2:
        tinst.flags.doM4Quote = 1
        if args[1]: tinst.flags.m4LeftQuote = args[1]
        else: tinst.flags.m4LeftQuote = "`" #` for emacs
        tinst.flags.m4RightQuote = "'"
    else:
        tinst.flags.doM4Quote = 1
        if args[1]: tinst.flags.m4LeftQuote = args[1]
        else: tinst.flags.m4LeftQuote = "`" #` for emacs
        if args[2]: tinst.flags.m4RightQuote = args[2]
        else: tinst.flags.m4RightQuote = "'"
        if len(args)>3:
            sys.stderr.write("Warning: Excess arguments to builtin "
                             "changequote ignored\n")
            if tinfo[0].flags.fatalWarnings:
                raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    return ""

def _LSEtk_m4_debugfile(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin debugfile ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])

    if len(args)==1:
        if tinfo[0].debugFile != sys.stderr:
            tinfo[0].debugFile.close()
        tinfo[0].debugFile = sys.stderr
    elif not args[1]:
        if tinfo[0].debugFile != sys.stderr:
            tinfo[0].debugFile.close()
        tinfo[0].debugFile = _LSEtk_BitBucket()
    else:
        if tinfo[0].debugFile != sys.stderr:
            tinfo[0].debugFile.close()
        tinfo[0].debugFile = open(args[1],"a")
    return ""

def _LSEtk_m4_debugmode(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin debugmode ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])

    if len(args)==1 or not args[1]:
        tinfo[0].debugMode = 0
    else:
        tinfo[0].debugMode = 1

    return ""

def _LSEtk_m4_decr(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin decr ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    try:
        a= string.atoi(args[1])-1
    except:
        raise LSEtk_TokenizeException("Non-numeric argument to builtin "
                                      "decr",tinfo[0])
    return repr(a)

def _LSEtk_m4_define(tinfo,args,extraargs,env):
    #sys.stderr.write("Defining %s\n" % args)
    tinst = tinfo[0]
    dpath = tinfo[1]
    if len(args)>3:
        sys.stderr.write("Warning: Excess arguments to builtin define "
                         "ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])

    if len(args)>2: d = args[2]
    else: d=""

    mname = args[1]
    if not mname: return ""
    
    if (len(d)>6) and d[0:5]=='\001\002\003\004\005':
        endex = string.index(d,'\006')
        d=d[5:endex]
        entry = eval(d)
        entry = (entry[0],entry[1],tinst.idMap[entry[2]])
    elif (len(d)>6) and d[0:5]=='\001\002\003\004\006':
        endex = string.index(d,'\005')
        d=d[5:endex]
        entry = eval(d)
        entry = (entry[0],entry[1],tinst.idMap[entry[2]],
                 tinst.idMap[entry[3]],None)
    else:
        entry = (LSEtk_m4_macro,LSEtk_NoWhiteSpace,d,None)

    if dpath[0].has_key(mname):
        dpath[0][mname][-1] = entry
    else:
        dpath[0][mname] = [entry]
    # destroy other definitions
    for d in dpath[1:]:
        if d.has_key(mname): del d[mname]
    return ""

def _LSEtk_m4_defn(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    dpath = tinfo[1]
    # Note: m4 only makes this do something useful while defining a function,
    # and only if this is the prefix of the definition (and everything after
    # it dies...)
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin defn ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    name = args[1]
    for d in dpath:
        if d.has_key(name):
            definition = d[name][-1]
            if definition[0] == LSEtk_m4_macro:
                if type(definition[2]) is types.StringType:
                    return tinst.flags.m4LeftQuote + definition[2] + \
                           tinst.flags.m4RightQuote
                else:
                    # We use funny control characters to wrap a reference
                    # which we can look up later when we use this definition
                    # in a define macro...
                    d2 = (definition[0],definition[1],id(definition[2]))
                    tinst.idMap[id(definition[2])]=definition[2]
                    return tinst.flags.m4LeftQuote + '\001\002\003\004\005' + \
                           repr(d2)+'\006' + tinst.flags.m4RightQuote
            elif definition[1] == LSEtk_Tok_macro:
                d2 = (definition[0],definition[1],id(definition[2]),
                      id(definition[3]))
                tinst.idMap[id(definition[2])]=definition[2]
                tinst.idMap[id(definition[3])]=definition[3]
                return tinst.flags.m4LeftQuote + '\001\002\003\004\006' + \
                       repr(d2)+'\005' + tinst.flags.m4RightQuote
    return ""

def _LSEtk_m4_divert(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin divert "
                         "ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)>1:
        try:
            dno = int(args[1])
        except ValueError, v:
            raise LSEtk_TokenizeException(str(v),tinst)
    else: dno = 0
    if dno < 0: # throw-away diversion
        tinst.currDivertNo = 0
    else:
        while dno+1 >= len(tinst.divertList):
            tinst.divertList.append(LSEtk_TokenTopLevel([]))
        tinst.currDivertNo = dno + 1
    return ""

def _LSEtk_m4_dnl(tinfo,args,extraargs,env):
    return ""

def _LSEtk_m4_divnum(tinfo,args,extraargs,env):
    if len(args)>1:
        sys.stderr.write("Excess arguments to builtin divnum ignored\n")
    return str(tinfo[0].currDivertNo-1)

def _LSEtk_m4_errprint(tinfo,args,extraargs,env):
    for a in args[1:]:
        sys.stderr.write(a)
    return ""

def _LSEtk_m4_esyscmd(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    if (len(args)>2):
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin esyscmd ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)<2:
        sys.stderr.write("Too few arguments to builtin esyscmd\n")
        return ""

    fd = os.popen(args[1],"r")
    s = string.join(fd.readlines(),"")
    tinst.sysval = fd.close()
    if not tinst.sysval: tinst.sysval = 0 # because no error == None
    return s

def _LSEtk_m4_eval(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin eval ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    try:
        s=re.sub('!',' not ',args[1])
        s=re.sub('\|\|',' or ',s)
        s=re.sub('&&',' and ',s)
        a= eval(s,{},{})
    except:
        raise LSEtk_TokenizeException("Bad expression for builtin eval",
                                      tinfo[0])
    return str(int(a))  # so no quotes on strings

def _LSEtk_m4_ifdef(tinfo,args,extraargs,env):
    if len(args)>4:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin ifdef ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    elif len(args)<3:
        sys.stderr.write("Too few arguments to builtin ifdef\n")
        return ""
    mname = args[1]
    for d in tinfo[1]:
        if d.has_key(mname): return args[2]
    if len(args)==4: return args[3]
    else: return ""

def _LSEtk_m4_ifelse(tinfo,args,extraargs,env):
    saveargs = args * 1
    if len(args)==2: return ""
    del args[0]
    while len(args)>=3:
        if (args[0] == args[1]): return args[2]
        del args[0:3]
    if len(args)==1: return args[0]  # else clause
    if len(args):
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin ifelse ignored\n")
        sys.stderr.write("If arguments %s\n" % saveargs)
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    return ""

def _LSEtk_m4_include(tinfo,args,reporterr,env):
    tinst = tinfo[0]
    if len(args)>2:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin %s ignored\n" % args[0])
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    try:
        f=LSEtk_find_file(tinst.incPath,args[1])
    except:
        if not reporterr: return ""
        raise LSEtk_TokenizeException("Unable to find file %s" %
                                      repr(args[1]), tinst)
    
    flines = [ "_LSEtk_push_if(%s)" % args[1] ]
    flines.extend(f.readlines())
    flines.append("@_LSEtk_pop_if()")
    f.close()
    return string.join(flines,"")

def _LSEtk_m4_incr(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin incr ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    try:
        a= string.atoi(args[1])+1
    except:
        raise LSEtk_TokenizeException("Non-numeric argument to builtin "
                                      "incr",tinfo[0])
    return repr(a)

def _LSEtk_m4_indir(tinfo,args,extraargs,env):
    if len(args): mname = args[1]
    else: mname = ""
    for d in tinfo[0]:
        if d.has_key(mname):
            entry = d[mname][-1]
            return entry[2](tinfo,[mname]+args[1:],extraargs,env)
    else:
        raise LSEtk_TokenizerError("Undefined name %s\n" % mname,tinfo[0])

def _LSEtk_m4_index(tinfo,args,extraargs,env):
    if len(args)>3:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin index ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)<3:
        sys.stderr.write("Too few arguments to builtin index\n")
        return ""
    return repr(string.find(args[1],args[2]))

def _LSEtk_m4_len(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin len "
                         "ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    return repr(len(args[1]))

def _LSEtk_m4_m4exit(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin m4exit ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)==1:
        raise LSEtk_Exit(0)
    try:
        a= string.atoi(args[1])
    except:
        raise LSEtk_TokenizeException("Non-numeric argument to builtin "
                                      "m4exit", tinfo[0])
    raise LSEtk_Exit(a)

def _LSEtk_m4_maketemp(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin maketemp ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    fname = args[1]
    if fname[-6:] != "XXXXXX": return ""
    tempfile.tempdir=None
    tempfile.template=fname[:-6]+'@'+str(os.getpid())+'.'
    return tempfile.mktemp()

def _LSEtk_m4_patsubst(tinfo,args,extraargs,env):
    if len(args)>4:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin patsubst ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)<3:
        sys.stderr.write("Too few arguments to builtin patsubst\n")
        return ""
    #sys.stderr.write("%s\n" % (args,))
    if len(args)>= 4: new = args[3]
    else: new = ""
    reo = re.compile(_oldre2newre(args[2]),re.MULTILINE)
    start=0; result=[]
    s=args[1]
    while (start<len(s)):
        rx = reo.search(s,start)
        if (rx is None):
            result.append(s[start:])
            break
        result.append(s[start:rx.start()]) # before the match
        i=0
        while (i<len(new)):
            c=new[i]
            i=i+1
            if c=='\\' and i<len(new):
                c=new[i]
                i=i+1
                if (c=='&'):
                    result.append(rx.group(0))
                    continue
                elif (c in string.digits):
                    off = ord(c) - ord('0')
                    l=rx.regs[off][0] ; r=rx.regs[off][1]
                    if l>=0 and off > 0 and off << len(rx.groups()):
                        result.append(rx.group(off))
                    continue
                else:
                    c = "\\" + c
            result.append(c)
        start = rx.end()
        if not len(rx.group(0)): # zero-length match needs to move on
            start = start + 1
            result.append(s[start-1]) # because we skip this character
    return string.join(result,"")

def _LSEtk_m4_pickarg(tinfo,args,extraargs,env):
    try:
        a= string.atoi(args[1])
    except:
        raise LSEtk_TokenizeException("Non-numeric argument to builtin "
                                      "pickarg\n", tinfo[0])
    if (len(args)==2): return ""
    if (a> len(args)-3 or a<1):
        if args[2]:
            raise LSEtk_TokenizeException("Argument %d of builtin pickarg "
                                          "out of range\n", tinfo[0])
        return ""
    return args[a+2]

def _LSEtk_m4_popdef(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    dpath = tinfo[1]
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin popdef "
                         "ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])

    mname = args[1]
    if not mname: return ""
    
    # remove newest definition
    for d in dpath:
        if d.has_key(mname):
            del d[mname][-1]
            break
    return ""
    
def _LSEtk_m4_pushdef(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    dpath = tinfo[1]
    if len(args)>3:
        sys.stderr.write("Warning: Excess arguments to builtin pushdef "
                         "ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)>2: d = args[2]
    else: d=""

    mname = args[1]
    if not mname: return ""
    
    if (len(d)>6) and d[0:5]=='\001\002\003\004\005':
        endex = string.index(d,'\006')
        d=d[5:endex]
        entry = eval(d)
        entry = (entry[0],entry[1],tinst.idMap[entry[2]])
    elif (len(d)>6) and d[0:5]=='\001\002\003\004\006':
        endex = string.index(d,'\005')
        d=d[5:endex]
        entry = eval(d)
        entry = (entry[0],entry[1],tinst.idMap[entry[2]],
                 tinst.idMap[entry[3]],None)
    else:
        entry = (LSEtk_m4_macro,LSEtk_NoWhiteSpace,d,None)

    if dpath[0].has_key(mname):
        dpath[0][mname].append(entry)
    else:
        dpath[0][mname] = [entry]
    return ""

def _LSEtk_m4_python(tinfo,args,extraargs,env):
    if len(args)>3:
        sys.stderr.write("Excess arguments to builtin python ignored\n")
        raise LSEtk_TokenizeException("Not good",tinfo[0])
    savestdout = sys.stdout
    sys.stdout = f = cStringIO.StringIO()
    try:
        cp = compile(args[1],"<input>", 'single')
        exec cp in tinfo[0].pythonEnv
    except:
        sys.stdout = savestdout
        f.close()
        sys.stderr.write("Python error: "
                         "python text was <<<%s>>>\n" % args[1])
        raise
        
    rstring = f.getvalue()
    if (len(args)==2):
        try:
            if rstring[-1] == '\n':
                rstring = rstring[:-1]
        except:
            pass
        f.close()
    sys.stdout = savestdout
    #if rstring=='True': rstring=1
    #elif rstring=='False': rstring=0
    return rstring
    
def _LSEtk_m4_pythonfile(tinfo,args,extraargs,env):
    if len(args)>2:
        sys.stderr.write("Excess arguments to builtin pythonfile ignored\n")
        raise LSEtk_TokenizeException("Not good",tinfo[0])
    savestdout = sys.stdout
    sys.stdout = f = cStringIO.StringIO()
    try:
        exec args[1] in tinfo[0].pythonEnv
    except:
        sys.stdout = savestdout
        f.close()
        sys.stderr.write("Python error: "
                         "python text was <<<%s>>>\n" % args[1])
        raise
    rstring = f.getvalue()
    f.close()
    sys.stdout = savestdout
    if rstring=='True': rstring=1
    elif rstring=='False': rstring=0
    return rstring

def _LSEtk_m4_regexp(tinfo,args,extraargs,env):
    if len(args)>4:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin regexp ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)<3:
        sys.stderr.write("Too few arguments to builtin regexp\n")
        raise SystemExit(1)
        return ""
    if len(args)==4:
        rx = re.search(_oldre2newre(args[2]),args[1],re.MULTILINE)
        result=[]
        new = args[3]
        i=0
        while (i<len(new)):
            c=new[i]
            i=i+1
            if c=='\\' and i<len(new):
                c=new[i]
                i=i+1
                if (c=='&'):
                    if rx:
                        result.append(rx.group(0))
                    continue
                elif (c in string.digits):
                    off = ord(c) - ord('0')
                    if rx and off > 0 and off <= len(rx.groups()) :
                        result.append(rx.group(off))
                    continue
                else:
                    c = "\\" + c
            result.append(c)
        return string.join(result,"")
    else:
        rx = re.search(_oldre2newre(args[2]),args[1],re.MULTILINE)
        if rx is None: return str(-1)
        else: return str(rx.start())

def _LSEtk_m4_shift(tinfo,args,extraargs,env):
    if len(args)<3: return ""
    return reduce(lambda x,y:x+","+y,args[2:])

def _LSEtk_m4_substr(tinfo,args,extraargs,env):
    if len(args)>4:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin substr ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)<3:
        sys.stderr.write("Too few arguments to builtin substr\n")
        return ""
    try:
        start=string.atoi(args[2])
    except:
        raise LSEtk_TokenizerError("Non-numeric argument to builtin substr\n",
                                   tinfo[0])
    if (len(args)==4):
        try:
            length=string.atoi(args[3])
        except:
            raise LSEtk_TokenizerError("Non-numeric argument to builtin "
                                       "substr\n",tinfo[0])
        if (start>=len(args[1])): return ""
        try:
            return args[1][start:start+length]
        except:
            return args[1][start:]
    else:
        if (start>=len(args[1])): return ""
        return args[1][start:]

def _LSEtk_m4_syscmd(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    if (len(args)>2):
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin syscmd ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])        
    if len(args)<2:
        sys.stderr.write("Too few arguments to builtin syscmd\n")
        return ""

    tinst.sysval = os.system(args[1])
    return ""
        
def _LSEtk_m4_sysval(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    return str(tinst.sysval)
        
def _LSEtk_expandranges(s):
    # will not work properly for non-C locales and non-Ascii, but hey, OK...
    pos=0
    news=[]
    while (pos<len(s)):
        c=s[pos]
        if (c=='-' and pos>0 and pos<len(s)-1):
            l=ord(s[pos-1])
            r=ord(s[pos+1])
            if (l<r):
                news.extend(map(lambda x:chr(x),range(l+1,r+1)))
            else:
                news.extend(map(lambda x:chr(x),range(l-1,r-1,-1)))
            pos=pos+2
            continue
        news.append(c)
        pos=pos+1
    return string.join(news,"")


def _LSEtk_m4_translit(tinfo,args,extraargs,env):
    if len(args)>4:
        sys.stderr.write("Warning: "
                         "Excess arguments to builtin translit ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)<3:
        sys.stderr.write("Too few arguments to builtin translit\n")
        return ""

    charstring=_LSEtk_expandranges(args[2])
    if len(args)==4:
        replstring=_LSEtk_expandranges(args[3])
        if len(replstring) < len(charstring):
            delstring=replstring[len(charstring):]
        else:
            delstring=""
            if len(replstring) > len(charstring):
                replstring=replstring[:len(charstring)]
    else:
        delstring=charstring
        replstring=charstring
    ttable = string.maketrans(charstring,replstring)
    return string.translate(args[1],ttable,delstring)

def _LSEtk_m4_undefine(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    dpath = tinfo[1]
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin undefine "
                         "ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])

    mname = args[1]
    if not mname: return ""
    
    # remove all definitions....
    for d in dpath:
        if d.has_key(mname): del d[mname]
    return ""

def _LSEtk_m4_undivert(tinfo,args,extraargs,env):
    tinst = tinfo[0]
    if len(args)>2:
        sys.stderr.write("Warning: Excess arguments to builtin undivert "
                         "ignored\n")
        if tinfo[0].flags.fatalWarnings:
            raise LSEtk_tokenizeExcept("Fatal warning", tinfo[0])
    if len(args)>1:
        try:
            dno = int(args[1])
        except ValueError, v:
            raise LSEtk_TokenizeException(str(v),tinst)
        try:
            rdiv = tinst.divertList[dno]
        except IndexError:
            return ""
        if dno+1 == tinst.currDivertNo: return ""
        else:
            # clean out the diversion
            tinst.divertList[dno+1] = LSEtk_TokenTopLevel([])
            # and return the old diversion directly so it won't be reparsed
            return rdiv
    else:
        if tinst.currDivertNo != 1: return ""
        rdlist = tinst.divertList[2:]
        del tinst.divertList[2:]
        return rdlist

def _LSEtk_m4_unsupported(tinfo,args,extraargs,env):
    raise LSEtk_TokenizeException("builtin %s not supported" % args[0],
                                  tinfo[0])
  
_LSEtk_m4Builtins = {
    
    ##### unsupported macros...

    "changeword" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                     _LSEtk_m4_unsupported, 1)],
    "dumpdef" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                 _LSEtk_m4_unsupported, 1)],
    "format" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                 _LSEtk_m4_unsupported, 1)],
    "m4wrap" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                 _LSEtk_m4_unsupported, 1)],
    "traceon" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                 _LSEtk_m4_unsupported, 1)],
    "traceoff" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                 _LSEtk_m4_unsupported, 1)],
    
    # and we do not define __gnu__, __unix__, or unix, as we are none of those

    # supported macros
    
    "__file__" : [ (LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                   _LSEtk_m4___file__, None) ],
    "__line__" : [ (LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                   _LSEtk_m4___line__, None) ],
    "builtin" : [ (LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                   _LSEtk_m4_builtin, None) ],
    "changecom" : [ (LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                     _LSEtk_m4_changecom, None) ],
    "changequote" : [ (LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                       _LSEtk_m4_changequote, None) ],
    "debugfile" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                    _LSEtk_m4_debugfile, 1)],
    "debugmode" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                      _LSEtk_m4_debugmode, None)],
    "decr" : [ (LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                _LSEtk_m4_decr, None)],
    "define" : [ (LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                  _LSEtk_m4_define, None)],
    "defn" : [(LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
               _LSEtk_m4_defn, None)],
    "divert" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
               _LSEtk_m4_divert, None)],
    "divnum" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                 _LSEtk_m4_divnum, None)],
    "dnl" : [(LSEtk_m4_macro, LSEtk_EatLine|LSEtk_NoArgs|LSEtk_NoWhiteSpace,
              _LSEtk_m4_dnl, None)],
    "errprint" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                   _LSEtk_m4_errprint, None)],
    "esyscmd" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                 _LSEtk_m4_esyscmd, 1)],
    "eval" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
               _LSEtk_m4_eval, 1)],
    "ifdef" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                 _LSEtk_m4_ifdef, 1)],
    "ifelse" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                 _LSEtk_m4_ifelse, 1)],
    "include" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
               _LSEtk_m4_include, 1)],
    "incr" : [ (LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                _LSEtk_m4_incr, None)],
    "index" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
               _LSEtk_m4_index, 1)],
    "indir" : [ (LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                 _LSEtk_m4_indir, None) ],
    "len" : [(LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
              _LSEtk_m4_len, None)],
    "m4exit" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
              _LSEtk_m4_m4exit, None)],
    "maketemp" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                   _LSEtk_m4_maketemp, 1)],
    "patsubst" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                   _LSEtk_m4_patsubst, 0)],
    "popdef" : [(LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                 _LSEtk_m4_popdef, None)],
    "pushdef" : [(LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                 _LSEtk_m4_pushdef, None)],
    "regexp" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                 _LSEtk_m4_regexp, 0)],
    "shift" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
               _LSEtk_m4_shift, 0)],
    "sinclude" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                   _LSEtk_m4_include, 0)],
    "substr" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                 _LSEtk_m4_substr, 0)],
    "syscmd" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                 _LSEtk_m4_syscmd, 1)],
    "sysval" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace,
                 _LSEtk_m4_sysval, 1)],
    "translit" : [(LSEtk_m4_macro, LSEtk_NoWhiteSpace|LSEtk_RequireArgs,
                   _LSEtk_m4_translit, 0)],
    "undefine" : [ (LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                    _LSEtk_m4_undefine, None)],
    "undivert" : [ (LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                    _LSEtk_m4_undivert, None)],

    # and new macros...
    
    "__liberty__" : [(LSEtk_m4_macro, LSEtk_NoArgs, "", None) ],
    
    "pickarg" : [(LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                  _LSEtk_m4_pickarg, None)],
    "python" : [(LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                 _LSEtk_m4_python, None)],
    "pythonfile" : [(LSEtk_m4_macro, LSEtk_RequireArgs|LSEtk_NoWhiteSpace,
                     _LSEtk_m4_pythonfile, None)],

    }

_LSEtk_PrefixedM4Builtins = {}
for (key,info) in _LSEtk_m4Builtins.items():
    _LSEtk_PrefixedM4Builtins["m4_" + key] = info

