from distutils.core import setup, Extension

tok = Extension('LSETokenizerExt',
		sources = ['LSETokenizerExt.c'])

setup(name='LSETokenizerExt',
      version='1.0',
      description = 'Tokenizer extensions',
      author = 'David A. Penry',
      url = 'http://liberty.princeton.edu',
      long_description = '''
  An extension module allowing us to parse code much more rapidly
  than if we were doing it all in Python
''',
	ext_modules=[tok])

