/*  -*- c++ -*-
 * Copyright (c) 2000-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Linux OS template file for the PowerPC emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *          David August <august@cs.princeton.edu>
 * 	    Ram Rangan <ram@cs.princeton.edu>
 *
 * TODO: 
 *      - extra information fields on faults
 *
 */
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#undef __USE_LARGEFILE64
#include "LSE_PowerPC.priv.h"

#define LSE_LINUX_COMPAT
#define LSE_LINUX_CHECKPOINT
#define LSE_LINUX_HEADERS
#define LSE_LINUX_HAS_SA_RESTORER
#include "OS/LSE_Linux.h"

namespace LSE_PowerPC {

extern LSE_emu_interface_t PPC_interface;
extern int EMU_context_copy(LSE_emu_interface_t *,
       	   	 	    PPC_context_t **, PPC_context_t *);
extern int EMU_context_destroy(LSE_emu_interface_t *, LSE_emu_ctoken_t);

extern LSE_chkpt::error_t EMU_write_context(LSE_chkpt::file_t *cptFile,
					    int nmark, PPC_context_t *ctx,
					    LSE_emu_chkpt_cntl_t *ctl);

extern LSE_chkpt::error_t EMU_parse_context(LSE_emu_interface_t *intr,
       			  		    LSE_chkpt::file_t *cptFile,
                                            PPC_context_t *&realct,
					    LSE_emu_chkpt_cntl_t *ctl);

static inline LSE_emu_addr_t 
  translate_elf_entry(PPC_context_t *context, LSE_emu_addr_t addr,
		      LSE_emu_addr_t loadbias);

#define LSE_LINUX_BASE
#define CC_isacontext_t     PPC_context_t
const bool Linux_bigendian = true;
#include "OS/LSE_Linux.h"

#define PPC_PAGE_SIZE UINT64_C(0x1000)
#define PAGE_BITS 12

#define PPC_TASK_SIZE64   (UINT64_C(0x1)<<44)
#define PPC_USTACK64_TOP  (PPC_TASK_SIZE64) 
#define PPC_USTACK64_BASE (PPC_USTACK64_TOP - UINT64_C(0x1000000) + 8) 
#define PPC_UBASE64       (PPC_TASK_SIZE64/4)

#define PPC_TASK_SIZE32   ((UINT64_C(0x1)<<32)-PPC_PAGE_SIZE)
#define PPC_USTACK32_TOP  (PPC_TASK_SIZE32) 
#define PPC_USTACK32_BASE (PPC_USTACK32_TOP - UINT64_C(0x1000000) + 4) 
#define PPC_UBASE32       ((PPC_TASK_SIZE32+PPC_PAGE_SIZE)/4)

struct OSBDefs : public OSBaseDefs {
  // fcntl
  static const int oO_DIRECTORY =  040000;
  static const int oO_NOFOLLOW  = 0100000;
  static const int oO_LARGEFILE = 0200000;
  static const int oO_DIRECT    = 0400000;

  // ioctls
  static const unsigned oTCGETS  = 0x402c7413;
  static const unsigned oTCGETA  = 0x40147417;
  static const unsigned oTCSETAW = 0x80147419;

  // termios
  static const int oNCC = 10;

  // ========== Other stuff ==============

  static const int oNUM_DLINFO_ENTRIES = 1;
  // Hopefully HWCAP = 0 is fine, the kernel for PPC does some computation
  //  to figure out what instruction set is available, but 
  //  for PPC64 it does not do this
  static const int oHWCAP              = 0;
  static const int oCLKTCK             = 100;

  static const unsigned oPAGE_SIZE   = PPC_PAGE_SIZE;
  static const unsigned oPAGE_BITS   = PAGE_BITS;
  static const int oCLONE_STACK_SIZE = 0;
  static const uint64_t oRLIMIT_INFINITY = ~uint64_t(0);

  static LSE_emu_addr_t oUTASK_SIZE(bool is64bit) { 
    return (is64bit ? PPC_TASK_SIZE64 : PPC_TASK_SIZE32);
  }
  static LSE_emu_addr_t oUNMAPPED_BASE(bool is64bit) { 
    return (is64bit ? PPC_UBASE64 : PPC_UBASE32);
  }
  static LSE_emu_addr_t oUSTACK_BASE(bool is64bit) {
    return (is64bit ? PPC_USTACK64_BASE : PPC_USTACK32_BASE);
  }
  static LSE_emu_addr_t oUSTACK_TOP(bool is64bit) { 
    return (is64bit ? PPC_USTACK64_TOP : PPC_USTACK32_TOP);
  }

  static const char *oDYNLOADER;
  static const char *oSYSNAME;
  static const char *oNODENAME;
  static const char *oRELEASE;
  static const char *oVERSION;
  static const char *oMACHINE;
  static const char *oDOMAINNAME;

  static const uint64_t oTIMESLICE = 10 * 1000 * 1000; // 10 ms

  // base types

  typedef  uint8_t L_cc_t;
  typedef  int64_t L_clock_t;
  typedef  int64_t L_long;
  typedef uint64_t L_ptr_t;
  typedef uint64_t L_size_t;
  typedef uint32_t L_speed_t;
  typedef  int64_t L_statfs_word;
  typedef  int64_t L_suseconds_t;
  typedef uint32_t L_tcflag_t;
  typedef  int64_t L_time_t;
  typedef uint64_t L_ulong;
  typedef uint64_t L_uptr_t;

  typedef  int32_t L_compat_clock_t;
  typedef  int32_t L_compat_long;
  typedef uint32_t L_compat_size_t;
  typedef  int32_t L_compat_suseconds_t;
  typedef  int32_t L_compat_time_t;
  typedef uint32_t L_compat_ulong;
  typedef uint32_t L_compat_uptr_t;

};

const char *OSBDefs::oDYNLOADER  = "ld.so";
const char *OSBDefs::oSYSNAME    = "Linux";
const char *OSBDefs::oNODENAME   = "LibertyPowerPC";
const char *OSBDefs::oRELEASE    = "2.6.31-1liberty";
const char *OSBDefs::oVERSION    = "Emulated #1 Fri Jun 8 13:06:07 EDT 2001";
const char *OSBDefs::oMACHINE    = "PPC";
const char *OSBDefs::oDOMAINNAME = "unknown";

  const int OSBDefs::oO_DIRECTORY;
  const int OSBDefs::oO_NOFOLLOW;
  const int OSBDefs::oO_LARGEFILE;
  const int OSBDefs::oO_DIRECT;
  const unsigned OSBDefs::oTCGETS;
  const unsigned OSBDefs::oTCGETA;
  const unsigned OSBDefs::oTCSETAW;
  const int OSBDefs::oNCC;
  const int OSBDefs::oNUM_DLINFO_ENTRIES;
  const int OSBDefs::oHWCAP;
  const int OSBDefs::oCLKTCK;
  const unsigned OSBDefs::oPAGE_SIZE;
  const unsigned OSBDefs::oPAGE_BITS;
  const uint64_t OSBDefs::oRLIMIT_INFINITY;
  const int OSBDefs::oCLONE_STACK_SIZE;
  const uint64_t OSBDefs::oTIMESLICE;

struct OSDefs : public OSDerivedDefs<OSBDefs> {

  typedef PPC_fault_t CC_fault_t;

  struct L___sysctl_args {
    LSE_emu_addr_t LSE_name;
    uint32_t       LSE_nlen;
    LSE_emu_addr_t LSE_oldval;
    LSE_emu_addr_t LSE_oldlenp;
    LSE_emu_addr_t LSE_newval;
    L_size_t       LSE_newlen;
  };

  struct L_newstat {
    uint64_t     LSE_st_dev;
    uint64_t     LSE_st_ino;
    uint64_t     LSE_st_nlink;
    uint32_t     LSE_st_mode;
    uint32_t     LSE_st_uid;
    uint32_t     LSE_st_gid;
    uint32_t     LSE_pad2;
    uint64_t     LSE_st_rdev;
    uint64_t     LSE_st_size;
    uint64_t     LSE_st_blksize;
    uint64_t     LSE_st_blocks;
    int64_t      LSE_st_atime;
    int64_t      LSE_reserved0;
    int64_t      LSE_st_mtime;
    int64_t      LSE_reserved1;
    int64_t      LSE_st_ctime;
    int64_t      LSE_reserved2;
    int64_t      LSE_unused[3];
    L_newstat& operator =(const struct stat &buf) {
      LSE_st_dev     = LSE_h2b((uint64_t)buf.st_dev);
      LSE_st_ino     = LSE_h2b((uint64_t)buf.st_ino);
      LSE_st_nlink   = LSE_h2b((uint64_t)buf.st_nlink);
      LSE_st_mode    = LSE_h2b((uint32_t)buf.st_mode);
      LSE_st_uid     = LSE_h2b((uint32_t)buf.st_uid);
      LSE_st_gid     = LSE_h2b((uint32_t)buf.st_gid);
      LSE_pad2 = 0;
      LSE_st_rdev    = LSE_h2b((uint64_t)buf.st_rdev);
      LSE_st_size    = LSE_h2b((uint64_t)buf.st_size);
      LSE_st_blksize = LSE_h2b((uint64_t)buf.st_blksize);
      LSE_st_blocks  = LSE_h2b((uint64_t)buf.st_blocks);
      LSE_st_atime   = LSE_h2b((int64_t)buf.st_atime);
      LSE_reserved0 = 0;
      LSE_st_mtime   = LSE_h2b((int64_t)buf.st_mtime);
      LSE_reserved1 = 0;
      LSE_st_ctime   = LSE_h2b((int64_t)buf.st_ctime);
      LSE_reserved2 = 0;
      LSE_unused[0] = LSE_unused[1] = LSE_unused[2] = LSE_unused[3] = 0;
    }
  };

  struct L_sigaltstack {
    L_uptr_t LSE_ss_sp;
    int32_t  LSE_ss_flags;
    int32_t  LSE_pad0; // ensure alignment
    L_size_t LSE_ss_size;

    inline L_sigaltstack e2h() {
      L_sigaltstack nb;
      nb.LSE_ss_sp    = LSE_b2h(LSE_ss_sp);
      nb.LSE_ss_flags = LSE_b2h(LSE_ss_flags);
      nb.LSE_ss_size  = LSE_b2h(LSE_ss_size);
      return nb;
    }
    inline L_sigaltstack h2e() { return e2h(); }
  };

  struct L_termios {
    L_tcflag_t LSE_c_iflag;
    L_tcflag_t LSE_c_oflag;
    L_tcflag_t LSE_c_cflag;
    L_tcflag_t LSE_c_lflag;
    L_cc_t     LSE_c_cc[19];
    L_cc_t     LSE_c_line;
    L_speed_t  LSE_c_ispeed;
    L_speed_t  LSE_c_ospeed;
#ifdef TCGETS
    L_termios& operator=(const struct termios &buf) {
      LSE_c_iflag = LSE_h2b((L_tcflag_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((L_tcflag_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((L_tcflag_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((L_tcflag_t)buf.c_lflag);
      if (oNCCS <= NCCS) {
	for (int i=0;i<oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<oNCCS;i++) LSE_c_cc[i] = 0;
      }
#ifdef __linux
      LSE_c_line = buf.c_line;
      LSE_c_ispeed = LSE_h2b((L_speed_t)buf.c_ispeed);
      LSE_c_ospeed = LSE_h2b((L_speed_t)buf.c_ospeed);
#else
      LSE_c_line = 0; // N_TTY
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
#endif
    }
#elif defined(TCGETA)
    L_termios& operator=(const struct termio &buf) {
      LSE_c_iflag = LSE_h2b((L_tcflag_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((L_tcflag_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((L_tcflag_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((L_tcflag_t)buf.c_lflag);
      if (oNCCS <= NCCS) {
	for (int i=0;i<oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<oNCCS;i++) LSE_c_cc[i] = 0;
      }
#ifdef __linux
      LSE_c_line = buf.c_line;
#else
      LSE_c_line = 0; // N_TTY
#endif
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
    }
#endif
  };

  struct L_compat_newstat {
    uint32_t     LSE_st_dev;
    uint32_t     LSE_st_ino;
    uint32_t     LSE_st_mode;
    uint16_t     LSE_st_nlink;
    uint32_t     LSE_st_uid;
    uint32_t     LSE_st_gid;
    uint32_t     LSE_st_rdev;
    int32_t      LSE_st_size;
    int32_t      LSE_st_blksize;
    int32_t      LSE_st_blocks;
    uint32_t     LSE_st_atime;
    int32_t      LSE_reserved0;
    uint32_t     LSE_st_mtime;
    int32_t      LSE_reserved1;
    uint32_t     LSE_st_ctime;
    int32_t      LSE_reserved2;
    int32_t      LSE_unused[2];
    L_compat_newstat& operator =(const struct stat &buf) {
      LSE_st_dev     = LSE_h2b((uint32_t)buf.st_dev);
      LSE_st_ino     = LSE_h2b((uint32_t)buf.st_ino);
      LSE_st_mode    = LSE_h2b((uint32_t)buf.st_mode);
      LSE_st_nlink   = LSE_h2b((uint16_t)buf.st_nlink);
      LSE_st_uid     = LSE_h2b((uint32_t)buf.st_uid);
      LSE_st_gid     = LSE_h2b((uint32_t)buf.st_gid);
      LSE_st_rdev    = LSE_h2b((uint32_t)buf.st_rdev);
      LSE_st_size    = LSE_h2b((int32_t)buf.st_size);
      LSE_st_blksize = LSE_h2b((int32_t)buf.st_blksize);
      LSE_st_blocks  = LSE_h2b((int32_t)buf.st_blocks);
      LSE_st_atime   = LSE_h2b((uint32_t)buf.st_atime);
      LSE_reserved0 = 0;
      LSE_st_mtime   = LSE_h2b((uint32_t)buf.st_mtime);
      LSE_reserved1 = 0;
      LSE_st_ctime   = LSE_h2b((uint32_t)buf.st_ctime);
      LSE_reserved2 = 0;
      LSE_unused[0] = LSE_unused[1] = 0;
    }
  };

  struct L_compat_sigaltstack {
    uint32_t        LSE_ss_sp;
    int32_t         LSE_ss_flags;
    int32_t         LSE_ss_size;
    inline L_compat_sigaltstack e2h() {
      L_compat_sigaltstack nb;
      nb.LSE_ss_sp    = LSE_b2h(LSE_ss_sp);
      nb.LSE_ss_flags = LSE_b2h(LSE_ss_flags);
      nb.LSE_ss_size  = LSE_b2h(LSE_ss_size);
      return nb;
    }
    inline L_compat_sigaltstack h2e() { return e2h(); }
  };

  struct L_compat_sigset { 
    // You have signals 1-32 in the lower word and
    // 33-64 in the upper word but each word is big-endian.
    uint32_t sig[2];
    uint64_t toword() {
      uint64_t v = 0;
      v = LSE_b2h(sig[1]);
      v <<= 32;
      v |= LSE_b2h(sig[0]);
      return v;
    }
    void fromword(uint64_t v) { 
      sig[0] = LSE_h2b((uint32_t)v);
      v >>= 32;
      sig[1] = LSE_h2b((uint32_t)v);
    }
  };

  struct L_compat_sigaction { 
    // redefined because L_compat_sigset is redefined
    L_compat_uptr_t LSE_sa_handler;
    L_compat_ulong  LSE_sa_flags;
#ifdef LSE_LINUX_HAS_SA_RESTORER
    L_compat_uptr_t LSE_sa_restorer;
#endif
    L_compat_sigset  LSE_sa_mask;
    inline struct Linux_sighandinf_t tosighand() {
      Linux_sighandinf_t nv;
      nv.handler = LSE_e2h(LSE_sa_handler, Linux_bigendian);
      nv.flags   = LSE_e2h(LSE_sa_flags, Linux_bigendian);
      nv.sigmask = LSE_sa_mask.toword();
      return nv;
    }
    L_sigaction& operator=(const struct Linux_sighandinf_t &v) {
      LSE_sa_handler = LSE_h2e((L_compat_uptr_t)v.handler, Linux_bigendian);
      LSE_sa_flags   = LSE_h2e((L_compat_ulong)v.flags, Linux_bigendian);
#ifdef LSE_LINUX_HAS_SA_RESTORER
      LSE_sa_restorer = 0;
#endif
      LSE_sa_mask.fromword(v.sigmask);
    }
  };

  typedef L_compat_sigaction L_sigaction32;

  struct L_compat_statfs {
    int32_t     LSE_f_type;     /* 0 */
    int32_t     LSE_f_bsize;     /* 0 */
    int32_t     LSE_f_blocks;     /* 0 */
    int32_t     LSE_f_bfree;     /* 0 */
    int32_t     LSE_f_bavail;     /* 0 */
    int32_t     LSE_f_files;     /* 0 */
    int32_t     LSE_f_ffree;     /* 0 */
    int32_t     LSE_pad1[2];   /* actually LSE_f_fsid */
    int32_t     LSE_f_namelen;     /* 0 */
    int32_t     LSE_f_frsize;
    int32_t     LSE_pad2[5];
#if defined (__SVR4) && defined(__sun)
    typedef struct statvfs thisstat;
#else
    typedef struct statfs thisstat;
#endif

    L_compat_statfs& operator=(const thisstat &buf) {
#if defined (__SVR4) && defined (__sun)
      LSE_f_type   = LSE_h2b((int32_t)0xEF53);
#else
      LSE_f_type   = LSE_h2b((int32_t)buf.f_type);
#endif
      LSE_f_bsize  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_blocks = LSE_h2b((int32_t)buf.f_type);
      LSE_f_bfree  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_bavail = LSE_h2b((int32_t)buf.f_type);
      LSE_f_files  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_ffree  = LSE_h2b((int32_t)buf.f_type);
      LSE_pad1[0] = LSE_pad1[1] = 0;
#if defined (__SVR4) && defined (__sun)
      LSE_f_namelen = LSE_h2b((int32_t)buf.f_namemax);
#else
      LSE_f_namelen = LSE_h2b((int32_t)buf.f_namelen);
#endif
      LSE_f_frsize = 0;
      for (int i = 0; i < 5; ++i) LSE_pad2[i] = 0;
    }
  };

  struct L_compat_termio {
    uint16_t LSE_c_iflag;
    uint16_t LSE_c_oflag;
    uint16_t LSE_c_cflag;
    uint16_t LSE_c_lflag;
    uint8_t  LSE_c_line;
    uint8_t  LSE_c_cc[8];
    L_compat_termio& operator=(const struct termio &buf) {
      LSE_c_iflag = LSE_h2b((uint16_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((uint16_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((uint16_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((uint16_t)buf.c_lflag);
      LSE_c_line = buf.c_line;
      if (8 <= NCC) {
	for (int i=0;i<8;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCC;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<8;i++) LSE_c_cc[i] = 0;
      }
    }
    struct termio get() {
      struct termio buf;
      buf.c_iflag = LSE_b2h(LSE_c_iflag);
      buf.c_oflag = LSE_b2h(LSE_c_oflag);
      buf.c_cflag = LSE_b2h(LSE_c_cflag);
      buf.c_lflag = LSE_b2h(LSE_c_lflag);
      buf.c_line  = LSE_c_line;
      if (NCC <= 8) {
	for (int i=0;i<NCC;i++) buf.c_cc[i] = LSE_c_cc[i];
      } else {
	int i;
	for (i=0;i<8;i++) buf.c_cc[i] = LSE_c_cc[i];
	for (;i<NCC;i++) buf.c_cc[i] = 0;
      }
      return buf;
    }
  };

  struct L_compat_termios {
    uint32_t LSE_c_iflag;
    uint32_t LSE_c_oflag;
    uint32_t LSE_c_cflag;
    uint32_t LSE_c_lflag;
    uint8_t  LSE_c_cc[32];
    uint8_t  LSE_c_line;
    uint32_t LSE_c_ispeed;
    uint32_t LSE_c_ospeed;
#ifdef TCGETS
    L_compat_termios& operator=(const struct termios &buf) {
      LSE_c_iflag = LSE_h2b((uint32_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((uint32_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((uint32_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((uint32_t)buf.c_lflag);
      if (32 <= NCCS) {
	for (int i=0;i<32;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<32;i++) LSE_c_cc[i] = 0;
      }
#ifdef __linux
      LSE_c_line = buf.c_line;
      LSE_c_ispeed = LSE_h2b((uint32_t)buf.c_ispeed);
      LSE_c_ospeed = LSE_h2b((uint32_t)buf.c_ospeed);
#else
      LSE_c_line = 0; // N_TTY
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
#endif
    }
#elif defined(TCGETA)
    L_compat_termios& operator=(const struct termio &buf) {
      LSE_c_iflag = LSE_h2b((uint32_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((uint32_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((uint32_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((uint32_t)buf.c_lflag);
      if (32 <= NCCS) {
	for (int i=0;i<32;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<32;i++) LSE_c_cc[i] = 0;
      }
#ifdef __linux
      LSE_c_line = buf.c_line;
#else
      LSE_c_line = 0; // N_TTY
#endif
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
    }
#endif
  };

  struct L_siginfo {
    int si_signo;
    int si_errno;
    int si_code;
    uint32_t __pad;
    union {
      int pad[28];
      struct {
	int pid;
	int uid;
	L_sigval_t sigval;
      } normal;
      struct {
	int pid;
	int uid;
	int status;
	L_clock_t utime;
	L_clock_t stime;
      } child;
      struct {
	int tid;
	int overrun;
	L_sigval_t sigval;
	int _private;
      } timer;
      struct {
	uint64_t band;
	int fd;
      } poll;
      struct {
	LSE_emu_addr_t addr;
	int imm;
      } fault;
    } d;
    
    inline struct Linux_siginfo_t tosiginfo();
    inline void fromsiginfo(struct Linux_siginfo_t &v);

    L_siginfo(struct Linux_siginfo_t &v) { fromsiginfo(v); }
    L_siginfo() {}
  };

  struct L_compat_siginfo {
    int si_signo;
    int si_errno;
    int si_code;
    union {
      int pad[29];
      struct {
	int pid;
	int uid;
	L_compat_sigval_t sigval;
      } normal;
      struct {
	int pid;
	int uid;
	int status;
	L_compat_clock_t utime;
	L_compat_clock_t stime;
      } child;
      struct {
	int tid;
	int overrun;
	L_compat_sigval_t sigval;
	int _private;
      } timer;
      struct {
	int band;
	int fd;
      } poll;
      struct {
	uint32_t addr;
      } fault;
    } d;
    
    inline struct Linux_siginfo_t tosiginfo();
    inline void fromsiginfo(struct Linux_siginfo_t &v);
    L_compat_siginfo(struct Linux_siginfo_t &v) { fromsiginfo(v); }
    L_compat_siginfo() {}
  };

};

#define LSE_LINUX_CODE
#define LSE_LINUX_USE_DEVMEM
#define CC_ict_osinfo(x)     (x)->osinfo
#define CC_ict_hwcno(x)      (x)->globalcno
#define CC_ict_startaddr(x)  (x)->startaddr
#define CC_ict_mem(x)        (x)->mem
#define CC_elftype            0
#define CC_emu_interface(x)  (*((x)->di->einterface))
#define OS_emuvar(x,y)       (((PPC_dinst_t *)((x)->etoken))->y)
#define OS_ict_emuvar(x,y)   ((x)->di->y)
#define OS_interface(x)      ((Linux_dinst_t *)OS_emuvar(x,ostoken))
#define OS_interface_set(x,y) OS_emuvar(x,ostoken) = (void *)(y)
#define CC_addrsize           (is64bit ? 8 : 4)
#define CC_get_stackptr(x)    ((x)->r[1])

#define CC_context_has_no_users(x) (!((CC_isacontext_t *)((x)->isacont)->users))

static inline void CC_sys_set_return(CC_isacontext_t *realct, int which,
				     uint64_t val) {
  if (which == 0)
    realct->cr = (val==-1) ? realct->cr | (1<<28) 
      : realct->cr & ~(uint64_t(1)<<28);
  else 
    realct->r[3]=val;
}

static inline uint64_t CC_sys_get_return(CC_isacontext_t *realct, int which) {
  // TODO: needs to handle which=0 case
  return realct->r[3];
}

#define CC_open_filter(fname) \
  (!strcmp(fname,"/usr/share/locale/locale.alias") || \
   !strcmp(fname,"/usr/lib/locale/locale-archive") || \
   !strncmp(fname,"/proc/",6))

static int CC_elfarchchecker(int foundflag, void *ct) {
  return foundflag == 20 || foundflag == 21;
}

#define CC_contextdecl_hook()  uint64_t orig_r3; int trapno;
#define CC_contextinit_hook()  trapno = 0;

#define CC_execveguts() \
  PPC_execveguts(realct, start_stack, osct, is64bit, ict.loadbias, argc, \
		 vargv, venvp)

static inline void PPC_execveguts(CC_isacontext_t *realct,
				  LSE_emu_addr_t start_stack,
				  struct Linux_context_t *osct,
				  bool is64bit, LSE_emu_addr_t loadbias,
				  int argc, LSE_emu_addr_t vargv,
				  LSE_emu_addr_t venvp);

#define CC_context_copy(i,ppc,pc,cm) EMU_context_copy(i,ppc,pc)
#define CC_context_destroy(i,cp)  EMU_context_destroy(i,cp)

#define CC_cloneguts() \
  PPC_cloneguts(stackstart,stacksize,newisact,tlsp,flags,newosct->is64bit)

static inline void PPC_cloneguts(LSE_emu_addr_t stackstart,
				 LSE_emu_addr_t stacksize,
				 CC_isacontext_t *newisact,
				 LSE_emu_addr_t tlsp, int flags,
				 bool is64bit) {
  if (stackstart + stacksize != 0) newisact->r[1] = stackstart + stacksize;
  CC_sys_set_return(newisact, 0, 0);
  CC_sys_set_return(newisact, 1, 0);
  if (flags & OSDefs::oCLONE_SETTLS) {
    newisact->r[is64bit ? 13 : 2] = tlsp;
  }
}

#define CC_sigreturn(n) PPC_sigreturn(n,realct,osct,sigmask)

static inline void PPC_sigreturn(int callno, CC_isacontext_t *realct,
				 struct Linux_context_t *osct, 
				 uint64_t &sigmask);

void PPC_drop_mapping(PPC_dinst_t *, LSE_emu_addr_t va, LSE_emu_addr_t pa);
bool CC_obtain_write_permission(CC_isacontext_t *realct, 
				LSE_emu_addr_t va, LSE_emu_addr_t pa);

#define CC_invalidate_tlb(ctx) \
  do { (ctx)->softmmu.clear(MMUhandler(*(ctx))); } while (0)

#define CC_drop_mapping(os, va, pa)				\
  do { PPC_drop_mapping((PPC_dinst_t *)(os->eint->etoken), va, pa); } while (0)

#define CC_chkpt_cntl_clear(c) ((c).recordOS = (c).incrementalMem = false)
#define CC_ict_write_chkpt(c,ci) EMU_write_context(cptFile,nmark,c,ci)
#define CC_ict_parse_chkpt(c)                            \
  if ((cerr = EMU_parse_context(intr, cptFile, c, ctl))) \
    return cerr;

#define CC_arg_ptr(n,s) (realct->r[n+3])
#define CC_arg_int(n,s) (realct->r[n+3])


#define CC_time_get_currtime(x) do_get_currtime(x)
#define CC_time_schedule_wakeup(t,f,di) do_schedule_wakeup(t,f,di)
#define CC_time_request_interrupt(os,n)	do_request_interrupt(os,n)

  static inline uint64_t do_get_currtime(struct Linux_dinst_t *os);
  static void do_schedule_wakeup(uint64_t attime,
				 void (*f)(void *),
				 struct Linux_dinst_t *os);
  static inline int do_request_interrupt(class Linux_dinst_t *os, int hwcno);

#include "OS/LSE_Linux.h"

  static inline uint64_t do_get_currtime(Linux_dinst_t *os) {
    LSE_clock_t clock = ((PPC_dinst_t *)(os->eint->etoken))->clock;
    if (!clock) return 0;
    if (clock) {
      // should be starttime + clock * scale factor
      return clock->get_cycle();
    }
  }

  static void do_schedule_wakeup(uint64_t attime,
				 void (*f)(void *),
				 Linux_dinst_t *os) {
    LSE_clock_t clock = ((PPC_dinst_t *)(os->eint->etoken))->clock;
    if (clock) {
      // convert from ns to clock cycles
      // (attime - starttime) * scale factor for clock = clock cycles;
      uint64_t clockcycles = attime; // for the moment, 1 GHz clock always...
      clock->add_todo(clockcycles, f, os);
    }
  }

  static inline int do_request_interrupt(Linux_dinst_t *os, int hwcno) {
    PPC_dinst_t *di = ((PPC_dinst_t *)(os->eint->etoken));
    LSE_emu_interrupt_callback_t &ic = di->interrupts[hwcno];
    if (ic.handler)
      return (*ic.handler)(hwcno, ic.data);
    else return false;
  }

static inline void PPC_execveguts(CC_isacontext_t *realct,
				  LSE_emu_addr_t start_stack,
				  struct Linux_context_t *osct,
				  bool is64bit, LSE_emu_addr_t loadbias,
				  int argc, LSE_emu_addr_t vargv,
				  LSE_emu_addr_t venvp) {

  realct->msr = is64bit ? uint64_t(1) << 63 : 0;

  /* Linux: set address limit to end of user; not yet used */

  for (int i=0;i<32;i++)
  {
    realct->r[i] = 0;
    realct->f[i] = 0;
  }
  realct->r[1]=start_stack+16;
 
  if (osct->is64bit) {
    LSE_emu_addr_t toc;
    int retval = OS_mem_read(realct,
			     realct->startaddr+8,8,
			     (unsigned char *)&toc);
    if (retval)
    {
      fprintf(stderr,"Could not read initial TOC at 0x%016"PRIx64"!\n",
	  realct->startaddr+8);
      exit(1);
    }

    realct->r[2]= LSE_b2h(toc) + loadbias;
    realct->r[3] = argc;
    realct->r[4] = vargv;
    realct->r[5] = venvp;
    realct->startaddr = translate_elf_entry(realct, realct->startaddr, 
					    loadbias);
  }

  realct->cr = 0;
  realct->fpscr = 0;
  realct->lr = 0;
  realct->xer = 0;
  realct->ctr = 0;
}

struct PPC_rtsignalframe {
  struct {
    uint64_t flags;
    uint64_t link;
    OSDefs::L_sigaltstack stack;
    uint64_t sigmask;
    uint64_t __pad[15];
    struct {
      uint64_t __pad[4];
      int signal;
      int __pad2;
      uint64_t handler;
      uint64_t oldmask;
      uint64_t regs;
      uint64_t gp_regs[48];
      uint64_t fp_regs[33];
      uint64_t v_regs;
      uint64_t reserved[2*34+33];
    } mcontext;
  } uc;
  uint64_t __pad[2];
  uint32_t tramp[6];
  uint64_t pinfo, puc;
  OSDefs::L_siginfo info;
  char gap[288];
};

struct PPC_mcontext32 {
  uint32_t gp_regs[48];
  uint64_t fp_regs[33];
  uint32_t tramp[2];
  uint64_t v_regs[33*2]; // crazy vector registers don't make much sense.
} mctx;

struct PPC_ucontext32 {
  uint32_t flags;
  uint32_t link;
  OSDefs::L_compat_sigaltstack stack;
  int      pad[7];
  uint32_t puc;
  OSDefs::L_compat_sigset sigmask;
  int      sigextra[30];
  int      pad2[3];
  PPC_mcontext32 mcontext;
  void setcontext(LSE_emu_isacontext_t *realct, bool keepr2);
  void savecontext(LSE_emu_isacontext_t *realct);
};

struct PPC_rtsignalframe32 {
  OSDefs::L_compat_siginfo info;
  struct {
    uint32_t flags;
    uint32_t link;
    OSDefs::L_compat_sigaltstack stack;
    uint32_t __pad[7];
    uint32_t puc;
    OSDefs::L_compat_sigset sigmask;
    int32_t  __pad2[30];
    int32_t  __pad3[3];

    struct {
      uint32_t gp_regs[48];
      uint32_t fp_regs[33];
      uint32_t tramp[3];
      uint64_t v_regs[33*2+32*2]; // crazy vector registers
    } mcontext;
  } uc;
  int32_t gap[56];
};

struct PPC_signalframe32 {
  struct {
    uint32_t __pad[4]; // unused[3] will actually hold part of the signal
    int32_t signal;
    uint32_t handler;
    uint32_t sigmask;
    uint32_t regs;
  } sctx;
  PPC_mcontext32 mctx;
  int32_t gap[56];
};

  inline struct Linux_siginfo_t OSDefs::L_siginfo::tosiginfo() { 
    struct Linux_siginfo_t nv; 
    nv.LSEsi_signo = LSE_b2h(si_signo);
    nv.LSEsi_errno = LSE_b2h(si_errno);
    nv.LSEsi_code = LSE_b2h(si_code);
    switch (nv.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      nv.LSEsi_pid = LSE_b2h(d.normal.pid);
      nv.LSEsi_uid = LSE_b2h(d.normal.uid);
      break;
    case OSDefs::oCLD_EXITED:
      nv.LSEsi_pid = LSE_b2h(d.child.pid);
      nv.LSEsi_uid = LSE_b2h(d.child.uid);
      nv.LSEsi_status = LSE_b2h(d.child.status);
      nv.LSEsi_utime = LSE_b2h(d.child.utime);
      nv.LSEsi_stime = LSE_b2h(d.child.stime);
      break;
    default: 
      memcpy(nv.rawbytes, d.pad, 28*4); // just keep the bytes
      break;
    }
    return nv;
  }

  inline void OSDefs::L_siginfo::fromsiginfo(struct Linux_siginfo_t &v) {
    si_signo = LSE_h2b((int)v.LSEsi_signo);
    si_errno = LSE_h2b((int)v.LSEsi_errno);
    si_code = LSE_h2b((int)v.LSEsi_code);
    switch (v.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      d.normal.pid = LSE_h2b((int)v.LSEsi_pid);
      d.normal.uid = LSE_h2b((int)v.LSEsi_uid);
      break;
    case OSDefs::oCLD_EXITED:
      d.child.pid = LSE_h2b((int)v.LSEsi_pid);
      d.child.uid = LSE_h2b((int)v.LSEsi_uid);
      d.child.status = LSE_h2b((int)v.LSEsi_status);
      d.child.utime = LSE_h2b((typeof d.child.utime)v.LSEsi_utime);
      d.child.stime = LSE_h2b((typeof d.child.stime)v.LSEsi_stime);
      break;
    default:
      memcpy(d.pad, v.rawbytes, 28*4); // just keep the bytes
      break;
    }
  }

  inline struct Linux_siginfo_t OSDefs::L_compat_siginfo::tosiginfo() {
    Linux_siginfo_t nv;
    nv.LSEsi_signo = LSE_b2h(si_signo);
    nv.LSEsi_errno = LSE_b2h(si_errno);
    nv.LSEsi_code = LSE_b2h(si_code);
    switch (nv.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      nv.LSEsi_pid = LSE_b2h(d.normal.pid);
      nv.LSEsi_uid = LSE_b2h(d.normal.uid);
      break;
    case OSDefs::oCLD_EXITED:
      nv.LSEsi_pid = LSE_b2h(d.child.pid);
      nv.LSEsi_uid = LSE_b2h(d.child.uid);
      nv.LSEsi_status = LSE_b2h(d.child.status);
      nv.LSEsi_utime = LSE_b2h(d.child.utime);
      nv.LSEsi_stime = LSE_b2h(d.child.stime);
      break;
    default: 
      memcpy(nv.rawbytes, d.pad, 29*4); // just keep the bytes
      break;
    }
    return nv;
  }

  inline void OSDefs::L_compat_siginfo::fromsiginfo(struct Linux_siginfo_t &v) {
    si_signo = LSE_h2b((int)v.LSEsi_signo);
    si_errno = LSE_h2b((int)v.LSEsi_errno);
    si_code = LSE_h2b((int)v.LSEsi_code);
    switch (v.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      d.normal.pid = LSE_h2b((int)v.LSEsi_pid);
      d.normal.uid = LSE_h2b((int)v.LSEsi_uid);
      break;
    case OSDefs::oCLD_EXITED:
      d.child.pid = LSE_h2b((int)v.LSEsi_pid);
      d.child.uid = LSE_h2b((int)v.LSEsi_uid);
      d.child.status = LSE_h2b((int)v.LSEsi_status);
      d.child.utime = LSE_h2b((typeof d.child.utime)v.LSEsi_utime);
      d.child.stime = LSE_h2b((typeof d.child.stime)v.LSEsi_stime);
      break;
    default:
      memcpy(d.pad, v.rawbytes, 29*4); // just keep the bytes
      break;
    }
  }

  void PPC_ucontext32::setcontext(LSE_emu_isacontext_t *realct,
				  bool keepr2 = true) {
    Linux_context_t *osct = OS_ict_osinfo(realct);
    uint32_t r2val;

    // Note: assumes that the regs pointer points inside the mcontext
    // and so we can just read the mcontext stuff.
    osct->sigblocked = sigmask.toword();
    osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
                           (osct->tgroup->sigpending & ~osct->sigblocked);
    
    if (keepr2) r2val = realct->r[2];
    for (int i = 0; i < 32; ++i) 
      realct->r[i] = LSE_b2h(mcontext.gp_regs[i]);
    if (keepr2) realct->r[2] = r2val;

    realct->startaddr = LSE_b2h(mcontext.gp_regs[32]);
    realct->msr = LSE_b2h(mcontext.gp_regs[33]);
    osct->orig_r3 = LSE_b2h(mcontext.gp_regs[34]);
    realct->ctr = LSE_b2h(mcontext.gp_regs[35]);
    realct->lr = LSE_b2h(mcontext.gp_regs[36]);
    realct->xer = LSE_b2h(mcontext.gp_regs[37]);
    realct->cr = LSE_b2h(mcontext.gp_regs[38]);

    for (int i = 0; i < 32; ++i) 
      realct->f[i] = LSE_b2h(mcontext.fp_regs[i]);
    realct->fpscr = LSE_b2h(mcontext.fp_regs[32]);
    realct->reservationList.drop_context(realct);
  }

  void PPC_ucontext32::savecontext(LSE_emu_isacontext_t *realct) {
    Linux_context_t *osct = OS_ict_osinfo(realct);
    flags = 0;
    link = 0;

    sigmask.fromword(osct->sigblocked);

    for (int i = 0; i < 32; ++i) 
      mcontext.fp_regs[i] = LSE_h2b(realct->f[i]);
    mcontext.fp_regs[32] = LSE_h2b(realct->fpscr);
	
    for (int i = 0; i < 32; ++i) 
      mcontext.gp_regs[i] = LSE_h2b(uint32_t(realct->r[i]));
    mcontext.gp_regs[32] = LSE_h2b(uint32_t(realct->startaddr));
    mcontext.gp_regs[33] = LSE_h2b(uint32_t(realct->msr));
    mcontext.gp_regs[34] = osct->orig_r3;
    mcontext.gp_regs[35] = LSE_h2b(uint32_t(realct->ctr));
    mcontext.gp_regs[36] = LSE_h2b(uint32_t(realct->lr));
    mcontext.gp_regs[37] = LSE_h2b(uint32_t(realct->xer));
    mcontext.gp_regs[38] = LSE_h2b(uint32_t(realct->cr));
    mcontext.gp_regs[39] = 0; // softte
    mcontext.gp_regs[40] = LSE_h2b((uint32_t)(osct->trapno));
    // rdar, dsisr
    mcontext.gp_regs[41] = 0;
    mcontext.gp_regs[42] = 0;
    mcontext.gp_regs[43] = osct->myerrno();    
  }

static inline void PPC_sigreturn(int callno, CC_isacontext_t *realct,
				 struct Linux_context_t *osct, 
				 uint64_t &sigmask) {

  LSE_emu_addr_t sp, slen;
  sp = realct->r[1];

#define frameoffset(Y) ((char *)&sf.Y - (char *)&sf)

  // This is really, really evil, but I do not have a better way to handle
  // it right now!
  if (callno == 172 /*rt_sigreturn*/) {
    PPC_rtsignalframe sf;

    // copy from target space
    OS_mem_read(realct, sp, sizeof(PPC_rtsignalframe), (CC_mem_data_t*)&sf);

    //fprintf(stderr,"return from signal %d ", LSE_b2h(sf.info.si_signo));

    sigmask = LSE_b2h(sf.uc.sigmask);

    for (int i = 0; i < 32; ++i) 
      realct->r[i] = LSE_b2h(sf.uc.mcontext.gp_regs[i]);

    realct->startaddr = LSE_b2h(sf.uc.mcontext.gp_regs[32]);
    realct->msr = LSE_b2h(sf.uc.mcontext.gp_regs[33]);
    osct->orig_r3 = LSE_b2h(sf.uc.mcontext.gp_regs[34]);
    realct->ctr = LSE_b2h(sf.uc.mcontext.gp_regs[35]);
    realct->lr = LSE_b2h(sf.uc.mcontext.gp_regs[36]);
    realct->xer = LSE_b2h(sf.uc.mcontext.gp_regs[37]);
    realct->cr = LSE_b2h(sf.uc.mcontext.gp_regs[38]);
    osct->trapno = LSE_b2h(sf.uc.mcontext.gp_regs[40]);
    // softte, rdar, dsisr, result

    for (int i = 0; i < 32; ++i) 
      realct->f[i] = LSE_b2h(sf.uc.mcontext.fp_regs[i]);
    realct->fpscr = LSE_b2h(sf.uc.mcontext.fp_regs[32]);

    realct->reservationList.drop_context(realct);
    handle_sigaltstack(realct, osct, sp+frameoffset(uc.stack), 
		       0, CC_get_stackptr(realct));
  } else if (callno == 1172 /* compat_rt_sigreturn */) {
    PPC_rtsignalframe32 sf;

    sp += 64 + 16;

    // copy from target space
    OS_mem_read(realct, sp, sizeof(PPC_rtsignalframe32), (CC_mem_data_t*)&sf);

    //fprintf(stderr,"return from signal %d ",LSE_b2h(sf.info.si_signo));

    sigmask = sf.uc.sigmask.toword();

    for (int i = 0; i < 32; ++i) 
      realct->r[i] = LSE_b2h(sf.uc.mcontext.gp_regs[i]);

    realct->startaddr = LSE_b2h(sf.uc.mcontext.gp_regs[32]);
    realct->msr = LSE_b2h(sf.uc.mcontext.gp_regs[33]);
    osct->orig_r3 = LSE_b2h(sf.uc.mcontext.gp_regs[34]);
    realct->ctr = LSE_b2h(sf.uc.mcontext.gp_regs[35]);
    realct->lr = LSE_b2h(sf.uc.mcontext.gp_regs[36]);
    realct->xer = LSE_b2h(sf.uc.mcontext.gp_regs[37]);
    realct->cr = LSE_b2h(sf.uc.mcontext.gp_regs[38]);

    for (int i = 0; i < 32; ++i) 
      realct->f[i] = LSE_b2h(sf.uc.mcontext.fp_regs[i]);
    realct->fpscr = LSE_b2h(sf.uc.mcontext.fp_regs[32]);

    realct->reservationList.drop_context(realct);

    handle_compat_sigaltstack(realct, osct, sp+frameoffset(uc.stack), 
			      0, CC_get_stackptr(realct));
  } else if (callno == 1119 /* compat_sigreturn */) {
    PPC_signalframe32 sf;

    sp += 64;

    // copy from target space
    OS_mem_read(realct, sp, sizeof(PPC_signalframe32), (CC_mem_data_t*)&sf);

    //fprintf(stderr,"return from signal %d ",
    //   LSE_b2h(sf.info.si_signo));

    sigmask = (((uint64_t)LSE_b2h(sf.sctx.__pad[3]))<<32) 
      + LSE_b2h(sf.sctx.sigmask);

    for (int i = 0; i < 32; ++i) 
      realct->r[i] = LSE_b2h(sf.mctx.gp_regs[i]);

    realct->startaddr = LSE_b2h(sf.mctx.gp_regs[32]);
    realct->msr = LSE_b2h(sf.mctx.gp_regs[33]);
    osct->orig_r3 = LSE_b2h(sf.mctx.gp_regs[34]);
    realct->ctr = LSE_b2h(sf.mctx.gp_regs[35]);
    realct->lr = LSE_b2h(sf.mctx.gp_regs[36]);
    realct->xer = LSE_b2h(sf.mctx.gp_regs[37]);
    realct->cr = LSE_b2h(sf.mctx.gp_regs[38]);

    for (int i = 0; i < 32; ++i) 
      realct->f[i] = LSE_b2h(sf.mctx.fp_regs[i]);
    realct->fpscr = LSE_b2h(sf.mctx.fp_regs[32]);

    realct->reservationList.drop_context(realct);
  }
#undef frameoffset
}
  
static LSE_LINUX_SIG(rt_sigaction32) {
  return rt_sigaction_body<OSDefs::L_sigaction32>(LSE_LINUX_ARGS,
						  "rt_sigaction32");
}

static int PPC_ftruncate64(LSE_LINUX_PARMS) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;  // 1 = do not handle signals because we blocked.
  int fd;
  int64_t offset;
  uint64_t offset1, offset2;
  int ret=0;

  fd     = CC_arg_int(0,"iiii");
  // 1 = 0
  offset1 = CC_arg_int(2,"iiii"); // high order bits
  offset2 = CC_arg_int(3, "iiii"); // low order bits

  offset = int64_t((offset1 << 32) | offset2);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): ftruncate64(%d,%lld) = ",
	    osct->tid, fd, offset);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_int(offset);

  ret = osct->fdtable->getGoodFD(fd)->ftruncate(offset);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
}

static inline LSE_emu_addr_t
translate_elf_entry(PPC_context_t *context, LSE_emu_addr_t addr,
		    LSE_emu_addr_t loadbias)
{
  Linux_context_t *osct = OS_ict_osinfo(context);
  if (!osct->is64bit || (addr == 0))
    return addr;
  else
    {
      /* 64-bit opd horking */
      uint64_t realaddr;
      int retval = OS_mem_read(context,context->startaddr,
			       8,(unsigned char *)&realaddr);
      if (retval)
	{
	  fprintf(stderr,"Could not read entry point 0x%016"PRIx64" in .opd table!\n", context->startaddr);
	  exit(1);
	}
      //std::cerr << std::hex << context->startaddr << " " << LSE_b2h(realaddr) << " " << loadbias << std::dec << "\n";
      return LSE_b2h(realaddr) + loadbias;
    }
}

// Invariant: the process is not blocked (but may be a zombie)
static bool CC_do_a_signal(PPC_context_t *realct, bool must_restart,
			   LSE_emu_iaddr_t pc, LSE_emu_iaddr_t &npc) {
  // This is, without a doubt, the most evil code in the whole OS emulation

  Linux_context_t *osct = OS_ict_osinfo(realct);

  if (osct->sigPendingFlag) {

    int actualerrno = realct->r[3];

    // pick a signal and find out all the information about it.
    Linux_siginfo_t sinfo;
    Linux_sighandinf_t shand;

    uint64_t maskafter = (osct->oldSigBlockedValid ? osct->oldsigblocked :
			  osct->sigblocked);

    osct->incr();
    pick_signal(osct, sinfo, shand);

    if (osct->state == ProcZombie || osct->state == ProcReaped) {
      osct->decr();
      return false;
    }
    osct->decr();

    // not inside a blocked system call at all
    if ((osct->trapno & ~0xF) != 0xc00) goto afterrestart;

    // no error signalled by restarting system call
    if (!(realct->cr & (1<<28))) goto afterrestart;

    if (actualerrno == OSDefs::oERESTART_RESTARTBLOCK ||
	actualerrno == OSDefs::oERESTARTNOHAND) 
      must_restart = sinfo.LSEsi_signo <= 0;
    else if (actualerrno == OSDefs::oERESTARTSYS)
      must_restart = ( (sinfo.LSEsi_signo <= 0) || 
		       (OSDefs::oSA_RESTART & shand.flags) );
    else if (actualerrno != OSDefs::oERESTARTNOINTR) goto afterrestart;
      
    if (must_restart) {
      if (actualerrno == OSDefs::oERESTART_RESTARTBLOCK) 
	realct->r[0] = 0; // TODO: __NR_restart_syscall
      else
	realct->r[3] = osct->orig_r3;
      npc -= 4;
      realct->startaddr = npc;
      osct->myerrno() = 0;
    } else { // syscall will get an EINTR...
      if (actualerrno == OSDefs::oERESTART_RESTARTBLOCK) {
	delete osct->restart; // so things get garbage collected earlier
	osct->restart = 0;
      }
      realct->r[3] = OSDefs::oEINTR;
      realct->cr |= (1<<28);
      osct->myerrno() = -OSDefs::oEINTR;
    }

  afterrestart:

    if (sinfo.LSEsi_signo > 0) { // something to deliver

      // ================================================================
      // Had to set this up *exactly* like the "official" way so that
      //  exception handlers do not freak out when unwinding from a 
      //  signal handler
      //
      //  old frame
      //  ========================= 
      //  signal handler caller frame:
      //  =========================
      //  and a dummy frame
      //  =========================
      //
      
      LSE_emu_addr_t sp;

      // save context information (sigcontext)

#define frameoffset(Y) ((char *)&sf.Y - (char *)&sf)

      if (osct->is64bit) {

	sp = realct->r[1] - 44*8+16;
	// use alternate stack?
	if ((shand.flags & OSDefs::oSA_ONSTACK) &&
	    osct->sigAlt.LSE_ss_size && !on_alt_stack(osct->sigAlt,sp))
	  sp = osct->sigAlt.LSE_ss_size + osct->sigAlt.LSE_ss_sp;

	PPC_rtsignalframe sf;

	sp -= sizeof(PPC_rtsignalframe);
	sp &= ~uint64_t(0xf);

	sf.pinfo = LSE_h2b(sp + frameoffset(info));
	sf.puc = LSE_h2b(sp + frameoffset(uc));
	sf.info = OSDefs::L_siginfo(sinfo); //TODO: check copy_siginfo_to_user
	sf.uc.flags = 0;
	sf.uc.link = 0;
	sf.uc.stack.LSE_ss_sp = LSE_h2b(osct->sigAlt.LSE_ss_sp);
	sf.uc.stack.LSE_ss_size 
	  = LSE_h2b((OSDefs::L_size_t)osct->sigAlt.LSE_ss_size);
	sf.uc.stack.LSE_ss_flags 
	  = LSE_h2b(calc_ss_flags(osct->sigAlt, realct->r[1]));
	sf.uc.sigmask = LSE_h2b(maskafter);
	sf.uc.mcontext.v_regs = 0;

	for (int i = 0; i < 32; ++i) 
	  sf.uc.mcontext.fp_regs[i] = LSE_h2b(realct->f[i]);
	sf.uc.mcontext.fp_regs[32] = LSE_h2b(realct->fpscr);
      
	sf.uc.mcontext.regs = LSE_h2b(sp + frameoffset(uc.mcontext.gp_regs));

	for (int i = 0; i < 32; ++i) 
	  sf.uc.mcontext.gp_regs[i] = LSE_h2b(realct->r[i]);
	sf.uc.mcontext.gp_regs[32] = LSE_h2b(npc);
	sf.uc.mcontext.gp_regs[33] = LSE_h2b(realct->msr);
	sf.uc.mcontext.gp_regs[34] = osct->orig_r3;
	sf.uc.mcontext.gp_regs[35] = LSE_h2b(realct->ctr);
	sf.uc.mcontext.gp_regs[36] = LSE_h2b(realct->lr);
	sf.uc.mcontext.gp_regs[37] = LSE_h2b(realct->xer);
	sf.uc.mcontext.gp_regs[38] = LSE_h2b((uint64_t)realct->cr);
	sf.uc.mcontext.gp_regs[39] = 0; // softte
	sf.uc.mcontext.gp_regs[40] = LSE_h2b((uint64_t)(osct->trapno));
	// rdar, dsisr
	sf.uc.mcontext.gp_regs[41] = 0;
	sf.uc.mcontext.gp_regs[42] = 0;
	sf.uc.mcontext.gp_regs[43] = 0;//osct->myerrno();

	sf.uc.mcontext.signal = LSE_h2b(sinfo.LSEsi_signo);
	sf.uc.mcontext.handler = LSE_h2b(shand.handler);

	sf.tramp[0] = LSE_h2b(0x38210000U | 128); // add r1,r1,128
	sf.tramp[1] = LSE_h2b(0x38000000U | 172); // li r0, NR_rt_sigreturn
	sf.tramp[2] = LSE_h2b(0x44000002U); // sc
	sf.tramp[3] = sf.tramp[4] = sf.tramp[5] = 0;

	// copy now to target space
	OS_mem_write(realct, sp, sizeof(PPC_rtsignalframe), 
		     (CC_mem_data_t*)&sf);
	realct->fpscr = 0;
	realct->lr = sp + frameoffset(tramp[0]);

	sp -= 128; // dummy stack frame 
	uint64_t nsp = LSE_h2b(sp);
	OS_mem_write(realct, sp, 8, (CC_mem_data_t*)&nsp);

	if (shand.flags & OSDefs::oSA_SIGINFO) {
	  realct->r[4] = sp + 128 + frameoffset(info);
	  realct->r[5] = sp + 128 + frameoffset(uc);
	  realct->r[6] = sp + 128;
	} else realct->r[4] = sp+128+frameoffset(uc.mcontext);

	realct->r[1] = sp;
	// TODO: clear endian mode if we start doing it
	realct->r[3] = sinfo.LSEsi_signo;

        LSE_emu_addr_t raddr;

        // now go to the handler.  We assume the syscall did not block!
        OS_mem_read(realct, shand.handler, 8, (CC_mem_data_t *)&raddr);
        npc = realct->startaddr = LSE_b2h(raddr);

	// and set up the TOC register
        OS_mem_read(realct, shand.handler+8, 8, (CC_mem_data_t *)&raddr);
	realct->r[2] = LSE_b2h(raddr);
	osct->myerrno() = 0;
	// NOTE: 64-bit should *not* clear trapno

      } // 64-bit context
      else if (shand.flags & OSDefs::oSA_SIGINFO) { // 32-bit RT context

	sp = uint32_t(realct->r[1]) - 44*4+16; // magic?

	// use alternate stack?
	if ((shand.flags & OSDefs::oSA_ONSTACK) &&
	    osct->sigAlt.LSE_ss_size && !on_alt_stack(osct->sigAlt,sp))
	  sp = osct->sigAlt.LSE_ss_size + osct->sigAlt.LSE_ss_sp;

	PPC_rtsignalframe32 sf;

	sp -= sizeof(PPC_rtsignalframe32);
	sp &= ~uint64_t(0xf);

	sf.info = OSDefs::L_compat_siginfo(sinfo);
	sf.uc.flags = 0;
	sf.uc.link = 0;
	sf.uc.stack.LSE_ss_sp = LSE_h2b(osct->sigAlt.LSE_ss_sp);
	sf.uc.stack.LSE_ss_size
	  = LSE_h2b((OSDefs::L_size_t)osct->sigAlt.LSE_ss_size);
	sf.uc.stack.LSE_ss_flags = LSE_h2b(calc_ss_flags(osct->sigAlt, 
							 realct->r[1]));
	sf.uc.sigmask.fromword(maskafter);
	sf.uc.puc = LSE_h2b(uint32_t(sp + frameoffset(uc.mcontext)));
	
	for (int i = 0; i < 32; ++i) 
	  sf.uc.mcontext.fp_regs[i] = LSE_h2b(realct->f[i]);
	sf.uc.mcontext.fp_regs[32] = LSE_h2b(realct->fpscr);
	
	for (int i = 0; i < 32; ++i) 
	  sf.uc.mcontext.gp_regs[i] = LSE_h2b(uint32_t(realct->r[i]));
	sf.uc.mcontext.gp_regs[32] = LSE_h2b(uint32_t(npc));
	sf.uc.mcontext.gp_regs[33] = LSE_h2b(uint32_t(realct->msr));
	sf.uc.mcontext.gp_regs[34] = osct->orig_r3;
	sf.uc.mcontext.gp_regs[35] = LSE_h2b(uint32_t(realct->ctr));
	sf.uc.mcontext.gp_regs[36] = LSE_h2b(uint32_t(realct->lr));
	sf.uc.mcontext.gp_regs[37] = LSE_h2b(uint32_t(realct->xer));
	sf.uc.mcontext.gp_regs[38] = LSE_h2b(uint32_t(realct->cr));
	sf.uc.mcontext.gp_regs[39] = 0; // softte
	sf.uc.mcontext.gp_regs[40] = LSE_h2b((uint32_t)(osct->trapno));
	// rdar, dsisr
	sf.uc.mcontext.gp_regs[41] = 0;
	sf.uc.mcontext.gp_regs[42] = 0;
	sf.uc.mcontext.gp_regs[43] = osct->myerrno();

	//sf.uc.mcontext.tramp[0] = LSE_h2b(0x38210000U | (64+16)); // add r1,r1,64+16
	sf.uc.mcontext.tramp[0] = LSE_h2b(0x38000000U | 172); // li r0, NR_compat_rt_sigreturn
	sf.uc.mcontext.tramp[1] = LSE_h2b(0x44000002U); // sc

	// copy now to target space
	OS_mem_write(realct, sp, sizeof(PPC_rtsignalframe32), 
		   (CC_mem_data_t*)&sf);

	realct->fpscr = 0;
	realct->lr = sp + frameoffset(uc.mcontext.tramp[0]);

	sp -=  64 + 16; // dummy stack frame 
	uint32_t nsp = LSE_h2b(uint32_t(sp));
	OS_mem_write(realct, sp, 4, (CC_mem_data_t*)&nsp);

	realct->r[4] = sp + 64 + 16 + frameoffset(info);
	realct->r[5] = sp + 64 + 16 + frameoffset(uc);
	realct->r[6] = sp + 64 + 16;

	realct->r[1] = sp;
	// TODO: clear endian mode if we start doing it
	realct->r[3] = sinfo.LSEsi_signo;
	osct->trapno = 0;
      	npc = realct->startaddr = shand.handler;

      } // 32-bit RT context
      else { // 32-bit normal context
	sp = uint32_t(realct->r[1]) - 44*4+16; // magic?
	// use alternate stack?
	if ((shand.flags & OSDefs::oSA_ONSTACK) &&
	    osct->sigAlt.LSE_ss_size && !on_alt_stack(osct->sigAlt,sp))
	  sp = osct->sigAlt.LSE_ss_size + osct->sigAlt.LSE_ss_sp;

	PPC_signalframe32 sf;

	sp -= sizeof(PPC_signalframe32);
	sp &= ~uint64_t(0xf);

	sf.sctx.handler = LSE_h2b(uint32_t(shand.handler));
	sf.sctx.sigmask = LSE_h2b(uint32_t(maskafter));
	sf.sctx.__pad[3] = LSE_h2b(uint32_t(maskafter>>32));
	sf.sctx.regs = LSE_h2b(uint32_t(sp + frameoffset(mctx)));
	sf.sctx.signal = LSE_h2b(sinfo.LSEsi_signo);

	for (int i = 0; i < 32; ++i) 
	  sf.mctx.fp_regs[i] = LSE_h2b(realct->f[i]);
	sf.mctx.fp_regs[32] = LSE_h2b(realct->fpscr);
	
	for (int i = 0; i < 32; ++i) 
	  sf.mctx.gp_regs[i] = LSE_h2b(uint32_t(realct->r[i]));
	sf.mctx.gp_regs[32] = LSE_h2b(uint32_t(npc));
	sf.mctx.gp_regs[33] = LSE_h2b(uint32_t(realct->msr));
	sf.mctx.gp_regs[34] = osct->orig_r3;
	sf.mctx.gp_regs[35] = LSE_h2b(uint32_t(realct->ctr));
	sf.mctx.gp_regs[36] = LSE_h2b(uint32_t(realct->lr));
	sf.mctx.gp_regs[37] = LSE_h2b(uint32_t(realct->xer));
	sf.mctx.gp_regs[38] = LSE_h2b(uint32_t(realct->cr));
	sf.mctx.gp_regs[39] = 0; // softte
	sf.mctx.gp_regs[40] = LSE_h2b((uint32_t)(osct->trapno));
	// rdar, dsisr
	sf.mctx.gp_regs[41] = 0;
	sf.mctx.gp_regs[42] = 0;
	sf.mctx.gp_regs[43] = osct->myerrno();

	sf.mctx.tramp[0] = LSE_h2b(0x38000000U | 119); // li r0, NR_compat_sigreturn
	sf.mctx.tramp[1] = LSE_h2b(0x44000002U); // sc

	// copy now to target space
	OS_mem_write(realct, sp, sizeof(PPC_signalframe32), 
		   (CC_mem_data_t*)&sf);

	realct->fpscr = 0;
	realct->lr = sp + frameoffset(mctx.tramp[0]);

	sp -=  64; // dummy stack frame 
	uint32_t nsp = LSE_h2b(uint32_t(sp));
	OS_mem_write(realct, sp, 4, (CC_mem_data_t*)&nsp);
	realct->r[4] = sp+64+frameoffset(mctx);

	realct->r[1] = sp;
	// TODO: clear endian mode if we start doing it
	realct->r[3] = sinfo.LSEsi_signo;
	npc = realct->startaddr = shand.handler;
	osct->trapno = 0;

      } // 32-bit context

#undef frameoffset

      // update pending state

      osct->sigblocked |= shand.sigmask;
      if (!(shand.flags & OSDefs::oSA_NODEFER)) {
        osct->sigblocked |= uint64_t(1) << (sinfo.LSEsi_signo-1);
        osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
  		               (osct->tgroup->sigpending & ~osct->sigblocked);
      }
      osct->oldSigBlockedValid = false;

#ifdef DEBUG_SIGNAL
      std::cerr << "Found signal " << sinfo.LSEsi_signo 
      << " handler " << std::hex << npc << std::dec
      << "\n";
#endif

      return true;
    } 

    // no handler for an unblocked signal, so we can go ahead and 
    // restore the mask

    if (osct->oldSigBlockedValid) {
      osct->sigblocked = osct->oldsigblocked;
      osct->oldSigBlockedValid = false;
      osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
	(osct->tgroup->sigpending & ~osct->sigblocked);
    }

  } // if sigpending

  return false;
}

static int PPC_swapcontext32(LSE_LINUX_PARMS) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;  // 1 = do not handle signals because we blocked.
  int fd;
  off_t offset;
  int ret=0;
  LSE_emu_addr_t oldctp, newctp, regsp;
  int csize, r6, r7, r8;

  oldctp    = CC_arg_ptr(0,"ppiiiip");
  newctp    = CC_arg_ptr(1,"ppiiiip");
  csize     = CC_arg_int(2,"ppiiiip");
  r6        = CC_arg_int(3,"ppiiiip");
  r7        = CC_arg_int(4,"ppiiiip");
  r8        = CC_arg_int(5,"ppiiiip");
  regsp     = CC_arg_ptr(6,"ppiiiip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): compat_swapcontext(0x" 
	    LSE_emu_addr_t_print_format ",0x" LSE_emu_addr_t_print_format 
	    ",%d,%d,%d,%d,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, oldctp, newctp, csize, r6, r7, r8, regsp);

  if (oldctp) {  // need to copy current context into oldctp;
    PPC_ucontext32 uc;
    memset(&uc, 0, sizeof(uc));
    uc.savecontext(realct);
    uc.puc = LSE_h2b(uint32_t(oldctp + offsetof(PPC_ucontext32, mcontext)));
    OS_memcpy_h2t(oldctp, &uc, sizeof(uc));
  }
  OS_report_results(0,ret); // must come before context restore
  if (newctp) { // need to load context from newctp. 
    PPC_ucontext32 uc;
    OS_memcpy_t2h(&uc, newctp, sizeof(uc));
    uc.setcontext(realct);
  }

  // This next line may be wrong... do we want to overwrite r3?

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
}
       		
int EMU_call_os(PPC_context_t *realct,
		LSE_emu_iaddr_t curr_pc,
		LSE_emu_iaddr_t &following_pc,
		bool is64bit) {

  uint64_t r0val;
  int osres=0;
  realct->startaddr = curr_pc; // keeps things sane for context switch
  realct->startaddr = following_pc;

  r0val = realct->r[0];
  // save return register in case we end up aborting the syscall in some way
  OS_ict_osinfo(realct)->orig_r3 = realct->r[3];
  OS_ict_osinfo(realct)->trapno = 0xc00;

  int callno = r0val + (is64bit ? 0 : 1000);

  switch (callno) {
     
    case 0 : 
       osres = Linux_s_restart_syscall(callno,realct,curr_pc,0);
       break;

    case 1 : 
       osres = Linux_s_exit(callno,realct,curr_pc,0);
       break;

    case 2 : 
       osres = Linux_s_fork(callno,realct,curr_pc,0);
       break;

    case 3 : 
       osres = Linux_s_read(callno,realct,curr_pc,0);
       break;

    case 4 : 
       osres = Linux_s_write(callno,realct,curr_pc,0);
       break;

    case 5 : 
       osres = Linux_s_open(callno,realct,curr_pc,0);
       break;

    case 6 : 
       osres = Linux_s_close(callno,realct,curr_pc,0);
       break;

    case 7 : 
       osres = Linux_s_waitpid(callno,realct,curr_pc,0);
       break;

    case 8 : 
       osres = Linux_s_creat(callno,realct,curr_pc,0);
       break;

    case 9 : 
       osres = Linux_s_link(callno,realct,curr_pc,0);
       break;

    case 10 : 
       osres = Linux_s_unlink(callno,realct,curr_pc,0);
       break;

    case 11 : 
       osres = Linux_s_execve(callno,realct,curr_pc,0);
       break;

    case 12 : 
       osres = Linux_s_chdir(callno,realct,curr_pc,0);
       break;

    case 13 : 
       osres = Linux_s_time(callno,realct,curr_pc,0);
       break;

    case 14 : 
       osres = Linux_s_mknod(callno,realct,curr_pc,0);
       break;

    case 15 : 
       osres = Linux_s_chmod(callno,realct,curr_pc,0);
       break;

    case 16 : 
       osres = Linux_s_lchown(callno,realct,curr_pc,0);
       break;

    case 19 : 
       osres = Linux_s_lseek(callno,realct,curr_pc,0);
       break;

    case 20 : 
       osres = Linux_s_getpid(callno,realct,curr_pc,0);
       break;

    case 21 : 
       osres = Linux_s_mount(callno,realct,curr_pc,0);
       break;

    case 23 : 
       osres = Linux_s_setuid(callno,realct,curr_pc,0);
       break;

    case 24 : 
       osres = Linux_s_getuid(callno,realct,curr_pc,0);
       break;

    case 25 : 
       osres = Linux_s_stime(callno,realct,curr_pc,0);
       break;

    case 26 : 
       osres = Linux_s_ptrace(callno,realct,curr_pc,0);
       break;

    case 27 : 
       osres = Linux_s_alarm(callno,realct,curr_pc,0);
       break;

    case 29 : 
       osres = Linux_s_pause(callno,realct,curr_pc,0);
       break;

    case 30 : 
       osres = Linux_s_utime(callno,realct,curr_pc,0);
       break;

    case 33 : 
      osres = Linux_s_access(callno,realct,curr_pc,0);
      break;

    case 34 : 
       osres = Linux_s_nice(callno,realct,curr_pc,0);
       break;

    case 36 : 
       osres = Linux_s_sync(callno,realct,curr_pc,0);
       break;

    case 37 : 
       osres = Linux_s_kill(callno,realct,curr_pc,0);
       break;

    case 38 : 
       osres = Linux_s_rename(callno,realct,curr_pc,0);
       break;

    case 39 : 
       osres = Linux_s_mkdir(callno,realct,curr_pc,0);
       break;

    case 40 : 
       osres = Linux_s_rmdir(callno,realct,curr_pc,0);
       break;

    case 41 : 
       osres = Linux_s_dup(callno,realct,curr_pc,0);
       break;

    case 42 : 
       osres = Linux_s_pipe(callno,realct,curr_pc,0);
       break;

    case 43 : 
       osres = Linux_s_times(callno,realct,curr_pc,0);
       break;

    case 45 : 
       osres = Linux_s_brk(callno,realct,curr_pc,0);
       break;

    case 46 : 
       osres = Linux_s_setgid(callno,realct,curr_pc,0);
       break;

    case 47 : 
       osres = Linux_s_getgid(callno,realct,curr_pc,0);
       break;

    case 48 : 
       osres = Linux_s_signal(callno,realct,curr_pc,0);
       break;

    case 49 : 
       osres = Linux_s_geteuid(callno,realct,curr_pc,0);
       break;

    case 50 : 
       osres = Linux_s_getegid(callno,realct,curr_pc,0);
       break;

    case 51 : 
       osres = Linux_s_acct(callno,realct,curr_pc,0);
       break;

    case 52 : 
       osres = Linux_s_umount(callno,realct,curr_pc,0);
       break;

    case 54 : 
       osres = Linux_s_ioctl(callno,realct,curr_pc,0);
       break;

    case 55 : 
       osres = Linux_s_fcntl(callno,realct,curr_pc,0);
       break;

    case 57 : 
       osres = Linux_s_setpgid(callno,realct,curr_pc,0);
       break;

    case 60 : 
       osres = Linux_s_umask(callno,realct,curr_pc,0);
       break;

    case 61 : 
       osres = Linux_s_chroot(callno,realct,curr_pc,0);
       break;

    case 62 : 
       osres = Linux_s_ustat(callno,realct,curr_pc,0);
       break;

    case 63 : 
       osres = Linux_s_dup2(callno,realct,curr_pc,0);
       break;

    case 64 : 
       osres = Linux_s_getppid(callno,realct,curr_pc,0);
       break;

    case 65 : 
       osres = Linux_s_getpgrp(callno,realct,curr_pc,0);
       break;

    case 66 : 
       osres = Linux_s_setsid(callno,realct,curr_pc,0);
       break;

    case 68 : 
       osres = Linux_s_sgetmask(callno,realct,curr_pc,0);
       break;

    case 69 : 
       osres = Linux_s_ssetmask(callno,realct,curr_pc,0);
       break;

    case 70 : 
       osres = Linux_s_setreuid(callno,realct,curr_pc,0);
       break;

    case 71 : 
       osres = Linux_s_setregid(callno,realct,curr_pc,0);
       break;

    case 74 : 
       osres = Linux_s_sethostname(callno,realct,curr_pc,0);
       break;

    case 75 : 
       osres = Linux_s_setrlimit(callno,realct,curr_pc,0);
       break;

    case 77 : 
       osres = Linux_s_getrusage(callno,realct,curr_pc,0);
       break;

    case 78 : 
       osres = Linux_s_gettimeofday(callno,realct,curr_pc,0);
       break;

    case 79 : 
       osres = Linux_s_settimeofday(callno,realct,curr_pc,0);
       break;

    case 80 : 
       osres = Linux_s_getgroups(callno,realct,curr_pc,0);
       break;

    case 81 : 
       osres = Linux_s_setgroups(callno,realct,curr_pc,0);
       break;

    case 83 : 
       osres = Linux_s_symlink(callno,realct,curr_pc,0);
       break;

    case 85 : 
       osres = Linux_s_readlink(callno,realct,curr_pc,0);
       break;

    case 86 : 
       osres = Linux_s_uselib(callno,realct,curr_pc,0);
       break;

    case 87 : 
       osres = Linux_s_swapon(callno,realct,curr_pc,0);
       break;

    case 88 : 
       osres = Linux_s_reboot(callno,realct,curr_pc,0);
       break;

    case 90 : 
       osres = Linux_s_mmap(callno,realct,curr_pc,0);
       break;

    case 91 : 
       osres = Linux_s_munmap(callno,realct,curr_pc,0);
       break;

    case 92 : 
       osres = Linux_s_truncate(callno,realct,curr_pc,0);
       break;

    case 93 : 
       osres = Linux_s_ftruncate(callno,realct,curr_pc,0);
       break;

    case 94 : 
       osres = Linux_s_fchmod(callno,realct,curr_pc,0);
       break;

    case 95 : 
       osres = Linux_s_fchown(callno,realct,curr_pc,0);
       break;

    case 96 : 
       osres = Linux_s_getpriority(callno,realct,curr_pc,0);
       break;

    case 97 : 
       osres = Linux_s_setpriority(callno,realct,curr_pc,0);
       break;

    case 99 : 
       osres = Linux_s_statfs(callno,realct,curr_pc,0);
       break;

    case 100 : 
       osres = Linux_s_fstatfs(callno,realct,curr_pc,0);
       break;

    case 102 : 
       osres = Linux_s_socketcall(callno,realct,curr_pc,0);
       break;

    case 103 : 
       osres = Linux_s_syslog(callno,realct,curr_pc,0);
       break;

    case 104 : 
       osres = Linux_s_setitimer(callno,realct,curr_pc,0);
       break;

    case 105 : 
       osres = Linux_s_getitimer(callno,realct,curr_pc,0);
       break;

    case 106 : 
       osres = Linux_s_newstat(callno,realct,curr_pc,0);
       break;

    case 107 : 
       osres = Linux_s_newlstat(callno,realct,curr_pc,0);
       break;
 
    case 108 : 
       osres = Linux_s_newfstat(callno,realct,curr_pc,0);
       break;

    case 111 : 
       osres = Linux_s_vhangup(callno,realct,curr_pc,0);
       break;

    case 114 : 
       osres = Linux_s_wait4(callno,realct,curr_pc,0);
       break;

    case 115 : 
       osres = Linux_s_swapoff(callno,realct,curr_pc,0);
       break;

    case 116 : 
       osres = Linux_s_sysinfo(callno,realct,curr_pc,0);
       break;

    case 117 : 
       osres = Linux_s_ipc(callno,realct,curr_pc,0);
       break;

    case 118 : 
       osres = Linux_s_fsync(callno,realct,curr_pc,0);
       break;

    case 120 : 
       osres = Linux_s_clone(callno,realct,curr_pc,0);
       break;

    case 121 : 
       osres = Linux_s_setdomainname(callno,realct,curr_pc,0);
       break;

    case 122 : 
       osres = Linux_s_newuname(callno,realct,curr_pc,0);
       break;

    case 124 : 
       osres = Linux_s_adjtimex(callno,realct,curr_pc,0);
       break;

    case 125 : 
       osres = Linux_s_mprotect(callno,realct,curr_pc,0);
       break;

    case 128 : 
       osres = Linux_s_init_module(callno,realct,curr_pc,0);
       break;

    case 129 : 
       osres = Linux_s_delete_module(callno,realct,curr_pc,0);
       break;

    case 131 : 
       osres = Linux_s_quotactl(callno,realct,curr_pc,0);
       break;

    case 132 : 
       osres = Linux_s_getpgid(callno,realct,curr_pc,0);
       break;

    case 133 : 
       osres = Linux_s_fchdir(callno,realct,curr_pc,0);
       break;

    case 134 : 
       osres = Linux_s_bdflush(callno,realct,curr_pc,0);
       break;

    case 135 : 
       osres = Linux_s_sysfs(callno,realct,curr_pc,0);
       break;

    case 136 : 
       osres = Linux_s_personality(callno,realct,curr_pc,0);
       break;

    case 138 : 
       osres = Linux_s_setfsuid(callno,realct,curr_pc,0);
       break;

    case 139 : 
       osres = Linux_s_setfsgid(callno,realct,curr_pc,0);
       break;

    case 140 : 
       osres = Linux_s_llseek(callno,realct,curr_pc,0);
       break;

    case 141 : 
       osres = Linux_s_getdents(callno,realct,curr_pc,0);
       break;

    case 142 : 
       osres = Linux_s_select(callno,realct,curr_pc,0);
       break;

    case 143 : 
       osres = Linux_s_flock(callno,realct,curr_pc,0);
       break;

    case 144 : 
       osres = Linux_s_msync(callno,realct,curr_pc,0);
       break;

    case 145 : 
       osres = Linux_s_readv(callno,realct,curr_pc,0);
       break;

    case 146 : 
       osres = Linux_s_writev(callno,realct,curr_pc,0);
       break;

    case 147 : 
       osres = Linux_s_getsid(callno,realct,curr_pc,0);
       break;

    case 148 : 
       osres = Linux_s_fdatasync(callno,realct,curr_pc,0);
       break;

    case 149 : 
       osres = Linux_s_sysctl(callno,realct,curr_pc,0);
       break;

    case 150 : 
       osres = Linux_s_mlock(callno,realct,curr_pc,0);
       break;

    case 151 : 
       osres = Linux_s_munlock(callno,realct,curr_pc,0);
       break;

    case 152 : 
       osres = Linux_s_mlockall(callno,realct,curr_pc,0);
       break;

    case 153 : 
       osres = Linux_s_munlockall(callno,realct,curr_pc,0);
       break;

    case 154 : 
       osres = Linux_s_sched_setparam(callno,realct,curr_pc,0);
       break;

    case 155 : 
       osres = Linux_s_sched_getparam(callno,realct,curr_pc,0);
       break;

    case 156 : 
       osres = Linux_s_sched_setscheduler(callno,realct,curr_pc,0);
       break;

    case 157 : 
       osres = Linux_s_sched_getscheduler(callno,realct,curr_pc,0);
       break;

    case 158 : 
       osres = Linux_s_sched_yield(callno,realct,curr_pc,0);
       break;

    case 159 : 
       osres = Linux_s_sched_get_priority_max(callno,realct,curr_pc,0);
       break;

    case 160 : 
       osres = Linux_s_sched_get_priority_min(callno,realct,curr_pc,0);
       break;

    case 161 : 
       osres = Linux_s_sched_rr_get_interval(callno,realct,curr_pc,0);
       break;

    case 162 : 
       osres = Linux_s_nanosleep(callno,realct,curr_pc,0);
       break;

    case 163 : 
       osres = Linux_s_mremap(callno,realct,curr_pc,0);
       break;

    case 164 : 
       osres = Linux_s_setresuid(callno,realct,curr_pc,0);
       break;

    case 165 : 
       osres = Linux_s_getresuid(callno,realct,curr_pc,0);
       break;

    case 167 : 
       osres = Linux_s_poll(callno,realct,curr_pc,0);
       break;

    case 168 : 
       osres = Linux_s_nfsservctl(callno,realct,curr_pc,0);
       break;

    case 169 : 
       osres = Linux_s_setresgid(callno,realct,curr_pc,0);
       break;

    case 170 : 
       osres = Linux_s_getresgid(callno,realct,curr_pc,0);
       break;

    case 171 : 
       osres = Linux_s_prctl(callno,realct,curr_pc,0);
       break;

    case 172 : 
       osres = Linux_s_rt_sigreturn(callno,realct,curr_pc,0);
       following_pc = realct->startaddr;
       break;

    case 173 : 
       osres = Linux_s_rt_sigaction(callno,realct,curr_pc,0);
       break;

    case 174 : 
       osres = Linux_s_rt_sigprocmask(callno,realct,curr_pc,0);
       break;

    case 175 : 
       osres = Linux_s_rt_sigpending(callno,realct,curr_pc,0);
       break;

    case 176 : 
       osres = Linux_s_rt_sigtimedwait(callno,realct,curr_pc,0);
       break;

    case 177 : 
       osres = Linux_s_rt_sigqueueinfo(callno,realct,curr_pc,0);
       break;

    case 178 : 
       osres = Linux_s_rt_sigsuspend(callno,realct,curr_pc,0);
       break;

    case 179 : 
       osres = Linux_s_pread(callno,realct,curr_pc,0);
       break;

    case 180 : 
       osres = Linux_s_pwrite(callno,realct,curr_pc,0);
       break;

    case 181 : 
       osres = Linux_s_chown(callno,realct,curr_pc,0);
       break;

    case 182 : 
       osres = Linux_s_getcwd(callno,realct,curr_pc,0);
       break;

    case 183 : 
       osres = Linux_s_capget(callno,realct,curr_pc,0);
       break;

    case 184 : 
       osres = Linux_s_capset(callno,realct,curr_pc,0);
       break;

    case 185 : 
       osres = Linux_s_sigaltstack(callno,realct,curr_pc,0);
       break;

    case 186 : 
       osres = Linux_s_sendfile(callno,realct,curr_pc,0);
       break;

    case 189 : 
       osres = Linux_s_vfork(callno,realct,curr_pc,0);
       break;

    case 190 : 
       osres = Linux_s_getrlimit(callno,realct,curr_pc,0);
       break;

    case 191 : 
       osres = Linux_s_readahead(callno,realct,curr_pc,0);
       break;

    case 202 : 
       osres = Linux_s_getdents64(callno,realct,curr_pc,0);
       break;

    case 203 : 
       osres = Linux_s_pivot_root(callno,realct,curr_pc,0);
       break;

    case 205 : 
       osres = Linux_s_madvise(callno,realct,curr_pc,0);
       break;

    case 206 : 
       osres = Linux_s_mincore(callno,realct,curr_pc,0);
       break;

    case 207 : 
       osres = Linux_s_gettid(callno,realct,curr_pc,0);
       break;

    case 208 : 
       osres = Linux_s_tkill(callno,realct,curr_pc,0);
       break;

    case 209 : 
       osres = Linux_s_setxattr(callno,realct,curr_pc,0);
       break;

    case 210 : 
       osres = Linux_s_lsetxattr(callno,realct,curr_pc,0);
       break;

    case 211 : 
       osres = Linux_s_fsetxattr(callno,realct,curr_pc,0);
       break;

    case 212 : 
       osres = Linux_s_getxattr(callno,realct,curr_pc,0);
       break;

    case 213 : 
       osres = Linux_s_lgetxattr(callno,realct,curr_pc,0);
       break;

    case 214 : 
       osres = Linux_s_fgetxattr(callno,realct,curr_pc,0);
       break;

    case 215 : 
       osres = Linux_s_listxattr(callno,realct,curr_pc,0);
       break;

    case 216 : 
       osres = Linux_s_llistxattr(callno,realct,curr_pc,0);
       break;

    case 217 : 
       osres = Linux_s_flistxattr(callno,realct,curr_pc,0);
       break;

    case 218 : 
       osres = Linux_s_removexattr(callno,realct,curr_pc,0);
       break;

    case 219 : 
       osres = Linux_s_lremovexattr(callno,realct,curr_pc,0);
       break;

    case 220 : 
       osres = Linux_s_fremovexattr(callno,realct,curr_pc,0);
       break;

    case 221 : 
       osres = Linux_s_futex(callno,realct,curr_pc,0);
       break;

    case 222 : 
       osres = Linux_s_sched_setaffinity(callno,realct,curr_pc,0);
       break;

    case 223 : 
       osres = Linux_s_sched_getaffinity(callno,realct,curr_pc,0);
       break;

    case 227 : 
       osres = Linux_s_io_setup(callno,realct,curr_pc,0);
       break;

    case 228 : 
       osres = Linux_s_io_destroy(callno,realct,curr_pc,0);
       break;

    case 229 : 
       osres = Linux_s_io_getevents(callno,realct,curr_pc,0);
       break;

    case 230 : 
       osres = Linux_s_io_submit(callno,realct,curr_pc,0);
       break;

    case 231 : 
       osres = Linux_s_io_cancel(callno,realct,curr_pc,0);
       break;

    case 232 : 
       osres = Linux_s_set_tid_address(callno,realct,curr_pc,0);
       break;

    case 233 : 
       osres = Linux_s_fadvise64(callno,realct,curr_pc,0);
       break;

    case 234 : 
       osres = Linux_s_exit_group(callno,realct,curr_pc,0);
       break;

    case 235 : 
       osres = Linux_s_lookup_dcookie(callno,realct,curr_pc,0);
       break;

    case 236 : 
       osres = Linux_s_epoll_create(callno,realct,curr_pc,0);
       break;

    case 237 : 
       osres = Linux_s_epoll_ctl(callno,realct,curr_pc,0);
       break;

    case 238 : 
       osres = Linux_s_epoll_wait(callno,realct,curr_pc,0);
       break;

    case 239 : 
       osres = Linux_s_remap_file_pages(callno,realct,curr_pc,0);
       break;

    case 240 : 
       osres = Linux_s_timer_create(callno,realct,curr_pc,0);
       break;

    case 241 : 
       osres = Linux_s_timer_settime(callno,realct,curr_pc,0);
       break;

    case 242 : 
       osres = Linux_s_timer_gettime(callno,realct,curr_pc,0);
       break;

    case 243 : 
       osres = Linux_s_timer_getoverrun(callno,realct,curr_pc,0);
       break;

    case 244 : 
       osres = Linux_s_timer_delete(callno,realct,curr_pc,0);
       break;

    case 245 : 
       osres = Linux_s_clock_settime(callno,realct,curr_pc,0);
       break;

    case 246 : 
       osres = Linux_s_clock_gettime(callno,realct,curr_pc,0);
       break;

    case 247 : 
       osres = Linux_s_clock_getres(callno,realct,curr_pc,0);
       break;

    case 248 : 
       osres = Linux_s_clock_nanosleep(callno,realct,curr_pc,0);
       break;

    case 249 : 
       osres = Linux_s_swapcontext(callno,realct,curr_pc,0);
       break;

    case 250 : 
       osres = Linux_s_tgkill(callno,realct,curr_pc,0);
       break;

    case 251 : 
       osres = Linux_s_utimes(callno,realct,curr_pc,0);
       break;

    case 252 : 
       osres = Linux_s_statfs64(callno,realct,curr_pc,0);
       break;

    case 253 : 
       osres = Linux_s_fstatfs64(callno,realct,curr_pc,0);
       break;

    case 255 : 
       osres = Linux_s_ppc_rtas(callno,realct,curr_pc,0);
       break;

    case 258 : 
       osres = Linux_s_migrate_pages(callno,realct,curr_pc,0);
       break;

    case 259 : 
       osres = Linux_s_mbind(callno,realct,curr_pc,0);
       break;

    case 260 : 
       osres = Linux_s_get_mempolicy(callno,realct,curr_pc,0);
       break;

    case 261 : 
       osres = Linux_s_set_mempolicy(callno,realct,curr_pc,0);
       break;

    case 262 : 
       osres = Linux_s_mq_open(callno,realct,curr_pc,0);
       break;

    case 263 : 
       osres = Linux_s_mq_unlink(callno,realct,curr_pc,0);
       break;

    case 264 : 
       osres = Linux_s_mq_timedsend(callno,realct,curr_pc,0);
       break;

    case 265 : 
       osres = Linux_s_mq_timedreceive(callno,realct,curr_pc,0);
       break;

    case 266 : 
       osres = Linux_s_mq_notify(callno,realct,curr_pc,0);
       break;

    case 267 : 
       osres = Linux_s_mq_getsetattr(callno,realct,curr_pc,0);
       break;

    case 268 : 
       osres = Linux_s_kexec_load(callno,realct,curr_pc,0);
       break;

    case 269 : 
       osres = Linux_s_add_key(callno,realct,curr_pc,0);
       break;

    case 270 : 
       osres = Linux_s_request_key(callno,realct,curr_pc,0);
       break;

    case 271 : 
       osres = Linux_s_keyctl(callno,realct,curr_pc,0);
       break;

    case 272 : 
       osres = Linux_s_waitid(callno,realct,curr_pc,0);
       break;

    case 273 : 
       osres = Linux_s_ioprio_set(callno,realct,curr_pc,0);
       break;

    case 274 : 
       osres = Linux_s_ioprio_get(callno,realct,curr_pc,0);
       break;

    case 275 : 
       osres = Linux_s_inotify_init(callno,realct,curr_pc,0);
       break;

    case 276 : 
       osres = Linux_s_inotify_add_watch(callno,realct,curr_pc,0);
       break;

    case 277 : 
       osres = Linux_s_inotify_rm_watch(callno,realct,curr_pc,0);
       break;

    case 278 : 
       osres = Linux_s_spu_run(callno,realct,curr_pc,0);
       break;

    case 279 : 
       osres = Linux_s_spu_create(callno,realct,curr_pc,0);
       break;

    case 280 : 
       osres = Linux_s_pselect6(callno,realct,curr_pc,0);
       break;

    case 281 : 
       osres = Linux_s_ppoll(callno,realct,curr_pc,0);
       break;

    case 282 : 
       osres = Linux_s_unshare(callno,realct,curr_pc,0);
       break;

    case 283 : 
       osres = Linux_s_splice(callno,realct,curr_pc,0);
       break;

    case 284 : 
       osres = Linux_s_tee(callno,realct,curr_pc,0);
       break;

    case 285 : 
       osres = Linux_s_vmsplice(callno,realct,curr_pc,0);
       break;

    case 286 : 
       osres = Linux_s_openat(callno,realct,curr_pc,0);
       break;

    case 287 : 
       osres = Linux_s_mkdirat(callno,realct,curr_pc,0);
       break;

    case 288 : 
       osres = Linux_s_mknodat(callno,realct,curr_pc,0);
       break;

    case 289 : 
       osres = Linux_s_fchownat(callno,realct,curr_pc,0);
       break;

    case 290 : 
       osres = Linux_s_futimesat(callno,realct,curr_pc,0);
       break;

    case 291 : 
       osres = Linux_s_newfstatat(callno,realct,curr_pc,0);
       break;

    case 292 : 
       osres = Linux_s_unlinkat(callno,realct,curr_pc,0);
       break;

    case 293 : 
       osres = Linux_s_renameat(callno,realct,curr_pc,0);
       break;

    case 294 : 
       osres = Linux_s_linkat(callno,realct,curr_pc,0);
       break;

    case 295 : 
       osres = Linux_s_symlinkat(callno,realct,curr_pc,0);
       break;

    case 296 : 
       osres = Linux_s_readlinkat(callno,realct,curr_pc,0);
       break;

    case 297 : 
       osres = Linux_s_fchmodat(callno,realct,curr_pc,0);
       break;

    case 298 : 
       osres = Linux_s_faccessat(callno,realct,curr_pc,0);
       break;

    case 299 : 
       osres = Linux_s_get_robust_list(callno,realct,curr_pc,0);
       break;

    case 300 : 
       osres = Linux_s_set_robust_list(callno,realct,curr_pc,0);
       break;

    case 301 : 
       osres = Linux_s_move_pages(callno,realct,curr_pc,0);
       break;

    case 302 : 
       osres = Linux_s_getcpu(callno,realct,curr_pc,0);
       break;

    case 303 : 
       osres = Linux_s_epoll_pwait(callno,realct,curr_pc,0);
       break;

    case 304 : 
       osres = Linux_s_utimensat(callno,realct,curr_pc,0);
       break;

    case 305 : 
       osres = Linux_s_signalfd(callno,realct,curr_pc,0);
       break;

    case 306 : 
       osres = Linux_s_timerfd_create(callno,realct,curr_pc,0);
       break;

    case 307 : 
       osres = Linux_s_eventfd(callno,realct,curr_pc,0);
       break;

    case 308 : 
       osres = Linux_s_sync_file_range2(callno,realct,curr_pc,0);
       break;

    case 309 : 
       osres = Linux_s_fallocate(callno,realct,curr_pc,0);
       break;

    case 310 : 
       osres = Linux_s_subpage_prot(callno,realct,curr_pc,0);
       break;

    case 311 : 
       osres = Linux_s_timerfd_settime(callno,realct,curr_pc,0);
       break;

    case 312 : 
       osres = Linux_s_timerfd_gettime(callno,realct,curr_pc,0);
       break;

    case 313 : 
       osres = Linux_s_signalfd4(callno,realct,curr_pc,0);
       break;

    case 314 : 
       osres = Linux_s_eventfd2(callno,realct,curr_pc,0);
       break;

    case 315 : 
       osres = Linux_s_epoll_create1(callno,realct,curr_pc,0);
       break;

    case 316 : 
       osres = Linux_s_dup3(callno,realct,curr_pc,0);
       break;

    case 317 : 
       osres = Linux_s_pipe2(callno,realct,curr_pc,0);
       break;

    case 318 : 
       osres = Linux_s_inotify_init1(callno,realct,curr_pc,0);
       break;

    case 319 : 
       osres = Linux_s_perf_counter_open(callno,realct,curr_pc,0);
       break;

    case 320 : 
       osres = Linux_s_preadv(callno,realct,curr_pc,0);
       break;

    case 321 : 
       osres = Linux_s_pwritev(callno,realct,curr_pc,0);
       break;

    case 322 : 
       osres = Linux_s_rt_tgsigqueueinfo(callno,realct,curr_pc,0);
       break;



    case 1000 : 
       osres = Linux_s_restart_syscall(callno,realct,curr_pc,0);
       break;

    case 1001 : 
       osres = Linux_s_exit(callno,realct,curr_pc,0);
       break;

    case 1002 : 
       osres = Linux_s_fork(callno,realct,curr_pc,0);
       break;

    case 1003 : 
       osres = Linux_s_read(callno,realct,curr_pc,0);
       break;

    case 1004 : 
       osres = Linux_s_write(callno,realct,curr_pc,0);
       break;

    case 1005 : 
       osres = Linux_s_open(callno,realct,curr_pc,2);
       break;

    case 1006 : 
       osres = Linux_s_close(callno,realct,curr_pc,0);
       break;

    case 1007 : 
       osres = Linux_s_waitpid(callno,realct,curr_pc,2);
       break;

    case 1008 : 
       osres = Linux_s_creat(callno,realct,curr_pc,2);
       break;

    case 1009 : 
       osres = Linux_s_link(callno,realct,curr_pc,0);
       break;

    case 1010 : 
       osres = Linux_s_unlink(callno,realct,curr_pc,0);
       break;

    case 1011 : 
       osres = Linux_s_execve(callno,realct,curr_pc,2);
       break;

    case 1012 : 
       osres = Linux_s_chdir(callno,realct,curr_pc,0);
       break;

    case 1013 : 
       osres = Linux_s_compat_time(callno,realct,curr_pc,0);
       break;

    case 1014 : 
       osres = Linux_s_mknod(callno,realct,curr_pc,0);
       break;

    case 1015 : 
       osres = Linux_s_chmod(callno,realct,curr_pc,0);
       break;

    case 1016 : 
       osres = Linux_s_lchown(callno,realct,curr_pc,0);
       break;

    case 1019 : 
       osres = Linux_s_lseek(callno,realct,curr_pc,2);
       break;

    case 1020 : 
       osres = Linux_s_getpid(callno,realct,curr_pc,0);
       break;

    case 1021 : 
       osres = Linux_s_compat_mount(callno,realct,curr_pc,0);
       break;

    case 1022 : 
       osres = Linux_s_oldumount(callno,realct,curr_pc,0);
       break;

    case 1023 : 
       osres = Linux_s_setuid(callno,realct,curr_pc,0);
       break;

    case 1024 : 
       osres = Linux_s_getuid(callno,realct,curr_pc,0);
       break;

    case 1025 : 
       osres = Linux_s_stime(callno,realct,curr_pc,2);
       break;

    case 1026 : 
       osres = Linux_s_ptrace(callno,realct,curr_pc,0);
       break;
    case 1027 : 
       osres = Linux_s_alarm(callno,realct,curr_pc,0);
       break;

    case 1029 : 
       osres = Linux_s_pause(callno,realct,curr_pc,0);
       break;

    case 1030 : 
       osres = Linux_s_compat_utime(callno,realct,curr_pc,0);
       break;

    case 1033 : 
      osres = Linux_s_access(callno,realct,curr_pc,2);
      break;

    case 1034 : 
       osres = Linux_s_nice(callno,realct,curr_pc,2);
       break;

    case 1036 : 
       osres = Linux_s_sync(callno,realct,curr_pc,0);
       break;

    case 1037 : 
       osres = Linux_s_kill(callno,realct,curr_pc,2);
       break;

    case 1038 : 
       osres = Linux_s_rename(callno,realct,curr_pc,0);
       break;

    case 1039 : 
       osres = Linux_s_mkdir(callno,realct,curr_pc,2);
       break;

    case 1040 : 
       osres = Linux_s_rmdir(callno,realct,curr_pc,0);
       break;

    case 1041 : 
       osres = Linux_s_dup(callno,realct,curr_pc,0);
       break;

    case 1042 : 
       osres = Linux_s_pipe(callno,realct,curr_pc,0);
       break;

    case 1043 : 
       osres = Linux_s_compat_times(callno,realct,curr_pc,0);
       break;

    case 1045 : 
       osres = Linux_s_brk(callno,realct,curr_pc,0);
       break;

    case 1046 : 
       osres = Linux_s_setgid(callno,realct,curr_pc,0);
       break;

    case 1047 : 
       osres = Linux_s_getgid(callno,realct,curr_pc,0);
       break;

    case 1048 : 
       osres = Linux_s_signal(callno,realct,curr_pc,0);
       break;

    case 1049 : 
       osres = Linux_s_geteuid(callno,realct,curr_pc,0);
       break;

    case 1050 : 
       osres = Linux_s_getegid(callno,realct,curr_pc,0);
       break;

    case 1051 : 
       osres = Linux_s_acct(callno,realct,curr_pc,0);
       break;

    case 1052 : 
       osres = Linux_s_umount(callno,realct,curr_pc,0);
       break;

    case 1054 : 
       osres = Linux_s_compat_ioctl(callno,realct,curr_pc,0);
       break;

    case 1055 : 
       osres = Linux_s_compat_fcntl(callno,realct,curr_pc,0);
       break;

    case 1057 : 
       osres = Linux_s_setpgid(callno,realct,curr_pc,2);
       break;

    case 1059 : 
       osres = Linux_s_olduname(callno,realct,curr_pc,0);
       break;
    case 1060 : 
       osres = Linux_s_umask(callno,realct,curr_pc,2);
       break;

    case 1061 : 
       osres = Linux_s_chroot(callno,realct,curr_pc,0);
       break;

    case 1062 : 
       osres = Linux_s_ustat(callno,realct,curr_pc,0);
       break;

    case 1063 : 
       osres = Linux_s_dup2(callno,realct,curr_pc,0);
       break;

    case 1064 : 
       osres = Linux_s_getppid(callno,realct,curr_pc,0);
       break;

    case 1065 : 
       osres = Linux_s_getpgrp(callno,realct,curr_pc,0);
       break;

    case 1066 : 
       osres = Linux_s_setsid(callno,realct,curr_pc,0);
       break;

    case 1067 : 
       osres = Linux_s_sigaction(callno,realct,curr_pc,0);
       break;
    case 1068 : 
       osres = Linux_s_sgetmask(callno,realct,curr_pc,0);
       break;

    case 1069 : 
       osres = Linux_s_ssetmask(callno,realct,curr_pc,2);
       break;

    case 1070 : 
       osres = Linux_s_setreuid(callno,realct,curr_pc,0);
       break;

    case 1071 : 
       osres = Linux_s_setregid(callno,realct,curr_pc,0);
       break;

    case 1073 : 
       osres = Linux_s_compat_sigpending(callno,realct,curr_pc,0);
       break;

    case 1074 : 
       osres = Linux_s_sethostname(callno,realct,curr_pc,2);
       break;

    case 1075 : 
       osres = Linux_s_compat_setrlimit(callno,realct,curr_pc,0);
       break;

    case 1076 : 
       osres = Linux_s_compat_old_getrlimit(callno,realct,curr_pc,0);
       break;

    case 1077 : 
       osres = Linux_s_compat_getrusage(callno,realct,curr_pc,0);
       break;

    case 1078 : 
       osres = Linux_s_compat_gettimeofday(callno,realct,curr_pc,2);
       break;

    case 1079 : 
       osres = Linux_s_compat_settimeofday(callno,realct,curr_pc,2);
       break;

    case 1080 : 
       osres = Linux_s_getgroups(callno,realct,curr_pc,2);
       break;

    case 1081 : 
       osres = Linux_s_setgroups(callno,realct,curr_pc,2);
       break;

    case 1083 : 
       osres = Linux_s_symlink(callno,realct,curr_pc,0);
       break;

    case 1085 : 
       osres = Linux_s_readlink(callno,realct,curr_pc,2);
       break;

    case 1086 : 
       osres = Linux_s_uselib(callno,realct,curr_pc,0);
       break;

    case 1087 : 
       osres = Linux_s_swapon(callno,realct,curr_pc,0);
       break;

    case 1088 : 
       osres = Linux_s_reboot(callno,realct,curr_pc,0);
       break;

    case 1089 : 
       osres = Linux_s_readdir(callno,realct,curr_pc,0);
       break;
    case 1090 : 
       osres = Linux_s_mmap(callno,realct,curr_pc,0);
       break;

    case 1091 : 
       osres = Linux_s_munmap(callno,realct,curr_pc,0);
       break;

    case 1092 : 
       osres = Linux_s_truncate(callno,realct,curr_pc,0);
       break;

    case 1093 : 
       osres = Linux_s_ftruncate(callno,realct,curr_pc,0);
       break;

    case 1094 : 
       osres = Linux_s_fchmod(callno,realct,curr_pc,0);
       break;

    case 1095 : 
       osres = Linux_s_fchown(callno,realct,curr_pc,0);
       break;

    case 1096 : 
       osres = Linux_s_getpriority(callno,realct,curr_pc,2);
       break;

    case 1097 : 
       osres = Linux_s_setpriority(callno,realct,curr_pc,2);
       break;

    case 1099 : 
       osres = Linux_s_compat_statfs(callno,realct,curr_pc,0);
       break;

    case 1100 : 
       osres = Linux_s_compat_fstatfs(callno,realct,curr_pc,0);
       break;

    case 1102 : 
       osres = Linux_s_compat_socketcall(callno,realct,curr_pc,0);
       break;

    case 1103 : 
       osres = Linux_s_syslog(callno,realct,curr_pc,2);
       break;

    case 1104 : 
       osres = Linux_s_compat_setitimer(callno,realct,curr_pc,0);
       break;

    case 1105 : 
       osres = Linux_s_compat_getitimer(callno,realct,curr_pc,0);
       break;

    case 1106 : 
       osres = Linux_s_compat_newstat(callno,realct,curr_pc,0);
       break;

    case 1107 : 
       osres = Linux_s_compat_newlstat(callno,realct,curr_pc,0);
       break;
 
    case 1108 : 
       osres = Linux_s_compat_newfstat(callno,realct,curr_pc,0);
       break;

    case 1109 : 
       osres = Linux_s_uname(callno,realct,curr_pc,0);
       break;

    case 1111 : 
       osres = Linux_s_vhangup(callno,realct,curr_pc,0);
       break;

    case 1114 : 
       osres = Linux_s_compat_wait4(callno,realct,curr_pc,0);
       break;

    case 1115 : 
       osres = Linux_s_swapoff(callno,realct,curr_pc,0);
       break;

    case 1116 : 
       osres = Linux_s_sysinfo(callno,realct,curr_pc,0);
       break;
    case 1117 : 
       osres = Linux_s_ipc(callno,realct,curr_pc,0);
       break;
    case 1118 : 
       osres = Linux_s_fsync(callno,realct,curr_pc,0);
       break;

    case 1119 : 
       osres = Linux_s_compat_sigreturn(callno,realct,curr_pc,0);
       following_pc = realct->startaddr;
       break;
     case 1120 : 
       osres = Linux_s_clone(callno,realct,curr_pc,0);
       break;

    case 1121 : 
       osres = Linux_s_setdomainname(callno,realct,curr_pc,2);
       break;

    case 1122 : 
       osres = Linux_s_newuname(callno,realct,curr_pc,0);
       break;
    case 1124 : 
       osres = Linux_s_adjtimex(callno,realct,curr_pc,0);
       break;
    case 1125 : 
       osres = Linux_s_mprotect(callno,realct,curr_pc,0);
       break;

    case 1126 : 
       osres = Linux_s_compat_sigprocmask(callno,realct,curr_pc,0);
       break;

    case 1128 : 
       osres = Linux_s_init_module(callno,realct,curr_pc,0);
       break;

    case 1129 : 
       osres = Linux_s_delete_module(callno,realct,curr_pc,0);
       break;

    case 1131 : 
       osres = Linux_s_quotactl(callno,realct,curr_pc,0);
       break;

    case 1132 : 
       osres = Linux_s_getpgid(callno,realct,curr_pc,2);
       break;

    case 1133 : 
       osres = Linux_s_fchdir(callno,realct,curr_pc,0);
       break;

    case 1134 : 
       osres = Linux_s_bdflush(callno,realct,curr_pc,0);
       break;

    case 1135 : 
       osres = Linux_s_sysfs(callno,realct,curr_pc,2);
       break;

    case 1136 : 
       osres = Linux_s_personality(callno,realct,curr_pc,0);
       break;
    case 1138 : 
       osres = Linux_s_setfsuid(callno,realct,curr_pc,0);
       break;

    case 1139 : 
       osres = Linux_s_setfsgid(callno,realct,curr_pc,0);
       break;

    case 1140 : 
       osres = Linux_s_llseek(callno,realct,curr_pc,0);
       break;

    case 1141 : 
       osres = Linux_s_getdents(callno,realct,curr_pc,0);
       break;
    case 1142 : 
       osres = Linux_s_select(callno,realct,curr_pc,0);
       break;
    case 1143 : 
       osres = Linux_s_flock(callno,realct,curr_pc,0);
       break;

    case 1144 : 
       osres = Linux_s_msync(callno,realct,curr_pc,0);
       break;

    case 1145 : 
       osres = Linux_s_compat_readv(callno,realct,curr_pc,0);
       break;

    case 1146 : 
       osres = Linux_s_compat_writev(callno,realct,curr_pc,0);
       break;

    case 1147 : 
       osres = Linux_s_getsid(callno,realct,curr_pc,2);
       break;

    case 1148 : 
       osres = Linux_s_fdatasync(callno,realct,curr_pc,0);
       break;

    case 1149 : 
       osres = Linux_s_sysctl(callno,realct,curr_pc,0);
       break;
    case 1150 : 
       osres = Linux_s_mlock(callno,realct,curr_pc,0);
       break;

    case 1151 : 
       osres = Linux_s_munlock(callno,realct,curr_pc,0);
       break;

    case 1152 : 
       osres = Linux_s_mlockall(callno,realct,curr_pc,0);
       break;

    case 1153 : 
       osres = Linux_s_munlockall(callno,realct,curr_pc,0);
       break;

    case 1154 : 
       osres = Linux_s_sched_setparam(callno,realct,curr_pc,2);
       break;

    case 1155 : 
       osres = Linux_s_sched_getparam(callno,realct,curr_pc,2);
       break;

    case 1156 : 
       osres = Linux_s_sched_setscheduler(callno,realct,curr_pc,2);
       break;

    case 1157 : 
       osres = Linux_s_sched_getscheduler(callno,realct,curr_pc,2);
       break;

    case 1158 : 
       osres = Linux_s_sched_yield(callno,realct,curr_pc,0);
       break;

    case 1159 : 
       osres = Linux_s_sched_get_priority_max(callno,realct,curr_pc,2);
       break;

    case 1160 : 
       osres = Linux_s_sched_get_priority_min(callno,realct,curr_pc,2);
       break;

    case 1161 : 
       osres = Linux_s_compat_sched_rr_get_interval(callno,realct,curr_pc,2);
       break;

    case 1162 : 
       osres = Linux_s_compat_nanosleep(callno,realct,curr_pc,0);
       break;

    case 1163 : 
       osres = Linux_s_mremap(callno,realct,curr_pc,0);
       break;

    case 1164 : 
       osres = Linux_s_setresuid(callno,realct,curr_pc,0);
       break;

    case 1165 : 
       osres = Linux_s_getresuid(callno,realct,curr_pc,0);
       break;

    case 1167 : 
       osres = Linux_s_poll(callno,realct,curr_pc,0);
       break;

    case 1168 : 
       osres = Linux_s_compat_nfsservctl(callno,realct,curr_pc,0);
       break;

    case 1169 : 
       osres = Linux_s_setresgid(callno,realct,curr_pc,0);
       break;

    case 1170 : 
       osres = Linux_s_getresgid(callno,realct,curr_pc,0);
       break;

    case 1171 : 
       osres = Linux_s_prctl(callno,realct,curr_pc,2);
       break;

    case 1172 : 
       osres = Linux_s_compat_rt_sigreturn(callno,realct,curr_pc,0);
       following_pc = realct->startaddr;
       break;
    case 1173 : 
       osres = Linux_s_rt_sigaction32(callno,realct,curr_pc,0);
       break;
    case 1174 : 
       osres = Linux_s_compat_rt_sigprocmask(callno,realct,curr_pc,0);
       break;
    case 1175 : 
       osres = Linux_s_compat_rt_sigpending(callno,realct,curr_pc,0);
       break;
    case 1176 : 
       osres = Linux_s_compat_rt_sigtimedwait(callno,realct,curr_pc,0);
       break;
    case 1177 : 
       osres = Linux_s_compat_rt_sigqueueinfo(callno,realct,curr_pc,0);
       break;
    case 1178 : 
       osres = Linux_s_compat_rt_sigsuspend(callno,realct,curr_pc,0);
       break;
    case 1179 : 
       osres = Linux_s_pread64(callno,realct,curr_pc,4);
       break;

    case 1180 : 
       osres = Linux_s_pwrite64(callno,realct,curr_pc,4);
       break;

    case 1181 : 
       osres = Linux_s_chown(callno,realct,curr_pc,0);
       break;

    case 1182 : 
       osres = Linux_s_getcwd(callno,realct,curr_pc,0);
       break;

    case 1183 : 
       osres = Linux_s_capget(callno,realct,curr_pc,0);
       break;

    case 1184 : 
       osres = Linux_s_capset(callno,realct,curr_pc,0);
       break;

    case 1185 : 
       osres = Linux_s_compat_sigaltstack(callno,realct,curr_pc,0);
       break;

    case 1186 : 
       osres = Linux_s_sendfile(callno,realct,curr_pc,2);
       break;

    case 1189 : 
       osres = Linux_s_vfork(callno,realct,curr_pc,0);
       break;

    case 1190 : 
       osres = Linux_s_compat_getrlimit(callno,realct,curr_pc,0);
       break;

    case 1191 : 
       osres = Linux_s_readahead(callno,realct,curr_pc,4);
       break;

    case 1192 : 
       osres = Linux_s_mmap2(callno,realct,curr_pc,0xa);
       break;

    case 1193 : 
       osres = Linux_s_truncate64(callno,realct,curr_pc,0);
       break;

    case 1194 : 
       osres = PPC_ftruncate64(callno,realct,curr_pc,0);
       break;

    case 1195 : 
       osres = Linux_s_stat64(callno,realct,curr_pc,0);
       break;

    case 1196 : 
       osres = Linux_s_lstat64(callno,realct,curr_pc,0);
       break;

    case 1197 : 
       osres = Linux_s_fstat64(callno,realct,curr_pc,0);
       break;

    case 1198 : 
       osres = Linux_s_pciconfig_read(callno,realct,curr_pc,2);
       break;

    case 1199 : 
       osres = Linux_s_pciconfig_write(callno,realct,curr_pc,2);
       break;

    case 1200 : 
       osres = Linux_s_pciconfig_iobase(callno,realct,curr_pc,2);
       break;

    case 1202 : 
       osres = Linux_s_getdents64(callno,realct,curr_pc,0);
       break;

    case 1203 : 
       osres = Linux_s_pivot_root(callno,realct,curr_pc,0);
       break;

    case 1204 : 
       osres = Linux_s_compat_fcntl64(callno,realct,curr_pc,0);
       break;

    case 1205 : 
       osres = Linux_s_madvise(callno,realct,curr_pc,0);
       break;

    case 1206 : 
       osres = Linux_s_mincore(callno,realct,curr_pc,0);
       break;

    case 1207 : 
       osres = Linux_s_gettid(callno,realct,curr_pc,0);
       break;

    case 1208 : 
       osres = Linux_s_tkill(callno,realct,curr_pc,0);
       break;

    case 1209 : 
       osres = Linux_s_setxattr(callno,realct,curr_pc,0);
       break;

    case 1210 : 
       osres = Linux_s_lsetxattr(callno,realct,curr_pc,0);
       break;

    case 1211 : 
       osres = Linux_s_fsetxattr(callno,realct,curr_pc,0);
       break;

    case 1212 : 
       osres = Linux_s_getxattr(callno,realct,curr_pc,0);
       break;

    case 1213 : 
       osres = Linux_s_lgetxattr(callno,realct,curr_pc,0);
       break;

    case 1214 : 
       osres = Linux_s_fgetxattr(callno,realct,curr_pc,0);
       break;

    case 1215 : 
       osres = Linux_s_listxattr(callno,realct,curr_pc,0);
       break;

    case 1216 : 
       osres = Linux_s_llistxattr(callno,realct,curr_pc,0);
       break;

    case 1217 : 
       osres = Linux_s_flistxattr(callno,realct,curr_pc,0);
       break;

    case 1218 : 
       osres = Linux_s_removexattr(callno,realct,curr_pc,0);
       break;

    case 1219 : 
       osres = Linux_s_lremovexattr(callno,realct,curr_pc,0);
       break;

    case 1220 : 
       osres = Linux_s_fremovexattr(callno,realct,curr_pc,0);
       break;

    case 1221 : 
       osres = Linux_s_compat_futex(callno,realct,curr_pc,0);
       break;

    case 1222 : 
       osres = Linux_s_compat_sched_setaffinity(callno,realct,curr_pc,0);
       break;

    case 1223 : 
       osres = Linux_s_compat_sched_getaffinity(callno,realct,curr_pc,0);
       break;

    case 1226 : 
       osres = Linux_s_sendfile64(callno,realct,curr_pc,2);
       break;

    case 1227 : 
       osres = Linux_s_compat_io_setup(callno,realct,curr_pc,0);
       break;

    case 1228 : 
       osres = Linux_s_io_destroy(callno,realct,curr_pc,0);
       break;

    case 1229 : 
       osres = Linux_s_compat_io_getevents(callno,realct,curr_pc,0);
       break;

    case 1230 : 
       osres = Linux_s_compat_io_submit(callno,realct,curr_pc,0);
       break;

    case 1231 : 
       osres = Linux_s_io_cancel(callno,realct,curr_pc,0);
       break;

    case 1232 : 
       osres = Linux_s_set_tid_address(callno,realct,curr_pc,2);
       break;

    case 1233 : 
       osres = Linux_s_fadvise64(callno,realct,curr_pc,4);
       break;

    case 1234 : 
       osres = Linux_s_exit_group(callno,realct,curr_pc,0);
       break;

    case 1235 : 
       osres = Linux_s_lookup_dcookie(callno,realct,curr_pc,4);
       break;

    case 1236 : 
       osres = Linux_s_epoll_create(callno,realct,curr_pc,0);
       break;

    case 1237 : 
       osres = Linux_s_epoll_ctl(callno,realct,curr_pc,0);
       break;

    case 1238 : 
       osres = Linux_s_epoll_wait(callno,realct,curr_pc,0);
       break;

    case 1239 : 
       osres = Linux_s_remap_file_pages(callno,realct,curr_pc,0);
       break;

    case 1240 : 
       osres = Linux_s_timer_create(callno,realct,curr_pc,0);
       break;
    case 1241 : 
       osres = Linux_s_timer_settime(callno,realct,curr_pc,0);
       break;

    case 1242 : 
       osres = Linux_s_timer_gettime(callno,realct,curr_pc,0);
       break;

    case 1243 : 
       osres = Linux_s_timer_getoverrun(callno,realct,curr_pc,0);
       break;

    case 1244 : 
       osres = Linux_s_timer_delete(callno,realct,curr_pc,0);
       break;

    case 1245 : 
       osres = Linux_s_compat_clock_settime(callno,realct,curr_pc,0);
       break;

    case 1246 : 
       osres = Linux_s_compat_clock_gettime(callno,realct,curr_pc,0);
       break;

    case 1247 : 
       osres = Linux_s_compat_clock_getres(callno,realct,curr_pc,0);
       break;

    case 1248 : 
       osres = Linux_s_compat_clock_nanosleep(callno,realct,curr_pc,0);
       break;

    case 1249 : 
       osres = PPC_swapcontext32(callno,realct,curr_pc,0);
       following_pc = realct->startaddr;
       break;
    case 1250 : 
       osres = Linux_s_tgkill(callno,realct,curr_pc,2);
       break;

    case 1251 : 
       osres = Linux_s_compat_utimes(callno,realct,curr_pc,0);
       break;

    case 1252 : 
       osres = Linux_s_compat_statfs64(callno,realct,curr_pc,0);
       break;

    case 1253 : 
       osres = Linux_s_compat_fstatfs64(callno,realct,curr_pc,0);
       break;

    case 1255 : 
       osres = Linux_s_ppc_rtas(callno,realct,curr_pc,0);
       break;

    case 1258 : 
       osres = Linux_s_compat_migrate_pages(callno,realct,curr_pc,0);
       break;

    case 1259 : 
       osres = Linux_s_compat_mbind(callno,realct,curr_pc,0);
       break;

    case 1260 : 
       osres = Linux_s_compat_get_mempolicy(callno,realct,curr_pc,0);
       break;

    case 1261 : 
       osres = Linux_s_compat_set_mempolicy(callno,realct,curr_pc,0);
       break;

    case 1262 : 
       osres = Linux_s_compat_mq_open(callno,realct,curr_pc,0);
       break;

    case 1263 : 
       osres = Linux_s_mq_unlink(callno,realct,curr_pc,0);
       break;

    case 1264 : 
       osres = Linux_s_compat_mq_timedsend(callno,realct,curr_pc,0);
       break;

    case 1265 : 
       osres = Linux_s_compat_mq_timedreceive(callno,realct,curr_pc,0);
       break;

    case 1266 : 
       osres = Linux_s_compat_mq_notify(callno,realct,curr_pc,0);
       break;

    case 1267 : 
       osres = Linux_s_compat_mq_getsetattr(callno,realct,curr_pc,0);
       break;

    case 1268 : 
       osres = Linux_s_compat_kexec_load(callno,realct,curr_pc,0);
       break;

    case 1269 : 
       osres = Linux_s_compat_add_key(callno,realct,curr_pc,0);
       break;

    case 1270 : 
       osres = Linux_s_compat_request_key(callno,realct,curr_pc,0);
       break;

    case 1271 : 
       osres = Linux_s_compat_keyctl(callno,realct,curr_pc,0);
       break;

    case 1272 : 
       osres = Linux_s_compat_waitid(callno,realct,curr_pc,0);
       break;

    case 1273 : 
       osres = Linux_s_compat_ioprio_set(callno,realct,curr_pc,0);
       break;

    case 1274 : 
       osres = Linux_s_compat_ioprio_get(callno,realct,curr_pc,0);
       break;

    case 1275 : 
       osres = Linux_s_inotify_init(callno,realct,curr_pc,0);
       break;

    case 1276 : 
       osres = Linux_s_inotify_add_watch(callno,realct,curr_pc,0);
       break;

    case 1277 : 
       osres = Linux_s_inotify_rm_watch(callno,realct,curr_pc,0);
       break;

    case 1278 : 
       osres = Linux_s_spu_run(callno,realct,curr_pc,0);
       break;

    case 1279 : 
       osres = Linux_s_spu_create(callno,realct,curr_pc,0);
       break;

    case 1280 : 
       osres = Linux_s_compat_pselect6(callno,realct,curr_pc,0);
       break;

    case 1281 : 
       osres = Linux_s_compat_ppoll(callno,realct,curr_pc,0);
       break;

    case 1282 : 
       osres = Linux_s_unshare(callno,realct,curr_pc,0);
       break;

    case 1283 : 
       osres = Linux_s_splice(callno,realct,curr_pc,0);
       break;

    case 1284 : 
       osres = Linux_s_tee(callno,realct,curr_pc,0);
       break;

    case 1285 : 
       osres = Linux_s_compat_vmsplice(callno,realct,curr_pc,0);
       break;

    case 1286 : 
       osres = Linux_s_compat_openat(callno,realct,curr_pc,0);
       break;

    case 1287 : 
       osres = Linux_s_mkdirat(callno,realct,curr_pc,0);
       break;

    case 1288 : 
       osres = Linux_s_mknodat(callno,realct,curr_pc,0);
       break;

    case 1289 : 
       osres = Linux_s_fchownat(callno,realct,curr_pc,0);
       break;

    case 1290 : 
       osres = Linux_s_compat_futimesat(callno,realct,curr_pc,0);
       break;

    case 1291 : 
       osres = Linux_s_newfstatat(callno,realct,curr_pc,0);
       break;

    case 1292 : 
       osres = Linux_s_unlinkat(callno,realct,curr_pc,0);
       break;

    case 1293 : 
       osres = Linux_s_renameat(callno,realct,curr_pc,0);
       break;

    case 1294 : 
       osres = Linux_s_linkat(callno,realct,curr_pc,0);
       break;

    case 1295 : 
       osres = Linux_s_symlinkat(callno,realct,curr_pc,0);
       break;

    case 1296 : 
       osres = Linux_s_readlinkat(callno,realct,curr_pc,0);
       break;

    case 1297 : 
       osres = Linux_s_fchmodat(callno,realct,curr_pc,0);
       break;

    case 1298 : 
       osres = Linux_s_faccessat(callno,realct,curr_pc,0);
       break;

    case 1299 : 
       osres = Linux_s_compat_get_robust_list(callno,realct,curr_pc,0);
       break;

    case 1300 : 
       osres = Linux_s_compat_set_robust_list(callno,realct,curr_pc,0);
       break;

    case 1301 : 
       osres = Linux_s_compat_move_pages(callno,realct,curr_pc,0);
       break;

    case 1302 : 
       osres = Linux_s_getcpu(callno,realct,curr_pc,0);
       break;

    case 1303 : 
       osres = Linux_s_compat_epoll_pwait(callno,realct,curr_pc,0);
       break;

    case 1304 : 
       osres = Linux_s_compat_utimensat(callno,realct,curr_pc,0);
       break;

    case 1305 : 
       osres = Linux_s_compat_signalfd(callno,realct,curr_pc,0);
       break;

    case 1306 : 
       osres = Linux_s_timerfd_create(callno,realct,curr_pc,0);
       break;

    case 1307 : 
       osres = Linux_s_eventfd(callno,realct,curr_pc,0);
       break;

    case 1308 : 
       osres = Linux_s_compat_sync_file_range2(callno,realct,curr_pc,0);
       break;

    case 1309 : 
       osres = Linux_s_compat_fallocate(callno,realct,curr_pc,0);
       break;

    case 1310 : 
       osres = Linux_s_subpage_prot(callno,realct,curr_pc,0);
       break;

    case 1311 : 
       osres = Linux_s_compat_timerfd_settime(callno,realct,curr_pc,0);
       break;

    case 1312 : 
       osres = Linux_s_compat_timerfd_gettime(callno,realct,curr_pc,0);
       break;

    case 1313 : 
       osres = Linux_s_compat_signalfd4(callno,realct,curr_pc,0);
       break;

    case 1314 : 
       osres = Linux_s_eventfd2(callno,realct,curr_pc,0);
       break;

    case 1315 : 
       osres = Linux_s_epoll_create1(callno,realct,curr_pc,0);
       break;

    case 1316 : 
       osres = Linux_s_dup3(callno,realct,curr_pc,0);
       break;

    case 1317 : 
       osres = Linux_s_pipe2(callno,realct,curr_pc,0);
       break;

    case 1318 : 
       osres = Linux_s_inotify_init1(callno,realct,curr_pc,0);
       break;

    case 1319 : 
       osres = Linux_s_perf_counter_open(callno,realct,curr_pc,0);
       break;

    case 1320 : 
       osres = Linux_s_compat_preadv(callno,realct,curr_pc,0);
       break;

    case 1321 : 
       osres = Linux_s_compat_pwritev(callno,realct,curr_pc,0);
       break;

    case 1322 : 
       osres = Linux_s_compat_rt_tgsigqueueinfo(callno,realct,curr_pc,0);
       break;

     default:
       fprintf(stderr,"Unimplemented Syscall: %d\n",callno);
       exit(1);

  }

  if (OS_ict_osinfo(realct)->state == ProcBlocked &&
      OS_ict_osinfo(realct)->sigPendingFlag) { 
    // we blocked with a signal pending!
    OS_ict_osinfo(realct)->state = ProcRunnable;
    OS_ict_osinfo(realct)->os->ready_list.push_back(OS_ict_osinfo(realct));
    OS_ict_osinfo(realct)->os->wantScheduling = true; 
  } else 
    CC_do_a_signal(realct, r0val != 172 && r0val != 1172 && r0val != 1119, 
		   curr_pc, following_pc);

  if (OS_ict_osinfo(realct)->state != ProcBlocked)
    OS_ict_osinfo(realct)->trapno = 0;

  if (OS_ict_osinfo(realct)->os->wantScheduling)
    Linux_schedule(&CC_emu_interface(realct));    

  following_pc = realct->startaddr;

  return 0;
}

} // namespace LSE_PowerPC
