/*%  -*-c-*- 
 * Copyright (c) 2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LIS file defining the PowerPC ISA FP types
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * This file defines just the types
 *
 * NOTE: this file is included *inside* the emulator namespace.
 */

const uint32_t fpscr_fx_mask = GBIT32<31>::v;
const uint32_t fpscr_fex_mask = GBIT32<30>::v;
const uint32_t fpscr_vx_mask = GBIT32<29>::v;
const uint32_t fpscr_ox_mask = GBIT32<28>::v;
const uint32_t fpscr_ux_mask = GBIT32<27>::v;
const uint32_t fpscr_zx_mask = GBIT32<26>::v;
const uint32_t fpscr_xx_mask = GBIT32<25>::v;
const uint32_t fpscr_vxsnan_mask = GBIT32<24>::v;
const uint32_t fpscr_vxisi_mask = GBIT32<23>::v;
const uint32_t fpscr_vxidi_mask = GBIT32<22>::v;
const uint32_t fpscr_vxzdz_mask = GBIT32<21>::v;
const uint32_t fpscr_vximz_mask = GBIT32<20>::v;
const uint32_t fpscr_vxvc_mask = GBIT32<19>::v;
const uint32_t fpscr_fr_mask = GBIT32<18>::v;
const uint32_t fpscr_fi_mask = GBIT32<17>::v;
const uint32_t fpscr_fprf_mask = GBITS32<5>::v << 12;
const uint32_t fpscr_fpcc_mask = GBITS32<4>::v << 12;
const uint32_t fpscr_uord_mask = GBIT32<12>::v;
const uint32_t fpscr_vxsoft_mask = GBIT32<10>::v;
const uint32_t fpscr_vxsqrt_mask = GBIT32<9>::v;
const uint32_t fpscr_vxcvi_mask = GBIT32<8>::v;
const uint32_t fpscr_ve_mask = GBIT32<7>::v;
const uint32_t fpscr_oe_mask = GBIT32<6>::v;
const uint32_t fpscr_ue_mask = GBIT32<5>::v;
const uint32_t fpscr_ze_mask = GBIT32<4>::v;
const uint32_t fpscr_xe_mask = GBIT32<3>::v;
const uint32_t fpscr_ni_mask = GBIT32<2>::v;
const uint32_t fpscr_rn_mask = GBITS32<2>::v;

const uint32_t fpscr_stickybits 
 = ( fpscr_fx_mask | fpscr_ox_mask | fpscr_ux_mask | fpscr_zx_mask |
     fpscr_xx_mask | fpscr_vxsnan_mask | fpscr_vxisi_mask | fpscr_vxidi_mask |
     fpscr_vxzdz_mask | fpscr_vximz_mask | fpscr_vxvc_mask |
     fpscr_vxsoft_mask | fpscr_vxsqrt_mask | fpscr_vxcvi_mask);
const uint32_t fpscr_clrbits = GBITS32<7>::v << 12;
const uint32_t fpscr_keepbits = ~fpscr_clrbits;
const uint32_t fpscr_setbits = fpscr_stickybits | fpscr_clrbits;

  // floating point goodies.  Need to define single/double types with their
  // own qnan stuff, as qnan is different on each system

  using LSE_ieee754::fp_rmodes;
using LSE_ieee754::fp_valtypes;

class pfp_single;
class pfp_double;

class pfp_single : public LSE_ieee754::fp_single {
 public:
 pfp_single() : fp_single() {}
  explicit pfp_single(uint32_t v) : fp_single(v) { }

  inline void form_qnan_significand(const pfp_single &a);
  inline void form_qnan_significand(const pfp_double &a);
  inline void form_qnan_significand(const pfp_single &a,
				    const pfp_single &b);
  inline int get_fprf();
};

class pfp_double : public LSE_ieee754::fp_double {
 public:
 pfp_double() : fp_double() {}
  explicit pfp_double(uint64_t v) : fp_double(v) { }

  inline void form_qnan_significand(const pfp_single &a);
  inline void form_qnan_significand(const pfp_double &a);
  inline void form_qnan_significand(const pfp_double &a,
				    const pfp_double &b);
  inline int get_fprf();
};

void pfp_single::form_qnan_significand(const pfp_single &a) {
  if (a.valtype != LSE_ieee754::nan) {
    sign = false;
    significand = LSE_ieee754::bits<sig_t>(ssize-1);
  } else {
    sign = a.sign;
    significand = a.significand;
    if (!a.nan_is_qnan()) significand |= LSE_ieee754::bit<sig_t>(ssize-3);
  }
}

void pfp_single::form_qnan_significand(const pfp_double &a) {
  if (a.valtype != LSE_ieee754::nan) {
    sign = false;
    significand = LSE_ieee754::bits<sig_t>(ssize-1);
  } else {
    sign = a.sign;
    significand = sig_t(a.significand >> (pfp_double::ssize - ssize));
    if (!a.nan_is_qnan()) significand |= LSE_ieee754::bit<sig_t>(ssize-3);
  }
}

void pfp_single::form_qnan_significand(const pfp_single &a,
				       const pfp_single &b) {
  if (a.valtype != LSE_ieee754::nan && b.valtype != LSE_ieee754::nan) {
    sign = false;
    significand = LSE_ieee754::bits<sig_t>(ssize-1);
  } else if (b.valtype == LSE_ieee754::nan && !b.nan_is_qnan()) {
    sign = b.sign;
    significand = b.significand | LSE_ieee754::bit<sig_t>(ssize-3);
  } else if (a.valtype == LSE_ieee754::nan && !a.nan_is_qnan()) {
    sign = a.sign;
    significand = a.significand | LSE_ieee754::bit<sig_t>(ssize-3);
  } else if (b.valtype == LSE_ieee754::nan) {
    sign = b.sign;
    significand = b.significand;
  } else {
    sign = a.sign;
    significand = a.significand;
  }
}

int pfp_single::get_fprf() {
  return (valtype == LSE_ieee754::nan ? (nan_is_qnan() ? 0x11 : 0x1)
	  : valtype == LSE_ieee754::infinity ? (sign ? 0x9 : 0x5 )
	  : is_denorm() ? (sign ? 0x18: 0x14 )
	  : valtype == LSE_ieee754::zero ? (sign ? 0x12 : 0x2)
	  : (sign ? 0x8 : 4));
}

void pfp_double::form_qnan_significand(const pfp_single &a) {
  if (a.valtype != LSE_ieee754::nan) {
    sign = false;
    significand = LSE_ieee754::bits<sig_t>(ssize-1);
  } else {
    sign = a.sign;
    significand = sig_t(a.significand) << (ssize - pfp_single::ssize);
    if (!a.nan_is_qnan()) significand |= LSE_ieee754::bit<sig_t>(ssize-3);
  }
}

void pfp_double::form_qnan_significand(const pfp_double &a) {
  if (a.valtype != LSE_ieee754::nan) {
    sign = false;
    significand = LSE_ieee754::bits<sig_t>(ssize-1);
  } else {
    sign = a.sign;
    significand = a.significand;
    if (!a.nan_is_qnan()) significand |= LSE_ieee754::bit<sig_t>(ssize-3);
  }
}

void pfp_double::form_qnan_significand(const pfp_double &a,
				       const pfp_double &b) {
  if (a.valtype != LSE_ieee754::nan && b.valtype != LSE_ieee754::nan) {
    sign = false;
    significand = LSE_ieee754::bits<sig_t>(ssize-1);
  } else if (b.valtype == LSE_ieee754::nan && !b.nan_is_qnan()) {
    sign = b.sign;
    significand = b.significand | LSE_ieee754::bit<sig_t>(ssize-3);
  } else if (a.valtype == LSE_ieee754::nan && !a.nan_is_qnan()) {
    sign = a.sign;
    significand = a.significand | LSE_ieee754::bit<sig_t>(ssize-3);
  } else if (b.valtype == LSE_ieee754::nan) {
    sign = b.sign;
    significand = b.significand;
  } else {
    sign = a.sign;
    significand = a.significand;
  }
}

int pfp_double::get_fprf() {
  return (valtype == LSE_ieee754::nan ? (nan_is_qnan() ? 0x11 : 0x1)
	  : valtype == LSE_ieee754::infinity ? (sign ? 0x9 : 0x5 )
	  : is_denorm() ? (sign ? 0x18: 0x14 )
	  : valtype == LSE_ieee754::zero ? (sign ? 0x12 : 0x2)
	  : (sign ? 0x8 : 4));
}

inline void fp_status_update(int status, uint32_t src_fsr, uint32_t &dest_fsr, 
			     int fprf) {
  int newmask = status & 0x1f;
  uint32_t tval = src_fsr & fpscr_keepbits;

  // if invalid, do 3.3.6.1.1.  Take trap if enabled and return
  if ((status & LSE_ieee754::exc_invalid) || (src_fsr & fpscr_vxsoft_mask)) {
    if (status & LSE_ieee754::signan) tval |= fpscr_vxsnan_mask;
    tval |= fprf << 12;
    dest_fsr = tval;
    return;
  }

  // if div by zero, do 3.3.6.1.2.   Take trap if enabled and return
  if (status & LSE_ieee754::exc_divbyzero) {
    tval |= fpscr_zx_mask;
    tval |= fprf << 12;
    dest_fsr = tval;
    return;
  }

  if (status & LSE_ieee754::roundedup) tval |= fpscr_fr_mask;

  if (status & LSE_ieee754::exc_underflow) {

    if (src_fsr & fpscr_ue_mask) tval |= fpscr_ux_mask;
    else if (status & LSE_ieee754::exc_inexact) tval |= fpscr_ux_mask;

    if (status & LSE_ieee754::roundedup) tval |= fpscr_fi_mask | fpscr_fr_mask;

  } else if (status & LSE_ieee754::exc_overflow) {

    tval |= fpscr_ox_mask;
    if (!(src_fsr & fpscr_oe_mask)) tval |= fpscr_xx_mask | fpscr_fi_mask;
    else if (status & LSE_ieee754::roundedup) 
      tval |= fpscr_fi_mask | fpscr_fr_mask;

  }

  if (status & LSE_ieee754::exc_inexact) tval |= fpscr_xx_mask;
  tval |= fprf << 12;
  
  dest_fsr = tval;
}

inline bool fp_fix_status(uint32_t ov, uint32_t &dest_fsr, bool dofx) {
  uint32_t nv = dest_fsr;

  // set VX bit
  if (nv & (GBITS32<6>::v << 19)) nv |= fpscr_vx_mask;

  // set FX bit
  if (dofx && (nv & ~ov & (GBITS32<11>::v<<19))) nv |= fpscr_fx_mask;

  // set FEX bit
  if (((nv >> 25) & (nv >> 3)) & 0x1f) nv |= fpscr_fex_mask;
  dest_fsr = nv;

  return (nv & fpscr_fex_mask);
}

inline uint64_t fp_f2d (uint32_t tval) {
  uint64_t rval;

  int exp = (tval >> 23) & 0xff;
  if (exp > 0 && exp < 255) {
    rval = ( ( uint64_t(7 * (((tval >> 30) & 1)^1)) << 59 ) |
	     ( uint64_t((tval >> 30) & 3) << 62 ) |
	     ( uint64_t(tval & GBITS32<30>::v) << 29) );
  } else if (exp == 0 && (tval & GBITS32<23>::v)) {
    exp = -126;
    uint64_t frac = uint64_t(tval & GBITS32<23>::v) << 29;
    while (!(frac & GBIT64<52>::v)) {
      frac <<= 1;
      exp --;
    }
    rval = ( ( (frac & GBITS64<52>::v) ) |
	     ( (uint64_t(tval & GBIT32<31>::v) << 32) ) |
	       ( uint64_t(exp + 1023) << 52) );
  } else {
    rval = ( ( uint64_t(7 * ((tval >> 30) & 1)) << 59 ) |
	     ( uint64_t((tval >> 30) & 3) << 62 ) |
	     ( uint64_t(tval & GBITS32<30>::v) << 29) );
  }

  //std::cerr << "Converting " << std::hex << tval << " to " << rval 
  //    << std::dec << "\n";
  return rval;
}

inline uint32_t fp_d2f (uint64_t tval) {
  // Because the value is stored in a "large" format, I need to make
  // sure it gets stuffed into the smaller format properly.  Conversion
  // routines might not be the best idea here.  The manual is pretty clear
  // what has to happen.
  uint32_t res, exp, rval;
  exp = (tval >> 52) & GBITS32<11>::v;
  if ( exp > 896 || !(tval & ~(uint64_t(1)<<63)) )
    rval = (tval >> 32) & (GBITS32<2>::v << 30) | (tval >> 29) & GBITS32<30>::v;
  else if (exp >= 874) {
    exp -= 1023;
    uint64_t frac = GBIT64<52>::v | (tval & GBITS64<52>::v);
    while (exp < -126) {
      frac >>= 1;
      exp++;
    }
    rval = ((tval >> 32) & bit31) | (frac >> 29) & GBITS32<23>::v;
  } else rval = 0;

  //std::cerr << "Converting " << std::hex << tval << " to " << rval 
  //	    << std::dec << "\n";
  return rval;
}
