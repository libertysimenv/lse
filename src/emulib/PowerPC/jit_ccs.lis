/* -*-c-*-
 * Copyright (c) 2011-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LIS PPC JIT-CCS description
 *
 * Authors: Kurtis Cahill <mathematica_k@hotmail.com>
 *
 * This file describes how to do jit-ccs for PPC
 *
 */
style JIT_CCS {

  codesection JIT_CCS_headers {
#define CACHE_REGPTRS
  }

  implement cache_with_regptrs = single;

  /*******************************************************************
   *             Definition of entrypoints and specialization
   *******************************************************************/

  specialize instr, addr, regptrsP;

  generator latepublic {
    void %%NAME()(%%PARMS());
  }

  /********************************************************************
   *                 Code cache definition     
   ********************************************************************/

  codesection codecache_entries {
    JIT_CCS::LIS_atable_t jitccs_table;
  }

  /********************************************************************
   *                 Code to do the work
   ********************************************************************/

  generator JIT_CCS_epilogue {
    void %%NAME()(%%PARMS()) {
      %%DECLS();
	
      // check the va cache first... 
    tryagain:
      codecache_ent *lb = codecache_vlookup(ctx, addr, true);
      
      if (!lb) {
	%%BEFORE();
	  
	// Still have to set the register pointers since the interpreter code
	// will use the pointer version.  %%BEFORE will have done decode
	CACHE_REGPTRS_ptrs regptrs;
	regptrsP = &regptrs;	  
	CACHE_REGPTRS_set_regptrs(LIS_ii, swcontexttok, decodetoken, instr, 
				  regptrsP);
	
	%%IFAFTER(%%RTYPE() (*ep)(%%AFTERPARMS()) = %%AFTERPTR();
		  ep(%%AFTERARGS()););
	return;
      }

      JIT_CCS::LIS_atable_t *ep = &lb->code.d.jitccs_table;
      if (!ep->%%NAME()) {

	if (ctx.di->codecache->bytesUsed / 1024 >= ctx.di->cache_size) {
	  ctx.di->codecache->reset(true); // hook?
	  goto tryagain;
	}

	%%BEFORE();
	lb->code.d.jitccs_table = *(ep = & %%AFTERREC());
	lb->code.d.instr = instr;
	lb->code.d.decodetoken = decodetoken;
	regptrsP = &lb->code.d.regptrs;
	CACHE_REGPTRS_set_regptrs(LIS_ii, swcontexttok, decodetoken, instr, 
				  regptrsP);
      } else {
	instr = lb->code.d.instr;
	regptrsP = &lb->code.d.regptrs;
	decodetoken = lb->code.d.decodetoken;
      }
      
      return (*ep->%%NAME())(%%AFTERARGS());
    } // %%NAME()
  } // JIT_CCS_epilogue

} // style JIT_CCS

