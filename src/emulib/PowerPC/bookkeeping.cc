/* 
 * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Bookkeeping routines
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file contains routines meant to handle the bookkeeping:
 * 1) context creation, deletion, loading
 * 2) context mapping
 * 3) emulator initialization/finalization
 * 4) PC manipulation
 * 5) Instruction info initialization
 *
 */

#include <LSE_inttypes.h>
#include "LSE_PowerPC.priv.h"
#include <stdio.h>
#include <stdlib.h>

namespace LSE_PowerPC {

  extern int Linux_context_init(LSE_emu_isacontext_t *);
  extern int Linux_context_load(LSE_emu_isacontext_t *,int,char **,char **);
  extern int Linux_init(LSE_emu_interface_t *);
  extern int Linux_finish(LSE_emu_interface_t *);
  extern void Linux_schedule(LSE_emu_interface_t *);
  extern int Linux_context_clone(LSE_emu_interface_t *ifc,
				 PPC_context_t **, PPC_context_t *);
  extern bool Linux_handle_fault(LSE_emu_isacontext_t *, 
				 PPC_fault_t f, LSE_emu_addr_t pc,
				 LSE_emu_addr_t &npc);

  // **************** Emulator initialization and finalization *******

  void EMU_init(LSE_emu_interface_t *ifc) {
    PPC_dinst_t *di = new PPC_dinst_t(ifc);
    ifc->etoken = static_cast<void *>(di);
#ifdef CHECKPOINTS
    emu_checkpoint_init();
#endif
    Linux_init(ifc);
  }

  void EMU_finish(LSE_emu_interface_t *ifc) {
#ifdef CHECKPOINTS
    emu_checkpoint_finish();
#endif
    Linux_finish(ifc);
    if (ifc->etoken) delete static_cast<PPC_dinst_t *>(ifc->etoken);
    ifc->etoken = 0;
  }

  // ***************** Play with the current PC **********************

  LSE_emu_iaddr_t EMU_get_start_addr(LSE_emu_ctoken_t ct) {
    LSE_emu_isacontext_t& ctx = *(static_cast < LSE_emu_isacontext_t * >(ct));
    return ctx.startaddr;
  }
  void EMU_set_start_addr(LSE_emu_ctoken_t ct, LSE_emu_iaddr_t addr) {
    LSE_emu_isacontext_t& ctx = *(static_cast < LSE_emu_isacontext_t * >(ct));
    ctx.startaddr = addr;
  }

  // ********************** clocks *******************

  int EMU_register_clock(LSE_emu_interface_t *ifc, int hwcno, int clockno,
			  LSE_clock_t clock) {
    PPC_dinst_t *di = reinterpret_cast<PPC_dinst_t *>(ifc->etoken);
    di->clock = clock;
    return 0;
  }

  int EMU_register_interrupt_callback(LSE_emu_interface_t *ifc, 
				      int hwcno, 
				      LSE_emu_interrupt_callback_t h) {
    PPC_dinst_t *di = static_cast<PPC_dinst_t *>(ifc->etoken);
    di->interrupts[hwcno] = h;
    return 0;
  }

  // ******************** Context creation ****************************

  /* 
   * Called by simulator, but not emulators to create software contexts; we
   * ask the Linux emulator to clone the context from the init process.
   */
  int EMU_context_create(LSE_emu_interface_t *ifc,
			 LSE_emu_ctoken_t *ctp, int cno)
  {
    if (cno >= 0) {
      *ctp = 0; // not mapped.
      return 0; 
    } else return 1;
  }

  int EMU_context_destroy(LSE_emu_interface_t *ifc,
			  LSE_emu_ctoken_t ct)
  {
    LSE_emu_isacontext_t *ctx = reinterpret_cast<LSE_emu_isacontext_t *>(ct);
    ctx->di->contexts.erase(ctx);
    delete ctx;
  }

  /*
   * Copy a context.  If old context is 0, then really it's a new context
   */
  int EMU_context_copy(LSE_emu_interface_t *ifc,
		       LSE_emu_isacontext_t **ctpp, 
		       LSE_emu_isacontext_t *oldct) {
    LSE_emu_isacontext_t *realct;
    PPC_dinst_t *di = reinterpret_cast<PPC_dinst_t *>(ifc->etoken);
    
    realct = new LSE_emu_isacontext_t;
    if (!realct) {
      fprintf(stderr,"Out of memory creating LSE PPC emulator context\n");
      return 1;
    }
    di->contexts.insert(realct);
    if (oldct) *realct = *oldct;
    else {
      realct->done = true;
      realct->r[0] = 0; // not truly constant, is it?
      realct->di = (PPC_dinst_t *)(ifc->etoken);
      realct->dcbz_size = 64; // some initial value
      realct->msr = 0;
      realct->mem = di->mem;
      realct->reservationList.init();

      realct->startaddr = 0;
      realct->lr = 0;
      realct->ctr = realct->xer = 0;
      realct->fpscr = realct->cr = 0;
      memset(&realct->r,0,sizeof(realct->r));
      memset(&realct->f,0,sizeof(realct->f));
      realct->osinfo = 0;
    }
    realct->globalcno = 0;
    realct->reservationList.incr();
    realct->softmmu.clear(MMUhandler(*realct));
    realct->copy_hook(oldct);
    realct->users = 0;

#ifdef TOMOVE
#ifdef CHECKPOINTING
    realct->OSRecords = LSE_chkpt_build_sequence(NULL);
    realct->currOSRecord = NULL;
    realct->wasInitialOS = FALSE;
#endif
#endif
    *ctpp = realct;
    return 0;
  }

  // ******************** Context loading *****************************

  int EMU_context_load(LSE_emu_interface_t *ifc, int cno,
		       int argc, char *argv[], char **envp) {
    if (cno <= 0 || cno > LSE_emu_hwcontexts_total ||
	!LSE_emu_hwcontexts_table[cno].valid) return 1;

    int rval;
    LSE_emu_isacontext_t *realctp 
      = (LSE_emu_isacontext_t *)(LSE_emu_hwcontexts_table[cno].ctok);

    // loading into an empty context is the same as running execve off of
    // a process forked from init.  Thus we need to call Linux_context_clone.
    if (!realctp) {
      int ret = Linux_context_clone(ifc, &realctp, 0);
      if (ret) return ret;
      realctp->globalcno = cno;
      LSE_emu_update_context_map(cno, realctp);
    }

    LSE_env::env_t tempenv((const char **)envp);
    tempenv.setenv(*realctp->di->evars, true);
    char **newenvp = tempenv.getenvbuf();

    rval = Linux_context_load(realctp, argc, argv, newenvp);
    if (rval) fprintf(stderr,
		      "LSE_PowerPC Linux emulator:Unable to read executable "
		      "\"%s\", errno was %d\n",
		      argv[0],-rval);
    if (newenvp) ::free(newenvp);
    return rval;
  }

  // ***************** Dumping and hashing of state ******************

  void PPC_dump_state(LSE_emu_ctoken_t ct, FILE *outfile, bool dofp) {

    LSE_emu_isacontext_t& ctx = *(static_cast < LSE_emu_isacontext_t * >(ct));

    fprintf(outfile, "cr:0x%-8" PRIx32, ctx.cr);
    fprintf(outfile, " lr:0x%-8" PRIx32, ctx.lr);
    fprintf(outfile, " xer:0x%-8" PRIx32, ctx.xer);
    fprintf(outfile, " ctr:0x%-8" PRIx32 "\n", ctx.ctr);

    for (int i=0; i<32/4;i++) {
      fprintf(outfile,"r%-2d 0x%16" PRIx64 " ! ",
	      i, ctx.r[i]);
      fprintf(outfile,"r%-2d 0x%16" PRIx64 " ! ",
	      i + 32/4, ctx.r[i+32/4]);
      fprintf(outfile,"r%-2d 0x%16" PRIx64 " ! ",
	      i + 2*32/4, ctx.r[i+2*32/4]);
      fprintf(outfile,"r%-2d 0x%16" PRIx64 "\n",
	      i + 3*32/4, ctx.r[i+3*32/4]);
    }
    fprintf(outfile,"--------------------------------"
	    "--------------------------------\n");

    if (dofp) {
      for (int i=0; i<32/4;i++) {
	fprintf(outfile,"f%-2d %16g ! ",
		i, *(double *)(&ctx.f[i]));
	fprintf(outfile,"f%-2d %16g ! ",
		i + 32/4, *(double *)&ctx.f[i+32/4]);
	fprintf(outfile,"f%-2d %32g ! ",
		i + 2*32/4, *(double *)&ctx.f[i+2*32/4]);
	fprintf(outfile,"f%-2d %32g\n",
		i + 3*32/4, *(double *)&ctx.f[i+3*32/4]);
      } 
     
      fprintf(outfile,"--------------------------------"
	      "--------------------------------\n");
    }

  } // SPARC_dump_state

  // *************** Operand information ***************************

  boolean
  EMU_spaceaddr_is_constant(LSE_emu_ctoken_t ctx,
			    LSE_emu_spaceid_t sid,
			    LSE_emu_spaceaddr_t *addr)
  {
    return ((sid == 0)&&(addr->LSE==1));
  }

  // ******************* Handle command-line stuff **************

  int
  EMU_parse_arg(LSE_emu_interface_t *ifc,
		int argc, char *arg, char *argv[])
  {
    PPC_dinst_t *di = static_cast<PPC_dinst_t *>(ifc->etoken);

    if (!strcmp(arg,"debugload")) di->EMU_debugcontextload = true;
    else if (!strcmp(arg,"tracesyscalls")) di->EMU_trace_syscalls = true;
    else if (!strncmp(arg,"env:",4)) {
      di->evars->setenv(arg+4, true);
    }
    else if (!strcmp(arg,"cleanenv")) {
      di->evars->clearenv();
    }
    else return di->parse_arg(argc, arg, argv);
    return 1;
  }
  
  void
  EMU_print_usage(LSE_emu_interface_t *ifc)
  {
    PPC_dinst_t *di = static_cast<PPC_dinst_t *>(ifc->etoken);
    fprintf(stderr,
	    "\tdebugload\t\tPrint debug messages when loading\n");
    fprintf(stderr,
            "\ttracesyscalls\t\tTrace system calls\n");
    di->print_usage();
    return;
  }

  /********************** operand printing *************************/

  static void printbits(FILE *fp, int lower, int upper, uint64_t value)
  {
    int i;
    for (i=upper;i>=lower;i--)
      {
	fprintf(fp,"%i",(unsigned int)((value>>i)&1));
      }
  }

  void PPC_print_instr_oper_vals(FILE *fp, const char *prefix,
				 LSE_emu_instr_info_t *ii) {
#if (LSE_emu_emulator_has_operandval)
    int i;
    fprintf(fp,"%s  SRC: ",prefix);
    for (i=0;i<LSE_emu_max_operand_src;i++)
      {
	switch(ii->operand_src[i].spaceid) 
	  {
	  case LSE_emu_spaceid_mem:
	    fprintf(fp,"[0x%016"PRIx64"] ",
		    (uint64_t)ii->operand_src[i].spaceaddr.mem);
	    break;
	  case LSE_emu_spaceid_GR:
	    fprintf(fp,"r%2i=0x%016"PRIx64" ",
		    ii->operand_src[i].spaceaddr.GR,
		    ii->operand_val_src[i].data.rval);
	    break;
	  case LSE_emu_spaceid_FPR:
	    fprintf(fp,"f%2i=0x%016"PRIx64" ",
		    ii->operand_src[i].spaceaddr.FPR,
		    ii->operand_val_src[i].data.fval);
	    break;
	  case LSE_emu_spaceid_SPR:
	    switch(ii->operand_src[i].spaceaddr.SPR)
	      {
	      case PPC_SPR_CTR:
		fprintf(fp,"ctr");
		break;
	      case PPC_SPR_XER:
		fprintf(fp,"xer");
		break;
	      case PPC_SPR_LR:
		fprintf(fp,"lr");
		break;
	      default: break;
	      }
	    fprintf(fp,"=0x%016"PRIx64" ",ii->operand_val_src[i].data.rval);
	    break;
	  case LSE_emu_spaceid_OUR:
	    switch(ii->operand_src[i].spaceaddr.OUR)
	      {
	      case PPC_OUR_CR:
		if (ii->operand_src[i].uses.reg.bits[0] == 0xffffffff)
		  fprintf(fp,"cr");
		else { // figure out which cr
		  //fprintf(fp, "%llx %llx", ii->operand_src[i].uses.reg.bits[0],
		  //	  ii->operand_val_src[i].data.rval);
		  for (int j = 0; j < 8; ++j) {
		    if (ii->operand_src[i].uses.reg.bits[0] & 
			(0xf << (28 - j*4))) {
		      fprintf(fp,"cr%i=",j);
		      printbits(fp, 0, 3, ii->operand_val_src[i].data.cval);
		      fprintf(fp, " ");
		    }
		  }
		  goto donesrc;
		}
		break;
	      case PPC_OUR_FPSCR:
		fprintf(fp,"fpscr");
		break;
	      default: break;
	      }
	    fprintf(fp,"=0x%016"PRIx64" ",ii->operand_val_src[i].data.cval);
	    break;
	  case LSE_emu_spaceid_RSRV:
	    fprintf(fp,"rsrv=0x%016"PRIx64" ",ii->operand_val_src[i].data.bval);
	    break;
	  default:
	    break;
	  }
      donesrc: ;
      }
    fprintf(fp,"\n%s DEST: ",prefix);
    for (i=0;i<LSE_emu_max_operand_dest;i++)
      {
	switch(ii->operand_dest[i].spaceid) 
	  {
	  case LSE_emu_spaceid_mem:
	    fprintf(fp,"[0x%016"PRIx64"] ",
		    (uint64_t)ii->operand_dest[i].spaceaddr.mem);
	    break;
	  case LSE_emu_spaceid_GR:
	    fprintf(fp,"r%2i=0x%016"PRIx64" ",
		    ii->operand_dest[i].spaceaddr.GR,
		    ii->operand_val_dest[i].data.rval);
	    break;
	  case LSE_emu_spaceid_FPR:
	    fprintf(fp,"f%2i=0x%016"PRIx64" ",
		    ii->operand_dest[i].spaceaddr.FPR,
		    ii->operand_val_dest[i].data.fval);
	    break;
	  case LSE_emu_spaceid_SPR:
	    switch(ii->operand_dest[i].spaceaddr.SPR)
	      {
	      case PPC_SPR_CTR:
		fprintf(fp,"ctr");
		break;
	      case PPC_SPR_XER:
		fprintf(fp,"xer");
		break;
	      case PPC_SPR_LR:
		fprintf(fp,"lr");
		break;
	      default: break;
	      }
	    fprintf(fp,"=0x%016"PRIx64" ",ii->operand_val_dest[i].data.rval);
	    break;
	  case LSE_emu_spaceid_OUR:
	    switch(ii->operand_dest[i].spaceaddr.OUR)
	      {
	      case PPC_OUR_CR:
		if (ii->operand_dest[i].uses.reg.bits[0] == 0xffffffff)
		  fprintf(fp,"cr");
		else { // figure out which cr
		  // fprintf(fp, "%llx %llx", ii->operand_dest[i].uses.reg.bits[0],
		  //	  ii->operand_val_dest[i].data.rval);
		  for (int j = 0; j < 8; ++j) {
		    if (ii->operand_dest[i].uses.reg.bits[0] & 
			(0xf << (28 - j*4))) {
		      fprintf(fp,"cr%i=",j);
		      printbits(fp,0, 3, ii->operand_val_dest[i].data.cval);
		      fprintf(fp, " ");
		    }
		  }
		  goto donedest;
		}
		break;
	      case PPC_OUR_FPSCR:
		fprintf(fp,"fpscr");
		break;
	      default: break;
	      }
	    fprintf(fp,"=0x%016"PRIx64" ",ii->operand_val_dest[i].data.cval);
	    break;
	  case LSE_emu_spaceid_RSRV:
	    fprintf(fp,"rsrv=0x%016"PRIx64" ",ii->operand_val_dest[i].data);
	    break;
	  default:
	    break;
	  }
      donedest: ;
      }

    fprintf(fp,"\n");
#endif
  }  // PPC_print_instr_oper_vals

  void EMU_space_read(LSE_emu_spacedata_t *sdata,
		      LSE_emu_ctoken_t ctoken,
		      LSE_emu_spaceid_t sid, LSE_emu_spaceaddr_t *saddr,
		      int flags) {
    LSE_emu_isacontext_t *realct = ((LSE_emu_isacontext_t *)ctoken);

    switch (sid) {
    case LSE_emu_spaceid_GR:
      sdata->GR = realct->r[saddr->GR];
      break;
    case LSE_emu_spaceid_FPR:
      sdata->FPR = realct->f[saddr->FPR];
      break;
    case LSE_emu_spaceid_OUR:
      switch (saddr->OUR) {
      case 0 : sdata->OUR = realct->fpscr; break;
      case 1 : sdata->OUR = realct->cr; break;
      default : break;
      }
      break;
    case LSE_emu_spaceid_SPR:
      switch (saddr->SPR) {
      case 1 : sdata->SPR = realct->xer; break;
      case 8 : sdata->SPR = realct->lr; break;
      case 9 : sdata->SPR = realct->ctr; break;
      default : break;
      }
      break;
    case LSE_emu_spaceid_RSRV:
      //sdata->RSRV = realct->reservationAddr;
      break;
    case LSE_emu_spaceid_mem:
      { 
	uint64_t len2=flags;
#ifdef LATER
	VMem_read(realct->mem,saddr->mem,len2, (unsigned char *)&sdata->mem[0]);
#endif
      }
      break;
    default:
      break;
    } /* switch (sid) */
  } // EMU_space_read

  void EMU_space_write(LSE_emu_ctoken_t ctoken,
		       LSE_emu_spaceid_t sid, LSE_emu_spaceaddr_t *saddr,
		       LSE_emu_spacedata_t *sdata,
		       int flags) {
    LSE_emu_isacontext_t *realct = ((LSE_emu_isacontext_t *)ctoken);

    switch (sid) {
    case LSE_emu_spaceid_GR:
      realct->r[saddr->GR] = sdata->GR;
      break;
    case LSE_emu_spaceid_FPR:
      realct->f[saddr->FPR] = sdata->FPR;
      break;
    case LSE_emu_spaceid_OUR:
      switch (saddr->OUR) {
      case 0 : realct->fpscr = sdata->OUR; break;
      case 1 : realct->cr = sdata->OUR; break;
      default:
	break;
      }
      break;
    case LSE_emu_spaceid_SPR:
      switch (saddr->SPR) {
      case 1 : realct->xer = sdata->SPR; break;
      case 8 : realct->lr = sdata->SPR; break;
      case 9 : realct->ctr = sdata->SPR; break;
      default:
	break;
      }
      break;
    case LSE_emu_spaceid_RSRV:
      //realct->reservationAddr = sdata->RSRV;
      break;
    case LSE_emu_spaceid_mem:
      { 
	uint64_t len2=flags;
#ifdef LATER
	VMem_write(realct->mem,saddr->mem,len2, (unsigned char *)&sdata->mem[0]);
#endif
      }
      break;
    default:
      break;
    } /* switch (sid) */

  } // EMU_space_write
	   

  /*********** Reservation handling I don't want inlined ***********/

  void resv_list_t::drop_context(struct PPC_context_s *cp) {
    for (std::list<resv_t>::iterator i = resvs.begin(), 
	   ie = resvs.end(); i != ie ; ++i)
      if (i->second == cp) {
	resvs.erase(i);
	return;
      }
    refcnt --;
    if (!refcnt) delete this;
  }
    
  void resv_list_t::add_reservation(struct PPC_context_s *cp, addr_t a) {
    for (std::list<resv_t>::iterator i = resvs.begin(), 
	   ie = resvs.end(); i != ie ; ++i)
      if (i->second == cp) {
	i->first = a;
	return;
      }
    resvs.push_back(resv_t(a,cp));
  }

  bool resv_list_t::check_reservation(struct PPC_context_s *cp, addr_t a) {
    for (std::list<resv_t>::iterator i = resvs.begin(), 
	   ie = resvs.end(); i != ie ; ++i)
      if (i->second == cp && i->first == a) return true;
    return false;
  }

  void resv_list_t::clear_reservations(struct PPC_context_s *cp, addr_t a, 
				       int size) {
    for (std::list<resv_t>::iterator i = resvs.begin(), 
	   ie = resvs.end(); i != ie ; )
      if (i->second != cp && 
	  ! (a + size <= i->first || i->first + 3 <= a) )
	i = resvs.erase(i);
      else ++i;
  }

  bool reservation_handler_t<1>::check_reservation(struct PPC_context_s *cp, 
						   addr_t a, int size) { 
      regR_t nv = 0;
      if (!rpresent || a != raddr) return false;
#ifdef LSE_BIGENDIAN
      cp->mem->read(a, reinterpret_cast<unsigned char *>(&nv) + (8-size), size);
#else
      cp->mem->read(a, reinterpret_cast<unsigned char *>(&nv), size);
#endif
      return rdata == (size == 8 ? LSE_b2h(nv) : 
		       uint64_t(LSE_b2h(uint32_t(nv))));
  }

  /************************* Fault handling ******************/

  bool EMU_handle_fault(LSE_emu_ctoken_t ctoken, PPC_fault_t f,
			LSE_emu_addr_t pc, LSE_emu_addr_t &fpc) {
    LSE_emu_isacontext_t *realct = ((LSE_emu_isacontext_t *)ctoken);
    if (f == gdb_breakpoint_entry) return true;
    return Linux_handle_fault(realct, f, pc, fpc);
  }

  /********************** context refcounting ****************/


} // namespace LSE_PowerPC

