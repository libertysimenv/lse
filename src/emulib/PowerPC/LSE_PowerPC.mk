include domain_info.mk
include $(TOPSRCINCDIR)/Make_include.mk

lib: libLSE_PowerPC.a

headers: ISA.h

include $(DOMNAME).inc.mk
LIS_OBJFILES = $(LIS_SRCFILES:.cc=.o)
FIXIT='s/LSE_PowerPC/$(DOMNAME)/g'

libLSE_PowerPC_OBJS=bookkeeping.o PowerPC_Linux.o checkpointing.o \
			$(LIS_OBJFILES)

libLSE_PowerPC.a: $(libLSE_PowerPC_OBJS)
	ar r $@ $(libLSE_PowerPC_OBJS)

# this is the file which domnameP.h expects to see
# make the assumed namespace remap to the proper one
ISA.h: $(DOMNAME).dsc
	ls-make-domain-header --impl --chain --instname=$(DOMNAME) --protect=_$(DOMNAME)_H LSE_emu $(DOMNAME) > $@

checkpointing.cc bookkeeping.cc PowerPC_FP.h PowerPC_Linux.cc: renamed
	sed $(FIXIT) $@ > $@.tmp 
	mv -f $@.tmp $@

checkpointing.o bookkeeping.o PowerPC_Linux.o: ISA.h

$(LIS_OBJFILES): ISA.h

renamed:
	touch renamed

clean: 
	rm -f ISA.h $(lib_OBJS) libLSE_PowerPC.a $(LIS_SRCFILES) \
		LSE_PowerPC.dsc LSE_PowerPC.mk

