/*
 * Copyright (c) 2010-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its doumentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LLVM binary translation support header
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * Declarations for LLVM binary translation support
 *
 */

#ifndef __LSE_LLVMBT_SUPPORT_H_
#define __LSE_LLVMBT_SUPPORT_H_
#include <map>
#include <string>
#include <vector>
#if (@foundudis86@)
#define LSE_LLVMBT_DO_UDIS86
#include <udis86.h>
#endif

// Yes, this is hackish, but prevents us from having to do bunches
// of indirection to keep LLVM headers out of LSE compilation (which
// leads to compilation speed issues)
namespace llvm {
  class Module;
  class FunctionPassManager;
  class ExecutionEngine;
  class CallInst;
  class Type;
  class Function;
  class Value;
}

namespace LSE_llvmbt_support {

  void LIS_start_code();
  void LIS_end_code();

  // This data structure represents the code in a single bitcode file
  // which will correspond to a single implementation style.  Doing 
  // things this way allows us to simply use different modules for
  // different styles ... a bit wasteful of space, but it shouldn't be 
  // too bad.

  class BTmanagerGroup {
    llvm::Module *module;
    llvm::FunctionPassManager *funcPassMgr;
    llvm::ExecutionEngine *EE;
    std::string ErrorMsg;
    std::map<void *, llvm::Function *> funcMap;    
    std::vector<llvm::Function *> generated;
    void init();
#ifdef LSE_LLVMBT_DO_UDIS86
    ud_t ud_obj;
#endif
  public:
    BTmanagerGroup(const std::string &bcFile);
    BTmanagerGroup(const char *start, const char *end);
    ~BTmanagerGroup();
    void resetCode();
    void setDebugFlag(bool);
    void setDebugType(const char *);
    friend class BTmanager;
  };

  class BTmanager {
    class BTmanagerGroup *MG;

    llvm::Function *currFunc;
    llvm::Function *funcTemplate, *callTemplate;
    llvm::Function *startCode, *endCode;
    llvm::CallInst *IP, *firstIP;

    llvm::Function *returnFunc;
    std::vector<llvm::Function *> specializers;
    std::string fname;

    void init(const std::string &f, void *ft, void *rf,
	      void *[], unsigned);
  public:
    BTmanager(const std::string &, const std::string &,
	      void *, void *, void *[], unsigned);
    BTmanager(const char *, const char *, const std::string &,
	      void *, void *, void *[], unsigned);
    BTmanager(BTmanagerGroup *, const std::string &,
	      void *, void *, void *[], unsigned);
    ~BTmanager();

    llvm::Function *findFunction(void *);
    llvm::Function *currFunction();
    const llvm::Type *specializerType(int ind);

    void startCodeBuffer();
    llvm::CallInst *addCall(llvm::Value *[], unsigned);
    void *finishCodeBuffer(llvm::Value *[], unsigned, unsigned &);
    void removeCodeBuffer(void *);
  };

} // namespace LSE_llvmbt_support

#endif /* __LSE_LLVMBT_SUPPORT_H_ */
