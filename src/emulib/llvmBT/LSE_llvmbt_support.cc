/*
 * Copyright (c) 2010-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its doumentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LLVM binary translation support
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 */
#define __STDC_LIMIT_MACROS 1
#define __STDC_CONSTANT_MACROS 1
#define DEBUG_TYPE "binarytrans"
#undef NDEBUG
#include "llvm/LLVMContext.h"
#include "llvm/Module.h"
#include "llvm/PassManager.h"
#include "llvm/Analysis/Dominators.h"
#include "llvm/CodeGen/MachineCodeInfo.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/ExecutionEngine/JITEventListener.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/IRBuilder.h"
#include "llvm/Support/IRReader.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/StandardPasses.h"
#include "llvm/Support/DynamicLibrary.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/Target/TargetSelect.h"
#include "llvm/Transforms/Utils/BasicInliner.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/FunctionUtils.h"
#include "llvm/Support/LeakDetector.h"
#include <deque>
#include <set>
#include <vector>

#include "LSE_llvmbt_support.h"

using namespace llvm;

namespace LSE_llvmbt_support {

  // standard functions ... do not want them in the bitcode file!
  void LIS_start_code() {}
  void LIS_end_code() {}

  namespace {

    // helper to find visible code.  A bit of oddness is that we need
    // to keep them in visit order, so we use a vector for that and 
    // keep a set to see who was visited.  Another way would have involved
    // keeping visit order indices and then sorting, but that is no better.
    void DFSVisit(std::vector<BasicBlock *>& DFSBlocks, 
		  std::set<BasicBlock *>& DFSSet,
		  BasicBlock *curr,
		  BasicBlock *end) {
      if (curr == end) return;            // reached end code's block

      if (DFSSet.count(curr)) return;  // looped back
      DFSSet.insert(curr);
      DFSBlocks.push_back(curr);

      const TerminatorInst *TI = curr->getTerminator();
      if (!TI) return;   // no terminator
      for (int s = 0, se = TI->getNumSuccessors() ; s != se ; ++s)
	DFSVisit(DFSBlocks, DFSSet, TI->getSuccessor(s), end);
    }

  } // anonymous namespace

  void BTmanager::init(const std::string &f, void *ft, void *rf,
		       void *specs[], unsigned specsSize) {

    funcTemplate = MG->funcMap[ft];
    callTemplate = 0;

    if (!funcTemplate) {
      errs() << "BTmanager(): funcTemplate = NULL" << "\n";
      abort();
    }

    returnFunc = MG->funcMap[rf];
    if (!returnFunc) {
      errs() << "BTmanager(): returnFunc = NULL" << "\n";
      abort();
    }

    startCode = MG->funcMap[(void *)LIS_start_code];
    if (!startCode) {
      errs() << "BTmanager(): startCode = NULL" << "\n";
      abort();
    }
    endCode = MG->funcMap[(void *)LIS_end_code];
    if (!startCode) {
      errs() << "BTmanager(): endCode = NULL" << "\n";
      abort();
    }

    for (unsigned I = 0, IE = specsSize; I != IE ; ++I) {
      Function *nf = MG->funcMap[specs[I]];
      if (!nf) {
	errs() << "BTmanager(): unable to find specializer" << "\n";
	abort();
      }
      specializers.push_back(nf);
    }

    // DEBUG(dbgs() << *funcTemplate);

    // Now, split up the entry by putting everything between code start
    // and code end into a new function and remembering this function.

    // split basic block at startCode and endCode.
    BasicBlock *StartB, *EndB;

    // Assumption is that there will be one start and one end only.
    for (int j = 0; j < 2; j++) {
      for (inst_iterator 
	     I = inst_begin(funcTemplate), IE = inst_end(funcTemplate);
	   I != IE ; ++I) {

	if (dyn_cast<CallInst>(&*I) && I->getOperand(0) == startCode) {
	  StartB = I->getParent()->splitBasicBlock(&*I);
	  I->eraseFromParent();
	  break;
	}
	if (dyn_cast<CallInst>(&*I) && I->getOperand(0) == endCode) {
	  EndB = I->getParent();
	  EndB->splitBasicBlock(&*I);
	  I->eraseFromParent();
	  break;
	}
      }
    }

    if (!StartB) {
      errs() << "BTmanager(): startCode not found\n";
      abort();
    }
    if (!EndB) {
      errs() << "BTmanager(): endCode not found\n";
      abort();
    }

    // DEBUG(dbgs() << "START:" << *StartB);
    // DEBUG(dbgs() << "END:" << *EndB);

    std::vector<BasicBlock *> DFSBlocks;
    std::set<BasicBlock *> DFSset;
    if (StartB == EndB) {
      DFSBlocks.push_back(StartB);
      // DFSBset.insert(DFSSet);  // not needed
    } else DFSVisit(DFSBlocks, DFSset, StartB, EndB);

    // DEBUG(dbgs() << *funcTemplate);

    DominatorTree DT;
    callTemplate = ExtractCodeRegion(DT, DFSBlocks, false);

    if (!callTemplate) {
      errs() << "BTmanager(): callTemplate could not be formed" << "\n";
      abort();
    }

    // DEBUG(dbgs() << *funcTemplate);
    // DEBUG(dbgs() << *callTemplate);
  }

  BTmanagerGroup::BTmanagerGroup(const char *start, const char *end) {
#ifndef NDEBUG
    llvm::SetCurrentDebugType("binarytrans");
#endif
    llvm::JITEmitDebugInfo = false;
    //llvm::PrintMachineCode = true;
    InitializeNativeTarget();
    
    if (MemoryBuffer *Buffer 
	= MemoryBuffer::getMemBuffer(StringRef(start,end-start))) {
      //module = getLazyBitcodeModule(Buffer, Context, &ErrorMsg);
      module = ParseBitcodeFile(Buffer, getGlobalContext(), &ErrorMsg);
      if (!module) {
	delete Buffer;
	errs() << "Error loading bitcode array: " 
	       << ErrorMsg << "\n";
	exit(1);
      }
      delete Buffer;
    }
    init();
  }

  BTmanagerGroup::BTmanagerGroup(const std::string &bcFile) {
#ifndef NDEBUG
    llvm::SetCurrentDebugType("binarytrans");
#endif
    llvm::JITEmitDebugInfo = false;
    InitializeNativeTarget();
    
    {
      OwningPtr<MemoryBuffer> BufferPtr;
      if (error_code ec = MemoryBuffer::getFileOrSTDIN(bcFile, BufferPtr)){
	ErrorMsg = ec.message();
	module = 0;
      } else module = ParseBitcodeFile(BufferPtr.get(), 
				       getGlobalContext(), &ErrorMsg);
      if (!module) {
	errs() << "Error loading bitcode file '" << bcFile << "': "
	       << ErrorMsg << "\n";
	exit(1);
      }
    }
    init();
  }

  void BTmanagerGroup::setDebugFlag(bool flag) {
#ifndef NDEBUG
    llvm::DebugFlag = flag;
#endif
  }

  void BTmanagerGroup::setDebugType(const char *s) {
#ifndef NDEBUG
    llvm::SetCurrentDebugType(s);
#endif
  }

  void BTmanagerGroup::init() {
    //llvm::DwarfExceptionHandling = true;
    llvm::JITExceptionHandling = true;

    EngineBuilder builder(module);
    builder.setEngineKind(EngineKind::JIT);
    builder.setErrorStr(&ErrorMsg);
    builder.setOptLevel(CodeGenOpt::Default);
    builder.setAllocateGVsWithCode(false);

    EE = builder.create();
    if (!EE) {
      errs() << "Error creating EE: " << ErrorMsg << "\n";
    }

    //EE->RegisterJITEventListener(createOProfileJITEventListener());

    for (Module::global_iterator I = module->global_begin(), 
	   E = module->global_end();
	 I != E; ++I) {

      // if it has a definition, but is in the JIT, then don't 
      // allow it to be generated.

      if (!I->isDeclaration()) { 
	if (void *Addr = sys::DynamicLibrary::SearchForAddressOfSymbol(I->getName())) {
	  GlobalValue &V = *I;
	  //EE->addGlobalMapping(&V,Addr);
	  //errs() << *I << "\n";
	}
      }
    }

    for (Module::iterator I = module->begin(), E = module->end(); I != E; ++I) {

      // if it has a definition, but is in the JIT, then don't 
      // allow it to be generated.

      if (true || !I->isDeclaration()) { 
	if (void *Addr = sys::DynamicLibrary::SearchForAddressOfSymbol(I->getName())) {
	  Function &V = *I;
	  funcMap[Addr] = &V;
	  EE->addGlobalMapping(&V,Addr);
	  //errs() << *I << "\n";
	}
      }
    }
    
    EE->DisableLazyCompilation(false);

    // Now start up the translator passes.  TODO: reduce them if possible.

    funcPassMgr = new FunctionPassManager(module);
    if (!funcPassMgr) {
      errs() << "initializeBT(): funcPassMgr = NULL" << "\n";
      exit(1);
    }

    funcPassMgr->add(new TargetData(*EE->getTargetData()));
    funcPassMgr->add(createBasicAliasAnalysisPass());

    // Set up the optimizer pipeline.  Start with registering info about how the
    // target lays out data structures.

    funcPassMgr->add(createScalarReplAggregatesPass());  // Break up aggregate allocas
    //funcPassMgr->add(createSimplifyLibCallsPass());    // Library Call Optimizations
    funcPassMgr->add(createInstructionCombiningPass());  // Cleanup for scalarrepl.
    funcPassMgr->add(createJumpThreadingPass());         // Thread jumps.
    funcPassMgr->add(createCFGSimplificationPass());     // Merge & remove BBs
    funcPassMgr->add(createInstructionCombiningPass());  // Combine silly seq's
    
    //funcPassMgr->add(createTailCallEliminationPass());   // Eliminate tail calls
    funcPassMgr->add(createCFGSimplificationPass());     // Merge & remove BBs
    funcPassMgr->add(createReassociatePass());           // Reassociate expressions
    //funcPassMgr->add(createLoopRotatePass());            // Rotate Loop
    //funcPassMgr->add(createLICMPass());                  // Hoist loop invariants
    //funcPassMgr->add(createLoopUnswitchPass(OptimizeSize || OptimizationLevel < 3));
    //funcPassMgr->add(createInstructionCombiningPass());  
    //funcPassMgr->add(createIndVarSimplifyPass());        // Canonicalize indvars
    //funcPassMgr->add(createLoopDeletionPass());          // Delete dead loops
    //funcPassMgr->add(createLoopUnrollPass());          // Unroll small loops
    //funcPassMgr->add(createInstructionCombiningPass());  // Clean up after the unroller
    funcPassMgr->add(createGVNPass());                 // Remove redundancies
    //funcPassMgr->add(createMemCpyOptPass());             // Remove memcpy / form memset
    //funcPassMgr->add(createSCCPPass());                  // Constant prop with SCCP
  
    // Run instcombine after redundancy elimination to exploit opportunities
    // opened up by them.
    funcPassMgr->add(createInstructionCombiningPass());
    funcPassMgr->add(createJumpThreadingPass());         // Thread jumps
    funcPassMgr->add(createDeadStoreEliminationPass());  // Delete dead stores
    //funcPassMgr->add(createAggressiveDCEPass());         // Delete dead instructions
    funcPassMgr->add(createCFGSimplificationPass());     // Merge & remove BBs
    funcPassMgr->add(createAggressiveDCEPass());         // Delete dead instructions

    funcPassMgr->doInitialization();
#ifdef LSE_LLVMBT_DO_UDIS86
    ud_init(&ud_obj);
    ud_set_mode(&ud_obj, 64);
    ud_set_syntax(&ud_obj, UD_SYN_ATT);
#endif
  }

  BTmanagerGroup::~BTmanagerGroup() {
    if (funcPassMgr) delete funcPassMgr;
    if (EE) delete EE;
    // NOTE: ExecutionEngine takes ownership of the module, so deleting 
    // EE deletes the module attached to it.
    funcPassMgr = 0;
    EE = 0;
  }

  BTmanager::BTmanager(const std::string &bcFile, 
		       const std::string &f, void *ft, void *rf,
		       void *specs[], unsigned specsSize) : fname(f) { 
    MG = new BTmanagerGroup(bcFile);
    init(f, ft, rf, specs, specsSize);
  }

  BTmanager::BTmanager(const char *start, const char *end, 
		       const std::string &f, void *ft, void *rf,
		       void *specs[], unsigned specsSize) : fname(f) { 
    MG = new BTmanagerGroup(start, end);
    init(f, ft, rf, specs, specsSize);
  }

  BTmanager::BTmanager(BTmanagerGroup *g,
		       const std::string &f, void *ft, void *rf,
		       void *specs[], unsigned specsSize) : fname(f) { 
    MG = g;
    init(f, ft, rf, specs, specsSize);
  }

  BTmanager::~BTmanager() { 
    //delete MG; 
    // llvm_shutdown();
  }

  Function *BTmanager::findFunction(void *selectedFunc) {
    return MG->funcMap[selectedFunc];
  }

  Function *BTmanager::currFunction() { return currFunc; }

  const llvm::Type *BTmanager::specializerType(int ind) {
    return specializers[ind]->getReturnType();
  }

  void BTmanager::startCodeBuffer() {

    Function *func = funcTemplate;

    ValueToValueMapTy VM;

    // NOTE: a simple CloneFunction doesn't work because it won't generate
    // a new name for the function
    currFunc = Function::Create(func->getFunctionType(),
				Function::ExternalLinkage, "myfunc",
				MG->module);

    Function::arg_iterator DestI = currFunc->arg_begin();
    for (Function::const_arg_iterator I = func->arg_begin(), 
	   E = func->arg_end(); I != E; ++I)
      if (VM.count(I) == 0) {   // Is this argument preserved?
	DestI->setName(I->getName()); // Copy the name over...
	VM[I] = DestI++;        // Add mapping to ValueMap
      }
    SmallVector<ReturnInst*, 8> Returns;
    CloneFunctionInto(currFunc, func, VM, true, Returns, "", 0);

    //DEBUG(dbgs() << *currFunc;);
    firstIP = 0;
    for (inst_iterator i = inst_begin(currFunc), 
	   e = inst_end(currFunc); i != e; ++i) {
      if (dyn_cast<CallInst>(&*i) && 
	  dyn_cast<CallInst>(&*i)->getCalledFunction() == callTemplate) {
	firstIP = IP = dyn_cast<CallInst>(&*i);
	break;
      }
    }
    if (!firstIP) {
      errs() << "startCodeBuffer(): Unable to set IP" << "\n";
      exit(1);
    }
  }

  CallInst *BTmanager::addCall(Value *args[], unsigned argsSize) {

    std::vector<Value *> templateArgs;

    unsigned numargs = firstIP->getNumArgOperands();
    for (unsigned i = 0; i < numargs ; ++i) {
      Value *AV = firstIP->getArgOperand(i);
      CallInst *CI = dyn_cast<CallInst>(AV);

      bool found = false;

      if (CI) {
	Function *CF = CI->getCalledFunction();

	if (CF) {
	  for (unsigned j = 0, je = argsSize; j != je ; ++j) {
	    if (specializers[j] == CI->getCalledFunction()) {
	      templateArgs.push_back(args[j]);
	      found = true;
	      break;
	    }
	  }
	}
      }
      
      if (!found) templateArgs.push_back(AV);
    }

    CallInst *nc =  CallInst::Create(callTemplate, templateArgs.begin(), 
				     templateArgs.end(),"");
    nc->insertAfter(IP);
    return (IP = nc);
  }

  class MCIListener : public JITEventListener {
    Function &F;
  public:
    void *address;
    size_t size;
    MCIListener(Function &f) : F(f), JITEventListener() {}
    void NotifyFunctionEmitted(const Function &FN,
			       void *Code, size_t Size,
			       const EmittedFunctionDetails &) {
      if (&F==&FN) {
	address = Code;
	size = Size;
      }
    }
  };

  namespace {
    struct wtd {
      CallInst *I;
      Value *V;
      wtd(CallInst *i, Value *v) : I(i), V(v) {}
    };
  } // anonymous namespace

  void *BTmanager::finishCodeBuffer(Value *args[], unsigned argsSize,
				    unsigned &sizep) {

    firstIP->dropAllReferences();
    firstIP->eraseFromParent();

    // replace all uses of the specializers.

    std::vector<wtd> workList;

    for (inst_iterator I = inst_begin(currFunc),
	   E = inst_end(currFunc); I != E; ++I) {
      CallInst *c = dyn_cast<CallInst>(&*I);
      if (!c) continue;
      Function *CF = c->getCalledFunction();
      if (!CF) continue;
      for (unsigned j = 0, je = argsSize; j != je ; ++j) {
	if (specializers[j] == CF) {
	  workList.push_back(wtd(c,args[j]));
	  break;
	}
      }
    }

    for (std::vector<wtd>::iterator i = workList.begin(), 
	   e = workList.end() ; i != e; ++i) {
      (i->I)->replaceAllUsesWith(i->V);
      (i->I)->eraseFromParent();
    }

    DEBUG(dbgs() << "Initial function generation: \n" << *currFunc;);

    {
      std::vector<CallInst *> workList;

      for (int j=0; j < 2 ; j++) {
	workList.clear();
	for (inst_iterator i = inst_begin(currFunc), 
	       e = inst_end(currFunc); 
	     i != e ; ++i) {
	  CallInst *c = dyn_cast<CallInst>(&*i);
	  if (c) workList.push_back(c);
	}
	for (std::vector<CallInst *>::iterator i = workList.begin(), 
	       e = workList.end() ; i != e; ++i) {
	  InlineFunctionInfo IFI(0, MG->EE->getTargetData());
	  bool ugh = InlineFunction(*i, IFI);
	  if (!ugh) {
	    Function *f = (*i)->getCalledFunction();
	    //      errs() << "ugh " << f << " " << f->isDeclaration() << " " 
	    //     << f->getFunctionType()->isVarArg() << "\n";
	  }
	}

	if (j == 0) {
	  workList.clear();
	  for (inst_iterator i = inst_begin(currFunc), 
		 e = inst_end(currFunc); 
	       i != e ; ++i) {
	    CallInst *c = dyn_cast<CallInst>(&*i);
	    if (c && c->getCalledFunction() == returnFunc) 
	      workList.push_back(c);
	  }

	  for (std::vector<CallInst *>::iterator i = workList.begin(), 
		 e = workList.end() ; i != e; ++i) {
	    CallInst *c = *i;
	    
	    // Now, the easiest thing to do would be to create a call
	    // instruction here and let the unreachable code after it
	    // be wiped out.  Unfortunately, LLVM doesn't do that quite
	    // right.  So instead, I have to split the basic block
	    // after the call, add the return to the previous code block
	    // and wipe out the branch instruction that split inserted.

	    BasicBlock *NB = c->getParent();
	    NB->splitBasicBlock(c);
	    TerminatorInst *ti = NB->getTerminator();
	    ReturnInst *ri = ReturnInst::Create(currFunc->getContext(),
						c->getArgOperand(0), NB);
	    ti->eraseFromParent();
	  }
	}      
      } // two passes
    }

    BasicInliner BI(const_cast<TargetData*>(MG->EE->getTargetData()));
    BI.addFunction(currFunc);
    BI.inlineFunctions();

    DEBUG(dbgs() << "After inlining: \n" << *currFunc;);

    MG->funcPassMgr->run(*currFunc);
    DEBUG(dbgs() << "After optimization passes: \n" << *currFunc;);

    MCIListener MCIL(*currFunc);
    MG->EE->RegisterJITEventListener(&MCIL);

    MG->EE->runJITOnFunction(currFunc, 0);
    void *code = MCIL.address;
    sizep = MCIL.size;

#ifdef LSE_LLVMBT_DO_UDIS86
    DEBUG({
	ud_set_input_buffer(&MG->ud_obj, 
			    (uint8_t *)MCIL.address, MCIL.size);
	ud_set_pc(&MG->ud_obj, (uint64_t)code);
	
	dbgs() << currFunc->getName() << ":\n";
	while (ud_disassemble(&MG->ud_obj)) {
	  dbgs() << "\t";
	  dbgs().write_hex(ud_insn_off(&MG->ud_obj));
	  dbgs() << ":\t" 
		 << ud_insn_asm(&MG->ud_obj) << "\n";
	}
      } );
#endif

    MG->EE->UnregisterJITEventListener(&MCIL);

    Value *falseV = ConstantInt::getFalse(currFunc->getContext());
    Value *trueV = ConstantInt::getTrue(currFunc->getContext());

    // Now, delete the contents of currFunc so I can release the constants
    // inside of it.  What a disaster!  Tricky part: do not delete 
    // true and false; they are not reference counted properly.

    SmallPtrSet<Constant *, 64> cworkList;
    for (inst_iterator I = inst_begin(currFunc), E = inst_end(currFunc); 
		I != E; ++I) {
      Instruction *pi = &*I;
      for (User::op_iterator i = (pi)->op_begin(), e = pi->op_end(); 
		i != e; ++i) {
  	Value *v = *i;
	if (isa<Constant>(v)) 
	  if (v != falseV && v != trueV)
	    cworkList.insert(dyn_cast<Constant>(v));
      }
    }
    currFunc->dropAllReferences();
    for (SmallPtrSet<Constant *, 64>::iterator i = cworkList.begin(),
	 e = cworkList.end(); i != e; ++i) {
	if ((*i)->use_empty()) (*i)->destroyConstant();
    }
     
    //llvm::LeakDetector::checkForGarbage(MG->module->getContext(),"reset");
    return code;

  } // finishCodeBuffer

  void BTmanager::removeCodeBuffer(void *llvmp) {
    Function *F = (Function *)llvmp;
    MG->EE->freeMachineCodeForFunction(F);
    F->eraseFromParent();
  }

}
