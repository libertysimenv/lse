/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * GDB support for the SPARC emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This is the GDB stub for the SPARC emulator.  It's a blast!
 *
 */
#include "LSE_SPARC.priv.h"
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
/* extern int inet_aton(const char *cp, struct in_addr * addr); */
#include <unistd.h>

#include <algorithm>
#include <string>
#include <sstream>

using namespace LSE_SPARC;

#include "emulib/LSE_emu_alonesupp.h"

namespace LSE_SPARC {
  
  //#define DEBUG_PACKETS
  //#define DEBUG_GDBSET
#define PACKET_SIZE 12000
  
  static FILE *debugfile;
  static char packetbuf[PACKET_SIZE+5]="$";
  static char sbuf[4] = "#00";
  
  static int fdgetchar(int fd) {
    char c;
    int rval;
    rval = read(fd,&c,1);
    if (rval < 0) {
      fprintf(stderr,"Error %d while reading from GDB socket\n",errno);
      exit(1);
    }
    return rval ? c : EOF;
  }

  static void sendpacket(int fd, const char *s) {
    int a = 0;
    char c;
    const char *p = s;
    
    while ((c=*(p++))) a = (a + c) % 256;
    
    sprintf(sbuf+1,"%02x",a);
    strcpy(packetbuf+1,s);
    strcat(packetbuf,sbuf);
    if (debugfile)
      fprintf(debugfile,"-> %-60.60s\n",packetbuf);
    write(fd,packetbuf,strlen(packetbuf));
  }

  static void sendpacket(int fd, std::ostringstream& s) {
    int a = 0;
    char c;
    const char *p = s.str().c_str();
    
    while ((c=*(p++))) a = (a + c) % 256;
    
    std::ostringstream np;
    np << '$' << s.str() << '#'
       << std::hex << std::setfill('0') << std::setw(2) << a;

    const char *c2 = np.str().c_str();
    if (debugfile)
      //fprintf(debugfile,"-> %-60.60s\n",c2);
      fprintf(debugfile,"-> %s\n",c2);
    write(fd,c2,strlen(c2));
  }

#ifdef NOMORE
  static char *printhex4(char *p, uint32_t val) {
    int j;
    for (j=0;j<4;j++) {
      sprintf(p,"%02x",(int)val&0xff);
      p+=2;
      val >>= 8;
    }
    return p;
  }

  static char *printhex8(char *p, uint64_t val) {
    int j;
    for (j=0;j<8;j++) {
      sprintf(p,"%02x",(int)val&0xff);
      p+=2;
      val >>= 8;
    }
    return p;
  }
#endif

  static int gethexint(char *p, int *valp) {
    int count = 0;
    int val = 0;
    while (*p && count<8) {
      if (*p >= '0' && *p <= '9') val = (val * 16) + *p - '0';
      else if (*p >= 'a' && *p <= 'f') val = (val * 16) + *p - 'a' + 10;
      else if (*p >= 'A' && *p <= 'F') val = (val * 16) + *p - 'A' + 10;
      else break;
      count ++;
      p++;
    }
    *valp = val;
    return count;
  }

  static int gethexbyte(char *p, char *valp) {
    int count = 0;
    unsigned char val = 0;
    while (*p && count<2) {
      if (*p >= '0' && *p <= '9') val = (val * 16) + *p - '0';
      else if (*p >= 'a' && *p <= 'f') val = (val * 16) + *p - 'a' + 10;
      else if (*p >= 'A' && *p <= 'F') val = (val * 16) + *p - 'A' + 10;
      else break;
      count ++;
      p++;
    }
    *valp = val;
    return count & 1 ? 0 : count; /* complain about odd bytes */
  }
  
#ifdef NOMORE
  static int gethex32(char *p, uint32_t *valp) {
    int count = 0;
    uint32_t val = 0;
    while (*p && count<8) { // assumes big-endianness
      if (*p >= '0' && *p <= '9') val = (val * 16) + *p - '0';
      else if (*p >= 'a' && *p <= 'f') val = (val * 16) + *p - 'a' + 10;
      else if (*p >= 'A' && *p <= 'F') val = (val * 16) + *p - 'A' + 10;
      else break;
      count ++;
      p++;
    }
    *valp = val;
    return count;
  }
#endif

  static int gethex64(char *p, uint64_t *valp) { // assumes big-endianness..
    int count = 0;
    uint64_t val = 0;
    while (*p && count<16) {
      if (*p >= '0' && *p <= '9') val = (val * 16) + *p - '0';
      else if (*p >= 'a' && *p <= 'f') val = (val * 16) + *p - 'a' + 10;
      else if (*p >= 'A' && *p <= 'F') val = (val * 16) + *p - 'A' + 10;
      else break;
      count ++;
      p++;
    }
    *valp = val;
    return count;
  }

  static int EMU_gdb_socket, EMU_gdb_conn;

  void EMU_gdb_init(int port) {
    struct sockaddr_in saddr;
    struct sockaddr caddr;
    socklen_t caddrs=sizeof(caddr);
    
    /* Get ourselves a connection */
    EMU_gdb_socket = socket(PF_INET, SOCK_STREAM,0);
    if (EMU_gdb_socket<0) {
      fprintf(stderr,"Unable to create GDB socket\n");
      exit(1);
    }
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    inet_aton("127.0.0.1",&saddr.sin_addr); /* loopback */
    if (bind(EMU_gdb_socket,(struct sockaddr *)&saddr,sizeof(saddr))) {
      fprintf(stderr,"bind() call for GDB socket failed\n");
      perror("");
      close(EMU_gdb_socket);
      exit(1);
    }
    if (listen(EMU_gdb_socket,1)) {
      fprintf(stderr,"listen() call for GDB socket failed\n");
      close(EMU_gdb_socket);
      exit(1);
    }
    EMU_gdb_conn = accept(EMU_gdb_socket,
			  (struct sockaddr *)&caddr, &caddrs);
    if (EMU_gdb_conn < 0) {
      fprintf(stderr,"accept() call for GDB socket failed with error %d\n",
	      errno);
      close(EMU_gdb_socket);
      exit(1);
    }
    
#ifdef DEBUG_PACKETS
    debugfile = stderr;
#else
    debugfile = NULL;
#endif
    
  }
  
  void EMU_gdb_start_context(LSE_emu_contextno_t swcno) {
#ifdef HMM
    LSE_emu_isacontext_t *ctx;
    ctx = (LSE_emu_isacontext_t *)LSE_emu_swcontexts_table[swcno].ctok;
#endif
  }

  void EMU_gdb_report_end() {
    char newpacket[7];
    sprintf(newpacket,"W%02x",LSE_sim_exit_status);
    sendpacket(EMU_gdb_conn,newpacket);
  }

  static void do_flush(LSE_emu_isacontext_t *ctx) {
  }
  static void undo_flush(LSE_emu_isacontext_t *ctx) {
  }


  /* return value indicates whether GDB has detached.... */
  int 
  EMU_gdb_enter(int signo, LSE_emu_contextno_t hwcno) {
    LSE_emu_isacontext_t *ctx;
    int c;
    int rval, stepping;
    char packet[PACKET_SIZE+1];
    char newpacket[PACKET_SIZE+1];
    int pcount;
    char *npp;
    int i;
    uint64_t val, addr;
    int len, lenmax;
    char *bufp;

    /* We are essentially in handle_exception now */

    ctx = (LSE_emu_isacontext_t *)LSE_emu_hwcontexts_table[hwcno].ctok;

    // flush general registers to the register stack
    do_flush(ctx);

    /* inform GDB of why we stopped */
    sprintf(newpacket,"S%02x",signo);
    sendpacket(EMU_gdb_conn,newpacket);
#ifdef DEBUG_GDBSET
    fprintf(stderr,"IP=%08llx\n",LSE_emu_addr_table[hwcno].PC);
#endif


    c = fdgetchar(EMU_gdb_conn);
    while (c>=0) { /* ends while loop at end of file */
      
      /* find packet start */
    
      if (debugfile) fputs("<- ",debugfile);
    
      while (c>=0 && c != '$' && c != '-') { 
	// I'm just snarfing their ack....
	if (debugfile) putc(c,debugfile);
	c = fdgetchar(EMU_gdb_conn);
      }
      if (c < 0) break;
    
      /* Check for re-runs */
      if (c == '-') {
	if (debugfile) fputs("<- -\n",debugfile);
	write(EMU_gdb_conn,packetbuf,strlen(packetbuf));
	c = fdgetchar(EMU_gdb_conn);
	continue;
      }
    
      /* get the packet.... watch out for overruns */
      pcount = 0;
      while (c != '#' && pcount < PACKET_SIZE) {
	packet[pcount++] = c;
	c = fdgetchar(EMU_gdb_conn);
      }
      packet[pcount] = 0;

      /* Throw away the checksum */
      fdgetchar(EMU_gdb_conn);
      fdgetchar(EMU_gdb_conn);
    
      /* Process the packet */
    
      if (debugfile) {
	fprintf(debugfile,"%-60.60s\n",packet);
	fflush(debugfile);
      }

      write(EMU_gdb_conn,"+",1);  /* we got the packet */ 
    
      stepping = 0;
    
      switch (packet[1]) {

      case 'H' : // set thread:  // TODO: actually do this
	sendpacket(EMU_gdb_conn,"OK");
	break;

      case '?':
	/* TODO: should be reason of last signal */
	sendpacket(EMU_gdb_conn,"S05");  /* sigtrap for debug? */
	break;

      case 'G': // write regs
	len = 0;
	lenmax = 20; // who cares?
	npp = packet+2;
	goto doregs;
      case 'P': // write single reg
	npp = packet+2;
	rval = gethexint(npp,&len);
	if (!rval || !(npp = npp + rval) || *npp++ != '=') {
	  sendpacket(EMU_gdb_conn,"E01");
	  break;
	}
	lenmax = len;
      doregs:
	{
	  for (;len<=lenmax;len++) {
	    npp+=gethex64(npp,&val);
	    if (len < 32) { /* gr */
	      *get_r_ptr(ctx, len, ctx->PR[CWP]) = LSE_l2h(val);
#ifdef DEBUG_GDBSET
	      fprintf(stderr,"r%d = %016llx\n",len,val);
#endif
	    } else if (len < 64) {
	      ctx->F[len] = LSE_l2h(val);
	    } else if (len == 0x40) { // state (cwp,pstate,asi,ccr)
	      uint64_t nval = LSE_l2h(val);
	      ctx->PR[PSTATE] = (nval >> 8) & 0xfff;
	      ctx->PR[CWP] = nval & 0x1f;
	      ctx->ASR[CCR] = (nval >> 32) & 0xff;
	    } else if (len == 0x41) { // fsr
	      ctx->FSR = LSE_l2h(val);
	    } else if (len == 0x42) { // fprs
	      ctx->ASR[FPRS] = LSE_l2h(val);
	    } else if (len == 0x43) { // y
	    }
	  } // for
	}
	sendpacket(EMU_gdb_conn,"OK");
	break;
      
      case 'g': { // read regs
	std::ostringstream newp;
	newp << std::hex << std::setfill('0');
	// should dump 64 bytes of G & O registers from wherever they hide
	// then the L and I reg regs (again 64 bytes)
	// then the FP registers (32 * 8 bytes, but stub shows 0)
	// then Y, PSR, WIM, TBR, PC, NPC, FPSR, CPSR

	// PC seems to be 4 bytes at 0x44.  Problem: gdb doesn't seem to do
	// the right thing for sparc64!  It's using only 32 bits
	// 0-7 = g
	// 8-15 = o
	// 16-23 = l
	// 24-31 = i
	// 32-63 = FP
	// 64-69 = y, psr, wim, tbr, pc, npc, fpsr, cpsr

	// 0-7 = g
	// 8-15 = o
	// 16-23 = l
	// 24-31 = i
	// 32-63 = FP
	// 64-68 (but P is 80-84) = pc, npc, state/asi/ccr/cwp, fsr, fprs, y, 

	// TODO: 64 vs. 32

	for (i=0;i<32;i++) { // do the integer registers
	  newp << std::setw(16) 
	       << LSE_h2l(*get_r_ptr(ctx, i, ctx->PR[CWP]));
	}
	for (i=0;i<32;i++) { // do the fp registers
	  newp << std::setw(16) << LSE_h2l(ctx->F[i]);
	}
	newp << std::setw(16) << LSE_h2l(LSE_emu_addr_table[hwcno].PC);
	newp << std::setw(16) << LSE_h2l(LSE_emu_addr_table[hwcno].NPC);
	newp << std::setw(16) << // state (cwp,pstate,asi,ccr)
	  LSE_h2l(( (ctx->PR[PSTATE] & 0xfff) << 8) |
		  ( (ctx->PR[CWP] & 0x1f) ) |
		  ( (ctx->ASR[CCR] & 0xff) << 32));
	newp << std::setw(16) << LSE_h2l(ctx->FSR); // fsr
	newp << std::setw(16) << LSE_h2l(ctx->ASR[FPRS]); // fprs
	newp << std::setw(16) << LSE_h2l(0); // y
	sendpacket(EMU_gdb_conn,newp);
	break;
      }

      case 'm': { // read memory
	std::ostringstream newp;
	newp << std::hex << std::setfill('0');

	npp = packet+2;
	rval=gethex64(npp,&addr);
	if (rval && (npp=npp+rval) && (*npp++ == ',')
	    && (rval=gethexint(npp,&len))) {

	  addr = LSE_l2h(addr);
	  LSE_emu_addr_t madelen;
	  try {
	    LSE_device::device_t *devp;
	    LSE_device::devtptr_t hp;
	    madelen = ctx->mem->translate(LSE_emu_addr_t(addr),
					  &devp, &hp);
	    bufp = reinterpret_cast<char *>(hp.hp);
	  } catch (LSE_device::deverror_t) {
	    sendpacket(EMU_gdb_conn,"E03");
	    break;
	  }
	  //LSE_emu_addr_t ulen = std::min(LSE_emu_addr_t(len), madelen);
	  for (i=0;i<len;i++) {
	    newp << std::setw(2) << static_cast<unsigned int>(bufp[i]&255);
	  }
	  sendpacket(EMU_gdb_conn,newp);
	}
	else
	  sendpacket(EMU_gdb_conn,"E01");
	break;
      }

      case 'M': // write memory
	npp = packet+2;
	rval=gethex64(npp,&addr);
	if (rval && (npp=npp+rval) && (*npp++ == ',')
	    && (rval=gethexint(npp,&len))) {

	  npp+=rval;
	  if (*npp++ != ':') {
	    sendpacket(EMU_gdb_conn,"E01");
	    break;
	  }

	  addr = LSE_l2h(addr);

	  bufp = new char[len];

	  for (i=0;i<len;i++) {
	    if (!*npp || !(rval=gethexbyte(npp,&bufp[i]))) {
	      sendpacket(EMU_gdb_conn,"E01");
	      free(bufp);
	      goto finishpacket;
	    }
	    npp+=rval;
	  }

	  try {
	    ctx->mem->write(LSE_emu_addr_t(addr), 
			    (LSE_device::devdata_t *)(bufp),
			    LSE_emu_addr_t(len)); //true);
	  } catch (LSE_device::deverror_t) {
	    sendpacket(EMU_gdb_conn,"E03");
	    break;
	  }
	  sendpacket(EMU_gdb_conn,"OK");
	  delete bufp;
	}
	else
	  sendpacket(EMU_gdb_conn,"E01");
	break;

#ifdef HMM // how do I do single-step?
      case 's':
	stepping = 1;
#endif
      case 'c':
	rval=gethex64(packet+2,&addr);
	if (rval) {
	  LSE_emu_addr_table[hwcno].PC = addr;
	  LSE_emu_addr_table[hwcno].NPC = addr+4;
	}
	undo_flush(ctx);
	return 0;
	break;

      default:
	sendpacket(EMU_gdb_conn,"");
	break;
      }

    finishpacket:
      c = fdgetchar(EMU_gdb_conn);
    
    } /* while c >= 0 */


    return 1; /* indicate that GDB has detached and program can run free... */
  
  } /* EMU_gdb_enter */

  void EMU_gdb_finish() {
    close(EMU_gdb_conn);
    close(EMU_gdb_socket);
    exit(0);
  }
  
} // namespace SPARC
