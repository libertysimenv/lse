include domain_info.mk
include $(TOPSRCINCDIR)/Make_include.mk

lib: libLSE_SPARC.a

headers: ISA.h

include $(DOMNAME).inc.mk
LIS_OBJFILES = $(LIS_SRCFILES:.cc=.o)
FIXIT='s/LSE_SPARC/$(DOMNAME)/g'

libLSE_SPARC_OBJS=bookkeeping.o SPARC_Linux.o gdbsupport.o \
			$(LIS_OBJFILES)

libLSE_SPARC.a: $(libLSE_SPARC_OBJS)
	ar r $@ $(libLSE_SPARC_OBJS)

# this is the file which domnameP.h expects to see
# make the assumed namespace remap to the proper one
ISA.h: $(DOMNAME).dsc
	ls-make-domain-header --impl --chain --instname=$(DOMNAME) --protect=_$(DOMNAME)_H LSE_emu $(DOMNAME) > $@

checkpointing.cc bookkeeping.cc SPARC_FP.h SPARC_Linux.cc gdbsupport.cc UltraSPARCT1.cc UltraSPARCT1P.h: renamed
	sed $(FIXIT) $@ > $@.tmp 
	mv -f $@.tmp $@

checkpointing.o bookkeeping.o SPARC_Linux.o: ISA.h

$(LIS_OBJFILES): ISA.h

renamed:
	touch renamed

clean: 
	rm -f ISA.h $(lib_OBJS) libLSE_SPARC.a $(LIS_SRCFILES) \
		LSE_SPARC.dsc SPARC_Linux.cc LSE_SPARC.mk

