/* 
 * Copyright (c) 2007-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * UltraSPARC T1 private header file
 *
 * Authors:  Zhuo Ruan
 *           Daniel Rich
 *           David A. Penry <dpenry@ee.byu.edu>
 *
 * This contains stuff that could be in private sections of the .lis file,
 * but seem more like clutter there.
 *
 * The trickiest thing here is the MMU code.  What is particularly nasty
 * is the interaction with the softmmu, which is necessary in order for
 * code caches to work properly and to improve speed (since the full 
 * lookup of the MMUs is insanely expensive).
 *
 * There is one softmmu table for each mode: hypervisor, supervisor, and user.
 * The funky enable bit *is* used to select modes.
 * For each mode, any time that any state which could affect translation
 * in that mode changes, the softMMU must be flushed.  I will attempt to 
 * document these changes here:
 *                             
 * hpstate.red:        S, U
 * lsu_control.immuon: S, U
 * lsu_control.dmmuon: S, U
 * lsu_control.vrM:    S, U
 * lsu_control.vwM:    S, U
 * lsu_control.vmM:    S, U
 * tl:                 H, S, U
 * PRIMARY_CONTEXT:    H, S, U
 * PARTITION_ID:       S, U
 * D_Z_CONFIG:         S, U
 * VIRT_WATCHPOINT:    S, U
 * pstate.cleM:        H, S, U
 * pstate.amM:         S, U
 * D_NZ_CONFIG:        S, U
 * TLB entries replaced/flushed: S, U
 *
 * However, to keep life easy, I'll just flush whenever any of these changes
 */

/***************** register masking *****************/

inline uint64_t hpr_mask(UltraSPARCT1_selector *d, int regno) { 
  if (regno == 0) {
    return (hpstate_ibeM|hpstate_redM|hpstate_hprivM|hpstate_tlzM|bit(11));
  } else return hpr_mask((SPARCv11_selector *)(0), regno);
}

inline uint64_t pr_mask(UltraSPARCT1_selector *d, int regno) { 
  if (regno == TSTATE) { 
    // don't allow writes to mm field or top bit of gl or top bit of pstate
    return pr_mask((SPARCv11_selector *)(0),regno) & ~bits(2,14) 
      & ~bit(42) & ~bit(20);
  } else if (regno == PSTATE) { // don't allow writes to mm field
    return pr_mask((SPARCv11_selector *)(0),regno) & ~bits(2,6) & ~bit(12);
  } else return pr_mask((SPARCv11_selector *)(0), regno);
}

inline uint64_t asr_mask(UltraSPARCT1_selector *d, int regno) { 
  if (regno == 26) return 5;
  else return asr_mask((SPARCv11_selector *)(0), regno);
}

inline int asr_remap(UltraSPARCT1_selector *d, int regno, int isWrite) {
  if (regno == STICK) return TICKP;
  else return regno;
}

// supply FSR mask
inline uint64_t fsr_mask(UltraSPARCT1_selector *d) { 
  return fsr_fcc3M | fsr_fcc2M | fsr_fcc1M | fsr_rdM | fsr_temM |
    fsr_fttM | fsr_fcc0M | fsr_aexcM | fsr_cexcM;
}

const int hpstate_enbO = 11; const uint64_t hpstate_enbM= Cbit(hpstate_enbO);

/******************* MMU handling *************/

enum T1_mmu_regs {
  DUMMY=0,
  PRIMARY_CONTEXT, SECONDARY_CONTEXT, 
  I_Z_TSB_PS0, D_Z_TSB_PS0, I_Z_TSB_PS1, D_Z_TSB_PS1, 
  I_Z_CONFIG, D_Z_CONFIG, 
  I_NZ_TSB_PS0, D_NZ_TSB_PS0, I_NZ_TSB_PS1, D_NZ_TSB_PS1,
  I_NZ_CONFIG, D_NZ_CONFIG, 
  I_TAG_TARGET, D_TAG_TARGET, 
  I_SF_STATUS, D_SF_STATUS,
  D_SF_ADDR, 
  I_TAG_ACCESS, D_TAG_ACCESS,
  VIRT_WATCHPOINT, 
  PARTITION_ID,
  I_PS0_PTR, D_PS0_PTR, I_PS1_PTR, D_PS1_PTR, D_DIRECT_PTR,
  I_DATA_IN, D_DATA_IN, I_DATA_ACCESS, D_DATA_ACCESS,
  I_TAG_READ, D_TAG_READ, 
  I_DEMAP, D_DEMAP, I_INVAL_ALL, D_INVAL_ALL,
  LSU_CONTROL, // 39

  SPARC_ERROR_EN_REG, SPARC_ERROR_STATUS_REG,
  SPARC_ERROR_ADDRESS_REG, SPARC_BIST_CONTROL, INST_MASK_REG, LSU_DIAG_REG,
  STM_CTL_REG, SWVR_INTR_RECEIVE, SWVR_UDB_INTR_W, SWVR_UDB_INTR_R,

  CPU_MONDO_HEAD, CPU_MONDO_TAIL, DEV_MONDO_HEAD, DEV_MONDO_TAIL,
  RES_ERROR_HEAD, RES_ERROR_TAIL, NONRES_ERROR_HEAD, NONRES_ERROR_TAIL,

  SCRATCH0, SCRATCH1, SCRATCH2, SCRATCH3,
  SCRATCH4, SCRATCH5, SCRATCH6, SCRATCH7,

  MYSTERY_REG,

  MA_CONTROL, MA_PA, MA_ADDR, MA_NP

};

const int lsu_control_reg_vmO = 25;
const uint64_t lsu_control_reg_vmM = uint64_t(0xff)<<25;
const uint64_t lsu_control_reg_vrM = uint64_t(0x1)<<22;
const uint64_t lsu_control_reg_vwM = uint64_t(0x1)<<21;
const uint64_t lsu_control_reg_dmM = uint64_t(0x1)<<3;
const uint64_t lsu_control_reg_imM = uint64_t(0x1)<<2;
const uint64_t lsu_control_reg_dcM = uint64_t(0x1)<<1;
const uint64_t lsu_control_reg_icM = uint64_t(0x1)<<0;

const uint64_t tte_vM = uint64_t(0x1) << 63;
const uint64_t tte_szlM = uint64_t(0x3) << 61;
const uint64_t tte_nfoM = uint64_t(0x1) << 60;
const uint64_t tte_ieM = uint64_t(0x1) << 59;
const uint64_t tte_soft2M = uint64_t(0x3ff) << 49;
const uint64_t tte_szhM = uint64_t(0x1) << 48;
const uint64_t tte_diagM = uint64_t(0xff) << 40;
const uint64_t tte_paM = uint64_t(0x7ffffff) <<13;
const uint64_t tte_softM = uint64_t(0x1f) << 8;
const uint64_t tte_lM = uint64_t(0x1) << 6;
const uint64_t tte_cpM = uint64_t(0x1) << 5;
const uint64_t tte_cvM = uint64_t(0x1) << 4;
const uint64_t tte_eM = uint64_t(0x1) << 3;
const uint64_t tte_pM = uint64_t(0x1) << 2;
const uint64_t tte_wM = uint64_t(0x1) << 1;

const uint32_t data_access_exception_priv = 1;
const uint32_t data_access_exception_spec = 2;
const uint32_t data_access_exception_atomic_io = 4;
const uint32_t data_access_exception_invalid = 8;
const uint32_t data_access_exception_nfo = 0x10;
const uint32_t data_access_exception_va_out_of_range = 0x20;
const uint32_t instruction_access_exception_priv = 1;
const uint32_t instruction_access_exception_fetch = 0x20;
const uint32_t instruction_access_exception_target = 0x40;

  inline void T1_mmu::calcInstrPA(const addr_t pc,
				  SPARC_addrtransl_t &addr,
				  SPARC_fault_t &fault,
				  uint64_t hpstate, uint64_t tl, 
				  uint64_t pstate) {

    int emode = ( (hpstate & (hpstate_hprivM|hpstate_enbM)) ==
		  (hpstate_hprivM|hpstate_enbM) )
      ? 2 : (pstate & pstate_privM) ? 1 : 0;

    addr.ha = ctx->softmmu.translate(pc, emode, 4, softmmu_t::Ifetch, 
				     MMUhandler(*ctx, fault, fetchOp, addr,
						hpstate, tl, pstate));
  }

  // version which just goes straight to the context for use by things like
  // main loops
  inline void T1_mmu::calcInstrPA(const addr_t pc, SPARC_addrtransl_t &flags,
				  SPARC_fault_t &fault) {
    calcInstrPA(pc, flags, fault, ctx->HPR[HPSTATE], ctx->PR[TL],
		ctx->PR[PSTATE]);
  }

inline void T1_mmu::calcPA(SPARC_addrtransl_t &addr,
			   SPARC_access_type atype, 
			   SPARC_fault_t &fault, 
			   uint64_t hpstate,
			   uint64_t pstate,
			   uint64_t tl) {
  addr.mmu_intercepted = false;
  addr.flags.e = 0;
  addr.le = false;
  addr.pa = addr.va;

  if (addr.asi != -1) 
    return handleExplicitASI(addr, atype, fault, hpstate, pstate, tl);

  int emode = ( (hpstate & (hpstate_hprivM|hpstate_enbM)) ==
		(hpstate_hprivM|hpstate_enbM) )
    ? 2 : (pstate & pstate_privM) ? 1 : 0;

  addr.ha = ctx->softmmu.translate(addr.va, emode, addr.size, 
				   (softmmu_t::kinds)((int)atype % 3),
				   MMUhandler(*ctx, fault, atype, addr, hpstate,
					      tl, pstate));
}

/******************* Specialized device models *******************/

using namespace LSE_device;

class T1_IntHandler : public device_t {
  int masterHWcno;
public:
  devaddr_t translate(devaddr_t addr, device_t **devpp, devtptr_t *datapp);
  devaddr_t writeSub(devaddr_t addr, const devdata_t *buffer, devaddr_t len,
		     int writeFlags=0, devdata_t *backup=0, 
		     resolver_t **reslist=0);
  devaddr_t readSub(devaddr_t addr, devdata_t *buffer, devaddr_t len,
		    resolver_t **reslist=0);
  void setMasterHWcno(int m);
  bool handleCommand(const char *cmd, ...);
};



/********************** exception reporting for MMU ****************/
/* Note that jmpl and return report information in the MMU registers
 */

void perform_trap_impl(UltraSPARCT1_selector *dummy, 
		       LSE_emu_isacontext_t &ctx, 
		       LSE_emu_instr_info_t &LIS_ii, bool redstate,
		       int tl, uint64_t hpstate, uint64_t pstate,
		       uint64_t gl);

