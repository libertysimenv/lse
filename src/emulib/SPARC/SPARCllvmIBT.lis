/* -*-c-*-
 * Copyright (c) 2007-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LIS SPARC LLVM binary translator description
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file describes how to do binary translation for SPARCs using LLVM
 *
 */

style SPARCllvmIBT {

  codesection SPARCllvmIBT_headers {
#include <emulib/LSE_llvmbt_support.h>
#include <vector>
#include "llvm/Constants.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Function.h"
  }

  // Do NOT exclude fetchEarlyOpStep because instructions need to refetch
  // these ops at present
  exclude translatePCStep-50:findOpcodeStep+50;

  entry {
    %%RTYPE() (*LIS_template)(%%AFTERPARMS()) = bt_%%NAME()::get_template();
    LSE_emu_addr_t t_addr = get_addr();
    instr_t t_instr = get_instr();

    LSE_llvmbt_support::LIS_start_code();

    %%IFNAFTER(%%DECLS(LIS_ii,instr,addr));
    %%IFAFTER(%%DECLS(LIS_ii,addr));

    %%BEFORE();
    fault.ft = no_fault;
    instr = t_instr;
    addr.PC = t_addr;
    %%IFAFTER(LIS_template(%%AFTERARGS(LIS_ii)));

    LSE_llvmbt_support::LIS_end_code();
  }

  specialize instr/*, addr*/;

  codesection privateheaders {
#include "emulib/LSE_llvmbt_support.h"
  }
  codesection private {
    namespace SPARCllvmIBT { const char *LIS_llvm_get_bytes(const char *&); }
  }
  codesection supportheaders {
#include <stdlib.h>
  }

  codesection interfaceFields {
    LSE_llvmbt_support::BTmanagerGroup *SPARCllvmIBTmanager;
    uint64_t SPARCllvmIBTgenerated;
    int hotlimit;
  }
  
  codesection initializeInterfaceFields {
    {
      const char *start, *end;
      start = SPARCllvmIBT::LIS_llvm_get_bytes(end);
      SPARCllvmIBTmanager = new LSE_llvmbt_support::BTmanagerGroup(start, end);
      SPARCllvmIBTgenerated = 0;
      hotlimit = 5000;
    }
  }
  codesection finalizeInterfaceFields {
    delete SPARCllvmIBTmanager;
  }

  codesection parse_arg {
    if (!strcmp(arg, "debugcodegen")) {
      SPARCllvmIBTmanager->setDebugFlag(true);
      rval = 1;
    } else if (!strncmp(arg,"hotlimit:",9)) {
      hotlimit = strtol(arg+9,0,0);
      rval = 1;
    }
  }

  codesection print_usage {
    fprintf(stderr, "\tdebugcodegen\t\tPrint generated bt code\n");
    fprintf(stderr, "\thotlimit:#\t\tExecutions to be hot\n");
  }

  codesection SPARCllvmIBT_prologue {
    namespace SPARCllvmIBT {
#ifndef LIS_LLVM_BT_GENERATING_BYTECODE
      instr_t get_instr() { return 0; }
      void do_return(int) {}
      LSE_emu_addr_t get_addr() { return 0; }
#else
      instr_t get_instr();
      LSE_emu_addr_t get_addr();
      void do_return(int);
#endif
    } // namespace SPARCllvmIBT
  }

  generator interfaceFields {
    struct bt_%%NAME() *fld_%%NAME();
  }

  generator initializeInterfaceFields {
    fld_%%NAME() = new bt_%%NAME()(this);
  }

  generator latepublic {
    void %%NAME()(%%PARMS());
  }

  codesection SPARCllvmIBT_headers {
#include "emulib/LSE_codecache.h"
  }

  codesection SPARCllvmIBT_prologue {
    struct SPARCllvmIBT_CodeBlock_pointers {
      struct pointers2 {
	LIS_CODESECTION(SPARCllvmIBT_codeblock_pointers)
	instr_t LIS_instr;
      } d;
      void clear() { memset(&d, 0, sizeof(d)); }
      SPARCllvmIBT_CodeBlock_pointers() { clear(); }
    };
    typedef LSE_codecache::HashedCodeCache<SPARCllvmIBT_CodeBlock_pointers, 
                                           LSE_emu_addr_t> CodeCache_t;
    //template<> CodeCache_t::CodeBlock CodeCache_t::ZeroBlock(0);
    static CodeCache_t codecache;
  } // SPARCllvmIBT_prologue

  generator SPARCllvmIBT_codeblock_pointers {
    typeof %%ENTRY() *%%NAME();
  }

  generator private {
    struct bt_%%NAME() {

      LSE_llvmbt_support::BTmanager *M;

      static %%RTYPE() (*get_template())(%%AFTERPARMS());

      bt_%%NAME()(struct SPARC_dinst_t *);
      ~bt_%%NAME()();
      inline void gen(%%PARMS(), LSE_emu_iaddr_t, int);

    private:
      uint64_t called, miss, generated;

      llvm::Function *templates[LSE_emu_decodetoken_LIS_max];
      const llvm::Type *types[3];
      llvm::Value *endArgs[3];
    };
  } // generator latepublic.

  generator SPARCllvmIBT_epilogue {
    void %%NAME()(%%PARMS()) {
      %%DECLS() ;      
      ctx.di->fld_%%NAME()->gen(%%ARGS(), addr, ctx.di->hotlimit);
    }
  }

  generator SPARCllvmIBT_prologue {

//#define TRACEIT
#ifdef TRACEIT
    uint64_t icount=0;
#endif

    bt_%%NAME()::bt_%%NAME()(SPARC_dinst_t *di) {

      using namespace llvm;
      using namespace %%BUILDSET();
      using namespace %%STYLE();

      void *specializers[] = { (void *)bt_%%NAME()::get_template,
			       (void *)get_addr,
			       (void *)get_instr };

      M = new LSE_llvmbt_support::BTmanager(di->SPARCllvmIBTmanager,
					    %%TEXT(%%NAME()),
					    (void *)%%ENTRY(),
					    (void *)do_return,
					    specializers,
					    sizeof(specializers)/
					    sizeof(specializers[0]));

      for (int %%TOKEN() = 1; %%TOKEN() < LSE_emu_decodetoken_LIS_max; 
	   ++%%TOKEN()) {
	void *selectedFunc = reinterpret_cast<void *>(%%AFTERPTR());
	templates[%%TOKEN()] = M->findFunction(selectedFunc);
      }

      for (unsigned i = 0; i < sizeof(types) / sizeof(types[0]) ; ++i)
	types[i] = M->specializerType(i);

      endArgs[0] = templates[1];
      endArgs[1] = ConstantInt::get(cast<IntegerType>(types[1]), 0);
      endArgs[2] = ConstantInt::get(cast<IntegerType>(types[2]), 0);
      
      called = miss = generated = 0;
    }

    bt_%%NAME()::~bt_%%NAME()() {
      delete M;
      std::cerr << "Code cache miss ratio = " << miss << "/" << called << "=" 
		<< ((float)miss)/called << "\n";
      std::cerr << "Generation ratio = " << generated << "/" << called << "=" 
		<< ((float)generated)/called << "\n";
    }

#ifndef LIS_LLVM_BT_GENERATING_BYTECODE
    %%RTYPE() (*bt_%%NAME()::get_template())(%%AFTERPARMS()) {
      LSE_emu_decodetoken_t %%TOKEN() = (LSE_emu_decodetoken_t)0;
      return %%AFTERPTR();
    }
#endif
    
    inline void bt_%%NAME()::gen(%%PARMS(), LSE_emu_iaddr_t addrToUse,
				 int calllimit) {
      using namespace llvm;
      typeof %%ENTRY() *codebuf = 0;
      LSE_emu_instr_info_t &ii = LIS_ii;
      bool LIS_dospeculation = false;

      %%DECLS();


      LSE_emu_isacontext_t &realct = *(LSE_emu_isacontext_t *)(swcontexttok);

      ++called;
      if (addrToUse.NPC != addrToUse.PC + 4 || addrToUse.annul || 
	  addrToUse.check_level_zero) { 

	// Don't try to codegen because it will be wrong
	LSE_emu_instr_info_t &ii = (&LIS_ii)[0];

	%%DECLS(ii);    
	%%BEFORE(ii);
	ii.iclasses.is_cti = false;
	ii.iclasses.is_sideeffect = false;
	fault.ft = no_fault;

	EMU_dollvmibt_decode(ii, addrToUse, swcontexttok, decodetoken,
			    instr, fault);

	%%RTYPE() (*ep)(%%AFTERPARMS()) = %%AFTERPTR();
	ep(%%AFTERARGS(ii));

//#define TRACEIT
#ifdef TRACEIT
	fprintf(stderr,"B %d/%-10lld ", hwcontextno,icount++);
	EMU_disassemble_addr(swcontexttok, addr.PC, stderr);
#endif
	return;
      }

    tryagain:
      CodeCache_t::CodeBlock *lb;
      if (!codecache.visit(addrToUse.PC, lb)) miss++;

      codebuf = lb->code.d.%%NAME();

      if (!codebuf) {
	if (lb->callcount < calllimit) {
	  lb->callcount++;
	  LSE_emu_iaddr_t caddr = addrToUse;

	  LSE_emu_instr_info_t &ii = LIS_ii;
	  %%DECLS(ii);
	    
	  // swcontexttok   Fake out the DECLS statement.  Nope, need
	  // to have copies here.... Not quite sure how to do that yet!
	  addr = caddr;
	  %%BEFORE(ii);
	  ii.iclasses.is_cti = false;
	  ii.iclasses.is_sideeffect = false;
	  fault.ft = no_fault;

	  EMU_dollvmibt_decode(ii, caddr, swcontexttok, decodetoken, 
			      instr, fault);
	  
	  if (fault.ft) fprintf(stderr, "Hi, here are errors\n");
	  %%RTYPE() (*ep)(%%AFTERPARMS()) = %%AFTERPTR();
	  ep(%%AFTERARGS(ii));

#ifdef TRACEIT
	fprintf(stderr,"I %d/%-10lld ", hwcontextno,icount++);
	EMU_disassemble_addr(swcontexttok, addr.PC, stderr);
#endif

	  lb->set_inline_pc(caddr.NPC);
	  return;

	} else { // actually generate
	  if (false && ++realct.di->SPARCllvmIBTgenerated >= 100000) {
	    realct.di->SPARCllvmIBTmanager->resetCode();
	    codecache.reset();
	    realct.di->SPARCllvmIBTgenerated = 0;
	    //std::cerr << "Clear buffers\n";
	    goto tryagain;
	  }
	  ++generated;
	  M->startCodeBuffer();
	  LSE_emu_iaddr_t caddr = addrToUse;

	  ii.iclasses.is_cti = false;
	  ii.iclasses.is_sideeffect = false;
	  fault.ft = no_fault;

	  EMU_dollvmibt_decode(ii, caddr, swcontexttok, decodetoken, 
			      instr, fault);
	  lb->set_inline_pc(caddr.NPC);

	  Function *func = templates[%%TOKEN()];

	  Value *args[] = {
	    func,
	    ConstantInt::get(cast<IntegerType>(types[1]), addr.PC),
	    ConstantInt::get(cast<IntegerType>(types[2]), instr) };
	    
	  M->addCall(args, sizeof(args)/sizeof(args[0]));
	  
	  lb->code.d.%%NAME() = codebuf  
	    = (typeof codebuf) 
	    M->finishCodeBuffer(endArgs, (sizeof(endArgs) 
					  / sizeof(endArgs[0])));
#ifdef TRACEIT
	  fprintf(stderr, "Generating\n");
#endif
	}
      }
      (*codebuf)(%%ARGS());
#ifdef TRACEIT
	fprintf(stderr,"T %d/%-10lld ", hwcontextno,icount++);
	EMU_disassemble_addr(swcontexttok, addrToUse.PC, stderr);
#endif
    } // gen

  } // generator

  // Be very careful when editing this codesection, as the indentation matters
  // and you MUST use tab characters!
  codesection makefile {
$(DOMNAME).SPARCllvmIBT.bytecodes.bc: $(DOMNAME).SPARCllvmIBT.cc
	clang++ -O3 -DLIS_LLVM_BT_GENERATING_BYTECODE -c -I. -I$(LSE)/include/domains -I `llvm-config --includedir` -Wno-parentheses -Wno-logical-op-parentheses $< -emit-llvm -o $@

$(DOMNAME).SPARCllvmIBT.bytecodes.cc: $(DOMNAME).SPARCllvmIBT.bytecodes.bc
	( echo 'namespace $(DOMNAME) { namespace SPARCllvmIBT {' ; \
	  echo 'const char *LIS_llvm_get_bytes(const char *&size) {' ; \
	  echo 'static const char bytes[] = {' ; \
	  perl -ne '$$s = $$_; foreach $$x (unpack("C*",$$s)) { print $$x; print ",";} print "\n";' < $< ; \
	  echo '0, };' ; \
	  echo 'size = bytes + sizeof(bytes) - 1;' ; \
	  echo 'return bytes;' ; \
	  echo '}' ; \
	  echo '}}' ) > $@

LIS_SRCFILES+=$(DOMNAME).SPARCllvmIBT.bytecodes.cc

$(DOMNAME).SPARCllvmIBT.o: override CXXFLAGS+=-I `llvm-config --includedir`
} // codesection makefile

  codesection description {
if 1:
    libraries += " #`cat #${LSE}/share/domains/LSE_llvmbt_support.libs`"
}

buildset SPARCllvmIBT_decode ALLbehavior split {
  show iclasses;
  hide &addr, &decodetoken, &instr, &swcontexttok, &fault;
  buildset ALL_decoding ALLbehavior single {}

  entrypoint void EMU_dollvmibt_decode(const LSE_emu_iaddr_t &addr, 
				       void *swcontexttok,
				       LSE_emu_decodetoken_t &decodetoken,
				       instr_t &instr,
				       SPARC_fault_t &fault) = 
    clearStep+50 : findOpcodeStep+50 { decodetoken } 
     reportOpcodeStep-50:lastOptDecodeStep+50;

  codesection description {
extrafuncs += [("EMU_dollvmibt_decode", LSE_domain.LSE_domainIDtype_R,
		    "void ??(LSE_emu_instr_info_t &, const LSE_emu_iaddr_t&, void *, LSE_emu_decodetoken_t&, instr_t&, SPARC_fault_t&)")]
  }
}
}
