/* 
 * Copyright (c) 2000-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Main program for testing the SPARC emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * A stub for calling the SPARC emulator so we can test it.  Also provides
 * some useful debugging features.
 *
 */
#include <LSE_chkpt.h>
#include "LSE_SPARC.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <ctype.h>
#include "LSE_SPARC.priv.h"
using namespace LSE_SPARC;

namespace {
  LSE_clock_t mainclock = 0;
  static const uint64_t speed = 10; // 100 MHz
  int interrupter(int cno, void *d);
}

static char *pname;

/*************** interface handling ************/

#include "LSE_emu_alonesupp.c" /* yes, that's the .c file */

static void usage() {
  fprintf(stderr,
          "\n%s <options> [ [(--|binary)] <program options>]\n",
          pname);
  fprintf(stderr,"\t-c|--cleanenv\t\tClean all environments\n");
  fprintf(stderr,"\t--env:VAR=VALUE\t\tAdd VAR to environment\n");
  fprintf(stderr,"\t--hw:#\t\t\tNumber of hardware contexts\n");
  fprintf(stderr,"\t--gdb:#\t\t\tStart GDB stub using port #\n");
  fprintf(stderr,"\t--ignorefault\t\tDo not end on faults\n");
  fprintf(stderr,"\t--readchecks\t\tExecute from a checkpoint file\n");
  fprintf(stderr,"\t--checkfile=<fname>\tCheckpoint file name\n");
  fprintf(stderr,"\t--checkname=<name>\tCheckpoint name\n");
  fprintf(stderr,"\t--checkcompress\t\tCompress when writing checkpoints\n");

  fprintf(stderr,"\t--oldstyle\t\tUse old style command-line\n");
#ifdef EXTRA_FEATURES
  fprintf(stderr,"\t--debug      \t\tStart in debug mode\n");
  fprintf(stderr,"\t--quitafter:#\t\tQuit after #\n");
#endif 

  fprintf(stderr,"\n Sampling options\n");
  fprintf(stderr,"\t--sample:first=#\tFirst instruction to sample\n");
  fprintf(stderr,"\t--sample:period=#\tSampling period\n");
  fprintf(stderr,"\t--sample:length=#\tLength of sampling state\n");
  fprintf(stderr,"\t--sample:warmup=#\tLength of warmup state\n");
  fprintf(stderr,"\t--sample:max=#\t\tMaximum number of samples\n");
  fprintf(stderr,"\t--sample:report\t\tReport sampling state changes\n");
  fprintf(stderr,"\t    setting period=0 prevents state transitions;\n");
  fprintf(stderr,"\t    the initial state is sampling if any of the\n");
  fprintf(stderr,"\t    actions but syscall are chosen\n");
  fprintf(stderr,"  Actions taking place during sampling/warmup\n");
  fprintf(stderr,"\t--trace\t\t\tTrace instructions\n");
  fprintf(stderr,"\t--optrace\t\tReport operands when tracing\n");
  fprintf(stderr,"\t--syscall\t\tReport syscalls\n");
  fprintf(stderr,"\t--forcesample\t\tDummy to force sampling initial state\n");
  fprintf(stderr,"\t--testrollback\t\tTest rollback\n");
  fprintf(stderr,"\t--steps\t\t\tTurn on steps\n");
  fprintf(stderr,"\t--sub\t\t\tTurn on sub-steps for opfetch/writeback\n");

  fprintf(stderr,"\t--writechecks=[full|os|incr]\tWrite checkpoints\n");
  fprintf(stderr,"\t--interrupt=#\tInterrupt period (0 = none)\n");
  fprintf(stderr,"\n Emulator options (prefix with `-emu:')\n");
  EMU_print_usage(&LSE_emu_interface);

  exit(1);
}

static char **cleanenvp = NULL;


uint64_t icount=0;
int substep = 0;
int64_t samplePeriod=0, sampleLength=0, sampleWarmup=0, sampleFirst= 0,
  sampleToGo, sampleCount, sampleMax= -1;
extern uint64_t graphperiod, graphlength;
int dotrace=0;
int doseparate=0;
int ignorefault=0;
int sampleReport=0;
int forcesample=0;
int dooptrace=0;
int doold=0;
static int dosyscall=0;
bool dorollback=false;
int dodump=0;
boolean dodebug = 0;
int numHWcontexts=1;
int64_t quitafter = -1;
#ifdef CHECKPOINTS
static int readCheckpoints=0;
static int writeCheckpoints = 0;
static char defaultCFN[]="emu.cpf";
static char *cptFileName=defaultCFN;
static LSE_chkpt::file_t *cptFile=NULL;
static char *cptName=NULL;
static boolean cptCompress=FALSE;
LSE_emu_chkpt_cntl_t cptControl;
#endif

typedef enum {
  ffwd, warm, sample
} sampleState_t;
static const char *sampleStateNames[] = { "Ffwd", "Warmup", "Sample" };

static sampleState_t sampleState;

static void run_stuff(int, boolean);

#ifdef GDB
namespace LSE_SPARC {
extern void EMU_gdb_init(int);
extern void EMU_gdb_finish(void);
extern void EMU_gdb_start_context(LSE_emu_contextno_t);
extern int EMU_gdb_enter(int, LSE_emu_contextno_t);
extern void EMU_gdb_report_end(void);
}
#else
namespace LSE_SPARC {
  void EMU_gdb_init(int) {}
  void EMU_gdb_finish(void) {}
  void EMU_gdb_start_context(LSE_emu_contextno_t) {}
  int EMU_gdb_enter(int, LSE_emu_contextno_t) {return 0;}
  void EMU_gdb_report_end(void) {}
}
#endif

namespace LSE_SPARC {
  extern int EMU_trace_syscalls;
}


extern int M;

void build_cleanenv() 
{
  cleanenvp = (char**)malloc(sizeof(char*));
  *cleanenvp = NULL;
}

void add_to_cleanenv(char *str)
{
  int i = 0;
  while(cleanenvp[i]) i++;
  cleanenvp = (char **)realloc(cleanenvp, sizeof(char*) * (i+2));
  cleanenvp[i] = str;
  cleanenvp[i+1] = NULL;
}

void destroy_cleanenv()
{
  if(cleanenvp)
    free(cleanenvp);
}

int main(int argc, char *argv[], char **envp)
{
  int cleanenv=0, numused, ret;
  int rval;
  int gdbport=0;
  int i, cno;

  pname = argv[0];
  LSE_sim_exit_status = 0;
  EMU_init(&LSE_emu_interface);

  for (i=1;i<argc;) {
    if (!strcmp(argv[i],"--")) {
      i++;
      break;
    }
    if (!strncmp(argv[i],"-emu:",5)) {
      numused = EMU_parse_arg(&LSE_emu_interface, argc-i,argv[i]+5,argv+i+1);
      if (numused <= 0) {
        usage();
      }
      i+= numused;
    }
    else if (!strncmp(argv[i],"--emu:",6)) {
      numused = EMU_parse_arg(&LSE_emu_interface, argc-i,argv[i]+6,argv+i+1);
      if (numused <= 0) {
        usage();
      }
      i+= numused;
    }
    else {
      if (!strcmp(argv[i],"--help")) usage();
      else if (!strcmp(argv[i],"-help")) usage();
      else if (!strcmp(argv[i],"-c") ||
	       !strcmp(argv[i],"--cleanenv")) {
	cleanenv = 1;
	build_cleanenv();
      }
      else if (!strncmp(argv[i],"--env:",6)) {
	if(!cleanenv) {
	  fprintf(stderr, "Only use --env with -c or --cleanenv\n");
	  usage();
	} else {
	  add_to_cleanenv(argv[i]+6);
	}
      }
      else if (!strncmp(argv[i],"--hw:",5)) {
	ret = sscanf(argv[i]+5,"%d",&numHWcontexts);
	if (ret<=0) numHWcontexts=1;
      }
#ifdef GDB
      else if (!strncmp(argv[i],"--gdb:",6)) {
	ret = sscanf(argv[i]+6,"%d",&gdbport);
	if (ret<=0) gdbport=0;
      }
#endif
      else if (!strcmp(argv[i],"--steps")) doseparate = 1;
      else if (!strcmp(argv[i],"--sub")) substep = 1;
      else if (!strcmp(argv[i],"--ignorefault")) ignorefault = 1;
      else if (!strcmp(argv[i],"--forcesample")) forcesample = 1;

#ifdef CHECKPOINTS
      else if (!strncmp(argv[i],"--writechecks=",14)) {
	if (!strcmp(argv[i]+14,"full")) {
	  writeCheckpoints=1;
	} else if (!strcmp(argv[i]+14,"os")) {
	  writeCheckpoints=3;
	} else {
	  writeCheckpoints=2;
	}
      }
      else if (!strcmp(argv[i],"--readchecks")) readCheckpoints=1;
      else if (!strncmp(argv[i],"--checkfile=",12)) {
	cptFileName = argv[i]+12;
      }
      else if (!strncmp(argv[i],"--checkname=",12)) {
	cptName = argv[i]+12;
      }
      else if (!strcmp(argv[i],"--checkcompress")) cptCompress=TRUE;
#endif // CHECKPOINTS
      else if (!strcmp(argv[i],"--sample:report")) sampleReport = 1;
      else if (!strncmp(argv[i],"--sample:period=",16)) {
	ret = sscanf(argv[i]+16,"%"SCNd64,&samplePeriod);
	if (ret<=0) samplePeriod=0;
      }
      else if (!strncmp(argv[i],"--sample:length=",16)) {
	ret = sscanf(argv[i]+16,"%"SCNd64,&sampleLength);
	if (ret<=0) sampleLength=0;
      }
      else if (!strncmp(argv[i],"--sample:warmup=",16)) {
	ret = sscanf(argv[i]+16,"%"SCNd64,&sampleWarmup);
	if (ret<=0) sampleWarmup=0;
      }
      else if (!strncmp(argv[i],"--sample:first=",15)) {
	ret = sscanf(argv[i]+15,"%"SCNd64,&sampleFirst);
	if (ret<=0) sampleFirst=0;
      } 
      else if (!strncmp(argv[i],"--sample:max=",13)) {
	ret = sscanf(argv[i]+13,"%"SCNd64,&sampleMax);
	if (ret<=0) sampleMax=0;
      } 
      else if (!strcmp(argv[i],"--trace")) dotrace = 1;
      else if (!strcmp(argv[i],"--optrace")) dooptrace = 1;
      else if (!strcmp(argv[i],"--syscall")) dosyscall = 1;
      else if (!strcmp(argv[i],"--testrollback")) dorollback = 1;

      else if (argv[i][0]!='-') break;
      else {
	fprintf(stderr,"Error: unrecognized argument\n");
	usage();
      }
      i++;
    }
  }
  if (i==argc/* && !readCheckpoints*/) { /* missing binary */
    fprintf(stderr,"Must have a binary name\n");
    usage();
  }

#ifdef CHECKPOINTS
  if (readCheckpoints && writeCheckpoints) {
    fprintf(stderr,
	    "Cannot both read and write checkpoints simultaneously\n");
    usage();
  }
#endif
  /* make things nice for just doing --sample:first */
  if (sampleFirst && !samplePeriod && !sampleLength)
     samplePeriod = sampleLength = INT64_MAX;

  /* fprintf(stderr,"%lld %lld %lld\n",samplePeriod,sampleLength,tracecount); */

  LSE_emu_standalone_init();

  mainclock = new ::LSE_clock::clock_t("mainclock");

  for (cno=1;cno<=numHWcontexts;cno++) {
    LSE_emu_create_context(cno); /* hardware */
    EMU_register_clock(&LSE_emu_interface, cno, 0, mainclock);
    EMU_register_clock(&LSE_emu_interface, cno, 1, mainclock);
    LSE_emu_interrupt_callback_t idea(interrupter, 0);
    EMU_register_interrupt_callback(&LSE_emu_interface, cno, idea);
  }

#ifdef CHECKPOINTS
  if (!readCheckpoints) {
#endif
    rval = EMU_context_load(&LSE_emu_interface, 1, argc-i,
			    argv+i,cleanenv? cleanenvp : envp);
    if (rval) {
      fprintf(stderr,"Error while loading program\n");
      exit(1);
    }

    LSE_emu_addr_table[1]
	= EMU_get_start_addr(LSE_emu_hwcontexts_table[1].ctok);
#ifdef CHECKPOINTS
  }
#endif

#ifdef CHECKPOINTS
  /* deal with starting up checkpoint here... */
  if (writeCheckpoints) {
    char pbuf[50];

    cptFile = new LSE_chkpt::file_t(cptFileName,"w");
    if (!cptName) cptName = (char *)"unknown";

    cptFile->begin_header_write(cptName);

    switch (writeCheckpoints) {
    case 1 : /* full */
      cptControl.recordOS = FALSE;
      cptControl.incrementalMem = FALSE;
      break;
    case 2 : /* incremental */
      cptControl.recordOS = TRUE;
      cptControl.incrementalMem = TRUE; 
      break;
    case 3 : /* OS */
      cptControl.recordOS = TRUE;
      cptControl.incrementalMem = FALSE; 
      break;
    default  :
      cptControl.recordOS = FALSE;
      cptControl.incrementalMem = FALSE;
      break;
    }

    /* global parameters -- sort of standard here... */
    sprintf(pbuf, "SPERIOD=%" PRId64,samplePeriod);
    cptFile->add_globalparm(pbuf);
    sprintf(pbuf, "SLENGTH=%" PRId64,sampleLength);
    cptFile->add_globalparm(pbuf);
    sprintf(pbuf, "SWARMUP=%" PRId64,sampleWarmup);
    cptFile->add_globalparm(pbuf);
    sprintf(pbuf, "SFIRST=%" PRId64,sampleFirst);
    cptFile->add_globalparm(pbuf);

    EMU_chkpt_add_toc(&LSE_emu_interface, cptFile, "emu.0", 0, &cptControl);
    EMU_chkpt_add_toc(&LSE_emu_interface, cptFile, "emu.1", 1, &cptControl);
    cptFile->end_header_write();

    /* and actually run things... */

    icount = 0;
    sampleCount = 0;

    run_stuff(gdbport,FALSE);

    cptFile->close();

  } else if (readCheckpoints) {
    LSE_chkpt::error_t cerr;
    char *fileID, *str;
    
    cptFile = new LSE_chkpt::file_t(cptFileName,"r");
    cerr = cptFile->begin_header_read(&fileID);
    if (cerr) {
      fprintf(stderr,"Checkpoint error #%d\n",cerr);
      exit(cerr);
    }

    cptFile->get_globalparm(&str,FALSE); /* SPERIOD */
    sscanf(str+8,"%" SCNd64,&samplePeriod);
    cptFile->get_globalparm(&str,FALSE); /* SLENGTH */
    sscanf(str+8,"%" SCNd64,&sampleLength);
    cptFile->get_globalparm(&str,FALSE); /* SWARMUP */
    sscanf(str+8,"%" SCNd64,&sampleWarmup);
    cptFile->get_globalparm(&str,FALSE); /* SFIRST */
    sscanf(str+7,"%" SCNd64,&sampleFirst);

    /* change variables to run off of checkpoints */
    sampleFirst = 0;

    /* verify the table of contents */
    EMU_chkpt_check_toc(&LSE_emu_interface, cptFile,NULL,0,NULL,&cptControl);
    EMU_chkpt_check_toc(&LSE_emu_interface, cptFile,NULL,1,NULL,&cptControl);

    icount = 0;
    sampleCount = 0;

    cptFile->seek(1,SEEK_SET); /* first checkpoint */
    while (cptFile->more_checkpoints()) {

      cptFile->begin_checkpoint_read(&icount,NULL);

      cerr = EMU_chkpt_read_segment(&LSE_emu_interface, cptFile,NULL,
				    0,&cptControl);

      if (cerr) {
	fprintf(stderr,"Checkpoint error #%d\n",cerr);
	exit(cerr);
      }

      cerr = EMU_chkpt_read_segment(&LSE_emu_interface, cptFile,NULL,
				    1,&cptControl);

      if (cerr) {
	fprintf(stderr,"Checkpoint error #%d\n",cerr);
	exit(cerr);
      }

      /* have to update address table... ugh! */
      for (i=1;i<=LSE_emu_hwcontexts_total;i++) {
	if (LSE_emu_hwcontexts_table[i].valid && 
	    LSE_emu_hwcontexts_table[i].ctok)
	  LSE_emu_addr_table[i] 
	    = ((PPC_context_t *)LSE_emu_hwcontexts_table[i].ctok)->startaddr;
      }

      run_stuff(gdbport,TRUE);

      if (sampleMax > 0 && sampleCount >= sampleMax) {
	/* hit max -- will end */
	fprintf(stderr, "Hit max samples -- terminating\n");
	break;
      }

    }

    cptFile->close();

    delete cptFile;

  } else {
#endif // CHECKPOINTS
    icount = 0;
    sampleCount = 0;
    run_stuff(gdbport,FALSE);
#ifdef CHECKPOINTS
  }
#endif 

  fprintf(stderr,"Attempted %lld instructions\n", icount);
  EMU_finish(&LSE_emu_interface);
  LSE_emu_standalone_finalize();
  destroy_cleanenv();

  return LSE_sim_exit_status;
}

static struct {
  int cno;
  boolean needDump;
} continuation;  /* continuation of where we were and all... */

/* way to run when in a fast-forward period.  Nothing can be printed and
 * the fastest modes are off
 * are off.  This is supposed to be the "blazing fast" mode.
 */
static void do_fast() {
  int cno; /* sometimes locals are faxster */
  LSE_emu_ctoken_t ctx;
  LSE_emu_iaddr_t addr;
  LSE_emu_instr_info_t ii;

  cno = continuation.cno;
  while (LSE_sim_terminate_count) {

    if (cno == 1) {
      mainclock->incr(speed);
      mainclock->run_todos();
    }

    for (;cno<=LSE_emu_hwcontexts_total;cno++) {
      if (!(ctx=LSE_emu_hwcontexts_table[cno].ctok)) continue;

      EMU_register_context_user(ctx);

      addr = LSE_emu_addr_table[cno];
	
      icount++;

      ii.addr = addr;
      ii.swcontexttok= ctx;
      ii.hwcontextno = cno;

      EMU_dofast(ii);

      if (ii.fault.ft) {
	fprintf(stderr,
		"Fault encountered: %s at %08llx "
		"(count=%d/%d/%lld)\n",
		EMU_intr_string(ii.fault),
		addr.PC,cno, cno,icount);
	EMU_disassemble_addr(ctx, addr.PC, stderr);
	if (!ignorefault) SPARC_dump_state(ctx,stderr,1);
	if (!ignorefault) exit(1);
      } /* ii.fault */

      if (LSE_emu_hwcontexts_table[cno].ctok == ctx)
	LSE_emu_addr_table[cno] = ii.next_pc;

      EMU_unregister_context_user(ctx);

      sampleToGo--;
      if (!sampleToGo) goto finish;

    } /* for cno */

    cno = 1;
  }
 finish:
  continuation.cno = ++cno;
}

/* way to run when in a non-fast-forward period.  Things can be printed out
 * and slower modes of operation, including gdb, can be used.
 */
static int do_slow(int gdbport) {
  int cno; /* sometimes locals are faxster */
  LSE_emu_ctoken_t ctx;
  LSE_emu_iaddr_t addr;
  LSE_emu_instr_info_t ii;
  int j;

  cno = continuation.cno;

  if (continuation.needDump) {
    int tcno;
    boolean done = FALSE;
    /* find the next mapped context so we can dump it ... */
    for (tcno = cno;tcno<=LSE_emu_hwcontexts_total && !done;tcno++) {
      if (!(LSE_emu_hwcontexts_table[tcno].ctok)) continue;
      done = TRUE;
      break;
    }
    if (!done) {
      for (tcno=1;tcno<cno && !done;tcno++) {
	if (!(LSE_emu_hwcontexts_table[tcno].ctok)) continue;
	done = TRUE;
	break;
      }
    }
    if (done) {
      SPARC_dump_state(LSE_emu_hwcontexts_table[tcno].ctok,stderr,1);
      continuation.needDump = FALSE;
    }
  }

  while (LSE_sim_terminate_count) {

    if (cno == 1) {
      mainclock->incr(speed);
      mainclock->run_todos();
    }

    for (;cno<=LSE_emu_hwcontexts_total;cno++) {
      if (!(ctx=LSE_emu_hwcontexts_table[cno].ctok)) continue;

      addr = LSE_emu_addr_table[cno];
      
      icount++;
      mainclock->incr(speed);
      mainclock->run_todos();

      ii.fault.ft = no_fault;

      // if dooptrace is set, need to do steps so we can see the ops
      if (dorollback) {
	int rval;
	ii.addr.PC += 4; // execute the wrong instruction

        // NOTE: this is not the way we really want to be executing
        // all of our instructions, but is just a means of testing the
        // resolution functionality.

	// execute bad instruction speculatively
	LSE_emu_iaddr_t badaddr = addr;
	badaddr.PC += 4;
        EMU_init_instr(&ii, cno, ctx, badaddr);
        for (int i = 0; i < LSE_emu_max_instrstep-1; i++) 
          EMU_do_step(&ii, static_cast<LSE_emu_instrstep_name_t>(i), true);

	// roll it back
	EMU_resolve_instr(&ii, LSE_emu_resolveOp_rollback);
	EMU_unregister_context_user(ctx);

	// execute good instruction speculatively
	EMU_init_instr(&ii, cno, ctx, addr);
 	
        for (int i = 0; i < LSE_emu_max_instrstep-1; i++) 
          EMU_do_step(&ii, static_cast<LSE_emu_instrstep_name_t>(i), true);

	rval = EMU_resolve_instr(&ii, LSE_emu_resolveOp_query);
	if (rval & LSE_emu_resolveFlag_redo) {
	  // cancel again
	  EMU_resolve_instr(&ii, LSE_emu_resolveOp_rollback);
	  EMU_unregister_context_user(ctx);
	  EMU_init_instr(&ii, cno, ctx, addr);
	  for (int i = 0; i < LSE_emu_max_instrstep; i++) 
	    EMU_do_step(&ii, static_cast<LSE_emu_instrstep_name_t>(i), false);
	} else {
	  EMU_resolve_instr(&ii, LSE_emu_resolveOp_commit);
	  EMU_do_step(&ii, 
		      static_cast<LSE_emu_instrstep_name_t>(LSE_emu_max_instrstep-1), false);
	}

      } else if (!doseparate && !dooptrace) {
	EMU_register_context_user(ctx);
        ii.hwcontextno = cno;
        ii.swcontexttok = ctx;
	ii.addr = addr;
	EMU_dofast(ii);
      } else {
	EMU_init_instr(&ii, cno, ctx, addr);
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)0, false);  /* ifetch */
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)1, false);  /* decode */
	if (substep) { 
	  for (j=0;j<LSE_emu_max_operand_src;j++) 
	    EMU_fetch_operand(&ii,(LSE_emu_operand_name_t)j, false);
	} else EMU_do_step(&ii,(LSE_emu_instrstep_name_t)2, false);  /* opfetch */
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)3, false);   /* evaluate */
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)4, false);   /* ldmemory */
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)5, false);   /* format */
	if (substep) { 
	  for (j=0;j<LSE_emu_max_operand_dest;j++) 
	    EMU_writeback_operand(&ii,(LSE_emu_operand_name_t)j, false);
	} else EMU_do_step(&ii,(LSE_emu_instrstep_name_t)6, false);   /* writeback */
      }

      if (ii.fault.ft) {
	if (gdbport) {
	  switch (ii.fault.ft) {
	  case gdb_breakpoint_entry:
	    ii.next_pc = addr;
	    if (EMU_gdb_enter(5,cno)) {
	      gdbport = 0;
	      EMU_unregister_context_user(ctx);
	      goto finish;
	    }
	    break;
	  default:
	    fprintf(stderr,
		    "Fault encountered: %d at %"PRIx64" "
		    "(count=%d/%d/%lld)\n",
		    ii.fault.ft,
		    addr.PC,cno,cno,
		    icount);
	    EMU_disassemble_addr(ctx,addr.PC,stderr);
            if (!ignorefault && dooptrace) 
              SPARC_print_instr_oper_vals(stderr,"+\t\t",&ii);
	    ii.next_pc = addr; // so restart is at same point
	    if (EMU_gdb_enter(5,cno)) {
	      gdbport = 0;
	      EMU_unregister_context_user(ctx);
	      goto finish;
	    }
	    break;
	  } /* switch interrupt */
	  //EMU_unregister_context_user(ctx);
	  EMU_unregister_context_user(ctx);
	  goto gdbchanged;
	} else if (ii.fault.ft <= gdb_breakpoint_entry/* || EMU_trace_faults*/) {
	    fprintf(stderr,
		    "Fault encountered: %d at %"PRIx64" "
		    "(count=%d/%d/%lld)\n",
		    ii.fault.ft, addr.PC,cno,cno, icount);
	    EMU_disassemble_addr(ctx,addr.PC,stderr);
            if (!ignorefault && dooptrace) 
              SPARC_print_instr_oper_vals(stderr,"+\t\t",&ii);
	    if (!ignorefault) SPARC_dump_state(ctx,stderr,1);
	    if (!ignorefault) exit(1);
	    break;
	  
	} else { /* gdbport */
	  fprintf(stderr,
		  "Fault encountered: %d at %"PRIx64" "
		  "(count=%d/%d/%lld)\n",
		  ii.fault.ft, addr.PC,cno,cno, icount);
	  EMU_disassemble_addr(ctx,addr.PC,stderr);
	  if (!ignorefault) SPARC_dump_state(ctx,stderr,1);
	  if (!ignorefault) exit(1);
	} /* else gdbport */
      } /* ii.fault */
      
      /* do the tracing */

      if (dotrace) {
	fprintf(stderr,"%d/%d/%-10lld ", cno,cno,icount-1);
	EMU_disassemble_addr(ctx,addr.PC,stderr);
	if (dooptrace) SPARC_print_instr_oper_vals(stderr,"+\t\t",&ii);
      }

      if (dodump) SPARC_dump_state(ctx, stderr, false);
      if (LSE_emu_hwcontexts_table[cno].ctok == ctx)
	LSE_emu_addr_table[cno] = ii.next_pc;

      EMU_unregister_context_user(ctx);

      sampleToGo--;
      if (!sampleToGo) goto finish;

    gdbchanged:;

    } /* for cno */

    cno = 1;

  } /* while 1 */
 finish:
  continuation.cno = ++cno;
  return gdbport;
} /* do_slow */

#ifdef CHECKPOINT
static void start_checkpoint(void) {
  int i;

  /* have to update address table... ugh! */
  for (i=1;i<=LSE_emu_hwcontexts_total;i++) {
    if (LSE_emu_hwcontexts_table[i].valid && LSE_emu_hwcontexts_table[i].ctok) 
      ((SPARC_context_t *)LSE_emu_hwcontexts_table[i].ctok)->startaddr =
	LSE_emu_addr_table[i];
  }

  /* at beginning of a "detailed" period; need to either dump state
   * or start recording
   */
  cptFile->begin_checkpoint_write(icount, cptCompress);
  EMU_chkpt_write_segment(&LSE_emu_interface, cptFile,"emu.0",0,&cptControl);
}

static void end_checkpoint(void) {
  EMU_chkpt_write_segment(&LSE_emu_interface, cptFile,"emu.1",1,&cptControl);
  cptFile->end_checkpoint_write();
}
#endif // CHECKPOINT

namespace {
  int interrupter(int cno, void *) {
    if (LSE_emu_hwcontexts_table[cno].ctok) {
      ((SPARC_context_t *)LSE_emu_hwcontexts_table[cno].ctok)->PC =
	LSE_emu_addr_table[cno];
    }
    //Linux_interrupt_cpu(&LSE_emu_interface, cno);
    //return true;
    return false; // immediately switch because we're atomic
  }
}

static void run_stuff(int gdbport, boolean stopAfterOne) {
  boolean mustbeslow;
  boolean hitstart;
  SPARC_dinst_t *di = static_cast<SPARC_dinst_t *>(LSE_emu_interface.etoken);
  int tc=0;
  bool hms = false;

  /* set up sampling */
  if (samplePeriod) {
    if (sampleFirst) { /* skip first */
      if (sampleFirst > sampleWarmup) {
	sampleState = ffwd;
	sampleToGo = sampleFirst - sampleWarmup;
      } else {
	sampleState = warm;
	sampleToGo = sampleFirst;
      }
    } else { /* start right off */
      if (sampleLength) {
	sampleState = sample;
	sampleToGo = sampleLength;
      } else {
	sampleState = ffwd;
	sampleToGo = (samplePeriod - sampleLength - sampleWarmup);
	if (sampleToGo <= 0) sampleState = sample;
      }
    }
  } else {
    sampleState = (dotrace || doseparate || substep || forcesample || dosyscall)
      ? sample : ffwd;
    sampleToGo = INT64_MAX;
  }
  hitstart = sampleState != ffwd;
  di->EMU_trace_syscalls = (hitstart && dosyscall);
  continuation.needDump = (hitstart && false);

#ifdef CHECKPOINTS
  if (hitstart && writeCheckpoints) {
    start_checkpoint();
  }
#endif

  /* Rather than set the single step flag, just call
     the gdb support routines directly.  This seems to fix an
     off-by-one error when dealing with shared objects and symbols. */
  /*if (gdbport) EMU_gdb_start_context(1);*/
#ifdef GDB
  if (gdbport)
    if (EMU_gdb_enter(5,1)) 
      gdbport = 0;
#endif

  continuation.cno = 1;

  if (sampleReport) {
    LSE_emu_contextno_t cno = continuation.cno;
    if (!cno) cno = LSE_emu_hwcontexts_total - 1;
    /* handle state change stuff */
    if (LSE_emu_hwcontexts_table[cno].ctok) {
      fprintf(stderr,"Start in context %d in state %s; "
	      "total inst=%lld toGo=%lld "
	      "IP now %"PRIx64"\n",cno,
	      sampleStateNames[sampleState],icount,sampleToGo,
	      LSE_emu_addr_table[cno]);
    } else {
      fprintf(stderr,"Start in context %d in state %s; "
	      "total inst=%lld toGo=%lld context unmapped\n",
	      cno, sampleStateNames[sampleState],icount,sampleToGo);
    }
  }      

  mustbeslow = gdbport != 0 || sampleState != ffwd;

 tryagain:
  while (LSE_sim_terminate_count) {

    if (mustbeslow || dorollback) {
      gdbport = do_slow(gdbport);
      mustbeslow = gdbport != 0 || sampleState != ffwd;
    }
    else do_fast();

    /* check for sampler state changes... */
    hitstart = false;

    while (sampleToGo <= 0) {
      switch (sampleState) {
      case ffwd:
	sampleState = warm;
	hitstart = TRUE;
	if (dodump) continuation.needDump = TRUE;
	sampleToGo += sampleWarmup;
	break;
      case warm:
	if (sampleLength) { /* actually a special case */
	  sampleState = sample;
	  sampleToGo += sampleLength;
	} else {
	  sampleState = ffwd;
	  sampleToGo += (samplePeriod - sampleWarmup);
	}
	break;
      case sample:
      default:
	sampleToGo += (samplePeriod - sampleLength - sampleWarmup);
	/* end sampling */
	sampleState = ffwd;
	sampleCount++;

#ifdef CHECKPOINTS
	if (!hitstart && writeCheckpoints) {
	  end_checkpoint();
	}
#endif
	hitstart = false;

	if (stopAfterOne) {
	  tc = LSE_sim_terminate_count;
	  LSE_sim_terminate_count=0;
	}
	if (sampleMax > 0 && sampleCount >= sampleMax) {
	  /* hit max -- will end */
	  fprintf(stderr, "Hit max samples -- terminating\n");
	  hms = true;
	  LSE_sim_terminate_count = 0;
	}
	break;
      } /* switch */

      di->EMU_trace_syscalls = (sampleState != ffwd && dosyscall);

#ifdef CHECKPOINTS
      if (hitstart && writeCheckpoints) {
	start_checkpoint();
      }
#endif
      hitstart = false;

      if (sampleReport) {
	LSE_emu_contextno_t cno = continuation.cno-1;
	if (!cno) cno = LSE_emu_hwcontexts_total - 1;

	/* handle state change stuff */
	if (LSE_emu_hwcontexts_table[cno].ctok) {
	  fprintf(stderr,"Switch context %d to state %s; "
		  "total inst=%lld toGo=%lld "
		  "IP now %"PRIx64"\n",cno,
		  sampleStateNames[sampleState],icount,sampleToGo,
		  LSE_emu_addr_table[cno]);
	} else {
	  fprintf(stderr,"Switch context %d to state %s; "
		  "total inst=%lld toGo=%lld context unmapped\n",
		  cno, sampleStateNames[sampleState],icount,sampleToGo);
	}
      }
      
      mustbeslow = gdbport != 0 || sampleState != ffwd;

    } /* while sampleTogo <= 0 */

  } /* while LSE_sim_terminate_count */

  if (!tc && !LSE_sim_terminate_count && !hms) { // ran out of work
    if (mainclock->has_todo()) { // need to skip time ahead...
      mainclock->skip_to_next();
      mainclock->run_todos();
      goto tryagain;
    }
  }

  if (stopAfterOne) LSE_sim_terminate_count= tc;
  /* deal with final sample to finish... */
#ifdef CHECKPOINTS
  if (sampleState != ffwd &&
      writeCheckpoints) {
    end_checkpoint();
  }
#endif

#ifdef GDB
  if (gdbport) EMU_gdb_report_end();
#endif


  return;

}

