/* 
 * Copyright (c) 2000-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Linux OS template file for the SPARC emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 */
/* ************************* NOTES ************************************
 *
 * The main issue for SPARC's Linux emulation is the handling of register
 * windows and the very odd stack frames that they require, particularly
 * when a trap has occurred.
 *
 * the regs structure, which ends up on the stack for trap frames, looks
 * likes:
 *  64-bit version:
 *      16 globals/ins
 *      tstate
 *      tpc
 *      tnpc
 *      y
 *      a magic int = 0x57ac6c00
 *  32-bit version:
 *      psr
 *      pc
 *      npc
 *      y
 *      16 globals/ins
 *
 * A 64-bit stack frame looks like:
 *    fp 
 *   ------------------------------------
 *          : more outgoing arguments
 *    sp+136: 6 outgoing arguments
 *    sp+128: struct ptr
 *            caller pc
 *            frame ptr
 *            6 ins
 *    sp ->   8 locals
 *   -------------------------------------
 *
 * A 32-bit stack frame looks the same, but 4-byte quantities instead
 *
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#undef  __USE_LARGEFILE64
#include "LSE_SPARC.priv.h"

#include <vector>
#define LSE_LINUX_COMPAT
// NOTE: the data structure has restorer, but it is not used from the 
// structure.
#define LSE_LINUX_HAS_SA_RESTORER
#define LSE_LINUX_HEADERS
#define LSE_LINUX_FUTEX_NEEDS_TO_ALLOW_PREEMPTION
#include "OS/LSE_Linux.h"

namespace LSE_SPARC {


extern int EMU_context_copy(LSE_emu_interface_t *,
       	   	 	    LSE_emu_isacontext_t **, LSE_emu_isacontext_t *);
extern int EMU_context_destroy(LSE_emu_interface_t *,
			       LSE_emu_ctoken_t);

extern LSE_chkpt::error_t EMU_write_context(LSE_chkpt::file_t *cptFile,
					    int nmark, LSE_emu_isacontext_t *ctx,
					    LSE_emu_chkpt_cntl_t *ctl);

extern LSE_chkpt::error_t EMU_parse_context(LSE_emu_interface_t *intr,
       			  		    LSE_chkpt::file_t *cptFile,
                                            LSE_emu_isacontext_t *&realct,
					    LSE_emu_chkpt_cntl_t *ctl);

static const uint64_t bias64 = 0x7ff;
static const uint64_t bias32 = 0x0;

#define LSE_LINUX_BASE
#define CC_isacontext_t          LSE_emu_isacontext_t
const bool Linux_bigendian = true;
#include "OS/LSE_Linux.h"

// Without a SPARC/Linux system to experiment on, this is mostly a guess

#define SPARC_PAGE_SIZE uint64_t(0x2000)
#define SPARC_USTACK64_TOP (uint64_t(8)<<40) // this is right
#define SPARC_USTACK64_BASE (SPARC_USTACK64_TOP - (uint64_t(1)<<24))
#define SPARC_USTACK32_TOP (uint64_t(8)<<28) // this is right
#define SPARC_USTACK32_BASE (SPARC_USTACK32_TOP - (uint64_t(1)<<24))
#define SPARC_UTASK64_SIZE (-(uint64_t(1)<<26))
#define SPARC_UTASK32_SIZE (uint64_t(1)<<32)
#define PAGE_BITS 13

struct OSBDefs : public OSBaseDefs {
  // errno
  static const int oENOSYS = 90;
  static const int oETIMEDOUT = 60;
  static const int oEBADFD = 93;
  static const int oEOVERFLOW = 92;

  // fcntl
  static const int oO_CREAT =       0x0200;
  static const int oO_EXCL =        0x0800;
  static const int oO_NOCTTY =      0x8000;
  static const int oO_TRUNC =       0x0400;
  static const int oO_APPEND =      0x0008;
  static const int oO_NONBLOCK =    0x4000;
  static const int oO_NDELAY =      0x4004;
  static const int oO_SYNC =        0x2000;
  static const int oO_DIRECT =    0x100000;
  static const int oO_LARGEFILE =  0x40000;
  static const int oO_DIRECTORY =  0x10000;
  static const int oO_NOFOLLOW =   0x20000;

  // <asm/termios.h>
  static const int oNCC = 8;
  // <bits/termios.h>
  static const int oNCCS = 17;

  // <asm/ioctls.h>
  // read=2, write=4 NRSHIFT 0, TYPESHIFT=8, SIZESHIFT=16, DIRSHIFT=29
  static const unsigned oTCGETS  = 0x40245408;
  static const unsigned oTCGETA  = 0x40125401;
  static const unsigned oTCSETAW = 0x80125403;

  // signals that are different that we care about
  static const int oSIGSTOP  = 17;
  static const int oSIGTSTP  = 18;
  static const int oSIGCONT  = 19;
  static const int oSIGCHLD  = 20;
  static const int oSIGTTIN  = 21;
  static const int oSIGTTOU  = 22;
  static const int oSIGURG   = 16;
  static const int oSIGWINCH = 28;

  // and some more flags that are different...
  static const int oSIG_BLOCK   = 1;
  static const int oSIG_UNBLOCK = 2;
  static const int oSIG_SETMASK = 4;

  // do not forget to redefine these if you redefine any of the signals 
  static const unsigned oSIGDEF_IGNORE() {
    return (OS_sigmask(oSIGCONT) | OS_sigmask(oSIGCHLD) | 
	    OS_sigmask(oSIGWINCH) | OS_sigmask(oSIGURG));
  }
  static const unsigned oSIGDEF_STOP() {
    return (OS_sigmask(oSIGSTOP) | OS_sigmask(oSIGTSTP) | 
	    OS_sigmask(oSIGTTIN) | OS_sigmask(oSIGTTOU));
  }

  static const unsigned oSA_NOCLDWAIT =  0x100u;
  static const unsigned oSA_SIGINFO   =  0x200u;
  static const unsigned oSA_ONSTACK   =  0x1u;
  static const unsigned oSA_RESTART   =  0x2u;
  static const unsigned oSA_RESETHAND =  0x4u;
  static const unsigned oSA_ONESHOT   =  oSA_RESETHAND;

  // ========== Other stuff ==============

  static const int oNUM_DLINFO_ENTRIES = 1;
  static const int oHWCAP              = 31; // v9 + stuff
  static const int oCLKTCK             = 1024;

  static const unsigned oPAGE_SIZE   = SPARC_PAGE_SIZE;
  static const unsigned oPAGE_BITS   = PAGE_BITS;
  static const int oCLONE_STACK_SIZE = 0;//8192 * 4;
  static const uint64_t oRLIMIT_INFINITY = ~uint64_t(0);

  static LSE_emu_addr_t oUTASK_SIZE(bool is64bit) { 
    return (is64bit ? SPARC_UTASK64_SIZE : SPARC_UTASK32_SIZE);
  }
  static LSE_emu_addr_t oUNMAPPED_BASE(bool is64bit) { 
    return (is64bit ? UINT64_C(0xfffff80000000000) : UINT64_C(70000000));
  }
  static LSE_emu_addr_t oUSTACK_BASE(bool is64bit) {
    return (is64bit ? SPARC_USTACK64_BASE : SPARC_USTACK32_BASE);
  }
  static LSE_emu_addr_t oUSTACK_TOP(bool is64bit) { 
    return (is64bit ? SPARC_USTACK64_TOP : SPARC_USTACK32_TOP);
  }

  static const char *oDYNLOADER;
  static const char *oSYSNAME;
  static const char *oNODENAME;
  static const char *oRELEASE;
  static const char *oVERSION;
  static const char *oMACHINE;
  static const char *oDOMAINNAME;

  static const uint64_t oTIMESLICE = 10 * 1000 * 1000; // 10 ms

  // base types

  typedef  uint8_t L_cc_t;
  typedef  int64_t L_clock_t;
  typedef  int64_t L_long;
  typedef uint64_t L_ptr_t;
  typedef uint64_t L_size_t;
  typedef uint32_t L_speed_t;
  typedef  int64_t L_statfs_word;
  typedef  int32_t L_suseconds_t;
  typedef uint32_t L_tcflag_t;
  typedef  int64_t L_time_t;
  typedef uint64_t L_ulong;
  typedef uint64_t L_uptr_t;

  typedef  int32_t L_compat_clock_t;
  typedef  int32_t L_compat_long;
  typedef uint32_t L_compat_size_t;
  typedef  int32_t L_compat_suseconds_t;
  typedef  int32_t L_compat_time_t;
  typedef uint32_t L_compat_ulong;
  typedef uint32_t L_compat_uptr_t;
};

const char *OSBDefs::oDYNLOADER  = "ld.so";
const char *OSBDefs::oSYSNAME    = "Linux";
const char *OSBDefs::oNODENAME   = "LibertySPARC";
const char *OSBDefs::oRELEASE    = "2.6.31";
const char *OSBDefs::oVERSION    = "Emulated #1 Fri Mar 16 20:05:00 MST 2007";
const char *OSBDefs::oMACHINE    = "SPARC";
const char *OSBDefs::oDOMAINNAME = "unknown";

  const int OSBDefs::oENOSYS;
  const int OSBDefs::oO_CREAT;
  const int OSBDefs::oETIMEDOUT;
  const int OSBDefs::oEBADFD;
  const int OSBDefs::oEOVERFLOW;
  const int OSBDefs::oO_EXCL;
  const int OSBDefs::oO_NOCTTY;
  const int OSBDefs::oO_TRUNC;
  const int OSBDefs::oO_APPEND;
  const int OSBDefs::oO_NONBLOCK;
  const int OSBDefs::oO_NDELAY;
  const int OSBDefs::oO_SYNC;
  const int OSBDefs::oO_DIRECT;
  const int OSBDefs::oO_LARGEFILE;
  const int OSBDefs::oO_DIRECTORY;
  const int OSBDefs::oO_NOFOLLOW;
  const int OSBDefs::oNCC;
  const int OSBDefs::oNCCS;
  const unsigned OSBDefs::oTCGETS;
  const unsigned OSBDefs::oTCGETA;
  const unsigned OSBDefs::oTCSETAW;
  const int OSBDefs::oNUM_DLINFO_ENTRIES;
  const int OSBDefs::oHWCAP;
  const int OSBDefs::oCLKTCK;
  const unsigned OSBDefs::oPAGE_SIZE;
  const unsigned OSBDefs::oPAGE_BITS;
  const uint64_t OSBDefs::oRLIMIT_INFINITY;
  const int OSBDefs::oCLONE_STACK_SIZE;
  const int OSBDefs::oSIGSTOP;
  const int OSBDefs::oSIGTSTP;
  const int OSBDefs::oSIGCONT;
  const int OSBDefs::oSIGCHLD;
  const int OSBDefs::oSIGTTIN;
  const int OSBDefs::oSIGTTOU;
  const int OSBDefs::oSIGURG;
  const int OSBDefs::oSIGWINCH;
  const int OSBDefs::oSIG_BLOCK;
  const int OSBDefs::oSIG_UNBLOCK;
  const int OSBDefs::oSIG_SETMASK;
  const unsigned OSBDefs::oSA_NOCLDWAIT;
  const unsigned OSBDefs::oSA_SIGINFO;
  const unsigned OSBDefs::oSA_ONSTACK;
  const unsigned OSBDefs::oSA_RESTART;
  const unsigned OSBDefs::oSA_RESETHAND;
  const unsigned OSBDefs::oSA_ONESHOT;
  const uint64_t OSBDefs::oTIMESLICE;

struct OSDefs : public OSDerivedDefs<OSBDefs> {

  typedef SPARC_fault_t CC_fault_t;
  
  struct L___sysctl_args {
    LSE_emu_addr_t LSE_name;
    int32_t        LSE_nlen;
    LSE_emu_addr_t LSE_oldval;
    LSE_emu_addr_t LSE_oldlenp;
    LSE_emu_addr_t LSE_newval;
    L_size_t       LSE_newlen;
    uint32_t       unused[4];
  };

  struct L_newstat {
    uint32_t     LSE_st_dev;
    uint32_t     LSE_pad1;
    uint64_t     LSE_st_ino;
    uint32_t     LSE_st_mode;
    uint16_t     LSE_st_nlink;
    uint16_t     LSE_pad2;
    uint32_t     LSE_st_uid;
    uint32_t     LSE_st_gid;
    uint32_t     LSE_st_rdev;
    int64_t      LSE_st_size;
    int64_t      LSE_st_atime;
    int64_t      LSE_st_mtime;
    int64_t      LSE_st_ctime;
    uint64_t     LSE_st_blksize;
    uint64_t     LSE_st_blocks;
    uint64_t     LSE_unused[2];
    L_newstat& operator =(const struct stat &buf) {
      LSE_st_dev     = LSE_h2b((uint32_t)buf.st_dev);
      LSE_pad1 = 0;
      LSE_st_ino     = LSE_h2b((uint64_t)buf.st_ino);
      LSE_st_mode    = LSE_h2b((uint32_t)buf.st_mode);
      LSE_st_nlink   = LSE_h2b((uint16_t)buf.st_nlink);
      LSE_pad2 = 0;
      LSE_st_uid     = LSE_h2b((uint32_t)buf.st_uid);
      LSE_st_gid     = LSE_h2b((uint32_t)buf.st_gid);
      LSE_st_rdev    = LSE_h2b((uint32_t)buf.st_rdev);
      LSE_st_size    = LSE_h2b((int64_t)buf.st_size);
      LSE_st_atime   = LSE_h2b((int64_t)buf.st_atime);
      LSE_st_mtime   = LSE_h2b((int64_t)buf.st_mtime);
      LSE_st_ctime   = LSE_h2b((int64_t)buf.st_ctime);
      LSE_st_blksize = LSE_h2b((uint64_t)buf.st_blksize);
      LSE_st_blocks  = LSE_h2b((uint64_t)buf.st_blocks);
      LSE_unused[0]  = LSE_unused[1] = 0;
    }
  };

  struct L_sigaltstack {
    L_uptr_t LSE_ss_sp;
    int32_t  LSE_ss_flags;
    int32_t  LSE_pad0; // ensure alignment
    L_size_t LSE_ss_size;

    inline L_sigaltstack e2h() {
      L_sigaltstack nb;
      nb.LSE_ss_sp    = LSE_b2h(LSE_ss_sp);
      nb.LSE_ss_flags = LSE_b2h(LSE_ss_flags);
      nb.LSE_ss_size  = LSE_b2h(LSE_ss_size);
      return nb;
    }
    inline L_sigaltstack h2e() { return e2h(); }
  };

  struct L_stat64 { // NOTE: there is a different stat64 for 32-bit sparc!
    uint64_t             LSE_st_dev;
    uint64_t             LSE_st_ino;
    uint64_t             LSE_st_nlink;
    uint32_t             LSE_st_mode; 
    uint32_t             LSE_st_uid; 
    uint32_t             LSE_st_gid; 
    uint32_t             pad;
    uint64_t             LSE_st_rdev; 
    int64_t              LSE_st_size;
    int64_t              LSE_st_blksize; 
    int64_t              LSE_st_blocks;  
    uint64_t             LSE_st_atime;
    uint64_t             LSE_st_atime_nsec; 
    uint64_t             LSE_st_mtime;    
    uint64_t             LSE_st_mtime_nsec; 
    uint64_t             LSE_st_ctime;  
    uint64_t             LSE_st_ctime_nsec; 
    uint64_t             LSE_unused[3]; 
    L_stat64& operator =(const LSE_stat64 &buf) {
      LSE_st_dev     = LSE_h2b((uint64_t)buf.st_dev);
      LSE_st_ino     = LSE_h2b((uint64_t)buf.st_ino);
      LSE_st_nlink   = LSE_h2b((uint64_t)buf.st_nlink);
      LSE_st_mode    = LSE_h2b((uint32_t)buf.st_mode);
      LSE_st_uid     = LSE_h2b((uint32_t)buf.st_uid);
      LSE_st_gid     = LSE_h2b((uint32_t)buf.st_gid);
      LSE_st_rdev    = LSE_h2b((uint64_t)buf.st_rdev);
      pad = 0;
      LSE_st_size    = LSE_h2b((int64_t)buf.st_size);
      LSE_st_blksize = LSE_h2b((int64_t)buf.st_blksize);
      LSE_st_blocks  = LSE_h2b((int64_t)buf.st_blocks);
      LSE_st_atime   = LSE_h2b((uint64_t)buf.st_atime);
      LSE_st_atime_nsec = 0;
      LSE_st_mtime   = LSE_h2b((uint64_t)buf.st_mtime);
      LSE_st_mtime_nsec = 0;
      LSE_st_ctime   = LSE_h2b((uint64_t)buf.st_ctime);
      LSE_st_ctime_nsec = 0;
      LSE_unused[0] = LSE_unused[1] = 0;
    }
  };

  struct L_statfs { // I bet this is wrong!
    int32_t     LSE_f_type;     /* 0 */
    int32_t     LSE_f_bsize;     /* 0 */
    int32_t     LSE_f_blocks;     /* 0 */
    int32_t     LSE_f_bfree;     /* 0 */
    int32_t     LSE_f_bavail;     /* 0 */
    int32_t     LSE_f_files;     /* 0 */
    int32_t     LSE_f_ffree;     /* 0 */
    int32_t     LSE_pad1[2];     /* actually LSE_f_fsid */ 
    int32_t     LSE_f_namelen;     /* 0 */
    int32_t     LSE_f_frsize;     /* 0 */
    int32_t     LSE_f_spare[5];     /* 0 */
    
#if defined (__SVR4) && defined(__sun)
    typedef struct statvfs thisstat;
#else
    typedef struct statfs thisstat;
#endif
    
    L_statfs& operator=(const thisstat &buf) {
#if defined (__SVR4) && defined (__sun)
      LSE_f_type   = LSE_h2b((int32_t)0xEF53);
#else
      LSE_f_type   = LSE_h2b((int32_t)buf.f_type);
#endif
      LSE_f_bsize  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_blocks = LSE_h2b((int32_t)buf.f_type);
      LSE_f_bfree  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_bavail = LSE_h2b((int32_t)buf.f_type);
      LSE_f_files  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_ffree  = LSE_h2b((int32_t)buf.f_type);
      LSE_pad1[0] = LSE_pad1[1] = 0;
#if defined (__SVR4) && defined (__sun)
      LSE_f_namelen = LSE_h2b((int32_t)buf.f_namemax);
#else
      LSE_f_namelen = LSE_h2b((int32_t)buf.f_namelen);
#endif
      LSE_f_frsize = 0;
      for (int i = 0; i < 5; ++i) LSE_f_spare[i] = 0;
    }
  };

  struct L_termios {
    L_tcflag_t LSE_c_iflag;  /* 0-3 */
    L_tcflag_t LSE_c_oflag;  /* 4-7 */
    L_tcflag_t LSE_c_cflag;  /* 8-11 */
    L_tcflag_t LSE_c_lflag;  /* 12-15 */
    L_cc_t     LSE_c_line;       /* 16 */
    L_cc_t     LSE_c_cc[oNCCS]; /* 17-48 */
    L_speed_t  LSE_c_ispeed;  /* 52-55 */
    L_speed_t  LSE_c_ospeed;  /* 56-59 */
#ifdef TCGETS
    L_termios& operator=(const struct termios &buf) {
      LSE_c_iflag = LSE_h2b((L_tcflag_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((L_tcflag_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((L_tcflag_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((L_tcflag_t)buf.c_lflag);
      if (oNCCS <= NCCS) {
	for (int i=0;i<oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<oNCCS;i++) LSE_c_cc[i] = 0;
      }
#ifdef __linux
      LSE_c_line = buf.c_line;
      LSE_c_ispeed = LSE_h2b((L_speed_t)buf.c_ispeed);
      LSE_c_ospeed = LSE_h2b((L_speed_t)buf.c_ospeed);
#else
      LSE_c_line = 0; // N_TTY
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
#endif
    }
#elif defined(TCGETA)
    L_termios& operator=(const struct termio &buf) {
      LSE_c_iflag = LSE_h2b((L_tcflag_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((L_tcflag_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((L_tcflag_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((L_tcflag_t)buf.c_lflag);
      if (oNCCS <= NCCS) {
	for (int i=0;i<oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<oNCCS;i++) LSE_c_cc[i] = 0;
      }
#ifdef __linux
      LSE_c_line = buf.c_line;
#else
      LSE_c_line = 0; // N_TTY
#endif
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
    }
#endif
  };

  struct L_rusage {
    L_timeval LSE_ru_utime;
    L_timeval LSE_ru_stime;
    int32_t   LSE_ru_maxrss;
    int32_t   LSE_ru_ixrss;
    int32_t   LSE_ru_idrss;
    int32_t   LSE_ru_isrss;
    int32_t   LSE_ru_minflt;
    int32_t   LSE_ru_majflt;
    int32_t   LSE_ru_nswap;
    int32_t   LSE_ru_inblock;
    int32_t   LSE_ru_outblock;
    int32_t   LSE_ru_msgsnd;
    int32_t   LSE_ru_msgrcv;
    int32_t   LSE_ru_nsignals;
    int32_t   LSE_ru_nvcsw;
    int32_t   LSE_ru_nivcsw;
  };

#ifdef NOT_NEEDED
  struct L_stat {
    uint32_t             LSE_st_dev;
    uint32_t             LSE___pad1;
    uint64_t             LSE_st_ino;
    uint32_t             LSE_st_mode; 
    int16_t              LSE_st_nlink;
    int16_t              LSE___pad2;
    uint32_t             LSE_st_uid;
    uint32_t             LSE_st_gid;
    uint32_t             LSE_st_rdev;
    uint32_t             LSE___pad3;
    int64_t              LSE_st_size;
    int64_t              LSE_st_atime;
    int64_t              LSE_st_mtime;
    int64_t              LSE_st_ctime;
    int64_t              LSE_st_blksize;
    int64_t              LSE_st_blocks;
    uint64_t             LSE____unused[2];
  };
#endif

  struct L_tms {
    int32_t     LSE_tms_utime;
    int32_t     LSE_tms_stime;
    int32_t     LSE_tms_cutime;
    int32_t     LSE_tms_cstime;
    L_tms & operator=(const struct tms buf) {
      LSE_tms_utime  = LSE_h2b(int32_t(buf.tms_utime));
      LSE_tms_stime  = LSE_h2b(int32_t(buf.tms_stime)); 
      LSE_tms_cutime = LSE_h2b(int32_t(buf.tms_cutime));
      LSE_tms_cstime = LSE_h2b(int32_t(buf.tms_cstime)); 
    }
  };

  struct L_compat_newstat {
    uint32_t     LSE_st_dev;
    uint32_t     LSE_st_ino;
    uint32_t     LSE_st_mode;
    uint16_t     LSE_st_nlink;
    uint32_t     LSE_st_uid;
    uint32_t     LSE_st_gid;
    uint32_t     LSE_st_rdev;
    int32_t      LSE_st_size;
    int32_t      LSE_st_blksize;
    int32_t      LSE_st_blocks;
    uint32_t     LSE_st_atime;
    int32_t      LSE_reserved0;
    uint32_t     LSE_st_mtime;
    int32_t      LSE_reserved1;
    uint32_t     LSE_st_ctime;
    int32_t      LSE_reserved2;
    int32_t      LSE_unused[2];
    L_compat_newstat& operator =(const struct stat &buf) {
      LSE_st_dev     = LSE_h2b((uint32_t)buf.st_dev);
      LSE_st_ino     = LSE_h2b((uint32_t)buf.st_ino);
      LSE_st_mode    = LSE_h2b((uint32_t)buf.st_mode);
      LSE_st_nlink   = LSE_h2b((uint16_t)buf.st_nlink);
      LSE_st_uid     = LSE_h2b((uint32_t)buf.st_uid);
      LSE_st_gid     = LSE_h2b((uint32_t)buf.st_gid);
      LSE_st_rdev    = LSE_h2b((uint32_t)buf.st_rdev);
      LSE_st_size    = LSE_h2b((int32_t)buf.st_size);
      LSE_st_blksize = LSE_h2b((int32_t)buf.st_blksize);
      LSE_st_blocks  = LSE_h2b((int32_t)buf.st_blocks);
      LSE_st_atime   = LSE_h2b((uint32_t)buf.st_atime);
      LSE_reserved0 = 0;
      LSE_st_mtime   = LSE_h2b((uint32_t)buf.st_mtime);
      LSE_reserved1 = 0;
      LSE_st_ctime   = LSE_h2b((uint32_t)buf.st_ctime);
      LSE_reserved2 = 0;
      LSE_unused[0] = LSE_unused[1] = 0;
    }
  };

  struct L_compat_sigaltstack {
    uint32_t        LSE_ss_sp;
    int32_t         LSE_ss_flags;
    int32_t         LSE_ss_size;
    inline L_compat_sigaltstack e2h() {
      L_compat_sigaltstack nb;
      nb.LSE_ss_sp    = LSE_b2h(LSE_ss_sp);
      nb.LSE_ss_flags = LSE_b2h(LSE_ss_flags);
      nb.LSE_ss_size  = LSE_b2h(LSE_ss_size);
      return nb;
    }
    inline L_compat_sigaltstack h2e() { return e2h(); }
  };

  struct L_compat_sigset { 
    // You have signals 1-32 in the lower word and
    // 33-64 in the upper word but each word is big-endian.
    uint32_t sig[2];
    uint64_t toword() {
      uint64_t v = 0;
      v = LSE_b2h(sig[1]);
      v <<= 32;
      v |= LSE_b2h(sig[0]);
      return v;
    }
    void fromword(uint64_t v) { 
      sig[0] = LSE_h2b((uint32_t)v);
      v >>= 32;
      sig[1] = LSE_h2b((uint32_t)v);
    }
  };

#ifdef NOT_NEEDED 
  struct L_compat_stat {
    uint32_t             LSE_st_dev;
    uint32_t             LSE___pad1;
    uint64_t             LSE_st_ino;
    uint32_t             LSE_st_mode; 
    int16_t              LSE_st_nlink;
    int16_t              LSE___pad2;
    uint32_t             LSE_st_uid;
    uint32_t             LSE_st_gid;
    uint32_t             LSE_st_rdev;
    uint32_t             LSE___pad3;
    int64_t             LSE_st_size;
    int64_t             LSE_st_atime;
    int64_t             LSE_st_mtime;
    int64_t             LSE_st_ctime;
    int64_t              LSE_st_blksize;
    int64_t              LSE_st_blocks;
    uint64_t             LSE____unused[2];
  };
#endif

  struct L_compat_stat64 { 
    uint64_t             LSE_st_dev;
    uint64_t             LSE_st_ino;
    uint32_t             LSE_st_mode; 
    uint32_t             LSE_st_nlink;
    uint32_t             LSE_st_uid; 
    uint32_t             LSE_st_gid; 
    uint64_t             LSE_st_rdev; 
    char                 pad[8];
    int64_t              LSE_st_size;
    uint32_t             LSE_st_blksize; 
    char                 pad2[8];
    uint32_t             LSE_st_blocks;  
    uint32_t             LSE_st_atime;
    uint32_t             LSE_st_atime_nsec; 
    uint32_t             LSE_st_mtime;    
    uint32_t             LSE_st_mtime_nsec; 
    uint32_t             LSE_st_ctime;  
    uint32_t             LSE_st_ctime_nsec; 
    uint32_t             LSE_unused[2]; 
    L_compat_stat64& operator =(const LSE_stat64 &buf) {
      LSE_st_dev     = LSE_h2b((uint64_t)buf.st_dev);
      LSE_st_ino     = LSE_h2b((uint64_t)buf.st_ino);
      LSE_st_nlink   = LSE_h2b((uint32_t)buf.st_nlink);
      LSE_st_mode    = LSE_h2b((uint32_t)buf.st_mode);
      LSE_st_uid     = LSE_h2b((uint32_t)buf.st_uid);
      LSE_st_gid     = LSE_h2b((uint32_t)buf.st_gid);
      LSE_st_rdev    = LSE_h2b((uint64_t)buf.st_rdev);
      memset(pad,0,8);
      LSE_st_size    = LSE_h2b((int64_t)buf.st_size);
      LSE_st_blksize = LSE_h2b((uint32_t)buf.st_blksize);
      memset(pad2,0,8);
      LSE_st_blocks  = LSE_h2b((uint32_t)buf.st_blocks);
      LSE_st_atime   = LSE_h2b((uint32_t)buf.st_atime);
      LSE_st_atime_nsec = 0;
      LSE_st_mtime   = LSE_h2b((uint32_t)buf.st_mtime);
      LSE_st_mtime_nsec = 0;
      LSE_st_ctime   = LSE_h2b((uint32_t)buf.st_ctime);
      LSE_st_ctime_nsec = 0;
      LSE_unused[0] = LSE_unused[1] = 0;
    }
  };

  struct L_compat_statfs {
    int32_t     LSE_f_type;     /* 0 */
    int32_t     LSE_f_bsize;     /* 0 */
    int32_t     LSE_f_blocks;     /* 0 */
    int32_t     LSE_f_bfree;     /* 0 */
    int32_t     LSE_f_bavail;     /* 0 */
    int32_t     LSE_f_files;     /* 0 */
    int32_t     LSE_f_ffree;     /* 0 */
    int32_t     LSE_pad1[2];   /* actually LSE_f_fsid */ 
    int32_t     LSE_f_namelen;     /* 0 */
    int32_t     LSE_f_frsize;     /* 0 */
    int32_t     LSE_f_spare[5];     /* 0 */
    
#if defined (__SVR4) && defined(__sun)
    typedef struct statvfs thisstat;
#else
    typedef struct statfs thisstat;
#endif

    L_compat_statfs& operator=(const thisstat &buf) {
#if defined (__SVR4) && defined (__sun)
      LSE_f_type   = LSE_h2b((int32_t)0xEF53);
#else
      LSE_f_type   = LSE_h2b((int32_t)buf.f_type);
#endif
      LSE_f_bsize  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_blocks = LSE_h2b((int32_t)buf.f_type);
      LSE_f_bfree  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_bavail = LSE_h2b((int32_t)buf.f_type);
      LSE_f_files  = LSE_h2b((int32_t)buf.f_type);
      LSE_f_ffree  = LSE_h2b((int32_t)buf.f_type);
      LSE_pad1[0] = LSE_pad1[1] = 0;
#if defined (__SVR4) && defined (__sun)
      LSE_f_namelen = LSE_h2b((int32_t)buf.f_namemax);
#else
      LSE_f_namelen = LSE_h2b((int32_t)buf.f_namelen);
#endif
      LSE_f_frsize = 0;
      for (int i = 0; i < 5; ++i) LSE_f_spare[i] = 0;
    }
  };

  typedef L_termio L_compat_termio;

  struct L_compat_termios {
    L_tcflag_t LSE_c_iflag;  /* 0-3 */
    L_tcflag_t LSE_c_oflag;  /* 4-7 */
    L_tcflag_t LSE_c_cflag;  /* 8-11 */
    L_tcflag_t LSE_c_lflag;  /* 12-15 */
    L_cc_t     LSE_c_line;       /* 16 */
    L_cc_t     LSE_c_cc[oNCCS]; /* 17-48 */
    L_speed_t  LSE_c_ispeed;  /* 52-55 */
    L_speed_t  LSE_c_ospeed;  /* 56-59 */
#ifdef TCGETS
    L_compat_termios& operator=(const struct termios &buf) {
      LSE_c_iflag = LSE_h2b((L_tcflag_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((L_tcflag_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((L_tcflag_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((L_tcflag_t)buf.c_lflag);
      if (oNCCS <= NCCS) {
	for (int i=0;i<oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<oNCCS;i++) LSE_c_cc[i] = 0;
      }
#ifdef __linux
      LSE_c_line = buf.c_line;
      LSE_c_ispeed = LSE_h2b((L_speed_t)buf.c_ispeed);
      LSE_c_ospeed = LSE_h2b((L_speed_t)buf.c_ospeed);
#else
      LSE_c_line = 0; // N_TTY
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
#endif
    }
#elif defined(TCGETA)
    L_compat_termios& operator=(const struct termio &buf) {
      LSE_c_iflag = LSE_h2b((L_tcflag_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2b((L_tcflag_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2b((L_tcflag_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2b((L_tcflag_t)buf.c_lflag);
      if (oNCCS <= NCCS) {
	for (int i=0;i<oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<oNCCS;i++) LSE_c_cc[i] = 0;
      }
#ifdef __linux
      LSE_c_line = buf.c_line;
#else
      LSE_c_line = 0; // N_TTY
#endif
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
    }
#endif
  };

  struct L_siginfo {
    int si_signo;
    int si_errno;
    int si_code;
    uint32_t __pad;
    union {
      int pad[28];
      struct {
	int pid;
	int uid;
	L_sigval_t sigval;
      } normal;
      struct {
	int pid;
	int uid;
	int status;
	L_clock_t utime;
	L_clock_t stime;
      } child;
      struct {
	int tid;
	int overrun;
	L_sigval_t sigval;
	int _private;
      } timer;
      struct {
	uint64_t band;
	int fd;
      } poll;
      struct {
	LSE_emu_addr_t addr;
	int imm;
      } fault;
    } d;
    
    inline struct Linux_siginfo_t tosiginfo();
    inline void fromsiginfo(struct Linux_siginfo_t &v);

    L_siginfo(struct Linux_siginfo_t &v) { fromsiginfo(v); }
    L_siginfo() {}
  };

  struct L_compat_siginfo {
    int si_signo;
    int si_errno;
    int si_code;
    union {
      int pad[29];
      struct {
	int pid;
	int uid;
	L_compat_sigval_t sigval;
      } normal;
      struct {
	int pid;
	int uid;
	int status;
	L_compat_clock_t utime;
	L_compat_clock_t stime;
      } child;
      struct {
	int tid;
	int overrun;
	L_compat_sigval_t sigval;
	int _private;
      } timer;
      struct {
	int band;
	int fd;
      } poll;
      struct {
	uint32_t addr;
	int trapno;
      } fault;
    } d;
    
    inline struct Linux_siginfo_t tosiginfo();
    inline void fromsiginfo(struct Linux_siginfo_t &v);

    L_compat_siginfo(struct Linux_siginfo_t &v) { fromsiginfo(v); }
    L_compat_siginfo() {}
  };

}; // OSDefs

#define LSE_LINUX_CODE
#define LSE_LINUX_USE_DEVMEM
#define CC_ict_osinfo(x)         (x)->osData
#define CC_ict_hwcno(x)          (x)->globalcno
#define CC_ict_startaddr(x)      (x)->PC
#define CC_ict_true_startaddr(x) (x)->PC.PC
#define CC_ict_mem(x)            (x)->mem
#define CC_elftype               0
#define CC_emu_interface(x)      (*((x)->di->einterface))
#define OS_emuvar(x,y)           (((SPARC_dinst_t *)((x)->etoken))->y)
#define OS_ict_emuvar(x,y)       ((x)->di->y)
#define OS_interface(x)          ((Linux_dinst_t *)OS_emuvar(x,Linux_ostoken))
#define OS_interface_set(x,y)    OS_emuvar(x,Linux_ostoken) = (void *)(y)
#define CC_addrsize              (is64bit ? 8 : 4)
#define CC_get_stackptr(x)       (*get_r_ptr(x,14,(x)->PR[CWP]))

#define CC_context_has_no_users(x) (!((CC_isacontext_t *)((x)->isacont)->users))

static inline void CC_sys_set_return(CC_isacontext_t *realct, int which,
				     uint64_t val) {
  if (which == 0) {
    realct->ASR[CCR] = Linux_is64bit_context(realct) ?
      (realct->ASR[CCR]&~uint64_t(0x10)| (val ? 0x10 : 0)) :
      (realct->ASR[CCR]&~uint64_t(0x01)| (val ? 0x01 : 0));
  } else if (which == 1) {
    *get_r_ptr(realct,8,realct->PR[CWP]) = val;
  } else { // only used in SPARC and used only when copying a context
    *get_r_ptr(realct,9,realct->PR[CWP]) = val;
  }
}

static inline uint64_t CC_sys_get_return(CC_isacontext_t *realct, int which) {
  // TODO: not right for 64-bit!
  if (which == 0) return realct->ASR[CCR] & 1;
  else return *get_r_ptr(realct, 8, realct->PR[CWP]);
}

#define CC_open_filter(fname) (!strcmp(fname,"/etc/localtime"))

static int CC_elfarchchecker(int foundflag, void *ct) {
  return foundflag == 2 || foundflag == 18 || foundflag == 43;
}

#define CC_contextdecl_hook() bool insyscall; uint64_t orig_i0;
#define CC_contextinit_hook() insyscall = false;

#define CC_execveguts() SPARC_execveguts(realct, start_stack, osct, is64bit)

static inline void SPARC_execveguts(CC_isacontext_t *realct,
				    LSE_emu_addr_t start_stack,
				    struct Linux_context_t *osct,
				    bool is64bit);

  static void SPARC_cloneguts(LSE_emu_addr_t sp,
			      CC_isacontext_t *oldisact,
			      CC_isacontext_t *newisact,
			      LSE_emu_addr_t tlsp, int flags,
			      Linux_context_t *newosct);

#define CC_context_copy(i,ppc,pc,cm) EMU_context_copy(i,ppc,pc)
#define CC_context_destroy(i,cp) EMU_context_destroy(i,cp)
  // cloneguts is not complete: it needs to set the stack and TLS 

#define CC_cloneguts() \
  SPARC_cloneguts(stackstart,oldisact,\
		  newisact,tlsp,flags,newosct)

// TODO: figure out how to do this
#define CC_sigreturn(n) SPARC_sigreturn(n,realct,osct,sigmask);

static inline void SPARC_sigreturn(int callno, CC_isacontext_t *realct,
				   struct Linux_context_t *osct, 
				   uint64_t &sigmask);

#define CC_invalidate_tlb(ctx) \
  do { (ctx)->mmu.clear(); } while (0)
#define CC_drop_mapping(os, va, pa)
#undef CC_mem_write

#define CC_obtain_write_permission(ct,va,ln) (false)

#define CC_chkpt_cntl_clear(c)
#define CC_ict_write_chkpt(c,ci) EMU_write_context(cptFile,nmark,c,ci)
#define CC_ict_parse_chkpt(c)                            \
  if ((cerr = EMU_parse_context(intr, cptFile, c, ctl))) \
    return cerr;

#define CC_context_map_update(c,p2,p3) do { \
    *get_asr_ptr(c,26) = p3 ? 1 : 0;       \
    (LSE_emu_update_context_map)(p2, p3);  \
  } while (0)

#define CC_arg_int(n,s) (*get_r_ptr(realct,n+8,realct->PR[CWP]))
#define CC_arg_ptr(n,s) ((*get_r_ptr(realct,n+8,realct->PR[CWP])) \
  & ((realct->PR[PSTATE]&pstate_amM)?bits(32):bits(64)))


#define CC_time_get_currtime(x) do_get_currtime(x)
#define CC_time_schedule_wakeup(t,f,di) do_schedule_wakeup(t,f,di)
#define CC_time_request_interrupt(os,n)	do_request_interrupt(os,n)

  static inline int do_request_interrupt(class Linux_dinst_t *os, int hwcno);
  static inline uint64_t do_get_currtime(class Linux_dinst_t *os);
  static void do_schedule_wakeup(uint64_t attime,
				 void (*f)(void *),
				 struct Linux_dinst_t *os);


#include "OS/LSE_Linux.h"
  
  static inline uint64_t do_get_currtime(Linux_dinst_t *os) {
    SPARC_dinst_t *di = ((SPARC_dinst_t *)(os->eint->etoken));
    LSE_clock_t clock 
      = di->hwclocks.size() ? di->hwclocks.begin()->second.c[0] : 0;

    if (!clock) return 0;
    if (clock) {
      // should be starttime + clock * scale factor
      return clock->get_cycle();
    }
  }

  static void do_schedule_wakeup(uint64_t attime,
				 void (*f)(void *),
				 Linux_dinst_t *os) {
    SPARC_dinst_t *di = ((SPARC_dinst_t *)(os->eint->etoken));
    LSE_clock_t clock 
      = di->hwclocks.size() ? di->hwclocks.begin()->second.c[0] : 0;

    if (clock) {
      // convert from ns to clock cycles
      // (attime - starttime) * scale factor for clock = clock cycles;
      uint64_t clockcycles = attime; // for the moment, 1 GHz clock always...
      clock->add_todo(clockcycles, f, os);
    }
  }

  static inline int do_request_interrupt(Linux_dinst_t *os, int hwcno) {
    SPARC_dinst_t *di = ((SPARC_dinst_t *)(os->eint->etoken));
    LSE_emu_interrupt_callback_t &ic = di->interrupts[hwcno];
    if (ic.handler)
      return (*ic.handler)(hwcno, ic.data);
    else return false;
  }

  inline struct Linux_siginfo_t OSDefs::L_siginfo::tosiginfo() { 
    struct Linux_siginfo_t nv; 
    nv.LSEsi_signo = LSE_b2h(si_signo);
    nv.LSEsi_errno = LSE_b2h(si_errno);
    nv.LSEsi_code = LSE_b2h(si_code);
    switch (nv.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      nv.LSEsi_pid = LSE_b2h(d.normal.pid);
      nv.LSEsi_uid = LSE_b2h(d.normal.uid);
      break;
    case OSDefs::oCLD_EXITED:
      nv.LSEsi_pid = LSE_b2h(d.child.pid);
      nv.LSEsi_uid = LSE_b2h(d.child.uid);
      nv.LSEsi_status = LSE_b2h(d.child.status);
      nv.LSEsi_utime = LSE_b2h(d.child.utime);
      nv.LSEsi_stime = LSE_b2h(d.child.stime);
      break;
    default: 
      memcpy(nv.rawbytes, d.pad, 28*4); // just keep the bytes
      break;
    }
    return nv;
  }

  inline void OSDefs::L_siginfo::fromsiginfo(struct Linux_siginfo_t &v) {
    si_signo = LSE_h2b((int)v.LSEsi_signo);
    si_errno = LSE_h2b((int)v.LSEsi_errno);
    si_code = LSE_h2b((int)v.LSEsi_code);
    switch (v.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      d.normal.pid = LSE_h2b((int)v.LSEsi_pid);
      d.normal.uid = LSE_h2b((int)v.LSEsi_uid);
      break;
    case OSDefs::oCLD_EXITED:
      d.child.pid = LSE_h2b((int)v.LSEsi_pid);
      d.child.uid = LSE_h2b((int)v.LSEsi_uid);
      d.child.status = LSE_h2b((int)v.LSEsi_status);
      d.child.utime = LSE_h2b((typeof d.child.utime)v.LSEsi_utime);
      d.child.stime = LSE_h2b((typeof d.child.stime)v.LSEsi_stime);
      break;
    default: 
      memcpy(d.pad, v.rawbytes, 28*4); // just keep the bytes
    }
  }

  inline struct Linux_siginfo_t OSDefs::L_compat_siginfo::tosiginfo() { 
    struct Linux_siginfo_t nv; 
    nv.LSEsi_signo = LSE_b2h(si_signo);
    nv.LSEsi_errno = LSE_b2h(si_errno);
    nv.LSEsi_code = LSE_b2h(si_code);
    switch (nv.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      nv.LSEsi_pid = LSE_b2h(d.normal.pid);
      nv.LSEsi_uid = LSE_b2h(d.normal.uid);
      break;
    case OSDefs::oCLD_EXITED:
      nv.LSEsi_pid = LSE_b2h(d.child.pid);
      nv.LSEsi_uid = LSE_b2h(d.child.uid);
      nv.LSEsi_status = LSE_b2h(d.child.status);
      nv.LSEsi_utime = LSE_b2h(d.child.utime);
      nv.LSEsi_stime = LSE_b2h(d.child.stime);
      break;
    default: 
      memcpy(nv.rawbytes, d.pad, sizeof(d.pad));
      break;
    }
    return nv;
  }

  inline void OSDefs::L_compat_siginfo::fromsiginfo(struct Linux_siginfo_t &v) {
    si_signo = LSE_h2b((int)v.LSEsi_signo);
    si_errno = LSE_h2b((int)v.LSEsi_errno);
    si_code = LSE_h2b((int)v.LSEsi_code);
    switch (v.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      d.normal.pid = LSE_h2b((int)v.LSEsi_pid);
      d.normal.uid = LSE_h2b((int)v.LSEsi_uid);
      break;
    case OSDefs::oCLD_EXITED:
      d.child.pid = LSE_h2b((int)v.LSEsi_pid);
      d.child.uid = LSE_h2b((int)v.LSEsi_uid);
      d.child.status = LSE_h2b((int)v.LSEsi_status);
      d.child.utime = LSE_h2b((typeof d.child.utime)v.LSEsi_utime);
      d.child.stime = LSE_h2b((typeof d.child.stime)v.LSEsi_stime);
      break;
    default: 
      memcpy(d.pad, v.rawbytes, sizeof(d.pad));
    }
  }

  // Note: this sp will be the stack base passed by the user.  We must
  // set it to the frame pointer if it is zero.
  // 0 = flags, 1 = base, 2 = sp+PTREGS_OFF.  3 = 0
  static inline void SPARC_cloneguts(LSE_emu_addr_t sp,
				     CC_isacontext_t *oldisact,
				     CC_isacontext_t *newisact,
				     LSE_emu_addr_t tlsp, int flags,
				     Linux_context_t *newosct) {

    // If we are not given a stack pointer, make it the frame pointer
    if (!sp) sp = *get_r_ptr(oldisact,14,oldisact->PR[CWP]);
    
    // At this point, SPARC's copy_thread copies the caller's frame,
    // which contains saved values for ins/globals onto the kernel stack
    // Because I do not really maintain a kernel stack, I do not need that.

    if (!newosct->is64bit) {
      sp &= 0xffffffffUL;
      *get_r_ptr(oldisact,14,oldisact->PR[CWP]) &= 0xffffffffUL;
    }
    
    *get_r_ptr(newisact,14,newisact->PR[CWP]) = sp;
    
    if (sp != *get_r_ptr(oldisact,14,oldisact->PR[CWP])) {
      // Must copy the caller stack frame
      LSE_emu_addr_t fp;
      LSE_emu_addr_t oldsp = *get_r_ptr(oldisact,14,oldisact->PR[CWP]);
      if (newosct->is64bit) {
	sp += bias64;
	oldsp += bias64;
	OS_mem_read(oldisact, oldsp+14*8, 8, &fp);
	fp = LSE_b2h(fp) + bias64;
      } else {
	uint32_t tmp;
	OS_mem_read(oldisact, oldsp+14*4, 4, &tmp);
	fp = LSE_b2h(tmp);
      }
      sp &= ~uint64_t(7);
      LSE_emu_addr_t bytestocopy = fp - oldsp;
      {
	LSE_emu_addr_t togo = bytestocopy;
	LSE_emu_addr_t f = oldsp;
	LSE_emu_addr_t t = sp - bytestocopy;
	while (togo) {
	  char buf[256];
	  int todo = std::min(togo,LSE_emu_addr_t(256));
	  OS_mem_read(oldisact, f, todo, buf);
	  OS_mem_write(newisact, t, todo, buf);
	  f += todo;
	  t += todo;
	  togo -= todo;
	}
      }
      if (newosct->is64bit) {
	LSE_emu_addr_t nv = LSE_h2b(sp - bias64);
	OS_mem_write(newisact, sp - bytestocopy + 14 * 8, 8, &nv);
	sp -= bias64;
      } else {
	uint32_t nv = LSE_h2b(uint32_t(sp));
	OS_mem_write(newisact, sp - bytestocopy + 14 * 4, 4, &nv);
      }
      *get_r_ptr(newisact,14,newisact->PR[CWP]) = sp - bytestocopy;
    }

    // Strange ABI for fork changes the parent context
    CC_sys_set_return(newisact, 0, 0);
    CC_sys_set_return(newisact, 1, newosct->tid);
    CC_sys_set_return(newisact, 2, 1);
    CC_sys_set_return(oldisact, 2, 0);

    if (flags & OSDefs::oCLONE_SETTLS) {
      *get_r_ptr(newisact,7,0) = tlsp;
    }
  }



static inline void SPARC_execveguts(CC_isacontext_t *realct,
				    LSE_emu_addr_t start_stack,
				    struct Linux_context_t *osct,
				    bool is64bit) {

  realct->PC.NPC = realct->PC.PC + 4;
  realct->PC.annul = false;
  realct->PC.check_level_zero = false;
  realct->PC.interrupted = false;

  // initial stack pointer has to have a frame reserved, plus usebias, plus
  // another 0x10 to make it all line up properly.
  *get_r_ptr(realct,14,0) = start_stack
				 - (osct->is64bit ? ((16*8)+bias64-0x10) :
                                  (16*4+bias32-0x10));

  *get_asr_ptr(realct, FPRS) = 0x4;
  // PSTATE
  *get_pr_ptr(realct, PSTATE) = pstate_pefM | (is64bit ? 0 : pstate_amM);

  *get_pr_ptr(realct, CANRESTORE) = 0;
  *get_pr_ptr(realct, OTHERWIN) = 0;
  *get_pr_ptr(realct, CLEANWIN) = 0;
  *get_pr_ptr(realct, CWP) = 0;
  *get_pr_ptr(realct, CANSAVE) = N_REG_WINDOWS-2;
  *get_pr_ptr(realct, GL) = 0;

  *get_hpr_ptr(realct, HPSTATE) = 0;
  *get_pr_ptr(realct, TT, MAXTL) = 0;
  *get_pr_ptr(realct, TL) = 0;
  *get_pr_ptr(realct, GL) = 0;
  *get_asr_ptr(realct, TICKP) = 0;
  *get_asr_ptr(realct, TICK_CMPR) = 0;
  *get_asr_ptr(realct, STICK_CMPR) = 0;
  *get_hpr_ptr(realct, HSTICK_CMPR) = 0;
  *get_asr_ptr(realct, TICKP) = 0;
  *get_asr_ptr(realct, STICK) = 0;

  *get_r_ptr(realct, 1) = 0;     // initial g1 should be zero; DL will modify
  *get_r_ptr(realct, 30, 0) = 0; // initial fp should be zero.

  // *get_asr_ptr(realct, 26) = realct->globalcno == 1 ? 1 : 0;
}
     
  struct SPARC_mcontext {
    // fp will be at reg[19] and _i7 at reg[20]
    uint64_t reg[21]; // need g1(4) to be at 0x40 .. thus, need 0x20 for start
    uint32_t fpreg[32*2];
    uint64_t fpustat[3]; // fsr does not show up in setjmp headers.
    unsigned char   fp_stuff[4];
  };
             
  struct SPARC_ucontext {
    uint64_t link; // 0
    uint64_t flags; // 8
    OSDefs::L_sigset sigmask; // 0x10
    uint64_t _pad1; // odd padding... without it, nothing works!
    SPARC_mcontext mctx;
    int savecontext(LSE_emu_isacontext_t *realct);
    int setcontext(LSE_emu_isacontext_t *realct, bool);
  };

  int SPARC_ucontext::savecontext(LSE_emu_isacontext_t *realct) {
    Linux_context_t *osct = OS_ict_osinfo(realct);

    link = 0;
    flags = 0;
    sigmask.fromword(osct->sigblocked);

    SPARC_fault_t fault;
    flush_register_windows(realct, fault, osct->is64bit, true);

    int cwp = *get_pr_ptr(realct, CWP);
    
    mctx.reg[0] = LSE_h2b((*get_pr_ptr(realct,GL) << tstate_glO) |
			  (*get_asr_ptr(realct, CCR) << tstate_ccrO) |
			  (*get_asr_ptr(realct, ASI) << tstate_asiO) |
			  (*get_pr_ptr(realct,PSTATE) << tstate_pstateO) | 
			  (osct->insyscall << 5) |
			  (*get_pr_ptr(realct, CWP) << tstate_cwpO));

    mctx.reg[1] = LSE_h2b(realct->PC.PC);   // already incremented over trap
    mctx.reg[2] = LSE_h2b(realct->PC.PC+4);
    mctx.reg[3] = LSE_h2b(*get_asr_ptr(realct,Y));
    mctx.reg[4] = LSE_h2b(*get_r_ptr(realct,1)); // globals
    mctx.reg[5] = LSE_h2b(*get_r_ptr(realct,2));
    mctx.reg[6] = LSE_h2b(*get_r_ptr(realct,3));
    mctx.reg[7] = LSE_h2b(*get_r_ptr(realct,4));
    mctx.reg[8] = LSE_h2b(*get_r_ptr(realct,5));
    mctx.reg[9] = LSE_h2b(*get_r_ptr(realct,6));
    mctx.reg[10] = LSE_h2b(*get_r_ptr(realct,7));
    mctx.reg[11] = LSE_h2b(*get_r_ptr(realct,8,cwp));
    mctx.reg[12] = LSE_h2b(*get_r_ptr(realct,9,cwp));
    mctx.reg[13] = LSE_h2b(*get_r_ptr(realct,10,cwp));
    mctx.reg[14] = LSE_h2b(*get_r_ptr(realct,11,cwp));
    mctx.reg[15] = LSE_h2b(*get_r_ptr(realct,12,cwp));
    mctx.reg[16] = LSE_h2b(*get_r_ptr(realct,13,cwp));
    mctx.reg[17] = LSE_h2b(*get_r_ptr(realct,14,cwp));
    mctx.reg[18] = LSE_h2b(*get_r_ptr(realct,15,cwp));

    uint64_t fp = *get_r_ptr(realct,14,cwp) + (osct->is64bit ? 0x7ff : 0);

    if (osct->is64bit) {
      OS_mem_read(realct, fp + 8 * 14, 8, (CC_mem_data_t *)&mctx.reg[19]);
      OS_mem_read(realct, fp + 8 * 15, 8, (CC_mem_data_t *)&mctx.reg[20]);
    } else {
      uint32_t nv;
      OS_mem_read(realct, fp + 4 * 14, 4, (CC_mem_data_t *)&nv);
      mctx.reg[19] = nv;
      OS_mem_read(realct, fp + 4 * 15, 4, (CC_mem_data_t *)&nv);
      mctx.reg[20] = nv;
    }
    
    for (int ii = 0; ii < 64; ii++) mctx.fpreg[ii] = LSE_h2b(realct->F[ii]);
    
    mctx.fpustat[0] = LSE_h2b(realct->FSR);
    mctx.fpustat[1] = LSE_h2b(*get_asr_ptr(realct,GSR));
    mctx.fpustat[2] = LSE_h2b(*get_asr_ptr(realct,FPRS));
    return 0;
  syserr:
    return -1;
  }

  int SPARC_ucontext::setcontext(LSE_emu_isacontext_t *realct, 
				 bool restoresig) {
    Linux_context_t *osct = OS_ict_osinfo(realct);    
    SPARC_fault_t fault;
    uint64_t tstate, fp;

    flush_register_windows(realct, fault, osct->is64bit, true);

    if (restoresig) {
      osct->sigblocked = sigmask.toword();
      osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
	(osct->tgroup->sigpending & ~osct->sigblocked);
    }

    tstate = LSE_b2h(mctx.reg[0]);
  
    *get_pr_ptr(realct, GL) = (tstate & tstate_glM) >> tstate_glO;
    *get_asr_ptr(realct, CCR) = (tstate & tstate_ccrM) >> tstate_ccrO;
    *get_asr_ptr(realct, ASI) = (tstate & tstate_asiM) >> tstate_asiO;
    *get_pr_ptr(realct, PSTATE) = (tstate & tstate_pstateM) >> tstate_pstateO;
    *get_pr_ptr(realct, CWP) = (tstate & tstate_cwpM) >> tstate_cwpO;
    osct->insyscall = (tstate & 0x20) ? true : false;

    int cwp = *get_pr_ptr(realct, CWP, 0); // current WP.
    
    realct->PC.PC = LSE_b2h(mctx.reg[1]);
    realct->PC.NPC = LSE_b2h(mctx.reg[2]);
    
    *get_asr_ptr(realct,Y) = LSE_b2h(mctx.reg[3]);
    
    *get_r_ptr(realct,1) = LSE_b2h(mctx.reg[4]); // globals
    *get_r_ptr(realct,2) = LSE_b2h(mctx.reg[5]);
    *get_r_ptr(realct,3) = LSE_b2h(mctx.reg[6]);
    *get_r_ptr(realct,4) = LSE_b2h(mctx.reg[7]);
    *get_r_ptr(realct,5) = LSE_b2h(mctx.reg[8]);
    *get_r_ptr(realct,6) = LSE_b2h(mctx.reg[9]);
    // *get_r_ptr(realct,7) = LSE_b2h(mctx.reg[10]); // do not do g7?
    *get_r_ptr(realct,8,cwp) = LSE_b2h(mctx.reg[11]);
    *get_r_ptr(realct,9,cwp) = LSE_b2h(mctx.reg[12]);
    *get_r_ptr(realct,10,cwp) = LSE_b2h(mctx.reg[13]);
    *get_r_ptr(realct,11,cwp) = LSE_b2h(mctx.reg[14]);
    *get_r_ptr(realct,12,cwp) = LSE_b2h(mctx.reg[15]);
    *get_r_ptr(realct,13,cwp) = LSE_b2h(mctx.reg[16]);
    fp = *get_r_ptr(realct,14,cwp) = LSE_b2h(mctx.reg[17]);
    *get_r_ptr(realct,15,cwp) = LSE_b2h(mctx.reg[18]);
    
    if (osct->is64bit) {
      fp += 0x7ff;
      OS_mem_write(realct, fp + 8 * 14, 8, (CC_mem_data_t *)&mctx.reg[19]);
      OS_mem_write(realct, fp + 8 * 15, 8, (CC_mem_data_t *)&mctx.reg[20]);
    } else {
      uint32_t nv = uint32_t(mctx.reg[19]);
      OS_mem_write(realct, fp + 4 * 14, 4, (CC_mem_data_t *)&nv);
      nv = uint32_t(mctx.reg[20]);
      OS_mem_write(realct, fp + 4 * 15, 5, (CC_mem_data_t *)&nv);
    }
    
    for (int ii = 0; ii < 64; ii++) realct->F[ii] = LSE_b2h(mctx.fpreg[ii]);

    realct->FSR = LSE_b2h(mctx.fpustat[0]);
    *get_asr_ptr(realct,GSR) = LSE_b2h(mctx.fpustat[1]);
    *get_asr_ptr(realct,FPRS) = LSE_b2h(mctx.fpustat[2]);

    fill_register_window(realct, cwp, fault, osct->is64bit, true);

    return 0;
  syserr:
    return -1;
  }

static LSE_LINUX_SIG(SPARC_getcontext) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;  // 1 = do not handle signals because we blocked.
  LSE_emu_addr_t targbuf;
  SPARC_ucontext tempbuf;
  int ret=0;
  uint64_t fp;

  targbuf = CC_arg_ptr(0,"p");
  
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
          "SYSCALL(%d): getcontext(0x" LSE_emu_addr_t_print_format ")\n",
	    osct->tid, targbuf);

  tempbuf.savecontext(realct);

  ret = 0;

  OS_mem_write(realct, targbuf, sizeof(tempbuf), (CC_mem_data_t *)&tempbuf);

  OS_sys_return();
syserr:
  OS_syserr_handler();
}

static LSE_LINUX_SIG(SPARC_setcontext) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;  // 1 = do not handle signals because we blocked.
  LSE_emu_addr_t targbuf;
  SPARC_ucontext tempbuf;
  int ret=0;
  bool dosig;

  targbuf = CC_arg_ptr(0,"pi");
  dosig = !!CC_arg_int(1,"pi");
  
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
          "SYSCALL(%d): setcontext(0x" LSE_emu_addr_t_print_format ",%d)\n",
	    osct->tid, targbuf, dosig);

  OS_mem_read(realct, targbuf, sizeof(tempbuf), (CC_mem_data_t *)&tempbuf);

  ret = tempbuf.setcontext(realct, dosig);

  OS_sys_return();
syserr:
  OS_syserr_handler();
}

template<class T> static inline int
SPARC_rt_sigaction_body(LSE_LINUX_PARMS, const char *name) {

  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int sig,sz;
  LSE_emu_addr_t act, oldact, rstr;
  T tempbuf, buf;
  int ret=0;

  sig    = CC_arg_int(0,"ipppi");
  act    = CC_arg_ptr(1,"ipppi");
  oldact = CC_arg_ptr(2,"ipppi");
  rstr   = CC_arg_ptr(3,"ipppi");
  sz     = CC_arg_int(4,"ipppi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
          "SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format ",0x" 
	    LSE_emu_addr_t_print_format ",0x"
	    LSE_emu_addr_t_print_format ",%d) = ",
	    osct->tid, name, sig, act, oldact, rstr, sz);

  // valid signal numbers
  sig--;
  if (sig < 0 || sig > 63 || sz != sizeof(OSDefs::L_sigset) ||
      ( (uint64_t(1)<<sig) & Linux_sighandinf_t::cantChangeMask ))
    OS_report_err(OSDefs::oEINVAL);

  if (osct->sigPendingFlag) OS_report_err(OSDefs::oERESTARTNOINTR);

  if (oldact) { 
    tempbuf = osct->sighandtable->handlers[sig];
    OS_memcpy_h2t(oldact, &tempbuf, sizeof(tempbuf));
  }

  if (act) {

    // update the action
    OS_memcpy_t2h(&tempbuf, act, sizeof(tempbuf));

    Linux_sighandinf_t &sh = osct->sighandtable->handlers[sig];
    sh = tempbuf.tosighand();
    sh.sigmask &= ~Linux_sighandinf_t::cantChangeMask;
    sh.restorer = rstr;
    if (sh.handler == 1 ||
        sh.handler == 0 && (OSDefs::oSIGDEF_IGNORE() &(uint64_t(1)<<(sig-1)))) {

      // Remove signals from pending masks and queues when set to ignore

      uint64_t mask = uint64_t(1) << (sig - 1);

      if (mask & osct->tgroup->sigpending) {

        osct->tgroup->sigpending &= ~mask;

	for (std::list<Linux_siginfo_t>::iterator 
	     j = osct->tgroup->signals.begin(), 
	     je = osct->tgroup->signals.end(); j != je; ) {
          if (j->LSEsi_signo == sig) j = osct->tgroup->signals.erase(j);
	  else j++;
        }
      }

      std::list<Linux_context_t *> &tgl = osct->tgroup->members;
      for (std::list<Linux_context_t *>::iterator i = tgl.begin(), ie=tgl.end();
           i != ie; ++i) {

	if (!(mask & (*i)->sigpending)) continue;
        (*i)->sigpending &= ~mask;

	for (std::list<Linux_siginfo_t>::iterator 
	     j = (*i)->signals.begin(), je = (*i)->signals.end(); j != je; ) {
          if (j->LSEsi_signo == sig) j = (*i)->signals.erase(j);
	  else j++;
        }
        (*i)->sigPendingFlag = ((*i)->sigpending & ~(*i)->sigblocked) || 
  		             ((*i)->tgroup->sigpending & ~(*i)->sigblocked);
      }
    } // setting handler to ignore
  }

  OS_report_results(0, ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(SPARC_rt_sigaction) {
  return SPARC_rt_sigaction_body<OSDefs::L_sigaction>(LSE_LINUX_ARGS,
						      "rt_sigaction");
}

static LSE_LINUX_SIG(SPARC_rt_sigaction32) {
  return SPARC_rt_sigaction_body<OSDefs::L_compat_sigaction>(LSE_LINUX_ARGS,
							     "rt_sigaction32");
}

  struct SPARC_rtsignalframe {
    uint64_t normalframe[24];
    OSDefs::L_siginfo info;
    struct {
      uint64_t r[16];
      uint64_t tstate;
      uint64_t tpc;
      uint64_t tnpc;
      uint32_t y;
      uint32_t magic; // 57ac6c00
    } pt;
    LSE_emu_addr_t fpuptr;
    OSDefs::L_sigaltstack stack;
    uint64_t sigmask;
    struct {
      uint32_t fpreg[64];
      uint64_t fsr;
      uint64_t gsr;
      uint64_t fprs;
    } fpustate;
  };

  struct SPARC_rtsignalframe32 {
    uint32_t normalframe[24]; 
    OSDefs::L_compat_siginfo info;
    struct {
      uint32_t tstate;
      uint32_t tpc;
      uint32_t tnpc;
      uint32_t y;
      uint32_t r[16];
    } pt;
    uint32_t sigmask[2];
    uint32_t fpuptr;
    uint32_t tramp[2];
    OSDefs::L_compat_sigaltstack stack;
    uint32_t v8extra_size;
    uint32_t v8extras[17];
    struct {
      uint32_t fpreg[64];
      uint64_t fsr;
      uint64_t gsr;
      uint64_t fprs;
    } fpustate;
  };

  struct SPARC_signalframe32 {
    uint32_t normalframe[24]; 
    struct {
      uint32_t tstate;
      uint32_t tpc;
      uint32_t tnpc;
      uint32_t y;
      uint32_t r[16];
    } pt;
    uint32_t sigmask;
    uint32_t fpuptr;
    uint32_t tramp[2];
    uint32_t sigmask_extra[1];
    uint32_t v8extra_size;
    uint32_t v8extras[17];
    struct {
      uint32_t fpreg[64];
      uint64_t fsr;
      uint64_t gsr;
      uint64_t fprs;
    } fpustate;
  };

static inline void SPARC_sigreturn(int callno, CC_isacontext_t *realct,
				 struct Linux_context_t *osct, 
				 uint64_t &sigmask) {

  int cwp = realct->PR[CWP];

  LSE_emu_addr_t sp = *get_r_ptr(realct, 14, cwp);
  LSE_emu_addr_t origsp = sp;
  if (osct->is64bit) sp += 0x7ff;

#define frameoffset(Y) ((char *)&sf.Y - (char *)&sf)

  if (callno == 1101) { /* 64-bit rt_sigreturn call */
    SPARC_rtsignalframe sf;

    // copy from target space
    OS_mem_read(realct, sp, sizeof(SPARC_rtsignalframe), (CC_mem_data_t*)&sf);

    //fprintf(stderr,"return from signal %d ", LSE_b2h(sf.info.si_signo));

    sigmask = LSE_b2h(sf.sigmask);

    realct->PC.PC = LSE_b2h(sf.pt.tpc);
    realct->PC.NPC = LSE_b2h(sf.pt.tnpc);
    realct->PC.annul = false;
    realct->PC.check_level_zero = false;
    realct->PC.interrupted = false;

    if (!osct->is64bit) {
      realct->PC.PC &= 0xffffffffU;
      realct->PC.NPC &= 0xffffffffU;
    }
    *get_asr_ptr(realct,Y) = LSE_b2h(sf.pt.y);
    uint64_t tstate = LSE_b2h(sf.pt.tstate);
    // only some fields should be set.
    *get_asr_ptr(realct, CCR) = (tstate & tstate_ccrM) >> tstate_ccrO;
    *get_asr_ptr(realct, ASI) = (tstate & tstate_asiM) >> tstate_asiO;
    osct->insyscall = false;

    for (int i = 1; i < 16; ++i) 
      *get_r_ptr(realct, i, cwp) = LSE_b2h(sf.pt.r[i]);

    SPARC_fault_t fault;
    fill_register_window(realct, cwp, fault, osct->is64bit, true);

    for (int ii = 0; ii < 64; ii++) 
      realct->F[ii] = LSE_b2h(sf.fpustate.fpreg[ii]);
    
    realct->FSR = LSE_b2h(sf.fpustate.fsr);
    *get_asr_ptr(realct,GSR) = LSE_b2h(sf.fpustate.gsr);
    *get_asr_ptr(realct,FPRS) = LSE_h2b(sf.fpustate.fprs);

    handle_sigaltstack(realct, osct, sp+frameoffset(stack), 
		       0, CC_get_stackptr(realct));
  }
  else if (callno == 101) { /* 32-bit rt_sigreturn call */
    SPARC_rtsignalframe32 sf;

    *get_r_ptr(realct, 14, cwp) &= 0xffffffffUL;

    // copy from target space
    OS_mem_read(realct, sp, sizeof(SPARC_rtsignalframe32), (CC_mem_data_t*)&sf);

    //fprintf(stderr,"return from signal %d ", LSE_b2h(sf.info.si_signo));

    sigmask = ( LSE_b2h(sf.sigmask[0]) |
		((uint64_t)LSE_b2h(sf.sigmask[1]) << 32) );

    realct->PC.PC = LSE_b2h(sf.pt.tpc);
    realct->PC.NPC = LSE_b2h(sf.pt.tnpc);
    realct->PC.annul = false;
    realct->PC.check_level_zero = false;
    realct->PC.interrupted = false;
    realct->PC.PC &= 0xffffffffU;
    realct->PC.NPC &= 0xffffffffU;

    *get_asr_ptr(realct,Y) = LSE_b2h(sf.pt.y);

    uint32_t psr = LSE_b2h(sf.pt.tstate);
    uint64_t tstate = ( (((uint64_t)(psr & 0xf00000)) << 12) |
			(((uint64_t)(psr & 0xf0000)) << 20) );

    // only some fields should be set.
    *get_asr_ptr(realct, CCR) = (tstate & tstate_ccrM) >> tstate_ccrO;
    *get_asr_ptr(realct, ASI) = LSE_b2h(sf.v8extras[16]);
    osct->insyscall = false;

    for (int i = 1; i < 16; ++i) {
      *get_r_ptr(realct, i, cwp) = LSE_b2h(sf.pt.r[i]);
      *get_r_ptr(realct, i, cwp) |= ((uint64_t)LSE_b2h(sf.v8extras[i]))<<32;
    }

    SPARC_fault_t fault;
    fill_register_window(realct, cwp, fault, osct->is64bit, true);

    for (int ii = 0; ii < 64; ii++) 
      realct->F[ii] = LSE_b2h(sf.fpustate.fpreg[ii]);
    
    realct->FSR = LSE_b2h(sf.fpustate.fsr);
    *get_asr_ptr(realct,GSR) = LSE_b2h(sf.fpustate.gsr);
    *get_asr_ptr(realct,FPRS) = LSE_h2b(sf.fpustate.fprs);

    handle_sigaltstack(realct, osct, sp+frameoffset(stack), 
		       0, CC_get_stackptr(realct));
  } else { /* 32-bit sigreturn call : 216 */
    SPARC_signalframe32 sf;

    *get_r_ptr(realct, 14, cwp) &= 0xffffffffUL;

    // copy from target space
    OS_mem_read(realct, sp, sizeof(SPARC_signalframe32), (CC_mem_data_t*)&sf);

    //fprintf(stderr,"return from signal %d ", LSE_b2h(sf.info.si_signo));

    sigmask = ( LSE_b2h(sf.sigmask) |
		((uint64_t)LSE_b2h(sf.sigmask_extra[0]) << 32) );

    realct->PC.PC = LSE_b2h(sf.pt.tpc);
    realct->PC.NPC = LSE_b2h(sf.pt.tnpc);
    realct->PC.annul = false;
    realct->PC.check_level_zero = false;
    realct->PC.interrupted = false;
    realct->PC.PC &= 0xffffffffU;
    realct->PC.NPC &= 0xffffffffU;

    *get_asr_ptr(realct,Y) = LSE_b2h(sf.pt.y);

    uint32_t psr = LSE_b2h(sf.pt.tstate);
    uint64_t tstate = ( (((uint64_t)(psr & 0xf00000)) << 12) |
			(((uint64_t)(psr & 0xf0000)) << 20) );

    // only some fields should be set.
    *get_asr_ptr(realct, CCR) = (tstate & tstate_ccrM) >> tstate_ccrO;
    *get_asr_ptr(realct, ASI) = LSE_b2h(sf.v8extras[16]);
    osct->insyscall = false;

    for (int i = 1; i < 16; ++i) {
      *get_r_ptr(realct, i, cwp) = LSE_b2h(sf.pt.r[i]);
      *get_r_ptr(realct, i, cwp) |= ((uint64_t)LSE_b2h(sf.v8extras[i]))<<32;
    }

    SPARC_fault_t fault;
    fill_register_window(realct, cwp, fault, osct->is64bit, true);

    for (int ii = 0; ii < 64; ii++) 
      realct->F[ii] = LSE_b2h(sf.fpustate.fpreg[ii]);
    
    realct->FSR = LSE_b2h(sf.fpustate.fsr);
    *get_asr_ptr(realct,GSR) = LSE_b2h(sf.fpustate.gsr);
    *get_asr_ptr(realct,FPRS) = LSE_h2b(sf.fpustate.fprs);
  }
#undef frameoffset
}
  

static bool CC_do_a_signal(SPARC_context_t *realct, bool must_restart,
			   LSE_emu_iaddr_t pc, LSE_emu_iaddr_t &npc) {

  Linux_context_t *osct = OS_ict_osinfo(realct);

  if (osct->sigPendingFlag) {

    int actualerrno = *get_r_ptr(realct, 8, realct->PR[CWP]);

    // pick a signal and find out all the information about it.
    Linux_siginfo_t sinfo;
    Linux_sighandinf_t shand;

    uint64_t maskafter = (osct->oldSigBlockedValid ? osct->oldsigblocked :
			  osct->sigblocked);

    osct->incr();
    pick_signal(osct, sinfo, shand);

    if (osct->state == ProcZombie || osct->state == ProcReaped) {
      osct->decr();
      return false;
    }
    osct->decr();

    // are we in a system call reporting an error?

    bool inrestart = (osct->insyscall && (realct->ASR[CCR] & 0x11));

    if (sinfo.LSEsi_signo > 0) { // something to deliver

#ifdef DEBUG_SIGNAL
      std::cerr << "Found signal " << sinfo.LSEsi_signo 
      << " handler " << std::hex << npc << std::dec
      << "\n";
#endif

      if (inrestart) { // syscall_restart(orig_i0)

	if ((actualerrno == OSDefs::oERESTARTSYS 
	     && !(OSDefs::oSA_RESTART & shand.flags)) ||
	    actualerrno == OSDefs::oERESTARTNOHAND ||
	    actualerrno == OSDefs::oERESTART_RESTARTBLOCK) {

	  if (actualerrno == OSDefs::oERESTART_RESTARTBLOCK) {
	    delete osct->restart; // so things get garbage collected earlier
	    osct->restart = 0;
	  }
	  *get_r_ptr(realct,8,realct->PR[CWP]) = OSDefs::oEINTR;
	  realct->ASR[CCR] |= 0x11;
	  osct->myerrno() = -OSDefs::oEINTR;
	} else if (actualerrno == OSDefs::oERESTARTSYS ||
		   actualerrno == OSDefs::oERESTARTNOINTR) {
	  *get_r_ptr(realct,8,realct->PR[CWP]) = osct->orig_i0;
	  npc.PC -= 4;  // no traps in delay slots, please!
	  npc.NPC -= 4;
	  realct->PC = npc;
	  osct->myerrno() = 0;
	}

      }

      // set up frame
      SPARC_fault_t fault;
      flush_register_windows(realct, fault, osct->is64bit, true);

      if (osct->is64bit) {
	// signal frame: gigantic!
	//   normal stack frame: 192 bytes
	//   L_siginfo
	//   pt_regs
	//   fpu save register
	//   stack
	//   L_sigset
	//   fpu_state

	int cwp = realct->PR[CWP];

	uint64_t sp = *get_r_ptr(realct, 14, cwp) + bias64;
	uint64_t origsp = sp;

	// use alternate stack?
	if ((shand.flags & OSDefs::oSA_ONSTACK) &&
	    osct->sigAlt.LSE_ss_size && !on_alt_stack(osct->sigAlt,sp))
	  sp = osct->sigAlt.LSE_ss_size + osct->sigAlt.LSE_ss_sp;
	
	sp &= ~uint64_t(7); // align stack
	sp -= sizeof(SPARC_rtsignalframe);

	SPARC_rtsignalframe sf;
#define frameoffset(Y) ((char *)&sf.Y - (char *)&sf)

	sf.pt.tstate = LSE_h2b((*get_pr_ptr(realct,GL) << tstate_glO) |
			       (*get_asr_ptr(realct, CCR) << tstate_ccrO) |
			       (*get_asr_ptr(realct, ASI) << tstate_asiO) |
			       (*get_pr_ptr(realct,PSTATE) << tstate_pstateO) | 
			       (osct->insyscall << 5) |
			       (*get_pr_ptr(realct, CWP) << tstate_cwpO));

	sf.pt.tpc  = LSE_h2b(realct->PC.PC);   // already incremented over trap
	sf.pt.tnpc = LSE_h2b(realct->PC.NPC);   // already incremented over trap
	sf.pt.y = LSE_h2b((uint32_t)(*get_asr_ptr(realct,Y)));
	sf.pt.magic = LSE_h2b(0x57ac6c00);

	sf.pt.r[0] = 0;
	for (int ii = 1; ii < 16; ii++)
	  sf.pt.r[ii] = LSE_h2b(*get_r_ptr(realct,ii,cwp));
	
	for (int ii = 0; ii < 64; ii++) 
	  sf.fpustate.fpreg[ii] = LSE_h2b(realct->F[ii]);
    
	sf.fpustate.fsr = LSE_h2b(realct->FSR);
	sf.fpustate.gsr= LSE_h2b(*get_asr_ptr(realct,GSR));
	sf.fpustate.fprs = LSE_h2b(*get_asr_ptr(realct,FPRS));
	sf.fpuptr = LSE_h2b(sp + frameoffset(fpustate));

	// set up the stack... pointer stuff
	sf.stack.LSE_ss_sp = LSE_h2b(osct->sigAlt.LSE_ss_sp);
	sf.stack.LSE_ss_size 
	  = LSE_h2b((OSDefs::L_size_t)osct->sigAlt.LSE_ss_size);
	sf.stack.LSE_ss_flags 
	  = LSE_h2b(calc_ss_flags(osct->sigAlt, origsp));

	sf.sigmask = LSE_h2b(maskafter);
	sf.info = OSDefs::L_siginfo(sinfo);

	// registers from previous frame
	OS_mem_read(realct, origsp, 128, sf.normalframe);

	// copy now to target space
	OS_mem_write(realct, sp, sizeof(SPARC_rtsignalframe), 
		     (CC_mem_data_t*)&sf);

	// handler parameters
	*get_r_ptr(realct, 14, cwp) = sp - bias64;
	*get_r_ptr(realct, 8, cwp) = sinfo.LSEsi_signo;
	*get_r_ptr(realct, 9, cwp) = sp + frameoffset(info);
	*get_r_ptr(realct, 10, cwp) = sp + frameoffset(info);

	// handler
	npc.PC = realct->PC.PC = shand.handler;
	npc.NPC = realct->PC.NPC = shand.handler + 4;
	*get_r_ptr(realct, 15, cwp) = shand.restorer;
#undef frameoffset
      } else if (shand.flags & OSDefs::oSA_SIGINFO) { // 32-bit rt frame

	int cwp = realct->PR[CWP];

	uint64_t sp = *get_r_ptr(realct, 14, cwp) & 0xffffffffUL;
	uint64_t origsp = sp;

	// use alternate stack?
	if ((shand.flags & OSDefs::oSA_ONSTACK) &&
	    osct->sigAlt.LSE_ss_size && !on_alt_stack(osct->sigAlt,sp))
	  sp = osct->sigAlt.LSE_ss_size + osct->sigAlt.LSE_ss_sp;
	
	sp &= ~uint64_t(7); // align stack
	sp -= (sizeof(SPARC_rtsignalframe32) + 7) & ~7UL;; 

	SPARC_rtsignalframe32 sf;
#define frameoffset(Y) ((char *)&sf.Y - (char *)&sf)

	uint64_t tstate = ((*get_pr_ptr(realct,GL) << tstate_glO) |
			   (*get_asr_ptr(realct, CCR) << tstate_ccrO) |
			   (*get_asr_ptr(realct, ASI) << tstate_asiO) |
			   (*get_pr_ptr(realct,PSTATE)<<tstate_pstateO)| 
			   (osct->insyscall << 5) |
			   (*get_pr_ptr(realct, CWP) << tstate_cwpO));
	uint32_t psr = ((tstate & 0x1f) | 0x80 |
			(((tstate >> 32) & 0xf) << 20) |
			(((tstate >> 36) & 0xf) << 16) |
			(osct->insyscall ? 0x4000 : 0) |
			0xff000000);

	realct->PC.PC &= 0xffffffffU;
	realct->PC.NPC &= 0xffffffffU;
	sf.pt.tpc  = LSE_h2b((uint32_t)realct->PC.PC);
	sf.pt.tnpc = LSE_h2b((uint32_t)realct->PC.NPC);
	sf.pt.y = LSE_h2b((uint32_t)(*get_asr_ptr(realct,Y)));
	sf.pt.tstate = LSE_h2b(psr);

	sf.pt.r[0] = 0;
	for (int ii = 1; ii < 16; ii++) {
	  sf.pt.r[ii] = LSE_h2b((uint32_t)*get_r_ptr(realct,ii,cwp));
	  sf.v8extras[ii] = LSE_h2b((uint32_t)(*get_r_ptr(realct,ii,cwp)>>32));
	}
	sf.v8extra_size = LSE_h2b((uint32_t)17 * 4);
	sf.v8extras[16] = LSE_h2b((uint32_t)*get_asr_ptr(realct, ASI));
	sf.v8extras[0] = LSE_h2b((uint32_t)0x130e269);
	for (int ii = 0; ii < 64; ii++) 
	  sf.fpustate.fpreg[ii] = LSE_h2b(realct->F[ii]);
    
	sf.fpustate.fsr = LSE_h2b((uint32_t)realct->FSR);
	sf.fpustate.gsr= LSE_h2b(*get_asr_ptr(realct,GSR));
	sf.fpustate.fprs = LSE_h2b(*get_asr_ptr(realct,FPRS));
	sf.fpuptr = LSE_h2b(sp + frameoffset(fpustate));

	// set up the stack... pointer stuff
	sf.stack.LSE_ss_sp = LSE_h2b(osct->sigAlt.LSE_ss_sp);
	sf.stack.LSE_ss_size 
	  = LSE_h2b((OSDefs::L_size_t)osct->sigAlt.LSE_ss_size);
	sf.stack.LSE_ss_flags 
	  = LSE_h2b(calc_ss_flags(osct->sigAlt, origsp));

	sf.info = OSDefs::L_compat_siginfo(sinfo);
	sf.sigmask[0] = LSE_h2b((uint32_t)maskafter);
	sf.sigmask[1] = LSE_h2b((uint32_t)(maskafter>>32));

	// registers from previous frame
	OS_mem_read(realct, origsp, 64, sf.normalframe);

	// handler parameters
	*get_r_ptr(realct, 14, cwp) = sp;
	*get_r_ptr(realct, 8, cwp) = sinfo.LSEsi_signo;
	*get_r_ptr(realct, 9, cwp) = sp + frameoffset(info);
	*get_r_ptr(realct, 10, cwp) = sp + frameoffset(pt);

	// handler
	npc.PC = realct->PC.PC = shand.handler;
	npc.NPC = realct->PC.NPC = shand.handler + 4;
	npc.PC &= 0xffffffffU;
	npc.NPC &= 0xffffffffU;
	if (shand.restorer)
	  *get_r_ptr(realct, 15, cwp) = shand.restorer;
	else {
	  sf.tramp[0] = LSE_h2b(0x82102065U);
	  sf.tramp[1] = LSE_h2b(0x91d02010U);
	  *get_r_ptr(realct, 15, cwp) = sp + frameoffset(tramp[0]) - 8;
	}

	// copy now to target space
	OS_mem_write(realct, sp, sizeof(sf), (CC_mem_data_t*)&sf);

#undef frameoffset
	
      } else { // 32-bit normal frame

	int cwp = realct->PR[CWP];

	uint64_t sp = *get_r_ptr(realct, 14, cwp) & 0xffffffffUL;
	uint64_t origsp = sp;

	// use alternate stack?
	if ((shand.flags & OSDefs::oSA_ONSTACK) &&
	    osct->sigAlt.LSE_ss_size && !on_alt_stack(osct->sigAlt,sp))
	  sp = osct->sigAlt.LSE_ss_size + osct->sigAlt.LSE_ss_sp;
	
	sp &= ~uint64_t(7); // align stack
	sp -= (sizeof(SPARC_signalframe32) + 7) & ~7UL;; 

	SPARC_signalframe32 sf;
#define frameoffset(Y) ((char *)&sf.Y - (char *)&sf)

	uint64_t tstate = ((*get_pr_ptr(realct,GL) << tstate_glO) |
			   (*get_asr_ptr(realct, CCR) << tstate_ccrO) |
			   (*get_asr_ptr(realct, ASI) << tstate_asiO) |
			   (*get_pr_ptr(realct,PSTATE)<<tstate_pstateO)| 
			   (osct->insyscall << 5) |
			   (*get_pr_ptr(realct, CWP) << tstate_cwpO));
	uint32_t psr = ((tstate & 0x1f) | 0x80 |
			(((tstate >> 32) & 0xf) << 20) |
			(((tstate >> 36) & 0xf) << 16) |
			(osct->insyscall ? 0x4000 : 0) |
			0xff000000);

	realct->PC.PC &= 0xffffffffU;
	realct->PC.NPC &= 0xffffffffU;
	sf.pt.tpc  = LSE_h2b((uint32_t)realct->PC.PC);
	sf.pt.tnpc = LSE_h2b((uint32_t)realct->PC.NPC);
	sf.pt.y = LSE_h2b((uint32_t)(*get_asr_ptr(realct,Y)));
	sf.pt.tstate = LSE_h2b(psr);

	sf.pt.r[0] = 0;
	for (int ii = 1; ii < 16; ii++) {
	  sf.pt.r[ii] = LSE_h2b((uint32_t)*get_r_ptr(realct,ii,cwp));
	  sf.v8extras[ii] = LSE_h2b((uint32_t)(*get_r_ptr(realct,ii,cwp)>>32));
	}
	sf.v8extra_size = LSE_h2b((uint32_t)17 * 4);
	sf.v8extras[16] = LSE_h2b((uint32_t)*get_asr_ptr(realct, ASI));
	sf.v8extras[0] = LSE_h2b((uint32_t)0x130e269);
	for (int ii = 0; ii < 64; ii++) 
	  sf.fpustate.fpreg[ii] = LSE_h2b(realct->F[ii]);
    
	sf.fpustate.fsr = LSE_h2b((uint32_t)realct->FSR);
	sf.fpustate.gsr= LSE_h2b(*get_asr_ptr(realct,GSR));
	sf.fpustate.fprs = LSE_h2b(*get_asr_ptr(realct,FPRS));
	sf.fpuptr = LSE_h2b(sp + frameoffset(fpustate));

	sf.sigmask = LSE_h2b((uint32_t)maskafter);
	sf.sigmask_extra[0] = LSE_h2b((uint32_t)(maskafter>>32));

	// registers from previous frame
	OS_mem_read(realct, origsp, 64, sf.normalframe);

	// handler parameters
	*get_r_ptr(realct, 14, cwp) = sp;
	*get_r_ptr(realct, 8, cwp) = sinfo.LSEsi_signo;
	*get_r_ptr(realct, 9, cwp) = sp + frameoffset(pt);
	*get_r_ptr(realct, 10, cwp) = sp + frameoffset(pt);

	// handler
	npc.PC = realct->PC.PC = shand.handler;
	npc.NPC = realct->PC.NPC = shand.handler + 4;
	npc.PC &= 0xffffffffU;
	npc.NPC &= 0xffffffffU;
	if (shand.restorer)
	  *get_r_ptr(realct, 15, cwp) = shand.restorer;
	else {
	  sf.tramp[0] = LSE_h2b(0x821020d8U);
	  sf.tramp[1] = LSE_h2b(0x91d02010U);
	  *get_r_ptr(realct, 15, cwp) = sp + frameoffset(tramp[0]) - 8;
	}

	// copy now to target space
	OS_mem_write(realct, sp, sizeof(sf), (CC_mem_data_t*)&sf);

#undef frameoffset

      }

      osct->sigblocked |= shand.sigmask;
      if (!(shand.flags & OSDefs::oSA_NODEFER)) {
        osct->sigblocked |= uint64_t(1) << (sinfo.LSEsi_signo-1);
      }
      osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
	(osct->tgroup->sigpending & ~osct->sigblocked);
      osct->oldSigBlockedValid = false;

      return true;
    }

    if (inrestart && 
	(actualerrno == OSDefs::oERESTARTNOHAND ||
	 actualerrno == OSDefs::oERESTARTSYS ||
	 actualerrno == OSDefs::oERESTARTNOINTR)) {
      *get_r_ptr(realct,8,realct->PR[CWP]) = osct->orig_i0;
      npc.PC -= 4;  // no traps in delay slots, please!
      npc.NPC -= 4;
      realct->PC = npc;
      osct->myerrno() = 0;
    }
    if (inrestart &&
	actualerrno == OSDefs::oERESTART_RESTARTBLOCK) {
      *get_r_ptr(realct, 1) = 0; // restart_syscall .... maybe 1000

      npc.PC -= 4;  // no traps in delay slots, please!
      npc.NPC -= 4;
      realct->PC = npc;
      osct->myerrno() = 0;
    }
	 
    // no handler for an unblocked signal, so we can go ahead and 
    // restore the mask

    if (osct->oldSigBlockedValid) {
      osct->sigblocked = osct->oldsigblocked;
      osct->oldSigBlockedValid = false;
      osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
	(osct->tgroup->sigpending & ~osct->sigblocked);
    }

  } // if sigpending

  return false;
}

static LSE_LINUX_SIG(SPARC_pipe) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fdesp;
  int ret=0;
  int fdes[2], swapdes[2];

  fdesp = CC_arg_ptr(0,"p");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): pipe() = ",
            osct->tid, fdesp);

  Linux_pipe_t *pp;

  pp = new Linux_pipe_t(osct->os);
  fdes[0] = osct->fdtable->addToTable(new Linux_pipe_file_t(osct->os,
                                                            pp, false));
  fdes[1] = osct->fdtable->addToTable(new Linux_pipe_file_t(osct->os,
                                                            pp, true));

  if (ret == -1) {
    OS_report_err(osct->myerrno());
  } else {
    CC_sys_set_return(realct, 0, 0);
    CC_sys_set_return(realct, 1, fdes[0]);
    CC_sys_set_return(realct, 2, fdes[1]);
  }

  CC_hook_pipe();

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(SPARC_compat_getrlimit) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int resource;
  LSE_emu_addr_t targrlim;
  struct rlimit rlim;
  OSDefs::L_compat_rlimit temprlim;
  int ret=0;

  resource = CC_arg_int(0,"ip");
  targrlim = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): compat_getrlimit(%d,0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, resource, targrlim);

  if (resource < 0 || resource >= OSDefs::oRLIMIT_NLIMITS)
    OS_report_err(OSDefs::oEINVAL);

  if (osct->tgroup->rlimits[resource][0] == OSDefs::oRLIMIT_INFINITY)
    temprlim.LSE_rlim_cur = LSE_h2b((typeof temprlim.LSE_rlim_cur)0x7fffffffU);
  else 
    temprlim.LSE_rlim_cur = LSE_h2e((typeof temprlim.LSE_rlim_cur)
				    (osct->tgroup->rlimits[resource][0]),
				    Linux_bigendian);

  if (osct->tgroup->rlimits[resource][1] == OSDefs::oRLIMIT_INFINITY)
    temprlim.LSE_rlim_max = LSE_h2b((typeof temprlim.LSE_rlim_cur)0x7fffffffU);
  else 
    temprlim.LSE_rlim_max = LSE_h2e((typeof temprlim.LSE_rlim_max)
				    (osct->tgroup->rlimits[resource][1]),
				    Linux_bigendian);

  OS_memcpy_h2t(targrlim,&temprlim,sizeof(temprlim)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(SPARC_compat_setrlimit) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int resource;
  LSE_emu_addr_t targrlim;
  struct rlimit rlim;
  OSDefs::L_compat_rlimit temprlim;
  int ret=0;

  resource = CC_arg_int(0,"ip");
  targrlim = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): compat_setrlimit(%d,0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, resource, targrlim);

  if (resource < 0 || resource >= OSDefs::oRLIMIT_NLIMITS)
    OS_report_err(OSDefs::oEINVAL);

  OS_memcpy_t2h(&temprlim,targrlim,sizeof(temprlim)); /* will report error */

  osct->tgroup->rlimits[resource][0] = LSE_e2h(temprlim.LSE_rlim_cur,
					       Linux_bigendian);
  if (osct->tgroup->rlimits[resource][0] == 0x7ffffffU)
    osct->tgroup->rlimits[resource][0] = OSDefs::oRLIMIT_INFINITY;

  osct->tgroup->rlimits[resource][1] = LSE_e2h(temprlim.LSE_rlim_max,
					       Linux_bigendian);
  if (osct->tgroup->rlimits[resource][1] == 0x7ffffffU)
    osct->tgroup->rlimits[resource][1] = OSDefs::oRLIMIT_INFINITY;

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(sparc_pipe) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;
  int fdes[2];

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): sparc_pipe()",
            osct->tid);

  Linux_pipe_t *pp;

  pp = new Linux_pipe_t(osct->os);
  ret = fdes[0] = osct->fdtable->addToTable(new Linux_pipe_file_t(osct->os,
                                                            pp, false));
  fdes[1] = osct->fdtable->addToTable(new Linux_pipe_file_t(osct->os,
                                                            pp, true));

  if (ret == -1) {
    OS_report_err(osct->myerrno());
  } else {
    OS_report_results(0, ret);
    CC_sys_set_return(realct, 2, fdes[1]);
  }

  CC_hook_pipe();

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(compat_fstat64) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  LSE_emu_addr_t targbuf;
  LSE_stat64 buf;
  struct OSDefs::L_compat_stat64 tempbuf;
  int ret=0;

  fd      = (int)CC_arg_int(0,"ip");
  targbuf = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
	    "SYSCALL(%d): compat_fstat64(%d,0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, fd,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);

#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->fstat64(&buf);)
#else
  CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->fstat(&buf);)
#endif
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    OS_report_err(osct->myerrno());
  }
  /* 
   * note that these values will depend upon the HOST system
   */

  CC_chkpt_guard(tempbuf = buf;);
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  CC_chkpt_finish();
  OS_syserr_handler()  
}

static LSE_LINUX_SIG(compat_lstat64) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname, targbuf;
  LSE_stat64 buf;
  struct OSDefs::L_compat_stat64 tempbuf;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;

  fname   = CC_arg_ptr(0,"pp");
  targbuf = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): compat_lstat64(\"%s\",0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, (char *)fnamebuf,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_str(fnamebuf,strlen(fnamebuf)+1);

  rfname = resolve_path(fnamebuf, osct);
#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(ret = lstat64(rfname, &buf);
		 osct->myerrno() = errno;);
#else
  CC_chkpt_guard(ret = lstat(rfname, &buf);
		 osct->myerrno() = errno;);
#endif
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));

  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): compat_lstat64(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, fname, targbuf);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

static LSE_LINUX_SIG(compat_stat64) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  LSE_emu_addr_t targbuf;
  LSE_stat64 buf;
  struct OSDefs::L_compat_stat64 tempbuf;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  LSE_emu_addr_t fname;
  int didfname = 0;
  int ret=0;

  fname   = CC_arg_ptr(0,"pp");
  targbuf = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): compat_stat64(%s,0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, fnamebuf,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_str(fnamebuf,strlen(fnamebuf)+1);

  rfname = resolve_path(fnamebuf, osct);
#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(ret = stat64(rfname, &buf);
		 osct->myerrno() = errno;);
#else
  CC_chkpt_guard(ret = stat(rfname, &buf);
		 osct->myerrno() = errno;);
#endif
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  /* 
   * note that these values will depend upon the HOST system
   */

  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): compat_stat64(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, fname, targbuf);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler()
}

int Linux_call_os(LSE_emu_isacontext_t *realct,
		  LSE_emu_iaddr_t curr_pc,
		  LSE_emu_iaddr_t &following_pc,
		  uint64_t swino)
{
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osres=0;
  int rino = swino & 0xfffff;
  // Looks like it should be called as ta 0x6d with swino in %g1
  // other arguments start in %o1..

  realct->PC = following_pc;
  if (rino < 2000) {
    OS_ict_osinfo(realct)->insyscall = true;
    OS_ict_osinfo(realct)->orig_i0 = *get_r_ptr(realct,8,realct->PR[CWP]);
  }

  SPARC_fault_t f;

  //std::cerr << CC_time_get_currtime(osct->os) << "\n";

  switch (rino) {

    case 0 : 
       osres = Linux_s_restart_syscall(rino,realct,curr_pc,0);
       break;

    case 1 : 
       osres = Linux_s_exit(rino,realct,curr_pc,0);
       break;

    case 2 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_fork(rino,realct,curr_pc,0);
       break;

    case 3 : 
       osres = Linux_s_read(rino,realct,curr_pc,0);
       break;

    case 4 : 
       osres = Linux_s_write(rino,realct,curr_pc,0);
       break;

    case 5 : 
       osres = Linux_s_open(rino,realct,curr_pc,0);
       break;

    case 6 : 
       osres = Linux_s_close(rino,realct,curr_pc,0);
       break;

    case 7 : 
       osres = Linux_s_compat_wait4(rino,realct,curr_pc,0);
       break;

    case 8 : 
       osres = Linux_s_creat(rino,realct,curr_pc,0);
       break;

    case 9 : 
       osres = Linux_s_link(rino,realct,curr_pc,0);
       break;

    case 10 : 
       osres = Linux_s_unlink(rino,realct,curr_pc,0);
       break;

    case 11 : 
       osres = Linux_s_execv(rino,realct,curr_pc,0);
       break;

    case 12 : 
       osres = Linux_s_chdir(rino,realct,curr_pc,0);
       break;

    case 13 : 
       osres = Linux_s_chown16(rino,realct,curr_pc,1);
       break;

    case 14 : 
       osres = Linux_s_mknod(rino,realct,curr_pc,0);
       break;

    case 15 : 
       osres = Linux_s_chmod(rino,realct,curr_pc,0);
       break;

    case 16 : 
       osres = Linux_s_lchown16(rino,realct,curr_pc,1);
       break;

    case 17 : 
       osres = Linux_s_brk(rino,realct,curr_pc,0);
       break;

    case 18 : 
       osres = Linux_s_perfctr(rino,realct,curr_pc,0);
       break;

    case 19 : 
       osres = Linux_s_lseek(rino,realct,curr_pc,0);
       break;

    case 20 : 
       osres = Linux_s_getpid(rino,realct,curr_pc,0);
       break;

    case 21 : 
       osres = Linux_s_capget(rino,realct,curr_pc,0);
       break;

    case 22 : 
       osres = Linux_s_capset(rino,realct,curr_pc,0);
       break;

    case 23 : 
       osres = Linux_s_setuid16(rino,realct,curr_pc,1);
       break;

    case 24 : 
       osres = Linux_s_getuid16(rino,realct,curr_pc,1);
       break;

    case 25 : 
       osres = Linux_s_time(rino,realct,curr_pc,0);
       break;

    case 26 : 
       osres = Linux_s_ptrace(rino,realct,curr_pc,0);
       break;

    case 27 : 
       osres = Linux_s_alarm(rino,realct,curr_pc,0);
       break;

    case 28 : 
       osres = Linux_s_compat_sigaltstack(rino,realct,curr_pc,0);
       break;

    case 29 : 
       osres = Linux_s_pause(rino,realct,curr_pc,0);
       break;

    case 30 : 
       osres = Linux_s_compat_utime(rino,realct,curr_pc,0);
       break;

    case 31 : 
       osres = Linux_s_lchown(rino,realct,curr_pc,0);
       break;

    case 32 : 
       osres = Linux_s_fchown(rino,realct,curr_pc,0);
       break;

    case 33 : 
       osres = Linux_s_access(rino,realct,curr_pc,0);
       break;

    case 34 : 
       osres = Linux_s_nice(rino,realct,curr_pc,0);
       break;

    case 35 : 
       osres = Linux_s_chown(rino,realct,curr_pc,0);
       break;

    case 36 : 
       osres = Linux_s_sync(rino,realct,curr_pc,0);
       break;

    case 37 : 
       osres = Linux_s_kill(rino,realct,curr_pc,0);
       break;

    case 38 : 
       osres = Linux_s_compat_newstat(rino,realct,curr_pc,0);
       break;

    case 39 : 
       osres = Linux_s_sendfile(rino,realct,curr_pc,0);
       break;

    case 40 : 
       osres = Linux_s_compat_newlstat(rino,realct,curr_pc,0);
       break;

    case 41 : 
       osres = Linux_s_dup(rino,realct,curr_pc,0);
       break;

    case 42 : 
       osres = Linux_s_sparc_pipe(rino,realct,curr_pc,0);
       break;

    case 43 : 
       osres = Linux_s_compat_times(rino,realct,curr_pc,0);
       break;

    case 44 : 
       osres = Linux_s_getuid(rino,realct,curr_pc,0);
       break;

    case 45 : 
       osres = Linux_s_umount(rino,realct,curr_pc,2);
       break;

    case 46 : 
       osres = Linux_s_setgid16(rino,realct,curr_pc,1);
       break;

    case 47 : 
       osres = Linux_s_getgid16(rino,realct,curr_pc,1);
       break;

    case 48 : 
       osres = Linux_s_signal(rino,realct,curr_pc,2);
       break;

    case 49 : 
       osres = Linux_s_geteuid16(rino,realct,curr_pc,1);
       break;

    case 50 : 
       osres = Linux_s_getegid16(rino,realct,curr_pc,1);
       break;

    case 51 : 
       osres = Linux_s_acct(rino,realct,curr_pc,0);
       break;

    case 53 : 
       osres = Linux_s_getgid(rino,realct,curr_pc,0);
       break;

    case 54 : 
       osres = Linux_s_compat_ioctl(rino,realct,curr_pc,0);
       break;

    case 55 : 
       osres = Linux_s_reboot(rino,realct,curr_pc,2);
       break;

    case 56 : 
       osres = Linux_s_mmap2(rino,realct,curr_pc,2);
       break;

    case 57 : 
       osres = Linux_s_symlink(rino,realct,curr_pc,0);
       break;

    case 58 : 
       osres = Linux_s_readlink(rino,realct,curr_pc,2);
       break;

    case 59 : 
       osres = Linux_s_execve(rino,realct,curr_pc,0);
       break;
    case 60 : 
       osres = Linux_s_umask(rino,realct,curr_pc,0);
       break;

    case 61 : 
       osres = Linux_s_chroot(rino,realct,curr_pc,0);
       break;

    case 62 : 
       osres = Linux_s_compat_newfstat(rino,realct,curr_pc,0);
       break;

    case 63 : 
       osres = Linux_s_compat_fstat64(rino,realct,curr_pc,0);
       break;

    case 64 : 
       osres = Linux_s_getpagesize(rino,realct,curr_pc,0);
       break;

    case 65 : 
       osres = Linux_s_msync(rino,realct,curr_pc,0);
       break;

    case 66 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_vfork(rino,realct,curr_pc,0);
       break;

    case 67 : 
       osres = Linux_s_pread64(rino,realct,curr_pc,4);
       break;

    case 68 : 
       osres = Linux_s_pwrite64(rino,realct,curr_pc,4);
       break;

    case 69 : 
       osres = Linux_s_geteuid(rino,realct,curr_pc,0);
       break;

    case 70 : 
       osres = Linux_s_getegid(rino,realct,curr_pc,0);
       break;

    case 71 : 
       osres = Linux_s_mmap(rino,realct,curr_pc,0);
       break;

    case 72 : 
       osres = Linux_s_setreuid(rino,realct,curr_pc,0);
       break;

    case 73 : 
       osres = Linux_s_munmap(rino,realct,curr_pc,0);
       break;

    case 74 : 
       osres = Linux_s_mprotect(rino,realct,curr_pc,0);
       break;

    case 75 : 
       osres = Linux_s_madvise(rino,realct,curr_pc,0);
       break;

    case 76 : 
       osres = Linux_s_vhangup(rino,realct,curr_pc,0);
       break;

    case 77 : 
       osres = Linux_s_truncate64(rino,realct,curr_pc,0);
       break;

    case 78 : 
       osres = Linux_s_mincore(rino,realct,curr_pc,0);
       break;

    case 79 : 
       osres = Linux_s_getgroups16(rino,realct,curr_pc,1);
       break;

    case 80 : 
       osres = Linux_s_setgroups16(rino,realct,curr_pc,1);
       break;

    case 81 : 
       osres = Linux_s_getpgrp(rino,realct,curr_pc,0);
       break;

    case 82 : 
       osres = Linux_s_setgroups(rino,realct,curr_pc,2);
       break;

    case 83 : 
       osres = Linux_s_compat_setitimer(rino,realct,curr_pc,2);
       break;

    case 84 : 
       osres = Linux_s_ftruncate64(rino,realct,curr_pc,4);
       break;

    case 85 : 
       osres = Linux_s_swapon(rino,realct,curr_pc,2);
       break;

    case 86 : 
       osres = Linux_s_compat_getitimer(rino,realct,curr_pc,2);
       break;

    case 87 : 
       osres = Linux_s_setuid(rino,realct,curr_pc,0);
       break;

    case 88 : 
       osres = Linux_s_sethostname(rino,realct,curr_pc,2);
       break;

    case 89 : 
       osres = Linux_s_setgid(rino,realct,curr_pc,0);
       break;

    case 90 : 
       osres = Linux_s_dup2(rino,realct,curr_pc,0);
       break;

    case 91 : 
       osres = Linux_s_setfsuid(rino,realct,curr_pc,0);
       break;

    case 92 : 
       osres = Linux_s_compat_fcntl(rino,realct,curr_pc,0);
       break;

    case 93 : 
       osres = Linux_s_select(rino,realct,curr_pc,0);
       break;

    case 94 : 
       osres = Linux_s_setfsgid(rino,realct,curr_pc,0);
       break;

    case 95 : 
       osres = Linux_s_fsync(rino,realct,curr_pc,0);
       break;

    case 96 : 
       osres = Linux_s_setpriority(rino,realct,curr_pc,2);
       break;

    case 100 : 
       osres = Linux_s_getpriority(rino,realct,curr_pc,2);
       break;

    case 101 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_rt_sigreturn(rino,realct,curr_pc,0);
       following_pc = realct->PC;
       break;
    case 102 : 
       osres = Linux_s_SPARC_rt_sigaction32(rino,realct,curr_pc,2);
       break;

    case 103 : 
       osres = Linux_s_compat_rt_sigprocmask(rino,realct,curr_pc,2);
       break;

    case 104 : 
       osres = Linux_s_compat_rt_sigpending(rino,realct,curr_pc,2);
       break;

    case 105 : 
       osres = Linux_s_compat_rt_sigtimedwait(rino,realct,curr_pc,2);
       break;

    case 106 : 
       osres = Linux_s_compat_rt_sigqueueinfo(rino,realct,curr_pc,2);
       break;

    case 107 : 
       osres = Linux_s_compat_rt_sigsuspend(rino,realct,curr_pc,0);
       break;
    case 108 : 
       osres = Linux_s_setresuid(rino,realct,curr_pc,0);
       break;

    case 109 : 
       osres = Linux_s_getresuid(rino,realct,curr_pc,0);
       break;

    case 110 : 
       osres = Linux_s_setresgid(rino,realct,curr_pc,0);
       break;

    case 111 : 
       osres = Linux_s_getresgid(rino,realct,curr_pc,0);
       break;

    case 112 : 
       osres = Linux_s_setregid(rino,realct,curr_pc,0);
       break;

    case 115 : 
       osres = Linux_s_getgroups(rino,realct,curr_pc,2);
       break;

    case 116 : 
       osres = Linux_s_compat_gettimeofday(rino,realct,curr_pc,0);
       break;

    case 117 : 
       osres = Linux_s_compat_getrusage(rino,realct,curr_pc,2);
       break;

    case 119 : 
       osres = Linux_s_getcwd(rino,realct,curr_pc,0);
       break;

    case 120 : 
       osres = Linux_s_compat_readv(rino,realct,curr_pc,0);
       break;

    case 121 : 
       osres = Linux_s_compat_writev(rino,realct,curr_pc,0);
       break;

    case 122 : 
       osres = Linux_s_compat_settimeofday(rino,realct,curr_pc,0);
       break;

    case 123 : 
       osres = Linux_s_fchown16(rino,realct,curr_pc,1);
       break;

    case 124 : 
       osres = Linux_s_fchmod(rino,realct,curr_pc,0);
       break;

    case 126 : 
       osres = Linux_s_setreuid16(rino,realct,curr_pc,1);
       break;

    case 127 : 
       osres = Linux_s_setregid16(rino,realct,curr_pc,1);
       break;

    case 128 : 
       osres = Linux_s_rename(rino,realct,curr_pc,0);
       break;

    case 129 : 
       osres = Linux_s_truncate(rino,realct,curr_pc,0);
       break;

    case 130 : 
       osres = Linux_s_ftruncate(rino,realct,curr_pc,0);
       break;

    case 131 : 
       osres = Linux_s_flock(rino,realct,curr_pc,0);
       break;

    case 132 : 
       osres = Linux_s_compat_lstat64(rino,realct,curr_pc,0);
       break;

    case 136 : 
       osres = Linux_s_mkdir(rino,realct,curr_pc,2);
       break;

    case 137 : 
       osres = Linux_s_rmdir(rino,realct,curr_pc,0);
       break;

    case 138 : 
       osres = Linux_s_compat_utimes(rino,realct,curr_pc,0);
       break;

    case 139 : 
       osres = Linux_s_compat_stat64(rino,realct,curr_pc,0);
       break;

    case 140 : 
       osres = Linux_s_sendfile64(rino,realct,curr_pc,0);
       break;

    case 142 : 
       osres = Linux_s_compat_futex(rino,realct,curr_pc,2);
       break;

    case 143 : 
       osres = Linux_s_gettid(rino,realct,curr_pc,0);
       break;

    case 144 : 
       osres = Linux_s_SPARC_compat_getrlimit(rino,realct,curr_pc,0);
       break;

    case 145 : 
       osres = Linux_s_SPARC_compat_setrlimit(rino,realct,curr_pc,0);
       break;

    case 146 : 
       osres = Linux_s_pivot_root(rino,realct,curr_pc,0);
       break;

    case 147 : 
       osres = Linux_s_prctl(rino,realct,curr_pc,2);
       break;

    case 148 : 
       osres = Linux_s_pciconfig_read(rino,realct,curr_pc,0);
       break;

    case 149 : 
       osres = Linux_s_pciconfig_write(rino,realct,curr_pc,0);
       break;

    case 153 : 
       osres = Linux_s_poll(rino,realct,curr_pc,0);
       break;

    case 154 : 
       osres = Linux_s_getdents64(rino,realct,curr_pc,0);
       break;

    case 155 : 
       osres = Linux_s_compat_fcntl64(rino,realct,curr_pc,0);
       break;

    case 157 : 
       osres = Linux_s_compat_statfs(rino,realct,curr_pc,0);
       break;

    case 158 : 
       osres = Linux_s_compat_fstatfs(rino,realct,curr_pc,0);
       break;

    case 159 : 
       osres = Linux_s_oldumount(rino,realct,curr_pc,0);
       break;

    case 160 : 
       osres = Linux_s_compat_sched_set_affinity(rino,realct,curr_pc,0);
       break;

    case 161 : 
       osres = Linux_s_compat_sched_get_affinity(rino,realct,curr_pc,0);
       break;

    case 162 : 
       osres = Linux_s_getdomainname(rino,realct,curr_pc,2);
       break;

    case 163 : 
       osres = Linux_s_setdomainname(rino,realct,curr_pc,2);
       break;

    case 165 : 
       osres = Linux_s_quotactl(rino,realct,curr_pc,0);
       break;

    case 166 : 
       osres = Linux_s_set_tid_address(rino,realct,curr_pc,0);
       break;

    case 167 : 
       osres = Linux_s_compat_mount(rino,realct,curr_pc,0);
       break;

    case 168 : 
       osres = Linux_s_ustat(rino,realct,curr_pc,0);
       break;

    case 169 : 
       osres = Linux_s_setxattr(rino,realct,curr_pc,2);
       break;

    case 170 : 
       osres = Linux_s_lsetxattr(rino,realct,curr_pc,2);
       break;

    case 171 : 
       osres = Linux_s_fsetxattr(rino,realct,curr_pc,2);
       break;

    case 172 : 
       osres = Linux_s_getxattr(rino,realct,curr_pc,0);
       break;

    case 173 : 
       osres = Linux_s_lgetxattr(rino,realct,curr_pc,0);
       break;

    case 174 : 
       osres = Linux_s_compat_getdents(rino,realct,curr_pc,0);
       break;

    case 175 : 
       osres = Linux_s_setsid(rino,realct,curr_pc,0);
       break;

    case 176 : 
       osres = Linux_s_fchdir(rino,realct,curr_pc,0);
       break;

    case 177 : 
       osres = Linux_s_fgetxattr(rino,realct,curr_pc,2);
       break;

    case 178 : 
       osres = Linux_s_listxattr(rino,realct,curr_pc,0);
       break;

    case 179 : 
       osres = Linux_s_llistxattr(rino,realct,curr_pc,0);
       break;

    case 180 : 
       osres = Linux_s_flistxattr(rino,realct,curr_pc,2);
       break;

    case 181 : 
       osres = Linux_s_removexattr(rino,realct,curr_pc,0);
       break;

    case 182 : 
       osres = Linux_s_lremovexattr(rino,realct,curr_pc,0);
       break;

    case 183 : 
       osres = Linux_s_compat_sigpending(rino,realct,curr_pc,0);
       break;

    case 184 : 
       osres = Linux_s_query_module(rino,realct,curr_pc,0);
       break;

    case 185 : 
       osres = Linux_s_setpgid(rino,realct,curr_pc,2);
       break;

    case 186 : 
       osres = Linux_s_fremovexattr(rino,realct,curr_pc,2);
       break;

    case 187 : 
       osres = Linux_s_tkill(rino,realct,curr_pc,2);
       break;

    case 188 : 
       osres = Linux_s_exit_group(rino,realct,curr_pc,2);
       break;

    case 189 : 
       osres = Linux_s_newuname(rino,realct,curr_pc,0);
       break;
    case 190 : 
       osres = Linux_s_init_module(rino,realct,curr_pc,2);
       break;

    case 191 : 
       osres = Linux_s_sparc64_personality(rino,realct,curr_pc,0);
       break;

    case 192 : 
       osres = Linux_s_remap_file_pages(rino,realct,curr_pc,0);
       break;

    case 193 : 
       osres = Linux_s_epoll_create(rino,realct,curr_pc,2);
       break;

    case 194 : 
       osres = Linux_s_epoll_ctl(rino,realct,curr_pc,2);
       break;

    case 195 : 
       osres = Linux_s_epoll_wait(rino,realct,curr_pc,2);
       break;

    case 197 : 
       osres = Linux_s_getppid(rino,realct,curr_pc,0);
       break;

    case 198 : 
       osres = Linux_s_sigaction(rino,realct,curr_pc,0);
       break;
    case 199 : 
       osres = Linux_s_sgetmask(rino,realct,curr_pc,0);
       break;

    case 200 : 
       osres = Linux_s_ssetmask(rino,realct,curr_pc,2);
       break;

    case 201 : 
       osres = Linux_s_sigsuspend(rino,realct,curr_pc,0);
       break;

    case 202 : 
       osres = Linux_s_compat_newlstat(rino,realct,curr_pc,0);
       break;

    case 203 : 
       osres = Linux_s_uselib(rino,realct,curr_pc,0);
       break;

    case 204 : 
       osres = Linux_s_compat_old_readdir(rino,realct,curr_pc,0);
       break;

    case 205 : 
       osres = Linux_s_readahead(rino,realct,curr_pc,0);
       break;
    case 207 : 
       osres = Linux_s_syslog(rino,realct,curr_pc,2);
       break;

    case 208 : 
       osres = Linux_s_lookup_dcookie(rino,realct,curr_pc,4);
       break;

    case 209 : 
       osres = Linux_s_fadvise64(rino,realct,curr_pc,4);
       break;

    case 210 : 
       osres = Linux_s_fadvise64_64(rino,realct,curr_pc,4);
       break;

    case 211 : 
       osres = Linux_s_tgkill(rino,realct,curr_pc,2);
       break;

    case 212 : 
       osres = Linux_s_waitpid(rino,realct,curr_pc,2);
       break;

    case 213 : 
       osres = Linux_s_swapoff(rino,realct,curr_pc,0);
       break;

    case 214 : 
       osres = Linux_s_sysinfo(rino,realct,curr_pc,0);
       break;
    case 215 : 
       osres = Linux_s_compat_ipc(rino,realct,curr_pc,2);
       break;
    case 216 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_rt_sigreturn(rino,realct,curr_pc,0);
       following_pc = realct->PC;
       break;
    case 217 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_clone(rino,realct,curr_pc,0);
       break;

    case 219 : 
       osres = Linux_s_adjtimex(rino,realct,curr_pc,2);
       break;
    case 220 : 
       osres = Linux_s_compat_sigprocmask(rino,realct,curr_pc,2);
       break;

    case 221 : 
       osres = Linux_s_create_module(rino,realct,curr_pc,0);
       break;

    case 223 : 
       osres = Linux_s_get_kernel_syms(rino,realct,curr_pc,0);
       break;

    case 224 : 
       osres = Linux_s_getpgid(rino,realct,curr_pc,2);
       break;

    case 225 : 
       osres = Linux_s_bdflush(rino,realct,curr_pc,2);
       break;

    case 226 : 
       osres = Linux_s_compat_sysfs(rino,realct,curr_pc,0);
       break;
    case 228 : 
       osres = Linux_s_setfsuid(rino,realct,curr_pc,1);
       break;

    case 229 : 
       osres = Linux_s_setfsgid(rino,realct,curr_pc,1);
       break;

    case 230 : 
       osres = Linux_s_compat_select(rino,realct,curr_pc,2);
       break;

    case 231 : 
       osres = Linux_s_time(rino,realct,curr_pc,0);
       break;

    case 233 : 
       osres = Linux_s_compat_stime(rino,realct,curr_pc,0);
       break;

    case 234 : 
       osres = Linux_s_compat_statfs64(rino,realct,curr_pc,0);
       break;

    case 235 : 
       osres = Linux_s_compat_fstatfs64(rino,realct,curr_pc,0);
       break;

    case 236 : 
       osres = Linux_s__llseek(rino,realct,curr_pc,0);
       break;

    case 237 : 
       osres = Linux_s_mlock(rino,realct,curr_pc,0);
       break;

    case 238 : 
       osres = Linux_s_munlock(rino,realct,curr_pc,0);
       break;

    case 239 : 
       osres = Linux_s_mlockall(rino,realct,curr_pc,2);
       break;

    case 240 : 
       osres = Linux_s_munlockall(rino,realct,curr_pc,0);
       break;

    case 241 : 
       osres = Linux_s_sched_setparam(rino,realct,curr_pc,2);
       break;

    case 242 : 
       osres = Linux_s_sched_getparam(rino,realct,curr_pc,2);
       break;

    case 243 : 
       osres = Linux_s_sched_setscheduler(rino,realct,curr_pc,2);
       break;

    case 244 : 
       osres = Linux_s_sched_getscheduler(rino,realct,curr_pc,2);
       break;

    case 245 : 
       osres = Linux_s_sched_yield(rino,realct,curr_pc,0);
       break;

    case 246 : 
       osres = Linux_s_sched_get_priority_max(rino,realct,curr_pc,2);
       break;

    case 247 : 
       osres = Linux_s_sched_get_priority_min(rino,realct,curr_pc,2);
       break;

    case 248 : 
       osres = Linux_s_sched_rr_get_interval(rino,realct,curr_pc,0);
       break;
    case 249 : 
       osres = Linux_s_compat_nanosleep(rino,realct,curr_pc,0);
       break;

    case 250 : 
       osres = Linux_s_mremap(rino,realct,curr_pc,0);
       break;
    case 251 : 
       osres = Linux_s__sysctl(rino,realct,curr_pc,0);
       break;
    case 252 : 
       osres = Linux_s_getsid(rino,realct,curr_pc,2);
       break;

    case 253 : 
       osres = Linux_s_fdatasync(rino,realct,curr_pc,0);
       break;

    case 254 : 
       osres = Linux_s_compat_nfsservctl(rino,realct,curr_pc,2);
       break;

    case 255 : 
       osres = Linux_s_aplib(rino,realct,curr_pc,0);
       break;

    case 256 : 
       osres = Linux_s_compat_clock_settime(rino,realct,curr_pc,2);
       break;

    case 257 : 
       osres = Linux_s_compat_clock_gettime(rino,realct,curr_pc,0);
       break;

    case 258 : 
       osres = Linux_s_compat_clock_getres(rino,realct,curr_pc,0);
       break;

    case 259 : 
       osres = Linux_s_compat_clock_nanosleep(rino,realct,curr_pc,2);
       break;

    case 260 : 
       osres = Linux_s_compat_sched_getaffinity(rino,realct,curr_pc,0);
       break;

    case 261 : 
       osres = Linux_s_compat_sched_setaffinity(rino,realct,curr_pc,0);
       break;

    case 262 : 
       osres = Linux_s_compat_timer_settime(rino,realct,curr_pc,2);
       break;

    case 263 : 
       osres = Linux_s_compat_timer_gettime(rino,realct,curr_pc,0);
       break;

    case 264 : 
       osres = Linux_s_timer_getoverrun(rino,realct,curr_pc,0);
       break;

    case 265 : 
       osres = Linux_s_timer_delete(rino,realct,curr_pc,0);
       break;

    case 266 : 
       osres = Linux_s_compat_timer_create(rino,realct,curr_pc,2);
       break;

    case 268 : 
       osres = Linux_s_compat_io_setup(rino,realct,curr_pc,0);
       break;

    case 269 : 
       osres = Linux_s_io_destroy(rino,realct,curr_pc,0);
       break;

    case 270 : 
       osres = Linux_s_compat_io_submit(rino,realct,curr_pc,2);
       break;

    case 271 : 
       osres = Linux_s_io_cancel(rino,realct,curr_pc,0);
       break;

    case 272 : 
       osres = Linux_s_compat_io_getevents(rino,realct,curr_pc,0);
       break;

    case 273 : 
       osres = Linux_s_compat_mq_open(rino,realct,curr_pc,2);
       break;

    case 274 : 
       osres = Linux_s_mq_unlink(rino,realct,curr_pc,0);
       break;

    case 275 : 
       osres = Linux_s_mq_timedsend(rino,realct,curr_pc,0);
       break;

    case 276 : 
       osres = Linux_s_mq_timedreceive(rino,realct,curr_pc,0);
       break;

    case 277 : 
       osres = Linux_s_compat_mq_notify(rino,realct,curr_pc,0);
       break;

    case 278 : 
       osres = Linux_s_compat_mq_getsetattr(rino,realct,curr_pc,0);
       break;

    case 279 : 
       osres = Linux_s_compat_waitid(rino,realct,curr_pc,0);
       break;

    case 300 :
       osres = Linux_s_compat_set_robust_list(rino,realct,curr_pc,0);
       break;
    
    case 301 :
       osres = Linux_s_compat_get_robust_list(rino,realct,curr_pc,0);
       break;

    case 321 :
      osres = Linux_s_pipe2(rino,realct,curr_pc,0);
      break;
    
    case 326:
       osres = Linux_s_compat_rt_tgsigqueueinfo(rino,realct,curr_pc,0);
       break;

    case 1000 : 
       osres = Linux_s_restart_syscall(rino,realct,curr_pc,0);
       break;

    case 1001 : 
       osres = Linux_s_exit(rino,realct,curr_pc,0);
       break;

    case 1002 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_fork(rino,realct,curr_pc,0);
       break;

    case 1003 : 
       osres = Linux_s_read(rino,realct,curr_pc,0);
       break;

    case 1004 : 
       osres = Linux_s_write(rino,realct,curr_pc,0);
       break;

    case 1005 : 
       osres = Linux_s_open(rino,realct,curr_pc,0);
       break;

    case 1006 : 
       osres = Linux_s_close(rino,realct,curr_pc,0);
       break;

    case 1007 : 
       osres = Linux_s_wait4(rino,realct,curr_pc,0);
       break;

    case 1008 : 
       osres = Linux_s_creat(rino,realct,curr_pc,0);
       break;

    case 1009 : 
       osres = Linux_s_link(rino,realct,curr_pc,0);
       break;

    case 1010 : 
       osres = Linux_s_unlink(rino,realct,curr_pc,0);
       break;

    case 1012 : 
       osres = Linux_s_chdir(rino,realct,curr_pc,0);
       break;

    case 1013 : 
       osres = Linux_s_chown(rino,realct,curr_pc,0);
       break;

    case 1014 : 
       osres = Linux_s_mknod(rino,realct,curr_pc,0);
       break;

    case 1015 : 
       osres = Linux_s_chmod(rino,realct,curr_pc,0);
       break;

    case 1016 : 
       osres = Linux_s_lchown(rino,realct,curr_pc,0);
       break;

    case 1017 : 
       osres = Linux_s_brk(rino,realct,curr_pc,0);
       break;

    case 1018 : 
       osres = Linux_s_perfctr(rino,realct,curr_pc,0);
       break;

    case 1019 : 
       osres = Linux_s_lseek(rino,realct,curr_pc,0);
       break;

    case 1020 : 
       osres = Linux_s_getpid(rino,realct,curr_pc,0);
       break;

    case 1021 : 
       osres = Linux_s_capget(rino,realct,curr_pc,0);
       break;

    case 1022 : 
       osres = Linux_s_capset(rino,realct,curr_pc,0);
       break;

    case 1023 : 
       osres = Linux_s_setuid(rino,realct,curr_pc,0);
       break;

    case 1024 : 
       osres = Linux_s_getuid(rino,realct,curr_pc,0);
       break;

    case 1025 : 
       osres = Linux_s_vmsplice(rino,realct,curr_pc,0);
       break;

    case 1026 : 
       osres = Linux_s_ptrace(rino,realct,curr_pc,0);
       break;

    case 1027 : 
       osres = Linux_s_alarm(rino,realct,curr_pc,0);
       break;

    case 1028 : 
       osres = Linux_s_sigaltstack(rino,realct,curr_pc,0);
       break;

    case 1030 : 
       osres = Linux_s_utime(rino,realct,curr_pc,0);
       break;

    case 1033 : 
       osres = Linux_s_access(rino,realct,curr_pc,0);
       break;

    case 1034 : 
       osres = Linux_s_nice(rino,realct,curr_pc,0);
       break;

    case 1036 : 
       osres = Linux_s_sync(rino,realct,curr_pc,0);
       break;

    case 1037 : 
       osres = Linux_s_kill(rino,realct,curr_pc,0);
       break;

    case 1038 : 
       osres = Linux_s_newstat(rino,realct,curr_pc,0);
       break;

    case 1039 : 
       osres = Linux_s_sendfile64(rino,realct,curr_pc,0);
       break;

    case 1040 : 
       osres = Linux_s_newlstat(rino,realct,curr_pc,0);
       break;

    case 1041 : 
       osres = Linux_s_dup(rino,realct,curr_pc,0);
       break;

    case 1042 : 
       osres = Linux_s_SPARC_pipe(rino,realct,curr_pc,0);
       break;

    case 1043 : 
       osres = Linux_s_times(rino,realct,curr_pc,0);
       break;

    case 1045 : 
       osres = Linux_s_umount(rino,realct,curr_pc,0);
       break;

    case 1046 : 
       osres = Linux_s_setgid(rino,realct,curr_pc,0);
       break;

    case 1047 : 
       osres = Linux_s_getgid(rino,realct,curr_pc,0);
       break;

    case 1048 : 
       osres = Linux_s_signal(rino,realct,curr_pc,0);
       break;

    case 1049 : 
       osres = Linux_s_geteuid(rino,realct,curr_pc,0);
       break;

    case 1050 : 
       osres = Linux_s_getegid(rino,realct,curr_pc,0);
       break;

    case 1051 : 
       osres = Linux_s_acct(rino,realct,curr_pc,0);
       break;

    case 1052 : 
       osres = Linux_s_memory_ordering(rino,realct,curr_pc,0);
       break;

    case 1054 : 
       osres = Linux_s_ioctl(rino,realct,curr_pc,0);
       break;

    case 1055 : 
       osres = Linux_s_reboot(rino,realct,curr_pc,0);
       break;

    case 1057 : 
       osres = Linux_s_symlink(rino,realct,curr_pc,0);
       break;

    case 1058 : 
       osres = Linux_s_readlink(rino,realct,curr_pc,0);
       break;

    case 1059 : 
       osres = Linux_s_execve(rino,realct,curr_pc,0);
       break;

    case 1060 : 
       osres = Linux_s_umask(rino,realct,curr_pc,0);
       break;

    case 1061 : 
       osres = Linux_s_chroot(rino,realct,curr_pc,0);
       break;

    case 1062 : 
       osres = Linux_s_newfstat(rino,realct,curr_pc,0);
       break;

    case 1063 : 
       osres = Linux_s_fstat64(rino,realct,curr_pc,0);
       break;

    case 1064 : 
       osres = Linux_s_getpagesize(rino,realct,curr_pc,0);
       break;

    case 1065 : 
       osres = Linux_s_msync(rino,realct,curr_pc,0);
       break;

    case 1066 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_vfork(rino,realct,curr_pc,0);
       break;

    case 1067 : 
       osres = Linux_s_pread64(rino,realct,curr_pc,0);
       break;

    case 1068 : 
       osres = Linux_s_pwrite64(rino,realct,curr_pc,0);
       break;

    case 1071 : 
       osres = Linux_s_mmap(rino,realct,curr_pc,0);
       break;

    case 1073 : 
       osres = Linux_s_munmap(rino,realct,curr_pc,0);
       break;
    case 1074 : 
       osres = Linux_s_mprotect(rino,realct,curr_pc,0);
       break;

    case 1075 : 
       osres = Linux_s_madvise(rino,realct,curr_pc,0);
       break;

    case 1076 : 
       osres = Linux_s_vhangup(rino,realct,curr_pc,0);
       break;

    case 1078 : 
       osres = Linux_s_mincore(rino,realct,curr_pc,0);
       break;

    case 1079 : 
       osres = Linux_s_getgroups(rino,realct,curr_pc,0);
       break;

    case 1080 : 
       osres = Linux_s_setgroups(rino,realct,curr_pc,0);
       break;

    case 1081 : 
       osres = Linux_s_getpgrp(rino,realct,curr_pc,0);
       break;

    case 1083 : 
       osres = Linux_s_setitimer(rino,realct,curr_pc,0);
       break;

    case 1085 : 
       osres = Linux_s_swapon(rino,realct,curr_pc,0);
       break;

    case 1086 : 
       osres = Linux_s_getitimer(rino,realct,curr_pc,0);
       break;

    case 1088 : 
       osres = Linux_s_sethostname(rino,realct,curr_pc,0);
       break;

    case 1090 : 
       osres = Linux_s_dup2(rino,realct,curr_pc,0);
       break;

    case 1092 : 
       osres = Linux_s_fcntl(rino,realct,curr_pc,0);
       break;

    case 1093 : 
       osres = Linux_s_select(rino,realct,curr_pc,0);
       break;

    case 1095 : 
       osres = Linux_s_fsync(rino,realct,curr_pc,0);
       break;

    case 1096 : 
       osres = Linux_s_setpriority(rino,realct,curr_pc,0);
       break;

    case 1097 : 
       osres = Linux_s_socket(rino,realct,curr_pc,0);
       break;

    case 1098 : 
       osres = Linux_s_connect(rino,realct,curr_pc,0);
       break;

    case 1099 : 
       osres = Linux_s_accept(rino,realct,curr_pc,0);
       break;

    case 1100 : 
       osres = Linux_s_getpriority(rino,realct,curr_pc,0);
       break;

    case 1101 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_rt_sigreturn(rino,realct,curr_pc,0);
       following_pc = realct->PC;
       break;

    case 1102 : 
       osres = Linux_s_SPARC_rt_sigaction(rino,realct,curr_pc,0);
       break;

    case 1103 : 
       osres = Linux_s_rt_sigprocmask(rino,realct,curr_pc,0);
       break;

    case 1104 : 
       osres = Linux_s_rt_sigpending(rino,realct,curr_pc,0);
       break;

    case 1105 : 
       osres = Linux_s_rt_sigtimedwait(rino,realct,curr_pc,0);
       break;

    case 1106 : 
       osres = Linux_s_rt_sigqueueinfo(rino,realct,curr_pc,0);
       break;

    case 1107 : 
       osres = Linux_s_rt_sigsuspend(rino,realct,curr_pc,0);
       break;

    case 1108 : 
       osres = Linux_s_setresuid(rino,realct,curr_pc,0);
       break;

    case 1109 : 
       osres = Linux_s_getresuid(rino,realct,curr_pc,0);
       break;

    case 1110 : 
       osres = Linux_s_setresgid(rino,realct,curr_pc,0);
       break;

    case 1111 : 
       osres = Linux_s_getresgid(rino,realct,curr_pc,0);
       break;

    case 1113 : 
       osres = Linux_s_recvmsg(rino,realct,curr_pc,0);
       break;

    case 1114 : 
       osres = Linux_s_sendmsg(rino,realct,curr_pc,0);
       break;

    case 1116 : 
       osres = Linux_s_gettimeofday(rino,realct,curr_pc,0);
       break;

    case 1117 : 
       osres = Linux_s_getrusage(rino,realct,curr_pc,0);
       break;

    case 1118 : 
       osres = Linux_s_getsockopt(rino,realct,curr_pc,0);
       break;

    case 1119 : 
       osres = Linux_s_getcwd(rino,realct,curr_pc,0);
       break;

    case 1120 : 
       osres = Linux_s_readv(rino,realct,curr_pc,0);
       break;

    case 1121 : 
       osres = Linux_s_writev(rino,realct,curr_pc,0);
       break;

    case 1122 : 
       osres = Linux_s_settimeofday(rino,realct,curr_pc,0);
       break;

    case 1123 : 
       osres = Linux_s_fchown(rino,realct,curr_pc,0);
       break;

    case 1124 : 
       osres = Linux_s_fchmod(rino,realct,curr_pc,0);
       break;

    case 1125 : 
       osres = Linux_s_recvfrom(rino,realct,curr_pc,0);
       break;

    case 1126 : 
       osres = Linux_s_setreuid(rino,realct,curr_pc,0);
       break;

    case 1127 : 
       osres = Linux_s_setregid(rino,realct,curr_pc,0);
       break;

    case 1128 : 
       osres = Linux_s_rename(rino,realct,curr_pc,0);
       break;

    case 1129 : 
       osres = Linux_s_truncate(rino,realct,curr_pc,0);
       break;

    case 1130 : 
       osres = Linux_s_ftruncate(rino,realct,curr_pc,0);
       break;

    case 1131 : 
       osres = Linux_s_flock(rino,realct,curr_pc,0);
       break;

    case 1132 : 
       osres = Linux_s_lstat64(rino,realct,curr_pc,0);
       break;

    case 1133 : 
       osres = Linux_s_sendto(rino,realct,curr_pc,0);
       break;

    case 1134 : 
       osres = Linux_s_shutdown(rino,realct,curr_pc,0);
       break;

    case 1135 : 
       osres = Linux_s_socketpair(rino,realct,curr_pc,0);
       break;

    case 1136 : 
       osres = Linux_s_mkdir(rino,realct,curr_pc,0);
       break;

    case 1137 : 
       osres = Linux_s_rmdir(rino,realct,curr_pc,0);
       break;

    case 1138 : 
       osres = Linux_s_utimes(rino,realct,curr_pc,0);
       break;

    case 1139 : 
       osres = Linux_s_stat64(rino,realct,curr_pc,0);
       break;

    case 1140 : 
       osres = Linux_s_sendfile64(rino,realct,curr_pc,0);
       break;

    case 1141 : 
       osres = Linux_s_getpeername(rino,realct,curr_pc,0);
       break;

    case 1142 : 
       osres = Linux_s_futex(rino,realct,curr_pc,0);
       break;

    case 1143 : 
       osres = Linux_s_gettid(rino,realct,curr_pc,0);
       break;

    case 1144 : 
       osres = Linux_s_getrlimit(rino,realct,curr_pc,0);
       break;

    case 1145 : 
       osres = Linux_s_setrlimit(rino,realct,curr_pc,0);
       break;

    case 1146 : 
       osres = Linux_s_pivot_root(rino,realct,curr_pc,0);
       break;

    case 1147 : 
       osres = Linux_s_prctl(rino,realct,curr_pc,0);
       break;

    case 1148 : 
       osres = Linux_s_pciconfig_read(rino,realct,curr_pc,0);
       break;

    case 1149 : 
       osres = Linux_s_pciconfig_write(rino,realct,curr_pc,0);
       break;

    case 1150 : 
       osres = Linux_s_getsockname(rino,realct,curr_pc,0);
       break;

    case 1151 : 
       osres = Linux_s_inotify_init(rino,realct,curr_pc,0);
       break;

    case 1152 : 
       osres = Linux_s_inotify_add_watch(rino,realct,curr_pc,0);
       break;

    case 1153 : 
       osres = Linux_s_poll(rino,realct,curr_pc,0);
       break;

    case 1154 : 
       osres = Linux_s_getdents64(rino,realct,curr_pc,0);
       break;

    case 1156 : 
       osres = Linux_s_inotify_rm_watch(rino,realct,curr_pc,0);
       break;

    case 1157 : 
       osres = Linux_s_statfs(rino,realct,curr_pc,0);
       break;

    case 1158 : 
       osres = Linux_s_fstatfs(rino,realct,curr_pc,0);
       break;

    case 1159 : 
       osres = Linux_s_old_umount(rino,realct,curr_pc,0);
       break;

    case 1160 : 
       osres = Linux_s_sched_set_affinity(rino,realct,curr_pc,0);
       break;

    case 1161 : 
       osres = Linux_s_sched_get_affinity(rino,realct,curr_pc,0);
       break;

    case 1162 : 
       osres = Linux_s_getdomainname(rino,realct,curr_pc,0);
       break;

    case 1163 : 
       osres = Linux_s_setdomainname(rino,realct,curr_pc,0);
       break;

    case 1164 : 
       osres = Linux_s_utrap_install(rino,realct,curr_pc,0);
       break;

    case 1165 : 
       osres = Linux_s_quotactl(rino,realct,curr_pc,0);
       break;

    case 1166 : 
       osres = Linux_s_set_tid_address(rino,realct,curr_pc,0);
       break;

    case 1167 : 
       osres = Linux_s_mount(rino,realct,curr_pc,0);
       break;

    case 1168 : 
       osres = Linux_s_ustat(rino,realct,curr_pc,0);
       break;

    case 1169 : 
       osres = Linux_s_setxattr(rino,realct,curr_pc,0);
       break;

    case 1170 : 
       osres = Linux_s_lsetxattr(rino,realct,curr_pc,0);
       break;

    case 1171 : 
       osres = Linux_s_fsetxattr(rino,realct,curr_pc,0);
       break;

    case 1172 : 
       osres = Linux_s_getxattr(rino,realct,curr_pc,0);
       break;

    case 1173 : 
       osres = Linux_s_lgetxattr(rino,realct,curr_pc,0);
       break;

    case 1174 : 
       osres = Linux_s_getdents(rino,realct,curr_pc,0);
       break;

    case 1175 : 
       osres = Linux_s_setsid(rino,realct,curr_pc,0);
       break;

    case 1176 : 
       osres = Linux_s_fchdir(rino,realct,curr_pc,0);
       break;

    case 1177 : 
       osres = Linux_s_fgetxattr(rino,realct,curr_pc,0);
       break;

    case 1178 : 
       osres = Linux_s_listxattr(rino,realct,curr_pc,0);
       break;

    case 1179 : 
       osres = Linux_s_llistxattr(rino,realct,curr_pc,0);
       break;

    case 1180 : 
       osres = Linux_s_flistxattr(rino,realct,curr_pc,0);
       break;

    case 1181 : 
       osres = Linux_s_removexattr(rino,realct,curr_pc,0);
       break;

    case 1182 : 
       osres = Linux_s_lremovexattr(rino,realct,curr_pc,0);
       break;

    case 1185 : 
       osres = Linux_s_setpgid(rino,realct,curr_pc,0);
       break;

    case 1186 : 
       osres = Linux_s_fremovexattr(rino,realct,curr_pc,0);
       break;

    case 1187 : 
       osres = Linux_s_tkill(rino,realct,curr_pc,0);
       break;

    case 1188 : 
       osres = Linux_s_exit_group(rino,realct,curr_pc,0);
       break;

    case 1189 : 
       osres = Linux_s_newuname(rino,realct,curr_pc,0);
       break;
    case 1190 : 
       osres = Linux_s_init_module(rino,realct,curr_pc,0);
       break;

    case 1191 : 
       osres = Linux_s_sparc64_personality(rino,realct,curr_pc,0);
       break;

    case 1192 : 
       osres = Linux_s_remap_file_pages(rino,realct,curr_pc,0);
       break;

    case 1193 : 
       osres = Linux_s_epoll_create(rino,realct,curr_pc,0);
       break;

    case 1194 : 
       osres = Linux_s_epoll_ctl(rino,realct,curr_pc,0);
       break;

    case 1195 : 
       osres = Linux_s_epoll_wait(rino,realct,curr_pc,0);
       break;

    case 1196 : 
       osres = Linux_s_ioprio_set(rino,realct,curr_pc,0);
       break;

    case 1197 : 
       osres = Linux_s_getppid(rino,realct,curr_pc,0);
       break;

    case 1199 : 
       osres = Linux_s_sgetmask(rino,realct,curr_pc,0);
       break;

    case 1200 : 
       osres = Linux_s_ssetmask(rino,realct,curr_pc,0);
       break;

    case 1202 : 
       osres = Linux_s_newlstat(rino,realct,curr_pc,0);
       break;

    case 1203 : 
       osres = Linux_s_uselib(rino,realct,curr_pc,0);
       break;

    case 1205 : 
       osres = Linux_s_readahead(rino,realct,curr_pc,0);
       break;

    case 1206 : 
       osres = Linux_s_socketcall(rino,realct,curr_pc,0);
       break;

    case 1207 : 
       osres = Linux_s_syslog(rino,realct,curr_pc,0);
       break;

    case 1208 : 
       osres = Linux_s_lookup_dcookie(rino,realct,curr_pc,0);
       break;

    case 1209 : 
       osres = Linux_s_fadvise64(rino,realct,curr_pc,0);
       break;

    case 1210 : 
       osres = Linux_s_fadvise64_64(rino,realct,curr_pc,0);
       break;

    case 1211 : 
       osres = Linux_s_tgkill(rino,realct,curr_pc,0);
       break;

    case 1212 : 
       osres = Linux_s_waitpid(rino,realct,curr_pc,0);
       break;

    case 1213 : 
       osres = Linux_s_swapoff(rino,realct,curr_pc,0);
       break;

    case 1214 : 
       osres = Linux_s_sysinfo(rino,realct,curr_pc,0);
       break;

    case 1215 : 
       osres = Linux_s_ipc(rino,realct,curr_pc,0);
       break;

    case 1217 : 
       flush_register_windows(realct,f,osct->is64bit, true);
       osres = Linux_s_clone(rino,realct,curr_pc,0);
       break;

    case 1218 : 
       osres = Linux_s_ioprio_get(rino,realct,curr_pc,0);
       break;

    case 1219 : 
       osres = Linux_s_adjtimex(rino,realct,curr_pc,0);
       break;

    case 1222 : 
       osres = Linux_s_delete_module(rino,realct,curr_pc,0);
       break;

    case 1224 : 
       osres = Linux_s_getpgid(rino,realct,curr_pc,0);
       break;

    case 1225 : 
       osres = Linux_s_bdflush(rino,realct,curr_pc,0);
       break;

    case 1226 : 
       osres = Linux_s_sysfs(rino,realct,curr_pc,0);
       break;

    case 1228 : 
       osres = Linux_s_setfsuid(rino,realct,curr_pc,0);
       break;

    case 1229 : 
       osres = Linux_s_setfsgid(rino,realct,curr_pc,0);
       break;

    case 1230 : 
       osres = Linux_s_select(rino,realct,curr_pc,0);
       break;

    case 1232 : 
       osres = Linux_s_splice(rino,realct,curr_pc,0);
       break;

    case 1233 : 
       osres = Linux_s_stime(rino,realct,curr_pc,0);
       break;

    case 1234 : 
       osres = Linux_s_statfs64(rino,realct,curr_pc,0);
       break;

    case 1235 : 
       osres = Linux_s_fstatfs64(rino,realct,curr_pc,0);
       break;

    case 1236 : 
       osres = Linux_s__llseek(rino,realct,curr_pc,0);
       break;

    case 1237 : 
       osres = Linux_s_mlock(rino,realct,curr_pc,0);
       break;

    case 1238 : 
       osres = Linux_s_munlock(rino,realct,curr_pc,0);
       break;

    case 1239 : 
       osres = Linux_s_mlockall(rino,realct,curr_pc,0);
       break;

    case 1240 : 
       osres = Linux_s_munlockall(rino,realct,curr_pc,0);
       break;

    case 1241 : 
       osres = Linux_s_sched_setparam(rino,realct,curr_pc,0);
       break;

    case 1242 : 
       osres = Linux_s_sched_getparam(rino,realct,curr_pc,0);
       break;

    case 1243 : 
       osres = Linux_s_sched_setscheduler(rino,realct,curr_pc,0);
       break;

    case 1244 : 
       osres = Linux_s_sched_getscheduler(rino,realct,curr_pc,0);
       break;

    case 1245 : 
       osres = Linux_s_sched_yield(rino,realct,curr_pc,0);
       break;

    case 1246 : 
       osres = Linux_s_sched_get_priority_max(rino,realct,curr_pc,0);
       break;

    case 1247 : 
       osres = Linux_s_sched_get_priority_min(rino,realct,curr_pc,0);
       break;

    case 1248 : 
       osres = Linux_s_sched_rr_get_interval(rino,realct,curr_pc,0);
       break;

    case 1249 : 
       osres = Linux_s_nanosleep(rino,realct,curr_pc,0);
       break;

    case 1250 : 
       osres = Linux_s_mremap(rino,realct,curr_pc,0);
       break;
    case 1251 : 
       osres = Linux_s__sysctl(rino,realct,curr_pc,0);
       break;

    case 1252 : 
       osres = Linux_s_getsid(rino,realct,curr_pc,0);
       break;

    case 1253 : 
       osres = Linux_s_fdatasync(rino,realct,curr_pc,0);
       break;

    case 1254 : 
       osres = Linux_s_nfsservctl(rino,realct,curr_pc,0);
       break;

#ifdef NOT_WANTED_SYSCALL
    case 1255 : 
       osres = Linux_s_sync_file_range(rino,realct,curr_pc,0);
       break;
#endif

    case 1256 : 
       osres = Linux_s_clock_settime(rino,realct,curr_pc,0);
       break;

    case 1257 : 
       osres = Linux_s_clock_gettime(rino,realct,curr_pc,0);
       break;

    case 1258 : 
       osres = Linux_s_clock_getres(rino,realct,curr_pc,0);
       break;

    case 1259 : 
       osres = Linux_s_clock_nanosleep(rino,realct,curr_pc,0);
       break;

    case 1260 : 
       osres = Linux_s_sched_getaffinity(rino,realct,curr_pc,0);
       break;

    case 1261 : 
       osres = Linux_s_sched_setaffinity(rino,realct,curr_pc,0);
       break;

    case 1262 : 
       osres = Linux_s_timer_settime(rino,realct,curr_pc,0);
       break;

    case 1263 : 
       osres = Linux_s_timer_gettime(rino,realct,curr_pc,0);
       break;

    case 1264 : 
       osres = Linux_s_timer_getoverrun(rino,realct,curr_pc,0);
       break;

    case 1265 : 
       osres = Linux_s_timer_delete(rino,realct,curr_pc,0);
       break;

    case 1266 : 
       osres = Linux_s_timer_create(rino,realct,curr_pc,0);
       break;

    case 1268 : 
       osres = Linux_s_io_setup(rino,realct,curr_pc,0);
       break;

    case 1269 : 
       osres = Linux_s_io_destroy(rino,realct,curr_pc,0);
       break;

    case 1270 : 
       osres = Linux_s_io_submit(rino,realct,curr_pc,0);
       break;

    case 1271 : 
       osres = Linux_s_io_cancel(rino,realct,curr_pc,0);
       break;

    case 1272 : 
       osres = Linux_s_io_getevents(rino,realct,curr_pc,0);
       break;

    case 1273 : 
       osres = Linux_s_mq_open(rino,realct,curr_pc,0);
       break;

    case 1274 : 
       osres = Linux_s_mq_unlink(rino,realct,curr_pc,0);
       break;

    case 1275 : 
       osres = Linux_s_mq_timedsend(rino,realct,curr_pc,0);
       break;

    case 1276 : 
       osres = Linux_s_mq_timedreceive(rino,realct,curr_pc,0);
       break;

    case 1277 : 
       osres = Linux_s_mq_notify(rino,realct,curr_pc,0);
       break;

    case 1278 : 
       osres = Linux_s_mq_getsetattr(rino,realct,curr_pc,0);
       break;

    case 1279 : 
       osres = Linux_s_waitid(rino,realct,curr_pc,0);
       break;

    case 1280 :
      osres = Linux_s_tee(rino,realct,curr_pc,0);
      break;

    case 1281 :
      osres = Linux_s_add_key(rino,realct,curr_pc,0);
      break;

    case 1282 :
      osres = Linux_s_request_key(rino,realct,curr_pc,0);
      break;

    case 1283 :
      osres = Linux_s_keyctl(rino,realct,curr_pc,0);
      break;

    case 1284 :
      osres = Linux_s_openat(rino,realct,curr_pc,0);
      break;

    case 1285 :
      osres = Linux_s_mkdirat(rino,realct,curr_pc,0);
      break;

    case 1286 :
      osres = Linux_s_mknodat(rino,realct,curr_pc,0);
      break;

    case 1287 :
      osres = Linux_s_fchownat(rino,realct,curr_pc,0);
      break;

    case 1288 :
      osres = Linux_s_futimesat(rino,realct,curr_pc,0);
      break;

    case 1289 :
      osres = Linux_s_fstatat(rino,realct,curr_pc,0);
      break;

    case 1290 :
      osres = Linux_s_unlinkat(rino,realct,curr_pc,0);
      break;

    case 1291 :
      osres = Linux_s_renameat(rino,realct,curr_pc,0);
      break;

    case 1292 :
      osres = Linux_s_linkat(rino,realct,curr_pc,0);
      break;

    case 1293 :
      osres = Linux_s_symlinkat(rino,realct,curr_pc,0);
      break;

    case 1294 :
      osres = Linux_s_readlinkat(rino,realct,curr_pc,0);
      break;

    case 1295 :
      osres = Linux_s_fchmodat(rino,realct,curr_pc,0);
      break;

    case 1296 :
      osres = Linux_s_faccessat(rino,realct,curr_pc,0);
      break;

    case 1297 :
      osres = Linux_s_pselect6(rino,realct,curr_pc,0);
      break;

    case 1298 :
      osres = Linux_s_ppoll(rino,realct,curr_pc,0);
      break;

    case 1299 :
      osres = Linux_s_unshare(rino,realct,curr_pc,0);
      break;

    case 1300 :
      osres = Linux_s_set_robust_list(rino,realct,curr_pc,0);
      break;

    case 1301 :
      osres = Linux_s_get_robust_list(rino,realct,curr_pc,0);
      break;

    case 1302 :
      osres = Linux_s_migrate_pages(rino,realct,curr_pc,0);
      break;

    case 1303 :
      osres = Linux_s_mbind(rino,realct,curr_pc,0);
      break;

    case 1304 :
      osres = Linux_s_get_mempolicy(rino,realct,curr_pc,0);
      break;

    case 1305 :
      osres = Linux_s_set_mempolicy(rino,realct,curr_pc,0);
      break;

    case 1306 :
      osres = Linux_s_kexec_load(rino,realct,curr_pc,0);
      break;

    case 1307 :
      osres = Linux_s_move_pages(rino,realct,curr_pc,0);
      break;

    case 1308 :
      osres = Linux_s_getcpu(rino,realct,curr_pc,0);
      break;

    case 1309 :
      osres = Linux_s_epoll_pwait(rino,realct,curr_pc,0);
      break;

    case 1310 :
      osres = Linux_s_utimensat(rino,realct,curr_pc,0);
      break;

    case 1311 :
      osres = Linux_s_signalfd(rino,realct,curr_pc,0);
      break;

    case 1312 :
      osres = Linux_s_timerfd_create(rino,realct,curr_pc,0);
      break;

    case 1313 :
      osres = Linux_s_eventfd(rino,realct,curr_pc,0);
      break;

    case 1314 :
      osres = Linux_s_fallocate(rino,realct,curr_pc,0);
      break;

    case 1315 :
      osres = Linux_s_timerfd_settime(rino,realct,curr_pc,0);
      break;

    case 1316 :
      osres = Linux_s_timerfd_gettime(rino,realct,curr_pc,0);
      break;

    case 1317 :
      osres = Linux_s_signalfd4(rino,realct,curr_pc,0);
      break;

    case 1318 :
      osres = Linux_s_eventfd2(rino,realct,curr_pc,0);
      break;

    case 1319 :
      osres = Linux_s_epoll_create1(rino,realct,curr_pc,0);
      break;

    case 1320 :
      osres = Linux_s_dup3(rino,realct,curr_pc,0);
      break;

    case 1321 :
      osres = Linux_s_pipe2(rino,realct,curr_pc,0);
      break;

    case 1322 :
      osres = Linux_s_inotify_init1(rino,realct,curr_pc,0);
      break;

#ifdef NOT_WANTED_SYSCALL
    case 1323 :
      osres = Linux_s_accept4(rino,realct,curr_pc,0);
      break;
#endif

    case 1324 :
      osres = Linux_s_preadv(rino,realct,curr_pc,0);
      break;

    case 1325 :
      osres = Linux_s_pwritev(rino,realct,curr_pc,0);
      break;

    case 1326 :
      osres = Linux_s_rt_tgsigqueueinfo(rino,realct,curr_pc,0);
      break;

    case 2000 : 
       osres = Linux_s_SPARC_getcontext(rino,realct,curr_pc,0);
       break;

    case 2001 : 
       osres = Linux_s_SPARC_setcontext(rino,realct,curr_pc,0);
       following_pc = realct->PC;
       break;

    default:
       fprintf(stderr,"Unimplemented Syscall: %d\n",rino);
       exit(1);
/*
     CC_sys_set_return(realct,0,-1);
     CC_sys_set_return(realct,1,OSDefs::oENOSYS);
     // *get_r_ptr(realct, 1, realct->PR[CWP]) = -(uint32_t)SPARC_Linux64__ENOSYS;
*/
     break;
  }

  if (OS_ict_osinfo(realct)->state == ProcBlocked &&
      OS_ict_osinfo(realct)->sigPendingFlag) { 
    // we blocked with a signal pending!
    OS_ict_osinfo(realct)->state = ProcRunnable;
    OS_ict_osinfo(realct)->os->ready_list.push_back(OS_ict_osinfo(realct));
    OS_ict_osinfo(realct)->os->wantScheduling = true; 
  } else 
    CC_do_a_signal(realct, rino != 101 && rino != 1101 && rino != 216,
		   curr_pc, following_pc);

  if (OS_ict_osinfo(realct)->state != ProcBlocked 
      && rino < 2000)
    OS_ict_osinfo(realct)->insyscall = false;

  if (OS_ict_osinfo(realct)->os->wantScheduling)
    Linux_schedule(&CC_emu_interface(realct));    

  following_pc = realct->PC;
    
  return 0;
}

bool Linux_is64bit_context(LSE_emu_isacontext_t *realct) {
  Linux_context_t *osct=OS_ict_osinfo(realct);
  return osct->is64bit;
}


} // namespace LSE_SPARC
