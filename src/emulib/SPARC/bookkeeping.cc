/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Bookkeeping routines
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file contains routines meant to handle the bookkeeping:
 * 1) context creation, deletion, loading
 * 2) context mapping
 * 3) emulator initialization/finalization
 * 4) PC manipulation
 * 5) Instruction info initialization
 *
 */

#include <LSE_inttypes.h>
#include "LSE_SPARC.priv.h"
#include <stdio.h>

namespace LSE_SPARC {

#ifdef LSE_LIS_SPARC_LINUX_EMU
  extern int Linux_context_init(LSE_emu_isacontext_t *);
  extern int Linux_context_load(LSE_emu_isacontext_t *,int,char **,char **);
  extern int Linux_init(LSE_emu_interface_t *);
  extern int Linux_finish(LSE_emu_interface_t *);
  extern void Linux_schedule(LSE_emu_interface_t *);
  extern int Linux_context_clone(LSE_emu_interface_t *ifc,
                                 SPARC_context_t **, SPARC_context_t *);
  extern bool Linux_handle_fault(LSE_emu_isacontext_t *ctx, SPARC_fault_t f,
				 LSE_emu_iaddr_t pc, LSE_emu_iaddr_t &fpc);
#endif
#ifdef LSE_LIS_SPARC_SOLARIS_EMU
  extern int Solaris_context_init(LSE_emu_isacontext_t *);
  extern int Solaris_context_load(LSE_emu_isacontext_t *,int,char **,char **);
  extern int Solaris_init(LSE_emu_interface_t *);
  extern void Solaris_schedule(LSE_emu_interface_t *);
#endif

  // **************** Emulator initialization and finalization *******

  void EMU_init(LSE_emu_interface_t *ifc) {
    SPARC_dinst_t *di = new SPARC_dinst_t(ifc);
    ifc->etoken = static_cast<void *>(di);
#ifdef CHECKPOINTS
    emu_checkpoint_init();
#endif
#ifdef LSE_LIS_SPARC_LINUX_EMU
    Linux_init(ifc);
#endif
#ifdef LSE_LIS_SPARC_SOLARIS_EMU
    Solaris_init(ifc);
#endif
  }

  void EMU_finish(LSE_emu_interface_t *ifc) {
#ifdef CHECKPOINTS
    emu_checkpoint_finish();
#endif
#ifdef LSE_LIS_SPARC_LINUX_EMU
    Linux_finish(ifc);
#endif

    // TODO: delete software and hardware contexts
    if (ifc->etoken) delete static_cast<SPARC_dinst_t *>(ifc->etoken);
    ifc->etoken = 0;
  }

  // ***************** Play with the current PC **********************

  LSE_emu_iaddr_t EMU_get_start_addr(LSE_emu_ctoken_t ct) {
    LSE_emu_isacontext_t& ctx = *(static_cast < LSE_emu_isacontext_t * >(ct));
    return ctx.PC;
  }
  void EMU_set_start_addr(LSE_emu_ctoken_t ct, LSE_emu_iaddr_t addr) {
    LSE_emu_isacontext_t& ctx = *(static_cast < LSE_emu_isacontext_t * >(ct));
    ctx.PC = addr;
  }

  // ******************** Context creation ****************************

  /*
   * Copy a context
   */
  int EMU_context_copy(LSE_emu_interface_t *ifc,
		       LSE_emu_isacontext_t **ctpp, 
		       LSE_emu_isacontext_t *oldct) {
    LSE_emu_isacontext_t *realct;
    SPARC_dinst_t *di = static_cast<SPARC_dinst_t *>(ifc->etoken);

    realct = new LSE_emu_isacontext_t;
    if (!realct) {
      fprintf(stderr,"Out of memory creating LSE SPARC emulator context\n");
      return 1;
    }
    if (oldct) {
      *realct = *oldct;
      realct->mmu.init(realct);
      // TODO: clear MMU
    } else {
      // This code only gets used for clean HW contexts or an OS init context
      // Presumable execve handles any state setup to match OS emulation
      realct->di = di;
      memset(realct->R, 0, sizeof(realct->R));
      memset(realct->F, 0, sizeof(realct->F));
      realct->FSR = 0;
      memset(realct->PR, 0, sizeof(realct->PR));
      memset(realct->ASR, 0, sizeof(realct->ASR));
      memset(realct->HPR, 0, sizeof(realct->HPR));
      realct->mem = di->mem;
      realct->done = false;
      realct->mmu.init(realct);
      realct->osType = 0;
      realct->osData = 0;
      realct->sharedcore = realct;
      realct->sharedchip = realct;

      // clear everything so the valgrind won't be annoyed.  But I cannot
      // simply memset the whole structure!

      if (true || !di->EMU_emulateOS) {
        realct->PC.PC = LSE_version_selector::RSTVADDR + 0x20;
        realct->PC.NPC = LSE_version_selector::RSTVADDR + 0x24;
        realct->PC.annul = false;
        realct->PC.check_level_zero = false;
        realct->PC.interrupted = false;

        // NOTE: the privM is set because SAS does it
        *get_pr_ptr(realct, PSTATE) = pstate_pefM | pstate_privM;
        *get_hpr_ptr(realct, HPSTATE) = hpstate_redM | hpstate_hprivM;
        *get_pr_ptr(realct, TT, MAXTL) = 1;
        *get_pr_ptr(realct, TL) = MAXTL;
        *get_pr_ptr(realct, GL) = MAXGL;
        *get_asr_ptr(realct, TICKP) = tick_nptM;
        *get_hpr_ptr(realct, HVER) = ( (MAXGL << 16) | (MAXTL <<8) | 
				   (N_REG_WINDOWS-1) );
        // manual disagrees, but Legion does this
        *get_asr_ptr(realct, TICK_CMPR) = tickcmp_int_disM;
        *get_asr_ptr(realct, STICK_CMPR) = stickcmp_int_disM;
        *get_hpr_ptr(realct, HSTICK_CMPR) = hstickcmp_int_disM;
        *get_asr_ptr(realct, TICKP) = tick_nptM;
        *get_asr_ptr(realct, STICK) = stick_nptM;
      
        // amazingly, the RTL diags for the T1 rely upon these being a reasonable
        // value.  I guess this is the T1 power-on value (not defined POR value!)
        *get_pr_ptr(realct, CWP) = 0;
        *get_asr_ptr(realct, SOFTINT) = 0;
        *get_hpr_ptr(realct, HINTP) = 0;
        *get_asr_ptr(realct, GSR) = 0;

        // 
        *get_asr_ptr(realct, 26) = 0; // create disabled to start with
      
        /* initialize the %g0s to 0 */
        for (int i = 0; i <= MAXGL; i++) 
	  *get_r_ptr(realct, 0, 0, i) = 0;

        // TODO: a few more registers
        // Icache control off
        // Dcache control off
        // MMU control off
        // L2 cache on
        // CMT: STRAND_ENABLE/XIR_STEERING/STRAND_RUNNING/STRAND_ID/CORE_INTR_ID 
        // (p. 652)
      }
    }
    realct->coreNo = realct->strandNo = 0;
    realct->globalcno = 0;
    realct->users = 0;

    realct->mmu.clear();
    //realct->softmmu.clear();

#ifdef TOMOVE
#ifdef CHECKPOINTING
    realct->OSRecords = LSE_chkpt_build_sequence(NULL);
    realct->currOSRecord = NULL;
    realct->wasInitialOS = FALSE;
#endif
#endif
    *ctpp = realct;
    return 0;
  }

  /* 
   * Called by simulator, but not emulators to create hardware contexts
   */
  int EMU_context_create(LSE_emu_interface_t *ifc,
                         LSE_emu_ctoken_t *ctp, int cno)
  {
    SPARC_dinst_t *di = static_cast<SPARC_dinst_t *>(ifc->etoken);
    if (cno >= 0) {
      *ctp = 0;
      if (!di->EMU_emulateOS) {
	LSE_emu_isacontext_t *ctx;
	EMU_context_copy(ifc, &ctx, 0);
	di->hwclocks[cno].c[0] = 0;
	di->hwclocks[cno].c[1] = 0;
	ctx->globalcno = cno;
	*ctp = (LSE_emu_ctoken_t)(ctx);
      }
      return 0; 
    } else return 1;
  }

  int EMU_context_destroy(LSE_emu_interface_t *ifc,
			  LSE_emu_ctoken_t ct) {
    LSE_emu_isacontext_t *ctx = reinterpret_cast<LSE_emu_isacontext_t *>(ct);
    delete ctx;
  }

  // ********************** timed capability *******************

  int EMU_register_clock(LSE_emu_interface_t *ifc, int hwcno, int clockno,
			 LSE_clock_t clock) {
    SPARC_dinst_t *di = static_cast<SPARC_dinst_t *>(ifc->etoken);
    if (clockno < 0 || clockno > 1) return 1;
    di->hwclocks[hwcno].c[clockno] = clock;
    return 0;
  }

  int EMU_register_interrupt_callback(LSE_emu_interface_t *ifc, 
				      int hwcno, 
				      LSE_emu_interrupt_callback_t h) {
    SPARC_dinst_t *di = static_cast<SPARC_dinst_t *>(ifc->etoken);
    di->interrupts[hwcno] = h;
    return 0;
  }

  // ******************** Context loading *****************************

  int EMU_context_load(LSE_emu_interface_t *ifc, int cno,
			int argc, char *argv[], char **envp) {

    SPARC_dinst_t *di = static_cast<SPARC_dinst_t *>(ifc->etoken);

    if (cno <= 0 || cno > LSE_emu_hwcontexts_total ||
	!LSE_emu_hwcontexts_table[cno].valid) return 1;

    int rval;
#ifdef LSE_LIS_SPARC_LINUX_EMU
    LSE_emu_isacontext_t *realctp 
      = reinterpret_cast<LSE_emu_isacontext_t *>(LSE_emu_hwcontexts_table[cno].ctok);

    if (!realctp) {
      int ret = Linux_context_clone(ifc, &realctp, 0);
      if (ret) return ret;
      realctp->globalcno = cno;
      *get_asr_ptr(realctp, 26) = 1;
      LSE_emu_update_context_map(cno, realctp);
    }
#endif

    LSE_env::env_t tempenv((const char **)envp);
    tempenv.setenv(*di->evars, true);

    char **newenvp = tempenv.getenvbuf();

    switch (di->EMU_emulateOS) {
#ifdef LSE_LIS_SPARC_LINUX_EMU
    case 1:
      rval = Linux_context_load(realctp, argc, argv, newenvp);
      if (rval) fprintf(stderr,
			"LSE_SPARC Linux emulator:Unable to read executable "
			"\"%s\", errno was %d\n",
			argv[0],-rval);
      if (newenvp) free(newenvp);
      return rval;
#endif
#ifdef LSE_LIS_SPARC_SOLARIS_EMU
    case 2:
      rval = Solaris_context_load(realctp, argc, argv, newenvp);
      if (rval) fprintf(stderr,
			"LSE_SPARC Solaris emulator:Unable to read executable "
			"\"%s\", errno was %d\n",
			argv[0],-rval);
      if (newenvp) free(newenvp);
      return rval;
#endif
    default:
      if (newenvp) free(newenvp);
      return 0;
    } // switch 
  }

  // ***************** Dumping and hashing of state ******************

  void SPARC_dump_state(LSE_emu_ctoken_t ct, FILE *outfile, bool dofp) {
#ifdef LATER
    LSE_emu_isacontext_t& ctx = *(static_cast < LSE_emu_isacontext_t * >(ct));


    fprintf(outfile, "cpsr:0x%-8" PRIx32, ctx.PSR[0]);
    fprintf(outfile, "\t%c%c%c%c\n", 
	    (ctx.PSR[0] & Nmask) ? 'N' : ' ',
	    (ctx.PSR[0] & Zmask) ? 'Z' : ' ',
	    (ctx.PSR[0] & Cmask) ? 'C' : ' ',
	    (ctx.PSR[0] & Vmask) ? 'V' : ' ');

    for (int i=0; i<16/4;i++) {
      fprintf(outfile,"r%-2d 0x%8" PRIx32 " ! ",
	      i, ctx.R[i]);
      fprintf(outfile,"r%-2d 0x%8" PRIx32 " ! ",
	      i + 16/4, ctx.R[i+16/4]);
      fprintf(outfile,"r%-2d 0x%8" PRIx32 " ! ",
	      i + 2*16/4, ctx.R[i+2*16/4]);
      fprintf(outfile,"r%-2d 0x%8" PRIx32 "\n",
	      i + 3*16/4, ctx.R[i+3*16/4]);
    }
    fprintf(outfile,"--------------------------------"
	    "--------------------------------\n");

    if (dofp) {
      for (int i=0; i<16/4;i++) {
	fprintf(outfile,"f%-2d %16g ! ",
		i, ctx.F[i]);
	fprintf(outfile,"f%-2d %16g ! ",
		i + 16/4, ctx.F[i+16/4]);
	fprintf(outfile,"f%-2d %16g ! ",
		i + 2*16/4, ctx.F[i+2*16/4]);
	fprintf(outfile,"f%-2d %16g\n",
		i + 3*16/4, ctx.F[i+3*16/4]);
      } 
     
      fprintf(outfile,"--------------------------------"
	      "--------------------------------\n");
    }

#endif

  } // SPARC_dump_state

  // *************** Operand information ***************************

  boolean
  EMU_spaceaddr_is_constant(LSE_emu_ctoken_t ctx,
			    LSE_emu_spaceid_t sid,
			    LSE_emu_spaceaddr_t *addr)
  {
    return ((sid == LSE_emu_spaceid_gp && addr->gp == 0));
  }

  // ******************* Handle command-line stuff **************

  int
  EMU_parse_arg(LSE_emu_interface_t *ifc,
		int argc, char *arg, char *argv[])
  {
    SPARC_dinst_t *di = static_cast<SPARC_dinst_t *>(ifc->etoken);

    if (!strcmp(arg,"debugload")) di->EMU_debugcontextload = true;
    else if (!strcmp(arg,"tracesyscalls")) di->EMU_trace_syscalls = true;
    else if (!strcmp(arg,"tracerws")) di->trace_reg_windows = true;
    else if (!strcmp(arg,"noOS")) di->EMU_emulateOS = 0;
#ifdef LSE_LIS_SPARC_LINUX_EMU
    else if (!strcmp(arg,"linux")) di->EMU_emulateOS = 1;
#endif
#ifdef LSE_LIS_SPARC_SOLARIS_EMU
    else if (!strcmp(arg,"solaris")) di->EMU_emulateOS = 2;
#endif
    else if (!strncmp(arg,"env:",4)) {
      di->evars->setenv(arg+4, true);
    }
    else if (!strcmp(arg,"cleanenv")) {
      di->evars->clearenv();
    }
    else return di->parse_arg(argc, arg, argv);
    return 1;
  }
  
  void
  EMU_print_usage(LSE_emu_interface_t *ifc)
  {
    SPARC_dinst_t *di = static_cast<SPARC_dinst_t *>(ifc->etoken);
    fprintf(stderr,
	    "\tdebugload\t\tPrint debug messages when loading\n");
    fprintf(stderr,
            "\ttracesyscalls\t\tTrace system calls\n");
    fprintf(stderr,
            "\ttracerws\t\tTrace register windows\n");
    fprintf(stderr, "\tnoOS\t\tDo not emulate OS (default)\n");
#ifdef LSE_LIS_SPARC_LINUX_EMU
    fprintf(stderr, "\tlinux\t\tEmulate Linux\n");
#endif
#ifdef LSE_LIS_SPARC_SOLARIS_EMU
    fprintf(stderr, "\tsolaris\t\tEmulate Solaris\n");
#endif
    di->print_usage();
    return;
  }

  // ************************ translation routines **************

  void EMU_translateInstrPA(LSE_emu_ctoken_t ct, addr_t pc,
			    SPARC_addrtransl_t &flags, SPARC_fault_t &fault) {
    LSE_emu_isacontext_t& ctx = *(static_cast < LSE_emu_isacontext_t * >(ct));
    ctx.mmu.calcInstrPA(pc, flags, fault);
  }

  /********************** operand printing *************************/

  void SPARC_print_instr_oper_vals(FILE *fp, const char *prefix,
				   LSE_emu_instr_info_t *ii) {
    int k;

    fprintf(fp, "%s", prefix);

    // TODO: print out effective addresses

    for(k=0;k<LSE_emu_max_operand_dest;k++) {
      switch(ii->operand_dest[k].spaceid) {
      case LSE_emu_spaceid_LSE:
	continue;
      case LSE_emu_spaceid_gp:
	fprintf(fp,"%d:G(%d)", k, ii->operand_dest[k].spaceaddr.gp);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_dest[k].data.rval);
	break;
      case LSE_emu_spaceid_mem:
	fprintf(fp,"%d:mem(%x/%"PRIx64"/%"PRIx64")", k, ii->effAddr.asi,
		ii->operand_dest[k].spaceaddr.mem, ii->effAddr.pa);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_dest[k].data.rval);
	break;
      case LSE_emu_spaceid_fp:
	switch (ii->operand_dest[k].uses.reg.bits[0]) {
	case 1: 
	  fprintf(fp,"%d:F(%d)", k, ii->operand_dest[k].spaceaddr.fp);
	  fprintf(fp,":0x%" PRIx64 "(%g) ", 
		  ii->operand_val_dest[k].data.fval.hi,
		  conv_single(ii->operand_val_dest[k].data.fval.hi));
	  break;
	case 2: 
	  fprintf(fp,"%d:D(%d)", k, ii->operand_dest[k].spaceaddr.fp);
	  fprintf(fp,":0x%" PRIx64 "(%g) ", 
		  ii->operand_val_dest[k].data.fval.hi,
		  conv_double(ii->operand_val_dest[k].data.fval.hi));
	  break;
	case 4: 
	  fprintf(fp,"%d:Q(%d)", k, ii->operand_dest[k].spaceaddr.fp);
	  fprintf(fp,":0x%" PRIx64 "0x%" PRIx64, 
		  ii->operand_val_dest[k].data.fval.hi,
		  ii->operand_val_dest[k].data.fval.lo);
	  break;
	default:
	  fprintf(fp,"%d:D8(%d):0x", k, ii->operand_dest[k].spaceaddr.fp);
	  for (int cnt=0 ; cnt < 8 ; cnt++) {
	    fprintf(fp, "%" PRIx64, ii->lop_buf[cnt]);
	    if (cnt != 7) fprintf(fp,".");
	  }
	  break;
	}
	break;
      case LSE_emu_spaceid_fsr:
	fprintf(fp,"%d:FSR(%d)", k, ii->operand_dest[k].spaceaddr.fsr);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_dest[k].data.fsr_val);
	break;
      case LSE_emu_spaceid_asr:
	fprintf(fp,"%d:ASR(%d)", k, ii->operand_dest[k].spaceaddr.asr);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_dest[k].data.asr_val);
	break;
      case LSE_emu_spaceid_pr:
	fprintf(fp,"%d:PR(%d)", k, ii->operand_dest[k].spaceaddr.pr);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_dest[k].data.pr_val);
	break;
      case LSE_emu_spaceid_hpr:
	fprintf(fp,"%d:HPR(%d)", k, ii->operand_dest[k].spaceaddr.hpr);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_dest[k].data.rval);
	break;
      default:
	break;
      }

    }
    if (ii->addr.annul) { fprintf(fp,"(annulled)"); }
    if (ii->iclasses.is_cmove && !ii->doMove) {
      fprintf(fp, "(not moved)");
    }

    /* sources */

    fprintf(fp,"\n%s",prefix);

    for(k=0;k<LSE_emu_max_operand_src;k++) {
      switch(ii->operand_src[k].spaceid) {
      case LSE_emu_spaceid_LSE:
	continue;
      case LSE_emu_spaceid_gp:
	fprintf(fp,"%d:G(%d)", k, ii->operand_src[k].spaceaddr.gp);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_src[k].data.rval);
	break;
      case LSE_emu_spaceid_mem:
	fprintf(fp,"%d:mem(%x/%"PRIx64"/%"PRIx64")", k, ii->effAddr.asi,
		ii->operand_src[k].spaceaddr.mem, ii->effAddr.pa);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_src[k].data.rval);
	break;
      case LSE_emu_spaceid_fp:
	switch (ii->operand_src[k].uses.reg.bits[0]) {
	case 1: 
	  fprintf(fp,"%d:F(%d)", k, ii->operand_src[k].spaceaddr.fp);
	  fprintf(fp,":0x%" PRIx64 "(%g) ", 
		  ii->operand_val_src[k].data.fval.hi,
		  conv_single(ii->operand_val_src[k].data.fval.hi));
	  break;
	case 2: 
	  fprintf(fp,"%d:D(%d)", k, ii->operand_src[k].spaceaddr.fp);
	  fprintf(fp,":0x%" PRIx64 "(%g) ", 
		  ii->operand_val_src[k].data.fval.hi,
		  conv_double(ii->operand_val_src[k].data.fval.hi));
	  break;
	case 4: 
	  fprintf(fp,"%d:Q(%d)", k, ii->operand_src[k].spaceaddr.fp);
	  fprintf(fp,":0x%" PRIx64 "0x%" PRIx64, 
		  ii->operand_val_src[k].data.fval.hi,
		  ii->operand_val_src[k].data.fval.lo);
	  break;
	default:
	  fprintf(fp,"%d:D8(%d)", k, ii->operand_src[k].spaceaddr.fp);
	  for (int cnt=0;cnt<8;cnt++) {
	    fprintf(fp,"%" PRIx64 ,ii->lop_buf[cnt]);
	    if (cnt != 7) fprintf(fp,".");
	  }
	  break;
	}
	break;
       case LSE_emu_spaceid_fsr:
	fprintf(fp,"%d:FSR(%d)", k, ii->operand_src[k].spaceaddr.fsr);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_src[k].data.fsr_val);
	break;
      case LSE_emu_spaceid_asr:
	fprintf(fp,"%d:ASR(%d)", k, ii->operand_src[k].spaceaddr.asr);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_src[k].data.asr_val);
	break;
      case LSE_emu_spaceid_pr:
	fprintf(fp,"%d:PR(%d)", k, ii->operand_src[k].spaceaddr.pr);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_src[k].data.pr_val);
	break;
      case LSE_emu_spaceid_hpr:
	fprintf(fp,"%d:HPR(%d)", k, ii->operand_src[k].spaceaddr.hpr);
	fprintf(fp,":0x%" PRIx64 " ",ii->operand_val_src[k].data.hpr_val);
	break;
      default:
	break;
      }
    } // for (k)
    fprintf(fp,"\n");
  }  // SPARC_print_instr_oper_val

  void EMU_space_read(LSE_emu_spacedata_t *sdata,
		      LSE_emu_ctoken_t ctoken,
		      LSE_emu_spaceid_t sid, LSE_emu_spaceaddr_t *saddr,
		      int flags) {
    LSE_emu_isacontext_t &ctx = *((LSE_emu_isacontext_t *)ctoken);

    switch (sid) {
    case LSE_emu_spaceid_hpr:
      sdata->hpr = ctx.HPR[saddr->hpr];
      break;
    case LSE_emu_spaceid_pr:
      sdata->pr = ctx.PR[saddr->pr];
      break;
    case LSE_emu_spaceid_asr:
      sdata->asr = ctx.ASR[saddr->asr];
      break;
    default : break;
    } // switch (sid)
  } // EMU_space_read

  void EMU_space_write(LSE_emu_ctoken_t ctoken,
		       LSE_emu_spaceid_t sid, LSE_emu_spaceaddr_t *saddr,
		       LSE_emu_spacedata_t *sdata,
		       int flags) {
    LSE_emu_isacontext_t &ctx = *((LSE_emu_isacontext_t *)ctoken);

    switch (sid) {
    case LSE_emu_spaceid_gp:
    default : break;
    } // switch (sid)
  } // EMU_space_write

  uint64_t SPARC_mem_read(LSE_emu_contextno_t cno,
			  SPARC_addrtransl_t &addr, int size,
			  void *dbuf) {
    LSE_emu_isacontext_t &ctx = *get_token(cno);
    SPARC_fault_t fault;
    fault.ft = no_fault;

    if (addr.mmu_intercepted) {
      return ctx.mmu.doMMURead(addr, fault, 0);
    }
    return 0;
  }

  void SPARC_mem_write(LSE_emu_contextno_t cno,
		       SPARC_addrtransl_t &addr, int size,
		       void *dbuf) {
    LSE_emu_isacontext_t &ctx = *get_token(cno);
    SPARC_fault_t fault;
    fault.ft = no_fault;

    if (addr.mmu_intercepted) {
      ctx.mmu.doMMUWrite(addr, *(uint64_t *)dbuf, fault, 0, 0, 0);
    }
  }

  void spill_register_window(LSE_emu_isacontext_t *realctp, int cwp,
			     SPARC_fault_t &fault, bool is64bit,
			     bool bigendian) {

    if (realctp->di->trace_reg_windows)
      std::cerr << "Spilling register window " << cwp << "\n";

    if (is64bit) {
      uint64_t sp = *get_r_ptr(realctp, 14, cwp) + 0x7ff;
      for (int ii = 0; ii < 16; ii++) {
	SPARC_addrtransl_t eaddr;
	eaddr.va = sp + ii*8;
	eaddr.size = 8;
	realctp->mmu.calcPA(eaddr, writeOp, fault);
	if (realctp->di->trace_reg_windows) 
	  std::cerr << "\tmem(ffffffff/" << std::hex << eaddr.va << "/" 
		    << eaddr.pa << ") = G(" << std::dec << ii + 16 
		    << std::hex << "):0x" << *get_r_ptr(realctp, 16+ii,cwp)
		    << std::dec << "\n";
	LSE_device::aligned_access<uint64_t>(realctp->mem).
	  write(eaddr.pa, LSE_h2e(uint64_t(*get_r_ptr(realctp, 16+ii,cwp)),
				  bigendian));
      }
    } else {
      uint64_t sp = uint32_t(*get_r_ptr(realctp, 14, cwp));
      for (int ii = 0; ii < 16; ii++) {
	SPARC_addrtransl_t eaddr;
	eaddr.va = sp + ii*4;
	eaddr.size = 4;
	realctp->mmu.calcPA(eaddr, writeOp, fault);
	if (realctp->di->trace_reg_windows) 
	  std::cerr << "\tmem(ffffffff/" << std::hex << eaddr.va << "/" 
		    << eaddr.pa << ") = G(" << std::dec << ii + 16 
		    << std::hex << "):0x" 
		    << (uint32_t)(*get_r_ptr(realctp, 16+ii,cwp))
		    << std::dec << "\n";
	LSE_device::aligned_access<uint32_t>(realctp->mem).
	  write(eaddr.pa, LSE_h2e(uint32_t(*get_r_ptr(realctp, 16+ii,cwp)),
				  bigendian));
      }
    }
  }

  void fill_register_window(LSE_emu_isacontext_t *realctp, int cwp,
			     SPARC_fault_t &fault, bool is64bit,
			     bool bigendian) {

    if (realctp->di->trace_reg_windows)
      std::cerr << "Filling register window " << cwp << "\n";

    if (is64bit) {
      uint64_t sp = *get_r_ptr(realctp, 14, cwp) + 0x7ff;
      for (int ii = 0; ii < 16; ii++) {
	SPARC_addrtransl_t eaddr;
	eaddr.va = sp + ii*8;
	eaddr.size = 8;
	realctp->mmu.calcPA(eaddr, writeOp, fault);
	*get_r_ptr(realctp, 16+ii, cwp) 
	  = LSE_e2h(LSE_device::aligned_access<uint64_t>(realctp->mem).
		    read(eaddr.pa), bigendian);
	if (realctp->di->trace_reg_windows) 
	  std::cerr << "\tG(" << std::dec << ii + 16 
		    << std::hex << "):0x" << *get_r_ptr(realctp, 16+ii,cwp)
		    << " = mem(ffffffff/" << std::hex << eaddr.va << "/" 
		    << eaddr.pa << ")" << std::dec << "\n";
      }
    } else {
      uint64_t sp = uint32_t(*get_r_ptr(realctp, 14, cwp));
      for (int ii = 0; ii < 16; ii++) {
	SPARC_addrtransl_t eaddr;
	eaddr.va = sp + ii*4;
	eaddr.size = 4;
	realctp->mmu.calcPA(eaddr, writeOp, fault);
	*get_r_ptr(realctp, 16+ii, cwp) 
	  = LSE_e2h(LSE_device::aligned_access<uint32_t>(realctp->mem).
		    read(eaddr.pa), bigendian);
	if (realctp->di->trace_reg_windows) 
	  std::cerr << "\tG(" << std::dec << ii + 16 
		    << std::hex << "):0x" 
		    << (uint32_t)(*get_r_ptr(realctp, 16+ii,cwp))
		    << " = mem(ffffffff/" << std::hex << eaddr.va << "/" 
		    << eaddr.pa << ")" << std::dec << "\n";
      }
    }
  }

  void flush_register_windows(LSE_emu_isacontext_t *realctp,
			      SPARC_fault_t &fault, bool is64bit,
			      bool bigendian) {

    int cwp = *get_pr_ptr(realctp, CWP, 0); // current WP.

    // save current frame and everybody who could be restored.
    int numtosave = *get_pr_ptr(realctp, CANRESTORE,0) + 1;
    
    *get_pr_ptr(realctp, CANRESTORE, 0) = 0;
    *get_pr_ptr(realctp, CANSAVE, 0) += numtosave - 1;

    while (numtosave--) {
      spill_register_window(realctp, cwp, fault, is64bit, bigendian);
      cwp = (cwp + N_REG_WINDOWS - 1) % N_REG_WINDOWS;
    } // numtosave
    
  }

  /************************* Fault handling ******************/

  bool EMU_handle_fault(LSE_emu_ctoken_t ctoken, SPARC_fault_t &f,
			LSE_emu_iaddr_t pc, LSE_emu_iaddr_t &fpc) {
    LSE_emu_isacontext_t *realct = ((LSE_emu_isacontext_t *)ctoken);
    bool keepfault;

    switch (realct->di->EMU_emulateOS) {
#ifdef LSE_LIS_SPARC_LINUX_EMU
    case 1: 
      keepfault = Linux_handle_fault(realct, f, pc, fpc);
      if (!keepfault) f.ft = no_fault;
      return keepfault;
#endif
    default: return true;
    }
  }

} // namespace SPARC

