/*%  -*-c-*- 
 * Copyright (c) 2007-2012 Brigham Young University
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL BRIGHAM YOUNG UNIVERSITY BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN
 * IF THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * BRIGHAM YOUNG UNIVERSITY SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND BRIGHAM YOUNG
 * UNIVERSITY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LIS file defining the SPARC Linux emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file defines additional behavior for Linux emulation
 *
 */

/************* context type *********************/

codesection private {
#define LSE_LIS_SPARC_LINUX_EMU
  extern int Linux_call_os(LSE_emu_isacontext_t *, 
			   LSE_emu_iaddr_t, LSE_emu_iaddr_t &,
			   uint64_t);
  extern bool Linux_is64bit_context(LSE_emu_isacontext_t *);
  extern int Linux_translate(LSE_emu_isacontext_t *, LSE_emu_addr_t va,
			     void *&ha, LSE_emu_addr_t &pa);
}

codesection description {
extrafuncs += [("Linux_interrupt_cpu", LSE_domain.LSE_domainIDtype_R,
                "bool ??(LSE_emu_interface_t *,int )")]
}

codesection user_unregistration_hook {
  {
    extern void Linux_unregister_context(LSE_emu_isacontext_t *);
    if (ctx.di->EMU_emulateOS == 1) Linux_unregister_context(&ctx);
  }
}

codesection softmmu_handle_miss {

  if (ctx.di->EMU_emulateOS == 1) {
    transl.flags.ie = false;
    transl.le = false;
    flags = Linux_translate(&ctx, va, transl.ha, transl.pa);
    transl.ha = 0;
    e.paoffset = transl.pa - transl.va;
    e.flags = transl.flags;
    if (!flags || (kind == softmmu_t::Store && !(flags & 4))) {
      ctx.mmu.report_error(transl, atype, fault);
      return 0;
    }
    ctx.mem->translate(transl.pa, (LSE_device::devdata_t **)(&transl.ha));
    return flags;
  }
}

instruction tcc {
  action +overrideExceptionStep = {
    if (ctx.di->EMU_emulateOS==1) {
      if (fault.ft == trap_instruction) {
	int trapno = fault.ft2;
	if (trapno == 1) fault.ft = gdb_breakpoint_entry;
        else if (trapno == 0x3) { // flush windows for setjmp?
          fault.ft = no_fault;
	  flush_register_windows(&ctx, fault, Linux_is64bit_context(&ctx),
				 true);
        } // window traps
	else if (trapno == 0x10) { // 32-bit syscall
	  fault.ft = no_fault;
	  Linux_call_os(&ctx, addr, next_pc, *get_r_ptr(&ctx,1));
	} else if (trapno == 0x11 || trapno == 0x6d) { // 64-bit syscall
	  fault.ft = no_fault;
	  Linux_call_os(&ctx, addr, next_pc, *get_r_ptr(&ctx,1) + 1000);
	} else if (trapno == 0x6e) { // get context
	  fault.ft = no_fault;
	  Linux_call_os(&ctx, addr, next_pc, 2000); 
	} else if (trapno==0x6f) { // load context
	  fault.ft = no_fault;
	  Linux_call_os(&ctx, addr, next_pc, 2001); 
	}
      }
    }
  }
}

// do the register spill and fill behaviors
// Note that they must directly manipulate the context, even for the operands
// because the exception handling step comes after writeback.  This allows
// us to delay them until instruction retirement and means that reading the
// registers is "safe".
// TODO: make memory operations exception-safe

instruction restore {
  action +evaluateStep = {
    if (ctx.di->EMU_emulateOS==1 && fault.ft == fill_n_normal) 
      iclasses.is_sideeffect = true;
  }
  action +overrideExceptionStep = {
    if (ctx.di->EMU_emulateOS==1) {
      if (fault.ft == fill_n_normal) {
	
	fault.ft = no_fault;
	int nwp = (src_cwp - 1 + N_REG_WINDOWS) % N_REG_WINDOWS;
	
	fill_register_window(&ctx, nwp, fault, Linux_is64bit_context(&ctx),
			     true);

	// put together state to match what we'd get after executing saved
	// followed by restart of save
	*get_r_ptr(&ctx, rd, nwp) = dest1;
	*get_pr_ptr(&ctx, CWP, 0) = dest2 = nwp;
	*get_pr_ptr(&ctx, CANSAVE, 0) = dest3 = src3;
	*get_pr_ptr(&ctx, CANRESTORE, 0) = dest4 = src4;
      }
    }
  }
}

instruction return {
  action +evaluateStep = {
    if (ctx.di->EMU_emulateOS==1 && fault.ft == fill_n_normal) 
      iclasses.is_sideeffect = true;
  }
  action +overrideExceptionStep = {
    if (ctx.di->EMU_emulateOS==1) {
      if (fault.ft == fill_n_normal) {
	
	fault.ft = no_fault;
	int nwp = (src_cwp - 1 + N_REG_WINDOWS) % N_REG_WINDOWS;
	
	fill_register_window(&ctx, nwp, fault, Linux_is64bit_context(&ctx),
			     true);

	// put together state to match what we'd get after executing saved
	// followed by restart of save
	*get_pr_ptr(&ctx, CWP, 0) = dest1 = nwp;
	*get_pr_ptr(&ctx, CANSAVE, 0) = dest2 = src3;
	*get_pr_ptr(&ctx, CANRESTORE, 0) = dest3 = src4;
      }
    }
  }
}

instruction save {
  action +evaluateStep = {
    if (ctx.di->EMU_emulateOS==1 && fault.ft == clean_window) 
      fault.ft = no_fault;
    else if (ctx.di->EMU_emulateOS==1 && fault.ft == spill_n_normal)
      iclasses.is_sideeffect = true;
  }
  action +overrideExceptionStep = {
    if (ctx.di->EMU_emulateOS==1) {
      if (fault.ft == spill_n_normal) {
	
	fault.ft = no_fault;
	int nwp = (src_cwp + 2) % N_REG_WINDOWS; // cleans one in advance!
	
	spill_register_window(&ctx, nwp, fault, Linux_is64bit_context(&ctx),
			      true);

	// put together state to match what we'd get after executing saved
	// followed by restart of save.  
	*get_r_ptr(&ctx, rd, (src_cwp+1)%N_REG_WINDOWS) = dest1;
	*get_pr_ptr(&ctx, CWP, 0) = dest2 = (src_cwp + 1) % N_REG_WINDOWS;
	*get_pr_ptr(&ctx, CANSAVE, 0) = dest3 = src3;
	*get_pr_ptr(&ctx, CANRESTORE, 0) = dest4 = src4;
      }
    }
  }
}

instruction flushw {
  action +evaluateStep = {
    if (ctx.di->EMU_emulateOS==1 && fault.ft == spill_n_normal)
      iclasses.is_sideeffect = true;
  }
  action +overrideExceptionStep = {
    if (ctx.di->EMU_emulateOS==1) {
      if (fault.ft == spill_n_normal) {

	fault.ft = no_fault;
	int cwp = *get_pr_ptr(&ctx, CWP, 0); // current WP.
	cwp = (cwp + N_REG_WINDOWS - 1) % N_REG_WINDOWS;

	// save everybody who could be restored.
	int numtosave = *get_pr_ptr(&ctx, CANRESTORE,0);
    
	*get_pr_ptr(&ctx, CANRESTORE, 0) = 0;
	*get_pr_ptr(&ctx, CANSAVE, 0) += numtosave;

	while (numtosave--) {
	  spill_register_window(&ctx, cwp, fault, 
				Linux_is64bit_context(&ctx), true);
	  cwp = (cwp + N_REG_WINDOWS - 1) % N_REG_WINDOWS;
	} // numtosave
      }
    }
  }
}

