/* 
 * Copyright (c) 2007-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * UltraSPARC T1 private code file
 *
 * Authors:  Zhuo Ruan
 *           Daniel Rich
 *           David A. Penry <dpenry@ee.byu.edu>
 *
 * This contains stuff that could be in support section of the .lis file,
 * but seem more like clutter there.
 *
 */

#include <LSE_inttypes.h>
#include "LSE_SPARC.priv.h"

namespace LSE_SPARC {

  struct T1_fault_info::trapinfo_s 
  T1_fault_info::trapinfo[T1_fault_info::numtraps] = {
    { "modular_arithmetic_interrupt", 0x3d, 1600, 0},
    { "interrupt_vector", 0x60, 2603, 0},
    { "data_error", 0x78, 1300, 0},
    { "mem_address_not_aligned", 0x34, 1002, 0},
    { "fast_data_access_protection.PS0", 0x6c, 1207, 0},
    { "fast_data_access_protection.PS1", 0x6c, 1207, 0},
    { "privileged_action", 0x37, 1101, 0},

    // NOTE: cpu_mondo/dev_mondo/resumable are defined in 2005 architecture,
    // though the priorities are a bit different.  T1 manual is not clear
    // about whether MAI, interrupt_vector, data_error are hyperprivileged
  };

  // Somehow the T1 doesn't get the priority right according to the 2005 spec.
  // Note that the T1 supplement doesn't mention this, but both SAS and RTL
  // diagnostics appear to expect this priority inversion.

  struct T1_fault_info::trapinfo_s T1_fault_info::fix_privileged_opcode =
    { "privileged_opcode", 0x11, 600, 4};

  struct T1_fault_info::trapinfo_s T1_fault_info::fix_illegal_instruction = 
    { "illegal_instruction", 0x10, 701, 0};

  struct T1_fault_info::trapinfo_s T1_fault_info::fix_instruction_breakpoint = 
    { "instruction_breakpoint", 0x76, 702, 0};

  T1_fault_info::trapinfo_s 
  T1_fault_info::getFaultInfo(const SPARC_fault_name_t f) {
    int nf = f;
    if (f == privileged_opcode) return fix_privileged_opcode;
    else if (f == illegal_instruction) return fix_illegal_instruction;
    else if (f == instruction_breakpoint) return fix_instruction_breakpoint;
    else if (nf < SPARC_fault_info::numtraps) 
      return SPARC_fault_info::getFaultInfo(f);
    nf -= SPARC_fault_info::numtraps;
    if (nf < numtraps) return trapinfo[nf];
    else return SPARC_fault_info::unknown_fault;
  }

  T1_fault_info::trapinfo_s 
  T1_fault_info::getFaultInfo(const SPARC_fault_t &f) {
    return getFaultInfo(f.ft);
  }


  /******************* TLB handling *************/

  uint64_t T1_tlb::masks[8] = { ~uint64_t(0), ~uint64_t(7), ~uint64_t(077),
				~uint64_t(0777), ~uint64_t(07777), 
				~uint64_t(077777), ~uint64_t(0777777),
				~uint64_t(07777777) };

  void T1_tlb::init(void) {
    for (int i = 0; i < size; ++i) array[i].valid = false;
    nextfree = 0;
    lastaccess = 0;
  }

  void T1_tlb::find_next_free(int nextone) {
    int ii;

    if (nextfree < nextone) return; // no effect
    // INVARIANT: nothing before nextone is free; therefore, all are valid

    // find an invalid one
    for (ii = nextone; ii < size; ii++) 
      if (!array[ii].valid) { nextfree = ii; return; }

    // find an unused unlocked one
    for (ii = 0; ii < size; ii++) 
      if (!array[ii].used && !array[ii].locked) { nextfree = ii; return; }

    // clear all the used bits
    for (ii = 0; ii < size; ii++)
      if (!array[ii].locked) array[ii].used = false;

    // now try again for an unused unlocked one
    for (ii = 0; ii < size; ii++) 
      if (!array[ii].used && !array[ii].locked) { nextfree = ii; return; }

    // all are locked, go for the last one
    nextfree = size-1;

  }

  void T1_tlb::install_entry(int location, const T1_mmu_entry &entry) {

    if (location < 0) location = nextfree;

    //std::cerr << "Mapping entry " << location << " " << array 
    // << " " << entry << "\n";
    array[location] = entry;

    // Handle the autodemap, and find a location at the same time

    for (int i = 0; i < size; i++) {
      if (i == location) continue;
      if (array[i].valid) { // do autodemap
	uint64_t mask = masks[std::max(array[i].size,entry.size)];
	if ((entry.va & mask) == (array[i].va & mask) && 
	    entry.partitionID == array[i].partitionID && 
	    entry.real == array[i].real && 
	    (entry.real || entry.contextID == array[i].contextID)) {
	  array[i].valid = false;
	  if (i < nextfree) nextfree = i;
	  //std::cerr << "Demapping entry " << i << "\n";
	}
      }
    }

    find_next_free(location);
  }

  void T1_tlb::demap_page(int pid, int cid, uint64_t va) {
    uint64_t pageno = va >> 13;
    bool isreal = cid < 0;

    for (int i = 0; i < size; i++) {
      if (!array[i].valid) continue;

      uint64_t mask = masks[array[i].size];

      if ((pageno & mask) == (array[i].va & mask) && 
	  pid == array[i].partitionID && 
	  isreal == array[i].real && (isreal || cid == array[i].contextID)) {

	array[i].valid  = false;
	if (i < nextfree) nextfree = i;

	//std::cerr << "Demapping (page) entry " << i << " " << array << "\n";
	return; // no more than one will match
      }
    }
  } // demap_page

  void T1_tlb::demap_context(int pid, int cid) {
    for (int i = 0; i < size; i++) {
      if (!array[i].valid) continue;

      if (pid == array[i].partitionID && 
	  !array[i].real && cid == array[i].contextID) {

	array[i].valid  = false;
	if (i < nextfree) nextfree = i;

	//std::cerr << "Demapping (context) entry " << i << " " << array 
	//	  << "\n";
      }
    }
  } // demap_context

  void T1_tlb::demap_all(int pid) {
    for (int i = 0; i < size; i++) {
      if (!array[i].valid) continue;

      if (pid == array[i].partitionID && !array[i].locked) {

	array[i].valid  = false;
	if (i < nextfree) nextfree = i;

	//std::cerr << "Demapping (all) entry " << i << " " << array << "\n";
      }
    }
  } // demap_context

  void T1_tlb::inval_all(void) {
    for (int i = 0; i < size; i++) {
      array[i].valid = false;
    }
    nextfree = 0;
  } // inval_all

  T1_mmu_entry *T1_tlb::findVAentry(const addr_t va, int cid, int pid,
				    uint64_t &mask) {
  
    addr_t pageno = va >> 13;
  
    // quick cache of the last one to be accessed; should speed things up
    // a little bit, particularly for I access
  
    if (array[lastaccess].valid) {
      uint64_t lmask = masks[array[lastaccess].size];
    
      if ((pageno & lmask) == (array[lastaccess].va & lmask) &&
	  pid == array[lastaccess].partitionID && !array[lastaccess].real 
	  && cid == array[lastaccess].contextID) {
      
      
	if (!array[lastaccess].used) {
	  array[lastaccess].used = true;
	  find_next_free(lastaccess);
	}

	mask = lmask;
	return &array[lastaccess];
      }
    }

    for (int i = 0; i < size; i++) {
      if (!array[i].valid) continue;

      uint64_t lmask = masks[array[i].size];

      if ((pageno & lmask) == (array[i].va & lmask) &&
	  pid == array[i].partitionID && !array[i].real 
	  && cid == array[i].contextID) {
	    
	if (!array[i].used) {
	  array[i].used = true;
	  find_next_free(i);
	}

	mask = lmask;
	lastaccess = i;
	return &array[i];
      }
    } // for entries
    return 0;
  } // findVAentry

  T1_mmu_entry *T1_tlb::findRAentry(const addr_t va, int pid,
				    uint64_t &mask) {

    addr_t pageno = va >> 13;

    // quick cache of the last one to be accessed; should speed things up
    // a little bit, particularly for I access

    if (array[lastaccess].valid) {

      uint64_t lmask = masks[array[lastaccess].size];

      if ((pageno & lmask) == (array[lastaccess].va & lmask) &&
	  pid == array[lastaccess].partitionID && array[lastaccess].real) {
	    

	if (!array[lastaccess].used) {
	  array[lastaccess].used = true;
	  find_next_free(lastaccess);
	}

	mask = lmask;
	return &array[lastaccess];
      }
    }

    for (int i = 0; i < size; i++) {
      if (!array[i].valid) continue;

      uint64_t lmask = masks[array[i].size];

      if ((pageno & lmask) == (array[i].va & lmask) &&
	  pid == array[i].partitionID && array[i].real) {
	    
	if (!array[i].used) {
	  array[i].used = true;
	  find_next_free(i);
	}

	mask = lmask;
	return &array[i];
      }
    } // for entries
    return 0;
  } // findRAentry

  /******************* MMU handling *************/

  // Cleverness: use the register name if it's a special-cased or read-only
  // register so we get occasional reminders of which register is which
  const uint64_t T1_mmu::controlRegMasks[numControlReg] = {
    0, bits(8), bits(8),  // dummy/primary/secondary context
    bits(52,12)|bits(4), bits(52,12)|bits(4), // tsb base
    bits(52,12)|bits(4), bits(52,12)|bits(4),
    bits(3,8)|bits(3), bits(3,8)|bits(3), // config 
    bits(52,12)|bits(4), bits(52,12)|bits(4), // tsb base
    bits(52,12)|bits(4), bits(52,12)|bits(4),
    bits(3,8)|bits(3), bits(3,8)|bits(3), // config 
    I_TAG_TARGET, D_TAG_TARGET, 
    bits(8,16)|bits(10,4)|bits(3), bits(8,16)|bits(10,4)|bits(3), // status
    bits(48),  // d_sf_addr (isn't this read-only?)
    I_TAG_ACCESS, D_TAG_ACCESS,
    bits(45,3), bits(8), // watchpoint / partition ID
    I_PS0_PTR, D_PS0_PTR, I_PS1_PTR, D_PS1_PTR, D_DIRECT_PTR,
    I_DATA_IN, D_DATA_IN, I_DATA_ACCESS, D_DATA_ACCESS,
    I_TAG_READ, D_TAG_READ, 
    I_DEMAP, D_DEMAP, I_INVAL_ALL, D_INVAL_ALL,
    bits(8,25)|bits(2,21)|bits(4,0), // LSU control

    bits(2), bits(3,29)|bits(15,11)|bit(9), // error en/status (W1C not covered)
    SPARC_ERROR_ADDRESS_REG, 
    bits(64), // bist control

    bits(39), bits(2), // inst_mask_reg, lsu_diag_reg
    bits(64), bits(64), // margin control/interrupt receive
    bits(2,16)|bits(5,8)|bits(6), // interrupt dispatch
    SWVR_UDB_INTR_R,

    bits(8,6), bits(8,6), bits(8,6), bits(8,6), // queue pointers
    bits(8,6), bits(8,6), bits(8,6), bits(8,6),

    bits(64), bits(64), bits(64), bits(64), // scratch
    bits(64), bits(64), bits(64), bits(64),

    bits(64), // mystery reg: I think it's the error injection register

    bits(3,11)|bits(10), bits(36,3), bits(48), bits(64) // ma ctll/pa/addr/np

  };


  // ensure that higher priority faults override lower priority
  void T1_mmu::report_dae(SPARC_fault_t &fault, uint32_t fv) {
    // merge reason bits for dae.  If none are set yet, there must have
    // been an illegal reported by the regular instruction definitions.
    if (fault.ft == data_access_exception) 
      fault.ft2 |= (fault.ft2 & bit(31) ? fv : 
		    fv | data_access_exception_invalid | bit(31));
    else {
      if (LSE_emu_fault_info::getFaultInfo(data_access_exception).prio <
	  LSE_emu_fault_info::getFaultInfo(fault).prio) {
	fault.ft = data_access_exception;
	fault.ft2 = fv | bit(31);
      }
    }
  }

  // ensure that higher priority faults override lower priority
  void T1_mmu::report_iae(SPARC_fault_t &fault, uint32_t fv) {
    // merge reason bits for dae.  If none are set yet, there must have
    // been an illegal reported by the regular instruction definitions.
    if (fault.ft == instruction_access_exception) fault.ft2 |= fv;
    else {
      if (LSE_emu_fault_info::getFaultInfo(instruction_access_exception).prio <
	  LSE_emu_fault_info::getFaultInfo(fault).prio) {
	fault.ft = instruction_access_exception;
	fault.ft2 = fv | bit(31);
      }
    }
  }

  void T1_mmu::init(struct SPARC_context_t *c) {
    ctx = c;
    ITLB.init();
    DTLB.init();
    for (int i = 0; i < numControlReg; i++) controlReg[i] = 0;
    wasPS1 = false;
    controlReg[SPARC_ERROR_STATUS_REG] = bit(28);
  }

  void T1_mmu::doInstrSoftMiss(const addr_t pcin, 
			       SPARC_addrtransl_t &addr,
			       SPARC_fault_t &fault,
			       uint64_t hpstate, uint64_t tl,
			       uint64_t pstate) {

    // TODO: p.61 of T1 manual check of I/O locations
    // NOTE: not catching an unaligned PC because it is not supposed to
    // be possible!

    // From digging around in SAS:
    //                  IM ENB HPRIV RED	       IM HPRIV RED
    // immu bypass: 	x   x	x     1			x  x      1
    //	  	        0   0   x     0			
    //                  x   1   1     0			x  1	  0
    // RA->PA:		0   1	0     0			0  0      0
    // VA->PA:		1   0   x     0			
    //                  1   1   0     0			1  0	  0
    addr_t pc = pcin & ((pstate & pstate_amM) ? bits(32) : bits(64));
    addr.va = addr.pa = pc;

    addr.ha = 0; // TODO: do this properly

    bool hpriv = hpstate & hpstate_hprivM;
    bool red = hpstate & hpstate_redM;
    bool immuon = controlReg[LSU_CONTROL] & lsu_control_reg_imM;
    bool enb = hpstate & hpstate_enbM;

    SPARC_mmu_flags &flags = addr.flags;

    if (red || !immuon && !enb || enb && hpriv) {

      flags.cp = true;
      flags.cv = true;
      flags.nfo = flags.ie = flags.e = flags.p = flags.ep = flags.w = false;

      addr.pa = pc & bits(40); // only 40 PA bits on T1
      return;

    } else { 
      uint64_t check = pc >> 47;

      if ( check != 0 && check != 0x1ffff) {

	report_iae(fault, instruction_access_exception_fetch);
	addr.pa = 0;
	return;

      } else if (immuon) { // VA->PA

	uint64_t mask;
	T1_mmu_entry *entry;

	entry = ctx->sharedcore
	  ->mmu.ITLB.findVAentry(pc, !tl ? controlReg[PRIMARY_CONTEXT] : 0,
				 controlReg[PARTITION_ID], mask);

	if (entry) {
	  if (!(pstate & pstate_privM) && entry->flags.p)
	    report_iae(fault, instruction_access_exception_priv);

	  flags = entry->flags;
	  addr.pa = (pc & ~(mask<<13)) | ((entry->pa & mask)<<13);
	  return;
	}

	report_fault(fault, fast_instruction_access_MMU_miss);
	addr.pa = 0;
	return;

      } else { // RA->PA

	uint64_t mask;
	T1_mmu_entry *entry;

	entry = ctx->sharedcore
	  ->mmu.ITLB.findRAentry(pc, controlReg[PARTITION_ID], mask);

	if (entry) {

	  // NOTE: the T1 does not check the ep flag or the nfo flag
	  if (!(pstate & pstate_privM) && entry->flags.p)
	    report_iae(fault, instruction_access_exception_priv);

	  flags = entry->flags;	  
	  addr.pa = (pc & ~(mask<<13)) | ((entry->pa & mask)<<13);
	  return;
	}

	report_fault(fault, instruction_real_translation_miss);
	addr.pa = 0;
	return;
      }
    }
  }

  static inline void bypass(SPARC_addrtransl_t &addr) {
    addr.pa = addr.va;
    addr.le = false;
    if (addr.va & bit(39)) {
      addr.flags.e = addr.flags.w = true;
      addr.flags.cp = addr.flags.ie = addr.flags.cv = addr.flags.p
	= addr.flags.nfo = false;
    } else {
      addr.flags.cp = addr.flags.w = true;
      addr.flags.e = addr.flags.ie = addr.flags.cv = addr.flags.p
	= addr.flags.nfo = false;
    }
  }

  void T1_mmu::doDataSoftMiss(SPARC_addrtransl_t &addr,
				     SPARC_access_type atype, 
				     SPARC_fault_t &fault, 
				     uint64_t hpstate, uint64_t tl,
				     uint64_t pstate) {

    addr.ha = 0;
    addr.atype = atype;

    bool hpriv = hpstate & hpstate_hprivM;
    bool dmon = controlReg[LSU_CONTROL] & lsu_control_reg_dmM;
    bool priv = pstate & pstate_privM;
    bool enb = hpstate & hpstate_enbM;

    // NOTE: behavior when dm is off is not well-described in manual!
    
    if (hpriv && enb || !enb && !dmon) { // no translation desired.
      bypass(addr);
      // NOTE: tlu_tdb seems to want ASI_N set when tl==0, but another one
      // wnated ASI_P under unclear conditions.
      addr.asi = tl == 0 ? (pstate & pstate_cleM) ? ASI_PL : ASI_P :
	(pstate & pstate_cleM) ? ASI_NL : ASI_N;
      addr.cid = tl == 0 ? controlReg[PRIMARY_CONTEXT] : 0;
      addr.flags.ie = false;
      addr.le = (pstate & pstate_cleM);
      return;
      
    } else if (tl) {

      addr.asi = (pstate & pstate_cleM) ? ASI_NL : ASI_N;
      addr.cid = 0;

      if (!priv) report_fault(fault, privileged_action_mmu);
      
      if (dmon || !enb) {
	check_watchpoint(addr, fault, pstate);
	doVA2PA(addr, atype, fault, pstate, !(pstate & pstate_privM));
      } else {
	doRA2PA(addr, atype, fault);
      }

      addr.le = addr.flags.ie ^ !!(pstate & pstate_cleM);
      return;

    } else {

      addr.asi = (pstate & pstate_cleM) ? ASI_PL : ASI_P;
      addr.cid = controlReg[PRIMARY_CONTEXT];
      
      if (dmon || !enb) {
	check_watchpoint(addr, fault, pstate);
	doVA2PA(addr, atype, fault, pstate, !(pstate & pstate_privM));
      } else {
	doRA2PA(addr, atype, fault); // check privilege?
      }
      
      addr.le = addr.flags.ie ^ !!(pstate & pstate_cleM);
      return;
      
    }
  } // doDataSoftMiss

  inline uint64_t T1_mmu_calc_ptr(uint64_t base, uint64_t ta,
					 int psfield, bool  isps1) { 
    // p. 186.  Sign extend at va47
    int n = base & 0xf;
    uint64_t ptr = base & ~bits(13);
    if (base & bit(12)) { // split TSB
      ptr &= ~bits(14+n);
      if (isps1) ptr |= bit(13+n);
    } else ptr &= ~bits(13+n);
    ptr |= ((ta >> (13+3*psfield)) & bits(21-13+n+1)) << 4;
    if (ptr & (uint64_t(1)<<47)) ptr |= bits(63-48+1) << 48;

    //std::cerr << "ptr calc " << std::hex << ptr << " = " << base << ','
    //	      << ta << ',' << psfield << std::dec << ' ' << isps1 << std::endl;
    return ptr;
  }

  // PA numbering matches order in table 13-13, p. 199

  void T1_mmu::doRA2PA(SPARC_addrtransl_t &addr, 
		       SPARC_access_type atype, 
		       SPARC_fault_t &fault) {

    uint64_t mask;
    T1_mmu_entry *entry;
    
    entry = ctx->sharedcore
      ->mmu.DTLB.findRAentry(addr.va, controlReg[PARTITION_ID], mask);

    if (entry) {
      
      addr.pa = (addr.va & ~(mask<<13)) | ((entry->pa & mask)<<13);
      addr.flags = entry->flags;

      if (atype == rmwOp && addr.pa & bit(39))
	report_dae(fault, data_access_exception_atomic_io);

      if ((atype == writeOp || atype == rmwOp) && !addr.flags.w)
	if ((controlReg[D_Z_CONFIG]>>8) == (unsigned)entry->size)
	  report_fault(fault, fast_data_access_protection_PS1);
	else
	  report_fault(fault, fast_data_access_protection_PS0);

      return;
    }
    report_fault(fault, data_real_translation_miss);
    addr.pa = 0;
    addr.flags.ie = false;
  }

  void T1_mmu::check_watchpoint(SPARC_addrtransl_t &addr, 
				       SPARC_fault_t &fault,
				       uint64_t pstate) {

    // quick match check
    if ( ! (!((controlReg[VIRT_WATCHPOINT] ^ addr.va) & bits(45,3)) ||
	    (pstate & pstate_amM) &&
	    !((controlReg[VIRT_WATCHPOINT] ^ addr.va) & bits(29,3)))) return;
      
    // check for right kind of access
    if (! ((controlReg[LSU_CONTROL] & lsu_control_reg_vrM) && 
	   (addr.atype == readOp || addr.atype == rmwOp) ||
	   (controlReg[LSU_CONTROL] & lsu_control_reg_vwM) &&
	   (addr.atype == writeOp || addr.atype == rmwOp)) ) return;


    // now check the byte selections
    int bm = (  (controlReg[LSU_CONTROL] & lsu_control_reg_vmM)
		>> lsu_control_reg_vmO );
    if (!bm) return;

    
    uint64_t bm2; 
    if (addr.size == 44) bm2 = bits(8);
    else if (addr.size == 88) bm2 = bits(16);
    else if (addr.size < 0) bm2 = -addr.size; // partial
    else bm2 = bits(addr.size, addr.va & 7);
    if (bm & bm2) report_fault(fault, VA_watchpoint);
  }

  void T1_mmu::doVA2PA(SPARC_addrtransl_t &addr, 
			      SPARC_access_type atype, 
			      SPARC_fault_t &fault, uint64_t pstate,
			      bool checkpriv, bool checknfo) {

    uint64_t check = addr.va >> 47;
    int cid = addr.cid;

    if ( check != 0 && check != 0x1ffff && 
	 !(pstate & pstate_amM)) { // VA hole

      report_dae(fault, data_access_exception_va_out_of_range);
      addr.pa = 0;
      addr.flags.ie = false;
    } else {

      uint64_t mask;
      T1_mmu_entry *entry;

      entry = ctx->sharedcore->
	mmu.DTLB.findVAentry(addr.va, cid, controlReg[PARTITION_ID], mask);

      if (entry) {
	addr.pa = (addr.va & ~(mask<<13)) | ((entry->pa & mask)<<13);
	addr.flags = entry->flags;

	if (checkpriv && addr.flags.p)
	  report_dae(fault, data_access_exception_priv);
	  
	if (checknfo && addr.flags.nfo) 
	  report_dae(fault, data_access_exception_nfo);
	  
	if (!checknfo && addr.flags.e && 
	    (atype == readOp || atype == prefetchOp))
	  report_dae(fault, data_access_exception_spec);
	    
	if (atype == rmwOp && addr.pa & bit(39))
	  report_dae(fault, data_access_exception_atomic_io);

	if ((atype == writeOp || atype == rmwOp) && !addr.flags.w)
	  if (cid && ((controlReg[D_NZ_CONFIG]>>8) == (unsigned)entry->size) ||
	      !cid && ((controlReg[D_Z_CONFIG]>>8) == (unsigned)entry->size))
	    report_fault(fault, fast_data_access_protection_PS1);
	  else
	    report_fault(fault, fast_data_access_protection_PS0);
	
	return;
      }

      report_fault(fault, fast_data_access_MMU_miss);
      addr.pa = 0;
      addr.flags.ie = false;
    }
  } // doVA2PA

  void T1_mmu::handleExplicitASI(SPARC_addrtransl_t &addr, 
				 SPARC_access_type atype, 
				 SPARC_fault_t &fault,
				 uint64_t hpstate, uint64_t pstate,
				 uint64_t tl) {

    // translation from ASI to register; saves a bunch of LOC
    static int mmuregmap[] = { D_Z_TSB_PS0, D_Z_TSB_PS1,
			       D_Z_CONFIG, DUMMY, 
			       I_Z_TSB_PS0, I_Z_TSB_PS1,
			       I_Z_CONFIG, DUMMY, /* 0x38 */
			       D_NZ_TSB_PS0, D_NZ_TSB_PS1,
			       D_NZ_CONFIG, DUMMY, /* 0x3c */
			       I_NZ_TSB_PS0, I_NZ_TSB_PS1,
			       I_NZ_CONFIG, DUMMY, /* 0x40 */
			       0, 0, MYSTERY_REG, STM_CTL_REG, 
			       LSU_CONTROL, 0,0,0, 
			       0,0,SPARC_ERROR_EN_REG,SPARC_ERROR_STATUS_REG, 
			       SPARC_ERROR_ADDRESS_REG,0,0,0, /* 0x50 */
			       I_PS0_PTR, I_PS1_PTR, DUMMY, /* 0x53 */
			       I_DATA_IN, I_DATA_ACCESS, 
			       I_TAG_READ, I_DEMAP, DUMMY, /* 0x58 */
			       D_PS0_PTR, D_PS1_PTR, D_DIRECT_PTR,
			       D_DATA_IN, D_DATA_ACCESS, 
			       D_TAG_READ, D_DEMAP, /* 0x5f */
			       0, 0, 0, 0, 0, 0, 0, 0, /* 0x67 */
			       0, 0, 0, 0, 0, 0, 0, 0, /* 0x6f */ 
			       0, 0, SWVR_INTR_RECEIVE, SWVR_UDB_INTR_W,
			       SWVR_UDB_INTR_R, 0, 0, 0, /* 0x73 */
    };
			       
    bool hpriv = hpstate & hpstate_hprivM;
    bool priv = pstate & pstate_privM;
    bool iswrite = atype != readOp && atype != prefetchOp;
    bool dmon = controlReg[LSU_CONTROL] & lsu_control_reg_dmM;

    // checking privilege the quick and easy way
    // NOTE: UA2005 says that privileged_action should happen in all cases,
    // but UST1 has DAE on privileged access to hyper-privileged registers
    addr.pa = 0;
    addr.ha = 0;
    addr.atype = atype;

    if (!hpriv && addr.asi <= 0x7f)
      if (!priv) report_fault(fault, privileged_action_mmu);
      else if (addr.asi >= 0x30) 
	report_dae(fault, data_access_exception_invalid);

    int ASIflags = ASItable[addr.asi];
    int isTranslating = ASIflags & 3;

    if (isTranslating) {  // All the memory ASIs

      bool checkpriv = !(hpriv || priv);
      bool enb = hpstate & hpstate_enbM;

      bool bypassit = !enb && !dmon || enb && hpriv;
      bool realtrans = enb && !dmon && !hpriv;

      addr.cid = isTranslating == nucleus ? 0 : isTranslating == primary ?
	controlReg[PRIMARY_CONTEXT] : controlReg[SECONDARY_CONTEXT];
      bool isreal = ASIflags & real;
      bool isasif = ASIflags & asif;

      // non-faulting ASIs only take load operations

      if ((ASIflags & nofault) && atype != readOp && atype != prefetchOp) {
	report_dae(fault, data_access_exception_invalid);
      }
	  
      if (ASIflags & notimpl) { // unimplemented ASIs are weird

	if (!(enb && hpriv)) check_watchpoint(addr, fault, pstate);
	report_dae(fault, data_access_exception_invalid);
	addr.pa = 0;
	addr.mmu_intercepted = false;
	addr.flags.ie = false;
      } else if (bypassit && !isreal && !isasif || !enb && isreal) {

	bypass(addr);

	if (addr.asi == ASI_REAL_IO || addr.asi == ASI_REAL_IO_L) {
	  addr.flags.cp = false;
	  addr.flags.e = true;
	}

	if (addr.asi == ASI_REAL || addr.asi == ASI_REAL_L) {
	  addr.flags.cp = true;
	  addr.flags.e = false;
	}
	addr.flags.ie = false;
      } else if (realtrans || isreal) doRA2PA(addr, atype, fault);
      else {

	doVA2PA(addr, atype, fault, pstate, 
		addr.asi >= 0x80 && checkpriv || isasif,
		!(ASIflags & nofault));

	if (!(enb && hpriv)) check_watchpoint(addr, fault, pstate);

      }

      addr.le = addr.flags.ie ^ !!(ASItable[addr.asi] & isle);

    } else {

      addr.le = false;
      addr.flags.ie = false;

      switch (addr.asi) {

	// Translating ASIs
	
	// MMU control
	
      case ASI_MMU_CONTEXTID: // DMMU Primary/Secondary context
	addr.pa = SECONDARY_CONTEXT-((addr.va >> 3) & 1); 
	addr.mmu_intercepted = true; 
	break;
	
      case 0x31: // DMMU Zero TSB Base PS0
      case 0x32: // DMMU Zero TSB Base PS1
      case 0x33: // DMMU Zero Config
      case 0x35: // IMMU Zero TSB Base PS0
      case 0x36: // IMMU Zero TSB Base PS1
      case 0x37: // IMMU Zero config
      case 0x39: // DMMU NonZero TSB Base PS0
      case 0x3a: // DMMU NonZero TSB Base PS1
      case 0x3b: // DMMU NonZero Config
      case 0x3d: // IMMU Nonzero TSB Base PS0
      case 0x3e: // IMMU Nonzero TSB Base PS1
      case 0x3f: // IMMU Nonzero Config
      case 0x45: // LSU_CONTROL_REG
	addr.pa = mmuregmap[addr.asi - 0x31];
	if (addr.va & 0xf8) addr.pa = DUMMY;
	addr.mmu_intercepted = true; 
	break;

      case ASI_IMMU: // IMMU Tag Target, SFSR, Tag Access
	{ 
	  unsigned int na = addr.va & 0xf8;
	  addr.mmu_intercepted = true;
	  if (na == 0) { 
	    addr.pa = I_TAG_TARGET; 
	    if (iswrite) report_dae(fault, data_access_exception_invalid);
	  } else if (na == 0x18) addr.pa = I_SF_STATUS;
	  else if (na == 0x28) report_dae(fault, data_access_exception_invalid);
	  else if (na == 0x30) addr.pa = I_TAG_ACCESS; 
	  else addr.pa = DUMMY;
	}
	break;

      case ASI_DMMU: // DMMU miscellaneous registers
	{ 
	  unsigned int na = addr.va & 0xf8;
	  addr.mmu_intercepted = true;
	  if (na == 0) { 
	    addr.pa = D_TAG_TARGET; 
	    if (iswrite) report_dae(fault, data_access_exception_invalid);
	  } else if (na == 0x8 || na == 0x10 || na == 0x28 || na == 0x40) 
	    report_dae(fault, data_access_exception_invalid);
	  else if (na == 0x18) addr.pa = D_SF_STATUS; 
	  else if (na == 0x20) {
	    addr.pa = D_SF_ADDR; 
	    if (iswrite) report_dae(fault, data_access_exception_invalid);
	  } else if (na == 0x30) addr.pa = D_TAG_ACCESS;
	  else if (na == 0x38) addr.pa = VIRT_WATCHPOINT; 
	  else if (na == 0x80) addr.pa = PARTITION_ID; 
	  else addr.pa = DUMMY;
	}
	break;
    
      case 0x51: // IMMU PS0 Pointer registers
      case 0x52: // IMMU PS1 Pointer registers
      case 0x59: // DMMU PS0 pointer registers
      case 0x5a: // DMMU PS1 pointer registers
      case 0x5b: // DMMU Direct pointer register
	addr.pa = mmuregmap[addr.asi - 0x31];
	if (addr.va & 0xf8) addr.pa = DUMMY;
	if (iswrite) report_dae(fault, data_access_exception_invalid);
	addr.mmu_intercepted = true; 
	break;
    
      case 0x54: // IMMU Data In Register
      case 0x5c: // DMMU Data In Register
	addr.pa = mmuregmap[addr.asi - 0x31];
	if (addr.va & 0xf8) addr.pa = DUMMY;
	if (!iswrite) report_dae(fault, data_access_exception_invalid);
	addr.mmu_intercepted = true;
	break;

      case 0x57: // IMMU Demap
      case 0x5f: // DMMU Demap
      case 0x73: // interrupt vector dispatch register; write-only
	addr.pa = mmuregmap[addr.asi - 0x31];
	if (!iswrite) report_dae(fault, data_access_exception_invalid);
	addr.mmu_intercepted = true;
	break;

      case 0x55: // IMMU Data Access Register
      case 0x5d: // DMMU Data Access Register
	addr.pa = mmuregmap[addr.asi - 0x31];
	addr.mmu_intercepted = true; 
	break;

      case 0x56: // IMMU Tag read register
      case 0x5e: // DMMU Tag read register
      case 0x74: // incoming vector register
	addr.pa = mmuregmap[addr.asi - 0x31];
	if (iswrite) report_dae(fault, data_access_exception_invalid);
	addr.mmu_intercepted = true; 
	break;

      case 0x60: // IMMU/DMMU Invalidate All
	{ unsigned int na = addr.va & 0xf8;
	if (na == 0) addr.pa = I_INVAL_ALL;
	else if (na == 8) addr.pa = D_INVAL_ALL;
	else addr.pa = DUMMY;
	if (!iswrite) report_dae(fault, data_access_exception_invalid);
	addr.mmu_intercepted = true; 
	}
	break;
    
	// Other internal registers

      case ASI_SCRATCHPAD: // sec 9.2.3
	{ 
	  int rno = (addr.va & 0x3f)>>3;
	  addr.pa = rno + SCRATCH0;
	  if (rno == 4 || rno == 5)
	    report_dae(fault, data_access_exception_invalid);
	  addr.mmu_intercepted = true; 
	}
	break;

      case ASI_QUEUE: 
	{ 
	  unsigned int na = addr.va & 0x3f8;
	  switch (na) {
	  case 0x3c0: addr.pa = CPU_MONDO_HEAD; break;
	  case 0x3d0: addr.pa = DEV_MONDO_HEAD; break;
	  case 0x3e0: addr.pa = RES_ERROR_HEAD; break;
	  case 0x3f0: addr.pa = NONRES_ERROR_HEAD; break;
	  case 0x3c8: addr.pa = CPU_MONDO_TAIL; break;
	  case 0x3d8: addr.pa = DEV_MONDO_TAIL; break;
	  case 0x3e8: addr.pa = RES_ERROR_TAIL; break;
	  case 0x3f8: addr.pa = NONRES_ERROR_TAIL; break;
	  default: addr.pa = DUMMY; break;
	  }

	  // basic privilege check was already done, but tail needs hyper-check
	  // for write
	  if (na >= 0x3c0 && (addr.va & 0x8) && iswrite && !hpriv)
	    report_dae(fault, data_access_exception_invalid);

	  addr.mmu_intercepted = true; 
	}
	break;
  
      case 0x40: // ASI_STREAM_MA: control registers (one per core)
	{ 
	  unsigned int na = addr.va & 0x3f8;
	  switch (na) {
	  case 0x80: addr.pa = MA_CONTROL; break;
	  case 0x88: addr.pa = MA_PA;      break;
	  case 0x90: addr.pa = MA_ADDR;    break;
	  case 0x98: addr.pa = MA_NP;      break;
	  case 0xa0: // Wait for MA: noop.
	    addr.pa = DUMMY; break;
	  default:
	    addr.pa = DUMMY; break;
	  }
	  addr.mmu_intercepted = true; 
	}
      break;
    
      case 0x42: // all one per core
	{
	  unsigned int na = addr.va & 0xf8;
	  switch (na) {
	  case 0x0: addr.pa = SPARC_BIST_CONTROL; break;
	  case 0x8: addr.pa = INST_MASK_REG; break;
	  case 0x10: addr.pa = LSU_DIAG_REG; break;
	  default: addr.pa = DUMMY;  break;
	  }
	  addr.mmu_intercepted=true;
	}
	break;

      case 0x46: // D-cache data array diagnostics
      case 0x47: // D-cache tag/valid diagnostics
      case 0x66: // I-cache data array diagnostics
      case 0x67: // I-cache tag diagnostics
	addr.pa = DUMMY;
	addr.mmu_intercepted = true;
	break;

      case 0x43: // mysterious undocumented register
      case 0x44: // margin control register
      case 0x4b: // error enable register
      case 0x4c: // error status register
      case 0x4d: // error address register
      case 0x72: // interrupt receive register
	addr.pa = mmuregmap[addr.asi - 0x31];
	addr.mmu_intercepted = true;
	break;
  
      case ASI_HYP_SCRATCHPAD: // 9.2.4
	addr.pa = ((addr.va & 0x3f) >> 3) + SCRATCH0;
	addr.mmu_intercepted = true;
	break;

      default:
	report_dae(fault, data_access_exception_invalid);
	addr.pa = 0;
	addr.mmu_intercepted = true; 
	return;
      }
    } // else translating

  } // T1_mmu::handleExplicitASI

  uint64_t T1_mmu::asi_fault(int asi, int tl, uint64_t pstate) {
    int newasi = (asi >= 0 ? asi :
		  (tl > 0 ? ((pstate & pstate_cleM) ? ASI_NL : ASI_N) :
		   ((pstate & pstate_cleM) ? ASI_PL : ASI_P)));
    int ct;

    if (newasi == ASI_P || newasi == ASI_PL || newasi == ASI_TWINX_P ||
	newasi == ASI_TWINX_PL || newasi == ASI_BLK_P || 
	newasi == ASI_BLK_PL || newasi == ASI_PNF || newasi == ASI_PNFL || 
	newasi == ASI_AIUP || newasi == ASI_AIUPL || newasi == ASI_TWINX_AIUP ||
	newasi == ASI_TWINX_AIUPL || newasi == ASI_BLK_AIUP || 
	newasi == ASI_BLK_AIUPL || newasi == ASI_PST8_P ||
	newasi == ASI_PST16_P || newasi ==  ASI_PST32_P ||
	newasi ==  ASI_PST8_PL || newasi ==  ASI_PST16_PL ||
	newasi ==  ASI_PST32_PL || newasi ==  ASI_FL8_P ||
	newasi ==  ASI_FL16_P || newasi ==  ASI_FL8_PL ||
	newasi ==  ASI_FL16_PL || newasi ==  ASI_BLK_COMMIT_P)
      ct = 0;
    else if (newasi == ASI_S || newasi == ASI_SL || newasi == ASI_TWINX_S ||
	     newasi == ASI_TWINX_SL || newasi == ASI_BLK_S || 
	     newasi == ASI_BLK_SL || newasi == ASI_SNF || 
	     newasi == ASI_SNFL || newasi == ASI_AIUS ||
	     newasi == ASI_AIUSL || newasi == ASI_TWINX_AIUS || 
	     newasi == ASI_TWINX_AIUSL || newasi == ASI_BLK_AIUS || 
	     newasi == ASI_BLK_AIUSL ||
	     newasi == ASI_PST8_S || newasi == ASI_PST16_S ||
	     newasi == ASI_PST32_S || newasi == ASI_PST8_SL ||
	     newasi == ASI_PST16_SL || newasi == ASI_PST32_SL ||
	     newasi == ASI_FL8_S || newasi == ASI_FL16_S ||
	     newasi == ASI_FL8_SL || newasi == ASI_FL16_SL ||
	     newasi == ASI_BLK_COMMIT_S) 
      ct = 1;
    else if (newasi == ASI_N || newasi == ASI_NL || newasi == 0x24 ||
	     newasi == ASI_TWINX_N || newasi == 0x2c || newasi == ASI_TWINX_NL
	     || newasi == ASI_REAL || newasi == ASI_REAL_L ||
	     newasi == ASI_TWINX_R || newasi == ASI_TWINX_REAL_L ||
	     newasi == ASI_REAL_IO || newasi == ASI_REAL_IO_L)
      ct = 2;
    else ct = 3;
    return (newasi << 16) | (ct<<4);
  }

  namespace {
    // the goal is to make valgrind not complain about uninitialized stuff,
    // since not all instructions have meaningful daddrs.
    inline bool iswrite(SPARC_addrtransl_t &daddr) {
      return daddr.atype == writeOp || daddr.atype == rmwOp;
    }
    inline uint64_t maskedva(SPARC_addrtransl_t &daddr, bool mode32) {
      return (mode32 ? daddr.va & bits(32) & ~bits(13) : 
	      extendm(daddr.va, 48) & ~bits(13));
    }
  }

  void T1_mmu::handleMMUFault(SPARC_addrtransl_t &daddr, full_iaddr_t &iaddr,
			      SPARC_fault_t &fault, uint64_t tl, 
			      uint64_t pstate) {

    bool mode32 = pstate & pstate_amM;
    uint64_t maskedia = ( mode32 ? iaddr.PC & bits(32) & ~bits(13) :
			  extendm(iaddr.PC,48) & ~bits(13));
    
    switch (fault.ft) {

    case data_access_exception: 
      // in case it was just reported by the instruction itself, mark cause
      if (!fault.ft2) fault.ft2 = bit(31)|data_access_exception_invalid;

      controlReg[D_TAG_ACCESS] = (maskedva(daddr,mode32) | daddr.cid);
      controlReg[D_SF_STATUS] = (1 | asi_fault(daddr.asi, tl, pstate) | 
				 daddr.flags.e<<6 | 
				 (fault.ft2 & bits(7)) << 7 | 
				 (iswrite(daddr) << 2) |
				 ((controlReg[D_SF_STATUS]&1)<<1));
      controlReg[D_SF_ADDR] = extendm(daddr.va,48);
      break;

    case instruction_access_exception:
      controlReg[I_TAG_ACCESS] = ( (fault.ft2 == 0x40 ? maskedva(daddr,mode32)
				    : maskedia)
				   | (tl ? 0 : controlReg[PRIMARY_CONTEXT]) );
      controlReg[I_SF_STATUS] = (1 | asi_fault(-1, tl, pstate) |
				 (fault.ft2 & bits(7)) << 7 |
				 ((controlReg[I_SF_STATUS]&1)<<1));
      break;

    case fast_instruction_access_MMU_miss:
      controlReg[I_TAG_ACCESS] 
	= maskedia | (tl ? 0 : controlReg[PRIMARY_CONTEXT]);
      break;

    case instruction_real_translation_miss:
      controlReg[I_TAG_ACCESS] 
	= maskedia | (tl ? 0 : controlReg[PRIMARY_CONTEXT]);
      break;

    case fast_data_access_MMU_miss:
      controlReg[D_TAG_ACCESS] = (maskedva(daddr,mode32) | daddr.cid);
      // std::cerr << "fdaMMU Setting D_TAG_ACCESS to " << std::hex
      // << controlReg[D_TAG_ACCESS] << std::dec << std::endl;
      break;

    case fast_data_access_protection_PS0:
      wasPS1 = false;
      controlReg[D_TAG_ACCESS] = (maskedva(daddr,mode32) | daddr.cid);
      controlReg[D_SF_STATUS] = (1 | asi_fault(daddr.asi, tl, pstate) | 
				 daddr.flags.e << 6 | (0x0 << 7) | 
				 (iswrite(daddr) << 2) |
				 ((controlReg[D_SF_STATUS]&1)<<1));
      controlReg[D_SF_ADDR] = extendm(daddr.va,48);
      break;

    case fast_data_access_protection_PS1:
      wasPS1 = true;
      controlReg[D_TAG_ACCESS] = (maskedva(daddr,mode32) | daddr.cid);
      controlReg[D_SF_STATUS] = (1 | asi_fault(daddr.asi, tl, pstate) | 
				 daddr.flags.e << 6 | (0x0 << 7) | 
				 (iswrite(daddr) << 2) |
				 ((controlReg[D_SF_STATUS]&1)<<1));
      controlReg[D_SF_ADDR] = extendm(daddr.va,48);
      break;

    case data_real_translation_miss:
      controlReg[D_TAG_ACCESS] = (maskedva(daddr,mode32) | daddr.cid);
      break;

    case mem_address_not_aligned:
    case STDF_mem_address_not_aligned:
    case LDDF_mem_address_not_aligned:
      controlReg[D_TAG_ACCESS] = (maskedva(daddr,mode32) | daddr.cid);
      controlReg[D_SF_STATUS] = (1 | asi_fault(daddr.asi, tl, pstate) | 
				 daddr.flags.e<<6 | (0x0 << 7) | 
				 (iswrite(daddr) << 2) |
				 ((controlReg[D_SF_STATUS]&1)<<1));
      controlReg[D_SF_ADDR] = extendm(daddr.va,48);
      break;

    case mem_address_not_aligned_target:
      controlReg[D_TAG_ACCESS] = (maskedva(daddr, mode32) |
				  (tl ? 0 : controlReg[PRIMARY_CONTEXT]));
      // NOTE: manual says that the ct bits are nondeterministic.  Aargh!
      controlReg[D_SF_STATUS] = (1 | 
				 ((pstate & pstate_cleM) ? ASI_PL : ASI_P)<<16|
				 (tl ? 0x10 : 0)<<4 |
				 ((controlReg[D_SF_STATUS]&1)<<1));
      controlReg[D_SF_ADDR] = extendm(daddr.va,48);
      break;

    case privileged_action_mmu:
      controlReg[D_TAG_ACCESS] = (maskedva(daddr,mode32) | daddr.cid);
      controlReg[D_SF_STATUS] = (1 | asi_fault(daddr.asi, tl, pstate) | 
				 daddr.flags.e<<6 | (0x0 << 7) | 
				 (iswrite(daddr) << 2) |
				 ((controlReg[D_SF_STATUS]&1)<<1));
      controlReg[D_SF_ADDR] = extendm(daddr.va,48);
      break;

    case VA_watchpoint:
      controlReg[D_TAG_ACCESS] = (maskedva(daddr,mode32) | daddr.cid);
      controlReg[D_SF_STATUS] = (1 | asi_fault(daddr.asi, tl, pstate) | 
				 daddr.flags.e<<6 | (0x0 << 7) | 
				 (iswrite(daddr) << 2) |
				 ((controlReg[D_SF_STATUS]&1)<<1));
      controlReg[D_SF_ADDR] = extendm(daddr.va,48);
      break;

    default:
      return;
    }
  }


  void T1_mmu::formMMUentry(T1_mmu_entry &ne, uint64_t accessreg,
			    uint64_t data,
			    bool isreal, bool is4v) {

    ne.real = isreal;
    ne.used = true;
    if (is4v) { // sun4v format
      ne.valid = data & bit(63);
      ne.flags.nfo = data & bit(62);
      ne.pa = (data >> 13) & (bit(44)-1);
      ne.flags.ie = data & bit(12);
      ne.flags.e = data & bit(11);
      ne.flags.cp = data & bit(10);
      ne.flags.cv = data & bit(9);
      ne.flags.p = data & bit(8);
      ne.flags.ep = data & bit(7);
      ne.flags.w = data & bit(6);
      ne.size = data & 7;  // NOTE: one bit is ignored in T1
      ne.locked = data & bit(61); // error in specification
      //  std::cerr << "Forming sun4v " << std::hex << data << std::data << ' '
      //	<< ne << std::endl;
    } else { // sun4u format
      ne.valid = data & bit(63);
      ne.size = ((data>>46)&4)|((data >> 61) & 3);
      ne.flags.nfo = data & bit(60);
      ne.flags.ie = data & bit(59);
      ne.pa = (data >> 13) & 0x7ffffff;
      ne.flags.cp = data & bit(5);
      ne.flags.cv = data & bit(4);
      ne.flags.e = data & bit(3);
      ne.flags.p = data & bit(2);
      ne.flags.w = data & bit(1);
      ne.flags.ep = false;
      ne.locked = data & bit(6);
      //std::cerr << "Forming sun4u " << std::hex << data << std::data << ' '
      //	<< ne << std::endl;
    }
    ne.partitionID = controlReg[PARTITION_ID];
    ne.contextID = accessreg & bits(13);
    ne.va = accessreg >> 13;
    ne.parity = (ne.valid ^ ne.flags.nfo ^ ne.flags.ie ^ ne.locked ^
		 ne.flags.cp ^ ne.flags.cv ^ ne.flags.e ^ ne.flags.p ^
		 ne.flags.w);
    for (int ii=0;ii<27;ii++) ne.parity ^= (ne.pa>>ii) & 1;
  }

  class T1_MMUWriteCommit : public LSE_device::resolver_t {
    T1_mmu *dev;
    SPARC_addrtransl_t addr;
    uint64_t data;
    SPARC_fault_t fault;
  public:
    T1_MMUWriteCommit(T1_mmu *m, SPARC_addrtransl_t a, uint64_t d, 
		      SPARC_fault_t f) 
      : resolver_t(LSE_device::resolveCommit), dev(m), addr(a), data(d), 
	fault(f) {}
    void commit() {
      dev->doMMUWrite(addr, data, fault, 0, 0, 0);
    }
  };

  void T1_mmu::doMMUWrite(SPARC_addrtransl_t &addr, uint64_t data,
			  SPARC_fault_t &fault, int writeFlags,
			  uint64_t *backup, LSE_device::resolver_t **reslist) 
  {
    //std::cerr << "Writing " << std::hex << data << " to " << std::dec 
    //   << addr.pa << "\n";

    if (fault.ft) return;

    switch (addr.pa) {

    case I_TAG_ACCESS:
      if (backup) *backup = controlReg[addr.pa];
      controlReg[addr.pa] = extendm(data,48);
      break;

    case D_TAG_ACCESS:
      if (backup) *backup = controlReg[addr.pa];
      controlReg[addr.pa] = extendm(data,48);
      break;

    case I_DATA_IN: // IMMU Data In Register (pp. 208-)
      if (backup) { 
	LSE_device::add_to_reslist(reslist, 
				   new T1_MMUWriteCommit(this, addr, data,
							 fault));
	T1_mmu_entry ne;
	formMMUentry(ne, controlReg[I_TAG_ACCESS], data, addr.va & 0x200, 
		     addr.va & 0x400);
	if (ne.size != 0 && ne.size != 1 && ne.size != 3 && ne.size != 5)
	  report_dae(fault, data_access_exception_invalid);
	return;
      }
      if (writeFlags) return; // no rollback
      {       // writing to this register writes to the TLB.
	T1_mmu_entry ne;
	formMMUentry(ne, controlReg[I_TAG_ACCESS], data, addr.va & 0x200, 
		     addr.va & 0x400);
	if (ne.size != 0 && ne.size != 1 && ne.size != 3 && ne.size != 5)
	  report_dae(fault, data_access_exception_invalid);
	else ctx->sharedcore->mmu.ITLB.install_entry(-1, ne);
      }
      break;

    case I_DATA_ACCESS: 
      if (backup) { 
	LSE_device::add_to_reslist(reslist, 
				   new T1_MMUWriteCommit(this, addr, data,
							 fault));
	T1_mmu_entry ne;
	formMMUentry(ne, controlReg[I_TAG_ACCESS], data, addr.va & 0x200, 
		     addr.va & 0x400);
	if (ne.size != 0 && ne.size != 1 && ne.size != 3 && ne.size != 5)
	  report_dae(fault, data_access_exception_invalid);
	return;
      }
      if (writeFlags) return; // no rollback
      {       // writing to this register writes to the TLB.
	T1_mmu_entry ne;
	formMMUentry(ne, controlReg[I_TAG_ACCESS], data, addr.va & 0x200, 
		     addr.va & 0x400);
	if (ne.size != 0 && ne.size != 1 && ne.size != 3 && ne.size != 5)
	  report_dae(fault, data_access_exception_invalid);
	else ctx->sharedcore->mmu.ITLB.install_entry((addr.va>>3) & 
						     (ITLB.size-1), ne);
      }
      break; 
    
    case D_DATA_IN: // DMMU Data In Register
      if (backup) { 
	LSE_device::add_to_reslist(reslist, 
				   new T1_MMUWriteCommit(this, addr, data,
							 fault));
	T1_mmu_entry ne;
	formMMUentry(ne, controlReg[D_TAG_ACCESS], data, addr.va & 0x200,
		     addr.va & 0x400);
	if (ne.size != 0 && ne.size != 1 && ne.size != 3 && ne.size != 5)
	  report_dae(fault, data_access_exception_invalid);
	return;
      }
      if (writeFlags) return; // no rollback
      // writing to this register writes to the TLB.
      {
	T1_mmu_entry ne;
	formMMUentry(ne, controlReg[D_TAG_ACCESS], data, addr.va & 0x200,
		     addr.va & 0x400);
	if (ne.size != 0 && ne.size != 1 && ne.size != 3 && ne.size != 5)
	  report_dae(fault, data_access_exception_invalid);
	else ctx->sharedcore->mmu.DTLB.install_entry(-1, ne);
      }
      break;
    
    case D_DATA_ACCESS:
      if (backup) { 
	LSE_device::add_to_reslist(reslist, 
				   new T1_MMUWriteCommit(this, addr, data,
							 fault));
	T1_mmu_entry ne;
	formMMUentry(ne, controlReg[D_TAG_ACCESS], data, addr.va & 0x200, 
		     addr.va & 0x400);
	if (ne.size != 0 && ne.size != 1 && ne.size != 3 && ne.size != 5)
	  report_dae(fault, data_access_exception_invalid);
	return;
      }
      if (writeFlags) return; // no rollback
      {       // writing to this register writes to the TLB.
	T1_mmu_entry ne;
	formMMUentry(ne, controlReg[D_TAG_ACCESS], data, addr.va & 0x200, 
		     addr.va & 0x400);
	if (ne.size != 0 && ne.size != 1 && ne.size != 3 && ne.size != 5)
	  report_dae(fault, data_access_exception_invalid);
	else ctx->sharedcore->mmu.DTLB.install_entry((addr.va>>3) & 
						     (DTLB.size-1), ne);
      }
      break; 

    case I_DEMAP:
      if (backup) { 
	LSE_device::add_to_reslist(reslist, 
				   new T1_MMUWriteCommit(this, addr, data,
							 fault));
	return;
      }
      if (writeFlags) return; // no rollback
      switch ((addr.va>>6)&3) {
      case 0: // demap page
	if (!(addr.va & bit(4))) {
	  int cont = (addr.va>>4)&3;
	  int cid = (addr.va>>9 & 1) ? -1 :
	    int((cont == 0 ? controlReg[PRIMARY_CONTEXT] :
		 cont == 1 ? controlReg[SECONDARY_CONTEXT] : 0));
	  ctx->sharedcore->mmu.ITLB.demap_page(controlReg[PARTITION_ID], cid, 
					       addr.va & bits(48));
	}
	break;
      case 1: // demap context
	if (!(addr.va & bit(4))) {
	  int cont = (addr.va>>4)&3;
	  int cid = (cont == 0 ? controlReg[PRIMARY_CONTEXT] :
		     cont == 1 ? controlReg[SECONDARY_CONTEXT] : 0);
	  ctx->sharedcore->mmu.ITLB.demap_context(controlReg[PARTITION_ID], cid);
	}
	break;
      case 2: // demap all
	ctx->sharedcore->mmu.ITLB.demap_all(controlReg[PARTITION_ID]);
	break;
      default: break;
      }
      break;
     
    case D_DEMAP:
      if (backup) { 
	LSE_device::add_to_reslist(reslist, 
				   new T1_MMUWriteCommit(this, addr, data,
							 fault));
	return;
      }
      if (writeFlags) return; // no rollback
      switch ((addr.va>>6)&3) {
      case 0: // demap page
	{
	  int cont = (addr.va>>4)&3;
	  if (cont != 3) {
	    int cid = (addr.va>>9 & 1) ? -1 :
	      int((cont == 0 ? controlReg[PRIMARY_CONTEXT] :
		   cont == 1 ? controlReg[SECONDARY_CONTEXT] : 0));
	    ctx->sharedcore->mmu.DTLB.demap_page(controlReg[PARTITION_ID], 
						 cid, addr.va & bits(48));
	  }
	}
	break;
      case 1: // demap context
	{
	  int cont = (addr.va>>4)&3;
	  if (cont != 3) {
	    int cid = (cont == 0 ? controlReg[PRIMARY_CONTEXT] :
		       cont == 1 ? controlReg[SECONDARY_CONTEXT] : 0);
	    ctx->sharedcore->mmu.DTLB.demap_context(controlReg[PARTITION_ID], 
						    cid);
	  }
	}
	break;
      case 2: // demap all
	ctx->sharedcore->mmu.DTLB.demap_all(controlReg[PARTITION_ID]);
	break;
      default: break;
      }
      break;
    
    case I_INVAL_ALL:
      if (backup) { 
	LSE_device::add_to_reslist(reslist, 
				   new T1_MMUWriteCommit(this, addr, data,
							 fault));
	return;
      }
      if (writeFlags) return; // no rollback
      ctx->sharedcore->mmu.ITLB.inval_all();
      break;
    
    case D_INVAL_ALL:
      if (backup) { 
	LSE_device::add_to_reslist(reslist, 
				   new T1_MMUWriteCommit(this, addr, data,
							 fault));
	return;
      }
      if (writeFlags) return; // no rollback
      ctx->sharedcore->mmu.DTLB.inval_all();
      break;
     
    case I_Z_CONFIG:
    case D_Z_CONFIG:
    case I_NZ_CONFIG:
    case D_NZ_CONFIG:
      {
	if (backup) *backup = controlReg[addr.pa];
	if (writeFlags) controlReg[addr.pa] = data;
	else {
	  // values get changed as they are written (UST1 p. 202)
	  int fv0 = (data & 7);
	  if (!(fv0 == 0 || fv0 == 1 || fv0 == 3 || fv0 == 5)) fv0 = 5;
	  int fv1 = ((data>>8) & 7);
	  if (!(fv1 == 0 || fv1 == 1 || fv1 == 3 || fv1 == 5)) fv1 = 5;
	  data = fv0 | (fv1 << 8);
	  controlReg[addr.pa] = data;
	}
      }
      break;

    case DUMMY:
      break;

    case SPARC_ERROR_STATUS_REG:
      if (backup) *backup = controlReg[addr.pa];
      if (writeFlags) controlReg[addr.pa] = data;
      else controlReg[addr.pa] &= ~(data & ~bit(28));
      break;

    case SWVR_INTR_RECEIVE:
      if (backup) *backup = controlReg[addr.pa];
      if (writeFlags) controlReg[addr.pa] = data;
      else controlReg[addr.pa] &= data;
      break;

    case SWVR_UDB_INTR_W:
      {
	if (backup) { 
	  if (reslist) 
	    LSE_device::add_to_reslist(reslist, 
				       new T1_MMUWriteCommit(this, addr, data,
							     fault));
	  return;
	}
	if (writeFlags) return; // no rollback
	//std::cerr << "Write to the INTR_W register\n";
	// ctx should be fine
	int vc_id = (data >> 8) & bits(5);
	int vector = data & bits(6);
	int cno = ctx->sharedchip->proc2HWcno[vc_id];
	if (!cno) std::cerr << " Unable to find " << vc_id << "\n";
	LSE_emu_isacontext_t *rct = get_token(cno);
	if (rct) {
	  rct->mmu.controlReg[SWVR_INTR_RECEIVE] |= (uint64_t(1)<<vector);
	}
      }
      break;

    case MA_CONTROL:
    case MA_PA:
    case MA_ADDR:
    case MA_NP:
    case SPARC_BIST_CONTROL:
    case INST_MASK_REG:
    case LSU_DIAG_REG:
    case STM_CTL_REG:
    case SPARC_ERROR_EN_REG:
      if (backup) *backup = ctx->sharedcore->mmu.controlReg[addr.pa];
      if (writeFlags) ctx->sharedcore->mmu.controlReg[addr.pa] = data;
      else ctx->sharedcore->mmu.controlReg[addr.pa] 
	     = data & controlRegMasks[addr.pa];
      break;

    default:
      if (backup) *backup = controlReg[addr.pa];
      if (writeFlags) controlReg[addr.pa] = data;
      else controlReg[addr.pa] = data & controlRegMasks[addr.pa];
      break;
    }
  
  } // doMMUWrite

  class T1_MMUReadRback : public LSE_device::resolver_t {
    T1_mmu *dev;
    uint64_t data;
  public:
    T1_MMUReadRback(T1_mmu *m, uint64_t d) : 
      resolver_t(LSE_device::resolveRollback), dev(m), data(d) {}
    void rollback() {
      dev->controlReg[SWVR_INTR_RECEIVE] = data;
    }
  };

  uint64_t T1_mmu::doMMURead(SPARC_addrtransl_t &addr, SPARC_fault_t &fault, 
			     LSE_device::resolver_t **reslist) {

    static uint64_t muxenc[8] = { uint64_t(7)<<56, uint64_t(6)<<56, 0, 
				  uint64_t(4)<<56, 
				  0, 0, 0, 0 };
    static uint64_t muxenc2[8] = { 0, uint64_t(1)<<56, 0, uint64_t(3)<<56,
				   0, uint64_t(7)<<56, 0, 0 };

    switch (addr.pa) {

    case I_TAG_TARGET:
      return ( ((controlReg[I_TAG_ACCESS] & bits(13))<<48) |
	       ((controlReg[I_TAG_ACCESS] >> 22) ) );
    case D_TAG_TARGET:
      //std::cerr << "D_TAG_TARGET reading D_TAG_ACCESS as " << std::hex
      // << controlReg[D_TAG_ACCESS] << std::dec << std::endl;
      return ( ((controlReg[D_TAG_ACCESS] & bits(13))<<48) |
	       ((controlReg[D_TAG_ACCESS] >> 22) ) );

    case I_PS0_PTR:
      if (controlReg[I_TAG_ACCESS] & bits(13)) 
	return T1_mmu_calc_ptr(controlReg[I_NZ_TSB_PS0], 
			       controlReg[I_TAG_ACCESS],
			       controlReg[I_NZ_CONFIG]&7, false);
      else
	return T1_mmu_calc_ptr(controlReg[I_Z_TSB_PS0], 
			       controlReg[I_TAG_ACCESS],
			       controlReg[I_Z_CONFIG]&7, false);
    case I_PS1_PTR:
      if (controlReg[I_TAG_ACCESS] & bits(13)) 
	return T1_mmu_calc_ptr(controlReg[I_NZ_TSB_PS1], 
			       controlReg[I_TAG_ACCESS],
			       (controlReg[I_NZ_CONFIG]>>8)&7, true);
      else
	return T1_mmu_calc_ptr(controlReg[I_Z_TSB_PS1], 
			       controlReg[I_TAG_ACCESS],
			       (controlReg[I_Z_CONFIG]>>8)&7, true);

    case D_PS0_PTR:
      if (controlReg[D_TAG_ACCESS] & bits(13)) 
	return T1_mmu_calc_ptr(controlReg[D_NZ_TSB_PS0], 
			       controlReg[D_TAG_ACCESS],
			       controlReg[D_NZ_CONFIG]&7, false);
      else
	return T1_mmu_calc_ptr(controlReg[D_Z_TSB_PS0], 
			       controlReg[D_TAG_ACCESS],
			       controlReg[D_Z_CONFIG]&7, false);
    case D_PS1_PTR:
      if (controlReg[D_TAG_ACCESS] & bits(13)) 
	return T1_mmu_calc_ptr(controlReg[D_NZ_TSB_PS1], 
			       controlReg[D_TAG_ACCESS],
			       (controlReg[D_NZ_CONFIG]>>8)&7, true);
      else
	return T1_mmu_calc_ptr(controlReg[D_Z_TSB_PS1], 
			       controlReg[D_TAG_ACCESS],
			       (controlReg[D_Z_CONFIG]>>8)&7, true);

    case D_DIRECT_PTR:
      if (!wasPS1) {
	if (controlReg[D_TAG_ACCESS] & bits(13)) 
	  return T1_mmu_calc_ptr(controlReg[D_NZ_TSB_PS0], 
				 controlReg[D_TAG_ACCESS],
				 controlReg[D_NZ_CONFIG]&7, false);
	else
	  return T1_mmu_calc_ptr(controlReg[D_Z_TSB_PS0], 
				 controlReg[D_TAG_ACCESS],
				 controlReg[D_Z_CONFIG]&7, false);
      } else {
	if (controlReg[D_TAG_ACCESS] & bits(13)) 
	  return T1_mmu_calc_ptr(controlReg[D_NZ_TSB_PS1], 
				 controlReg[D_TAG_ACCESS],
				 (controlReg[D_NZ_CONFIG]>>8)&7, true);
	else
	  return T1_mmu_calc_ptr(controlReg[D_Z_TSB_PS1], 
				 controlReg[D_TAG_ACCESS],
				 (controlReg[D_Z_CONFIG]>>8)&7, true);
      }


    case I_TAG_READ:
    case D_TAG_READ:
      { 
	T1_mmu_entry &ent 
	  = ( addr.pa == I_TAG_READ ? 
	      ctx->sharedcore->mmu.ITLB.array[(addr.va >> 3) & (ITLB.size-1)] :
	      ctx->sharedcore->mmu.DTLB.array[(addr.va >> 3) & (DTLB.size-1)]);

	uint64_t tmp = ( (uint64_t(ent.partitionID) << 61) | ent.contextID |
			 muxenc[ent.size] | (uint64_t(ent.real) << 60) |
			 extendm(ent.va<<13,48) & bits(56) );
	bool parity = ent.locked;
	for (int ii = 0; ii < 64; ++ii) parity ^= (tmp>>ii)&1;
	return tmp | (uint64_t(parity)<<59);
      }

    case D_SF_ADDR:
      return extend(controlReg[D_SF_ADDR],48);


    case I_DATA_ACCESS:
    case D_DATA_ACCESS:
      { 
	T1_mmu_entry &ent 
	  = ( addr.pa == I_DATA_ACCESS ? 
	      ctx->sharedcore->mmu.ITLB.array[(addr.va >> 3) & (ITLB.size-1)] :
	      ctx->sharedcore->mmu.DTLB.array[(addr.va >> 3) & (DTLB.size-1)]);
	uint64_t tmp = ( (uint64_t(ent.valid) << 63) |
			 (uint64_t(ent.size & 3) << 61) |
			 (uint64_t(ent.flags.nfo) << 60) |
			 (uint64_t(ent.flags.ie) << 59) |
			 (uint64_t(ent.used) << (40+7)) |
			 (muxenc2[ent.size] >> (56-43)) |
			 (uint64_t(ent.size & 4) << 46) |
			 (uint64_t(ent.pa) << 13) |
			 (uint64_t(ent.locked) << 6) |
			 (uint64_t(ent.flags.cp) << 5) |
			 (uint64_t(ent.flags.cv) << 4) |
			 (uint64_t(ent.flags.e) << 3) |
			 (uint64_t(ent.flags.p) << 2) |
			 (uint64_t(ent.flags.w) << 1)
			 );
	bool parity = ent.parity;
	for (int ii = 0; ii < 3; ++ii) 
	  parity ^= (muxenc2[ent.size]>>(ii+56))&1;
	return ( tmp | (uint64_t(parity) << (40+6)));
      }

    case SWVR_UDB_INTR_R:
      { // NOTE: read has a side effect!
	uint64_t val = controlReg[SWVR_INTR_RECEIVE];
	int ii;
	for (ii = 63; ii > 0; --ii) {
	  if (val & (uint64_t(1)<<ii)) break;
	}
	if (reslist) add_to_reslist(reslist, new T1_MMUReadRback(this, val));
	controlReg[SWVR_INTR_RECEIVE] &= ~(uint64_t(1)<<ii);
	return ii;
      }
      break;

    case DUMMY:
      return 0;

    case MA_CONTROL:
    case MA_PA:
    case MA_ADDR:
    case MA_NP:
    case SPARC_BIST_CONTROL:
    case INST_MASK_REG:
    case LSU_DIAG_REG:
    case STM_CTL_REG:
    case SPARC_ERROR_EN_REG:
      return ctx->sharedcore->mmu.controlReg[addr.pa];

    default:
      return controlReg[addr.pa];
    }
  }

  const char T1_mmu::ASItable[256] = {
    0, 0, 0, 0, nucleus,      0, 0, 0, // 0-7
    0, 0, 0, 0, nucleus|isle, 0, 0, 0, // 8-f
    primary|asif,      secondary|asif,      0, 0, // 10-13
    real|nucleus, real|nucleus, primary|asif, secondary|asif, // 14-17
    primary|asif|isle, secondary|asif|isle, 0, 0, // 18-1b
    real|nucleus|isle, real|nucleus|isle, // 1c-ad
    primary|asif|isle, secondary|asif|isle, // 1e-1f
    0, 0, primary|asif,      secondary|asif,      // 20-23
    nucleus,      0, real|nucleus,      nucleus, // 24-27
    0, 0, primary|asif|isle, secondary|asif|isle, // 28-2b
    nucleus|isle, 0, real|nucleus|isle, nucleus|isle, // 2c-2f
    0, 0, 0, 0, 0, 0, 0, 0, // 30-37
    0, 0, 0, 0, 0, 0, 0, 0, // 38-3f
    0, 0, 0, 0, 0, 0, 0, 0, // 40-47
    0, 0, 0, 0, 0, 0, 0, 0, // 48-4f
    0, 0, 0, 0, 0, 0, 0, 0, // 50-57
    0, 0, 0, 0, 0, 0, 0, 0, // 58-5f
    0, 0, 0, 0, 0, 0, 0, 0, // 60-67
    0, 0, 0, 0, 0, 0, 0, 0, // 68-6f
    0, 0, 0, 0, 0, 0, 0, 0, // 70-77
    0, 0, 0, 0, 0, 0, 0, 0, // 78-7f
    primary,      secondary,   primary|nofault, secondary|nofault, // 80-83
    0, 0, 0, 0, // 84-87
    primary|isle, secondary|isle, // 88-89
    primary|nofault|isle, secondary|nofault|isle, // 8a-8b
    0, 0, 0, 0, // 8c-8f
    0, 0, 0, 0, 0, 0, 0, 0, // 90-97
    0, 0, 0, 0, 0, 0, 0, 0, // 98-9f
    0, 0, 0, 0, 0, 0, 0, 0, // a0-a7
    0, 0, 0, 0, 0, 0, 0, 0, // a8-af
    0, 0, 0, 0, 0, 0, 0, 0, // b0-b7
    0, 0, 0, 0, 0, 0, 0, 0, // b8-bf
    primary|notimpl, secondary|notimpl, // c0-c1
    primary|notimpl, secondary|notimpl, // c2-c3
    primary|notimpl, secondary|notimpl, 0, 0, // c4-c7
    primary|notimpl|isle, secondary|notimpl|isle, // c8-c9
    primary|notimpl|isle, secondary|notimpl|isle, // ca-cb
    primary|notimpl|isle, secondary|notimpl|isle, 0, 0, // cc-cf
    primary|notimpl, secondary|notimpl, // d0-d1
    primary|notimpl, secondary|notimpl, // d2-d3
    0, 0, 0, 0, // d4-d7
    primary|notimpl|isle, secondary|notimpl|isle, // d8-d9
    primary|notimpl|isle, secondary|notimpl|isle, // da-db
    0, 0, 0, 0, // dc-df
    primary|notimpl, secondary|notimpl, // e0-e1
    primary,      secondary,      0, 0, 0, 0, // e2-e7
    0, 0, primary|isle, secondary|isle, 0, 0, 0, 0, // e8-ef
    primary,      secondary,      0, 0, 0, 0, 0, 0, // f0-f7
    primary|isle, secondary|isle, 0, 0, 0, 0, 0, 0, // f8-ff
  };

  /******************* Specialized device models *******************/

  devaddr_t T1_IntHandler::translate(devaddr_t addr, 
				     device_t **devpp, devtptr_t *datapp) {
    *devpp = this;
    datapp->a = addr;
    return 8 - addr % 8;
  }

  devaddr_t T1_IntHandler::readSub(devaddr_t addr, devdata_t *buffer, 
				   devaddr_t len, resolver_t **reslist) {
    ::memset(buffer, 0, len);
    std::cerr << "Reading " << std::hex << addr << std::dec << " " 
	      << len << std::endl;
    return len;
  } // rom::read
  
  // Note: no speculative writes for cross-thread stuff.

  devaddr_t T1_IntHandler::writeSub(devaddr_t addr, const devdata_t *buffer, 
				    devaddr_t len, int writeFlags,
				    devdata_t *backup, resolver_t **reslist) {

    uint64_t nv;
    if (writeFlags & writeOpRollback) { return len; } // no rollback
    if (backup) {
      if (reslist) 
	add_to_reslist(reslist, 
		       new LSE_device::writeCommit(this, addr, buffer, len));
      return len;
    }
	       
    if (len == 8) nv = LSE_b2h(*((uint64_t *)(buffer)));
    else { 
      memcpy(&nv, buffer, len);
      nv = LSE_b2h(nv);
    }
    //std::cerr << "Writing " << std::hex << addr << std::dec << " " 
    //	      << len << " " << std::hex << nv << std::dec << std::endl;

    switch (addr / 8) {
    case 0x100: { // interrupt/vector dispatch register

      //int vector = nv & 0x3f;
      int vc_id = (nv >> 8) & 0x1f;
      int atype = (nv >> 16) & 3;
      LSE_emu_isacontext_t *realct = get_token(masterHWcno);
      if (!realct) return len;

      int cno = realct->proc2HWcno[vc_id];

      LSE_emu_isacontext_t *ctx = get_token(cno);
      if (!ctx) return len;

      switch (atype) {
      case 2 : // idle
	//std::cerr << "Idling " << vc_id << " " << cno << std::endl;
	SPARC_thread_set_active(cno, false);
	break;
      case 3 : // resume
	//std::cerr << "Resuming " << vc_id << " " << cno << std::endl;
	SPARC_thread_set_active(cno, true);
	break;
      case 0: // interrupt
      case 1 : // reset
      default:
	break;
      }
      break;
    }
    default:
      break;
    }
    return len;
  } // rom::read

  void T1_IntHandler::setMasterHWcno(int m) {
    masterHWcno = m; 
    LSE_emu_isacontext_t *realct = get_token(m);
    realct->interruptDev = this;
  }

  bool T1_IntHandler::handleCommand(const char *cmd, ...) {
    args_t argv;

    va_list ap;
    va_start(ap, cmd);

    form_args(cmd, argv);
    if (!argv.size()) goto nothandled;

    if (argv[0] == "setmasterhwcno") {
      int cno = va_arg(ap, int);
      setMasterHWcno(cno);
    } else std::cerr << "T1_IntHandler command: unknown command\n";

  nothandled:
    va_end(ap);
    return false;
  handled:
    va_end(ap);
    return true;
  }

  device_t *create_T1_IntHandler_inst(device_t *parent, 
				      const LSE_device::args_t &argv) {

    devaddr_t len, base;
    if (argv.size() < 2 || argv.size() > 3) {
      std::cerr << "T1_IntHandler instantiation: bad arguments\n";
      return 0;
    }
    if (get_addr_arg(argv[0], &base)) {
      std::cerr << "T1_IntHandler instantation: bad base\n";
      return 0;
    }
    if (get_addr_arg(argv[1], &len)) {
      std::cerr << "T1_IntHandler instantation: bad length\n";
      return 0;
    }
    T1_IntHandler *nd = new T1_IntHandler();
    if (argv.size() > 2) {
      long cno = strtol(argv[2].c_str(), 0, 0);
      nd->setMasterHWcno(cno);
    }
    parent->addchild(base, len, nd);
    return nd;
  }

  LSE_device::registrar T1_IntHandler_registered("UltraSPARC_T1_IntHandler", 
						 create_T1_IntHandler_inst);

  void perform_trap_impl(UltraSPARCT1_selector *dummy, 
			 LSE_emu_isacontext_t &ctx,
			 LSE_emu_instr_info_t &LIS_ii, bool redstate,
			 int tl, uint64_t hpstate, uint64_t pstate, 
			 uint64_t gl) {

    // If we go to RED state, pstate/hpstate don't follow UA 2005 manual
    // and what they actually do isn't really documented in the T1 supplement

    if (redstate) {
      *get_pr_ptr(&ctx, PSTATE) |= pstate_privM;
      *get_pr_ptr(&ctx, PSTATE) &= ~pstate_tleM;
      *get_hpr_ptr(&ctx, HPSTATE) &= ~hpstate_enbM;
      ctx.mmu.controlReg[LSU_CONTROL] = 0; // register gets cleared
    } else if (!(hpstate & hpstate_enbM)) { 

      // crazy undocumented hypervisor enable stuff... a weird mix of
      // hypervisor mode and supervisor mode

      *get_pr_ptr(&ctx, PSTATE) 
	= ( (pstate & ~pstate_amM & ~pstate_cleM & 
	     ~pstate_ieM & ~pstate_tctM) 
	    | pstate_pefM | pstate_privM
	    | ((pstate & pstate_tleM) ? pstate_cleM : 0) );
      *get_hpr_ptr(&ctx, HPSTATE) |= hpstate_hprivM;

      uint64_t htba = *get_hpr_ptr(&ctx, HTBA) & ~bits(14);
      int tt = *get_pr_ptr(&ctx, TT, tl + 1);
      LIS_ii.next_pc.PC = htba | ((tl + 1 > 1) << 14) | (tt << 5);
      LIS_ii.next_pc.NPC = htba | ((tl + 1 > 1) << 14) | (tt << 5) | 4;
      LIS_ii.next_pc.annul = 0;
    }

    // p.40: T1 doesn't follow the UA 2005 spec and doesn't clear ibe
    // however, it appears to do so when going to redmode.
    if (LIS_ii.fault.ft != instruction_breakpoint && !redstate)
      *get_hpr_ptr(&ctx, HPSTATE) |= hpstate & hpstate_ibeM;

    // TPC/TNPC should be sign-extended on read.  But SAS does it on write,
    // so we need to do so to keep sanity in validation; besides, it's 
    // convenient and should be equivalent.  The trick is that we can't lose
    // the fact that TPC/TNPC are in the VA hole, as pointed out on p.64 of the
    // T1 manual.  This isn't too hard; how can they get there?
    // 1) trap in delay slot of jmpl/return: already checked in those 
    // 2) trap in delay slot of call/branch into bad territory.
    //    instructions.  If in lower half, TPC[47] is 0 and TNPC[47] is 1.
    //    If in upper half, TPC[47] is 1, but TNPC[47] is 0.  So only
    //    sign extend if both are 1.
    // 3) trap in last instruction before the hole.  TPC[47] is 0, TNPC[47] is
    //    1.  Sign extend only if both are 1 works.
    //
    // The way the HW does all this is by keeping 49 bits (not 48), doing the
    // check based upon bit[48]^[bit47] and sign extending from bit 47 on all
    // of the reads.  I bet it does the same for PC to handle the 
    // "run-off-the-end" case.  Rather clever!
    //
    // Note that explicit writes into TPC/TNPC can't put you in the VA hole
    // because they sign extend.

    int ntl = *get_pr_ptr(&ctx, TL);
    *get_pr_ptr(&ctx, TPC, ntl) = extend(*get_pr_ptr(&ctx, TPC, ntl),48);
    if (*get_pr_ptr(&ctx, TPC, ntl) & bit(47))
      *get_pr_ptr(&ctx, TNPC, ntl) = extend(*get_pr_ptr(&ctx, TNPC, ntl),48);
  }

} // namespace LSE_SPARC
