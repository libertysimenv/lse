/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header for ELF support
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file defines the routines which support the ELF header
 *
 */
#ifndef _LSE_ELF_H_
#define _LSE_ELF_H_
#include <LSE_inttypes.h>
#include <stdio.h>

namespace LSE_elf {

  struct Elf32_Ehdr {
    unsigned char e_ident[16];
    uint16_t e_type;
    uint16_t e_machine;
    uint32_t e_version;
    uint32_t e_entry;
    uint32_t e_phoff;
    uint32_t e_shoff;
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;
    uint16_t e_shentsize;
    uint16_t e_shnum;
    uint16_t e_shstrndx;
  };

  struct  Elf64_Ehdr {
    unsigned char e_ident[16];
    uint16_t e_type;
    uint16_t e_machine;
    uint32_t e_version;
    uint64_t e_entry;
    uint64_t e_phoff;
    uint64_t e_shoff;
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;
    uint16_t e_shentsize;
    uint16_t e_shnum;
    uint16_t e_shstrndx;
  };

  enum e_ident_vals { EI_MAG0=0, EI_MAG1=1, EI_MAG2=2, EI_MAG3=3, EI_CLASS=4, 
		      EI_DATA=5, EI_VERSION=6, EI_PAD=7, EI_NIDENT=16 };
  enum EI_CLASS_vals { ELFCLASSNONE=0, ELFCLASS32=1, ELFCLASS64=2 };
  enum EI_DATA_vals { ELFDATANONE=0, ELFDATA2LSB=1, ELFDATA2MSB=2 };
  enum EI_VERSION_vals { EV_NONE=0, EV_CURRENT=1 };
  enum e_type_vals { ET_NONE=0, ET_REL=1, ET_EXEC=2, ET_DYN=3, ET_CORE=4 };

  static const char *ELFMAG = "\177ELF";
  static const int SELFMAG = 4;

  union ElfB_Ehdr {
    Elf32_Ehdr h32;
    Elf64_Ehdr h64;
  };

  struct Elf32_Shdr {
    uint32_t sh_name;
    uint32_t sh_type;
    uint32_t sh_flags;
    uint32_t sh_addr;
    uint32_t sh_offset;
    uint32_t sh_size;
    uint32_t sh_link;
    uint32_t sh_info;
    uint32_t sh_addralign;
    uint32_t sh_entsize;
  };

  struct Elf64_Shdr {
    uint32_t sh_name;
    uint32_t sh_type;
    uint64_t sh_flags;
    uint64_t sh_addr;
    uint64_t sh_offset;
    uint64_t sh_size;
    uint32_t sh_link;
    uint32_t sh_info;
    uint64_t sh_addralign;
    uint64_t sh_entsize;
  };

  union ElfB_Shdr {
    Elf32_Shdr h32;
    Elf64_Shdr h64;
  };

  struct Elf32_Phdr {
    uint32_t p_type;
    uint32_t p_offset;
    uint32_t p_vaddr;
    uint32_t p_paddr;
    uint32_t p_filesz;
    uint32_t p_memsz;
    uint32_t p_flags;
    uint32_t p_align;
  };
  
  struct Elf64_Phdr {
    uint32_t p_type;
    uint32_t p_flags;
    uint64_t p_offset;
    uint64_t p_vaddr;
    uint64_t p_paddr;
    uint64_t p_filesz;
    uint64_t p_memsz;
    uint64_t p_align;
  };
  
  union ElfB_Phdr {
    Elf32_Phdr h32;
    Elf64_Phdr h64;
  };

  /****************** Types *****************************/

  typedef enum error_t {
    error_none = 0,
    error_outofmemory,
    error_badargument,
    error_notimplemented,
    error_fileerror,
    error_fileheader,
    error_notelf,
    error_not32bit,
    error_not64bit,
    error_notrightsize,
    error_badhversion,
    error_notexec,
    error_badmachinetype,
    error_eof,
    error_pheaders,
    error_sheaders,
  } error_t;

  /***************** Miscellaneous APIs ****************/

  extern const char *errstring(error_t errval);

  /***************** Load APIs **************************/

  typedef int (*Elf32_pfunc)(int, uint64_t, int, Elf32_Phdr *, FILE *,
			     void *, int);
  typedef int (*Elf64_pfunc)(int, uint64_t, int, Elf64_Phdr *, FILE *,
			     void *, int);
  typedef int (*ElfB_pfunc)(int, uint64_t, int, ElfB_Phdr *, FILE *,
			    void *, int);
  typedef int (*Elf32_sfunc)(Elf32_Shdr *, void *, int);
  typedef int (*Elf64_sfunc)(Elf64_Shdr *, void *, int);
  typedef int (*ElfB_sfunc)(ElfB_Shdr *, void *, int);

  typedef int (*Elf_mtypecheck)(int type, void *);

  /* pfunc is called for each loadable program header segment with the 
   * file positioned to the file position for the program header.
   * sfunc is called for each program segment
   * stuff is passed as an argument to each
   */
  extern error_t load64(const char *fname, 
			Elf_mtypecheck mtype,
			Elf64_Ehdr *topheader,
			Elf64_pfunc pfunc,
			Elf64_sfunc sfunc,
			void *stuff);

  extern error_t load32(const char *fname, 
			Elf_mtypecheck mtype,
			Elf32_Ehdr *topheader,
			Elf32_pfunc pfunc,
			Elf32_sfunc sfunc,
			void *stuff);

  extern error_t load(const char *fname, 
		      Elf_mtypecheck mtype,
		      ElfB_Ehdr *topheader,
		      unsigned char match,
		      ElfB_pfunc pfunc,
		      ElfB_sfunc sfunc,
		      void *stuff);

  extern error_t check(const char *fname, 
		       Elf_mtypecheck mtype,
		       ElfB_Ehdr *topheader,
		       unsigned char match);
  
} // namespace LSE_elf

#endif /* _LSE_ELF_H_ */
