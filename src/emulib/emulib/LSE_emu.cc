/* 
 * Copyright (c) 2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Emulator domain class functions
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 */

#include <LSE_emu.h>
#include <stdlib.h>

extern int LSE_sim_terminate_count;

namespace LSE_emu {

  LSE_emu_contextno_t LSE_emu_hwcontexts_total = 0;
  LSE_emu_context_t *LSE_emu_hwcontexts_table = 0;

  void (*map_in_hook)(LSE_emu_contextno_t) = 0;
  void (*realloc_hook)(LSE_emu_contextno_t) = 0;

  /* Update the context mapping; called from an emulator */
  int LSE_emu_update_context_map(LSE_emu_contextno_t hwcno,
				 LSE_emu_ctoken_t swtok) {
    int deltaalive;

    if (hwcno <= 0 || hwcno > LSE_emu_hwcontexts_total || 
	!LSE_emu_hwcontexts_table[hwcno].valid) return 1;

    deltaalive = -(LSE_emu_hwcontexts_table[hwcno].ctok != 0);
      
    LSE_emu_hwcontexts_table[hwcno].ctok = swtok;
    if (swtok) {
      if (map_in_hook) (*map_in_hook)(hwcno);
    }

    deltaalive += (swtok != 0) ;
    LSE_sim_terminate_count += deltaalive;
    return 0;
  }

  LSE_emu_contextno_t LSE_emu_get_contextno(boolean wanthw) 
  {
    int i;
    for (i=1;i<=LSE_emu_hwcontexts_total;i++) 
      if (!LSE_emu_hwcontexts_table[i].valid) break;
    return i;
  }

  // Functions internal to the domain, with appropriate notification
  // functions as needed.

  int init_int(void) {
    if (LSE_emu_hwcontexts_table) free(LSE_emu_hwcontexts_table);

    LSE_emu_hwcontexts_table
      = (LSE_emu_context_t *)calloc(1,sizeof(LSE_emu_context_t));
    LSE_emu_hwcontexts_table[0].valid = TRUE;
    LSE_emu_hwcontexts_total = 0; 
    return 0;
  }

  int finalize_int(void) {
    if (LSE_emu_hwcontexts_table) free(LSE_emu_hwcontexts_table);
    LSE_emu_hwcontexts_table = 0;
    return 0;
  }

  int create_context_int(LSE_emu_interface_t *ifc, int instid,
			 LSE_emu_contextno_t cno, 
			 int (*cc)(LSE_emu_interface_t *,
				   LSE_emu_ctoken_t *, LSE_emu_contextno_t)
			 ) {

    if (cno <= 0) return 1;
    if (cno > LSE_emu_hwcontexts_total) {
      LSE_emu_hwcontexts_table
	= (LSE_emu_context_t *)realloc(LSE_emu_hwcontexts_table,
				       sizeof(LSE_emu_context_t) * (cno+1));
      if (*realloc_hook) (*realloc_hook)(cno+1);
      for (int i=LSE_emu_hwcontexts_total+1 ; i < cno ; i++) {
	LSE_emu_hwcontexts_table[i].valid = FALSE;
      }
      LSE_emu_hwcontexts_total = cno;
    } else {
      if (LSE_emu_hwcontexts_table[cno].valid) return 0;
    }
    LSE_emu_hwcontexts_table[cno].valid = TRUE;
    LSE_emu_hwcontexts_table[cno].emuinstid = instid;
    return (*cc)(ifc, &LSE_emu_hwcontexts_table[cno].ctok, cno);
  }
	
} /* namespace LSE_emu */

