/* 
 * Copyright (c) 2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Environment variable support
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file provides a means for emulators to manipulate environment variables
 * very easily.
 *
 */
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <LSE_env.h>

namespace LSE_env {

  env_t::env_t(const char **env) {
    setenv(env, true);
  }

  void env_t::clearenv() {
    entries.clear();
  }

  void env_t::setenv(const env_t &that, int overwrite) {
    for (enventries_t::const_iterator i = that.entries.begin(); 
	 i != that.entries.end(); ++i) {
      setenv(i->first.c_str(), i->second.c_str(), overwrite);
    }
  }

  void env_t::setenv(const char **env, int overwrite) {
    if (!env) return;
    while (*env) {
      setenv(*env, overwrite);
      env++;
    }
  }

  void env_t::setenv(const char *pair, int overwrite) {
    if (!pair || !*pair) return;
    const char *eq = std::strchr(pair, '=');
    if (!eq) { // empty var
      setenv(pair, "", overwrite);
    } else if (eq != pair) {
      std::string ns(pair,eq - pair);
      setenv(ns.c_str(), eq + 1, overwrite);
    }
  }

  void env_t::setenv(const char *name, const char *val, int overwrite) {
    enventries_t::iterator findit = find(name);
    if (findit == entries.end()) {
      entries.push_back(enventry_t(name,val));
    } else if (overwrite) {
      findit->second = std::string(val);
    }
  }

  void env_t::unsetenv(const char *name) {
    enventries_t::iterator findit = find(name);
    if (findit != entries.end()) entries.erase(findit);
  }

  const char *env_t::getenv(const char *name) {
    enventries_t::iterator findit = find(name);
    if (findit == entries.end()) return 0;
    else return findit->second.c_str();
  }

  int env_t::size() const {
    return entries.size();
  }

  char **env_t::getenvbuf() {
    int size = (entries.size() + 1) * sizeof(char *);
    int strsize = 0;
    for (enventries_t::iterator i = entries.begin(); i != entries.end(); ++i) {
      strsize = strsize + i->first.size() + i->second.size() + 2;
    }
    char **base = (char **)std::malloc(size + strsize);
    char **pp = base;
    char *sp = reinterpret_cast<char *>(base + (entries.size() + 1));

    for (enventries_t::iterator i = entries.begin(); i != entries.end(); ++i) {
      *pp++ = sp;
      sp += std::sprintf(sp,"%s=%s",i->first.c_str(),i->second.c_str())+1;
    }

    *pp = 0;
    return base;
  }

  env_t::enventries_t::iterator env_t::find(const char *name) {
    for (enventries_t::iterator i = entries.begin(); i != entries.end(); ++i) {
      if (i->first == name) return i;
    }
    return entries.end();
  }

  
} // namespace LSE_env

