/* 
 * Copyright (c) 2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Environment variable support header
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file provides a means for emulators to manipulate environment variables
 * very easily.
 *
 */
#include <string>
#include <vector>

namespace LSE_env {

  class env_t {

    // Because I want to maintain the order without sorting, I will maintain
    // this in a list.

    typedef std::pair<std::string, std::string> enventry_t;
    typedef std::vector<enventry_t> enventries_t;
    enventries_t entries;

  public:
    // constructors
    env_t() { }
    env_t(const char **env);

    // accessors
    void clearenv();
    void setenv(const env_t &, int overwrite);
    void setenv(const char **env, int overwrite);
    void setenv(const char *pair, int overwrite);
    void setenv(const char *name, const char *val, int overwrite);
    void unsetenv(const char *name);
    const char *getenv(const char *name);
    int size() const;

    // and form a buffer, which should be freed after it has been used.
    char **getenvbuf();

  private:
    enventries_t::iterator find(const char *name);
  }; // class env_t

} // namespace LSE_env

