/* -*-c++-*- 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Template file for translation address spaces
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * The class defined by this template represents an address space that
 * translates to another address space.  This space can be used for 
 * VA to PA translation.
 * 
 * The pagebits indicates the granularity at which mappings are to be
 * maintained.
 *
 * Notes:
 *  1) addchild and delchild are passed through to the other space, so these
 *     addresses are *not* translated
 *  2) The "demander" is responsible for allocating pages when needed
 *  
 */

#ifndef _LSE_TRANSLATION_H_
#define _LSE_TRANSLATION_H_

#include <LSE_device.h>
#include <map>
#include <set>
#include <cstring>

namespace LSE_device {

  template<unsigned int pagebits, class D>
    class translation : public device_t {

  public:
    typedef devaddr_t addr_t; 
    typedef LSE_device::devdata_t devdata_t;

    device_t *subspace;

  protected:

    D *demander;

    typedef translation<pagebits, D> thismemory_t;

    static const addr_t pagesize = addr_t(1) << pagebits;
    static const addr_t pagemask = ~(pagesize-1);

     //////////
     // Constructor
     //////////

    public:
    explicit translation(D *d) : demander(d) {
      char crazy[40];
      snprintf(crazy, 39, "translation<%d,D>", pagebits);
      deviceType = crazy;
    } // translation()

    explicit translation(uint64_t oid, D *d) : device_t(oid), demander(d) {
      char crazy[40];
      snprintf(crazy, 39, "translation<%d,D>", pagebits);
      deviceType = crazy;
    } // translation()

     //////////
     // Destructor
     //////////

     ~translation() {
     } // nothing special here


     //////////
     // Add a mapping
     //////////

     void addchild(const devaddr_t addr, const devaddr_t len, device_t *child) {
       subspace->addchild(addr, len, child); 
    }

     //////////// 
     // Delete a mapping
     /////////////

     void delchild(devaddr_t addr, devaddr_t len) {
       subspace->delchild(addr, len);
     } // delchild


     ///////////////
     // Translation
     ///////////////

  public:
     inline devaddr_t translate(devaddr_t addr, device_t **devpp,
				devtptr_t *datapp) {
       devaddr_t pbase = addr & pagemask;
       devaddr_t nb = lookup(addr);
       return std::min(pagesize - addr - pbase,
		       subspace->translate(nb, devpp, datapp));
     } // translate
	 
     inline devaddr_t translate(devaddr_t addr, devdata_t **datapp) {
       device_t *devp;
       devtptr_t tp;
       devaddr_t ra = translate(addr, &devp, &tp);
       if (devp) throw deverror_t(deverror_illegalop);
       *datapp = tp.hp;
       return ra;
     } // translate


    /////////////////////////
    // Mapping control
    ////////////////////

    // the map is visible to the outside world for now.  Eventually
    // I need to do iterators
    typedef std::map<devaddr_t, devaddr_t>::iterator iterator;
    typedef std::map<devaddr_t, devaddr_t>::const_iterator const_iterator;

    std::map<devaddr_t, devaddr_t> mappings;

    void addmap(devaddr_t addr, devaddr_t saddr) {
      mappings[addr] = saddr - addr;
      //std::cerr << "Adding " << std::hex << addr << " -> " << saddr 
      //<< std::dec << "\n";
    }

    void delmap(devaddr_t addr) {
      //std::cerr << "Removing " << std::hex << addr << " -> " 
      //	<< (mappings[addr] + addr)
      //	<< std::dec << "\n";
      mappings.erase(addr);
    }

    devaddr_t lookup(devaddr_t addr, bool doallocate=true) {
      devaddr_t pbase = addr & pagemask;
      iterator fi = mappings.find(pbase);
      if (fi != mappings.end()) return fi->second + addr;
      else if (doallocate) return demander->demand(pbase) + addr;
      else return 0;
    }

    iterator begin() { return mappings.begin(); }
    const_iterator begin() const { return mappings.begin(); }

    iterator end() { return mappings.end(); }
    const_iterator end() const { return mappings.end(); }
          
     /************************* checkpointing **************************/
     // Format for translation body is:
     // transbody := SEQUENCE OF { addr addr ... }

     LSE_chkpt::error_t writeChkptGuts(LSE_chkpt::file_t *cptFile, 
				       bool isIncr) {
       LSE_chkpt::data_t *dp;
       LSE_chkpt::error_t cerr;

       // write out memory contents
       
       dp = LSE_chkpt::build_header(0, LSE_chkpt::TAG_SEQUENCE, 
				    LSE_chkpt::CONSTRUCTED, -1);
       if ((cerr = cptFile->write_to_segment(true, dp))) return cerr; 
       
       for (iterator i = begin(), ie = end(); i != ie ; ++i) {
	 
	 dp = LSE_chkpt::build_unsigned(0, i->first);
	 if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
	 
	 dp = LSE_chkpt::build_unsigned(0, i->second);
	 if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
       }

       dp = LSE_chkpt::build_indefinite_end(0);
       return cptFile->write_to_segment(true,dp);
     }

     LSE_chkpt::error_t readChkptGuts(LSE_chkpt::file_t *cptFile, 
				      bool isIncr) {
       LSE_chkpt::error_t cerr;
       LSE_chkpt::data_t *dp;
       
       // SEQUENCE header
       if ((cerr = cptFile->read_taglen_from_segment(0, &dp))) return cerr;
       delete dp;
       
       mappings.clear();

       do {  // just suck up everything in the indefinite section for now
	 if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
	 if (!dp) break;

	 devaddr_t addr = dp->content.uint64Val;
	 delete dp;

	 if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;

	 devaddr_t paddr = dp->content.uint64Val;
	 delete dp;

	 mappings[addr] = paddr;
       } while(1);

       return LSE_chkpt::error_None;
     } // readChkptGuts

  }; // template translation

}; // namespace LSE_device

#endif /* _LSE_TRANSLATION_H */

