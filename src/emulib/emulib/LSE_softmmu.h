/* -*-c++-*- 
 * Copyright (c) 2010-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
#ifndef _LSE_SOFTMMU_H_
#define _LSE_SOFTMMU_H_
#include <map>
#include <iostream>

namespace LSE_softmmu {

  template <class A, class H, class E, // so we do not have to get order right
    unsigned int nummodes, unsigned int pagebits, unsigned int mmubits,
    unsigned int simplesize> 
    class softMMU {
  public:
    struct entry {
      A va[3];
      E extra; // used for holding physical addresses
      uint64_t offset;
    };
    static const int mmusize = 1<<mmubits;
    static const int nModes = nummodes;

    entry entries[nummodes][1<<mmubits];
    enum kinds {
      Ifetch = 0,
      Load = 1,
      Store = 2
    };
    static const uint64_t INVALID_ENTRY = simplesize;

    static const uint64_t pmask = ~((uint64_t(1) << pagebits)-1);
    static inline uint64_t maskVA(A va) { return va & pmask; }
    static inline uint64_t pageno(A va) { return va >> pagebits; }

    // This method handles the cases where the access was unaligned,
    // the translation is not present, or there is a hook on the access
    void *slow_translate(A va, int mode, int sz, kinds kind, const H& handler);

    // This method is the fast path of translation and returns a host address
    // when there is a hit in the softMMU and the access is aligned and there
    // was no hook.
    inline void *translate(A va, int mode, int sz,
			   kinds kind, const H& handler) {
      uint64_t pga = va & pmask;
      uint64_t pn = pga >> pagebits;
      entry &ent = entries[mode][pn & ((1<<mmubits)-1)];

      if (ent.va[kind] == (va & (pmask | (sz-1)))) {
	handler.hit(va, mode, kind, ent.extra);
	return reinterpret_cast<void *>(ent.offset + va);
      } else return slow_translate(va, mode, sz, kind, handler);
    }

    inline void clear(const H& handler) {
      for (int j = 0; j < nummodes; ++j) 
	for(int i = 0; i < (1 << mmubits); ++i) {
	  entries[j][i].va[0] = entries[j][i].va[1] = entries[j][i].va[2] = -1;
	}
      handler.clear();
    }

  };

  template <class A, class H, class E, unsigned int nummodes, 
    unsigned int pagebits, unsigned int mmubits, unsigned int simplesize> 
    void *softMMU<A,H,E,nummodes,pagebits,mmubits,simplesize>
    ::slow_translate(A va, int mode, int sz, kinds kind, const H& handler) {

      if (va & (sz - 1)) { 
	// ensure that both ends of an unaligned access are mapped.  Want
	// to be a bit clever so we only hit the pages that are needed
	A bytestogo = std::min(A(sz),(1 << pagebits) - (va & ~pmask));
	while (bytestogo) {
	  slow_translate(va, mode, 1, kind, handler);
	  sz -= bytestogo;
	  va += bytestogo;
	  bytestogo = std::min(A(sz),(1 << pagebits) - (va & ~pmask));
	}
	return 0; // do not use the soft MMU
      }

      uint64_t pga = va & pmask;
      uint64_t pn = pga >> pagebits;
      entry &ent = entries[mode][pn & ((1<<mmubits)-1)];

    tryagain:
      if (pga == (ent.va[kind] & (pmask | INVALID_ENTRY))) { 

	// Are there any hooks?  If not, you are done.
	if (!(ent.va[kind] & ~pmask)) { 
	  handler.hit(va, mode, kind, ent.extra);
	  return reinterpret_cast<void *>(ent.offset + va);
	}

	return handler.dohooks(va, mode, sz, kind, ent);
      } // matching valid entry

      // miss handler will install the entry, leaving the ha in the
      // offset.  If the entry is not valid for the kind of access
      // we are attempting to do, then miss *must* return false to 
      // avoid creating an infinite loop, mark the entry as invalid for
      // the access, and set ent.offset so that ha points somewhere non-null 
      if (handler.miss(va, mode, kind, ent)) goto tryagain;
      else return reinterpret_cast<void *>(ent.offset + va);
  } // slow_translate

} // namespace LSE_softmmu
#endif // _LSE_SOFTMMU_H_

