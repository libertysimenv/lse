/* 
 * Copyright (c) 2009-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Emulator domain class header file.
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * This file defines all the class types/functions which don't depend on
 * per-instance types.
 *
 */
#ifndef _LSE_EMU_H_
#define _LSE_EMU_H_

#include <LSE_inttypes.h>
#include <LSE_chkpt.h>
#include <stdio.h>

namespace LSE_emu {

  typedef int LSE_emu_contextno_t;

  typedef void * LSE_emu_ctoken_t;

  typedef enum LSE_emu_spacetype_t {
    LSE_emu_spacetype_nil = 0,
    LSE_emu_spacetype_mem = 1,
    LSE_emu_spacetype_reg = 2,
    LSE_emu_spacetype_other = 3,
  } LSE_emu_spacetype_t;

  struct LSE_emu_context_t {
    int emuinstid;
    boolean valid;
    LSE_emu_ctoken_t ctok;
  };

  struct LSE_emu_interface_t {
    int emuinstid;      /* emulator instance number */
    void *etoken;       /* emulator instance token */
  };

    // static so we can reuse this in multiple files, even though it's a C-ism
  static const char *const LSE_emu_context_toc_name="LSE_emu.contexts";

  typedef LSE_emu_contextno_t (*LSE_emu_context_filter_t)
    (LSE_emu_contextno_t, boolean, void *, void *, int *, 
     LSE_emu_contextno_t *);


  extern int LSE_emu_hwcontexts_total;
  extern LSE_emu_context_t *LSE_emu_hwcontexts_table;

  extern void (*map_in_hook)(LSE_emu_contextno_t);
  extern void (*realloc_hook)(LSE_emu_contextno_t);

  LSE_emu_contextno_t LSE_emu_get_contextno(boolean);

  int LSE_emu_update_context_map(LSE_emu_contextno_t, LSE_emu_ctoken_t);

  inline LSE_emu_ctoken_t LSE_emu_get_context_mapping(LSE_emu_contextno_t cno) {
    if (cno > LSE_emu_hwcontexts_total) return 0;
    return LSE_emu_hwcontexts_table[cno].ctok;
  }

  // Internal functions .. users should not call these functions

  int create_context_int(LSE_emu_interface_t *ifc, int instid,
			 LSE_emu_contextno_t cno,
			 int (*cc)(LSE_emu_interface_t *,
				   LSE_emu_ctoken_t *, LSE_emu_contextno_t)
			 );

  int init_int(void);
  int finalize_int(void);


  LSE_chkpt::error_t write_ctable(LSE_emu_interface_t *ifc, 
				  LSE_chkpt::file_t *cptFile);

  LSE_chkpt::error_t read_ctable(LSE_emu_interface_t *ifc, 
				 LSE_chkpt::file_t *cptFile,
				 void (*fixup)(LSE_emu_interface_t *, int,int));

} /* namespace LSE_emu */

extern int LSE_sim_exit_status;
extern int LSE_sim_terminate_now;
extern int LSE_sim_terminate_count;

#endif /* _LSE_EMU_H_ */
