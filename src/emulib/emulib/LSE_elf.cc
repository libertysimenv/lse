/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * ELF support
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file supports the ELF file format
 *
 * Known issues: uses 32-bit OS calls to load a 64-bit binary.  64-bit calls
 *               wouldn't work on a 32-bit platform, but 32-bit calls don't
 *               work if the binary is > 4GB.  But then, the file probably
 *               doesn't exist either.
 *
 */
#include "LSE_elf.h"
#include <LSE_endianness.h>
#include <stdlib.h>
#include <string.h>

namespace LSE_elf {

  /***************** Miscellaneous APIs ****************/

  const char *errstring(error_t errval) {
    switch (errval) {
    case error_none: return "";
    case error_outofmemory : return "Out of host memory";
    case error_badargument : return "Bad argument";
    case error_notimplemented : return "Function not yet implemented";
    case error_fileerror : return "File system error";
    case error_fileheader : return "Unable to read ELF file header";
    case error_notelf: return "Not an ELF file";
    case error_not32bit: return "Not a 64-bit ELF file";
    case error_not64bit: return "Not a 64-bit ELF file";
    case error_notrightsize: return "Not expected-bit(32/64) ELF file";
    case error_badhversion: return "Bad ELF header version";
    case error_notexec: return "Not an executable file";
    case error_badmachinetype: return "Incorrect ELF machine type";
    case error_eof: return "Premature end-of-file";
    case error_pheaders: return "Unable to process ELF pheader";
    case error_sheaders: return "Unable to process ELF sheader";
    default :
      return "Unknown ELF subsystem error";
    }
  }

  /***************** Load APIs **************************/

  static error_t LSE_do_elf64(FILE *pfile, 
			      Elf_mtypecheck mtype,
			      Elf64_Ehdr *topheader,
			      Elf64_pfunc pfunc,
			      Elf64_sfunc sfunc,
			      void *stuff) {
    Elf64_Phdr *proghdrs;
    Elf64_Shdr *secthdrs;
    size_t num_read;
    int retval,i;
    error_t errval=error_none;
    char endianness;
    uint64_t prog_len, minaddr, maxaddr;
    int fixed;
 
    endianness = topheader->e_ident[EI_DATA]==ELFDATA2MSB ? 1 : 0;

    if (topheader->e_ident[EI_VERSION]!=EV_CURRENT) {
      errval = error_badhversion;
      goto exit_pfile;
    }
    if (LSE_end_any2h_s(endianness,topheader->e_type)!=ET_EXEC &&
	LSE_end_any2h_s(endianness,topheader->e_type)!=ET_DYN) {
      errval = error_notexec;
      goto exit_pfile;
    }
    fixed = (LSE_end_any2h_s(endianness,topheader->e_type)==ET_EXEC);
#define FIX_ENDIANNESS(var, type) var = LSE_end_any2h_ ## type(endianness, var)
    /* Fix Endianness of all fields */
    FIX_ENDIANNESS(topheader->e_type, s);
    FIX_ENDIANNESS(topheader->e_machine, s);
    FIX_ENDIANNESS(topheader->e_version, l);


    FIX_ENDIANNESS(topheader->e_entry, ll);
    FIX_ENDIANNESS(topheader->e_phoff, ll);
    FIX_ENDIANNESS(topheader->e_shoff, ll);

    FIX_ENDIANNESS(topheader->e_flags, l);

    FIX_ENDIANNESS(topheader->e_ehsize, s);
    FIX_ENDIANNESS(topheader->e_phentsize, s);
    FIX_ENDIANNESS(topheader->e_phnum, s);
    FIX_ENDIANNESS(topheader->e_shentsize, s);
    FIX_ENDIANNESS(topheader->e_shnum, s);
    FIX_ENDIANNESS(topheader->e_shstrndx, s);

    if (!(*mtype)(topheader->e_machine,stuff)) {
      errval = error_badmachinetype;
      goto exit_pfile;
    }

    /* Load the program headers */
    proghdrs = (Elf64_Phdr *)malloc(sizeof(Elf64_Phdr)*topheader->e_phnum);
    if (proghdrs == NULL) {
      errval = error_outofmemory;
      goto exit_pfile;
    }

    for (i=0;i<topheader->e_phnum;i++) {
      retval = fseek(pfile,
		     (long)(topheader->e_phoff+i*topheader->e_phentsize),
		     SEEK_SET);
      if (retval!=0) {
	errval = error_eof;
	goto exit_pheaders;
      }
      num_read=fread(proghdrs+i,sizeof(Elf64_Phdr),1,pfile);
      /* anything but an EOF is bad if we didn't get the header.  On the 
	 other hand, an EOF is just fine if the real phdr size is less than
	 the structure size. */
      if (num_read != 1 && 
	  !(feof(pfile) && sizeof(Elf64_Phdr)>topheader->e_phentsize)) {
	errval = error_eof;
	goto exit_pheaders;
      }
    }

    /* fix headers first */
    for (i=0;i<topheader->e_phnum;i++) {
      FIX_ENDIANNESS(proghdrs[i].p_type, l);
      FIX_ENDIANNESS(proghdrs[i].p_offset, ll);
      FIX_ENDIANNESS(proghdrs[i].p_vaddr, ll);
      FIX_ENDIANNESS(proghdrs[i].p_paddr, ll);
      FIX_ENDIANNESS(proghdrs[i].p_filesz, ll);
      FIX_ENDIANNESS(proghdrs[i].p_memsz, ll);
      FIX_ENDIANNESS(proghdrs[i].p_flags, l);
      FIX_ENDIANNESS(proghdrs[i].p_align, ll);
    }

    /* Compute the total size of the program */
    maxaddr = 0;
    minaddr = ~maxaddr;
    for (i = 0; i < topheader->e_phnum; i++) {
      if (proghdrs[i].p_type != 1) continue;
      uint64_t new_len = (proghdrs[i].p_vaddr + proghdrs[i].p_memsz);
      if (proghdrs[i].p_vaddr < minaddr) minaddr = proghdrs[i].p_vaddr;
      if (maxaddr < new_len-1) maxaddr = new_len-1;
    }
    prog_len = maxaddr - minaddr + 1;

    /* Iterate on the program headers */
    for (i=0;i<topheader->e_phnum;i++) {
      if (proghdrs[i].p_memsz > 0) {
	retval = fseek(pfile,(long)(proghdrs[i].p_offset),SEEK_SET);
	if (retval != 0) {
	  errval = error_eof;
	  goto exit_pheaders;
	}
	retval = (*pfunc)(i,prog_len,fixed,&proghdrs[i],pfile,stuff,1);
	if (retval) {
	  errval = error_pheaders;
	  goto exit_pheaders;
	}
      }
    } /* for program header entries */

    /* Load the section headers */
    secthdrs = (Elf64_Shdr *)malloc(sizeof(Elf64_Shdr)*topheader->e_shnum);
    if (secthdrs == NULL) {
      errval = error_outofmemory;
      goto exit_pheaders;
    }

    for (i=1;i<topheader->e_shnum;i++) {
      retval = fseek(pfile,
		     (long)(topheader->e_shoff+i*topheader->e_shentsize),
		     SEEK_SET);
      if (retval!=0) {
	errval = error_eof;
	goto exit_sheaders;
      }
      num_read=fread(secthdrs+i,sizeof(Elf64_Shdr),1,pfile);
      /* anything but an EOF is bad if we didn't get the header.  On the 
	 other hand, an EOF is just fine if the real shdr size is less than
	 the structure size. */
      if (num_read != 1 && 
	  !(feof(pfile) && sizeof(Elf64_Shdr)>topheader->e_shentsize)) {
	errval = error_eof;
	goto exit_sheaders;
      }
    }
 
    /* Iterate on the section headers; skip the first one */
    for (i=1;i<topheader->e_shnum;i++) {
      FIX_ENDIANNESS(secthdrs[i].sh_name, l);
      FIX_ENDIANNESS(secthdrs[i].sh_type, l);
      FIX_ENDIANNESS(secthdrs[i].sh_flags, ll);
      FIX_ENDIANNESS(secthdrs[i].sh_addr, ll);
      FIX_ENDIANNESS(secthdrs[i].sh_offset, ll);
      FIX_ENDIANNESS(secthdrs[i].sh_size, ll);
      FIX_ENDIANNESS(secthdrs[i].sh_link, l);
      FIX_ENDIANNESS(secthdrs[i].sh_info, l);
      FIX_ENDIANNESS(secthdrs[i].sh_addralign, ll);
      FIX_ENDIANNESS(secthdrs[i].sh_entsize, ll);

      retval = (*sfunc)(&secthdrs[i],stuff,1);
      if (retval) {
	errval = error_sheaders;
	goto exit_pheaders;
      }

    } /* for section header entries */

    free(proghdrs);
    free(secthdrs);
    fclose(pfile);
    return error_none; 

  exit_sheaders:
    free(secthdrs);
  exit_pheaders:
    free(proghdrs);
  exit_pfile:
    fclose(pfile);
    return errval;
#undef FIX_ENDIANNESS
  }

  static error_t LSE_do_elf32(FILE *pfile,
			      Elf_mtypecheck mtype,
			      Elf32_Ehdr *topheader,
			      Elf32_pfunc pfunc,
			      Elf32_sfunc sfunc,
			      void *stuff) {

    Elf32_Phdr *proghdrs;
    Elf32_Shdr *secthdrs;
    size_t num_read;
    int retval,i;
    error_t errval=error_none;
    char endianness;

    uint32_t prog_len, maxaddr, minaddr;
    int fixed;
 
    endianness = topheader->e_ident[EI_DATA]==ELFDATA2MSB ? 1 : 0;
#define FIX_ENDIANNESS(var, type) var = LSE_end_any2h_ ## type(endianness, var)
    /* Fix Endianness of all fields */
    FIX_ENDIANNESS(topheader->e_type, s);
    FIX_ENDIANNESS(topheader->e_machine, s);
    FIX_ENDIANNESS(topheader->e_version, l);
    FIX_ENDIANNESS(topheader->e_entry, l);
    FIX_ENDIANNESS(topheader->e_phoff, l);
    FIX_ENDIANNESS(topheader->e_shoff, l);
    FIX_ENDIANNESS(topheader->e_flags, l);
    FIX_ENDIANNESS(topheader->e_ehsize, s);
    FIX_ENDIANNESS(topheader->e_phentsize, s);
    FIX_ENDIANNESS(topheader->e_phnum, s);
    FIX_ENDIANNESS(topheader->e_shentsize, s);
    FIX_ENDIANNESS(topheader->e_shnum, s);
    FIX_ENDIANNESS(topheader->e_shstrndx, s);

    if (topheader->e_ident[EI_VERSION]!=EV_CURRENT) {
      errval = error_badhversion;
      goto exit_pfile;
    }
    if (topheader->e_type!=ET_EXEC &&
	topheader->e_type!=ET_DYN) {
      errval = error_notexec;
      goto exit_pfile;
    }
    if (!(*mtype)(topheader->e_machine,stuff)) {
      errval = error_badmachinetype;
      goto exit_pfile;
    }

    fixed = (topheader->e_type==ET_EXEC);

    /* Load the program headers */
    proghdrs = (Elf32_Phdr *)malloc(sizeof(Elf32_Phdr)*topheader->e_phnum);
    if (proghdrs == NULL) {
      errval = error_outofmemory;
      goto exit_pfile;
    }

    for (i=0;i<topheader->e_phnum;i++) {
      retval = fseek(pfile,
		     (long)(topheader->e_phoff+i*topheader->e_phentsize),
		     SEEK_SET);
      if (retval!=0) {
	errval = error_eof;
	goto exit_pheaders;
      }
      num_read=fread(proghdrs+i,sizeof(Elf32_Phdr),1,pfile);
      /* anything but an EOF is bad if we didn't get the header.  On the 
	 other hand, an EOF is just fine if the real phdr size is less than
	 the structure size. */
      if (num_read != 1 && 
	  !(feof(pfile) && sizeof(Elf32_Phdr)>topheader->e_phentsize)) {
	errval = error_eof;
	goto exit_pheaders;
      }
    }

    for (i=0;i<topheader->e_phnum;i++) {
      /* Fix Endianness of all fields */
      FIX_ENDIANNESS(proghdrs[i].p_type, l);
      FIX_ENDIANNESS(proghdrs[i].p_offset, l);
      FIX_ENDIANNESS(proghdrs[i].p_vaddr, l);
      FIX_ENDIANNESS(proghdrs[i].p_paddr, l);
      FIX_ENDIANNESS(proghdrs[i].p_filesz, l);
      FIX_ENDIANNESS(proghdrs[i].p_memsz, l);
      FIX_ENDIANNESS(proghdrs[i].p_flags, l);
      FIX_ENDIANNESS(proghdrs[i].p_align, l);
    }

    /* Compute the total size of the program */
    maxaddr = 0;
    minaddr = ~maxaddr;
    for (i = 0; i < topheader->e_phnum; i++) {
      if (proghdrs[i].p_type != 1) continue;
      uint32_t new_len = (proghdrs[i].p_vaddr + proghdrs[i].p_memsz);
      if (proghdrs[i].p_vaddr < minaddr) minaddr = proghdrs[i].p_vaddr;
      if (maxaddr < new_len-1) maxaddr = new_len-1;
    }
    prog_len = maxaddr - minaddr + 1;

    /* Iterate on the program headers */
    for (i=0;i<topheader->e_phnum;i++) {
      retval = fseek(pfile,(long)(proghdrs[i].p_offset),SEEK_SET);
      if (retval != 0) {
	errval = error_eof;
	goto exit_pheaders;
      }
      retval = (*pfunc)(i,(uint64_t)prog_len,fixed,&proghdrs[i],pfile,stuff,0);
      if (retval) {
	errval = error_pheaders;
	goto exit_pheaders;
      }
    } /* for program header entries */

    /* Load the section headers */
    secthdrs = (Elf32_Shdr *)malloc(sizeof(Elf32_Shdr)*topheader->e_shnum);
    if (secthdrs == NULL) {
      errval = error_outofmemory;
      goto exit_pheaders;
    }

    for (i=1;i<topheader->e_shnum;i++) {
      retval = fseek(pfile,
		     (long)(topheader->e_shoff+i*topheader->e_shentsize),
		     SEEK_SET);
      if (retval!=0) {
	errval = error_eof;
	goto exit_sheaders;
      }
      num_read=fread(secthdrs+i,sizeof(Elf32_Shdr),1,pfile);
      /* anything but an EOF is bad if we didn't get the header.  On the 
	 other hand, an EOF is just fine if the real shdr size is less than
	 the structure size. */
      if (num_read != 1 && 
	  !(feof(pfile) && sizeof(Elf32_Shdr)>topheader->e_shentsize)) {
	errval = error_eof;
	goto exit_sheaders;
      }
    }

    /* Iterate on the section headers; skip the first one */
    for (i=1;i<topheader->e_shnum;i++) {
      /* Fix Endianness of all fields */
      FIX_ENDIANNESS(secthdrs[i].sh_name, l);
      FIX_ENDIANNESS(secthdrs[i].sh_type, l);
      FIX_ENDIANNESS(secthdrs[i].sh_flags, l);
      FIX_ENDIANNESS(secthdrs[i].sh_addr, l);
      FIX_ENDIANNESS(secthdrs[i].sh_offset, l);
      FIX_ENDIANNESS(secthdrs[i].sh_size, l);
      FIX_ENDIANNESS(secthdrs[i].sh_link, l);
      FIX_ENDIANNESS(secthdrs[i].sh_info, l);
      FIX_ENDIANNESS(secthdrs[i].sh_addralign, l);
      FIX_ENDIANNESS(secthdrs[i].sh_entsize, l);

      retval = (*sfunc)(&secthdrs[i],stuff,0);
      if (retval) {
	errval = error_sheaders;
	goto exit_pheaders;
      }

    } /* for section header entries */

    free(proghdrs);
    free(secthdrs);
    fclose(pfile);
    return error_none; 

  exit_sheaders:
    free(secthdrs);
  exit_pheaders:
    free(proghdrs);
  exit_pfile:
    fclose(pfile);
    return errval;
#undef FIX_ENDIANNESS
  }


  error_t load64(const char *fname, 
			 Elf_mtypecheck mtype,
			 Elf64_Ehdr *topheader,
			 Elf64_pfunc pfunc,
			 Elf64_sfunc sfunc,
			 void *stuff) {
    FILE *pfile;
    Elf64_Phdr *proghdrs;
    Elf64_Shdr *secthdrs;
    size_t num_read;
    int retval,i;
    error_t errval=error_none;
    char endianness;
    uint64_t prog_len;
    int fixed;

    pfile = fopen(fname,"r");
    if (pfile==NULL) {
      errval = error_fileerror;
      goto nocleanup;
    }
    /* Read in the file header */
    num_read=fread(topheader,sizeof(*topheader),1,pfile);
    if (num_read==0) {
      errval = error_fileheader;
      goto exit_pfile;
    }

    /* See that this ELF file matches what we understand for this emulator */

    if (memcmp(&topheader->e_ident[0],ELFMAG,SELFMAG)) {
      errval = error_notelf;
      goto exit_pfile;
    }
    if (topheader->e_ident[EI_CLASS]!=ELFCLASS64) {
      errval = error_not64bit;
      goto exit_pfile;
    }
 
    return LSE_do_elf64(pfile, mtype, topheader, pfunc, sfunc, stuff);
  exit_pfile:
    fclose(pfile);
  nocleanup:
    return errval;
  }

  error_t load32(const char *fname, 
		 Elf_mtypecheck mtype,
		 Elf32_Ehdr *topheader,
		 Elf32_pfunc pfunc,
		 Elf32_sfunc sfunc,
		 void *stuff) {
    FILE *pfile;
    Elf32_Phdr *proghdrs;
    Elf32_Shdr *secthdrs;
    size_t num_read;
    int retval,i;
    error_t errval=error_none;
    char endianness;

    uint32_t prog_len;
    int fixed;

    pfile = fopen(fname,"r");
    if (pfile==NULL) {
      errval = error_fileerror;
      goto nocleanup;
    }
    /* Read in the file header */
    num_read=fread(topheader,sizeof(*topheader),1,pfile);
    if (num_read==0) {
      errval = error_fileheader;
      goto exit_pfile;
    }

    /* See that this ELF file matches what we understand for this emulator */

    if (memcmp(&topheader->e_ident[0],ELFMAG,SELFMAG)) {
      errval = error_notelf;
      goto exit_pfile;
    }
    if (topheader->e_ident[EI_CLASS]!=ELFCLASS32) {
      errval = error_not32bit;
      goto exit_pfile;
    }

    return LSE_do_elf32(pfile, mtype, topheader, pfunc, sfunc, stuff); 
  exit_pfile:
    fclose(pfile);
  nocleanup:
    return errval;
  }

  error_t check(const char *fname, 
		Elf_mtypecheck mtype,
		ElfB_Ehdr *topheader,
		unsigned char match) {
    FILE *pfile;
    error_t errval=error_none;
    size_t num_read;

    pfile = fopen(fname,"r");
    if (pfile==NULL) {
      errval = error_fileerror;
      goto nocleanup;
    }
    /* Read in the file header */
    num_read=fread(topheader,sizeof(*topheader),1,pfile);
    if (num_read==0) {
      errval = error_fileheader;
      goto exit_pfile;
    }

    /* See that this ELF file matches what we understand for this emulator */

    if (memcmp(&topheader->h32.e_ident[0],ELFMAG,SELFMAG)) {
      errval = error_notelf;
      goto exit_pfile;
    }
    if (match && topheader->h32.e_ident[EI_CLASS] != match ||
	(topheader->h32.e_ident[EI_CLASS] != ELFCLASS32 && 
	 topheader->h32.e_ident[EI_CLASS] != ELFCLASS64)) {
      errval = error_notrightsize;
      goto exit_pfile;
    }
    // fall-through

  exit_pfile:
    fclose(pfile);
  nocleanup:
    return errval;
  }

  error_t load(const char *fname, 
	       Elf_mtypecheck mtype,
	       ElfB_Ehdr *topheader,
	       unsigned char match,
	       ElfB_pfunc pfunc,
	       ElfB_sfunc sfunc,
	       void *stuff) {
    FILE *pfile;
    error_t errval=error_none;
    size_t num_read;

    pfile = fopen(fname,"r");
    if (pfile==NULL) {
      errval = error_fileerror;
      goto nocleanup;
    }
    /* Read in the file header */
    num_read=fread(topheader,sizeof(*topheader),1,pfile);
    if (num_read==0) {
      errval = error_fileheader;
      goto exit_pfile;
    }

    /* See that this ELF file matches what we understand for this emulator */

    if (memcmp(&topheader->h32.e_ident[0],ELFMAG,SELFMAG)) {
      errval = error_notelf;
      goto exit_pfile;
    }
    if (match && topheader->h32.e_ident[EI_CLASS] != match ||
	(topheader->h32.e_ident[EI_CLASS] != ELFCLASS32 && 
	 topheader->h32.e_ident[EI_CLASS] != ELFCLASS64)) {
      errval = error_notrightsize;
      goto exit_pfile;
    }
 
    if (topheader->h32.e_ident[EI_CLASS] == ELFCLASS32) 
      return LSE_do_elf32(pfile, mtype, &topheader->h32, 
			  (Elf32_pfunc)pfunc, 
			  (Elf32_sfunc)sfunc, stuff);
    else return LSE_do_elf64(pfile, mtype, &topheader->h64, 
			     (Elf64_pfunc)pfunc, 
			     (Elf64_sfunc)sfunc, stuff);
  
  exit_pfile:
    fclose(pfile);
  nocleanup:
    return errval;
  }

} // namespace LSE_elf
