/* -*-c++-*- 
 * Copyright (c) 2010-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
#ifndef _LSE_CODECACHE_H_
#define _LSE_CODECACHE_H_
#include <map>
#include <iostream>

namespace LSE_codecache {

  // CP holds the code pointers.  A is the type of the pc/status stuff
  // hsize is the size of the hash table.  A must be hashable by %

  template <class CP, class A, unsigned int hsize=32768> class HashedCodeCache {
  public:

    struct CodeBlock {
      A pc;
      CP code;
      int callcount;
      CodeBlock(A a);
      CodeBlock();
      inline void set_inline_pc(A npc) { }

      struct CodeBlock *next;
    };

    static CodeBlock ZeroBlock;

    class filter_t {
    public:
      virtual bool operator()(A, A) const { return false; }
    };

    class removeHook {
    public:
      virtual void operator()(CodeBlock *) const { }
    };

    unsigned long bytesUsed; // how big is the cache, approximately

  private:
    typedef std::vector<CodeBlock *> cache_t;

    cache_t codecache;
    CodeBlock *freeblocks;

    template<typename F>
    inline void deletelist(CodeBlock *p, F r, bool dodelete) {
      while (p && p != &ZeroBlock) {
	CodeBlock *q = p->next;
	r(p);
	if (dodelete) {
	  delete p;
	  bytesUsed -= sizeof(CodeBlock);
	}
	else {
	  p->next = freeblocks;
	  freeblocks = p;
	}
	p = q;
      }
    }

  public:

    HashedCodeCache() : codecache(hsize, &ZeroBlock), freeblocks(0) {}

    template<typename F>
    inline void reset(bool dodelete, F r) {
      for (int i = 0; i < hsize; ++i) {
	deletelist(codecache[i], r, dodelete);
	codecache[i] = &ZeroBlock; // reset it
      }
    }

    inline void reset(bool dodelete) {
      reset(dodelete, removeHook());
    }

    // NOTE: if you want the hooks to be run before destruction, you must
    // call reset first

    ~HashedCodeCache() {
      reset(true, removeHook());
      deletelist(freeblocks, removeHook(), true);
    }

    inline bool visit(A const &addr, CodeBlock *&lb) {
      unsigned hv = addr % hsize;

      CodeBlock *p = codecache[hv];

      if (p->pc == addr) {
	lb = codecache[hv];
	return true;
      }

      CodeBlock *q = p;
      p = p->next;

      while (p) {
	if (p->pc == addr) { // move to front
	  q->next = p->next;
	  p->next = codecache[hv];
	  codecache[hv] = p;
	  lb = p;
	  return true;
	}
	q = p;
	p = p->next;
      }

      if (freeblocks) {
	p = freeblocks;
	freeblocks = p->next;
	p->pc = addr;
	p->code.clear();
      } else {
	p = new CodeBlock(addr);
	bytesUsed += sizeof(CodeBlock);
      }
      p->next = codecache[hv];
      codecache[hv] = p;
      lb = p;
      return false;
    }

    template<typename F>
    inline void remove(CodeBlock *pd, bool dodelete, F r) {
      unsigned hv = pd->pc % hsize;

      CodeBlock *p = codecache[hv], **q = &codecache[hv];

      while (p) {
	if (p == pd) {
	  *q = p->next;
	  r(p);
	  if (dodelete) {
	    delete p;
	    bytesUsed -= sizeof(CodeBlock);
	  }
	  else {
	    p->next = freeblocks;
	    freeblocks = p;
	  }
	  break;
	} else {
	  q = &p->next;
	  p = p->next;
	}
      }
    } // remove

    inline void remove(CodeBlock *pd, bool dodelete) {
      remove(pd, dodelete, removeHook());
    }
    
    // removes codeblocks matching a filter

    template<typename F, typename G>
    inline void remove_if(A addr,
			  G f, bool dodelete, F r) {
      unsigned hv = addr % hsize;

      CodeBlock *p = codecache[hv], **q = &codecache[hv];

      while (p) {
	if (f(addr, p->pc)) {
	  *q = p->next;
	  r(p);
	  if (dodelete) delete p;
	  else {
	    p->next = freeblocks;
	    freeblocks = p;
	  }
	} else {
	  q = &p->next;
	  p = p->next;
	}
      }
    } // remove

  };

  // very important or I cannot force instantiation easily!
  template<class CP, class A, unsigned int hsize>
      typename HashedCodeCache<CP,A,hsize>::CodeBlock 
    HashedCodeCache<CP,A,hsize>::ZeroBlock;

  template <class CP, class A, unsigned int hsize> 
  HashedCodeCache<CP,A,hsize>::CodeBlock::CodeBlock(const A a) 
    : pc(a), callcount(0), next(0) {
    code.clear();
  }

  template <class CP, class A, unsigned int hsize> 
  HashedCodeCache<CP,A,hsize>::CodeBlock::CodeBlock()  
    : pc(A()), callcount(0), next(0) {
    code.clear();
  }

} // namespace LSE_codecache.h
#endif // _LSE_CODECACHE_H_

