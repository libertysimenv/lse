/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Support code for stand-alone emulator and test programs
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This code pretends to be a Liberty simulator.  This code is
 * not compiled and placed into a library; rather, it should be
 * #included into one and only one file in the stand-alone binary
 *
 */
#include "LSE_emu_alonesupp.h"

int LSE_sim_exit_status=0;
int LSE_sim_terminate_count=0;
LSE_time_t LSE_time_now;

LSE_emu_interface_t LSE_emu_interface = {
  1, (void *)(0)
};

namespace LSE_emu {

LSE_emu_iaddr_t *LSE_emu_addr_table=NULL;
void alonesupp_realloc(LSE_emu_contextno_t n) {
  LSE_emu_addr_table = (LSE_emu_iaddr_t *)realloc(LSE_emu_addr_table,
						 sizeof(LSE_emu_iaddr_t) * n);
}

void alonesupp_map_in(LSE_emu_contextno_t c) {
  LSE_emu_addr_table[c]	= EMU_get_start_addr(LSE_emu_hwcontexts_table[c].ctok);
}

/**************** additional useful simulator stuff ************/

int 
LSE_emu_create_context(LSE_emu_contextno_t cno) 
{
  return LSE_emu::create_context_int(&LSE_emu_interface, 1, cno,
				     EMU_context_create);
}

/* and an initialization function */

void LSE_emu_standalone_init(void) {
  LSE_emu::init_int();
  realloc_hook = alonesupp_realloc;
  map_in_hook = alonesupp_map_in;
  if (*realloc_hook) (*realloc_hook)(1);
}

void LSE_emu_standalone_finalize(void) {
  if (LSE_emu_addr_table) free(LSE_emu_addr_table);
  LSE_emu_addr_table = 0;
  LSE_emu::finalize_int();
}

}
