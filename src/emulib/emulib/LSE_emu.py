# /* 
#  * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python emulator domain class
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Description of the emulator domain class
#  * 
#  *
#  */
import os, re, string, types, LSE_domain, sys, getopt, LSE_hashwrap, filecmp, copy

searchpath = ["."]
try:
    searchpath.extend(string.split(os.environ["LSE_LIB_PATH"],":"))
except:
    pass
try:
    searchpath.extend(string.split(os.environ["LIBERTY_SIM_LIB_PATH"],":"))
except:
    pass

# no need to check; the environment variable is checked by all calling scripts
searchpath.append(os.path.dirname(LSE_domain.__file__)+"/LSE_emu")

LSE=os.path.dirname(os.path.dirname(os.path.dirname(LSE_domain.__file__)))

if LSE_domain.inCodeGen:
    from SIM_apidefs import *  # routines useful for parsing arguments
    from SIM_codegen import *  # imports all we need to do code generation

LSE_emu_currinst = None

#############################################
#          Emulator support                 #
#############################################

LSE_emu_spacetype_nil = 0
LSE_emu_spacetype_mem = 1
LSE_emu_spacetype_reg = 2
LSE_emu_spacetype_other = 3

# known attributes and default values; None indicates required attribute
knownattrs={
  "addedfields" : [],                # fields to add directly to instr_info_t
  "addrtype" : None,                 # C datatype of target addresses
  "addrtype_print_format" : None,    # C printf format for target addresses
  "iaddrtype" : "",                  # C datatype of target ifetch info
  "iaddr_true_addr" : "",            # C datatype of target ifetch info
  "capabilities" : [],               # capabilities (determines interface)
  "tcapabilities" : [],              # capabilities whose types should also
                                     # be declared
  "compileFlags" : "",               # compilation flags (generally includes)
  "compiled" : None,                 # compiled-code emulation?
  "extrafields" : "",                # extra fields
  "extrafuncs" : [],                 # extra backend functions/ids
  "extraids" : [],                   # extra identifiers in general (types)
  "headers" : [],                    # implementation header
  "useHeaders" : [],                 # headers needed by the implementation
  "iclasses"   : [],                 # instruction classes
  "libraries" : "",                  # libraries in which emulator is found
  "name" : None,                     # name of emulator
  "predecodefields" : [],            # fields in predecode structure
  "privatefields" : "",              # private fields
  "statespaces" : [],                # state spaces
  "step_names" : None,               # instruction execution steps
  "multicontext" : 0,                # does it support multiple contexts?
  "requiresDomains" : [],            # what domains might it require?
  "namespaces" : [],                 # instance namespaces
  "speculationFlags" : 0,            # how does speculation work?
                                     # 1=commit/2=dynid stuff
  "useLIS" : 0,                      # generate via LIS
  }

# capabilities that state spaces can have 
stateSpaceCaps=["access", "shareable"]

# attributes required for each capabilitiy

capattrs={

  # branch information supplied
  "branchinfo"
  : { "max_branch_targets" : None,   # number of possible next instructions
  },

  # checkpoint
  "checkpoint"
  : { "checkpointcontroltype" : None,   # type of extra checkpoint interfaces
  },
  
  # commandline
  "commandline"
  : {},

  # disassemble
  "disassemble"
  : {},

  # emulator provides register operand information
  "operandinfo"
  : { "max_operand_src" : None,             # maximum source operands
      "max_operand_dest" : None,            # maximum destination operands
      "operand_names" : [("LSE_emu_dummy",-1)], # names for operands
      },

  # emulator provides register operand information
  "operandval"
  : { "operandvaltype" : None,        # operand value type
      },

  "reclaiminstr"
  : {},

  # emulator can undo after mis-speculation
  "speculation"
  : {},

  # emulator itself is threadsafe (not yet implemented)
  
  "threadsafe"
  : {},

  "timed"
  : {},
  }

def make_addr_name(s):
    if type(s[2])==types.StringType:
        if (s[2]=='s'):
            return "  char *%s;" % s[0]
        elif (s[2][-1]=='b'):
            bsize = string.atoi(s[2][:-1])
            if (bsize<=32):
                return "  uint32_t %s;" % s[0]
            elif (bsize<=64):
                return "  uint64_t %s;" % s[0]
            else:
                return "  unsigned char %s[%d];" % (s[0], (bsize+7)/bsize)
        elif (s[2][-1]=='c'):
            return "  char %s[%d];" % (s[0],1+string.atoi(s[2][:-1]))
        else:
            raise LSE_domain.LSE_DomainException("'%s' is not a valid size "
                                                 "for a state space\n"
                                                 % s[2])
    else:
        return "  int %s;" % s[0]

def print_fields(flist): # print out structure fields
    s = ""
    for f in flist:
        if len(f) < 4 or f[3]=="":
            s=s+ "  %s %s %s;\n" % (f[0], f[1], f[2])
        else:
            s=s+ "  %s %s %s; \t\t/* %s */\n" % (f[0], f[1], f[2], f[3])
    return s

def cname(tinfo,args,name,dinfo,extraargs,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    newname = "EMU" + name[7:]
    if extraargs[1]==0:
        if (len(args)!=2 or len(args[1].contents)):
            raise LSEtk_TokenizeException(name + " should have an empty"
                                          " argument list",tinfo[0])
    else:
        if (len(args) != extraargs[1]+1):
            LSEap_report_badargs(name,extraargs[1],tinfo[0])

         
    # figure out the domain class/instance from the way the name was mangled...
    (dc,di) = dinfo
    uname = "::" + LSEcg_domain_namespace_mangler(di,0) + "::" + newname
        
    if (len(args)>1):
        parmtok = LSEtk_TokenPList(
            reduce(lambda x,y:x+[LSEtk_TokenOther(","),y],
                   args[2:],[args[1]]))
        return [ LSEtk_TokenIdentifier(uname),
                 parmtok ]
    else:
        return [ LSEtk_TokenIdentifier(uname) ]


defaultClassCode = r'''
// These are placed in the code because I don't want them in the header
// files, since they are not supposed to be user-accessible.

inline int calc_bits(int size) {
  int i=0;
  if (size==1) i=1;
  size--;
  while (size>0) { i++; size = size >> 1; }
  return i;
}

int LSE_domain_hook(init)(void) {
  return LSE_emu::init_int();
}

int LSE_domain_hook(finalize)(void) {
  return LSE_emu::finalize_int();
}

static int context_load_no;
int LSE_domain_hook(start)(void) {
  // TODO: do we need 
  int i;
  /* clear out contexts table... cannot just clear the total variables
   * because they are used in keeping track of how much table is
   * allocated.
   */
  context_load_no = 0;
  for (i=1;i<=LSE_emu_hwcontexts_total;i++) 
     LSE_emu_hwcontexts_table[i].valid = FALSE;
  return 0;
}

static int argcSave;
static char **argvSave;
static char **envpSave;
static char *emptyEnv[]={0};

int LSE_domain_hook(parse_leftovers)(int argc, char *argv[], char **envp) {
  int i;

  /* all emulators will share the arguments; they need to not modify them */

  if (m4_python(len(filter(lambda x:not x.noInitialContexts,
	                   LSE_db.domains["LSE_emu"][1]))) && !argc) {
    fprintf(stderr,
            "LSE: A program name is required when there are "
	    "emulators with initial contexts\n");
    return -1;
  }

  /* Save the command line and environment */

  argcSave = argc;
  if (argc > 0) {
    argvSave = (char **)malloc(argc*sizeof(char *));
  } else argvSave = NULL;
  for (i=0;i<argc;i++) {
    argvSave[i] = (char *)malloc(strlen(argv[i])+1);
    strcpy(argvSave[i],argv[i]);
  }
  if (envp != NULL) {
    for (i=0;envp[i];i++);
    envpSave = (char **)malloc((i+1)*sizeof(char *));
    for (i=0;envp[i];i++) {
      envpSave[i] = (char *)malloc(strlen(envp[i])+1);
      strcpy(envpSave[i],envp[i]);
    }
    envpSave[i] = 0;
  } else {
    envpSave = emptyEnv;
  }
  return argc; /* eat all arguments */
}

m4_python(if 1:
 hasreclaim = 0
 for i in LSE_db.domains["LSE_emu"][1]:
    if "reclaiminstr" in i.capabilities: hasreclaim = 1
)

m4_python(if hasreclaim:
  print """void LSE_domain_hook(dynid_allocate)(LSE_dynid_t t) {
  t->attrs.LSE_domain_class_name.emuinstid = -1;
}"""
)

m4_python(if hasreclaim:
  print """void LSE_domain_hook(dynid_reclaim)(LSE_dynid_t t) {
  switch(t->attrs.LSE_domain_class_name.emuinstid) {"""
  if 1:
   for i in LSE_db.domains["LSE_emu"][1]:
      if "reclaiminstr" in i.capabilities:
        print "  case %d: %s(&t->attrs.%s.instr_info); break;" % \
	  (i.instId, "EMU_reclaim_instr", i.instName)
  print """
    default: break;
  }
  t->attrs.LSE_domain_class_name.emuinstid = -1;
}
"""
)
'''

class LSE_DomainObject(LSE_domain.LSE_BaseDomainObject):

    className = "LSE_emu"
    
    #classRequiresDomains = [ "LSE_chkpt", "LSE_clock" ]
    classRequiresDomains = [ "LSE_chkpt" ]
   
    classHeaders = [ "LSE_emu.h" ]

    classLibraries = "-lsimEMULSUPP"

    classIdentifiers = [
        # all of these are defined in LSE_emu.h, but I want them to work 
        # without a namespace required.
        ("LSE_emu_ctoken_t",            LSE_domain.LSE_domainID_type, None),
        ("LSE_emu_spacetype_t",         LSE_domain.LSE_domainID_type, None),
        ("LSE_emu_spacetype_nil",       LSE_domain.LSE_domainID_const, None),
        ("LSE_emu_spacetype_mem",       LSE_domain.LSE_domainID_const, None),
        ("LSE_emu_spacetype_reg",       LSE_domain.LSE_domainID_const, None),
        ("LSE_emu_spacetype_other",     LSE_domain.LSE_domainID_const, None),
        ("LSE_emu_context_t",           LSE_domain.LSE_domainID_type, None),
        ("LSE_emu_interface_t",         LSE_domain.LSE_domainID_type, None),
        ("LSE_emu_hwcontexts_table",    LSE_domain.LSE_domainID_var, None),
        ("LSE_emu_hwcontexts_total",    LSE_domain.LSE_domainID_var, None),
        ("LSE_emu_get_contextno",       LSE_domain.LSE_domainID_func, None),
        ("LSE_emu_update_context_map",  LSE_domain.LSE_domainID_func, None),
        ("LSE_emu_get_context_mapping", LSE_domain.LSE_domainID_func, None),
        ]

    classNamespaces = [ "LSE_emu" ]

    # Hooks.....

    classHooks = [
      "init",
      "parse_leftovers",
      "start",
      "finalize",
      ]

    instHooks = [
      "dynid_dump",
      "init",
      "finish",
      "start",
    ]

    instIdentifiers = [
      ("LSE_emu_call_extra_func",        LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_create_context",          LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_disassemble",             LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_disassemble_addr",        LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_do_instrstep",            LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_doback",                  LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_docommit",                LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_dofront",                 LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_fetch_operand",           LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_fetch_remaining_operands",LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_get_start_addr",          LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_get_statespace_bitsize",  LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_get_statespace_name",     LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_get_statespace_size",     LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_get_statespace_type",     LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_get_statespace_width",    LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_has_capability",        LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_has_instr_class",       LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_load_context",            LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_spaceref_equ",          LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_spaceref_is_constant",  LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_spaceref_to_int",       LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_statespace_has_capability", LSE_domain.LSE_domainID_m4macro,
       None),
      ("LSE_emu_init_instr",            LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_backup_operand",        LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_resolve_dynid",          LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_resolve_operand",        LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_rollback_dynid",        LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_rollback_operand",        LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_rollback_resolution",   LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_set_start_addr",        LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_writeback_operand",     LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_writeback_remaining_operands",LSE_domain.LSE_domainID_func,
       None),
      ("LSE_emu_instr_info_get",        LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_instr_info_set",        LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_instr_info_is",         LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_dynid_is",              LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_dynid_get",             LSE_domain.LSE_domainID_m4macro, None),
      ("LSE_emu_dynid_set",             LSE_domain.LSE_domainID_m4macro, None),

      ("LSE_emu_chkpt_add_toc",         LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_chkpt_write_segment",   LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_chkpt_check_toc",       LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_chkpt_read_segment",    LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_chkpt_end_replay",      LSE_domain.LSE_domainID_func, None),

      ("LSE_emu_register_clock",        LSE_domain.LSE_domainID_func, None),

      # access stuff
      ("LSE_emu_space_read",            LSE_domain.LSE_domainID_func, None),
      ("LSE_emu_space_write",            LSE_domain.LSE_domainID_func, None),

      ("LSE_emu_interface",             LSE_domain.LSE_domainID_var, None),
      ]

    implIdentifiers = []

    classCodeText = defaultClassCode

    changesTerminateCount = 1

    # Note: the dynid attributes will be created by the object
    baseAttributes = {
        "LSE_resolution_t" : "struct { LSE_emu_iaddr_t next_addr; }"
        }

    def check_attr_value(self, attr, filedict):
        try:
            (name, dvalue) = attr
            nvalue = filedict[name]
            self.__dict__[name] = nvalue
            if dvalue == None and nvalue==None: return 1
            return 0
        except:
            if dvalue == None:
                sys.stderr.write("Must have value for '%s' "
                                 "attribute\n" % name)
                return 1
            else:
                self.__dict__[name] = dvalue
                return 0


    def __init__(self, instname, buildArgs, runArgs, buildPath):

        LSE_domain.LSE_BaseDomainObject.__init__(self, instname,buildArgs,
                                                 runArgs, buildPath)

        self.buildArgs = buildArgs
        self.runArgs = runArgs

        self.instIdentifiers.extend(LSE_DomainObject.instIdentifiers)
        self.implIdentifiers.extend(LSE_DomainObject.implIdentifiers)

        self.instMacroText += defaultInstMacro
        self.instHeaderText += defaultInstHeader
        self.instCodeText += defaultInstCode

        # parse the build arguments...
        buildArgsList = string.split(buildArgs)
        if not len(buildArgsList):
            raise LSE_domain.\
                  LSE_DomainException("Missing emulator implementation name " \
                                      "for instance '%s'" % instname)
        
        implfile = buildArgsList[0]

        try:
            opts, args = getopt.getopt(buildArgsList[1:], "",
                                      ["noinitialcontexts",
                                       "initialcontexts",
                                       ])
        except getopt.GetoptError, (msg,opt):
                s = "Error %s in domain build arguments for instance '%s'" % \
                    (s,msg)
                raise LSE_domain.LSE_DomainException(s)

        self.noInitialContexts = 0
        for opt, arg in opts:
            if opt == "--noinitialcontexts":
                self.noInitialContexts = 1
            elif opt == "--initialcontexts":
                self.noInitialContexts = 0
        
        self.implName = os.path.basename(implfile)
        self.implName = os.path.splitext(self.implName)[0]

        ## Find the filename

        pimplname = implfile + ".dsc"

        sys.stderr.write("LSE_emu domain searching for implementation %s\n" 
                         % pimplname)
        if os.access(pimplname, os.R_OK): # local file
            if pimplname[0] == "/": dfname = pimplname
            else: dfname = os.path.abspath(os.getcwd() + "/" + pimplname)
        else: # need to search
            for dd in searchpath:
                dfname = dd + "/" + pimplname
                if os.access(dfname, os.R_OK): break
            else:
                raise LSE_domain.LSE_DomainException(\
              "Unable to open emulator implementation description file '%s'" \
                    % pimplname)

        self.implLibPath.append(os.path.dirname(os.path.abspath(dfname)))

        ## Read in the description file

        ## Initialize file dictionary before reading description file;
        ## this allows us to append to lists reliably in description files
        
        filedict = {}
        for e in knownattrs.items():
            if e[1] is not None:
                filedict[e[0]] = copy.copy(e[1])


        global LSE_emu_currinst
        LSE_emu_currinst = self
        try:
            execfile(dfname,globals(),filedict)
        except SystemExit:
            pass

        if (filedict.get("useLIS",0)):
            # we're going to generate the text using LIS
            self.generated = 1

            LIStext = filedict.get("LIStext","")
            
            # make a directory for it and move down there
            if not os.access(buildPath,os.F_OK):
                os.makedirs(buildPath)
            savepath = os.getcwd()
            os.chdir(buildPath)

            # have tarballs changed?
            try:
                fp = open("LSE_checksum")
                checksum = fp.read()
                fp.close()
            except:
                self.changed = "previous tarball checksum not found"
                
            tarfile = os.path.splitext(os.path.abspath(dfname))[0]+".tar"
            try:
                fp = open(tarfile)
                fbytes = fp.read()
                hash = LSE_hashwrap(fbytes)
                fp.close()
            except:
                hash = ""

            if not LSE_domain.inCodeGen:
                self.changed = "in ls-make-domain-header"

            if not self.changed and hash != checksum:
                self.changed = "tarball changed"
                #sys.stderr.write("%s\n%s\n" % (hash, checksum))
            
            # See whether top script changed
            if not self.changed:
                try:
                    fp = open("LSEemu.lis")
                    fbytes = fp.read()
                    if fbytes != LIStext:
                        self.changed = "build arguments changed"
                except:
                    self.changed = "previous build arguments not found"
                        
            saveName = filedict.get("name",None)
            if LSE_domain.inCodeGen:
                mangledName = self.instName + "__" + self.implName
            else:
                mangledName = self.instName

            if self.changed:
                sys.stderr.write("le-domain-build=> " 
                                 "Generating emulator '%s' using LIS "
                                 "because %s\n"
                                 % (self.instName, self.changed))

                # wipe out directory contents
                os.system("/bin/rm -fr *")
                
                # detar files
                os.system("tar xvf " + tarfile + " > /dev/null")

                # write top script
                lf = open("LSEemu.lis","w")
                lf.write(LIStext)
                lf.close()

                # And always say that we will need the header

                # execute LIS
                cmdstring = ("le-genemu --namespace=%s --dheader=ISA.h "
                             "%s LSEemu.lis") % (mangledName, mangledName)
                sys.stderr.write("le-domain-build=> %s\n" % cmdstring);
                rstat = os.system(cmdstring)

                if rstat:
                    # remove the LSEemu.lis and LSEemu.sav
                    raise LSE_domain.LSE_DomainException("Error generating "
                                                         "emulator from LIS "
                                                         "files")
            
                # and remember the save file and checksum
                lf = open("LSE_checksum","w")
                lf.write(hash)
                lf.close()

                # move any makefile we find

                if os.access(self.implName +".mk", os.R_OK): # local file
                    os.system("/bin/mv -f " + self.implName + ".mk Makefile");

                # and update the library thing
                if not LSE_domain.inCodeGen:
                    lf = open(mangledName+".dsc", "a")
                    lf.write("""libraries += " " + %s\n""" %
                             repr(filedict["libraries"]))
                    lf.close()

            # read the results
            try:
                execfile("./%s.dsc" % mangledName, globals(), filedict)
                filedict["name"] = mangledName
                self.implName = mangledName
            except:
                os.chdir(savepath) 
                raise
            
            os.chdir(savepath) 
            self.implLibPath[0:0] = [buildPath]

            # write makefile pieces after reading the new description file 
            # so that the description file can affect the makefile through the
            # compiler flags (this is useful for binary translators which
            # need to pull in specific header files

            if self.changed:
                if not LSE_domain.inCodeGen:
                    # write makefile pieces 
                    lf = open("domain_info.mk","w")
                    lf.write("DOMNAME=%s\n" % mangledName)
                    lf.write("TOPSRCINCDIR=.\n")
                    lf.close

                    lf = open("Make_include.mk","w")
		    lf.write("LSE=%s\n" % LSE)
                    lf.write("""
INCLUDES=-I$(LSE)/include/lse -I$(LSE)/include/domains\n
CCOMP=$(CC) -I. $(INCLUDES) $(CPPFLAGS) $(CFLAGS) %(compileFlags)s

CXXCOMP=$(CXX) -I. $(INCLUDES) $(CPPFLAGS) $(CXXFLAGS) %(compileFlags)s

.c.o:
\t$(CCOMP) -c $<

.cc.o:
\t$(CXXCOMP) -c $<
""" % filedict)
                    lf.close()

        ## Check that description file defined all needed attributes
        
        haderror = 0
        for e in knownattrs.items():
            haderror = self.check_attr_value(e, filedict) or haderror

        haderror = 0

        ## deal with capabilities as part of the checking

        self.tcapabilities += self.capabilities
        
        try: # must put in try because we might be missing attributes

            for c in self.capabilities:
                if not capattrs.has_key(c):
                    sys.stderr.write("Unknown capability '%s'" % c)
                    haderror = 1
                else:
                    for ea in capattrs[c].items():
                        haderror = self.check_attr_value(ea, filedict) \
                                   or haderror

            # and now copy remaining stuff from filedict to self
            for (k,v) in filedict.items():
                if k not in self.__dict__: self.__dict__[k] = v
                    
            if self.name != self.implName and self.implName != None:
                sys.stderr.write(("Emulator name(%s) must match " + 
                                  "name in description file(%s)\n") %
                                 (self.implName,self.name))
                haderror=1

            # sys.stderr.write("%s\n" % self.__dict__)

            # make certain at least sideeffect in instruction classes
            if "sideeffect" not in self.iclasses:
                self.iclasses.append("sideeffect")

            # check for correct combinations of capabilities

            if ("operandval" in self.capabilities and
                "operandinfo" not in self.capabilities):
                sys.stderr.write("The 'operandval' capability requires "
                                 "the 'operandinfo' capability\n")
                haderror = 1

            if ("speculation" in self.capabilities and
                (self.speculationFlags & 1) and
                "operandval" not in self.capabilities):
                sys.stderr.write("The 'speculation' capability requires "
                                 "the 'operandval' capability\n")
                haderror = 1

            # calculate fields
            haderror = self.calculate_fields() or haderror

            if (len(self.step_names) < 1):
                sys.stderr.write("You must provide at least one step name\n")
                haderror = 1

            # check step name stuff
            self.earliest=[100000,100000,100000]
            self.latest=[-1,-1,-1]
            for n in self.step_names:
                if n[2] < 0 or n[2] > 2:
                    sys.stderr.write(("Step name %s is assigned to " +
                                      "an invalid group %d\n") % (n[0],n[2]))
                    haderror = 1
                if n[1] < self.earliest[n[2]]: self.earliest[n[2]] = n[1]
                if n[1] > self.latest[n[2]]: self.latest[n[2]] = n[1]

            if (self.latest[0] >= self.earliest[1] or
                self.latest[1] >= self.earliest[2]):
                sys.stderr.write("Step assignments to groups are invalid\n")
                haderror = 1

            self.implCompileFlags = self.compileFlags
            self.implHeaders.extend(self.headers)
            self.implUseHeaders.extend(self.useHeaders)
            self.implRequiresDomains.extend(self.requiresDomains)
            self.implNamespaces.extend(self.namespaces)
            self.implLibraries = self.libraries

            if not self.namespaces: self.namespaces = [ self.implName ]
            if ("timed" in self.capabilities):
                self.implRequiresDomains.append(("LSE_clock",'',''))

        except AttributeError(e):
            haderror = 1


        actualStateSpaceCaps = {}
        try:
            for s in self.statespaces:
                for c in s[5]:
                    if c not in stateSpaceCaps:
                        sys.stderr.write("Unknown space capability '%s'\n" % c)
                        haderror = 1
                    else:
                        actualStateSpaceCaps[c] = 1
        except AttributeError(e):
            haderror = 1
            
        ############# Put together hooks ###################################

        self.instHooks = LSE_DomainObject.instHooks * 1
        if "commandline" in self.capabilities:
            self.instHooks.extend(["parse_arg", "usage"])

        ############# Put together constants/types/APIs ####################

        ## No instance headers

        ## Base interface constants
        tl = [
            
            ( "LSE_emu_addr_t_print_format", LSE_domain.LSE_domainID_cmacro,
              ("",'"' + self.addrtype_print_format + '"' )),
           
            ( "LSE_emu_max_instrstep",       LSE_domain.LSE_domainID_const,
              1+reduce(max,map(lambda x:x[1],self.step_names))),
           
            ( "LSE_emu_num_statespaces",     LSE_domain.LSE_domainID_const,
              len(self.statespaces)+1 ),
            
            ]

        ## capability "has" macros (constants)

        for c in capattrs.keys():
            tl.append(("LSE_emu_emulator_has_" + c,
                       LSE_domain.LSE_domainID_cmacro,
                       ("",int(c in self.capabilities))))

        ## statespace names (values will be in enumerated type)
        for s in self.statespaces:
            tl.append(("LSE_emu_spaceid_" + s[0],
                       LSE_domain.LSE_domainID_const, None))
        tl.append(("LSE_emu_spaceid_LSE", LSE_domain.LSE_domainID_const, None))

        ## instruction set names (values will be in enumerated type)
        for s in self.step_names:
            tl.append(("LSE_emu_instrstep_name_" + s[0],
                       LSE_domain.LSE_domainID_const, None))

        ## branchinfo constants
        if 1 or "branchinfo" in self.tcapabilities:
            self.max_branch_targets = self.__dict__.get("max_branch_targets",1)
            tl.append(("LSE_emu_max_branch_targets",
                       LSE_domain.LSE_domainID_const,
                       self.max_branch_targets))

        ## operandinfo constants
        if 1 or "operandinfo" in self.tcapabilities:
            self.max_operand_src = self.__dict__.get("max_operand_src",0)
            self.max_operand_dest = self.__dict__.get("max_operand_dest",0)
            self.operand_names = self.__dict__.get("operand_names",[])

            tl.extend([("LSE_emu_max_operand_src",
                        LSE_domain.LSE_domainID_const,
                        self.max_operand_src),
                       ("LSE_emu_max_operand_dest",
                        LSE_domain.LSE_domainID_const,
                        self.max_operand_dest),

                       # memory access flags
                       ("LSE_emu_memaccess_read",
                        LSE_domain.LSE_domainID_const, 1),
                       
                       ("LSE_emu_memaccess_write",
                        LSE_domain.LSE_domainID_const, 2),
                       
                       ("LSE_emu_memaccess_atomic",
                        LSE_domain.LSE_domainID_const, 4),
                       
                       ("LSE_emu_memaccess_noaccess",
                        LSE_domain.LSE_domainID_const, 8),
                       ])

            for n in self.operand_names:
                tl.append(("LSE_emu_operand_name_" + n[0],
                           LSE_domain.LSE_domainID_const,
                           None))

        ## speculation constants
        if 1 or "speculation" in self.tcapabilities:
            # Note: these should match resolveX in LSE_device
            tl.extend([("LSE_emu_resolveFlag_rollback",
                        LSE_domain.LSE_domainID_const, 1),
                       ("LSE_emu_resolveFlag_redo",
                        LSE_domain.LSE_domainID_const, 2),
                       ("LSE_emu_resolveFlag_commit",
                        LSE_domain.LSE_domainID_const, 4)])
            # Note: these should match writeOpX in LSE_device
            tl.extend([("LSE_emu_resolveOp_rollback",
                        LSE_domain.LSE_domainID_const, 1),
                       ("LSE_emu_resolveOp_commit",
                        LSE_domain.LSE_domainID_const, 0),
                       ("LSE_emu_resolveOp_query",
                        LSE_domain.LSE_domainID_const, 2)])
            
        ## base types

        if not self.iaddrtype:
            self.iaddrtype = self.addrtype

        tl.extend([

          ("LSE_emu_addr_t",         LSE_domain.LSE_domainID_type,
           self.addrtype),
          
          ("LSE_emu_iaddr_t",         LSE_domain.LSE_domainID_type,
           self.iaddrtype),

          ("LSE_emu_spaceid_t",      LSE_domain.LSE_domainID_type,
           ("enum ?? {\n" +
            reduce(lambda x,y:x+"  LSE_emu_spaceid_" + y[0] + ",\n",
                   self.statespaces,
                   "LSE_emu_spaceid_LSE = 0,\n")
            + "} ??")),
          
          ("LSE_emu_instrstep_name_t",      LSE_domain.LSE_domainID_type,
           ("enum ?? {\n" + 
            reduce(lambda x,y:x+"  LSE_emu_instrstep_name_" +y[0]+ \
                   ("= %d,\n"%y[1]),
                   self.step_names,
                   "")
            + "} ??")),
          
          ("LSE_emu_spaceaddr_t",     LSE_domain.LSE_domainID_type,
           ("union ??{\n" +
            reduce(lambda x,y:x+make_addr_name(y)+"\n",
                   self.statespaces,
                   "  int LSE;\n")
            + "} ??")),
          
          ])

          
        ## get_true_type
        if not self.iaddr_true_addr: self.iaddr_true_addr = "addr"
        tl.append( ( "LSE_emu_get_true_addr",
                     LSE_domain.LSE_domainID_inlinefunc,
                     "LSE_emu_addr_t ??(const LSE_emu_iaddr_t addr) {\n"
                     "  return %s;\n"
                     "}" % self.iaddr_true_addr) )

        tl.append( ( "LSE_emu_get_true_addr_ref",
                     LSE_domain.LSE_domainID_inlinefunc,
                     "LSE_emu_addr_t &??(LSE_emu_iaddr_t& addr) {\n"
                     "  return %s;\n"
                     "}" % self.iaddr_true_addr) )

        ## instruction class type
        
        tl.append(( "LSE_emu_iclasses_t", LSE_domain.LSE_domainID_type,
                    "struct ?? {\n" + reduce(lambda x,y: \
                                             x+("    boolean is_%s;\n" % y),
                                self.iclasses,"") +
                         "  } ??"))


        ## state space datatypes
        
        tl.extend(map(lambda x:("LSE_emu_space_%s_t" % x[0],
                                LSE_domain.LSE_domainID_type,x[4]),
                      self.statespaces))

        tl.extend([
            ("LSE_emu_spacedata_t",     LSE_domain.LSE_domainID_type,
             ("union ?? {\n" +
              reduce(lambda x,y:x+("  LSE_emu_space_%s_t %s;\n" % (y[0],y[0])),
                     self.statespaces,
                     "  int LSE;\n")
              + "} ??")),
            ])

        ## checkpoint types

        if 1 or "checkpoint" in self.tcapabilities:
            self.checkpointcontroltype = \
                         self.__dict__.get("checkpointcontroltype","void *")
            tl.extend([
                ("LSE_emu_chkpt_cntl_t", LSE_domain.LSE_domainID_type,
                 self.checkpointcontroltype),
                ])

        ## branchinfo types
        if 1 or "branchinfo" in self.tcapabilities:
            tl.append(("LSE_emu_branchtargs_t",
                       LSE_domain.LSE_domainID_type,
                       "LSE_emu_addr_t ??[LSE_emu_max_branch_targets]"));

        ## operandinfo types... define so that accessors can be happy
        if 1 or "operandinfo" in self.tcapabilities:
            maxbits = 0
            for n in self.statespaces:
                if (n[1] == LSE_emu_spacetype_reg or \
                    n[1] == LSE_emu_spacetype_other):
                    if (n[3] > maxbits): maxbits = n[3]
            if maxbits == 0: maxbits = 1

            if (len(self.operand_names)):
                tl.append(("LSE_emu_operand_name_t",
                           LSE_domain.LSE_domainID_type,
                           ("enum ??{\n" + 
                            reduce(lambda x,y:x+"  LSE_emu_operand_name_" + \
                                   y[0] + ("= %d,\n" % y[1]),
                                   self.operand_names,
                                   "")
                            + "} ??")))
            else:
                tl.append(("LSE_emu_operand_name_t",
                           LSE_domain.LSE_domainID_type, "int"))
                    
            tl.extend([
                ("LSE_emu_operand_info_t",   LSE_domain.LSE_domainID_type,
                 """struct ?? {
                    union {
                      struct {
                        uint64_t bits[%d];
                      } reg;
                      struct {
                        int flags;
                        unsigned int size;
                      } mem;
                    } uses;
                    LSE_emu_spaceaddr_t spaceaddr;
                    LSE_emu_spaceid_t spaceid;
                  } ??""" % ((maxbits+63)/64, )),
              ])

        ## operandval types ... always create something for them, too.
        if 1 or "operandval" in self.tcapabilities:
            self.operandvaltype = self.__dict__.get("operandvaltype","void *")
            tl.append(("LSE_emu_operand_data_t", LSE_domain.LSE_domainID_type,
                       self.operandvaltype))
            tl.append(("LSE_emu_operand_val_t",   LSE_domain.LSE_domainID_type,
                       """struct ?? {
                            LSE_emu_operand_data_t data;
                            char valid;
                          } ??"""))

        ## timed types
        if 1 or "timed" in self.tcapabilities:
            tl.append(("LSE_emu_interrupt_callback_t",
                       LSE_domain.LSE_domainID_type,
                       """struct ??{
                            int (*handler)(int, void *);
                            void *data;
                            ??() : handler(0), data(0) {}
                            ??(int (*h)(int, void*), void *d) 
                               : handler(h), data(d) {}
                       } ??"""))

        ## predecode fields
        if (len(self.predecode_fields)):
            tl.append(("LSE_emu_predecode_info_t",LSE_domain.LSE_domainID_type,
                       "struct ?? {\n" + print_fields(self.predecode_fields)+"} ??"))

            
        ## instruction information
        tl.append(("LSE_emu_instr_info_t",    LSE_domain.LSE_domainID_type,
                   "struct ?? {\n" +
                   print_fields(
                           filter(lambda x,y=self.predecodefields:\
                                  x[1] not in y,
                           self.instr_fields))+"} ??"))

        ## extra identifiers
        tl.extend(self.extraids)

        self.implIdentifiers.extend(tl)

        # drop identifier implementations for everything we put in the
        # header file
        if LSE_domain.inCodeGen:
            self.implIdentifiers = LSE_domain.dropDefs(self.implIdentifiers)
            
        self.instIdentifiers.extend(LSE_domain.dropDefs(tl))

        ##########################################################

        self.instAttributes.update(LSE_DomainObject.baseAttributes)

        if ("operandinfo" in self.capabilities):
            self.instAttributes["LSE_dynid_t"] =  ("struct {\n" 
                                "LSE_emu_instr_info_t instr_info;\n"
                                "}")
        else:
            self.instAttributes["LSE_dynid_t"] =  ("struct {\n" 
                            "LSE_emu_instr_info_t instr_info;\n"
                            "}")

        if (haderror): 
            raise LSE_domain.LSE_DomainException("Errors encountered while "
                                                 "creating emulator instance")

        ############# put together backend functions ##############
        tl2 = []
        tl2.extend(apiprotos["core"])
        tl2.extend(self.extrafuncs)

        for c in self.capabilities:
            tl2.extend(apiprotos[c])

        s = actualStateSpaceCaps.keys()
        s.sort()
        for c in s:
            tl2.extend(apiprotos[c])

        def changeit(x):
            return (x[0], LSE_domain.LSE_domainID_func,x[2])

        if LSE_domain.inCodeGen:
            tl2 = [] # don't make backend functions domain-searchable

        self.implIdentifiers.extend(map(changeit, tl2))

        ################### END OF __init__ ######################

    ################### Routine to calculate fields needed ############

    def calculate_fields(emu):

        haderror = 0
        emu.instr_fields = []
        emu.predecode_fields = []

        fields = []

        fields.append(("LSE_emu_iaddr_t", "addr",""))

        if ("branchinfo" in emu.capabilities):
            fields.append(("int", "branch_dir","","which next instr"))
            fields.append(("int", "branch_num_targets","",
                           "number of potential next instructions"))
            fields.append(("LSE_emu_branchtargs_t", "branch_targets","",
                           "addresses of potential next instructions"))
            
        fields.append(("int", "hwcontextno","",
                       "hardware context"))

        fields.append(("LSE_emu_ctoken_t", "swcontexttok","",
                       "token for context"))

        fields.append(("LSE_emu_iclasses_t", "iclasses", "",
                       "instruction classes"))
        
        fields.append(("LSE_emu_iaddr_t","next_pc","","real next PC"))

        if ("operandinfo" in emu.capabilities):
            fields.append(("LSE_emu_operand_info_t", "operand_dest",
                            "[LSE_emu_max_operand_dest]","destination ops"))
            fields.append(("LSE_emu_operand_info_t", "operand_src",
                            "[LSE_emu_max_operand_src]","source ops"))

        if ("operandval" in emu.capabilities):
            fields.append(("LSE_emu_operand_val_t", "operand_val_dest",
                            "[LSE_emu_max_operand_dest]",
                           "destination op vals"))
            fields.append(("LSE_emu_operand_val_t", "operand_val_src",
                            "[LSE_emu_max_operand_src]","source op vals"))
            fields.append(("boolean", "operand_written_dest",
                            "[LSE_emu_max_operand_dest]",
                           "destination op written?"))

        fields.append(("LSE_emu_addr_t", "size",""))

	########################## important ############################
	# Emulators can be written with the assumption that the private,
	# extra, and predecode fields come last.  This allows them to
	# avoid clearing these fields when not necessary.  Do NOT
	# invalidate this assumption!
	#################################################################

        # want this before added fields
        if (len(emu.predecodefields)):
            fields.append(("LSE_emu_predecode_info_t *","pre_info",
                           "","predecoded stuff"))

        if (emu.extrafields and emu.extrafields != ""):
            fields.append((emu.extrafields, "extra","",
                            "emulator-specific fields"))

        if (emu.privatefields and emu.privatefields != ""):
            fields.append((emu.privatefields,"privatef","",
                            "emulator-private fields"))


        # added fields        
        if (len(emu.addedfields)):
            fields.extend(emu.addedfields)
            
        # figure out the predecode fields
        emu.instr_fields = fields
        emu.field_names = fnamelist = map(lambda x:x[1],fields)

        def getf(s,l):
            for f in l:
                if f[1] == s: return f

        for s in emu.predecodefields:
            if (s=="hwcontextno" 
                or s=="swcontexttok" or s=="pre_info"):
                sys.stderr.write("Field '%s' is not allowed as a "
                                 "predecoded field\n"% s)
                haderror = 1
            if s not in fnamelist:
                sys.stderr.write(("Field '%s' cannot be predecoded " +
                                  "because it does not exist\n") % s)
                haderror = 1
            emu.predecode_fields.append(getf(s,fields))

        return haderror

    ################### check the attributes #############################

    def checkAttribute(self,struct,attrname):
        # sys.stderr.write("%s %s\n" % (struct, attrname))
        if struct=="LSE_dynid_t":
            if attrname == "instr_info": return attrname
            if attrname[:11] == "instr_info.":
                nname = attrname[11:]
                nname = re.sub("[[.].*$","",nname)
                if nname in self.predecodefields:
                    return "instr_info.pre_info->" + attrname[11:]
                else: return attrname
            if attrname == "oper_tags" and "operandinfo" in self.capabilities:
                return attrname
            if (attrname[:10] == "oper_tags." and
                  "operandinfo" in self.capabilities): return attrname
        elif struct == "LSE_resolution_t":
            if attrname == "next_addr": return attrname
        return None

    ################### interface prototypes ###############


# Create the merged constants, types, and APIs
def createMergedInfo(self, objlist):
    self.classAttributes = {}
    counter = 1
    for o in objlist:
        o.instIdentifiers.append(("LSE_emu_instid",
                                  LSE_domain.LSE_domainID_const|
                                  LSE_domain.LSE_domainID_inst,
                                  counter))
        o.instId = counter
        counter = counter+1

    self.mergedIdentifiers = []
    hasReclaim = reduce(lambda x,o:x or ("reclaiminstr" in o.capabilities),
                        objlist, 0)
    if hasReclaim:
        self.classHooks.extend(["dynid_reclaim", "dynid_allocate"])
        self.classAttributes["LSE_dynid_t"] = "struct { int emuinstid; }"
        

def checkAttribute(self,struct,attrname):
    # sys.stderr.write("%s %s\n" % (struct, attrname))
    if struct=="LSE_dynid_t":
        if attrname == "emuinstid": return attrname
    return None

################## api prototypes for different capabilities #######

apiprotos={

    "core" : [
    
      ("EMU_context_create", LSE_domain.LSE_domainIDtype_CC,
       "int ??(LSE_emu_interface_t *ifc, LSE_emu_ctoken_t *ctokenp, "
       "int cno)"),
      
      ("EMU_context_load", LSE_domain.LSE_domainIDtype_CC,
       "int ??(LSE_emu_interface_t *ifc, int cno, "
       "int argc, char *argv[], char **envp)"),

      ("EMU_do_step", LSE_domain.LSE_domainIDtype_CC, "void ??("
       "LSE_emu_instr_info_t *ii, "
       "LSE_emu_instrstep_name_t sname, boolean isSpeculative)"),
      
      ("EMU_finish", LSE_domain.LSE_domainIDtype_CC,
       "void ??(LSE_emu_interface_t *ifc)"),
      
      ("EMU_get_start_addr", LSE_domain.LSE_domainIDtype_CC,
       "LSE_emu_iaddr_t ??(LSE_emu_ctoken_t ctoken)"),
      
      ("EMU_get_statespace_size", LSE_domain.LSE_domainIDtype_CC,"int ??("
       "LSE_emu_ctoken_t ctoken, "
       "LSE_emu_spaceid_t sid)"),
      
      ("EMU_init", LSE_domain.LSE_domainIDtype_CC,
       "void ??(LSE_emu_interface_t *ifc)"),
      
      ("EMU_init_instr", LSE_domain.LSE_domainIDtype_CC, 
       "void ??(LSE_emu_instr_info_t *, int c, "
       "LSE_emu_ctoken_t t, const LSE_emu_iaddr_t &a)"),
      
      ("EMU_set_start_addr", LSE_domain.LSE_domainIDtype_CC, "void ??("
       "LSE_emu_ctoken_t ctoken, "
       "LSE_emu_iaddr_t addr)"),
      
      ],

  "access" : [
    ("EMU_space_read", LSE_domain.LSE_domainIDtype_CC, "void ??("
     "LSE_emu_spacedata_t *sdata, "
     "LSE_emu_ctoken_t ctoken, LSE_emu_spaceid_t sid, "
     "LSE_emu_spaceaddr_t *saddr, int flags)"),
    
    ("EMU_space_write", LSE_domain.LSE_domainIDtype_CC, "void ??("
     "LSE_emu_ctoken_t ctoken, LSE_emu_spaceid_t sid, "
     "LSE_emu_spaceaddr_t *saddr, LSE_emu_spacedata_t *sdata, int flags)"),
     
    ],

  "branchinfo" : [],

  "checkpoint" : [
    
    ("EMU_chkpt_add_toc", LSE_domain.LSE_domainIDtype_CC,
     "LSE_chkpt::error_t ??("
     "LSE_emu_interface_t *ifc, "
     "LSE_chkpt::file_t *cptFile, "
     "const char *emuName, "
     "int step, "
     "LSE_emu_chkpt_cntl_t *ctl)"),
    
    ("EMU_chkpt_write_segment", LSE_domain.LSE_domainIDtype_CC,
     "LSE_chkpt::error_t ??("
     "LSE_emu_interface_t *ifc, "
     "LSE_chkpt::file_t *cptFile, "
     "const char *segmentName, "
     "int step, "
     "LSE_emu_chkpt_cntl_t *ctl)"),

    ("EMU_chkpt_check_toc", LSE_domain.LSE_domainIDtype_CC,
     "LSE_chkpt::error_t ??("
     "LSE_emu_interface_t *ifc, "
     "LSE_chkpt::file_t *cptFile, "
     "const char *emuName, "
     "int step, "
     "unsigned int *position, "
     "LSE_emu_chkpt_cntl_t *ctl)"),
    
    ("EMU_chkpt_read_segment", LSE_domain.LSE_domainIDtype_CC,
     "LSE_chkpt::error_t ??("
     "LSE_emu_interface_t *ifc, "
     "LSE_chkpt::file_t *cptFile, "
     "const char *segmentName, "
     "int step, "
     "LSE_emu_chkpt_cntl_t *ctl)"),

    ("EMU_chkpt_end_replay",LSE_domain.LSE_domainIDtype_CC,
     "void ??(LSE_emu_interface_t *ifc)"),
    
    ],
    
  "commandline" : [
    
    ("EMU_parse_arg", LSE_domain.LSE_domainIDtype_CC, "int ??("
     "LSE_emu_interface_t *ifc, "
     "int argc, "
     "char *arg, "
     "char *argv[])"),

    ("EMU_print_usage", LSE_domain.LSE_domainIDtype_CC, 
     "void ??(LSE_emu_interface_t *ifc)"),
    
    ],

  "disassemble" : [

    ( "EMU_disassemble_addr", LSE_domain.LSE_domainIDtype_CC, "void ??("
      "LSE_emu_ctoken_t ctoken, " 
      "LSE_emu_addr_t addr, " 
      "FILE *outfile)"),
    
    ( "EMU_disassemble_instr", LSE_domain.LSE_domainIDtype_CC, "void ??("
      "LSE_emu_instr_info_t *ii, " 
      "FILE *outfile)"),
    
    ],

  "shareable" : [
    ("EMU_space_get_accessor", LSE_domain.LSE_domainIDtype_CC,
     "void * ??("
     "LSE_emu_ctoken_t ctoken, "
     "LSE_emu_spaceid_t sid)"),

    ("EMU_space_set_accessor", LSE_domain.LSE_domainIDtype_CC,
     "void ??("
     "LSE_emu_ctoken_t ctoken, "
     "LSE_emu_spaceid_t sid, void *acc)"),

    ],
    
  "operandinfo" : [

    ("EMU_spaceaddr_is_constant", LSE_domain.LSE_domainIDtype_CC,
     "boolean ??("
     "LSE_emu_ctoken_t ctoken, "
     "LSE_emu_spaceid_t sid, "
     "LSE_emu_spaceaddr_t *addr)"),

    ("EMU_spaceaddr_to_int", LSE_domain.LSE_domainIDtype_CC, "int ??("
     "LSE_emu_ctoken_t ctoken, "
     "LSE_emu_spaceid_t sid, "
     "LSE_emu_spaceaddr_t *addr)"),

    ],

  "operandval" : [
    ("EMU_fetch_operand", LSE_domain.LSE_domainIDtype_CC, "void ??("
     "LSE_emu_instr_info_t *ii, "
     "LSE_emu_operand_name_t oname, boolean isSpeculative)"),

    ("EMU_writeback_operand", LSE_domain.LSE_domainIDtype_CC, "void ??("
     "LSE_emu_instr_info_t *ii, "
     "LSE_emu_operand_name_t oname, boolean isSpeculative)"),

    ],

  "reclaiminstr" : [

    ("EMU_reclaim_instr", LSE_domain.LSE_domainIDtype_CC, "void ??("
     "LSE_emu_instr_info_t *ii)"),

    ],

  "speculation" : [

    ("EMU_resolve_instr", LSE_domain.LSE_domainIDtype_CC, "int ??("
     "LSE_emu_instr_info_t *ii, int resolveOper)"),

    ("EMU_resolve_operand", LSE_domain.LSE_domainIDtype_CC, "int ??("
     "LSE_emu_instr_info_t *ii, "
     "LSE_emu_operand_name_t oname, int resolveOper)"),

    ],
 
  "threadsafe" : [],

  "timed" : [
    ("EMU_register_clock", LSE_domain.LSE_domainIDtype_CC, "int ??("
     "LSE_emu_interface_t *, int hwcno, int clockno, LSE_clock_t clock)"),
    ("EMU_register_interrupt_callback", 
     LSE_domain.LSE_domainIDtype_CC, "int ??("
     "LSE_emu_interface_t *, int hwcno, LSE_emu_interrupt_callback_t handler)"),
    ],

  }

defaultInstMacro = r'''
LSE_domain_inst_define(LSE_emu_spaceref_equ,
          ::$1::LSE_emu_spaceref_equ_int($2,&$3,$4,&$5))

LSE_domain_inst_define(LSE_emu_spaceref_is_constant,
          ::$1::LSE_emu_spaceref_is_constant_int($2,$3,&$4))

LSE_domain_inst_define(LSE_emu_spaceref_to_int,
          ::$1::LSE_emu_spaceref_to_int_int($2,$3,&$4))

m4_dnl LSE_emu_statespace_has_capability
m4_dnl   $2 = state space id
m4_dnl   $3 = capability
LSE_domain_inst_define(LSE_emu_statespace_has_capability,
m4_python(if 1:
  cname = string.strip(r"""$3""")
  ename="$1"
  parms=r"""$2"""
  thisemu = LSE_db.domainInstances.get(ename)
  if cname in thisemu.stateSpaceCaps:
	print "%s(%s)" % (parms,
			  LSEcg_domain_mangler(ename,"LSE_emu_sshc" + cname,
					       LSE_domain.LSE_domainID_inst))
  else:
    print """LSE_report_err_at_codegen(Invalid capability name $3)"""
))

m4_dnl **************** BASIC accessors ********************

m4_dnl LSE_emu_instr_info_get
m4_dnl   $2 = instr_info info structure
m4_dnl   $3 = field name
LSE_domain_inst_define(LSE_emu_instr_info_get,
m4_python(if 1:
 fname=string.strip(r"""$3""")
 lfname=re.sub("[[.].*$","",fname)
 ename="$1"
 ptr=r"""$2"""
 thisemu = LSE_db.domainInstances.get(ename)
 if thisemu:
	if (lfname in thisemu.predecodefields):
	   print "\037(%s)->pre_info->%s\036" % (ptr,fname)
	else: 
	   print "\037(%s)->%s\036" % (ptr,fname)
))

m4_dnl LSE_emu_instr_info_is
m4_dnl   $2 = instr_info info structure
m4_dnl   $3 = iclass name
LSE_domain_inst_define(LSE_emu_instr_info_is,
m4_python(if 1:
 fname=string.strip(r"""$3""")
 ename="$1"
 ptr=r"""$2"""
 thisemu = LSE_db.domainInstances.get(ename)
 if "iclasses" in thisemu.predecodefields:
    print "\037((%s)->pre_info->iclasses.is_%s)\036" % (ptr,fname)
 else:
    print "\037((%s)->iclasses.is_%s)\036" % (ptr,fname)
))

m4_dnl LSE_emu_instr_info_set
m4_dnl   $2 = instr_info info structure
m4_dnl   $3 = field name
m4_dnl   $4 = value
LSE_domain_inst_define(LSE_emu_instr_info_set,
m4_python(if 1:
 fname=string.strip(r"""$3""")
 lfname=re.sub("\[.*$","",fname)
 ename="$1"
 val="""$4"""
 thisemu = LSE_db.domainInstances.get(ename)
 if (lfname in thisemu.predecodefields):
   print """do{\037($2)->pre_info->%s = %s\036;}while(0)""" % (fname,val)
 else: 
   print """do{\037($2)->%s = %s\036;}while(0)""" % (fname,val)
))

m4_dnl LSE_emu_dynid_is
m4_dnl   $2 = dynid structure pointer
m4_dnl   $3 = field name
LSE_domain_inst_define(LSE_emu_dynid_is,
LSE_emu_instr_info_is([$1])(
LSE_dynid_element_ptr($2,attr:$1:instr_info),$3))

m4_dnl LSE_emu_dynid_get
m4_dnl   $2 = dynid structure pointer
m4_dnl   $3 = field name
LSE_domain_inst_define(LSE_emu_dynid_get,
LSE_emu_instr_info_get([$1])(
LSE_dynid_element_ptr($2,attr:$1:instr_info),$3))

m4_dnl LSE_emu_dynid_set
m4_dnl   $2 = dynid structure pointer
m4_dnl   $3 = field name
m4_dnl   $4 = value
LSE_domain_inst_define(LSE_emu_dynid_set,
LSE_emu_instr_info_set([$1])(LSE_dynid_element_ptr($2,attr:$1:instr_info),
$3,$4))

m4_dnl ************ Emulator information accessors **********88

m4_dnl  LSE_emu_has_capability
m4_dnl   $2 = capability name
LSE_domain_inst_define(LSE_emu_has_capability,
m4_python(if 1:
  ename="$1"
  thisemu = LSE_db.domainInstances.get(ename)
  print int(string.strip(r"""$2""") in thisemu.capabilities)
))

m4_dnl  LSE_emu_has_instr_class
m4_dnl   $2 = instruction class name
LSE_domain_inst_define(LSE_emu_has_instr_class,
m4_python(if 1:
  ename="$1"
  thisemu = LSE_db.domainInstances.get(ename)
  print int(string.strip(r"""$2""") in thisemu.iclasses)
))

m4_dnl LSE_emu_call_extra_func
m4_dnl    $2 = function
m4_dnl    rest = arguments
m4_dnl  ***** Lots of work to be done to make this safe... ****
LSE_domain_inst_define(LSE_emu_call_extra_func, 
m4_python(if 1:
  ename=string.strip(r"""$1""")
  name=string.strip(r"""$2""")
  print name
)(m4_shift(m4_shift($@))))
'''

defaultInstHeader = r'''
m4_pythonfile(import types)

extern LSE_emu_interface_t LSE_emu_interface;

inline const char *
LSE_emu_get_statespace_name(LSE_emu_spaceid_t sid) {
  switch (sid) {
m4_pythonfile(
for s in LSE_domain_inst.statespaces:
	print "    case LSE_emu_spaceid_%s : return \"%s\";" % (s[0],s[0])
)
    default: break;
  }
  return "";
}

inline int 
LSE_emu_get_statespace_bitsize(int cno, 
                               LSE_emu_spaceid_t id) 
{
  LSE_emu_ctoken_t ctx;
  ctx = LSE_emu_hwcontexts_table[cno].ctok;

  switch (id) {
m4_pythonfile(
for s in LSE_domain_inst.statespaces:
	 if type(s[2]) != types.StringType:
	   a = s[2]
	   j = 0
           if (a==1): 
	     j=1
	     a=0
 	   a=a-1
	   while (a > 0):
		j = j + 1
		a = a >> 1
           cond="%d" % j
         elif (s[2]=="s"):
	   cond = ("calc_bits"
		   "(EMU_get_statespace_size(ctx,id))")
         elif (s[2][-1]=="b"):
           cond = string.atoi(s[2][:-1])
         elif (s[2][-1]=="c"):
	   cond = ("calc_bits"
		   "(EMU_get_statespace_size(ctx,id))")
         print "case LSE_emu_spaceid_%s : return %s;" % (s[0],cond)
)
    default: break;
  }
  return 0;
}

inline int 
LSE_emu_get_statespace_size(int cno,
                            LSE_emu_spaceid_t id) 
{
  LSE_emu_ctoken_t ctx;
  ctx = LSE_emu_hwcontexts_table[cno].ctok;

  switch (id) {
m4_pythonfile(
for s in LSE_domain_inst.statespaces:
	 if type(s[2]) != types.StringType:
           cond="%d" % s[2]
         elif (s[2]=="s"):
	   cond = "EMU_get_statespace_size(ctx,id)"
         elif (s[2][-1]=="b"):
           bsize = string.atoi(s[2][:-1])
           if (bsize<32):
	     cond="%d" % (s[2])
           elif (bsize<64):
	     cond="-1"
           else:
	     cond = "-1"
         elif (s[2][-1]=="c"):
	   cond = "EMU_get_statespace_size(ctx,id)"
         print "case LSE_emu_spaceid_%s : return %s;" % (s[0],cond)
)
    default: break;
  }
  return 0;
}

inline LSE_emu_spacetype_t
LSE_emu_get_statespace_type(LSE_emu_spaceid_t sid) {
  switch (sid) {
m4_pythonfile(
for s in LSE_domain_inst.statespaces:
  print "    case LSE_emu_spaceid_%s : return (LSE_emu_spacetype_t)%d;" % (s[0],s[1])
)
    default: break;
  }
  return (LSE_emu_spacetype_t)0;
}

inline int
LSE_emu_get_statespace_width(LSE_emu_spaceid_t sid) {
  switch (sid) {
m4_pythonfile(
i=1
for s in LSE_domain_inst.statespaces:
	print "    case LSE_emu_spaceid_%s : return %d;" % (s[0],s[3])
        i=i+1
)
    default: break;
  }
  return 0;
}

inline boolean LSE_emu_spaceref_equ_int(
  LSE_emu_spaceid_t id1, 
  LSE_emu_spaceaddr_t *addr1,
  LSE_emu_spaceid_t id2, 
  LSE_emu_spaceaddr_t *addr2) {
  if (id1 != id2) return 0;
  switch (id1) {
m4_pythonfile(
for s in LSE_domain_inst.statespaces:
	 if type(s[2]) != types.StringType:
           cond="addr1->%s == addr2->%s" % (s[0],s[0])
         elif (s[2]=="s"):
	   cond = "!strcmp(addr1->%s,addr2->%s)" % (s[0], s[0])
         elif (s[2][-1]=="b"):
           bsize = string.atoi(s[2][:-1])
           if (bsize<=32):
             cond="addr1->%s == addr2->%s" % (s[0],s[0])
           elif (bsize<=64):
	     cond="addr1->%s == addr2->%s" % (s[0],s[0])
           else:
	     cond = ("!memcmp(addr1->%s,addr2->%s,%d)" %
                      (s[0],s[0],(bsize+7)/bsize))
         elif (s[2][-1]=="c"):
	     cond = ("!strncmp(addr1->%s,addr2->%s,%d)" %
                      (s[0],s[0],string.atoi(s[2][:-1])))
         print "case LSE_emu_spaceid_%s : return %s;" % (s[0],cond)
)
  default: break; /* one less -Wall report */
  }
  return 0;
}

inline boolean 
LSE_emu_spaceref_is_constant_int(int cno, LSE_emu_spaceid_t id, 
				 LSE_emu_spaceaddr_t *addr) 
{
  LSE_emu_ctoken_t ctx;
  ctx = LSE_emu_hwcontexts_table[cno].ctok;

  return EMU_spaceaddr_is_constant(ctx,id,addr);
}

inline int
LSE_emu_spaceref_to_int_int(int cno, LSE_emu_spaceid_t id, 
			     LSE_emu_spaceaddr_t *addr)
{
  LSE_emu_ctoken_t ctx;
  ctx = LSE_emu_hwcontexts_table[cno].ctok;

  switch (id) {
m4_pythonfile(
i=1
for s in LSE_domain_inst.statespaces:
	 if type(s[2]) != types.StringType:
           cond="addr->%s" % (s[0])
         elif (s[2]=="s"):
	   cond = "EMU_spaceaddr_to_int(ctx,id,addr)"
         elif (s[2][-1]=="b"):
           bsize = string.atoi(s[2][:-1])
           if (bsize<=32):
	     cond="addr->%s" % (s[0])
           elif (bsize<=64):
	     cond="0"
           else:
	     cond = "0"
         elif (s[2][-1]=="c"):
	   cond = "EMU_spaceaddr_to_int(ctx,id,addr)"
         print "case LSE_emu_spaceid_%s : return %s;" % (s[0],cond)
	 i=i+1
)
    default: break;
  }
  return 0;
}

m4_pythonfile(
for c in LSE_emu.stateSpaceCaps:
  print """
inline boolean
LSE_emu_sshc_%s(LSE_emu_spaceid_t sid) {
  switch (sid) {
m4_pythonfile(
i=1
for s in LSE_domain_inst.statespaces:
	print "    case LSE_emu_spaceid_%%s : return %%d;" %% (s[0],"%s" in s[5])
        i=i+1
)
    default: break;
  }
  return 0;
}
""" % (c,c)
)

m4_dnl State space Access capability

m4_define(has_access,
m4_python(print "%d" % reduce(lambda x,y:x or ("access" in y[5]),\
			 LSE_domain_inst.statespaces,0)))

inline void
LSE_emu_space_read(LSE_emu_spacedata_t *datap, int cno,
                   LSE_emu_spaceid_t sid, LSE_emu_spaceaddr_t *addr,
		   int flags) {
  LSE_emu_ctoken_t ctx = LSE_emu_hwcontexts_table[cno].ctok;

#LSE if (has_access)
  EMU_space_read(datap,ctx,sid,addr,flags);
#LSE endif
}

inline void
LSE_emu_space_write(int cno,
                    LSE_emu_spaceid_t sid, LSE_emu_spaceaddr_t *addr,
		    LSE_emu_spacedata_t *datap,
		    int flags) {
  LSE_emu_ctoken_t ctx = LSE_emu_hwcontexts_table[cno].ctok;

#LSE if (has_access)
  EMU_space_write(ctx,sid,addr,datap,flags);
#LSE endif
}
m4_undefine(has_access)

m4_dnl Disassemble capability

inline void
LSE_emu_disassemble(LSE_dynid_t t, FILE *outfile) {
#LSE if (LSE_emu_has_capability(disassemble))
  EMU_disassemble_instr(
		    &t->attrs.LSE_domain_inst_name.instr_info, outfile);
#LSE else
  fprintf(outfile,"<" LSE_emu_addr_t_print_format 
	  "> Emulator does not support disassembly\n",
	 LSE_emu_get_true_addr(t->attrs.LSE_domain_inst_name.instr_info.addr));
#LSE endif
}

inline void
LSE_emu_disassemble_addr(int cno, LSE_emu_addr_t addr, 
			 FILE *outfile) {
#LSE if (LSE_emu_has_capability(disassemble))
  LSE_emu_ctoken_t ctx = LSE_emu_hwcontexts_table[cno].ctok;
  EMU_disassemble_addr(ctx,addr,outfile);
#LSE else
  fprintf(outfile,"<" LSE_emu_addr_t_print_format 
	  "> Emulator does not support disassembly\n", 
	  LSE_emu_get_true_addr(addr));
#LSE endif
}

inline void
LSE_emu_do_instrstep(LSE_dynid_t t, LSE_emu_instrstep_name_t sname,
		     boolean isSpeculative=false) {
  EMU_do_step(&t->attrs.LSE_domain_inst_name.instr_info,
		    sname, isSpeculative);
}

inline void
LSE_emu_doback(LSE_dynid_t t, boolean isSpeculative=false) {
  int i;
  for (i = m4_python(LSE_domain_inst.earliest[1]);
       i <= m4_python(LSE_domain_inst.latest[1]) ; i++) {
    EMU_do_step(&t->attrs.LSE_domain_inst_name.instr_info,
			(LSE_emu_instrstep_name_t)i, isSpeculative);
  }
}

inline void
LSE_emu_dofront(LSE_dynid_t t, boolean isSpeculative=false) {
  int i;
  for (i = m4_python(LSE_domain_inst.earliest[0]);
       i <= m4_python(LSE_domain_inst.latest[0]) ; i++) {
    EMU_do_step(&t->attrs.LSE_domain_inst_name.instr_info,
			(LSE_emu_instrstep_name_t)i, isSpeculative);
  }
}

inline LSE_emu_iaddr_t LSE_emu_get_start_addr(int cno) {
  return EMU_get_start_addr(LSE_emu_hwcontexts_table[cno].ctok);
}

inline void LSE_emu_set_start_addr(int cno, LSE_emu_iaddr_t addr) {
  EMU_set_start_addr(LSE_emu_hwcontexts_table[cno].ctok, addr);
}

inline void
LSE_emu_init_instr(LSE_dynid_t t, int cno, LSE_emu_iaddr_t addr) {
m4_python(if "reclaiminstr" in LSE_domain_inst.capabilities:
  print "  t->attrs.LSE_domain_class_name.emuinstid = LSE_emu_instid;"
)
  EMU_init_instr(&t->attrs.LSE_domain_inst_name.instr_info, cno,
                 LSE_emu_hwcontexts_table[cno].ctok, addr);
}

inline int
LSE_emu_resolve_dynid(LSE_dynid_t id, int resolveOp) {
m4_ifelse(m4_python(int("speculation" in LSE_domain_inst.capabilities)),1,
m4_ifelse(m4_python(int(LSE_domain_inst.speculationFlags & 1)),1,
return EMU_resolve_instr( 
		    &id->attrs.LSE_domain_inst_name.instr_info, resolveOp);,
int flags=0;
  for (int i = 0; i < LSE_emu_max_operand_dest;i++)
    flags |= EMU_resolve_operand(
		    &id->attrs.LSE_domain_inst_name.instr_info,
		    static_cast<LSE_emu_operand_name_t>(i), resolveOp);
  return flags;
),
LSE_report_err("Emulator does not support speculation");
return 0;
)
}

inline int
LSE_emu_resolve_operand(LSE_dynid_t id, LSE_emu_operand_name_t oname,
			int resolveOp) {
m4_ifelse(m4_python(int("speculation" in LSE_domain_inst.capabilities)),1,
return EMU_resolve_operand(
                    &id->attrs.LSE_domain_inst_name.instr_info, oname, 
		    resolveOp);,
LSE_report_err("Emulator does not support speculation");
 return 0;
)
}

inline void
LSE_emu_rollback_dynid(LSE_dynid_t id) {
m4_ifelse(m4_python(int("speculation" in LSE_domain_inst.capabilities)),1,
m4_ifelse(m4_python(int(LSE_domain_inst.speculationFlags & 1)),1,
EMU_resolve_instr( 
		    &id->attrs.LSE_domain_inst_name.instr_info, 
		    LSE_emu_resolveOp_rollback);,
for (int i = LSE_emu_max_operand_dest-1; i >= 0; --i)
    EMU_resolve_operand( 
		      &id->attrs.LSE_domain_inst_name.instr_info,
		      static_cast<LSE_emu_operand_name_t>(i), 
		      LSE_emu_resolveOp_rollback);),
LSE_report_err("Emulator does not support speculation");)
}

inline void
LSE_emu_rollback_operand(LSE_dynid_t id, LSE_emu_operand_name_t oname) {
m4_ifelse(m4_python(int("speculation" in LSE_domain_inst.capabilities)),1,
EMU_resolve_operand(
                    &id->attrs.LSE_domain_inst_name.instr_info, oname, 
		    LSE_emu_resolveOp_rollback);,
LSE_report_err("Emulator does not support speculation");)
}

inline void
LSE_emu_rollback_resolution(LSE_resolution_t r) {
  int i;
  for (i=r->num_ids-1;i>=0;i--) 
    LSE_emu_rollback_dynid(r->ids[i]);
}

/* TODO: need to do something nice if operandval not present */
#LSE if (LSE_emu_has_capability(operandval))
inline void
LSE_emu_fetch_operand(LSE_dynid_t id, LSE_emu_operand_name_t oname,
		      boolean isSpeculative=false) {
  EMU_fetch_operand(
		    &id->attrs.LSE_domain_inst_name.instr_info, oname,
		    isSpeculative);
}

inline void
LSE_emu_fetch_remaining_operands(LSE_dynid_t id, boolean isSpeculative=false) {
  int i;
  for (i=0;i<LSE_emu_max_operand_src;i++) 
    if (!id->attrs.LSE_domain_inst_name.instr_info.operand_val_src[i].valid)
      EMU_fetch_operand(
			&id->attrs.LSE_domain_inst_name.instr_info,
			(LSE_emu_operand_name_t)i, isSpeculative);
}

inline void
LSE_emu_writeback_operand(LSE_dynid_t id, LSE_emu_operand_name_t oname,
			  bool isSpeculative=false) {
  EMU_writeback_operand(
		    &id->attrs.LSE_domain_inst_name.instr_info, oname,
		    isSpeculative);
}

inline void
LSE_emu_writeback_remaining_operands(LSE_dynid_t id, 
				     bool isSpeculative=false) {
  int i;
  for (i=0;i<LSE_emu_max_operand_dest;i++) 
    if (!id->attrs.LSE_domain_inst_name.instr_info.operand_written_dest[i])
       EMU_writeback_operand(
			 &id->attrs.LSE_domain_inst_name.instr_info,
			 (LSE_emu_operand_name_t)i, isSpeculative);
}
#LSE endif

#LSE if (LSE_emu_has_capability(checkpoint))
inline LSE_chkpt::error_t
LSE_emu_chkpt_add_toc(LSE_chkpt::file_t *cptFile, const char *emuName, int step,
		      LSE_emu_chkpt_cntl_t *ctl) {
  return EMU_chkpt_add_toc( &LSE_emu_interface,
			   cptFile, emuName, step, ctl);
}

inline LSE_chkpt::error_t
LSE_emu_chkpt_write_segment(LSE_chkpt::file_t *cptFile, 
			    const char *segmentName, 
			    int step, LSE_emu_chkpt_cntl_t *ctl) {
  return EMU_chkpt_write_segment( &LSE_emu_interface,
			   cptFile, segmentName, step, ctl);
}

inline LSE_chkpt::error_t
LSE_emu_chkpt_check_toc(LSE_chkpt::file_t *cptFile, const char *emuName, 
			int step, unsigned int *position,
			LSE_emu_chkpt_cntl_t *ctl) {
  return EMU_chkpt_check_toc( &LSE_emu_interface,
			   cptFile, emuName, step, position, ctl);
}

inline LSE_chkpt::error_t
LSE_emu_chkpt_read_segment(LSE_chkpt::file_t *cptFile, const char *segmentName, 
			   int step, LSE_emu_chkpt_cntl_t *ctl) {
  return EMU_chkpt_read_segment( &LSE_emu_interface,
			   cptFile, segmentName, step, ctl);
}

inline void
LSE_emu_chkpt_end_replay(void) {
  EMU_chkpt_end_replay( &LSE_emu_interface);
}
#LSE endif

m4_ifelse(m4_python(int("timed" in LSE_domain_inst.capabilities)),1,
inline int
LSE_emu_register_clock(int cno, int clockno, 
		       ::LSE_clock::clock_t *clock) {
  return EMU_register_clock(&LSE_emu_interface, cno, clockno, clock);
})


/*************** always present stuff ****************/

extern int LSE_emu_load_context(int, int, char **, char **);
extern int LSE_emu_create_context(int cno); 
'''

defaultInstCode = r'''
LSE_emu_interface_t LSE_emu_interface = {
  LSE_emu_instid, 0,
};

int LSE_emu_create_context(int cno) 
{
  return LSE_emu::create_context_int(&LSE_emu_interface, 
				     LSE_emu_instid, cno, EMU_context_create);
}

int LSE_emu_load_context(int cno, int argc, char *argv[], char **envp) {
  return EMU_context_load(&LSE_emu_interface, cno, argc, argv, envp);
}

int LSE_domain_hook(init)(void) {
  EMU_init( &LSE_emu_interface);
  return 0;
}

int LSE_domain_hook(finish)(void) {
  EMU_finish( &LSE_emu_interface);
  if (argvSave) {
    for (int i = 0; i < argcSave; ++i)
      free(argvSave[i]);
    free(argvSave);
    argvSave = 0;
  }
  if (envpSave != emptyEnv) {
    char **pp = envpSave;
    while (*pp) {
      free(*pp);
      pp++;
    }
    free(envpSave);
    envpSave = emptyEnv;
  }
  return 0;
}

int LSE_domain_hook(start)(void) {

  /* all emulators which have initial contexts create a hardware context,
   * a software context, map them together, and load in something
   */
  if (m4_python(print int(not LSE_domain_inst.noInitialContexts))) {
    int rval;
    LSE_emu::context_load_no++;

    LSE_emu_create_context(LSE_emu::context_load_no);

    rval = LSE_emu_load_context(LSE_emu::context_load_no, LSE_emu::argcSave,
				LSE_emu::argvSave, LSE_emu::envpSave);
    if (rval) {
      fprintf(stderr,"LSE: Error: Unable to load initial context\n");
      return 1;
    }
  }
  return 0;
}

void LSE_domain_hook(dynid_dump)(LSE_dynid_t p) {
  fprintf(stderr, LSE_emu_addr_t_print_format " " ,
          LSE_emu_get_true_addr(p->attrs.LSE_domain_inst_name.instr_info.addr)
	  );
}

m4_ifelse(m4_python(print int("commandline" in LSE_domain_inst.capabilities)),
1,
int LSE_domain_hook(parse_arg)(int argc, char *arg, char **argv) {
  return EMU_parse_arg( &LSE_emu_interface, 
			   argc, arg, argv);
}

void LSE_domain_hook(usage)(void) {
  fprintf(stderr,"\n Domain instance "
          m4_python(print "\"" + LSE_domain_inst.instName + "\"")
	  " options (prefix with `--dom:"
          m4_python(print "\"" + LSE_domain_inst.instName + "\"")
	  ":')\n");
  EMU_print_usage( &LSE_emu_interface);
}
)m4_dnl commandline capability for emulator

'''
