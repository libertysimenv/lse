/* 
 * Copyright (c) 2009-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Emulator domain class functions (checkpointing)
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 */

#include <LSE_emu.h>

extern int LSE_sim_terminate_count;

namespace LSE_emu {

  LSE_chkpt::error_t 
  write_ctable(LSE_emu_interface_t *ifc, LSE_chkpt::file_t *cptFile) {
    LSE_chkpt::data_t *tdp;

    tdp = LSE_chkpt::build_sequence(0);
    
    for (int cno=1 ; cno <= LSE_emu_hwcontexts_total ; ++cno) {

      if (LSE_emu_hwcontexts_table[cno].valid && 
	  LSE_emu_hwcontexts_table[cno].emuinstid == ifc->emuinstid) {
	LSE_chkpt::build_unsigned(tdp, cno);
      }
    }
    tdp->update_size();
    return cptFile->write_to_segment(true, tdp);
  }

  LSE_chkpt::error_t 
  read_ctable(LSE_emu_interface_t *ifc, LSE_chkpt::file_t *cptFile,
	      void (*fixup)(LSE_emu_interface_t *, int, int)) {

    LSE_chkpt::error_t cerr;
    LSE_chkpt::data_t *tdp, *dp;
    int cno = 1;

    if ((cerr = cptFile->read_from_segment(0,&tdp))) return cerr;

    dp = tdp->oldestChild;

    while (dp) {
      int ocno = dp->content.uint64Val;

      // Find a HWcno
      for (; cno <= LSE_emu_hwcontexts_total ; ++cno) {
	if (LSE_emu_hwcontexts_table[cno].valid &&
	    LSE_emu_hwcontexts_table[cno].emuinstid == ifc->emuinstid) break;
      }

      if (cno > LSE_emu_hwcontexts_total) { // Not enough 
	fprintf(stderr, "Not enough hardware contexts to load checkpoint\n");
	return LSE_chkpt::error_Application;
      } 

      fixup(ifc, ocno, cno);
      dp = dp->sibling;
      ++cno;
    }
    delete tdp;
    return LSE_chkpt::error_None;
  }

} // namespace LSE_emu
