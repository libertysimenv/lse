/* -*-c++-*- 
 * Copyright (c) 2000-2014 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Code for doing Linux system calls
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * This file is used as part of the OS processing.
 *
 * TODO: 
 *      - Finish documentation for users
 *
 * NOTE: - this code is not thread-safe because of the global kernel structures
 *         and the possibility of sharing various resources between processes 
 *
 * Unimplemented Linux features:
 *      - lots of syscalls
 *      - System V shared memory stuff
 *      - user-defined timers; timers based on process/thread clocks
 *      - anything that would only apply to privileged processes
 *      - pretty much all security-related stuff, including uid/gid checks
 *      - core dumps
 *      - lots of fcntl stuff
 *      - sockets: listen, accept, connect
 *      - select
 *      - rusage throughout
 *      - non-blocking pipes
 *      - asynchronous I/O
 *      - PI futexes, futex requeue commands
 *        Note that one reason to not do PI futexes is that they require a
 *        "real" RT-capable scheduler to make any sense.
 *      - /proc filesystem
 *      - actual use/remembering of scheduler affinity and parameters
 *      - stacks growing on demand (partially implemented)
 *      - mremap
 *      - mlock (should be empty)
 *      - CLONE_NEWNS, CLONE_FS (different umask for different processes)
 *      - special root for simulator
 *      - execve	- *do the mm_release stuff: releasing futexes, etc.
 *                      - *needs to wipe out all the other threads in the
 *                        thread group by sending SIGKILL.  Ugh... it should
 *                        actually context switch out inside the kernel, which
 *                        is not so good for handling this.  Hmm...
 *	- wait		- *todos with respect to flags.
 *      - sessions with suspend/resume stuff
 *      - permissions on signal sends (kill -1 actually goes everywhere)
 *      - vfork parent gets woken up improperly when a signal is received?
 *
 */
/***********************************************************************
NOTES about how contexts get initialized and all that:

Emulator initialization
  - create a physical address space device

Linux initialization:
  - creates an OS init context with pid = 1 and new Linux memory structure
    - calls EMU_context_copy(from null context)
      - initializes ISA contexts
      - points context memory device at physical memory
    - makes ISA context point at OS context

LSE_emu_create_context
   - creates location in table for the context
   - calls EMU_context_create
     - sets the token to null (unmapped) when emulated OS handles contexts
     - note: for non-emulation situations, 
       may call EMU_context_copy with a null parent.
     - may set up other stuff.in the context

EMU_context_load: 
  - calls Linux_context_clone off of init process if the context is "empty"
       - calls Linux_cloneguts with CLONE_VM|CLONE_VFORK
	 - call EMU_context_copy (from initial context)
           - allocate a new ISA context and copy from init process
         - create child process structure; assign tid
	 - use CC_cloneguts to set up stack and registers
  - calls Linux_context_load 
    - calls Linux_execveguts
      - loads the file, mapping as it goes
      - if the file is not a good file, return an error
      - maps the stack
      - copies arguments to stack (appears to map stack as it goes along)
      - use CC_execveguts to:
        - set up gate pages
        - set up stack pointers, new register values
    - adds to ready list
    - calls Linux_schedule()

clone/clone2 call:  (fork/vfork are variants with particular flags)
  - calls Linux_cloneguts with stack start/end (supplied by program)  
  - adds to ready list
  - calls Linux_schedule()

execve call:
  - find arguments, environment
  - call Linux_execveguts (see EMU_context_load) for the thing

What is supposed to be done by functions supplied by user:
  EMU_context_copy:  copy an ISA context; if old context is 0, create a
	  new context.  If old context is not 0, memory must point at
	  the same memory as before.

NOTES about memories:
  ISA contexts point at physical memories.  OS contexts point at Linux_memory_t
  which has a translation space whose subspace points at the corresponding
  physical memory.  There is a single physical memory per OS instance.

NOTES about groups:
Thread groups and process groups and sessions are really confusing concepts.  
Here is what I think I understand:
- Thread groups are groups of threads which are treated as if they were
  one thread for purposes of status change notifications (e.g. SIGCHLD) and
  parentage.  They are said to all be "in the same process".  Thus
  thread_group = process in Linux documentation.
  - threads in the group do *not* signal anything to a parent on termination
    (e.g. they are detached).  This is enforced by making sure the clone
    call marks the exit signal correctly.
  - when all threads in the group terminate, then the parent of the group
    gets *one* termination signal.
  - when a thread terminates, its children become children of some other
    thread in the group.
  - an execve in any thread wipes out the whole thread group and the execve
    actually runs in the the group leader.
  - Thread groups also are "one process" for most signalling; any thread
    in the group can take the signal.
  - The kill system call with a positive id sends the signal to a thread
    group, not a particular thread.  However, the tkill sends to a particular
    thread.  The preferred thread it get the signal is the leader.
  - fatal signals get sent to all threads in the thread group, not just
    the leader.
  - Linux's signal_struct is closely tied to (actually is?) the thread group
- Process groups are groups of thread group (processes) which are supposed 
  to start/stop together.  Normally the pgid is inherited on clone/fork; it 
  has to be explicitly set to change it.
  - The "wait" family of system calls waits for process groups or
    thread groups, depending on the parameters
  - The kill call with a negative id sends to every process in a process
    group.
  - pgid are preserved across execve
  - if a process group gets "orphaned" and any member is stopped, then all
    members get a SIGHUP followed by SIGCONT.
- Sessions are groups of process groups used for terminal control
  - only one process group per session can be in the foreground.  Only
    the foreground process group receives SIGINT
- A detached thread is one which reports no signal at all to a parent on exit;
  detached threads are immediately reaped.
  
- Process suspension
  This is a very ugly problem, because we do not have a kernel stack to
  suspend on, so we need to fake it.  Also, signals are recognized on
  the transition from system to user mode; that occurs when we context
  switch in or *after* a system call.  Different system calls do different
  things when they block, and how they resume depends on why they are now
  not blocked.

  To keep it all straight, we have a stack of "continuation" records for
  syscalls which are blocked.  These records finish up the system call
  appropriately.  These get used when we are scheduled in.  We know whether
  or not we are switching in from a system call or not because a preemption
  also puts on a record.  So, if the stack is empty, we can pretend we
  are not in a system call.   These continuation records can also be used to 
  hold information needed to internally complete or restart the system call.

  IMPORTANT INVARIANT: once a thread is blocked, is is unblocked only because
  its blocking condition resolves, it times out (if that is one of the
  ways it can resolve), or it receives a signal.  The continuation code
  needs to distinguish between these cases, because a blocking condition
  resolution or timeout *cannot* take any of the ERESTART return values, as 
  those are only checked when there is a pending signal.  This does make life
  difficult for the wait continuation, which must recheck child completion
  and gets wakeup conditions from many different places

  And, things get even hairier if you consider what sort of evil nesting
  can take place if we run the scheduler at all the places that seem
  reasonable.  We end up with half-updated data structures and it all falls
  to pieces.  Therefore, we will just mark that scheduling is "desired" and
  do it at the end of a system call.

- And some coding notes
  - You may note that curr_pc doesn't really seem to be necessary; the only
    place it is actually used is to set the start address back to the
    system call in the exit system call, and that could be done by each
    of the ISA files.  However, it may turn out to be necessary for 
    ISAs like SPARC where moving back an instruction is not as easy as 
    just subtracting 4 from the current address if the system call happens
    to be in a delay slot.

************************************************************************
  Here I will eventually document what users of this header file are 
  supposed to do.

  The header must be included exactly three times

< section 1>
#define LSE_LINUX_HEADERS
#include "OS/LSE_Linux.h"

namespace whatever {

#define LSE_LINUX_BASE
#define CC_isacontext_t       <appropriate type>
  const bool Linux_bigendian = <appropriate value>;

#include "OS/LSE_Linux.h"

< section 2 >

#define LSE_LINUX_CODE
#include "OS/LSE_Linux.h"

<section 3>
}

Section 1: 
----------
#define LSE_LINUX_COMPAT         if a 32-bit/64-bit version is desired
#define LSE_LINUX_CHECKPOINT     if checkpointing is desired

Section 2:
----------

Section 3:
----------

**************************************************************************/

#ifdef LSE_LINUX_HEADERS
#undef LSE_LINUX_HEADERS
#include <assert.h>
#include <dirent.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/uio.h>
#include <sys/vfs.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#ifndef __APPLE__
#include <termio.h>
#endif
#include <termios.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#if defined(__GNUC__) && !defined(LIS_UNUSED)
#define LIS_UNUSED __attribute__ ((unused))
#endif 
#include <algorithm>
#include <deque>
#include <list>
#include <map>
#include <iostream>
#include <queue>
#include <sstream>
#include <iomanip>
#ifdef __USE_LARGEFILE64
  typedef struct stat64 LSE_stat64;
#else
  typedef struct stat LSE_stat64;
#endif

#include <emulib/LSE_elf.h>
#include <emulib/LSE_translation.h>
#endif // LSE_LINUX_HEADERS

#ifdef LSE_LINUX_BASE
#undef LSE_LINUX_BASE
static uint64_t OS_sigmask(int x) { return uint64_t(1) << (x-1); }

// Constants and types that must be redefined by an operating
// system implementation if they are different.  The final 
// definition with things overridden should go in OSDefs
//
// The o is needed on constants because Linux has C headers in some cases
// and thus the constants are not always in a nice namespace

struct OSBaseDefs {
  // error numbers.  The o is needed because C headers define these as macros
  static const int oEPERM                 =   1;
  static const int oESRCH                 =   3;
  static const int oEINTR                 =   4;
  static const int oENOEXEC               =   8;
  static const int oECHILD                =  10;
  static const int oEAGAIN                =  11;
  static const int oEWOULDBLOCK           =  oEAGAIN;
  static const int oENOMEM                =  12;
  static const int oEFAULT                =  14;
  static const int oEINVAL                =  22;
  static const int oENOSPC                =  28;
  static const int oESPIPE                =  29;
  static const int oEPIPE                 =  32;
  static const int oERANGE                =  34;
  static const int oENOSYS                =  38;
  static const int oEOVERFLOW             =  75;
  static const int oEBADFD                =  77;
  static const int oETIMEDOUT             = 110;
  static const int oERESTARTSYS           = 512;
  static const int oERESTARTNOINTR        = 513;
  static const int oERESTARTNOHAND        = 514;
  static const int oERESTART_RESTARTBLOCK = 516;

  // fcntl
  static const int oO_RDONLY =          00;
  static const int oO_WRONLY =          01;
  static const int oO_RDWR =            02;
  static const int oO_CREAT =         0100;
  static const int oO_EXCL =          0200;
  static const int oO_NOCTTY =        0400;
  static const int oO_TRUNC =        01000;
  static const int oO_APPEND =       02000;
  static const int oO_NONBLOCK =     04000;
  static const int oO_SYNC =        010000;
  static const int oO_DIRECT =      040000;
  static const int oO_LARGEFILE =  0100000;
  static const int oO_DIRECTORY =  0200000;
  static const int oO_NOFOLLOW =   0400000;
  static const int oO_NOATIME =   01000000;
  static const int oO_CLOEXEC =   02000000;
  static const int oO_NDELAY =    oO_NONBLOCK;
  static const int oFD_CLOEXEC =   1;

  // futex
  static const int oFUTEX_WAIT            =   0;
  static const int oFUTEX_WAKE            =   1;
  static const int oFUTEX_FD              =   2;
  static const int oFUTEX_REQUEUE         =   3;
  static const int oFUTEX_CMP_REQUEUE     =   4;
  static const int oFUTEX_WAKE_OP         =   5;
  static const int oFUTEX_LOCK_PI         =   6;
  static const int oFUTEX_UNLOCK_PI       =   7;
  static const int oFUTEX_TRYLOCK_PI      =   8;
  static const int oFUTEX_WAIT_BITSET     =   9;
  static const int oFUTEX_WAKE_BITSET     =  10;
  static const int oFUTEX_WAIT_REQUEUE_PI =  11;
  static const int oFUTEX_CMP_REQUEUE_PI  =  12;
  static const int oFUTEX_PRIVATE_FLAG    = 128;
  static const int oFUTEX_CLOCK_REALTIME  = 256;

  // ioctls
  static const int oTCGETS  = 0x5401;
  static const int oTCGETA  = 0x5405;
  static const int oTCSETAW = 0x5407;

  // limits
  static const int oPATH_MAX = 4096;
  static const int oNAME_MAX =  255;

  // mman
  static const int oMAP_SHARED     =  0x01;
  static const int oMAP_PRIVATE    =  0x02;
  static const int oMAP_TYPE       =  0x0f;
  static const int oMAP_FIXED      = 0x010;
  static const int oMAP_ANONYMOUS  = 0x020;
  static const int oPROT_READ      =   0x1;
  static const int oPROT_WRITE     =   0x2;
  static const int oPROT_EXEC      =   0x4;
  static const int oPROT_NONE      =   0x0;
  static const int oPROT_ALL       =   0x7;
  static const int oPROT_GROWSDOWN =  0x1000000;
  static const int oPROT_GROWSUP   =  0x2000000;
  static const int oPROT_STACKDIR  = oPROT_GROWSDOWN;
  static const int oSTACKGROWS     = 0;

  // poll
  static const int oPOLLIN =      0x01;
  static const int oPOLLOUT =     0x04;
  static const int oPOLLERR =     0x08;
  static const int oPOLLNVAL =    0x20;
  static const int oPOLLHUP =     0x10;
  static const int oPOLLRDNORM =  0x40;
  static const int oPOLLWRNORM = 0x100;

  // rlimits
  static const int oRLIMIT_NLIMITS  = 16;
  static const int oRLIMIT_INFINITY = ~0;

  // sched
  static const int oCLONE_CHILD_CLEARTID =  0x0200000;
  static const int oCLONE_CHILD_SETTID =    0x1000000;
  static const int oCLONE_FILES =           0x0000400;
  static const int oCLONE_FS =              0x0000200;
  static const int oCLONE_PARENT =          0x0008000;
  static const int oCLONE_PARENT_SETTID =   0x0100000;
  static const int oCLONE_SETTLS =          0x0080000;
  static const int oCLONE_SIGHAND =         0x0000800;
  static const int oCLONE_STOPPED =         0x2000000;
  static const int oCLONE_SYSVSEM =         0x0040000;
  static const int oCLONE_THREAD =          0x0010000;
  static const int oCLONE_VFORK =           0x0004000;
  static const int oCLONE_VM =              0x0000100;
  static const int oCLONE_EXITSIGNAL =      0x00000ff;

  // signal
  static const int oSIGCHLD  = 17;
  static const int oSIGCONT  = 18;
  static const int oSIGSTOP  = 19;
  static const int oSIGTSTP  = 20;
  static const int oSIGTTIN  = 21;
  static const int oSIGTTOU  = 22;
  static const int oSIGURG   = 23;
  static const int oSIGWINCH = 28;
  static const unsigned oSA_NOCLDWAIT =  0x2u;
  static const unsigned oSA_SIGINFO   =  0x4u;
  static const unsigned oSA_ONSTACK   =  0x08000000u;
  static const unsigned oSA_RESTART   =  0x10000000u;
  static const unsigned oSA_NODEFER   =  0x40000000u;
  static const unsigned oSA_RESETHAND =  0x80000000u;
  static const unsigned oSA_ONESHOT   =  oSA_RESETHAND;
  // do not forget to redefine these if you redefine any of the signals 
  static const unsigned oSIGDEF_IGNORE() {
    return (OS_sigmask(oSIGCONT) | OS_sigmask(oSIGCHLD) | 
	    OS_sigmask(oSIGWINCH) | OS_sigmask(oSIGURG));
  }
  static const unsigned oSIGDEF_STOP() {
    return (OS_sigmask(oSIGSTOP) | OS_sigmask(oSIGTSTP) | 
	    OS_sigmask(oSIGTTIN) | OS_sigmask(oSIGTTOU));
  }
  static const int oSS_ONSTACK   =     1;
  static const int oSS_DISABLE   =     2;
  static const int oMINSIGSTKSZ  =  2048;
  static const int oSIGSTKSZ     =  8192;
  static const int o_NSIG        =  64; // number of signals
  static const int ocompat__NSIG =  64; // number of signals

  // signal-defs
  static const int oSIG_BLOCK   =  0;
  static const int oSIG_UNBLOCK =  1;
  static const int oSIG_SETMASK =  2;
  // siginfo
  static const int oSI_USER    =       0;
  static const int oSI_KERNEL  =       0x80;
  static const int oSI_QUEUE   =       -1;
  static const int oSI_TIMER  =       0xfffe;
  static const int oSI_TKILL   =      -6;
  static const int oCLD_EXITED =  0x30001;
  static const int oCLD_KILLED =  0x30002;
  static const int oSIGEV_SIGNAL = 0;
  static const int oSIGEV_NONE = 1;
  static const int oSIGEV_THREAD = 2;
  static const int oSIGEV_THREAD_ID = 4;

  // termbits
  static const int oNCCS = 19;

  // termios
  static const int oNCC = 8;

  // utsname
  static const int o_UTSNAME_LENGTH = 65;
  static const int oSYS_NMLN        = o_UTSNAME_LENGTH;

  // wait
  static const unsigned oWNOHANG     =          1;
  static const unsigned oWEXITED     =          4;
  static const unsigned oWSTOPPED    =          2;
  static const unsigned oWUNTRACED   =          2;
  static const unsigned oWCONTINUED  =          8;
  static const unsigned oWNOWAIT     = 0x01000000;
  static const unsigned o__WNOTHREAD = 0x20000000;
  static const unsigned o__WALL      = 0x40000000;
  static const unsigned o__WCLONE    = 0x80000000;

  // others which *must* be redefined

  static const int oNUM_DLINFO_ENTRIES = 0;
  static const int oHWCAP              = 0;
  static const int oCLKTCK             = 0;

  static const unsigned oPAGE_SIZE        = 4096;
  static const unsigned oCLONE_STACK_SIZE = 16; // number of stack pages
  static const unsigned oPAGE_BITS        = 12;

  static LSE_emu_addr_t oUTASK_SIZE(bool)    { return 65536; }
  static LSE_emu_addr_t oUNMAPPED_BASE(bool) { return 0; }
  static LSE_emu_addr_t oUSTACK_BASE(bool)   { return 0; }
  static LSE_emu_addr_t oUSTACK_TOP(bool)    { return 0; }

  static const char *oDYNLOADER;
  static const char *oSYSNAME;
  static const char *oNODENAME;
  static const char *oRELEASE;
  static const char *oVERSION;
  static const char *oMACHINE;
  static const char *oDOMAINNAME;

  static const uint64_t oTIMESLICE = 0; 

  // types which *must* be defined.  They are not actually defined here
  // so that you do not forget to define them.  L_ prefixes are there to
  // avoid any types that may be macros in the system header files.
  //   L_cc_t
  //   L_clock_t
  //   L_long
  //   L_ptr_t
  //   L_size_t
  //   L_statfs_word
  //   L_suseconds_t
  //   L_tcflag_t
  //   L_time_t
  //   L_ulong
  //   L_uptr_t
  //  these may be done after structures
  //   L_newstat
  //   L___sysctl_args
  // if doing compat:
  //   L_compat_clock_t
  //   L_compat_long
  //   L_compat_size_t
  //   L_compat_suseconds_t
  //   L_compat_time_t
  //   L_compat_ulong
  //   L_compat_uptr_t
  // again, may be done after structures
  //   L_compat_newstat
  //   L_compat_sigaltstack
  //   L_compat_statfs
  //   L_compat_termio
  //   L_compat_termios

  // and types that seem to not change with architecture

  typedef int L_pid_t;

  struct L_sched_param {
    int priority;
  };

  struct L_sigval_t {
    int ival;	  
    LSE_emu_addr_t pval;
  };

  struct L_timezone {
    int32_t    LSE_tz_minuteswest;
    int32_t    LSE_tz_dsttime;
    L_timezone& operator=(const struct timezone &buf) {
      LSE_tz_minuteswest = LSE_h2e((int32_t)buf.tz_minuteswest, 
				   Linux_bigendian);
      LSE_tz_dsttime = LSE_h2e((int32_t)buf.tz_dsttime, Linux_bigendian);
    }
  };

  typedef int CC_fault_t;
};

const char *OSBaseDefs::oDYNLOADER  = "ld.so";
const char *OSBaseDefs::oSYSNAME    = "Linux";
const char *OSBaseDefs::oNODENAME   = "LSELinux";
const char *OSBaseDefs::oRELEASE    = "2.6.31";
const char *OSBaseDefs::oVERSION    = "Emulated #1 Fri Jun 8 12:00:00 EDT 2001";
const char *OSBaseDefs::oMACHINE    = "unknown";
const char *OSBaseDefs::oDOMAINNAME = "unknown";

// EVIL fact: I have to redeclare all of the static const's so that they
// will have space allocated.  They're not quite constants, and if an lvalue
// is taken for them, undefined references occur if they're not defined.

const int OSBaseDefs::oEPERM;
const int OSBaseDefs::oESRCH;
const int OSBaseDefs::oEINTR;
const int OSBaseDefs::oENOEXEC;
const int OSBaseDefs::oECHILD;
const int OSBaseDefs::oEAGAIN;
const int OSBaseDefs::oEWOULDBLOCK;
const int OSBaseDefs::oENOMEM;
const int OSBaseDefs::oEFAULT;
const int OSBaseDefs::oERANGE;
const int OSBaseDefs::oEINVAL;
const int OSBaseDefs::oENOSPC;
const int OSBaseDefs::oESPIPE;
const int OSBaseDefs::oEPIPE;
const int OSBaseDefs::oENOSYS;
const int OSBaseDefs::oEOVERFLOW;
const int OSBaseDefs::oEBADFD;
const int OSBaseDefs::oETIMEDOUT;
const int OSBaseDefs::oERESTARTSYS;
const int OSBaseDefs::oERESTARTNOINTR;
const int OSBaseDefs::oERESTARTNOHAND;
const int OSBaseDefs::oERESTART_RESTARTBLOCK;
const int OSBaseDefs::oO_RDONLY;
const int OSBaseDefs::oO_WRONLY;
const int OSBaseDefs::oO_RDWR;
const int OSBaseDefs::oO_CREAT;
const int OSBaseDefs::oO_EXCL;
const int OSBaseDefs::oO_NOCTTY;
const int OSBaseDefs::oO_TRUNC;
const int OSBaseDefs::oO_APPEND;
const int OSBaseDefs::oO_NONBLOCK;
const int OSBaseDefs::oO_SYNC;
const int OSBaseDefs::oO_DIRECT;
const int OSBaseDefs::oO_LARGEFILE;
const int OSBaseDefs::oO_DIRECTORY;
const int OSBaseDefs::oO_NOFOLLOW;
const int OSBaseDefs::oO_NOATIME;
const int OSBaseDefs::oO_CLOEXEC;
const int OSBaseDefs::oO_NDELAY;
const int OSBaseDefs::oFD_CLOEXEC;
const int OSBaseDefs::oFUTEX_WAIT;
const int OSBaseDefs::oFUTEX_WAKE;
const int OSBaseDefs::oFUTEX_FD;
const int OSBaseDefs::oFUTEX_REQUEUE;
const int OSBaseDefs::oFUTEX_CMP_REQUEUE;
const int OSBaseDefs::oFUTEX_WAKE_OP;
const int OSBaseDefs::oFUTEX_LOCK_PI;
const int OSBaseDefs::oFUTEX_UNLOCK_PI;
const int OSBaseDefs::oFUTEX_TRYLOCK_PI;
const int OSBaseDefs::oFUTEX_WAIT_BITSET;
const int OSBaseDefs::oFUTEX_WAKE_BITSET;
const int OSBaseDefs::oFUTEX_WAIT_REQUEUE_PI;
const int OSBaseDefs::oFUTEX_CMP_REQUEUE_PI;
const int OSBaseDefs::oFUTEX_PRIVATE_FLAG;
const int OSBaseDefs::oFUTEX_CLOCK_REALTIME;
const int OSBaseDefs::oTCGETS;
const int OSBaseDefs::oTCGETA;
const int OSBaseDefs::oTCSETAW;
const int OSBaseDefs::oPATH_MAX;
const int OSBaseDefs::oNAME_MAX;
const int OSBaseDefs::oMAP_SHARED;
const int OSBaseDefs::oMAP_PRIVATE;
const int OSBaseDefs::oMAP_TYPE;
const int OSBaseDefs::oMAP_FIXED;
const int OSBaseDefs::oMAP_ANONYMOUS;
const int OSBaseDefs::oPROT_READ;
const int OSBaseDefs::oPROT_WRITE;
const int OSBaseDefs::oPROT_EXEC;
const int OSBaseDefs::oPROT_NONE;
const int OSBaseDefs::oPROT_ALL;
const int OSBaseDefs::oPROT_GROWSDOWN;
const int OSBaseDefs::oPROT_GROWSUP;
const int OSBaseDefs::oPROT_STACKDIR;
const int OSBaseDefs::oSTACKGROWS;
const int OSBaseDefs::oPOLLIN;
const int OSBaseDefs::oPOLLOUT;
const int OSBaseDefs::oPOLLERR;
const int OSBaseDefs::oPOLLNVAL;
const int OSBaseDefs::oPOLLHUP;
const int OSBaseDefs::oPOLLRDNORM;
const int OSBaseDefs::oPOLLWRNORM;
const int OSBaseDefs::oRLIMIT_NLIMITS;
const int OSBaseDefs::oRLIMIT_INFINITY;
const int OSBaseDefs::oCLONE_CHILD_CLEARTID;
const int OSBaseDefs::oCLONE_CHILD_SETTID;
const int OSBaseDefs::oCLONE_FILES;
const int OSBaseDefs::oCLONE_FS;
const int OSBaseDefs::oCLONE_PARENT;
const int OSBaseDefs::oCLONE_PARENT_SETTID;
const int OSBaseDefs::oCLONE_SETTLS;
const int OSBaseDefs::oCLONE_SIGHAND;
const int OSBaseDefs::oCLONE_STOPPED;
const int OSBaseDefs::oCLONE_SYSVSEM;
const int OSBaseDefs::oCLONE_THREAD;
const int OSBaseDefs::oCLONE_VFORK;
const int OSBaseDefs::oCLONE_VM;
const int OSBaseDefs::oCLONE_EXITSIGNAL;
const int OSBaseDefs::oSIGCHLD;
const int OSBaseDefs::oSIGCONT;
const int OSBaseDefs::oSIGSTOP;
const int OSBaseDefs::oSIGTSTP;
const int OSBaseDefs::oSIGTTIN;
const int OSBaseDefs::oSIGTTOU;
const int OSBaseDefs::oSIGURG;
const int OSBaseDefs::oSIGWINCH;
const unsigned OSBaseDefs::oSA_NOCLDWAIT;
const unsigned OSBaseDefs::oSA_SIGINFO;
const unsigned OSBaseDefs::oSA_ONSTACK;
const unsigned OSBaseDefs::oSA_RESTART;
const unsigned OSBaseDefs::oSA_NODEFER;
const unsigned OSBaseDefs::oSA_RESETHAND;
const unsigned OSBaseDefs::oSA_ONESHOT;
const int OSBaseDefs::oSS_ONSTACK;
const int OSBaseDefs::oSS_DISABLE;
const int OSBaseDefs::oMINSIGSTKSZ;
const int OSBaseDefs::oSIGSTKSZ;
const int OSBaseDefs::o_NSIG;
const int OSBaseDefs::ocompat__NSIG;
const int OSBaseDefs::oSIG_BLOCK;
const int OSBaseDefs::oSIG_UNBLOCK;
const int OSBaseDefs::oSIG_SETMASK;
const int OSBaseDefs::oSI_USER;
const int OSBaseDefs::oSI_KERNEL;
const int OSBaseDefs::oSI_QUEUE;
const int OSBaseDefs::oSI_TIMER;
const int OSBaseDefs::oSI_TKILL;
const int OSBaseDefs::oCLD_EXITED;
const int OSBaseDefs::oCLD_KILLED;
const int OSBaseDefs::oNCCS;
const int OSBaseDefs::oNCC;
const int OSBaseDefs::o_UTSNAME_LENGTH;
const int OSBaseDefs::oSYS_NMLN;
const unsigned OSBaseDefs::oWNOHANG;
const unsigned OSBaseDefs::oWEXITED;
const unsigned OSBaseDefs::oWSTOPPED;
const unsigned OSBaseDefs::oWUNTRACED;
const unsigned OSBaseDefs::oWCONTINUED;
const unsigned OSBaseDefs::oWNOWAIT;
const unsigned OSBaseDefs::o__WNOTHREAD;
const unsigned OSBaseDefs::o__WALL;
const unsigned OSBaseDefs::o__WCLONE;
const int OSBaseDefs::oNUM_DLINFO_ENTRIES;
const int OSBaseDefs::oHWCAP;
const int OSBaseDefs::oCLKTCK;
const unsigned OSBaseDefs::oPAGE_SIZE;
const unsigned OSBaseDefs::oCLONE_STACK_SIZE; // number of stack pages
const unsigned OSBaseDefs::oPAGE_BITS;
const int OSBaseDefs::oSIGEV_SIGNAL;
const int OSBaseDefs::oSIGEV_NONE;
const int OSBaseDefs::oSIGEV_THREAD;
const int OSBaseDefs::oSIGEV_THREAD_ID;
const uint64_t OSBaseDefs::oTIMESLICE;

template<class T> struct OSDerivedDefs : public T {
  // copy definitions into this namespace for convenience
  typedef typename T::L_cc_t        L_cc_t;
  typedef typename T::L_clock_t     L_clock_t;
  typedef typename T::L_long        L_long;
  typedef typename T::L_ptr_t       L_ptr_t;
  typedef typename T::L_size_t      L_size_t;
  typedef typename T::L_statfs_word L_statfs_word;
  typedef typename T::L_suseconds_t L_suseconds_t;
  typedef typename T::L_tcflag_t    L_tcflag_t;
  typedef typename T::L_time_t      L_time_t;
  typedef typename T::L_ulong       L_ulong;
  typedef typename T::L_uptr_t      L_uptr_t;

  struct L_iovec {
    L_uptr_t iov_base;
    L_size_t iov_len;
  };

  struct L_rlimit {
    L_ulong LSE_rlim_cur; /* 0 */
    L_ulong LSE_rlim_max; /* 8 */
    L_rlimit& operator=(const struct rlimit &buf) {
      LSE_rlim_cur = LSE_h2e((L_ulong)(L_long)buf.rlim_cur, Linux_bigendian);
      LSE_rlim_max = LSE_h2e((L_ulong)(L_long)buf.rlim_max, Linux_bigendian);
    }
    struct rlimit get() {
      struct rlimit buf;
      buf.rlim_cur = LSE_e2h(LSE_rlim_cur,Linux_bigendian);
      buf.rlim_max = LSE_e2h(LSE_rlim_max,Linux_bigendian);
      return buf;
    }
  };

  struct L_robust_list {
    L_ptr_t LSE_next;
  };

  struct L_robust_list_head {
    L_ptr_t LSE_head_next;
    L_long  LSE_foffset;
    L_ptr_t LSE_op_pending;
    L_robust_list_head e2h() {
      L_robust_list_head nb;
      nb.LSE_head_next  = LSE_e2h(LSE_head_next, Linux_bigendian);
      nb.LSE_foffset    = LSE_e2h(LSE_foffset, Linux_bigendian);
      nb.LSE_op_pending = LSE_e2h(LSE_op_pending, Linux_bigendian);
      return nb;
    };
  };

  struct L_sigaltstack {
    L_uptr_t LSE_ss_sp;
    int32_t  LSE_ss_flags;
    L_size_t LSE_ss_size;

    inline L_sigaltstack e2h() {
      L_sigaltstack nb;
      nb.LSE_ss_sp    = LSE_e2h(LSE_ss_sp,Linux_bigendian);
      nb.LSE_ss_flags = LSE_e2h(LSE_ss_flags,Linux_bigendian);
      nb.LSE_ss_size  = LSE_e2h(LSE_ss_size,Linux_bigendian);
      return nb;
    }
    inline L_sigaltstack h2e() { return e2h(); }
  };

  struct L_sigset { 
    // note: the number of signals could actually vary from arch to arch,
    // but I do not see evidence that it actually does _NSIG_WORDS
    static const int o_NSIG_WORDS = T::o_NSIG/ (8 * sizeof(L_ulong));
    L_ulong sig[o_NSIG_WORDS];
    uint64_t toword() {
      uint64_t v = 0;
      if (Linux_bigendian) { // big-endian target
	for (int i = 0; i < o_NSIG_WORDS; ++i)
	  v = (v << (o_NSIG_WORDS > 1 ? (8 * sizeof(L_ulong)) : 0))
	    | LSE_e2h(sig[i], Linux_bigendian);
      } else {
	for (int i = o_NSIG_WORDS-1; i >= 0; --i)
	  v = (v << (o_NSIG_WORDS > 1 ? (8 * sizeof(L_ulong)) : 0))
	    | LSE_e2h(sig[i], Linux_bigendian);
      }
      return v;
    }
    void fromword(uint64_t v) { 
      if (Linux_bigendian) { // big-endian target
	for (int i = 0; i < o_NSIG_WORDS; ++i) {
	  sig[i] = LSE_h2e((L_ulong)v, Linux_bigendian);
	  v >= (o_NSIG_WORDS > 1 ? (8 * sizeof(L_ulong)) : 0);
	}
      } else {
	for (int i = o_NSIG_WORDS - 1; i >= 0; --i) {
	  sig[i] = LSE_h2e((L_ulong)v, Linux_bigendian);
	  v >= (o_NSIG_WORDS > 1 ? (8 * sizeof(L_ulong)) : 0);
	}
      }
    }   
  };

  struct Linux_sighandinf_t { 
    // needs to be here because of dependence on oSIGSTOP
    static const uint64_t cantChangeMask 
    = (1<<8) | (uint64_t(1) << (T::oSIGSTOP-1));
    LSE_emu_addr_t handler;
    LSE_emu_addr_t restorer;
    uint64_t sigmask;
    uint64_t flags;
  };

  struct L_sigaction {
    LSE_emu_addr_t LSE_sa_handler;
    L_ulong        LSE_sa_flags;
    LSE_emu_addr_t LSE_sa_restorer;
    L_sigset       LSE_sa_mask;
    inline struct Linux_sighandinf_t tosighand() {
      Linux_sighandinf_t nv;
      nv.handler = LSE_e2h(LSE_sa_handler, Linux_bigendian);
      nv.flags   = LSE_e2h(LSE_sa_flags, Linux_bigendian);
      nv.sigmask = LSE_sa_mask.toword();
#ifdef LSE_LINUX_HAS_SA_RESTORER
      nv.restorer = LSE_e2h(LSE_sa_restorer, Linux_bigendian);
#endif
      return nv;
    }
    L_sigaction& operator=(const struct Linux_sighandinf_t &v) {
      LSE_sa_handler = LSE_h2e((LSE_emu_addr_t)v.handler, Linux_bigendian);
      LSE_sa_flags   = LSE_h2e((L_ulong)v.flags, Linux_bigendian);
#ifdef LSE_LINUX_HAS_SA_RESTORER
      LSE_sa_restorer = LSE_h2e((LSE_emu_addr_t)v.restorer, Linux_bigendian);
#else
      LSE_sa_restorer = 0;
#endif
      LSE_sa_mask.fromword(v.sigmask);
    }
  };

  struct L_sigevent {
    union L_sigval {
      int            LSE_sival_int;
      LSE_emu_addr_t LSE_sival_ptr;
    } LSE_sigev_value;
    int LSE_sigev_signo;
    int LSE_sigev_notify;
    union {
      int LSE_pad[(64 - 8 - sizeof(LSE_emu_addr_t))/4];
      int LSE_tid;
      struct {
	LSE_emu_addr_t LSE__function;
	LSE_emu_addr_t LSE__attribute;
      } LSE__sigev_thread;
    } LSE__sigev_un;
  };

  struct L_statfs {
    L_statfs_word     LSE_f_type;     /* 0 */
    L_statfs_word     LSE_f_bsize;     /* 0 */
    L_statfs_word     LSE_f_blocks;     /* 0 */
    L_statfs_word     LSE_f_bfree;     /* 0 */
    L_statfs_word     LSE_f_bavail;     /* 0 */
    L_statfs_word     LSE_f_files;     /* 0 */
    L_statfs_word     LSE_f_ffree;     /* 0 */
    uint32_t                 LSE_pad1[2];   /* actually LSE_f_fsid */
    L_statfs_word     LSE_f_namelen;     /* 0 */
#if defined (__SVR4) && defined(__sun)
    typedef struct statvfs thisstat;
#else
    typedef struct statfs thisstat;
#endif
    
    L_statfs& operator=(const thisstat &buf) {
#if defined (__SVR4) && defined (__sun)
      LSE_f_type   = LSE_h2e((L_statfs_word)0xEF53,Linux_bigendian);
#else
      LSE_f_type   = LSE_h2e((L_statfs_word)buf.f_type,Linux_bigendian);
#endif
      LSE_f_bsize  = LSE_h2e((L_statfs_word)buf.f_type,Linux_bigendian);
      LSE_f_blocks = LSE_h2e((L_statfs_word)buf.f_type,Linux_bigendian);
      LSE_f_bfree  = LSE_h2e((L_statfs_word)buf.f_type,Linux_bigendian);
      LSE_f_bavail = LSE_h2e((L_statfs_word)buf.f_type,Linux_bigendian);
      LSE_f_files  = LSE_h2e((L_statfs_word)buf.f_type,Linux_bigendian);
      LSE_f_ffree  = LSE_h2e((L_statfs_word)buf.f_type,Linux_bigendian);
      LSE_pad1[0] = LSE_pad1[1] = 0;
#if defined (__SVR4) && defined (__sun)
      LSE_f_namelen = LSE_h2e((L_statfs_word)buf.f_namemax,
			      Linux_bigendian);
#else
      LSE_f_namelen = LSE_h2e((L_statfs_word)buf.f_namelen,
			      Linux_bigendian);
#endif
    }
  };

  struct L_stat64 {
    uint64_t LSE_st_dev;
    uint64_t LSE_st_ino;
    uint32_t LSE_st_mode;
    uint32_t LSE_st_nlink;
    uint32_t LSE_st_uid;
    uint32_t LSE_st_gid;
    uint64_t LSE_st_rdev;
    uint64_t LSE_pad1;
    int64_t LSE_st_size;
    int32_t LSE_st_blksize;
    int32_t LSE_pad2;
    int64_t LSE_st_blocks;
    int32_t LSE_st_atime;
    uint32_t LSE_st_atimensec;
    int32_t LSE_st_mtime;
    uint32_t LSE_st_mtimensec;
    int32_t LSE_st_ctime;
    uint32_t LSE_st_ctimensec;
    uint32_t LSE_unused4;
    uint32_t LSE_unused5;
    L_stat64 & operator =(const LSE_stat64 &buf) {
      LSE_st_dev     = LSE_h2e((uint64_t)buf.st_dev, Linux_bigendian);
      LSE_st_ino     = LSE_h2e((uint64_t)buf.st_ino, Linux_bigendian);
      LSE_st_mode    = LSE_h2e((uint32_t)buf.st_mode, Linux_bigendian);
      LSE_st_nlink   = LSE_h2e((uint32_t)buf.st_nlink, Linux_bigendian);
      LSE_st_uid     = LSE_h2e((uint32_t)buf.st_uid, Linux_bigendian);
      LSE_st_gid     = LSE_h2e((uint32_t)buf.st_gid, Linux_bigendian);
      LSE_st_rdev    = LSE_h2e((uint64_t)buf.st_rdev, Linux_bigendian);
      LSE_pad1 = 0;
      LSE_st_size    = LSE_h2e((int64_t)buf.st_size, Linux_bigendian);
      LSE_st_blksize = LSE_h2e((int32_t)buf.st_blksize, Linux_bigendian);
      LSE_pad2 = 0;
      LSE_st_blocks  = LSE_h2e((int64_t)buf.st_blocks, Linux_bigendian);
      LSE_st_atime   = LSE_h2e((int32_t)buf.st_atime, Linux_bigendian);
      LSE_st_atimensec = 0;
      LSE_st_mtime   = LSE_h2e((int32_t)buf.st_mtime, Linux_bigendian);
      LSE_st_mtimensec = 0;
      LSE_st_ctime   = LSE_h2e((int32_t)buf.st_ctime, Linux_bigendian);
      LSE_st_ctimensec = 0;
      LSE_unused4 = 0;
      LSE_unused5 = 0;
    }
  };

  struct L_timespec {
    L_time_t  LSE_tv_sec;
    L_long    LSE_tv_nsec;
    L_timespec& operator=(const struct timespec buf) {
      LSE_tv_sec  = LSE_h2e((L_time_t)buf.tv_sec, Linux_bigendian);
      LSE_tv_nsec = LSE_h2e((L_long)buf.tv_nsec, Linux_bigendian);
    }
    L_timespec e2h() {
      L_timespec nv;
      nv.LSE_tv_sec  = LSE_e2h(LSE_tv_sec, Linux_bigendian);
      nv.LSE_tv_nsec = LSE_e2h(LSE_tv_nsec, Linux_bigendian);
      return nv;
    }
    L_timespec h2e() { return e2h(); }
  };

  struct L_timeval {
    L_time_t      LSE_tv_sec;
    L_suseconds_t LSE_tv_usec;
    L_timeval& operator=(const struct timeval buf) {
      LSE_tv_sec  = LSE_h2e((L_time_t)buf.tv_sec, Linux_bigendian);
      LSE_tv_usec = LSE_h2e((L_suseconds_t)buf.tv_usec, Linux_bigendian);
   }
  };

  struct L_rusage {
    L_timeval LSE_ru_utime;
    L_timeval LSE_ru_stime;
    L_long    LSE_ru_maxrss;
    L_long    LSE_ru_ixrss;
    L_long    LSE_ru_idrss;
    L_long    LSE_ru_isrss;
    L_long    LSE_ru_minflt;
    L_long    LSE_ru_majflt;
    L_long    LSE_ru_nswap;
    L_long    LSE_ru_inblock;
    L_long    LSE_ru_outblock;
    L_long    LSE_ru_msgsnd;
    L_long    LSE_ru_msgrcv;
    L_long    LSE_ru_nsignals;
    L_long    LSE_ru_nvcsw;
    L_long    LSE_ru_nivcsw;
  };

  struct L_termio {
    uint16_t LSE_c_iflag;
    uint16_t LSE_c_oflag;
    uint16_t LSE_c_cflag;
    uint16_t LSE_c_lflag;
    uint8_t  LSE_c_line;
    uint8_t  LSE_c_cc[T::oNCC];
    L_termio & operator=(const struct termio &buf) {
      LSE_c_iflag = LSE_h2e((uint16_t)buf.c_iflag,Linux_bigendian);
      LSE_c_oflag = LSE_h2e((uint16_t)buf.c_oflag,Linux_bigendian);
      LSE_c_cflag = LSE_h2e((uint16_t)buf.c_cflag,Linux_bigendian);
      LSE_c_lflag = LSE_h2e((uint16_t)buf.c_lflag,Linux_bigendian);
      LSE_c_line = buf.c_line;
      if (T::oNCC <= NCC) {
	for (int i=0;i<T::oNCC;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCC;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<T::oNCC;i++) LSE_c_cc[i] = 0;
      }
    }
    struct termio get() {
      struct termio buf;
      buf.c_iflag = LSE_e2h(LSE_c_iflag,Linux_bigendian);
      buf.c_oflag = LSE_e2h(LSE_c_oflag,Linux_bigendian);
      buf.c_cflag = LSE_e2h(LSE_c_cflag,Linux_bigendian);
      buf.c_lflag = LSE_e2h(LSE_c_lflag,Linux_bigendian);
      buf.c_line  = LSE_c_line;
      if (NCC <= T::oNCC) {
	for (int i=0;i<NCC;i++) buf.c_cc[i] = LSE_c_cc[i];
      } else {
	int i;
	for (i=0;i<T::oNCC;i++) buf.c_cc[i] = LSE_c_cc[i];
	for (;i<NCC;i++) buf.c_cc[i] = 0;
      }
      return buf;
    }
  };

  struct L_termios {
    L_tcflag_t LSE_c_iflag;
    L_tcflag_t LSE_c_oflag;
    L_tcflag_t LSE_c_cflag;
    L_tcflag_t LSE_c_lflag;
    L_cc_t     LSE_c_line;
    L_cc_t     LSE_c_cc[T::oNCCS];
#ifdef TCGETS
    L_termios& operator=(const struct termios &buf) {
      LSE_c_iflag = LSE_h2e((L_tcflag_t)buf.c_iflag, Linux_bigendian);
      LSE_c_oflag = LSE_h2e((L_tcflag_t)buf.c_oflag, Linux_bigendian);
      LSE_c_cflag = LSE_h2e((L_tcflag_t)buf.c_cflag, Linux_bigendian);
      LSE_c_lflag = LSE_h2e((L_tcflag_t)buf.c_lflag, Linux_bigendian);
#ifdef __linux
      LSE_c_line = buf.c_line;
#else
      LSE_c_line = 0; // N_TTY
#endif
      if (T::oNCCS <= NCCS) {
	for (int i=0;i<T::oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<T::oNCCS;i++) LSE_c_cc[i] = 0;
      }
    }
#elif defined(TCGETA)
    L_termios& operator=(const struct termio &buf) {
      LSE_c_iflag = LSE_h2e((L_tcflag_t)buf.c_iflag, Linux_bigendian);
      LSE_c_oflag = LSE_h2e((L_tcflag_t)buf.c_oflag, Linux_bigendian);
      LSE_c_cflag = LSE_h2e((L_tcflag_t)buf.c_cflag, Linux_bigendian);
      LSE_c_lflag = LSE_h2e((L_tcflag_t)buf.c_lflag, Linux_bigendian);
#ifdef __linux
      LSE_c_line = buf.c_line;
#else
      LSE_c_line = 0; // N_TTY
#endif
      if (T::oNCCS <= NCCS) {
	for (int i=0;i<T::oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<T::oNCCS;i++) LSE_c_cc[i] = 0;
      }
    }
#endif
  };

  struct L_tms {
    L_clock_t     LSE_tms_utime;
    L_clock_t     LSE_tms_stime;
    L_clock_t     LSE_tms_cutime;
    L_clock_t     LSE_tms_cstime;
    L_tms& operator=(const struct tms buf) {
      LSE_tms_utime  = LSE_h2e((L_clock_t)buf.tms_utime, Linux_bigendian);
      LSE_tms_stime  = LSE_h2e((L_clock_t)buf.tms_stime, Linux_bigendian);
      LSE_tms_cutime = LSE_h2e((L_clock_t)buf.tms_cutime, Linux_bigendian);
      LSE_tms_cstime = LSE_h2e((L_clock_t)buf.tms_cstime, Linux_bigendian);
    }
  };

  // And now compat stuff
#ifdef LSE_LINUX_COMPAT
  typedef typename T::L_compat_clock_t     L_compat_clock_t;
  typedef typename T::L_compat_long        L_compat_long;
  typedef typename T::L_compat_size_t      L_compat_size_t;
  typedef typename T::L_compat_suseconds_t L_compat_suseconds_t;
  typedef typename T::L_compat_time_t      L_compat_time_t;
  typedef typename T::L_compat_ulong       L_compat_ulong;
  typedef typename T::L_compat_uptr_t      L_compat_uptr_t;
  static const int ocompat__NSIG = T::ocompat__NSIG;

  struct L_compat_iovec {
    L_compat_uptr_t iov_base;
    L_compat_size_t iov_len;
  };

  struct L_compat_rlimit {
    L_compat_ulong    LSE_rlim_cur; /* 0 */
    L_compat_ulong    LSE_rlim_max; /* 8 */
    L_compat_rlimit& operator=(const struct rlimit &buf) {
      LSE_rlim_cur = LSE_h2e((L_compat_ulong)buf.rlim_cur, Linux_bigendian);
      LSE_rlim_max = LSE_h2e((L_compat_ulong)buf.rlim_max, Linux_bigendian);
    }
    struct rlimit get() {
      struct rlimit buf;
      buf.rlim_cur = LSE_e2h(LSE_rlim_cur,Linux_bigendian);
      buf.rlim_max = LSE_e2h(LSE_rlim_max,Linux_bigendian);
      return buf;
    }
  };

  struct L_compat_robust_list_head {
    L_compat_uptr_t LSE_head_next;
    L_compat_long   LSE_foffset;
    L_compat_uptr_t LSE_op_pending;
    L_compat_robust_list_head e2h() {
      L_compat_robust_list_head nb;
      nb.LSE_head_next  = LSE_h2e(LSE_head_next, Linux_bigendian);
      nb.LSE_foffset    = LSE_h2e(LSE_foffset, Linux_bigendian);
      nb.LSE_op_pending = LSE_h2e(LSE_op_pending, Linux_bigendian);
      return nb;
    }
  };

  struct L_compat_robust_list {
    L_compat_uptr_t LSE_next;
    L_compat_robust_list e2h() {
      L_compat_robust_list nb;
      nb.LSE_next  = LSE_h2e(LSE_next, Linux_bigendian);
      return nb;
    }
  };

  struct L_compat_sigval_t {
    int32_t         ival;
    L_compat_uptr_t pval;
  };

  struct L_compat_timeval {
    L_compat_time_t      LSE_tv_sec;
    L_compat_suseconds_t LSE_tv_usec;
    L_timeval& operator=(const struct timeval buf) {
      LSE_tv_sec  = LSE_h2e((L_compat_time_t)buf.tv_sec, Linux_bigendian);
      LSE_tv_usec = LSE_h2e((L_compat_suseconds_t)buf.tv_usec, 
			    Linux_bigendian);
   }
  };

  struct L_compat_rusage {
    L_compat_timeval LSE_ru_utime;
    L_compat_timeval LSE_ru_stime;
    L_compat_long    LSE_ru_maxrss;
    L_compat_long    LSE_ru_ixrss;
    L_compat_long    LSE_ru_idrss;
    L_compat_long    LSE_ru_isrss;
    L_compat_long    LSE_ru_minflt;
    L_compat_long    LSE_ru_majflt;
    L_compat_long    LSE_ru_nswap;
    L_compat_long    LSE_ru_inblock;
    L_compat_long    LSE_ru_outblock;
    L_compat_long    LSE_ru_msgsnd;
    L_compat_long    LSE_ru_msgrcv;
    L_compat_long    LSE_ru_nsignals;
    L_compat_long    LSE_ru_nvcsw;
    L_compat_long    LSE_ru_nivcsw;
  };

  struct L_compat_sigset { 
    // note: the number of signals could actually vary from arch to arch,
    // but I do not see evidence that it actually does _NSIG_WORDS
    static const int o_NSIG_WORDS =ocompat__NSIG/ (8 * sizeof(L_compat_ulong));
    L_compat_ulong sig[o_NSIG_WORDS];

    uint64_t toword() {
      uint64_t v = 0;
      if (Linux_bigendian) { // big-endian target
	for (int i = 0; i < o_NSIG_WORDS; ++i)
	  v = (v << (o_NSIG_WORDS > 1 ? (8 * sizeof(L_compat_ulong)) : 0))
	    | LSE_e2h(sig[i], Linux_bigendian);
      } else {
	for (int i = o_NSIG_WORDS-1; i >= 0; --i)
	  v = (v << (o_NSIG_WORDS > 1 ? (8 * sizeof(L_compat_ulong)) : 0))
	    | LSE_e2h(sig[i], Linux_bigendian);
      }
      return v;
    }
    void fromword(uint64_t v) { 
      if (Linux_bigendian) { // big-endian target
	for (int i = 0; i < o_NSIG_WORDS; ++i) {
	  sig[i] = LSE_h2e((L_compat_ulong)v, Linux_bigendian);
	  v >= (o_NSIG_WORDS > 1 ? (8 * sizeof(L_compat_ulong)) : 0);
	}
      } else {
	for (int i = o_NSIG_WORDS - 1; i >= 0; --i) {
	  sig[i] = LSE_h2e((L_compat_ulong)v, Linux_bigendian);
	  v >= (o_NSIG_WORDS > 1 ? (8 * sizeof(L_compat_ulong)) : 0);
	}
      }
    }   

  };

  struct L_compat_sigaction {
    L_compat_uptr_t LSE_sa_handler;
    L_compat_ulong  LSE_sa_flags;
#ifdef LSE_LINUX_HAS_SA_RESTORER
    L_compat_uptr_t LSE_sa_restorer;
#endif
    L_compat_sigset  LSE_sa_mask;
    inline struct Linux_sighandinf_t tosighand() {
      Linux_sighandinf_t nv;
      nv.handler = LSE_e2h(LSE_sa_handler, Linux_bigendian);
      nv.flags   = LSE_e2h(LSE_sa_flags, Linux_bigendian);
      nv.sigmask = LSE_sa_mask.toword();
      return nv;
    }
    L_sigaction& operator=(const struct Linux_sighandinf_t &v) {
      LSE_sa_handler = LSE_h2e((L_compat_uptr_t)v.handler, Linux_bigendian);
      LSE_sa_flags   = LSE_h2e((L_compat_ulong)v.flags, Linux_bigendian);
#ifdef LSE_LINUX_HAS_SA_RESTORER
      LSE_sa_restorer = 0;
#endif
      LSE_sa_mask.fromword(v.sigmask);
    }
  };

  struct L_compat_timespec {  // may not be right as it is not yet used
    L_compat_time_t  LSE_tv_sec;
    L_compat_long    LSE_tv_nsec;
    L_compat_timespec& operator=(const struct timespec buf) {
      LSE_tv_sec  = LSE_h2e((L_compat_time_t)buf.tv_sec, Linux_bigendian);
      LSE_tv_nsec = LSE_h2e((L_long)buf.tv_nsec, Linux_bigendian);
    }
    L_compat_timespec e2h() {
      L_compat_timespec nv;
      nv.LSE_tv_sec  = LSE_e2h(LSE_tv_sec, Linux_bigendian);
      nv.LSE_tv_nsec = LSE_e2h(LSE_tv_nsec, Linux_bigendian);
      return nv;
    }
    L_compat_timespec h2e() { return e2h(); }
  };

  struct L_compat_tms {
    L_compat_clock_t     LSE_tms_utime;
    L_compat_clock_t     LSE_tms_stime;
    L_compat_clock_t     LSE_tms_cutime;
    L_compat_clock_t     LSE_tms_cstime;
    L_compat_tms& operator=(const struct tms buf) {
      LSE_tms_utime  = LSE_h2e((L_compat_clock_t)buf.tms_utime, 
			       Linux_bigendian);
      LSE_tms_stime  = LSE_h2e((L_compat_clock_t)buf.tms_stime, 
			       Linux_bigendian);
      LSE_tms_cutime = LSE_h2e((L_compat_clock_t)buf.tms_cutime, 
			       Linux_bigendian);
      LSE_tms_cstime = LSE_h2e((L_compat_clock_t)buf.tms_cstime,
			       Linux_bigendian);
    }
  };

#endif // LSE_LINUX_COMPAT
};

#endif // LSE_LINUX_BASE

#ifdef LSE_LINUX_CODE
#undef LSE_LINUX_CODE

// *****************************************************************
// ********** Memory accessors *************************************
// *****************************************************************

#ifdef LSE_LINUX_USE_DEVMEM

#define CC_mem_memset(m,a,v,l) devmem_memset(m,a,v,l)
#define CC_mem_memcmp(m,a,l,b,p) devmem_memcmp(m,a,l,b,p)
#define CC_mem_read(m,a,l,h) devmem_read(m,a,l,(CC_mem_data_t *)(h))
#define CC_mem_write(m,a,l,h) devmem_write(m,a,l,(CC_mem_data_t *)(h))
#define CC_mem_translate(m,a,lp,hp) devmem_translate(m,a,lp,hp)
#define CC_mem_destroy(m) devmem_destroy(m)
#define CC_mem_addmap(m,a,l) devmem_addmap(m,a,l)
#define CC_mem_delmap(m,a,l,rr) devmem_delmap(m,a,l,rr)
#define CC_mem_data_t LSE_device::devdata_t
#define CC_mem_addr_t LSE_device::devaddr_t
#define CC_mem_error_t LSE_device::deverror_code_t
#define CC_memory_t LSE_device::device_t

  inline void devmem_destroy(LSE_device::device_t *memp) {
    memp->decr();
  }

  inline CC_mem_error_t devmem_addmap(LSE_device::device_t *memp, 
				      CC_mem_addr_t addr,
				      CC_mem_addr_t len) 
  {
    try {
      memp->addchild(addr, len, 0);
    } catch (LSE_device::deverror_t e) {
      return e.err;
    }
    return LSE_device::deverror_none;
  }

  inline CC_mem_error_t devmem_delmap(LSE_device::device_t *memp, 
				      CC_mem_addr_t addr,
			              CC_mem_addr_t len, bool release) 
  {
    try {
      memp->delchild(addr, len);
    } catch (LSE_device::deverror_t e) {
      return e.err;
    }
    return LSE_device::deverror_none;
  }

  inline const char *devmem_errstring(CC_mem_error_t eno) {
    return LSE_device::errstring(eno);
  }

  inline CC_mem_error_t devmem_memset(LSE_device::device_t *memp, 
				      CC_mem_addr_t addr,
				      int value, CC_mem_addr_t len) 
  {
    try {
      memp->memset(addr, value, len);
    } catch (LSE_device::deverror_t e) {
      return e.err;
    }
    return LSE_device::deverror_none;
  }

  inline CC_mem_error_t devmem_memcmp(LSE_device::device_t *memp, 
				      CC_mem_addr_t addr, 
                                      CC_mem_addr_t len,
				      CC_mem_data_t *buffer, int *res)
  {
    try {
      *res = memp->memcmp(addr, buffer, len);
    } catch (LSE_device::deverror_t e) {
      return e.err;
    }
    return LSE_device::deverror_none;
  }

 inline CC_mem_error_t devmem_write(LSE_device::device_t *memp, 
				    CC_mem_addr_t addr, 
                                    CC_mem_addr_t len, CC_mem_data_t *buffer)
  {
    try {
      memp->write(addr, buffer, len);
    } catch (LSE_device::deverror_t e) {
      return e.err;
    }
    return LSE_device::deverror_none;
  }


 inline CC_mem_error_t devmem_read(LSE_device::device_t * memp, 
				   CC_mem_addr_t addr, 
                                   CC_mem_addr_t len, CC_mem_data_t *buffer)
  {
    try {
      memp->read(addr, buffer, len);
    } catch (LSE_device::deverror_t e) {
      return e.err;
    }
    return LSE_device::deverror_none;
  }

 inline CC_mem_error_t devmem_translate(LSE_device::device_t *memp, 
					CC_mem_addr_t addr,
				        CC_mem_addr_t *lenp,
				        CC_mem_data_t **hostaddrp)
  {
    try {
      CC_mem_addr_t mlen;
      mlen = memp->translate(addr, hostaddrp);
      if (lenp) *lenp = mlen;
    } catch (LSE_device::deverror_t e) {
      return e.err;
    }
    return LSE_device::deverror_none;
  }

#endif // LSE_LINUX_USE_DEVMEM

#define OS_report_results(x1,x2)    \
 do {                               \
    CC_sys_set_return(realct,0,x1); \
    CC_sys_set_return(realct,1,x2); \
  } while (0)

#define OS_report_err(x1)           \
 do {                               \
    CC_sys_set_return(realct,0,-1); \
    CC_sys_set_return(realct,1,x1); \
    goto syserr;                    \
  } while(0)

#define LSE_LINUX_NAME(s) Linux_s_ ## s
#define LSE_LINUX_PARMS int callno,		\
	  CC_isacontext_t *realct,		\
	  LSE_emu_iaddr_t curr_pc,		\
	  int changelen
#define LSE_LINUX_SIG(s) int LSE_LINUX_NAME(s)(LSE_LINUX_PARMS)
#define LSE_LINUX_ARGS callno,realct,curr_pc,changelen

#define OS_sys_get_return(x)    CC_sys_get_return(realct,x)
#define OS_ict_osinfo(x)        ((Linux_context_t *)(CC_ict_osinfo(x)))
#define OS_sys_return()         return osret;
#define OS_syserr_handler()     return osret;

#ifndef CC_ict_true_startaddr
#define CC_ict_true_startaddr(x) CC_ict_startaddr(x)
#endif
#ifndef CC_mem_redirect
#define CC_mem_redirect(x,y)
#endif

#ifndef CC_context_has_no_users
#define CC_context_has_no_users(x) (false)
#endif

#ifndef CC_hook_pipe
#define CC_hook_pipe()
#endif
#ifndef CC_hook_pipe2
#define CC_hook_pipe2()
#endif
#ifndef CC_dlinfo
#define CC_dlinfo()
#endif
#ifndef CC_contextdecl_hook
#define CC_contextdecl_hook()
#endif
#ifndef CC_contextinit_hook
#define CC_contextinit_hook()
#endif
#ifndef CC_context_map_update
#define CC_context_map_update(c,p2,p3) ((LSE_emu_update_context_map)(p2,p3))
#endif
#ifndef CC_psect_hook
#define CC_psect_hook()
#endif

// Why do we need this?  Host locale files may not have the right 
// endianness, so we have to fool the program into thinking they do not 
// exist!
//   fname = file name to test
// Must return true if the file is to be ignored
#ifndef CC_open_filter
#define CC_open_filter(fname) (false)
#endif

#ifndef CC_execveguts
#define CC_execveguts()
#endif
#ifndef CC_cloneguts
#define CC_cloneguts()
#endif
#ifndef CC_init_hook
#define CC_init_hook(ifc)
#endif
#ifndef CC_finish_hook
#define CC_finish_hook(ifc)
#endif

#ifndef CC_time_get_currtime
#define CC_time_get_currtime(x) 0
#endif
#ifndef CC_time_schedule_wakeup
#define CC_time_schedule_wakeup(t,f,p)
#endif
#ifndef CC_time_request_interrupt
#define CC_time_request_interrupt(os,n) (true)
#endif

// CC_chkpt_cntl_clear
//   arguments
//   $1: control structure
//
// CC_ict_write_chkpt
//   arguments
//     $1 : CC_isacontext_t *
//     $2 : LSE_emu_chkpt_cntl_t *
//   context to expect: 
//     LSE_chkpt::file_t *cpFile;
//     int nmark; // marker for what has been written so far.
//     on error, simply return the error code

// CC_ict_parse_chkpt
//   arguments
//     $1: CC_icacontext_t *& (may be null for new context)
//   context to expect: 
//     LSE_emu_interface_t *intr;
//     Linux_dinst_t *di;
//     int step;
//     LSE_emu_chkpt_cntl_t *ctl;
//     LSE_chkpt::file_t *cptFile;
//     LSE_chkpt::data_t *tdp; // pointer to the header for the record
//     On error, simply return the error code

static inline bool end_switched() {
  return (Linux_bigendian ^ LSE_end_is_big);
}

static int Linux_check_addr_len(CC_isacontext_t *realct, LSE_emu_addr_t va, 
				LSE_emu_addr_t &mlen);
static bool OS_strncpy_t2h_int(CC_isacontext_t *realct,
			       char *targ, LSE_emu_addr_t src, 
			       unsigned int size);
void Linux_schedule(LSE_emu_interface_t *);
static bool block_thread(Linux_context_t *osct, bool doschedule=true);
static bool ready_thread(Linux_context_t *osct, bool doschedule=true);

typedef std::list<std::pair<LSE_emu_addr_t, LSE_emu_addr_t> > addr_addr_t;

enum Linux_objkind { 
  // used for a lower-cost typeinfo alternative without virtual calls
  obj_unknown, obj_context, obj_sighandtable, obj_tgroup, obj_pgroup,
  obj_futex, obj_fdtable, obj_errorfile, obj_nativefile, obj_pipefile,
  obj_socketfile, obj_pipe, obj_memory, obj_anonfile, obj_sigtimer, 
  obj_wutimer, obj_reschedtimer, obj_fs
};

// Base for serializable and sharable classes.
class Linux_obj_t {
public:
  int refcount;
  class Linux_dinst_t *os;
  uint64_t oid;
  int mark;
  enum Linux_objkind kind;

  Linux_obj_t(class Linux_dinst_t *o);
  Linux_obj_t(class Linux_dinst_t *o, enum Linux_objkind k);
#ifdef LSE_LINUX_CHECKPOINT
  // variant for setting the ID directly from checkpoints
  Linux_obj_t(uint64_t id, class Linux_dinst_t *o, enum Linux_objkind k);
#endif
  virtual ~Linux_obj_t();
  int decr(bool dodel = true) { 
    int c = --refcount; 
    //std::cerr << "D " << refcount << " " << oid << " " << kind << "\n";
    if (dodel && !refcount) delete this; 
    return c; 
  }
  int incr() { 
    //std::cerr << "I " << (refcount+1) << " " << oid << " " << kind << "\n";
    return ++refcount; 
  }
  int &myerrno();
};

// Linux_memory_t: represents a virtual address space

class Linux_memory_t;

class pagemapper_t {
public:
  class Linux_memory_t *PM;
  pagemapper_t(class Linux_memory_t *m) : PM(m) {}
  LSE_device::devaddr_t demand(LSE_device::devaddr_t);
};

typedef LSE_device::translation<OSDefs::oPAGE_BITS, pagemapper_t> vmtrans_t;

struct inode_id_t {
  dev_t dev;
  ino_t ino;
  inode_id_t(dev_t d, ino_t i) : dev(d), ino(i) {}
  inode_id_t() : dev(0), ino(0) {}
  bool operator<(inode_id_t o) const {
    return dev < o.dev || dev==o.dev && ino < o.ino;
  }
};

static int do_msync(struct Linux_context_t *osct, 
		    LSE_emu_addr_t start, LSE_emu_addr_t mlen,
		    bool errorifunmapped);

static int do_munmap(struct Linux_context_t *osct, 
		     LSE_emu_addr_t start, LSE_emu_addr_t mlen);

class Linux_memory_t : public Linux_obj_t {
public:
  LSE_emu_addr_t heapstart, heapbreak;
  std::list<std::pair<LSE_emu_addr_t, LSE_emu_addr_t> > codelocs;
  pagemapper_t pmapper;
  vmtrans_t vmdevice;

  class range_t {
  public:
    LSE_emu_addr_t start, end;
    int flags;
    bool isShared;
    class Linux_file_t *fp;
    LSE_emu_addr_t offset;
  };
  
  // NOTE: we key this by the end address for easy top-down search?
  typedef std::map<LSE_emu_addr_t, range_t> ranges_t;
  ranges_t vranges;

  Linux_memory_t(class Linux_dinst_t *o) : Linux_obj_t(o, obj_memory),
					   pmapper(this),
					   vmdevice(&pmapper) { }

  Linux_memory_t(class Linux_dinst_t *o, Linux_memory_t *p) 
  : Linux_obj_t(o, obj_memory), heapstart(p->heapstart), 
    heapbreak(p->heapbreak), pmapper(this), vmdevice(&pmapper) {
    vranges.insert(p->vranges.begin(), p->vranges.end());
    vmdevice.subspace = p->vmdevice.subspace;
    copy_space(p);
  }
  ~Linux_memory_t();
#ifdef LSE_LINUX_CHECKPOINT
  Linux_memory_t(uint64_t id, class Linux_dinst_t *o) 
    : Linux_obj_t(id, o, obj_memory), pmapper(this), vmdevice(&pmapper) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di,
					LSE_chkpt::file_t *cptFile,
					LSE_chkpt::data_t *tdp);
#endif
private:
  void copy_space(Linux_memory_t *p);
};

struct Linux_timer_t : public Linux_obj_t { // describes a timer
  enum sigaction_kind { sigaction_kind_signal };
  sigaction_kind kind;

  int clockid;  // which clock
  struct itval {
    uint64_t interval;
    uint64_t expiration;
  } tval;

  struct todo_timer *todo;

  Linux_timer_t(class Linux_dinst_t *o, Linux_objkind lo, 
		sigaction_kind k, int c) 
   : Linux_obj_t(o,lo), kind(k), clockid(c), todo(0)
    { tval.expiration = tval.interval = 0; }

  virtual ~Linux_timer_t();
  void planfuture(uint64_t);
  void settime(struct itval *newp, struct itval *oldp);
  void expired(uint64_t);
  virtual void fire(uint64_t) = 0;
  void remove_todo();
#ifdef LSE_LINUX_CHECKPOINT
  virtual LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark) = 0;
protected:
  Linux_timer_t(uint64_t id, class Linux_dinst_t *o, Linux_objkind lo) 
    : Linux_obj_t(id, o, lo), todo(0) {}
  void build_chkpt_piece(LSE_chkpt::data_t *tdp);
  LSE_chkpt::error_t parse_chkpt_piece(class Linux_dinst_t *di,
				       LSE_chkpt::data_t *&tdp);
#endif
};

struct Linux_sig_timer_t : public Linux_timer_t {
  int signo;
  struct Linux_context_t *target;
  struct Linux_tgroup_t *tgroup;
  Linux_sig_timer_t(class Linux_dinst_t *o, int c, struct Linux_tgroup_t *tg,
		    int s, struct Linux_context_t *t);
  void fire(uint64_t);
  ~Linux_sig_timer_t();
#ifdef LSE_LINUX_CHECKPOINT
  Linux_sig_timer_t(uint64_t id, class Linux_dinst_t *o) :
    Linux_timer_t(id, o, obj_sigtimer), target(0), tgroup(0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di,
					LSE_chkpt::data_t *tdp);
#endif
};

struct Linux_wu_timer_t : public Linux_timer_t {
  struct Linux_context_t *target;
  Linux_wu_timer_t(class Linux_dinst_t *o, int c, struct Linux_context_t *t);
  void fire(uint64_t);
  ~Linux_wu_timer_t();
#ifdef LSE_LINUX_CHECKPOINT
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di,
					LSE_chkpt::data_t *tdp);
  Linux_wu_timer_t(uint64_t id, class Linux_dinst_t *o) :
    Linux_timer_t(id, o, obj_wutimer), target(0) {}
#endif
};

struct Linux_reschedule_timer_t : public Linux_timer_t {
  Linux_reschedule_timer_t(class Linux_dinst_t *o, int c);
  void fire(uint64_t);
  ~Linux_reschedule_timer_t();
#ifdef LSE_LINUX_CHECKPOINT
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di,
					LSE_chkpt::data_t *tdp);
  Linux_reschedule_timer_t(uint64_t id, class Linux_dinst_t *o) :
    Linux_timer_t(id, o, obj_reschedtimer) {}
#endif
};

class Linux_context_t;


class Linux_file_t : public Linux_obj_t { // describes a file
public:
  int status;
  bool OSblocking;
  Linux_file_t(class Linux_dinst_t *o, Linux_objkind k) 
    : Linux_obj_t(o,k), status(0), OSblocking(false) { refcount = 0; }

  virtual ~Linux_file_t() {}
  int close();
  int doerr() { myerrno() = (OSDefs::oEBADFD); return -1; }
  virtual int fchmod(int) { return doerr(); }
  virtual int fcntl(int, OSDefs::L_long) { return doerr(); }
  virtual int fchown(uid_t o, gid_t g) { return doerr(); }
  virtual int fstat(struct stat *b) { return doerr(); }
#ifdef __USE_LARGEFILE64
  virtual int fstat64(struct stat64 *b) { return doerr(); }
#endif
  virtual int ftruncate(int64_t) { return doerr(); }
#ifdef TCGETS
  virtual int ioctl(int, struct termios *t) { return doerr(); }
#endif
#if defined(TCGETA) || defined(TCSETAW)
  virtual int ioctl(int, struct termio *t) { return doerr(); }
#endif
  virtual int lseek(off_t, int) { return doerr(); }
#ifdef HAS__LLSEEK
  virtual int _llseek(off_t, off_t, loff_t *, int) { return doerr(); }
#endif
  virtual int readv(Linux_context_t *, struct iovec *, int) { return doerr(); }
  virtual int writev(Linux_context_t *, struct iovec *, int) { return doerr(); }
  virtual short poll() { return OSDefs::oPOLLNVAL; }
  virtual int doClose() { return 0; }
  virtual int doRealClose() { return 0; }
  virtual int getRealFD() { return -1; }
  virtual void register_poll_waiter(struct cont_poll *) {}
  virtual void unregister_poll_waiter(struct cont_poll *) {}
  virtual inode_id_t getInodeID() {
    inode_id_t res(0,0);
    return res;
  }
  int read(Linux_context_t *c, void *p, size_t l) {
    struct iovec v = { p, l };
    return readv(c, &v, 1);
  }
  int write(Linux_context_t *c, void *p, size_t l) {
    struct iovec v = { p, l };
    return writev(c, &v, 1);
  }
  bool readingCheckpoint();
#ifdef LSE_LINUX_CHECKPOINT
  Linux_file_t(uint64_t id, class Linux_dinst_t *o, Linux_objkind k) 
    : Linux_obj_t(id, o, k), status(0), OSblocking(false) { }
  virtual LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark) {
    return LSE_chkpt::error_None; 
  }
#endif
};

int Linux_file_t::close() {
  int ret = doClose();
  if (!decr(false)) {
    ret = doRealClose();
    delete this;
  }
  return ret;
}

class Linux_error_file_t : public Linux_file_t {
public:
  Linux_error_file_t(class Linux_dinst_t *o) 
    : Linux_file_t(o, obj_errorfile) {} 
#ifdef LSE_LINUX_CHECKPOINT
  Linux_error_file_t(uint64_t id, class Linux_dinst_t *o) 
    : Linux_file_t(id, o, obj_errorfile) {} 
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

class Linux_native_file_t : public Linux_file_t {
public:
  struct stat statbuf;
  int nativefd;
  Linux_native_file_t(class Linux_dinst_t *o, int ofd) 
    : Linux_file_t(o, obj_nativefile),nativefd(ofd) { 
    OSblocking = true;
    ::fstat(ofd, &statbuf);
  }
  ~Linux_native_file_t() { doRealClose(); }
  int fchmod(int m) { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::fchmod(nativefd, m);
      myerrno() = errno;
    }
    return ret;
  }
  int fchown(uid_t o, gid_t g) { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::fchown(nativefd, o, g);
      myerrno() = errno;
    }
    return ret;
  }
  int fcntl(int c, OSDefs::L_long a) { 
    int ret=0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::fcntl(nativefd, c, (long)a); 
      myerrno() = errno;
    }
    return ret;
  }
  int fstat(struct stat *b) { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::fstat(nativefd, b); 
      myerrno() = errno;
    }
    return ret;
  }
#ifdef __USE_LARGEFILE64
  int fstat64(struct stat64 *b) { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::fstat64(nativefd, b); 
      myerrno() = errno;
    }
    return ret;
  }
#endif
#ifdef TCGETS
  int ioctl(int c, struct termios *t) { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::ioctl(nativefd, c, t); 
      myerrno() = errno;
    }
    return ret;
  }
#endif
#if defined(TCGETA) || defined(TCSETAW)
  int ioctl(int c, struct termio *t) { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::ioctl(nativefd, c, t); 
      myerrno() = errno;
    }
    return ret;
   }
#endif
  int lseek(off_t o, int w) {
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::lseek(nativefd, o, w);
      myerrno() = errno;
    } 
    return ret;
  }
#ifdef HAS__LLSEEK
  int _llseek(off_t h, off_t l, loff_t *r, int w) {
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::_llseek(nativefd, h, l, &res, w); 
      osct->myerrno() = errno;
    }
    return ret;
  }
#endif
  int readv(Linux_context_t *, struct iovec *b, int c) {
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::readv(nativefd, b, c);
      myerrno() = errno;
    }
    return ret;
  }
  int writev(Linux_context_t *osct, struct iovec *b, int c) {
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::writev(nativefd, b, c);
      myerrno() = errno;
    }
    return ret;
  }
  short ready() { 
      struct pollfd pfd = { nativefd, ~0, 0 };
      ::poll(&pfd, 1, 0); // do not block
      return pfd.revents;
    }
  int doRealClose() { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::close(nativefd);
      myerrno() = errno;
      nativefd = -1; // so I do not close again
    }
    return ret;
   }
  int getRealFD() { return nativefd; }
  int ftruncate(int64_t o) { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
#ifdef __USE_LARGEFILE64
      ret = ::ftruncate64(nativefd, o);
#else
      ret = ::ftruncate(nativefd, (off_t)o);
#endif
      myerrno() = errno;
    }
    return ret;
  }
  inode_id_t getInodeID() {
    return inode_id_t(statbuf.st_dev, statbuf.st_ino);
  }

#ifdef LSE_LINUX_CHECKPOINT
  Linux_native_file_t(uint64_t id, class Linux_dinst_t *o, int ofd) 
    : Linux_file_t(id, o, obj_nativefile),nativefd(ofd) { 
    OSblocking = true;
    ::fstat(ofd, &statbuf);
  }
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

class Linux_anon_file_t : public Linux_file_t {
  struct stat statbuf;
public:
  ino_t ino;
  Linux_anon_file_t(class Linux_dinst_t *o, ino_t i) 
    : Linux_file_t(o, obj_anonfile),ino(i) { 
  }
  int fstat(struct stat *b) { 
    b->st_dev = 0;
    b->st_ino = ino;
    return 0;
  }
  int lseek(off_t o, int w) { return 0; }
#ifdef HAS__LLSEEK
  int _llseek(off_t h, off_t l, loff_t *r, int w) { return 0; }
#endif
  int readv(Linux_context_t *, struct iovec *b, int c) { return 0; }
  int writev(Linux_context_t *osct, struct iovec *b, int c) { return 0; }
#ifdef LSE_LINUX_CHECKPOINT
  Linux_anon_file_t(uint64_t id, class Linux_dinst_t *o, ino_t i) 
    : Linux_file_t(id, o, obj_anonfile),ino(i) { 
  }
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

class Linux_pipe_t : public Linux_obj_t { // NOTE: I never block on writes here!
public:
  const static int bufsize=256;
  int numwriters, numreaders;
  std::string buf;  // ought really to be a stringbuf? but I cannot make it work
  std::deque<class cont_piperead *> waiters;

  Linux_pipe_t(class Linux_dinst_t *o) 
    : numwriters(0), numreaders(0), Linux_obj_t(o, obj_pipe) { refcount = 0; }
  ~Linux_pipe_t();
  int readv(Linux_context_t *, struct iovec *b, int c);
  int writev(Linux_context_t *, struct iovec *b, int c);
  void wakeup_readers();
#ifdef LSE_LINUX_CHECKPOINT
  Linux_pipe_t(uint64_t id, class Linux_dinst_t *o) 
    : Linux_obj_t(id, o, obj_pipe) { }
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

class Linux_pipe_file_t : public Linux_file_t {
public:
  Linux_pipe_t *mypipe;
  bool isWriter;

  Linux_pipe_file_t(class Linux_dinst_t *o, Linux_pipe_t *p, bool dir) 
    : mypipe(p), isWriter(dir), Linux_file_t(o, obj_pipefile) {
    p->incr();
    if (dir) {
      p->numwriters++;
      status = OSDefs::oO_WRONLY;
    } else {
      p->numreaders++;
      status = OSDefs::oO_RDONLY;
    }
  }
  ~Linux_pipe_file_t() {
    if (isWriter) {
      mypipe->numwriters--;
      if (!mypipe->numwriters) mypipe->wakeup_readers();
    }
    else mypipe->numreaders--;
    mypipe->decr();
  }
  int readv(Linux_context_t *osct, struct iovec *b, int c) {
    if (isWriter) { myerrno() = (OSDefs::oEBADFD); return -1; }
    else return mypipe->readv(osct, b, c);
  }
  int writev(Linux_context_t *osct, struct iovec *b, int c) {
    if (!isWriter) { myerrno() = (OSDefs::oEBADFD); return -1; }
    else return mypipe->writev(osct, b, c);
  }
  short poll() {
    short r = 0;
    if (isWriter) { 
      r = OSDefs::oPOLLOUT | OSDefs::oPOLLWRNORM;
      if (!mypipe->numreaders) r |= OSDefs::oPOLLERR;
    } else {
      if (mypipe->buf.size()) r = OSDefs::oPOLLIN | OSDefs::oPOLLRDNORM;
      if (!mypipe->numwriters) r |= OSDefs::oPOLLHUP;
    }
    return r;
  }
  int fcntl(int c, OSDefs::L_long a);
  int fstat(struct stat *b);
  int lseek(off_t, int) { 
    myerrno() = OSDefs::oESPIPE; return -1; 
  }
  void register_poll_waiter(class cont_poll *);
  void unregister_poll_waiter(class cont_poll *);
#ifdef LSE_LINUX_CHECKPOINT
  Linux_pipe_file_t(uint64_t id, class Linux_dinst_t *o) 
    : Linux_file_t(id, o, obj_pipefile) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

class Linux_socket_file_t : public Linux_file_t {
public:
  int nativefd;
  Linux_socket_file_t(class Linux_dinst_t *o, int ofd) 
    : nativefd(ofd), Linux_file_t(o, obj_socketfile)  {}
  int readv(Linux_context_t *osct, struct iovec *b, int c) {
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::readv(nativefd, b, c);
      myerrno() = errno;
    }
    return ret;
  }
  int writev(Linux_context_t *osct, struct iovec *b, int c) {
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::writev(nativefd, b, c);
      myerrno() = errno;
    }
    return ret;
  }
  int doRealClose() { 
    int ret = 0;
    if (nativefd >= 0 && !readingCheckpoint()) {
      ret = ::close(nativefd);
      myerrno() = errno;
      nativefd = -1; // so I do not close again
    }
    return ret;
   }
#ifdef LSE_LINUX_CHECKPOINT
  Linux_socket_file_t(uint64_t id, class Linux_dinst_t *o, int ofd) 
    : nativefd(ofd), Linux_file_t(id, o, obj_socketfile) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

class Linux_fd_t {
public:
   Linux_file_t *fp;
   int flags; 
   Linux_fd_t() : fp(0), flags(0) {}
   Linux_fd_t(Linux_file_t *p, int f) : fp(p), flags(f) {}
};

class Linux_fdtable_t : public Linux_obj_t { // a table of file descriptors
public:
  typedef std::map<int, Linux_fd_t> fds_t;
  fds_t fds;

  Linux_fdtable_t(class Linux_dinst_t *o); 
  Linux_fdtable_t(class Linux_dinst_t *d, Linux_fdtable_t *o); 

  int findNextOpen(int first=0) {
    int j = 0;
    for (std::map<int, Linux_fd_t>::iterator
         i=fds.begin(), f=fds.end(); i != f ; ++i, ++j) {
      if (i->first < first) { j = i->first; continue; }
      if (i->first != j || !i->second.fp) return j;
    }
    return j;
  }

  int addToTable(Linux_file_t *fp, int flags=0, int first=0) {
    int nfd = findNextOpen(first); 
    fds[nfd] = Linux_fd_t(fp, flags);
    fp->incr();
    return nfd;
  }

  Linux_file_t *getFD(int fd) { return fds[fd].fp; }
  Linux_file_t *getGoodFD(int fd); 
  int getFlags(int fd) { return fds[fd].flags; }
  Linux_fd_t *getDesc(int fd) { return &fds[fd]; }

  int closeOne(int fd) {
    std::map<int, Linux_fd_t>::iterator i = fds.find(fd);
    if (i == fds.end()) { myerrno() = OSDefs::oEBADFD; return -1; }
    if (!i->second.fp) { myerrno() = OSDefs::oEBADFD; return -1; }
    int ret = i->second.fp->close();
    fds.erase(i);
    return ret;
  }

  ~Linux_fdtable_t() {
    for (std::map<int, Linux_fd_t>::iterator
         i=fds.begin(), f=fds.end(); i != f ; ++i) {
      if (!i->second.fp) continue;
      i->second.fp->close();
    }
  }

  void closeExec() {
    for (std::map<int, Linux_fd_t>::iterator
         i=fds.begin(), f=fds.end(); i != f ; ++i) {
      if (!i->second.fp) continue;
      if (!(i->second.flags & OSDefs::oFD_CLOEXEC)) continue;
      i->second.fp->close();
      fds.erase(i);
    }
  } // closeExec

#ifdef LSE_LINUX_CHECKPOINT
  Linux_fdtable_t(uint64_t id, class Linux_dinst_t *o);
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

typedef OSDefs::Linux_sighandinf_t Linux_sighandinf_t;

// This structure covers all the possibilities; the architecture code will
// have to copy it to a specific structure, because the layout and types are
// quite architecture-specific and has a bunch of unions in it.
struct Linux_siginfo_t {
  int LSEsi_signo;
  int LSEsi_errno;
  int LSEsi_code;
  int LSEsi_trapno;
  int LSEsi_pid;
  int LSEsi_uid;
  int LSEsi_status;
  OSDefs::L_clock_t LSEsi_utime;
  OSDefs::L_clock_t LSEsi_stime;
  OSDefs::L_long LSEsi_value;
  int LSEsi_int;
  LSE_emu_addr_t LSEsi_ptr;
  int LSEsi_overrun;
  int LSEsi_timerid;
  LSE_emu_addr_t LSEsi_addr;
  int LSEsi_band;
  int LSEsi_fd;
  char rawbytes[128]; // are only 128 going to be enough?
};

class Linux_sighandtable_t : public Linux_obj_t { // a table of signal handlers
public:
  typedef enum { Term, Core, Ignore, Stop } actions_t;
  Linux_sighandinf_t handlers[64];
  Linux_sighandtable_t(class Linux_dinst_t *o) 
  : Linux_obj_t(o, obj_sighandtable) {
    ::memset(handlers, 0, sizeof(handlers)); // default all of them.
  }
  Linux_sighandtable_t(class Linux_dinst_t *d,
		       Linux_sighandtable_t *o) 
    : Linux_obj_t(d, obj_sighandtable) {
    ::memcpy(handlers, o->handlers, sizeof(handlers));
  }
#ifdef LSE_LINUX_CHECKPOINT
  Linux_sighandtable_t(uint64_t oid, class Linux_dinst_t *d) 
    : Linux_obj_t(oid, d, obj_sighandtable) { }
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

class Linux_fs_t : public Linux_obj_t {
public:
  char *cwd, *fsroot;  // current working directory and filesystem root
  int umask;
  Linux_fs_t(class Linux_dinst_t *d) 
    : Linux_obj_t(d, obj_fs), cwd(0), fsroot(0), umask(0022) {}
  Linux_fs_t(class Linux_dinst_t *d, Linux_fs_t *o) 
    : Linux_obj_t(d, obj_fs) {
    cwd = strdup(o->cwd);
    fsroot = strdup(o->fsroot);
    umask = o->umask;
  }
  ~Linux_fs_t() {
    if (cwd) free(cwd);
    if (fsroot) free(fsroot);
  }
#ifdef LSE_LINUX_CHECKPOINT
  Linux_fs_t(uint64_t oid, class Linux_dinst_t *d) :
    Linux_obj_t(oid, d, obj_fs), cwd(0), fsroot(0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

class Linux_waitqueue_t {
public:
  std::list<Linux_context_t *> waiters;
  int wakeup(int numtowake, bool eraseit=false);
  void add(Linux_context_t *);
  void remove(Linux_context_t *);
#ifdef LSE_LINUX_CHECKPOINT
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::data_t *p,
                                  LSE_chkpt::file_t *fp, int nmark);
#endif
};

// information about the process group
class Linux_pgroup_t : public Linux_obj_t { 
public:
  int pgid;
  std::list<class Linux_tgroup_t *> members;
  Linux_pgroup_t(class Linux_dinst_t *o, int g); 
#ifdef LSE_LINUX_CHECKPOINT
  Linux_pgroup_t(uint64_t id, class Linux_dinst_t *o) 
    : Linux_obj_t(id, o, obj_pgroup) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
  ~Linux_pgroup_t();
};

// information about the thread group and signals
class Linux_tgroup_t : public Linux_obj_t {
public:
  int tgid; 
  class Linux_pgroup_t *pgroup;
  std::list<class Linux_context_t *> members;
  std::list<Linux_siginfo_t> signals;
  uint64_t sigpending;
  bool exiting, exitcodeValid;
  struct Linux_context_t *exittask;
  class Linux_waitqueue_t childwait;
  int exitcode;
  int nCount;
  int nLive;
  class Linux_timer_t *timers[3]; // the three process timers
  uint64_t cputime_curr_interval;
  uint64_t cputime_cumulative;
  OSDefs::L_ulong rlimits[OSDefs::oRLIMIT_NLIMITS][2];

  Linux_tgroup_t(class Linux_dinst_t *o, int g);
#ifdef LSE_LINUX_CHECKPOINT
  Linux_tgroup_t(uint64_t id, class Linux_dinst_t *o) 
    : Linux_obj_t(id, o, obj_tgroup), pgroup(0), exittask(0) { 
    timers[0] = timers[1] = timers[2] = 0;
  }
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
  ~Linux_tgroup_t();
};

struct Linux_continuation_t {

  enum cont_kinds { cont_kind_wait, cont_kind_futex, cont_kind_nsleep,
		    cont_kind_poll, cont_kind_piperead, 
		    cont_kind_ssuspend, cont_kind_vfork, cont_kind_pause,
		    cont_kind_stimedwait };
  cont_kinds kind;
  Linux_context_t *osct; // not refcounted

  virtual void run() = 0;
  Linux_continuation_t(cont_kinds k, Linux_context_t *o) 
  : kind(k), osct(o) {}
  virtual ~Linux_continuation_t() {}
#ifdef LSE_LINUX_CHECKPOINT
  virtual LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
					 LSE_chkpt::data_t *rp, 
					 int nmark) = 0;
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp); 
#endif
};

struct cont_wait : public Linux_continuation_t {
  int pid, options;
  LSE_emu_addr_t stataddr, infop, ru;
  bool iscompat;
  int changelen;
  cont_wait(Linux_context_t *o, int p, int optn, LSE_emu_addr_t sa,
	    LSE_emu_addr_t ip, LSE_emu_addr_t r, bool ic, int cl) 
   : Linux_continuation_t(cont_kind_wait,o), pid(p), options(optn),
     stataddr(sa), infop(ip), ru(r), iscompat(ic), changelen(cl) {}
  void run();
  ~cont_wait();
#ifdef LSE_LINUX_CHECKPOINT
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp, 
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					cont_wait *&nv);
  cont_wait() : Linux_continuation_t(cont_kind_wait,0) {};
#endif
};

struct cont_futex : public Linux_continuation_t {
  LSE_emu_addr_t uaddr, paddr;
  uint64_t futexBitset;
  struct Linux_futex_t *futexwaitrec;
  struct Linux_wu_timer_t *timer;
 cont_futex(Linux_context_t *o, LSE_emu_addr_t u, LSE_emu_addr_t p,
	    uint64_t bs, Linux_futex_t *f, Linux_wu_timer_t *t) 
   : Linux_continuation_t(cont_kind_futex,o), uaddr(u), paddr(p),
     futexBitset(bs), futexwaitrec(f), timer(t) {}
  void run();
  ~cont_futex();
#ifdef LSE_LINUX_CHECKPOINT
  cont_futex() : Linux_continuation_t(cont_kind_futex, 0), 
		 futexwaitrec(0), timer(0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp, 
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					struct cont_futex *&nv);
#endif
};

struct cont_nsleep : public Linux_continuation_t {
  struct Linux_timer_t *timer;
  int mode;
  LSE_emu_addr_t rem;
  cont_nsleep(Linux_context_t *o, struct Linux_timer_t *t, int m,
	      LSE_emu_addr_t r) 
    : Linux_continuation_t(cont_kind_nsleep,o), timer(t), mode(m),
      rem (r) {}
  void run();
  ~cont_nsleep();
#ifdef LSE_LINUX_CHECKPOINT
  cont_nsleep() : Linux_continuation_t(cont_kind_nsleep, 0), timer(0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp, 
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					cont_nsleep *&nv);
#endif
};

struct cont_pause : public Linux_continuation_t {
 cont_pause(Linux_context_t *o) : Linux_continuation_t(cont_kind_pause,o) {}
  void run();
#ifdef LSE_LINUX_CHECKPOINT
  cont_pause() : Linux_continuation_t(cont_kind_pause,0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp,
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					cont_pause *&nv);
#endif
};

struct cont_poll : public Linux_continuation_t {
  struct pollfd *pfd;
  LSE_emu_addr_t fds;
  int nfds;
  int64_t timeout;
  std::set<struct Linux_file_t *> files;

 cont_poll(Linux_context_t *o, struct pollfd *p, LSE_emu_addr_t f,
	   int n, int64_t tmo) 
   : Linux_continuation_t(cont_kind_poll,o), pfd(p), fds(f), nfds(n),
    timeout(tmo) {}
  void run();
  ~cont_poll();
#ifdef LSE_LINUX_CHECKPOINT
  cont_poll() : Linux_continuation_t(cont_kind_poll, 0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp, 
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					cont_poll *&nv);
#endif
};

struct cont_piperead : public Linux_continuation_t {
  Linux_pipe_t *pp;
 cont_piperead(Linux_context_t *o, Linux_pipe_t *p) 
   : Linux_continuation_t(cont_kind_piperead,o), pp(p) { p->incr(); }
  void run();
  ~cont_piperead();
#ifdef LSE_LINUX_CHECKPOINT
  cont_piperead() : Linux_continuation_t(cont_kind_piperead, 0), pp(0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp, 
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					cont_piperead *&np);
#endif
};

struct cont_ssuspend : public Linux_continuation_t {
 cont_ssuspend(Linux_context_t *o) 
   : Linux_continuation_t(cont_kind_ssuspend,o) {}
  void run();
#ifdef LSE_LINUX_CHECKPOINT
  cont_ssuspend() : Linux_continuation_t(cont_kind_ssuspend, 0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp, 
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					cont_ssuspend *&nv);
#endif
};

template<class SIGINFO>
struct cont_stimedwait : public Linux_continuation_t {
  LSE_emu_addr_t infop;
  uint64_t savemask, thismask;
  struct Linux_timer_t *timer;
  static const int typeno;
  cont_stimedwait(Linux_context_t *o, LSE_emu_addr_t u, uint64_t tm, uint64_t m,
		  struct Linux_timer_t *t) 
    : Linux_continuation_t(cont_kind_stimedwait,o), infop(u), savemask(m),
      thismask(tm), timer (t) {}
  void run();
  ~cont_stimedwait();
#ifdef LSE_LINUX_CHECKPOINT
  cont_stimedwait() : Linux_continuation_t(cont_kind_stimedwait,0), timer(0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp, 
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					cont_stimedwait<SIGINFO> *&nv);
#endif
};

struct cont_vfork : public Linux_continuation_t {
 cont_vfork(Linux_context_t *o) : Linux_continuation_t(cont_kind_vfork,o) {}
  void run() {} // nothing to do, but I want to keep the stack right...
#ifdef LSE_LINUX_CHECKPOINT
  cont_vfork() : Linux_continuation_t(cont_kind_vfork,0) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, 
				 LSE_chkpt::data_t *rp,
				 int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp,
					cont_vfork *&nv);
#endif
};

typedef std::vector<Linux_continuation_t *> continuationStack_t;

void Linux_pipe_file_t::register_poll_waiter(class cont_poll *cp) {
  cp->files.insert(this);
}

void Linux_pipe_file_t::unregister_poll_waiter(class cont_poll *cp) {
  cp->files.erase(this);
}

enum Linux_context_state_t {
  ProcBlocked, ProcStopped, ProcZombie, ProcRunning, ProcRunnable,
  ProcReaped /* i.e., waiting for timing simulation to release resources */
};

class Linux_context_t : public Linux_obj_t { 
public:
  Linux_memory_t *mem;
  Linux_fdtable_t *fdtable;
  Linux_sighandtable_t *sighandtable;
  Linux_tgroup_t *tgroup;
  Linux_waitqueue_t reportVforkDone;
  Linux_fs_t *fs;

  CC_isacontext_t *isacont;
  OSDefs::L_long exit_code;
  int signalOnExit;
  Linux_context_state_t state;
  LSE_emu_addr_t clear_child_tid;
  LSE_emu_addr_t robustFutexList;
#ifdef LSE_LINUX_COMPAT
  LSE_emu_addr_t robustFutexListCompat;
#endif
  int tid, gid, egid, uid, euid;
  int is64bit;
  std::list<Linux_context_t *> children;
  Linux_context_t *parent;
 
  uint64_t sigblocked, oldsigblocked;
  std::list<Linux_siginfo_t> signals;
  uint64_t sigpending;
  bool oldSigBlockedValid, sigPendingFlag;

  int schedPolicy;
  OSDefs::L_sched_param schedParms;
  OSDefs::L_sigaltstack sigAlt;

  continuationStack_t continuationStack;
  Linux_continuation_t *restart;

  uint64_t cputime_curr_interval;
  uint64_t cputime_cumulative;

#ifdef LSE_LINUX_CHECKPOINT
  LSE_chkpt::data_t *currOSRecord, *currArg, *currRes;
  void clear_chkpt(int kind);
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *cptFile, int mark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					Linux_context_t *&foundcont,
					LSE_chkpt::data_t *tdp);
#endif

  CC_contextdecl_hook()

  Linux_context_t(class Linux_dinst_t *d, int tid) 
  : Linux_obj_t(d, obj_context), 
  mem(0), fdtable(0), sighandtable(0), tgroup(0), 
  exit_code(0), signalOnExit(-1), 
  robustFutexList(0), tid(tid), parent(0), sigblocked(0), oldsigblocked(0), 
  sigpending(0), oldSigBlockedValid(false), sigPendingFlag(false), 
    clear_child_tid(0), schedPolicy(0), restart(0), cputime_curr_interval(0),
    cputime_cumulative(0) { 
    memset(&schedParms, 0, sizeof(schedParms));
    memset(&sigAlt, 0, sizeof(sigAlt));
#ifdef LSE_LINUX_COMPAT
    robustFutexListCompat=0;
#endif
#ifdef LSE_LINUX_CHECKPOINT
    clear_chkpt(101);
#endif
    CC_contextinit_hook()
  }

#ifdef LSE_LINUX_CHECKPOINT
  Linux_context_t(uint64_t id, class Linux_dinst_t *d) :
    Linux_obj_t(id, d, obj_context),
    mem(0), fdtable(0), sighandtable(0), tgroup(0), fs(0), isacont(0),
    restart(0)  {
    currOSRecord=0;
  }
#endif

  Linux_context_t(class Linux_dinst_t *d) : Linux_obj_t(d, obj_context) {}
  ~Linux_context_t();
  void detach();
};

class Linux_futex_t : public Linux_obj_t {
public:
  uint32_t bitset; // trickiness allowing this to handle multiple conditions
  std::deque<class cont_futex *> waiters;
  Linux_futex_t (class Linux_dinst_t *o) : bitset(0),
					   Linux_obj_t(o, obj_futex) {}
#ifdef LSE_LINUX_CHECKPOINT
  Linux_futex_t (uint64_t id, class Linux_dinst_t *o) 
  : Linux_obj_t(id, o, obj_futex) {}
  LSE_chkpt::error_t build_chkpt(LSE_chkpt::file_t *fp, int nmark);
  static LSE_chkpt::error_t parse_chkpt(class Linux_dinst_t *di, 
					LSE_chkpt::data_t *tdp);
#endif
};

struct Linux_todo_t {

  enum todo_kinds { todo_kind_timer };
  todo_kinds kind;
  uint64_t intendedTime; // measured in ns since epoch

  virtual void run(uint64_t currtime) = 0;
  Linux_todo_t(todo_kinds k) : kind(k), cancelled(false) {}
  virtual ~Linux_todo_t() {}

  void unschedule(class Linux_dinst_t *) {
    // unfortunately, I cannot just wipe stuff out of the queue.
    this->cancelled = true;
  }

  inline void schedule(uint64_t attime, class Linux_dinst_t *);

  bool cancelled;
};

struct todo_comparator {
  bool operator()(Linux_todo_t *x, Linux_todo_t *y) {
    return x->intendedTime > y->intendedTime; // switch direction
  }
};

typedef std::priority_queue<Linux_todo_t *, std::vector<Linux_todo_t *>,
			    todo_comparator> eventQueue_t;

struct todo_timer : public Linux_todo_t {
  Linux_timer_t *timer;
  static todo_timer *schedule(uint64_t tt, Linux_timer_t *t);
  todo_timer(Linux_timer_t *t) : Linux_todo_t(todo_kind_timer), timer(t) {
    t->todo = this;
  }
  void run(uint64_t currtime);
  void unschedule() { 
    if (timer) static_cast<Linux_todo_t *>(this)->unschedule(timer->os); 
  }
  ~todo_timer();
};

class Linux_dinst_t {
public:
  LSE_emu_interface_t *eint;

  // An invariant: the "first" thread in a thread group is *always* still
  // in existance as long as the thread group is!  Thus we do not have to
  // map thread groups separately; we can always look up the thread and
  // go through its tgroup pointer.
  std::map<int, Linux_context_t *> threads;
  std::deque<Linux_context_t *> ready_list;
  std::list<Linux_context_t *> running_list;
  class Linux_reschedule_timer_t *reschedule_timer;
  class Linux_error_file_t *errorFile;
  std::map<int, Linux_pgroup_t *> pgroups; // for lookup
  std::map<int, Linux_tgroup_t *> tgroups; // for lookup

  // Yes, errno should be thread-local, but if we assume that system calls
  // are serialized, keeping one around for the internals in fine.
  int myerrno_int;

  // futexes, indexed by physical address.  Physical is safe because I ensure
  // that the physical pages are present if they hold futexes.  If I ever
  // make those pages swappable, I will have to do something more clever.
  typedef std::map<LSE_emu_addr_t, class Linux_futex_t *> fm_t;
  fm_t futexes;

  int last_pid;
  // counters for objects so that we can identify each object uniquely.
  uint64_t objC;
  typedef std::map<uint64_t, Linux_obj_t *> objmap_t;
  objmap_t objects;

#ifdef LSE_LINUX_CHECKPOINT
  int cptcount; // current checkpoint mark
  LSE_emu_chkpt_cntl_t ctl;
  int OSstate; /* 0 = doing nothing, 1 = writing, 2 = reading */
  boolean doingCPoint;
  std::map<int, LSE_chkpt::data_t *> osCalls;
  typedef std::pair<uint64_t, Linux_obj_t **> fixup_t;
  std::vector<fixup_t> fixups;
  uint64_t clockdiff;
#endif

  struct prange_t {
    LSE_emu_addr_t pno, num;
  };
  struct pinfo_t {
    int refcnt;
    off_t offset;
  };

  std::list<prange_t> freePpages;
  std::map<LSE_emu_addr_t, pinfo_t> usedPpages;

  struct fpagemap_t {
    std::map<LSE_emu_addr_t,LSE_emu_addr_t> offset2pno;
    fpagemap_t() {}
  };

  std::map<inode_id_t, fpagemap_t> inode2pages;
  ino_t anonCounter;

  eventQueue_t eventQueue;
  bool wantScheduling; // no need to checkpoint
  uint64_t boottime; // will be adjusted when settime is called

   Linux_dinst_t(LSE_emu_interface_t *ifc) 
     : eint(ifc), objC(0), anonCounter(1), wantScheduling(false) {
    OS_interface_set(ifc, this);

    errorFile = new Linux_error_file_t(this);
    errorFile->incr();
    myerrno_int = 0;
    reschedule_timer = 0;

    prange_t npp;
    npp.pno = 1;
    npp.num = ((uint64_t(1)< (CC_elftype == 32 ? 31 :63))/
	       OSDefs::oPAGE_SIZE)*2 - 1;
    freePpages.push_back(npp);

    Linux_context_t *initcontext = new Linux_context_t(this, 1);
    initcontext->fdtable = new Linux_fdtable_t(this);
    initcontext->fdtable->addToTable(new Linux_native_file_t(this, 0));
    initcontext->fdtable->addToTable(new Linux_native_file_t(this, 1));
    initcontext->fdtable->addToTable(new Linux_native_file_t(this, 2));

    initcontext->sighandtable = new Linux_sighandtable_t(this); // defaults
    initcontext->tgroup = new Linux_tgroup_t(this, 1);
    initcontext->tgroup->pgroup = new Linux_pgroup_t(this, 1);
    initcontext->tgroup->pgroup->members.push_back(initcontext->tgroup);
    initcontext->tgroup->timers[0] =
      initcontext->tgroup->timers[1] =
      initcontext->tgroup->timers[2] = 0;
    initcontext->tgroup->cputime_curr_interval = 0;
    initcontext->tgroup->cputime_cumulative = 0;
    initcontext->fs = new Linux_fs_t(this);

    CC_isacontext_t *initisacontext;

    // create an initial context (with no additional parent)
    CC_context_copy(ifc, &initisacontext, 0, false);
    CC_ict_osinfo(initisacontext) = initcontext;
    initcontext->isacont = initisacontext;
    initcontext->tid = 1;
    initcontext->gid = getgid();
    initcontext->egid = getegid();
    initcontext->uid = getuid();
    initcontext->euid = geteuid();
    initcontext->exit_code = 0;
    initcontext->state = ProcBlocked; // so it never runs!
    initcontext->mem = new Linux_memory_t(this);
    initcontext->mem->heapbreak = initcontext->mem->heapstart = 0;
    initcontext->mem->vmdevice.subspace = OS_ict_emuvar(initisacontext, mem);
    initcontext->is64bit = CC_elftype == 32 ? 0 : 1;
    initcontext->cputime_curr_interval = 0;
    initcontext->cputime_cumulative = 0;

    // Set up initial paths

    char wdbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
    wdbuf[0] = '.'; wdbuf[1] = 0;
    getcwd(wdbuf, OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
    initcontext->fs->cwd = (char *)malloc(strlen(wdbuf)+1);
    strcpy(initcontext->fs->cwd, wdbuf);

    initcontext->fs->fsroot = (char *)malloc(1);
    initcontext->fs->fsroot[0] = 0;

    CC_mem_redirect(initisacontext, &initcontext->mem->vmdevice);
    threads[1] = initcontext;
    last_pid = 299;

    struct timeval tv;
    gettimeofday(&tv, 0);
    boottime = int64_t(tv.tv_sec) * 1000000000 + int64_t(tv.tv_usec)*1000;

#ifdef LSE_LINUX_CHECKPOINT
    cptcount=0;
    OSstate=0; /* 0 = doing nothing, 1 = writing, 2 = reading */
    CC_chkpt_cntl_clear(ctl);
    boolean doingCPoint=false;
#endif
  }
  ~Linux_dinst_t();
};

Linux_dinst_t::~Linux_dinst_t() {
  // only works because threads remove themselves from the list, but we
  // do need to break all the parent links because we will not be deleting
  // in reverse tree order and we don't want to traverse a bad link.
  while (threads.size()) {
    threads.begin()->second->parent = 0;
    threads.begin()->second->decr();
  }
  errorFile->decr();
  while (!eventQueue.empty()) {
    delete eventQueue.top();
    eventQueue.pop();
  }

  if (reschedule_timer) reschedule_timer->decr();

#ifdef LSE_LINUX_CHECKPOINT
  for (std::map<int, LSE_chkpt::data_t *>::iterator
	 i = osCalls.begin(), ie = osCalls.end(); i != ie ; ++i) {
    delete i->second;
  }
#endif
  
}

Linux_obj_t::Linux_obj_t(class Linux_dinst_t *o) 
	  :  refcount(1), os(o), oid(++(o->objC)), mark(0), kind(obj_unknown) {
  os->objects[oid] = this;
  //std::cerr << "C " << refcount << " " << oid << " " << kind << "\n";
}

Linux_obj_t::Linux_obj_t(class Linux_dinst_t *o, enum Linux_objkind k) 
	  :  refcount(1), os(o), oid(++(o->objC)), mark(0), kind(k) {
  os->objects[oid] = this;
  //std::cerr << "C " << refcount << " " << oid << " " << kind << "\n";
}

#ifdef LSE_LINUX_CHECKPOINT
Linux_obj_t::Linux_obj_t(uint64_t id, class Linux_dinst_t *o, 
			 enum Linux_objkind k) 
	  :  refcount(0), os(o), oid(id), mark(0), kind(k) {
  os->objects[oid] = this;
  //std::cerr << "C " << refcount << " " << oid << " " << kind << "\n";
}
#endif

Linux_obj_t::~Linux_obj_t() {
  std::map<uint64_t, Linux_obj_t *>::iterator i = os->objects.find(oid);
  if (i != os->objects.end() && i->second == this) os->objects.erase(i);
}

int &Linux_obj_t::myerrno() { 
  return os->myerrno_int;
}

int Linux_waitqueue_t::wakeup(int numtowake, bool eraseit) {
  int count = 0;
  for (std::list<Linux_context_t *>::iterator
       i = waiters.begin(), ie = waiters.end(); i != ie ; ) {
    Linux_context_t *waiter = *i;
    if (ready_thread(waiter, true)) {
      if (eraseit) i = waiters.erase(i);
      else ++i;
      if (numtowake && (++count == numtowake)) break;
    } else ++i;
  }
  return count;
}

void Linux_waitqueue_t::add(Linux_context_t *c) {
  waiters.remove(c);
  waiters.push_back(c);
}

void Linux_waitqueue_t::remove(Linux_context_t *c) {
  waiters.remove(c);
}

Linux_pgroup_t::Linux_pgroup_t(class Linux_dinst_t *o, int g) 
: Linux_obj_t(o, obj_pgroup), pgid(g) {
  o->pgroups[g] = this;
}

Linux_pgroup_t::~Linux_pgroup_t() {
  if (os->pgroups.find(pgid) != os->pgroups.end() &&
      os->pgroups[pgid] == this) 
    os->pgroups.erase(pgid);
}

Linux_tgroup_t::Linux_tgroup_t(class Linux_dinst_t *o, int g) 
  : Linux_obj_t(o, obj_tgroup), tgid(g), pgroup(0), sigpending(0), 
    exiting(false),
    exitcodeValid(false), exittask(0), exitcode(0), nCount(0), nLive(1),
    cputime_curr_interval(0), cputime_cumulative(0) {
    o->tgroups[g] = this;
    for (int i = 0; i < OSDefs::oRLIMIT_NLIMITS; ++i) {
      rlimits[i][0] = rlimits[i][1] = OSDefs::oRLIMIT_INFINITY;
    }
  }

Linux_tgroup_t::~Linux_tgroup_t() {
  if (os->tgroups.find(tgid) != os->tgroups.end() &&
      os->tgroups[tgid] == this) 
    os->tgroups.erase(tgid);
  pgroup->members.remove(this);
  pgroup->decr();
  if (timers[0]) timers[0]->decr();
  if (timers[1]) timers[1]->decr();
  if (timers[2]) timers[2]->decr();
}

// copy all the pages in the old VM space to the new one.  Also makes sure
// file refrence counts are incremented.
// Precondition: vranges has been copied already.
void Linux_memory_t::copy_space(Linux_memory_t *old) {

  for (ranges_t::iterator i = vranges.begin(), ie = vranges.end();
       i != ie; ++i)
    if (i->second.fp) i->second.fp->incr();

  unsigned char buf[OSDefs::oPAGE_SIZE];

  for (vmtrans_t::const_iterator i = old->vmdevice.begin(),
	 ie = old->vmdevice.end() ; i != ie ; ++i) {

    // This is horribly inefficient; I ought to go through the VM spaces
    // but I do not feel like it.  At least we only copy actually allocated
    // pages :-)

    Linux_memory_t::ranges_t::iterator hv;
    hv = old->vranges.upper_bound(i->first);  // first w/hv->end > start
    if (!(hv != old->vranges.end() && hv->second.start <= i->first)) {
      // Should not happen!
      fprintf(stderr, "Attempting to copy a page outside of VA space\n");
      return;
    }

    if (hv->second.isShared) {
      // new VA->old PA (note: i->second is *difference*, not pa)
      LSE_emu_addr_t pa = old->vmdevice.lookup(i->first);
      //std::cerr << "Preserving mapping " << std::hex <<
      //	i->first << " -> " << pa << std::dec << "\n";
      vmdevice.addmap(i->first, pa);
      os->usedPpages[pa / OSDefs::oPAGE_SIZE].refcnt++;
    } else { // allocate a new PA
      Linux_dinst_t::prange_t &pr = os->freePpages.front();
      os->usedPpages[pr.pno].refcnt ++; // one page out there.

      vmdevice.addchild(pr.pno * OSDefs::oPAGE_SIZE, 
			OSDefs::oPAGE_SIZE, 0);
      vmdevice.addmap(i->first, pr.pno * OSDefs::oPAGE_SIZE);

      //std::cerr << "Creating new mapping " << std::hex <<
      //	i->first << " -> " <<( pr.pno * OSDefs::oPAGE_SIZE)
      //		<< std::dec << "\n";
      
      pr.pno++;
      if (!--pr.num) {
	os->freePpages.pop_front();
	if (!os->freePpages.size()) {
	  std::cerr << "Ran out of physical pages!\n";
	}
      }
      old->vmdevice.read(i->first, buf, OSDefs::oPAGE_SIZE);
      vmdevice.write(i->first, buf, OSDefs::oPAGE_SIZE);
    }
  }
}

static void drop_pages(struct Linux_dinst_t *os, struct Linux_memory_t *mem, 
		       LSE_emu_addr_t start, LSE_emu_addr_t mlen,
		       Linux_memory_t::range_t &rg);

// The memory destructur must drop pages from physical memory
Linux_memory_t::~Linux_memory_t() {
  for (ranges_t::iterator i = vranges.begin(), ie = vranges.end();
       i != ie; ++i) {
    drop_pages(this->os, this, 
	       i->second.start, i->second.end - i->second.start, i->second);
    if (i->second.fp) i->second.fp->decr();
  }
}

Linux_fdtable_t::Linux_fdtable_t(class Linux_dinst_t *o) 
	  : Linux_obj_t(o, obj_fdtable) { }

#ifdef LSE_LINUX_CHECKPOINT
Linux_fdtable_t::Linux_fdtable_t(uint64_t id, class Linux_dinst_t *o)
	  : Linux_obj_t(id, o, obj_fdtable) { }
#endif

Linux_fdtable_t::Linux_fdtable_t(class Linux_dinst_t *d, Linux_fdtable_t *o) 
	  : Linux_obj_t(d, obj_fdtable) { 
  // assumption is that we start with an empty table
  for (std::map<int, Linux_fd_t>::iterator
         i=o->fds.begin(), f=o->fds.end(); i != f ; ++i) {
    if (i->second.fp) {
      fds[i->first] = i->second;
      i->second.fp->incr();
    }
  }
}	 

Linux_file_t *  Linux_fdtable_t::getGoodFD(int fd) { 
  Linux_file_t *fp = fds[fd].fp; 
  if (!fp) return os->errorFile;
  return fp;
}

bool Linux_file_t::readingCheckpoint() {
#ifdef LSE_LINUX_CHECKPOINT
  return !(os->OSstate != 2 || !os->ctl.recordOS);
#else
  return false;
#endif
}

// Must do pipe implementations down here so they can affect
// contexts and the emulator instance structure.

int Linux_pipe_t::readv(Linux_context_t *osct, struct iovec *b, int c) {
  CC_isacontext_t *realct = osct->isacont;
  int tot = 0;
  bool wantedsome = false;

  for (int i = 0; i < c; i++) {
    if (!b[i].iov_len) continue;
    wantedsome = true;
    int nread = std::min(b[i].iov_len, buf.size());
    if (nread) {
      ::memcpy(b[i].iov_base, buf.c_str(), nread);
      buf.erase(0, nread);
      tot += nread;
    } else break;
  }
  if (!tot && wantedsome) {
    if (!numwriters) return 0;

    if (osct->sigPendingFlag) {
      OS_report_err(OSDefs::oERESTARTSYS);
    } else {
      cont_piperead *pr = new cont_piperead(osct,this);
      waiters.push_back(pr);
      osct->continuationStack.push_back(pr);
      block_thread(osct, true);
    }

    return -2;
  } else return tot;
 syserr:
  return -1;
}

int Linux_pipe_t::writev(Linux_context_t *osct, struct iovec *b, int c) {
  if (!numreaders) { 
    // TODO: how to send sigpipe
    myerrno() = (OSDefs::oEPIPE);
    return -1;
  }
  else { 
    int tot = 0;
    for (int i = 0; i < c; ++i) {
      buf.append((char *)(b[i].iov_base), b[i].iov_len);
      tot += b[i].iov_len;
    }
    bool didsomething = false;
    if (tot && waiters.size()) wakeup_readers();
    return tot;
  }
}

void Linux_pipe_t::wakeup_readers() {
  bool didsomething = false;

  while (waiters.size()) {
    cont_piperead *cp = waiters.front();
    waiters.pop_front();
    ready_thread(cp->osct, true);
    cp->pp = 0;
    decr(); // lose the waiter's reference to us..
  }
}

Linux_pipe_t::~Linux_pipe_t() {
  // pipe cannot be deleted unless waiters is already empty, since each
  // waiter has a reference to the pipe
  assert(waiters.size() == 0);
}

int Linux_pipe_file_t::fcntl(int cmd, OSDefs::L_long arg) { 
  int mask = (OSDefs::oO_APPEND | OSDefs::oO_NONBLOCK | OSDefs::oO_NDELAY 
              | OSDefs::oO_DIRECT | OSDefs::oO_NOATIME);

  switch (cmd) {
   case F_GETFL: 
     myerrno() = (0);
     return status;
   case F_SETFL: 
     status = (arg & mask) | (status & ~mask);
     myerrno() = (0);
     return 0;
   default: myerrno() = (OSDefs::oEBADFD); return -1;
  }
}

int Linux_pipe_file_t::fstat(struct stat *b) { 
  // TODO some day: set up stat properly. But for pipes there is not much
  ::memset(b, 0, sizeof(struct stat));
  b->st_mode = S_IFIFO | S_IRUSR | S_IWUSR; // is this right?
  myerrno() = (0);
  return 0;
}  

// Get rid of all the stuff a zombie refers to.
// In the real world, the init process or the parent process
// really reaps the context.  However, in our world, we need the context
// to stay around for a while because it could still have instructions in 
// flight.  However, OS recording stuff is actually stored somewhere else,
// so that is not an issue.
void Linux_context_t::detach() { 
  if (fdtable) { fdtable->decr(); fdtable = 0; }
  if (sighandtable) { sighandtable->decr(); sighandtable = 0; }
  if (tgroup) {
    tgroup->members.remove(this);
    tgroup->decr();
    tgroup = 0;
  }
  if (fs) { fs->decr(); fs = 0; }
  if (parent) parent->children.remove(this);
  parent = 0;
  sigPendingFlag = false;  // just to make sure we don't try to deliver to it

  while (continuationStack.size()) {
    Linux_continuation_t *cc = continuationStack.back();
    continuationStack.pop_back();
    delete cc;
  }

  delete restart;
  restart = 0;

  do_msync(this, 0, LSE_emu_addr_t(0) - OSDefs::oPAGE_SIZE, false);
  if (os && os->threads.find(tid) != os->threads.end()) os->threads.erase(tid);
}

Linux_context_t::~Linux_context_t() {
  detach();

  // note: memory destructor will drop physical pages
  if (mem) mem->decr(); // need to keep old address space around until GC
  mem = 0;
  // Delete the ISA context (and memories along with it, we hope)
  CC_context_destroy(&CC_emu_interface(isacont), isacont);
}

static void perform_Linux_event(void *p) {
  Linux_dinst_t *os = (Linux_dinst_t *)(p);
  uint64_t currtime = CC_time_get_currtime(os);
  //std::cerr << "event @" << currtime << "\n";
  while (!os->eventQueue.empty() &&
	 (os->eventQueue.top()->intendedTime <= currtime ||
	  os->eventQueue.top()->cancelled)) {


    Linux_todo_t *t = os->eventQueue.top();
    os->eventQueue.pop();

    //std::cerr << "time = " << t->intendedTime << "\n";

    if (!t->cancelled) t->run(currtime);
    else delete t;
  }
  if (!os->eventQueue.empty())
    CC_time_schedule_wakeup(os->eventQueue.top()->intendedTime,
			    perform_Linux_event, os);
  if (os->wantScheduling) Linux_schedule(os->eint);

}

void Linux_todo_t::schedule(uint64_t attime, Linux_dinst_t *di) {
  intendedTime = attime;
  di->eventQueue.push(this);
  if (di->eventQueue.top() == this) {
    CC_time_schedule_wakeup(attime, perform_Linux_event, di);
  }
}

int Linux_init(LSE_emu_interface_t *ifc) {
  Linux_dinst_t *ldi = new Linux_dinst_t(ifc);
  CC_init_hook(ifc);
  return 0;
}

void Linux_finish(LSE_emu_interface_t *ifc) {
  Linux_dinst_t *ip=OS_interface(ifc);

  if (ip) {
    if (ip->threads.size() > 1) {
      std::cerr << "Not all Linux threads were reaped!\n";
      for (std::map<int, Linux_context_t *>::iterator 
           i = ip->threads.begin(), ie = ip->threads.end(); i != ie; ++i)
       if (i->first != 1) std::cerr << "\tThread " << i->first << "\n";
    }
    delete ip;
  }
  OS_interface_set(ifc,0);
  CC_finish_hook(ifc);
}

static LSE_emu_addr_t sextend(int change, LSE_emu_addr_t v) {
  if ((change & 2) && (v & 0x80000000U))
     return v | ( (~((LSE_emu_addr_t)(0))) << 31 << 1 );
  else if ((change & 1) && (v & 0x8000U))
     return v | ( (~((LSE_emu_addr_t)(0))) << 15 << 1 );
  else return v;
}


static LSE_emu_addr_t sextend32(int change, LSE_emu_addr_t v) {
  if ((change & 2) && (v & 0x80000000U))
     return v | ( (~((LSE_emu_addr_t)(0))) << 31 << 1 );
  else return v;
}

static LSE_emu_addr_t sextend16(int change, LSE_emu_addr_t v) {
  if ((change & 1) && (v & 0x8000U))
     return v | ( (~((LSE_emu_addr_t)(0))) << 15 << 1 );
  else return v;
}

void Linux_schedule(LSE_emu_interface_t *);

int Linux_cloneguts(CC_isacontext_t **newisact, CC_isacontext_t *oldisact,
                    int flags,
	            LSE_emu_addr_t stackstart, LSE_emu_addr_t stacksize,
		    bool fromClone, LSE_emu_iaddr_t startpc,
		    LSE_emu_addr_t ptidp, LSE_emu_addr_t ctidp,
		    LSE_emu_addr_t tlsp);

static LSE_emu_addr_t 
	  do_mmap(Linux_context_t *osct, LSE_emu_addr_t start, 
		  LSE_emu_addr_t length, LSE_emu_addr_t offset, int prot, 
		  int flags, Linux_file_t *lfp);

struct psect_context {
  CC_isacontext_t *ct;
  LSE_emu_addr_t bss, brk, start_code, end_code, start_data, end_data, loadbias,
    baseaddr;
  bool use_interp, loadbiasknown;
  Linux_native_file_t *lfp;
#ifdef LSE_LINUX_CHECKPOINT
  LSE_chkpt::data_t *seqToUse;
#endif
  psect_context(CC_isacontext_t *c) 
    : ct(c), bss(0), brk(0), start_code(~LSE_emu_addr_t(0)), end_code(0),
      start_data(0), end_data(0), loadbias(0), baseaddr(0),
      use_interp(false), loadbiasknown(false), lfp(0) {}
#ifdef LSE_LINUX_CHECKPOINT
  void chkptBuild(LSE_chkpt::data_t *parent);
  LSE_chkpt::error_t chkptParse(LSE_chkpt::data_t *tdp);
#endif
};

static bool OS_strncpy_t2h_int(CC_isacontext_t *realct,
			char *targ, LSE_emu_addr_t src, unsigned int size) {
  bool done=false;
  CC_mem_data_t *hostsrc,*p;
  CC_mem_addr_t madelen, madelen2;
  LSE_emu_addr_t ml2;
  unsigned int i;

  while (!done) {

    if (!(2 & Linux_check_addr_len(realct,src,ml2))) {
      CC_sys_set_return(realct,0,-1);
      CC_sys_set_return(realct,1,OSDefs::oEFAULT);
      return true;
    }

    if (CC_mem_translate(&OS_ict_osinfo(realct)->mem->vmdevice,
			 src,&madelen, &hostsrc)) {
      CC_sys_set_return(realct,0,-1);
      CC_sys_set_return(realct,1,OSDefs::oEFAULT);
      return true;
    }

    madelen = std::min(madelen, (CC_mem_addr_t)ml2);
    for (i=0,p=hostsrc; i<madelen && i<size; i++,p++) 
      if (!*p) { /* done */
        memcpy(targ,hostsrc,i+1);
        done = true;
	break;
      }
    if (!done) {
      memcpy(targ,hostsrc,i);
      if (i<madelen) { /* ran up against size limit */
         targ[i-1]=0;
	 done = true;
      }
      targ += madelen;
      src += madelen;   /* not good for > 64 bits */
      size -= madelen;
    }
  }
  return false;
}


#ifdef LSE_LINUX_CHECKPOINT
#include "Linux_cpoint.h"
#else
#define CC_chkpt_start(x)
#define CC_chkpt_finish(x)
#define CC_chkpt_guard_true   (true)
#define CC_chkpt_guard_os(x)  (true)
#define CC_chkpt_guard(...)  __VA_ARGS__
#define CC_chkpt_arg_ptr(v)
#define CC_chkpt_arg_str(v,l)
#define CC_chkpt_arg_hmem(v,l)
#define CC_chkpt_arg_tmem(v,l)
#define CC_chkpt_arg_int(v)
#define CC_chkpt_result_ptr(v)
#define CC_chkpt_result_str(v,l)
#define CC_chkpt_result_hmem(v,l)
#define CC_chkpt_result_tmem(v,l)
#define CC_chkpt_result_psect(v)
#define CC_chkpt_result_int(v)
#endif

/****************** memory mapping ************************/

// This rather important function essentially handles page faults.  The
// handling depends upon the VA segment attributes:
//  1) If private, allocate space and read/clear the space as needed
//  2) If shared, determine whether present.  If so, point at it. If not,
//     allocate space and read/clear as needed.
// 
// The function must go after checkpoint macros are defined
LSE_device::devaddr_t pagemapper_t::demand(LSE_device::devaddr_t addr) {
  // IMPORTANT precondition: addr is page-aligned
  // Also: must use devaddr_t, which is 64-bit even for 32-bit emulators
  // because the upper 32 bits have to be correct for LSE_translation to work
  LSE_device::devaddr_t naddr;
  Linux_dinst_t::fpagemap_t *pm;
  LSE_device::devaddr_t vpno = addr / OSDefs::oPAGE_SIZE;

  // Find attributes
  Linux_memory_t::ranges_t::iterator hv;
  hv = PM->vranges.upper_bound(addr);  // first w/hv->end > start
  if (!(hv != PM->vranges.end() && hv->second.start <= addr)) {
    // This should not be normal, but some ISAs which do not have a softMMU
    // need to not map bogus pages!
    throw LSE_device::deverror_t(LSE_device::deverror_unmapped);
    // goto allocateinmemory;
  }
  
  // If shared, need to point to page if already in memory
  if (hv->second.isShared) {
    pm = &PM->os->inode2pages[hv->second.fp->getInodeID()];
    naddr = pm->offset2pno[addr - hv->second.start + hv->second.offset];
    if (naddr) { // add to this VA map
      PM->vmdevice.addmap(vpno * OSDefs::oPAGE_SIZE, 
			  naddr * OSDefs::oPAGE_SIZE);
      PM->os->usedPpages[naddr].refcnt++; // another mapping to page
      goto done;
    }
  }

 allocateinmemory:

  // Allocate a physical page

  {
    Linux_dinst_t::prange_t &pr = PM->os->freePpages.front();
    PM->os->usedPpages[pr.pno].refcnt ++; // one page out there.
    
    PM->vmdevice.addchild(pr.pno * OSDefs::oPAGE_SIZE, 
			  OSDefs::oPAGE_SIZE, 0);
    PM->vmdevice.addmap(vpno * OSDefs::oPAGE_SIZE,
			pr.pno * OSDefs::oPAGE_SIZE);

    naddr = pr.pno++;
    // tricky; the offset2pno mapping os only good for the shared files,
    // but each page still does have an offset mapping used for sync
    if (hv->second.isShared) {
      pm->offset2pno[addr - hv->second.start + hv->second.offset] = naddr;
    }
    PM->os->usedPpages[naddr].offset 
      = addr - hv->second.start + hv->second.offset;
    
    if (!--pr.num) {
      PM->os->freePpages.pop_front();
      if (!PM->os->freePpages.size()) {
	std::cerr << "Ran out of physical pages!\n";
      }
    }
  }

  // so many things depend upon the page being zeroed first...
  
  PM->vmdevice.subspace->memset(naddr * OSDefs::oPAGE_SIZE,
				0,OSDefs::oPAGE_SIZE);

#ifdef DONOTDO
  // While it would be lovely to actually fill the page here, we
  // cannot checkpoint it.  Instead, the file read needs to be pulled
  // in by do_mmap.
  if (hv->second.fp) {
    // it is a file read the contents (anonymous will do nothing)
    unsigned char p[OSDefs::oPAGE_SIZE];
    if (true /*CC_chkpt_guard_true*/) {
      off_t off = hv->second.fp->lseek(0,SEEK_CUR);
      hv->second.fp->lseek(hv->second.offset + addr - hv->second.start, 
			    SEEK_SET);
      hv->second.fp->read(0, p, OSDefs::oPAGE_SIZE);
      hv->second.fp->lseek(off, SEEK_SET);
    }
    if (hv->second.fp->getInodeID().dev) {
      //C C_chkpt_result(ptr, addr);
      //C C_chkpt_result(hmem,p,OSDefs::oPAGE_SIZE);
      PM->vmdevice.subspace->write(naddr * OSDefs::oPAGE_SIZE,
				   p, OSDefs::oPAGE_SIZE);
    }
  }
#endif

 done:
  return naddr * OSDefs::oPAGE_SIZE - vpno * OSDefs::oPAGE_SIZE;
}

int Linux_translate(CC_isacontext_t *realct, LSE_emu_addr_t va,
		    void *&ha, LSE_emu_addr_t &pa) { 

  Linux_context_t *osct = OS_ict_osinfo(realct);

  Linux_memory_t::ranges_t::iterator hv;
  hv = osct->mem->vranges.upper_bound(va);

  if (hv == osct->mem->vranges.end()) return 0; // not in ranges
  if (hv->second.start > va) return 0; // not in range

  pa = osct->mem->vmdevice.lookup(va);

  return ( ((hv->second.flags & OSDefs::oPROT_EXEC) ? 1 : 0) |
	   ((hv->second.flags & OSDefs::oPROT_READ) ? 2 : 0) |
	   ((hv->second.flags & OSDefs::oPROT_WRITE) ? 4 : 0) );	   
}

// Check whether an address range is valid the the context's virtual space

static int Linux_check_addr(CC_isacontext_t *realct, LSE_emu_addr_t va, 
                            LSE_emu_addr_t mlen) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int flags = OSDefs::oPROT_ALL;
  Linux_memory_t::ranges_t::iterator hv;
  
keepchecking:
  hv = osct->mem->vranges.upper_bound(va);
  if (hv == osct->mem->vranges.end()) return 0; // not in ranges
  if (hv->second.start > va) return 0; // not in range
  flags &= hv->second.flags;
  if (hv->second.end < va + mlen) {
    mlen = va + mlen - hv->second.end;
    va = hv->second.end;
    goto keepchecking;
  }
  return ( ((flags & OSDefs::oPROT_EXEC) ? 1 : 0) |
	   ((flags & OSDefs::oPROT_READ) ? 2 : 0) |
	   ((flags & OSDefs::oPROT_WRITE) ? 4 : 0) );  
}

// Check whether an address is valid in the context's virtual space and report
// how many more contigous addresses are also in the space.

static int Linux_check_addr_len(CC_isacontext_t *realct, LSE_emu_addr_t va, 
				LSE_emu_addr_t &mlen) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int flags = OSDefs::oPROT_ALL;
  Linux_memory_t::ranges_t::iterator hv;

  hv = osct->mem->vranges.upper_bound(va);
  if (hv == osct->mem->vranges.end()) return 0; // not in ranges
  if (hv->second.start > va) return 0; // not in range
  flags &= hv->second.flags;
  mlen = hv->second.end - va;
  return ( ((flags & OSDefs::oPROT_EXEC) ? 1 : 0) |
	   ((flags & OSDefs::oPROT_READ) ? 2 : 0) |
	   ((flags & OSDefs::oPROT_WRITE) ? 4 : 0) );  
}

// Wrap checks of the address attributes with the read/write operators

inline bool OS_mem_read(CC_isacontext_t *realct, LSE_emu_addr_t va,
			uint64_t ln, void *hp) {
  return (!(2 & Linux_check_addr(realct,va,ln)) ||			
	  CC_mem_read(&OS_ict_osinfo(realct)->mem->vmdevice,va,ln,hp));
}

inline bool OS_mem_write(CC_isacontext_t *realct, LSE_emu_addr_t va,
			 uint64_t ln, void *hp) {
  return (!(4 & Linux_check_addr(realct,va,ln)) ||
	  CC_obtain_write_permission(realct,va,ln) ||
	  CC_mem_write(&OS_ict_osinfo(realct)->mem->vmdevice,va,ln,hp));
}

#define OS_memcpy_h2t(va,hp,ln)			   \
 do {                                              \
    if (OS_mem_write(realct, va, ln, hp)) {        \
      CC_sys_set_return(realct,0,-1);              \
      CC_sys_set_return(realct,1,OSDefs::oEFAULT); \
      goto syserr;                                 \
    }                                              \
 } while (0)

#define OS_memcpy_t2h(hp,va,ln)                    \
 do {                                              \
   if (OS_mem_read(realct, va, ln, hp)) {          \
     CC_sys_set_return(realct,0,-1);               \
     CC_sys_set_return(realct,1,OSDefs::oEFAULT);  \
     goto syserr;                                  \
   }                                               \
  } while (0)

#define OS_strncpy_t2h(hp,va,ln)                   \
  do {                                             \
    if (OS_strncpy_t2h_int(realct,(char *)hp, va,(unsigned int)ln)) \
       goto syserr;                                \
  } while(0)

// do_mprotect
//
//  Change protection.  I will not worry about whether any underlying
//  files have protection that matches.  I also will not attempt to coalesce
//  with neighbors; that is just too messy.
//
//  Assumes page alignment for start and length

static int
do_mprotect(Linux_context_t *osct, LSE_emu_addr_t start, LSE_emu_addr_t mlen,
	    int flags) {
  Linux_memory_t::ranges_t::iterator hv;
  
  while (mlen) {
    hv = osct->mem->vranges.upper_bound(start);
    if (hv == osct->mem->vranges.end()) return -OSDefs::oENOMEM;

    // start/mlen:  AAAA
    //               XX......
    if (hv->second.start > start) // some unmapped stuff
      return -OSDefs::oENOMEM;

    if (hv->second.flags == flags) { // nothing to do w.r.t. this one
      if (start + mlen <= hv->second.end) return 0; // done
      mlen = start + mlen - hv->second.end;
      start = hv->second.end;
      continue;  // and try again
    }

    if (hv->second.start != start) {
      // split off first part and try again
      Linux_memory_t::range_t nv = { hv->second.start, start, 
				     hv->second.flags, hv->second.isShared,
				     hv->second.fp, hv->second.offset };
      hv->second.offset += start - hv->second.start;
      hv->second.start = start;
      osct->mem->vranges[nv.end] = nv;
      if (nv.fp) nv.fp->incr();
      continue;
    } else if (hv->second.end > start + mlen) {
      // split off part to be changed
      Linux_memory_t::range_t nv = { start, start + mlen,
				     flags, hv->second.isShared,
				     hv->second.fp, hv->second.offset };
      hv->second.start = start + mlen;
      hv->second.offset += mlen;
      osct->mem->vranges[nv.end] = nv;
      if (nv.fp) nv.fp->incr();
      return 0; // all done
    } else {
      hv->second.flags = flags;  // change protection
      mlen = start + mlen - hv->second.end;
      start = hv->second.end;
    }
  }

  // must invalidate everywhere!
  for (std::map<int, Linux_context_t *>::iterator i = osct->os->threads.begin(),
	 ie = osct->os->threads.end() ; i != ie ; ++i) 
    CC_invalidate_tlb(i->second->isacont);

  return 0;
}

// do_msync
//
//  Synchronize mmap to disk
//
//  Assumes page alignment for start and length.  This is a *very* inefficient
//  implementation because it looks up the ranges over and over and it writes
//  back clean pages as well.
//

static int
do_msync(Linux_context_t *osct, LSE_emu_addr_t start, LSE_emu_addr_t mlen,
	 bool errorifunmapped) {

  Linux_memory_t::ranges_t::iterator hv;
  
  while (mlen) {
    hv = osct->mem->vranges.upper_bound(start);
    if (hv == osct->mem->vranges.end())
      if (errorifunmapped) return -OSDefs::oENOMEM;
      else return 0;
    if (hv->second.start > start && errorifunmapped)
      return -OSDefs::oENOMEM;
	
    LSE_emu_addr_t rstart = std::max(hv->second.start, start);
    LSE_emu_addr_t rend = std::min(hv->second.end, start + mlen);

    mlen = start + mlen - rend;
    start = rend;

    if (!hv->second.isShared || !hv->second.fp ||
	!(hv->second.flags & PROT_WRITE) ||
	!hv->second.fp->getInodeID().dev) continue; // not shared writeable fp

    for (LSE_emu_addr_t pno = rstart ; pno < rend;
       pno += OSDefs::oPAGE_SIZE) {

      LSE_emu_addr_t paddr = osct->mem->vmdevice.lookup(pno, false);
      if (!paddr) continue; // never got into memory
      
      unsigned char p[OSDefs::oPAGE_SIZE];
      
      osct->mem->vmdevice.subspace->read(paddr, p, OSDefs::oPAGE_SIZE);
      // I am not going to check these results
      //CC_chkpt_arg_ptr(pno);
      //CC_chkpt_arg_hmem(p,OSDefs::oPAGE_SIZE);
      if (CC_chkpt_guard_true) {
	off_t off = hv->second.fp->lseek(0,SEEK_CUR);
	hv->second.fp->lseek(hv->second.offset + pno - hv->second.start, 
			     SEEK_SET);
	hv->second.fp->write(0, p, OSDefs::oPAGE_SIZE);
	hv->second.fp->lseek(off, SEEK_SET);
      }
    }
  }
  return 0;
}


// do_munmap
//
//  Remove a VA range from the ranges structure.  Remove from the
//  physical address space.  Invaliate softMMUs.
//
//  NOTE: only unmaps whole pages.  Returns an error if start is not on a full
//  full page.

static void
drop_pages(Linux_dinst_t *os, Linux_memory_t *mem, LSE_emu_addr_t start,
	   LSE_emu_addr_t mlen, Linux_memory_t::range_t &rg) {
  
  for (LSE_emu_addr_t pno = start ; pno < start + mlen ;
       pno += OSDefs::oPAGE_SIZE) {

    LSE_emu_addr_t paddr = mem->vmdevice.lookup(pno, false);
      
    if (paddr) {
      mem->vmdevice.delmap(pno);
      CC_drop_mapping(os, pno, paddr);

      LSE_emu_addr_t ppno = paddr / OSDefs::oPAGE_SIZE;

#ifdef LSE_LINUX_CHECKPOINT
      // If we are reading an initial checkpoint, do not mess with physical page
      // refcounting and freeing; the state of page allocation is 
      // handled in bulk.
      if (os->OSstate == 4) continue;
#endif

      if (!--(os->usedPpages[ppno].refcnt)) { // all references gone

	if (rg.isShared) {
	  Linux_dinst_t::fpagemap_t &pm = os->inode2pages[rg.fp->getInodeID()];
	  pm.offset2pno.erase(os->usedPpages[ppno].offset);
	  if (!pm.offset2pno.size()) os->inode2pages.erase(rg.fp->getInodeID());
	}

	os->usedPpages.erase(ppno);
 	mem->vmdevice.delchild(paddr, OSDefs::oPAGE_SIZE);

	if (os->freePpages.size()) {
	  if (os->freePpages.front().pno == ppno + 1) {
	    os->freePpages.front().num++;
	    os->freePpages.front().pno--;
	  } else if (os->freePpages.front().pno + 
		     os->freePpages.front().num == ppno) {
	    os->freePpages.front().num++;
	  } else {
	    Linux_dinst_t::prange_t np = { ppno, 1 };
	    os->freePpages.push_front(np);
	  }
	} else {
	  Linux_dinst_t::prange_t np = { ppno, 1 };
	  os->freePpages.push_front(np);
	}
      }
    } // if (paddr)
  }
}

static int
do_munmap(Linux_context_t *osct, LSE_emu_addr_t start, LSE_emu_addr_t mlen) {
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;
  bool is64bit = osct->is64bit;

  if ((start & amask) || start > OSDefs::oUTASK_SIZE(is64bit) || 
      start + mlen > OSDefs::oUTASK_SIZE(is64bit)) return -OSDefs::oEINVAL;
  
  mlen = (mlen + amask) & ~amask;
  if (!mlen) return -OSDefs::oEINVAL;

  // First, synchronize
  do_msync(osct, start, mlen, false);

  // Remove from the vm ranges

  Linux_memory_t::ranges_t::iterator hv;
  
 unmap_loop:
  hv = osct->mem->vranges.upper_bound(start);
  if (hv == osct->mem->vranges.end()) return 0;

  // invar:        AAAA
  //            ....X++++++
  if (hv->second.start < start + mlen) { // overlaps area
    drop_pages(osct->os, osct->mem,
	       start, std::min(start + mlen, hv->second.end) - start, 
	       hv->second);
    if (hv->second.start < start) { // split in two
      Linux_memory_t::range_t nv = { hv->second.start, start, 
				     hv->second.flags, hv->second.isShared,
				     hv->second.fp, hv->second.offset };
      hv->second.offset += start + mlen - hv->second.start;
      hv->second.start = start + mlen;
      osct->mem->vranges[nv.end] = nv;
      if (nv.fp) nv.fp->incr();
      if (hv->second.start >= hv->second.end) {
	if (hv->second.fp) hv->second.fp->close();
	osct->mem->vranges.erase(hv);
      }
    } else {
      hv->second.offset += start + mlen - hv->second.start;
      hv->second.start = start + mlen;
      if (hv->second.start >= hv->second.end) {
	if (hv->second.fp) hv->second.fp->close();
	osct->mem->vranges.erase(hv);
      }
    }
    goto unmap_loop;
  }

  // must invalidate everywhere!
  for (std::map<int, Linux_context_t *>::iterator i = osct->os->threads.begin(),
	 ie = osct->os->threads.end() ; i != ie ; ++i) 
    CC_invalidate_tlb(i->second->isacont);

  return 0;
}

// map_something
//
//  Insert a VA range which has been determined to be good to allocate
//  into the ranges structure.  If it overlaps an existing range, then
//  calls do_munmap to remove the overlapped portions.  Add to the physical
//  address space (in the real world, that would happen on page fault, but I
//  do not want the complication and this way is easier to checkpoint.)  
//  Invalidate softMMUs.
//
// Precondition: start/mlen are page-aligned
//
static Linux_memory_t::ranges_t::iterator 
map_something(Linux_context_t *osct,
	      LSE_emu_addr_t start, LSE_emu_addr_t mlen, int flags,
	      Linux_file_t *lfp = 0, bool isShared=false, 
	      LSE_emu_addr_t offset = 0) {

  //std::cerr << "Allocating " << std::hex << start << ":" 
  //	    << start + mlen << "\n";
  
  // Remove old mappings
  do_munmap(osct, start, mlen);

  // first one w/ hv->end > start
  Linux_memory_t::ranges_t::iterator hv, phv, rethv;
  phv = hv = osct->mem->vranges.upper_bound(start);
  if (hv == osct->mem->vranges.begin()) phv = osct->mem->vranges.end();
  else --phv;

  // NOTE: will not merge ranges around file mappings because I need to 
  // ensure that I set the offset correctly.
  bool rp = (hv != osct->mem->vranges.end() && hv->second.flags == flags &&
	     !hv->second.fp && !lfp && hv->second.start == start + mlen);

  bool lp = (phv != osct->mem->vranges.end() &&phv->second.flags == flags &&
	     !phv->second.fp && !lfp && phv->second.end == start);

  if (rp && lp) { // merge with both
    hv->second.start = phv->second.start;
    osct->mem->vranges.erase(phv);
    rethv = hv;
  } else if (rp) { // merge with right
    hv->second.start = start;
    rethv = hv;
  } else if (lp) { // merge with left
    Linux_memory_t::range_t nv = { phv->second.start, start+mlen, 
				   phv->second.flags, isShared, lfp, 0};
    osct->mem->vranges.erase(phv);
    osct->mem->vranges[nv.end] = nv;
    rethv = osct->mem->vranges.find(nv.end);
  } else { // no merging
    Linux_memory_t::range_t nv = { start, start+mlen, flags, isShared, lfp,
                                   offset };
    osct->mem->vranges[nv.end] = nv;
    rethv = osct->mem->vranges.find(nv.end);
    if (lfp) lfp->incr();
  }

  // must invalidate everywhere!
  for (std::map<int, Linux_context_t *>::iterator i = osct->os->threads.begin(),
	 ie = osct->os->threads.end() ; i != ie ; ++i) 
    CC_invalidate_tlb(i->second->isacont);

  return rethv;
}

static int do_brk(Linux_context_t *osct,
		  LSE_emu_addr_t start, LSE_emu_addr_t length) {
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;
  bool is64bit = osct->is64bit;

  length = (length + amask) & ~amask;
  if (!length) return start; // 0 length returns current break

  // prevent rollover
  if (start + length > OSDefs::oUTASK_SIZE(is64bit) ||
      start + length < start) return -OSDefs::oEINVAL;

  map_something(osct, start, length, OSDefs::oPROT_ALL);

  return 0;
}

// do_mmap_found
//   
//   Add a VA space and then clear it or read in the file that is mapped to it

static LSE_emu_addr_t 
do_mmap_found(Linux_context_t *osct, LSE_emu_addr_t start, 
	      LSE_emu_addr_t mlen, LSE_emu_addr_t offset,
	      int prot, int flags, Linux_file_t *lfp) {

  off_t sres=0;
  int mres=0;

  if ((flags & OSDefs::oMAP_ANONYMOUS) && 
      (flags & OSDefs::oMAP_SHARED)) {
    lfp = new Linux_anon_file_t(osct->os, osct->os->anonCounter++);
    lfp->incr();
  }

  Linux_memory_t::ranges_t::iterator hv =
    map_something(osct, start, mlen, prot, lfp, flags & OSDefs::oMAP_SHARED,
		  offset);

  if ((flags & OSDefs::oMAP_ANONYMOUS) && 
      (flags & OSDefs::oMAP_SHARED)) {
    lfp->close();
  }

  if (lfp && lfp->getInodeID().dev) {

    // When real files back up the mapping, we need to load the pages from
    // the file because the demand routine cannot checkpoint the results.
    // However, we do not want to double-read when pages are already there.
    // So, we check to see whether the mapping is ok.  But first, we
    // double-check the offset.
    // However, we should only do this for pages which are not already
    // allocated.
    
    off_t off = 0;
    if (CC_chkpt_guard_true) {
      off = lfp->lseek(0,SEEK_CUR);
      sres = lfp->lseek(offset, SEEK_SET);
      lfp->lseek(off, SEEK_SET); // return file pointer
    }
    CC_chkpt_result_int(sres);
    if (sres == (off_t)-1) return OSDefs::oEINVAL;
    
    // Touch each of the pages, causing it to be mapped if necessary.
    // then put together the checkpoint if necessary.

    if (CC_chkpt_guard_true) {
      off = lfp->lseek(0,SEEK_CUR);
      lfp->lseek(hv->second.offset + start - hv->second.start, 
		 SEEK_SET);
    }

    for (LSE_emu_addr_t eaddr = start;
	 eaddr < start + mlen; eaddr += OSDefs::oPAGE_SIZE) {
      unsigned char p[OSDefs::oPAGE_SIZE];

      LSE_emu_addr_t pa = osct->mem->vmdevice.lookup(eaddr, true);

      if (osct->os->usedPpages[pa/OSDefs::oPAGE_SIZE].refcnt != 1) {
	if (CC_chkpt_guard_true)
	  lfp->lseek(OSDefs::oPAGE_SIZE, SEEK_CUR);  // skip forward a page
	continue;
      }

      if (CC_chkpt_guard_true) lfp->read(0, p, OSDefs::oPAGE_SIZE);
      CC_chkpt_result_ptr(eaddr);
      CC_chkpt_result_hmem(p, OSDefs::oPAGE_SIZE);
      osct->mem->vmdevice.subspace->write(pa, p, OSDefs::oPAGE_SIZE);
    }

    if (CC_chkpt_guard_true) lfp->lseek(off, SEEK_SET); // return file pointer

  } // if file descriptor was given
  
  return start;
}

// do_mmap_faligned:
//
//  Find somewhere in the VA space to put a new allocation

LSE_emu_addr_t 
do_mmap_faligned(Linux_context_t *osct, LSE_emu_addr_t start, 
		 LSE_emu_addr_t mlen, LSE_emu_addr_t offset,
		 int prot, int flags, Linux_file_t *lfp) {

  // TODO: might need to force EXEC for readable
  if (!mlen) return static_cast<LSE_emu_addr_t>(-OSDefs::oEINVAL);

  LSE_emu_addr_t heapbreak, saveaddr;
  off_t sres=0;
  int mres=0;
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;
  int ret;
  bool is64bit = osct->is64bit;

  // Growing file mappings that are not anonymous are not a good thing?
  if (!(flags & OSDefs::oMAP_ANONYMOUS) && lfp &&
      (flags & (OSDefs::oPROT_GROWSDOWN|OSDefs::oPROT_GROWSUP))) {
    return static_cast<LSE_emu_addr_t>(-OSDefs::oEINVAL);
  }

  if (!(flags & OSDefs::oMAP_FIXED)) {
    start &= ~amask;
    if (start && start < OSDefs::oUNMAPPED_BASE(is64bit)) 
      start = OSDefs::oUNMAPPED_BASE(is64bit);
  }

  mlen = (mlen + OSDefs::oPAGE_SIZE-1) & ~amask;
  if (!mlen || mlen > OSDefs::oUTASK_SIZE(is64bit)) 
    return static_cast<LSE_emu_addr_t>(-OSDefs::oENOMEM);
  
  if (offset + mlen < offset) 
    return static_cast<LSE_emu_addr_t>(-OSDefs::oEOVERFLOW);

  Linux_memory_t::ranges_t::iterator hv;

  heapbreak = start;
  if (flags & OSDefs::oMAP_FIXED) goto foundaddr;

  if (heapbreak) { // look for a particular address.
    heapbreak = heapbreak & ~amask;
    hv = osct->mem->vranges.upper_bound(heapbreak);
    if ((hv == osct->mem->vranges.end() ||
	 heapbreak + mlen <= hv->second.start) &&
	heapbreak + mlen <= OSDefs::oUTASK_SIZE(is64bit)) goto foundaddr;
  }

  heapbreak = (OSDefs::oUSTACK_BASE(is64bit) & ~amask) - mlen; // try just below stack
  do {
    hv = osct->mem->vranges.upper_bound(heapbreak);
    if ((hv == osct->mem->vranges.end() ||
	 heapbreak + mlen <= hv->second.start)) goto foundaddr;
    heapbreak = hv->second.start - mlen; // try just below this one
  } while (mlen < hv->second.start);

  // Did not find a hole going down, now let us try going up.
  heapbreak = OSDefs::oUNMAPPED_BASE(is64bit);
  for (hv = osct->mem->vranges.upper_bound(heapbreak); ; ++hv) {
    if ((hv == osct->mem->vranges.end() ||
	 heapbreak + mlen <= hv->second.start)) goto foundaddr;
    heapbreak = hv->second.end;
  }
  return static_cast<LSE_emu_addr_t>(-OSDefs::oENOMEM);

 foundaddr:

  if (lfp) {
    if ((flags & OSDefs::oMAP_TYPE)==OSDefs::oMAP_SHARED) {
    } else if ((flags & OSDefs::oMAP_TYPE)==OSDefs::oMAP_PRIVATE) {
    } else { 
      return static_cast<LSE_emu_addr_t>(-OSDefs::oEINVAL);
    }
  } else {
    if ((flags & OSDefs::oMAP_TYPE)==OSDefs::oMAP_SHARED) {
      offset = 0;
    } else if ((flags & OSDefs::oMAP_TYPE)==OSDefs::oMAP_PRIVATE) {
    } else { 
      return static_cast<LSE_emu_addr_t>(-OSDefs::oEINVAL);
    }
  }

  return do_mmap_found(osct, heapbreak, mlen, offset, prot, flags, lfp);
}

static LSE_emu_addr_t 
do_mmap(Linux_context_t *osct, LSE_emu_addr_t start, LSE_emu_addr_t length,
	LSE_emu_addr_t offset, int prot, int flags, Linux_file_t *lfp) {
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;
  LSE_emu_addr_t nl = (length + OSDefs::oPAGE_SIZE - 1) & ~amask;
  if ((offset + nl) < offset) return -OSDefs::oEINVAL;
  if (offset & amask) return -OSDefs::oEINVAL;
  return do_mmap_faligned(osct, start, length, offset, prot, flags, lfp);
}

/*********************** path resolution *************************/

static char * normalize_path(char *s) {
  char *p;
  // Only works for an absolute path
  int l = strlen(s);
  while ((p = strstr(s, "/../"))) {
    // find the next one up...
    *p = 0;
    char *t = strrchr(s, '/');
    if (!t) t = s;
    memmove(t, p+3, l - 2 - (p-s));
    l = strlen(s);
  }
  while ((p = strstr(s, "/./"))) {
    memmove(p, p+2, l - 1 - (p-s));
    l-= 2;
  }
  p = strstr(s, "/..");
  if (p && !*(p+3)) {
    *p = 0;
    char *t = strrchr(s, '/');
    if (!t) *(p+1)=0;
    else *(t+1) = 0;
  }
  p = strstr(s, "/.");
  if (p && !*(p+2)) *(p+1) = 0;    
  return s;
}

static char *resolve_path(char *fnamebuf, Linux_context_t *osct) {
  if (fnamebuf[0] == '/') { // absolute pathname
    char *s = (char *)malloc(strlen(fnamebuf) + strlen(osct->fs->fsroot)+1);
    strcpy(s, osct->fs->fsroot);
    strcat(s, fnamebuf);
    return s;
  } else {
    char *s = (char *)malloc(strlen(fnamebuf) + strlen(osct->fs->cwd)+2);
    strcpy(s, osct->fs->cwd);
    strcat(s, "/");
    strcat(s, fnamebuf);
    return s;
  }
}

/************************ context management ********************/

int Linux_context_clone(LSE_emu_interface_t *ifc,
    			CC_isacontext_t **newisact,
    			CC_isacontext_t *oldisact) {
  Linux_context_t *oldc;
  bool frominit = !oldisact;
  if (!oldisact) {
     oldc = OS_interface(ifc)->threads[1];
     oldisact = oldc->isacont;
  } else oldc = OS_ict_osinfo(oldisact);

  int rval = Linux_cloneguts(newisact, oldisact, 
      	     		     OSDefs::oCLONE_VM|OSDefs::oCLONE_VFORK,
			     0, 0, false, CC_ict_startaddr(oldisact),0,0,0);
  if (rval) return ENOMEM;

  if (frominit) { // need to set up a new process group for this context
    Linux_context_t *osct = OS_ict_osinfo(*newisact);
    int pid = osct->tgroup->tgid;
    Linux_tgroup_t *tgrp = osct->tgroup;

    tgrp->pgroup->members.remove(tgrp);
    tgrp->pgroup->decr();

    int newpgid = tgrp->tgid;

    Linux_pgroup_t *newpg = osct->os->pgroups[newpgid];
    if (!newpg) {
      tgrp->pgroup = new Linux_pgroup_t(osct->os, newpgid);
      tgrp->pgroup->members.push_back(tgrp);
    } else {
      tgrp->pgroup = newpg;
      newpg->members.push_back(tgrp);
      newpg->incr();
    }
  }
  return 0;
}

int Linux_cloneguts(CC_isacontext_t **newisactp,
	            CC_isacontext_t *oldisact,
                    int flags,
	            LSE_emu_addr_t stackstart, LSE_emu_addr_t stacksize,
		    bool fromClone,
		    LSE_emu_iaddr_t startpc,
		    LSE_emu_addr_t ptidp,
		    LSE_emu_addr_t ctidp,
		    LSE_emu_addr_t tlsp) {

  Linux_context_t *newosct;
  Linux_context_t *oldosct = OS_ict_osinfo(oldisact);
  Linux_dinst_t *ldi = oldosct->os;
  CC_isacontext_t *newisact;

  // check the flags for impossible combinations

  if ((flags & (OSDefs::oCLONE_THREAD) && !(flags & OSDefs::oCLONE_SIGHAND)))
     return -OSDefs::oEINVAL; 
  if ((flags & (OSDefs::oCLONE_SIGHAND) && !(flags & OSDefs::oCLONE_VM)))
     return -OSDefs::oEINVAL;
 
  /* Create the new context, copying/sharing from parent.  Exact semantics
   * depends upon the instruction set.
   */

  bool rolled = false;
  int newtid;
  do {
    newtid = ++ldi->last_pid;
    if (newtid < 2) { 
      if (rolled) return -OSDefs::oEAGAIN;
      newtid = ldi->last_pid = 2; 
      rolled = true;
    }
  } while (ldi->threads.find(newtid) != ldi->threads.end());
  newosct = new Linux_context_t(ldi);
  if (!newosct) return -OSDefs::oENOMEM;

  int rval = CC_context_copy(&CC_emu_interface(oldisact), 
			     &newisact, oldisact, 
      	     		     flags & OSDefs::oCLONE_VM);
  if (rval) {
    delete newosct;
    return -OSDefs::oENOMEM;
  }

  *newisactp = newisact;

  if (fromClone) CC_ict_startaddr(newisact) = startpc;

  /* get a new pid */
  // allocate task structure ... 
  CC_ict_osinfo(newisact) = newosct;
  uint64_t saveid = newosct->oid;
  *newosct = *oldosct; // copy everything to start with
  newosct->state = ProcBlocked;  // new context is blocked to start
  newosct->oid = saveid; // give it a unique identifier
  newosct->refcount = 1; // and fix the refcounts!

  // set/clear stuff that is new each time
  newosct->tid = newtid;
  newosct->reportVforkDone.waiters.clear();
  newosct->children.clear();
  newosct->sigpending = 0;
  newosct->sigPendingFlag = false;
  newosct->oldSigBlockedValid = false;
#ifdef LSE_LINUX_CHECKPOINT
  {
    Linux_context_t *osct = oldosct;
    if (!CC_chkpt_reading) newosct->clear_chkpt(101);
    else { 
      newosct->currOSRecord = ldi->osCalls[newtid]; 
      if (newosct->currOSRecord) 
	newosct->currOSRecord = newosct->currOSRecord->oldestChild;
    }
  }
#endif

  newosct->parent = (flags & (OSDefs::oCLONE_THREAD|OSDefs::oCLONE_PARENT)) 
    ? oldosct->parent : oldosct;
  newosct->isacont = newisact;
  newosct->signalOnExit = (flags & OSDefs::oCLONE_THREAD) ? -1 :
                          (flags & OSDefs::oCLONE_EXITSIGNAL);

  if (flags & (OSDefs::oCLONE_VM)) {
    newosct->mem->incr();
  } else { 
    newosct->mem = new Linux_memory_t(oldosct->os, oldosct->mem);
    CC_mem_redirect(newisact, &newosct->mem->vmdevice);
  }

  if (flags & (OSDefs::oCLONE_FILES)) {
    newosct->fdtable->incr();
  } else newosct->fdtable = new Linux_fdtable_t(oldosct->os,
						oldosct->fdtable);

  if (flags & OSDefs::oCLONE_SIGHAND) {
    newosct->sighandtable->incr();
  } else newosct->sighandtable 
           = new Linux_sighandtable_t(oldosct->os, oldosct->sighandtable);

  if (flags & OSDefs::oCLONE_THREAD) {
    newosct->tgroup->incr();
    newosct->tgroup->nLive++;
  } else {
    newosct->tgroup = new Linux_tgroup_t(oldosct->os, newtid);
    newosct->tgroup->pgroup = oldosct->tgroup->pgroup;
    oldosct->tgroup->pgroup->members.push_back(newosct->tgroup);
    oldosct->tgroup->pgroup->incr();
    newosct->tgroup->timers[0] = new Linux_sig_timer_t(oldosct->os, 0, 
						       newosct->tgroup,
						       14 /*SIGALRM*/, 
						       newosct);
    newosct->tgroup->timers[1] = new Linux_sig_timer_t(oldosct->os, 2, 
						       newosct->tgroup,
						       26 /*SIGVTALRM*/, 
						       newosct);
    newosct->tgroup->timers[2] = new Linux_sig_timer_t(oldosct->os, 2, 
						       newosct->tgroup,
						       27 /*SIGPROF*/, 
						       newosct);
  }

  if (flags & OSDefs::oCLONE_FS) {
    newosct->fs->incr();
  } else {
    newosct->fs = new Linux_fs_t(oldosct->os, oldosct->fs);
  }
  
  /* processor-specific copies (copy_thread) */
  CC_cloneguts();

  newosct->parent->children.push_back(newosct);
  newosct->tgroup->members.push_back(newosct);

  ldi->threads[newosct->tid] = newosct;

  if (flags & OSDefs::oCLONE_CHILD_CLEARTID) {
    newosct->clear_child_tid = ctidp;
  } else newosct->clear_child_tid = 0;

  if (flags & OSDefs::oCLONE_PARENT_SETTID) {
    int nval = LSE_h2e((int)newosct->tid, Linux_bigendian);
    if (OS_mem_write(oldisact,ptidp,4,&nval)) return -OSDefs::oEFAULT;
    if (OS_mem_write(newisact,ptidp,4,&nval)) return -OSDefs::oEFAULT;
  }

  if ((flags & (OSDefs::oCLONE_VM|OSDefs::oCLONE_VFORK)) == 
      OSDefs::oCLONE_VM) {
    newosct->sigAlt.LSE_ss_sp = 0;
    newosct->sigAlt.LSE_ss_size = 0;
  }

  // do last; should really be done right before scheduling the task

  if (flags & OSDefs::oCLONE_CHILD_SETTID) {
    int nval = LSE_h2e((int)newosct->tid, Linux_bigendian);
    if (OS_mem_write(newisact,ctidp,4,&nval)) return -OSDefs::oEFAULT;
  }

  // TODO: when CLONE_THREAD, deal with exiting or stopping in progress
  
  return 0;
}

void Linux_context_getcoderegion(void *oldct,
     std::list< std::pair<LSE_emu_addr_t, LSE_emu_addr_t> > &c) {

  CC_isacontext_t *oldisact = reinterpret_cast<CC_isacontext_t *>(oldct); 
  Linux_context_t *oldc = OS_ict_osinfo(oldisact);

  c = oldc->mem->codelocs;
}

static int pad_page(Linux_context_t *osct, LSE_emu_addr_t start) {
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;
  return (CC_mem_memset(&osct->mem->vmdevice, start, 0,
                        OSDefs::oPAGE_SIZE - (start & amask)) ? 1 : 0);
}

static int set_brk(Linux_context_t *osct,
		   LSE_emu_addr_t start, LSE_emu_addr_t end) {
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;
  start = (start + amask) & ~amask;
  end = (end + amask) & ~amask;
  if (end > start) {
    int res = do_brk(osct, start, end-start);
    if (res) return res;
  }
  osct->mem->heapstart = osct->mem->heapbreak = end;
  return 0;
}

static int handle_psect(int psect_num, uint64_t length,
	                int fixed, LSE_elf::ElfB_Phdr *hdrp,
			FILE *fp,
			void *vct, int is64bit) {
  psect_context *pct = reinterpret_cast<psect_context *>(vct);
  CC_isacontext_t *realct = pct->ct;
  Linux_context_t *osct=OS_ict_osinfo(realct);
  uint64_t p_offset = is64bit ? hdrp->h64.p_offset : hdrp->h32.p_offset;
  uint64_t p_filesz = is64bit ? hdrp->h64.p_filesz : hdrp->h32.p_filesz;
  uint64_t p_vaddr = is64bit ? hdrp->h64.p_vaddr : hdrp->h32.p_vaddr;
  uint64_t p_memsz = is64bit ? hdrp->h64.p_memsz : hdrp->h32.p_memsz;
  uint32_t p_flags = is64bit ? hdrp->h64.p_flags : hdrp->h32.p_flags;

  LSE_emu_addr_t loadaddr;
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;

  if (OS_ict_emuvar(realct,EMU_debugcontextload))
    std::cerr << std::hex << "p "
	      << std::setw(16) << std::setfill('0') << p_offset << " "
	      << std::setw(16) << std::setfill('0') << p_filesz << " "
	      << std::setw(16) << std::setfill('0') << p_vaddr << " "
	      << std::setw(16) << std::setfill('0') << p_memsz
	      << std::dec << std::endl;

  if (hdrp->h32.p_type == 3 /*PT_INTERP*/) pct->use_interp = true;

    /* we should not be doing anything with non-load sections */
  if (hdrp->h32.p_type != 1) return 0;

  if (pct->brk > pct->bss) { // map and clear bss section from previous area
    if (set_brk(osct, pct->bss + pct->loadbias, pct->brk + pct->loadbias)) {
      fprintf(stderr, "Unable to set brk\n");
      return 1;
    }
    if (pct->bss & amask) {
      uint64_t tempaddr = pct->bss + pct->loadbias;
      uint64_t templen = OSDefs::oPAGE_SIZE - (pct->bss & amask);
      if (templen > pct->brk - pct->bss) templen = pct->brk - pct->bss;
      if (CC_mem_memset(&osct->mem->vmdevice,tempaddr,0,templen)) return 1;
    }
  }

  int prot = 0;
  if (p_flags & 4) prot |= OSDefs::oPROT_READ;
  if (p_flags & 2) prot |= OSDefs::oPROT_WRITE;
  if (p_flags & 1) prot |= OSDefs::oPROT_EXEC;

  int flags;

  if (fixed || pct->loadbiasknown) {
    flags = OSDefs::oMAP_FIXED|OSDefs::oMAP_PRIVATE;
  } else {
    flags = OSDefs::oMAP_PRIVATE;
    pct->loadbias = -p_vaddr & ~amask;
  }

  LSE_emu_addr_t size = ( (p_filesz + (p_vaddr & amask) + amask) & ~amask );
  LSE_emu_addr_t off = (p_offset - (p_vaddr & amask));
  LSE_emu_addr_t addr = p_vaddr + pct->loadbias;
  LSE_emu_addr_t addrA = addr & ~amask;

  if (size != 0) {
    LSE_emu_addr_t ret;

    if (!pct->lfp) { // must dup the descriptor so that the file stays open! 
      pct->lfp = new Linux_native_file_t(osct->os, dup(fileno(fp)));
      pct->lfp->incr(); // the file open does *not* increment refcount!
    }

    if (!fixed && !pct->loadbiasknown) {
      ret = do_mmap(osct, addrA, length, off, prot, flags, pct->lfp);
      if (ret < OSDefs::oUTASK_SIZE(is64bit)) 
        do_munmap(osct, ret + size, length - size);
    } else {
      ret = do_mmap(osct, addrA, size, off, prot, flags, pct->lfp);
    }
    
#ifdef LSE_LINUX_CHECKPOINT
  if (pct->seqToUse && pct->seqToUse->sibling) { // adopt mmap records
    // last one added.
    LSE_chkpt::data_t *tdp = LSE_chkpt::build_sequence(pct->seqToUse);

    LSE_chkpt::build_unsigned(tdp, addrA);
    LSE_chkpt::build_unsigned(tdp, size);
    LSE_chkpt::build_unsigned(tdp, length);
    LSE_chkpt::build_signed(tdp, prot);
    LSE_chkpt::build_signed(tdp, flags);
    LSE_chkpt::build_unsigned(tdp, off);
    LSE_chkpt::build_boolean(tdp, !fixed && !pct->loadbiasknown);
    LSE_chkpt::build_unsigned(tdp, ret);
    LSE_chkpt::build_boolean(tdp, !!pct->lfp);

    if (pct->lfp) {
      LSE_chkpt::build_signed(tdp, pct->lfp->statbuf.st_dev);
      LSE_chkpt::build_signed(tdp, pct->lfp->statbuf.st_ino);
    } else {
      LSE_chkpt::build_signed(tdp, 0);
      LSE_chkpt::build_signed(tdp, 0);
    }

    LSE_chkpt::data_t *dp = pct->seqToUse->sibling;
    while (dp) {
      LSE_chkpt::data_t *p = dp;
      dp = dp->sibling;
      tdp->adopt_child(p);
    }
    //LSE_chkpt::build_indefinite_end(tdp);
    pct->seqToUse->sibling = 0;
    pct->seqToUse->parent->youngestChild = pct->seqToUse;
  }
#endif

    if (ret >= OSDefs::oUTASK_SIZE(is64bit)) {
      std::cerr << std::hex << addr << " " << size << " " << off << " " << ret
		<< std::dec << "\n";
      fprintf(stderr, "Memory error: unable to map load segment\n");
      return 1;
    }
    if (!pct->loadbiasknown) {
      pct->baseaddr = p_vaddr - p_offset;
      pct->loadbiasknown = true;
      if (!fixed) {
	pct->loadbias += ret - (addr & ~amask);
	pct->baseaddr += pct->loadbias;
      }
    }
  }

  if (p_vaddr < pct->start_code) pct->start_code = p_vaddr;
  if (pct->start_data < p_vaddr) pct->start_data = p_vaddr;
      
  LSE_emu_addr_t tmpa = p_vaddr + p_filesz;
  if (tmpa > pct->bss) pct->bss = tmpa;
  if ((p_flags & 1) && pct->end_code < tmpa) pct->end_code = tmpa;
  if (pct->end_data < tmpa) pct->end_data = tmpa;
  tmpa = p_vaddr + p_memsz;
  if (tmpa > pct->brk) pct->brk = tmpa;
  
  CC_psect_hook();

  return 0;
}

static int handle_ssect(LSE_elf::ElfB_Shdr *hdrp, void *vct, int is64bit) {
  /* IA64 executables actually have read-only sections which are aligned to 
     16 byte boundaries mixed into the same OS page with non-read-only.
     I am not going to track read-only attributes to 16 byte granularity..
     that is ludicrous!  So... we are droppping read-only attributes
     entirely.
  */
  psect_context *pct = reinterpret_cast<psect_context *>(vct);
  CC_isacontext_t *realct = pct->ct;
  Linux_context_t *osct=OS_ict_osinfo(realct);

  if (is64bit) {
     if (hdrp->h64.sh_flags & 0x4) 
     	osct->mem->codelocs.push_back(std::pair<LSE_emu_addr_t,
					LSE_emu_addr_t>(hdrp->h64.sh_addr,
						      hdrp->h64.sh_addr +
						      hdrp->h64.sh_size - 1));
  } else {
     if (hdrp->h32.sh_flags & 0x4) 
     	osct->mem->codelocs.push_back(std::pair<LSE_emu_addr_t,
					LSE_emu_addr_t>(hdrp->h32.sh_addr,
						      hdrp->h32.sh_addr +
						      hdrp->h32.sh_size - 1));
  }

  if (OS_ict_emuvar(realct,EMU_debugcontextload)) {
    if (is64bit) 
      fprintf(stderr, 
	     "s  %" PRIx64"  %" PRIx64 "  %" PRIx64 "\n",
            hdrp->h64.sh_offset,hdrp->h64.sh_addr,hdrp->h64.sh_size);
    else
      fprintf(stderr, 
	     "s  %" PRIx32"  %" PRIx32 "  %" PRIx32 "\n",
            hdrp->h32.sh_offset,hdrp->h32.sh_addr,hdrp->h32.sh_size);
  }	
  return 0;
}

int 
Linux_start_thread(CC_isacontext_t *realct, LSE_emu_iaddr_t ip,
		   LSE_emu_addr_t sp, int argc, LSE_emu_addr_t argv,
		   LSE_emu_addr_t envp);

#define NUM_AUXV_ENTRIES (10 + OSDefs::oNUM_DLINFO_ENTRIES)

static bool OS_auxv_entry_int(CC_isacontext_t *realct,
			      LSE_emu_addr_t &sbase,
			      LSE_emu_addr_t &stemp8,
			      char *stp8,
			      bool is64bit,
			      LSE_emu_addr_t x, 
			      LSE_emu_addr_t y) {
  LSE_emu_addr_t _auxv_v1 = (LSE_emu_addr_t)(x);
  LSE_emu_addr_t _auxv_v2 = (LSE_emu_addr_t)(y);
  stemp8 = LSE_h2e(_auxv_v1, Linux_bigendian);
  if (OS_mem_write(realct,sbase,CC_addrsize,stp8)) return true;
  sbase +=CC_addrsize;
  stemp8 = LSE_h2e(_auxv_v2, Linux_bigendian); /* actual page size */
  if (OS_mem_write(realct,sbase,CC_addrsize,stp8)) return true;
  sbase +=CC_addrsize;
  return false;
}

#define OS_auxv_entry(x,y) \
	  if (OS_auxv_entry_int(realct, sbase, stemp8, stp8, is64bit, x, y)) \
	    goto badloadmem;

int Linux_execveguts(CC_isacontext_t *realct, char *filename, int argc,
		     char *argv[], char **envp) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int fd;
  LSE_elf::error_t retval;
  LSE_emu_addr_t stacksize, sbase, slen, start_stack;
  LSE_emu_addr_t program_entry;
  LSE_emu_addr_t vargv,venvp;
  uint64_t saddr;
  LSE_emu_addr_t stemp8;
  int numenv;
  char **sp;
  int i;
  int match;
  int is64bit;
  char *stp8;
  int ecode = 0;
  LSE_emu_addr_t ret;
  LSE_emu_addr_t entry, phoff;
  int phentsize, phnum;
  char iclass;

  if (CC_chkpt_guard_true) {
    char buf[4];
    LSE_elf::ElfB_Ehdr topheader;

    /* open file (should ensure it is executable)  and make sure it is an ELF.*/
    fd = open(filename,O_RDONLY);
    if (fd<0) return -errno;

    /* Read in first part of file (128 bytes) */
    memset(buf,0,4);
    read(fd,buf,4);
    if (memcmp(buf,LSE_elf::ELFMAG,LSE_elf::SELFMAG)) ecode=-OSDefs::oENOEXEC;
    else {
      close(fd);

      match = (CC_elftype == 64) ? 2 : (CC_elftype == 32) ? 1 : 0;
      retval = LSE_elf::check(filename, CC_elfarchchecker, &topheader, match);
      if (retval) ecode = -OSDefs::oENOEXEC;
    }
    iclass = topheader.h32.e_ident[LSE_elf::EI_CLASS];
  }
  CC_chkpt_result_int(ecode);
  if (ecode) return ecode;
  CC_chkpt_result_int(iclass);

  psect_context pct(realct);
  psect_context ict(realct);
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;

  // save old memory (and sync it before destroying it)
  typeof(CC_ict_mem(realct)) saveisamem = CC_ict_mem(realct);
  typeof(osct->mem) savemem = osct->mem;
  do_msync(osct, 0, LSE_emu_addr_t(0) - OSDefs::oPAGE_SIZE, false);
  bool saveis64bit = is64bit;

  osct->is64bit = is64bit = iclass != LSE_elf::ELFCLASS32;
  osct->mem = new Linux_memory_t(osct->os);
  osct->mem->vmdevice.subspace = OS_ict_emuvar(realct, mem);
  CC_mem_redirect(realct, &osct->mem->vmdevice);

  // calculate the size of the argument pages

  stacksize = (4 /* argc + argv pad + envp pad + endpad */ 
	       + 2 /* scratch */+(2*NUM_AUXV_ENTRIES)  /* auxv */) 
    * CC_addrsize;

  if (!envp) {
    stacksize += CC_addrsize;
    numenv = 0;
  } else 
    for (sp=envp,numenv=0;*sp;sp++,numenv++) {
      stacksize+=CC_addrsize; /* for envp itself */
      stacksize+=strlen(*sp)+1; /* for contents */
    }
  for (i=0;i<argc;i++) {
    stacksize+=CC_addrsize; /* for argv pointer */
    stacksize+=strlen(argv[i])+1;
  }
  stacksize += 4 * CC_addrsize; /* for aux vector */
  stacksize = (stacksize+15) & ~15;  /* align the stack */

  sbase = OSDefs::oUSTACK_TOP(is64bit) - stacksize;

  start_stack = sbase;

  /* Map the argument pages into user space.  If the ISA memory abstraction
   * is not able to grow the stack on page fault, then allocate the whole stack.
   */
  Linux_memory_t::range_t nv;
  if (!OSDefs::oSTACKGROWS) nv.start = OSDefs::oUSTACK_BASE(is64bit) & ~amask;
  else nv.start = start_stack & ~amask;
  nv.end = (OSDefs::oUSTACK_TOP(is64bit) + amask) & ~amask;
  nv.flags = OSDefs::oPROT_ALL|OSDefs::oPROT_STACKDIR;
  nv.isShared = false;
  nv.fp = 0;

  osct->mem->vranges[nv.end] = nv;
  //std::cerr << "Allocating " << std::hex << nv.start << ":" << nv.end 
  //	      << std::dec << "\n";
  do_mmap(osct, nv.start, nv.end - nv.start, 0,
	  OSDefs::oPROT_ALL|OSDefs::oPROT_STACKDIR, 
	  OSDefs::oMAP_PRIVATE|OSDefs::oMAP_FIXED|OSDefs::oMAP_ANONYMOUS,
	  0);

  if (CC_chkpt_guard_true) {
    LSE_elf::ElfB_Ehdr topheader;

#ifdef LSE_LINUX_CHECKPOINT
    if (osct->os->ctl.recordOS && osct->os->OSstate==1) {
      pct.seqToUse = LSE_chkpt::build_sequence(CC_chkpt_result_loc);
    } else pct.seqToUse = 0;
#endif
    retval = LSE_elf::load(filename, CC_elfarchchecker, &topheader, 
		           match, handle_psect, handle_ssect, &pct);
    if (pct.lfp) pct.lfp->close(); // drop the reference created by ELF load
    pct.lfp = 0;
    if (retval == LSE_elf::error_outofmemory) ecode = -OSDefs::oENOMEM;
    else if (retval) ecode = -OSDefs::oENOEXEC;

    entry = is64bit ? topheader.h64.e_entry : topheader.h32.e_entry;
    phoff = is64bit ? topheader.h64.e_phoff : topheader.h32.e_phoff;
    phentsize = is64bit ? topheader.h64.e_phentsize : topheader.h32.e_phentsize;
    phnum = is64bit ? topheader.h64.e_phnum : topheader.h32.e_phnum; 
  } else { 
#ifdef LSE_LINUX_CHECKPOINT
    parse_mmap_records(osct, osct->currRes);
    osct->currRes = osct->currRes->sibling;
#endif
  }

  CC_chkpt_result_int(ecode);
  if (ecode) goto badload;
  CC_chkpt_result_psect(pct);
  CC_chkpt_result_ptr(entry);
  CC_chkpt_result_ptr(phoff);
  CC_chkpt_result_int(phentsize);
  CC_chkpt_result_int(phnum);

  ict.loadbias = pct.loadbias;
  CC_ict_true_startaddr(realct) = entry + pct.loadbias;
  pct.bss += pct.loadbias;
  pct.brk += pct.loadbias;
  pct.start_code += pct.loadbias;
  pct.end_code += pct.loadbias;
  pct.start_data += pct.loadbias;
  pct.end_data += pct.loadbias;

  // create pages for bss section
  if (set_brk(osct, pct.bss, pct.brk)) goto badload;
  if (pct.bss != pct.brk && pad_page(osct, pct.bss)) {
    ecode = -OSDefs::oEFAULT;
    goto badload;
  }

  program_entry = CC_ict_true_startaddr(realct);

  if (OS_ict_emuvar(realct, EMU_debugcontextload)) {
    std::cerr << std::hex << "code:[" << pct.start_code << ":" 
	      << pct.end_code << "] data:[" << pct.start_data << ":"
	      << pct.end_data << "] bss:" << pct.bss << " brk:"
	      << pct.brk << " entry:" << program_entry << " baseaddr:"
	      << pct.baseaddr << std::dec << "\n";
  }

  if (pct.use_interp) {
    LSE_elf::ElfB_Ehdr interpheader;
    char fname[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3]; 
    const char *filename = OSDefs::oDYNLOADER;
    struct stat statbuf;

    if (CC_chkpt_guard_true) {
      strcpy(fname,filename);

      if (!stat(fname,&statbuf)) {}
      else if (filename[0] != 0x2f) { /* NOTE: cannot use single quotes in m4*/
        /* time to search the LD_LIBRARY_PATH */
        char **ep = envp;
        while (ep && *ep) {
	  if (!strncmp(*ep,"LD_LIBRARY_PATH",15)) {
	    char *totokenize = (char *)malloc(strlen(*ep)+1);
  	    strcpy(totokenize, (*ep)+16);
            char *p = totokenize;
	    while (*p) {
	      char *pe = strchr(p, 0x3a); /* NOTE: no single quotes in m4! */
	      if (pe) *pe = 0;
	      sprintf(fname,"%s/%s",p, filename);
	      if (!stat(fname, &statbuf)) {
	        free(totokenize);
	        goto interpsearchdone;
	      }
	      if (!pe) break;
	      p = pe+1;
	    }
	    free(totokenize);
 	    ecode = -OSDefs::oENOEXEC;
	    goto interpbad;
	  }
	  ep++;
        } /* while environment */
      } else {
 	ecode = -OSDefs::oENOEXEC;
	goto interpbad;
      }

    interpsearchdone:

      ict.use_interp = true;

#ifdef LSE_LINUX_CHECKPOINT
      if (osct->os->ctl.recordOS && osct->os->OSstate==1) {
        ict.seqToUse = LSE_chkpt::build_sequence(CC_chkpt_result_loc);
      } else ict.seqToUse = 0;
#endif
      retval = LSE_elf::load(fname, CC_elfarchchecker,
			     &interpheader, match,
			     handle_psect, handle_ssect, &ict);
      if (ict.lfp) ict.lfp->close(); // drop reference created in ELF load
      ict.lfp = 0;

      if (retval == LSE_elf::error_outofmemory) ecode = -OSDefs::oENOMEM;
      else if (retval) ecode = -OSDefs::oENOEXEC;

    interpbad: ;
    } else { 
#ifdef LSE_LINUX_CHECKPOINT
      parse_mmap_records(osct, osct->currRes);
      osct->currRes = osct->currRes->sibling;
#endif
    }
    CC_chkpt_result_int(ecode);
    if (ecode) goto badload;
    CC_chkpt_result_hmem(&interpheader, sizeof(interpheader));
    CC_chkpt_result_psect(ict);

    is64bit = interpheader.h32.e_ident[LSE_elf::EI_CLASS] != 
      LSE_elf::ELFCLASS32;
    CC_ict_true_startaddr(realct) = ict.baseaddr + 
      (is64bit ? interpheader.h64.e_entry : interpheader.h32.e_entry);

    ict.bss += ict.loadbias;
    ict.brk += ict.loadbias;
    ict.start_code += ict.loadbias;
    ict.end_code += ict.loadbias;
    ict.start_data += ict.loadbias;
    ict.end_data += ict.loadbias;

    if (ict.brk > ict.bss) { // need to fill out bss section
      if (pad_page(osct, ict.bss)) {
	ecode = -OSDefs::oEFAULT;
	goto badload;
      }
      ict.bss = (ict.bss + amask) & ~amask;
      do_brk(osct, ict.bss, ict.brk - ict.bss);
    }

    if (OS_ict_emuvar(realct, EMU_debugcontextload)) {
      std::cerr << std::hex << "code:[" << ict.start_code << ":" 
		<< ict.end_code << "] data:[" << ict.start_data << ":"
		<< ict.end_data << "] bss:" << ict.bss << " brk:"
		<< ict.brk << " entry:" << CC_ict_true_startaddr(realct)
		<< " baseaddr:" << ict.baseaddr
		<< std::dec << "\n";
    }

  } // pct.use_interp

  ecode = -OSDefs::oENOMEM;

  /* set up the argument pages now... */
  // Ugh.... 
  //  When address size is not 8 bytes, we need to adjust.
  //  Target BE, host BE: +4
  //  Target BE, host LE: +4
  //  Target LE, host LE: 0
  //  Target LE, host BE: 0
  stp8 = ((char *)&stemp8 + 
	  ((CC_addrsize != sizeof(stemp8) && Linux_bigendian) ? 4 : 0));

  sbase = start_stack;

  /* write argc */
  stemp8 = LSE_h2e((LSE_emu_addr_t)argc, Linux_bigendian);
  sbase += 16;
  if (OS_ict_emuvar(realct,EMU_debugcontextload))
    fprintf(stderr,"Argc at " LSE_emu_addr_t_print_format "\n",
	    sbase);

  if (OS_mem_write(realct,sbase,CC_addrsize,stp8)) goto badloadmem;
  
  /* copy arguments */
  saddr = (numenv + argc + 2)*CC_addrsize + sbase + CC_addrsize + (2*NUM_AUXV_ENTRIES)*CC_addrsize; /* first string location */

  sbase += CC_addrsize;
	
  if (OS_ict_emuvar(realct,EMU_debugcontextload))
    fprintf(stderr,"Starting %d argument strings at " LSE_emu_addr_t_print_format "\n",
	    argc, saddr);
  vargv = saddr;	
  for (i=0;i<argc;i++) {
    /* write argv value */

    stemp8 = LSE_h2e((LSE_emu_addr_t)saddr, Linux_bigendian);
    if (OS_mem_write(realct,sbase,CC_addrsize,stp8)) goto badloadmem;

    sbase += CC_addrsize;
    slen = strlen(argv[i])+1;
    if (OS_mem_write(realct,saddr,slen,argv[i])) goto badloadmem;
    saddr += slen;
  }

  stemp8 = 0;
  if (OS_mem_write(realct,sbase,CC_addrsize,stp8)) goto badloadmem;
  sbase += CC_addrsize;

  /* copy environment */

  if (OS_ict_emuvar(realct,EMU_debugcontextload))
    fprintf(stderr,
	    "Starting %d environment strings at " LSE_emu_addr_t_print_format "\n",
	    numenv, saddr);
  venvp = saddr;
  for (i=0;i<numenv;i++) {
    /* write envp value */
    stemp8 = LSE_h2e((LSE_emu_addr_t)saddr, Linux_bigendian);
    if (OS_mem_write(realct,sbase,CC_addrsize,stp8)) goto badloadmem;

    sbase += CC_addrsize;
    slen = strlen(envp[i])+1;
    if (OS_mem_write(realct,saddr,slen,envp[i])) goto badloadmem;
    saddr += slen;
  }
  stemp8 = 0;
  if (OS_mem_write(realct,sbase,CC_addrsize,stp8)) goto badloadmem;
  sbase += CC_addrsize;

  /* aux vector... A bunch of fun information that applications (really
   * libc) can rely upon.  Very important for DLLs
   */

  if (OS_ict_emuvar(realct,EMU_debugcontextload))
    fprintf(stderr,
	    "Starting aux vector (%d entries) at " LSE_emu_addr_t_print_format 
	    "\n", NUM_AUXV_ENTRIES, sbase);

  // NOTE: the order does not matter, except for the DL info

  CC_dlinfo();

  OS_auxv_entry(6,  OSDefs::oPAGE_SIZE); /* AT_PAGESZ */
  OS_auxv_entry(16, OSDefs::oHWCAP);     /* AT_HWCAP */
  OS_auxv_entry(17, OSDefs::oCLKTCK);    /* AT_CLKTCK */
  OS_auxv_entry(3,                        /* AT_PHDR */
		pct.baseaddr + phoff);
  OS_auxv_entry(4,  phentsize);           /* AT_PHENT */
  OS_auxv_entry(5,  phnum);               /* AT_PHNUM */
  OS_auxv_entry(7, pct.use_interp ? ict.baseaddr : 0);  /* AT_BASE */
  OS_auxv_entry(8, 0);                         /* AT_FLAGS */
  OS_auxv_entry(9, program_entry);             /* AT_ENTRY */
  OS_auxv_entry(0, 0);                         /* AT_NULL */

  if (OS_ict_emuvar(realct,EMU_debugcontextload)) {
    fprintf(stderr,
	    LSE_emu_addr_t_print_format " " 
	    LSE_emu_addr_t_print_format " " 
	    LSE_emu_addr_t_print_format "\n",
	    start_stack,sbase,saddr);
    fprintf(stderr,"Entry=" LSE_emu_addr_t_print_format "\n",
	    CC_ict_true_startaddr(realct));
  }

  /*
   * This hook should:
   * 1) set up anything else that the memory manager needs for the
   *    new address space.
   * 2) Set up any stuff that a context needs to properly start a new
   *    thread.
   * There also ought to be some way to say, "hey", this is a new address
   * space and we need to redo the memory manager to do right things for
   * the new ABI.  So we will put in a hook */
  CC_execveguts();

  // delete the old memory space
  osct->reportVforkDone.wakeup(1,true);
  // CC_mem_destroy(saveisamem);
  savemem->decr();

  // close  file descriptors on exec if so marked
  osct->fdtable->closeExec();

  return 0;

 badloadmem:
   std::cerr << "Memory error for binary file " << filename << "\n";

 badload:

  // delete the new memory space
  delete osct->mem;
  CC_ict_mem(realct) = saveisamem;
  osct->mem = savemem;
  osct->is64bit = saveis64bit;
  return ecode;
   
}

int Linux_context_load(CC_isacontext_t *realct, int argc, 
                        char **argv, char **envp) {
  /* This routine is essentially execve.  The fork does not
   * need to allocate the context structure, as that will be done in context
   * init
   */
  char *fname = resolve_path(argv[0], OS_ict_osinfo(realct));
  int rval = Linux_execveguts(realct,fname,argc,argv,envp);
  free(fname);
  if (rval) return rval;

  // schedule the context
  OS_ict_osinfo(realct)->state = ProcRunning; // so block works correctly
  block_thread(OS_ict_osinfo(realct), false);
  if (ready_thread(OS_ict_osinfo(realct), false))
    Linux_schedule(&CC_emu_interface(realct));

  return 0;
}

/********************** signal handling ***********************/

static void group_exit(Linux_context_t *osct, int ecode);

// declare this forward so we can use it; if an ISA forgets it,
// there will be a linker error.
static bool CC_do_a_signal(CC_isacontext_t *realct, bool must_restart,
			   LSE_emu_iaddr_t pc, LSE_emu_iaddr_t &npc);

static int send_signal_to(Linux_context_t *fct, Linux_context_t *tct,
    		          int sig, bool useTG, Linux_siginfo_t &nv) {

  uint64_t smask = uint64_t(1) << (sig - 1);

  // NOT SUPPORTED: handle stop and continue

  if (smask & OSDefs::oSIGDEF_STOP()) {
    tct->myerrno() = (OSDefs::oENOSYS);
    return -1;
  } else if (sig == OSDefs::oSIGCONT) {
    tct->myerrno() = (OSDefs::oENOSYS);
    return -1;
  }

  // if the signal is ignored, we are done.
  if (!(tct->sigblocked & smask) &&
      (tct->sighandtable->handlers[sig-1].handler == 1 ||
	 !tct->sighandtable->handlers[sig-1].handler &&
       (smask & OSDefs::oSIGDEF_IGNORE()))) 
     return 0;

  // if not an RT signal and already queued, do not queue it again

  if (sig < 32) {
    uint64_t *lp = useTG ? &tct->tgroup->sigpending : &tct->sigpending;
    if ((*lp) & (uint64_t(1) << (sig-1))) return 0;
  }

  if (useTG) {
    tct->tgroup->signals.push_back(nv);
    tct->tgroup->sigpending |= uint64_t(1)<<(sig-1);
  } else {
    tct->signals.push_back(nv);
    tct->sigpending |= uint64_t(1)<<(sig-1);
  }

  // Find someone in the thread group to take the signal.  Also, do not
  // bother zombies or stopped tasks other than stopped.
  // and wakeup if not blocked.
  if (useTG) {

    if (!(smask & tct->sigblocked) 
        && tct->state != ProcZombie && tct->state != ProcReaped
	&& (sig == 9 ||
	    (tct->state != ProcStopped &&
	     (tct->state == ProcRunning || !tct->sigPendingFlag)))) {

    } else if (tct->tgroup->members.size()==1) {
      return 0; // no one to deliver it to
    }
    else { // find a thread in the thread group.
      // Note: Linux actually round-robins, but it is probably not required.
      for (std::list<Linux_context_t *>::iterator 
            i = tct->tgroup->members.begin(),
            e = tct->tgroup->members.end(); i != e; ++i) {
        tct = *i;
	if (!(smask & tct->sigblocked) 
	    && tct->state != ProcZombie && tct->state != ProcReaped
	    && (sig == 9 ||
		(tct->state != ProcStopped &&
		 (tct->state == ProcRunning || !tct->sigPendingFlag)))) 
	  goto tfound;
      } 
      return 0;// no thread found!     
    }
tfound:

    // Interesting thing... the thread group is supposed to be wiped out
    // on fatal signals.  However, this wiping out also happens when
    // the signal is received, so doing it here is just a performance
    // optimization for the real kernel.  I won't bother doing so...
   ;
  }  else if (smask & tct->sigblocked) return(0);
  // now, tct has the *true* target

#ifdef DEBUG_SIGNAL
  std::cerr << "Sent " << sig << " to " << tct->tid 
	  << " blocked:" << std::hex << tct->sigblocked << std::dec
	  << "\n";
#endif
  tct->sigPendingFlag = true;
  ready_thread(tct, true);

  return 0;
}

int send_signal_to_pgrp(Linux_context_t *osct, int pgid, Linux_siginfo_t &nv) {
  int ret, retval;
  bool wasgood = false;

  // find the target process group
  Linux_pgroup_t *pgrp = osct->os->pgroups[pgid];
  if (!pgrp) {
    osct->os->pgroups.erase(pgid);
    osct->myerrno() = OSDefs::oENOSYS;
    return -1;
  }

  for (std::list<Linux_tgroup_t *>::iterator i = 
	 pgrp->members.begin(), ie = pgrp->members.end(); i != ie; ++i) {
    Linux_tgroup_t *tct = *i;
    ret = send_signal_to(osct, tct->members.front(), nv.LSEsi_signo, true, nv);
    wasgood |= !wasgood;
    retval = ret;
  }
  osct->myerrno() = retval;
  return wasgood ? 0 : -1;
}
 
static bool try_to_get_signal(uint64_t blocked,
       	    		      uint64_t &pending, 
       	   	              std::list<Linux_siginfo_t> &sl,
                              Linux_siginfo_t &si) {
  int signo;
  if (!(pending & ~blocked)) return false;
  
  for (signo = 0; !(pending & ~blocked & (uint64_t(1)<<signo)); signo++);

  uint64_t mask = (uint64_t(1)<<signo);
  signo++;

  int qsize = sl.size();
  for (std::list<Linux_siginfo_t>::iterator i = sl.begin(),
       ie = sl.end(); i != ie; ++i) { // now look in queue...

     if (i->LSEsi_signo == signo) {
       si = *i;

       // update pending flags
       pending &= ~mask;
       for (std::list<Linux_siginfo_t>::iterator 
            j = sl.erase(i); j != sl.end(); ++j)
	 if (j->LSEsi_signo == signo) pending |= mask;
         
       return 1;
     }
  }

  // wow, it was not in the queue.  How odd....  give some default values.
  si.LSEsi_signo = signo;
  si.LSEsi_pid = si.LSEsi_uid = 0;
  si.LSEsi_code = 0;
  si.LSEsi_errno = 0;

  pending &= ~mask;
  return 1;
}

static void pick_signal(Linux_context_t *osct, Linux_siginfo_t &si,
       	    		Linux_sighandinf_t &hand) {

  while (1) { // because we can ignore signals

    if (!try_to_get_signal(osct->sigblocked, osct->sigpending, 
                           osct->signals, si)
       && !try_to_get_signal(osct->sigblocked, osct->tgroup->sigpending,
                             osct->tgroup->signals, si)) {
      si.LSEsi_signo = 0;
      osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
  		             (osct->tgroup->sigpending & ~osct->sigblocked);
      return;
    }

    osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
  		           (osct->tgroup->sigpending & ~osct->sigblocked);

    Linux_sighandinf_t *hp = &osct->sighandtable->handlers[si.LSEsi_signo-1];
    if (hp->handler==1) continue; // ignore.... do nothing.
    if (hp->handler != 0) { // not default behavior
      hand = *hp;
      if (hp->flags & OSDefs::oSA_ONESHOT) hp->handler = 0;
      return;
    } 

    // default actions
    uint64_t mask = uint64_t(1) << (si.LSEsi_signo-1);

    if (OSDefs::oSIGDEF_IGNORE() & mask) continue;
    if (osct->tid == 1) continue; // init ignores all by default

    if (OSDefs::oSIGDEF_STOP() & mask) {
      // NOT IMPLEMENTED: stop signals
       continue;
    }
    // if we reached here, it is a fatal signal and all threads in the group
    // must exit.
    group_exit(osct, si.LSEsi_signo);
    si.LSEsi_signo = -1; // so we do not keep going.
    return;  // do not keep going after exiting!
  }
}


inline bool on_alt_stack(OSDefs::L_sigaltstack &os, OSDefs::L_uptr_t realsp) {
  return realsp - os.LSE_ss_sp < os.LSE_ss_size;
}

inline int calc_ss_flags(OSDefs::L_sigaltstack &os, OSDefs::L_uptr_t realsp) {
  return (os.LSE_ss_size == 0 ? OSDefs::oSS_DISABLE :
          on_alt_stack(os, realsp) ? OSDefs::oSS_ONSTACK : 0);
}

static int handle_sigaltstack(CC_isacontext_t *realct,
			      Linux_context_t *osct,
                              LSE_emu_addr_t newstack,
                              LSE_emu_addr_t oldstack, LSE_emu_addr_t realsp) {
  OSDefs::L_sigaltstack ns, os, ns2, os2;

  os = osct->sigAlt;
  os.LSE_ss_flags = calc_ss_flags(os, realsp);

  if (newstack) {
    if (OS_mem_read(realct,newstack,sizeof(ns),&ns)) return -OSDefs::oEFAULT;
    ns2 = ns.e2h();

    if (os.LSE_ss_flags == OSDefs::oSS_ONSTACK) return -OSDefs::oEPERM;

    if (ns2.LSE_ss_flags != OSDefs::oSS_DISABLE &&
        ns2.LSE_ss_flags != OSDefs::oSS_ONSTACK &&
	ns2.LSE_ss_flags != 0) 
      return -OSDefs::oEINVAL;

    if (ns2.LSE_ss_flags == OSDefs::oSS_DISABLE) {
      ns2.LSE_ss_size = 0;
      ns2.LSE_ss_sp = 0;
    } else {
      if (ns2.LSE_ss_size < OSDefs::oMINSIGSTKSZ) return -OSDefs::oENOMEM;
    }
    osct->sigAlt = ns2;
  }

  if (oldstack) {
    os2 = os.h2e();
    if (OS_mem_write(realct,oldstack,sizeof(os2),(unsigned char *)&os2))
      return -OSDefs::oEFAULT;
  }
  return 0;  
}

#ifdef LSE_LINUX_COMPAT
static int handle_compat_sigaltstack(CC_isacontext_t *realct,
				      Linux_context_t *osct,
				      LSE_emu_addr_t newstack,	  
				      LSE_emu_addr_t oldstack, 
				      LSE_emu_addr_t realsp) {
   OSDefs::L_compat_sigaltstack ns, os, ns2, os2;
  
  os.LSE_ss_sp = osct->sigAlt.LSE_ss_sp;
  os.LSE_ss_size = osct->sigAlt.LSE_ss_size;
  os.LSE_ss_flags = calc_ss_flags(osct->sigAlt, realsp);

  if (newstack) {
    if (OS_mem_read(realct,newstack,sizeof(ns),(unsigned char *)&ns))
      return -OSDefs::oEFAULT;
    ns2 = ns.e2h();

    if (os.LSE_ss_flags == OSDefs::oSS_ONSTACK) return -OSDefs::oEPERM;

    if (ns2.LSE_ss_flags != OSDefs::oSS_DISABLE &&
        ns2.LSE_ss_flags != OSDefs::oSS_ONSTACK &&
	ns2.LSE_ss_flags != 0) 
      return -OSDefs::oEINVAL;

    if (ns2.LSE_ss_flags == OSDefs::oSS_DISABLE) {
      ns2.LSE_ss_size = 0;
      ns2.LSE_ss_sp = 0;
    } else {
      if (ns2.LSE_ss_size < OSDefs::oMINSIGSTKSZ) return -OSDefs::oENOMEM;
    }
    osct->sigAlt.LSE_ss_sp = ns2.LSE_ss_sp;
    osct->sigAlt.LSE_ss_flags = ns2.LSE_ss_flags;
    osct->sigAlt.LSE_ss_size = ns2.LSE_ss_size;
  }

  if (oldstack) {
    os2 = os.h2e();
    if (OS_mem_write(realct,oldstack,sizeof(os2),(unsigned char *)&os2))
       return -OSDefs::oEFAULT;
  }
  return 0;  
}
#endif // LSE_LINUX_COMPAT

/********************** todo handling ***********************/

todo_timer *todo_timer::schedule(uint64_t tt, Linux_timer_t *t) {
  todo_timer *n = new todo_timer(t);
  static_cast<Linux_todo_t *>(n)->schedule(tt, t->os);
  return n;
}

void todo_timer::run(uint64_t currtime) {
  if (timer) timer->expired(currtime);
  delete this;
}

todo_timer::~todo_timer() {
  if (timer) {
    timer->todo = 0;
    timer = 0;
  }
}

/********************** timer handling ****************************/

void Linux_timer_t::settime(struct itval *newp, struct itval *oldp) {
  uint64_t currtime = CC_time_get_currtime(os);

  if (oldp) {
    *oldp = tval;
    if (oldp->expiration <= currtime)
      oldp->expiration = 0;
    else oldp->expiration -= currtime;
  }
  tval = *newp;

  if (todo) {
    todo->unschedule();
    todo->timer = 0;
  }
  todo = 0;

  // Whatever the clock, the new time is the same amount in the future.
  tval.expiration = currtime + tval.expiration;
  planfuture(tval.expiration);
}

void Linux_timer_t::planfuture(uint64_t attime) {
  todo = todo_timer::schedule(attime, this);
  //std::cerr << "Scheduling " << oid << " at " << attime 
  //	    << " now is " << CC_time_get_currtime(os)
  //	    << "\n";
}

void Linux_timer_t::remove_todo() {
  if (todo) {
    todo->unschedule();
    todo->timer = 0;
    todo = 0;
  }
}

Linux_timer_t::~Linux_timer_t() { 
  if (todo) {
    todo->unschedule();
    todo->timer = 0;
  }
}

void Linux_timer_t::expired(uint64_t currtime) {
  if (todo) todo->timer = 0;
  if (tval.expiration) 
    if (tval.expiration <= currtime) {
      todo = 0;
      fire(currtime);
      // TODO: reschedule
    } else {
      todo = 0;
      // TODO: reschedule
    }
  else todo = 0;
}

Linux_sig_timer_t::Linux_sig_timer_t(class Linux_dinst_t *o, int c, 
				     struct Linux_tgroup_t *tg,
				     int s, struct Linux_context_t *t) 

  : Linux_timer_t(o, obj_sigtimer, sigaction_kind_signal, c), signo(s), 
    target(t), tgroup(tg) {
  // NOTE: do not reference count target or tgroup because sig_timers can
  // only be pointed to from a tgroup.
}

void Linux_sig_timer_t::fire(uint64_t currtime) {
  Linux_siginfo_t nv;
  Linux_context_t *osct = target;

  //std::cerr << "Firing sig timer " << oid << " at " << currtime << "\n";
  
  nv.LSEsi_signo = signo ; // sigalarm
  nv.LSEsi_errno = 0;
  nv.LSEsi_code = OSDefs::oSI_TIMER;
  nv.LSEsi_pid = 0;
  nv.LSEsi_uid = 0;
  nv.LSEsi_utime = 0;
  nv.LSEsi_stime = 0;
  send_signal_to(target, target, signo, true, nv);
}

Linux_sig_timer_t::~Linux_sig_timer_t() {
}

Linux_wu_timer_t::Linux_wu_timer_t(class Linux_dinst_t *o, int c,
				   struct Linux_context_t *t) 
  : Linux_timer_t(o, obj_wutimer, sigaction_kind_signal, c), target(t) {
  //t->incr();
}

void Linux_wu_timer_t::fire(uint64_t currtime) {
  //std::cerr << "Firing wu timer " << oid << " at " << currtime << "\n";
  ready_thread(target, true);
}

Linux_wu_timer_t::~Linux_wu_timer_t() {
  //target->decr();
}

Linux_reschedule_timer_t::Linux_reschedule_timer_t(class Linux_dinst_t *o, 
						   int c)
  : Linux_timer_t(o, obj_reschedtimer, sigaction_kind_signal, c) {
}

void Linux_reschedule_timer_t::fire(uint64_t currtime) {
  //std::cerr << "Firing reschedule timer " << oid << " at " <<currtime << "\n";

  os->reschedule_timer = 0;
  if (os->running_list.size()) {
    Linux_context_t *osct = os->running_list.front();
    if (currtime - osct->cputime_curr_interval >= OSDefs::oTIMESLICE) {
      if (!CC_time_request_interrupt(os, CC_ict_hwcno(osct->isacont))) {
	if (OS_ict_emuvar(osct->isacont,EMU_trace_syscalls))
	  std::cerr << "SYSCALL(" << osct->tid << "): preempted at " <<
	    currtime << " on " << CC_ict_hwcno(osct->isacont) << "\n";
	block_thread(osct, false);
	ready_thread(osct, true);
      }
    } else {
      Linux_reschedule_timer_t *nt = new Linux_reschedule_timer_t(osct->os,1);
      osct->os->reschedule_timer = nt;
      Linux_timer_t::itval v;
      v.interval = 0;
      v.expiration = OSDefs::oTIMESLICE - currtime
	+ osct->cputime_curr_interval;
      nt->settime(&v, 0);
      this->decr();
      return;
    }
  }
  this->decr();
}

Linux_reschedule_timer_t::~Linux_reschedule_timer_t() {
}

/********************** completion handling ***********************/

void cont_futex::run() {
  CC_isacontext_t *realct = osct->isacont;
  bool deleteit = true;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume futex = ", osct->tid);

  if (!futexwaitrec) {  // futex is cleared
    OS_report_results(0, 0);
  } else if (timer && timer->tval.expiration <= 
	     CC_time_get_currtime(osct->os)) { // timeout
    OS_report_err(OSDefs::oETIMEDOUT);
  } else if (!timer) {
    OS_report_err(OSDefs::oERESTARTSYS);
  } else if (osct->restart == this) {
    // in a restart; queue and timer are still good
    osct->restart = 0;
    osct->continuationStack.push_back(this);
    block_thread(osct,true);
    if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
      fprintf(stderr, "blocked\n");
    return;
  } else {
    delete osct->restart;
    deleteit = false;
    osct->restart = this;
    OS_report_err(OSDefs::oERESTART_RESTARTBLOCK);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "%d\n", (int)OS_sys_get_return(1));

  if (deleteit) delete this;
  return;

 syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  if (deleteit) delete this;
  return;
}

cont_futex::~cont_futex() {
  if (osct->restart == this) osct->restart = 0;
  if (timer) {
    timer->remove_todo();
    timer->decr();
  }
  if (futexwaitrec) {
    // TODO: why is this updating paddr?  shouldn't it be the same?  why do
    // we even need uaddr?
    paddr = osct->mem->vmdevice.lookup(uaddr);
    std::deque<cont_futex *>::iterator place;
    place = std::find(futexwaitrec->waiters.begin(), 
		      futexwaitrec->waiters.end(),
		      this);
    if (place != futexwaitrec->waiters.end())
      futexwaitrec->waiters.erase(place);
    if (!futexwaitrec->waiters.size()) {
      osct->os->futexes.erase(paddr);
      futexwaitrec->decr();
    }
    futexwaitrec->decr();
    futexwaitrec = 0;  
  }
}

void cont_nsleep::run() {
  uint64_t currtime = CC_time_get_currtime(osct->os);
  CC_isacontext_t *realct = osct->isacont;
  bool deleteit = true;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume nanosleep = ", osct->tid);

  // if timer has expired, return 0
  if (!timer || timer->tval.expiration <= currtime) {
    OS_report_results(0, 0);
  }  else if (mode == 0) { // absolute 
    OS_report_err(OSDefs::oERESTARTNOHAND);
  } else { // relative timing
    if (rem) { // update timing
      uint64_t togo = timer->tval.expiration - currtime;
      OSDefs::L_timespec buf, tempbuf;
      buf.LSE_tv_sec = togo / 1000000000;
      buf.LSE_tv_nsec = togo / 1000000000;
      tempbuf = buf.h2e();
      OS_memcpy_h2t(rem, &tempbuf, sizeof(tempbuf));
    }
    if (osct->restart == this) { // timer is still good; sleep in restart
      osct->restart = 0;
      osct->continuationStack.push_back(this);
      block_thread(osct, true);
      if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
	fprintf(stderr, "blocked\n");
      return;
    } else {
      delete osct->restart;
      deleteit = false;
      osct->restart = this;
      OS_report_err(OSDefs::oERESTART_RESTARTBLOCK);
    }
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "%d\n", (int)OS_sys_get_return(1));

  if (deleteit) delete this;
  return;
 syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  if (deleteit) delete this;
  return;
}

cont_nsleep::~cont_nsleep() {
  if (timer) {
    timer->remove_todo();
    timer->decr();
  }
}

void cont_pause::run() {
  CC_isacontext_t *realct = osct->isacont;
  OS_report_err(OSDefs::oERESTARTNOHAND);
 syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume pause = -1 %d\n",
	    osct->tid, OSDefs::oERESTARTNOHAND);
  delete this;
  return;
}

void cont_piperead::run() {
  CC_isacontext_t *realct = osct->isacont;
  OS_report_err(OSDefs::oERESTARTSYS);
 syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume pipe read = -1 %d\n",
	    osct->tid, OSDefs::oERESTARTSYS);
  delete this;
  return;
}

cont_piperead::~cont_piperead() {
  if (pp) {
    pp->waiters.erase(std::find(pp->waiters.begin(),pp->waiters.end(),this));
    pp->decr(); 
    pp = 0;
  } 
}

static bool poll_piece(LSE_LINUX_PARMS,
		       Linux_context_t *osct,
		       int &osret,
		       LSE_emu_addr_t fds,
		       int nfds, int64_t timeout,
		       struct pollfd *pfd, 
		       int&ret, bool &mustfree,
		       cont_poll *&cp);

void cont_poll::run() {
  CC_isacontext_t *realct = osct->isacont;
  int osret = 0; // just a dummy
  int ret = 0;
  LSE_emu_iaddr_t curr_pc = CC_ict_startaddr(realct);
  int callno = 0; // not important
  bool mustfree = true;
  cont_poll *cp;
  int changelen; // dummy

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume poll = ", osct->tid);

  if (poll_piece(LSE_LINUX_ARGS, osct, osret, fds, nfds, timeout, pfd,
		 ret, mustfree, cp))
    goto syserr;

  pfd = 0; // the new cp has taken ownership of the pfd array
  if (mustfree) delete cp;

  if (osct->restart == this) osct->restart = 0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    if (ret == -2) fprintf(stderr,"blocked\n");
    else fprintf(stderr,"%d\n",ret);  

  delete this;
  return;
 syserr:
  if (ret == OSDefs::oEINTR) {
    ret = OSDefs::oERESTART_RESTARTBLOCK;
    mustfree = false;
    if (osct->restart != this) delete osct->restart;
    osct->restart = cp;
    OS_report_err(ret);
  }

  pfd = 0; // the new cp has taken ownership of the pfd array
  if (mustfree) delete cp;
  
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  delete this;
  return;
}

cont_poll::~cont_poll() {
  while (files.size())
    (*files.begin())->unregister_poll_waiter(this);
  if (pfd) delete[] pfd;
}

void cont_ssuspend::run() {
  CC_isacontext_t *realct = osct->isacont;
  osct->oldSigBlockedValid = true;
  OS_report_err(OSDefs::oERESTARTNOHAND);
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume signal suspend = -1 %d\n",
	    osct->tid, OSDefs::oERESTARTNOHAND);
 syserr:
  delete this;
  return;
}

template<class SIGINFO>
void cont_stimedwait<SIGINFO>::run() {
  CC_isacontext_t *realct = osct->isacont;
  int ret = 0;
  Linux_siginfo_t si;

  if (osct->sigPendingFlag &&
      (try_to_get_signal(thismask, osct->sigpending, 
			 osct->signals, si) ||
       try_to_get_signal(thismask, osct->tgroup->sigpending,
			 osct->tgroup->signals, si))) {
    
    osct->sigblocked = savemask;
    osct->sigPendingFlag = ( (osct->sigpending & ~osct->sigblocked) || 
			     (osct->tgroup->sigpending & ~osct->sigblocked) );
    
    ret = si.LSEsi_signo;
    if (infop) { 
      SIGINFO tempsibuf = SIGINFO(si);
      OS_memcpy_h2t(infop, &tempsibuf, sizeof(tempsibuf));
    }
    OS_report_results(0,ret);
  } else if (!timer || 
	     timer->tval.expiration <= CC_time_get_currtime(osct->os)) {
    osct->sigblocked = savemask;
    osct->sigPendingFlag = ( (osct->sigpending & ~osct->sigblocked) || 
			     (osct->tgroup->sigpending & ~osct->sigblocked) );
    OS_report_err(OSDefs::oEINTR);    
  } else {
    osct->sigblocked = savemask;
    osct->sigPendingFlag = ( (osct->sigpending & ~osct->sigblocked) || 
			     (osct->tgroup->sigpending & ~osct->sigblocked) );
    OS_report_err(OSDefs::oEAGAIN);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume signal timed suspend = %d\n",
	    osct->tid, ret);
  delete this;
  return;
 syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume signal timed suspend = -1 %d\n",
	    osct->tid, (int)OS_sys_get_return(1));
  delete this;
  return;
}

template<class SIGINFO>
cont_stimedwait<SIGINFO>::~cont_stimedwait() {
  if (timer) {
    timer->remove_todo();
    timer->decr();
  } 
}

static inline bool wait4_piece(LSE_LINUX_PARMS,
			       Linux_context_t *osct,
			       int& osret, int& pid, int& options,
			       LSE_emu_addr_t &stataddr,
			       LSE_emu_addr_t &infop,
			       LSE_emu_addr_t &ru, int&ret,
			       bool iscompat);

void cont_wait::run() {
  CC_isacontext_t *realct = osct->isacont;
  int osret = 0; // just a dummy
  int ret = 0;
  LSE_emu_iaddr_t curr_pc = CC_ict_startaddr(realct);
  int callno = 0; // not important

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): resume wait = ",osct->tid);

  // redo the waiting
  if (wait4_piece(LSE_LINUX_ARGS, osct, osret, pid, options, 
		  stataddr, infop, ru, ret, iscompat)) goto syserr;

  if (ret == -2) {
    osct->tgroup->childwait.add(osct);
    osct->continuationStack.push_back(this);
    block_thread(osct, true);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    if (ret == -2) fprintf(stderr,"blocked\n");
    else fprintf(stderr,"%d\n",ret);  

  if (ret != -2) delete this;
  return;
 syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  delete this;
  return;
}

cont_wait::~cont_wait() {
  osct->tgroup->childwait.remove(osct);
}

/********************** fault handling ***********************/

bool Linux_handle_fault(CC_isacontext_t *realct, OSDefs::CC_fault_t,
			LSE_emu_iaddr_t pc, LSE_emu_iaddr_t &following_pc) {

  Linux_context_t *osct = OS_ict_osinfo(realct);
  Linux_siginfo_t nv;
  nv.LSEsi_signo = 11; // segv
  nv.LSEsi_errno = 0;
  nv.LSEsi_code = OSDefs::oSI_KERNEL;
  nv.LSEsi_pid = osct->tgroup->tgid;
  nv.LSEsi_uid = osct->uid;
  send_signal_to(osct, osct, 11, true, nv);
  // need to actually handle the signal delivery as well, since
  // otherwise we just keep executing.  We are not in a system call
  // right now!
  CC_ict_startaddr(realct) = following_pc;
  if (CC_do_a_signal(realct, false, pc, following_pc)) {
    following_pc = CC_ict_startaddr(realct);
    return false;
  }
  else return true;
}

/****************** futex wakeup ************************/

static int wakeup_futex(Linux_context_t *osct,
                         LSE_emu_addr_t uaddr, int num, uint32_t bset) {
  LSE_emu_addr_t paddr = osct->mem->vmdevice.lookup(uaddr);
  Linux_futex_t *fut = osct->os->futexes[paddr];
  if (!fut) {
    osct->os->futexes.erase(paddr);
    return 0; // No one needed to wake up.
  }
  
  bool doreschedule = false;

  int cnt = 0;
  for (std::deque<cont_futex *>::iterator 
       i = fut->waiters.begin(), ie = fut->waiters.end(); i != ie;) {
    cont_futex *cf = (*i);
    Linux_context_t *wct = cf->osct;
    if (cf->futexBitset & bset) { // wake up
      cf->futexwaitrec->decr();
      cf->futexwaitrec = 0; 
      i = fut->waiters.erase(i);
      ie = fut->waiters.end(); // because waiters is a deque
      ready_thread(wct);
      cnt++;
      if (cnt >= num) break;
    } else ++i;
  }
  if (!fut->waiters.size()) {
    osct->os->futexes.erase(paddr);
    fut->decr();
  }
  return cnt;
}

static bool dying_futex(Linux_context_t *osct,
       	    	        LSE_emu_addr_t faddr, int pi) {
  int32_t fval = 0;

  if (OS_mem_read(osct->isacont, faddr, 4, &fval)) return true;
  fval = LSE_e2h(fval, Linux_bigendian);
  if ((fval & 0x3fffffff) != osct->tid) return false;
  // died
  int32_t nfval = LSE_h2e((fval & 0x80000000) | 0x40000000, Linux_bigendian);
  if (OS_mem_write(osct->isacont, faddr, 4, &nfval)) return true;
  if (!pi && (fval & 0x80000000)) wakeup_futex(osct, faddr, 1, ~0);
  return false;
}

/********************* thread exits ***************************/

static int notify_my_parent(Linux_context_t *osct, int sig);

static void reap_zombie(Linux_context_t *osct) {
  // if we are not the TG leader and the leader is a zombie, then it 
  // is up to us to notify the parent when the thread group is done.

  if (osct->state == ProcReaped) return; // already happened...

  Linux_context_t *leader = osct->tgroup->members.front();
  int exit_code = osct->exit_code >> 8;

  if (osct != leader && leader->state == ProcZombie && 
      osct->tgroup->members.size()==2) {  // 1 is us, 2 is the leader

    osct->incr();
    leader->incr();
    notify_my_parent(leader, leader->signalOnExit);
    if (osct->state == ProcReaped) osct->decr();
    else {
      osct->decr();
      osct->detach();
      if (CC_context_has_no_users(osct)) osct->decr();
      else osct->state = ProcReaped;
    }

    // and if leader does not notify, reap it.
    if (leader->signalOnExit == -1) reap_zombie(leader); 
    leader->decr();
  } else {
    osct->detach();
    if (CC_context_has_no_users(osct)) osct->decr();
    else osct->state = ProcReaped;
  }
  LSE_sim_exit_status = exit_code;

}

// Called when there are no more users of a context in the simulator.
void Linux_unregister_context(CC_isacontext_t *ctx) {
  if (OS_ict_osinfo(ctx)->state == ProcReaped) 
    delete OS_ict_osinfo(ctx);
}

static int notify_my_parent(Linux_context_t *osct, int sig) {
  int ret = sig; // returns the signal we did...

  if (osct->parent->tid == 1) { // init should reap it automagically.
    osct->signalOnExit = -1;
    return -1;
  }

  if (osct->tgroup->tgid != osct->tid) 
    std::cerr << "Help!  wrong task!\n";

  // if the parent does not care, must be reaped without a signal

  if (sig == OSDefs::oSIGCHLD &&
      (osct->parent->sighandtable->handlers[sig-1].handler == 1 ||
       (osct->parent->sighandtable->handlers[sig-1].flags & 
        OSDefs::oSA_NOCLDWAIT))) {
    osct->signalOnExit = -1; // indicate that we should reap it.
    ret = -1;
    if (osct->parent->sighandtable->handlers[sig-1].handler == 1) sig = -1;
  }

  Linux_siginfo_t nv;
  nv.LSEsi_signo = sig;
  nv.LSEsi_errno = 0;
  nv.LSEsi_code = (osct->exit_code & 0x7f) 
  		  ? OSDefs::oCLD_KILLED : OSDefs::oCLD_EXITED;
  nv.LSEsi_pid = osct->tid;
  nv.LSEsi_uid = osct->uid;
  nv.LSEsi_status = osct->exit_code & 0x7f;
  if (!nv.LSEsi_status) nv.LSEsi_status = osct->exit_code >> 8;
  nv.LSEsi_utime = 0;
  nv.LSEsi_stime = 0;

  osct->incr();
  if (sig > 0) send_signal_to(osct, osct->parent, sig, true, nv);

  // wake up everyone who was waiting in the parent's thread group.
  osct->parent->tgroup->childwait.wakeup(0);
  osct->decr();
  return ret;
}

// NOTE: this function is expected to call the scheduler in all cases.

static void perform_exit(Linux_context_t *osct, int ecode) {
  CC_isacontext_t *realct = (CC_isacontext_t *)(osct->isacont);

  osct->incr(); // so we do not delete the context in some sub-call...

  // Release the memory manager...
  // start by dealing with futexes.  Any faults are silently ignored.
  if (osct->robustFutexList) {

    LSE_emu_addr_t rl, nrl=0;
    OSDefs::L_robust_list_head rlhv, trlhv;
    int pi, npi=0, pendpi=0;

    if (OS_mem_read(realct, osct->robustFutexList,
		    sizeof(trlhv),(unsigned char *)&trlhv)) 
      goto robustdone;

    rlhv = trlhv.e2h();

    pi = rlhv.LSE_head_next & LSE_emu_addr_t(1);
    rl = rlhv.LSE_head_next & ~LSE_emu_addr_t(1);
    pendpi = rlhv.LSE_op_pending & LSE_emu_addr_t(1);
    rlhv.LSE_op_pending &= ~LSE_emu_addr_t(1);
    
    int limitcnt = 2048;
    while (rl != osct->robustFutexList) {

      // fetch the next entry.  get rid of bit 0 of the address first!
      OSDefs::L_robust_list thead, tthead;
      bool wasbad = OS_mem_read(realct, rl,
				sizeof(tthead), (unsigned char *)&tthead);
      thead.LSE_next = LSE_e2h(tthead.LSE_next, Linux_bigendian);

      npi = thead.LSE_next & LSE_emu_addr_t(1);
      nrl = thead.LSE_next & ~LSE_emu_addr_t(1);
      
      if (rlhv.LSE_op_pending != rl)
	if (dying_futex(osct, rl + rlhv.LSE_foffset, pi)) goto robustdone;

      if (wasbad) goto robustdone;

      if (!--limitcnt) break; // watch out for circular lists
      rl = nrl;
      pi = npi;
    } 

    if (rlhv.LSE_op_pending) { 
      dying_futex(osct, rlhv.LSE_op_pending + rlhv.LSE_foffset, pendpi);
    }
  } // robustFutexList

 robustdone:;

#ifdef LSE_LINUX_COMPAT
  if (osct->robustFutexListCompat) {
    LSE_emu_addr_t rl, nrl=0;
    OSDefs::L_compat_robust_list_head rlhv, trlhv;
    int pi, npi=0, pendpi=0;

    if(OS_mem_read(realct, osct->robustFutexListCompat,
		   sizeof(trlhv),(unsigned char *)&trlhv)) goto robustcdone;
    rlhv = trlhv.e2h();

    pi = rlhv.LSE_head_next & LSE_emu_addr_t(1);
    rl = rlhv.LSE_head_next & ~LSE_emu_addr_t(1);
    pendpi = rlhv.LSE_op_pending & LSE_emu_addr_t(1);
    rlhv.LSE_op_pending &= ~LSE_emu_addr_t(1);
    
    int limitcnt = 2048;
    while (rl != osct->robustFutexListCompat) {

      // fetch the next entry.  get rid of bit 0 of the address first!
      OSDefs::L_compat_robust_list thead, tthead;
      bool wasbad = OS_mem_read(realct, rl,
				sizeof(tthead), (unsigned char *)&tthead);
      thead = tthead.e2h();

      npi = thead.LSE_next & LSE_emu_addr_t(1);
      nrl = thead.LSE_next & ~LSE_emu_addr_t(1);
      
      if (rl != rlhv.LSE_op_pending)
	if (dying_futex(osct, rl + rlhv.LSE_foffset, pi)) goto robustcdone;

      if (wasbad) goto robustcdone;
      if (!--limitcnt) break;
      rl = nrl;
      pi = npi;
    } 

    if (rlhv.LSE_op_pending) { 
      dying_futex(osct, rlhv.LSE_op_pending + rlhv.LSE_foffset, pendpi);
    }
  } // robustFutexListCompat
 robustcdone:;
#endif // LSE_LINUX_COMPAT

  if (osct->clear_child_tid) {
    // TODO: only wake up if not dying from a signal
    if (osct->mem->refcount > 1) {
      int32_t zero = 0;
      OS_mem_write(realct, osct->clear_child_tid, 4, &zero);
      wakeup_futex(osct, osct->clear_child_tid, 1, ~0);
    }
    osct->clear_child_tid = 0;
  }

  if (osct->state == ProcRunning) {
    osct->os->running_list.remove(osct);
  }
  osct->exit_code = ecode;
  osct->state = ProcZombie;
  osct->tgroup->nLive--;
  osct->reportVforkDone.wakeup(1,true);

  bool lastingroup = !osct->tgroup->nLive;

  // Send any pending signals for this thread to all the other threads
  // in the group.  However, if the thread group is already exiting, don't
  // bother.

  if (!osct->tgroup->nLive ||
      osct->tgroup->exittask || osct->tgroup->exiting ||
      !osct->sigPendingFlag) {
  } else {

    for (std::list<Linux_context_t *>::iterator i = 
	   osct->tgroup->members.begin(), ie = osct->tgroup->members.end();
	 i != ie; ++i) {
      Linux_context_t *nct = *i;
      if (!nct->sigPendingFlag && nct->state != ProcZombie && 
	  nct->state != ProcReaped) {
	nct->sigPendingFlag = true;
	ready_thread(nct, false);
      }
    }
  }
  
  // Now, we need to find out who is going to take care of reaping our
  // children, since we didn't do it!  This can be anyone in our thread group.
  // If there isn't anyone in our thread group, init will have to do the
  // reaping.  (Meaning that we just reap right now, since init never runs in
  // the emulator.)  If any of the children have already
  // exited, we need to let the new reaper know that they have
  // finished.

  Linux_context_t *reaper = osct->os->threads[1];
  for (std::list<Linux_context_t *>::iterator 
	 i = osct->tgroup->members.begin(), ie = osct->tgroup->members.end();
       i != ie; ++i) {
    if ((*i)->state != ProcZombie && (*i)->state != ProcReaped) {
      reaper = *i;
      break;
    }
  }

  for (std::list<Linux_context_t *>::iterator i = osct->children.begin(),
       e = osct->children.end(); i != e; ++i) {

    reaper->children.push_back(*i);
    (*i)->parent = reaper;

    // no notifications or reaping when child is detached or same tgrp
    if ((*i)->signalOnExit == -1 || 
        (*i)->tgroup->tgid == reaper->tgroup->tgid) continue;

    (*i)->signalOnExit = OSDefs::oSIGCHLD;
    if (((*i)->state == ProcZombie || (*i)->state == ProcReaped)
	&& osct->tgroup->members.size()==1) {
      (*i)->incr(); // in case notify would kill it...
      notify_my_parent((*i), (*i)->signalOnExit);
      // really and truly dead!
      if ((*i)->signalOnExit == -1) reap_zombie(*i);
      (*i)->decr();
    }


  }
  osct->children.clear();

  // kill_orphaned_pgrp(child, osct)  ONLY needed when we do STOP/CONT/HUP

  // IGNORED: SIGHUP/SIGCONT when process groups are orphaned

  // send the exit signal to the parent if the thread group will be done
  int sigtouse = (osct->signalOnExit < 0 ? osct->signalOnExit :
		  osct->tgroup->members.size()==1 ? osct->signalOnExit :
		  -2);

  if (sigtouse >= 0) sigtouse = notify_my_parent(osct, sigtouse);

  // if there was someone waiting for the thread group to exit (basically,
  // this is an execve waiting for all threads in the group but one to
  // exit and so we need to tell it.)
  // NOTE: I will allow the last thread in the group to signal this.
  //
  // This code is probably vary wrong, because notify_my_parent may
  // actually result in scheduling in the parent, which then reaps.  If
  // this is the case, the tgroup is no longer valid and may have been
  // deleted.
  if (osct->tgroup &&
      osct->tid == osct->tgroup->tgid && osct->tgroup->exittask &&
      osct->tgroup->nCount < 0) { // parent is waiting for us!
    ready_thread(osct->tgroup->exittask, false);
  }

  CC_context_map_update(realct, CC_ict_hwcno(realct), 0);
  CC_ict_hwcno(realct) = 0;
  osct->os->wantScheduling = true;

  // And if no one will wait for us, reap the process
  if (sigtouse == -1) reap_zombie(osct);

  osct->decr(); // so we do not delete the context in some sub-call...
}

static void group_exit(Linux_context_t *osct, int ecode) {
  // If the group is already exiting, we just use the original code and
  // kill off this process.  Otherwise, we send kills to everyone else
  // in the *thread* group.  Oddly, the thread group could be different
  // from the signal group....
  
  if (osct->tgroup->exiting || osct->tgroup->exittask)
    ecode = osct->tgroup->exitcode;
  else if (osct->tgroup->members.size() > 1) {
    osct->tgroup->exitcode = ecode;
    osct->tgroup->exiting = true;
    osct->tgroup->exitcodeValid = true;

    for (std::list<Linux_context_t *>::iterator 
         i = osct->tgroup->members.begin(),
         ie = osct->tgroup->members.end(); i != ie; ++i) {
      Linux_context_t *tct = *i;

//fprintf(stderr,"Attempting to send kill to %d\n",tct->tid);
      if (tct == osct) continue; // do not kill yourself!
      if (tct->state == ProcZombie) continue; // do not talk to zombies!
      if (tct->state == ProcReaped) continue; // do not talk to zombies!
      tct->sigpending |= 1<<8; // SIGKILL
      tct->sigPendingFlag = true;
//fprintf(stderr,"Set sigpending for %d\n",tct->tid);
      ready_thread(tct, false);
    }
  }

  // No need to reschedule because perform_exit always does

  perform_exit(osct, ecode);
}

/****************** scheduling **************************/

static int count_hwcontexts(LSE_emu_interface_t *ifc) {
  int cnt=0;
  for (int cno=1; cno <= LSE_emu_hwcontexts_total ; ++cno) {
    if ((LSE_emu_hwcontexts_table)[cno].valid && 
	(LSE_emu_hwcontexts_table)[cno].emuinstid == ifc->emuinstid) cnt++;
  }
  return cnt;
}

static bool block_thread(Linux_context_t *osct, bool doschedule) {
  CC_isacontext_t *realct = osct->isacont;
  if (osct->state == ProcRunning) {
    osct->os->running_list.remove(osct);
    osct->state = ProcBlocked;
    CC_context_map_update(realct, CC_ict_hwcno(realct), 0);
    CC_ict_hwcno(realct) = 0;
    if (doschedule) osct->os->wantScheduling = true;
    osct->cputime_cumulative += (CC_time_get_currtime(osct->os) - 
				 osct->cputime_curr_interval);
    return true;
  }
  return false;
}

static bool ready_thread(Linux_context_t *osct, bool doschedule) {
  if (osct->state == ProcBlocked) {
    osct->state = ProcRunnable;
    osct->os->ready_list.push_back(osct);
    if (doschedule) osct->os->wantScheduling = true;
    return true;
  }
  return false;
}

void Linux_schedule(LSE_emu_interface_t *ifc) {
  int cno;
  uint64_t currtime = CC_time_get_currtime(OS_interface(ifc));

 tryagain:
  
  do { 
    OS_interface(ifc)->wantScheduling = false;
    // while there is a hardware context needing work and there is
    // a process ready to run, go for it.
    for (cno=1;cno<=(LSE_emu_hwcontexts_total)
	   && OS_interface(ifc)->ready_list.size();cno++) {

      if ((LSE_emu_hwcontexts_table)[cno].valid && 
	  (LSE_emu_hwcontexts_table)[cno].emuinstid == ifc->emuinstid &&
	  !(LSE_emu_hwcontexts_table)[cno].ctok) {
	
	Linux_context_t *osct=OS_interface(ifc)->ready_list.front();
	OS_interface(ifc)->ready_list.pop_front();
	if (osct->state != ProcRunnable) {
	  std::cerr << "Hey, trying to run something which is not waiting\n";
	  continue;
	}
	osct->state = ProcRunning;

	OS_interface(ifc)->running_list.push_back(osct);
	osct->cputime_curr_interval = currtime;

	if (OS_ict_emuvar(osct->isacont,EMU_trace_syscalls)) 
	  fprintf(stderr, "SYSCALL(%d): scheduling on %d\n", osct->tid, cno);

	if (osct->continuationStack.size()) {
	  Linux_continuation_t *cc = osct->continuationStack.back();
	  osct->continuationStack.pop_back();
	  cc->run(); // continuation will delete itself
	}
	
	if (osct->state != ProcRunning) { // in case continuation blocks/kills
	  cno--;
	  continue;
	}

	// NOW, we've mapped in the context, so we need to handle any signals
	// that might be there.
	LSE_emu_iaddr_t npc = CC_ict_startaddr(osct->isacont);
	osct->incr();
	CC_do_a_signal(osct->isacont, true /*unclear */, 
		       CC_ict_startaddr(osct->isacont), npc);
	
	if (osct->state == ProcRunning) { // was not killed or some such
	  // Now, let everyone know what has happened so they can pick up 
	  // the address
	  CC_ict_hwcno(osct->isacont) = cno;      
	  CC_context_map_update(osct->isacont, cno, osct->isacont);
	  osct->decr();

	  // and schedule next running thread
	  if (OSDefs::oTIMESLICE) {
	    if (osct->os->reschedule_timer) {
	      delete osct->os->reschedule_timer;
	      osct->os->reschedule_timer = 0;
	    }
	    Linux_reschedule_timer_t *nt 
	      = new Linux_reschedule_timer_t(osct->os,1);
	    osct->os->reschedule_timer = nt;
	    Linux_timer_t::itval v;
	    v.interval = 0;
	    v.expiration = OSDefs::oTIMESLICE - currtime 
	      + osct->os->running_list.front()->cputime_curr_interval;
	    nt->settime(&v, 0);
	  }

	} else {
	  osct->decr();
	  cno--;
	}
	//std::cerr << "Putting SW " << osct->tid << " on " << cno << "\n";
	// return; /* no more free contexts */
      } /* if */
    } /* for hardware */
  } while (OS_interface(ifc)->wantScheduling);

}

bool Linux_interrupt_cpu(LSE_emu_interface_t *ifc, int cno) {
  //std::cerr << "Waiting: " << OS_interface(ifc)->ready_list.size() << "\n";

  if ((LSE_emu_hwcontexts_table)[cno].valid && 
      (LSE_emu_hwcontexts_table)[cno].emuinstid == ifc->emuinstid &&
      (LSE_emu_hwcontexts_table)[cno].ctok) {

    CC_isacontext_t *oldisact 
      = (CC_isacontext_t *)(LSE_emu_hwcontexts_table)[cno].ctok;
    Linux_context_t *oldosct = OS_ict_osinfo(oldisact);
    if (OS_ict_emuvar(oldisact,EMU_trace_syscalls)) 
      fprintf(stderr, "SYSCALL(%d): preempted on %d\n", 
	      oldosct->tid, CC_ict_hwcno(oldisact));
    block_thread(oldosct, false);
    ready_thread(oldosct, false);
    Linux_schedule(ifc);
    return true;
  }
  return false;
}

/*****************************************************************
 ************************** Implementations **********************
 *****************************************************************
 PLEASE KEEP THESE IN ALPHABETICAL ORDER... EXCEPTIONS CAN BE MADE
 FOR VARIATIONS FOUND... such as stat vs. newstat or getrlimit vs.
 old_getrlimit .. IN THOSE CASES, KEEP RELATED FUNCTIONS TOGETHER
 COMPATS should go in a section by themselves at the end

 Coding standards:

 1) All syscalls must trace their arguments and results when
    EMU_trace_syscalls is true.
    a) Try to print out strings when possible, but do so only when
       you have guaranteed that the string is safe to print.  See
       open for a good example of dealing with this
    b) when an error is returned, print both the -1 and the return 1
       value (which becomes errno)

 2) Do not add a syscall which affects the host OS without adding
    checkpointing
    a) do not start the checkpoint until after the initial syscall
       trace message
    b) For structures, we checkpoint the pointer as an argument, but
       checkpoint the contents of the target structure *in host 
       address space* as an argument or result.

***********************************************************************/



static int unimplementedSyscall(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;  // 1 = do not handle signals because we blocked.
  int ret=0, parm;

  fprintf(stderr, "Unimplemented syscall %s (%d)\n", name, callno);

  Linux_siginfo_t nv;
  nv.LSEsi_signo = 9; // kill
  nv.LSEsi_errno = OSDefs::oENOSYS;
  nv.LSEsi_code = OSDefs::oSI_KERNEL;
  nv.LSEsi_pid = osct->tgroup->tgid;
  nv.LSEsi_uid = osct->uid;
  send_signal_to_pgrp(osct, osct->tgroup->pgroup->pgid, nv);

  OS_report_results(0,ret);

  OS_sys_return();
}

#define LSE_LINUX_UNIMPLEMENTED(s) static LSE_LINUX_SIG(s) { \
  return unimplementedSyscall(LSE_LINUX_ARGS,#s); \
  }

static LSE_LINUX_SIG(_sysctl) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t argptr;
  struct OSDefs::L___sysctl_args args;

  argptr = CC_arg_ptr(0,"p");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
	    "SYSCALL(%d): _sysctl(0x" LSE_emu_addr_t_print_format ") = ",
   	    osct->tid,argptr);

  OS_memcpy_t2h(&args, argptr, sizeof(args));
  /* Fix endianness */

  /* Just return error for now.  If we end up *really* needing this
     syscall we will have to be more sophisticated */
  OS_report_results(0,-1);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "%d\n",(int)OS_sys_get_return(0));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(sysctl) {
  return LSE_LINUX_NAME(_sysctl)(LSE_LINUX_ARGS);
}

LSE_LINUX_UNIMPLEMENTED(accept);

static LSE_LINUX_SIG(access) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;  // 1 = do not handle signals because we blocked.

  char *rfname, fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  LSE_emu_addr_t fname;
  int mode;
  int ret=0;
  int didfname = 0;

  fname = CC_arg_ptr(0,"pi");
  mode  = CC_arg_int(1,"pi");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): access(\"%s\",%i) = ",
	    osct->tid,fnamebuf,mode);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_int(mode);
  CC_chkpt_arg_str(fnamebuf,(strlen(fnamebuf)+1));

  rfname = resolve_path(fnamebuf, osct);
  CC_chkpt_guard(ret = access(rfname,mode); osct->myerrno() = (errno););
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): access(0x" LSE_emu_addr_t_print_format "%d) = ",
              osct->tid,fname, mode);
    } 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(acct);
LSE_LINUX_UNIMPLEMENTED(add_key);
LSE_LINUX_UNIMPLEMENTED(adjtimex);

static LSE_LINUX_SIG(alarm) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  unsigned int seconds;
  int ret=0;

  seconds = CC_arg_int(0,"ii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): alarm(%d) = ",osct->tid, seconds);

  // supposed to return seconds remaining until any previous alarm.
  // then cancels alarm, then 
  uint64_t currtime = CC_time_get_currtime(osct->os); // ns since epoch

  Linux_todo_t *todo = osct->tgroup->timers[0]->todo;
  if (todo && todo->intendedTime > currtime)
    ret = (todo->intendedTime - currtime) / 1000000000;
  else ret = 0;
  if (todo) todo->unschedule(osct->os);

  if (seconds) {
    Linux_timer_t::itval v;
    v.interval = 0;
    v.expiration = (uint64_t)seconds * 1000000000;
    osct->tgroup->timers[0]->settime(&v, 0);
  }    

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);

  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(aplib);
LSE_LINUX_UNIMPLEMENTED(bdflush);
LSE_LINUX_UNIMPLEMENTED(bind);

static LSE_LINUX_SIG(brk) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t newbrk, newbrka, brkaddr;
  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;

  newbrk = CC_arg_ptr(0,"p");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): brk(0x" LSE_emu_addr_t_print_format ") = ",
	osct->tid, newbrk);

  /* NOTE: Linux is a MORECORE_CLEARS=2 OS as far as glibc is concerned.  This
   * means that we have to clear memory between old and new when new > old
   */
  if (newbrk > osct->mem->heapstart) { 
    brkaddr = (osct->mem->heapbreak + amask) & ~amask;
    newbrka = (newbrk + amask) & ~amask;

    if (newbrka == brkaddr) osct->mem->heapbreak = newbrk;
    else if (newbrk <= osct->mem->heapbreak) { 
      if (!do_munmap(osct, newbrka, brkaddr - newbrka))
	osct->mem->heapbreak = newbrk;
    } else {
      /* need to check whether mmap has already allocated in between */
      
      Linux_memory_t::ranges_t::iterator hv;
      hv = osct->mem->vranges.upper_bound(brkaddr);

      if ((hv == osct->mem->vranges.end() ||
	   newbrka + OSDefs::oPAGE_SIZE <= hv->second.start) &&
	  !do_brk(osct, brkaddr, newbrka - brkaddr))
	osct->mem->heapbreak = newbrk;
    }
  }

  OS_report_results(0, osct->mem->heapbreak);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"0x" LSE_emu_addr_t_print_format "\n",
	    OS_sys_get_return(1));
  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(capget);
LSE_LINUX_UNIMPLEMENTED(capset);

static LSE_LINUX_SIG(chdir) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;
  LSE_emu_addr_t fname;
  char *rfname, fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  char dbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;

  fname = CC_arg_ptr(0,"p");
  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): chdir(\"%s\") = ",
            osct->tid, (char *)fnamebuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_str(fnamebuf,(strlen(fnamebuf)+1));

  rfname = resolve_path(fnamebuf, osct);
  getcwd(dbuf, sizeof(dbuf));
  CC_chkpt_guard(ret=chdir(fnamebuf); 
		 osct->myerrno() = (errno); 
		 chdir(dbuf););
  CC_chkpt_result_int(ret);

  if (ret != -1) {
    free(osct->fs->cwd);
    osct->fs->cwd = normalize_path(rfname);
    OS_report_results(0,ret);
  } else {
    free(rfname);
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }

  if(OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): chdir(0x" LSE_emu_addr_t_print_format ") = ",
              osct->tid, fname);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(chmod);
LSE_LINUX_UNIMPLEMENTED(chown);
LSE_LINUX_UNIMPLEMENTED(chown16);
LSE_LINUX_UNIMPLEMENTED(chroot);

template<class T> static inline int 
	  clock_getres_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int clockid;
  LSE_emu_addr_t targbuf;
  T tempbuf, buf;
  int ret=0;
  uint64_t ctime;

  clockid      = (int)CC_arg_int(0,"ip");
  targbuf = CC_arg_ptr(1,"ip");

  if (clockid < 0 || clockid > 1) // TODO: 4 POSIX clocks
    OS_report_err(OSDefs::oEINVAL);

  // I do not know what resolution to report here; mine is actually pretty
  // fine, so I could report 1 ns resolution for everything.
  
  buf.LSE_tv_sec = 0;
  buf.LSE_tv_nsec = 1;

  tempbuf = buf.h2e();
  if (targbuf)
    OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  else OS_report_err(OSDefs::oEFAULT);

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(clock_getres) {
  return clock_getres_body<OSDefs::L_timespec>(LSE_LINUX_ARGS,"clock_getres");
}

template<class T> static inline int 
	  clock_gettime_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int clockid;
  LSE_emu_addr_t targbuf;
  T tempbuf, buf;
  int ret=0;
  uint64_t ctime;

  clockid      = (int)CC_arg_int(0,"ip");
  targbuf = CC_arg_ptr(1,"ip");
  ctime = CC_time_get_currtime(osct->os);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
	    "SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, clockid, targbuf);

  if (clockid < 0 || clockid > 1) // 4 POSIX clocks
    OS_report_err(OSDefs::oEINVAL);

  // TODO: handle the clocks other than 0 and 1

  switch (clockid) {
  case 0: // REALTIME
    ctime += osct->os->boottime;
    break;
  case 1: break; // MONOTONIC does nothing
  case 2:
  case 3:
  default: break;
  }

  buf.LSE_tv_sec = ctime / 1000000000;
  buf.LSE_tv_nsec = ctime % 1000000000;

  tempbuf = buf.h2e();
  if (targbuf)
    OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  else OS_report_err(OSDefs::oEFAULT);

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(clock_gettime) {
  return clock_gettime_body<OSDefs::L_timespec>(LSE_LINUX_ARGS,"clock_gettime");
}

template<class T> static inline int 
	  clock_nanosleep_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t req, rem;
  int ret=0;
  uint64_t saved_return;
  T buf, tempbuf;
  uint64_t currtime;
  uint64_t oldblocked;
  int clockid, flags;
  
  clockid = CC_arg_int(0, "iipp");
  flags = CC_arg_int(1, "iipp");
  req = CC_arg_ptr(2,"iipp");
  rem = CC_arg_ptr(3,"iipp");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): %s(%d,%d,0x"
             LSE_emu_addr_t_print_format ",0x" LSE_emu_addr_t_print_format 
	    ") = ", osct->tid, name, clockid, flags, req, rem);

  if (clockid < 0 || clockid > 1) // can only sleep on real or monotonic
    OS_report_err(OSDefs::oEINVAL);

  if (!req) OS_report_err(OSDefs::oEFAULT);
  else {
    OS_memcpy_t2h(&tempbuf,req,sizeof(tempbuf)); /* will report error */
    buf = tempbuf.e2h();
  }
  
  // create a timer, a continuation pointing at the timer, and an event
  // on the timer.
  {
    Linux_wu_timer_t *nt = new Linux_wu_timer_t(osct->os,clockid,osct);
    osct->continuationStack.push_back(new cont_nsleep(osct, nt, flags&1, rem));
    block_thread(osct, true);
    Linux_timer_t::itval v;
    v.interval = 0;
    v.expiration = (uint64_t)buf.LSE_tv_sec * 1000000000 + buf.LSE_tv_nsec;
    nt->settime(&v, 0);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"blocked\n");

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
aftersyserr:
  OS_syserr_handler();
}

static LSE_LINUX_SIG(clock_nanosleep) {
  return clock_nanosleep_body<OSDefs::L_timespec>(LSE_LINUX_ARGS,
						  "clock_nanosleep");
}


template<class T> static inline int 
	  clock_settime_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int clockid;
  LSE_emu_addr_t targbuf;
  T tempbuf, buf;
  int ret=0;
  uint64_t ctime, ntime;

  clockid = (int)CC_arg_int(0,"ip");
  targbuf = CC_arg_ptr(1,"ip");
  ctime = CC_time_get_currtime(osct->os);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
	    "SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, clockid, targbuf);

  if (clockid != 0) // only the realtime clock can be set
    OS_report_err(OSDefs::oEINVAL);

  if (targbuf)
    OS_memcpy_t2h(&tempbuf, targbuf, sizeof(tempbuf)); /* will report error */
  else OS_report_err(OSDefs::oEFAULT);

  tempbuf = buf.e2h();

  ntime = tempbuf.LSE_tv_sec * 1000000000 + tempbuf.LSE_tv_nsec;

  osct->os->boottime = ntime - ctime;

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(clock_settime) {
  return clock_settime_body<OSDefs::L_timespec>(LSE_LINUX_ARGS,"clock_settime");
}


static int clone_common(LSE_LINUX_PARMS,
			Linux_context_t *osct,
			int& flags,
			LSE_emu_addr_t& stack,
			LSE_emu_addr_t& size,
			LSE_emu_addr_t& ptidp,
			LSE_emu_addr_t& ctidp,
			LSE_emu_addr_t& tlsp) {
  int osret = 0;
  CC_isacontext_t *newct;
  int rval;
  boolean automap;

  rval=Linux_cloneguts(&newct, realct, flags, stack-size, size, 
                       true, CC_ict_startaddr(realct), ptidp,ctidp,tlsp);

  if (rval) {
    OS_report_err(OSDefs::oENOMEM);
  } else {
    ready_thread(OS_ict_osinfo(newct), true);
    int tid = OS_ict_osinfo(newct)->tid;
    OS_report_results(0,tid);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(clone) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  /* parameters are flags, stackaddr, parent tidp, child tidp, tls */
  int flags            = CC_arg_int(0,"ipppp");
  LSE_emu_addr_t stack = CC_arg_ptr(1,"ipppp");
  LSE_emu_addr_t size  = OSDefs::oCLONE_STACK_SIZE;
  LSE_emu_addr_t ptidp = CC_arg_ptr(2,"ipppp");
  LSE_emu_addr_t tlsp  = CC_arg_ptr(3,"ipppp");
  LSE_emu_addr_t ctidp = CC_arg_ptr(4,"ipppp");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): clone(0x%x" 
            ",0x"LSE_emu_addr_t_print_format
            ",0x"LSE_emu_addr_t_print_format",0x"LSE_emu_addr_t_print_format 
            ",0x"LSE_emu_addr_t_print_format") = ",
	    osct->tid, flags, stack, ptidp,tlsp,ctidp);

  return clone_common(LSE_LINUX_ARGS, osct, flags, stack, size, ptidp,
		      ctidp, tlsp);
}

static LSE_LINUX_SIG(clone2) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  /* parameters are flags, stackaddr, stack size, parent tidp, 
     child tidp, tls */
  int flags            = CC_arg_int(0,"ippppp");
  LSE_emu_addr_t stack = CC_arg_ptr(1,"ippppp");
  LSE_emu_addr_t size  = CC_arg_int(2,"ippppp");
  LSE_emu_addr_t ptidp = CC_arg_ptr(3,"ippppp");
  LSE_emu_addr_t tlsp  = CC_arg_ptr(4,"ippppp");
  LSE_emu_addr_t ctidp = CC_arg_ptr(5,"ippppp");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): clone2(0x%x" 
            ",0x"LSE_emu_addr_t_print_format",0x"LSE_emu_addr_t_print_format 
            ",0x"LSE_emu_addr_t_print_format",0x"LSE_emu_addr_t_print_format 
            ",0x"LSE_emu_addr_t_print_format") = ",
	    osct->tid, flags, stack, size, ptidp,tlsp,ctidp);

  return clone_common(LSE_LINUX_ARGS, osct, flags, stack, size, ptidp,
		      ctidp, tlsp);
}

static LSE_LINUX_SIG(close) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd = CC_arg_int(0,"i");
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,"SYSCALL(%d): close(%d) = ", osct->tid, fd);

  ret = osct->fdtable->closeOne(fd);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

/*
	FIXME: connect is a stub implementation; it does not even
	       record its parameters properly
 */
static LSE_LINUX_SIG(connect) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd = CC_arg_int(0,"i");
  int ret= -1;
  int myerrno = OSDefs::oENOSYS;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,"SYSCALL(%d): connect(%d,...) = ", 
            osct->tid, fd);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(myerrno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(creat) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname;
  int flags,realflags;
  mode_t mode;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;
  Linux_native_file_t *fp;

  fname = CC_arg_ptr(0,"pi");
  flags = OSDefs::oO_CREAT | OSDefs::oO_WRONLY | OSDefs::oO_TRUNC;
  mode  = CC_arg_int(1,"pi");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): creat(\"%s\",%d) = ",
            osct->tid, (char *)fnamebuf,mode);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_int(mode);
  CC_chkpt_arg_str(fnamebuf,(strlen(fnamebuf)+1));

  /* 
     The following flags may vary between platforms
     Check them and remap them if necessary.

     The special flags start at 040000.
     For the time being, we will only support O_DIRECT, O_DIRECTORY, O_NOFOLLOW
     since these are the only ones that are consistently available.
  */

  realflags = flags & 3; /* should be good for all systems */

  if (flags & OSDefs::oO_CREAT) realflags |= O_CREAT;
  if (flags & OSDefs::oO_EXCL) realflags |= O_EXCL;
  if (flags & OSDefs::oO_NOCTTY) realflags |= O_NOCTTY;
  if (flags & OSDefs::oO_TRUNC) realflags |= O_TRUNC;
  if (flags & OSDefs::oO_APPEND) realflags |= O_APPEND;
  if (flags & OSDefs::oO_NONBLOCK) realflags |= O_NONBLOCK;
  if (flags & OSDefs::oO_NDELAY) realflags |= O_NDELAY;
  if (flags & OSDefs::oO_SYNC) realflags |= O_SYNC;
#ifdef O_LARGEFILE
  if (flags & OSDefs::oO_LARGEFILE) realflags |= O_LARGEFILE;
#endif

  /* These flags only exist if __USE_GNU is on */
  #ifdef __USE_GNU
  if (flags & OSDefs::oO_DIRECT) realflags |= O_DIRECT;
  if (flags & OSDefs::oO_DIRECTORY) realflags |= O_DIRECTORY;
  #endif
#ifdef O_NOFOLLOW
  if (flags & OSDefs::oO_NOFOLLOW) realflags |= O_NOFOLLOW;  
#endif

  rfname = resolve_path(fnamebuf, osct);
  if (CC_open_filter(rfname)) ret = -1;
  else {
    CC_chkpt_guard(ret=open(rfname,realflags,
			    mode & ~osct->fs->umask);
                    osct->myerrno() = errno;)
  }
  free(rfname);

  if (ret != -1) {
    // TODO: at some point, try looking for O_CLOEXEC - version 2.6.23
    Linux_native_file_t *fp;
    if (CC_chkpt_guard_true) 
      ret = osct->fdtable->addToTable(fp = new 
				      Linux_native_file_t(osct->os,ret));
    else 
      ret = osct->fdtable->addToTable(fp = 
				      new Linux_native_file_t(osct->os, -1));

    CC_chkpt_result_int(ret);
    CC_chkpt_result_int(fp->statbuf.st_dev);
    CC_chkpt_result_int(fp->statbuf.st_ino);
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(ret);
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): creat(0x" LSE_emu_addr_t_print_format ",%d) = ",
              osct->tid, fname, mode);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(create_module);
LSE_LINUX_UNIMPLEMENTED(delete_module);

static LSE_LINUX_SIG(dup) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd = CC_arg_int(0,"i");
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,"SYSCALL(%d): dup(%d) = ", osct->tid, fd);

  Linux_file_t *fp;
  if (!(fp = osct->fdtable->getFD(fd))) {
    ret = -1;
    osct->myerrno() = (OSDefs::oEBADFD);
  } else {
    ret = osct->fdtable->addToTable(fp, 0, 0);
  }

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(dup2) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd  = CC_arg_int(0,"ii");
  int nfd = CC_arg_int(1,"ii");
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,"SYSCALL(%d): dup2(%d,%d) = ", osct->tid, 
    	    fd, nfd);

  Linux_file_t *fp;
  Linux_fd_t *fdp;

  if (!(fp = osct->fdtable->getFD(fd))) {
      ret = -1;
      osct->myerrno() = OSDefs::oEBADFD;
  } else {
      fdp = osct->fdtable->getDesc(nfd);
      ret = nfd;
      if (fdp->fp) { // close old one
        fdp->fp->close();
        fdp->fp = 0;
      }
      fdp->flags = 0;
      fdp->fp = fp;
      fp->incr();
  }

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(dup3);
LSE_LINUX_UNIMPLEMENTED(epoll_create);
LSE_LINUX_UNIMPLEMENTED(epoll_create1);
LSE_LINUX_UNIMPLEMENTED(epoll_ctl);
LSE_LINUX_UNIMPLEMENTED(epoll_pwait);
LSE_LINUX_UNIMPLEMENTED(epoll_wait);
LSE_LINUX_UNIMPLEMENTED(eventfd2);
LSE_LINUX_UNIMPLEMENTED(eventfd);
LSE_LINUX_UNIMPLEMENTED(execv);

static LSE_LINUX_SIG(execve) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname, argvp, envp, tp;
  LSE_emu_addr_t stemp8, saddr;
  char fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  char *stp8, **nargv, **nenv;
  int rval = 0, argc, envc;
  bool is64bit = osct->is64bit; 

  fname = CC_arg_ptr(0, "ppp");
  argvp = CC_arg_ptr(1, "ppp");
  envp  = CC_arg_ptr(2, "ppp");

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_ptr(argvp);
  CC_chkpt_arg_ptr(envp);

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;
  CC_chkpt_arg_str( fnamebuf, (strlen(fnamebuf)+1));
  
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): execve(\"%s\" . .) = ", 
            osct->tid, fnamebuf);

  stemp8 = 0;
  stp8 = ((char*)&stemp8 + 
	  ((CC_addrsize != sizeof(stemp8) && end_switched()) ? 4 : 0));

  nargv = 0;
  for (tp = argvp, argc=0; argvp; tp += CC_addrsize) {
    if (OS_mem_read(realct, tp, CC_addrsize, stp8)) 
      OS_report_err(OSDefs::oEFAULT);
    saddr = LSE_e2h(stemp8, Linux_bigendian);
    if (!saddr) break;
    argc++;
    nargv = (char **)realloc(nargv, argc * sizeof(char *));
    nargv[argc-1] = (char *)malloc(4096);
    OS_strncpy_t2h(nargv[argc-1],saddr,4096);
  }

  nenv = 0;
  for (tp = envp, envc=0; envp; tp += CC_addrsize) {
    if (OS_mem_read(realct, tp, CC_addrsize, stp8))
      OS_report_err(OSDefs::oEFAULT);
    saddr = LSE_e2h(stemp8, Linux_bigendian);
    envc++;
    nenv = (char **)realloc(nenv, envc * sizeof(char *));
    if (!saddr) {
      nenv[envc-1] = 0;
      break;
    }
    nenv[envc-1] = (char *)malloc(4096);
    OS_strncpy_t2h(nenv[envc-1],saddr,4096);
  }

  rval = Linux_execveguts(realct, fnamebuf, argc, nargv, nenv);

  for (int i = 0; i < argc; ++i) free(nargv[i]);
  free(nargv);
  for (int i = 0; i < envc; ++i) free(nenv[i]);
  free(nenv);

  if (rval) {
    OS_report_err(-rval);
  } else {
    OS_report_results(0,0);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  CC_chkpt_finish();
  OS_syserr_handler()
}

static LSE_LINUX_SIG(exit) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0, parm;

  parm = CC_arg_int(0,"i");
  ret = parm & 0xff;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): exit(%d) = ", osct->tid, parm);

  CC_ict_startaddr(realct) = curr_pc; /* in case of sim error */

  perform_exit(osct, ret << 8);

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
}

static LSE_LINUX_SIG(exit_group) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0, parm;

  parm = sextend32(changelen, CC_arg_int(0,"i"));

  bool tracing = OS_ict_emuvar(realct, EMU_trace_syscalls);

  if (tracing)
    fprintf(stderr,"SYSCALL(%d): exit_group(%d) = ", 
            osct->tid, parm);

  group_exit(osct, (parm & 0xff) << 8);

  OS_report_results(0,ret);

  if (tracing)
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(faccessat);
LSE_LINUX_UNIMPLEMENTED(fadvise64);
LSE_LINUX_UNIMPLEMENTED(fadvise64_64);
LSE_LINUX_UNIMPLEMENTED(fallocate);
LSE_LINUX_UNIMPLEMENTED(fchdir);

static LSE_LINUX_SIG(fchmod) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  int mode;
  int ret=0;

  fd    = CC_arg_int(0,"ii");
  mode  = CC_arg_int(1,"ii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): fchmod(%i,%i) = ",
	    osct->tid, fd,mode);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_int(mode);

  ret = osct->fdtable->getGoodFD(fd)->fchmod(mode);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(fchmodat)
LSE_LINUX_UNIMPLEMENTED(fchown16)

static LSE_LINUX_SIG(fchown) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  uid_t owner;
  gid_t group;
  int ret=0;

  fd     = CC_arg_int(0,"iii");
  owner  = sextend(changelen,CC_arg_int(1,"iii"));
  group  = sextend(changelen,CC_arg_int(2,"iii"));

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): fchown(%i,%i,%i) = ",
	    osct->tid, fd,owner,group);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_int(owner);
  CC_chkpt_arg_int(group);

  ret = osct->fdtable->getGoodFD(fd)->fchown(owner,group);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(fchownat)

// I do not know what to do with 32 vs. 64-bit versions of this
static inline int fcntl_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  int cmd;
  OSDefs::L_long arg;
  int ret=0;
  fd  = (int)CC_arg_int(0,"iii");
  cmd = (int)CC_arg_int(1,"iii");
  arg = CC_arg_int(2,"iii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(%d,%d,%ld) = ", 
            osct->tid,name,fd,cmd,arg);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_int(cmd);
  CC_chkpt_arg_int(arg);

  switch (cmd) {
    case F_DUPFD :
        Linux_file_t *fp;
        if (arg < 0) {
          ret = -1;
          osct->myerrno() = (OSDefs::oEINVAL);
        } else if (!(fp = osct->fdtable->getFD(fd))) {
          ret = -1;
          osct->myerrno() = (OSDefs::oEBADFD);
        } else {
          ret = osct->fdtable->addToTable(fp, 0, arg);
        }
      CC_chkpt_result_int(ret);
      break;
    case F_GETFD :
       ret = osct->fdtable->getFlags(fd);
       CC_chkpt_result_int(ret);
       break;
    case F_SETFD :
       osct->fdtable->getDesc(fd)->flags = arg;
       ret = 0;
       CC_chkpt_result_int(ret);
       break;
    case F_GETFL :
    case F_SETFL :
	ret = osct->fdtable->getGoodFD(fd)->fcntl(cmd, arg);
	CC_chkpt_result_int(ret);
	break;
    /* NOTE: F_GETOWN/F_SETOWN/F_GETSIG/F_SETSIG are Linux-specific and may
     * not exist on the host sytem.  They are not implemented so far...
     */
    default:
      if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
        fprintf(stderr,"unimplemented cmd\n");
      CC_chkpt_finish();
      return -1;
  }

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  CC_chkpt_finish();
  OS_syserr_handler()  
}

static LSE_LINUX_SIG(fcntl) {
  return fcntl_body(LSE_LINUX_ARGS,"fcntl");
}
static LSE_LINUX_SIG(fcntl64) {
  return fcntl_body(LSE_LINUX_ARGS,"fcntl64");
}

LSE_LINUX_UNIMPLEMENTED(fdatasync)
LSE_LINUX_UNIMPLEMENTED(fgetxattr)
LSE_LINUX_UNIMPLEMENTED(flistxattr)
LSE_LINUX_UNIMPLEMENTED(flock)

static LSE_LINUX_SIG(fork) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  CC_isacontext_t *newct;
  int rval = 0;
  bool automap;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): fork() = ",osct->tid);

  rval=Linux_cloneguts(&newct,realct,OSDefs::oSIGCHLD,0,0,true,
		       CC_ict_startaddr(realct), 0,0,0);

  if (rval) {
    OS_report_err(ENOMEM);
  } else {
    ready_thread(OS_ict_osinfo(newct), true); 
    OS_report_results(0,(OS_ict_osinfo(newct))->tid);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(fremovexattr)
LSE_LINUX_UNIMPLEMENTED(fsetxattr)

// 32-bit fstat craziness
template<class T> static inline int 
	  fstat_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  LSE_emu_addr_t targbuf;
  struct stat buf;
  T tempbuf;
  int ret=0;

  fd      = (int)CC_arg_int(0,"ip");
  targbuf = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
	    "SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, fd, targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);

  // no side effect for pipes, so we can just guard it.
  CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->fstat(&buf);)
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }

  /* 
   * note that these values will depend upon the HOST system
   */
  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  CC_chkpt_finish();
  OS_syserr_handler()
}
// LSE_LINUX_UNIMPLEMENTED(fstat)

static LSE_LINUX_SIG(fstat64) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  LSE_emu_addr_t targbuf;
  LSE_stat64 buf;
  struct OSDefs::L_stat64 tempbuf;
  int ret=0;

  fd      = (int)CC_arg_int(0,"ip");
  targbuf = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
	    "SYSCALL(%d): fstat64(%d,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, fd,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);

#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->fstat64(&buf);)
#else
  CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->fstat(&buf);)
#endif
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    OS_report_err(osct->myerrno());
  }
  /* 
   * note that these values will depend upon the HOST system
   */

  CC_chkpt_guard(tempbuf = buf;);
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  CC_chkpt_finish();
  OS_syserr_handler()  
}

LSE_LINUX_UNIMPLEMENTED(fstatat)
LSE_LINUX_UNIMPLEMENTED(fstatfs)
LSE_LINUX_UNIMPLEMENTED(fstatfs64)
LSE_LINUX_UNIMPLEMENTED(fsync)

static LSE_LINUX_SIG(ftruncate) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  off_t offset;
  int ret=0;

  fd     = CC_arg_int(0,"ii");
  offset = CC_arg_int(1,"ii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): ftruncate(%d,%d) = ",
	    osct->tid, fd, offset);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_int(offset);

  ret = osct->fdtable->getGoodFD(fd)->ftruncate(offset);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler()
}

static int LSE_LINUX_NAME(ftruncate64)(LSE_LINUX_PARMS, bool bigendian=true) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  uint64_t offset1, offset2; // so we won't sign extend
  int64_t offset;
  int ret=0;

  fd     = CC_arg_int(0,"iii");
  offset1 = CC_arg_int(1,"iii");
  offset2 = CC_arg_int(2,"iii");

  if (bigendian)
    offset = int64_t((offset1 << 32) | offset2);
  else
    offset = int64_t((offset2 << 32) | offset1);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): ftruncate64(%d,%lld) = ",
	    osct->tid, fd, offset);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_int(offset);

  ret = osct->fdtable->getGoodFD(fd)->ftruncate(offset);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler()
}

// NOTE: the futex timeout will not work; it will just block.
// Futex waiting:
//   Linux code blocks.  When it wakes up, it sets up the RESTART stuff: this
//   is either ERESTARTSYS (for non-timed) or ERESTART_RESTARTBLOCK
//   (for timed).  Interestingly, the code knows why it is waking up
//   by whether the wakeup is already unqueued.
template<class T> 
static inline int futex_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret = 0;
  LSE_emu_addr_t uaddr, utime, uaddr2, paddr;
  int op, val, val3;
  T buf, tempbuf;

  // TODO: compat_futex ought to cast to compat_uptr_t
  uaddr  = CC_arg_ptr(0,"piippi");
  op     = CC_arg_int(1,"piippi");
  val    = CC_arg_int(2,"piippi");
  utime  = CC_arg_ptr(3,"piippi");
  uaddr2 = CC_arg_ptr(4,"piippi");
  val3   = CC_arg_int(5,"piippi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): %s("LSE_emu_addr_t_print_format",%d,%d,"
	    LSE_emu_addr_t_print_format","LSE_emu_addr_t_print_format
	    ",%d) = ",
            osct->tid, name, uaddr, op, val, utime, uaddr2, val3);

  // NOTE: utime needs to be copied from the user, but it is not going to
  //  be used, so I do not care about it right now!

  if (utime) {
    OS_memcpy_t2h(&tempbuf,utime,sizeof(tempbuf)); /* will report error */
    buf = tempbuf.e2h();
  }

  int32_t val4;

  switch (op & 0x7f) {
    case OSDefs::oFUTEX_WAIT: 
      val3 = ~0;
    case OSDefs::oFUTEX_WAIT_BITSET:
      { 
	// read memory location.  If val != value in memory, return EWOULDBLOCK
	if (OS_mem_read(realct, uaddr, 4, &val4)) 
	   OS_report_err(OSDefs::oEFAULT);
	val4 = LSE_e2h(val4, Linux_bigendian);
	if (val4 != val) {
#ifdef LSE_LINUX_FUTEX_NEEDS_TO_ALLOW_PREEMPTION
	  // This logic deals with an ugliness in SPARC32's code which
	  // can have two threads timed in such a way as to cause the value
	  // to never match.  We just allow a preemption if there is anything
	  // there.
	  if (osct->os->ready_list.size()) {
	    block_thread(osct, true);
	    ready_thread(osct, true);
	  }
#endif
	  OS_report_err(OSDefs::oEWOULDBLOCK);
	}


	Linux_wu_timer_t *nt = 0;
	if (utime) {
	  Linux_timer_t::itval v;
	  v.interval = 0;
	  // FUTEX_WAIT: relative time to wait for
	  // everything else: absolute time to wait for
	  v.expiration = (uint64_t)buf.LSE_tv_sec * 1000000000 
	    + buf.LSE_tv_nsec;
	  if ( (op & 0x7f) != OSDefs::oFUTEX_WAIT ) {
	    if (v.expiration <= CC_time_get_currtime(osct->os))
	      OS_report_err(OSDefs::oETIMEDOUT);
	    v.expiration -= CC_time_get_currtime(osct->os);
	  }

	  nt = new Linux_wu_timer_t(osct->os, 0, osct);
	  nt->settime(&v, 0);
	}

	// search the memory manager for this futex.  Then add to the wait queue
	paddr = osct->mem->vmdevice.lookup(uaddr);
	Linux_futex_t *fut = osct->os->futexes[paddr];
	if (!fut) 
	  osct->os->futexes[paddr] = fut = new Linux_futex_t(osct->os);

	cont_futex *cf = new cont_futex(osct, uaddr, paddr, val3, fut, nt);

	fut->waiters.push_back(cf);
	fut->incr();  // 1 + N waiters references

	osct->continuationStack.push_back(cf);

	block_thread(osct, true);

	//fprintf(stderr,"Waiting on futex\n");
	if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
	  fprintf(stderr,"blocked\n");
	goto noreport;
      } 
      break;
    case OSDefs::oFUTEX_WAKE:
      val3 = ~0; 
      // intentional fall-through.
    case OSDefs::oFUTEX_WAKE_BITSET:
      if (!val3) OS_report_err(OSDefs::oEINVAL);
      else if ((ret = wakeup_futex(osct, uaddr, val, val3)) < 0)
        OS_report_err(-ret);
      break;
    default:
      OS_report_err(OSDefs::oENOSYS);
  }

  OS_report_results(0,ret);
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);
noreport:

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(futex) {
  return futex_body<OSDefs::L_timespec>(LSE_LINUX_ARGS,"futex");
}

LSE_LINUX_UNIMPLEMENTED(futimesat)
LSE_LINUX_UNIMPLEMENTED(getcontext)
LSE_LINUX_UNIMPLEMENTED(getcpu)

// Note that the getcwd C library function in linux can malloc some
// additional space....  but the OS call will not.  This is the OS
// call, so the return value is going to be the buffer that was
// passed to it unless there was an error, in which case it will be -1
static LSE_LINUX_SIG(getcwd) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  char *ret;
  LSE_emu_addr_t fname;
  char *tempspace=0;
  uint64_t size;
  boolean goodresult=false;
  char mybuf[1024];
  DIR *od;

  fname = CC_arg_ptr(0,"pi");
  size  = CC_arg_int(1,"pi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): getcwd(" LSE_emu_addr_t_print_format ",%0lld) = ",
            osct->tid, fname,size);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_int(size);

  if (strlen(osct->fs->cwd)+1 > size) {
    OS_report_err(OSDefs::oERANGE);
  }

  // In reality, we ought to check whether the path is still searchable.
  CC_chkpt_guard(
    od=opendir(osct->fs->cwd);
    osct->myerrno() = errno;
    goodresult = od != 0;
    if (od) closedir(od);
  )
  CC_chkpt_result_int(goodresult);

  if (!goodresult) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }

  OS_memcpy_h2t(fname,osct->fs->cwd,strlen(osct->fs->cwd)+1);

  OS_report_results(0,strlen(osct->fs->cwd));

  if(OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, 
	    "0x" LSE_emu_addr_t_print_format "\n", (uint64_t)fname);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
      fprintf(stderr,
              "SYSCALL(%d): getcwd(0x" LSE_emu_addr_t_print_format ",%d) = ",
              osct->tid, fname,(int)size);
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(getdents)
LSE_LINUX_UNIMPLEMENTED(getdents64)
LSE_LINUX_UNIMPLEMENTED(getdomainname)

static LSE_LINUX_SIG(getegid) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): getegid() = ", osct->tid);

  ret = osct->egid;

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(getegid16)

static LSE_LINUX_SIG(geteuid) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): geteuid() = ", osct->tid);

  ret = osct->euid;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(geteuid16)

static LSE_LINUX_SIG(getgid) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): getgid() = ", osct->tid);

  ret = osct->gid;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(getgid16)
LSE_LINUX_UNIMPLEMENTED(getgroups16)
LSE_LINUX_UNIMPLEMENTED(getgroups)
LSE_LINUX_UNIMPLEMENTED(getitimer)
LSE_LINUX_UNIMPLEMENTED(get_kernel_syms)
LSE_LINUX_UNIMPLEMENTED(get_mempolicy)

static LSE_LINUX_SIG(getpagesize) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): getpagesize() = ", osct->tid);

  ret = OSDefs::oPAGE_SIZE;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(getpeername)
LSE_LINUX_UNIMPLEMENTED(getpgid)
LSE_LINUX_UNIMPLEMENTED(getpgrp)

static LSE_LINUX_SIG(getpid) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): getpid() = ", osct->tid);

  ret = osct->tgroup->tgid;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
}

static LSE_LINUX_SIG(getppid) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): getppid() = ", osct->tid);

  ret = osct->parent->tgroup->tgid;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(getpriority)
LSE_LINUX_UNIMPLEMENTED(getresgid)
LSE_LINUX_UNIMPLEMENTED(getresuid)

template<class T> static inline int 
	  getrlimit_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int resource;
  LSE_emu_addr_t targrlim;
  struct rlimit rlim;
  T temprlim;
  int ret=0;

  resource = CC_arg_int(0,"ip");
  targrlim = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format 
            ") = ",
	    osct->tid, name, resource, targrlim);

  if (resource < 0 || resource >= OSDefs::oRLIMIT_NLIMITS)
    OS_report_err(OSDefs::oEINVAL);

  temprlim.LSE_rlim_cur = LSE_h2e((typeof temprlim.LSE_rlim_cur)
				  (osct->tgroup->rlimits[resource][0]),
				  Linux_bigendian);
  temprlim.LSE_rlim_max = LSE_h2e((typeof temprlim.LSE_rlim_max)
				  (osct->tgroup->rlimits[resource][1]),
				  Linux_bigendian);

  OS_memcpy_h2t(targrlim,&temprlim,sizeof(temprlim)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(getrlimit) {
  return getrlimit_body<OSDefs::L_rlimit>(LSE_LINUX_ARGS,"getrlimit");
}

LSE_LINUX_UNIMPLEMENTED(get_robust_list)


template<class R> static inline int 
	  getrusage_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int resource;
  LSE_emu_addr_t targrusage;
  R temprusage;
  int ret=0;

  resource   = CC_arg_int(0,"ip");
  targrusage = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format 
            ") = ",
	    osct->tid, name, resource, targrusage);

  memset(&temprusage,0,sizeof(temprusage));

  /* will report error */
  OS_memcpy_h2t(targrusage,&temprusage,sizeof(temprusage));
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()  
}

static LSE_LINUX_SIG(getrusage) {
  return getrusage_body<OSDefs::L_rusage>(LSE_LINUX_ARGS,"getrusage");
}

LSE_LINUX_UNIMPLEMENTED(getsid)
LSE_LINUX_UNIMPLEMENTED(getsockname)
LSE_LINUX_UNIMPLEMENTED(getsockopt)

static LSE_LINUX_SIG(gettid) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): gettid() = ", osct->tid);

  ret = osct->tid;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
}

template<class T> static inline int 
	  gettimeofday_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t targtv, targtz;
  struct timeval tv;
  T temptv;
  struct OSDefs::L_timezone temptz;         /* always fixed */
  int ret=0;

  targtv = CC_arg_ptr(0,"pp");
  targtz = CC_arg_ptr(1,"pp");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format 
            ",0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, targtv, targtz);

  if (targtv) {
    uint64_t ctime = CC_time_get_currtime(osct->os) + osct->os->boottime;
    tv.tv_sec = ctime / 1000000000;
    tv.tv_usec = (ctime % 1000000000)/1000;
    temptv = tv;
    OS_memcpy_h2t(targtv,&temptv,sizeof(temptv));
  }
  if (targtz) {
    temptz.LSE_tz_minuteswest = 0;
    temptz.LSE_tz_dsttime = 0;
    OS_memcpy_h2t(targtz,&temptz,sizeof(temptz));
  }

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(gettimeofday) {
  return gettimeofday_body<OSDefs::L_timeval>(LSE_LINUX_ARGS,"gettimeofday");
}

static LSE_LINUX_SIG(getuid) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): getuid() = ", osct->tid);

  ret = osct->uid;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(getuid16)
LSE_LINUX_UNIMPLEMENTED(getunwind)
LSE_LINUX_UNIMPLEMENTED(getxattr)
LSE_LINUX_UNIMPLEMENTED(init_module)
LSE_LINUX_UNIMPLEMENTED(inotify_add_watch)
LSE_LINUX_UNIMPLEMENTED(inotify_init1)
LSE_LINUX_UNIMPLEMENTED(inotify_init)
LSE_LINUX_UNIMPLEMENTED(inotify_rm_watch)
LSE_LINUX_UNIMPLEMENTED(io_cancel)

//  This is a very, very tricky OS call because the type of the
//  third argument depends upon the value of the second argument.
//  Often that third argument points at a structure, so we need
//  to be very careful about how we make types go.... 
//  For real fun, "newer" ioctls are created using a very nice
//  encoding that tells you things about the arguments.  Unfortunately,
//  there are lots of older, "normal" ones that do not...
//  Even more fun.... the exact numbers are *not* standardized, *and*
//  not all IOCTLs are present everywhere.  For example, CYGWIN does not
//  have TCGETS
//
//  Checkpoints are fun, too.  But since we do not expect ioctls on 
//  pipes (fingers crossed) we can simply checkpoint here.

template<class T, class TS> static inline int 
	  ioctl_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd, request;
  LSE_emu_addr_t argp;
  int ret=0;

  fd      = CC_arg_int(0,"iip");
  request = CC_arg_int(1,"iip");
  argp    = CC_arg_ptr(2,"iip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): %s(%d,%d," LSE_emu_addr_t_print_format ") = ", 
	    osct->tid, name, fd, request, argp);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_int(request);
  CC_chkpt_arg_ptr(argp);

  /* The true way of doing this would be to dispatch to the ioctl handler
   * of a device driver.  However, we are above the OS, so we should be
   * able to just pass along.  Unfortunately, we have to deal with structure
   * translation.  Sigh!
   */
  switch (request) {
    /* ioctls found in the benchmarks whose values matter... */

#ifndef NCC
#define NCC NCCS
#endif
    case OSDefs::oTCGETS:
      {
#ifdef TCGETS
	struct termios t;
	TS targt;

	CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->ioctl(TCGETS,&t);)
        CC_chkpt_result_int(ret);

        if (ret>=0) {
          CC_chkpt_guard(targt = t;)
	  CC_chkpt_arg_int(sizeof(targt)); /* for sanity */
	  CC_chkpt_result_hmem(&targt,sizeof(targt));
	  OS_memcpy_h2t(argp,&targt,sizeof(targt));
 	}
#elif defined(TCGETA)
	struct termio t;
	TS targt;

        CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->ioctl(TCGETA,&t);)
        CC_chkpt_result_int(ret);

        if (ret>=0) {
          CC_chkpt_guard(targt = t;)
	  CC_chkpt_arg_int(sizeof(targt)); /* for sanity */
	  CC_chkpt_result_hmem(&targt,sizeof(targt));
	  OS_memcpy_h2t(argp,&targt,sizeof(targt));
 	}
#else
        ret = -1;
#endif
      }
      break;

    case OSDefs::oTCGETA :
#ifdef TCGETA
      {
	struct termio t;
	T targt;

        CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->ioctl(TCGETA,&t);)
	CC_chkpt_result_int(ret);

        if (ret>=0) {
	  CC_chkpt_guard(targt = t;)
	  CC_chkpt_arg_int(sizeof(targt)); /* for sanity */
	  CC_chkpt_result_hmem(&targt,sizeof(targt));
	  OS_memcpy_h2t(argp,&targt,sizeof(targt));
 	}
     }
#else
     ret=-1;
#endif
     break;

    /* ioctls found in the benchmarks that I can wave my hands on.... */
    case OSDefs::oTCSETAW :
#ifdef TCSETAW
      {
	struct termio t;
	T targt;

	OS_memcpy_t2h(&targt,argp,sizeof(targt));

	CC_chkpt_arg_int(sizeof(targt)); /* for sanity */
        CC_chkpt_arg_hmem(&targt,sizeof(targt));
	t = targt.get();

        CC_chkpt_guard(ret = osct->fdtable->getGoodFD(fd)->ioctl(TCSETAW,&t);)
        CC_chkpt_result_int(ret);
     }
#else
     ret = -1;
#endif
     break;

    /* Any other ioctls, we will complain about because we do not know
     * how to handle them due to size and endianness concerns in the structure
     * pointed to in the third argument
     */
    default:
	CC_chkpt_guard(ret = -1;)
        CC_chkpt_result_int(ret);
	break;
  }
  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

static LSE_LINUX_SIG(ioctl) {
  return ioctl_body<OSDefs::L_termio,
                    OSDefs::L_termios>(LSE_LINUX_ARGS,"ioctl");
}

LSE_LINUX_UNIMPLEMENTED(io_destroy)
LSE_LINUX_UNIMPLEMENTED(io_getevents)
LSE_LINUX_UNIMPLEMENTED(ioprio_get)
LSE_LINUX_UNIMPLEMENTED(ioprio_set)
LSE_LINUX_UNIMPLEMENTED(io_setup)
LSE_LINUX_UNIMPLEMENTED(io_submit)
LSE_LINUX_UNIMPLEMENTED(ipc)
LSE_LINUX_UNIMPLEMENTED(kexec_load)
LSE_LINUX_UNIMPLEMENTED(keyctl)

static LSE_LINUX_SIG(kill) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  pid_t pid;
  int sig;
  int ret=0;

  pid = CC_arg_int(0,"ii");
  sig = CC_arg_int(1,"ii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): kill(%i,%i) = ",
	    osct->tid, pid,sig);

  // append to the queue
  Linux_siginfo_t nv;
  nv.LSEsi_signo = sig;
  nv.LSEsi_errno = 0;
  nv.LSEsi_code = OSDefs::oSI_USER;
  nv.LSEsi_pid = osct->tgroup->tgid;
  nv.LSEsi_uid = osct->uid;

  if (pid == -1) { // TODO: everyone allowed, but not 1.  Crazy!
    OS_report_err(OSDefs::oENOSYS);
  } else if (pid <= 0) { // send to a process group

    if (sig < 0 || sig > 64) OS_report_err(OSDefs::oEINVAL);

    if (sig == 0) goto done; // do not actually send it.

    if (pid == 0) pid = osct->tgroup->pgroup->pgid;
    ret = send_signal_to_pgrp(osct, pid,  nv);

  } else { // send to a process

    // find the target thread group's leader
    Linux_tgroup_t *tgrp = osct->os->tgroups[pid];
    if (!tgrp) {
      osct->os->tgroups.erase(pid);
      OS_report_err(OSDefs::oESRCH);
    }
    Linux_context_t *tct = tgrp->members.front();

    if (sig < 0 || sig > 64) OS_report_err(OSDefs::oEINVAL);

    // TODO: check the permissions.
    //   user sending the signal and
    //     sender EUID/UID ! target UID/SUID and
    //     (sig != SIGCONT || different session): EPERM
    //   

    if (sig == 0) goto done; // do not actually send it.

    ret = send_signal_to(osct, tct, sig, true, nv);
  }

done:
  if (ret == -1) {
    OS_report_err(osct->myerrno());
  }
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(lchown16)
LSE_LINUX_UNIMPLEMENTED(lchown)
LSE_LINUX_UNIMPLEMENTED(lgetxattr)

static LSE_LINUX_SIG(link) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t oldpath, newpath;
  int ret=0;
  char *rfname,oldpathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  char *rfname2,newpathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didoldpath = 0;
  int didnewpath = 0;

  oldpath = CC_arg_ptr(0,"pp");
  newpath = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(oldpathbuf,oldpath,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  oldpathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didoldpath = 1;
  OS_strncpy_t2h(newpathbuf,newpath,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  newpathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didnewpath = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): link(\"%s\",\"%s\") = ", 
	    osct->tid, oldpathbuf,newpathbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(oldpath);
  CC_chkpt_arg_ptr(newpath);
  CC_chkpt_arg_str(oldpathbuf,strlen(oldpathbuf)+1);
  CC_chkpt_arg_str(newpathbuf,strlen(newpathbuf)+1);

  rfname = resolve_path(oldpathbuf, osct);
  rfname2 = resolve_path(newpathbuf, osct);
  CC_chkpt_guard(ret=link(rfname,rfname2);
		 osct->myerrno() = errno;);
  free(rfname);
  free(rfname2);
  CC_chkpt_result_int(ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didnewpath) {
      fprintf(stderr,
              "SYSCALL(%d): link(0x" LSE_emu_addr_t_print_format ",0x" 
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, oldpath,newpath);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(linkat)
LSE_LINUX_UNIMPLEMENTED(listen)
LSE_LINUX_UNIMPLEMENTED(listxattr)
LSE_LINUX_UNIMPLEMENTED(llistxattr)

static LSE_LINUX_SIG(llseek) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  off_t offsethigh;
  off_t offsetlow;
#ifdef HAS__LLSEEK
  loff_t result;
#else
  uint64_t result;
#endif
  LSE_emu_addr_t tempres;
  int whence;
  int ret=0;

  fd         = CC_arg_int(0,"iiipi");
  offsethigh = CC_arg_int(1,"iiipi");
  offsetlow  = CC_arg_int(2,"iiipi");
  tempres    = CC_arg_ptr(3,"iiipi");
  whence     = CC_arg_int(4,"iiipi");

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(offsethigh);
  CC_chkpt_arg_ptr(offsetlow);
  CC_chkpt_arg_ptr(tempres);
  CC_chkpt_arg_int(whence);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): llseek(%d,%ld,%ld,0x" LSE_emu_addr_t_print_format 
	    ",%d) = ", osct->tid, fd, offsethigh, offsetlow, tempres, whence);

  /* llseek probably does not exist on many platforms */
  
#ifdef HAS__LLSEEK
  ret = osct->fdtable->getGoodFD(fd)->_llseek(offsethigh, offsetlow, 
                                              &result, whence);
  CC_chkpt_result_int(ret);
  if (ret != -1) {
    CC_chkpt_result_int(result);
    result = LSE_h2e(result, Linux_bigendian);
    OS_memcpy_h2t(tempres,&result,sizeof(result)); /* will report error */
  }
#else
  if (((offsetlow >= 0)&&(offsethigh != 0)) || ((offsetlow < 0) && (offsethigh != -1))) {
    ret = -1;
    CC_chkpt_result_int(ret);
  } else 
   {
     ret = osct->fdtable->getGoodFD(fd)->lseek(offsetlow,whence);
     /* llseek returns 0 on success */
    CC_chkpt_result_int(ret);
     if (ret != -1)
     {
       result = ret;
       CC_chkpt_result_int(result);
       result = LSE_h2e(result, Linux_bigendian);
       OS_memcpy_h2t(tempres,&result,sizeof(result)); /* will report error */
       ret = 0;
     }
   }
#endif

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

static LSE_LINUX_SIG(_llseek) {
  return LSE_LINUX_NAME(llseek)(LSE_LINUX_ARGS);
}

LSE_LINUX_UNIMPLEMENTED(lookup_dcookie)
LSE_LINUX_UNIMPLEMENTED(lremovexattr)

static LSE_LINUX_SIG(lseek) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  off_t offset;
  int whence;
  int ret=0;

  fd     = CC_arg_int(0,"iii");
  offset = CC_arg_int(1,"iii");
  whence = CC_arg_int(2,"iii");

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(offset);
  CC_chkpt_arg_int(whence);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): lseek(%d,%ld,%d) = ", osct->tid, fd, offset, whence);

  ret = osct->fdtable->getGoodFD(fd)->lseek(offset, whence);
  CC_chkpt_result_int(ret);
  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  CC_chkpt_finish();
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(lsetxattr)

template<class T> static inline int 
	  lstat_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname, targbuf;
  struct stat buf;
  T tempbuf;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;

  fname   = CC_arg_ptr(0,"pp");
  targbuf = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(\"%s\",0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, (char *)fnamebuf,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_str(fnamebuf,strlen(fnamebuf)+1);

  rfname = resolve_path(fnamebuf, osct);
  CC_chkpt_guard(ret = lstat(rfname, &buf);
		 osct->myerrno() = errno;);
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));

  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, name, fname, targbuf);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

static LSE_LINUX_SIG(lstat64) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname, targbuf;
  LSE_stat64 buf;
  struct OSDefs::L_stat64 tempbuf;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;

  fname   = CC_arg_ptr(0,"pp");
  targbuf = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): lstat64(\"%s\",0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, (char *)fnamebuf,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_str(fnamebuf,strlen(fnamebuf)+1);

  rfname = resolve_path(fnamebuf, osct);
#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(ret = lstat64(rfname, &buf);
		 osct->myerrno() = errno;);
#else
  CC_chkpt_guard(ret = lstat(rfname, &buf);
		 osct->myerrno() = errno;);
#endif
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));

  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): lstat64(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, fname, targbuf);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(madvise)
LSE_LINUX_UNIMPLEMENTED(mbind)
LSE_LINUX_UNIMPLEMENTED(memory_ordering)
LSE_LINUX_UNIMPLEMENTED(migrate_pages)
LSE_LINUX_UNIMPLEMENTED(mincore)

static LSE_LINUX_SIG(mkdir) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname;
  mode_t mode;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;

  fname = CC_arg_ptr(0,"pi");
  mode  = CC_arg_int(1,"pi");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): mkdir(\"%s\",%d) = ",
            osct->tid, (char *)fnamebuf, mode);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_int(mode);
  CC_chkpt_arg_str(fnamebuf,(strlen(fnamebuf)+1));

  rfname = resolve_path(fnamebuf, osct);
  if (CC_open_filter(rfname)) ret = -1;
  else {
    // TODO: translate mode
    CC_chkpt_guard(ret=::mkdir(rfname,mode & ~osct->fs->umask);
		   osct->myerrno() = errno;)
  }
  free(rfname);

  if (ret != -1) {
    CC_chkpt_result_int(ret);
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(ret);
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): mkdir(0x" LSE_emu_addr_t_print_format ",%d) = ",
              osct->tid, fname, mode);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(mkdirat)

static LSE_LINUX_SIG(mknod) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname;
  mode_t mode;
  dev_t dev;
  int ret=-1;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;

  fname = CC_arg_ptr(0,"pii");
  mode  = CC_arg_int(1,"pii");
  dev   = CC_arg_int(2,"pii");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): mknod(\"%s\",%d,%d) = ",
            osct->tid, (char *)fnamebuf, mode,
	    (int)(dev));

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_int(mode);
  CC_chkpt_arg_int(dev);
  CC_chkpt_arg_str(fnamebuf,(strlen(fnamebuf)+1));

  rfname = resolve_path(fnamebuf, osct);
  if (CC_open_filter(rfname)) ret = -1;
  else {
    CC_chkpt_guard(ret=::mknod(rfname,mode & ~osct->fs->umask,dev);
		   osct->myerrno() = errno;)
  }
  free(rfname);

  if (ret != -1) {
    CC_chkpt_result_int(ret);
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(ret);
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): mknod(0x" LSE_emu_addr_t_print_format ",%d,%d) = ",
              osct->tid, fname, mode, (int)(dev));
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(mknodat)
LSE_LINUX_UNIMPLEMENTED(mlock)
LSE_LINUX_UNIMPLEMENTED(mlockall)

static inline int mmap_body(LSE_LINUX_PARMS, const char *name, int units) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t start;
  LSE_emu_addr_t length;
  int prot;
  int flags;
  int fd;
  LSE_emu_addr_t offset;
  LSE_emu_addr_t rval;
  int ret;
  bool is64bit = osct->is64bit;

  start  = CC_arg_ptr(0,"ppiiip");
  length = CC_arg_ptr(1,"ppiiip");
  prot   = CC_arg_int(2,"ppiiip");
  flags  = CC_arg_int(3,"ppiiip");
  fd     = CC_arg_int(4,"ppiiip");
  offset = sextend32(changelen, CC_arg_ptr(5,"ppiiip"));

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format ",0x"
	    LSE_emu_addr_t_print_format ",0x%x,0x%x,%d,0x"
	    LSE_emu_addr_t_print_format ") = ",
            osct->tid, name, start, length, prot, flags, fd, offset);

  LSE_emu_addr_t amask; amask = OSDefs::oPAGE_SIZE-1;

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(start);
  CC_chkpt_arg_ptr(length);
  CC_chkpt_arg_int(prot);
  CC_chkpt_arg_int(flags);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(offset);

  offset *= units;
  if (offset & amask) OS_report_err(OSDefs::oEINVAL);

  // lookup true file pointer/descriptor
  Linux_file_t *lfp; lfp = 0;
  if (!(flags & OSDefs::oMAP_ANONYMOUS)) {
    lfp = osct->fdtable->getGoodFD(fd);
    if (!lfp) OS_report_err(OSDefs::oEINVAL);
  }
  rval = do_mmap_faligned(osct, start, length, offset, prot, flags, lfp);

  if (rval > OSDefs::oUTASK_SIZE(is64bit)) OS_report_err(-rval);

  OS_report_results(0,rval);
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"0x" LSE_emu_addr_t_print_format "\n", rval);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  CC_chkpt_finish();
  OS_syserr_handler();
}

static LSE_LINUX_SIG(mmap) {
  return mmap_body(LSE_LINUX_ARGS,"mmap",1);
}
static LSE_LINUX_SIG(mmap2) {
  return mmap_body(LSE_LINUX_ARGS,"mmap2",4096);
}

LSE_LINUX_UNIMPLEMENTED(mount)
LSE_LINUX_UNIMPLEMENTED(move_pages)

static LSE_LINUX_SIG(mprotect) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t addr, end;
  LSE_emu_addr_t mlen;
  int prot;
  int rval,ret=0;

  addr = CC_arg_ptr(0,"ppi");
  mlen = CC_arg_ptr(1,"ppi");
  prot = CC_arg_int(2,"ppi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): mprotect(0x" LSE_emu_addr_t_print_format ",0x"
              LSE_emu_addr_t_print_format ",%d) = ",
              osct->tid, addr, mlen, prot);

  LSE_emu_addr_t amask; amask = OSDefs::oPAGE_SIZE-1;

  if ((prot & (OSDefs::oPROT_GROWSDOWN|OSDefs::oPROT_GROWSUP))
      == (OSDefs::oPROT_GROWSDOWN|OSDefs::oPROT_GROWSUP))
      OS_report_err(OSDefs::oEINVAL);
	
  if (addr & amask) OS_report_err(OSDefs::oEINVAL);

  if (mlen) {
    mlen = (mlen + amask) & ~amask;
    end = mlen + addr;
    if (end <= addr) OS_report_err(OSDefs::oENOMEM);

    Linux_memory_t::ranges_t::iterator hv;
    hv = osct->mem->vranges.upper_bound(addr);
    if (hv == osct->mem->vranges.end()) OS_report_err(OSDefs::oENOMEM);

    // now have hv->second.end > addr
    if (OSDefs::oPROT_GROWSDOWN & prot) {
      // check for overlap
      if (hv->second.start >= end) OS_report_err(OSDefs::oENOMEM);
      // can we really grow down?
      if (!(hv->second.flags & OSDefs::oPROT_GROWSDOWN)) 
        OS_report_err(OSDefs::oEINVAL);
      addr = hv->second.start;
    } else {
      if (hv->second.start > addr) OS_report_err(OSDefs::oENOMEM);
      if (OSDefs::oPROT_GROWSUP & prot) {
	// can we really grow up?
	if (!(hv->second.flags & OSDefs::oPROT_GROWSUP)) 
	  OS_report_err(OSDefs::oEINVAL);
	end = hv->second.end;
      }
    }
    rval = do_mprotect(osct, addr, end-addr, prot);
    if (rval) OS_report_err(-rval);
  }

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(mq_getsetattr)
LSE_LINUX_UNIMPLEMENTED(mq_notify)
LSE_LINUX_UNIMPLEMENTED(mq_open)
LSE_LINUX_UNIMPLEMENTED(mq_timedreceive)
LSE_LINUX_UNIMPLEMENTED(mq_timedsend)
LSE_LINUX_UNIMPLEMENTED(mq_unlink)

static LSE_LINUX_SIG(mremap) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t start,oldlength, newlength;
  int flags;
  int ret = 0;
  LSE_emu_addr_t rval = 0;

  start     = CC_arg_ptr(0,"pppi");
  oldlength = CC_arg_ptr(1,"pppi");
  newlength = CC_arg_ptr(2,"pppi");
  flags     = CC_arg_int(3,"pppi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): mremap(0x" LSE_emu_addr_t_print_format ",0x" 
	    LSE_emu_addr_t_print_format ",0x" LSE_emu_addr_t_print_format
	    ",%d) = ",
            osct->tid, start, oldlength, newlength, flags);

  // For now, this is unimplemented and generates errors...
  OS_report_err(OSDefs::oENOSYS);

  OS_report_results(0,rval);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"0x" LSE_emu_addr_t_print_format "\n", rval);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(msgctl)
LSE_LINUX_UNIMPLEMENTED(msgget)
LSE_LINUX_UNIMPLEMENTED(msgrcv)
LSE_LINUX_UNIMPLEMENTED(msgsnd)

static LSE_LINUX_SIG(msync) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t start,length;
  int rval = 0;
  int flags;

  start  = CC_arg_ptr(0,"ppi");
  length = CC_arg_ptr(1,"ppi");
  flags  = CC_arg_int(2,"ppi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): msync(0x" LSE_emu_addr_t_print_format " 0x" 
	    LSE_emu_addr_t_print_format ",%d) = ",
            osct->tid, start, length,flags);

  LSE_emu_addr_t amask = OSDefs::oPAGE_SIZE-1;

  if (start & amask) OS_report_err(OSDefs::oEINVAL);
  length = (length + amask) & ~amask;

  rval = do_msync(osct, start, length, true);
  if (rval) OS_report_err(-rval);

  OS_report_results(0,0);
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"0x" LSE_emu_addr_t_print_format "\n", rval);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(munlock)
LSE_LINUX_UNIMPLEMENTED(munlockall)

static LSE_LINUX_SIG(munmap) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t start,length;
  int rval = 0;

  start  = CC_arg_ptr(0,"pp");
  length = CC_arg_ptr(1,"pp");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): munmap(0x" LSE_emu_addr_t_print_format " 0x" 
	    LSE_emu_addr_t_print_format ") = ",
            osct->tid, start, length);

  rval = do_munmap(osct, start, length);
  if (rval) OS_report_err(-rval);

  OS_report_results(0,0);
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"0x" LSE_emu_addr_t_print_format "\n", rval);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

template<class T> static inline int 
	  nanosleep_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t req, rem;
  int ret=0;
  uint64_t saved_return;
  T buf, tempbuf;
  uint64_t currtime;
  uint64_t oldblocked;

  req = CC_arg_ptr(0,"pp");
  rem = CC_arg_ptr(1,"pp");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): %s(0x"
             LSE_emu_addr_t_print_format ",0x" LSE_emu_addr_t_print_format 
	     ") = ", osct->tid, name, req, rem);

  if (!req) OS_report_err(OSDefs::oEFAULT);
  else {
    OS_memcpy_t2h(&tempbuf,req,sizeof(tempbuf)); /* will report error */
    buf = tempbuf.e2h();
  }
  
  // create a timer, a continuation pointing at the timer, and an event
  // on the timer.
  {
    Linux_wu_timer_t *nt = new Linux_wu_timer_t(osct->os,1,osct);
    osct->continuationStack.push_back(new cont_nsleep(osct, nt, 0, rem));
    block_thread(osct, true);
    Linux_timer_t::itval v;
    v.interval = 0;
    v.expiration = (uint64_t)buf.LSE_tv_sec * 1000000000 + buf.LSE_tv_nsec;
    nt->settime(&v, 0);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"blocked\n");

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
aftersyserr:
  OS_syserr_handler();
}

static LSE_LINUX_SIG(nanosleep) {
  return nanosleep_body<OSDefs::L_timespec>(LSE_LINUX_ARGS,"nanosleep");
}

static LSE_LINUX_SIG(newfstat) {
  return fstat_body<OSDefs::L_newstat>(LSE_LINUX_ARGS,"newfstat");
}

LSE_LINUX_UNIMPLEMENTED(newfstatat)

static LSE_LINUX_SIG(newlstat) {
  return lstat_body<OSDefs::L_newstat>(LSE_LINUX_ARGS,"newlstat");
}

// 32-bit fstat craziness
template<class T> static inline int 
	  stat_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname, targbuf;
  struct stat buf;
  T tempbuf;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;

  fname   = CC_arg_ptr(0,"pp");
  targbuf = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(\"%s\",0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, (char *)fnamebuf,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_str(fnamebuf,strlen(fnamebuf)+1);

  rfname = resolve_path(fnamebuf, osct);
  CC_chkpt_guard(ret = stat(rfname, &buf);
		 osct->myerrno() = errno;);
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  /* 
   * note that these values will depend upon the HOST system
   */
  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): stat(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, fname, targbuf);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

static LSE_LINUX_SIG(newstat) {
  return stat_body<OSDefs::L_newstat>(LSE_LINUX_ARGS,"newstat");
}

static int uname_body(LSE_LINUX_PARMS, const char *);
static LSE_LINUX_SIG(newuname) {
  return uname_body(LSE_LINUX_ARGS,"newuname");
}

LSE_LINUX_UNIMPLEMENTED(nfsservctl)
LSE_LINUX_UNIMPLEMENTED(nice)
LSE_LINUX_UNIMPLEMENTED(ni_syscall)
// LSE_LINUX_UNIMPLEMENTED(fstat)
LSE_LINUX_UNIMPLEMENTED(oldumount)
LSE_LINUX_UNIMPLEMENTED(old_umount)
LSE_LINUX_UNIMPLEMENTED(olduname)

static LSE_LINUX_SIG(open) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname;
  int flags,realflags;
  mode_t mode;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;
  Linux_native_file_t *fp;

  fname = CC_arg_ptr(0,"pii");
  flags = CC_arg_int(1,"pii");
  mode  = CC_arg_int(2,"pii");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;
  didfname = 1;

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_int(flags);
  CC_chkpt_arg_int(mode);
  CC_chkpt_arg_str(fnamebuf,(strlen(fnamebuf)+1));

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): open(\"%s\",%d,%d) = ",
            osct->tid, (char *)fnamebuf,flags,mode);

  /* 
     The following flags may vary between platforms
     Check them and remap them if necessary.

     The special flags start at 040000.
     For the time being, we will only support O_DIRECT, O_DIRECTORY, O_NOFOLLOW
     since these are the only ones that are consistently available.
  */

  realflags = flags & 3; /* should be good for all systems */

  if (flags & OSDefs::oO_CREAT) realflags |= O_CREAT;
  if (flags & OSDefs::oO_EXCL) realflags |= O_EXCL;
  if (flags & OSDefs::oO_NOCTTY) realflags |= O_NOCTTY;
  if (flags & OSDefs::oO_TRUNC) realflags |= O_TRUNC;
  if (flags & OSDefs::oO_APPEND) realflags |= O_APPEND;
  if (flags & OSDefs::oO_NONBLOCK) realflags |= O_NONBLOCK;
  if (flags & OSDefs::oO_NDELAY) realflags |= O_NDELAY;
  if (flags & OSDefs::oO_SYNC) realflags |= O_SYNC;
#ifdef O_LARGEFILE
  if (flags & OSDefs::oO_LARGEFILE) realflags |= O_LARGEFILE;
#endif

  /* These flags only exist if __USE_GNU is on */
  #ifdef __USE_GNU
  if (flags & OSDefs::oO_DIRECT) realflags |= O_DIRECT;
  if (flags & OSDefs::oO_DIRECTORY) realflags |= O_DIRECTORY;
  #endif
#ifdef O_NOFOLLOW
  if (flags & OSDefs::oO_NOFOLLOW) realflags |= O_NOFOLLOW;  
#endif

  rfname = resolve_path(fnamebuf, osct);
  osct->myerrno() = 0;
  if (CC_open_filter(rfname)) ret = -1;
  else {
    CC_chkpt_guard(ret=open(rfname,realflags,
			    mode & ~osct->fs->umask);
		   osct->myerrno() = errno;)
  }
  free(rfname);

  CC_chkpt_result_int(ret);
  if (ret != -1) {
    // TODO: at some point, try looking for O_CLOEXEC - version 2.6.23
    if (CC_chkpt_guard_true)
      ret = osct->fdtable->addToTable(fp = new 
				      Linux_native_file_t(osct->os,ret));
    else 
      ret = osct->fdtable->addToTable(fp = new 
				      Linux_native_file_t(osct->os, -1));
  }
  if (ret >= 0) {
    CC_chkpt_result_int(ret);
    CC_chkpt_result_int(fp->statbuf.st_dev);
    CC_chkpt_result_int(fp->statbuf.st_ino);
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(ret);
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): open(0x" LSE_emu_addr_t_print_format ",%d,%d) = ",
              osct->tid, fname, flags, mode);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(openat)

static LSE_LINUX_SIG(pause) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int rval = 0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): pause() = blocked\n",osct->tid);

  osct->continuationStack.push_back(new cont_pause(osct));
  block_thread(osct, true);
  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(pciconfig_iobase)
LSE_LINUX_UNIMPLEMENTED(pciconfig_read)
LSE_LINUX_UNIMPLEMENTED(pciconfig_write)
LSE_LINUX_UNIMPLEMENTED(perf_counter_open)
LSE_LINUX_UNIMPLEMENTED(perfctr)
LSE_LINUX_UNIMPLEMENTED(perfmonctl)
LSE_LINUX_UNIMPLEMENTED(personality)

static LSE_LINUX_SIG(pipe) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fdesp;
  int ret=0;
  int fdes[2], swapdes[2];

  fdesp = CC_arg_ptr(0,"p");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): pipe(0x" LSE_emu_addr_t_print_format ") = ",
            osct->tid, fdesp);

  Linux_pipe_t *pp;

  pp = new Linux_pipe_t(osct->os);
  fdes[0] = osct->fdtable->addToTable(new Linux_pipe_file_t(osct->os,
                                                            pp, false));
  fdes[1] = osct->fdtable->addToTable(new Linux_pipe_file_t(osct->os,
                                                            pp, true));

  if (ret == -1) {
    OS_report_err(osct->myerrno());
  } else {
    swapdes[0] = LSE_h2e(fdes[0], Linux_bigendian);
    swapdes[1] = LSE_h2e(fdes[1], Linux_bigendian);
    OS_memcpy_h2t(fdesp,swapdes,8);
    OS_report_results(0,ret);
  }

  CC_hook_pipe();

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(pipe2) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fdesp;
  int ret=0;
  int fdes[2], swapdes[2];
  int flags=0;

  fdesp = CC_arg_ptr(0,"pi");
  flags = CC_arg_int(0,"pi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): pipe2(0x" LSE_emu_addr_t_print_format ",%d) = ",
            osct->tid, fdesp,flags);

  Linux_pipe_t *pp;
  pp = new Linux_pipe_t(osct->os);
  fdes[0] = osct->fdtable->addToTable(new Linux_pipe_file_t(osct->os,
							    pp, false));
  fdes[1] = osct->fdtable->addToTable(new Linux_pipe_file_t(osct->os,
							    pp, true));
  osct->fdtable->getGoodFD(fdes[0])->fcntl(F_SETFL, flags);
  osct->fdtable->getGoodFD(fdes[1])->fcntl(F_SETFL, flags);

  if (ret == -1) {
    OS_report_err(osct->myerrno());
  } else {
    swapdes[0] = LSE_h2e(fdes[0], Linux_bigendian);
    swapdes[1] = LSE_h2e(fdes[1], Linux_bigendian);
    OS_memcpy_h2t(fdesp,swapdes,8);
    OS_report_results(0,ret);
  }

  CC_hook_pipe2();

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(pivot_root)

static bool poll_piece(LSE_LINUX_PARMS,
		       Linux_context_t *osct,
		       int &osret,
		       LSE_emu_addr_t fds,
		       int nfds, int64_t timeout,
		       struct pollfd *pfd, 
		       int&ret, bool &mustfree,
		       cont_poll *&cp) {
  
  cp = new cont_poll(osct, pfd, fds, nfds, timeout);

  // for each in set, check for readiness
  int cnt = 0;

  for (int i=0 ; i < nfds; ++i) {
    Linux_file_t *fp = osct->fdtable->getGoodFD(pfd[i].fd);
    short result = fp->poll();
    result &= (pfd[i].events | OSDefs::oPOLLNVAL | OSDefs::oPOLLHUP | 
               OSDefs::oPOLLERR);
    pfd[i].revents = result;
    if (result) cnt++;
    else fp->register_poll_waiter(cp);
  }

  if (cnt == 0)
    if (osct->sigPendingFlag) OS_report_err(OSDefs::oEINTR);
    else if (timeout) { 
      osct->continuationStack.push_back(cp);
      block_thread(osct, true);
      mustfree = false;
      ret = -2;
      return false;
    }  

  // fix the endianness back
  for (int i=0 ; i < nfds; ++i) {
    pfd[i].fd = LSE_h2e(pfd[i].fd, Linux_bigendian);
    pfd[i].events = LSE_h2e(pfd[i].events, Linux_bigendian);
    pfd[i].revents = LSE_h2e(pfd[i].revents, Linux_bigendian);
  }

  // copy back
  CC_chkpt_result_hmem(&pfd[0], sizeof(struct pollfd) * nfds);
  OS_memcpy_h2t(fds, &pfd[0], sizeof(struct pollfd) * nfds);

  ret = cnt;
  OS_report_results(0,ret);

  return false;
 syserr:
  return true;
 }

static LSE_LINUX_SIG(poll) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fds;
  int nfds;
  int64_t timeout;
  int ret=0,cnt=0;
  bool mustfree = false;
  struct pollfd *pfd;
  short amask = 0;
  cont_poll *cp;

  fds     = CC_arg_ptr(0,"pii");
  nfds    = CC_arg_int(1,"pii");
  timeout = CC_arg_int(2,"pii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
        "SYSCALL(%d): poll(0x" LSE_emu_addr_t_print_format ",%d,%d) = ", 
        osct->tid, fds, nfds, timeout);

  if (nfds < 0) OS_report_err(OSDefs::oEINVAL);

  // copy the descriptor structures

  pfd = new pollfd[nfds];
  if (!pfd) OS_report_err(OSDefs::oENOMEM);
  mustfree = true;
  OS_memcpy_t2h(&pfd[0], fds, sizeof(struct pollfd) * nfds);

  // fix the endianness
  for (int i=0 ; i < nfds; ++i) {
    pfd[i].fd = LSE_h2e(pfd[i].fd, Linux_bigendian);
    pfd[i].events = LSE_h2e(pfd[i].events, Linux_bigendian);
    pfd[i].revents = LSE_h2e(pfd[i].revents, Linux_bigendian);
  }

  if (poll_piece(LSE_LINUX_ARGS, osct, osret, fds, nfds, timeout,
		 pfd, ret, mustfree, cp))
    goto syserr;

  if (mustfree) delete cp;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    if (ret == -2) fprintf(stderr, "blocked\n");
    else fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (mustfree) delete cp;
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(ppc_rtas)
LSE_LINUX_UNIMPLEMENTED(ppoll)
LSE_LINUX_UNIMPLEMENTED(prctl)

static LSE_LINUX_SIG(pread) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  char *buf=0;
  char mybuf[1024];
  LSE_emu_addr_t targbuf;
  int count, ocount;
  int ret=0, mustfree=0;
  off_t offset;

  fd      = CC_arg_int(0,"ipii");
  targbuf = CC_arg_ptr(1,"ipii");
  ocount  = count = CC_arg_int(2,"ipii");
  offset  = CC_arg_int(3,"ipii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
        "SYSCALL(%d): pread(%d, 0x" LSE_emu_addr_t_print_format ",%d, %d) = ", 
	    osct->tid, fd, targbuf, ocount, offset);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_int(count);
  CC_chkpt_arg_int(offset);

  if (!count) {
    ret = 0;
    CC_chkpt_result_int(ret);
    OS_report_results(0,ret);
    buf = mybuf; /* shut up warnings */
  } else {
    if (count > 1024) {
      buf = (char *)malloc(count);
      if (buf == 0) {
	count = 1024;
	buf = mybuf;
      }
      else mustfree = 1;
    } else buf = mybuf;

    {
      struct iovec thisio = { buf, count };
      off_t oseek = osct->fdtable->getGoodFD(fd)->lseek(0, SEEK_CUR);
      if (ret != -1) {
        ret = osct->fdtable->getGoodFD(fd)->lseek(offset, SEEK_SET);
        if (ret != -1) {
          ret = osct->fdtable->getGoodFD(fd)->readv(osct, &thisio, 1);
          osct->fdtable->getGoodFD(fd)->lseek(oseek, SEEK_SET);
        }
      }
    }
    CC_chkpt_result_int(ret);

    if (ret == -2) { // everything has already been done in readv
    } else if (ret == -1) {
      CC_chkpt_result_int(osct->myerrno());
      OS_report_err(osct->myerrno());
    } else {
      CC_chkpt_result_hmem(buf, ret);
      OS_memcpy_h2t(targbuf,buf,ret); /* will report error */
      OS_report_results(0,ret);
    }
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (ret == -2) fprintf(stderr,"blocked\n");
    else {
      int ii;
      fprintf(stderr, "%d\t\"",ret);    
      for (int ii=0;ii<30 && ii<ret && buf[ii]; ii++)
	fputc(isprint(buf[ii])?buf[ii] : 0x2e, stderr);
      fprintf(stderr,"%s\"\n", ii < ret ? "...":"");
    }
  }
  if (mustfree) free(buf);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"-1 %d\n",(int)OS_sys_get_return(1));
  if (mustfree) free(buf);
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(pread64)
LSE_LINUX_UNIMPLEMENTED(preadv)
LSE_LINUX_UNIMPLEMENTED(pselect6)
LSE_LINUX_UNIMPLEMENTED(ptrace)

static LSE_LINUX_SIG(pwrite) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  char *buf=0;
  char mybuf[1024];
  LSE_emu_addr_t targbuf;
  int count;
  int ret=0, mustfree=0;
  off_t offset;

  fd      = CC_arg_int(0,"ipii");
  targbuf = CC_arg_ptr(1,"ipii");
  count   = CC_arg_int(2,"ipii");
  offset  = CC_arg_int(3,"ipii");

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_int(count);
  CC_chkpt_arg_int(offset);

  if (!count) {
    OS_report_results(0,0);
    buf = mybuf;
    ret = 0;
  } else {
    if (count > 1024) {
      buf = (char *)malloc(count);
      if (buf == 0) {
	OS_report_err(OSDefs::oENOSPC);
      }
      mustfree = 1;
    } else buf = mybuf;

    OS_memcpy_t2h(buf, targbuf,count); /* will report error */

    CC_chkpt_arg_hmem(buf,count);
    {
      struct iovec thisio = { buf, count };
      off_t oseek = osct->fdtable->getGoodFD(fd)->lseek(0, SEEK_CUR);
      if (ret != -1) {
        ret = osct->fdtable->getGoodFD(fd)->lseek(offset, SEEK_SET);
        if (ret != -1) {
          ret = osct->fdtable->getGoodFD(fd)->writev(osct, &thisio, 1);
          osct->fdtable->getGoodFD(fd)->lseek(oseek, SEEK_SET);
        }
      }
    }
    CC_chkpt_result_int(ret);

    if (ret == -1) {
      CC_chkpt_result_int(osct->myerrno());
      OS_report_err(osct->myerrno());
    } else {
      OS_report_results(0,ret);
    }
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    int i;
    fprintf(stderr,"SYSCALL(%d): pwrite(%d,\"",osct->tid, fd);
    for (i=0;i<30 && i<ret && buf[i]; i++) 
      fputc(isprint(buf[i])?buf[i] : 0x2e, stderr);
    fprintf(stderr,"\"%s,%d,%d) = %d\n",i<ret?"...":"",count,
            (int)offset,ret);
  }
  if (mustfree) free(buf);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): pwrite(%d," LSE_emu_addr_t_print_format ",%d,%d) = -1 %d\n",
	    osct->tid, fd, targbuf, count,(int)offset,
            (int)OS_sys_get_return(1));
  if (mustfree) free(buf);
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(pwrite64)
LSE_LINUX_UNIMPLEMENTED(pwritev)
LSE_LINUX_UNIMPLEMENTED(query_module)
LSE_LINUX_UNIMPLEMENTED(quotactl)

static LSE_LINUX_SIG(read) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  char *buf=0;
  char mybuf[1024];
  LSE_emu_addr_t targbuf;
  int count, ocount;
  int ret=0, mustfree=0;

  fd      = CC_arg_int(0,"ipi");
  targbuf = CC_arg_ptr(1,"ipi");
  ocount  = count = CC_arg_int(2,"ipi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
        "SYSCALL(%d): read(%d, 0x" LSE_emu_addr_t_print_format ",%d) = ", 
	    osct->tid, fd, targbuf, ocount);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_int(count);

  if (!count) {
    ret = 0;
    CC_chkpt_result_int(ret);
    OS_report_results(0,ret);
    buf = mybuf; /* shut up warnings */
  } else {
    if (count > 1024) {
      buf = (char *)malloc(count);
      if (buf == 0) {
	count = 1024;
	buf = mybuf;
      }
      else mustfree = 1;
    } else buf = mybuf;

    struct iovec thisio = { buf, count };
    
    ret = osct->fdtable->getGoodFD(fd)->readv(osct, &thisio, 1);
    CC_chkpt_result_int(ret);

    if (ret == -2) {  // everything handled inside readv
    } else if (ret == -1) {
      CC_chkpt_result_int(osct->myerrno());
      OS_report_err(osct->myerrno());
    } else {
      CC_chkpt_result_hmem(buf,ret);
      OS_memcpy_h2t(targbuf,buf,ret); /* will report error */
      OS_report_results(0,ret);
    }
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (ret == -2) fprintf(stderr,"blocked\n");
    else {
      int ii=0;
      fprintf(stderr, "%d\t\"",ret);    
      for (ii=0;ii<30 && ii<ret && buf[ii]; ii++)
	fputc(isprint(buf[ii])?buf[ii] : 0x2e, stderr);
      fprintf(stderr,"%s\"\n", ii < ret ? "...":"");
    }
  }
  if (mustfree) free(buf);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"-1 %d\n",(int)OS_sys_get_return(1));
  if (mustfree) free(buf);
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(readahead)
LSE_LINUX_UNIMPLEMENTED(readdir)

static LSE_LINUX_SIG(readlink) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t path;
  LSE_emu_addr_t buf;
  size_t bufsiz;
  int ret=0;
  char *rfname,pathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  char mybuf[1024];
  char *buftouse = &mybuf[0];
  int didpath = 0, mustfree=0;

  path   = CC_arg_ptr(0,"ppi");
  buf    = CC_arg_ptr(1,"ppi");
  bufsiz = sextend32(changelen,CC_arg_int(2,"ppi"));

  OS_strncpy_t2h(pathbuf,path,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  pathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didpath = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): readlink(\"%s\",0x" LSE_emu_addr_t_print_format 
            ",%d) = ",
            osct->tid, pathbuf, buf, bufsiz);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(path);
  CC_chkpt_arg_ptr(buf);
  CC_chkpt_arg_int(bufsiz);
  CC_chkpt_arg_str(pathbuf,strlen(pathbuf)+1);

  if (bufsiz > 1024) {
    buftouse = (char *)malloc(bufsiz);
    if (buftouse == 0) {
      OS_report_err(ENOMEM);
    }
    else mustfree = 1;
  }

  rfname = resolve_path(pathbuf, osct);
  CC_chkpt_guard(ret=readlink(rfname,buftouse,bufsiz);
		 osct->myerrno() = errno;);
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret != -1) {
    CC_chkpt_result_str(buftouse,bufsiz);
    OS_memcpy_h2t(buf,buftouse,bufsiz); /* will report error */
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  if (mustfree) free(buftouse);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didpath) {
      fprintf(stderr,
              "SYSCALL(%d): readlink(0x" LSE_emu_addr_t_print_format ",0x" 
                                 LSE_emu_addr_t_print_format ",%d) = ",
              osct->tid, path, buf, bufsiz);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  if (mustfree) free(buftouse);
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(readlinkat)

static LSE_LINUX_SIG(readv) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  OSDefs::L_iovec *buf=0;
  struct iovec *hostbuf=0;
  LSE_emu_addr_t targbuf;
  int count;
  int ret=0, mustfree=0;
  int i, rcount=0;

  fd      = CC_arg_int(0,"ipi");
  targbuf = CC_arg_ptr(1,"ipi");
  count   = CC_arg_int(2,"ipi");

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_int(count);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): readv(%d," LSE_emu_addr_t_print_format ",%d) = ",
	    osct->tid, fd, targbuf, count);

  buf = (OSDefs::L_iovec *)malloc(sizeof(OSDefs::L_iovec)*count+1);
  hostbuf = (struct iovec *)malloc(sizeof(struct iovec)*count+1);
  OS_memcpy_t2h(buf, targbuf,
		count*sizeof(OSDefs::L_iovec)); /* will report error */
  mustfree = 1;

  CC_chkpt_arg_int(sizeof(OSDefs::L_iovec)); 	 
  CC_chkpt_arg_hmem(buf,count * sizeof(OSDefs::L_iovec));

  for (i=0;i<count;i++)
  {
    OSDefs::L_size_t len = LSE_e2h(buf[i].iov_len,Linux_bigendian);
    hostbuf[i].iov_base = (char *)malloc(len+1);
    hostbuf[i].iov_len = len;
    rcount = i+1;
  }

  ret = osct->fdtable->getGoodFD(fd)->readv(osct, hostbuf,count);

  if (ret == -2) { // blocking already taken care of in FD readv
  } else if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  } else {
    OSDefs::L_size_t togo = ret;
    for (i=0;i<rcount;i++) {
      LSE_emu_addr_t base;
      OSDefs::L_size_t thisone 
	= std::min((OSDefs::L_size_t)(hostbuf[i].iov_len), togo);
      CC_chkpt_result_hmem(hostbuf[i].iov_base, thisone);
      base = LSE_e2h(buf[i].iov_base, Linux_bigendian);
      OS_memcpy_h2t(base, hostbuf[i].iov_base, thisone);
      togo -= thisone;
    }

    OS_report_results(0,ret);
  }

  if (mustfree) {
    for (i=0;i<rcount;i++) {
      free(hostbuf[i].iov_base);
    }
    free(buf);
    free(hostbuf);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    if (ret == -2) fprintf(stderr, "blocked\n");
    else fprintf(stderr, "%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  if (mustfree) {
     for (i=0;i<rcount;i++)    
     {
       free(hostbuf[i].iov_base);
     }
     free(buf);
     free(hostbuf);
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(reboot)
LSE_LINUX_UNIMPLEMENTED(recv)
LSE_LINUX_UNIMPLEMENTED(recvfrom)
LSE_LINUX_UNIMPLEMENTED(recvmsg)
LSE_LINUX_UNIMPLEMENTED(remap_file_pages)
LSE_LINUX_UNIMPLEMENTED(removexattr)

static LSE_LINUX_SIG(rename) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t oldpath, newpath;
  int ret=0;
  char *rfname,oldpathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  char *rfname2,newpathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didoldpath = 0;
  int didnewpath = 0;

  oldpath = CC_arg_ptr(0,"pp");
  newpath = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(oldpathbuf,oldpath,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  oldpathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didoldpath = 1;
  OS_strncpy_t2h(newpathbuf,newpath,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  newpathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didnewpath = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): rename(\"%s\",\"%s\") = ", 
	    osct->tid, oldpathbuf,newpathbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(oldpath);
  CC_chkpt_arg_ptr(newpath);
  CC_chkpt_arg_str(oldpathbuf,strlen(oldpathbuf)+1);
  CC_chkpt_arg_str(newpathbuf,strlen(newpathbuf)+1);

  rfname = resolve_path(oldpathbuf, osct);
  rfname2 = resolve_path(newpathbuf, osct);
  CC_chkpt_guard(ret=rename(rfname,rfname2);
		 osct->myerrno() = errno;);
  free(rfname);
  free(rfname2);
  CC_chkpt_result_int(ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didnewpath) {
      fprintf(stderr,
              "SYSCALL(%d): rename(0x" LSE_emu_addr_t_print_format ",0x" 
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, oldpath,newpath);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(renameat)
LSE_LINUX_UNIMPLEMENTED(request_key)

static LSE_LINUX_SIG(restart_syscall) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): restart_syscall(\"%s\")");

  if (!osct->restart) OS_report_err(OSDefs::oEINTR);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, " restarting a call\n");

  osct->restart->run(); // will take care of setting up status, etc..

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, " = -1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(rmdir) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname=0;

  fname = CC_arg_ptr(0,"p");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): rmdir(\"%s\") = ",
	    osct->tid, (char *)fnamebuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_str(fnamebuf,strlen(fnamebuf)+1);

  rfname = resolve_path(fnamebuf, osct);
  CC_chkpt_guard(ret=rmdir(rfname);
		 osct->myerrno() = errno;);
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): rmdir(0x" LSE_emu_addr_t_print_format ") = ",
              osct->tid, fname);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

template<class T> static inline int 
	  rt_sigaction_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int sig,sz;
  LSE_emu_addr_t act, oldact;
  T tempbuf, buf;
  int ret=0;

  sig    = CC_arg_int(0,"ippi");
  act    = CC_arg_ptr(1,"ippi");
  oldact = CC_arg_ptr(2,"ippi");
  sz     = CC_arg_int(3,"ippi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
          "SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format ",0x" 
                                 LSE_emu_addr_t_print_format ",%d) = ",
          osct->tid, name, sig, act, oldact, sz);

  // valid signal numbers
  sig--;
  if (sig < 0 || sig > 63 || sz != sizeof(OSDefs::L_sigset) ||
      ( (uint64_t(1)<<sig) & Linux_sighandinf_t::cantChangeMask ))
    OS_report_err(OSDefs::oEINVAL);

  if (osct->sigPendingFlag) OS_report_err(OSDefs::oERESTARTNOINTR);

  if (oldact) { 
    tempbuf = osct->sighandtable->handlers[sig];
    OS_memcpy_h2t(oldact, &tempbuf, sizeof(tempbuf));
  }

  if (act) {

    // update the action
    OS_memcpy_t2h(&tempbuf, act, sizeof(tempbuf));

    Linux_sighandinf_t &sh = osct->sighandtable->handlers[sig];
    sh = tempbuf.tosighand();
    sh.sigmask &= ~Linux_sighandinf_t::cantChangeMask;

    if (sh.handler == 1 ||
        sh.handler == 0 && (OSDefs::oSIGDEF_IGNORE() &(uint64_t(1)<<(sig-1)))) {

      // Remove signals from pending masks and queues when set to ignore

      uint64_t mask = uint64_t(1) << (sig - 1);

      if (mask & osct->tgroup->sigpending) {

        osct->tgroup->sigpending &= ~mask;

	for (std::list<Linux_siginfo_t>::iterator 
	     j = osct->tgroup->signals.begin(), 
	     je = osct->tgroup->signals.end(); j != je; ) {
          if (j->LSEsi_signo == sig) j = osct->tgroup->signals.erase(j);
	  else j++;
        }
      }

      std::list<Linux_context_t *> &tgl = osct->tgroup->members;
      for (std::list<Linux_context_t *>::iterator i = tgl.begin(), ie=tgl.end();
           i != ie; ++i) {

	if (!(mask & (*i)->sigpending)) continue;
        (*i)->sigpending &= ~mask;

	for (std::list<Linux_siginfo_t>::iterator 
	     j = (*i)->signals.begin(), je = (*i)->signals.end(); j != je; ) {
          if (j->LSEsi_signo == sig) j = (*i)->signals.erase(j);
	  else j++;
        }
        (*i)->sigPendingFlag = ((*i)->sigpending & ~(*i)->sigblocked) || 
  		             ((*i)->tgroup->sigpending & ~(*i)->sigblocked);
      }
    } // setting handler to ignore
  }

  OS_report_results(0, ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(rt_sigaction) {
  return rt_sigaction_body<OSDefs::L_sigaction>(LSE_LINUX_ARGS,"rt_sigaction");
}

template<class T> static inline int 
	  rt_sigpending_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int sz;
  LSE_emu_addr_t set;
  T tempbuf;
  int ret=0;

  set = CC_arg_ptr(0,"pi");
  sz  = CC_arg_int(1,"pi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
          "SYSCALL(%d): %s(%0x" LSE_emu_addr_t_print_format ",%d) = ",
	    osct->tid, name, set, sz);

  if (sz > sizeof(OSDefs::L_sigset)) OS_report_err(OSDefs::oEINVAL);

  if (set) { 
    tempbuf.fromword((osct->sigpending | osct->tgroup->sigpending)
    		     & osct->sigblocked);
    OS_memcpy_h2t(set, &tempbuf, sizeof(tempbuf));
  }

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(rt_sigpending) {
  return rt_sigpending_body<OSDefs::L_sigset>(LSE_LINUX_ARGS,"rt_sigpending");
}

template<class T> static inline int 
	  rt_sigprocmask_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int how, sz;
  LSE_emu_addr_t set, oldset;
  T tempbuf;
  int ret=0;

  how    = CC_arg_int(0,"ippi");
  set    = CC_arg_ptr(1,"ippi");
  oldset = CC_arg_ptr(2,"ippi");
  sz     = CC_arg_int(3,"ippi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
          "SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format ",0x" 
                                 LSE_emu_addr_t_print_format ",%d) = ",
	    osct->tid, name, how, set, oldset, sz);

  if (sz != sizeof(OSDefs::L_sigset)) OS_report_err(OSDefs::oEINVAL);

  if (how != OSDefs::oSIG_BLOCK && how != OSDefs::oSIG_UNBLOCK &&
      how != OSDefs::oSIG_SETMASK)
    OS_report_err(OSDefs::oEINVAL);

  if (oldset) { 
    tempbuf.fromword(osct->sigblocked);
    OS_memcpy_h2t(oldset, &tempbuf, sizeof(tempbuf));
  }

  if (set) {
    OS_memcpy_t2h(&tempbuf, set, sizeof(tempbuf));
    uint64_t nv = tempbuf.toword();
    nv &= ~Linux_sighandinf_t::cantChangeMask;

    if (how == OSDefs::oSIG_BLOCK) {
      osct->sigblocked |= nv;
    } else if (how == OSDefs::oSIG_UNBLOCK) {
      osct->sigblocked &= ~nv;
    } else {
      osct->sigblocked = nv;
    }
    osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
  		             (osct->tgroup->sigpending & ~osct->sigblocked);
  }

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(rt_sigprocmask) {
  return rt_sigprocmask_body<OSDefs::L_sigset>(LSE_LINUX_ARGS,"rt_sigprocmask");
}

template<class T> static inline int 
	  rt_sigqueueinfo_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int pid, sig;
  LSE_emu_addr_t infop;
  T tempbuf;
  Linux_siginfo_t si;
  int ret=0;
  Linux_context_t *tct;
  Linux_tgroup_t *tgrp;

  pid    = CC_arg_int(0,"iip");
  sig    = CC_arg_int(1,"iip");
  infop  = CC_arg_ptr(2,"iip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
          "SYSCALL(%d): %s(%d,%d,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, pid, sig, infop);

  if (!infop) OS_report_err(OSDefs::oEFAULT);
  OS_memcpy_t2h(&tempbuf, infop, sizeof(tempbuf));
  si = tempbuf.tosiginfo();

  if (si.LSEsi_code >= 0) OS_report_err(OSDefs::oEPERM);
  si.LSEsi_signo = sig;

  if (pid == -1) OS_report_err(OSDefs::oENOSYS);
  if (pid <= 0) OS_report_err(OSDefs::oESRCH);

  // find the target thread group's leader
  tgrp = osct->os->tgroups[pid];
  if (!tgrp) {
    osct->os->tgroups.erase(pid);
    OS_report_err(OSDefs::oESRCH);
  }

  tct = tgrp->members.front();
  
  if (sig < 0 || sig > 64) OS_report_err(OSDefs::oEINVAL);
  
  // TODO: check the permissions.
  //   user sending the signal and
  //     sender EUID/UID ! target UID/SUID and
  //     (sig != SIGCONT || different session): EPERM
  //   

  if (sig != 0) 
    ret = send_signal_to(osct, tct, sig, true, si); 

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(rt_sigqueueinfo) {
  return rt_sigqueueinfo_body<OSDefs::L_siginfo>
    (LSE_LINUX_ARGS,"rt_sigqueueinfo");
}

static int rt_sigreturn_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;
  uint64_t sigmask=0;

  // NOTE: I really should send out a SIGSEGV when memory goes bad, but
  // I do not really feel like it.

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "SYSCALL(%d): %s() = ", osct->tid, name);

  // I suppose if the stack pointer got fried this system call will freak
  // out, but that is OK ... we only run "correct" programs

  delete osct->restart;
  osct->restart = 0;

  CC_sigreturn(callno);

  sigmask &= ~Linux_sighandinf_t::cantChangeMask;
  osct->sigblocked = sigmask;
  osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
  		         (osct->tgroup->sigpending & ~osct->sigblocked);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();  
}

static LSE_LINUX_SIG(rt_sigreturn) {
  return rt_sigreturn_body(LSE_LINUX_ARGS,"rt_sigreturn");
}

template<class T> static inline int 
	  rt_sigsuspend_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int sz;
  LSE_emu_addr_t setp;
  T tempbuf;
  int ret=0;
  uint64_t nv, oldblocked;

  setp = CC_arg_ptr(0,"pi");
  sz   = CC_arg_int(1,"pi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
        "SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format ",%d) = ",
	    osct->tid, name, setp, sz);

  if (sz != sizeof(OSDefs::L_sigset))
    OS_report_err(OSDefs::oEINVAL);

  OS_memcpy_t2h(&tempbuf, setp, sizeof(tempbuf));
  nv = tempbuf.toword() & ~Linux_sighandinf_t::cantChangeMask;

  // do not allow kill or stop to be masked..
  nv &= ~( (uint64_t(1)<<8) | (uint64_t(1)<<18) );

  osct->oldsigblocked = osct->sigblocked;
  osct->sigblocked = nv;
  osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
  		         (osct->tgroup->sigpending & ~osct->sigblocked);
 
  osct->continuationStack.push_back(new cont_ssuspend(osct));
  block_thread(osct, true);
  
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "blocked\n");
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(rt_sigsuspend) {
  return rt_sigsuspend_body<OSDefs::L_sigset>(LSE_LINUX_ARGS,"rt_sigsuspend");
}

template<class SSET, class TSPEC, class SIGINFO> static inline int 
	  rt_sigtimedwait_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int sz;
  LSE_emu_addr_t setp, infop, utsp;
  SSET tempbuf;
  TSPEC temptbuf;
  SIGINFO tempsibuf;
  int ret=0;
  uint64_t nv, oldblocked;
  uint64_t savemask;
  Linux_siginfo_t si;
  bool foundsig;

  setp = CC_arg_ptr(0,"pppi");
  infop = CC_arg_ptr(1,"pppi");
  utsp = CC_arg_ptr(2,"pppi");
  sz   = CC_arg_int(3,"pppi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format 
	    "," LSE_emu_addr_t_print_format
	    "," LSE_emu_addr_t_print_format
	    ",%d) = ",
	    osct->tid, name, setp, infop, utsp, sz);

  if (sz != sizeof(OSDefs::L_sigset))
    OS_report_err(OSDefs::oEINVAL);

  OS_memcpy_t2h(&tempbuf, setp, sizeof(tempbuf));
  nv = tempbuf.toword() & ~Linux_sighandinf_t::cantChangeMask;

  if (utsp) { 
    OS_memcpy_t2h(&temptbuf,utsp,sizeof(temptbuf)); /* will report error */
    temptbuf = temptbuf.e2h();
  }

  // Cannot wait for kill or stop
  nv &= ~( (uint64_t(1)<<8) | (uint64_t(1)<<18) );
  nv = ~nv;  // signals we want to wait for are those we do not want to block

  foundsig = try_to_get_signal(nv, osct->sigpending, 
				     osct->signals, si) ||
    try_to_get_signal(nv, osct->tgroup->sigpending,
		      osct->tgroup->signals, si);
  
  if (!foundsig) {
    savemask = osct->sigblocked;
    osct->sigblocked &= nv;
    osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
      (osct->tgroup->sigpending & ~osct->sigblocked);

    Linux_wu_timer_t *nt = 0;
    if (utsp) {
      nt = new Linux_wu_timer_t(osct->os, 0, osct);
      Linux_timer_t::itval v;
      v.interval = 0;
      v.expiration = (uint64_t)temptbuf.LSE_tv_sec * 1000000000 
	+ temptbuf.LSE_tv_nsec;
      nt->settime(&v, 0);
    }
    osct->continuationStack.push_back(new cont_stimedwait<SIGINFO>
				      (osct, infop, nv, savemask, nt));
    block_thread(osct, true);
    ret = -2;
  } else {
    osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
      (osct->tgroup->sigpending & ~osct->sigblocked);

    ret = si.LSEsi_signo;
    if (infop) {
      tempsibuf = SIGINFO(si);
      OS_memcpy_h2t(infop, &tempsibuf, sizeof(tempsibuf));
    }
    OS_report_results(0,ret);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    if (ret == -2) fprintf(stderr, "blocked\n");
    else fprintf(stderr, "%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

template<>
const int cont_stimedwait<OSDefs::L_siginfo>::typeno = 0;

static LSE_LINUX_SIG(rt_sigtimedwait) {
  return rt_sigtimedwait_body<OSDefs::L_sigset, OSDefs::L_timespec,
                              OSDefs::L_siginfo>
    (LSE_LINUX_ARGS,"rt_sigtimedwait");
}

LSE_LINUX_UNIMPLEMENTED(rt_tgsigqueueinfo)
LSE_LINUX_UNIMPLEMENTED(sched_get_affinity)

static LSE_LINUX_SIG(sched_getaffinity) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  // just a dummy implementation
  OSDefs::L_pid_t pid;
  unsigned int ulen;
  LSE_emu_addr_t umaskbuf;
  
  int ret=0;

  pid      = (OSDefs::L_pid_t)CC_arg_int(0,"iip");
  ulen     = (unsigned int)CC_arg_int(1,"iip");
  umaskbuf = CC_arg_ptr(2,"iip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
	    "SYSCALL(%d): sched_getaffinity(%d,%u,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, (int)pid, ulen, umaskbuf);

  int numhw, hwsizebytes, hwsizelongs, masksize;
  numhw = count_hwcontexts(&CC_emu_interface(realct));
  hwsizebytes = (numhw + 7)/8;
  hwsizelongs = ((hwsizebytes + sizeof(OSDefs::L_long)- 1)/ 
                     sizeof(OSDefs::L_long));
  masksize = hwsizelongs * sizeof(OSDefs::L_long);

  if (ulen < masksize) OS_report_err(OSDefs::oEINVAL);

  {
    OSDefs::L_long mval[hwsizelongs];
    // I am not going to keep real masks around!
    for (int i=0 ; i < hwsizelongs ; ++i) mval[i] = 0;
    ret = masksize;
    OS_memcpy_h2t(umaskbuf,mval,masksize); /* will report error */
  }

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(sched_getparam) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  // Just a dummy implementation so linuxthreads does not choke
  int rval = 0;
  int pid;
  LSE_emu_addr_t parms;
  Linux_context_t *tct;

  pid   = CC_arg_int(0,"ip");
  parms = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): sched_getparam(%d,0x"LSE_emu_addr_t_print_format") = ",
            osct->tid, pid, parms);

  // TODO: permissions?

  if (!pid) pid = osct->tid;
  tct = osct->os->threads[pid];
  if (!tct) {
    osct->os->threads.erase(pid);
    OS_report_err(OSDefs::oESRCH);
  }

  OS_memcpy_h2t(parms, &osct->schedParms, sizeof(osct->schedParms));

  OS_report_results(0,rval);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(sched_get_priority_max)
LSE_LINUX_UNIMPLEMENTED(sched_get_priority_min)

static LSE_LINUX_SIG(sched_getscheduler) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  // Just a dummy implementation so linuxthreads does not choke
  int rval = 0;
  int pid;

  pid = CC_arg_int(0, "i");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): sched_getcheduler(%d) = ",
            osct->tid, pid);

  // TODO: permissions?

  if (!pid) pid = osct->tid;
  {
    Linux_context_t *tct = osct->os->threads[pid];
    if (!tct) {
      osct->os->threads.erase(pid);
      OS_report_err(OSDefs::oESRCH);
    }
    rval = tct->schedPolicy;
  }

  OS_report_results(0,rval);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(sched_rr_get_interval)
LSE_LINUX_UNIMPLEMENTED(sched_set_affinity)

static LSE_LINUX_SIG(sched_setaffinity) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  // just a dummy implementation
  OSDefs::L_pid_t pid;
  unsigned int ulen;
  LSE_emu_addr_t umaskbuf;
  
  int ret=0;

  pid      = (OSDefs::L_pid_t)CC_arg_int(0,"iip");
  ulen     = (unsigned int)CC_arg_int(1,"iip");
  umaskbuf = CC_arg_ptr(2,"iip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
	    "SYSCALL(%d): sched_setaffinity(%d,%u,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, (int)pid, ulen, umaskbuf);

  int numhw, hwsizebytes, hwsizelongs, masksize;
  numhw = count_hwcontexts(&CC_emu_interface(realct));
  hwsizebytes = (numhw + 7)/8;
  hwsizelongs = ((hwsizebytes + sizeof(OSDefs::L_long)- 1)/ 
                     sizeof(OSDefs::L_long));
  masksize = hwsizelongs * sizeof(OSDefs::L_long);

  if (ulen < masksize) OS_report_err(OSDefs::oEINVAL);

  {
    OSDefs::L_long mval[hwsizelongs];
    // I am not going to keep real masks around!
     for (int i=0 ; i < hwsizelongs ; ++i) mval[i] = 0;
    ret = 0;
    OS_memcpy_t2h(mval,umaskbuf,masksize); /* will report error */
  }

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(sched_setparam) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  // Just a dummy implementation so linuxthreads does not choke
  int rval = 0;
  int pid;
  LSE_emu_addr_t parms;
  Linux_context_t *tct;

  pid   = CC_arg_int(0, "ip");
  parms = CC_arg_ptr(1, "ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): sched_setparam(%d,0x"LSE_emu_addr_t_print_format") = ",
            osct->tid, pid, parms);

  // TODO: permissions?

  if (!pid) pid = osct->tid;
  tct = osct->os->threads[pid];
  if (!tct) {
    osct->os->threads.erase(pid);
    OS_report_err(OSDefs::oESRCH);
  }

  OS_memcpy_t2h(&tct->schedParms, parms, sizeof(tct->schedParms));

  OS_report_results(0,rval);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(sched_setscheduler) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  // Just a dummy implementation so linuxthreads does not choke
  int rval = 0;
  int pid, policy;
  LSE_emu_addr_t parms;

  pid    = CC_arg_int(0, "iip");
  policy = CC_arg_int(1, "iip");
  parms  = CC_arg_ptr(2, "iip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
     "SYSCALL(%d): sched_setscheduler(%d,%d,0x"LSE_emu_addr_t_print_format") = ",
            osct->tid, pid, policy, parms);

  // TODO: permissions?

  if (!pid) pid = osct->tid;
  {
    Linux_context_t *tct = osct->os->threads[pid];
    if (!tct) {
      osct->os->threads.erase(pid);
      OS_report_err(OSDefs::oESRCH);
    }
    tct->schedPolicy = policy;
    OS_memcpy_t2h(&tct->schedParms, parms, sizeof(tct->schedParms));
  }

  OS_report_results(0,rval);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(sched_yield) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int rval = 0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): sched_yield() = ",osct->tid);

  // Take off the processor, but then put on the ready list.
  block_thread(osct, true);
  ready_thread(osct, true);

  OS_report_results(0,rval);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(select)
LSE_LINUX_UNIMPLEMENTED(semctl)
LSE_LINUX_UNIMPLEMENTED(semget)
LSE_LINUX_UNIMPLEMENTED(semop)
LSE_LINUX_UNIMPLEMENTED(semtimedop)
LSE_LINUX_UNIMPLEMENTED(send)
LSE_LINUX_UNIMPLEMENTED(sendfile)
LSE_LINUX_UNIMPLEMENTED(sendfile64)
LSE_LINUX_UNIMPLEMENTED(sendmsg)
LSE_LINUX_UNIMPLEMENTED(sendto)
LSE_LINUX_UNIMPLEMENTED(setcontext)
LSE_LINUX_UNIMPLEMENTED(setdomainname)
LSE_LINUX_UNIMPLEMENTED(setfsgid)
LSE_LINUX_UNIMPLEMENTED(setfsuid)
LSE_LINUX_UNIMPLEMENTED(setgid)
LSE_LINUX_UNIMPLEMENTED(setgid16)
LSE_LINUX_UNIMPLEMENTED(setgroups)
LSE_LINUX_UNIMPLEMENTED(setgroups16)
LSE_LINUX_UNIMPLEMENTED(sethostname)

static int setitimer_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;
  ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
            "SYSCALL(%d): %s() = ", osct->tid, name);

  // we seem to be ignoring the parameters for now!
  // NOTE: also needs a 32 to 64 conversion check

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(osct->myerrno());
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "%d\n", ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(setitimer) {
  return setitimer_body(LSE_LINUX_ARGS, "setitimer");
}

LSE_LINUX_UNIMPLEMENTED(set_mempolicy)

static LSE_LINUX_SIG(setpgid) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int pid, pgid, newpgid;
  int ret=0;
  Linux_context_t *tct;
  Linux_tgroup_t *tgrp;

  pid  = sextend32(changelen, CC_arg_int(0,"ii"));
  pgid = sextend32(changelen, CC_arg_int(1,"ii"));

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): setpgid(%d,%d) = ",
            osct->tid, pid, pgid);

  if (pid == 0) pid = osct->tgroup->tgid;
  if (pgid < 0) OS_report_err(OSDefs::oEINVAL);
  if (pid < 0) OS_report_err(OSDefs::oEINVAL);

  tgrp = osct->os->tgroups[pid];
  if (!tgrp) {
    osct->os->tgroups.erase(pid);
    OS_report_err(OSDefs::oESRCH);
  }
  tct = tgrp->members.front();

  // TODO: permission checks are really odd here. 
  // if target is not its thread group leader, EINVAL (Don't see how possible)
  // else if parent of target is in same thread group as current:
  //   if not in same session, EPERM
  //   else if target did an exec, EACCES
  // else if target is not the current thread group leader, ESRCH
  // else if target is a session leader, EPERM
  // make sure we have a thread group with a leader (don't they always have
  // one?) and make sure we are sending to a child

  if (tgrp != osct->tgroup) OS_report_err(OSDefs::oEINVAL);

  // TODO: permission check needs to look at whether an exec was done and
  // return EACCES

  if (tct->parent->tgroup != osct->tgroup && 
      tct != osct->tgroup->members.front()) 
    OS_report_err(OSDefs::oESRCH);

  // check whether new pgid works...
  newpgid = pgid ? pgid : pid;

  if (newpgid != pid) {
    Linux_pgroup_t *newpg = osct->os->pgroups[newpgid];
    if (!newpg) {
      osct->os->pgroups.erase(newpgid);
      OS_report_err(OSDefs::oEPERM);
    }
  }

  if (tgrp->pgroup->pgid != newpgid) { 
    tgrp->pgroup->members.remove(tct->tgroup);
    tgrp->pgroup->decr();
    Linux_pgroup_t *newpg = osct->os->pgroups[newpgid];
    if (!newpg) {
      tgrp->pgroup = new Linux_pgroup_t(tct->parent->os, newpgid);
      tgrp->pgroup->members.push_back(tgrp);
    } else { 
      tgrp->pgroup = newpg;
      newpg->members.push_back(tgrp);
      newpg->incr();
    }
  }

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(setpriority) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int which;
  int who;
  int prio;
  int ret=0;

  which = sextend32(changelen, CC_arg_int(0,"iii"));
  who = sextend32(changelen, CC_arg_int(1,"iii"));
  prio = sextend32(changelen, CC_arg_int(2,"iii"));

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "SYSCALL(%d): setpriority(%d,%d,%d) = ",
            osct->tid, which, who, prio);

  /* This syscall just sets a priority.  Hopefully, having an empty
     definition is safe */ 
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);

  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(setregid16)
LSE_LINUX_UNIMPLEMENTED(setregid)
LSE_LINUX_UNIMPLEMENTED(setresgid)
LSE_LINUX_UNIMPLEMENTED(setresuid)
LSE_LINUX_UNIMPLEMENTED(setreuid)
LSE_LINUX_UNIMPLEMENTED(setreuid16)

template<class T> static inline int 
	  setrlimit_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int resource;
  LSE_emu_addr_t targrlim;
  struct rlimit rlim;
  T temprlim;
  int ret=0;

  resource = CC_arg_int(0,"ip");
  targrlim = CC_arg_ptr(1,"ip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format 
            ") = ",
	    osct->tid, name, resource, targrlim);

  if (resource < 0 || resource >= OSDefs::oRLIMIT_NLIMITS)
    OS_report_err(OSDefs::oEINVAL);

  OS_memcpy_t2h(&temprlim,targrlim,sizeof(temprlim)); /* will report error */

  osct->tgroup->rlimits[resource][0] = LSE_e2h(temprlim.LSE_rlim_cur,
					       Linux_bigendian);
  osct->tgroup->rlimits[resource][1] = LSE_e2h(temprlim.LSE_rlim_max,
					       Linux_bigendian);

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(setrlimit) {
  return setrlimit_body<OSDefs::L_rlimit>(LSE_LINUX_ARGS,"setrlimit");
}

template<class T> static inline int 
set_robust_list_body(LSE_LINUX_PARMS, const char *name, bool iscompat=false) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;

  int sz;
  LSE_emu_addr_t lst;
  int ret=0;

  lst = CC_arg_ptr(0, "pi");
  sz  = CC_arg_int(1, "pi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
          "SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format",%d) = ",
	    osct->tid, name, lst, sz);

  if (sz != sizeof(T)) OS_report_err(OSDefs::oEINVAL);

#ifdef LSE_LINUX_COMPAT
  if (iscompat) osct->robustFutexListCompat = lst;
  else osct->robustFutexList = lst;
#else
  osct->robustFutexList = lst;
#endif

  // TODO: do I need to walk it?

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
}

static LSE_LINUX_SIG(set_robust_list) {
  return set_robust_list_body<OSDefs::L_robust_list_head>(LSE_LINUX_ARGS,
							  "set_robust_list");
}

LSE_LINUX_UNIMPLEMENTED(setsid)
LSE_LINUX_UNIMPLEMENTED(setsockopt)

static LSE_LINUX_SIG(set_tid_address) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int rval = 0;
  LSE_emu_addr_t tidp;

  tidp = CC_arg_ptr(0, "p");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
     "SYSCALL(%d): set_tid_address(0x"LSE_emu_addr_t_print_format") = ",
            osct->tid, tidp);

  osct->clear_child_tid = tidp;
  rval = osct->tid;

  OS_report_results(0,rval);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(settimeofday)
LSE_LINUX_UNIMPLEMENTED(setuid)
LSE_LINUX_UNIMPLEMENTED(setuid16)
LSE_LINUX_UNIMPLEMENTED(setxattr)
LSE_LINUX_UNIMPLEMENTED(sgetmask)
LSE_LINUX_UNIMPLEMENTED(shmat)
LSE_LINUX_UNIMPLEMENTED(shmctl)
LSE_LINUX_UNIMPLEMENTED(shmdt)
LSE_LINUX_UNIMPLEMENTED(shmget)
LSE_LINUX_UNIMPLEMENTED(shutdown)
LSE_LINUX_UNIMPLEMENTED(sigaction)

static LSE_LINUX_SIG(sigaltstack) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t newstack, oldstack, realsp;
  int ret=0;

  newstack = CC_arg_ptr(0,"pp");
  oldstack = CC_arg_ptr(1,"pp");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): sigaltstack(0x" LSE_emu_addr_t_print_format ",0x"
                               LSE_emu_addr_t_print_format ") = ",
	    osct->tid, newstack, oldstack);

  ret = handle_sigaltstack(realct, osct, newstack, oldstack, 
			   CC_get_stackptr(realct));

  OS_report_results(0,ret);

  if (ret < 0) {
    OS_report_err(-ret);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(signal)
LSE_LINUX_UNIMPLEMENTED(signalfd)
LSE_LINUX_UNIMPLEMENTED(signalfd4)
LSE_LINUX_UNIMPLEMENTED(sigreturn)
LSE_LINUX_UNIMPLEMENTED(sigsuspend)

LSE_LINUX_UNIMPLEMENTED(socket)
#ifdef NOTYET
static LSE_LINUX_SIG(socket) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int ret=0;
  int domain, type, protocol;

  domain   = CC_arg_int(0,"iii");
  type     = CC_arg_int(1,"iii");
  protocol = CC_arg_int(2,"iii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): socket(%d,%d,%d) = ",
            osct->tid, domain,type,protocol);

  CC_chkpt_start(callno); 
  CC_chkpt_arg_int(domain);
  CC_chkpt_arg_int(type);
  CC_chkpt_arg_int(protocol);
  
  CC_chkpt_guard(ret=socket(domain,type,protocol);
		 osct->myerrno() = errno;)
  CC_chkpt_result_int(ret);

  if (ret != -1) {
    if (CC_chkpt_guard_true)
      ret = osct->fdtable->addToTable(new Linux_socket_file_t(osct->os,ret));
    else
      ret = osct->fdtable->addToTable(new Linux_socket_file_t(osct->os, -1));
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n",
	    (int)OS_sys_get_return(1));
  CC_chkpt_finish();
  OS_syserr_handler();
}
#endif

LSE_LINUX_UNIMPLEMENTED(socketcall)
LSE_LINUX_UNIMPLEMENTED(socketpair)
LSE_LINUX_UNIMPLEMENTED(sparc64_personality)
LSE_LINUX_UNIMPLEMENTED(splice)
LSE_LINUX_UNIMPLEMENTED(spu_create)
LSE_LINUX_UNIMPLEMENTED(spu_run)
LSE_LINUX_UNIMPLEMENTED(ssetmask)

static LSE_LINUX_SIG(stat64) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  LSE_emu_addr_t targbuf;
  LSE_stat64 buf;
  struct OSDefs::L_stat64 tempbuf;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  LSE_emu_addr_t fname;
  int didfname = 0;
  int ret=0;

  fname   = CC_arg_ptr(0,"pp");
  targbuf = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): stat64(%s,0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, fnamebuf,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_str(fnamebuf,strlen(fnamebuf)+1);

  rfname = resolve_path(fnamebuf, osct);
#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(ret = stat64(rfname, &buf);
		 osct->myerrno() = errno;);
#else
  CC_chkpt_guard(ret = stat(rfname, &buf);
		 osct->myerrno() = errno;);
#endif
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  /* 
   * note that these values will depend upon the HOST system
   */

  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): stat64(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, fname, targbuf);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler()
}

template<class T> static inline int 
statfs_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t fname, targbuf;
#if defined (__SVR4) && defined (__sun)
  struct statvfs buf;
#else
  struct statfs buf;
#endif
  T tempbuf;
  int ret=0;
  char *rfname,fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didfname = 0;

  fname   = CC_arg_ptr(0,"pp");
  targbuf = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(\"%s\",0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, (char *)fnamebuf,targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_str(fnamebuf,strlen(fnamebuf)+1);

  rfname = resolve_path(fnamebuf, osct);
#if defined (__SVR4) && defined (__sun)
  CC_chkpt_guard(ret = statvfs(rfname, &buf);
		 osct->myerrno() = errno;);
#else
  CC_chkpt_guard(ret = statfs(rfname, &buf);
		 osct->myerrno() = errno;);
#endif
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  /* 
   * note that these values will depend upon the HOST system
   */
  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(stderr,
              "SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              osct->tid, name, fname, targbuf);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

static LSE_LINUX_SIG(statfs) {
  return statfs_body<OSDefs::L_statfs>(LSE_LINUX_ARGS,"statfs");
}

LSE_LINUX_UNIMPLEMENTED(statfs64)
LSE_LINUX_UNIMPLEMENTED(stime)
LSE_LINUX_UNIMPLEMENTED(subpage_prot)
LSE_LINUX_UNIMPLEMENTED(swapcontext)
LSE_LINUX_UNIMPLEMENTED(swapoff)
LSE_LINUX_UNIMPLEMENTED(swapon)
LSE_LINUX_UNIMPLEMENTED(symlink)
LSE_LINUX_UNIMPLEMENTED(symlinkat)
LSE_LINUX_UNIMPLEMENTED(sync)
LSE_LINUX_UNIMPLEMENTED(sync_file_range2)
LSE_LINUX_UNIMPLEMENTED(sysfs)
LSE_LINUX_UNIMPLEMENTED(sysinfo)
LSE_LINUX_UNIMPLEMENTED(syslog)
LSE_LINUX_UNIMPLEMENTED(tee)

static LSE_LINUX_SIG(tgkill) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int pid, tgid;
  int sig;
  int ret=0;

  tgid = CC_arg_int(0,"iii");
  pid  = CC_arg_int(1,"iii");
  sig  = CC_arg_int(2,"iii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): tgkill(%d,%d,%d) = ",
	    osct->tid, tgid, pid, sig);

  if (tgid <= 0 || pid <= 0) OS_report_err(OSDefs::oEINVAL);
  else { // send to a process
    // find the target thread
    Linux_context_t *tct = osct->os->threads[pid];
    if (!tct) {
      osct->os->threads.erase(pid);
      OS_report_err(OSDefs::oESRCH);
    }
    if (tct->tgroup->tgid != tgid) OS_report_err(OSDefs::oESRCH);
  
    if (sig < 0 || sig > 64) OS_report_err(OSDefs::oEINVAL);

    // IGNORED: checking the permissions

    if (sig == 0) goto done; // do not actually send it.

    // append to the queue
    Linux_siginfo_t nv;
    nv.LSEsi_signo = sig;
    nv.LSEsi_errno = 0;
    nv.LSEsi_code = OSDefs::oSI_TKILL;
    nv.LSEsi_pid = osct->tgroup->tgid;
    nv.LSEsi_uid = osct->uid;
    ret = send_signal_to(osct, tct, sig, false, nv);
  }

done:
  if (ret == -1) {
    OS_report_err(osct->myerrno());
  }
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

template<class T> static inline int 
time_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t targbuf;
  T buf, ret;
  uint64_t ctime;

  targbuf = CC_arg_ptr(0,"p");
  ctime = CC_time_get_currtime(osct->os);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, targbuf);

  ctime += osct->os->boottime;

  buf = ret = ctime / 1000000000;
  if (targbuf != 0) {
    buf = LSE_h2e(buf, Linux_bigendian);
    OS_memcpy_h2t(targbuf,&buf,sizeof(buf)); /* will report error */
  }

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(time) {
  return time_body<OSDefs::L_time_t>(LSE_LINUX_ARGS,"time");
}

LSE_LINUX_UNIMPLEMENTED(timer_create)
LSE_LINUX_UNIMPLEMENTED(timer_delete)
LSE_LINUX_UNIMPLEMENTED(timerfd_create)
LSE_LINUX_UNIMPLEMENTED(timerfd_gettime)
LSE_LINUX_UNIMPLEMENTED(timerfd_settime)
LSE_LINUX_UNIMPLEMENTED(timer_getoverrun)
LSE_LINUX_UNIMPLEMENTED(timer_gettime)
LSE_LINUX_UNIMPLEMENTED(timer_settime)

template<class T> static inline int 
times_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t targbuf;
  T tempbuf;
  int ret=0;

  targbuf = CC_arg_ptr(0,"p");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, targbuf);

  memset(&tempbuf,0,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(times) {
  return times_body<OSDefs::L_tms>(LSE_LINUX_ARGS,"times");
}

static LSE_LINUX_SIG(tkill) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int pid, sig;
  int ret=0;

  pid = CC_arg_int(0,"ii");
  sig = CC_arg_int(1,"ii");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): tkill(%i,%i) = ",
	    osct->tid, pid,sig);

  if (pid <= 0) OS_report_err(OSDefs::oEINVAL);
  else { // send to a process
    // find the target thread
    Linux_context_t *tct = osct->os->threads[pid];
    if (!tct) {
      osct->os->threads.erase(pid);
      OS_report_err(OSDefs::oESRCH);
    }

    if (sig < 0 || sig > 64) OS_report_err(OSDefs::oEINVAL);

    // IGNORED: checking the permissions

    if (sig == 0) goto done; // do not actually send it.

    // append to the queue
    Linux_siginfo_t nv;
    nv.LSEsi_signo = sig;
    nv.LSEsi_errno = 0;
    nv.LSEsi_code = OSDefs::oSI_TKILL;
    nv.LSEsi_pid = osct->tgroup->tgid;
    nv.LSEsi_uid = osct->uid;
    ret = send_signal_to(osct, tct, sig, false, nv);
  }

done:
  if (ret == -1) {
    OS_report_err(osct->myerrno());
  }
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(truncate)
LSE_LINUX_UNIMPLEMENTED(truncate64)

static LSE_LINUX_SIG(umask) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  mode_t mask;
  int ret=0;

  mask = (mode_t) CC_arg_int(0,"i");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): umask(%d) = ", osct->tid, mask);

  ret = osct->fs->umask;
  osct->fs->umask = mask & 0777;

  OS_report_results(0,ret);
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  OS_sys_return();
}

LSE_LINUX_UNIMPLEMENTED(umount)

static int uname_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  static struct localutsname {
    char sysname[OSDefs::oSYS_NMLN];
    char nodename[OSDefs::oSYS_NMLN];
    char release[OSDefs::oSYS_NMLN];
    char version[OSDefs::oSYS_NMLN];
    char machine[OSDefs::oSYS_NMLN];
    char domainname[OSDefs::oSYS_NMLN];
    localutsname() {
      memset(this,0,sizeof(this));
      strncpy(sysname,    OSDefs::oSYSNAME,    OSDefs::oSYS_NMLN);
      strncpy(nodename,   OSDefs::oNODENAME,   OSDefs::oSYS_NMLN);
      strncpy(release,    OSDefs::oRELEASE,    OSDefs::oSYS_NMLN);
      strncpy(version,    OSDefs::oVERSION,    OSDefs::oSYS_NMLN);
      strncpy(machine,    OSDefs::oMACHINE,    OSDefs::oSYS_NMLN);
      strncpy(domainname, OSDefs::oDOMAINNAME, OSDefs::oSYS_NMLN);
    }
  } hardcodedname;

  LSE_emu_addr_t buf = CC_arg_ptr(0,"p");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, buf);

  OS_memcpy_h2t(buf,&hardcodedname,sizeof(hardcodedname));

  OS_report_results(0,0);
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"0\n");
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
}

static LSE_LINUX_SIG(uname) {
  return uname_body(LSE_LINUX_ARGS,"uname");
}

static LSE_LINUX_SIG(unlink) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t path;
  int ret=0;
  char *rfname,pathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  int didpath = 0;

  path = CC_arg_ptr(0,"p");

  OS_strncpy_t2h(pathbuf,path,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  pathbuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2] = 0;
  didpath = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): unlink(\"%s\") = ", osct->tid, pathbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(path);
  CC_chkpt_arg_str(pathbuf,strlen(pathbuf)+1);
  
  rfname = resolve_path(pathbuf, osct);
  CC_chkpt_guard(ret=unlink(rfname);
		 osct->myerrno() = errno;);
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(stderr,"%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didpath) {
      fprintf(stderr,
              "SYSCALL(%d): unlink(0x" LSE_emu_addr_t_print_format ") = ",
              osct->tid, path);
    }
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

LSE_LINUX_UNIMPLEMENTED(unlinkat)
LSE_LINUX_UNIMPLEMENTED(unshare)
LSE_LINUX_UNIMPLEMENTED(uselib)
LSE_LINUX_UNIMPLEMENTED(ustat)
LSE_LINUX_UNIMPLEMENTED(utime)
LSE_LINUX_UNIMPLEMENTED(utimensat)

template<class T> static inline int 
	  utimes_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t targbuf;
  struct timeval buf;
  T tempbuf;
  char *rfname, fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+3];
  LSE_emu_addr_t fname;

  int ret=0;

  fname   = CC_arg_ptr(0,"pp");
  targbuf = CC_arg_ptr(1,"pp");

  OS_strncpy_t2h(fnamebuf,fname,OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2);
  fnamebuf[OSDefs::oPATH_MAX+OSDefs::oNAME_MAX+2]=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): %s(0x" LSE_emu_addr_t_print_format ") = ",
	    osct->tid, name, targbuf);

  CC_chkpt_start(callno);
  CC_chkpt_arg_ptr(fname);
  CC_chkpt_arg_str(fnamebuf,(strlen(fnamebuf)+1));
  CC_chkpt_arg_ptr(targbuf);

  rfname = resolve_path(fnamebuf, osct);
  CC_chkpt_guard(ret = utimes(rfname,&buf);
		 osct->myerrno() = errno;);
  free(rfname);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  }

  CC_chkpt_guard(tempbuf = buf;)
  CC_chkpt_arg_int(sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result_hmem(&tempbuf,sizeof(tempbuf));
  OS_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  CC_chkpt_finish();
  OS_syserr_handler()
}

static LSE_LINUX_SIG(utimes) {
  return utimes_body<OSDefs::L_timeval>(LSE_LINUX_ARGS,"utimes");
}

LSE_LINUX_UNIMPLEMENTED(utrap_install)

static LSE_LINUX_SIG(vfork) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  CC_isacontext_t *newct;
  int rval = 0;
  bool automap;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"SYSCALL(%d): vfork() = ", osct->tid);

  rval=Linux_cloneguts(&newct,realct,
	       OSDefs::oSIGCHLD|OSDefs::oCLONE_VM|OSDefs::oCLONE_VFORK,
		       0, 0, true, CC_ict_startaddr(realct), 0,0,0);

  if (rval) {
    OS_report_err(ENOMEM);
  } else {
    // block parent, but run child
    block_thread(osct, true);
    osct->continuationStack.push_back(new cont_vfork(osct));
    OS_ict_osinfo(newct)->reportVforkDone.add(osct);
    ready_thread(OS_ict_osinfo(newct), true);
    OS_report_results(0,(OS_ict_osinfo(newct))->tid);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1)); 
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(vhangup)
LSE_LINUX_UNIMPLEMENTED(vmsplice)

// WAIT4:
//  Actual kernel behavior on rescheduling is to loop back into the middle
//  and try again.  However, after a signal, the instruction is just rerun.
//  We will go ahead with the rerun in all cases so that we can more
//  easily trace the syscall.

static inline bool wait4_piece(LSE_LINUX_PARMS, Linux_context_t *osct,
			       int& osret, int& pid, int& options,
			       LSE_emu_addr_t &stataddr,
			       LSE_emu_addr_t &infop,
			       LSE_emu_addr_t &ru, int&ret,
			       bool iscompat) {
   // tricky... we have to wait for all the children of all the threads
   // in our thread group unless __WNOTHREAD is set.  Yuck.
   int tid = 0;
   ret = 0;
   bool haschild = false;

   for (std::list<Linux_context_t *>::iterator 
        j = osct->tgroup->members.begin(), je = osct->tgroup->members.end();
	j != je ; ++j) {

     if ((options & OSDefs::o__WNOTHREAD) && *j != osct) continue;
 
     for (std::list<Linux_context_t *>::iterator i = (*j)->children.begin(),
          ie = (*j)->children.end(); i != ie; ++i) {
 
       if ((*i)->signalOnExit == -1) continue; // detached

       if (!(pid == -1 ||
           pid < 0 && (*i)->tgroup->pgroup->pgid == -pid ||
	   pid == 0 && (*i)->tgroup->pgroup == osct->tgroup->pgroup ||
	   pid > 0 && (*i)->tgroup->tgid == pid)) continue; // not a match

       // who to wait for?
       // __WALL: everyone
       // __CLONE: only clone children
       // !__CLONE: only non-clone children.
       if (!((options & OSDefs::o__WALL) ||
             ( (options & OSDefs::o__WCLONE) 
	       && (*i)->signalOnExit != OSDefs::oSIGCHLD) ||
	     ( !(options & OSDefs::o__WCLONE)
	       && (*i)->signalOnExit == OSDefs::oSIGCHLD))) continue;

       haschild = true;

       if ((*i)->state != ProcZombie) continue;

       // if the task is a group leader which has subthreads, it should not be 
       // reaped, because the *process* has not yet terminated.  However,
       // tasks which are not group leaders can be reaped.  Very odd..
       if ((*i)->tgroup->members.front() == (*i) && 
	   (*i)->tgroup->members.size() > 1)
	 continue;

       // reap the child now.
       tid = (*i)->tid;
         
       int status;
       status = (*i)->tgroup->exitcodeValid ? (*i)->tgroup->exitcode 
	 : (*i)->exit_code;

       if (stataddr) { // store the status
         int ec = LSE_h2e(status, Linux_bigendian);
         OS_mem_write(realct, stataddr, 4, &ec);
       }
       if (infop) { 
	 Linux_siginfo_t nv;
	 nv.LSEsi_signo = OSDefs::oSIGCHLD;
	 nv.LSEsi_errno = 0;
	 nv.LSEsi_code = ( ((status & 0x7f) == 0) ? 1 :
			(status & 0x80) ? 3 : 2 );
	 nv.LSEsi_status = ( ((status & 0x7f) == 0) ? (status >> 8) :
			  (status & 0x7f) );
	 nv.LSEsi_pid = pid;
	 nv.LSEsi_uid = osct->uid;
	 if (!iscompat) {
	   OSDefs::L_siginfo nsi(nv);
	   OS_memcpy_h2t(infop, &nsi, sizeof(nsi));
	 } else {
#ifdef LSE_LINUX_COMPAT 
	   OSDefs::L_compat_siginfo nsi(nv);
	   OS_memcpy_h2t(infop, &nsi, sizeof(nsi));
#endif
	 }
	 
       }
       // TODO: fill in ru

       // The WNOWAIT option says to leave the child around!
       if (!(options & WNOWAIT)) reap_zombie(*i);

       break;
     } // children
     if (tid) break;
   } // others in thread group

   if (!haschild) OS_report_err(OSDefs::oECHILD);

   if (tid) {
     ret = infop ? 0 : tid;
     OS_report_results(0, ret);
     osct->tgroup->childwait.remove(osct);
   } else if (!(options & OSDefs::oWNOHANG)) {
     if (osct->sigPendingFlag) {
       // try again after the signal is handled
       OS_report_err(OSDefs::oERESTARTSYS);
     } else {
       ret = -2;
     }
   } else { // WNOHANG return happening here
     if (infop) {
       Linux_siginfo_t nv;
       nv.LSEsi_signo = 0;
       nv.LSEsi_errno = 0;
       nv.LSEsi_code = 0;
       nv.LSEsi_pid = 0;
       nv.LSEsi_uid = 0;
       nv.LSEsi_status = 0;
       if (!iscompat) {
	 OSDefs::L_siginfo nsi(nv);
	 OS_memcpy_h2t(infop, &nsi, sizeof(nsi));
       } else {
#ifdef LSE_LINUX_COMPAT 
	 OSDefs::L_compat_siginfo nsi(nv);
	 OS_memcpy_h2t(infop, &nsi, sizeof(nsi));
#endif
       }
     }
     OS_report_results(0, 0);
   }
   return false;
 syserr:
   return true;
}

static inline int wait4_body(LSE_LINUX_PARMS, const char *name, bool iscompat) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int pid, options;
  LSE_emu_addr_t stataddr, ru, infop = 0;
  int ret=0;

  pid      = CC_arg_int(0,"ipip");
  stataddr = CC_arg_ptr(1,"ipip");
  options  = CC_arg_int(2,"ipip");
  ru       = CC_arg_ptr(3,"ipip");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): %s(%d,0x" LSE_emu_addr_t_print_format ",%u,0x" 
	    LSE_emu_addr_t_print_format ") = ", 
	    osct->tid, name, pid, stataddr, options, ru);

  if (wait4_piece(LSE_LINUX_ARGS, osct, osret, pid, options, stataddr, infop,
		  ru, ret, iscompat))
    goto syserr;

  if (ret == -2) {
    osct->tgroup->childwait.add(osct);
    osct->continuationStack.push_back(new cont_wait(osct,pid,options,
						    stataddr, infop,
						    ru, iscompat,
						    changelen));
    block_thread(osct, true);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    if (ret == -2) fprintf(stderr,"blocked\n");
    else fprintf(stderr,"%d\n",ret);
  
  OS_sys_return();
 syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(wait4) {
  return wait4_body(LSE_LINUX_ARGS,"wait4",false);
}

static inline int waitid_body(LSE_LINUX_PARMS, const char *name, 
			      bool iscompat) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int pid, options, kind;
  LSE_emu_addr_t stataddr=0, ru=0, infop;
  int ret=0;

  kind     = CC_arg_int(0,"iipi");
  pid      = CC_arg_int(1,"iipi");
  infop    = CC_arg_ptr(2,"iipi");
  options  = CC_arg_int(3,"iipi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
	    "SYSCALL(%d): %s(%d,%d,0x" LSE_emu_addr_t_print_format ",%u) = ", 
	    osct->tid, name, kind, pid, infop, options);

  if (kind == 2) pid = -pid;      // process group
  else if (kind == 1) pid = pid;  // process
  else if (kind == 0) pid = -1;   // any child
  else OS_report_err(OSDefs::oEINVAL);

  if (wait4_piece(LSE_LINUX_ARGS, osct, osret, pid, options, stataddr, infop,
		  ru, ret, iscompat))
    goto syserr;

  if (ret == -2) {
    osct->tgroup->childwait.add(osct);
    osct->continuationStack.push_back(new cont_wait(osct,pid,options,
						    stataddr, infop,
						    ru, iscompat,
						    changelen));
    block_thread(osct, true);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    if (ret == -2) fprintf(stderr,"blocked\n");
    else fprintf(stderr,"%d\n",ret);
  
  OS_sys_return();
 syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
}

static LSE_LINUX_SIG(waitid) {
  return waitid_body(LSE_LINUX_ARGS,"waitid",false);
}

static LSE_LINUX_SIG(waitpid) { // does not appear to have a compat version
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int pid, options;
  LSE_emu_addr_t stataddr, ru=0, infop = 0;
  int ret=0, exit_code=0;

  pid      = CC_arg_int(0,"ipi");
  stataddr = CC_arg_ptr(1,"ipi");
  options  = CC_arg_int(2,"ipi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr,
        "SYSCALL(%d): waitpid(%d,0x" LSE_emu_addr_t_print_format ",%u) = ",
        osct->tid, pid, stataddr, options);

  if (wait4_piece(LSE_LINUX_ARGS, osct, osret, pid, options, stataddr, 
		  infop, ru, ret, false))
    goto syserr;

  if (ret == -2) {
    osct->tgroup->childwait.add(osct);
    osct->continuationStack.push_back(new cont_wait(osct,pid,options,
						    stataddr, infop, ru, 
						    false,
						    changelen));
    block_thread(osct, true);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    if (ret == -2) fprintf(stderr,"blocked\n");
    else fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()  
}

static LSE_LINUX_SIG(write) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  char *buf=0;
  char mybuf[1024];
  LSE_emu_addr_t targbuf;
  int count;
  int ret=0, mustfree=0;

  fd      = CC_arg_int(0,"ipi");
  targbuf = CC_arg_ptr(1,"ipi");
  count   = CC_arg_int(2,"ipi");

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_int(count);

  if (!count) {
    OS_report_results(0,0);
    buf = mybuf;
    ret = 0;
  } else {
    if (count > 1024) {
      buf = (char *)malloc(count);
      if (buf == 0) {
	OS_report_err(OSDefs::oENOSPC);
      }
      mustfree = 1;
    } else buf = mybuf;

    OS_memcpy_t2h(buf, targbuf,count); /* will report error */

    CC_chkpt_arg_hmem(buf,count);
    struct iovec thisio = { buf, count };
    ret = osct->fdtable->getGoodFD(fd)->writev(osct, &thisio, 1);
    CC_chkpt_result_int(ret);
      
    if (ret == -1) {
      CC_chkpt_result_int(osct->myerrno());
      OS_report_err(osct->myerrno());
    } else {
      OS_report_results(0,ret);
    }
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    int i;
    fprintf(stderr,"SYSCALL(%d): write(%d,\"",osct->tid, fd);
    for (i=0;i<30 && i<ret && buf[i]; i++) 
      fputc(isprint(buf[i])?buf[i] : 0x2e, stderr);
    fprintf(stderr,"\"%s,%d) = %d\n",i<ret?"...":"",count,ret);
  }
  if (mustfree) free(buf);

  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): write(%d," LSE_emu_addr_t_print_format ",%d) = -1 %d\n",
	    osct->tid, fd, targbuf, count,(int)OS_sys_get_return(1));
  if (mustfree) {
     free(buf);
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

template<class T> static inline int 
	  writev_body(LSE_LINUX_PARMS, const char *name) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  int fd;
  T *buf=0;
  struct iovec *hostbuf=0;
  LSE_emu_addr_t targbuf;
  int count;
  int ret=0, mustfree=0;
  int i, rcount=0;

  fd      = CC_arg_int(0,"ipi");
  targbuf = CC_arg_ptr(1,"ipi");
  count   = CC_arg_int(2,"ipi");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): %s(%d," LSE_emu_addr_t_print_format ",%d) = ",
	    osct->tid, name, fd, targbuf, count);

  CC_chkpt_start(callno);
  CC_chkpt_arg_int(fd);
  CC_chkpt_arg_ptr(targbuf);
  CC_chkpt_arg_int(count);

  buf = (T *)malloc(sizeof(T)*count+1);
  hostbuf = (struct iovec *)malloc(sizeof(const struct iovec)*count+1);
  OS_memcpy_t2h(buf, targbuf, count*sizeof(T)); /* will report error */
  mustfree = 1;

  CC_chkpt_arg_int(sizeof(T));
  CC_chkpt_arg_hmem(buf,count * sizeof(T));

  for (i=0;i<count;i++)
  {
    OSDefs::L_size_t len;
    LSE_emu_addr_t base;
    /* switching stuff in place in buf */
    len = LSE_e2h(buf[i].iov_len,Linux_bigendian);
    base = LSE_e2h(buf[i].iov_base,Linux_bigendian);
    hostbuf[i].iov_base = (char *)malloc(len+1);
    OS_memcpy_t2h(hostbuf[i].iov_base,base,len);
    CC_chkpt_arg_hmem(hostbuf[i].iov_base,len);
    hostbuf[i].iov_len = len;
    rcount = i+1;
  }

  ret = osct->fdtable->getGoodFD(fd)->writev(osct, hostbuf,count);
  CC_chkpt_result_int(ret);

  if (ret == -1) {
    CC_chkpt_result_int(osct->myerrno());
    OS_report_err(osct->myerrno());
  } else {
    OS_report_results(0,ret);
  }

  if (mustfree) {
    for (i=0;i<rcount;i++) {
      free(hostbuf[i].iov_base);
    }
    free(buf);
    free(hostbuf);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "%d\n", ret);
  CC_chkpt_finish();
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  if (mustfree) {
     for (i=0;i<rcount;i++)    
     {
       free(hostbuf[i].iov_base);
     }
     free(buf);
     free(hostbuf);
  }
  CC_chkpt_finish();
  OS_syserr_handler();
}

static LSE_LINUX_SIG(writev) {
  return writev_body<OSDefs::L_iovec>(LSE_LINUX_ARGS,"writev");
}

#ifdef LSE_LINUX_COMPAT
LSE_LINUX_UNIMPLEMENTED(compat_add_key)

static LSE_LINUX_SIG(compat_clock_getres) {
  return clock_getres_body<OSDefs::L_compat_timespec>(LSE_LINUX_ARGS,
  "compat_clock_getres");
}
static LSE_LINUX_SIG(compat_clock_gettime) {
  return clock_gettime_body<OSDefs::L_compat_timespec>(LSE_LINUX_ARGS,
  "compat_clock_gettime");
}
static LSE_LINUX_SIG(compat_clock_nanosleep) {
  return clock_nanosleep_body<OSDefs::L_compat_timespec>(LSE_LINUX_ARGS,
  "compat_clock_nanosleep");
}
static LSE_LINUX_SIG(compat_clock_settime) {
  return clock_settime_body<OSDefs::L_compat_timespec>(LSE_LINUX_ARGS,
  "compat_clock_settime");
}

LSE_LINUX_UNIMPLEMENTED(compat_epoll_pwait)
LSE_LINUX_UNIMPLEMENTED(compat_fallocate)
static LSE_LINUX_SIG(compat_fcntl) {
  return fcntl_body(LSE_LINUX_ARGS,"compat_fcntl");
}
static LSE_LINUX_SIG(compat_fcntl64) {
  return fcntl_body(LSE_LINUX_ARGS,"compat_fcntl64");
}
LSE_LINUX_UNIMPLEMENTED(compat_fstatfs)
LSE_LINUX_UNIMPLEMENTED(compat_fstatfs64)
static LSE_LINUX_SIG(compat_futex) {
  return futex_body<OSDefs::L_compat_timespec>(LSE_LINUX_ARGS,"compat_futex");
}
LSE_LINUX_UNIMPLEMENTED(compat_futimesat)
LSE_LINUX_UNIMPLEMENTED(compat_getdents)
LSE_LINUX_UNIMPLEMENTED(compat_getitimer)
LSE_LINUX_UNIMPLEMENTED(compat_get_mempolicy)
static LSE_LINUX_SIG(compat_getrlimit) {
  return getrlimit_body<OSDefs::L_compat_rlimit>(LSE_LINUX_ARGS,
                                                "compat_getrlimit");
}
LSE_LINUX_UNIMPLEMENTED(compat_get_robust_list)
static LSE_LINUX_SIG(compat_getrusage) {
  return getrusage_body<OSDefs::L_compat_rusage>(LSE_LINUX_ARGS,
						"compat_getrusage");
}
static LSE_LINUX_SIG(compat_gettimeofday) {
  return gettimeofday_body<OSDefs::L_compat_timeval>(LSE_LINUX_ARGS,
						    "compat_gettimeofday");
}
static LSE_LINUX_SIG(compat_ioctl) {
  return ioctl_body<OSDefs::L_compat_termio,
                    OSDefs::L_compat_termios>(LSE_LINUX_ARGS,"ioctl");
}
LSE_LINUX_UNIMPLEMENTED(compat_io_getevents)
LSE_LINUX_UNIMPLEMENTED(compat_ioprio_get)
LSE_LINUX_UNIMPLEMENTED(compat_ioprio_set)
LSE_LINUX_UNIMPLEMENTED(compat_io_setup)
LSE_LINUX_UNIMPLEMENTED(compat_io_submit)
LSE_LINUX_UNIMPLEMENTED(compat_ipc)
LSE_LINUX_UNIMPLEMENTED(compat_kexec_load)
LSE_LINUX_UNIMPLEMENTED(compat_keyctl)
LSE_LINUX_UNIMPLEMENTED(compat_mbind)
LSE_LINUX_UNIMPLEMENTED(compat_migrate_pages)
LSE_LINUX_UNIMPLEMENTED(compat_mount)
LSE_LINUX_UNIMPLEMENTED(compat_move_pages)
LSE_LINUX_UNIMPLEMENTED(compat_mq_getsetattr)
LSE_LINUX_UNIMPLEMENTED(compat_mq_notify)
LSE_LINUX_UNIMPLEMENTED(compat_mq_open)
LSE_LINUX_UNIMPLEMENTED(compat_mq_timedsend)
LSE_LINUX_UNIMPLEMENTED(compat_mq_timedreceive)
static LSE_LINUX_SIG(compat_nanosleep) {
  return nanosleep_body<OSDefs::L_compat_timespec>(LSE_LINUX_ARGS,
  "compat_nanosleep");
}
static LSE_LINUX_SIG(compat_newfstat) {
  return fstat_body<OSDefs::L_compat_newstat>(LSE_LINUX_ARGS,"compat_newfstat");
}
static LSE_LINUX_SIG(compat_newlstat) {
  return lstat_body<OSDefs::L_compat_newstat>(LSE_LINUX_ARGS,"compat_newlstat");
}
static LSE_LINUX_SIG(compat_newstat) {
  return stat_body<OSDefs::L_compat_newstat>(LSE_LINUX_ARGS,"compat_newstat");
}
LSE_LINUX_UNIMPLEMENTED(compat_nfsservctl)
static LSE_LINUX_SIG(compat_old_getrlimit) {
  return getrlimit_body<OSDefs::L_rlimit>(LSE_LINUX_ARGS,
					  "compat_old_getrlimit");
}
LSE_LINUX_UNIMPLEMENTED(compat_old_readdir)
LSE_LINUX_UNIMPLEMENTED(compat_openat)
LSE_LINUX_UNIMPLEMENTED(compat_ppoll)
LSE_LINUX_UNIMPLEMENTED(compat_preadv)
LSE_LINUX_UNIMPLEMENTED(compat_pselect6)
LSE_LINUX_UNIMPLEMENTED(compat_pwritev)
LSE_LINUX_UNIMPLEMENTED(compat_readv)
LSE_LINUX_UNIMPLEMENTED(compat_request_key)
static LSE_LINUX_SIG(compat_rt_sigpending) {
  return rt_sigpending_body<OSDefs::L_compat_sigset>(LSE_LINUX_ARGS,
						    "compat_rt_sigpending");
}
static LSE_LINUX_SIG(compat_rt_sigprocmask) {
  return rt_sigprocmask_body<OSDefs::L_compat_sigset>(LSE_LINUX_ARGS,
						    "compat_rt_sigprocmask");
}
static LSE_LINUX_SIG(compat_rt_sigqueueinfo) {
  return rt_sigqueueinfo_body<OSDefs::L_compat_siginfo>
    (LSE_LINUX_ARGS,"compat_rt_sigqueueinfo");
}
static LSE_LINUX_SIG(compat_rt_sigreturn) {
  return rt_sigreturn_body(LSE_LINUX_ARGS,"compat_rt_sigreturn");
}
static LSE_LINUX_SIG(compat_rt_sigsuspend) {
  return rt_sigsuspend_body<OSDefs::L_compat_sigset>(LSE_LINUX_ARGS,
						    "compat_rt_sigsuspend");
}

template<>
const int cont_stimedwait<OSDefs::L_compat_siginfo>::typeno = 1;
static LSE_LINUX_SIG(compat_rt_sigtimedwait) {
  return rt_sigtimedwait_body<OSDefs::L_compat_sigset, 
			      OSDefs::L_compat_timespec,
                              OSDefs::L_compat_siginfo>
    (LSE_LINUX_ARGS,"compat_rt_sigtimedwait");
}

LSE_LINUX_UNIMPLEMENTED(compat_rt_tgsigqueueinfo)
static LSE_LINUX_SIG(compat_sched_getaffinity) {
  // Just calling the 64-bit version is OK for now since we do not actually
  // fill in the data structure.
  return LSE_LINUX_NAME(sched_getaffinity)(LSE_LINUX_ARGS);
}
LSE_LINUX_UNIMPLEMENTED(compat_sched_get_affinity)
LSE_LINUX_UNIMPLEMENTED(compat_sched_rr_get_interval)
LSE_LINUX_UNIMPLEMENTED(compat_sched_setaffinity)
LSE_LINUX_UNIMPLEMENTED(compat_sched_set_affinity)
LSE_LINUX_UNIMPLEMENTED(compat_select)
static LSE_LINUX_SIG(compat_setitimer) {
  return setitimer_body(LSE_LINUX_ARGS, "compat_setitimer");
}
LSE_LINUX_UNIMPLEMENTED(compat_set_mempolicy)
static LSE_LINUX_SIG(compat_setrlimit) {
  return setrlimit_body<OSDefs::L_compat_rlimit>(LSE_LINUX_ARGS,
						"compat_setrlimit");
}
static LSE_LINUX_SIG(compat_set_robust_list) {
  return set_robust_list_body<OSDefs::L_compat_robust_list_head>
    (LSE_LINUX_ARGS,"compat_set_robust_list",true);
}
LSE_LINUX_UNIMPLEMENTED(compat_settimeofday)

static LSE_LINUX_SIG(compat_sigaltstack) {
  Linux_context_t *osct = OS_ict_osinfo(realct);
  int osret = 0;
  LSE_emu_addr_t newstack, oldstack, realsp;
  int ret=0;

  newstack = CC_arg_ptr(0,"pp");
  oldstack = CC_arg_ptr(1,"pp");

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,
            "SYSCALL(%d): compat_sigaltstack(0x" 
                               LSE_emu_addr_t_print_format ",0x"
                               LSE_emu_addr_t_print_format ") = ",
	    osct->tid, newstack, oldstack);

  ret = handle_compat_sigaltstack(realct, osct, newstack, oldstack, 
			          CC_get_stackptr(realct));

  OS_report_results(0,ret);

  if (ret < 0) {
    OS_report_err(-ret);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(stderr,"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
}

LSE_LINUX_UNIMPLEMENTED(compat_signalfd)
LSE_LINUX_UNIMPLEMENTED(compat_signalfd4)
LSE_LINUX_UNIMPLEMENTED(compat_sigpending)
LSE_LINUX_UNIMPLEMENTED(compat_sigprocmask)
static LSE_LINUX_SIG(compat_sigreturn) {
  return rt_sigreturn_body(LSE_LINUX_ARGS,"compat_sigreturn");
}
LSE_LINUX_UNIMPLEMENTED(compat_socketcall)
static LSE_LINUX_SIG(compat_statfs) {
  return statfs_body<OSDefs::L_compat_statfs>(LSE_LINUX_ARGS,"compat_statfs");
}
LSE_LINUX_UNIMPLEMENTED(compat_statfs64)
LSE_LINUX_UNIMPLEMENTED(compat_stime)
LSE_LINUX_UNIMPLEMENTED(compat_sync_file_range2)
LSE_LINUX_UNIMPLEMENTED(compat_sysfs)
static LSE_LINUX_SIG(compat_time) {
  return time_body<OSDefs::L_compat_time_t>(LSE_LINUX_ARGS,"compat_time");
}
LSE_LINUX_UNIMPLEMENTED(compat_timerfd_gettime)
LSE_LINUX_UNIMPLEMENTED(compat_timerfd_settime)
LSE_LINUX_UNIMPLEMENTED(compat_timer_create)
LSE_LINUX_UNIMPLEMENTED(compat_timer_gettime)
LSE_LINUX_UNIMPLEMENTED(compat_timer_settime)
static LSE_LINUX_SIG(compat_times) {
  return times_body<OSDefs::L_compat_tms>(LSE_LINUX_ARGS,"compat_times");
}
LSE_LINUX_UNIMPLEMENTED(compat_utime)
LSE_LINUX_UNIMPLEMENTED(compat_utimensat)
static LSE_LINUX_SIG(compat_utimes) {
  return utimes_body<OSDefs::L_compat_timeval>(LSE_LINUX_ARGS,"compat_utimes");
}
LSE_LINUX_UNIMPLEMENTED(compat_vmsplice)
static LSE_LINUX_SIG(compat_wait4) {
  return wait4_body(LSE_LINUX_ARGS,"compat_wait4",true);
}
static LSE_LINUX_SIG(compat_waitid) {
  return waitid_body(LSE_LINUX_ARGS,"compat_waitid",true);
}
static LSE_LINUX_SIG(compat_writev) {
  return writev_body<OSDefs::L_compat_iovec>(LSE_LINUX_ARGS,"compat_writev");
}
#endif // LSE_LINUX_COMPAT

#endif // LSE_LINUX_CODE

#ifdef LSE_LINUX_CLEANUP
#undef CC_hook_pipe
#undef CC_hook_pipe2
#endif // LSE_LINUX_CLEANUP


