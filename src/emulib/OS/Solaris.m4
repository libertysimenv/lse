/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Code for doing Solaris system calls
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file is used as part of the OS processing.
 *
 * TODO: 
 *      - lots of syscalls to go....
 *      - keep target file descriptor table (instead of relying on host)
 *      - brk: integrate properly with mmap structures
 *      - brk: make it work more like the real thing!
 *      - connect:  you need to know the protocol
 *        of the file descriptor to translate the address structure
 *      - fcntl: variable arguments (basically the lock stuff)
 *      - getrusage: make real
 *      - mmap: proper duplication of file descriptors, file-system info,
 *              memory, what else?
 *      - rt_sigaction: see if there is a way to make it real
 *      - rt_sigprocmask: make real
 *
 * NOTE: - this code is not thread-safe because of the global kernel structures
 *         and the possibility of sharing various resources between processes.
 *
 * Naming conventions for macros:
 * COM - a common macro, defined across all OS/ISA combinations
 * OS  - a macro that must be defined by an OS implementation
 * CC  - a macro that must be defined by an ISA/OS implementation
 * 
 * Macro files are to be read in the following order: common, OS, ISA/OS.
 * Files should define the macros they require later files to define to
 * report errors; that way, missing things can be found at m4 processing time
 * with a sensible error message.
 *
 */
m4_ifelse(`
NOTES about how contexts get initialized and all that:

Totally new, fresh, uncloned contexts:
--------------------------------------
EMU_create_context
   - creates location in table for the context
   - calls EMU_context_create
     - calls EMU_context_alloc (TRUE)
	- allocs space
	- creates new memory because of TRUE input
	
EMU_context_load: 
  - calls Solaris_context_load 
    NOTE: we do not know that this is a Solaris context yet
    - calls Solaris_execveguts
      - loads the file, mapping as it goes
      - if the file is not a good file, return an error
      - calls Solaris_context_init (filled in by emulator)
	- since it has a new memory, it ought to block out regions which
	  should not be part of heap, if known without loading a context
      - calls Solaris_setup_context (w/stack start,end)
	- sets up pid and stuff
	- adds to task list
      - calls Solaris_schedule
      - maps the stack
      - copies arguments to stack (appears to map stack as it goes along)
      * optionally blocks out regions which should not be part of heap
      - calls Solaris_start_thread with the start_stack pointer

clone call:
  - calls EMU_context_copy
    - allocates a new context and does *not* create a new memory
  - calls Solaris_cloneguts with stack start/end (supplied by program)
	- sets up pid and stuff
	- calls Solaris_copy_thread
	- adds to task list
	- calls Solaris_schedule()

execve call:
  - not implemented, but should allocate a new memory (wiping out the old one
    as necessary) and then call Solaris_execveguts.  Might need to do the
    blocking out of regions when it creates the new memory.  Also, as the
    context will be a valid Linux context, it shouldn't do Solaris_context_init
    again?  Or maybe re-initializing is OK

What is supposed to be done by functions supplied by user:
  Solaris_context_init: put context in state in which a binary can be loaded
  Solaris_start_thread: put context in state to run after binary has been
                      loaded
  Solaris_copy_thread: put context in state to run after being cloned off
                     another thread
')

m4_include(top_srcdir/OS/Common.m4)

m4_dnl *********************************************************************
m4_dnl ************* Things required by Common.m4 **************************
m4_dnl *********************************************************************

m4_dnl Constants defined by the OS
m4_define(`OS_constants',`CC_constants')

m4_dnl Types defined by the OS
m4_define(`OS_types',`CC_types')

m4_dnl Host headers required by the OS implementation
m4_define(`OS_headers',`
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/uio.h>
#include <sys/vfs.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#ifndef __APPLE__
#include <termio.h>
#endif
#include <termios.h>
#include <unistd.h>
#if defined(__GNUC__) && !defined(LIS_UNUSED)
#define LIS_UNUSED __attribute__ ((unused))
#endif 
');

m4_dnl Additional functions defined by the OS... will define later
m4_define(`OS_helpers',`COM_build_error(`OS_helpers not defined')')

m4_dnl List of syscalls
m4_define(`OS_tags',`CC_tags')

m4_dnl How to generate code for a syscall
m4_define(`OS_tag_code',
`int 
Solaris__$1(CC_isacontext_t *realct, LSE_emu_instr_info_t *ii) {
  m4_ifdef(`OS_impl__'$1,`OS_impl__$1($2)',`CC_UnimplementedSyscall($@)')
}
')

m4_dnl How to call the code for a syscall
m4_define(`OS_tag_call',
`    case $2 : 
       osres = Solaris__$1(realct,ii);
       if (osres>=0) return osres;
       break;
')

m4_define(`OS_emu_headers',`#include <emulib/SIM_elf.h>')

m4_dnl *********************************************************************
m4_dnl *********** Requirements upon <ISA>_Linux.m4 files.... **************
m4_dnl *********************************************************************
m4_dnl
m4_dnl Miscellaneous requirements:
m4_dnl - The function from which Linux calls are made must have the following
m4_dnl   variables: 
m4_dnl      - CC_isacontext_t *realct;
m4_dnl      - LSE_emu_instr_info_t *ii;
m4_dnl
m4_dnl - The memory structure used by the emulator must be a memory structure
m4_dnl   generated using le-mem-gen
m4_dnl
m4_dnl All the following macros must be defined; if they are not, then errors
m4_dnl will occur during OS generation or compilation...  If a macro cannot
m4_dnl be defined because your data structures do not support it, then your
m4_dnl data structures are inadequate!
m4_dnl

m4_dnl ******************** Definitions **********************

m4_dnl The type of contexts in the ISA
m4_define(`CC_isacontext_t',`COM_build_error(`CC_isacontext_t not defined')'))

m4_dnl How to form a constant name
m4_define(`CC_const',`COM_build_error(`CC_const not defined')')

m4_dnl ******************* Context manipulations ********************/

m4_dnl CC_isa_context_alloc
m4_dnl     allocate a context and store a pointer to OS information in it
m4_dnl   $1 = lvalue of OS information structure
m4_define(`CC_isa_context_alloc',
	  `COM_build_error(`CC_isa_context_alloc not defined')'))

m4_dnl CC_isa_context_copy
m4_dnl     allocate and copy a context 
m4_dnl   $1 = destination pointer
m4_dnl   $2 = source pointer
m4_define(`CC_isa_context_copy',`
  rval = EMU_context_copy($1,$2);
  if (rval) OS_report_err(ENOMEM);
')

m4_dnl ***************** Context information accessors ****************/
m4_dnl All produce lvalues and have $1 of type CC_isacontext_t *

m4_dnl Stored OS context pointer
m4_define(`CC_ict_osinfo',
	  `COM_build_error(`CC_ict_osinfo not defined')')

m4_dnl Global context number
m4_define(`CC_ict_globalcno',
	  `COM_build_error(`CC_ict_globalcno not defined')')

m4_dnl Starting address of context
m4_define(`CC_ict_startaddr',
	  `COM_build_error(`CC_ict_startaddr not defined')')

m4_dnl Starting address of context
m4_define(`CC_ict_true_startaddr',`CC_ict_startaddr($@)')

m4_dnl Address of interpreter
m4_define(`CC_ict_interpaddr',
	  `COM_build_error(`CC_ict_interpaddr not defined')')

m4_dnl Base address?
m4_define(`CC_ict_baseaddr',
	  `COM_build_error(`CC_ict_baseaddr not defined')')

m4_dnl Base address?
m4_define(`CC_ict_loadaddr',
	  `COM_build_error(`CC_ict_loadaddr not defined')')

m4_dnl Load bias
m4_define(`CC_ict_loadbias',
	  `COM_build_error(`CC_ict_loadbias not defined')')

m4_dnl Use the interpreter?
m4_define(`CC_ict_use_interp',
	  `COM_build_error(`CC_ict_use_interp not defined')')

m4_dnl Memory pointer
m4_define(`CC_ict_mem',
	  `COM_build_error(`CC_ict_mem not defined')')

m4_dnl Pointer to per-memory OS information from context
m4_define(`CC_ict_memOSinfo',
	  `COM_build_error(`CC_ict_memOSinfo not defined')')

m4_dnl ******************** ELF support ******************************/

m4_dnl The expected ELF file type: 32 or 64
m4_define(`CC_elftype',`COM_build_error(`CC_elftype not defined')')

m4_dnl ******************* Calling convention *****************************/

m4_dnl CC_arg
m4_dnl     Extract an argument to a system call (while in the system call code)
m4_dnl   $1 = argument number (starting at 0)
m4_dnl   $2... = list of argument types for the call; this is needed to
m4_dnl           properly find the argument
m4_dnl
m4_dnl   Argument types that must be supported (at present) are:
m4_dnl     i - an integer; generally a target long
m4_dnl     ptr - a pointer; again, a target long
m4_dnl   
m4_define(`CC_arg',
	  `COM_build_error(`CC_arg not defined')'))

m4_dnl CC_sys_set_return
m4_dnl      Set return values for a system call into context given by
m4_dnl      an instruction (while in the system call code)
m4_dnl   $1 = return value number
m4_dnl   $2 = value
m4_dnl   $3... = return type list; like the argument list of CC_arg
m4_dnl   
m4_define(`CC_sys_set_return',
	  `COM_build_error(`CC_sys_set_return not defined')'))

m4_dnl CC_sys_get_return
m4_dnl      Get return value for a system call from context given
m4_dnl      by an instruction
m4_dnl   $1 = return value number
m4_define(`CC_sys_get_return',
	  `COM_build_error(`CC_sys_get_return not defined')')

m4_dnl ***************** Instruction information accessors ****************/
m4_dnl All produce lvalues and have $1  of type LSE_instr_info_t *

m4_define(`CC_curr_pc',
	  `COM_build_error(`CC_curr_pc not defined')')

m4_define(`CC_following_pc',
	  `COM_build_error(`CC_following_pc not defined')')

m4_dnl ************************ Memory accessors ***************************/

m4_dnl CC_mem_name
m4_dnl       The name (prefix) of the LSE memory to be used by the OS
m4_dnl       (only if the standard memory accessors are used)
m4_define(`CC_mem_name',
	  `COM_build_error(`CC_mem_name not defined')')

m4_dnl CC_mem_fill_attr
m4_dnl       Fill in the extra fields of a memory attribute structure
m4_dnl    $1 = memory name
m4_dnl    $2 = pointer to attribute structure
m4_define(`CC_mem_fill_attr',
	  `COM_build_error(`CC_mem_fill_attr not defined')')

m4_dnl CC_mem_errstring
m4_dnl        Report a memory error
m4_dnl    $1 = error code
m4_define(`CC_mem_errstring',
	  `COM_build_error(`CC_mem_errstring not defined')')

m4_dnl CC_mem_addmap
m4_dnl        Add a mapping to the memory
m4_dnl    $1 = memory
m4_dnl    $2 = pointer to address
m4_dnl    $3 = pointer to length
m4_dnl    $4 = flags
m4_define(`CC_mem_addmap',
	  `COM_build_error(`CC_mem_addmap not defined')')

m4_dnl CC_mem_read_from_file
m4_dnl         Read memory from a file
m4_dnl    $1 = memory
m4_dnl    $2 = pointer to address
m4_dnl    $3 = pointer to length
m4_dnl    $4 = file pointer
m4_dnl    $5 = file descriptor
m4_define(`CC_mem_read_from_file',
	  `COM_build_error(`CC_mem_read_from_file not defined')')

m4_dnl CC_mem_memset
m4_dnl       Set bytes of memory to a specific value
m4_dnl    $1 = memory
m4_dnl    $2 = pointer to address
m4_dnl    $3 = value
m4_dnl    $4 = pointer to length
m4_define(`CC_mem_memset',
	  `COM_build_error(`CC_mem_memset not defined')')

m4_dnl CC_mem_write
m4_dnl       Write to memory
m4_dnl    $1 = memory
m4_dnl    $2 = pointer to address
m4_dnl    $3 = pointer to length
m4_dnl    $4 = host buffer
m4_define(`CC_mem_write',
	  `COM_build_error(`CC_mem_write not defined')')

m4_dnl CC_memcpy_h2t
m4_dnl       Perform a memcpy from host memory to target memory
m4_dnl    $1 = target memory address
m4_dnl    $2 = host address
m4_dnl    $3 = length
m4_define(`CC_memcpy_h2t'
	  `COM_build_error(`CC_memcpy_h2t not defined')')

m4_dnl CC_memcpy_t2h
m4_dnl       Perform a memcpy from target memory to host memory
m4_dnl    $1 = host address
m4_dnl    $2 = target memory address
m4_dnl    $3 = length
m4_define(`CC_memcpy_t2h'
	  `COM_build_error(`CC_memcpy_t2h not defined')')

m4_dnl CC_strncpy_t2h
m4_dnl       Perform a strncpy from target memory to host memory
m4_dnl    $1 = host address
m4_dnl    $2 = target memory address
m4_dnl    $3 = maximum length
m4_define(`CC_strncpy_t2h'
	  `COM_build_error(`CC_strncpy_t2h not defined')')

m4_dnl ******************** Checkpointing code **************************/

m4_dnl CC_chkpt_start
m4_dnl    Start processing an OS record
m4_dnl  $1 = tag number
m4_define(`CC_chkpt_start')

m4_dnl CC_chkpt_arg
m4_dnl     Add/check an argument record (must be done in order)
m4_dnl  $1 = type (ptr, str, hmem, i)
m4_dnl  $2 = value
m4_dnl  $3 (optional) = length
m4_define(`CC_chkpt_arg')

m4_dnl CC_chkpt_result
m4_dnl     Add/read a result record (must be done in order)
m4_dnl  $1 = type (ptr, str, hmem, any integer type)
m4_dnl  $2 = value (must be an lvalue)
m4_dnl  $3 (optional) = length
m4_define(`CC_chkpt_result')

m4_dnl CC_chkpt_finish
m4_dnl      Finish processing an OS record.  Needs to do nothing if
m4_dnl      it is called while not processing a record (because it
m4_dnl      is called as part of the default epilogues of syscalls)
m4_define(`CC_chkpt_finish')

m4_dnl CC_chkpt_guard
m4_dnl     Perform the code only if not replaying
m4_define(`CC_chkpt_guard',`$*')

m4_dnl ******************** Program sections **********************

m4_dnl Constants defined by the OS
m4_define(`CC_constants',`COM_build_error(`CC_constants not defined')')

m4_dnl Types defined by the OS
m4_define(`CC_types',`COM_build_error(`CC_types not defined')')

m4_dnl Syscalls
m4_define(`CC_tags',`COM_build_error(`CC_tags not defined')')

m4_dnl Handle unimplemented syscalls
m4_dnl  $1 = syscall name
m4_dnl  $2 = syscall number
m4_define(`CC_UnimplementedSyscall',
	  `COM_build_error(`CC_UnimplementedSyscall not defined')')

m4_dnl ****************** Miscellaneous *******************************

m4_dnl CC_open_filter
m4_dnl    Weird sequence of code that prevents opening certain files
m4_dnl   $1 = char *filename, 
m4_dnl   $2 = return variable
m4_dnl Why do we need this?  Host locale files may not have the right 
m4_dnl endianness, so we have to fool the program into thinking they do not 
m4_dnl exist!
m4_dnl But this one we will not report as an error if someone forgets
m4_define(`CC_open_filter')

m4_dnl CC_rewrite_filename
m4_dnl   Rename files to find them.
m4_dnl   $1 = char *filename buffer
m4_define(`CC_rewrite_filename')

m4_dnl CC_psect_hook
m4_dnl     Extra stuff to do while loading psections
m4_define(`CC_psect_hook')

m4_dnl CC_elfarchcheck_hook
m4_dnl    Extra tests when checking legal architectures
m4_define(`CC_elfarchcheck_hook')

m4_dnl CC_execveguts_hook
m4_dnl    Extra stuff to do in execveguts
m4_define(`CC_execveguts_hook')

m4_dnl CC_getcontext
m4_dnl    Stuff to fill in the context structure
m4_define(`CC_getcontext')

m4_dnl ******************** Required functions *****************************
m4_dnl
m4_dnl int Solaris_copy_thread(IA64_context_t *newct, IA64_context_t *oldct,
m4_dnl			     int flags, LSE_emu_addr_t stackstart,
m4_dnl			     LSE_emu_addr_t stacksize) 
m4_dnl   Set up a thread forked by clone2 to have an appropriate stack; the 
m4_dnl   context copy has probably already set up registers and all that
m4_dnl   stuff
m4_dnl
m4_dnl int Solaris_start_thread(CC_isacontext_t *realct, LSE_emu_iaddr_t ip,
m4_dnl			      LSE_emu_addr_t sp);
m4_dnl   Set up a thread forked from init
m4_dnl

m4_dnl *********************************************************************
m4_dnl ******************** Linux goodies **********************************
m4_dnl *********************************************************************

m4_dnl Lvalue for the current context when in syscall code
m4_define(`OS_ct',realct)

m4_dnl macro for defining macros to hold syscall code
m4_define(`OS_impl',`m4_define(`OS_impl__'$1,`$2')');

m4_dnl how we report syscall results in Linux
m4_define(`OS_report_results',
`  {
    CC_sys_set_return(realct,0,$1,i,i);
    CC_sys_set_return(realct,1,$2,i,i);
  }')
m4_define(`OS_report_err',
`  {
    CC_sys_set_return(realct,0,-1,i,i);
    CC_sys_set_return(realct,1,$1,i,i);
    goto syserr;
  }')

m4_dnl More ways of setting and getting return values
m4_define(`OS_sys_get_return',`CC_sys_get_return(realct,$@)')

m4_dnl Let the structure stuff use the nice common routines
m4_define(`OS_struct',m4_defn(`COM_struct'))
m4_define(`OS_typedef',m4_defn(`COM_typedef'))
m4_define(`OS_const',`#define CC_const($1) $2')m4_dnl

m4_dnl OS_ict_osinfo
m4_dnl     Return OS context pointer properly cast
m4_dnl   $1 = context pointer
m4_define(`OS_ict_osinfo',`((Solaris_context_t *)CC_ict_osinfo($1))')

m4_dnl OS_osvar
m4_dnl     Lvalue for an OS context variable in the context of a syscall
m4_dnl   $1 = variable name
m4_define(`OS_osvar',`OS_ict_osinfo(realct)->$1')

m4_dnl OS_set_curr_context_done
m4_dnl    Set the current context to be finished...
m4_define(`OS_set_curr_context_done',
`{
   COM_context_map_update(OS_ct,0,CC_ict_globalcno(OS_ct));
   EMU_context_map_notify(&CC_emu_interface(OS_ct),
			  0,CC_ict_globalcno(OS_ct),OS_ct);
}')

m4_dnl How to return from the individual syscall routines
m4_define(`OS_sys_return',`
CC_chkpt_finish();
return 0;')
m4_define(`OS_syserr_handler',
`CC_chkpt_finish();
return 0;')

m4_dnl OS_interface
m4_dnl    Return the OS interface pointer properly cast
m4_dnl    $1 = interface
m4_define(`OS_interface',`COM_build_error(`OS_interface not defined')')
m4_define(`OS_interface_set',`COM_build_error(`OS_interface_set not defined')')
m4_define(`OS_interface_define',`')

m4_dnl OS_emuvar
m4_dnl   Lvalue for an emulator variable in the context of an interface
m4_dnl     $1 = interface
m4_dnl     $2 = name of variable
m4_define(`OS_emuvar',`$2')

m4_dnl OS_ict_emuvar
m4_dnl   Lvalue for an emulator variable in the context of a context
m4_dnl     $1 = context
m4_dnl     $2 = name of variable
m4_define(`OS_ict_emuvar',`$2')

m4_dnl *****************************************************************
m4_dnl ********** Memory accessors using the LSE memory generator ******
m4_dnl *****************************************************************

m4_define(`OS_set_standard_memory_accessors',`
  m4_define(`CC_mem_fill_attr',m4_defn(`OS_mem_fill_attr'))
  m4_define(`CC_mem_errstring',m4_defn(`OS_mem_errstring'))
  m4_define(`CC_mem_addmap',m4_defn(`OS_mem_addmap'))
  m4_define(`CC_mem_read_from_file',m4_defn(`OS_mem_read_from_file'))
  m4_define(`CC_mem_memset',m4_defn(`OS_mem_memset'))
  m4_define(`CC_mem_write',m4_defn(`OS_mem_write'))
  m4_define(`CC_memcpy_h2t',m4_defn(`OS_memcpy_h2t'))
  m4_define(`CC_memcpy_t2h',m4_defn(`OS_memcpy_t2h'))
  m4_define(`CC_strncpy_t2h',m4_defn(`OS_strncpy_t2h'))
')

m4_define(`OS_mem_fill_attr')
m4_define(`OS_mem_errstring',`CC_mem_name`'_errstring($@)')
m4_define(`OS_mem_addmap',`Solaris_do_map($1,*$2,*$3,$4)')
m4_define(`OS_mem_read_from_file',
	  `CC_mem_name`'_read_from_file($1,*$2,*$3,$4,$5)')
m4_define(`OS_mem_memset',`CC_mem_name`'_memset($1,*$2,$3,*$4)')
m4_define(`OS_mem_write',`CC_mem_name`'_write($1,*$2,*$3,(CC_mem_name`'_data_t *)$4)')

m4_dnl Memory copy operators

m4_dnl $1 = destination, $2 = src, $3 = size
m4_define(`OS_memcpy_h2t',`
{
  LSE_emu_addr_t _dest = $1;
  unsigned char *_src = (unsigned char *)$2;
  uint64_t _size = $3;
  LSE_mgen_error_t memerr;
  memerr = CC_mem_name`'_write(CC_ict_mem(OS_ct),_dest,_size,_src);
  if (memerr) {
    CC_sys_set_return(OS_ct,0,-1,i,i);
    CC_sys_set_return(OS_ct,1,CC_const(EFAULT),i,i);
    goto syserr;
  }
}')

m4_dnl $1 = destination, $2 = src, $3 = size
m4_define(`OS_memcpy_t2h',`
{
  unsigned char *_dest = (unsigned char *)$1;
  LSE_emu_addr_t _src = $2;
  uint64_t _size = $3;
  LSE_mgen_error_t memerr;
  memerr=CC_mem_name`'_read(CC_ict_mem(OS_ct),_src,_size,_dest);
  if (memerr) {
    CC_sys_set_return(OS_ct,0,-1,i,i);
    CC_sys_set_return(OS_ct,1,CC_const(EFAULT),i,i);
    goto syserr;
  }
}')

m4_dnl $1 = destination, $2 = src, $3 = size
m4_define(`OS_strncpy_t2h',`
{
  char *_targ = (char *)$1;
  LSE_emu_addr_t _src = $2;
  unsigned int _size = (unsigned int)$3;
  LSE_mgen_error_t memerr;
  int *flagsp;
  int done=0;
  CC_mem_name`'_data_t *hostsrc,*p;
  CC_mem_name`'_addr_piece_t madelen;
  unsigned int i;

  while (!done) {
    memerr = CC_mem_name`'_translate(CC_ict_mem(OS_ct),_src,&madelen, 
				     &flagsp, &hostsrc);
    if (memerr) {
      CC_sys_set_return(OS_ct,0,-1,i,i);
      CC_sys_set_return(OS_ct,1,CC_const(EFAULT),i,i);
      goto syserr;
    }
    for (i=0,p=hostsrc;
	 i<madelen && i<_size;
	 i++,p++) 
      if (!*p) { /* done */
        memcpy(_targ,hostsrc,i+1);
        done = 1;
	break;
      }
    if (!done) {
      memcpy(_targ,hostsrc,i);
      if (i<madelen) { /* ran up against size limit */
         _targ[i-1]=0;
      }
      _targ += madelen;
      _src += madelen;   /* not good for > 64 bits */
      _size -= madelen;
    }
  }
}')

m4_dnl *****************************************************************
m4_dnl ********************* Helper routines ***************************
m4_dnl *****************************************************************

m4_define(`OS_helpers',`

struct Solaris_vm_area_s {
  LSE_emu_addr_t start;
  LSE_emu_addr_t end;
  struct Solaris_vm_area_s *next;
};

typedef struct Solaris_context_s {
    CC_isacontext_t *isacont;
    LSE_emu_addr_t heapbreak;
    CC_type(long) exit_code;
    int pid, ppid;
    int gid, egid, uid, euid;
    int is64bit;
    struct Solaris_context_s *next, *prev;
    struct Solaris_context_s *next_run, *prev_run;
    struct Solaris_vm_area_s *allocated_regions; /* not always */
} Solaris_context_t;

typedef struct Solaris_dinst_s {
   Solaris_context_t initcontext;
   CC_isacontext_t() *initisacontext;
   int last_pid;
   CC_interface_fields()
} Solaris_dinst_t;

OS_interface_define()

int Solaris_init(LSE_emu_interface_t *ifc) {
  Solaris_dinst_t *ldi = (Solaris_dinst_t *)malloc(sizeof(Solaris_dinst_t));
  OS_interface_set(ifc, ldi);

  CC_isa_context_alloc(ldi->initisacontext,&ldi->initcontext,ifc);
  ldi->initcontext.isacont = ldi->initisacontext;
  ldi->initcontext.next = ldi->initcontext.prev = &ldi->initcontext;
  ldi->initcontext.next_run = ldi->initcontext.prev_run = &ldi->initcontext;
  ldi->initcontext.pid = 1;
  ldi->initcontext.ppid = 0;
  ldi->initcontext.gid = getgid();
  ldi->initcontext.egid = getegid();
  ldi->initcontext.uid = getuid();
  ldi->initcontext.euid = geteuid();
  ldi->initcontext.heapbreak = 0;
  ldi->initcontext.exit_code = 0;
  ldi->initcontext.allocated_regions = NULL;
  ldi->initcontext.is64bit = m4_ifelse(CC_elftype,32,0,1);
  ldi->last_pid = 299;

  CC_init_hook(ifc);
  return 0;
}

void Solaris_finish(LSE_emu_interface_t *ifc) {
  /* really ought to free the context */
  if (OS_interface(ifc)) free(OS_interface(ifc));
  OS_interface_set(ifc,NULL);
  CC_finish_hook(ifc);
}

static LSE_mgen_error_t Solaris_do_map(CC_mem_name`'_memory_t memp,
                                     CC_mem_name`'_addr_piece_t addr,
                                     CC_mem_name`'_addr_piece_t len,
                                     int flags) {
  CC_mem_name`'_attr_t a;
  a.flags = flags;
  CC_mem_fill_attr(CC_mem_name,(&a));
  return CC_mem_name`'_addmap(memp,addr,len,&a);
}

int Solaris_copy_thread(CC_isacontext_t *newct, 
		      CC_isacontext_t *oldct,
		      int flags, LSE_emu_addr_t stackstart,
		      LSE_emu_addr_t stacksize);

void Solaris_schedule(LSE_emu_interface_t *);

LSE_emu_addr_t Solaris_find_free_region(CC_isacontext_t *realct,
                                        LSE_emu_addr_t len) {
  struct Solaris_vm_area_s *vmm;
  LSE_emu_addr_t addr = 0;
  LSE_emu_addr_t align_mask = CC_const(PAGE_SIZE)-1;

  for(vmm = (struct Solaris_vm_area_s *)
            CC_ict_memOSinfo(realct)->allocated_regions; ; vmm = vmm->next) {
    if(!vmm || addr + len <= vmm->start) return addr;
    addr = (vmm->end + align_mask) & ~align_mask;
  }

  return 0;
}


LSE_emu_addr_t 
Solaris_ffrt(LSE_emu_addr_t len, struct Solaris_vm_area_s *vmm)
 {
  LSE_emu_addr_t align_mask = CC_const(PAGE_SIZE)-1;
  LSE_emu_addr_t fnd;

  if (!vmm || !vmm->next) return 0;
  fnd = Solaris_ffrt(len, vmm->next);
  if (fnd) return fnd;
  else {
    LSE_emu_addr_t start = (vmm->next->start) - len & ~align_mask;
    if (start > vmm->end) return start;
  } 
  return 0;
}

LSE_emu_addr_t Solaris_find_free_region_top(CC_isacontext_t *realct,
                                            LSE_emu_addr_t len) {
  LSE_emu_addr_t addr = 0;

  addr = Solaris_ffrt(len, (struct Solaris_vm_area_s *)
                      CC_ict_memOSinfo(realct)->allocated_regions);
  if (!addr) return Solaris_find_free_region(realct, len); // try other way.
  else return addr;
}

int Solaris_allocate_region(CC_isacontext_t *realct, 
                            LSE_emu_addr_t start, LSE_emu_addr_t len) {
  struct Solaris_vm_area_s *vmm, *vmm_prev, *new_vmm = NULL;
  LSE_emu_addr_t end = start + len - 1;

  for (vmm_prev = NULL, 
         vmm = (struct Solaris_vm_area_s *)
               CC_ict_memOSinfo(realct)->allocated_regions; 
      vmm;
      vmm_prev = vmm, vmm = vmm->next) {
    if(start > vmm->end) continue;

    /* We are between the last page and this page.  Figure out if we
       can merge any entries, and if not create and insert a new entry */
    if(vmm_prev && start == vmm_prev->end + 1) {
      new_vmm = vmm_prev;
      new_vmm->end = end;
      vmm = vmm_prev;
    } else if(start >= vmm->start || end >= vmm->end) {
      new_vmm = vmm;
      vmm->start = (start > vmm->start) ? vmm->start : start;
      vmm->end = (end > vmm->end) ? end : vmm->end;
    } else {
      new_vmm = 
        (struct Solaris_vm_area_s *)malloc(sizeof(struct Solaris_vm_area_s));
      if(new_vmm == NULL) {
        return -1;
      }

      new_vmm->start = start;
      new_vmm->end = end;
      new_vmm->next = vmm;
      if(vmm_prev)
        vmm_prev->next = new_vmm;
      else
        CC_ict_memOSinfo(realct)->allocated_regions = (void *)new_vmm;
    }

    /* Merge with all later entries */
    for(vmm = vmm->next; vmm; vmm = new_vmm->next) {
      if(new_vmm->end < vmm->start) {
        break;
      } else if(new_vmm->end >= vmm->end) {
        new_vmm->next = vmm->next;
        free(vmm);
      } else {
        new_vmm->end = vmm->end;
        new_vmm->next = vmm->next;
        free(vmm);
        break;
      }
    }

    return 0;
  }

  /* We are being appended on to the end of the list */
  vmm = (struct Solaris_vm_area_s *)malloc(sizeof(struct Solaris_vm_area_s));
  if(vmm == NULL) {
    return -1;
  }
  vmm->start = start;
  vmm->end = end;
  vmm->next = NULL;

  if(vmm_prev) {
    vmm_prev->next = vmm;
  } else {
    CC_ict_memOSinfo(realct)->allocated_regions = (void *)vmm;
  }

  return 0;
}

int Solaris_setup_context(CC_isacontext_t *newisact,
	                CC_isacontext_t *oldisact) {

  Solaris_context_t *newosct;
  Solaris_context_t *oldosct = OS_ict_osinfo(oldisact);
  Solaris_dinst_t *ldi = OS_interface(&CC_emu_interface(newisact));

  /* Linux tests for CLONE_PID, since it is not allowed.  */
  /* allocate task structure... Linux allocates space for the stack
   * as well in kernel space (later it is mapped into user space) */
  newosct = (Solaris_context_t *) malloc(sizeof(Solaris_context_t));
  CC_ict_osinfo(newisact) = newosct;
  if (!newosct) return 1;
  *newosct = *oldosct; /* copy attributes of parent */
  newosct->isacont = newisact;

  /* Linux: check for exceeding process limit of user... */
  /* Linux: check for exceeding maximum total threads */
  /* Linux: exec domain and module stuff.... */
  /* Linux: set some mysterious flags */

  /* get a new pid */
  newosct->pid = ++ldi->last_pid;  /* TODO: deal with rollover eventually */
  newosct->ppid = oldosct->pid;

  /* Linux: initialize process wait queue and pending signal queue */
  /* Linux: clear times */

  /* Add to task list */
  newosct->next = &ldi->initcontext;
  newosct->prev = ldi->initcontext.prev;
  ldi->initcontext.prev->next = newosct;
  ldi->initcontext.prev = newosct;

  /* add to run list */
  newosct->next_run = &ldi->initcontext;
  newosct->prev_run = ldi->initcontext.prev_run;
  ldi->initcontext.prev_run->next_run = newosct;
  ldi->initcontext.prev_run = newosct;

  return 0;
}

int Solaris_cloneguts(CC_isacontext_t *newisact,
	            CC_isacontext_t *oldisact,
                    int flags,
	            LSE_emu_addr_t stackstart, LSE_emu_addr_t stacksize) {

  Solaris_setup_context(newisact, oldisact);

  /* processor-specific copies (copy_thread) */
  Solaris_copy_thread(newisact, oldisact, flags, stackstart,stacksize);

  /* Linux: more weird execution domain stuff.... */
  /* Linux: scheduling policy stuff */

  Solaris_schedule(&CC_emu_interface(newisact));

  return 0;
}

static int elfarchchecker(int foundflag, void *ct) {
  CC_isacontext_t *realct LIS_UNUSED = (CC_isacontext_t *)ct;
  Solaris_context_t *osct LIS_UNUSED = OS_ict_osinfo(realct);

  CC_elfarchcheck_hook()
  if (foundflag == CC_const(elfarchflag)) return 1;
  return 0;
}

static int handle_psect(int psect_num, uint64_t len,
	                int fixed, ElfB_Phdr *hdrp,
			FILE *fp,
			void *ct, int is64bit) {
  CC_isacontext_t *realct = (CC_isacontext_t *)ct;
  Solaris_context_t *osct=OS_ict_osinfo(realct);
  uint64_t p_offset = is64bit ? hdrp->h64.p_offset : hdrp->h32.p_offset;
  uint64_t p_filesz = is64bit ? hdrp->h64.p_filesz : hdrp->h32.p_filesz;
  uint64_t p_vaddr = is64bit ? hdrp->h64.p_vaddr : hdrp->h32.p_vaddr;
  uint64_t p_memsz = is64bit ? hdrp->h64.p_memsz : hdrp->h32.p_memsz;
  uint32_t p_flags = is64bit ? hdrp->h64.p_flags : hdrp->h32.p_flags;
  uint64_t p_align = is64bit ? hdrp->h64.p_align : hdrp->h32.p_align;
  uint64_t pagememsz = p_align ? (p_memsz + p_align - 1) & ~(p_align - 1) :
	p_memsz;

  LSE_mgen_error_t rval;
  LSE_emu_addr_t endofsect;
  LSE_emu_addr_t loadaddr;

  if(psect_num == 0) {
    uint64_t base;
    uint64_t aligned_vaddr;
    uint64_t page_align_mask = CC_const(PAGE_SIZE)-1;

    /* Here we will allocate ourselves space, and determine the offset
       that future segments should use */

    if (p_align) aligned_vaddr = (p_vaddr & ~page_align_mask) & ~(p_align-1);
    else aligned_vaddr = p_vaddr & ~page_align_mask;

    if(!fixed) {
      base = Solaris_find_free_region_top(realct, len);
      CC_ict_loadbias(realct) = (base - aligned_vaddr);
    } else {
      base = aligned_vaddr;
      CC_ict_loadbias(realct) = 0;
    }

    if(CC_ict_use_interp(realct)) 
      CC_ict_interpaddr(realct) = base;
    else 
      CC_ict_baseaddr(realct) = base;
  }

  if (OS_ict_emuvar(realct,EMU_debugcontextload))
    fprintf(COM_stderr(realct),
	   "p off:%016"PRIx64" filesz:%016"PRIx64" vaddr:%016"
	   PRIx64" memsz:%016"PRIx64" loadaddr:%016"PRIx64"\n",
	   p_offset,p_filesz,
	   p_vaddr,p_memsz, p_vaddr + CC_ict_loadbias(realct));

  if (hdrp->h32.p_type == 3 /*PT_INTERP*/) {
      CC_ict_use_interp(realct) = 1;
  }

    /* we should not be doing anything with non-load sections */
  if (hdrp->h32.p_type != 1) return 0;

  if (!CC_ict_loadaddr(realct) && (p_flags & PF_W)) 
    CC_ict_loadaddr(realct) =  p_vaddr+1;

  if (p_memsz != 0) {
      loadaddr = p_vaddr + CC_ict_loadbias(realct);

      rval = CC_mem_addmap(CC_ict_mem(realct),&loadaddr,&pagememsz,0);
      if (rval) {
        fprintf(COM_stderr(realct),"Error adding memory segment: %s\n",
	        CC_mem_errstring(rval));
        return rval;
      }
      Solaris_allocate_region(realct, loadaddr, p_memsz);
      rval = CC_mem_read_from_file(CC_ict_mem(realct),&loadaddr,&p_filesz,fp,0);
      if (!(p_flags & 2)) { /* if read-only, mark it so in memory */
        rval = CC_mem_addmap(CC_ict_mem(realct),&loadaddr,&pagememsz,
			     LSE_mgen_flags_READONLY);
      }

      CC_psect_hook()

      if (rval) {
        fprintf(COM_stderr(realct),"Error reading program segment: %s\n",
	        CC_mem_errstring(rval));
        return rval;
      }
  }

  if (p_filesz < p_memsz) { /* map zeros... */
    uint64_t tempaddr = loadaddr + p_filesz;
    uint64_t templen = p_memsz - p_filesz;
    rval=CC_mem_memset(CC_ict_mem(realct),&tempaddr,0,&templen);
    if (rval) {
      fprintf(COM_stderr(realct),"Error zeroing memory segment: %s\n",
	      CC_mem_errstring(rval));
      return rval;
    }
  }
  endofsect = loadaddr + p_memsz;

  /* ensure that we only update heapbreak for the main program, not the
     dynamic loader. */
  if (endofsect > osct->heapbreak && !CC_ict_interpaddr(realct))
    osct->heapbreak = endofsect;

  return 0;
}

static int handle_ssect(ElfB_Shdr *hdrp, void *ct, int is64bit) {
  /* IA64 executables actually have read-only sections which are aligned to 
     16 byte boundaries mixed into the same OS page with non-read-only.
     I am not going to track read-only attributes to 16 byte granularity..
     that is ludicrous!  So... we are droppping read-only attributes
     entirely.
  */
  CC_isacontext_t *realct = (CC_isacontext_t *)ct;

  if (OS_ict_emuvar(realct,EMU_debugcontextload))
    if (is64bit) 
      fprintf(COM_stderr(realct), 
	     "s  %" PRIx64"  %" PRIx64 "  %" PRIx64 "\n",
            hdrp->h64.sh_offset,hdrp->h64.sh_addr,hdrp->h64.sh_size);
    else
      fprintf(COM_stderr(realct), 
	     "s  %" PRIx32"  %" PRIx32 "  %" PRIx32 "\n",
            hdrp->h32.sh_offset,hdrp->h32.sh_addr,hdrp->h32.sh_size);
  return 0;
}

static int add_aux_entry(CC_isacontext_t *realct, int is64bit, char *pname,
			 LSE_emu_addr_t *sbasep, int kind, COM_addrtype val) {
  COM_addrtype stemp8;
  char *stp8;
  LSE_emu_addr_t slen;
  LSE_mgen_error_t rval;

  stp8 = ((char *)(&stemp8) + 
         ((CC_addrsize != sizeof(stemp8) &&COM_end_switched()) ? 4 : 0));


    if (OS_ict_emuvar(realct,EMU_debugcontextload))
      fprintf(COM_stderr(realct),
	"\t aux entry: " LSE_emu_addr_t_print_format " %d " 
		LSE_emu_addr_t_print_format "\n", *sbasep, kind, val);

    stemp8 = COM_end_h2e_addr((COM_addrtype())kind);
    slen = CC_addrsize;
    rval = CC_mem_write(CC_ict_mem(realct),sbasep,&slen,stp8);
    if (rval) {
      fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	      CC_mem_errstring(rval),pname);
      return (int)(rval);
    }
    *sbasep +=CC_addrsize;

    stemp8 = COM_end_h2e_addr(val);
    rval = CC_mem_write(CC_ict_mem(realct),sbasep,&slen,stp8);
    if (rval) {
      fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	      CC_mem_errstring(rval),pname);
      return (int)(rval);
    }
    *sbasep +=CC_addrsize;
    return 0;
}

int Solaris_context_init(CC_isacontext_t *realct);
int 
Solaris_start_thread(CC_isacontext_t *realct, LSE_emu_iaddr_t ip,
		   LSE_emu_addr_t sp, int argc, LSE_emu_addr_t argv,
		   LSE_emu_addr_t envp);

int Solaris_execveguts(CC_isacontext_t *realct,
  			    char *filename, int argc,
			    char *argv[], char **envp) {
  int fd;
  char buf[128];
  ElfB_Ehdr topheader, interpheader;
  LSE_elf_error_t retval;
  LSE_mgen_error_t rval;
  LSE_emu_addr_t stacksize, sbase, slen, start_stack;
  LSE_emu_addr_t program_entry;
  LSE_emu_addr_t vargv,venvp;
  uint64_t saddr;
  COM_addrtype stemp8;
  int numenv;
  char **sp;
  int i;
  int match;
  int is64bit;
  char *stp8;
  int rvalint;

  /* open file (should ensure it is executable) */
  fd = open(filename,O_RDONLY);
  if (fd<0) return -errno;

  /* check arguments */

  /* Linux: fill in bprm structure with current context info
     bprm.p = pagesize * max arg pages - size of a pointer */

  /* Read in first part of file (128 bytes) */
  memset(buf,0,128);
  read(fd,buf,4);

  /* Linux: copy arguments from user to kernel space
   * bprm.p gets decremented by size used for arguments
   */

  /* figure out what kind of executable and execute */
  if (!memcmp(buf,ELFMAG,SELFMAG)) { /* ELF file */
    close(fd);

    m4_ifelse(CC_elftype,64,match=2,CC_elftype,32,match=1,match=0);
    retval = LSE_elf_check(argv[0], elfarchchecker, &topheader, match);
    if (retval) return -CC_const(ENOEXEC);

    /* TODO: do this only if the context is not already Linux-initialized */
    {
      rvalint = Solaris_context_init(realct);
      if (rvalint) return rvalint;

      Solaris_setup_context(realct,
                  OS_interface(&CC_emu_interface(realct))->initisacontext);

      Solaris_schedule(&CC_emu_interface(realct));
    }

    CC_ict_baseaddr(realct) = 0;
    CC_ict_interpaddr(realct) = 0;
    CC_ict_use_interp(realct) = 0;

    retval = LSE_elf_load(argv[0], elfarchchecker, &topheader, 
	                  match, handle_psect, handle_ssect, realct);
    if (retval == LSE_elf_error_outofmemory) return -CC_const(ENOMEM);
    else if (retval) return -CC_const(ENOEXEC);

    OS_ict_osinfo(realct)->is64bit = is64bit = 
                     topheader.h32.e_ident[EI_CLASS] != ELFCLASS32;
    CC_ict_true_startaddr(realct) = (is64bit ? topheader.h64.e_entry 
			              : topheader.h32.e_entry) + 
				     CC_ict_loadbias(realct);


    CC_contextsetup_hook()

    program_entry = CC_ict_true_startaddr(realct);

    if (CC_ict_use_interp(realct)) {
      char fname[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3]; 
      const char *filename = CC_const(DYNLOADER);
      struct stat statbuf;

      strcpy(fname,filename);

      if (!stat(fname,&statbuf)) {}
      else if (filename[0] != 0x2f) { /* NOTE: cannot use single quotes in m4*/
	/* time to search the LD_LIBRARY_PATH */
	char **ep = envp;
	while (ep && *ep) {
	  if (!strncmp(*ep,"LD_LIBRARY_PATH",15)) {
	    char *totokenize = (char *)malloc(strlen(*ep)+1);
	    strcpy(totokenize, (*ep)+16);
	    char *p = totokenize;
	    while (*p) {
	      char *pe = strchr(p, 0x3a); /* NOTE: no single quotes in m4! */
	      if (pe) *pe = 0;
	      sprintf(fname,"%s/%s",p, filename);
	      if (!stat(fname, &statbuf)) {
		free(totokenize);
		goto interpsearchdone;
	      }
	      if (!pe) break;
	      p = pe+1;
            }
	    free(totokenize);
	    return -CC_const(ENOEXEC);
	  }
	  ep++;
	} /* while environment */
      } else return -CC_const(ENOEXEC);

     interpsearchdone:
      
      /* TODO: some day do search in LD_LIBRARY_PATH for emulator...
       * for emulator (/lib:/usr/lib)
       */
      retval = LSE_elf_load(fname, elfarchchecker,
			    &interpheader, match,
			    handle_psect, handle_ssect, realct);

      if (retval == LSE_elf_error_outofmemory) return -CC_const(ENOMEM);
      else if (retval) return -CC_const(ENOEXEC);

      is64bit = interpheader.h32.e_ident[EI_CLASS] != ELFCLASS32;
      CC_ict_true_startaddr(realct) = CC_ict_interpaddr(realct) + 
	(is64bit ? interpheader.h64.e_entry : interpheader.h32.e_entry);
    }

    /* set up the argument pages now...
       stack base is USTACK_TOP - max arg pages * page_size
       bprm.p is incremented by stack base.  So now points to where
       arguments will be in user space
     */
    /* Map the argument pages into user space.  I cannot see that
     * any larger stack actually gets allocated.  Actually, I am mapping
     * too much; Linux maps only from the stack pointer up.
     *
     * But somewhere the whole stack needs to be mapped, and I do not quite
     * understand where, so I am going to do it here -- DAP
     */
    //slen =  CC_const(NUM_ARG_PAGES)*CC_const(PAGE_SIZE);
    slen = CC_const(USTACK_TOP) - CC_const(USTACK_BASE);
    sbase = CC_const(USTACK_TOP)-slen;

    rval = CC_mem_addmap(CC_ict_mem(realct),&sbase,&slen,0);
    if (rval) {
      fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	      CC_mem_errstring(rval),argv[0]);
      return -1;
    }

    // so the Solaris source codesays...
    int num_auxv_entries = CC_ict_use_interp(realct) ? 13 : 8;

    stacksize = (4 /* argc + argv pad + envp pad + endpad */ 
                 + 2 /* scratch */+(2*num_auxv_entries)  /* auxv */) 
	* CC_addrsize;

    for (sp=envp,numenv=0;*sp;sp++,numenv++) {
      stacksize+=CC_addrsize; /* for envp itself */
      stacksize+=strlen(*sp)+1; /* for contents */
    }
    for (i=0;i<argc;i++) {
      stacksize+=CC_addrsize; /* for argv pointer */
      stacksize+=strlen(argv[i])+1;
    }
    stacksize += 4 * CC_addrsize; /* for aux vector */
    stacksize = (stacksize+15) & ~15;  /* align the stack */

    sbase = CC_const(USTACK_TOP) - stacksize;

    start_stack = sbase;

    stp8 = ((char *)&stemp8 + 
           ((CC_addrsize != sizeof(stemp8) &&COM_end_switched()) ? 4 : 0));

    /* write argc */
    stemp8 = COM_end_h2e((COM_addrtype)argc,COM_addrtype);
    sbase += 16;
    slen = CC_addrsize;
    if (OS_ict_emuvar(realct,EMU_debugcontextload))
      fprintf(COM_stderr(realct),"Argc at " LSE_emu_addr_t_print_format "\n",
	      sbase);

    rval = CC_mem_write(CC_ict_mem(realct),&sbase,&slen,stp8);
    if (rval) {
      fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	      CC_mem_errstring(rval),argv[0]);
      return -1;
    }
  
    /* copy arguments */
    saddr = (numenv + argc + 2)*CC_addrsize + sbase + CC_addrsize + 
		(2*num_auxv_entries)*CC_addrsize; /* first string location */

    sbase += CC_addrsize;
	
    if (OS_ict_emuvar(realct,EMU_debugcontextload))
      fprintf(COM_stderr(realct),"Starting %d argument strings at " 
		LSE_emu_addr_t_print_format "\n",
	      argc, saddr);
    vargv = saddr;	
    for (i=0;i<argc;i++) {
      /* write argv value */

      slen = CC_addrsize; 
      stemp8 = COM_end_h2e_addr((COM_addrtype())saddr);
      rval = CC_mem_write(CC_ict_mem(realct),&sbase,&slen,stp8);

      if (rval) {
        fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	        CC_mem_errstring(rval),argv[0]);
        return -1;
      }
      sbase += CC_addrsize;
      slen = strlen(argv[i])+1;
      rval = CC_mem_write(CC_ict_mem(realct),&saddr,&slen,argv[i]);
      if (rval) {
        fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	        CC_mem_errstring(rval),argv[0]);
        return -1;
      }
      saddr += slen;
    }
    slen=CC_addrsize;
    stemp8 = 0;
    rval = CC_mem_write(CC_ict_mem(realct),&sbase,&slen,stp8);
    if (rval) {
      fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	      CC_mem_errstring(rval),argv[0]);
      return -1;
    }
    sbase += CC_addrsize;

    /* copy environment */

    if (OS_ict_emuvar(realct,EMU_debugcontextload))
      fprintf(COM_stderr(realct),
	"Starting %d environment strings at " LSE_emu_addr_t_print_format "\n",
	      numenv, saddr);
    venvp = saddr;
    for (i=0;i<numenv;i++) {
      /* write envp value */
      slen = CC_addrsize;
      stemp8 = COM_end_h2e_addr((COM_addrtype())saddr);

      rval = CC_mem_write(CC_ict_mem(realct),&sbase,&slen,stp8);

      if (rval) {
        fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	        CC_mem_errstring(rval),argv[0]);
        return -1;
      }
      sbase += CC_addrsize;
      slen = strlen(envp[i])+1;
      rval = CC_mem_write(CC_ict_mem(realct),&saddr,&slen,envp[i]);
      if (rval) {
        fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	        CC_mem_errstring(rval),argv[0]);
        return -1;
      }
      saddr += slen;
    }
    slen=CC_addrsize;
    stemp8 = 0;
    rval = CC_mem_write(CC_ict_mem(realct),&sbase,&slen,stp8);
    if (rval) {
      fprintf(COM_stderr(realct),"Memory error: %s for binary file %s\n",
	      CC_mem_errstring(rval),argv[0]);
      return -1;
    }
    sbase += CC_addrsize;


    /* aux vector... it has something to do with DLLs, but we do need
     * to put in the page size.  Note that we cannot use the ELF header
     * file definition because it uses void * for the pointer value instead
     * of something guaranteed to be 64 bits wide.
     */

    if (OS_ict_emuvar(realct,EMU_debugcontextload))
      fprintf(COM_stderr(realct),
	"Starting aux vector (%d entries) at " LSE_emu_addr_t_print_format 
	"\n", num_auxv_entries, sbase);

    rvalint = 0;
    if (CC_ict_use_interp(realct)) { /* handle dynamic library entries */
      rvalint = (add_aux_entry(realct,is64bit,argv[0],&sbase, 3 /* AT_PHDR */,
		 	       CC_ict_baseaddr(realct) + 
		  		(is64bit ? topheader.h64.e_phoff : 
				topheader.h32.e_phoff)) |
	         add_aux_entry(realct,is64bit,argv[0],&sbase, 4/* AT_PHENT */, 
			       is64bit ? topheader.h64.e_phentsize : 
			      		topheader.h32.e_phentsize) |
	         add_aux_entry(realct,is64bit,argv[0],&sbase, 5/* AT_PHNUM */,
				is64bit ? topheader.h64.e_phnum :
			          topheader.h32.e_phnum) |
	         add_aux_entry(realct,is64bit,argv[0],&sbase, 9/* AT_ENTRY */, 
				program_entry) |
	         add_aux_entry(realct,is64bit,argv[0],&sbase, 
				2016/* AT_SUN_LDDATA */, 
				CC_ict_loadaddr(realct) ? 
				  CC_ict_loadaddr(realct) : 0));
    }
    rvalint |= (add_aux_entry(realct,is64bit,argv[0],&sbase, 7 /* AT_BASE */, 
			      CC_ict_interpaddr(realct)) |
	        add_aux_entry(realct,is64bit,argv[0],&sbase, 8 /* AT_FLAGS */, 
				CC_const(AT_FLAGS)) |
	        add_aux_entry(realct,is64bit,argv[0],&sbase, 6 /* AT_PAGESZ */,
				CC_const(PAGE_SIZE)) |
	        add_aux_entry(realct,is64bit,argv[0],&sbase, 
				2017 /* AT_SUN_AUXFLAGS */, 
				0x2 /* AF_SUN_HWCAPVERIFY */) |
	        add_aux_entry(realct,is64bit,argv[0],&sbase, 
				2009 /* AT_SUN_HWCAP */, CC_const(HWCAP)) |
	        add_aux_entry(realct,is64bit,argv[0],&sbase, 
				0 /* AT_NULL */, 0));

    if (rvalint) return -1;

    if (OS_ict_emuvar(realct,EMU_debugcontextload)) {
      fprintf(COM_stderr(realct),
              LSE_emu_addr_t_print_format " " 
              LSE_emu_addr_t_print_format " " 
              LSE_emu_addr_t_print_format "\n",
	      start_stack,sbase,saddr);
      fprintf(COM_stderr(realct),"Entry=" LSE_emu_addr_t_print_format "\n",
	      CC_ict_true_startaddr(realct));
    }

    // There also ought to be some way to say, "hey", this is a new address
    // space and we need to redo the memory manager to do right things for
    // the new ABI.  So we will put in a hook
    CC_execveguts_hook()

    return Solaris_start_thread(realct,CC_ict_startaddr(realct), start_stack,
	                      argc,vargv,venvp);

    return -1;
    
  } else {
    return -CC_const(ENOEXEC);
  }
  return 0;
}

int Solaris_context_load(CC_isacontext_t *realct, int argc, 
                        char **argv, char **envp) {
  /* This routine is essentially execve.  The fork does not
   * need to allocate the context structure, as that will be done in context
   * init
   */
  return Solaris_execveguts(realct,argv[0],argc,argv,envp);
}

extern void Solaris_schedule_swcontext(CC_isacontext_t *);

void Solaris_schedule(LSE_emu_interface_t *ifc) {
  LSE_emu_contextno_t cno;
  Solaris_context_t *osct=OS_interface(ifc)->initcontext.next_run;
  
  for (cno=1;cno<=(LSE_emu_hwcontexts_total);cno++) {

    if ((LSE_emu_hwcontexts_table)[cno].automap && 
	(LSE_emu_hwcontexts_table)[cno].valid && 
	(LSE_emu_hwcontexts_table)[cno].emuinstid 
					== ifc->emuinstid &&
	!(LSE_emu_hwcontexts_table)[cno].mappedcno) {
      /* free hw context */
      while (osct != &OS_interface(ifc)->initcontext) {
	LSE_emu_contextno_t tcno;
	tcno = -CC_ict_globalcno(osct->isacont); /* make positive */
	if ((LSE_emu_swcontexts_table)[tcno].automap &&
	    (LSE_emu_swcontexts_table)[tcno].valid && 
	    !(LSE_emu_swcontexts_table)[tcno].mappedcno &&
	    !CC_ict_done(osct->isacont)) { 
            /* unscheduled software context */
#ifdef REMOVE
          osct->next_run->prev_run = osct->prev_run;
          osct->prev_run->next_run = osct->next_run;
          osct->next_run = NULL;
#endif
          LSE_emu_update_context_map(cno,-tcno);
          return;
        } /* if */
        osct = osct->next_run;
      } /* while */
      return; /* no more free contexts */
    } /* if */
  } /* for hardware */
}

/****************************** checkpointing *************************/

#if (LSE_emu_emulator_has_checkpoint)

/*
 * Write OS context information to a checkpoint file.
 */
LSE_chkpt::error_t 
Solaris_chkpt_write_context(LSE_chkpt::file_t *cpFile,
	         	  LSE_emu_chkpt_cntl_t *ctl, 
			  CC_isacontext_t *realct) {
  LSE_chkpt::data_t *tdp, *dp;
  LSE_chkpt::error_t cerr;
  Solaris_context_t *osct;

  tdp = LSE_chkpt::build_sequence(NULL);
  if (!tdp) return LSE_chkpt::error_OutOfMemory;

  osct = OS_ict_osinfo(realct);
  
  dp = LSE_chkpt::build_unsigned(tdp,osct->heapbreak);
  if (!dp) goto nobuild;
  dp = LSE_chkpt::build_signed(tdp,osct->exit_code);
  if (!dp) goto nobuild;
  dp = LSE_chkpt::build_signed(tdp,osct->pid);
  if (!dp) goto nobuild;
  dp = LSE_chkpt::build_signed(tdp,osct->ppid);
  if (!dp) goto nobuild;
  dp = LSE_chkpt::build_signed(tdp,osct->gid);
  if (!dp) goto nobuild;
  dp = LSE_chkpt::build_signed(tdp,osct->egid);
  if (!dp) goto nobuild;
  dp = LSE_chkpt::build_signed(tdp,osct->uid);
  if (!dp) goto nobuild;
  dp = LSE_chkpt::build_signed(tdp,osct->euid);
  if (!dp) goto nobuild;
  dp = LSE_chkpt::build_signed(tdp,osct->is64bit);
  if (!dp) goto nobuild;

  tdp->update_size();
  cerr = cpFile->write_to_segment(TRUE, tdp);
  if (cerr) return cerr;

  return LSE_chkpt::error_None;
nobuild:
  delete (tdp);
  return LSE_chkpt::error_OutOfMemory;
}

/*
 * Parse OS context information from a checkpoint data structure
 */
LSE_chkpt::error_t 
Solaris_chkpt_parse_context(LSE_chkpt::data_t *tdp,
	         	  LSE_emu_chkpt_cntl_t *ctl, 
			  CC_isacontext_t *realct) {
  LSE_chkpt::data_t *dp;
  Solaris_context_t *osct;

  dp = tdp->oldestChild;
  osct = OS_ict_osinfo(realct);
  
  if (!dp) return LSE_chkpt::error_FileFormat;
  osct->heapbreak = dp->content.uint64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  osct->exit_code = dp->content.int64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  osct->pid = dp->content.int64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  osct->ppid = dp->content.int64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  osct->gid = dp->content.int64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  osct->egid = dp->content.int64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  osct->uid = dp->content.int64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  osct->euid = dp->content.int64Val;
  dp = dp->sibling;

  if (dp) { 
    osct->is64bit = dp->content.int64Val;
    dp = dp->sibling;
  } else {
    osct->is64bit = m4_ifelse(CC_elftype,64,1,0);
  }

  /* should be done */
  if (dp) return LSE_chkpt::error_FileFormat;
  return LSE_chkpt::error_None;
}

/*
 * Read OS context information from a checkpoint file.
 */
LSE_chkpt::error_t 
Solaris_chkpt_read_context(LSE_chkpt::file_t *cpFile,
	         	  LSE_emu_chkpt_cntl_t *ctl, 
			  CC_isacontext_t *realct) {
  LSE_chkpt::data_t *dp;
  LSE_chkpt::error_t cerr;

  cerr = cpFile->read_from_segment(NULL,&dp);
  if (cerr) return cerr;

  cerr = Solaris_chkpt_parse_context(dp,ctl,realct);
  delete (dp);
  return cerr;
}


/*
 * Write OS memory information to a checkpoint file.
 */
LSE_chkpt::error_t 
Solaris_chkpt_write_memory(LSE_emu_interface_t *ifc,
			 LSE_chkpt::file_t *cpFile,
	         	 LSE_emu_chkpt_cntl_t *ctl, int memID) {
  struct Solaris_vm_area_s *vmm;
  LSE_chkpt::data_t *dp;
  LSE_chkpt::error_t cerr;

  /* indefinite sequence to hold these */
  dp = LSE_chkpt::build_header(NULL, LSE_chkpt::TAG_SEQUENCE, 
			      LSE_chkpt::CONSTRUCTED, -1);
  if (!dp) return LSE_chkpt::error_OutOfMemory;
  cerr = cpFile->write_to_segment(TRUE, dp);
  if (cerr) return cerr;

  /* iterate across the segments */
  for (vmm = (struct Solaris_vm_area_s *)
             CC_OSmeminfo_ptr(memID, ifc)->allocated_regions; vmm; 
       vmm = vmm->next) {
    dp = LSE_chkpt::build_unsigned(NULL,vmm->start);
    if (!dp) return LSE_chkpt::error_OutOfMemory;
    cerr = cpFile->write_to_segment(TRUE,dp);
    if (cerr) return cerr;

    dp = LSE_chkpt::build_unsigned(NULL,vmm->end);
    if (!dp) return LSE_chkpt::error_OutOfMemory;
    cerr = cpFile->write_to_segment(TRUE,dp);
    if (cerr) return cerr;
  }

  /* terminate the sequence */

  dp = LSE_chkpt::build_indefinite_end(NULL);
  if (!dp) return LSE_chkpt::error_OutOfMemory;
  cerr = cpFile->write_to_segment(TRUE, dp);
  if (cerr) return cerr;

  return LSE_chkpt::error_None;
}

/*
 * Read OS memory information from a checkpoint file.
 */
LSE_chkpt::error_t 
Solaris_chkpt_read_memory(LSE_emu_interface_t *ifc,
			LSE_chkpt::file_t *cpFile,
	         	LSE_emu_chkpt_cntl_t *ctl, int memID) {
  struct Solaris_vm_area_s *vmm, *q, **pp;
  LSE_chkpt::data_t *dp;
  LSE_chkpt::error_t cerr;

  /* iterate across the regions, freeing up the old stuff */
  for (vmm = (struct Solaris_vm_area_s *)
       CC_OSmeminfo_ptr(memID, ifc)->allocated_regions; vmm; ) {
    q = vmm->next;
    free(vmm);
    vmm = q;
  }

  CC_OSmeminfo_ptr(memID, ifc)->allocated_regions = NULL;

  /* indefinite header for the mappings */
  cerr = cpFile->read_taglen_from_segment(NULL,&dp);
  if (cerr) return cerr;
  delete (dp);
  pp = (struct Solaris_vm_area_s **)
        (&(CC_OSmeminfo_ptr(memID, ifc)->allocated_regions));

  do {
      LSE_emu_addr_t start, end;

      /* pull in a mapping */
      cerr = cpFile->read_from_segment(NULL,&dp);
      if (cerr) return cerr;
      if (!dp) break;
      start = dp->content.uint64Val;
      delete (dp);

      cerr = cpFile->read_from_segment(NULL,&dp);
      if (cerr) return cerr;
      end = dp->content.uint64Val;
      delete (dp);

      q = (struct Solaris_vm_area_s *)malloc(sizeof(struct Solaris_vm_area_s));
      if (!q) return LSE_chkpt::error_OutOfMemory;
    
      q->start = start; 
      q->end = end;
      q->next = NULL;
      
      *pp = q;
      pp = &q->next;
  } while (1);

  return LSE_chkpt::error_None;
}

#endif

')m4_dnl OS_helpers

m4_dnl ****************************************************************
m4_dnl ************************ Implementation support ****************
m4_dnl ****************************************************************

m4_dnl Common code for memory mapping
m4_dnl $1 = desired address; $2 = length; $3 = prot ; $4 = flags 
m4_dnl $5 = fd; $6 = offset; $7 = return variable to use
m4_define(`OS_mmap',`
#if 0
  /* ii->privatef.fault = (int)IA64_fault_notimplemented; */
  CC_sys_set_return(realct,0,-1,i,i);
  CC_sys_set_return(realct,1,CC_const(ENOMEM),i,i);
  goto syserr;
#endif
{
   LSE_emu_addr_t heapbreak;
   LSE_emu_addr_t mlen = $2;
   off_t sres=0;
   int mres=0;

   if (($3 & CC_const(PROT_WRITE)) && (!($4 & CC_const(MAP_PRIVATE)))) {
     if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
       fprintf(COM_stderr(realct), "can only map writeable pages privately for now\n");
     return -1;
   }

   if (($4 & CC_const(MAP_FIXED))) {
     heapbreak = $1;
   } else {
     heapbreak = Solaris_find_free_region_top(realct, mlen);
   }

   ret = Solaris_allocate_region(realct, heapbreak, mlen);
   if(ret < 0) 
     OS_report_err(ENOMEM);
   Solaris_do_map(CC_ict_mem(realct),heapbreak,mlen,0);

  
   if(!($4 & CC_const(MAP_ANONYMOUS))) {
     CC_chkpt_guard(`sres = lseek($5,$6,SEEK_SET);')
     CC_chkpt_result(off_t,sres);
     if (sres == (off_t)-1) {
       CC_chkpt_result(int,errno);
       goto syserr;
     }

     /* the memories have a nice read from file routine defined, but we
      * do still have to set up the checkpoints...
      */
     CC_chkpt_guard(`mres = CC_mem_read_from_file(CC_ict_mem(realct),
                    &heapbreak,&mlen,NULL,$5);')

     CC_chkpt_result(int,mres);

     if (mres) {
       OS_report_err(ENOMEM); /* may not be the right value */
     }
     CC_chkpt_result(tmem,heapbreak,mlen);
   } else {
     CC_mem_memset(CC_ict_mem(realct),&heapbreak,0,&mlen);
   }

 
   $7 = heapbreak;
}
')

m4_define(`OS_munmap',`
  /* ii->privatef.fault = (int)IA64_fault_notimplemented; */
  CC_sys_set_return(realct,0,-1,i,i);
  CC_sys_set_return(realct,1,CC_const(ENOMEM),i,i);
  goto syserr;
')

m4_dnl *****************************************************************
m4_dnl ************************** Implementations **********************
m4_dnl *****************************************************************
m4_dnl PLEASE KEEP THESE IN ALPHABETICAL ORDER... EXCEPTIONS CAN BE MADE
m4_dnl FOR VARIATIONS FOUND... such as stat vs. newstat or getrlimit vs.
m4_dnl old_getrlimit .. IN THOSE CASES, KEEP RELATED FUNCTIONS TOGETHER
m4_dnl
m4_dnl Coding standards:
m4_dnl
m4_dnl 1) All syscalls must trace their arguments and results when
m4_dnl    EMU_trace_syscalls is TRUE.
m4_dnl    a) Try to print out strings when possible, but do so only when
m4_dnl       you have guaranteed that the string is safe to print.  See
m4_dnl       `open' for a good example of dealing with this
m4_dnl    b) when an error is returned, print both the -1 and the return 1
m4_dnl       value (which becomes errno)
m4_dnl
m4_dnl 2) Do not add a syscall without adding checkpointing
m4_dnl    a) do not start the checkpoint until after the initial syscall
m4_dnl       trace message
m4_dnl    b) For structures, we checkpoint the pointer as an argument, but
m4_dnl       checkpoint the contents of the target structure *in host 
m4_dnl       address space* as an argument or result.

OS_impl(`_sysctl',
`
  LSE_emu_addr_t argptr;
  struct CC_type(__sysctl_args) args;

  argptr = CC_arg(0,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
	    "SYSCALL: _sysctl(0x" LSE_emu_addr_t_print_format ") = ",
   	    argptr);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,argptr);
 
  CC_memcpy_t2h(&args, argptr, sizeof(args));
  /* Fix endianness */

  /* Just return error for now.  If we end up *really* needing this
     syscall we will have to be more sophisticated */
  OS_report_results(0,-1);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "%d\n",(int)OS_sys_get_return(1));

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl _sysctl

OS_impl(`access',
`
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  LSE_emu_addr_t fname;
  int mode;
  int ret=0;
  int didfname = 0;

  fname = CC_arg(0,ptr,i);
  mode  = CC_arg(1,ptr,i);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: access(\"%s\",%i) = ",
	    fnamebuf,mode);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(i,mode);
  CC_chkpt_arg(str,fnamebuf,(strlen(fnamebuf)+1));

  CC_rewrite_filename(fnamebuf)

  CC_chkpt_guard(`ret = access(fnamebuf,mode);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: access(0x" LSE_emu_addr_t_print_format "%d) = ",
              fname, mode);
    } 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
')m4_dnl access

OS_impl(`alarm',
`
  int seconds;
  int ret=0;

  seconds = CC_arg(0,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: alarm(%d) = ",
              seconds);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,seconds);

  /* This syscall just sets a priority.  Hopefully, having an empty
     definition is safe */ 
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);

  OS_sys_return();
m4_ifdef(`CC_chkpt_implemented',`syserr:')
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl alarm

OS_impl(`brk',
`
  LSE_emu_addr_t newbrk, brkdiff, brkaddr;
  LSE_mgen_error_t memerr;

  newbrk = CC_arg(0,ptr);
  brkaddr = OS_osvar(heapbreak);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: brk(0x" LSE_emu_addr_t_print_format ") = ",
	newbrk);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,newbrk);

  /* NOTE: Solaris is a MORECORE_CLEARS=2 OS as far as glibc is concerned.  This
   * means that we have to clear memory between old and new when new > old
   */
  if (newbrk <= brkaddr) {
    OS_report_results(0, brkaddr);
  }
  else { /* get more heap space */
    /* need to figure out how to limit this flexibly...
     */
    if (0) {
      OS_report_err(CC_const(ENOMEM));
    } else {
      brkdiff = newbrk - brkaddr;
      memerr=CC_mem_addmap(CC_ict_mem(OS_ct),&brkaddr,
                           &brkdiff,LSE_mgen_flags_ZERO);

      if (!memerr) {
        /* 
	  Now try to allocate the region.  A brief explanation:
          Some versions of glibc (e.g. 2.3.3) can use MMAP to
          malloc large regions of memory.  This implementation of
          MMAP favors lower addresses; that is, it picks the lowest
          address where it can safely cram itself.  Unfortunately,
          if you allocate a large enough chunk and your address space
          is sufficiently limited, then you may run into trouble 
          when MMAP cannot place the region below the heap and decides
          instead to put it above the heap.  Now if the MMAP does not know 
	  about the addresses that have been brked, then you will trash
          the heap.  But brk needs to be fixed to report out of mem if
          the brk address lands in the MMAP region.
	*/
        int ret;
	ret=Solaris_allocate_region(realct, brkaddr, brkdiff);
        if (!ret) 
	{
          OS_osvar(heapbreak)=newbrk;
          OS_report_results(0, newbrk);
        } else {
          OS_report_err(CC_const(ENOMEM));
        }
      } else {
	OS_report_err(CC_const(ENOMEM));
      }
    }
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"0x" LSE_emu_addr_t_print_format "\n",
	    OS_sys_get_return(1));
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl brk

OS_impl(`chdir',
`
  int ret=0;
  LSE_emu_addr_t fname;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didfname = 0;

  fname = CC_arg(0,ptr,i);
  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "SYSCALL: chdir(\"%s\") = ",
            (char *)fnamebuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(str,fnamebuf,(strlen(fnamebuf)+1));

  CC_rewrite_filename(fnamebuf)

  CC_chkpt_guard(`ret=chdir(fnamebuf);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }

  if(OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: chdir(0x" LSE_emu_addr_t_print_format ") = ",
              fname);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl chdir


m4_dnl This is almost certainly wrong.
OS_impl(`fork1',
`/* parameters are flags, stack addr, stack size */
  int flags = CC_arg(0,i,ptr,ptr);
  LSE_emu_addr_t stack = CC_arg(1,i,ptr,ptr);
  LSE_emu_addr_t size = CC_const(CLONE_STACK_SIZE);
  CC_isacontext_t *newct;
  int rval;
  boolean automap;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: clone(%d,0x" LSE_emu_addr_t_print_format 
	    ") = ", flags, stack);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,flags);
  CC_chkpt_arg(ptr,stack);
  CC_chkpt_arg(ptr,size);

  /* Create the new context, copying/sharing from parent.  Exact semantics
   * depends upon the instruction set, but generally it should be sharing
   * memory at this point.  Later on, in cloneguts, we will work out
   * all the sharing.
   */

  CC_isa_context_copy(&newct,OS_ct)

  /* set up starting address and context number; do not forget that
   * software context numbers are negative
   */
  CC_ict_startaddr(newct) = CC_following_pc(ii);
  automap = COM_emu_swcontexts_table(OS_ct)[-CC_ict_globalcno(newct)].automap;
  CC_ict_globalcno(newct) 
    = COM_context_announce(OS_ct, newct, COM_emu_instid(OS_ct), automap);

  /* now do clone stuff shared with clone from init. */

  rval=Solaris_cloneguts(newct,OS_ct,flags,stack-size,size);

  if (rval) {
    OS_report_err(ENOMEM);
  } else {
    OS_report_results(0,(OS_ict_osinfo(newct))->pid);
    /* NOTE: copy_thread sets up the new thread return values */
/*
    CC_sys_set_return(OS_ct,2,0,i,i);
    CC_sys_set_return(newct,0,0,i,i);
    CC_sys_set_return(newct,1,0,i,i);
    CC_sys_set_return(newct,2,1,i,i);
*/
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",(int)OS_sys_get_return(1));
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl clone


OS_impl(`close',
`
  int fd = CC_arg(0,i);
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),"SYSCALL: close(%d) = ", fd);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);

  CC_chkpt_guard(`ret = close(fd);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl close

/*
	FIXME: connect is a stub implementation; it does not even
	       record its parameters properly
 */

OS_impl(`connect',
`
  int fd = CC_arg(0,i);
  int ret= -1;
  int myerrno = CC_const(ENOSYS);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),"SYSCALL: connect(%d,...) = ", fd);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);

  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,myerrno);
    OS_report_err(myerrno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl connect

/*
	FIXME: context is a stub implementation; it does not even
	       record its parameters properly
 */
OS_impl(`context',
`
  LSE_emu_addr_t targbuf;
  int oper;
  int ret=0;
  int myerrno = CC_const(ENOSYS);

  oper = CC_arg(0,i,ptr);
  targbuf = CC_arg(1,i,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: context(%d,0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    oper,targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,oper);
  CC_chkpt_arg(ptr,targbuf);
  
  if (oper == 0) {
    CC_getcontext(realct,targbuf);
    ret = 0;
  } else {
    ret = -1;
  }

  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,myerrno);
    OS_report_err(myerrno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl context

OS_impl(`dup',
`
  int fd = CC_arg(0,i);
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),"SYSCALL: dup(%d) = ", fd);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);

  CC_chkpt_guard(`ret = dup(fd);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl dup


OS_impl(`exit',
`
  int ret=0, parm;

  parm = CC_arg(0,i);
  ret = parm & 0xff;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: exit(%d) = ",parm);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,parm);

  CC_following_pc(ii) = CC_curr_pc(ii); /* in case of sim error */

  /* remove from run queue */
  OS_osvar(next_run)->prev_run = OS_osvar(prev_run);
  OS_osvar(prev_run)->next_run = OS_osvar(next_run);
  OS_osvar(next_run) = NULL;  
  OS_set_curr_context_done();

  COM_set_exit_status(OS_ct,ret);
  OS_osvar(exit_code) = ret;
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
m4_ifdef(`CC_chkpt_implemented',`syserr:')
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl exit

OS_impl(`exit_group',
`
  int ret=-1, parm;

  parm = CC_arg(0,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: exit_group(%d) = ",parm);

  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl exit_group

OS_impl(`fchmod',
` 
  int fd;
  int mode;
  int ret=0;

  fd    = CC_arg(0,i,i);
  mode  = CC_arg(1,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: fchmod(%i,%i) = ",
	    fd,mode);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(i,mode);

  CC_chkpt_guard(`ret = fchmod(fd,mode);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
')m4_dnl fchmod

OS_impl(`fchown',
` 
  int fd;
  uid_t owner;
  gid_t group;
  int ret=0;

  fd    = CC_arg(0,i,i,i);
  owner  = CC_arg(1,i,i,i);
  group  = CC_arg(2,i,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: fchown(%i,%i,%i) = ",
	    fd,owner,group);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(i,owner);
  CC_chkpt_arg(i,group);

  CC_chkpt_guard(`ret = fchown(fd,owner,group);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
')m4_dnl fchown

OS_impl(`fcntl',
`
  int fd;
  int cmd;
  long arg;
  int ret=0;
  fd = (int)CC_arg(0,i,i,i);
  cmd = (int)CC_arg(1,i,i,i);
  arg = CC_arg(2,i,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: fcntl(%d,%d,%ld) = ",fd,cmd,arg);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(i,cmd);
  CC_chkpt_arg(i,arg);

  switch (cmd) {
    case F_DUPFD :
    case F_GETFD :
    case F_SETFD :
    case F_GETFL :
    case F_SETFL :
	CC_chkpt_guard(`ret = fcntl(fd, cmd, arg);')
	CC_chkpt_result(int,ret);
	break;
    /* NOTE: F_GETOWN/F_SETOWN/F_GETSIG/F_SETSIG are Linux-specific and may
     * not exist on the host sytem.  They are not implemented so far...
     */
    default:
      if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
        fprintf(COM_stderr(realct),"unimplemented cmd\n");
      CC_chkpt_finish();
      return -1;
  }

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
') m4_dnl fcntl

OS_impl(`fpathconf',
`
  int name, fd;
  int ret=0;

  fd = CC_arg(0,i,i);
  name = CC_arg(1,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),
	    "SYSCALL: readlink(%d,%d) = ", fd, name);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(i,name);

  CC_chkpt_guard(`ret=fpathconf(fd,name);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    CC_chkpt_result(int,ret);
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl fpathconf

OS_impl(`fstat',
`
  int fd;
  LSE_emu_addr_t targbuf;
  struct stat buf;
  struct CC_type(stat) tempbuf;
  int ret=0;

  fd = (int)CC_arg(0,i,ptr);
  targbuf = CC_arg(1,i,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
	    "SYSCALL: fstat(%d,0x" LSE_emu_addr_t_print_format ") = ",
	    fd,targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(ptr,targbuf);

  CC_chkpt_guard(`ret = fstat(fd, &buf);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  /* 
   * note that these values will depend upon the HOST system
   */
  CC_chkpt_guard(`
    memset(&tempbuf,0,sizeof(tempbuf));
    COM_struct_end_h2e(tempbuf,buf,stat);
  ')

  CC_chkpt_arg(i,sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl oldfstat

OS_impl(`fstat64',
`
  int fd;
  LSE_emu_addr_t targbuf;
  struct stat buf;
  struct CC_type(stat64) tempbuf;
  int ret=0;

  fd = (int)CC_arg(0,i,ptr);
  targbuf = CC_arg(1,i,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
	    "SYSCALL: fstat64(%d,0x" LSE_emu_addr_t_print_format ") = ",
	    fd,targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(ptr,targbuf);

#if 1
#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(`ret = fstat64(fd, &buf);')
#else
  CC_chkpt_guard(`ret = fstat(fd, &buf);')
#endif
#else
  CC_chkpt_guard(`ret = -1;')
#endif
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    OS_report_err(errno);
  }
  /* 
   * note that these values will depend upon the HOST system
   */

  CC_chkpt_guard(`memset(&tempbuf,0,sizeof(tempbuf));
	COM_struct_end_h2e(tempbuf,buf,stat64);')

  CC_chkpt_arg(i,sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  /*
  printf("%"PRIx64" = %"PRIx64"\n",tempbuf.LSE_st_dev,(uint64_t)buf.st_dev);
  printf("%"PRIx64" = %"PRIx64"\n",tempbuf.LSE_st_ino,(uint64_t)buf.st_ino);
  printf("%lx = %lx\n",tempbuf.LSE_st_mode,buf.st_mode);
  printf("%lx = %lx\n",tempbuf.LSE_st_nlink,buf.st_nlink);
  printf("%lx = %lx\n",tempbuf.LSE_st_uid,buf.st_uid);
  printf("%lx = %lx\n",tempbuf.LSE_st_gid,buf.st_gid);
  printf("%"PRIx64" = %"PRIx64"\n",tempbuf.LSE_st_rdev,(uint64_t)buf.st_rdev);
  */

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl fstat64

m4_dnl Note that the getcwd C library function in linux can malloc some
m4_dnl additional space....  but the OS call will not.  This is the OS
m4_dnl call, so the return value is going to be the buffer that was
m4_dnl passed to it unless there was an error, in which case it will be -1
OS_impl(`getcwd',
`
  char *ret;
  LSE_emu_addr_t fname;
  char *tempspace=NULL;
  uint64_t size;
  boolean goodresult=FALSE;
  boolean tempalloc=FALSE;
  char mybuf[1024];

  fname = CC_arg(0,ptr,i);
  size = CC_arg(1,ptr,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),
	    "SYSCALL: getcwd(" LSE_emu_addr_t_print_format ",%0lld) = ",
            fname,size);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(i,size);

  tempspace = (char *)malloc(size);
  if (tempspace) tempalloc = TRUE;
  else {
    size = 1024;
    tempspace = mybuf;
  }
  CC_chkpt_guard(`
    ret=getcwd(tempspace,size);
    goodresult = ret != NULL;
  ')
  CC_chkpt_result(int,goodresult);

  if (goodresult) {
    CC_chkpt_result(str,tempspace,size);
    OS_report_results(0,strlen(tempspace)+1);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }

  CC_memcpy_h2t(fname,tempspace,size);

  if (tempalloc) free(tempspace);
  if(OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), 
	    "0x" LSE_emu_addr_t_print_format "\n", (uint64_t)fname);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
      fprintf(COM_stderr(realct),
              "SYSCALL: getcwd(0x" LSE_emu_addr_t_print_format ",%d) = ",
              fname,(int)size);
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  if (tempalloc) free(tempspace);
  OS_syserr_handler();
')m4_dnl getcwd

OS_impl(`getgid',
`
  int ret=0,ret2=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: getgid() = ");

  CC_chkpt_start($1);
  CC_chkpt_guard(`ret2=OS_osvar(egid); ret = OS_osvar(gid);')
  CC_chkpt_result(int,ret);
  CC_chkpt_result(int,ret2);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d %d\n",ret,ret2);
  OS_report_results(0,ret);
  CC_sys_set_return(realct,2,ret2,i,i,i);
  OS_sys_return();
m4_ifdef(`CC_chkpt_implemented',`syserr:')
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl getgid

m4_ifelse(`
m4_dnl  We will implement pid as the global context number (NOT!)
OS_impl(`getpagesizes',
`
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: getpagesize() = ");

  CC_chkpt_start($1);
  CC_chkpt_guard(`ret = CC_const(PAGE_SIZE);')
  CC_chkpt_result(int,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl getpagesize
')

OS_impl(`gethrestime',m4_dnl a not so good implementation
` time_t ret;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: get_hrestime() = ");

  CC_chkpt_start($1);
  
#if defined (__SVR4) && defined (__sun)
  CC_chkpt_guard(`ret = time();')
#else
  CC_chkpt_guard(`ret = time(NULL);')
#endif
  CC_chkpt_result(time_t,ret);
  CC_sys_set_return(realct,2,0,i,i,i); m4_dnl Just set ns to 0

  if (ret == -1) {
    CC_chkpt_result(int, errno);
    OS_report_err(errno);
  }
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d 0\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')

m4_dnl  We will implement pid as the global context number (NOT!)
OS_impl(`getpid',
`
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: getpid() = ");

  CC_chkpt_start($1);
  CC_chkpt_guard(`ret = OS_osvar(pid);')
  CC_chkpt_result(int,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);
  OS_report_results(0,ret);
  OS_sys_return();
m4_ifdef(`CC_chkpt_implemented',`syserr:')
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl getpid

OS_impl(`getrlimit',
`
  int resource;
  LSE_emu_addr_t targrlim;
  struct rlimit rlim;
  struct CC_type(rlimit) temprlim;
  int ret=0;

  resource = CC_arg(0,i,ptr);
  targrlim = CC_arg(1,i,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: getrlimit(%d,0x" LSE_emu_addr_t_print_format 
            ") = ",
	    resource, targrlim);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,resource);
  CC_chkpt_arg(ptr,targrlim);

  CC_chkpt_guard(`ret = getrlimit(resource, &rlim);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }

  CC_chkpt_guard(`memset(&temprlim,0,sizeof(temprlim));
	COM_struct_end_h2e(temprlim,rlim,rlimit);')

  CC_chkpt_arg(i,sizeof(temprlim)); /* for sanity */
  CC_chkpt_result(hmem,&temprlim,sizeof(temprlim));

  CC_memcpy_h2t(targrlim,&temprlim,sizeof(temprlim)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
') m4_dnl getrlimit


m4_dnl  We will implement uid as the uid on the host
OS_impl(`getuid',
`
  int ret=0, ret2=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: getuid() = ");

  CC_chkpt_start($1);
  CC_chkpt_guard(`ret2=OS_osvar(euid); ret = OS_osvar(uid);')
  CC_chkpt_result(int,ret);
  CC_chkpt_result(int,ret2);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d %d\n",ret,ret2);
  OS_report_results(0,ret);
  CC_sys_set_return(realct,2,ret2,i,i,i);
  OS_sys_return();
m4_ifdef(`CC_chkpt_implemented',`syserr:')
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl getuid

m4_dnl  This is a very, very tricky OS call because the type of the
m4_dnl  third argument depends upon the value of the second argument.
m4_dnl  Often that third argument points at a structure, so we need
m4_dnl  to be very careful about how we make types go.... 
m4_dnl  For real fun, "newer" ioctls are created using a very nice
m4_dnl  encoding that tells you things about the arguments.  Unfortunately,
m4_dnl  there are lots of older, "normal" ones that don't...
m4_dnl  Even more fun.... the exact numbers are *not* standardized, *and*
m4_dnl  not all IOCTLs are present everywhere.  For example, CYGWIN doesn't
m4_dnl  have TCGETS
OS_impl(`ioctl',
`
  int d, request;
  LSE_emu_addr_t argp;
  int ret=0;
  int myerrno = 0;

  d = CC_arg(0,i,i,ptr);
  request = CC_arg(1,i,i,ptr);
  argp = CC_arg(2,i,i,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: ioctl(%d,%d," LSE_emu_addr_t_print_format ") = ", 
	    d, request, argp);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,d);
  CC_chkpt_arg(i,request);
  CC_chkpt_arg(ptr,argp);

  /* The true way of doing this would be to dispatch to the ioctl handler
   * of a device driver.  However, we are above the OS, so we should be
   * able to just pass along.  Unfortunately, we have to deal with structure
   * translation.  Sigh!
   */
  switch (request) {
    /* ioctls found in the benchmarks whose values matter... */

#ifndef NCC
#define NCC NCCS
#endif
    case CC_const(TCGETS):
      {
#ifdef TCGETS
	struct termios t;
	struct CC_type(termios) targt;
	int i;

	CC_chkpt_guard(`ret = ioctl(d,TCGETS,&t);')
	myerrno = errno;
        CC_chkpt_result(int,ret);

        if (ret>=0) {

          CC_chkpt_guard(`
	    memset(&targt,0,sizeof(targt));
	    if (CC_const(NCCS) <= NCCS) {
	      for (i=0;i<CC_const(NCCS);i++) targt.LSE_c_cc[i] = t.c_cc[i];
	    } else {
	      for (i=0;i<NCCS;i++) targt.LSE_c_cc[i] = t.c_cc[i];
	      for (;i<CC_const(NCCS);i++) targt.LSE_c_cc[i] = 0;
	    }
	    COM_struct_end_h2e(targt,t,termios);
#ifndef __linux
	    /* fix up the fields that are not there in Posix  */
            targt._c_ispeed = targt._c_ospeed = COM_end_h2e(9600,CC_type(speed_t));
	    targt._c_line = 0; /* for Linux, this is N_TTY */
#endif
          ')
	
	  CC_chkpt_arg(i,sizeof(targt)); /* for sanity */
	  CC_chkpt_result(hmem,&targt,sizeof(targt));

	  CC_memcpy_h2t(argp,&targt,sizeof(targt));
 	}
#else
#ifdef TCGETA
	struct termio t;
	struct CC_type(termios) targt;
	int i;

        CC_chkpt_guard(`ret = ioctl(d,TCGETA,&t);')
	myerrno = errno;
        CC_chkpt_result(int,ret);

        if (ret>=0) {

          CC_chkpt_guard(`
	    memset(&targt,0,sizeof(targt));
	    if (CC_const(NCC) <= NCC) {
	      for (i=0;i<CC_const(NCC);i++) targt.LSE_c_cc[i] = t.c_cc[i];
	    } else {
	      for (i=0;i<NCC;i++) targt.LSE_c_cc[i] = t.c_cc[i];
	      for (;i<CC_const(NCC);i++) targt.LSE_c_cc[i] = 0;
	    }
	    COM_struct_end_h2e(targt,t,termios);
          ')

	  CC_chkpt_arg(i,sizeof(targt)); /* for sanity */
	  CC_chkpt_result(hmem,&targt,sizeof(targt));

	  CC_memcpy_h2t(argp,&targt,sizeof(targt));
 	}
#else
        ret = -1;
#endif
#endif
      }
      break;

    case CC_const(TCGETA) :
#ifdef TCGETA
      {
	struct termio t;
	struct CC_type(termio) targt;
	int i;

	CC_chkpt_guard(`ret = ioctl(d,TCGETA,&t);')
	myerrno = errno;
        CC_chkpt_result(int,ret)

        if (ret>=0) {

	  CC_chkpt_guard(`
	    memset(&targt,0,sizeof(targt));
	    if (CC_const(NCC) <= NCC) {
	      for (i=0;i<CC_const(NCC);i++) targt.LSE_c_cc[i] = t.c_cc[i];
	    } else {
	      for (i=0;i<NCC;i++) targt.LSE_c_cc[i] = t.c_cc[i];
	      for (;i<CC_const(NCC);i++) targt.LSE_c_cc[i] = 0;
	    }
	    COM_struct_end_h2e(targt,t,termio);
          ')

	  CC_chkpt_arg(i,sizeof(targt)); /* for sanity */
	  CC_chkpt_result(hmem,&targt,sizeof(targt));

	  CC_memcpy_h2t(argp,&targt,sizeof(targt));
 	}
     }
#else
     ret=-1;
#endif
     break;

    /* ioctls found in the benchmarks that I can wave my hands on.... */
    case CC_const(TCSETAW) :
#ifdef TCSETAW
      {

	struct termio t;
	struct CC_type(termio) targt;
	int i;

	CC_memcpy_t2h(&targt,argp,sizeof(targt));

	CC_chkpt_arg(i,sizeof(targt)); /* for sanity */
        CC_chkpt_arg(hmem,&targt,sizeof(targt));

	COM_struct_end_e2h(t,targt,termio);

	for (i=0;i<CC_const(NCC);i++) t.c_cc[i] = targt.LSE_c_cc[i];

	CC_chkpt_guard(`ret = ioctl(d,TCSETAW,&t);')
	myerrno = errno;
        CC_chkpt_result(int,ret);
     }
#else
     ret = -1;
#endif
     break;

    /* Any other ioctls, we will complain about because we do not know
     * how to handle them due to size and endianness concerns in the structure
     * pointed to in the third argument
     */
    default:
	CC_chkpt_guard(`ret = -1;')
	myerrno = EINVAL;
        CC_chkpt_result(int,ret);
	break;
  }
  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,myerrno);
    OS_report_err(myerrno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl ioctl

OS_impl(`kill',
`
  pid_t pid;
  int sig;
  int ret=0;

  pid = CC_arg(0,i,i);
  sig  = CC_arg(1,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: kill(%i,%i) = ",
	    pid,sig);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,pid);
  CC_chkpt_arg(i,sig);

  /* we do not know yet support inter-process signal emulation, so we will	    just pretend we succeeded. */
  ret = 0;  

  if (ret == -1) {
    OS_report_err(errno);
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl kill

OS_impl(`lseek',
`
  int fildes;
  off_t offset;
  int whence;
  int ret=0;

  fildes = CC_arg(0,i,i,i);
  offset = CC_arg(1,i,i,i);
  whence = CC_arg(2,i,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: lseek(%d,%ld,%d) = ", fildes, offset, whence);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fildes);
  CC_chkpt_arg(i,offset);
  CC_chkpt_arg(i,whence);

  CC_chkpt_guard(`ret = lseek(fildes, offset, whence);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl lseek

OS_impl(`llseek',
`
  int fildes;
  int32_t offsethigh, offsetlow;
  int whence;
  int64_t ret=0;

  fildes = CC_arg(0,i,i,i,i);
  offsethigh = CC_arg(1,i,i,i,i);
  offsetlow = CC_arg(2,i,i,i,i);
  whence = CC_arg(3,i,i,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: llseek(%d,%" PRIx32 ",%"PRIx32",%d) = ",
	    fildes, offsethigh, offsetlow, whence);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fildes);
  CC_chkpt_arg(i,offsethigh);
  CC_chkpt_arg(i,offsetlow);
  CC_chkpt_arg(i,whence);

  /* llseek probably does not exist on many platforms */
  
#ifdef HAS__LLSEEK
  CC_chkpt_guard(`ret = _llseek(fildes, offsethigh, offsetlow, 
                                &result, whence);')
  CC_chkpt_result(int,ret);
  if (ret != -1) {
    CC_chkpt_guard(`ret = COM_end_h2e(result,uint64_t);')

    CC_chkpt_arg(i,sizeof(result)); /* for sanity */
    CC_chkpt_result(hmem,&ret,sizeof(ret));
  }
#else
  if (((offsetlow >= 0)&&(offsethigh != 0)) || ((offsetlow < 0) && (offsethigh != -1))) {
    CC_chkpt_guard(`ret = -1;')
    CC_chkpt_result(int,ret);
  } else 
   {
     CC_chkpt_guard(`ret = lseek(fildes,offsetlow,whence);')
     CC_chkpt_result(int,ret);
     /* llseek returns 0 on success */
     if (ret != -1)
     {
       CC_chkpt_arg(i,sizeof(result)); /* for sanity */
       CC_chkpt_result(hmem,&ret,sizeof(ret));
     }
   }
#endif

  if (ret != -1) {
    OS_report_results(0,(ret>>32) & 0xffffffffU);
    CC_sys_set_return(realct,2,ret & 0xffffffffU,i,i,i);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d %d\n", (int)(OS_sys_get_return(1)),
			(int)(OS_sys_get_return(2)));
  }
  OS_syserr_handler();
')m4_dnl llseek

OS_impl(`lstat',
`
  LSE_emu_addr_t targbuf;
  struct stat buf;
  struct CC_type(stat) tempbuf;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  LSE_emu_addr_t fname;
  int didfname = 0;
  int ret=0;

  fname = CC_arg(0,i,ptr);
  targbuf = CC_arg(1,i,ptr);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: lstat64(%s,0x" LSE_emu_addr_t_print_format ") = ",
	    fnamebuf,targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(str,fnamebuf,strlen(fnamebuf)+1);

  CC_rewrite_filename(fnamebuf)

  CC_chkpt_guard(`ret = lstat(fnamebuf, &buf);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  /* 
   * note that these values will depend upon the HOST system
   */

  CC_chkpt_guard(`memset(&tempbuf,0,sizeof(tempbuf));
	COM_struct_end_h2e(tempbuf,buf,stat64);')
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: stat(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              fname, targbuf);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
')m4_dnl lstat

OS_impl(`lstat64',
`
  LSE_emu_addr_t targbuf;
#ifdef __USE_LARGEFILE64
  struct stat64 buf;
#else
  struct stat buf;
#endif
  struct CC_type(stat64) tempbuf;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  LSE_emu_addr_t fname;
  int didfname = 0;
  int ret=0;

  fname = CC_arg(0,i,ptr);
  targbuf = CC_arg(1,i,ptr);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: lstat64(%s,0x" LSE_emu_addr_t_print_format ") = ",
	    fnamebuf,targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(str,fnamebuf,strlen(fnamebuf)+1);

  CC_rewrite_filename(fnamebuf)

#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(`ret = lstat64(fnamebuf, &buf);')
#else
  CC_chkpt_guard(`ret = lstat(fnamebuf, &buf);')
#endif
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  /* 
   * note that these values will depend upon the HOST system
   */

  CC_chkpt_guard(`memset(&tempbuf,0,sizeof(tempbuf));
	COM_struct_end_h2e(tempbuf,buf,stat64);')
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: stat(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              fname, targbuf);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
')m4_dnl lstat64

OS_impl(`lwp_sigmask',
`
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: lwp_sigmask(...) = ");

  CC_chkpt_start($1);

  /* This syscall just sets a priority.  Hopefully, having an empty
     definition is safe */ 
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);

  OS_sys_return();
m4_ifdef(`CC_chkpt_implemented',`syserr:')
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl lwp_sigmask

OS_impl(`memcntl',
` LSE_emu_addr_t start;
  int length;
  int cmd;
  LSE_emu_addr_t arg;
  int attr;
  int mask;
  int ret;

  start = CC_arg(0,ptr,i,i,ptr,i,i);
  length = CC_arg(1,ptr,i,i,ptr,i,i);
  cmd = CC_arg(2,ptr,i,i,ptr,i,i);
  arg = CC_arg(3,ptr,i,i,ptr,i,i);
  attr = CC_arg(4,ptr,i,i,ptr,i,i);
  mask = CC_arg(5,ptr,i,i,ptr,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: memcntl(0x" LSE_emu_addr_t_print_format 
            ",%d,%d," LSE_emu_addr_t_print_format ",%d,%d) = ",
	    start, length, cmd, arg, attr, mask);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,start);
  CC_chkpt_arg(i,length);
  CC_chkpt_arg(i,cmd);
  CC_chkpt_arg(ptr,arg);
  CC_chkpt_arg(i,attr);
  CC_chkpt_arg(i,mask);

  /* Just report that we did OK */
  ret = 0;

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"0x" LSE_emu_addr_t_print_format "\n", ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl memcntl

OS_impl(`mmap',
`
  LSE_emu_addr_t start;
  int length;
  int prot;
  int flags;
  int fd;
  int offset;
  LSE_emu_addr_t rval;
  int ret;

  start = CC_arg(0,ptr,i,i,i,i,i);
  length = CC_arg(1,ptr,i,i,i,i,i);
  prot = CC_arg(2,ptr,i,i,i,i,i);
  flags = CC_arg(3,ptr,i,i,i,i,i);
  fd = CC_arg(4,ptr,i,i,i,i,i);
  offset = CC_arg(5,ptr,i,i,i,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: mmap(0x" LSE_emu_addr_t_print_format 
            ",%d,%d,%d,%d,%d) = ",
            start, length, prot, flags, fd, offset);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,start);
  CC_chkpt_arg(i,length);
  CC_chkpt_arg(i,prot);
  CC_chkpt_arg(i,flags);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(i,offset);

#if 0
  if (fd != -1) { /* cannot map files right now.... */
    if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
      fprintf(COM_stderr(realct),"unable to map files yet\n");
    return -1;
  }
#endif
  if ((flags & CC_const(MAP_SHARED))) { /* cannot do sharing right now */
    if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
      fprintf(COM_stderr(realct),"unable to map shared yet\n");
    return -1;
  }
#if 1
  OS_mmap(start,length,prot,flags,fd,offset,rval);
#else
  ret = -1;
  errno = ENOSYS;
#endif
  if (ret != -1) {
    OS_report_results(0,rval);
  } else {
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"0x" LSE_emu_addr_t_print_format "\n", rval);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl mmap

OS_impl(`mprotect',
`
  LSE_emu_addr_t addr;
  int length;
  int prot;
  int ret=0;

  addr = CC_arg(0,ptr,i,i);
  length = CC_arg(1,ptr,i,i);
  prot = CC_arg(2,ptr,i,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: mprotect(0x" LSE_emu_addr_t_print_format ",%d,%d) = ",
              addr, length, prot);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,addr);
  CC_chkpt_arg(i,length);
  CC_chkpt_arg(i,prot);

  /* This syscall just adds protection.  Having an empty definition is
     safe */ 
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);

  OS_sys_return();
m4_ifdef(`CC_chkpt_implemented',`syserr:')
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl mprotect

OS_impl(`munmap',`
  LSE_emu_addr_t start,length;
  LSE_emu_addr_t rval = 0;
  int ret = 0;

  start = CC_arg(0,ptr,i);
  length = CC_arg(1,ptr,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: munmap(0x" LSE_emu_addr_t_print_format " 0x" 
	    LSE_emu_addr_t_print_format ") = ",
            start, length);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,start);
  CC_chkpt_arg(i,length);

  m4_dnl For now, this is unimplemented and generates errors...
  OS_munmap(start,length,rval); 

  if (ret != -1) {
    OS_report_results(0,rval);
  } else {
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"0x" LSE_emu_addr_t_print_format "\n", rval);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl munmap

OS_impl(`open',
`
  LSE_emu_addr_t fname;
  int flags,realflags;
  mode_t mode;
  int ret=0;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didfname = 0;

  fname = CC_arg(0,ptr,i,i);
  flags = CC_arg(1,ptr,i,i);
  mode = CC_arg(2,ptr,i,i);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "SYSCALL: open(\"%s\",%d,%d) = ",
            (char *)fnamebuf,flags,mode);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(i,flags);
  CC_chkpt_arg(i,mode);
  CC_chkpt_arg(str,fnamebuf,(strlen(fnamebuf)+1));

  /* 
     The following flags may vary between platforms
     Check them and remap them if necessary.

     The special flags start at 040000.
     For the time being, we will only support O_DIRECT, O_DIRECTORY, O_NOFOLLOW
     since these are the only ones that are consistently available.
  */

  realflags = flags & 3; // should be good for all systems

  if (flags & CC_const(O_CREAT)) realflags |= O_CREAT;
  if (flags & CC_const(O_EXCL)) realflags |= O_EXCL;
  if (flags & CC_const(O_NOCTTY)) realflags |= O_NOCTTY;
  if (flags & CC_const(O_TRUNC)) realflags |= O_TRUNC;
  if (flags & CC_const(O_APPEND)) realflags |= O_APPEND;
  if (flags & CC_const(O_NONBLOCK)) realflags |= O_NONBLOCK;
  if (flags & CC_const(O_NDELAY)) realflags |= O_NDELAY;
  if (flags & CC_const(O_SYNC)) realflags |= O_SYNC;
#ifdef O_LARGEFILE
  if (flags & CC_const(O_LARGEFILE)) realflags |= O_LARGEFILE;
#endif

  /* These flags only exist if __USE_GNU is on */
  #ifdef __USE_GNU
  if (flags & CC_const(O_DIRECT)) realflags |= O_DIRECT;
  if (flags & CC_const(O_DIRECTORY)) realflags |= O_DIRECTORY;
  #endif
#ifdef O_NOFOLLOW
  if (flags & CC_const(O_NOFOLLOW)) realflags |= O_NOFOLLOW;  
#endif

  CC_rewrite_filename(fnamebuf)

  CC_open_filter(fnamebuf,ret) {
    CC_chkpt_guard(`ret=open(fnamebuf,realflags,mode);')
    CC_chkpt_result(int,ret);
  }

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: open(0x" LSE_emu_addr_t_print_format ",%d,%d) = ",
              fname, flags, mode);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl open

OS_impl(`open64',m4_dnl I'm really just going to do a normal open here...
`
  LSE_emu_addr_t fname;
  int flags,realflags;
  mode_t mode;
  int ret=0;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didfname = 0;

  fname = CC_arg(0,ptr,i,i);
  flags = CC_arg(1,ptr,i,i);
  mode = CC_arg(2,ptr,i,i);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "SYSCALL: open(\"%s\",%d,%d) = ",
            (char *)fnamebuf,flags,mode);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(i,flags);
  CC_chkpt_arg(i,mode);
  CC_chkpt_arg(str,fnamebuf,(strlen(fnamebuf)+1));

  /* 
     The following flags may vary between platforms
     Check them and remap them if necessary.

     The special flags start at 040000.
     For the time being, we will only support O_DIRECT, O_DIRECTORY, O_NOFOLLOW
     since these are the only ones that are consistently available.
  */

  realflags = flags & 3; // should be good for all systems

  if (flags & CC_const(O_CREAT)) realflags |= O_CREAT;
  if (flags & CC_const(O_EXCL)) realflags |= O_EXCL;
  if (flags & CC_const(O_NOCTTY)) realflags |= O_NOCTTY;
  if (flags & CC_const(O_TRUNC)) realflags |= O_TRUNC;
  if (flags & CC_const(O_APPEND)) realflags |= O_APPEND;
  if (flags & CC_const(O_NONBLOCK)) realflags |= O_NONBLOCK;
  if (flags & CC_const(O_NDELAY)) realflags |= O_NDELAY;
  if (flags & CC_const(O_SYNC)) realflags |= O_SYNC;
#ifdef O_LARGEFILE
  if (flags & CC_const(O_LARGEFILE)) realflags |= O_LARGEFILE;
#endif

  /* These flags only exist if __USE_GNU is on */
  #ifdef __USE_GNU
  if (flags & CC_const(O_DIRECT)) realflags |= O_DIRECT;
  if (flags & CC_const(O_DIRECTORY)) realflags |= O_DIRECTORY;
  #endif
#ifdef O_NOFOLLOW
  if (flags & CC_const(O_NOFOLLOW)) realflags |= O_NOFOLLOW;  
#endif

  CC_open_filter(fnamebuf,ret) {
    CC_chkpt_guard(`ret=open(fnamebuf,realflags,mode);')
    CC_chkpt_result(int,ret);
  }

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: open(0x" LSE_emu_addr_t_print_format ",%d,%d) = ",
              fname, flags, mode);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl open64

OS_impl(`pathconf',
`
  LSE_emu_addr_t path;
  int name;
  int ret=0;
  char pathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didpath = 0;

  path = CC_arg(0,ptr,i);
  name = CC_arg(1,ptr,i);

  CC_strncpy_t2h(pathbuf,path,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  pathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didpath = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),
	    "SYSCALL: readlink(\"%s\",%d) = ",pathbuf, name);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,path);
  CC_chkpt_arg(i,name);
  CC_chkpt_arg(str,pathbuf,strlen(pathbuf)+1);

  CC_chkpt_guard(`ret=pathconf(pathbuf,name);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    CC_chkpt_result(int,ret);
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didpath) {
      fprintf(COM_stderr(realct),
              "SYSCALL: pathconf(0x" LSE_emu_addr_t_print_format ",%d) = ",
              path, name);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl pathconf

OS_impl(`pipe',
`
  LSE_emu_addr_t fdesp;
  int ret=0;
  int fdes[2], swapdes[2];

  fdesp = CC_arg(0,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "SYSCALL: pipe(0x" LSE_emu_addr_t_print_format ") = ",
            fdesp);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fdesp);

  CC_chkpt_guard(`ret = pipe(&fdes[0]);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int, errno);
    OS_report_err(errno);
  } else {
    CC_chkpt_result(int,fdes[0]);
    CC_chkpt_result(int,fdes[1]);
    swapdes[0] = COM_end_h2e(fdes[0],int32_t);
    swapdes[1] = COM_end_h2e(fdes[1],int32_t);
    CC_memcpy_h2t(fdesp,swapdes,8);
    OS_report_results(0,ret);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl pipe

OS_impl(`read',
`
  int fd;
  char *buf=NULL;
  char mybuf[1024];
  LSE_emu_addr_t targbuf;
  int count, ocount;
  int ret=0, mustfree=0;

  fd = CC_arg(0,i,ptr,i);
  targbuf = CC_arg(1,i,ptr,i);
  ocount = count = CC_arg(2,i,ptr,i);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(i,count);

  if (!count) {
    CC_chkpt_result(int,ret);
    OS_report_results(0,0);
    buf = mybuf; /* shut up warnings */
    ret = 0;
  } else {
    if (count > 1024) {
      buf = (char *)malloc(count);
      if (buf == NULL) {
	count = 1024;
	buf = mybuf;
      }
      else mustfree = 1;
    } else buf = mybuf;

    CC_chkpt_guard(`ret = read(fd,buf,count);')
    CC_chkpt_result(int,ret);

    if (ret == -1) {
      CC_chkpt_result(int,errno);
      OS_report_err(errno);
    } else {
      CC_chkpt_result(hmem,buf,ret);
      CC_memcpy_h2t(targbuf,buf,ret); /* will report error */
      OS_report_results(0,ret);
    }
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    int i;
    fprintf(COM_stderr(realct),"SYSCALL: read(%d,\"",fd);
    for (i=0;i<30 && i<ret && buf[i]; i++) 
      fputc(isprint(buf[i])?buf[i] : 0x2e, COM_stderr(realct));
    fprintf(COM_stderr(realct),"\"%s,%d) = %d\n",i<ret?"...":"",ocount,ret);
  }
  if (mustfree) free(buf);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: read(%d," LSE_emu_addr_t_print_format ",%d) = -1 %d\n",
	    fd, targbuf, ocount,(int)OS_sys_get_return(1));
  if (mustfree) free(buf);
  OS_syserr_handler();
')m4_dnl read

OS_impl(`readlink',
`
  LSE_emu_addr_t path;
  LSE_emu_addr_t buf;
  size_t bufsiz;
  int ret=0;
  char pathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  char mybuf[1024];
  char *buftouse = &mybuf[0];
  int didpath = 0, mustfree=0;

  path = CC_arg(0,ptr,ptr,i);
  buf = CC_arg(1,ptr,ptr,i);
  bufsiz = CC_arg(2,ptr,ptr,i);

  CC_strncpy_t2h(pathbuf,path,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  pathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didpath = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),
	    "SYSCALL: readlink(\"%s\",0x" LSE_emu_addr_t_print_format 
            ",%d) = ",
            pathbuf, buf, bufsiz);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,path);
  CC_chkpt_arg(ptr,buf);
  CC_chkpt_arg(i,bufsiz);
  CC_chkpt_arg(str,pathbuf,strlen(pathbuf)+1);

  if (bufsiz > 1024) {
    buftouse = (char *)malloc(bufsiz);
    if (buftouse == NULL) {
      OS_report_err(ENOMEM);
    }
    else mustfree = 1;
  }

  CC_chkpt_guard(`ret=readlink(pathbuf,buftouse,bufsiz);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    CC_chkpt_result(str,buftouse,bufsiz);
    CC_memcpy_h2t(buf,buftouse,bufsiz); /* will report error */
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  if (mustfree) free(buftouse);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didpath) {
      fprintf(COM_stderr(realct),
              "SYSCALL: readlink(0x" LSE_emu_addr_t_print_format ",0x" 
                                 LSE_emu_addr_t_print_format ",%d) = ",
              path, buf, bufsiz);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  if (mustfree) free(buftouse);
  OS_syserr_handler();
')m4_dnl readlink

OS_impl(`rename',
`
  LSE_emu_addr_t oldpath, newpath;
  int ret=0;
  char oldpathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  char newpathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didoldpath = 0;
  int didnewpath = 0;

  oldpath = CC_arg(0,ptr,ptr);
  newpath = CC_arg(1,ptr,ptr);

  CC_strncpy_t2h(oldpathbuf,oldpath,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  oldpathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didoldpath = 1;
  CC_strncpy_t2h(newpathbuf,newpath,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  newpathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didnewpath = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),
	    "SYSCALL: rename(\"%s\",\"%s\") = ", oldpathbuf,newpathbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,oldpath);
  CC_chkpt_arg(ptr,newpath);
  CC_chkpt_arg(str,oldpathbuf,strlen(oldpathbuf)+1);
  CC_chkpt_arg(str,newpathbuf,strlen(newpathbuf)+1);

  CC_chkpt_guard(`ret=rename(oldpathbuf,newpathbuf);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didnewpath) {
      fprintf(COM_stderr(realct),
              "SYSCALL: rename(0x" LSE_emu_addr_t_print_format ",0x" 
	      LSE_emu_addr_t_print_format ") = ",
              oldpath,newpath);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl rename

OS_impl(`rmdir',
`
  LSE_emu_addr_t fname;
  int ret=0;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didfname=0;

  fname = CC_arg(0,ptr);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2]=0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "SYSCALL: rmdir(\"%s\") = ",
	    (char *)fnamebuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(str,fnamebuf,strlen(fnamebuf)+1);

  CC_rewrite_filename(fnamebuf)

  CC_chkpt_guard(`ret=rmdir(fnamebuf);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: rmdir(0x" LSE_emu_addr_t_print_format ") = ",
              fname);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl rmdir

OS_impl(`rusagesys',
`
  int op, myerrno;
  LSE_emu_addr_t targrusage;
  struct rusage rusage;
  struct CC_type(rusage) temprusage;
  int ret=0;

  op = CC_arg(0,i,ptr);
  targrusage = CC_arg(1,i,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: getrusage(%d,0x" LSE_emu_addr_t_print_format 
            ") = ",
	    op, targrusage);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,op);
  CC_chkpt_arg(ptr,targrusage);

  switch (op) {
    case 0 : 
       CC_chkpt_guard(`ret = getrusage(RUSAGE_SELF, &rusage);');
       myerrno = errno;
       break;
    case 1 : 
       CC_chkpt_guard(`ret = getrusage(RUSAGE_CHILDREN, &rusage);');
       myerrno = errno;
       break;
    default: ret = -1; myerrno = EINVAL;
  }
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,myerrno);
    OS_report_err(myerrno);
  }

  CC_chkpt_guard(`
    memset(&temprusage,0,sizeof(temprusage));
    /* leaving all the other fields 0 */
    COM_struct_end_h2e(temprusage.LSE_ru_utime,rusage.ru_utime,timeval);
    COM_struct_end_h2e(temprusage.LSE_ru_stime,rusage.ru_stime,timeval);
  ')

  CC_chkpt_arg(i,sizeof(temprusage)); /* for sanity */
  CC_chkpt_result(hmem,&temprusage,sizeof(temprusage));

  /* will report error */
  CC_memcpy_h2t(targrusage,&temprusage,sizeof(temprusage));
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl rusagesys

OS_impl(`schedctl',
`
  int fd = CC_arg(0,i);
  int ret= -1;
  int myerrno = CC_const(ENOSYS);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),"SYSCALL: schedctl(%d,...) = ", fd);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);

  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,myerrno);
    OS_report_err(myerrno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl schedctl

OS_impl(`setitimer',
`
  int ret=0;
  ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),
            "SYSCALL: setitimer() = ");

  CC_chkpt_start($1); 
  m4_dnl we seem to be ignoring the parameters for now!

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    OS_report_err(errno);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "%d\n", ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl setitimer

OS_impl(`setrlimit',
`
  int resource;
  LSE_emu_addr_t targrlim;
  struct rlimit rlim;
  struct CC_type(rlimit) temprlim;
  int ret=0;

  resource = CC_arg(0,i,ptr);
  targrlim = CC_arg(1,i,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: setrlimit(%d,0x" LSE_emu_addr_t_print_format 
            ") = ",
	    resource, targrlim);

  CC_chkpt_start($1); 
  CC_chkpt_arg(i,resource); 
  CC_chkpt_arg(ptr,targrlim); 

  CC_memcpy_t2h(&temprlim,targrlim,sizeof(temprlim)); /* will report error */

  CC_chkpt_arg(i,sizeof(temprlim)); /* for sanity */
  CC_chkpt_arg(hmem,&temprlim,sizeof(temprlim));

  CC_chkpt_guard(`
    COM_struct_end_e2h(rlim,temprlim,rlimit);
    ret = setrlimit(resource, &rlim);
  ')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl setrlimit

OS_impl(`sigaction',
`
  int ret=0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: sigaction(...) = ");

  CC_chkpt_start($1);

  /* This syscall just sets a priority.  Hopefully, having an empty
     definition is safe */ 
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);

  OS_sys_return();
m4_ifdef(`CC_chkpt_implemented',`syserr:')
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl sigaction

OS_impl(`stat',
`
  LSE_emu_addr_t fname, targbuf;
  struct stat buf;
  struct CC_type(stat) tempbuf;
  int ret=0;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didfname = 0;

  fname = CC_arg(0,ptr,ptr);
  targbuf = CC_arg(1,ptr,ptr);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: stat(\"%s\",0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    (char *)fnamebuf,targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(str,fnamebuf,strlen(fnamebuf)+1);

  CC_rewrite_filename(fnamebuf)

  CC_chkpt_guard(`ret = stat((char *)fnamebuf, &buf);')
  CC_chkpt_result(int,ret);

#ifdef NOTNOW
  fprintf(COM_stderr(realct), "\n%s:", fnamebuf);
  for (int ii=0;ii<sizeof(buf);ii++) fprintf(COM_stderr(realct),"%02x ",((char *)&buf)[ii]); 
  fprintf(COM_stderr(realct), "\n\t%lld %ld ", buf.st_dev, buf.st_ino);
#endif

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  /* 
   * note that these values will depend upon the HOST system
   */
  CC_chkpt_guard(`memset(&tempbuf,0,sizeof(tempbuf));
	COM_struct_end_h2e(tempbuf,buf,stat)')
  CC_chkpt_arg(i,sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: stat(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              fname, targbuf);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl stat

OS_impl(`stat64',
`
  LSE_emu_addr_t targbuf;
  struct stat buf;
  struct CC_type(stat64) tempbuf;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  LSE_emu_addr_t fname;
  int didfname = 0;
  int ret=0;

  fname = CC_arg(0,i,ptr);
  targbuf = CC_arg(1,i,ptr);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: stat64(%s,0x" LSE_emu_addr_t_print_format ") = ",
	    fnamebuf,targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(str,fnamebuf,strlen(fnamebuf)+1);

  CC_rewrite_filename(fnamebuf)

#ifdef __USE_LARGEFILE64
  CC_chkpt_guard(`ret = stat64(fnamebuf, &buf);')
#else
  CC_chkpt_guard(`ret = stat(fnamebuf, &buf);')
#endif
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  /* 
   * note that these values will depend upon the HOST system
   */

  CC_chkpt_guard(`memset(&tempbuf,0,sizeof(tempbuf));
	COM_struct_end_h2e(tempbuf,buf,stat64);')
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: stat(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              fname, targbuf);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler()
')m4_dnl stat64

OS_impl(`statfs',
`
  LSE_emu_addr_t fname, targbuf;
#if defined (__SVR4) && defined (__sun)
  struct statvfs buf;
#else
  struct statfs buf;
#endif
  struct CC_type(statfs) tempbuf;
  int ret=0;
  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didfname = 0;

  fname = CC_arg(0,ptr,ptr);
  targbuf = CC_arg(1,ptr,ptr);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didfname = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: stat(\"%s\",0x" 
	    LSE_emu_addr_t_print_format ") = ",
	    (char *)fnamebuf,targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(str,fnamebuf,strlen(fnamebuf)+1);

  CC_rewrite_filename(fnamebuf)

#if defined (__SVR4) && defined (__sun)
  CC_chkpt_guard(`ret = statvfs((char *)fnamebuf, &buf);')
#else
  CC_chkpt_guard(`ret = statfs((char *)fnamebuf, &buf);')
#endif
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  /* 
   * note that these values will depend upon the HOST system
   */
  CC_chkpt_guard(`memset(&tempbuf,0,sizeof(tempbuf));
	COM_struct_end_h2e(tempbuf,buf,statfs)
#if defined (__SVR4) && defined (__sun)
   tempbuf._f_type = COM_end_h2e(0xEF53,CC_type(ulong)); /* pretend EXT2 */
   tempbuf._f_namelen = COM_end_h2e(buf.f_namemax,CC_type(ulong)); 
#endif
        ')
  CC_chkpt_arg(i,sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didfname) {
      fprintf(COM_stderr(realct),
              "SYSCALL: statfs(0x" LSE_emu_addr_t_print_format ",0x"
	      LSE_emu_addr_t_print_format ") = ",
              fname, targbuf);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl statfs

OS_impl(`sysconfig',
`
  int ret=0;
  int vname = CC_arg(0,i);
  int myerrno = CC_const(EINVAL);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: sysconfig(%d) = ", vname);

  CC_chkpt_start($1);
  CC_chkpt_arg(i, vname);

  switch (vname) {
    case 3 /* _SC_CLK_TICK */: ret = 1000; break;
    case 7 /* _CONFIG_CLK_TICK */: ret = 1000; break;
    case 11 /* _SC_PAGESIZE */: ret = CC_const(PAGE_SIZE); break;
    default:
	ret = -1;
	break;
  }

  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,myerrno);
    OS_report_err(myerrno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler();
')m4_dnl sysconfig

/*
	FIXME: systeminfo is a stub implementation
 */
OS_impl(`systeminfo',
`
  LSE_emu_addr_t targbuf;
  int oper, length;
  int ret=0;
  int myerrno = CC_const(ENOSYS);

  oper = CC_arg(0,i,ptr,i);
  targbuf = CC_arg(1,i,ptr,i);
  length = CC_arg(2,i,ptr,i);
  static const char *names[] = { "",
	CC_const(SYSNAME), CC_const(NODENAME), CC_const(RELEASE),
	CC_const(VERSION), CC_const(MACHINE), "sparc",
	"1234.5678", "LSE emulator", CC_const(DOMAINNAME),

	"Platform", CC_const(ISALIST), "DHCP_CACHE", CC_const(ARCHITECTURE_32), 
	CC_const(ARCHITECTURE_64), CC_const(ARCHITECTURE_K), CC_const(ARCHITECTURE_NATIVE)
	};

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: systeminfo(%d,0x" 
	    LSE_emu_addr_t_print_format ",%d) = ",
	    oper,targbuf,length);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,oper);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(i,length);
  
  int toper;
  if (oper < 0) toper = 0;
  else if (oper <= 9) toper = oper;
  else if (oper < 513) toper = 0;
  else if (oper <= 519) toper = oper - 513 + 10;
  else toper = 0;

  if (length > (int)strlen(names[toper])+1) length = strlen(names[toper])+1;
  CC_memcpy_h2t(targbuf,names[toper],length-1);
  CC_memcpy_h2t(targbuf+length-1,names[0],1);
  ret = length;

  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,myerrno);
    OS_report_err(myerrno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl systeminfo

OS_impl(`time',  m4_dnl In Solaris, this system call has no arguments.
`
  time_t ret;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: time() = ");

  CC_chkpt_start($1);
  
#if defined (__SVR4) && defined (__sun)
  CC_chkpt_guard(`ret = time();')
#else
  CC_chkpt_guard(`ret = time(NULL);')
#endif
  CC_chkpt_result(time_t,ret);

  if (ret == -1) {
    CC_chkpt_result(int, errno);
    OS_report_err(errno);
  }
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl time

OS_impl(`times',
`
  LSE_emu_addr_t targbuf;
  struct tms buf;
  struct CC_type(tms) tempbuf;
  int ret=0;

  targbuf = CC_arg(0,ptr);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: times(0x" LSE_emu_addr_t_print_format ") = ",
	    targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,targbuf);

  CC_chkpt_guard(`ret = times(&buf);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }

  CC_chkpt_guard(`memset(&tempbuf,0,sizeof(tempbuf));
	COM_struct_end_h2e(tempbuf,buf,tms)')
  CC_chkpt_arg(i,sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl times

OS_impl(`umask',
`
  mode_t mask;
  int ret=0;

  mask = (mode_t) CC_arg(0,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: umask(%d) = ", mask);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,mask);

  CC_chkpt_guard(`ret = (int)umask(mask);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl umask

OS_impl(`uname',
`
  static struct localutsname {
    char sysname[CC_const(SYS_NMLN)];
    char nodename[CC_const(SYS_NMLN)];
    char release[CC_const(SYS_NMLN)];
    char version[CC_const(SYS_NMLN)];
    char machine[CC_const(SYS_NMLN)];
  } hardcodedname 
    = { CC_const(SYSNAME), CC_const(NODENAME),
	CC_const(RELEASE), CC_const(VERSION),
	CC_const(MACHINE) };

  LSE_emu_addr_t buf = CC_arg(0,ptr);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,buf);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: uname(0x" LSE_emu_addr_t_print_format ") = ",buf);

  CC_memcpy_h2t(buf,&hardcodedname,sizeof(hardcodedname));

  OS_report_results(0,0);
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"0\n");
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl uname

OS_impl(`unlink',
`
  LSE_emu_addr_t path;
  int ret=0;
  char pathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  int didpath = 0;

  path = CC_arg(0,ptr);

  CC_strncpy_t2h(pathbuf,path,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  pathbuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2] = 0;
  didpath = 1;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),
	    "SYSCALL: unlink(\"%s\") = ", pathbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,path);
  CC_chkpt_arg(str,pathbuf,strlen(pathbuf)+1);
  
  CC_chkpt_guard(`ret=unlink(pathbuf);')
  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) fprintf(COM_stderr(realct),"%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    if (!didpath) {
      fprintf(COM_stderr(realct),
              "SYSCALL: unlink(0x" LSE_emu_addr_t_print_format ") = ",
              path);
    }
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  }
  OS_syserr_handler();
')m4_dnl unlink

OS_impl(`utimes',
`
  LSE_emu_addr_t targbuf;
  struct timeval buf;
  struct CC_type(timeval) tempbuf;

  char fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+3];
  LSE_emu_addr_t fname;

  int ret=0;

  fname = CC_arg(0,ptr,ptr);
  targbuf = CC_arg(1,ptr,ptr);

  CC_strncpy_t2h(fnamebuf,fname,CC_const(PATH_MAX)+CC_const(NAME_MAX)+2);
  fnamebuf[CC_const(PATH_MAX)+CC_const(NAME_MAX)+2]=0;


  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"SYSCALL: utimes(0x" LSE_emu_addr_t_print_format ") = ",
	    targbuf);

  CC_chkpt_start($1);
  CC_chkpt_arg(ptr,fname);
  CC_chkpt_arg(str,fnamebuf,(strlen(fnamebuf)+1));
  CC_chkpt_arg(ptr,targbuf);

  CC_rewrite_filename(fnamebuf)

  CC_chkpt_guard(`ret = utimes(fnamebuf,&buf);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }

  CC_chkpt_guard(`memset(&tempbuf,0,sizeof(tempbuf));
	COM_struct_end_h2e(tempbuf,buf,timeval)')
  CC_chkpt_arg(i,sizeof(tempbuf)); /* for sanity */
  CC_chkpt_result(hmem,&tempbuf,sizeof(tempbuf));

  CC_memcpy_h2t(targbuf,&tempbuf,sizeof(tempbuf)); /* will report error */
  
  OS_report_results(0,ret);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl utimes

/*
	FIXME: wait is a stub implementation
 */

OS_impl(`wait',
`
  int fd = CC_arg(0,i);
  int ret=0;
  ret = 0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),"SYSCALL: wait(%d) = ", fd);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);

  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl waitpid


m4_ifelse(`
/*
	FIXME: wait is a stub implementation
 */
OS_impl(`waitsys',
`
  int fd = CC_arg(0,i);
  int ret=0;
  ret = 0;

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct),"SYSCALL: waitsys(%d) = ", fd);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);

  CC_chkpt_result(int,ret);

  if (ret != -1) {
    OS_report_results(0,ret);
  } else {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),"%d\n",ret);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  OS_syserr_handler()
')m4_dnl waitsys
')

OS_impl(`write',
`
  int fd;
  char *buf=NULL;
  char mybuf[1024];
  LSE_emu_addr_t targbuf;
  int count;
  int ret=0, mustfree=0;

  fd = CC_arg(0,i,ptr,i);
  targbuf = CC_arg(1,i,ptr,i);
  count = CC_arg(2,i,ptr,i);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(i,count);

  if (!count) {
    OS_report_results(0,0);
    buf = mybuf;
    ret = 0;
  } else {
    if (count > 1024) {
      buf = (char *)malloc(count);
      if (buf == NULL) {
	OS_report_err(CC_const(ENOSPC));
      }
      mustfree = 1;
    } else buf = mybuf;

    CC_memcpy_t2h(buf, targbuf,count); /* will report error */

    CC_chkpt_arg(hmem,buf,count)
    CC_chkpt_guard(`ret = write(fd,buf,count);')
    CC_chkpt_result(int,ret)

    if (ret == -1) {
      CC_chkpt_result(int,errno)
      OS_report_err(errno);
    } else {
      OS_report_results(0,ret);
    }
  }
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) {
    int i;
    fprintf(COM_stderr(realct),"SYSCALL: write(%d,\"",fd);
    for (i=0;i<30 && i<ret && buf[i]; i++) 
      fputc(isprint(buf[i])?buf[i] : 0x2e, COM_stderr(realct));
    fprintf(COM_stderr(realct),"\"%s,%d) = %d\n",i<ret?"...":"",count,ret);
  }
  if (mustfree) free(buf);

  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: write(%d," LSE_emu_addr_t_print_format ",%d) = -1 %d\n",
	    fd, targbuf, count,(int)OS_sys_get_return(1));
  if (mustfree) {
     free(buf);
  }
  OS_syserr_handler();
')m4_dnl write

OS_impl(`writev',
`
  int fd;
  struct CC_type(iovec) *buf=NULL;
  struct iovec *hostbuf=NULL;
  LSE_emu_addr_t targbuf;
  int count;
  int ret=0, mustfree=0;
  int i, rcount=0;

  fd = CC_arg(0,i,ptr,i);
  targbuf = CC_arg(1,i,ptr,i);
  count = CC_arg(2,i,ptr,i);

  if (OS_ict_emuvar(realct,EMU_trace_syscalls)) 
    fprintf(COM_stderr(realct),
            "SYSCALL: writev(%d," LSE_emu_addr_t_print_format ",%d) = ",
	    fd, targbuf, count);

  CC_chkpt_start($1);
  CC_chkpt_arg(i,fd);
  CC_chkpt_arg(ptr,targbuf);
  CC_chkpt_arg(i,count);

  buf = (struct CC_type(iovec) *)malloc(sizeof(struct CC_type(iovec))*count+1);
  hostbuf = (struct iovec *)malloc(sizeof(const struct iovec)*count+1);
  CC_memcpy_t2h(buf, targbuf,
                count*sizeof(struct CC_type(iovec))); /* will report error */
  mustfree = 1;

  CC_chkpt_arg(i,sizeof(struct CC_type(iovec)));
  CC_chkpt_arg(hmem,buf,count * sizeof(struct CC_type(iovec)));

  for (i=0;i<count;i++)
  {
    CC_type(size_t) len;
    COM_addrtype base;
    /* switching stuff in place in buf */
    len = COM_end_h2e(buf[i].iov_len,CC_type(size_t));
    base = COM_end_h2e(buf[i].iov_base, COM_addrtype());
    hostbuf[i].iov_base = (char *)malloc(len+1);
    CC_memcpy_t2h(hostbuf[i].iov_base,base,len);
    CC_chkpt_arg(hmem,hostbuf[i].iov_base,len);
    hostbuf[i].iov_len = len;
    rcount = i+1;
  }

  CC_chkpt_guard(`ret = writev(fd,hostbuf,count);')
  CC_chkpt_result(int,ret);

  if (ret == -1) {
    CC_chkpt_result(int,errno);
    OS_report_err(errno);
  } else {
    OS_report_results(0,ret);
  }

  if (mustfree) {
    for (i=0;i<rcount;i++) {
      free(hostbuf[i].iov_base);
    }
    free(buf);
    free(hostbuf);
  }

  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "%d\n", ret);
  OS_sys_return();
syserr:
  if (OS_ict_emuvar(realct,EMU_trace_syscalls))
    fprintf(COM_stderr(realct), "-1 %d\n", (int)OS_sys_get_return(1));
  if (mustfree) {
     for (i=0;i<rcount;i++)    
     {
       free(hostbuf[i].iov_base);
     }
     free(buf);
     free(hostbuf);
  }
  OS_syserr_handler();
')m4_dnl writev

m4_dnl *********************************************************************
m4_dnl *********************************************************************
m4_dnl *********************************************************************
