/* 
 * Copyright (c) 2000-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Code for doing Linux checkpointing
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * This file is used as part of the OS processing.  It must not be directly
 * processed; rather, it is an include file.
 *
 * Some assumptions:
 * - If an OS emulator is used, it will control the writing/reading of
 *   contexts.  It provides the master IDs and calls CC_ functions to do it.
 * - If an OS emulator is not used, then read/write is based off of the HW
 *   list.
 * - Ordering of elements in a checkpoint is important because of the
 *   possibility of circular links.  We output pointed-to objects before
 *   the pointers whenever possible.  The network of pointers is:
 *     memory    -> futex, *_file (including some not in fdtables),
 *                  physical memory
 *     pipe      -> continuation (-> pipe, context)
 *     pipe_file -> pipe, continuations (-> files)
 *     fdtable   -> *_file, 
 *     pgroup    -> context
 *     tgroup    -> context, timer
 *     context   -> context, memory, fdtable, sighandtable, tgroup, pgroup, 
 *                  continuations, physical memory, filesystem
 *     futex     -> continuation (-> timer, futex)
 *     timer     -> todo (not refcounted)
 *     todo      -> timer (not refcounted)
 *     sigtimer  -> context, tgroup
 *     (dinst    -> context)
 *   The exceptions requiring "fixups" are therefore:
 *   1) pointers to contexts.  NOTE: contexts are *not* reference counted
 *      through these pointers.  Therefore it is imperative that when a
 *      context is deleted it be removed from all the structures that may
 *      point to it.  Fixups do not need to affect reference counts.
 *   2) physical memory pointers from OS memories.  Because of the way
 *      the records are laid out, it is more convenient to defer writing
 *      physical memories until contexts are written.  As a result, each
 *      translation space's subdevice must be pointed at the corresponding
 *      physical memory during fixups.
 *   3) timer pointers to tgroups.  The pointer is not reference counted and
 *      needs to be fixed up with affecting the reference count.
 *   Circulars references not requiring fixups:
 *   -  The continuations often end up pointing back at their owner, whether
 *      a context, a file, a pipe, or a futex.  However, their owner
 *      is always guaranteed to be in the process of being read in and will
 *      have already registered itself, thus the oid will be good.
 *
 *  In general: insert_obj(..., true) for ref-counted object
 *              insert_obj(..., false) for ref-counted object which will be
 *                                     decremented through some list traversal
 *              insert_obj_noref(..., true) for non-ref-counted object
 *              record_fixup(...) for circular pointers (always non-ref-counted)
 *
 * IMPORTANT: physical memories must be written and parsed before OS
 *            instances so that the vmdevice.subspace can be fixed up.  An
 *            ISA may do this either by directly checkpointing its physical
 *            memory before checkpointing the OS or by checkpointing physical
 *            memory while checkpointing contexts.
 *
 */
//#define DEBUG_CPOINT

/***************************** Macro definitions **************************/

#define CC_chkpt_reading    (osct->os->ctl.recordOS && osct->os->OSstate==2)
#define CC_chkpt_result_loc (osct->currOSRecord->oldestChild->sibling->sibling)

#define CC_chkpt_start(tag)    chkpt_start(osct, tag)
#define CC_chkpt_finish()      chkpt_finish(osct)

#define CC_chkpt_arg_ptr(v)    chkpt_arg_ptr(osct, v)
#define CC_chkpt_arg_str(v,l)  chkpt_arg_str(osct, (char *)(v), l)
#define CC_chkpt_arg_hmem(v,l) chkpt_arg_hmem(osct, (unsigned char *)(v), l)
#define CC_chkpt_arg_tmem(v,l) chkpt_arg_hmem(osct, v, l)
#define CC_chkpt_arg_int(v)    chkpt_arg_int(osct, v)

#define CC_chkpt_result_ptr(v)    chkpt_result_ptr(osct, v)
#define CC_chkpt_result_str(v,l)  chkpt_result_str(osct, (char *)(v), l)
#define CC_chkpt_result_hmem(v,l) chkpt_result_hmem(osct,(unsigned char *)(v),l)
#define CC_chkpt_result_tmem(v,l) chkpt_result_tmem(osct, v, l)
#define CC_chkpt_result_psect(v)  chkpt_result_psect_context(osct, v)
#define CC_chkpt_result_int(v)    chkpt_result_int(osct, v)

#define CC_chkpt_guard(...) \
  if (osct->os->OSstate!=2 || !osct->os->ctl.recordOS) { __VA_ARGS__ }

#define CC_chkpt_guard_os(x) ((x)->OSstate!=2 || !(x)->ctl.recordOS)
#define CC_chkpt_guard_true CC_chkpt_guard_os(osct->os)

/**************************** Start code ****************************/

enum Linux_cpoint_tag {
  tag_NONE = 100,             // value matters
  tag_OSTRACE = 101, // value matters
  tag_OSINST = 103,
  tag_OSCONTEXT = 104,
  tag_SIGHANDTABLE = 105,
  tag_TGROUP = 106,
  tag_PGROUP = 107,
  tag_FUTEX = 108,
  tag_FDTABLE = 109,
  tag_ERRORFILE = 110,
  tag_NATIVEFILE = 111,
  tag_PIPEFILE = 112,
  tag_SOCKETFILE = 113,
  tag_PIPE = 114,
  tag_OSMEMORY = 116,
  tag_ANONFILE = 117,
  tag_FILESYSTEM = 118,
  tag_SIGTIMER = 119,
  tag_WUTIMER = 120,
  tag_RESCHEDULETIMER = 121,
  tag_LINUX = 150
};

/****************** memory accessors ****************/

CC_mem_error_t targ2chkpt(LSE_chkpt::data_t *parent, CC_memory_t *memp,
			  LSE_emu_addr_t addr, LSE_emu_addr_t len) {
  CC_mem_data_t *realbuf;
  CC_mem_error_t rval;
  CC_mem_addr_t madelen, uaddr, ulen = len;
  int *flagsp;
  LSE_chkpt::data_t *dp;

  /* create a segmented octet string */
  dp=LSE_chkpt::build_explicit_tag(parent,LSE_chkpt::TAG_OCTETSTRING,
				   LSE_chkpt::CLASS_UNIVERSAL | 
				   LSE_chkpt::CONSTRUCTED);

  rval = CC_mem_translate(memp,addr,&madelen, &realbuf);
  if (rval) return rval;
  if (madelen < ulen) {
    uaddr = addr;
    while (madelen < ulen) {
      LSE_chkpt::build_octetstring(dp,realbuf,madelen*sizeof(CC_mem_data_t),TRUE);
      uaddr += madelen;
      ulen -= madelen;
      rval = CC_mem_translate(memp,uaddr,&madelen,&realbuf);
      if (rval) return rval;
    }
  }
  LSE_chkpt::build_octetstring(dp,realbuf,ulen*sizeof(CC_mem_data_t),TRUE);
  return (CC_mem_error_t)(0);
}


CC_mem_error_t chkpt2targ(LSE_chkpt::data_t *parent, CC_memory_t *memp,
			  LSE_emu_addr_t addr, LSE_emu_addr_t len) {
  CC_mem_error_t rval;
  LSE_chkpt::data_t *dp;

  dp = parent->oldestChild;

  while (dp) {
    LSE_emu_addr_t todo = len > dp->size ? dp->size : len;
    if (todo) {
      rval = CC_mem_write(memp,addr,todo,dp->content.stringVal);
      if (rval) return rval;
    }
    len -= todo;
    addr += todo;
    dp = dp->sibling;
  }
  return (CC_mem_error_t)(0);
}

int chkpt2targ_cmp(LSE_chkpt::data_t *parent, CC_memory_t *memp,
		   LSE_emu_addr_t addr, LSE_emu_addr_t len) {
  CC_mem_error_t rval;
  LSE_chkpt::data_t *dp;
  int res;

  dp = parent->oldestChild;

  while (dp) {
    LSE_emu_addr_t todo = len > dp->size ? dp->size : len;
    if (todo) {
      rval = CC_mem_memcmp(memp,addr,todo,
      	                   (CC_mem_data_t *)dp->content.stringVal,&res);
      if (rval) return -1;
      if (res) return res;
    }
    len -= todo;
    addr += todo;
    dp = dp->sibling;
  }
  return 0;
}

/****************** checkpoint helpers ******************/

static void chkpt_start(Linux_context_t *osct, int callno) {
  if (osct->os->OSstate == 1) { /* write */
    LSE_chkpt::data_t *dp;
    dp = LSE_chkpt::build_sequence(osct->os->osCalls[osct->tid]);
    osct->currOSRecord = dp;
    LSE_chkpt::build_signed(dp,callno);
    LSE_chkpt::build_sequence(dp); /* arguments */
    LSE_chkpt::build_sequence(dp); /* results */
    osct->os->doingCPoint = true;
  } else if (osct->os->OSstate == 2) { /* read */
    LSE_chkpt::data_t *dp;
    dp = osct->currOSRecord;
    if (!dp || callno != dp->oldestChild->content.int64Val) { /* error */

      CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);

      fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
	      osct->tid, CC_ict_true_startaddr(realct));
      fprintf(stderr,"Mismatched OS call: simulator %d, checkpoint %d\n",
	      callno, dp ? ((int)dp->oldestChild->content.int64Val) : -1);
      CC_sys_set_return(realct,0,-1);
      CC_sys_set_return(realct,1,OSDefs::oEINVAL);
      if (OS_ict_emuvar(realct,EMU_trace_syscalls))
	fprintf(stderr, "-1 %d\n", (int)OS_sys_get_return(1));
      abort();

    } else {
      osct->currArg = dp->oldestChild->sibling->oldestChild;
      osct->currRes = dp->oldestChild->sibling->sibling->oldestChild;
    }
    osct->os->doingCPoint = true;
  }
}

static void chkpt_arg_ptr(Linux_context_t *osct, LSE_emu_addr_t v) {
  if (osct->os->ctl.recordOS)  /* are we supposed to do args? */
    if (osct->os->OSstate == 1) { /* write */
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling;
      LSE_chkpt::build_unsigned(dp,(uint64_t)(v));
    } else if (osct->os->OSstate == 2) { /* read */
      if (!osct->currArg || (uint64_t)(v)!=osct->currArg->content.uint64Val) {

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);
	
	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
		osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Mismatched OS call argument\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      osct->currArg = osct->currArg->sibling;
    }
}

static void chkpt_arg_str(Linux_context_t *osct, char *v, 
			  LSE_emu_addr_t l) {
  if (osct->os->ctl.recordOS)  /* are we supposed to do args? */
    if (osct->os->OSstate == 1) { /* write */
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling;
      LSE_chkpt::build_stringN(dp, v, l, true);
    } else if (osct->os->OSstate == 2) { /* read */
      if (!osct->currArg || 
	  strncmp(v, (char *)osct->currArg->content.stringVal, l)) {

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);
	
	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
		osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Mismatched OS call argument\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      osct->currArg = osct->currArg->sibling;
    }
}

static void chkpt_arg_hmem(Linux_context_t *osct, unsigned char *v, 
			   LSE_emu_addr_t l) {
  if (osct->os->ctl.recordOS)  /* are we supposed to do args? */
    if (osct->os->OSstate == 1) { /* write */
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling;
      LSE_chkpt::build_octetstring(dp, v, l, true);
    } else if (osct->os->OSstate == 2) { /* read */
      if (!osct->currArg || 
	  memcmp(v, (void *)osct->currArg->content.stringVal, l)) {

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);
	
	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
		osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Mismatched OS call argument\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      osct->currArg = osct->currArg->sibling;
    }
}

static void chkpt_arg_tmem(Linux_context_t *osct, LSE_emu_addr_t v,
			   LSE_emu_addr_t l) {
  CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);
  if (osct->os->ctl.recordOS)  /* are we supposed to do args? */
    if (osct->os->OSstate == 1) { /* write */
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling;
      targ2chkpt(dp, CC_ict_mem(realct), v, l);
    } else if (osct->os->OSstate == 2) { /* read */
      if (!osct->currArg ||
	  chkpt2targ_cmp(osct->currArg, CC_ict_mem(realct), v, l)) {

	
	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
		osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Mismatched OS call argument\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      osct->currArg = osct->currArg->sibling;
    }
}

static void chkpt_arg_int(Linux_context_t *osct, uint64_t v) {
  if (osct->os->ctl.recordOS)  /* are we supposed to do args? */
    if (osct->os->OSstate == 1) { /* write */
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling;
      LSE_chkpt::build_unsigned(dp,v);
    } else if (osct->os->OSstate == 2) { /* read */
      if (!osct->currArg || v !=osct->currArg->content.uint64Val) {

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);
	
	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
		osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Mismatched OS call argument\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      osct->currArg = osct->currArg->sibling;
    }
}

static void chkpt_result_ptr(Linux_context_t *osct, LSE_emu_addr_t &v) {
  if (osct->os->ctl.recordOS) /* do we do results? */
    if (osct->os->OSstate == 1) {
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling->sibling;
      LSE_chkpt::build_unsigned(dp,(uint64_t)(v));
    } else if (osct->os->OSstate == 2) {
      if (!osct->currRes) { /* error */

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);

	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
                osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Unable to get OS call result\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      v = (LSE_emu_addr_t)osct->currRes->content.uint64Val;
      osct->currRes = osct->currRes->sibling;
    }
}

static void chkpt_result_str(Linux_context_t *osct, char *v, int l) {
  if (osct->os->ctl.recordOS) /* do we do results? */
    if (osct->os->OSstate == 1) {
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling->sibling;
      LSE_chkpt::build_stringN(dp, v, l, true);
    } else if (osct->os->OSstate == 2) {
      if (!osct->currRes) { /* error */

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);

	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
                osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Unable to get OS call result\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      strncpy(v, (char *)osct->currRes->content.stringVal, l);
      osct->currRes = osct->currRes->sibling;
    }
}

static void chkpt_result_tmem(Linux_context_t *osct, LSE_emu_addr_t v,
			      LSE_emu_addr_t l) {
  CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);

  if (osct->os->ctl.recordOS) /* do we do results? */
    if (osct->os->OSstate == 1) {
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling->sibling;
      targ2chkpt(dp, CC_ict_mem(realct), v, l);
    } else if (osct->os->OSstate == 2) {
      if (!osct->currRes) { /* error */

	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
                osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Unable to get OS call result\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      chkpt2targ(osct->currRes, CC_ict_mem(realct), v, l);
      osct->currRes = osct->currRes->sibling;
    }
}

static void chkpt_result_hmem(Linux_context_t *osct, unsigned char *v,
			      LSE_emu_addr_t l) {
  if (osct->os->ctl.recordOS) /* do we do results? */
    if (osct->os->OSstate == 1) {
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling->sibling;
      LSE_chkpt::build_octetstring(dp, v, l, true);
    } else if (osct->os->OSstate == 2) {
      if (!osct->currRes) { /* error */

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);

	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
                osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Unable to get OS call result\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      memcpy((void *)v, (void *)(osct->currRes->content.stringVal), l);
      osct->currRes = osct->currRes->sibling;
    }
}

static void chkpt_result_psect_context(Linux_context_t *osct, psect_context &v)
{
  if (osct->os->ctl.recordOS) /* do we do results? */
    if (osct->os->OSstate == 1) {
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling->sibling;
      v.chkptBuild(dp);
    } else if (osct->os->OSstate == 2) {
      if (!osct->currRes) { /* error */

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);

	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
                osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Unable to get OS call result\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      v.chkptParse(osct->currRes);
      osct->currRes = osct->currRes->sibling;
    }
}

template<class T> static void chkpt_result_int(Linux_context_t *osct, T &v) {
  if (osct->os->ctl.recordOS) /* do we do results? */
    if (osct->os->OSstate == 1) {
      LSE_chkpt::data_t *dp = osct->currOSRecord->oldestChild->sibling->sibling;
      LSE_chkpt::build_unsigned(dp,(uint64_t)(v));
    } else if (osct->os->OSstate == 2) {
      if (!osct->currRes) { /* error */

	CC_isacontext_t *realct = static_cast<CC_isacontext_t *>(osct->isacont);

	fprintf(stderr, "tid = %d, addr = 0x%"PRIx64"\n",
                osct->tid, CC_ict_true_startaddr(realct));
	fprintf(stderr,"Unable to get OS call result\n");
	CC_sys_set_return(realct,0,-1);
	CC_sys_set_return(realct,1,OSDefs::oEINVAL);
	abort();
      }
      v = (T)osct->currRes->content.uint64Val;
      osct->currRes = osct->currRes->sibling;
    }
}

static void chkpt_finish(Linux_context_t *osct) {
  if (osct->os->OSstate == 1 && osct->os->doingCPoint) {
     osct->currOSRecord->update_size();
     osct->os->doingCPoint = false;
  } else if (osct->os->OSstate == 2 && osct->os->doingCPoint) {
    if (osct->currOSRecord) osct->currOSRecord = osct->currOSRecord->sibling;
    osct->os->doingCPoint = false;
  }
}

/************** writing *****************/

void Linux_context_t::clear_chkpt(int kind) {
  LSE_chkpt::data_t *OSRecords;
  if (kind) {
    int ttype = LSE_chkpt::CLASS_CONTEXTSPECIFIC | LSE_chkpt::CONSTRUCTED;
    OSRecords = LSE_chkpt::build_explicit_tag(0, kind, ttype);
    LSE_chkpt::build_unsigned(OSRecords, oid);
    LSE_chkpt::build_unsigned(OSRecords, tid);
  } else OSRecords = 0;
  os->osCalls[tid] = OSRecords;
  currOSRecord = currArg = currRes = 0;
}

static LSE_chkpt::error_t build_contextlist(LSE_chkpt::data_t *p,
					    LSE_chkpt::file_t *cptFile,
					    int nmark, 
					    std::list<Linux_context_t *> &l) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *tdp = LSE_chkpt::build_sequence(p);
  for (std::list<Linux_context_t *>::const_iterator i = l.begin(),
       ie = l.end(); i != ie; ++i) {
    LSE_chkpt::build_signed(tdp, (*i)->oid);
  }
  tdp->update_size();
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t build_tgrouplist(LSE_chkpt::data_t *p,
					   LSE_chkpt::file_t *cptFile,
					   int nmark, 
					   std::list<Linux_tgroup_t *> &l) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *tdp = LSE_chkpt::build_sequence(p);
  for (std::list<Linux_tgroup_t *>::const_iterator i = l.begin(),
       ie = l.end(); i != ie; ++i) {
    LSE_chkpt::build_signed(tdp, (*i)->oid);
  }
  tdp->update_size();
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t build_signallist(LSE_chkpt::data_t *p, 
					   std::list<Linux_siginfo_t> &l) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *sl = LSE_chkpt::build_sequence(p);
  for (std::list<Linux_siginfo_t>::const_iterator i = l.begin(),
       ie = l.end(); i != ie; ++i) {
    LSE_chkpt::data_t *dp = LSE_chkpt::build_sequence(sl);
    LSE_chkpt::build_signed(dp, i->LSEsi_signo);
    LSE_chkpt::build_signed(dp, i->LSEsi_errno);
    LSE_chkpt::build_signed(dp, i->LSEsi_code);
    LSE_chkpt::build_signed(dp, i->LSEsi_trapno);
    LSE_chkpt::build_signed(dp, i->LSEsi_pid);
    LSE_chkpt::build_signed(dp, i->LSEsi_uid);
    LSE_chkpt::build_signed(dp, i->LSEsi_status);
    LSE_chkpt::build_unsigned(dp, i->LSEsi_utime);
    LSE_chkpt::build_unsigned(dp, i->LSEsi_stime);
    LSE_chkpt::build_signed(dp, i->LSEsi_value);
    LSE_chkpt::build_signed(dp, i->LSEsi_int);
    LSE_chkpt::build_unsigned(dp, i->LSEsi_ptr);
    LSE_chkpt::build_signed(dp, i->LSEsi_overrun);
    LSE_chkpt::build_signed(dp, i->LSEsi_timerid);
    LSE_chkpt::build_unsigned(dp, i->LSEsi_addr);
    LSE_chkpt::build_signed(dp, i->LSEsi_band);
    LSE_chkpt::build_signed(dp, i->LSEsi_fd);
  }
  sl->update_size();
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t
build_continuation(LSE_chkpt::data_t *p,
		   LSE_chkpt::file_t *cptFile,
		   int nmark,
		   Linux_continuation_t *c) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *tdp = LSE_chkpt::build_sequence(p);
  if (c) {
    LSE_chkpt::build_signed(tdp, (int)(c->kind));
    if ((cerr = c->build_chkpt(cptFile, tdp, nmark))) goto err;
  }
  tdp->update_size();
  return LSE_chkpt::error_None;
 err:
  delete tdp;
  return cerr;
}

static LSE_chkpt::error_t 
build_continuationvec(LSE_chkpt::data_t *p,
		      LSE_chkpt::file_t *cptFile,
		      int nmark, 
		      std::vector<Linux_continuation_t *> &l) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *tdp = LSE_chkpt::build_sequence(p);
  for (std::vector<Linux_continuation_t *>::const_iterator i = l.begin(),
       ie = l.end(); i != ie; ++i) {
    if ((cerr = build_continuation(tdp, cptFile, nmark, *i))) goto err;
  }
  tdp->update_size();
  return LSE_chkpt::error_None;
 err:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t cont_wait::build_chkpt(LSE_chkpt::file_t *cptFile,
					  LSE_chkpt::data_t *p,
					  int nmark) {
  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  LSE_chkpt::build_signed(p, pid);
  LSE_chkpt::build_signed(p, options);
  LSE_chkpt::build_unsigned(p, stataddr);
  LSE_chkpt::build_unsigned(p, infop);
  LSE_chkpt::build_unsigned(p, ru);
  LSE_chkpt::build_signed(p, iscompat);
  LSE_chkpt::build_signed(p, changelen);

  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t cont_futex::build_chkpt(LSE_chkpt::file_t *cptFile,
					   LSE_chkpt::data_t *p,
					   int nmark) {
  LSE_chkpt::error_t cerr;

  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  LSE_chkpt::build_unsigned(p, uaddr);
  LSE_chkpt::build_unsigned(p, paddr);
  LSE_chkpt::build_unsigned(p, futexBitset);
  if (futexwaitrec) {
    if ((cerr = futexwaitrec->build_chkpt(cptFile, nmark))) return cerr;
    LSE_chkpt::build_unsigned(p, futexwaitrec->oid);
    LSE_chkpt::build_unsigned(p, 
			      std::find(futexwaitrec->waiters.begin(),
					futexwaitrec->waiters.end(),
					this) - futexwaitrec->waiters.begin());
  } else {
    LSE_chkpt::build_unsigned(p, 0);
    LSE_chkpt::build_unsigned(p, 0);
  }
  if (timer) {
    if ((cerr = timer->build_chkpt(cptFile, nmark))) return cerr;
    LSE_chkpt::build_unsigned(p, timer->oid);
  } else LSE_chkpt::build_unsigned(p, 0);
    
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t cont_nsleep::build_chkpt(LSE_chkpt::file_t *cptFile,
					   LSE_chkpt::data_t *p,
					   int nmark) {
  LSE_chkpt::error_t cerr;

  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  if (timer) {
    if ((cerr = timer->build_chkpt(cptFile, nmark))) return cerr;
    LSE_chkpt::build_unsigned(p, timer->oid);
  } else LSE_chkpt::build_unsigned(p, 0);
  LSE_chkpt::build_signed(p, mode);
  LSE_chkpt::build_unsigned(p, rem);
    
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t cont_pause::build_chkpt(LSE_chkpt::file_t *cptFile,
					   LSE_chkpt::data_t *p,
					   int nmark) {
  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t cont_poll::build_chkpt(LSE_chkpt::file_t *cptFile,
					   LSE_chkpt::data_t *p,
					   int nmark) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *tdp;

  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  LSE_chkpt::build_unsigned(p, fds);
  LSE_chkpt::build_signed(p, nfds);
  LSE_chkpt::build_signed(p, timeout);
  tdp = LSE_chkpt::build_sequence(p);
  for (int i = 0; i < nfds ; ++i) {
    LSE_chkpt::build_signed(tdp, pfd[i].fd);
    LSE_chkpt::build_signed(tdp, pfd[i].events);
    LSE_chkpt::build_signed(tdp, pfd[i].revents);
  }
  tdp->update_size();
  tdp = LSE_chkpt::build_sequence(p);
  for (std::set<Linux_file_t *>::iterator i = files.begin(), ie = files.end();
       i != ie ; ++i) {
    if ((cerr = (*i)->build_chkpt(cptFile, nmark))) return cerr;
    LSE_chkpt::build_signed(tdp, (*i)->oid);
  }
  tdp->update_size();  
    
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t cont_piperead::build_chkpt(LSE_chkpt::file_t *cptFile,
					   LSE_chkpt::data_t *p,
					   int nmark) {
  LSE_chkpt::error_t cerr;

  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  if (pp) {
    if ((cerr = pp->build_chkpt(cptFile, nmark))) return cerr;
    LSE_chkpt::build_unsigned(p, pp->oid);
    LSE_chkpt::build_unsigned(p, 
			      std::find(pp->waiters.begin(),
					pp->waiters.end(),
					this) - pp->waiters.begin());
  } else {
    LSE_chkpt::build_unsigned(p, 0);
    LSE_chkpt::build_unsigned(p, 0);
  }
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t cont_ssuspend::build_chkpt(LSE_chkpt::file_t *cptFile,
					   LSE_chkpt::data_t *p,
					   int nmark) {
  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  return LSE_chkpt::error_None;
}

template<class SIGINFO>
LSE_chkpt::error_t 
cont_stimedwait<SIGINFO>::build_chkpt(LSE_chkpt::file_t *cptFile,
					   LSE_chkpt::data_t *p,
					   int nmark) {

  LSE_chkpt::error_t cerr;

  LSE_chkpt::build_signed(p, typeno);
  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  LSE_chkpt::build_unsigned(p, infop);
  LSE_chkpt::build_unsigned(p, savemask);
  LSE_chkpt::build_unsigned(p, thismask);
  if (timer) {
    if ((cerr = timer->build_chkpt(cptFile, nmark))) return cerr;
    LSE_chkpt::build_unsigned(p, timer->oid);
  } else LSE_chkpt::build_unsigned(p, 0);
    
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t cont_vfork::build_chkpt(LSE_chkpt::file_t *cptFile,
					   LSE_chkpt::data_t *p,
					   int nmark) {
  LSE_chkpt::build_unsigned(p, osct ? osct->oid : 0);
  return LSE_chkpt::error_None;
}

void Linux_timer_t::build_chkpt_piece(LSE_chkpt::data_t *tdp) {
  LSE_chkpt::build_unsigned(tdp, oid);
  LSE_chkpt::build_signed(tdp, clockid);
  LSE_chkpt::build_unsigned(tdp, tval.interval);
  LSE_chkpt::build_unsigned(tdp, tval.expiration);
  LSE_chkpt::build_unsigned(tdp, todo ? 1 : 0);
}

LSE_chkpt::error_t Linux_sig_timer_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						  int nmark) {
  LSE_chkpt::error_t cerr;

  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_SIGTIMER,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );

  build_chkpt_piece(tdp);
  LSE_chkpt::build_signed(tdp, signo);
  if (target) {
    if ((cerr = target->build_chkpt(cptFile, nmark))) goto bad;
    LSE_chkpt::build_unsigned(tdp, target->oid);
  } else LSE_chkpt::build_unsigned(tdp, 0);
  if (tgroup) {
    if ((cerr = tgroup->build_chkpt(cptFile, nmark))) goto bad;
    LSE_chkpt::build_unsigned(tdp, tgroup->oid);
  } else LSE_chkpt::build_unsigned(tdp, 0);

  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_wu_timer_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						 int nmark) {
  LSE_chkpt::error_t cerr;

  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_WUTIMER,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );

  build_chkpt_piece(tdp);
  if (target) {
    if ((cerr = target->build_chkpt(cptFile, nmark))) goto bad;
    LSE_chkpt::build_unsigned(tdp, target->oid);
  } else LSE_chkpt::build_unsigned(tdp, 0);

  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t 
Linux_reschedule_timer_t::build_chkpt(LSE_chkpt::file_t *cptFile,
				      int nmark) {
  LSE_chkpt::error_t cerr;

  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_RESCHEDULETIMER,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  build_chkpt_piece(tdp);
  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_waitqueue_t::build_chkpt(LSE_chkpt::data_t *p,
						  LSE_chkpt::file_t *cptFile,
                                                  int nmark) {
  build_contextlist(p, cptFile, nmark, waiters);
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t Linux_sighandtable_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						     int nmark) {
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_SIGHANDTABLE,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  for (Linux_sighandinf_t *i = handlers, *ie = &handlers[0]+64; i != ie ; ++i) {
    LSE_chkpt::build_unsigned(tdp, i->handler);
    LSE_chkpt::build_unsigned(tdp, i->restorer);
    LSE_chkpt::build_unsigned(tdp, i->sigmask);
    LSE_chkpt::build_unsigned(tdp, i->flags);
  }
  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
}

LSE_chkpt::error_t Linux_tgroup_t::build_chkpt(LSE_chkpt::file_t *cptFile,
					       int nmark) {
  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_TGROUP,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::build_signed(tdp, tgid);
  if (pgroup) {
    if ((cerr = pgroup->build_chkpt(cptFile, nmark))) goto bad;
    LSE_chkpt::build_unsigned(tdp,pgroup->oid);
  } else LSE_chkpt::build_unsigned(tdp, 0);
  if ((cerr = build_contextlist(tdp, cptFile, nmark, members))) goto bad;
  if ((cerr = build_signallist(tdp, signals))) goto bad;
  LSE_chkpt::build_unsigned(tdp, sigpending);
  LSE_chkpt::build_signed(tdp, exiting);
  LSE_chkpt::build_signed(tdp, exitcodeValid);
  LSE_chkpt::build_signed(tdp, exittask ? exittask->oid : 0);
  if ((cerr = childwait.build_chkpt(tdp, cptFile, nmark))) goto bad;
  LSE_chkpt::build_signed(tdp, exitcode);
  LSE_chkpt::build_signed(tdp, nCount);
  LSE_chkpt::build_signed(tdp, nLive);
  for (int i = 0; i < 3; ++i) {
    if (timers[i]) {
      if ((cerr = timers[i]->build_chkpt(cptFile, nmark))) goto bad;
      LSE_chkpt::build_unsigned(tdp, timers[i]->oid);
    }
    else LSE_chkpt::build_unsigned(tdp, 0);
  }
  LSE_chkpt::build_unsigned(tdp, cputime_curr_interval);
  LSE_chkpt::build_unsigned(tdp, cputime_cumulative);
  for (int i = 0; i < OSDefs::oRLIMIT_NLIMITS; ++i) 
    for (int j = 0; j < 2; ++j)
      LSE_chkpt::build_unsigned(tdp, rlimits[i][j]);
  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_pgroup_t::build_chkpt(LSE_chkpt::file_t *cptFile,
					       int nmark) {
  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_PGROUP,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::build_signed(tdp, pgid);
  if ((cerr = build_tgrouplist(tdp, cptFile, nmark, members))) goto bad;

  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_fs_t::build_chkpt(LSE_chkpt::file_t *cptFile,
					   int nmark) {
  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_FILESYSTEM,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);
  LSE_chkpt::build_stringN(tdp, cwd, strlen(cwd), true);
  LSE_chkpt::build_stringN(tdp, fsroot, strlen(fsroot), true);

  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_futex_t::build_chkpt(LSE_chkpt::file_t *cptFile,
					      int nmark) {
  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_FUTEX,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);
  LSE_chkpt::build_unsigned(tdp, bitset);
  LSE_chkpt::build_unsigned(tdp, waiters.size());

  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_fdtable_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						int nmark) {
  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_FDTABLE,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::data_t *dp; dp = LSE_chkpt::build_sequence(tdp);
  for (std::map<int, Linux_fd_t>::iterator i = fds.begin(), ie = fds.end();
       i != ie; ++i) {
    LSE_chkpt::build_signed(dp, i->first);
    if ((cerr = i->second.fp->build_chkpt(cptFile, nmark))) goto bad;
    LSE_chkpt::build_unsigned(dp, i->second.fp->oid);
    LSE_chkpt::build_signed(dp, i->second.flags);
  }

  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_pipe_t::build_chkpt(LSE_chkpt::file_t *cptFile,
					     int nmark) {
  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_PIPE,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::build_signed(tdp, numwriters);
  LSE_chkpt::build_signed(tdp, numreaders);
  LSE_chkpt::build_stringN(tdp, buf.c_str(), buf.size(), true);
  LSE_chkpt::build_unsigned(tdp, waiters.size());

  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_error_file_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						   int nmark) {
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_ERRORFILE,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);
  
  LSE_chkpt::build_signed(tdp, status);
  LSE_chkpt::build_signed(tdp, OSblocking);
  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
}

LSE_chkpt::error_t Linux_native_file_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						    int nmark) {
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_NATIVEFILE,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::build_signed(tdp, status);
  LSE_chkpt::build_signed(tdp, OSblocking);
  LSE_chkpt::build_signed(tdp, nativefd);  // NOTE: useless!
  LSE_chkpt::build_signed(tdp, statbuf.st_dev);
  LSE_chkpt::build_signed(tdp, statbuf.st_ino);
  
  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
}

LSE_chkpt::error_t Linux_anon_file_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						    int nmark) {
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_NATIVEFILE,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::build_signed(tdp, status);
  LSE_chkpt::build_signed(tdp, OSblocking);
  LSE_chkpt::build_signed(tdp, ino);
  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
}

LSE_chkpt::error_t Linux_pipe_file_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						  int nmark) {
  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_NATIVEFILE,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::data_t *p;
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::build_signed(tdp, status);
  LSE_chkpt::build_signed(tdp, OSblocking);
  if (mypipe) {
    if ((cerr = mypipe->build_chkpt(cptFile, nmark))) goto bad;
    LSE_chkpt::build_unsigned(tdp, mypipe->oid);
  } else LSE_chkpt::build_unsigned(tdp, 0);
  LSE_chkpt::build_signed(tdp, isWriter);

  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_socket_file_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						    int nmark) {
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_NATIVEFILE,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::build_signed(tdp, status);
  LSE_chkpt::build_signed(tdp, OSblocking);
  LSE_chkpt::build_signed(tdp, nativefd);  // NOTE: useless!
  tdp->update_size();
  return cptFile->write_to_segment(true, tdp);
}

LSE_chkpt::error_t Linux_memory_t::build_chkpt(LSE_chkpt::file_t *cptFile,
					       int nmark) {
  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *dp;
  LSE_chkpt::data_t *tdp = LSE_chkpt::build_explicit_tag(0, tag_OSMEMORY,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(tdp, oid);

  LSE_chkpt::build_unsigned(tdp, heapstart);
  LSE_chkpt::build_unsigned(tdp, heapbreak);

  dp = LSE_chkpt::build_sequence(tdp);
  for (std::list<std::pair<LSE_emu_addr_t, LSE_emu_addr_t> >::iterator
	 i = codelocs.begin(), ie = codelocs.end(); i != ie ; ++i) {
    LSE_chkpt::build_unsigned(dp, (*i).first);
    LSE_chkpt::build_unsigned(dp, (*i).second);
  }

  if (pmapper.PM) {
    if ((cerr = pmapper.PM->build_chkpt(cptFile, nmark))) goto bad;
    LSE_chkpt::build_unsigned(tdp, pmapper.PM->oid);
  } else LSE_chkpt::build_unsigned(tdp, 0);

  dp = LSE_chkpt::build_sequence(tdp);
  for (ranges_t::iterator i=vranges.begin(), ie=vranges.end(); i != ie ; ++i) {
    // Note: no need to save i->first as it equals i->second.end
    LSE_chkpt::build_unsigned(dp, i->second.start);
    LSE_chkpt::build_unsigned(dp, i->second.end);
    LSE_chkpt::build_signed(dp, i->second.flags);
    LSE_chkpt::build_signed(dp, i->second.isShared);
    LSE_chkpt::build_unsigned(dp, i->second.offset); 
    if (i->second.fp) {
      if ((cerr = i->second.fp->build_chkpt(cptFile, nmark))) goto bad;
      LSE_chkpt::build_unsigned(dp, i->second.fp->oid);
    } else {
      LSE_chkpt::build_unsigned(dp, 0);
    }
  }

  tdp->update_size();
  if ((cerr = cptFile->write_to_segment(true, tdp))) goto bad;  
  tdp = 0;

  /* write out the translation device information */
  cerr = vmdevice.writeChkptGuts(cptFile, false);
  
 bad:
  delete tdp;
  return cerr;
}

LSE_chkpt::error_t Linux_context_t::build_chkpt(LSE_chkpt::file_t *cptFile,
						int nmark) {

  LSE_chkpt::error_t cerr;
  if (mark == nmark) return LSE_chkpt::error_None; // already written
  mark = nmark;

  LSE_chkpt::data_t *crec = LSE_chkpt::build_explicit_tag(0, tag_OSCONTEXT,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			       	         LSE_chkpt::CONSTRUCTED) );
  LSE_chkpt::build_unsigned(crec, oid);

  if ((cerr = mem->build_chkpt(cptFile, nmark))) goto err;
  LSE_chkpt::build_unsigned(crec,mem->oid);
  if (fdtable) {
    if ((cerr = fdtable->build_chkpt(cptFile, nmark))) goto err;
    LSE_chkpt::build_unsigned(crec,fdtable->oid);
  } else LSE_chkpt::build_unsigned(crec, 0);
  if (sighandtable) {
    if ((cerr = sighandtable->build_chkpt(cptFile, nmark))) goto err;
    LSE_chkpt::build_unsigned(crec,sighandtable->oid);
  } else LSE_chkpt::build_unsigned(crec, 0);
  if (tgroup) {
    if ((cerr = tgroup->build_chkpt(cptFile, nmark))) goto err;
    LSE_chkpt::build_unsigned(crec,tgroup->oid);
  } else LSE_chkpt::build_unsigned(crec, 0);
  if ((cerr = reportVforkDone.build_chkpt(crec, cptFile, nmark))) goto err;
  if (fs) {
    if ((cerr = fs->build_chkpt(cptFile, nmark))) goto err;
    LSE_chkpt::build_unsigned(crec, fs->oid);
  } else LSE_chkpt::build_unsigned(crec, 0);
  LSE_chkpt::build_signed(crec, exit_code);
  LSE_chkpt::build_signed(crec, signalOnExit);
  LSE_chkpt::build_signed(crec, state);
  LSE_chkpt::build_unsigned(crec, clear_child_tid);
  LSE_chkpt::build_unsigned(crec, robustFutexList);
#ifdef LSE_LINUX_COMPAT
  LSE_chkpt::build_unsigned(crec, robustFutexListCompat);
#endif
  LSE_chkpt::build_signed(crec, tid);
  LSE_chkpt::build_signed(crec, gid);
  LSE_chkpt::build_signed(crec, egid);
  LSE_chkpt::build_signed(crec, uid);
  LSE_chkpt::build_signed(crec, euid);
  LSE_chkpt::build_signed(crec, is64bit);
  if ((cerr = build_contextlist(crec, cptFile, nmark, children))) goto err;

  if (parent) LSE_chkpt::build_unsigned(crec, parent->oid);
  else LSE_chkpt::build_signed(crec, 0);

  LSE_chkpt::build_unsigned(crec, sigblocked);
  LSE_chkpt::build_unsigned(crec, oldsigblocked);
  if ((cerr = build_signallist(crec, signals))) goto err;
  LSE_chkpt::build_unsigned(crec, sigpending);
  LSE_chkpt::build_signed(crec, oldSigBlockedValid);
  LSE_chkpt::build_signed(crec, sigPendingFlag);
  LSE_chkpt::build_signed(crec, schedPolicy);
  LSE_chkpt::build_octetstring(crec, (unsigned char *)&schedParms,
                               sizeof(schedParms), false);
  LSE_chkpt::build_unsigned(crec, sigAlt.LSE_ss_sp);
  LSE_chkpt::build_signed(crec, sigAlt.LSE_ss_flags);
  LSE_chkpt::build_unsigned(crec, sigAlt.LSE_ss_size);

  if ((cerr = build_continuationvec(crec, cptFile, nmark, continuationStack)))
      goto err;
  if ((cerr = build_continuation(crec, cptFile, nmark, restart))) goto err;
  LSE_chkpt::build_unsigned(crec, cputime_curr_interval);
  LSE_chkpt::build_unsigned(crec, cputime_cumulative);

  crec->update_size();
  if ((cerr = cptFile->write_to_segment(true, crec))) return cerr;
  crec = 0;

  CC_ict_write_chkpt(isacont, &os->ctl);
  return LSE_chkpt::error_None;
err:
  delete crec;
  return cerr;
}

LSE_chkpt::error_t 
Linux_chkpt_write_segment(LSE_emu_interface_t *intr,
			  LSE_chkpt::file_t *cptFile, const char *segmentName, 
			  int step, LSE_emu_chkpt_cntl_t *ctl) {
  Linux_dinst_t *di = OS_interface(intr);

  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp,*tdp, *tdp2;

  di->ctl = *ctl;

  if (step == 0) {

    ++di->cptcount;

    tdp = LSE_chkpt::build_header(0, tag_LINUX,
	 			  (LSE_chkpt::CLASS_CONTEXTSPECIFIC |
			           LSE_chkpt::CONSTRUCTED), -1 );
    if ((cerr = cptFile->write_to_segment(true, tdp))) return cerr;

    tdp = LSE_chkpt::build_unsigned(0, CC_time_get_currtime(di));
    if ((cerr = cptFile->write_to_segment(true, tdp))) return cerr;
    
    // handle all the contexts mapped to this emulator
    for (std::map<int, Linux_context_t *>::iterator i = di->threads.begin(),
         ie = di->threads.end() ; i != ie ; ++i) {
      Linux_context_t *osct = i->second;

      // set up syscall recording
      delete di->osCalls[i->first]; di->osCalls[i->first] = 0;
      osct->clear_chkpt(tag_OSTRACE);

      // write out context and anything else it depends upon
      if ((cerr = osct->build_chkpt(cptFile, di->cptcount))) return cerr;
    }

    // output OS instance; just last_pid at present!
    tdp = LSE_chkpt::build_explicit_tag(0, tag_OSINST,
					(LSE_chkpt::CLASS_CONTEXTSPECIFIC |
					 LSE_chkpt::CONSTRUCTED) );

    // start with futexes (might have been possible to do later, but does
    // not hurt
    dp = LSE_chkpt::build_sequence(tdp);
    for (Linux_dinst_t::fm_t::iterator i=di->futexes.begin(), 
	   ie = di->futexes.end(); 
	 i != ie ; ++i) {
      LSE_chkpt::build_unsigned(dp, i->first);
      if ((cerr = i->second->build_chkpt(cptFile, di->cptcount))) return cerr;
      LSE_chkpt::build_unsigned(dp, i->second->oid);
    }

    // threads
    dp = LSE_chkpt::build_sequence(tdp);
    for (std::map<int, Linux_context_t *>::iterator i = di->threads.begin(),
	   ie = di->threads.end(); i != ie ; ++i)
      // no need to write out thread ID; it is in the context
      LSE_chkpt::build_unsigned(dp, (i->second)->oid);

    // tgroups
    dp = LSE_chkpt::build_sequence(tdp);
    for (std::map<int, Linux_tgroup_t *>::iterator i = di->tgroups.begin(),
	   ie = di->tgroups.end(); i != ie ; ++i)
      LSE_chkpt::build_unsigned(dp, (i->second)->oid);

    // pgroups
    dp = LSE_chkpt::build_sequence(tdp);
    for (std::map<int, Linux_pgroup_t *>::iterator i = di->pgroups.begin(),
	   ie = di->pgroups.end(); i != ie ; ++i)
      LSE_chkpt::build_unsigned(dp, (i->second)->oid);

    // running/ready list: running goes first, then ready so that we
    // schedule on in the current order.  However, scheduling will start
    // again with current values...
    dp = LSE_chkpt::build_sequence(tdp);
    for (std::list<Linux_context_t *>::iterator i = di->running_list.begin(),
	   ie = di->running_list.end(); i != ie ; ++i)
      LSE_chkpt::build_unsigned(dp, (*i)->oid);
    for (std::deque<Linux_context_t *>::iterator i = di->ready_list.begin(),
	   ie = di->ready_list.end(); i != ie ; ++i)
      LSE_chkpt::build_unsigned(dp, (*i)->oid);

    LSE_chkpt::build_signed(tdp, di->last_pid);
    LSE_chkpt::build_unsigned(tdp, di->objC);
    LSE_chkpt::build_signed(tdp, di->anonCounter);
    LSE_chkpt::build_unsigned(tdp, di->boottime);

    if (di->reschedule_timer) {
      if ((cerr = di->reschedule_timer->build_chkpt(cptFile, di->cptcount))) 
	return cerr;
      LSE_chkpt::build_unsigned(tdp,di->reschedule_timer->oid);
    } else LSE_chkpt::build_unsigned(tdp, 0);

    if (di->errorFile) {
      if ((cerr = di->errorFile->build_chkpt(cptFile, di->cptcount))) 
	return cerr;
      LSE_chkpt::build_unsigned(tdp, di->errorFile->oid);
    } else LSE_chkpt::build_unsigned(tdp, 0);

    // event Queue.  Annoying because I cannot iterate across it easily
    dp = LSE_chkpt::build_sequence(tdp);
    {
      eventQueue_t tq(di->eventQueue);
      while (!tq.empty()) {
	Linux_todo_t *todo = tq.top();
	tq.pop();
	if (todo->cancelled) continue; // no need to save cancelled events
	LSE_chkpt::build_unsigned(dp, todo->kind);
	LSE_chkpt::build_unsigned(dp, todo->intendedTime);
	switch (todo->kind) {
	case Linux_todo_t::todo_kind_timer : 
	  {
	    todo_timer *tt = dynamic_cast<todo_timer *>(todo);
	    if (tt->timer) {
	      if ((cerr = tt->timer->build_chkpt(cptFile, di->cptcount)))
		return cerr;
	      LSE_chkpt::build_unsigned(dp, tt->timer->oid);
	    } else LSE_chkpt::build_unsigned(dp, 0);
	    break;
	  }
	default: break;
	} // case
      } // queue
    }

    dp = LSE_chkpt::build_sequence(tdp);
    for (std::list<Linux_dinst_t::prange_t>::iterator 
	   i = di->freePpages.begin(), ie = di->freePpages.end();
	 i != ie ; ++i) {
      LSE_chkpt::build_unsigned(dp, i->pno);
      LSE_chkpt::build_unsigned(dp, i->num);
    }

    dp = LSE_chkpt::build_sequence(tdp);
    for (std::map<LSE_emu_addr_t, Linux_dinst_t::pinfo_t>::iterator
	   i = di->usedPpages.begin(), ie = di->usedPpages.end();
	 i != ie; ++i) {
      LSE_chkpt::build_unsigned(dp, i->first);
      LSE_chkpt::build_signed(dp, i->second.refcnt);
      LSE_chkpt::build_signed(dp, i->second.offset);
    }

    dp = LSE_chkpt::build_sequence(tdp);
    for (std::map<inode_id_t, Linux_dinst_t::fpagemap_t>::iterator
	   i = di->inode2pages.begin(), ie = di->inode2pages.end();
	 i != ie; ++i) {
      LSE_chkpt::build_signed(dp, i->first.dev);
      LSE_chkpt::build_signed(dp, i->first.ino);
      LSE_chkpt::data_t *ndp = LSE_chkpt::build_sequence(dp);
      for (std::map<LSE_emu_addr_t,LSE_emu_addr_t>::iterator
	     j = i->second.offset2pno.begin(), je = i->second.offset2pno.end();
	   j != je; ++j) {
	LSE_chkpt::build_unsigned(ndp, j->first);
	LSE_chkpt::build_unsigned(ndp, j->second);
      }
    }

    tdp->update_size();
    if ((cerr = cptFile->write_to_segment(true, tdp))) return cerr;
  
    dp = LSE_chkpt::build_indefinite_end(0);
    if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;

    di->OSstate = ctl->recordOS ? 1 : 0;

  } else if (step == 1) {

    tdp = LSE_chkpt::build_header(0, tag_LINUX,
				  (LSE_chkpt::CLASS_CONTEXTSPECIFIC |
				   LSE_chkpt::CONSTRUCTED), -1 );
    if ((cerr = cptFile->write_to_segment(true, tdp))) return cerr;

    // handle all the recorded OS calls
    for (std::map<int, LSE_chkpt::data_t *>::iterator i = di->osCalls.begin(),
         ie = di->osCalls.end() ; i != ie ; ++i) 
      if (ctl->recordOS && i->second) 
	if ((cerr = cptFile->write_to_segment(true, i->second))) return cerr;
    
    di->OSstate = 0;
    di->osCalls.clear();

    dp = LSE_chkpt::build_indefinite_end(0);
    if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;

  } // step == 1
  else return LSE_chkpt::error_BadArgument;

  return LSE_chkpt::error_None;
}

/************** reading *****************/

// This is scary code which relies upon the pointers to a subclass being
// the same as a pointer to a parent..... ugh..
template<class T> static void 
record_fixup(class Linux_dinst_t *di, uint64_t oid, T **addr) {
  Linux_obj_t **p = reinterpret_cast<Linux_obj_t **>(addr);
  if (oid) di->fixups.push_back(Linux_dinst_t::fixup_t(oid, p));
  else *p = 0;
}

template<class T> static void 
insert_obj(class Linux_dinst_t *di, uint64_t oid, 
	   T **addr, bool repl) {
  T *p;
  Linux_dinst_t::objmap_t::iterator o 
    = oid ? di->objects.find(oid) : di->objects.end();
  if (o == di->objects.end()) p = 0;
  else p = static_cast<T *>(o->second);
  if (p) p->incr();
  if (repl && *addr) (*addr)->decr();
  *addr = static_cast<T *>(p);
}

template<class T> static void 
insert_obj_noref(class Linux_dinst_t *di, uint64_t oid, 
		 T **addr) {
  T *p;
  Linux_dinst_t::objmap_t::iterator o 
    = oid ? di->objects.find(oid) : di->objects.end();
  if (o == di->objects.end()) p = 0;
  else p = static_cast<T *>(o->second);
  *addr = static_cast<T *>(p);
}

static LSE_chkpt::error_t 
test_for_indefinite(LSE_chkpt::file_t *cptFile) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp;

  /* make sure we got the indefinite end */
  if ((cerr = cptFile->read_from_segment(NULL,&dp))) return cerr;
  if (dp) {
    delete (dp);
    return LSE_chkpt::error_FileFormat;
  }
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t validate_children(LSE_chkpt::data_t *dp, int num,
                                            bool atleast=false) {
  dp = dp->oldestChild;
  while (num && dp)  { dp=dp->sibling; num--; }
  if (dp && !num && !atleast || num && !dp) return LSE_chkpt::error_FileFormat;
  else return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t validate_siblings(LSE_chkpt::data_t *dp, int num,
                                            bool atleast=false) {
  while (num && dp->sibling)  { dp=dp->sibling; num--; }
  if (dp->sibling && !num && !atleast || num && !dp->sibling) 
    return LSE_chkpt::error_FileFormat;
  else return LSE_chkpt::error_None;
}

static inline LSE_chkpt::data_t *next(LSE_chkpt::data_t *&p) {
  LSE_chkpt::data_t *t = p;
  p = p->sibling;
  return t;
}

static LSE_chkpt::error_t
parse_contextlist(class Linux_dinst_t *di, LSE_chkpt::data_t *dp,
		  std::list<Linux_context_t *> &l) {
  LSE_chkpt::data_t *cdp;

  l.clear();
  cdp = dp->oldestChild;
  while (cdp) {
    uint64_t oid = next(cdp)->content.uint64Val;
    l.push_back(0);
    record_fixup(di, oid, &(l.back()));
#ifdef DEBUG_CPOINT
      std::cerr << "\t" << oid << "\n";
#endif
  }
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t
parse_tgrouplist(class Linux_dinst_t *di, LSE_chkpt::data_t *dp,
		  std::list<Linux_tgroup_t *> &l) {
  LSE_chkpt::data_t *cdp;

  l.clear();
  cdp = dp->oldestChild;
  while (cdp) {
    uint64_t oid = next(cdp)->content.uint64Val;
    l.push_back(0);
    record_fixup(di, oid, &(l.back()));
#ifdef DEBUG_CPOINT
      std::cerr << "\t" << oid << "\n";
#endif
  }
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t
parse_signallist(class Linux_dinst_t *di, LSE_chkpt::data_t *dp,
		  std::list<Linux_siginfo_t> &l) {
  LSE_chkpt::data_t *cdp;

  l.clear();
  cdp = dp->oldestChild;
  while (cdp) {
    if (validate_children(cdp,17)) return LSE_chkpt::error_FileFormat;
    dp = next(cdp)->oldestChild;
    Linux_siginfo_t ni;

    ni.LSEsi_signo = next(dp)->content.int64Val,
    ni.LSEsi_errno = next(dp)->content.int64Val,
    ni.LSEsi_code = next(dp)->content.int64Val,
    ni.LSEsi_trapno = next(dp)->content.int64Val,
    ni.LSEsi_pid = next(dp)->content.int64Val,
    ni.LSEsi_uid = next(dp)->content.int64Val,
    ni.LSEsi_status = next(dp)->content.int64Val,
    ni.LSEsi_utime = next(dp)->content.uint64Val,
    ni.LSEsi_stime = next(dp)->content.uint64Val,
    ni.LSEsi_value = next(dp)->content.int64Val,
    ni.LSEsi_int = next(dp)->content.int64Val,
    ni.LSEsi_ptr = next(dp)->content.uint64Val,
    ni.LSEsi_overrun = next(dp)->content.int64Val,
    ni.LSEsi_timerid = next(dp)->content.int64Val,
    ni.LSEsi_addr = next(dp)->content.uint64Val,
    ni.LSEsi_band = next(dp)->content.int64Val,
    ni.LSEsi_fd = next(dp)->content.int64Val,

    l.push_back(ni);
  }
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t
parse_continuation(class Linux_dinst_t *di, LSE_chkpt::data_t *dp,
		   Linux_continuation_t * &l) {
  // dp points to the continuation record
  Linux_continuation_t *np = 0;
  LSE_chkpt::data_t *cdp;
  LSE_chkpt::error_t cerr;

  delete l;
  cdp = dp->oldestChild; 
  if (cdp) { // not an empty record ; cdp now points to kind
    switch ((Linux_continuation_t::cont_kinds)(cdp->content.int64Val)) {
    case Linux_continuation_t::cont_kind_wait: {
      cont_wait *p = 0;
      if ((cerr = cont_wait::parse_chkpt(di, dp, p))) return cerr;
      np = p;
      break;
    }
    case Linux_continuation_t::cont_kind_futex: {
      cont_futex *p = 0;
      if ((cerr = cont_futex::parse_chkpt(di, dp, p))) return cerr;
      np = p;
      break;
    }
    case Linux_continuation_t::cont_kind_nsleep: {
      cont_nsleep *p = 0;
      if ((cerr = cont_nsleep::parse_chkpt(di, dp, p))) return cerr;
      np = p;
      break;
    }
    case Linux_continuation_t::cont_kind_poll: {
      cont_poll *p = 0;
      if ((cerr = cont_poll::parse_chkpt(di, dp, p))) return cerr;
      np = p;
      break;
    }
    case Linux_continuation_t::cont_kind_piperead: {
      cont_piperead *p = 0;
      if ((cerr = cont_piperead::parse_chkpt(di, dp, p))) return cerr;
      np = p;
      break;
    }
    case Linux_continuation_t::cont_kind_ssuspend: {
      cont_ssuspend *p = 0;
      if ((cerr = cont_ssuspend::parse_chkpt(di, dp, p))) return cerr;
      np = p;
      break;
    }
    case Linux_continuation_t::cont_kind_vfork: {
      cont_vfork *p = 0;
      if ((cerr = cont_vfork::parse_chkpt(di, dp, p))) return cerr;
      np = p;
      break;
    }
    case Linux_continuation_t::cont_kind_pause: {
      cont_pause *p = 0;
      if ((cerr = cont_pause::parse_chkpt(di, dp, p))) return cerr;
      np = p;
      break;
    }
    case Linux_continuation_t::cont_kind_stimedwait: {
      switch (cdp->sibling->content.int64Val) {
      case 0: {
	cont_stimedwait<OSDefs::L_siginfo> *p = 0;
	if ((cerr = cont_stimedwait<OSDefs::L_siginfo>::parse_chkpt(di, dp, p)
	     )) return cerr;
	np = p;
	break;
      }
#ifdef LSE_LINUX_COMPAT
      case 1: {
	cont_stimedwait<OSDefs::L_compat_siginfo> *p = 0;
	if ((cerr = cont_stimedwait<OSDefs::L_compat_siginfo>::
	     parse_chkpt(di, dp, p))) return cerr;
	np = p;
	break;
      }
#endif
      default:
	break;
      }
      break;
    }
    default:
      break;
    }
  }
  l = np;
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t
parse_continuationvec(class Linux_dinst_t *di, LSE_chkpt::data_t *dp,
		      std::vector<Linux_continuation_t *> &l) {
  // dp points to the sequence of continuation records
  LSE_chkpt::data_t *cdp;

  //l.clear();
  cdp = dp->oldestChild; // cdp will point to each continuation rec
  std::vector<Linux_continuation_t *> newvec;
  while (cdp) {
    Linux_continuation_t *nv = 0;
    if (parse_continuation(di, next(cdp), nv))
      return LSE_chkpt::error_FileFormat;
    newvec.push_back(nv);
  }
  for (std::vector<Linux_continuation_t *>::iterator 
	 i = l.begin(), ie = l.end() ; i != ie; ++i) {
    delete *i;
  }
  l.clear();
  l = newvec;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
cont_wait::parse_chkpt(class Linux_dinst_t *di,
		       LSE_chkpt::data_t *tdp, cont_wait *&nv) {

  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 9))) return cerr;
  
  next(dp); // skip the marker for kind of continuation

  cont_wait *n = new cont_wait();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  n->pid = next(dp)->content.int64Val;
  n->options = next(dp)->content.int64Val;
  n->stataddr = next(dp)->content.uint64Val;
  n->infop = next(dp)->content.uint64Val;
  n->ru = next(dp)->content.uint64Val;
  n->iscompat = next(dp)->content.int64Val;
  n->changelen = next(dp)->content.int64Val;
  nv = n;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
cont_futex::parse_chkpt(class Linux_dinst_t *di,
			LSE_chkpt::data_t *tdp, cont_futex *&nv) {

  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 8))) return cerr;
  
  next(dp); // skip the marker for kind of continuation

  cont_futex *n = new cont_futex();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  n->uaddr       = next(dp)->content.uint64Val;
  n->paddr       = next(dp)->content.uint64Val;
  n->futexBitset = next(dp)->content.uint64Val;
  insert_obj_noref(di, next(dp)->content.uint64Val, &n->futexwaitrec);
  if (n->futexwaitrec)
    n->futexwaitrec->waiters[next(dp)->content.uint64Val] = n;
  else next(dp);
  insert_obj(di, next(dp)->content.uint64Val, &n->timer, false);
  nv = n;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
cont_nsleep::parse_chkpt(class Linux_dinst_t *di,
		       LSE_chkpt::data_t *tdp, cont_nsleep *&nv) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 5))) return cerr;
  
  next(dp); // skip the marker for kind of continuation

  cont_nsleep *n = new cont_nsleep();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  insert_obj(di, next(dp)->content.uint64Val, &n->timer, false);
  n->mode       = next(dp)->content.int64Val;
  n->rem        = next(dp)->content.uint64Val;
  nv = n;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
cont_pause::parse_chkpt(class Linux_dinst_t *di,
			LSE_chkpt::data_t *tdp, cont_pause *&nv) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 2))) return cerr;
  
  next(dp); // skip the marker for kind of continuation

  cont_pause *n = new cont_pause();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  nv = n;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
cont_poll::parse_chkpt(class Linux_dinst_t *di,
		       LSE_chkpt::data_t *tdp, cont_poll *&nv) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;
  LSE_chkpt::data_t *sdp;

  if ((cerr = validate_children(tdp, 7))) return cerr;  
  next(dp); // skip the marker for kind of continuation

  cont_poll *n = new cont_poll();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  n->fds     = next(dp)->content.uint64Val;
  n->nfds    = next(dp)->content.int64Val;
  n->timeout = next(dp)->content.int64Val;
  sdp = next(dp)->oldestChild;
  n->pfd = new struct pollfd[n->nfds];
  for (int i = 0; i < n->nfds; ++i) {
    n->pfd[i].fd      = next(sdp)->content.int64Val;
    n->pfd[i].events  = next(sdp)->content.int64Val;
    n->pfd[i].revents = next(sdp)->content.int64Val;
  }
  sdp = next(dp)->oldestChild;
  while (sdp) {
    Linux_file_t *f = 0;
    insert_obj_noref(di, next(sdp)->content.uint64Val, &f);
    if (f) n->files.insert(f);
  }
  nv = n;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
cont_piperead::parse_chkpt(class Linux_dinst_t *di,
			   LSE_chkpt::data_t *tdp, cont_piperead *&nv) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 4))) return cerr;
  
  next(dp); // skip the marker for kind of continuation

  cont_piperead *n = new cont_piperead();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  insert_obj_noref(di, next(dp)->content.uint64Val, &n->pp);
  if (n->pp)
    n->pp->waiters[next(dp)->content.uint64Val] = n;
  else next(dp);
  nv = n;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
cont_ssuspend::parse_chkpt(class Linux_dinst_t *di,
		       LSE_chkpt::data_t *tdp, cont_ssuspend *&nv) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 2))) return cerr;
  
  next(dp); // skip the marker for kind of continuation

  cont_ssuspend *n = new cont_ssuspend();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  nv = n;
  return LSE_chkpt::error_None;
}

template<class T>
LSE_chkpt::error_t 
cont_stimedwait<T>::parse_chkpt(class Linux_dinst_t *di,
				LSE_chkpt::data_t *tdp, 
				cont_stimedwait<T> *&nv) {
  
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 6))) return cerr;
  
  next(dp); // skip the marker for kind of continuation
  next(dp); // skip the marker for type
  cont_stimedwait<T> *n = new cont_stimedwait<T>();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  n->infop    = next(dp)->content.uint64Val;
  n->savemask = next(dp)->content.uint64Val;
  n->thismask = next(dp)->content.uint64Val;
  insert_obj(di, next(dp)->content.uint64Val, &n->timer, false);
  nv = n;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
cont_vfork::parse_chkpt(class Linux_dinst_t *di,
		       LSE_chkpt::data_t *tdp, cont_vfork *&nv) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 2))) return cerr;
  
  next(dp); // skip the marker for kind of continuation

  cont_vfork *n = new cont_vfork();
  record_fixup(di, next(dp)->content.uint64Val, &n->osct);
  nv = n;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t Linux_timer_t::parse_chkpt_piece(class Linux_dinst_t *di,
						    LSE_chkpt::data_t *&dp) {
  clockid = next(dp)->content.int64Val;
  tval.interval = next(dp)->content.uint64Val;
  tval.expiration = next(dp)->content.uint64Val + di->clockdiff;
  bool realtodo = next(dp)->content.uint64Val;
  remove_todo(); // we cannot just delete it as the event queue owns the pointer
  // New todo will be built by event queue
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t Linux_sig_timer_t::parse_chkpt(class Linux_dinst_t *di,
						  LSE_chkpt::data_t *tdp) {

  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 8))) return cerr;
  
  uint64_t oid = next(dp)->content.uint64Val;
  Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

  Linux_sig_timer_t *st;
  if (o == di->objects.end() || o->second->kind != obj_sigtimer) {
    st = new Linux_sig_timer_t(oid, di);
  } else st = static_cast<Linux_sig_timer_t *>(o->second);

  st->parse_chkpt_piece(di, dp);
  
  st->signo = next(dp)->content.int64Val;
  record_fixup(di, next(dp)->content.uint64Val, &st->target);
  record_fixup(di, next(dp)->content.uint64Val, &st->tgroup);
  
  delete tdp;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t Linux_wu_timer_t::parse_chkpt(class Linux_dinst_t *di,
						 LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 6))) return cerr;
  
  uint64_t oid = next(dp)->content.uint64Val;
  Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

  Linux_wu_timer_t *st;
  if (o == di->objects.end() || o->second->kind != obj_wutimer) {
    st = new Linux_wu_timer_t(oid, di);
  } else st = static_cast<Linux_wu_timer_t *>(o->second);

  st->parse_chkpt_piece(di, dp);
  
  oid = next(dp)->content.uint64Val;
  record_fixup(di, oid, &st->target);
  
  delete tdp;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
Linux_reschedule_timer_t::parse_chkpt(class Linux_dinst_t *di,
				      LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 5))) return cerr;
  
  uint64_t oid = next(dp)->content.uint64Val;
  Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

  Linux_reschedule_timer_t *st;
  if (o == di->objects.end() || o->second->kind != obj_reschedtimer) {
    st = new Linux_reschedule_timer_t(oid, di);
  } else st = static_cast<Linux_reschedule_timer_t *>(o->second);

  st->parse_chkpt_piece(di, dp);
  
  delete tdp;
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
Linux_sighandtable_t::parse_chkpt(class Linux_dinst_t *di, 
				  LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if ((cerr = validate_children(tdp, 4*64 + 1))) return cerr;
  
  uint64_t oid = dp->content.uint64Val;   dp = dp->sibling;
  Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

  Linux_sighandtable_t *st;
  if (o == di->objects.end() || o->second->kind != obj_sighandtable) {
    st = new Linux_sighandtable_t(oid, di);
  } else st = static_cast<Linux_sighandtable_t *>(o->second);

  for (Linux_sighandinf_t *i = st->handlers, 
	 *ie = &st->handlers[0]+64; i != ie ; ++i) {
    i->handler = dp->content.uint64Val; dp = dp->sibling;
    i->restorer = next(dp)->content.uint64Val;
    i->sigmask = dp->content.uint64Val; dp = dp->sibling;
    i->flags = dp->content.uint64Val; dp = dp->sibling;
#ifdef DEBUG_CPOINT
    std::cerr << "SIGHAND: " << std::hex << i->handler << " "
	      << i->sigmask << " " << i->flags << std::dec << "\n";
#endif
  }
  
  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_memory_t::parse_chkpt(class Linux_dinst_t *di, 
			    LSE_chkpt::file_t *cptFile,
			    LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *cdp, *dp = tdp->oldestChild;
  uint64_t noid;

  if ((cerr = validate_children(tdp, 6))) return cerr;
  
  uint64_t oid = next(dp)->content.uint64Val;
  Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

  Linux_memory_t *st;
  if (o == di->objects.end() || o->second->kind != obj_memory) {
    st = new Linux_memory_t(oid, di);
  } else st = static_cast<Linux_memory_t *>(o->second);

  st->heapstart = next(dp)->content.uint64Val;
  st->heapbreak = next(dp)->content.uint64Val;

  cdp = next(dp)->oldestChild;
  st->codelocs.clear();
  while (cdp) {
    if (validate_siblings(cdp,1,true)) goto badformat;
    std::pair<LSE_emu_addr_t, LSE_emu_addr_t> 
      x(cdp->content.uint64Val, cdp->sibling->content.uint64Val);
    st->codelocs.push_back(x);
    cdp = cdp->sibling->sibling;
  }

  insert_obj_noref(di, next(dp)->content.uint64Val, &st->pmapper.PM);

  cdp = next(dp)->oldestChild;
  {
    typeof(st->vranges) svranges(st->vranges);
    st->vranges.clear();
    while (cdp) {
      if (validate_siblings(cdp,5,true)) goto badformat;
      range_t t = { cdp->content.uint64Val, // start
		    cdp->sibling->content.uint64Val, // end
		    cdp->sibling->sibling->content.int64Val, //flags
		    cdp->sibling->sibling->sibling->content.int64Val, // shared
		    0, // fp
		    cdp->sibling->sibling->sibling->sibling // offset
		    ->content.uint64Val,
      };
      uint64_t noid = cdp->sibling->sibling->sibling->sibling->sibling
	->content.uint64Val;
      insert_obj(di, noid, &t.fp, false);
#ifdef DEBUG_CPOINT
      std::cerr << "MEM:vrange " << std::hex << t.start << " " 
		<< t.end << " " << t.flags << " " << t.isShared << " " 
		<< t.offset << std::dec << "\n";
#endif
      st->vranges[t.end] = t;
      cdp = cdp->sibling->sibling->sibling->sibling->sibling->sibling;
    }
    for (std::map<LSE_emu_addr_t, range_t>::iterator 
	   i = svranges.begin(), ie = svranges.end() ; i != ie ; ++i)
      if (i->second.fp) i->second.fp->close();
  }

  st->vmdevice.readChkptGuts(cptFile, false);

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_error_file_t::parse_chkpt(class Linux_dinst_t *di, 
				LSE_chkpt::data_t *tdp) {
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 3)) goto badformat;
  else { 
    uint64_t oid = dp->content.uint64Val;   dp = dp->sibling;
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);
    
    Linux_error_file_t *st;
    if (o == di->objects.end() || o->second->kind != obj_errorfile) {
      st = new Linux_error_file_t(oid, di);
    } else st = static_cast<Linux_error_file_t *>(o->second);

#ifdef DEBUG_CPOINT
    std::cerr << "ERRORFILE:" << st->status << " " << st->OSblocking << "\n";
#endif
    st->status = dp->content.int64Val;
    st->OSblocking = dp->sibling->content.int64Val;
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_native_file_t::parse_chkpt(class Linux_dinst_t *di, 
				LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 6)) goto badformat;
  else {
    uint64_t oid = dp->content.uint64Val;   dp = dp->sibling;
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_native_file_t *st;
    if (o == di->objects.end() || o->second->kind != obj_nativefile) {
      st = new Linux_native_file_t(oid, di, 
				   dp->sibling->sibling->content.int64Val);
    } else st = static_cast<Linux_native_file_t *>(o->second);
    
    st->status = dp->content.int64Val;
    st->OSblocking = dp->sibling->content.int64Val;
    st->nativefd = dp->sibling->sibling->content.int64Val;
    st->statbuf.st_dev = dp->sibling->sibling->sibling->content.int64Val;
    st->statbuf.st_ino 
      = dp->sibling->sibling->sibling->sibling->content.int64Val;
#ifdef DEBUG_CPOINT
    std::cerr << "NATIVEFILE:" << st->status << " " << st->OSblocking << " " 
	      << st->nativefd << " " << st->statbuf.st_dev << " "
	      << st->statbuf.st_ino << "\n";
#endif
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_anon_file_t::parse_chkpt(class Linux_dinst_t *di, 
				LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 4)) goto badformat;
  else {
    uint64_t oid = dp->content.uint64Val;   dp = dp->sibling;
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_anon_file_t *st;
    if (o == di->objects.end() || o->second->kind != obj_anonfile) {
      st = new Linux_anon_file_t(oid, di, 
				 dp->sibling->sibling->content.int64Val);
    } else st = static_cast<Linux_anon_file_t *>(o->second);
    
    st->status = dp->content.int64Val;
    st->OSblocking = dp->sibling->content.int64Val;
    st->ino = dp->sibling->sibling->content.int64Val;
#ifdef DEBUG_CPOINT
    std::cerr << "ANONFILE:" << st->status << " " << st->OSblocking << " " 
	      << st->ino << "\n";
#endif
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_pipe_file_t::parse_chkpt(class Linux_dinst_t *di, 
				LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 5)) goto badformat;
  else {
    uint64_t oid = dp->content.uint64Val;   dp = dp->sibling;
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_pipe_file_t *st;
    if (o == di->objects.end() || o->second->kind != obj_pipefile) {
      st = new Linux_pipe_file_t(oid, di); 
    } else st = static_cast<Linux_pipe_file_t *>(o->second);
    
    st->status = next(dp)->content.int64Val;
    st->OSblocking = next(dp)->content.int64Val;
    insert_obj(di, next(dp)->content.uint64Val, &st->mypipe, true);
    st->isWriter = next(dp)->content.int64Val;

#ifdef DEBUG_CPOINT
    std::cerr << "PIPEFILE:" << st->status << " " << st->OSblocking << " " 
	      << st->mypipe << " " << st->isWriter << "\n";
#endif
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_socket_file_t::parse_chkpt(class Linux_dinst_t *di, 
				LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 4)) goto badformat;
  else {
    uint64_t oid = dp->content.uint64Val;   dp = dp->sibling;
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_socket_file_t *st;
    if (o == di->objects.end() || o->second->kind != obj_socketfile) {
      st = new Linux_socket_file_t(oid, di, 
				   dp->sibling->sibling->content.int64Val);
    } else st = static_cast<Linux_socket_file_t *>(o->second);
    
    st->status = dp->content.int64Val;
    st->OSblocking = dp->sibling->content.int64Val;
    st->nativefd = dp->sibling->sibling->content.int64Val;
#ifdef DEBUG_CPOINT
    std::cerr << "SOCKETFILE:" << st->status << " " << st->OSblocking << " " 
	      << st->nativefd << "\n";
#endif
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_pipe_t::parse_chkpt(class Linux_dinst_t *di, 
				LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 5)) goto badformat;
  else {
    uint64_t oid = next(dp)->content.uint64Val;   
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_pipe_t *st;
    if (o == di->objects.end() || o->second->kind != obj_socketfile) {
      st = new Linux_pipe_t(oid, di);
    } else st = static_cast<Linux_pipe_t *>(o->second);
    
    st->numwriters = next(dp)->content.int64Val;
    st->numreaders = next(dp)->content.int64Val;
    st->buf.assign(dp->content.stringVal, dp->size); next(dp);

    uint64_t sz = next(dp)->content.uint64Val; // new ref counts
    st->refcount += sz;
    st->waiters.resize(sz);
#ifdef DEBUG_CPOINT
    std::cerr << "PIPE:" << st->numreaders << " " << st->numwriters << " " 
	      << st->buf.size() << "\n";
#endif
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_fdtable_t::parse_chkpt(class Linux_dinst_t *di, LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 2)) goto badformat;
  else {
    uint64_t oid = next(dp)->content.uint64Val;   
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_fdtable_t *st;
    if (o == di->objects.end() || o->second->kind != obj_fdtable) {
      st = new Linux_fdtable_t(oid, di);
    } else st = static_cast<Linux_fdtable_t *>(o->second);
    
#ifdef DEBUG_CPOINT
    std::cerr << "FDTABLE:\n";
#endif

    fds_t keep_em(st->fds);
    st->fds.clear();
    LSE_chkpt::data_t *cdp = dp->oldestChild;
    while (cdp) {
      if (validate_siblings(cdp, 2, true)) goto badformat;
      int       fd = next(cdp)->content.int64Val;
      uint64_t oid = next(cdp)->content.uint64Val;
      int    flags = next(cdp)->content.int64Val;

      Linux_fd_t nfd(0, flags);
      insert_obj(di, oid, &nfd.fp, false);
      st->fds[fd] = nfd;
#ifdef DEBUG_CPOINT
      std::cerr << "\t" << fd << " " << oid << " " << flags << "\n";
#endif
    }
    for (fds_t::iterator i = keep_em.begin(), ie = keep_em.end() ; 
	 i != ie ; ++i) 
      i->second.fp->decr();
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_pgroup_t::parse_chkpt(class Linux_dinst_t *di, LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 3)) goto badformat;
  else {
    uint64_t oid = next(dp)->content.uint64Val;   
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_pgroup_t *st;
    if (o == di->objects.end() || o->second->kind != obj_pgroup) {
      st = new Linux_pgroup_t(oid, di);
    } else st = static_cast<Linux_pgroup_t *>(o->second);
    
    st->pgid = next(dp)->content.int64Val;
    di->pgroups[st->pgid] = st;
    
#ifdef DEBUG_CPOINT
    std::cerr << "PGROUP:" << st->pgid << "\n";
#endif
    if ((cerr = parse_tgrouplist(di, dp, st->members))) goto badformat;
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_tgroup_t::parse_chkpt(class Linux_dinst_t *di, LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 18 + 2 * OSDefs::oRLIMIT_NLIMITS)) goto badformat;
  else {
    uint64_t oid = next(dp)->content.uint64Val;   
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_tgroup_t *st;
    if (o == di->objects.end() || o->second->kind != obj_tgroup) {
      st = new Linux_tgroup_t(oid, di);
    } else st = static_cast<Linux_tgroup_t *>(o->second);
    
    st->tgid = next(dp)->content.int64Val;
    di->tgroups[st->tgid] = st;

#ifdef DEBUG_CPOINT
    std::cerr << "TGROUP:" << st->tgid << "\n";
#endif
    insert_obj(di, next(dp)->content.uint64Val, &st->pgroup, true);
    if ((cerr = parse_contextlist(di, next(dp), st->members))) goto badformat;
    if ((cerr = parse_signallist(di, next(dp), st->signals))) goto badformat;
    st->sigpending = next(dp)->content.uint64Val;
    st->exiting = next(dp)->content.int64Val;
    st->exitcodeValid = next(dp)->content.int64Val;
    record_fixup(di, next(dp)->content.uint64Val, &st->exittask);

    if ((cerr = parse_contextlist(di, next(dp), st->childwait.waiters))) 
      goto badformat;
    st->exitcode = next(dp)->content.int64Val;
    st->nCount = next(dp)->content.int64Val;
    st->nLive = next(dp)->content.int64Val;
    for (int i = 0; i < 3; ++i)
      insert_obj(di, next(dp)->content.uint64Val, &st->timers[i], true);
    st->cputime_curr_interval = next(dp)->content.uint64Val;
    st->cputime_cumulative = next(dp)->content.uint64Val;
    for (int i = 0; i < OSDefs::oRLIMIT_NLIMITS; ++i) 
      for (int j = 0; j < 2; ++j)
	st->rlimits[i][j] = next(dp)->content.uint64Val;
#ifdef DEBUG_CPOINT
    std::cerr << "\t" << st->sigpending << " " << st->exiting << " " 
	      << st->exitcodeValid << " " << st->exitcode << " " 
	      << st->nCount << " " << st->nLive << "\n";
#endif
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_fs_t::parse_chkpt(class Linux_dinst_t *di, LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 3)) goto badformat;
  else {
    uint64_t oid = next(dp)->content.uint64Val;   
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_fs_t *st;
    if (o == di->objects.end() || o->second->kind != obj_futex) {
      st = new Linux_fs_t(oid, di);
    } else st = static_cast<Linux_fs_t *>(o->second);
    
    if (st->cwd) free(st->cwd); st->cwd = 0;
    if (st->fsroot) free(st->fsroot); st->fsroot = 0;
    st->cwd = strdup(dp->content.stringVal); next(dp);
    st->fsroot = strdup(dp->content.stringVal); next(dp);
    
#ifdef DEBUG_CPOINT
    std::cerr << "FS:" << "\n";
#endif
  }
  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_futex_t::parse_chkpt(class Linux_dinst_t *di, LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  if (validate_children(tdp, 3)) goto badformat;
  else {
    uint64_t oid = next(dp)->content.uint64Val;   
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_futex_t *st;
    if (o == di->objects.end() || o->second->kind != obj_futex) {
      st = new Linux_futex_t(oid, di);
      st->incr();
    } else st = static_cast<Linux_futex_t *>(o->second);
    
    st->bitset = next(dp)->content.int64Val;
    uint64_t sz = next(dp)->content.uint64Val;

    st->refcount += sz;
    // NOW: if new, has 1 + N references.  If old, has 1 + N + oldN references.
    // the deletion of continuations should remove the oldN references
    st->waiters.resize(sz, 0);

#ifdef DEBUG_CPOINT
    std::cerr << "FUTEX:" << st->bitset << "\n";
#endif
  }

  delete tdp;
  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

LSE_chkpt::error_t 
Linux_context_t::parse_chkpt(class Linux_dinst_t *di, 
			     Linux_context_t *&foundcont,
			     LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp = tdp->oldestChild;

#ifdef LSE_LINUX_COMPAT
  int nchild = 36;
#else
  int nchild = 35;
#endif

  if (validate_children(tdp, nchild,true)) 
    goto badformat;
  else {
    uint64_t oid = next(dp)->content.uint64Val;   
    Linux_dinst_t::objmap_t::iterator o = di->objects.find(oid);

    Linux_context_t *st;
    if (o == di->objects.end() || o->second->kind != obj_context) {
      st = new Linux_context_t(oid, di);
    } else st = static_cast<Linux_context_t *>(o->second);
    
    insert_obj(di, next(dp)->content.uint64Val, &st->mem, true);
    insert_obj(di, next(dp)->content.uint64Val, &st->fdtable, true);
    insert_obj(di, next(dp)->content.uint64Val, &st->sighandtable, true);
    insert_obj(di, next(dp)->content.uint64Val, &st->tgroup, true);
    if ((cerr = parse_contextlist(di, next(dp), st->reportVforkDone.waiters))) 
      goto badformat;
    insert_obj(di, next(dp)->content.uint64Val, &st->fs, true);
    st->exit_code = next(dp)->content.int64Val;
    st->signalOnExit = next(dp)->content.int64Val;
    st->state = (Linux_context_state_t)(next(dp)->content.int64Val);
    st->clear_child_tid = next(dp)->content.uint64Val;
    st->robustFutexList = next(dp)->content.uint64Val;
#ifdef LSE_LINUX_COMPAT
    st->robustFutexListCompat = next(dp)->content.uint64Val;
#endif
    st->tid = next(dp)->content.int64Val;
    st->gid = next(dp)->content.int64Val;
    st->egid = next(dp)->content.int64Val;
    st->uid = next(dp)->content.int64Val;
    st->euid = next(dp)->content.int64Val;
    st->is64bit = next(dp)->content.int64Val;
    if ((cerr = parse_contextlist(di, next(dp), st->children)))  goto badformat;
    record_fixup(di, next(dp)->content.uint64Val, &st->parent);
    st->sigblocked = next(dp)->content.uint64Val;
    st->oldsigblocked = next(dp)->content.uint64Val;
    if (parse_signallist(di, next(dp), st->signals)) goto badformat;
    st->sigpending = next(dp)->content.uint64Val;
    st->oldSigBlockedValid = next(dp)->content.int64Val;
    st->sigPendingFlag = next(dp)->content.int64Val;
    st->schedPolicy = next(dp)->content.int64Val;
    memcpy(&st->schedParms, dp->content.stringVal, dp->size);  next(dp);
    st->sigAlt.LSE_ss_sp = next(dp)->content.uint64Val;
    st->sigAlt.LSE_ss_flags = next(dp)->content.int64Val;
    st->sigAlt.LSE_ss_size = next(dp)->content.uint64Val;
    if ((cerr = parse_continuationvec(di, next(dp), st->continuationStack))) 
      goto badformat;
    if ((cerr = parse_continuation(di, next(dp), st->restart))) goto badformat;
    st->cputime_curr_interval = next(dp)->content.uint64Val;
    st->cputime_cumulative = next(dp)->content.uint64Val;
    foundcont = st;
  }

  delete tdp;

  return LSE_chkpt::error_None;
 badformat:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

static int find_valid_cno(LSE_emu_interface_t *ifc, int cno) {
  for (; cno <= LSE_emu_hwcontexts_total ; cno++)
    if (LSE_emu_hwcontexts_table[cno].valid &&
	LSE_emu_hwcontexts_table[cno].emuinstid == ifc->emuinstid)
      return cno;
  return 0;
}

LSE_chkpt::error_t 
Linux_chkpt_read_segment(LSE_emu_interface_t *intr,
			  LSE_chkpt::file_t *cptFile, const char *segmentName, 
			  int step, LSE_emu_chkpt_cntl_t *ctl) {
  Linux_dinst_t *di = OS_interface(intr);

  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp,*tdp, *cdp;
  int tid;
  uint64_t oid;

  //std::cerr << "Starting read " << step << "\n";

  if (step > 1 || step < 0) return LSE_chkpt::error_BadArgument;
  di->ctl = *ctl;
  if (step == 0) di->fixups.clear();

  // To make it easier to deal with large segments, I stream things in.
  if ((cerr = cptFile->read_taglen_from_segment(0,&tdp))) return cerr;
  if (!tdp) return LSE_chkpt::error_FileFormat;
  delete tdp;

  di->OSstate = 4;

  if (step == 0) {
    // read time

    if ((cerr = cptFile->read_from_segment(0, &tdp))) return cerr;
    di->clockdiff = CC_time_get_currtime(di) - tdp->content.uint64Val;
    delete tdp;
  }

  do {
    if ((cerr = cptFile->read_taglen_from_segment(0,&tdp))) return cerr;
    if (!tdp) break; /* we are done */

    switch (tdp->actualTag) {

    case tag_OSINST:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;

      if ((cerr = validate_children(tdp, 15))) goto deltdp;
      else {
	dp = tdp->oldestChild;
	cdp = next(dp)->oldestChild;

	// NOTE: cont_futex deletions should have cleaned up all left-over
	// futexes
#ifdef NOMORE
	Linux_dinst_t::fm_t keep_em(di->futexes); 
#endif
	di->futexes.clear();
	while (cdp) {
	  LSE_emu_addr_t a = next(cdp)->content.uint64Val;
	  if (!cdp) goto deltdp;
	  di->futexes[a] = static_cast<Linux_futex_t *>(0);
	  insert_obj_noref(di, next(cdp)->content.uint64Val, &di->futexes[a]);
	}
#ifdef NOMORE
	for (Linux_dinst_t::fm_t::iterator i = keep_em.begin(), 
	       ie = keep_em.end(); 
	     i != ie ; ++i)
	  i->second->decr();
#endif

	std::map<int, Linux_context_t *> kt(di->threads);
	di->threads.clear();
	cdp = next(dp)->oldestChild;
	while (cdp) {
	  Linux_context_t *p=0;
	  insert_obj(di, next(cdp)->content.uint64Val, &p, false);
	  di->threads[p->tid] = p;
	  p->mem->vmdevice.subspace = OS_ict_emuvar(p->isacont, mem);
	}
	for (std::map<int, Linux_context_t *>::iterator 
	       i = kt.begin(), ie = kt.end(); i != ie ; ++i) {
	  // if about to delete, do not remove from parent's list because
	  // the parent's child list will already be rebuilt and the parent
	  // may actually be already deleted.
	  if (i->second->refcount == 1) i->second->parent = 0; 
	  i->second->decr();
	}

	std::map<int, Linux_tgroup_t *> tg(di->tgroups);
	di->tgroups.clear();
	cdp = next(dp)->oldestChild;
	while (cdp) {
	  Linux_tgroup_t *p=0;
	  insert_obj_noref(di, next(cdp)->content.uint64Val, &p);
	  di->tgroups[p->tgid] = p;
	}

	std::map<int, Linux_pgroup_t *> pg(di->pgroups);
	di->pgroups.clear();
	cdp = next(dp)->oldestChild;
	while (cdp) {
	  Linux_pgroup_t *p=0;
	  insert_obj_noref(di, next(cdp)->content.uint64Val, &p);
	  di->pgroups[p->pgid] = p;
	}

	{
	  // scheduling is an interesting problem.  Want to have the same
	  // things running if possible, but since the number of hardware
	  // contexts may vary, we may end up with greater or fewer actually
	  // running and we have to get the times right.

	  di->running_list.clear();
	  di->ready_list.clear();

	  int cno = find_valid_cno(intr, 1);
	  int actuallyrunning = 0;

	  cdp = next(dp)->oldestChild;
	  while (cdp) {
	    Linux_context_t *p=0;
	    insert_obj_noref(di, next(cdp)->content.uint64Val, &p);

	    if (cno) {
	      if (p->state == ProcRunning) {
		p->cputime_curr_interval += di->clockdiff;
		di->running_list.push_back(p);
		CC_ict_hwcno(p->isacont) = cno;
		p->incr();
		CC_context_map_update(p->isacont, cno, p->isacont);
		p->decr();
		cno = find_valid_cno(intr, cno+1);
	      } else di->ready_list.push_back(p);
	    } else { // must end up blocked
	      di->ready_list.push_back(p);
	      if (p->state == ProcRunning) {
		p->cputime_cumulative += (CC_time_get_currtime(di)
					  - di->clockdiff 
					  - p->cputime_curr_interval);
		p->state = ProcRunnable;
	      }
	    }
	  }

	  while (cno) {
	    CC_context_map_update(0, cno, 0); // scary
	    cno = find_valid_cno(intr, cno+1);
	  }

	  // TODO: if there are any contexts left, deal with them
	}

	di->last_pid = next(dp)->content.int64Val;
	di->objC = next(dp)->content.uint64Val;
	di->anonCounter = next(dp)->content.int64Val;
	di->boottime = next(dp)->content.uint64Val + di->clockdiff;
	insert_obj(di, next(dp)->content.uint64Val, &di->reschedule_timer, 
		   true);
	insert_obj(di, next(dp)->content.uint64Val, &di->errorFile, true);

	// event queue: first wipe out all old events.
	while (!di->eventQueue.empty()) {
	  delete di->eventQueue.top();
	  di->eventQueue.pop();
	}
	// now get new ones
	cdp = next(dp)->oldestChild;
	while (cdp) {
	  int kind = next(cdp)->content.uint64Val;
	  uint64_t itime = next(cdp)->content.uint64Val + di->clockdiff;
	  switch ((Linux_todo_t::todo_kinds)kind) {
	  case Linux_todo_t::todo_kind_timer : 
	    {
	      Linux_timer_t *t = 0;
	      insert_obj(di, next(cdp)->content.uint64Val, &t, true);
	      if (t) {
		t->decr(); // did not need a reference count
		t->planfuture(itime);
	      }
	      break;
	    }
	  default: break;
	  } // case
	} // while cdp

	di->freePpages.clear();
	cdp = next(dp)->oldestChild;
	while (cdp) {
	  Linux_dinst_t::prange_t np;
	  np.pno = next(cdp)->content.uint64Val;
	  np.num = next(cdp)->content.uint64Val;
	  di->freePpages.push_back(np);
	}

	di->usedPpages.clear();
	cdp = next(dp)->oldestChild;
	while (cdp) {
	  LSE_emu_addr_t na;
	  Linux_dinst_t::pinfo_t np;
	  na = next(cdp)->content.uint64Val;
	  np.refcnt = next(cdp)->content.int64Val;
	  np.offset = next(cdp)->content.int64Val;
	  di->usedPpages[na] = np;
	}

	di->inode2pages.clear();
	cdp = next(dp)->oldestChild;
	while (cdp) {
	  inode_id_t ni;
	  ni.dev = next(cdp)->content.int64Val;
	  ni.ino = next(cdp)->content.int64Val;
	  LSE_chkpt::data_t *sdp = next(cdp)->oldestChild;
	  while (sdp) {
	    LSE_emu_addr_t a1, a2;
	    a1 = next(sdp)->content.uint64Val;
	    a2 = next(sdp)->content.uint64Val;
	    di->inode2pages[ni].offset2pno[a1] = a2;
	  }
	}

      }

      // clean up any pointers that needed to be updated

      for (std::vector<Linux_dinst_t::fixup_t>::iterator i= di->fixups.begin(), 
	     ie = di->fixups.end() ; i != ie ; ++i) {
	*(i->second) = di->objects[i->first];
      }
      di->fixups.clear();

      if (dp) { cerr = LSE_chkpt::error_FileFormat; goto deltdp; }
      delete tdp;
      break;

    case tag_SIGHANDTABLE:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_sighandtable_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_OSMEMORY:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_memory_t::parse_chkpt(di, cptFile, tdp))) return cerr;
      break;
    case tag_ERRORFILE:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_error_file_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_NATIVEFILE:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_native_file_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_ANONFILE:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_anon_file_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_PIPEFILE:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_pipe_file_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_SOCKETFILE:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_socket_file_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_PIPE:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_pipe_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_FDTABLE:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_fdtable_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_PGROUP:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_pgroup_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_TGROUP:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_tgroup_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_FUTEX:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_futex_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_FILESYSTEM:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_fs_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_SIGTIMER:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_sig_timer_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_WUTIMER:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_wu_timer_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_RESCHEDULETIMER:
      if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
      if ((cerr = Linux_reschedule_timer_t::parse_chkpt(di, tdp))) return cerr;
      break;
    case tag_OSCONTEXT: 
      {
	Linux_context_t *osct;
	if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;
	if ((cerr = Linux_context_t::parse_chkpt(di, osct, tdp))) return cerr;
	CC_ict_parse_chkpt(osct->isacont);
	CC_ict_osinfo(osct->isacont) = osct;
      }
      break;

    case tag_OSTRACE:  // OS trace information from a first thread
      if ((cerr = cptFile->read_from_segment(0,&dp))) goto deltdp;// ignore OID
      delete dp;
      if ((cerr = cptFile->read_from_segment(0,&dp))) goto deltdp;
      tid = dp->content.uint64Val;
      delete dp;
      {
	Linux_context_t *osct = 0;
	if (di->threads.find(tid) != di->threads.end()) osct = di->threads[tid];

	LSE_chkpt::data_t *&OSRecords = di->osCalls[tid];
	if (OSRecords) { delete OSRecords; OSRecords=0; }
        OSRecords = tdp;
        do {
	  if ((cerr = cptFile->read_from_segment(0,&dp))) goto deltdp;
	  if (!dp) break;
          tdp->adopt_child(dp);
        } while(1);
	if (osct) osct->currOSRecord = OSRecords->oldestChild;
      }
      break;

    default:
      delete tdp;
      return cerr;
    }

  } while (1);

  //if ((cerr = test_for_indefinite(cptFile))) return cerr;

  di->OSstate = ctl->recordOS ? 2 : 0;
  di->doingCPoint = false;
  Linux_schedule(intr);

  return LSE_chkpt::error_None;
deltdp:
  delete tdp;
  return cerr;
}


/*************** strange things *****************/

void psect_context::chkptBuild(LSE_chkpt::data_t *parent) {
  LSE_chkpt::data_t *tdp = LSE_chkpt::build_sequence(parent);
  LSE_chkpt::build_unsigned(tdp, bss);
  LSE_chkpt::build_unsigned(tdp, brk);
  LSE_chkpt::build_unsigned(tdp, start_code);
  LSE_chkpt::build_unsigned(tdp, end_code);
  LSE_chkpt::build_unsigned(tdp, start_data);
  LSE_chkpt::build_unsigned(tdp, end_data);
  LSE_chkpt::build_unsigned(tdp, loadbias);
  LSE_chkpt::build_unsigned(tdp, baseaddr);
  LSE_chkpt::build_boolean(tdp, use_interp);
  LSE_chkpt::build_boolean(tdp, loadbiasknown);
  tdp->update_size();
}

LSE_chkpt::error_t psect_context::chkptParse(LSE_chkpt::data_t *tdp) {
  LSE_chkpt::error_t cerr;
  if ((cerr = validate_children(tdp, 10, false))) {
    fprintf(stderr, "Unable to get OS call results\n");
    abort();
  }
  LSE_chkpt::data_t *dp = tdp->oldestChild;
  bss = next(dp)->content.uint64Val;
  brk = next(dp)->content.uint64Val;
  start_code = next(dp)->content.uint64Val;
  end_code = next(dp)->content.uint64Val;
  start_data = next(dp)->content.uint64Val;
  end_data = next(dp)->content.uint64Val;
  loadbias = next(dp)->content.uint64Val;
  baseaddr = next(dp)->content.uint64Val;
  use_interp = next(dp)->content.booleanVal;
  loadbiasknown = next(dp)->content.booleanVal;
}

static void parse_mmap_records(Linux_context_t *osct, LSE_chkpt::data_t *tdp) {
  LSE_chkpt::data_t *dp = tdp->oldestChild;

  while (dp) {
    if (validate_children(dp, 11, true)) {
      fprintf(stderr, "Unable to get OS call (execve mmap) results\n");
      abort();
    }
    LSE_chkpt::data_t *ndp = dp->oldestChild;

    LSE_emu_addr_t addrA = next(ndp)->content.uint64Val;
    LSE_emu_addr_t size = next(ndp)->content.uint64Val;
    LSE_emu_addr_t len = next(ndp)->content.uint64Val;
    int prot = next(ndp)->content.int64Val;
    int flags = next(ndp)->content.int64Val;
    LSE_emu_addr_t off = next(ndp)->content.uint64Val;
    bool mustunmap = next(ndp)->content.booleanVal;
    LSE_emu_addr_t ret = next(ndp)->content.uint64Val;
    bool dofile = next(ndp)->content.booleanVal;
    bool is64bit = osct->is64bit;
    dev_t dev = next(ndp)->content.int64Val;
    ino_t ino = next(ndp)->content.int64Val;

    Linux_native_file_t *lfp = dofile ? new Linux_native_file_t(osct->os,-1):0;
    if (dofile) {
      lfp->incr(); // has no refcount to start with!
      lfp->statbuf.st_dev = dev;
      lfp->statbuf.st_ino = ino;
    }

    LSE_chkpt::data_t *sdp = osct->currRes;
    osct->currRes = ndp;
    if (mustunmap) {
      do_mmap(osct, addrA, len, off, prot, flags, lfp);
      if (ret < OSDefs::oUTASK_SIZE(is64bit)) 
	do_munmap(osct, ret+size, len-size);
    } else {
      do_mmap(osct, addrA, size, off, prot, flags, lfp);
    }
    lfp->close();
    osct->currRes = sdp;
#ifdef NOTNEEDED
    if (ndp->sibling) {
      if (validate_children(dp, 10, false)) {
	fprintf(stderr, "Unable to get OS call (execve mmap) results\n");
	abort();
      }

      int sres = next(ndp)->content.int64Val;
      if (sres >= 0) {
	int mres = next(ndp)->content.int64Val;
	if (mres >= 0) {
	  LSE_emu_addr_t start = next(ndp)->content.uint64Val;
	  uint64_t mlen = ndp->size;
	  
	  std::cerr << "Adding map " << std::hex << start << "/" << mlen << std::dec << "\n";
	  CC_mem_write(CC_ict_mem(osct->isacont), start, mlen, 
		       ndp->content.stringVal);
	}
      }
    }
#endif
    dp = dp->sibling;
  }
}
