/*
 * Copyright (c) 2006-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LIS common types and functions
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This program declares types and functions common to all LIS processing.
 *
 */
#ifndef LIS_H
#define LIS_H

#include <LSE_inttypes.h>
#include <list>
#include <string>
#include <vector>

namespace LIS {

  // types and definitions for both parsing support and data storage

  struct myloc_t {
    const char *filename;
    int first_line, last_line;
    int first_column, last_column;
  };  // location in a file

  inline myloc_t loc_range(myloc_t &l1, myloc_t &l2) {
    myloc_t t = { l1.filename, l1.first_line, l2.last_line, 
		  l1.first_column, l2.last_column };
    return t;
  }

  inline std::ostream &operator<<(std::ostream &os, const myloc_t &loc) {
    if (loc.filename) os << loc.filename << ':';
    os << loc.first_line << '/' << loc.first_column << ':' 
       << loc.last_line << '/' << loc.last_column;
    return os;
  }

  typedef struct range {
    int from, length;
  } range_t;
  typedef class std::list<range_t> rangelist_t;

  // needed here because input file readers play with it
#define YYLTYPE LIS::myloc_t
#define YYLTYPE_IS_TRIVIAL 0

  // Types used when defining the ISA

  enum hide_status_t { nothidden, hidden, refhidden };

  // Other goodies

  /* put in copy_if because somehow it isn't in the standard library! */

  template<class In, class Out, class Pred>
    Out copy_if(In first, In last, Out res, Pred p)
    {
      while (first != last) {
	if (p(*first)) *res++ = *first;
	++first;
      }
      return res;
    }

  class LIS_isa;

} // namespace LIS


namespace LIS_user {
  extern void reportError(const char *str, const LIS::myloc_t &yylloc);
  extern void reportError(const std::string &str, const LIS::myloc_t &yylloc);
  extern void reportError(const std::string &str);
  extern void reportError(const char *str);
  extern int errorCount(void);
  extern class LIS::LIS_isa *theISA;
  extern bool yynextfile(void);
  extern void yynewfile(const char *fname);
  extern void yyendfile(void);
}
extern int yyparse(void);
extern FILE *yyin;
extern YYLTYPE yylloc;
extern "C" {
  extern int yydebug;
}

#endif /* LIS_H */

