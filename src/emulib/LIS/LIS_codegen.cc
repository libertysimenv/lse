/*
 * Copyright (c) 2006-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its doumentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LIS functional simulator generator
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This program generates a functional simulator from a LIS database.  The
 * simulator is generated as four files: a description file, a
 * public header file, a private header file, and a code file.  The 
 * description file can be processed by l-make-domain-header to generate an 
 * instance header file and the code file can then be compiled.  The generated 
 * files are named <name>.dsc, <name>P.h, <name>.h, and <name>.cc where the 
 * name comes from the command-line.
 *
 * Note, however, that the generated simulator may generate code which returns
 * code, so that it may be used to produce compiled-code simulators.  This
 * makes things a little bit more complex, but not overly so.
 *
 */
#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <ostream>
#include <set>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <cctype>
#include "LIS_isa.h"

using namespace LIS;
using namespace LIS_decoding_support;

namespace {

  const char *progname = "";

  const char *descFileSuffix = ".dsc";
  const char *publicHSuffix = ".h";
  const char *privateHSuffix = ".priv.h";
  const char *supportSuffix = ".support.cc";
  const char *mkSuffix = ".inc.mk";

  std::list<char *>fileList;
  std::list<char *>fnameList;
  
  std::string dominame("SIM_isa.h");
  std::string namespaceName("");

  typedef std::list<std::string> includePaths_t;
  includePaths_t includePaths;

  std::string normalizePath(const std::string &p) {
    // It turns out I don't really need to do this!
    return std::string(p);
  }

  typedef std::set<std::string> wordSet_t;
  typedef std::vector<std::string> wordList_t;
  typedef std::map<std::string, std::string> stringMap_t;
  typedef std::multimap<std::string, std::string> stringMMap_t;

} // namespace anonymous

void LIS_user::yynewfile(const char *fname) {
  std::string nfname;
  char *fname2;
  if (fname && fname[0]=='/') {
    std::string t = std::string(fname);
    nfname = normalizePath(t);
    fname2 = strdup(nfname.c_str());
    yyin = fopen(fname2,"r");
  } else {
    for (includePaths_t::reverse_iterator 
	   i = includePaths.rbegin(); i != includePaths.rend(); i++) {
      std::string t = std::string(*i) + "/" + fname;
      nfname = normalizePath(t);
      fname2 = strdup(nfname.c_str());
      yyin = fopen(fname2,"r");
      if (yyin) break;
    }
  }
  if (!yyin) {
    std::cerr << "error: unable to open file " << fname << " for reading\n";
    exit(2);
  }
  includePaths.push_back(nfname.substr(0,nfname.find_last_of('/')));
  yylloc.first_line = yylloc.last_line = 1;
  yylloc.first_column = yylloc.last_column = 1;
  yylloc.filename = fname2;
  fnameList.push_back(fname2);
}

bool LIS_user::yynextfile(void) {
  if (fileList.size()) {
    char *fname = fileList.front();
    if (!strcmp(fname,"-")) {
      yylloc.filename="<stdin>";
      yyin = stdin;
      // figure out how to duplicate std::cin
      yylloc.first_line = yylloc.last_line = 1;
      yylloc.first_column = yylloc.last_column = 1;
      includePaths.push_back(".");
    } else {
      yynewfile(fname);
    }
    fileList.pop_front();
    return true;
  }
  else return false;
}

void LIS_user::yyendfile(void) {
  if (yyin != stdin) fclose(yyin);
  includePaths.pop_back();
}

namespace {

  typedef std::list<codepoint_t *> entrypointList_t;
  typedef std::list<buildset_t *> buildsetList_t;
  struct styleExt_t {
    buildsetList_t buildsets;
    entrypointList_t entrypoints;
    bool needsAtable; // had AFTERPTR/AFTERPTRS
    bool needsTtable; // had some kind of text in a generator
    bool needsEntry; // had ENTRY in generator (for non-generator, assumed)
  };
  struct buildsetExt_t {
    hiddenFields_t specializedFields;
  };
  
  class LIS_sim : public LIS_isa {

    // generated information
    std::map<style_t *, styleExt_t> styleExts;
    std::map<buildset_t *, buildsetExt_t> buildsetExts;

  public:
    void dumpISA(std::ostream &os);
    void figureOut(void);
    void createDSC(std::string name);
    void createCode(std::string name);
  };

  std::ostream &operator<<(std::ostream &os, const action_t &a) {
    os << "\t\tid=" << a.id << std::endl
       << a.code << std::endl 
       << "\t\t############ from " << a.loc;
    return os;
  }

  std::ostream &operator<<(std::ostream &os, const hide_status_t &h) {
    switch (h) {
    case nothidden: os << "nothidden"; break;
    case hidden: os << "hidden"; break;
    case refhidden: os << "refhidden"; break;
    default: break;
    }
    return os;
  }

} // anonymous namespace

void LIS_sim::dumpISA(std::ostream &os)
{
  for (cvals_t::const_iterator i = options.begin(); 
       i != options.end(); i++) {
    os << "Option " << i->first << " = " << i->second.val << std::endl;
  }

  for (typeOrder_t::const_iterator
	 i = typeOrder.begin(); i != typeOrder.end(); i++) {
    typedef_t &t = typedefs[*i];
    switch (t.ttype) {
    case scalar: 
      os << "  typedef " << t.strdef << ";\n"; 
      break;
    case structdef: 
      {
	os << "  struct " << t.id << " {\n";
	for (typeOrder_t::const_iterator 
	       j = t.defnOrder.begin(); j != t.defnOrder.end(); j++)
	  os << "    " << t.defn[*j].defn << ";\n";
	os << "  };\n";
      } 
      break;
    case enumdef: 
      {
	os << "  typedef enum " << t.id << " {\n";
	for (typeOrder_t::const_iterator 
	       j = t.defnOrder.begin(); j != t.defnOrder.end(); j++)
	  os << "    " << t.defn[*j].defn << ",\n";
	os << "  } " << t.id << ";\n";
      }
      break;
    default: break;
    } // switch
  }  // for i in types


  os << "Operand value fields:\n";
  for (opvaluetype_t::const_iterator 
	 i = opvaluetype.begin(); i != opvaluetype.end(); i++) 
    os << "\t" << i->second << " " << i->first << ";\n";
  
  for (std::map<std::string, std::string>::const_iterator i = code.begin(); 
       i != code.end(); i++) {
    os << "Code " << i->first << " = " << i->second <<
      "*****" << std::endl;
  }

  for (std::map<std::string, operandname_t>::iterator i = opernames.begin(); 
       i != opernames.end(); i++) {
    os << "OperName " << i->first ;
    if (i->second.opertype == oper_src) os << " src";
    else if (i->second.opertype == oper_dest) os << " dest";
    else os << " int";
    os << " = " << i->second.offset
       << "\t from " << i->second.loc << std::endl;
  }

  for (accessors_t::iterator i = accessors.begin();
       i != accessors.end(); i++) {
    for (accessor_codes_t::iterator j = i->second.codes.begin();
	 j != i->second.codes.end(); j++) {
      os << "Accessor " << j->second.rettype << " " 
	 << i->first << "::" << j->first << " (" 
	 << j->second.params << ")\n"
	 << j->second.code << std::endl 
	 << "############ from " << j->second.loc << std::endl;
      for (implicitfields_t::iterator k = j->second.implicitfields.begin(),
	     ke = j->second.implicitfields.end(); k != ke ; ++k) {
	os << "\tField " << *k << "\n";
      }
    }
  }

  for (fields_t::iterator i = fields.begin(); 
       i != fields.end(); i++) {
    os << "Field " << i->first << ": Cname=" << i->second.cname 
       << " aText=" << i->second.accessorText 
       << (i->second.predecode ? " predecoded" : " !predecoded")
       << (i->second.toDef ? " toDef" : " !toDef")
       << " from " << i->second.loc << std::endl;
  }

  for (iclasses_t::const_iterator i = iclasses.begin(); 
       i != iclasses.end(); i++) {
    os << "IClass " << i->first << ":" << std::endl;

    os << "\tOpcode: " << i->second.opcode << std::endl;

    if (i->second.isInstruction) {
      os << "\tCanDecode: " << (i->second.canDecode ? "true" : "false") 
	 << "\tInstruction idno: " << i->second.idno  
	 << "\tFrequency: " << i->second.frequency << std::endl;
    }

    os << "\tclasses:";
    for (iclass_set_t::const_iterator j = i->second.classes.begin();
	 j != i->second.classes.end(); j++)
      os << " " << (*j)->id;
    os   << std::endl;

    os << "\tused by classes:";
    for (iclass_set_t::const_iterator j = i->second.usedby_class.begin();
	 j != i->second.usedby_class.end(); j++)
      os << " " << (*j)->id;
    os   << std::endl;

   
    os << "\tbitfields: ";
    for (bitfields_t::const_iterator j = i->second.bitfields.begin();
	 j != i->second.bitfields.end(); j++) {
      os << " " << j->second.id << ":" << j->second.field << "[" 
	 << (j->second.from + j->second.length - 1) 
	 << ":" << j->second.from << "]";
    }
    os << std::endl;

    os << "\tmatchinfo: " << i->second.matchinfo << std::endl;
    os << "\tmatchcommon: " << i->second.matchcommon << std::endl;

    os << "\toperands: " << std::endl;
    for (operands_t::const_iterator j = i->second.operands.begin();
	 j != i->second.operands.end(); j++) {
      os << "\t\t" << j->second.id << ":" << j->second.decodeNo << "/" 
	 << j->second.actionNo << " Acc=" << j->second.accessor 
	 << " ctype=" << (j->second.acc ? j->second.acc->ctype : "<nil>")
	 << " #=" << (j->second.opername ? j->second.opername->offset : -1)
	 << std::endl;
    }

    for (actions_t::const_iterator j = i->second.actiondefs.begin();
	 j != i->second.actiondefs.end(); j++) {
      os << "\taction def " << j->first << ":" << std::endl;
      if (j->second) os << *(j->second) << std::endl;
      else os << " (null)\n";
    }

    os << "\tsequence";
    for (sequence_t::const_iterator j = i->second.sequence.begin();
	 j != i->second.sequence.end(); j++) {
      os << " " << j->seqNo << " " << j->action->id;
    }
    os << std::endl << "\tfrom " << i->second.loc << std::endl;
  }

  for (buildsets_t::const_iterator i = buildsets.begin();
       i != buildsets.end(); i++) {
    os << "Buildset " << i->first << ": " 
       << " style " << i->second.style << std::endl;

    for (cvals_t::const_iterator j = i->second.options.begin(); 
	 j != i->second.options.end(); j++) {
      os << "\tOption " << j->first << " = " << j->second.val << std::endl;
    }

    os << "\tbaseclass: " << i->second.baseclass << std::endl;

    os << "\tcapabilities:";
    for (capabilities_t::const_iterator j = i->second.capabilities.begin();
	 j != i->second.capabilities.end(); j++)
      os << " " << *j;
    os << std::endl;

    os << "\thidden fields:";
    for (hiddenFields_t::const_iterator j = i->second.hiddenFields.begin();
	 j != i->second.hiddenFields.end(); j++)
      os << " " << j->first << '(' << j->second.hidestatus << ')';
    os << std::endl;

    for (entrypoints_t::const_iterator j = i->second.entrypoints.begin();
	 j != i->second.entrypoints.end(); j++) {
      j->second->dump(os);
    }

    for (decoders_t::const_iterator j = i->second.decoders.begin();
	 j != i->second.decoders.end(); j++) {

      j->second->dump(os);
      os << "\t\tDecode tree:\n";
      if (j->second->decodeTree) showDecode(os, 20, j->second->decodeTree);

    }

  } // for buildsets

  // report on styles 
  for (styleOrder_t::const_iterator i = styleOrder.begin();
       i != styleOrder.end(); i++) {
    os << "Style " << (*i)->id << "\n"
       << "\tEntry:" << (*i)->entry.code << "\n";

    for (cvals_t::const_iterator j = (*i)->options.begin(); 
	 j != (*i)->options.end(); j++) {
      os << "\tOption " << j->first << " = " << j->second.val << std::endl;
    }

    for (generators_t::const_iterator j = (*i)->generators.begin(); 
	 j != (*i)->generators.end(); j++) {
      os << "\tGenerator " << j->first << " " << j->second.section <<
	":" << j->second.code << std::endl;
    }

    os << "\tspecialized fields:";
    for (hiddenFields_t::const_iterator 
	   j = (*i)->specializedFields.begin();
	 j != (*i)->specializedFields.end(); j++)
      os << " " << j->first << '(' << j->second.hidestatus << ')';
    os << std::endl;

    os << "\tspecialized bitfields:";
    for (specializedBitfields_t::const_iterator 
	   j = (*i)->specializedBitfields.begin();
	 j != (*i)->specializedBitfields.end(); j++)
      os << " " << *j;
    os << std::endl;

    os << "\tExcluded actions:";
    for (rangelist_t::const_iterator j = (*i)->excl.begin(), je = (*i)->excl.end();
	 j != je; ++j) 
      os << " (" << j->from << "," << j->length << ")";
    os << "\n";

  } // for styles

  if (steps.size()) {
    os << "Steps:\n";
    for (steps_t::const_iterator i = steps.begin(); i != steps.end(); i++) {
      os << "\tstep " << i->second.id << " " << i->second.stepNo << " " 
	 << (int)i->second.steptype << std::endl;
    }
  }

  os << "Implementation order:";
  for (buildsetOrder_t::const_iterator
	 i = buildsetOrder.begin(); i != buildsetOrder.end(); i++) {
    os << " " << buildsets[*i].id;
  }
  os << std::endl;

}

void LIS_sim::createDSC(std::string name)
{
  std::fstream f;

  f.open((name + descFileSuffix).c_str(), std::ios::out);

  f << "################################################################" 
    << std::endl 
    << "# Emulator description file generated by " << progname
    << std::endl
    << "################################################################\n\n"
  
    << "name = " << '"' << name << '"' << std::endl << std::endl;

  if (isBuildable) {
    // Code to make the thing buildable...
    f << "mat = re.search('\\s--(?:\\s|$)(.*)',LSE_emu_currinst.buildArgs,\n"
      "        re.MULTILINE|re.DOTALL)\n"
      "if not useLIS and mat is not None:\n"
      "  useLIS = 1\n"
      "  LIStext = mat.group(1)\n"
      "  LIStext.strip()\n"
      "  del mat\n"
      "  sys.exit(1)\n\n";
  }

  // a couple of attributes

  f  << "compiled = 0" << std::endl

    // add in the header file
    << "\nheaders += ['" << name << ".h']\n" 

    // and the namespace
    << "\nnamespaces += ['" << namespaceName << "']\n";

  // declare added fields
  f << "\naddedfields += [\n";
  for (fields_t::const_iterator i = fields.begin(); i != fields.end(); i++) {
    if (!i->second.toDef) continue;
    f << "  ('" << i->second.cname << "', '" << i->second.id 
      << "', '', ''),\n";
  }
  f << "]\n";

  // predecode fields
  f << "\npredecodefields = [";
  for (fields_t::const_iterator i = fields.begin(); i != fields.end(); i++) {
    if (i->second.accessorText == "" && i->second.predecode) {
      f << "'" << i->second.id << "', ";
    }
  }
  f << "]\n";

  // capabilities

  f << "\ncapabilities = [\n";
  
  for (capabilities_t::iterator i = allCapabilities.begin();
       i != allCapabilities.end(); i++) {
    f << "  '" << (*i) << "',\n";
  }
  f << ']' << std::endl;

  // step names

  if (isLSEemu && steps.size()) {
    f << "\nstep_names = [\n";
    
    for (steps_t::iterator i = steps.begin(); i != steps.end(); i++) {
      f << "  ( " << "'" << i->second.id << "', " << i->second.stepNo 
	<< ", " << ((int)(i->second.steptype)) << "), # from " 
	<< i->second.loc << std::endl;
    }
    f << ']' << std::endl;
  } else f << "\nstep_names = [( 'dummy', 0, 0), ]\n";

  // There is no harm in putting out extra attributes into the description
  // file when the capabilities are not used, so I might as well just do
  // all of them now

  // operand names

  f << "\noperand_names = [" << std::endl;
  for (std::map<std::string, operandname_t>::iterator i = opernames.begin(); 
       i != opernames.end(); i++) {

    int otype = (int)i->second.opertype;

    f << "  ( '" << i->first << "', "<< i->second.offset 
      << "), # from " << i->second.loc << std::endl;
  }
  f << ']' << std::endl;
  for (int i = 0; i < 2; i++) 
    if (max_opers[i] >= 0) 
      f << "max_operand_" << otnames[i] << " = " 
	<< (max_opers[i]+1) << std::endl;

  // operandval

  f << "\noperandvaltype = '''" << std::endl
    << "union {" << std::endl;

  for (opvaluetype_t::const_iterator 
	 i = opvaluetype.begin(); i != opvaluetype.end(); i++) 
    f << "  " << i->second << " " << i->first << ";\n";

  f << "}'''" << std::endl;


  f << code["description"] << std::endl << std::endl;

  f.close();

}

namespace {

  wordSet_t get_word_set(const std::string &s) {
    wordSet_t nwl;
    nwl.insert("LIS_WORDLIST_DUMMY"); // so we can deal with empty lists
    std::string::size_type sl, ws;
    bool inword = false;
    sl = s.size();
    for (std::string::size_type i = 0 ; i < sl; i++) {
      char c = s[i];
      if (inword) {
	if (!(std::isalnum(c) || c == '_')) {
	  nwl.insert(s.substr(ws,i-ws));
	  inword = false;
	}
      } else {
	if (std::isalpha(c) || c == '_') { ws = i; inword = true;}
      }
    }
    if (inword) nwl.insert(s.substr(ws,sl-ws));
    return nwl;
  }

  std::string strip_space(const std::string &s) {
    std::string::size_type l = s.find_first_not_of(" \t\n");
    if (l == std::string::npos) return s;
    return s.substr(l,s.find_last_not_of(" \t\n")+1-l);
  }

  template<class T> wordList_t split(const std::string &s, T c) {
    wordList_t nwl;
    if (s.size() == 0) return nwl;
    std::string::size_type i = 0, it;
    while ( ( it = s.find(c,i)) != std::string::npos) {
      nwl.push_back(strip_space(s.substr(i,it-i)));
      i = it + 1;
    }
    nwl.push_back(strip_space(s.substr(i,s.size()-i)));
    return nwl;
  }

  wordList_t typesplit(const std::string &s) {
    wordList_t nwl;
    std::string::size_type i = s.find_last_of(" \t\n");
    if (i == std::string::npos) {
      nwl.push_back("int");
      nwl.push_back(s);
    } else {
      nwl.push_back(strip_space(s.substr(0,i)));
      nwl.push_back(strip_space(s.substr(i+1)));
    }
    return nwl;
  }

  std::string h_to_define(std::string name) {
    std::string newstr("_");
    for (std::string::iterator i = name.begin();
	 i != name.end(); i++) {
      char n = toupper(*i);
      if (!isalpha(n)) n = '_';
      newstr += n;
    }
    newstr += '_';
    return newstr;
  }

  const char stdintdefs[] = "#include <LSE_inttypes.h>\n";

  std::string make_decodetoken(std::string tname) {
    return "LSE_emu_decodetoken_" + tname;
  }

  std::string make_opcode(std::string tname) {
    return "LSE_emu_opcode_" + tname;
  }

  void print_codesection(std::ostream &f, const char *keyn, code_t &gcode,
			 const std::string &name)
  {
    std::string key = std::string(keyn);
    std::string code = gcode[key];

    // Replace sub-codesection text
    std::set<std::string> included;
    std::string::size_type lfs, rfs;

    lfs = code.find("LIS_CODESECTION(");
    while (lfs != std::string::npos) {

      rfs = code.find(")", lfs);
      if (rfs == std::string::npos) break;
      std::string tname = code.substr(lfs+16, rfs-lfs-16);

      if (!included.count(tname)) { // prevent recursion of codesections
	code.replace(lfs,rfs-lfs+1,gcode[tname]);
	included.insert(tname);

	lfs = code.find("LIS_CODESECTION(");
      }
    }

    while ( (lfs = code.find("%%EMUNAME()")) != std::string::npos) {
      code.replace(lfs, 11, name);
    }

    f << code << std::endl;
  }

  // Replace type definitions; note that it ignores both quotes and comments

  void replace_types(std::string &code, 
		     std::map<std::string, std::string> &typestrings, 
		     std::map<std::string, bool> &wasFound) {
    std::string::size_type lfs, rfs;
    lfs = code.find("LIS_TYPE(");
    while (lfs != std::string::npos) {
      rfs = code.find(")",lfs);
      if (rfs == std::string::npos) break;
      std::string tname = code.substr(lfs+9, rfs-lfs-9);
      wasFound[tname] = true;
      code.replace(lfs,rfs-lfs+1,typestrings[tname]);
      lfs = code.find("LIS_TYPE(", lfs);
    }
  }

  void generate_options(std::ostream &f, 
			const cvals_t &options)
  {
    for (cvals_t::const_iterator 
	   i = options.begin(); i != options.end(); i++)
      f << "  const int " << i->first << " = " 
	<< static_cast<int>(i->second.val) << ";\n";
  } // generate_options

  void check_contents(styleExt_t &styleExt, const std::string &s, 
		      bool isEntry) {
    std::string::size_type it;

    if ( (it = s.find("%%AFTERREC()",0)) != std::string::npos) {
      styleExt.needsAtable = true;
    }

    if ( (it = s.find("%%AFTERPTR()",0)) != std::string::npos) {
      styleExt.needsAtable = true;
    }
    
    if (!isEntry && (it = s.find("%%ENTRYTEXT(",0)) != std::string::npos) {
      styleExt.needsTtable = true;
    }

    if (!isEntry && (it = s.find("%%ENTRY()",0)) != std::string::npos) {
      styleExt.needsEntry = true;
    }

  } // check_contents

} // anonymous namespace

// Figure out stuff we'll need to know to build.
void LIS_sim::figureOut(void) {

  // build entrypoint lists for styles
  for (buildsetOrder_t::const_iterator 
	 i = buildsetOrder.begin(); i != buildsetOrder.end(); i++) {
    buildset_t &bs = buildsets[*i];
    style_t *sp = &styles[bs.style];
    styleExts[sp].buildsets.push_back(const_cast<buildset_t *>(&bs));

    for (entrypoints_t::const_iterator 
	   j = bs.entrypoints.begin(); j != bs.entrypoints.end(); j++) {
      styleExts[sp].entrypoints.push_back(j->second);
    }
  }

  // build needs fields for styles.  

  for (styleOrder_t::iterator 
	 i = styleOrder.begin(); i != styleOrder.end(); i++) {
    style_t& style = **i;
    styleExt_t& styleExt = styleExts[&style];

    styleExt.needsAtable = false;
    styleExt.needsTtable = false;
    styleExt.needsEntry = false;

    if (styleExt.buildsets.size() && styleExt.entrypoints.size()) {

      check_contents(styleExt, style.entry.code, true);

      for (generators_t::const_iterator j = style.generators.begin(); 
	   j != style.generators.end(); j++) {
	check_contents(styleExt, j->second.code, false);
      }

    } // has some buildsets associated, so should be used

  } // for styles

  for (buildsetOrder_t::const_iterator 
	 i = buildsetOrder.begin(); i != buildsetOrder.end(); i++) {
    buildset_t &bs = buildsets[*i];
    style_t &S = styles[bs.style];

    hiddenFields_t &SF = buildsetExts[&bs].specializedFields;

    std::ostringstream pss;
    bool isfirst = true;

    for (hiddenFields_t::const_iterator j = S.specializedFields.begin(),
	   je = S.specializedFields.end() ; j != je ; ++j) {

      const field_t &fld =* j->second.fp;
      hide_status_t h = (bs.hiddenFields.count(fld.id) ? 
			 bs.hiddenFields[fld.id].hidestatus : 
			 ((fld.accessorText.size() && !fld.toDef) 
			  ? nothidden : hidden));

      hide_t h2 = { const_cast<field_t *>(&fld), h };
      SF[j->first] = h2;
      if (h != refhidden) {
	if (!isfirst) pss << ", ";
	isfirst = false;
	pss << fld.cname << " " << fld.id;
      }
    }

    for (specializedBitfields_t::const_iterator 
	   j = S.specializedBitfields.begin(), 
	   je = S.specializedBitfields.end() ; 
	   j != je ; ++j) {
      if (!isfirst) pss << ", ";
      isfirst = false;
      pss << "const unsigned long " << *j;
    }

    for (entrypoints_t::const_iterator 
	   j = bs.entrypoints.begin(); j != bs.entrypoints.end(); j++) {

      if (pss.str().size())
	if (j->second->params.size())
	  j->second->extendedParams = j->second->params + ", " + pss.str();
	else
	  j->second->extendedParams = pss.str();
      else j->second->extendedParams = j->second->params;
    }

  } // buildsets

} // LSE_sim::figureOut

namespace {

  class rangeEarlier : public std::unary_function<range_t, bool>
  {
    int no;
  public:
    explicit rangeEarlier(const int n) : no(n) {}
    bool operator() (const range_t &t) const { return t.from < no; }
  };

  inline uint64_t make_mask(const int f, const int l) {
    return ((static_cast<uint64_t>(1)<<l)-1) << f;
  }

  class GenerateControls {
  public:
    std::string name;
    bool hasOperVal;
    bool hasOperInfo;
    buildset_t &bs;
    buildsetExt_t &bsExt;
    iclasses_t &iclasses;
    style_t &style;
    GenerateControls(std::string &n,
		     buildset_t &b, buildsetExt_t &be, iclasses_t &ic, 
		     style_t &s) 
      : name(n), bs(b), bsExt(be), iclasses(ic), style(s) 
    {
      hasOperVal = (std::find(bs.capabilities.begin(),
			      bs.capabilities.end(),
			      std::string("operandval")) != 
		    bs.capabilities.end());
      hasOperInfo = (std::find(bs.capabilities.begin(),
			       bs.capabilities.end(),
			       std::string("operandinfo")) != 
		     bs.capabilities.end());
    }
  };

  void 
  generate_operands(std::ostream &f, operands_t &operands, 
		    GenerateControls &gc, const wordSet_t &wl,
		    bool suppressDuplicate)
  {
    // deal with operandinfo and operandval
    operands_t& bops = gc.iclasses[gc.bs.baseclass].operands;

    for (operands_t::const_iterator l = operands.begin();
	 l != operands.end(); l++) {
      const operand_t &op = l->second;

      if (suppressDuplicate && bops.count(op.id)) continue;
      
      if (!wl.size() || wl.count(std::string("LIS_oper_info_")+op.id))
	if (gc.hasOperInfo) 
	  f << "  LSE_emu_operand_info_t& LIS_oper_info_" << op.id 
	    << " LIS_UNUSED = %%%II.operand_" 
	    << (op.opername->opertype == LIS::oper_src ? "src" : "dest") 
	    << "[" << op.opername->offset << "];\n";
	else
	  f << "  LSE_emu_operand_info_t LIS_oper_info_" << op.id 
	    << " LIS_UNUSED;\n";

      if (!wl.size() || wl.count(std::string("LIS_oper_valid_")+op.id))
	if (gc.hasOperVal) {
	  f << "  char& LIS_oper_valid_" << op.id 
	    << " LIS_UNUSED = %%%II.operand_val_"
	    << (op.opername->opertype == LIS::oper_src ? "src" : "dest") 
	    << "[" << op.opername->offset << "].valid;\n";
	} else {
	  f << "  char LIS_oper_valid_" << op.id << " LIS_UNUSED;\n";
	}

      if (!wl.size() || wl.count(op.id))
	if (gc.hasOperVal) {
	  f << "  " << op.acc->ctype << "& " << op.id << " "
	    << "LIS_UNUSED = %%%II.operand_val_" 
	    << (op.opername->opertype == LIS::oper_src ? "src" : "dest") 
	    << "[" << op.opername->offset << "].data." << op.acc->ovfield 
	    << ";\n";
	} else {
	  f << "  " << op.acc->ctype << " " << op.id << " " 
	    << "LIS_UNUSED;\n";
	}
      
    }
       
  } // generate_operands

  void 
  generate_bitfields(std::ostream& f, bitfields_t& bitfields,
		     matchinfo_t &matchcomm,
		     GenerateControls& gc, const wordSet_t &wl,
		     bool dospec,
		     bool suppressDuplicate=false)
  {
    // deal with operandinfo and operandval
    bitfields_t& bbfs = gc.iclasses[gc.bs.baseclass].bitfields;

    // bitfields
    for (bitfields_t::const_iterator l = bitfields.begin();
	 l != bitfields.end(); l++) {
      const bitfield_t &bf = l->second;

      if (suppressDuplicate && bbfs.count(bf.id)) continue;
      if (wl.size() && !wl.count(bf.id)) continue;
      if (dospec && gc.style.specializedBitfields.count(bf.id)) continue;

      if ((matchcomm[bf.field].mask & make_mask(bf.from,bf.length))
	  == make_mask(bf.from,bf.length)) { /* constant */
	uint64_t val = ((matchcomm[bf.field].val >> bf.from) &
			make_mask(0,bf.length));
	if (bf.length >= 32) 
	  f << "const uint64_t " << bf.id << " LIS_UNUSED = UINT64_C(0x" 
	    << std::hex << val << std::dec << ");\n";
	else
	  f << "const unsigned long " << bf.id << " LIS_UNUSED = 0x" 
	    << std::hex << val << std::dec << "UL;\n";
      } else { /* not constant */
	if (bf.length >= 32) {

	  f << "const uint64_t " << bf.id 
	    << " LIS_UNUSED = UINT64_C(0x" << std::hex  
	    << make_mask(0,bf.length) << std::dec << ") & (";
	  if (bf.field.length()) f << "instr." << bf.field;
	  else f << "instr";
	  f << " >> " << bf.from << ")";
	} else {
	  long val = (1 << bf.length) - 1;
	  f << "const unsigned long " << bf.id << " LIS_UNUSED = 0x" 
	    << std::hex << val << "UL & (" << std::dec;
	  if (bf.field.length()) f << "instr." << bf.field;
	  else f << "instr";
	  f << " >> " << bf.from << ")";
	}
	f << ";\n";
      }
    } // for bitfields

  }

  std::string substituteII(std::string s, std::string NN) {
    std::string::size_type it;
    while ( (it = s.find("%%%II")) != std::string::npos)
      s.replace(it, 5, NN);
    return s;
  }

  void 
  print_instruction_piece(std::ostream &f, iclass_t &instr,
			  const std::string &itext, 
			  GenerateControls &gc)
  {
    wordSet_t wl = get_word_set(itext);
    std::ostringstream f2;

    // f << "{ // " <<  instr.id << " " << instr.loc << std::endl;
    f << "{\n";

    if (!wl.size() || wl.count("LIS_opcode"))
      f2 << "  LSE_emu_opcode_t LIS_opcode LIS_UNUSED = " 
	<< make_opcode(instr.opcode) << ";\n";

    // deal with operandinfo and operandval
    generate_operands(f2, instr.operands, gc, wl, true);

    // bitfields
    generate_bitfields(f2, instr.bitfields, instr.matchcommon, gc, wl, 
		       true, false);

    f2 << std::endl;
    f << substituteII(f2.str(),"LIS_ii") << itext;

    f << "}\n";
  } // print_instruction_piece

  void decodeArray(std::ostream &f, std::ostringstream &comp,
		   int nspace, int &currind,
		   decisionfunc_t *dt, std::map<std::string,int> &fieldenc,
		   std::map<std::string,int> &pieces
		   ) {
    std::string spaces(nspace,' ');
    std::ostringstream sub, compsub;
    int saveind = 0;
    
    f << ",\n" << spaces;
    comp << ",";

    switch (dt->kind) {

    case decisionfunc_t::dec_leaf:
      f << "uint64_t(-" << (dt->leaf ? dt->leaf->idno : 0) 
	<< ") /* " << (dt->leaf ? dt->leaf->id : "<none>") 
	<< " freq=" << dt->freq << " */";
      comp << "-" << (dt->leaf ? dt->leaf->idno : 0);
      break;

    case decisionfunc_t::dec_pattern:
      
      if (currind) f << "/* " << currind << " */ ";
      for (matchinfo_t::const_iterator j = dt->pattern.begin();
	   j != dt->pattern.end(); j++) {
	if (fieldenc.find(j->first) == fieldenc.end()) {
	  int sz = 1+fieldenc.size();
	  fieldenc[j->first] = sz;
	}
	f << fieldenc[j->first]*2
	  << ", UINT64_C(0x" << std::hex << j->second.mask << ")"
	  << ", UINT64_C(0x" << j->second.val << ") " << std::dec << ", ";
	comp << fieldenc[j->first]*2 << ',' << j->second.mask << ','
	     << j->second.val << ',';
	currind += 3;
      }
      f << "0";
      comp << "0";
      currind += 3;

      if (dt->child[1]->kind == decisionfunc_t::dec_leaf) 
	decodeArray(f, comp, nspace+4, saveind, dt->child[1], fieldenc, 
		    pieces);
      else {
	saveind = currind;
	std::ostringstream sub2, compsub2;
	decodeArray(sub2, compsub2, nspace+2, currind, dt->child[1], fieldenc,
		    pieces);
	std::string s = compsub2.str();
	int num;
	if ((num = pieces[s])) { // it's been done before
	  f << ",\n" << spaces << "    " << num;
	  comp << "," << num;
	  currind = saveind;
	} else {
	  f << ",\n" << spaces << "    " << saveind;
	  comp << "," << saveind;
	  pieces[s] = saveind;
	  sub << sub2.str();
	  compsub << s;
	}
      } 

      if (dt->child[0]->kind == decisionfunc_t::dec_leaf) 
	decodeArray(f, comp, nspace+4, saveind, dt->child[0], fieldenc, 
		    pieces);
      else {
	saveind = currind;
	std::ostringstream sub2, compsub2;
	decodeArray(sub2, compsub2, nspace+2, currind, dt->child[0], fieldenc,
		    pieces);
	std::string s = compsub2.str();
	int num;
	if ((num = pieces[s])) { // it's been done before
	  f << ",\n" << spaces << "    " << num;
	  comp << "," << num;
	  currind = saveind;
	} else {
	  f << ",\n" << spaces << "    " << saveind;
	  comp << "," << saveind;
	  pieces[s] = saveind;
	  sub << sub2.str();
	  compsub << s;
	}
      }
      f << sub.str();
      comp << compsub.str();
      break;

    case decisionfunc_t::dec_table:

      if (currind) f << "/* " << currind << " */ ";
      if (fieldenc.find(dt->table.field) == fieldenc.end()) {
	int sz = 1+fieldenc.size();
	fieldenc[dt->table.field] = sz;
      }
      f << fieldenc[dt->table.field]*2+1 
	<< ", " << dt->table.from << ", 0x" << std::hex 
	<< ((1<<dt->table.length)-1) << std::dec;
      comp << fieldenc[dt->table.field]*2+1 << ',' << dt->table.from << ','
	   << ((1<<dt->table.length)-1);

      currind += (3 + (1<<dt->table.length));
      for (int i=0; i < (1<<dt->table.length); i++) {
	if (dt->child[i]->kind == decisionfunc_t::dec_leaf)
	  decodeArray(f, comp, nspace+4, saveind, dt->child[i], fieldenc,
		      pieces);
	else {
	  saveind = currind;
	  std::ostringstream sub2, compsub2;
	  decodeArray(sub2, compsub2, nspace+2, currind, dt->child[i], 
		      fieldenc, pieces);
	  std::string s = compsub2.str();
	  int num;
	  if ((num = pieces[s])) { // it's been done before
	    f << ",\n" << spaces << "    " << num;
	    comp << "," << num;
	    currind = saveind;
	  } else {
	    f << ",\n" << spaces << "    " << saveind;
	    comp << "," << saveind;
	    pieces[s] = saveind;
	    sub << sub2.str();
	    compsub << s;
	  }
	}
      }
      f << sub.str();
      comp << compsub.str();
      break;

    default: break;
    }
  }

  void doDecodeArray(std::ostream &f, int nspace, int &currind,
		     decisionfunc_t *dt, std::map<std::string,int> &fieldenc
		     ) {
    std::map<std::string, int> goodies;
    std::ostringstream comp;
    decodeArray(f, comp, nspace, currind, dt, fieldenc, goodies);
  }

  void generate_fields(std::ostream &f, 
		       const LIS_isa::fieldsOrdered_t &fieldsOrdered,
		       hiddenFields_t &hiddenFields, 
		       hiddenFields_t &specializedFields, const wordSet_t &wl,
		       bool useSpec=true)
  {
    // fields should be treated as refhidden if they are not in the
    // word list and they are not referred to by later fields.  This
    // does make it a bit harder to deal with...
    //
    // They should also be treated as refhidden if they are specialized.
    LIS_isa::fieldsOrdered_t toDo;
    wordSet_t wl2;

    for (LIS_isa::fieldsOrdered_t::const_reverse_iterator 
	   k = fieldsOrdered.rbegin();
	 k != fieldsOrdered.rend(); k++) {
      const field_t &fld = **k;
      hide_status_t h = (hiddenFields.count(fld.id) ? 
			 hiddenFields[fld.id].hidestatus : 
			 ((fld.accessorText.size() && !fld.toDef) 
			  ? nothidden : hidden));

      if (h != refhidden && 
	  (!wl.size() || wl.count(fld.id) || wl2.count(fld.id))) {
	toDo.push_back(*k);
	if (h == nothidden && fld.accessorText.size()) {
	  wordSet_t wl3 = get_word_set(fld.accessorText);
	  wl2.insert(wl3.begin(),wl3.end());
	}
      }
    } // put together the list of fields we really should generate

    for (LIS_isa::fieldsOrdered_t::reverse_iterator k = toDo.rbegin();
	 k != toDo.rend(); k++) {
      const field_t &fld = **k;
      hide_status_t h = (hiddenFields.count(fld.id) ? 
			 hiddenFields[fld.id].hidestatus : 
			 ((fld.accessorText.size() && !fld.toDef) 
			  ? nothidden : hidden));

      if (useSpec && specializedFields.count(fld.id)) h = refhidden;
			 
      switch (h) {
      case hidden: 
	  f << "    " << fld.cname << " " << fld.id << " LIS_UNUSED ;\n";
	  break;
      case refhidden: break; // do not show it
      case nothidden:
	if (fld.accessorText.size()) {
	  if (fld.isExpr) 
	    f << "    " << fld.cname << " " << fld.id 
	      << " LIS_UNUSED = " << fld.accessorText << ";\n";
	  else 
	    f << "    " << fld.cname << " &" << fld.id 
	      << " LIS_UNUSED = " << fld.accessorText << ";\n";
	} else {
	  if (fld.predecode) 
	    f << "    " << fld.cname << " &" << fld.id 
	      << " LIS_UNUSED = " << "%%%II.pre_info->" << fld.id << ";\n";
	  else 
	    f << "    " << fld.cname << " &" << fld.id 
	      << " LIS_UNUSED = " << "%%%II." << fld.id << ";\n";
	}
	break;
      default: break;
      } // switch (h)
    }
  } // generate_fields

  std::string string_constant_form(std::string p) {
    std::string rval;
    char tbuf[30];
    for (unsigned int i = 0; i < p.size(); i++) {
      char c = p[i];
      if (c == '"') rval += "\\\"";
      else if (c == '\\') rval += "\\\\";
      else if (c == '\n') rval += "\\n\"\n\""; // newline and line break
      else if (c == '\t') rval += "\\t";
      else if (c == '\v') rval += "\\v";
      else if (c == '\r') rval += "\\r";
      else if (c == ' ') rval += " ";
      else if (!std::isgraph(c)) {
	sprintf(tbuf,"\\%03o",c);
	rval += tbuf;
      } else rval += c;
    }
    return rval;
  } // string_constant_form

  std::string::size_type matchedfind(const std::string &s, 
				     std::string::size_type curr) {
    int level = 1;
    std::string::size_type end = s.size();
    while (curr != end) {
      switch (s[curr]) {
      case '(' : level++; break;
      case ')' : if (!--level) return curr;
      default: break;
      }
      curr++;
    }
  }

  void handletext(std::string &s) {
    std::string::size_type it;
    while ( ( it = s.find("%%TEXT(")) != std::string::npos) {
      std::string::size_type p2 = matchedfind(s, it+7);
      if (p2 == std::string::npos) p2 = s.size();
      std::string s2 = ( "\"" 
			 + string_constant_form(s.substr(it + 7, p2 - it - 7))
			 + "\"" );
      s.replace(it, p2 + 1 - it, s2);
    }
  }

  void do_macros(std::string& s, const codepoint_t &ep,
		 const LIS_isa::instructionsOrdered_t & ilist,
		 GenerateControls& gc, bool templateKnown = false) {

    const buildset_t& bs = gc.bs;
    buildsetExt_t &bsExt = gc.bsExt;
    const iclasses_t& iclasses = gc.iclasses;
    const style_t& style = gc.style;
    std::string::size_type it;

    std::string extP = style.entry.eparms.size() ?
      ep.extendedParams.size() ? (style.entry.eparms + "," + ep.extendedParams)
      : style.entry.eparms : ep.extendedParams;

    // %%IFAFTER()
    while ( ( it = s.find("%%IFAFTER(")) != std::string::npos) {
      std::string::size_type p2 = matchedfind(s, it+10);
      if (!ep.hasAfter(gc.style.excl)) s.replace(it, p2 + 1 - it, "");
      else {
	s.replace(it, p2 + 1 - it, s.substr(it + 10, p2 - it - 10));
      }
    }

    // %%IFNAFTER()
    while ( ( it = s.find("%%IFNAFTER(")) != std::string::npos) {
      std::string::size_type p2 = matchedfind(s, it+11);
      if (ep.hasAfter(gc.style.excl)) s.replace(it, p2 + 1 - it, "");
      else {
	s.replace(it, p2 + 1 - it, s.substr(it + 11, p2 - it - 11));
      }
    }

    // %%BUILDSET()
    while ( (it = s.find("%%BUILDSET()")) != std::string::npos) {
      s.replace(it, 12, bs.id);
    }

    // %%STYLE()
    while ( (it = s.find("%%STYLE()")) != std::string::npos) {
      s.replace(it, 9, bs.style);
    }

    // %%NAME()
    while ( (it = s.find("%%NAME()")) != std::string::npos) {
      s.replace(it, 8, ep.id);
    }

    // %%EMUNAME()
    while ( (it = s.find("%%EMUNAME()")) != std::string::npos) {
      s.replace(it, 11, gc.name);
    }

    const iclass_t &bclass = iclasses.find(bs.baseclass)->second;
    std::string bef = ep.beforeText(bclass, std::string("  "), gc.style.excl);

    // %%BEFORE()
    while ( (it = s.find("%%BEFORE(")) != std::string::npos) {
      std::string::size_type p2 = s.find(')', it+9);
      if (p2 == std::string::npos) break; // TODO: really an error
      std::string ps = bef;
      if (p2 != it + 9) {
	std::string ns = s.substr(it + 9, p2 - it - 9);
	std::string::size_type foundIt;
	while ((foundIt = ps.find("LIS_ii")) != std::string::npos) 
	  ps.replace(foundIt, 6, ns);
      }
      s.replace(it, p2-it+1, ps);
    }

    // %%ENTRYTEXT()
    while ( (it = s.find("%%ENTRYTEXT(")) != std::string::npos) {
      std::string::size_type p2 = s.find(')',it+12);
      if (p2 == std::string::npos) break; // TODO: really an error

      std::ostringstream call;
      call << "(" << style.id << "::LIS_ttable." << ep.id << ".text["
	   << s.substr(it + 12 , p2 - it - 12) << "])";
      s.replace(it, p2 - it + 1,call.str());
    }

    // %%PARMS
    while ( (it = s.find("%%PARMS()")) != std::string::npos) {
      s.replace(it, 9, ep.params);
    }

    // %%AFTERPARMS
    while ( (it = s.find("%%AFTERPARMS()")) != std::string::npos) {
      s.replace(it, 14, extP);
    }

    // %%ARGS
    while ( (it = s.find("%%ARGS(")) != std::string::npos) {
      std::string::size_type p2 = s.find(')', it+7);
      if (p2 == std::string::npos) break; // TODO: really an error
      std::string ps = find_pnames(ep.params);
      if (p2 != it + 7)
	ps.replace(ps.find("LIS_ii"), 6, s.substr(it + 7, p2 - it - 7));
      s.replace(it, p2-it+1, ps);
    }

    // %%AFTERARGS
    while ( (it = s.find("%%AFTERARGS(")) != std::string::npos) {
      std::string::size_type p2 = s.find(')', it+12);
      if (p2 == std::string::npos) break; // TODO: really an error
      std::string ps = find_pnames(extP);
      if (p2 != it + 12)
	ps.replace(ps.find("LIS_ii"), 6, s.substr(it + 12, p2 - it - 12));
      s.replace(it, p2-it+1, ps);
    }

    // %%ENTRY()
    while ( (it = s.find("%%ENTRY()")) != std::string::npos) {
      std::string s2 = "LIS_entry_" + ep.id;
      s.replace(it, 9, s2);
    }

    // %%RTYPE()
    while ( (it = s.find("%%RTYPE()")) != std::string::npos) {
      std::string s2 = ep.rtype;
      s.replace(it, 9, s2);
    }
    
    // %%AFTERREC()
    if (s.find("%%AFTERREC()") != std::string::npos) {

      std::string parms = find_pnames(extP);
      
      std::ostringstream call;
      call << "(" << style.id << "::LIS_atable["
	   << ep.dcode << "])";
      
      while ( (it = s.find("%%AFTERREC()")) != std::string::npos) {
	s.replace(it, 12, call.str());
      }
    } // if there is a templatefind()

    // %%AFTERPTR()
    if (!templateKnown && s.find("%%AFTERPTR()") != std::string::npos) {

      std::string parms = find_pnames(extP);
      
      std::ostringstream call;
      call << "(" << style.id << "::LIS_atable["
	   << ep.dcode << "]." << ep.id << ")";
     
      while ( (it = s.find("%%AFTERPTR()")) != std::string::npos) {
	s.replace(it, 12, call.str());
      }
    } // %%AFTERPTR()

    // %%TOKENSWITCH
    std::ostringstream decsw;
    if (s.find("%%TOKENSWITCH()") != std::string::npos) {
      
      decsw << "      switch (" << ep.dcode << ") {\n";
	
      stringMMap_t alreadyDone;
      
      for (LIS_isa::instructionsOrdered_t::const_iterator k = ilist.begin();
	   k != ilist.end(); ++k) {
	iclass_t &instr = **k;
	std::ostringstream piece;
	
	print_instruction_piece(piece, instr, 
				ep.afterText(instr, std::string("  "),
					     gc.style.excl), gc);
	
	alreadyDone.insert(make_pair(piece.str(),
				     make_decodetoken(instr.id)));
	
      } /* for k in instructions */
	
      std::string lastOne;
      stringMMap_t::const_iterator k = alreadyDone.begin();
      if (k != alreadyDone.end()) { // something there
	lastOne = k->first;
	decsw << "  case " << k->second << ":\n";
	++k;
	for (;k != alreadyDone.end(); ++k) {
	  if (k->first != lastOne) {
	    decsw << lastOne;
	    decsw << "  break;\n";
	    lastOne = k->first;
	  }
	  decsw << "  case " << k->second << ":\n";
	}
	decsw << lastOne;
	decsw << "  break;\n";
      }
      
      decsw << "      default: break;\n"
	    << "      }\n";
      
      while ( (it = s.find("%%TOKENSWITCH()")) != std::string::npos) {
	s.replace(it, 15, decsw.str()); // TODO: fill this in
      }
    } // %%TOKENSWITCH

    // %%TOKEN
    while ( (it = s.find("%%TOKEN()")) != std::string::npos) {
      s.replace(it, 9, ep.dcode);
    }

    // %%COMMA
    while ( (it = s.find("%%COMMA(")) != std::string::npos) {
      std::string::size_type p2 = matchedfind(s, it+8);
      if (p2 == std::string::npos) p2 = s.size();
      std::string s2 = strip_space(s.substr(it+8,p2-it-8));
      std::string s3 = s2.size() ? "," : "";
      s3 += s2;
      s.replace(it, p2 + 1 - it, s3);
    }

  } // do_macros

  std::string find_fields(std::string &g,
			  const LIS_isa::fieldsOrdered_t &fieldsOrdered,
			  buildset_t &bs, buildsetExt_t &bsExt,
			  GenerateControls &gc,
			  iclass_t &bc, bool useSpec=true) {

    wordSet_t wl = get_word_set(g);
    std::ostringstream oss2;

    generate_fields(oss2, fieldsOrdered, bs.hiddenFields, 
		    bsExt.specializedFields, wl, useSpec);
    oss2 << std::endl;
	  
    generate_operands(oss2, bc.operands, gc, wl, false);
    oss2 << std::endl;
    
    hiddenFields_t dummySF;
    generate_bitfields(oss2, bc.bitfields, bc.matchcommon, gc, 
		       wl, useSpec, false);
    oss2 << std::endl;
    return oss2.str();
  }

  void handledecls(std::string &s, std::string flds) {
    std::string::size_type it;
    while ( (it = s.find("%%DECLS(")) != std::string::npos) {
      std::string fldsToUse(flds);
      std::string::size_type p2 = s.find(')', it+8);
      std::string::size_type p3 = s.find(',', it+8);

      if (p2 == std::string::npos) break; // TODO: really an error

      // Discover any fields which should be treated as hidden
      if (p3 != std::string::npos && p3 < p2) {
	std::string::size_type pt = p3 + 1;
	while (1) {
	  std::string::size_type p4 = s.find(',', pt);
	  std::string::size_type p5 = s.find(')', pt);
	  p4 = std::min(p4,p5);

	  std::string id = s.substr(pt, p4 - pt);
	  std::string tofind = "&" + id + " LIS_UNUSED = %%%II." + id;
	  std::string torepl = id + " LIS_UNUSED ";

	  std::string::size_type it2;
	  while ( (it2 = fldsToUse.find(tofind)) != std::string::npos)
	    fldsToUse.replace(it2, tofind.size(), torepl);
	  pt = p4 + 1;
	  if (p4 == p5) break;
	}
      } else p3 = p2;

      // now substitute the fields into the string
      if (p3 == it + 8) 
	s.replace(it, p2 - it + 1, substituteII(fldsToUse, "LIS_ii"));
      else 
	s.replace(it, p2 - it + 1, 
		  substituteII(fldsToUse, s.substr(it+8, p3 - it - 8)));
    }
  }

  std::string genStyleName(std::string emuname, std::string style) {
    return emuname + "." + style + ".cc";
  }
  std::string genTemplName(std::string emuname, std::string style) {
    return emuname + "." + style + ".i";
  }

} // anonymous namespace 

void LIS_sim::createCode(std::string name)
{
  std::fstream spt, entry, gen, pubH, privH, mk;

  //        Open and put in headers

  spt.open((name + supportSuffix).c_str(), std::ios::out);
  mk.open((name + mkSuffix).c_str(), std::ios::out);

  pubH.open((name + publicHSuffix).c_str(), std::ios::out);
  privH.open((name + privateHSuffix).c_str(), std::ios::out);
  
  std::string pubGuard = h_to_define(name+".h");
  std::string privGuard = h_to_define(name+privateHSuffix);

  // top of files

  pubH << "/*************************************************************\n"
       << " * Emulator public header file generated by " << progname << "\n"
       << " *************************************************************/\n\n"
       << "#ifndef " << pubGuard << "\n"
       << "#define " << pubGuard << "\n"
       << stdintdefs;

  privH << "/***********************************************************\n"
	<< " * Emulator private header file generated by " << progname << "\n"
	<< " ***********************************************************/\n\n"
	<< "#ifndef " << privGuard << "\n"
	<< "#define " << privGuard << "\n"
	<< stdintdefs;


  spt << "/**************************************************************\n"
      << " * Emulator support file generated by " << progname << std::endl
      << " **************************************************************/\n\n"
      << "#include " << '"' << name << privateHSuffix << '"' << std::endl;
  spt << "/**************** codesection supportheaders *****************/\n";
  print_codesection(spt, "supportheaders", code, name);
  spt << "namespace " << namespaceName << " {\n" 
      << "/*********************** prototypes ***************************/\n";
  
  mk << "#*****************************************************************"
     << std::endl 
     << "# Emulator makefile generated by " << progname
     << std::endl
     << "#*****************************************************************"
     << std::endl << std::endl;

  /************************* makefile ******************************/

  mk << "LIS_SRCFILES=" << namespaceName << ".support.cc";

  /************* Prepare per-entry codesection stuff ***************/

  for (styleOrder_t::const_iterator SI = styleOrder.begin(),
	 SE = styleOrder.end(); SI != SE ; ++SI) {

    style_t &style = **SI;
    styleExt_t &styleExt = styleExts[*SI];

    if (!style.generators.size()) continue;

    for (buildsetOrder_t::iterator BSI = buildsetOrder.begin(),
	   BSE = buildsetOrder.end(); BSI != BSE ; ++BSI) {

      buildset_t &bs = buildsets[*BSI];

      if (style.id != bs.style) continue;

      buildsetExt_t &bsExt = buildsetExts[&bs];
      GenerateControls gc = GenerateControls(name, bs, buildsetExts[&bs],
					     iclasses, style);
      
      // restrict list to the instructions in the buildset ... this
      // ought to be remembered.
      iclass_t &bc = iclasses[bs.baseclass];
      instructionsOrdered_t ilist;
      iclass_set_t &uc = bc.usedby_class;
      for (instructionsOrdered_t::const_iterator 
	     j = instrsOrdered.begin(); j != instrsOrdered.end(); j++) {

	if (!((*j)->canDecode)) continue;

	if (uc.count(*j) || *j == &bc) ilist.push_back(*j);
      }

      // Iterate across entrypoints of the buildset

      for (entrypoints_t::const_iterator EI = bs.entrypoints.begin(),
	     EE = bs.entrypoints.end() ; EI != EE ; ++EI) {
	const codepoint_t &ep = *EI->second;

	// entry must come before generator so that typeof in the 
	// generator will work properly (and save me from having to
	// create yet another macro).

	if (style.generators.size() && styleExt.needsEntry) {

	  std::ostringstream oss;
	  std::string s = style.entry.code;

	  oss << (style.entry.rtype.size() ? style.entry.rtype : ep.rtype) 
	      << " LIS_entry_" << ep.id	<< '(';

	  if (style.entry.eparms.size()) {
	    oss << style.entry.eparms;
	    if (ep.params.size()) oss << ",";
	  }

	  oss << ep.params << ")\n  {\n"
	      << " using namespace " << bs.id << ";\n" 
	      << " using namespace " << style.id << ";\n";

	  do_macros(s, ep, ilist, gc);
	  handledecls(s, find_fields(s, fieldsOrdered, bs, bsExt, gc, bc, 
				     false));
	  handletext(s);
	  oss << "\n" << s << "  } // LIS_entry_" << ep.id << "\n";
	  addCode(style.id + "_prologue", oss.str());
	}

	// And now iterate across the generators
	for (generators_t::const_iterator GI = style.generators.begin(),
	       GE = style.generators.end() ; GI != GE ; ++GI) {

	  std::string g = "\n"; g += GI->second.code;
	  std::ostringstream os;

	  do_macros(g, ep, ilist, gc);
	  handledecls(g, find_fields(g, fieldsOrdered, 
				     bs, bsExt, gc, bc, false));
	  handletext(g);

	  addCode(GI->second.section, g);
	} // for GI in generators

      } // for EI in entrypoints

    } // for BS in buildsets

  } // for SI in styles


  /************* user type preparation *****************************/

  std::map<std::string, std::string> typestrings;
  std::map<std::string, bool> typewasFound;

  // Calculate type definitions
  for (typeOrder_t::const_iterator
	 i = typeOrder.begin(); i != typeOrder.end(); i++) {
    std::ostringstream os;
    typedef_t &t = typedefs[*i];
    switch (t.ttype) {
    case scalar: 
      os << "  typedef " << t.strdef << ";\n"; 
      break;
    case structdef: 
      {
	os << "  struct " << t.id << " {\n";
	for (typeOrder_t::const_iterator 
	       j = t.defnOrder.begin(); j != t.defnOrder.end(); j++)
	  os << "    " << t.defn[*j].defn << ";\n";
	os << "  };\n";
      } 
      break;
    case enumdef: 
      {
	os << "  enum " << t.id << " {\n";
	for (typeOrder_t::const_iterator 
	       j = t.defnOrder.begin(); j != t.defnOrder.end(); j++)
	  os << "    " << t.defn[*j].defn << ",\n";
	os << "  };\n";
      }
      break;
    default: break;
    } // switch
    typestrings[t.id] = os.str();
  }  // for i in types

  // Now search for types in the code sections and replace them

  for (code_t::iterator ii = code.begin(); ii != code.end(); ++ii) 
    replace_types(ii->second, typestrings, typewasFound);

  // Public header file

  pubH << "/**************** codesection headers ************************/\n";
  print_codesection(pubH, "headers", code, name);

  privH << "/**************** codesection privateheaders *****************/\n";
  print_codesection(privH, "privateheaders", code, name);

  pubH << "\n\nnamespace " << namespaceName << " {\n"

       << "  /**************** public options ****************************/\n";
  generate_options(pubH, options);

  pubH << "  /******* GENERATED TYPES not depending on user types ********/\n";

  pubH << "  enum LSE_emu_decodetoken_t {\n";
  for (instructionsOrdered_t::const_iterator 
	 i = instrsOrdered.begin(); i != instrsOrdered.end(); i++) {
    pubH << "  " << make_decodetoken((*i)->id) << " = " << (*i)->idno << ",\n";
  }
  pubH << "  " << make_decodetoken("LIS_max") << " = " 
       << (instrsOrdered.size()+1) << "\n"
       << "};\n\n";
  
  // generate the opcode type
  std::set<std::string> opcodeNames;
  for (iclasses_t::const_iterator 
	 i = iclasses.begin(); i != iclasses.end(); i++) {
    if (!i->second.isInstruction) continue;
    opcodeNames.insert(i->second.opcode);
  }
  pubH << "  enum LSE_emu_opcode_t {\n";
  for (std::set<std::string>::const_iterator 
	 i = opcodeNames.begin(); i != opcodeNames.end(); i++) {
    pubH << "  " << make_opcode(*i) << ",\n";
  }
  pubH << "  LSE_emu_max_opcode\n"
       << "};\n\n";
  
  pubH << "  extern const char *const LSE_emu_opcode_names[];\n\n";
  pubH << "  extern const char *const LSE_emu_decodetoken_names[];\n\n";
  
  pubH << "  /**************** codesection earlypublic ******************/\n";
  print_codesection(pubH, "earlypublic", code, name);
  pubH << std::endl;

  pubH << "  /**************** GENERATED TYPES ***************************/\n";

  // include user-declared types here
  for (typeOrder_t::const_iterator
	 i = typeOrder.begin(); i != typeOrder.end(); i++) {
    if (!typewasFound[*i]) pubH << typestrings[*i];
  } // for i in types
  
  pubH << "  /**************** codesection public ************************/\n";
  print_codesection(pubH, "public", code, name);
  pubH << std::endl;

  // Private header file

  privH << "#if defined(__GNUC__) && !defined(LIS_UNUSED)\n"
	<< "#define LIS_UNUSED __attribute__ ((unused))\n"
	<< "#else\n"
	<< "#define LIS_UNUSED\n"
	<< "#endif\n"
	<< "#include " << '"' << (name+publicHSuffix) << '"' << std::endl

	<< "namespace " << namespaceName << " {\n\n"

	<< "  /**************** needed typedefs **************************/\n";

  for (styleOrder_t::const_iterator i = styleOrder.begin(); 
       i != styleOrder.end(); i++) {

    style_t &style = **i;
    styleExt_t &styleExt = styleExts[*i];

    if (!styleExt.buildsets.size()) continue;

    privH << "  namespace " << style.id << "{\n";

    if (styleExt.needsAtable) {
      privH << "    struct LIS_atable_t {\n";
      for (entrypointList_t::const_iterator 
	     j = styleExt.entrypoints.begin(); 
	   j != styleExt.entrypoints.end(); ++j) 
	{
	  privH << "    " << (*j)->rtype << "(*" << (*j)->id << ")(";
	  if (style.entry.eparms.size()) {
	    privH << style.entry.eparms;
	    if ((*j)->extendedParams.size()) privH << ",";
	  }
	  privH << (*j)->extendedParams << ");\n";
	}
      privH << "    };\n";
      privH << "    extern LIS_atable_t LIS_atable[];\n";
    }

    if (styleExt.needsTtable) {
      privH << "    struct LIS_ttable_entry_t {\n"
	    << "      const char *name;\n"
	    << "      const char *parms;\n"
	    << "      const char *call;\n"
	    << "      const char *textbase;\n"
	    << "      const char *text[" << (instrsOrdered.size()+1) << "];\n"
	    << "    };\n";
	
      privH << "    struct LIS_ttable_t {\n";
      for (entrypointList_t::const_iterator 
	     j = styleExt.entrypoints.begin(); 
	   j != styleExt.entrypoints.end(); ++j) 
	{
	  privH << "      LIS_ttable_entry_t " << (*j)->id << ";\n";
	}
      privH << "    };\n";
      privH << "    extern LIS_ttable_t LIS_ttable;\n";
    }

    privH << "  } // namespace " << style.id << "\n";
  }

  std::set<std::string> foundBase;

  // decode tables
  for (buildsetOrder_t::iterator i = buildsetOrder.begin();
       i != buildsetOrder.end(); i++) {
    buildset_t &bs = buildsets[*i];
    
    if (bs.decoders.size() && 
	foundBase.find(bs.baseclass) == foundBase.end()) {
      privH << "  extern uint64_t LIS_da_" << bs.baseclass << "[];\n";
      foundBase.insert(bs.baseclass);
    }
  }

  privH << "\n/**************** codesection private *********************/\n";
  print_codesection(privH, "private", code, name);
  privH << std::endl
	<< "/**************** GENERATED prototypes? ********************/\n\n"
	<< "/**************** GENERATED accessors **********************/\n\n";
  
  privH << "    namespace LSEemu_accessors {\n";

  for (accessors_t::const_iterator i = accessors.begin();
       i != accessors.end(); i++) {

    privH << "    namespace " << i->first << "{\n";
    
    // declare first
    for (accessor_codes_t::const_iterator j = i->second.codes.begin();
	 j != i->second.codes.end(); j++) {
      const accessor_code_t &acc = j->second;
      
      privH << "      static inline " << acc.rettype << " " 
	    << j->first << "(" << acc.params << ");\n";
    }
    
    // new define
    for (accessor_codes_t::const_iterator j = i->second.codes.begin();
	 j != i->second.codes.end(); j++) {
      const accessor_code_t &acc = j->second;
      
      privH << "\n      static inline " << acc.rettype << " " 
	    << j->first << "(" << acc.defnparams << ")\n" 
	    << "{ // " << acc.loc << std::endl
	    << acc.code << std::endl
	    << "}\n";
    }
    privH << "\n} // namespace " << i->first << "\n";
  }
    
  privH << "  } // namespace LSEemu_accessors\n";

  for (buildsetOrder_t::iterator i = buildsetOrder.begin();
       i != buildsetOrder.end(); i++) {
    buildset_t &bs = buildsets[*i];
    
    // if buildset has no entrypoints/decoders, no reason to generate 
    // accessors.
    if (!bs.decoders.size() && !bs.entrypoints.size()) continue;

    bool hasRollback = false;
    for (capabilities_t::const_iterator 
	   j = bs.capabilities.begin(); j != bs.capabilities.end(); 
	 ++j) {
      if (*j == "speculation") { hasRollback = true; break; }
    }

    privH << "  namespace " << bs.id << " {\n";

    if (hasRollback) privH << "    const bool LIS_dospeculation = true;\n";
    else privH << "    const bool LIS_dospeculation = false;\n";

    generate_options(privH, bs.options);
    privH << std::endl;

    privH << "  } // namespace " << bs.id << std::endl;
  }

  // prologues

  spt << "/**************** codesection prologue  ***********************/\n";
  print_codesection(spt, "prologue", code, name);
  spt << std::endl << std::endl;

  spt << "/**************** codesection support_prologue  ***********************/\n";
  print_codesection(spt, "support_prologue", code, name);
  spt << std::endl << std::endl;

  spt << "/**************** codesection support  ***********************/\n";
  print_codesection(spt, "support", code, name);
  spt << std::endl << std::endl;

  // GENERATE code

  spt << "/**************** GENERATED CODE ******************************/\n";

  // Iterate across the styles.  We use styles as the outermost iterator
  // because each style will go in its own file.  Unfortunately, this does
  // remove the possibility of inlining across styles for now.

  typedef std::map<std::string, std::string> pointEntry_t;
  std::vector<pointEntry_t > splits(instrsOrdered.size()+1);
  std::vector<pointEntry_t > splitsT(instrsOrdered.size()+1);

  foundBase.clear();
  std::map<std::string, std::map<std::string,int> > fieldencs;
  std::ostringstream genH;

  for (styleOrder_t::const_iterator SI = styleOrder.begin(); 
       SI != styleOrder.end(); ++SI) {

    style_t &style = **SI;
    styleExt_t &styleExt = styleExts[*SI];

    if (!styleExt.buildsets.size()) continue;

    std::fstream sfile;
    std::string fname = genStyleName(name, style.id);
    sfile.open(fname.c_str(), std::ios::out);
    mk << " " << fname;
    
    sfile << "/******************************************************\n"
	  << " * Emulator style " << style.id << " file generated by " 
	  << progname << std::endl
	  << " *****************************************************/\n\n"
	  << "#include " << '"' << name << privateHSuffix << '"' 
	  << std::endl;

    sfile << "\n/************ codesection style headers *************/\n";
    print_codesection(sfile, (style.id+"_headers").c_str(), code, name);
    sfile << std::endl << std::endl;

    sfile << "namespace " << namespaceName << " {\n";

    sfile << "/************** codesection prologue  *****************/\n";
    print_codesection(sfile, "prologue", code, name);
    sfile << std::endl << std::endl;

    sfile << "/************* codesection style prologue  ***********/\n";
    print_codesection(sfile, (style.id+"_prologue").c_str(), code, name);
    sfile << std::endl << std::endl;

    std::fstream tfile;
    if (styleExt.needsAtable) {
      std::string tname = genTemplName(name, style.id);
      tfile.open(tname.c_str(), std::ios::out);
      
      sfile << "#include " << '"' << tname << '"'
	    << "\n";

      tfile << "\nnamespace " << style.id << " {";
    }

    // Iterate across the buildsets, building entry points according to the
    // style specifications.  This is actually rather ugly!
  
    for (buildsetOrder_t::iterator i = buildsetOrder.begin();
	 i != buildsetOrder.end(); i++) {
      buildset_t &bs = buildsets[*i];
      buildsetExt_t &bsExt = buildsetExts[&bs];

      // This way I keep the big order rather than depending upon the list in
      // the style being ordered.  I don't know that it matters.
      if (style.id != bs.style) continue;

      GenerateControls gc = GenerateControls(name, bs, buildsetExts[&bs],
					     iclasses, style);

      // restrict list to the instructions in the buildset
      iclass_t &bc = iclasses[bs.baseclass];
      instructionsOrdered_t ilist;
      iclass_set_t &uc = bc.usedby_class;
      for (instructionsOrdered_t::const_iterator 
	     j = instrsOrdered.begin(); j != instrsOrdered.end(); j++) {

	if (!((*j)->canDecode)) continue;

	if (uc.count(*j) || *j == &bc) ilist.push_back(*j);
      }

      sfile << "\n  /***** Buildset " << bs.id << " *******/\n";

      sfile << "/*********** codesection buildset prologue  ***************/\n";
      print_codesection(sfile, (bs.id+"_prologue").c_str(), code, name);
      sfile << std::endl << std::endl;

      // generate the decode functions of the buildset into the private header
      // file (so they can be inlined by others)

      for (decoders_t::iterator 
	     j = bs.decoders.begin(); j != bs.decoders.end(); j++) {
	decoder_t &ep = *j->second;

	std::ostringstream oss, dss;
	std::map<std::string,int> &fieldenc = fieldencs[bs.baseclass];

	if (!foundBase.count(bs.baseclass)) {
	  foundBase.insert(bs.baseclass);
	  sfile << "  uint64_t LIS_da_" << bs.baseclass << "[] = { 0";
	  int currindex = 1;
	  doDecodeArray(sfile, 4, currindex, j->second->decodeTree, fieldenc);
	  sfile << "\n  };\n";
	}

	oss << "\n  inline LSE_emu_decodetoken_t " << ep.id << '(' << ep.params
	    << ")\n  {\n"
	    << "  using namespace " << bs.id << ";\n";
      
	oss << " int LIS_token_tmp=1;\n"
	    << " const uint64_t *LIS_da = &LIS_da_" << bs.baseclass << "[0];\n"; 
      
	if (fieldenc.size()) {

	  dss << "  while (LIS_token_tmp > 0) {\n"
	      << "    uint64_t ntok;\n"
	      << "    if ((ntok = LIS_da[LIS_token_tmp]) & 1) {\n"
	      << "      int ind=0;\n"
	      << "      if (0) {}\n";

	  for (std::map<std::string,int>::const_iterator ii=fieldenc.begin();
	       ii != fieldenc.end(); ++ii) {
	    dss << "      else if (ntok == " << ii->second*2+1 << ")";
	    if (ii->first.size()) 
	      dss << " ind=(instr." << ii->first << " >> " 
		  << "LIS_da[LIS_token_tmp+1]) & "
		  << "LIS_da[LIS_token_tmp+2];\n";
	    else 
	      dss << " ind=(instr >> " 
		  << "LIS_da[LIS_token_tmp+1]) & "
		  << "LIS_da[LIS_token_tmp+2];\n";
	  }

	  dss << "      LIS_token_tmp = LIS_da[LIS_token_tmp+3+ind];\n"
	      << "    } else {\n"
	      << "      bool matched=true;\n"
	      << "      while ((ntok = LIS_da[LIS_token_tmp])) {\n";
	  dss << "        if (0) {}\n";

	  for (std::map<std::string,int>::const_iterator ii=fieldenc.begin();
	       ii != fieldenc.end(); ++ii) {
	    dss << "        else if (ntok==" << ii->second*2 << ")";
	    if (ii->first.size()) 
	      dss << " matched &= ((instr." << ii->first << " & " 
		  << "LIS_da[LIS_token_tmp+1]) == "
		  << "LIS_da[LIS_token_tmp+2]);\n";
	    else 
	      dss << " matched &= ((instr & " 
		  << "LIS_da[LIS_token_tmp+1]) == "
		  << "LIS_da[LIS_token_tmp+2]);\n";
	  }

	  dss << "        LIS_token_tmp += 3;\n"
	      << "      }//while\n"
	      << "      if (matched) LIS_token_tmp = LIS_da[LIS_token_tmp+1];\n"
	      << "      else LIS_token_tmp = LIS_da[LIS_token_tmp+2];\n"
	      << "    }\n" ;
	  dss << "  }\n";

	} else {
	  dss << "  LIS_token_tmp = 0;\n";
	}

	dss   << "  LSE_emu_decodetoken_t LIS_decodetoken LIS_UNUSED = "
	      << "static_cast<LSE_emu_decodetoken_t>(-LIS_token_tmp);\n";

	if (ep.range.size()) {
	  dss << "      switch (LIS_decodetoken) {\n";
	
	  stringMMap_t alreadyDone;

	  for (LIS_isa::instructionsOrdered_t::const_iterator 
		 k = ilist.begin(); k != ilist.end(); k++) {
	    iclass_t &instr = **k;
	    std::ostringstream piece;

	    print_instruction_piece(piece, instr, 
				    ep.afterText(instr, std::string("  "),
						 gc.style.excl), gc);
	  
	    alreadyDone.insert(make_pair(piece.str(),
					 make_decodetoken(instr.id)));

	  } /* for k in instructions */
	    
	  std::string lastOne;
	  stringMMap_t::const_iterator k = alreadyDone.begin();
	  if (k != alreadyDone.end()) { // something there
	    lastOne = k->first;
	    dss << "  case " << k->second << ":\n";
	    ++k;
	    for (;k != alreadyDone.end(); ++k) {
	      if (k->first != lastOne) {
		dss << lastOne;
		dss << "  break;\n";
		lastOne = k->first;
	      }
	      dss << "  case " << k->second << ":\n";
	    }
	    dss << lastOne;
	    dss << "  break;\n";
	  }

	  dss << "      default: break;\n"
	      << "      }\n";
	} /* if (nrb.size()) */
      
	wordSet_t wl = get_word_set(dss.str());

	std::ostringstream oss2;

	generate_fields(oss2, fieldsOrdered, bs.hiddenFields, 
			bsExt.specializedFields, wl);
	oss2 << std::endl;

	generate_operands(oss2, bc.operands, gc, wl, false);
	oss2 << std::endl;
      
	generate_bitfields(oss2, bc.bitfields, bc.matchcommon, gc, wl, 
			   true, false);
	oss2 << std::endl;
      
	oss << substituteII(oss2.str(),"LIS_ii") << dss.str();

	oss << "  return LIS_decodetoken;\n  }\n";

	privH << oss.str();
      }

      // Iterate across entrypoints of the buildset

      for (entrypoints_t::const_iterator j = bs.entrypoints.begin();
	   j != bs.entrypoints.end(); j++) {
	const codepoint_t &ep = *j->second;

	//spt << "\n  /***** Entrypoint " << ep.id << " *******/\n";

	// entry code for each entrypoint

	std::string s = style.entry.code;

	if (style.generators.size()) { // handle generator text

	  genH << (style.entry.rtype.size() ? style.entry.rtype : ep.rtype) 
	       << " LIS_entry_" << ep.id << '(';

	  if (style.entry.eparms.size()) {
	    genH << style.entry.eparms;
	    if (ep.params.size()) genH << ",";
	  }
	  genH << ep.params << ");\n";

	} else if (s.length()) {

	  privH << "\n  " 
		<< ep.rtype << " " << ep.id << '(' << ep.params << ");\n";

	  sfile << "\n" 
		<< ep.rtype << " " << ep.id << '(' << ep.params << ")\n  {\n"
		<< " using namespace " << bs.id << ";\n" 
		<< " using namespace " << style.id << ";\n";

	  do_macros(s, ep, ilist, gc);
	  std::string flds = find_fields(s, fieldsOrdered, bs, bsExt, gc, bc);
	  sfile << substituteII(flds,"LIS_ii") << s << "  } // " 
		<< ep.id << "\n";

	} // no generator

	// Now, create per-instruction templates for the entrypoint if needed.

	if (styleExt.needsAtable) {

	  std::ostringstream nss;
	  nss << "\n  inline " << ep.rtype << " " << ep.id << "%%%N(" ;

	  if (style.entry.eparms.size()) {
	    nss << style.entry.eparms;
	    if (ep.extendedParams.size()) nss << ",";
	  }

	  nss << ep.extendedParams << ") {\n"
	      << "  using namespace " << bs.id << ";\n";
	  nss << "\n%%%FB\n\n";
	  
	  stringMap_t alreadyDone;

	  for (instructionsOrdered_t::iterator k = ilist.begin();
	       k != ilist.end(); k++) {
	    iclass_t &instr = **k;
	    
	    std::ostringstream tmpl, aft, aftc;

	    // code section that allows us to insert text for specializations
	    // based upon bitfields
	    aftc << "{\n";
	    print_codesection(aftc, (style.id + "_after").c_str(), code, name);
	  
	    print_instruction_piece(aft, instr, 
				    aftc.str() +
				    ep.afterText(instr, std::string("  "),
						 gc.style.excl), gc);
	    aft << "}\n";
	    std::string aftstr = aft.str();
	    stringMap_t::const_iterator mi;

	    if ((mi = alreadyDone.find(aftstr)) != alreadyDone.end()) {
	      splits[instr.idno][ep.id] = mi->second;
	      continue;
	    }

	    wordSet_t wl = get_word_set(aftstr);
	  
	    tmpl << nss.str();

	    std::ostringstream tmpl2;

	    generate_fields(tmpl2, fieldsOrdered, bs.hiddenFields, 
			    bsExt.specializedFields, wl);
	    generate_operands(tmpl2, bc.operands, gc, wl, false);
	    generate_bitfields(tmpl2, bc.bitfields, bc.matchcommon, gc, 
			       wl, true, false);
	    
	    tmpl << substituteII(tmpl2.str(),"LIS_ii") << std::endl;
	    tmpl << "\n%%%FE\n\n";
	    tmpl << aftstr;
	    tmpl << "  } /* " << ep.id << "%%%N */\n";
	  
	    // put out function into file
	    
	    std::string s2(tmpl.str());
	    
	    std::string buf("_token_");
	    buf += instr.id;
	    std::string::size_type it;
	    while ( (it = s2.find("%%%N")) != std::string::npos)
	      s2.replace(it, 4, buf);
	    s2.replace(s2.find("%%%FB"),5,"");
	    s2.replace(s2.find("%%%FE"),5,"");

	    tfile << s2; // and put out the template
	    // put out declaration
	    //privH << "\n  " << ep.rtype << " " << ep.id << buf << "("
	    //	  << ep.extendedParams << ");";

	    splits[instr.idno][ep.id] = alreadyDone[aftstr] = ep.id + buf;
	  
	  } // for instructions

	} // if needsAtable

	if (styleExt.needsTtable) {

	  std::ostringstream nss;
	  nss << "{\n"
	      << "  using namespace " << bs.id << ";\n"
	      << "  using namespace " << style.id << ";\n";
	  nss << "\n%%%FB\n\n";
	  
	  for (instructionsOrdered_t::iterator k = ilist.begin();
	       k != ilist.end(); k++) {
	    iclass_t &instr = **k;
	  
	    std::string styletext = style.entry.code;

	    do_macros(styletext, ep, ilist, gc, true);

	    std::ostringstream aft;
	    print_instruction_piece(aft, instr, 
				    ep.afterText(instr, std::string("  "),
						 gc.style.excl), gc);
	  
	    // %%BEFORE() is taken care of by do_macros

	    // %%AFTER()
	    std::string::size_type it;
	    while ( (it = styletext.find("%%AFTER()")) != std::string::npos) {
	      styletext.replace(it, 9, aft.str());
	    }

	    // %%AFTERPTR()
	    if ( styletext.find("%%AFTERPTR()") != std::string::npos) {
	      std::ostringstream call;
	      if (ilist.size() > 0) {
		call << splits[instr.idno][ep.id];
	      }
	      while ( (it = styletext.find("%%AFTERPTR()")) !=
		      std::string::npos) 
		styletext.replace(it, 12, call.str());
	    }

	    std::ostringstream tmpl;
	    std::string &aftstr = styletext;
	    stringMap_t::const_iterator mi;

	    wordSet_t wl = get_word_set(aftstr);
	    std::ostringstream tmpl2;

	    tmpl << nss.str();
	    generate_fields(tmpl2, fieldsOrdered, bs.hiddenFields, 
			    bsExt.specializedFields, wl);
	    generate_operands(tmpl2, bc.operands, gc, wl, false);
	    generate_bitfields(tmpl2, bc.bitfields, bc.matchcommon, gc, 
			       wl, true,  false);

	    tmpl << substituteII(tmpl2.str(),"LIS_ii") << std::endl;
	    tmpl << "\n%%%FE\n\n";
	    tmpl << aftstr;
	    tmpl << "  } /* " << ep.id << "%%%N */\n";
	  
	    // put out function into file
	    
	    std::string s2(tmpl.str());
	    
	    while ( (it = s2.find("%%%N")) != std::string::npos)
	      s2.replace(it, 4, instr.id);
	    s2.replace(s2.find("%%%FB"),5,"");
	    s2.replace(s2.find("%%%FE"),5,"");
	    
	    splitsT[instr.idno][ep.id] = s2;
	  
	  } // for instructions
	
	} // if needsTtable

      } /* for j in entrypoints */
  
      // Generate capabilities as needed

      for (implcaps_t::const_iterator 
	     j = bs.implCapabilities.begin(); j != bs.implCapabilities.end(); 
	   ++j) {

	if (j->first == "operandval") {

	  std::string exparm;
	  if (hasSpeculation) exparm = ", isSpeculative";

	  sfile << "\nvoid EMU_fetch_operand(LSE_emu_instr_info_t *ii,"
		<< "LSE_emu_operand_name_t opname, boolean isSpeculative) {\n"
		<< "  switch (static_cast<int>(opname)) {\n";
	  for (std::set<int>::const_iterator k = oper_used[0].begin();
	       k != oper_used[0].end(); ++k) {
	    sfile << "  case " << *k 
		  << ": return LSE_emu_fetch_operand_" << *k << "(*ii" 
		  << exparm << ");\n";
	  }
	  sfile << "  default: break;\n  }\n}\n\n";

	  sfile << "void EMU_writeback_operand(LSE_emu_instr_info_t *ii,"
		<< "LSE_emu_operand_name_t opname, boolean isSpeculative) {\n"
		<< "  switch (static_cast<int>(opname)) {\n";
	  for (std::set<int>::const_iterator k = oper_used[1].begin();
	       k != oper_used[1].end(); ++k) {
	    sfile << "  case " << *k 
		  << ": return LSE_emu_writeback_operand_" << *k 
		  << "(*ii" << exparm << ");\n";
	  }
	  sfile << "  default: break;\n  }\n}\n\n";
	} // operandval

	else if (j->first == "speculation") {

	  sfile << "int EMU_resolve_operand(LSE_emu_instr_info_t *ii,"
		<< "LSE_emu_operand_name_t opname, int LIS_resolveOp){\n"
		<< "  int i = static_cast<int>(opname);\n"
		<< "  if (!(ii->LIS_operand_backed & (1 << i))) return 0;\n"
		<< "  ii->LIS_operand_backed &= ~(1 << i);\n"
		<< "  switch (i) {\n";
	  for (std::set<int>::const_iterator k = oper_used[1].begin();
	       k != oper_used[1].end(); ++k) {
	    sfile << "  case " << *k 
		  << ": return LSE_emu_resolve_operand_" << *k 
		  << "(*ii, LIS_resolveOp);\n";
	  }
	  sfile << "  default: return 0;\n"
		<< "  }\n"
		<< "}\n\n";

	}

      } // for j in implCapabilities

      sfile << "/*********** codesection buildset epilogue  ***************/\n";
      print_codesection(sfile, (bs.id+"_epilogue").c_str(), code, name);
      sfile << std::endl << std::endl;

    } /* for i in buildsets */

    if (styleExt.needsAtable) {

      tfile << "} // namespace " << style.id << " \n";
      tfile.close();

      sfile << "  namespace " << style.id << "{\n";

      std::ostringstream oss;

      // the trick here is that entrypoints may be valid for different sets
      // of instructions because the buildsets were different.  So we do
      // have to figure out which are valid where...
      
      oss << "LIS_atable_t LIS_atable[] = {\n";

      for (unsigned int i = 0; i < splits.size(); i++) {
	oss << "  {";
	for (entrypointList_t::const_iterator 
	       j = styleExt.entrypoints.begin(); 
	     j != styleExt.entrypoints.end(); j++) {
	  std::string &s = splits[i][(*j)->id];
	  if (s != "") oss << " " << s << ",";
	  else oss << " 0,";
	}
	oss << "},\n";
      }

      oss << "};\n";
      sfile << oss.str();
      sfile << "} // namespace " << style.id << "\n\n";
    }

    if (styleExt.needsTtable) {
      sfile << "  namespace " << style.id << "{\n";

      sfile << "LIS_ttable_t LIS_ttable = {\n"; 

      for (entrypointList_t::const_iterator ie = styleExt.entrypoints.begin(); 
	   ie != styleExt.entrypoints.end(); ++ie) {

	sfile << "    { " << '"' << (*ie)->id << '"' << ','
	    << '"' << (*ie)->extendedParams << '"' << ',' 
	    << '"' << find_pnames((*ie)->extendedParams) << '"' << ", 0, \n"
	    << "      { 0,\n";

	for (unsigned int i = 1; i <= instrsOrdered.size(); i++) {
	  const std::string& s = splitsT[i][(*ie)->id];
	  if (s.length()) {
	    sfile << "\t" << '"' << string_constant_form(s)
		<< '"' << ",\n";
	  }
	  else sfile << "\t\"\",";
	}
	sfile << "      } },\n";
	
      } // for entrypoints
      
      sfile << "};\n";
      sfile << "} // namespace " << style.id << "\n";

    } // needsTtable

    sfile << "/**************** codesection style epilogue  ***************/\n";
    print_codesection(sfile, (style.id+"_epilogue").c_str(), code, name);
    sfile << std::endl << std::endl;

    sfile << "/**************** codesection epilogue  *********************/\n";
    print_codesection(sfile, "epilogue", code, name);
    sfile << std::endl << std::endl;

    sfile << "} /* namespace " << namespaceName << " */" << std::endl;

    sfile.close();
  } /* for SI in styles */

  // Now generate EMU_do_step

  if (steps.size()) {
    spt << "void EMU_do_step(LSE_emu_instr_info_t *LIS_ii,"
	<< "LSE_emu_instrstep_name_t sname, boolean isSpeculative) {\n"
	<< "  switch(sname) {\n";
    
    for (steps_t::const_iterator i = steps.begin(); i != steps.end(); i++) {
      std::string eparms;
      if (buildsets[i->second.bsname].capabilities.count("speculation")) 
	eparms=", isSpeculative";
      spt << "  case LSE_emu_instrstep_name_" << i->second.id << ":\n"
	  << "    LIS_step_" << i->second.id << "(*LIS_ii"
	  << eparms << ");\n"
	  << "    break;\n";
    }
    
    spt << "  default: break;\n"
	<< "  }\n"
	<< "} // EMU_do_step\n";
  }

  // generate opcode/decodetoken name table

  spt << "  const char *const LSE_emu_opcode_names[] = {\n";
  for (std::set<std::string>::const_iterator 
	 i = opcodeNames.begin(); i != opcodeNames.end(); i++) {
    spt << "  \"" << (*i) << "\",\n";
  }
  spt << "  \"LSE_maximum_opcode\" };\n\n";

  spt << "  const char *const LSE_emu_decodetoken_names[] = { \"\",\n";
  for (instructionsOrdered_t::const_iterator 
	 i = instrsOrdered.begin(); i != instrsOrdered.end(); i++) {
    spt << "  \"" << (*i)->id << "\",\n";
  }
  spt << "  \"LSE_maximum_decodetoken\" };\n\n";

  // epilogues

  spt << "/**************** codesection epilogue  ***********************/\n";
  print_codesection(spt, "epilogue", code, name);
  spt << std::endl << std::endl;

  // And we're done 

  pubH << "} /* namespace " << namespaceName << " */" << std::endl
       << "#include " << '"' << dominame << '"' << std::endl
       << "namespace " << namespaceName << "{" << std::endl
       << genH.str() << std::endl;
  print_codesection(pubH, "latepublic", code, name);
  pubH << "} /* namespace " << namespaceName << " */" << std::endl
       << "#endif /* " << pubGuard << " */" << std::endl;
  pubH.close();

  privH << "\n} /* namespace " << namespaceName << " */" << std::endl
    << "#endif /* " << privGuard << " */" << std::endl;
  privH.close();

  spt << "} /* namespace " << namespaceName << " */" << std::endl;
  spt.close();

  mk << std::endl << std::endl;
  mk << "#**************** codesection makefile ***********************\n";
  print_codesection(mk, "makefile", code, name);
  mk << std::endl << std::endl;

  mk.close();
}

namespace {
int errCount = 0;
}

class LIS_isa *LIS_user::theISA; /* this is the global we'll use to parse */

int LIS_user::errorCount() {
  return errCount;
}

void LIS_user::reportError(const char *str, const myloc_t &yylloc) {
  reportError(std::string(str), yylloc);
}

void LIS_user::reportError(const std::string &str, const myloc_t &yylloc) {
  errCount++;
  std::cerr << "error: " << str;
  if (yylloc.filename) std::cerr << " in file " << yylloc.filename;
  std::cerr << " around line " << yylloc.first_line
	    << " column " << yylloc.first_column
	    << std::endl;
}

void LIS_user::reportError(const std::string &str) {
  errCount++;
  std::cerr << "error: " << str << std::endl;
}

void LIS_user::reportError(const char *str) {
  reportError(std::string(str));
}

namespace {
  void reportError(const semantic_error_t serr) {
    std::cerr << "error: " << errToString(serr) << std::endl;
    errCount++;
  }
}

void usage(const char *message, int errorNum, std::ostream &os=std::cerr) {
  if (message) os << message << std::endl;
  os << "Usage: " << progname << " [options] name LISfile..." << std::endl
     << std::endl
     << "  Options are:" << std::endl
     << "  --dheader=<file>        Domain instance header name\n"
     << "  --namespace=<name>      Namespace for generated code\n"
     << "  --gamma=#               Set decoding table size tradeoff factor\n"
     << "  --[no]dump              Dump the ISA\n"
     << "  --[no]gen               Generate simulator files (default=gen)\n"
     << "  --[no]showset           Show set operations\n"
     << "  --[no]showdecoding      Show decoder generation operations\n"
     << "  --[no]buildable         Should be a buildable emulator\n"
     << "  -I <path>               Add <path> to the include search path\n"
    ;
  exit(errorNum);
  
}

int main(int argc, char *argv[], char **envp) {
  std::ios::sync_with_stdio();
  int optind;
  progname = argv[0];
  bool dodump=false;
  bool skipgen=false;
  bool buildable=false;

  // put current directory into path
  for (int size=256;true;size*=2) {
    char *n = new char[size], *s;
    s = getcwd(n,size);
    if (s) {
      includePaths.push_back(s);
      delete[] n;
      break;
    } else if (errno != ERANGE) {
      std::cerr << "You're on a 2.6.30 kernel, aren't you?  "
		<< "Let's try something...\n";
      includePaths.push_back(".");
      delete[] n;
      break;
    }
    delete[] n;
  }

  for (optind = 1; optind < argc; optind++) {
    char *optarg = argv[optind];
    if (optarg[0]=='-') {
      switch (optarg[1]) {
      case 0: goto donewithargs; // first argument is -
      case 'h': usage(NULL,0,std::cout);
      case '-': /* double-argument */
	if (!strcmp(optarg,"--")) {
	  optind++; 
	  goto donewithargs; // -- argument deleted
	}
	if (!strcmp(optarg,"--help")) usage(NULL,0,std::cout);
	else if (!strncmp(optarg,"--dheader=",10)) {
	  dominame = std::string(optarg+10);
	}
	else if (!strncmp(optarg,"--gamma=",8)) {
	  gammaval = atof(optarg+8);
	}
	else if (!strcmp(optarg,"--dheader")) {
	  optind++;
	  if (optind == argc) usage("Missing argument text",1);
	  optarg = argv[optind];
	  dominame = std::string(optarg);
	}
	else if (!strncmp(optarg,"--namespace=",12)) {
	  namespaceName = std::string(optarg+12);
	}
	else if (!strcmp(optarg,"--dump")) dodump=true;
	else if (!strcmp(optarg,"--nodump")) dodump=false;
	else if (!strcmp(optarg,"--showset")) SHOW_SETOPS=true;
	else if (!strcmp(optarg,"--noshowset")) SHOW_SETOPS=false;
	else if (!strcmp(optarg,"--showdecoding")) SHOW_DECODING=true;
	else if (!strcmp(optarg,"--noshowdecoding")) SHOW_DECODING=false;
	else if (!strcmp(optarg,"--gen")) skipgen=false;
	else if (!strcmp(optarg,"--nogen")) skipgen=true;
	else if (!strcmp(optarg,"--buildable")) buildable=true;
	else if (!strcmp(optarg,"--nobuildable")) buildable=false;
	else if (!strcmp(optarg,"-I")) {
	  ++optind;
	  if (optind == argc) usage("Missing argument text",1);
	  optarg = argv[optind];
	  if (optarg[0] == '/') includePaths.push_back(normalizePath(optarg));
	  else includePaths.push_back(normalizePath(includePaths.front() 
						    + "/" + optarg));
	}
	else usage("Unknown argument",1);
	break;
      default:
	usage("Unknown argument",1);
      }
    } else goto donewithargs;
  }
 donewithargs:

  if (argc - optind < 2) 
    usage("Must specify an emulator name and at least one LIS file",1);

  std::string isaName(argv[optind]);

  if (!namespaceName.size()) namespaceName = isaName;

  for (int i = optind+1; i < argc; i++) {
    fileList.push_back(argv[i]);
  }

  class LIS_sim myISA;
  myISA.isBuildable = buildable;

  /* now read the file */
  LIS_user::theISA = &myISA;
  LIS_user::yynextfile();
  yyparse();
  
  myISA.completeISA();

  if (dodump) {
    myISA.dumpISA(std::cout);
  }

  if (errCount) {
    std::cerr << "Aborting due to errors in specification; " << errCount
	      << " errors found\n";
    exit(1);
  }

  if (!skipgen) {
    myISA.figureOut();
    /* generate files: description, code */
    if (myISA.isLSEemu) {
      myISA.createDSC(isaName);
    }
    myISA.createCode(isaName);
  }
  for (std::list<char *>::iterator 
	 i = fnameList.begin(); i != fnameList.end(); i++) 
    free(*i);
  return 0;
}
