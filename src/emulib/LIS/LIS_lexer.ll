/*
 * Copyright (c) 2006-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * Tokenizer for LIS
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file contains the tokenizer definition for LIS
 * YLLTYPE has been redefined to allow filenames as well.
 *
 * Note that I have tried not to use flex extensions.  This means that each
 * rule must call da() in order to properly track the token locations.
 *
 * I would have liked to wrap the lexer in a namespace, but unfortunately
 * g++ then starts spitting out warnings about static functions which are 
 * not used, and since I try to compile with -Wall -Werror....
 *
 */

%pointer 

%{
#include <stdlib.h>
#include <iostream>
#include <stack>
#include "LIS_parsedefs.h"
// must go after LIS_parsedefs.h becuase YYLTYPE is defined
#include "LIS_parser.h"

extern YYLTYPE yylloc;

using namespace LIS;
using namespace LIS_user;
using namespace LIS_parsedefs;

namespace {

  YYLTYPE save_loc;

  int get_number(char* text)
  {
    if((text[1] != 'b' && text[1] != 'B')) return strtoll(text,NULL,0);
    text+=2;
    return (strtoll(text,NULL,2));
  }
  
  void da(void) 
  {
    yylloc.first_line = yylloc.last_line; 
    yylloc.first_column = yylloc.last_column; 
    yylloc.last_column += yyleng; 
  }
  
  int codenestcount = 0;
  char thecode;
   
  std::stack<YY_BUFFER_STATE> include_stack;
  std::stack<YYLTYPE> include_loc_stack;
   
} // anonymous namespace

%}

%x TRIPLE
%x TRIPLEEND
%x COMMENT
%x CXXCOMMENT
%x LINEDIR
%x INCODE
%x INCODEEND
%x INCODECOMMENT
%x INCODECXXCOMMENT
%x INCLUDE

IDENT           [A-Za-z_][A-Za-z0-9_]*

CDECINT         [1-9][0-9_]*
CBININT         0[bB][01_]*
COCTINT         0[0-7_]*
CHEXINT         0[xX][0-9a-fA-F_]*
CINT            {CDECINT}|{CBININT}|{COCTINT}|{CHEXINT}

%%

<INITIAL>"accessor"      { da(); return ACCESSOR; }
<INITIAL>"action"        { da(); return ACTION; }
<INITIAL>"buildset"      { da(); return BUILDSET; }
<INITIAL>"capability"    { da(); return CAPABILITY; }
<INITIAL>"classes"       { da(); return CLASSES; }
<INITIAL>"codesection"   { da(); return CODESECTION; }
<INITIAL>"constant"      { da(); return CONSTANT; }
<INITIAL>"cross"         { da(); return CROSS; }
<INITIAL>"decoder"       { da(); return DECODER; }
<INITIAL>"elseif"        { da(); return ELSEIF; }
<INITIAL>"else"          { da(); return ELSE; }
<INITIAL>"entrypoint"    { da(); return ENTRYPOINT; }
<INITIAL>"entry"         { da(); return ENTRY; }
<INITIAL>"enumvalue"     { da(); return ENUMVALUE; }
<INITIAL>"exclude"       { da(); return EXCLUDE; }
<INITIAL>"field"         { da(); return FIELD; }
<INITIAL>"format"        { da(); return FORMAT; }
<INITIAL>"frequency"     { da(); return FREQUENCY; }
<INITIAL>"generator"     { da(); return GENERATOR; }
<INITIAL>"hide"          { da(); return HIDE; }
<INITIAL>"if"            { da(); return IF; }
<INITIAL>"implement"     { da(); return IMPLEMENT; }
<INITIAL>"instrclasslist"  { da(); return INSTRCLASSLIST; }
<INITIAL>"instructionlist"  { da(); return INSTRUCTIONLIST; }
<INITIAL>"instrclass"    { da(); return INSTRCLASS; }
<INITIAL>"instruction"   { da(); return INSTRUCTION; }
<INITIAL>"match"         { da(); return MATCH; }
<INITIAL>"opcode"        { da(); return OPCODE; }
<INITIAL>"operand"       { da(); return OPERAND; }
<INITIAL>"operandname"   { da(); return OPERANDNAME; }
<INITIAL>"option"        { da(); return OPTION; }
<INITIAL>"predecode"     { da(); return PREDECODE; }
<INITIAL>"show"          { da(); return SHOW; }
<INITIAL>"specialize"    { da(); return SPECIALIZE; }
<INITIAL>"step"          { da(); return STEP; }
<INITIAL>"structfield"   { da(); return STRUCTFIELD; }
<INITIAL>"style"         { da(); return STYLE; }
<INITIAL>"template"      { da(); return TEMPLATE; }
<INITIAL>"typedef"       { da(); return TYPEDEF; }
<INITIAL>"?="      { da(); return MAYASSIGN; }
<INITIAL>">="      { da(); return GE; }
<INITIAL>"<="      { da(); return LE; }
<INITIAL>"=="      { da(); return EQ; }
<INITIAL>"!="      { da(); return NE; }
<INITIAL>"<<"      { da(); return SSHL; }
<INITIAL>">>"      { da(); return SSHR; }
<INITIAL>"&&"      { da(); return SLAND; }
<INITIAL>"||"      { da(); return SLOR; }

<INITIAL>"include" { da(); BEGIN(INCLUDE); }

<INITIAL>{CINT}    { da(); yylval.cint = get_number(yytext); return CINT; }
<INITIAL>{IDENT}   { da(); yylval.str = strdup(yytext); return IDENT; }
<INITIAL>"<<<"     { da(); BEGIN(TRIPLE); save_loc = yylloc; }
<INITIAL>"/*"      { da(); BEGIN(COMMENT); save_loc = yylloc; }
<INITIAL>"//"      { da(); BEGIN(CXXCOMMENT); }
<INITIAL>^#line[ \t]  { da(); BEGIN(LINEDIR); }
<INITIAL>[ \t]+    { da(); /* eat whitespace */ }
<INITIAL>\n        { da(); ++yylloc.last_line; yylloc.last_column = 1; }
<INITIAL>.         { da(); return yytext[0]; }

<LINEDIR>{CDECINT}   { da(); yylloc.last_line = get_number(yytext); 
                        yylloc.last_column = 1;}
<LINEDIR>\"[^\"]+\"  { da(); yylloc.filename = strdup(yytext); }
<LINEDIR>[ \t]+     { da(); /* eat whitespace */ }
<LINEDIR>\n         { da(); yylloc.last_column = 1; BEGIN(INITIAL); }
<LINEDIR>.          { da(); /* eat remaining characters */ }

<TRIPLE>">>>"    { da();  yyless(yyleng-3);
                   yylval.str = strdup(yytext);
	           yylloc = save_loc;
	           yylloc.last_column += 3;
                   BEGIN(TRIPLEEND);
                   return CODE;
                 }
<TRIPLE>\n      { da(); yymore(); 
	           ++save_loc.last_line; save_loc.last_column = 1;}
<TRIPLE>.       { da(); save_loc.last_column++; yymore(); }
<TRIPLEEND>">>>"    { da(); yylloc.first_column -= 3;
	              yylloc.last_column -= 3; // due to yyless
	              BEGIN(INITIAL); }

<COMMENT>[^*\n]*       { da(); yylloc.last_column += yyleng; }
<COMMENT>"*"+"/"       { da(); BEGIN(INITIAL); yylloc.last_column+=yyleng; }
<COMMENT>"*"+[^*/\n]*  { da(); yylloc.last_column += yyleng; }
<COMMENT>\n            { da(); ++yylloc.last_line; yylloc.last_column = 1; }
<CXXCOMMENT>\n         { da(); ++yylloc.last_line; yylloc.last_column = 1; 
                         BEGIN(INITIAL); }
<CXXCOMMENT>.          { da(); yylloc.last_column++; }

<INCODE>"}"  { da(); 
               if (thecode=='}') {
		 if (codenestcount == 0) {
		   yyless(yyleng-1);
		   yylval.str = strdup(yytext);
		   yylloc = save_loc;
		   BEGIN(INCODEEND);
		   return CODE;
		 } else {
		   yymore();
		   save_loc.last_column++;
		   codenestcount--;
		 }
	       } else { yymore(); save_loc.last_column++; }
             }
<INCODE>")"  { da(); 
               if (thecode==')') {
		 if (codenestcount == 0) {
		   yyless(yyleng-1);
		   yylval.str = strdup(yytext);
		   yylloc = save_loc;
		   BEGIN(INCODEEND);
		   return CODE;
		 } else {
		   yymore();
		   save_loc.last_column++;
		   codenestcount--;
		 }
	       } else { yymore(); save_loc.last_column++; }
             }
<INCODE>";"  { da(); 
               if (thecode==';') {
		 yyless(yyleng-1);
		 yylval.str = strdup(yytext);
		 yylloc = save_loc;
		 BEGIN(INCODEEND);
		 return CODE;
	       } else { yymore(); save_loc.last_column++; }
             }
<INCODE>"{"  { da(); yymore(); save_loc.last_column++;
	       if (thecode=='}') codenestcount++;
	     }
<INCODE>"("  { da(); yymore(); save_loc.last_column++;
	       if (thecode==')') codenestcount++;
	     }
<INCODE>"/*"  { da(); BEGIN(INCODECOMMENT); yymore(); 
                save_loc.last_column += 2; }
<INCODE>"//"  { da(); BEGIN(INCODECXXCOMMENT); yymore(); 
                save_loc.last_column += 2; }
<INCODE>\n    { da(); yymore(); 
                 ++save_loc.last_line; save_loc.last_column = 1;}
<INCODE>.     { da(); save_loc.last_column++; yymore(); }
<INCODEEND>"}"	{ da(); BEGIN(INITIAL); return '}'; }
<INCODEEND>")"	{ da(); BEGIN(INITIAL); return ')'; }
<INCODEEND>";"	{ da(); BEGIN(INITIAL); return ';'; }

<INCODECOMMENT>[^*\n]*       { da(); yymore(); 
                               save_loc.last_column += yyleng; }
<INCODECOMMENT>"*"+[^*/\n]*  { da(); yymore(); 
                               save_loc.last_column += yyleng; }
<INCODECOMMENT>\n            { da(); yymore(); 
                              ++save_loc.last_line; save_loc.last_column = 1; }
<INCODECOMMENT>"*"+"/"       { da(); yymore(); 
                               BEGIN(INCODE); save_loc.last_column+=2; }
<INCODECXXCOMMENT>\n         { da(); yymore(); 
                               ++save_loc.last_line; save_loc.last_column = 1; 
		               BEGIN(INCODE); 
	                     }
<INCODECXXCOMMENT>.          { da(); yymore(); ++save_loc.last_column; }

<INCLUDE>[ \t]*     { da(); /* eat whitespace */ }
<INCLUDE>[^ \t\n]+  { da(); /* filename */
                      yylloc.last_column = 1; 
		      //++yylloc.last_line;
		      if (!LIS_internal::suppressed) {
                        include_stack.push(YY_CURRENT_BUFFER);
		        include_loc_stack.push(yylloc);
		        yynewfile(yytext);
		        yy_switch_to_buffer(yy_create_buffer(yyin, 
							     YY_BUF_SIZE));
		      }
		      BEGIN(INITIAL);
                    }

%%

void LIS_internal::startcode(char lastcode) {
   BEGIN(INCODE);
   thecode = lastcode;
   codenestcount = 0;
   save_loc = yylloc;
}

int yywrap() {
  switch (YY_START) {
  case INITIAL:
  case LINEDIR:
  case CXXCOMMENT:
    if (!include_stack.empty()) {
      yyendfile(); // must do this before yyin changes
      /* return to previous file */
      yy_delete_buffer(YY_CURRENT_BUFFER);
      yy_switch_to_buffer(include_stack.top());
      yylloc = include_loc_stack.top();
      LIS_internal::suppressed = false;	
      include_stack.pop();
      include_loc_stack.pop();
      return 0;
    } else {
      yyendfile();
      if (yynextfile()) return 0; // yynextfile moved us along to next file
      return 1;
    }
    break;
  case TRIPLE:
  case COMMENT:
  case INCODECOMMENT:
  case INCODECXXCOMMENT:
  case INCODE:
    std::cerr << "Unexpected EOF at line " << save_loc.last_line 
	      << " of " << yylloc.filename << "; started element at line "
	      << save_loc.first_line << std::endl;
    exit(3);
    break;
  default:
    yyunput(0,0); // this is to make gcc warning go away
    break;
  }
  return 1;
}

