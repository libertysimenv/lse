/*
 * Copyright (c) 2006-2007 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * Main ISA class methods
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file has the class methods which build the ISA description.  It
 * is up to subclasses to do something interesting with the description.
 *
 *********************** decoding stuff ********************************
 *
 * This is based upon the paper "Automated Synthesis of Efficient Binary
 * Decoders for Retargetable Software Toolkits", 
 * by Wei Qin and Sharad Malik, DAC 2003, June 2003.
 *
 * The new thing here is that we have patterns that overlap each other
 * in a nested fashion; the idea is that the more specific pattern overrides
 * the more general pattern.  Thus we need to compute set differences on
 * the nested patterns before starting the decoder tree algorithm from Qin
 * and Malik.
 *
 */
#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <queue>
#include <sstream>
#include "LIS_isa.h"
#define SET_DIFFS // use the set-difference method
#define COMBINE_INSTRUCTION

using namespace LIS;
using namespace LIS_decoding_support;

namespace LIS_decoding_support {
  bool SHOW_SETOPS = false;
  bool SHOW_DECODING = false;
  double gammaval = 0.125;
  onebitdiffs_t onebitdiffs;

  void init_helpers(void) {
    uint64_t mask = 1;
    for (int i=0; i<64 ; ++i, mask <<= 1) {
      onebitdiffs[mask] = i;
    }
  }

}

namespace { // everything here is static!

  /**************** support initialization ***************************/

  typedef std::pair<iclass_t *, iclass_t *> overlap_t;
  typedef std::list<overlap_t> overlaps_t; 

  typedef struct decode_entry_t {
    matchinfo_t p;
    iclass_t *l;
    double lambda;
  } decode_entry_t;
  typedef std::vector<decode_entry_t> dlist_t;
  typedef std::vector<dlist_t> dlists_t;

  /*********************** set helper routines ****************************/

  // NOTE: relies upon having normalized matchInfo!
  inline bool 
  isStrictSubpattern(matchinfo_t &m1, matchinfo_t &m2) 
  {
    bool somediff = false;
    for (matchinfo_t::iterator 
	   i = m1.begin(), j=m2.begin(); i != m1.end(); ++i, ++j) {
      matchval_t &mv1 = i->second;
      matchval_t &mv2 = j->second;
      if (~mv1.mask & mv2.mask) return false; // m1 not defined; m2 defined
      if (mv1.mask != mv2.mask) somediff = true;
      if ((mv1.val ^ mv2.val) & mv2.mask) return false;
    }
    return somediff;
  }

  inline bool 
  isEqual(matchinfo_t &m1, matchinfo_t &m2) 
  {
    for (matchinfo_t::iterator 
	   i = m1.begin(), j=m2.begin(); i != m1.end(); ++i, ++j) {
      matchval_t &mv1 = i->second;
      matchval_t &mv2 = j->second;
      if (mv1.mask != mv2.mask) return false; // m1 not defined; m2 defined
      if ((mv1.val ^ mv2.val) & mv2.mask) return false;
    }
    return true;
  }

  bool operator ==(const matchinfo_t &m1, const matchinfo_t &m2) {
    for (matchinfo_t::const_iterator 
	   i = m1.begin(), j=m2.begin(); i != m1.end(); ++i, ++j) {
      const matchval_t &mv1 = i->second;
      const matchval_t &mv2 = j->second;
      if (mv1.mask != mv2.mask) return false;
      if ((mv1.val ^ mv2.val) & mv2.mask) return false;
    }
    return true;
  }

  // Calculate A/B for patterns.  B must be a subpattern of A!
  inline void
  form_difference_sets(const matchinfo_t &A, 
		       const matchinfo_t &B,
		       matchSet_t &Ys) {

    matchinfo_t cumpat = A;

    // the idea is to iterate across the bits which are more defined in
    // the subset.  As each bit is found it is set to the opposite of
    // the value in the subset and the pattern is added to the list.  Then
    // the bit is returned to the proper value.
    for (matchinfo_t::const_iterator 
	   i = A.begin(), j = B.begin(); i != A.end(); ++i, ++j) {
      const matchval_t &Av = i->second;
      const matchval_t &Bv = j->second;

      uint64_t thebits = ~Av.mask & Bv.mask;
      uint64_t somebits = thebits;
      uint64_t lmask = static_cast<uint64_t>(1);
      for (int k = 0; 
	   k < 64 && somebits; 
	   ++k, somebits >>=1, lmask <<=1) {
	if (!(thebits & lmask)) continue;
	cumpat[i->first].mask |= lmask;
	cumpat[i->first].val |= lmask & ~Bv.val; // flipped bit
	Ys.push_back(cumpat);
	cumpat[i->first].val ^= lmask;  // and put bit back
      }
    }
  }

  // Calculate Y = A&B, returning false if empty set.  Note that this is not
  // a boolean and: it is a set intersection
  inline bool 
  do_pattern_and(const matchinfo_t &A, const matchinfo_t &B,
		 matchinfo_t &Y)
  {
    for (matchinfo_t::const_iterator 
	   i = A.begin(), j = B.begin(); i != A.end(); ++i, ++j) {
      const matchval_t &Av = i->second;
      const matchval_t &Bv = j->second;
      matchval_t &Yv = Y[j->first];
      uint64_t tmask = Av.mask & Bv.mask;
      if ((Av.val ^ Bv.val) & tmask) return false;
      Yv.mask = Av.mask | Bv.mask;
      Yv.val = (Av.val & Av.mask) | (Bv.val & Bv.mask);
    }
    return true;
  }

  int 
  count_difference_bits(const matchinfo_t &A, const matchinfo_t &B) {
    int count = 0;

    for (matchinfo_t::const_iterator 
	   i = A.begin(), j = B.begin(); i != A.end(); ++i, ++j) {
      const matchval_t &Av = i->second;
      const matchval_t &Bv = j->second;

      uint64_t thebits = ~Av.mask & Bv.mask;
      uint64_t somebits = thebits;
      uint64_t lmask = static_cast<uint64_t>(1);
      for (int k = 0; 
	   k < 64 && somebits; 
	   ++k, somebits >>=1, lmask <<=1) {
	if (!(thebits & lmask)) continue;
	count++;
      }
    }
    return count;
  } // count_difference_bits()

  int 
  count_difference_bits(const matchinfo_t &B) {
    int count = 0;

    for (matchinfo_t::const_iterator i = B.begin(); i != B.end(); ++i) {
      const matchval_t &Bv = i->second;

      uint64_t thebits = Bv.mask;
      uint64_t somebits = thebits;
      uint64_t lmask = static_cast<uint64_t>(1);
      for (int k = 0; 
	   k < 64 && somebits; 
	   ++k, somebits >>=1, lmask <<=1) {
	if (!(thebits & lmask)) continue;
	count++;
      }
    }
    return count;
  } // count_difference_bits()

  // make sure matchset entries are disjoint
  void make_disjoint(matchSet_t& ms) {
    if (ms.size() < 2) return; // nothing to do
    
    matchSet_t nl;

    while (ms.size()) {
      matchinfo_t &a = ms.front();

      for (matchSet_t::iterator j = nl.begin(); j != nl.end(); ++j) {
	matchinfo_t intersect;
	if (do_pattern_and(a,*j,intersect)) { // overlap present
	  matchSet_t diffs;

	  form_difference_sets(a,intersect,diffs);
	  std::copy(diffs.begin(),diffs.end(),std::back_inserter(ms));
	  goto end_loop;  // need to jump out of the for loop and not push
	}
      }

      nl.push_back(a); // pattern passed
    end_loop:
      ms.pop_front();
    }
    ms = nl;
    return;
  } // make_disjoint

  // assumes disjoint matchset entries...
  std::list<double> normalize_frequencies(const matchSet_t& ms) {
    std::list<int> diffs;
    int maxdiff = 0;
    for (matchSet_t::const_iterator i = ms.begin(); i != ms.end(); ++i) {
      int newdiff = count_difference_bits(*i);
      maxdiff = std::max(maxdiff,newdiff);
      diffs.push_back(newdiff);
    }
    std::list<double> nv;
    double totallambda = 0.0;
    for (std::list<int>::const_iterator 
	   i = diffs.begin(); i != diffs.end(); ++i) {
      double lambda = std::pow(2.0, maxdiff - (*i));
      nv.push_back(lambda);
      totallambda += lambda;
    }
    for (std::list<double>::iterator i = nv.begin(); i != nv.end(); ++i)
      *i /= totallambda;
    return nv;
  } // normalize_frequencies

  /********************* huffman cost **********************/

  typedef struct huffman_t {
    double lambda;
    int depth;
  } huffman_t;

  class Compare_lambda { public:
    bool operator()(huffman_t const &e1, 
		    huffman_t const &e2) const
    { return e1.lambda > e2.lambda; }
  };

  // The real size function actually tries to calculate how many unique
  // instructions there are in the list of patterns.  This is primarily
  // an issue because of hierarchical relationships between instructions.

  double inline real_size(const dlist_t &dl) {
    
    std::set<iclass_t *> icm;
    for (dlist_t::const_iterator i = dl.begin(); i != dl.end(); ++i) {
      icm.insert(i->l);
    }
    return static_cast<double>(icm.size());
  }

  double huffman_cost(const dlist_t &dl) {

    if (!dl.size()) return 0;

    std::priority_queue<huffman_t, std::vector<huffman_t>, Compare_lambda> nl;
    // create the huffman heap and add tlambda.  But default patterns are
    // being a pain.  So, I'm going to help things along by merging
    // elements with the same final instruction
    
    double tlambda = 0.0;

    if (0 && SHOW_DECODING) {
      std::cerr << "  Huffman cost of:\n";

      for (dlist_t::const_iterator i = dl.begin(); i != dl.end(); ++i) 
	std::cerr << "\t" << i->p << " " << i->lambda << " " 
		  << (i->l ? i->l->id : "<none>")
		  << std::endl;
    }

#ifdef COMBINE_INSTRUCTION
    std::map<iclass_t *, double> icm;
    for (dlist_t::const_iterator i = dl.begin(); i != dl.end(); ++i) {
      icm[i->l] += i->lambda;
      tlambda += i->lambda;
    }
    for (std::map<iclass_t *, double>::const_iterator 
	   i = icm.begin(); i != icm.end(); ++i) {
      huffman_t h = { i->second, 0 };
      nl.push(h);
    }
#else
    for (dlist_t::const_iterator i = dl.begin(); i != dl.end(); ++i) {
      tlambda += i->lambda;
      huffman_t h = { i->lambda, 0 };
      nl.push(h);
    }
#endif

    // we really don't care about results for uninteresting instructions
    if (!nl.size()) return 0.0; 

    while (nl.size()>1) {
      huffman_t h1 = nl.top();
      nl.pop();
      huffman_t h2 = nl.top();
      nl.pop();

      huffman_t h = { h1.lambda + h2.lambda, 
		      std::max(h1.depth,h2.depth)+1};
      nl.push(h);
    }

    huffman_t h = nl.top();
    if (0 && SHOW_DECODING) {
      std::cerr << "\tis " << ((h.depth+1)*tlambda) << "\n";
    }
    return (h.depth+1) * tlambda;
  }

  /******************** apply decoding function to patterns ***************/

  // purpose of this little function is to make sure duplicate entries
  // due to overlapping go away eventually while splitting
  inline void add_decode_entry(dlist_t &dl, decode_entry_t &ntv) {
    for (dlist_t::iterator i = dl.begin(); i != dl.end(); ++i) {
      if (i->p == ntv.p) return;
    }
    dl.push_back(ntv);
  }

  dlists_t *split_list(dlist_t &dlist,
		       const decisionfunc_t *df,
		       double *costp)
  {
    dlists_t *nl = new dlists_t;
    unsigned int numbranch 
      = (df->kind == decisionfunc_t::dec_pattern ? 2 :
	 (1<<df->table.length));
  
    nl->resize(numbranch);

    double mr;
    bool didsomething=false;
    unsigned int truecnt = 0, falsecnt = 0;

    if (SHOW_DECODING) {
      std::cerr << " Trying: ";
      showDecode(std::cerr, 0, df, false); // children aren't valid yet
    }

    if (df->kind == decisionfunc_t::dec_pattern) {

      std::map<iclass_t *,int> sides;

      for (dlist_t::iterator i = dlist.begin(); i != dlist.end(); ++i) {

	// determine whether it matches.  We may need to split
	// entries when the decode entry doesn't care about a bit in the pattern.
	// Func:    x  x  x  0  0  0  1  1  1
	// Pattern: x  0  1  x  0  1  x  0  1
	// Result:  T  T  T  B  T  F  B  T  T

	bool matchfalse = false;
	bool matchpart = false;
	for (matchinfo_t::const_iterator j = df->pattern.begin();
	     j != df->pattern.end(); ++j) {
	  const matchval_t &ip = i->p[j->first];
	  const matchval_t &dp = j->second;
	
	  matchpart |= ((ip.mask & dp.mask) != dp.mask);
	  matchfalse |= ((ip.val & dp.mask) != (dp.val & ip.mask));
	}
	if (matchfalse) { // overall match to false side
	  add_decode_entry((*nl)[0],*i);
	  sides[i->l] |= 1;
	  falsecnt++;
	} else if (!matchpart) { // match to true side
	  add_decode_entry((*nl)[1],*i);
	  sides[i->l] |= 2;
	} else { // match to both.
	  sides[i->l] |= 3;
	  truecnt++;
	  falsecnt++;

	  // because it matches to both, we know that instruction is a
	  // proper superpattern, so we can use set intersection and
	  // difference to do the trick here. 
	  //
	  // NOTE: the lambda calculation relies on the form of difference sets

	  matchinfo_t trueside;
	  matchSet_t falseside;

	  do_pattern_and(i->p, df->pattern, trueside);
	  form_difference_sets(i->p, df->pattern, falseside);
	  int numbits = falseside.size(); // number of sets is number of bits

	  decode_entry_t ntv = { trueside, i->l, 
				 i->lambda / 
				 (static_cast<uint64_t>(1)<<numbits)
	  };
	  add_decode_entry((*nl)[1],ntv);

	  int l=1;
	  for (matchSet_t::const_iterator 
		 k = falseside.begin(); k != falseside.end(); ++k, l++) {
	    decode_entry_t nfv = { *k, i->l, 
				   i->lambda / (static_cast<uint64_t>(1)<<l) };
	    add_decode_entry((*nl)[0],nfv);
	  }

	} /* else match to both */
      } /* for dlist */

      // actually split if some actual class (not just a pattern)
      // went wholly down the true side and we have something on both sides.
      // Note that we want the asymmetry here because the true side is no 
      // larger than the original number of patterns, so we want to favor it.
      for (std::map<iclass_t *,int>::const_iterator 
		 i = sides.begin(); i != sides.end(); ++i) {
	if (i->second == 2) {
	  didsomething = true;
	  break;
	}
      }
      // I think the previous logic was unsafe for multi-pattern instructions
      didsomething=true;
      dlist_t::size_type s0 = (*nl)[0].size();
      dlist_t::size_type s1 = (*nl)[1].size();
      if (!s0 || !s1) didsomething=false;
      if (truecnt == dlist.size() && falsecnt == truecnt) didsomething=false;

      mr =(  (real_size((*nl)[0])*1.01 + real_size((*nl)[1]) - 1) 
	     / (real_size(dlist) - 1)  );

    } else {

      for (dlist_t::iterator i = dlist.begin(); i != dlist.end(); ++i) {

	// figure out on what branches it matches.  We may need to split
	// entries when the decode entry doesn't care about a bit in the 
	// pattern.
	// Func:    x  x  x  0  0  0  1  1  1
	// Pattern: x  0  1  x  0  1  x  0  1
	// Result:  T  T  T  B  T  F  B  T  T


	const matchval_t &ip = i->p[df->table.field];
	uint64_t mask0 = ((static_cast<uint64_t>(1)<<df->table.length)-1);
	uint64_t mask = mask0 << df->table.from;

	if ((mask & ip.mask) == mask) { // all bits there 

	  add_decode_entry((*nl)[(ip.val >> df->table.from) & mask0], *i);

	} else { // must split them among the table

	  uint64_t ival = ip.val >> df->table.from;
	  uint64_t imask = ip.mask >> df->table.from;
	  unsigned int didcnt = 0;

	  for (unsigned int j=0; j < numbranch; ++j) {

	    if ((ival & mask0) == (j & imask)) { // if pattern matches

	      decode_entry_t nd = { i->p, i->l, i->lambda };
	    
	      nd.p[df->table.field].mask |= mask;
	      nd.p[df->table.field].val 
		|= (static_cast<uint64_t>(j)<<df->table.from);
	    
	      add_decode_entry((*nl)[j],nd);
	      didcnt ++;
	    }
	  }
	  // and fix the lambda by doing this again!
	  for (unsigned int j=0; j < numbranch; ++j) {
	    if ((ival & mask0) == (j & imask)) 
	      (*nl)[j].back().lambda /= didcnt;
	  }
	} /* else */
      } /* for dlist */

      mr = 1 + numbranch;
      int cntgood = 0;
      for (dlists_t::const_iterator i = nl->begin(); i != nl->end(); ++i)
	if (i->size()) {
	  mr += i->size()-1;
	  if (i->size() < dlist.size()) cntgood++;
	}
      mr /= real_size(dlist) - 1;
      // split that didn't split!
      if (cntgood < 1) didsomething = false;
      else didsomething = true;
    } /* table */

    if (!didsomething) {
      // if we didn't make any progress at all at splitting the space, then
      // don't accept this division.  Should prevent us from infinite loops
      // when the user has messed up
      *costp = std::numeric_limits<double>::max();
    } else {
      // Now, what is the cost function?
      double cost = 1.0;
      for (dlists_t::iterator i = nl->begin(); i != nl->end(); ++i)
	cost += huffman_cost(*i);
      cost += gammaval * std::log(mr) / std::log(2.0);
      if (SHOW_DECODING) 
	std::cerr << " mr = " << mr 
		  << "\n Cost for attempted split:" << cost << "\n";
      *costp = cost;
    }

    return nl;
  }

  /*********** figure out the best decoding function for list ***************/

  semantic_error_t 
  find_decision(dlist_t &dlist,
		const matchinfo_t &goodbitsIn,
		decisionfunc_t **odf)
  {

    // figure out which bits matter for this list

    matchinfo_t was0, was1, goodbits;

    // now get the was1/was0, which need the normalized list
    for (matchinfo_t::const_iterator 
	   j = goodbitsIn.begin(); j != goodbitsIn.end(); ++j) {
      was1[j->first].mask = ~static_cast<uint64_t>(0);
      was0[j->first].mask = ~static_cast<uint64_t>(0);

      for (dlist_t::iterator k = dlist.begin(); k != dlist.end(); ++k) {
	  matchval_t & im = (k->p)[j->first];
	  was1[j->first].mask &= (im.mask & im.val);
	  was0[j->first].mask &= (im.mask & ~im.val);
      }
    }
    
    for (matchinfo_t::const_iterator 
	   j = goodbitsIn.begin(); j != goodbitsIn.end(); ++j) 
      goodbits[j->first].mask = (j->second.mask
				 & ~was0[j->first].mask 
				 & ~was1[j->first].mask);

    if (SHOW_DECODING) {
      std::cerr << "Deciding dlist: " << goodbits << std::endl;

      for (dlist_t::const_iterator i = dlist.begin(); i != dlist.end(); ++i) 
	std::cerr << "\t" << i->p << " " << i->lambda << " " 
		  << (i->l ? i->l->id : "<none>")
		  << std::endl;
    }

    // check whether there are more than one thing to chose from.
    bool foundmore = false;
    iclass_t *saveit = 0;
    double lambda = 0.0;
    for (dlist_t::const_iterator i = dlist.begin(); i != dlist.end(); ++i) {
      if (saveit && i->l != saveit) {
	foundmore = true;
	break;
      }
      if (!saveit) saveit = i->l;
      lambda += i->lambda;
    }
    // if not more than one thing to chose from, make no decision. This is
    // very important so that instructions with multiple decode patterns don't
    // get stuck while trying to ensure that a pattern actually separates
    // instructions.
   if (!foundmore || is_empty(goodbits)) {
      decisionfunc_t *nd = new decisionfunc_t;
      if (dlist.size()) {
	nd->kind = decisionfunc_t::dec_leaf;
	nd->leaf = dlist[0].l;
      } else {
	nd->kind = decisionfunc_t::dec_empty;
      }
      nd->freq = lambda;
      *odf = nd;
      return no_error;
    }

    // find the best pattern
    decisionfunc_t *best, *current;
    best = new decisionfunc_t;
    current = new decisionfunc_t;

    best->kind = decisionfunc_t::dec_pattern;
    current->kind = decisionfunc_t::dec_pattern;

    dlists_t *bestList = 0;
    double bestCost = std::numeric_limits<double>::max();

    matchinfo_t cumpattern, bestbitsT, bestbitsF, currbits = goodbits;
  
    bool foundSomething = false;
    int bitsinbest = 0, bitsincurrent = 1;

    while (1) {
      bool improved = false;
      // invariant: best holds best N-1 bit pattern

      for (matchinfo_t::const_iterator i = currbits.begin();
	   i != currbits.end(); ++i) {

	uint64_t btg = i->second.mask;

	for (int j = 0; j < 64 && btg; ++j, btg>>=1) {
	  uint64_t nm = (static_cast<uint64_t>(1)<<j);
	  if (!(i->second.mask & nm)) continue;
	
	  current->pattern = cumpattern;
	  current->pattern[i->first].mask |= nm;

	  // try with bit = 0
	  // must split the list and evaluate cost of each list.  Note that
	  // I use a slight tolerance on the cost when the pattern is being
	  // extended on the theory that bigger patterns are usually a bit
	  // better.  This also keeps us from getting silly nested if 
	  // statements due to rounding errors in cost calculations
	  dlists_t *zl;
	  double cost;

	  zl = split_list(dlist, current, &cost);
	  if (cost < bestCost || 
	      bitsincurrent > bitsinbest && cost < bestCost * 1.0000001) {
	    decisionfunc_t *t = current;
	    current = best;
	    best = t;
	    bestList = zl;
	    bestCost = cost;
	    improved = true;
	    bitsinbest = bitsincurrent;
	  } else delete zl;

	  // try with bit = 1
	  current->pattern = cumpattern;
	  current->pattern[i->first].mask |= nm;
	  current->pattern[i->first].val |= nm;
	  zl = split_list(dlist, current, &cost);
	  if (cost < bestCost ||
	      bitsincurrent > bitsinbest && cost < bestCost * 1.0000001) {
	    decisionfunc_t *t = current;
	    current = best;
	    best = t;
	    bestList = zl;
	    bestCost = cost;
	    improved = true;
	    bitsinbest = bitsincurrent;
	  } else delete zl;

	} /* bits iterator j */
      } /* matchinfo iterator i */
      // invariant: best holds best N bit pattern or N-1 bit pattern if N
      // didn't work out.  bestList holds the split instruction lists.
      // bestCost holds the best cost

      if (!improved) break; // we're done looking for patterns

      foundSomething = true;

      // remove best choice bits from currbits and add them to cumpattern
      for (matchinfo_t::iterator i = best->pattern.begin();
	   i != best->pattern.end(); ++i) {
	currbits[i->first].mask &= ~i->second.mask;
	currbits[i->first].val &= ~i->second.mask;
      }
      cumpattern = best->pattern;
      bitsincurrent++;
    }

    // NOTE: currbits has the remaining bits after the best pattern is
    // considered.

    /* now remember the best pattern */
    decisionfunc_t *bestPattern = best;
    dlists_t *bestPatternList = bestList;
    double bestPatternCost = bestCost;

    delete current;
    best = new decisionfunc_t;
    current = new decisionfunc_t;
    best->kind = decisionfunc_t::dec_table;
    current->kind = decisionfunc_t::dec_table;
    bestList = 0;
    bestCost = std::numeric_limits<double>::max();

    // NOW, find the best table.  It's actually easier.

    int tsize = 2;
    uint64_t tmask = 0x3;
    // we won't look at bigger than 8 bit=256 entry tables because 
    // we are generating switch statements and can't have them get
    // too terribly large.  Also, the decode generation starts to 
    // take an obnoxiously long time.
    while (tsize <= 8) {  
      bool improved = false;
      // invariant: best holds best N-1 bit table

      for (matchinfo_t::const_iterator i = goodbits.begin();
	   i != goodbits.end(); ++i) {

	uint64_t btg = i->second.mask;

	for (int j = 0; j < 64 && btg; ++j, btg>>=1) {
	  uint64_t nm = tmask << j;
	  if ((i->second.mask & nm) != nm) continue; // missing bits
	
	  current->table.length = tsize;
	  current->table.from = j;
	  current->table.field = i->first;

	  dlists_t *zl;
	  double cost;
	
	  zl = split_list(dlist, current, &cost);
	  if (cost < bestCost) {
	    decisionfunc_t *t = current;
	    current = best;
	    best = t;
	    bestList = zl;
	    bestCost = cost;
	    improved = true;
	    bestbitsT = goodbits;
	    bestbitsF = goodbits;
	    uint64_t mask = (((static_cast<uint64_t>(1)<<tsize)-1)<<j);
	    bestbitsT[i->first].mask &= ~mask;
	    bestbitsF[i->first].mask &= ~mask;
	  } else delete zl;
	
	} /* j in bits */

      } /* i in fields */
      if (!improved) break;

      foundSomething = true;

      tmask = (tmask << 1) | 1; // update tmask
      tsize ++;

      // no cumulative pattern or changes in choice bits as we look for
      // the next largest pattern.
    
    } /* while looking for best table */

    if (!foundSomething) { // we weren't able to decode at all!
      delete current;
      delete best;
      delete bestList;
      delete bestPattern;
      delete bestPatternList;

      // make a dummy decision function to prevent coredumps on destruction
      decisionfunc_t *nd = new decisionfunc_t;
      nd->kind = decisionfunc_t::dec_empty;
      nd->freq = 0;
      *odf = nd;
      return no_decoding_found;
    }

    /* and which one was the best! */
    if (bestPatternCost < bestCost) {
      delete best;
      delete bestList;
      best = bestPattern;
      bestList = bestPatternList;
      bestCost = bestPatternCost;
      bestbitsT = currbits;
      bestbitsF = goodbits;
    } else {
      delete bestPattern;
      delete bestPatternList;
    }

    if (!bestList) { 
#ifndef SET_DIFFS
      // we weren't able to split at all, which indicates that we're looking
      // at a case where there's overlap; it should be safe to ignore the last
      // element
      // it should be safe to ignore the overlap.
      decisionfunc_t *nd = new decisionfunc_t;
      dlist.pop_back();
      return find_decision(dlist, goodbits, odf);
#else
      decisionfunc_t *nd = new decisionfunc_t;
      nd->kind = decisionfunc_t::dec_empty;
      *odf = nd;
      return no_decoding_found;
#endif
    }

    // Now that we have the best decision function, we need to move down the
    // tree
    best->child.resize(bestList->size());

    if (SHOW_DECODING) {
      std::cerr << "\tCost = " << bestCost << std::endl;
      showDecode(std::cerr, 8, best, false); // children aren't valid yet
    }

    for (unsigned int i = 0; i < bestList->size(); ++i) {
      semantic_error_t serr;
      serr = find_decision((*bestList)[i], i ? bestbitsT : bestbitsF, 
			   &best->child[i]);
      if (serr) {
	delete bestList;
	return serr;
      }
    }

    // Make a copy of the best decision function to return
    *odf = best;

    delete current; // get rid of the extra one
    delete bestList;
    return no_error;
  }

  /************************ decode tree construction ******************/

  typedef struct decode_entry_tree_s {
    struct decode_entry_tree_s *parent;
    decode_entry_t node;
    std::list<struct decode_entry_tree_s *> children;
    ~decode_entry_tree_s() {
      for (std::list<struct decode_entry_tree_s *>::iterator
	     i = children.begin(); i != children.end(); ++i) {
	delete *i;
      }
    }
  } decode_entry_tree_t;
  typedef std::list<decode_entry_tree_t *> de_forest_t;

  /* Insert dt into a parent node.  it is invariant that the we have already
   * determined that dt is a subpattern of the parent node.  Also, we'll
   * assume that patterns have been normalized (all fields have definitions)
   */
  void
  insert_into_decode_tree(decode_entry_tree_t *dparent, 
			  decode_entry_tree_t *dt, overlaps_t &ol) 
  {
    dt->parent = dparent;
    for (de_forest_t::iterator 
	   i = dparent->children.begin(); i!= dparent->children.end(); ++i) {
      //std::cerr << dt->node.p << " " << (*i)->node.p << " " 
      //	<< (*i)->node.l->id << "\n";

      if (isStrictSubpattern(dt->node.p, (*i)->node.p)) {
	//std::cerr << "\tIs child\n";
	// dt is a subpattern of the child.  Need to insert into child
	insert_into_decode_tree((*i), dt, ol);
	return; // not our direct child
      }
      if (isStrictSubpattern((*i)->node.p, dt->node.p)) {
	//std::cerr << "\tIs parent\n";
	// move child into dt and keep going
	(*i)->parent = dt;
	dt->children.push_back((*i));
	de_forest_t::iterator j = i;
	++i;
	dparent->children.erase(j);
	--i;
      } else if (isEqual((*i)->node.p, dt->node.p)) {
	//std::cerr << "\tIs Equal\n";
	// overwrite the old node
	(*i)->node = dt->node;
	return; // don't add the child again, but replace it!
      } else { // should not have overlap with siblings
	matchinfo_t dummy;
	if (do_pattern_and(dt->node.p, (*i)->node.p, dummy)) { 
	  //std::cerr << "\tIs overlapped sibling\n";
	  ol.push_back(overlap_t(dt->node.l, (*i)->node.l));
	}
      }
    }
    dparent->children.push_back(dt);
  }

  inline void split_apart(const matchinfo_t A, const matchSet_t &B,
			  matchSet_t *D) {
    
    //std::cerr << "Must split " << A << " by " << B << "\n";
    if (!B.size()) {
      //std::cerr << "\tFinal piece: " << A << "\n";
      add_to_matchset(*D, A);
      return;
    } else if (B.size() == 1 && A == *(B.begin())) return; // empty

    int bestcost = B.size()*2+1;
    std::string bestname;
    int bestindex;

    for (matchinfo_t::const_iterator ai = A.begin(); ai != A.end(); ++ai) {
      const matchval_t &Av = ai->second;
      for (int j=63; j >=0; --j) {
	uint64_t mask = (uint64_t(1) << j);
	if (!(Av.mask & mask)) { // found a bit to split
	  matchSet_t m0, m1;
	  matchinfo_t a0 = A, a1 = A;

	  a0[ai->first].mask |= mask;
	  a0[ai->first].val &= ~mask; // clear bit just in case
	  a1[ai->first].mask |= mask; 
	  a1[ai->first].val |= mask;

	  for (matchSet_t::const_iterator 
		 bi = B.begin(); bi != B.end(); ++bi) {
	    matchinfo_t Y0, Y1;
	    if (do_pattern_and(a0, *bi, Y0)) add_to_matchset(m0,Y0);
	    if (do_pattern_and(a1, *bi, Y1)) add_to_matchset(m1,Y1);
	  } // for B

	  if (m0.size() + m1.size() <= B.size()) { // we're done, recurse.

	    //std::cerr << "Split lists are " << m0 << " and " << m1 << "\n";

	    split_apart(a0, m0, D);
	    split_apart(a1, m1, D);
	    return;
	  } else if (m0.size() + m1.size() <= bestcost) {
	    bestcost = m0.size() + m1.size();
	    bestname = ai->first;
	    bestindex = j;
	  }
	} // found a bit
      }
    }
    //std::cerr << "No good split found; pick the best there was\n";
    if (bestcost < B.size()*2+1) { 
      matchinfo_t::const_iterator ai = A.find(bestname);
      const matchval_t &Av = ai->second;
      uint64_t mask = (uint64_t(1) << bestindex);
      matchSet_t m0, m1;
      matchinfo_t a0 = A, a1 = A;

      a0[bestname].mask |= mask;
      a0[bestname].val &= ~mask; // clear bit just in case
      a1[bestname].mask |= mask; 
      a1[bestname].val |= mask;

      for (matchSet_t::const_iterator 
	     bi = B.begin(); bi != B.end(); ++bi) {
	matchinfo_t Y0, Y1;
	if (do_pattern_and(a0, *bi, Y0)) add_to_matchset(m0,Y0);
	if (do_pattern_and(a1, *bi, Y1)) add_to_matchset(m1,Y1);
      } // for B

      //std::cerr << "Split lists are " << m0 << " and " << m1 << "\n";

      split_apart(a0, m0, D);
      split_apart(a1, m1, D);
      return;
    }
    //std::cerr << "No split found at all.  Give up and go home";
    exit(1);
  } // split_apart

  // do the set differencing and flatten everything up.
  // entry invariant: dt is tree of decode entries which reflects a
  //                   hierarchy of pattern subsets; 
  //                  finallist contains the list of final decode entries 
  //                   found so far with no overlaps
  // exit invariant:  finallist contains the list of final decode entries 
  //                   found so far with no overlaps

  // invariant is that parent has already performed its set difference.
  // collect the goodies in one list, throwing out the garbage we didn't
  // need
  void 
  flatten_decode_entry_tree(const decode_entry_tree_t *dt, dlist_t &finallist)
  {
    matchSet_t sofar;

    // merge children first, because that's guaranteed to result in 
    // no more elements to deal with

    matchSet_t allChildren;
    for (de_forest_t::const_iterator
	   i = dt->children.begin(); i!= dt->children.end(); ++i) {
      // std::cerr << "Adding " << (*i)->node.l->id << " to siblings\n";
#ifdef SET_DIFFS
      add_to_matchset(allChildren, (*i)->node.p);
#endif
      flatten_decode_entry_tree(*i, finallist);
    }


    // now compute the set difference
#ifdef SET_DIFFS
    //    if (dt->node.l)
    //      std::cerr << "Splitting " << dt->node.l->id << "\n";
    split_apart(dt->node.p, allChildren, &sofar);

    if (SHOW_SETOPS)
      std::cerr << "Computing " << dt->node.p << " / " << allChildren 
		<< " = " << sofar << std::endl;
#else
    // causes overlap
    sofar.push_back(dt->node.p);
#endif

    // splice in setdiff, figuring out how to appropriately split lambda
    double totallambda=0.0;
    unsigned int matchcnt = 0;
    for (matchSet_t::const_iterator 
	   i = sofar.begin(); i != sofar.end(); ++i) {

      int numdiff = count_difference_bits(dt->node.p,*i);
      double lambda = std::pow(2.0, -numdiff);

      if (dt->node.p.size()) {
	decode_entry_t nd = { *i, dt->node.l, lambda };
	finallist.push_back(nd);
	matchcnt++;
	totallambda += lambda;
      }
    }
    for (unsigned int i = 1; i <= matchcnt; ++i) {
      finallist[finallist.size()-i].lambda *= (dt->node.lambda / totallambda);
    }
  } // flatten_decode_entry_tree()

  // invariant is that parent has already performed its set difference.
  // collect the goodies in one list, throwing out the garbage we didn't
  // need
  void 
  print_decode_entry_tree(const decode_entry_tree_t *dt, int level)
  {
    for (int j = 0; j < level; ++j) std::cerr << " ";
    
    if (dt->node.l) 
      std::cerr << dt->node.l->id << " " << dt->node.p << "\n";
    else
      std::cerr << "empty node" << " " << dt->node.p << "\n";

    for (de_forest_t::const_iterator
	   i = dt->children.begin(); i!= dt->children.end(); ++i) {
      print_decode_entry_tree(*i, level+2);
    }
  } // print_decode_entry_tree()

  semantic_error_t
  createDecodingForSet(LIS_isa::instructionsOrdered_t &ilist, 
		       decisionfunc_t **decodeTreeP)
  {

    // Figure out which bits actually matter.  The bits that
    // matter are those which are not constant across all instructions and
    // which are not don't care across all instructions.
    // Note that cared, was1, and was0 will be fully defined when this is done

    matchinfo_t cared;

    // cared will be normalized
    for (LIS_isa::instructionsOrdered_t::const_iterator i = ilist.begin();
	 i != ilist.end(); ++i) 
      for (matchSet_t::const_iterator 
	     k = (*i)->matchinfo.begin(); k != (*i)->matchinfo.end(); ++k) {
	const matchinfo_t & im = *k;
	for (matchinfo_t::const_iterator j = im.begin(); j != im.end(); ++j) {
	  cared[j->first].mask |= j->second.mask; // we care about these bits
	}
      }

    // prepare to normalize frequencies
    uint64_t tFreq = 0;
    for (LIS_isa::instructionsOrdered_t::iterator i = ilist.begin();
	 i != ilist.end(); ++i) {
      tFreq += (*i)->frequency;
    }

    // create the decode entry set and form it into a tree, normalizing 
    // frequencies as we go
  
    decode_entry_tree_t *dforest = new decode_entry_tree_t();
    dforest->parent = 0;
    dforest->node.l = 0;
    dforest->node.lambda = 0.0;
    overlaps_t ol;

    for (LIS_isa::instructionsOrdered_t::iterator i = ilist.begin();
	 i != ilist.end(); ++i) {

      //std::cerr << "Inserting " << (*i)->id << "\n";

      make_disjoint((*i)->matchinfo);

      std::list<double> fl = normalize_frequencies((*i)->matchinfo);
      std::list<double>::const_iterator fi = fl.begin();

      for (matchSet_t::iterator 
	     k = (*i)->matchinfo.begin(); k != (*i)->matchinfo.end(); 
	   ++k, fi++) {

	decode_entry_tree_t *dt = new decode_entry_tree_t();
	dt->node.l = *i;
	dt->node.lambda = (*fi) * static_cast<double>((*i)->frequency) / tFreq;

	// normalize the pattern as we copy it
	for (matchinfo_t::const_iterator 
	       j = cared.begin(); j != cared.end(); ++j) {
	  dt->node.p[j->first] = (*k)[j->first];
	}
    
	insert_into_decode_tree(dforest, dt, ol);
      }
    }

    // were there any overlaps?  If yes, we cannot complete the decoding
    // process and we should not attempt to do anything else.

    if (ol.size()) {
      dlist_t dlist;
      find_decision(dlist, cared, decodeTreeP); // create dummy tree

      for (overlaps_t::const_iterator i = ol.begin(); i != ol.end(); ++i) {
	std::cerr << "Overlap between instructions " << i->first->id 
		  << "(" << i->first->loc << ") and "
		  << i->second->id << "(" << i->second->loc
		  << ")\n";
      }

      return instruction_pattern_overlaps;
    }
  
    // now flatten the decode tree and add its nodes to the list.  We will 
    // reserve some initial space for efficiency's sake, but the list is likely
    // to be resized as patterns are split.

    dlist_t dlist;
    dlist.reserve(ilist.size());
    if (ilist.size()) {
      if (SHOW_SETOPS) print_decode_entry_tree(dforest,0);
      flatten_decode_entry_tree(dforest, dlist);
    }

    if (SHOW_SETOPS) {
      std::cerr << "Final dlist:" << std::endl;
      for (dlist_t::const_iterator i = dlist.begin(); i != dlist.end(); ++i) 
	std::cerr << "\t" << i->p << " " << i->lambda << " " 
		  << (i->l ? i->l->id : "<none>")
		  << std::endl;
    }

    // mark existing instructions so we can skip those that aren't decodable
    for (dlist_t::const_iterator i = dlist.begin(); i != dlist.end(); ++i) 
      if (i->l) i->l->canDecode = true;

    return (find_decision(dlist, cared, decodeTreeP));
  }

} // anonymous namespace 

  /*********************** and the overall entry point ******************/

semantic_error_t
LIS_isa::createDecoding(void) {

  // create the decode functions for each implemented buildset
  for (buildsetOrder_t::iterator 
	 i = buildsetOrder.begin(); i != buildsetOrder.end(); ++i) {
    buildset_t &bs = buildsets[*i];
    
    // restrict list to the instructions in the buildset
    iclass_t &bc = iclasses[bs.baseclass];
    instructionsOrdered_t ilist;
    iclass_set_t &uc = bc.usedby_class;
    for (instructionsOrdered_t::const_iterator 
	   j = instrsOrdered.begin(); j != instrsOrdered.end(); ++j) {
      if (uc.count(*j) || *j == &bc) ilist.push_back(*j);
    }

    // now do each decode function
    for (decoders_t::iterator 
	   j = bs.decoders.begin(); j != bs.decoders.end(); ++j) {

      semantic_error_t serr;
      serr = createDecodingForSet(ilist, &j->second->decodeTree);
      if (serr) return serr;
    }
  }

  return no_error;
}
