/*
 * Copyright (c) 2006-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LIS parser definitions
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This program declares types and functions needed for parsing/lexing
 *
 */
#ifndef LIS_PARSEDEFS_H
#define LIS_PARSEDEFS_H

#include <list>
#include <sstream>
#include <string>
#include <vector>
#include "LIS.h"

namespace LIS_parsedefs {

  // types and definitions coming from the parser

  typedef long long int cint_t;

  typedef class std::list<std::string> strlist_t;
  typedef class std::vector<strlist_t> strlistvec_t;

  typedef class std::pair<std::string, LIS::myloc_t> ident_t;

  typedef class std::list<ident_t> identlist_t;

  typedef class std::pair<int, std::string> sequenceitem_t;
  typedef class std::list<sequenceitem_t> sequenceitemlist_t;

  using LIS::range_t;
  using LIS::rangelist_t;

  typedef struct fmtfield {
    char *id;
    const char *field;
    int from, length;
    int defval;
    LIS::myloc_t loc;
    bool defpresent; 
  } fmtfield_t;
  typedef class std::list<fmtfield_t> fmtfieldlist_t;
  typedef class std::list<fmtfieldlist_t> fmtfieldlistlist_t;

  typedef struct cap {
    char *id;
    int actionNo;
    char *code;
    LIS::myloc_t loc;
  } cap_t;
  typedef class std::list<cap_t> caplist_t;

  typedef struct parm {
    enum { num, bitfield, code } valtype;
    union {
      fmtfield_t bitfield;
      int num;
      char *code;
    } val;
  } parm_t;
  typedef class std::list<parm_t> parmlist_t;

  typedef struct operandspec {
    char *opname;
    char *accessor;
    parmlist_t parmlist;
  } operandspec_t;
  typedef class std::list<operandspec_t> operandspeclist_t;

  typedef struct ctxsp_t {
    char *str;
    enum { in_iclass, in_buildset, in_style, in_top } loc;
  } ctxsp_t;

  /****************** IR elements **************************/

  enum IR_elementtype_t {
    IR_none,
    IR_accessoradd,
    IR_action,
    IR_bitfield,
    IR_buildset,
    IR_capability,
    IR_codesection,
    IR_cross,
    IR_decoder,
    IR_entry,
    IR_entrypoint,
    IR_enumadd,
    IR_enumdel,
    IR_exclude,
    IR_fieldadd,
    IR_frequency,
    IR_generator,
    IR_hide,
    IR_iclassadd,
    IR_implement,
    IR_match,
    IR_opcode,
    IR_operadd,
    IR_operdel,
    IR_opername,
    IR_predecode,
    IR_parentadd,
    IR_parentdel,
    IR_specialize,
    IR_step,
    IR_structadd,
    IR_structdel,
    IR_style,
    IR_template,
    IR_typeadd,
    IR_typedel,
  };

  using LIS::myloc_t;

  typedef struct IR_element_s {
    IR_elementtype_t typ;
    myloc_t loc;
    IR_element_s(const IR_elementtype_t t, const myloc_t &l): typ(t), loc(l) {}
    virtual ~IR_element_s() { }
  } IR_element_t;

  typedef std::list<IR_element_t *> IR_t;

  typedef struct IR_accessoradd_s : IR_element_s {
    std::string name, cType, valField, accType, params, code;
    IR_accessoradd_s(const char *n, const char *ct, const char *f, 
		     const char *at, const char *p, const char *c, 
		     const myloc_t &loc) 
      : IR_element_s(IR_accessoradd, loc), name(n), cType(ct), valField(f), 
	 accType(at), params(p), code(c) {}
    ~IR_accessoradd_s() {}
  } IR_accessoradd_t;

  typedef struct IR_action_s : IR_element_s {
    std::string instName, name, code;
    bool replace, disconnect;
    int seqNo;
    IR_action_s(const char *i , const char *n, const char *c,
		const bool r, const bool d, const int s, const myloc_t &loc) 
      : IR_element_s(IR_action, loc), instName(i), name(n), code(c), 
	 replace(r), disconnect(d), seqNo(s) {}
    IR_action_s(const char *i, const int s, const char *c,
		const bool r, const bool d, const myloc_t &loc) 
      : IR_element_s(IR_action, loc), instName(i), code(c), replace(r), 
	 disconnect(d), seqNo(s) {
      std::ostringstream os; os << seqNo;
      name = os.str();
    }
    ~IR_action_s() {}
  } IR_action_t;

  typedef struct IR_bitfield_s : IR_element_s {
    std::string instName;
    fmtfield_t fmt;
    IR_bitfield_s(const char *i, fmtfield_t &f, const myloc_t &loc) 
      : IR_element_s(IR_bitfield, loc), instName(i), fmt(f) {}
    ~IR_bitfield_s() { if (fmt.id) free(fmt.id); }
  } IR_bitfield_t;

  typedef struct IR_buildset_s : IR_element_s {
    std::string name, base, style;
    IR_t IR;
    IR_buildset_s(const char *n, const char *b, const char *s, 
		  const myloc_t &loc) 
      : IR_element_s(IR_buildset, loc), name(n), base(b), style(s) {}
    ~IR_buildset_s() {
      for (IR_t::iterator i = IR.begin(); i != IR.end(); i++)
	delete *i;
    }
  } IR_buildset_t;

  typedef struct IR_capability_s : IR_element_s {
    std::string buildsetName, name;
    int actionNo;
    std::string decodetoken;
    IR_capability_s(const char *b, const char *n, const int a, const char *d,
		    const myloc_t &loc) 
      : IR_element_s(IR_capability, loc), buildsetName(b), name(n),
	 actionNo(a), decodetoken(d) {}
    ~IR_capability_s() {}
  } IR_capability_t;

  typedef struct IR_codesection_s : IR_element_s {
    std::string secName, code; 
    bool append;
    IR_codesection_s(const char *s, const char *c, bool a, const myloc_t &loc) 
      : IR_element_s(IR_codesection, loc), secName(s), code(c), append(a) {}
    ~IR_codesection_s() {}
  } IR_codesection_t;

  typedef struct IR_cross_s : IR_element_s {
    std::string instName;
    strlistvec_t crosses;
    bool overrideIFlag;
    IR_cross_s(const char *i, const strlistvec_t &c, bool o, 
	       const myloc_t &loc) 
      : IR_element_s(IR_cross, loc), instName(i), crosses(c), 
	 overrideIFlag(o) {}
    ~IR_cross_s() {}
  } IR_cross_t;

  typedef struct IR_decoder_s : IR_element_s {
    std::string buildsetName, name, params;
    rangelist_t arange;
    IR_decoder_s(const char *b, const char *n, const char *p, 
		 const rangelist_t &ar, const myloc_t &loc)
      : IR_element_s(IR_decoder, loc), buildsetName(b), name(n), 
	 params(p), arange(ar) {}
    IR_decoder_s(const char *b, const char *n, const char *p, 
		 const myloc_t &loc) 
      : IR_element_s(IR_decoder,loc), buildsetName(b), name(n), params(p) {}
    ~IR_decoder_s() {}
  } IR_decoder_t;

  typedef struct IR_entry_s : IR_element_s {
    std::string styleName, code, returnType, extraParms;
    IR_entry_s(const char *s, const char *c, const char *rt, const char *ep,
	       const myloc_t &loc) 
      : IR_element_s(IR_entry, loc), styleName(s), code(c), returnType(rt),
      extraParms(ep) {}
    ~IR_entry_s() {}
  } IR_entry_t;

  typedef struct IR_entrypoint_s : IR_element_s {
    std::string buildsetName, name, rtype, params, dtoken;
    rangelist_t brange, arange;
    IR_entrypoint_s(const char *b, const char *n, const char *r, 
		    const char *p, const rangelist_t &br, const char *d,
		    const rangelist_t &ar, const myloc_t &loc)
      : IR_element_s(IR_entrypoint, loc),
	 buildsetName(b), name(n), rtype(r), params(p), dtoken(d),
	 brange(br), arange(ar) {}
    IR_entrypoint_s(const char *b, const char *n, const char *r, 
		    const char *p, const char *d,
		    const rangelist_t &ar, const myloc_t &loc)
      : IR_element_s(IR_entrypoint, loc),
	buildsetName(b), name(n), rtype(r), params(p), dtoken(d), arange(ar) {}
    IR_entrypoint_s(const char *b, const char *n, const char *r, 
		    const char *p, const rangelist_t &br, const char *d,
		    const myloc_t &loc)
      : IR_element_s(IR_entrypoint, loc),
	 buildsetName(b), name(n), rtype(r), params(p), dtoken(d),
	 brange(br) {}
    IR_entrypoint_s(const char *b, const char *n, const char *r, 
		    const char *p, const rangelist_t &br, const myloc_t &loc)
      : IR_element_s(IR_entrypoint, loc),
	buildsetName(b), name(n), rtype(r), params(p), brange(br) {}
    ~IR_entrypoint_s() {}
  } IR_entrypoint_t;

  typedef struct IR_enumadd_s : IR_element_s {
    std::string typeName, enumName, enumVal;
    IR_enumadd_s(const char *t, const char *n, const char *v, 
		 const myloc_t &loc) 
      : IR_element_s(IR_enumadd, loc), typeName(t), enumName(n), enumVal(v) {}
    ~IR_enumadd_s() {}
  } IR_enumadd_t;

  typedef struct IR_enumdel_s : IR_element_s {
    std::string typeName, enumName;
    IR_enumdel_s(const char *t, const char *n, const myloc_t &loc) 
      : IR_element_s(IR_enumdel, loc), typeName(t), enumName(n) {}
    ~IR_enumdel_s() {}
  } IR_enumdel_t;

  typedef struct IR_exclude_s : IR_element_s {
    std::string styleName;
    rangelist_t excl;
    IR_exclude_s(const char *s, const rangelist_t &br, const myloc_t &loc)
      : IR_element_s(IR_exclude, loc), styleName(s), excl(br) {}
  } IR_exclude_t;

  typedef struct IR_fieldadd_s : IR_element_s {
    std::string name, ctype, code;
    bool addDef, isExpr;
    IR_fieldadd_s(const char *n, const char *t, const char *c, bool a,
		  bool ie, const myloc_t &loc) 
      : IR_element_s(IR_fieldadd, loc), 
      name(n), ctype(t), code(c), addDef(a), isExpr(ie) {}
    ~IR_fieldadd_s() {}
  } IR_fieldadd_t;

  typedef struct IR_frequency_s : IR_element_s {
    std::string instName;
    uint64_t val;
    IR_frequency_s(const char *i, const uint64_t v, const myloc_t &loc)
      : IR_element_s(IR_frequency, loc), instName(i), val(v) {}
    ~IR_frequency_s() {}
  } IR_frequency_t;

  typedef struct IR_generator_s : IR_element_s {
    std::string styleName, sectionName, code;
    IR_generator_s(const char *s, const char *cs, const char *c, 
		   const myloc_t &loc) 
      : IR_element_s(IR_generator, loc), styleName(s), sectionName(cs), 
      code(c) {}
    ~IR_generator_s() {}
  } IR_generator_t;

  typedef struct IR_hide_s : IR_element_s {
    std::string buildsetName, fieldName;
    LIS::hide_status_t hidestatus;
    IR_hide_s(const char *b, const char *f, const LIS::hide_status_t h,
	      const myloc_t &loc) 
      : IR_element_s(IR_hide, loc), 
	 buildsetName(b), fieldName(f), hidestatus(h) {}
    ~IR_hide_s() {}
  } IR_hide_t;

  typedef struct IR_iclassadd_s : IR_element_s {
    std::string name;
    bool isInstruction;
    bool overrideIFlag;
    IR_iclassadd_s(const char *n, const bool i, const bool o, 
		   const myloc_t &loc) 
      : IR_element_s(IR_iclassadd, loc), name(n), isInstruction(i),
    overrideIFlag(o) {}
    ~IR_iclassadd_s() {}
  } IR_iclassadd_t;

  typedef struct IR_implement_s : IR_element_s {
    std::string buildsetName;
    std::string style;
    IR_implement_s(const char *b, const char *s, const myloc_t &loc) 
      : IR_element_s(IR_implement, loc), buildsetName(b), style(s) {}
    ~IR_implement_s() {}
  } IR_implement_t;

  typedef struct IR_match_s : IR_element_s {
    std::string instName;
    fmtfieldlistlist_t fll;
    IR_match_s(const char *i, fmtfieldlistlist_t &f, const myloc_t &loc) 
      : IR_element_s(IR_match, loc), instName(i), fll(f) {}
    ~IR_match_s() {
      for (fmtfieldlistlist_t::iterator 
	     i = fll.begin(); i != fll.end(); i++) {
	for (fmtfieldlist_t::iterator
	       j = i->begin(); j != i->end(); j++)
	  if (j->id) free(j->id);
      }
    }
  } IR_match_t;

  typedef struct IR_opcode_s : IR_element_s {
    std::string instName, name;
    int piece;
    IR_opcode_s(const char *i, const char *n, const int p, const myloc_t &loc) 
      : IR_element_s(IR_opcode, loc), instName(i), name(n), piece(p) {}
    ~IR_opcode_s() {}
  } IR_opcode_t;

  typedef struct IR_operadd_s : IR_element_s {
    std::string iclassName, operName;
    std::string accessor, parmlist;
    IR_operadd_s(const char *i, const char *n, 
		 const char *ac, const char *p, const myloc_t &loc) 
      : IR_element_s(IR_operadd, loc), iclassName(i), operName(n), 
	accessor(ac), parmlist(p) {}
    ~IR_operadd_s() {}
  } IR_operadd_t;

  typedef struct IR_operdel_s : IR_element_s {
    std::string iclassName, operName;
    IR_operdel_s(const char *t, const char *n, const myloc_t &loc) 
      : IR_element_s(IR_operdel, loc), iclassName(t), operName(n) {}
    ~IR_operdel_s() {}
  } IR_operdel_t;

  typedef struct IR_opername_s : IR_element_s {
    std::string name, operType;
    int offset;
    int decodeNo;
    int actionNo;
    IR_opername_s(const char *n, const char *t, int o, int d, int a,
		  const myloc_t &loc)
      : IR_element_s(IR_opername, loc), name(n), operType(t), offset(o),
	decodeNo(d), actionNo(a) {}
    ~IR_opername_s() {}
  } IR_opername_t;

  typedef struct IR_parentadd_s : IR_element_s {
    std::string instName;
    std::string parentName;
    IR_parentadd_s(const char *i, const char *p, const myloc_t &loc) 
      : IR_element_s(IR_parentadd, loc), instName(i), parentName(p) {}
    ~IR_parentadd_s() {}
  } IR_parentadd_t;

  typedef struct IR_parentdel_s : IR_element_s {
    std::string instName;
    std::string parentName;
    IR_parentdel_s(const char *i, const char *p, const myloc_t &loc) 
      : IR_element_s(IR_parentdel, loc), instName(i), parentName(p) {}
    ~IR_parentdel_s() {}
  } IR_parentdel_t;

  typedef struct IR_predecode_s : IR_element_s {
    std::string fieldName;
    bool setIt;
    IR_predecode_s(const char *f, const bool s, const myloc_t &loc) 
      : IR_element_s(IR_predecode, loc), fieldName(f), setIt(s) {}
    ~IR_predecode_s() {}
  } IR_predecode_t;

  typedef struct IR_step_s : IR_element_s {
    std::string buildsetName, name, stepName, dtoken;
    int sno;
    rangelist_t brange, arange;
    IR_step_s(const char *b, const char *n, int sn, const char *s, 
	      const rangelist_t &br, const char *d,
	      const rangelist_t &ar, const myloc_t &loc)
      : IR_element_s(IR_step, loc), 
	 buildsetName(b), name(n), stepName(s), dtoken(d), sno(sn),
	 brange(br), arange(ar) {}
    IR_step_s(const char *b, const char *n, int sn, const char *s, 
	      const char *d, const rangelist_t &ar, const myloc_t &loc)
      : IR_element_s(IR_step, loc), 
	 buildsetName(b), name(n), stepName(s), dtoken(d), sno(sn),
	 arange(ar) {}
    IR_step_s(const char *b, const char *n, int sn, const char *s, 
	      const rangelist_t &br, const myloc_t &loc)
      : IR_element_s(IR_step, loc), 
	 buildsetName(b), name(n), stepName(s), sno(sn), brange(br) {}
    ~IR_step_s() {}
  } IR_step_t;

  typedef struct IR_structadd_s : IR_element_s {
    std::string typeName, fieldDef;
    IR_structadd_s(const char *t, const char *f, const myloc_t &loc) 
      : IR_element_s(IR_structadd, loc), typeName(t), fieldDef(f) {}
    ~IR_structadd_s() {}
  } IR_structadd_t;

  typedef struct IR_structdel_s : IR_element_s {
    std::string typeName, fieldName;
    IR_structdel_s(const char *t, const char *f, const myloc_t &loc) 
      : IR_element_s(IR_structdel, loc), typeName(t), fieldName(f) {}
    ~IR_structdel_s() {}
  } IR_structdel_t;

  typedef struct IR_style_s : IR_element_s {
    std::string name;
    IR_t IR;
    IR_style_s(const char *n, const myloc_t &loc) 
      : IR_element_s(IR_style, loc), name(n) {}
    ~IR_style_s() {
      for (IR_t::iterator i = IR.begin(); i != IR.end(); i++)
	delete *i;
    }
  } IR_style_t;

  typedef struct IR_typeadd_s : IR_element_s {
    std::string def1, def2;
    IR_typeadd_s(const char *d1, const char *d2, const myloc_t &loc) 
      : IR_element_s(IR_typeadd, loc), def1(d1), def2(d2) {}
    ~IR_typeadd_s() {}
  } IR_typeadd_t;

  typedef struct IR_typedel_s : IR_element_s {
    std::string name;
    IR_typedel_s(const char *n, const myloc_t &loc) 
      : IR_element_s(IR_typedel, loc), name(n) {}
    ~IR_typedel_s() {}
  } IR_typedel_t;

  typedef struct IR_specialize_s : IR_element_s {
    std::string styleName, fieldName;
    IR_specialize_s(const char *st, const char *f, const myloc_t &loc) 
      : IR_element_s(IR_specialize, loc), 
	 styleName(st), fieldName(f) {}
    ~IR_specialize_s() {}
  } IR_specialize_t;

  // Declarations going to the parser

} // namespace LIS_parsedefs

namespace LIS_internal {
  extern void startcode(char lastcode);  
  extern bool suppressed;
}

#endif /* LIS_PARSEDEFS_H */

