/*
 * Copyright (c) 2006-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * Main ISA class methods
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file has the class methods which build the ISA description.  It
 * is up to subclasses to do something interesting with the description.
 *
 */
#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>
#include <limits>
#include <queue>
#include <sstream>
#include "LIS_isa.h"

using namespace LIS;
using namespace LIS_decoding_support;
using namespace LIS_parsedefs;

/**************** Initialization and dumping ***********************/

const char *LIS_isa::otnames[3]={"src","dest","int"};
const char *LIS_isa::stnames[4]={"nil","reg","mem","other"};

namespace {
  myloc_t dummyloc = { "<no file>", 0, 0, 0, 0 };

  std::string strip_space(const std::string &s) {
    std::string::size_type l = s.find_first_not_of(" \t\n");
    if (l == std::string::npos) return s;
    return s.substr(l,s.find_last_not_of(" \t\n")-l+1);
  }

}

namespace LIS {
  std::string find_pnames(std::string p) {
    std::string rval;
    if (p == "void" || p == "") return std::string("");
    int pcnt = 0, firstchar=0, lastchar=0;
    bool inword = false, outword=false;
    for (unsigned int i = 0; i < p.size(); i++) {
      char c = p[i];
      if (c == ',' && !pcnt) { 
	if (rval.size()) rval += ", ";
	rval += p.substr(firstchar, lastchar+1-firstchar);
	inword = false; outword=false;
      }
      else if (c == '(') { pcnt++; inword = false; }
      else if (c == ')') { pcnt--; inword = false; }
      else if (outword) {}
      else if (c == '=' && !pcnt) { outword=true; inword=false; }
      else if (std::isalpha(c) || c == '_') {
	if (pcnt) {}
	else if (inword) lastchar = i;
	else { firstchar = i; lastchar = i; inword = true; }
      } else if (std::isdigit(c) && inword) { lastchar = i; }
      else { inword = false; }
    }
    if (rval.size()) rval += ", ";
    rval += p.substr(firstchar, lastchar+1-firstchar);
    return rval;
  } // find_pnames
}

LIS_isa::LIS_isa(bool le, bool bld)
{

  isLSEemu = le;
  isBuildable = bld;
  LIS_decoding_support::init_helpers();

  validAccessors.insert(std::string("decode"));
  validAccessors.insert(std::string("read"));
  validAccessors.insert(std::string("write"));
  validAccessors.insert(std::string("backup"));
  validAccessors.insert(std::string("resolve"));

  iclass_t newi = { "ALL", "", iclass_set_t(), iclass_set_t(), 
		    sequence_t(), actions_t(), bitfields_t(),
		    operands_t(), matchSet_t(), dummyloc, false, 
		    0, 0, false, matchinfo_t() };
  newi.matchinfo.push_back(matchinfo_t());
  iclasses[std::string("ALL")] = newi;

  // create a default instruction
  addIClass("DEFAULT",true,dummyloc);
  addFrequency("DEFAULT",-DEFAULT_FREQ+1,dummyloc); // set back to 1
  addParent("DEFAULT","ALL",dummyloc);

  // default fields that are always inserted by LSE_emu.py
  if (isLSEemu) {
    addField("addr", "LSE_emu_iaddr_t", "", false, dummyloc);
    addField("hwcontextno", "int", "", false, dummyloc);
    addField("swcontexttok", "LSE_emu_ctoken_t", "", false, dummyloc);
    addField("ctx","LSE_emu_isacontext_t",
	     "*(static_cast<LSE_emu_isacontext_t *>(swcontexttok))", 
	     false, dummyloc);
    addField("iclasses", "LSE_emu_iclasses_t", "", false, dummyloc);
    addField("size", "LSE_emu_addr_t", "", false, dummyloc);
    addField("next_pc", "LSE_emu_iaddr_t", "", false, dummyloc);

    addField("branch_targets", "LSE_emu_branchtargs_t", "", false, dummyloc);
    addField("branch_dir", "int", "", false, dummyloc);
    addField("branch_num_targets", "int", "", false, dummyloc);

    // TODO: operand_info?  operand_val?
  }

  // the built-in styles

  addStyle("unimplemented", dummyloc);
  addStyle("single", dummyloc);
  addEntry("single", "%%BEFORE() %%IFAFTER(%%TOKENSWITCH())", "", "",dummyloc);
  addStyle("split", dummyloc);
  addEntry("split", "%%BEFORE() %%IFAFTER(return %%AFTERPTR()(%%AFTERARGS()));", 
	   "", "", dummyloc);
  // add here so they can be counted in the order lists...
  IR.push_back(new IR_style_s("single",dummyloc)); 
  IR.push_back(new IR_style_s("split",dummyloc));
}

LIS_isa::~LIS_isa()
{
  for (IR_t::iterator i = IR.begin(); i != IR.end(); ++i)
    delete *i;
  IR.clear();
  for (actionlist_t::iterator i = actions.begin(); i != actions.end(); ++i)
    delete *i;
  actions.clear();
}

/******************* building the ISA data structures **************/

void
LIS_isa::buildpassStyle(const LIS_parsedefs::IR_t &IR, 
			LIS_isa::buildWork_t &worker, 
			const buildi_t &parent) {

  // Input assumptions: all buildsets and styles have been added provisionally
  //                    worker sets are clean
  // Outputs: list of buildsets and styles which have been explicitly 
  //          implemented, buildset to style mapping, style to sub-buildsets
  //          mapping.
  for (IR_t::const_iterator i = IR.begin(); i != IR.end(); ++i) {
    IR_element_t &ie = **i;
      
    try {
      switch (ie.typ) {
      case IR_buildset:
	{
	  IR_buildset_t &tp = *dynamic_cast<IR_buildset_t *>(*i);
	  
	  worker.subBuildsets[parent].insert(tp.name);

	  if (tp.style != "") {

	    if (!styles.count(tp.style))
	      throw semantic_exception(undefined_style, ie.loc);

	    worker.bs2style[tp.name] = tp.style;

	  } else if (!worker.bs2style.count(tp.name))
	    worker.bs2style[tp.name] = "unimplemented";
	  buildpassStyle(tp.IR, worker, buildi_t(true, tp.name));
	}
	break;
      case IR_implement:
      {
	IR_implement_t &tp = *dynamic_cast<IR_implement_t *>(*i);
	if (!styles.count(tp.style))
	  throw semantic_exception(undefined_style, ie.loc);
	if (!buildsets.count(tp.buildsetName))
	  throw semantic_exception(undefined_buildset, ie.loc);

	worker.bs2style[tp.buildsetName] = tp.style;
      }
      break;
      case IR_style:
	{
	  IR_style_t &tp = *dynamic_cast<IR_style_t *>(*i);
	  worker.subStyles[parent].insert(tp.name);
	  buildpassStyle(tp.IR, worker, buildi_t(false, tp.name));
	}
      default: break;
      }
    } catch (semantic_exception se) {
      LIS_user::reportError(errToString(se.err), se.loc);
    }
  } // for IR
} // buildpassStyle

void 
LIS_isa::buildpass(LIS_parsedefs::IR_t &IR, 
			LIS_isa::buildWork_t &worker) {

  for (IR_t::iterator i = IR.begin(); i != IR.end(); ++i) {
    IR_element_t &ie = **i;
      
    try {
      switch (ie.typ) {
      case IR_accessoradd:
	{
	  IR_accessoradd_t &tp = *dynamic_cast<IR_accessoradd_t *>(*i);
	  addAccessor(tp.cType, tp.valField, tp.name, tp.accType, 
		      tp.params, tp.code, ie.loc);
	}
	break;
      case IR_action:
	{
	  IR_action_t &tp = *dynamic_cast<IR_action_t *>(*i);
	  addAction(tp.instName, tp.name, tp.replace, tp.disconnect, false,
		    tp.seqNo, tp.code, ie.loc);
	}
	break;
      case IR_bitfield: 
	{
	  IR_bitfield_t &tp = *dynamic_cast<IR_bitfield_t *>(*i);
	  addBitfield(tp.instName, tp.fmt, ie.loc);
	}
	break;
      case IR_buildset:
	{
	  IR_buildset_t &bs = *dynamic_cast<IR_buildset_t *>(*i);

	  if (bs.base != "") {
	    buildsets[bs.name].baseclass = bs.base;
	    if (!iclasses.count(bs.base)) 
	      throw semantic_exception(undefined_iclass, ie.loc);
	  }

	  std::string &sn = buildsets[bs.name].style;
	  if (sn != "unimplemented") {

	    buildpass(bs.IR, worker);

	    // do this in postfix order so that we get sub-buildsets out
	    // before their parents
	    if (!std::count(buildsetOrder.begin(),
			    buildsetOrder.end(),bs.name))
	      buildsetOrder.push_back(bs.name);
	  }
	}
	break;
      case IR_capability:
	{
	  IR_capability_t &cp = *dynamic_cast<IR_capability_t *>(*i);
	  addCapability(cp.buildsetName, cp.name, cp.actionNo,
			cp.decodetoken, ie.loc);
	  // maintain global list of capabilities
	  allCapabilities.insert(cp.name);
	}
	break;
      case IR_codesection:
	{
	  IR_codesection_t &cs = *dynamic_cast<IR_codesection_t *>(*i);
	  addCode(cs.secName, cs.code, cs.append, ie.loc);
	}
	break;
      case IR_cross:
	{
	  IR_cross_t &cs = *dynamic_cast<IR_cross_t *>(*i);
	  performCross(cs.instName, cs.overrideIFlag, cs.crosses, ie.loc);
	}
	break;
      case IR_decoder:
	{
	  IR_decoder_t &dc = *dynamic_cast<IR_decoder_t *>(*i);
	  if (worker.dps.count(dc.name))
	    throw semantic_exception(duplicate_decoder, ie.loc);
	    
	  addDecoder(dc.buildsetName, dc.name, dc.params, dc.arange, ie.loc);

	  worker.dps.insert(dc.name);
	}
	break;
      case IR_entry:
	{
	  IR_entry_t &tp = *dynamic_cast<IR_entry_t *>(*i);
	  addEntry(tp.styleName, tp.code, tp.returnType, tp.extraParms,
		   ie.loc);
	}
	break;
      case IR_entrypoint:
	{
	  IR_entrypoint_t &ep = *dynamic_cast<IR_entrypoint_t *>(*i);
	  if (worker.eps.count(ep.name))
	    throw semantic_exception(duplicate_entrypoint, ie.loc);

	  addEntrypoint(ep.buildsetName, ep.rtype, ep.name, ep.params,
			ep.brange, ep.dtoken, ep.arange, ie.loc);

	  worker.eps.insert(ep.name);
	}
	break;
      case IR_enumadd: 
	{
	  IR_enumadd_t &tp = *dynamic_cast<IR_enumadd_t *>(*i);
	  addEnumValue(tp.typeName, tp.enumName, tp.enumVal, ie.loc);
	}
	break;
      case IR_enumdel: 
	{
	  IR_enumdel_t &tp = *dynamic_cast<IR_enumdel_t *>(*i);
	  removeEnumValue(tp.typeName, tp.enumName, ie.loc);
	}
	break;
      case IR_exclude:
	{
	  IR_exclude_t &tp = *dynamic_cast<IR_exclude_t *>(*i);
	  addExclude(tp.styleName, tp.excl, ie.loc);
	}
	break;
      case IR_fieldadd: 
	{
	  IR_fieldadd_t &tp = *dynamic_cast<IR_fieldadd_t *>(*i);
	  addField(tp.name, tp.ctype, tp.code, tp.addDef, tp.isExpr, ie.loc);
	}
	break;
      case IR_frequency: 
	{
	  IR_frequency_t &tp = *dynamic_cast<IR_frequency_t *>(*i);
	  addFrequency(tp.instName, tp.val, ie.loc);
	}
	break;
      case IR_generator:
	{
	  IR_generator_t &tp = *dynamic_cast<IR_generator_t *>(*i);
	  addGenerator(tp.styleName, tp.sectionName, tp.code, ie.loc);
	}
	break;
      case IR_hide:
	{
	  IR_hide_t &hd = *dynamic_cast<IR_hide_t *>(*i);
	  addHide(hd.buildsetName, hd.fieldName, hd.hidestatus, ie.loc);
	}
	break;
      case IR_iclassadd:
	{
	  IR_iclassadd_t &tp = *dynamic_cast<IR_iclassadd_t *>(*i);
	  addIClass(tp.name, tp.isInstruction, ie.loc);
	  if (tp.overrideIFlag) 
	    setIFlag(tp.name,tp.isInstruction, ie.loc);
	}
	break;
      case IR_implement: break; // TODO: check that it's good already done
      case IR_match: 
	{
	  IR_match_t &tp = *dynamic_cast<IR_match_t *>(*i);
	  updateMatch(tp.instName, tp.fll, ie.loc);
	}
	break;
      case IR_opcode:
	{
	  IR_opcode_t &tp = *dynamic_cast<IR_opcode_t *>(*i);
	  setOpcode(tp.instName, tp.name, tp.piece, ie.loc);
	}
	break;
      case IR_operadd: 
	{
	  IR_operadd_t &tp = *dynamic_cast<IR_operadd_t *>(*i);
	  addOperand(tp.iclassName, tp.operName, 
		     tp.accessor, tp.parmlist, ie.loc);
	}
	break;
      case IR_operdel: 
	{
	  IR_operdel_t &tp = *dynamic_cast<IR_operdel_t *>(*i);
	  removeOperand(tp.iclassName, tp.operName, ie.loc);
	}
	break;
      case IR_opername: 
	{
	  IR_opername_t &tp = *dynamic_cast<IR_opername_t *>(*i);
	  addOperandName(tp.name, tp.operType, tp.offset, 
			 tp.decodeNo, tp.actionNo, ie.loc);
	}
	break;
      case IR_parentadd:
	{
	  IR_parentadd_t &tp = *dynamic_cast<IR_parentadd_t *>(*i);
	  addParent(tp.instName, tp.parentName, ie.loc);
	}
	break;
      case IR_parentdel:
	{
	  IR_parentdel_t &tp = *dynamic_cast<IR_parentdel_t *>(*i);
	  removeParent(tp.instName, tp.parentName, ie.loc);
	}
	break;
      case IR_predecode: 
	{
	  IR_predecode_t &tp = *dynamic_cast<IR_predecode_t *>(*i);
	  setPredecode(tp.fieldName, tp.setIt, ie.loc);
	}
	break;
      case IR_specialize:
	{
	  IR_specialize_t &hd = *dynamic_cast<IR_specialize_t *>(*i);
	  addSpecialize(hd.styleName, hd.fieldName, ie.loc);
	}
	break;
      case IR_step:
	{
	  IR_step_t &ep = *dynamic_cast<IR_step_t *>(*i);
	  if (worker.eps.count(ep.name))
	    throw semantic_exception(duplicate_entrypoint, ie.loc);

	  addStep(ep.buildsetName, ep.name, ep.sno, ep.stepName,
		  ep.brange, ep.dtoken, ep.arange, ie.loc);

	  worker.eps.insert(ep.name);
	}
	break;
      case IR_structadd: 
	{
	  IR_structadd_t &tp = *dynamic_cast<IR_structadd_t *>(*i);
	  addStructField(tp.typeName, tp.fieldDef, ie.loc);
	}
	break;
      case IR_structdel: 
	{
	  IR_structdel_t &tp = *dynamic_cast<IR_structdel_t *>(*i);
	  removeStructField(tp.typeName, tp.fieldName, ie.loc);
	}
	break;
      case IR_style:
	{
	  IR_style_t &tp = *dynamic_cast<IR_style_t *>(*i);
	  if (worker.implStyles.count(tp.name)) {
	    buildpass(tp.IR, worker);
	    style_t *sp = &styles[tp.name];
	    if (!std::count(styleOrder.begin(), styleOrder.end(), sp))
	      styleOrder.push_back(sp);
	  }
	}
	break;
      case IR_typeadd: 
	{
	  IR_typeadd_t &tp = *dynamic_cast<IR_typeadd_t *>(*i);
	  addType(tp.def1, tp.def2, ie.loc);
	}
	break;
      case IR_typedel: 
	{
	  IR_typedel_t &tp = *dynamic_cast<IR_typedel_t *>(*i);
	  removeType(tp.name, ie.loc);
	}
	break;
      default: break;
      } // case element type
    } catch (semantic_exception se) {
      LIS_user::reportError(errToString(se.err), se.loc);
    }
    delete *i;
    *i = 0;
  } // for IR elements
  IR.clear();
} // buildpass

namespace {
  void find_implicit(accessor_code_t &acc, fields_t &fields) {
    acc.implicitfields.clear();
    
    std::string::size_type sl, ws;
    bool inword = false;
    sl = acc.code.size();
    for (std::string::size_type i = 0 ; i < sl; i++) {
      char c = acc.code[i];
      if (inword) {
	if (!(std::isalnum(c) || c == '_')) {
	  std::string word = acc.code.substr(ws, i-ws);
	  if (fields.count(word))
	    acc.implicitfields.insert(word);
	  inword = false;
	}
      } else {
	if (std::isalpha(c) || c == '_') { ws = i; inword = true;}
      }
    }
    if (inword) {
      std::string word = acc.code.substr(ws, sl-ws);
      if (fields.count(word))
	acc.implicitfields.insert(word);
    }
  } // find_implicit
}

bool
LIS_isa::completeISA(void)
{

  /********************** Actually add everything in *******************/

  /* The logic for this function is a bit complex because a couple of
     passes are needed over the IR before we get everything together.
     These passes are:

     1) Figure out which buildsets are actually implemented
     2) Add properties to individual classes, determine class hierarchy
     3) Determine final properties for each instruction.  Note that matches 
        should not be checked for validity until this step because the 
	bitfields aren't completely defined until we define the parents.
     4) Determine global properties relying on the final definition of things

  */	

  buildWork_t worker;

  // Pass 1: Figure out which buildsets are implemented

  // compute the hierarchical structure of the buildsets and top-level
  // definitions of buildsets.
  buildi_t topLevel = buildi_t(false,"");
  buildpassStyle(IR, worker, topLevel);

  // now transistively find all implemented buildsets and styles and set
  // the appropriate style in the buildset structure.

  std::list<buildi_t> workList;
  workList.push_back(topLevel);

  while (workList.size()) {
    buildi_t curr = workList.front();
    workList.pop_front();

    // handle substyles
    stringset_t &subs = worker.subStyles[curr];
    for (stringset_t::const_iterator i = subs.begin(); i != subs.end(); ++i) {
      const std::string &s = *i;
      if (s == "unimplemented") continue; // not implemented, so nothing to do
      if (!worker.implStyles.count(s)) {
	workList.push_back(buildi_t(false,s));
      }
    }

    // handle subbuildsets
    subs = worker.subBuildsets[curr];
    for (stringset_t::const_iterator i = subs.begin(); i != subs.end(); ++i) {
      std::string &s = worker.bs2style[*i];
      if (s == "unimplemented") continue; // not implemented, so nothing to do

      // recurse on style definition
      if (!worker.implStyles.count(s)) {
	worker.implStyles.insert(s);
	workList.push_back(buildi_t(false,s));
      }
      // recurse on buildsets
      if (!worker.implBuildsets.count(*i)) {
	worker.implBuildsets.insert(*i);
	buildsets[*i].style = s;
	workList.push_back(buildi_t(true,*i));
      }
    }
  } // while worklist not empty

  // Pass 2: individual class/buildset properties and hierarchy
  buildpass(IR, worker);

  // Pass 3: work out results of inheritance
  
  // Pass 4: work out the global stuff

  /* collect the ordered list of instructions and the global list
   * of instruction fields.
   */
  int icnt = 0;
  for (iclasses_t::iterator i = iclasses.begin();
       i != iclasses.end(); ++i) {

    std::set<int> usednumssrc, usednumsdest;

    // set up operand accessors properly.  Need for classes as well as
    // instructions because of possibility of being a base class

    for (operands_t::iterator
	   j = i->second.operands.begin(); j != i->second.operands.end(); 
	 j++) {
      j->second.acc = &accessors[j->second.accessor];
      j->second.opername = &opernames[j->first];
      int ooff = opernames[j->first].offset;
      if (opernames[j->first].opertype==oper_src ? 
	  usednumssrc.count(ooff) : usednumsdest.count(ooff)) {
	std::cerr << "In instruction " << i->second.id << ", operand #" 
		  << ooff << "\n";
	LIS_user::reportError(errToString(operand_offset_conflict),
			      j->second.loc);
	continue;
      }
      else if (opernames[j->first].opertype==oper_src) 
	usednumssrc.insert(ooff);
      else usednumsdest.insert(ooff);
    }

    // and total list of bitfields

    for (bitfields_t::const_iterator j = i->second.bitfields.begin();
	 j != i->second.bitfields.end(); j++) {
      instrfields.insert(j->second.field);
    }

    // and create the common match information for the instruction

    matchinfo_t cared, was0, was1;
    matchinfo_t &nv = i->second.matchcommon;

    for (matchSet_t::const_iterator 
	   k = i->second.matchinfo.begin(); k != i->second.matchinfo.end(); 
	 k++) {
      const matchinfo_t & im = *k;
      for (matchinfo_t::const_iterator j = im.begin(); j != im.end(); j++) {
	cared[j->first].mask |= j->second.mask; // we care about these bits
      }
    }

    for (matchinfo_t::const_iterator 
	   j = cared.begin(); j != cared.end(); j++) {
      was1[j->first].mask = ~static_cast<uint64_t>(0);
      was0[j->first].mask = ~static_cast<uint64_t>(0);
      for (matchSet_t::iterator 
	     k = i->second.matchinfo.begin(); k != i->second.matchinfo.end(); 
	   k++) {
	matchval_t & im = (*k)[j->first];
	was1[j->first].mask &= (im.mask & im.val);
	was0[j->first].mask &= (im.mask & ~im.val);
      }
    }

    for (matchinfo_t::const_iterator 
	   j = cared.begin(); j != cared.end(); j++) {
      nv[j->first].mask = was1[j->first].mask | was0[j->first].mask;
      nv[j->first].val = was1[j->first].mask;
    }

    // now only instructions should continue

    if (!i->second.isInstruction) continue;

    i->second.idno = ++icnt;
    instrsOrdered.push_back(&i->second);

  } // for i in iclasses

  /* Determine whether fields should be generated in ii structure */
  for (fields_t::iterator 
	 i = fields.begin(); i != fields.end(); ++i) {
    bool found = false;
    for (buildsets_t::iterator j = buildsets.begin();
	 j != buildsets.end(); j++) {
      if (j->second.style == "unimplemented") continue;
      if (!j->second.hiddenFields.count(i->second.id)) continue;
      if (j->second.hiddenFields[i->second.id].hidestatus == nothidden)
	found = true;
    }
    i->second.toDef &= found;
  }

  // Learn whether speculation is present

  bool foundSpeculation = false;

  for (buildsets_t::iterator j = buildsets.begin();
       j != buildsets.end() && !foundSpeculation; ++j) {

    buildset_t &bs = j->second;
    if (bs.style == "unimplemented") continue;

    for (implcaps_t::iterator i = bs.implCapabilities.begin();
	 i != bs.implCapabilities.end() && !foundSpeculation; ++i) {
      if (i->second.id == "speculation") foundSpeculation = true;
    }
  }
  hasSpeculation = foundSpeculation;

  /* collect operand val type and create accessor parameter lists.  We
   * had to wait to create these lists because the accessor types might
   * have been overridden.
   *
   * Also, if speculation is present, insert code as necessary.
   */
  opvaluetype_t &ov = opvaluetype;
  for (accessors_t::iterator 
	 i = accessors.begin(); i != accessors.end(); ++i) {
    if (!ov.count(i->second.ovfield)) {
      ov[i->second.ovfield] = i->second.ctype;
    } else if (ov[i->second.ovfield] != i->second.ctype) {
      LIS_user::reportError(errToString(inconsistent_operand_value_type),
			    i->second.loc);
      continue;
    }

    accessor_t &acc = i->second;

    // determine which fields are present in accessor text

    for (accessor_codes_t::iterator
	   j = acc.codes.begin(); j != acc.codes.end(); j++) {
      find_implicit(j->second, fields);
    }

    // Fix the accessors to do backup and the like...
    if (foundSpeculation) {

      if (acc.codes.count("write")) { 
	accessor_code_t &acd = acc.codes["write"];

	// Insert backup code
	if (!acc.codes.count("backup")) {
	  std::string s = "if (backup) *backup = read(LIS_ii, false";

	  // now handle implicit fields
	  for (implicitfields_t::iterator
		 k = acc.codes["read"].implicitfields.begin(), 
		 ke = acc.codes["read"].implicitfields.end(); k != ke ; ++k) {
	    s += ", " + *k;
	  }
	  
	  std::string p = find_pnames(acc.codes["read"].params);
	  if (p.size()) s += ", " + p;
	  s += ");\n";
	  s += acd.code;
	  acd.code = s;
	} else {
	  std::string s = (acc.codes["backup"].code + std::string("\n") +
			   acc.codes["write"].code);
	  acd.code = s;
	  acc.codes.erase("backup");
	}
	find_implicit(acd, fields); // redo the write accessor

	// Insert resolution code
	if (!acc.codes.count("resolve")) {
	  std::string s="  if (LIS_resolveOp == LSE_emu_resolveOp_rollback) " \
	    "write(LIS_ii, 0, *backup";

	  for (implicitfields_t::iterator
		 k = acd.implicitfields.begin(), 
		 ke = acd.implicitfields.end(); k != ke ; ++k) {
	    s += ", " + *k;
	  }

	  std::string p = find_pnames(acc.codes["write"].params);
	  if (p.size()) s += ", " + p;
	  s += ");\n";
	  s += "  return 0;\n";
	  accessor_code_t t = { "int", acd.params, acd.defnparams, 
				s, implicitfields_t(), acd.loc };
	  acc.codes["resolve"] = t;
	  find_implicit(acc.codes["resolve"],fields);
	}
      } else {
	acc.codes.erase("backup");
	acc.codes.erase("resolve");
      }
    } else {
      acc.codes.erase("backup");
      acc.codes.erase("resolve");
    }

    for (accessor_codes_t::iterator
	   j = i->second.codes.begin(); j != i->second.codes.end(); j++) {

      std::string rtype, ps;
      if (j->first == "read") {
	rtype = i->second.ctype;
	ps = "LSE_emu_instr_info_t &LIS_ii, bool isSpeculative";
      } else if (j->first == "write") {
	rtype = "void";
	ps = "LSE_emu_instr_info_t &LIS_ii, ";
	ps += i->second.ctype;
	ps += " *backup, ";
	ps += i->second.ctype;
	ps += " &data";
      } else if (j->first == "decode") { 
	rtype = "void";
	ps = "LSE_emu_instr_info_t &LIS_ii, LSE_emu_operand_info_t &oi";
      } else if (j->first == "resolve") { // resolve
	rtype = "int";
	ps = "LSE_emu_instr_info_t &LIS_ii, int LIS_resolveOp, ";
	ps += i->second.ctype;
	ps += " *backup";
      } else { // backup
      }
      std::string cs = ps;

      // now handle implicit fields
      for (implicitfields_t::iterator
	     k = j->second.implicitfields.begin(), 
	     ke = j->second.implicitfields.end(); k != ke ; ++k) {
	field_t &f = fields[*k];
	ps += ", " + f.cname + "&";
	cs += ", " + f.cname + "& " + *k;
      }

      if (j->second.params != "" && j->second.params != "void") {
	ps += ", ";
	cs += ", ";
	ps += j->second.params;
	cs += j->second.defnparams;
      }
      j->second.params = ps;
      j->second.defnparams = cs;
      j->second.rettype = rtype;
    }
  }

  /* Update instruction pieces to insert implicit accessor parameters */
  
  for (iclasses_t::const_iterator i = iclasses.begin(); 
       i != iclasses.end(); i++) {

    for (actions_t::const_iterator j = i->second.actiondefs.begin();
	 j != i->second.actiondefs.end(); j++) {

      std::string::size_type it;
      if (!j->second) continue;
      std::string &code = j->second->code;
      
      while ( (it = code.find("%%%APARM(")) != std::string::npos) {
	std::string::size_type p2 = code.find(',',it+9);
	std::string::size_type p3 = code.find(')',it+9);
	std::string atype = code.substr(it+9, p2 - it - 9);
	std::string aname = code.substr(p2+1, p3 - p2 - 1);

	std::string r;
	if (accessors[aname].codes.count(atype)) {
	  accessor_code_t &acc = accessors[aname].codes[atype];

	  for (implicitfields_t::iterator
		 k = acc.implicitfields.begin(), 
		 ke = acc.implicitfields.end(); k != ke ; ++k) {
	    if (!fields.count(*k)) continue;
	    r += ", " + *k;
	  }
	}

	code.replace(it, p3 - it + 1, r);
      }
    }

  } // for iclasses

  /* Figure out how many operands there are */

  max_opers[0] = max_opers[1] = -1;

  for (std::map<std::string, operandname_t>::iterator i = opernames.begin(); 
       i != opernames.end(); ++i) {

    int otype = (int)i->second.opertype;
    max_opers[otype] = std::max(max_opers[otype],i->second.offset);
    oper_used[otype].insert(i->second.offset);
  }

  /* check that capabilities are implemented in only one place and
   * declare their entrypoints
   */

  stringset_t didcaps;
  std::set<int> usednumssrc, usednumsdest, usednumsbackup;

  for (buildsets_t::iterator j = buildsets.begin();
       j != buildsets.end(); ++j) {

    buildset_t &bs = j->second;
    if (bs.style == "unimplemented") continue;

    entrypoints_t &es = bs.entrypoints;

    for (implcaps_t::iterator i = bs.implCapabilities.begin();
	 i != bs.implCapabilities.end(); ++i) {
      
      implcap_t &ic = i->second;

      // make certain implemented in only one place
      if (didcaps.count(ic.id)) {
	LIS_user::reportError(errToString(duplicate_capability) + ic.id, 
			      ic.loc);
	continue;
      }
      didcaps.insert(ic.id);

      if (ic.id == "operandval") {

	for (std::map<std::string, operandname_t>::iterator 
	       i = opernames.begin(); i != opernames.end(); ++i) {

	  operandname_t &onm = i->second;
	  bool beforeP = onm.accessNo < ic.actionNo;

	  if (onm.opertype == oper_src) {
	    std::string ename = (std::string("LSE_emu_fetch_operand_") + 
				 to_string(onm.offset));
	      
	    if (!usednumssrc.count(onm.offset)) {
	      usednumssrc.insert(onm.offset);
	      es[ename] = new operacc_t(ename, ic.decodeToken, 
					foundSpeculation ? 4 : 0, onm.offset, 
					ic.actionNo, beforeP, !beforeP, 
					ic.loc);
	    } else {
	      dynamic_cast<operacc_t *>(es[ename])->beforeP |= beforeP;
	      dynamic_cast<operacc_t *>(es[ename])->afterP |= !beforeP;
	    }
	  } else if (onm.opertype == oper_dest) {
	    std::string ename = (std::string("LSE_emu_writeback_operand_") + 
				 to_string(onm.offset));
	      
	    if (!usednumsdest.count(onm.offset)) {
	      usednumsdest.insert(onm.offset);
	      es[ename] = new operacc_t(ename, ic.decodeToken, 
					foundSpeculation ? 2 : 1, onm.offset, 
					ic.actionNo, beforeP, !beforeP, 
					ic.loc);
	    } else {
	      dynamic_cast<operacc_t *>(es[ename])->beforeP |= beforeP;
	      dynamic_cast<operacc_t *>(es[ename])->afterP |= !beforeP;
	    }
	  }

	} // for operand names

      } // if operandval

      else if (ic.id == "speculation") {

	for (std::map<std::string, operandname_t>::iterator 
	       i = opernames.begin(); i != opernames.end(); ++i) {

	  operandname_t &onm = i->second;
	  bool beforeP = onm.accessNo < ic.actionNo;

	  if (onm.opertype == oper_dest) {

	    std::string ename2 = (std::string("LSE_emu_resolve_operand_") + 
				  to_string(onm.offset));
	      
	    if (!usednumsbackup.count(onm.offset)) {
	      usednumsbackup.insert(onm.offset);

	      es[ename2] = new operacc_t(ename2, ic.decodeToken, 3, 
					 onm.offset, ic.actionNo, beforeP, 
					 !beforeP, ic.loc);
	    } else {
	      dynamic_cast<operacc_t *>(es[ename2])->beforeP |= beforeP;
	      dynamic_cast<operacc_t *>(es[ename2])->afterP |= !beforeP;
	    }
	  }
	} // for operand names

      } // speculation
    } // for caps
  } // for buildsets

  if (foundSpeculation) {

    addField("LIS_operand_backed", "int", "", true, dummyloc);
    std::ostringstream os;
    os << "\naddedfields += [ ('LSE_emu_operand_data_t', 'LIS_operand_rb', "
       << "'[LSE_emu_max_operand_dest]', '') ]";
    addCode("description", os.str());

  } // foundSpeculation

  // Any steps in buildsets with speculation need to have speculation arg

  for (steps_t::const_iterator i = steps.begin(); i != steps.end(); ++i) {
    buildset_t &bs = buildsets[i->second.bsname];
    if (bs.capabilities.count("speculation")) {
      std::string sname3("LIS_step_"); sname3 += i->second.id;
      bs.entrypoints[sname3]->params += ", bool LIS_dospeculation";
    }
  }

  // Fix up the operand fetch/writeback call for speculation

  for (iclasses_t::iterator i = iclasses.begin(); i != iclasses.end(); ++i) {

    for (operands_t::const_iterator j = i->second.operands.begin();
	 j != i->second.operands.end(); j++) {

      const operandname_t &onm = *j->second.opername;
      
      if (onm.opertype != oper_dest) continue;
      
      std::string aname = onm.id + ".write";
      if (!i->second.actiondefs.count(aname)) continue;
      action_t *actp = i->second.actiondefs[aname];
   
      // note: there are weird inheritance cases where the action is deleted
      if (!actp) continue;

      if (foundSpeculation) {
	std::ostringstream os, os2;
	os << "(LIS_dospeculation && (!(LIS_ii.LIS_operand_backed & " 
	   << (1<<onm.offset) << ")) ? &LIS_ii.LIS_operand_rb["
	   << onm.offset << "]." << accessors[j->second.accessor].ovfield 
	   << " : 0)";
	os2 << "\nif (LIS_dospeculation) LIS_ii.LIS_operand_backed |= " 
	    << (1<<onm.offset) << ";\n";
	actp->code.replace(actp->code.find("LIS_backup_data_ptr"),19,os.str());
	actp->code += os2.str();
      } else {
	actp->code.replace(actp->code.find("LIS_backup_data_ptr"),19,"0");
      }
    }
  }

  if (LIS_user::errorCount()) return true;
  
  semantic_error_t serr;
  serr = createDecoding();
  if (serr) { 
    myloc_t dummyloc = { "<no file>", 0, 0, 0, 0 };
    LIS_user::reportError(errToString(serr), dummyloc);
    return true;
  }
  return false;
}

/********************* Global stuff **********************/

semantic_error_t 
LIS_isa::addConstant(const char *name, const bool isOption, cval_t **cp,
		     const myloc_t loc)
{
  std::string name2(name);
  if (isOption) {
    if (options.count(name2)) {
      *cp = &options[name2];
      return options[name2].used ? redefined_symbol : no_error;
    } else {
      cval_t t = { 0, false };
      options[name2] = t;
      *cp = &options[name2];
    }
  } else {
    if (constants.count(name2)) {
      *cp = &constants[name2];
      return constants[name2].used ? redefined_symbol : no_error;
    } else {
      cval_t t = { 0, false };
      constants[name2] = t;
      *cp = &constants[name2];
    }
  }
  return no_error;
}

semantic_error_t 
LIS_isa::addConstant(const char *bname, const char *name,
		     const bool isOption, cval_t **cp,
		     const myloc_t loc)
{
  std::string name2(name);
  buildset_t &bs = buildsets[bname];
  if (isOption) {
    if (bs.options.count(name2)) {
      *cp = &bs.options[name2];
      return bs.options[name2].used ? redefined_symbol : no_error;
    } else {
      cval_t t = { 0, false };
      bs.options[name2] = t;
      *cp = &bs.options[name2];
    }
  } else {
    if (bs.constants.count(name2)) {
      *cp = &bs.constants[name2];
      return bs.constants[name2].used ? redefined_symbol : no_error;
    } else {
      cval_t t = { 0, false };
      bs.constants[name2] = t;
      *cp = &bs.constants[name2];
    }
  }
  return no_error;
}

semantic_error_t 
LIS_isa::addSConstant(const char *bname, const char *name,
		      const bool isOption, cval_t **cp,
		      const myloc_t loc)
{
  std::string name2(name);
  style_t &bs = styles[bname];
  if (isOption) {
    if (bs.options.count(name2)) {
      *cp = &bs.options[name2];
      return bs.options[name2].used ? redefined_symbol : no_error;
    } else {
      cval_t t = { 0, false };
      bs.options[name2] = t;
      *cp = &bs.options[name2];
    }
  } else {
    if (bs.constants.count(name2)) {
      *cp = &bs.constants[name2];
      return bs.constants[name2].used ? redefined_symbol : no_error;
    } else {
      cval_t t = { 0, false };
      bs.constants[name2] = t;
      *cp = &bs.constants[name2];
    }
  }
  return no_error;
}

void 
LIS_isa::fillSymbolTable(const std::string &bname, cvalsP_t *stab) {
  if (!buildsets.count(bname)) return;
  buildset_t &bs = buildsets[bname];
  for (cvals_t::iterator 
	 i = bs.options.begin(); i != bs.options.end(); ++i) {
    (*stab)[i->first] = &i->second;
  }
  for (cvals_t::iterator 
	 i = bs.constants.begin(); i != bs.constants.end(); ++i) {
    (*stab)[i->first] = &i->second;
  }
}

void 
LIS_isa::fillSSymbolTable(const std::string &bname, cvalsP_t *stab) {
  if (!styles.count(bname)) return;
  style_t &bs = styles[bname];
  for (cvals_t::iterator 
	 i = bs.options.begin(); i != bs.options.end(); ++i) {
    (*stab)[i->first] = &i->second;
  }
  for (cvals_t::iterator 
	 i = bs.constants.begin(); i != bs.constants.end(); ++i) {
    (*stab)[i->first] = &i->second;
  }
}

void 
LIS_isa::addCode(const std::string &name, const std::string &value,
		 bool append, const myloc_t &loc)
{
  std::ostringstream os;
  if (name != "description" && name != "makefile") os << "// ";
  else os << "# ";
  os << loc << std::endl;
  if (append) code[name] += os.str();
  else code[name] = os.str();
  code[name] += value;
}

void 
LIS_isa::addCode(const std::string &name, const std::string &value)
{
  code[name] += value;
}

namespace {
  std::string find_id(std::string s) {
    // the id is the last id before the first completed set of parenthesis, 
    // if there are parenthesis.  Square brackets are counted as parenthesis,
    // but we don't check for good matching

    bool inword = false, foundpar=false;
    int firstchar=0, lastchar=0, pcnt=0;
    for (unsigned int i=s.size(); i!=0; i--) {
      char c = s[i-1];
      if ((c == ')' || c==']') && !foundpar) { pcnt++; }
      else if ((c == '(' || c== '[') && !foundpar) { 
	if (!--pcnt) { foundpar = true; }
      }
      else if (pcnt) continue;
      else if (std::isalpha(c) || c == '_' || std::isdigit(c)) {
	foundpar = true;
	if (inword) firstchar = i;
	else { lastchar = firstchar = i; inword = true;}
      } else if (inword) break;
    }

    return s.substr(firstchar-1, lastchar+1-firstchar);
  }
} // anonymous namespace

void 
LIS_isa::addType(const std::string &tdef1, const std::string &tdef2, 
		 const myloc_t &loc) 
{
  std::string tdefn(tdef1);
  tdefn += " "; tdefn += tdef2;
  std::string tname = find_id(tdefn);
  if (!typedefs.count(tname)) typeOrder.push_back(tname);
  typedef_t &td = typedefs[tname];
  td.ttype = scalar;
  td.strdef = tdefn;
  td.id = tname;
  td.loc = loc;
}

void
LIS_isa::removeType(const std::string &tname, const myloc_t &loc) 
{
  typeOrder.remove(tname);
  typedefs.erase(typedefs.find(tname));
}

void
LIS_isa::addStructField(const std::string &tname, 
			const std::string &fdef, const myloc_t &loc)
{
  bool isnew = typedefs.count(tname) == 0;
  typedef_t &td = typedefs[tname];
  if (isnew) {
    typeOrder.push_back(tname);
    td.id = tname;
    td.loc = loc;
    td.ttype = structdef;
  }
  std::string fname = find_id(fdef);

  if (!td.defn.count(fname)) td.defnOrder.push_back(fname);
  structentry_t &s = td.defn[fname];
  s.name = fname;
  s.defn = fdef;
  s.loc = loc;
}

void
LIS_isa::removeStructField(const std::string &tname, const std::string &fname, 
			   const myloc_t &loc)
{
  if (!typedefs.count(tname)) return;
  typedef_t &td = typedefs[tname];
  td.defnOrder.remove(fname);
  td.defn.erase(td.defn.find(fname));
}

void
LIS_isa::addEnumValue(const std::string &tname, const std::string &ename,
		      const std::string &edef, const myloc_t &loc)
{
  bool isnew = typedefs.count(tname) == 0;
  typedef_t &td = typedefs[tname];
  if (isnew) {
    typeOrder.push_back(tname);
    td.id = tname;
    td.loc = loc;
    td.ttype = enumdef;
  }
  std::string fname(ename);
  if (!td.defn.count(fname)) td.defnOrder.push_back(fname);
  structentry_t &s = td.defn[fname];
  s.name = fname;
  s.defn = ename;
  if (edef[0]) { s.defn += " = "; s.defn += edef; }
  s.loc = loc;
}

void
LIS_isa::removeEnumValue(const std::string &tname, const std::string &ename,
			 const myloc_t &loc)
{
  if (!typedefs.count(tname)) return;
  typedef_t &td = typedefs[tname];
  td.defnOrder.remove(ename);
  td.defn.erase(td.defn.find(ename));
}

void
LIS_isa::addOperandName(const std::string &name, const std::string &opertype, 
			const int offset, const int decodeNo, 
			const int accessNo, const myloc_t &loc) 
{
  if (fields.count(name)) 
    throw semantic_exception(redefined_field_as_opername, loc);
  if (offset < 0) throw semantic_exception(invalid_operand_offset, loc);
  else {
    opertype_t opertype2;
    
    if (opertype == "src") {
      opertype2 = LIS::oper_src;
    } else if (opertype == "dest") {
      opertype2 = LIS::oper_dest;
    } else throw semantic_exception(unknown_oper_type, loc);

    if (opernames.count(name)) {
      operandname_t& oln = opernames[name];
      if (opertype2 != oln.opertype) 
	throw semantic_exception(inconsistent_oper_type, loc);
    }

    operandname_t n = { name, opertype2, offset, decodeNo, accessNo, loc };
    opernames[name] = n;
  }
}

namespace {

  inline
  std::string strip_defaults(std::string p) {
    std::string rval;
    if (p == "void" || p == "") return std::string("");
    int pcnt = 0, firstchar=0, lastchar=0;
    bool inword = false;
    for (unsigned int i = 0; i < p.size(); i++) {
      char c = p[i];
      if (c == ',' && !pcnt) { 
	if (rval.size()) rval += ", ";
	if (!inword) lastchar = i-1;
	rval += p.substr(firstchar, lastchar+1-firstchar);
	inword = false;
	firstchar = i+1;
      }
      else if (c == '(') { pcnt++; }
      else if (c == ')') { pcnt--; }
      else if (c == '=') { 
	if (pcnt) {}
	else {
	  if (!inword) lastchar = i-1;
	  inword = true;
	}
      }
    }
    if (rval.size()) rval += ", ";
    if (!inword) lastchar = p.size()-1;
    rval += p.substr(firstchar, lastchar+1-firstchar);
    return rval;
  } // strip_defaults
}

void
LIS_isa::addAccessor(const std::string &ctype, const std::string &ovfield,
		     const std::string &name, const std::string &atype, 
		     const std::string &params, const std::string &code, 
		     const myloc_t &loc) 
{
  
  if (!validAccessors.count(atype)) 
    throw semantic_exception(unknown_accessor_type, loc);

  accessor_t *ac;
  accessor_t n = { name, accessor_codes_t(), ctype, ovfield, loc };
  if (!accessors.count(name)) {
    accessors[name] = n;
  } else {
    accessor_t &oac = accessors[name];
    oac.ctype = n.ctype;
    oac.ovfield = n.ovfield;
    oac.loc = n.loc;
  }
  ac = &accessors[name];

  // strip whitespace from either end so void and empty can be properly
  // recognized
  std::string pstring(params);
  while (pstring.size() && isspace(pstring[0])) pstring.erase(0,1);
  while (pstring.size() && isspace(pstring[pstring.size()-1])) 
    pstring.erase(pstring.size()-1,1);

  accessor_code_t t = { "", pstring, strip_defaults(pstring), code, 
			implicitfields_t(), loc };
  ac->codes[atype] = t;
}

void
LIS_isa::addField(const std::string &fname, const std::string &ctype,
		  const std::string &code, const bool addDef,  
		  const bool isExpr, const myloc_t &loc)
{
  // NOTE: redefinition of fields is okay because they're open, but
  // operand names should not be redefined.
  if (opernames.count(fname)) 
    throw semantic_exception(redefined_opername_as_field, loc);

  field_t f = {fname, ctype, code, false, addDef, isExpr, loc };
  if (fields.count(fname)) {
    fields[fname] = f;
  } else {
    fields[fname] = f;
    fieldsOrdered.push_back(&fields[fname]);
  }
}

void
LIS_isa::addField(const std::string &fname, const std::string &ctype,
		  const std::string &code, const bool addDef, 
		  const myloc_t &loc)
{
  LIS_isa::addField(fname, ctype, code, addDef, false, loc);
}

void
LIS_isa::setPredecode(const std::string &fname, const bool setIt, 
		      const myloc_t &loc)
{
  if (!fields.count(fname)) 
    throw semantic_exception(undefined_field, loc);

  fields[fname].predecode = setIt;
}

/********************* Instruction class ************************/

void
LIS_isa::addIClass(const std::string &name, const bool isInstruction, 
		   const myloc_t &loc) 
{
  if (iclasses.count(name)) return; // reopening class

  iclass_t i = { name, isInstruction ? name : "", 
		 iclass_set_t(), iclass_set_t(),
		 sequence_t(), 
		 actions_t(), bitfields_t(), 
		 operands_t(), matchSet_t(), loc, isInstruction, 0, 
		 DEFAULT_FREQ, false, matchinfo_t()};
  i.matchinfo.push_back(matchinfo_t());
  iclasses[name] = i;
  if (name != "ALL") {
    addParent(name, "ALL", loc);
  }
}

void
LIS_isa::setOpcode(const std::string &iname, const std::string &name, 
		   const int piece, const myloc_t &loc) 
{
  if (!iclasses.count(iname)) 
    throw semantic_exception(undefined_iclass, loc);

  iclass_t &ic = iclasses[iname];
  ic.opcode = name;
  
  // open semantics
  for (iclass_set_t::iterator
	 i = ic.usedby_class.begin(); i!= ic.usedby_class.end(); ++i) {
    (*i)->opcode = ic.opcode;
  }
}

void 
LIS_isa::setIFlag(const std::string &cname, const bool newFlag,
		  const myloc_t &loc) {
  if (!iclasses.count(cname)) 
    throw semantic_exception(undefined_iclass, loc);

  iclass_t &ic = iclasses[cname];
  ic.isInstruction = newFlag;
  
  // open semantics
  for (iclass_set_t::iterator
	 i = ic.usedby_class.begin(); i!= ic.usedby_class.end(); ++i) {
    (*i)->isInstruction = newFlag;
  }
}

/********************* Actions ************************/

void
LIS_isa::addAction(const std::string &pname, const std::string& aname,
		   const bool replace, const bool disconnect, 
		   const bool forceentry, const int seqNo,
		   const std::string &code, const myloc_t &loc)
{
  if (!iclasses.count(pname)) 
    throw semantic_exception(undefined_iclass, loc);

  iclass_t &parent = iclasses[pname];
  actions_t &actiondefs = parent.actiondefs;

  action_t *ap = actiondefs[aname];
  bool haddefn = ap != 0;

  if (haddefn) {

    if (replace) { ap->code = code; ap->loc = loc; }
    else ap->code += code;
    parent.addSeqElement(ap, seqNo, disconnect, true);

  } else {

    actiondefs[aname] = ap = new action_t();
    actions.push_back(ap);
    
    action_t a = { std::string(pname)+std::string(":") + 
		   aname, aname, code, loc};
    *ap = a;
    
    parent.addSeqElement(ap, seqNo, disconnect, true);

  }
    
  // open semantics: current children must have this action

  for (iclass_set_t::iterator i = parent.usedby_class.begin(); 
       i != parent.usedby_class.end(); ++i) {
    (*i)->addSeqElement(ap, seqNo, false, true);
  }

} // LIS_isa::addAction

/********************* Class refs ************************/

namespace {

  void
  merge_matchinfo(matchinfo_t &to, const matchinfo_t &from, const myloc_t &loc)
  {
    for (matchinfo_t::const_iterator i = from.begin();
	 i != from.end(); ++i) {

      matchval_t &mv = to[i->first];

      // if any differences in bits defined in both, it is an error
      if ((mv.mask & i->second.mask) & (mv.val ^ i->second.val)) {
	//mv.mask &= ~i->second.mask;
	//mv.val &= ~i->second.mask;
	throw semantic_exception(incompatible_decoding, loc);
      } else {
	mv.mask |= i->second.mask;
	mv.val |= i->second.val;
      }
    
    } /* for i in matchinfo */
  }

  void
  merge_matchinfo(matchSet_t &to, const matchSet_t &from, const myloc_t &loc)
  {
    matchSet_t resSet;
    if (SHOW_SETOPS)
      std::cerr << "Merging " << to << " with " << from << std::endl;

    for (matchSet_t::const_iterator i = to.begin(); i != to.end(); ++i) 
      for (matchSet_t::const_iterator j = from.begin(); j != from.end(); j++) {
	matchinfo_t tmp = *i;
	merge_matchinfo(tmp, *j, loc);
	// will do merging, but won't say if there were overlaps
	add_to_matchset(resSet, tmp); 
      }

    to = resSet;
  } // merge_matchinfo

} // namespace $$

void
LIS_isa::addParent(const std::string &cname, const std::string &pname,
		   const myloc_t &loc) 
{
  if (!iclasses.count(cname)) throw semantic_exception(undefined_iclass,loc);
  if (!iclasses.count(pname)) throw semantic_exception(undefined_iclass,loc);
  
  iclass_t *parent = &iclasses[pname];
  iclass_t &child = iclasses[cname];

  if (cname == pname || 
      find(parent->classes.begin(), parent->classes.end(), 
	   &child) != parent->classes.end()) {
    // circular inheritance is not good; I cannot just ignore them because
    // a statement like instructionlist a [ b a ] = cond will leave a mess
    // for the matches of b
    throw semantic_exception(circular_inheritance,loc);
  }

  child.classes.insert(parent);
  parent->usedby_class.insert(&child);

  // merge in all instruction classes from previous one.  This forms a
  // strict hierarchy of classes.
  child.classes.insert(parent->classes.begin(),parent->classes.end());
  for (iclass_set_t::iterator i = parent->classes.begin(); 
       i != parent->classes.end();
       ++i) (*i)->usedby_class.insert(&child);

  // merge in opcode
  if (parent->opcode.size()) child.opcode += parent->opcode;

  // merge in bitfields and match information
  child.bitfields.insert(parent->bitfields.begin(),parent->bitfields.end());
  
  merge_matchinfo(child.matchinfo,parent->matchinfo,loc);

  // merge operands.  If we've defined an operand in a child and the
  // new parent defines the operand, too.  The sequence elements
  // corresponding to the redefined operand should override previous
  // ones.  The easiest way to make this work is to just delete the extra
  // child operands.  Then the sequence elements will just point at the
  // parent class accessor definitions.
  for (operands_t::const_iterator i = parent->operands.begin();
       i != parent->operands.end(); ++i) {
    removeOperand(cname, i->first, i->second.loc);
  }
  child.operands.insert(parent->operands.begin(), parent->operands.end());

  // merge sequence.  Need to be careful not to double-merge something
  // when inherited through multiple paths.  Cannot use the unique
  // function because the order of sequence element addition matters when
  // the numbers are the same.
  for (sequence_t::const_iterator 
	 i = parent->sequence.begin(); i != parent ->sequence.end(); ++i) {
    child.addSeqElement(i->action, i->seqNo, false, false);
  }
  child.sequence.sort(sequence_elem_t::stableCompare());

  // open semantics
  for (iclass_set_t::iterator
	 i = child.usedby_class.begin(); i!= child.usedby_class.end(); ++i) {
    addParent((*i)->id.c_str(), pname, loc);
  }
}

void
LIS_isa::removeParent(const std::string &cname, 
		      const std::string &pname, const myloc_t &loc)
{
  iclass_t *parent;

  if (!iclasses.count(cname)) throw semantic_exception(undefined_iclass,loc);
  if (!iclasses.count(pname));

  parent = &iclasses[pname];
  if (parent) {
    iclass_t &child = iclasses[cname];
    child.classes.erase(parent);
    parent->usedby_class.erase(&child);
  }
}

namespace {
  typedef std::vector<std::string> itervec_t;

  void
  generate_cross(const std::string &pname, itervec_t &iv, 
		 const unsigned int n, 
		 bool overrideIFlag,
		 const strlistvec_t &sll, LIS_isa *ip, const myloc_t &loc) {

    if (n == sll.size()) {
      // form a class name
      std::string cname(pname);
      for (itervec_t::iterator i = iv.begin(); i != iv.end(); ++i)
	cname += *i;

      // if already exists, don't regenerate
      if (ip->iclasses.count(cname)) {
	if (overrideIFlag) ip->setIFlag(cname,true, loc);
	return;
      }

      //std::cerr << "Generating " << cname << "\n";

      ip->addIClass(cname.c_str(), true, loc);
      ip->iclasses[cname].opcode = ""; // clear out opcode

      // will set new class opcode to parent
      ip->addParent(cname.c_str(), pname.c_str(), loc);

      for (itervec_t::iterator i = iv.begin(); i != iv.end(); ++i) {
	// NOTE: will suffix any opcodes from other parents
	ip->addParent(cname.c_str(), i->c_str(), loc);	
      }
    } else {
      for (strlist_t::const_iterator 
	     i = sll[n].begin(); i != sll[n].end(); ++i) {
	iv[n] = *i;
	generate_cross(pname, iv, n+1, overrideIFlag, sll, ip, loc);
      }
    }
    return;
  } // generate_cross

} // anonymous namespace

void
LIS_isa::performCross(const std::string &pname, bool overrideIFlag,
		      const strlistvec_t &sll,
		      const myloc_t &loc)
{
  if (!iclasses.count(pname)) throw semantic_exception(undefined_iclass,loc);

  iclass_t &ic = iclasses[pname];
  iclass_set_t ics(ic.usedby_class); // copy the used-by set

  itervec_t iv(sll.size());
  if (iclasses[pname].isInstruction) {
    generate_cross(pname, iv, 0, overrideIFlag, sll, this, loc);
  }
  for (iclass_set_t::iterator i = ics.begin(); i != ics.end(); ++i) {
    if ((*i)->isInstruction) {
      generate_cross((*i)->id, iv, 0, overrideIFlag, sll, this, loc);
    }
  }
}

/********************* Operands ************************/

void
LIS_isa::addOperand(const std::string &pname, const std::string &oname,
		    const std::string &accessor, const std::string &parmlist, 
		    const myloc_t &loc)
{

  if (!iclasses.count(pname)) 
    throw semantic_exception(undefined_iclass, loc);
  if (!opernames.count(oname)) 
    throw semantic_exception(undefined_opername, loc);
  if (!accessors.count(accessor))
    throw semantic_exception(undefined_accessor, loc);

  operandname_t &onm = opernames[oname];
  int decodeNo = onm.decodeNo;
  int actionNo = onm.accessNo;

  std::string parmstr;

  if (parmlist != "" && parmlist != "void") {
    parmstr = ", ";
    parmstr += parmlist;
  }
  
  operand_t o = { oname, accessor, parmstr, decodeNo, actionNo, loc, 0, 0};
  
  iclass_t &parent = iclasses[pname];

  if (parent.operands.count(oname)) { /* delete the old one */
    removeOperand(pname, oname, loc);
    // open semantics
    for (iclass_set_t::iterator
	   i = parent.usedby_class.begin(); i!= parent.usedby_class.end(); ++i)
      removeOperand((*i)->id, oname, loc);
  }

  parent.operands[oname] = o;

  // open semantics
  for (iclass_set_t::iterator
	 i = parent.usedby_class.begin(); i!= parent.usedby_class.end(); ++i) {
    (*i)->operands[oname] = o;
  }

  accessor_t &acc = accessors[accessor];

  if (decodeNo >= 0) {  
    if (!acc.codes.count("decode")) 
      throw semantic_exception(undefined_accessor, loc);

    std::string aname = std::string(oname) + ".decode";
    std::ostringstream os;
    os << "LSEemu_accessors::" << accessor 
       << "::decode(LIS_ii, LIS_oper_info_" << oname 
       << " %%%APARM(decode," << accessor << ")"
       << parmstr << ");\n";
    addAction(pname, aname, true, false, true, decodeNo, os.str(), loc);
  }

  if (actionNo >= 0) {

    if (onm.opertype == oper_src) {
      // form code for read action  
      if (!acc.codes.count("read")) 
	throw semantic_exception(undefined_accessor, loc);

      std::string aname = std::string(oname) + ".read";
      std::ostringstream os;
      os << oname << " = " << "LSEemu_accessors::" << accessor 
	 << "::read(LIS_ii, LIS_dospeculation" 
	 << " %%%APARM(read," << accessor << ")"
	 << parmstr << ");" << std::endl
	 << "LIS_oper_valid_" << oname << " = true;" << std::endl;
      addAction(pname, aname, true, false, true, actionNo, os.str(), loc);

    } else if (onm.opertype == oper_dest) {

      // form code for write action  
      if (!acc.codes.count("write")) 
	throw semantic_exception(undefined_accessor, loc);

      {
	std::string aname = std::string(oname) + ".write";
	std::ostringstream os;
	// TODO: problem is that type definitions don't exist!
	os << "  LSEemu_accessors::" << accessor 
	   << "::write(LIS_ii, LIS_backup_data_ptr, " 
	   << oname 
	   << " %%%APARM(write," << accessor << ")"
	   << parmstr << ");" << std::endl
	   << "LIS_oper_valid_" << oname << " = true;" << std::endl;
	
	addAction(pname, aname, true, false, true, actionNo, os.str(), loc);
      }

      {
	std::string aname = std::string(oname) + ".resolve";
	std::ostringstream os;
	os << "return LSEemu_accessors::" << accessor 
	   << "::resolve(LIS_ii, LIS_resolveOp, "
	   << "&LIS_ii.LIS_operand_rb[" << onm.offset << "]." 
	   << acc.ovfield 
	   << " %%%APARM(resolve," << accessor << ")"
	   << parmstr << ");" << std::endl;
	addAction(pname, aname, true, false, true, -1, os.str(), loc);
      }
    }
  } // if actionNo >= 0
}

void
LIS_isa::removeOperand(const std::string &pname, const std::string &oname,
		       const myloc_t &loc)
{
  if (!iclasses.count(pname)) return;
  if (!opernames.count(oname)) return;

  //std::cerr << "Removing " << pname << "/" << oname << std::endl;
  iclass_t &parent = iclasses[pname];

  if (!parent.operands.count(oname)) return;

  operand_t &op = parent.operands[oname];
  operandname_t &onm = opernames[oname];

  //std::cerr << op.decodeNo << " " << op.actionNo << std::endl;

  // remove sequence elements
  {  
    std::string aname = std::string(oname) + ".decode";
    if (op.decodeNo >= 0) {
      parent.removeSeqElement(aname);
      parent.actiondefs[aname] = 0;
    }
  }
  if (onm.opertype == oper_src) {
    std::string aname = std::string(oname) + ".read";
    if (op.actionNo >= 0) {
      parent.removeSeqElement(aname);
      parent.actiondefs[aname] = 0;
    }

  } else if (onm.opertype == oper_dest) {
    if (op.actionNo >= 0) {
      std::string aname = std::string(oname) + ".write";
      parent.removeSeqElement(aname);
      parent.actiondefs[aname] = 0;
      std::string aname3 = std::string(oname) + ".resolve";
      parent.removeSeqElement(aname3);
      parent.actiondefs[aname3] = 0;
    }
  }

  parent.operands.erase(oname);
}

/********************* Sequence elements ************************/

void
iclass_s::addSeqElement(const action_t *ap, const int seqNo,
			const bool replace, const bool dosort) {

  sequence_elem_t nv = sequence_elem_t(seqNo, const_cast<action_t *>(ap));

  if (replace) {
    sequence.remove_if(sequence_elem_t::matchesName(ap->name));
    sequence.push_back(nv);
  } else {
    sequence_t::iterator si;
    si = std::find_if(sequence.begin(), sequence.end(),
		      sequence_elem_t::matches(ap));

    // We don't want to repeat an action pointer inherited from multiple
    // classes.
    if (si == sequence.end()) sequence.push_back(nv);
    else return;
  }
  if (dosort) sequence.sort(sequence_elem_t::stableCompare());
}

void
iclass_s::removeSeqElement(const std::string &aname) {
  sequence.remove_if(sequence_elem_t::matchesName(aname));
}

/************************* formats and matches *************************/

void
LIS_isa::addBitfield(const std::string &pname, const fmtfield_t &f,
		     const myloc_t &loc)
{
  std::string fname2(f.id);

  if (!iclasses.count(pname)) throw semantic_exception(undefined_iclass, loc);

  iclass_t &parent = iclasses[pname];
  bitfields_t &bitfields = parent.bitfields;

  /* now, if it is there, we do different things than if it isn't:
   * not present, no range def: error
   * not present, range def: define the bitfield
   * present, no range def: only redefine default value
   * present, range def: redefine bitfield
   *
   */
  if (f.length < 0) throw semantic_exception(invalid_rangespec, loc);

  bitfield_t newf = { fname2, std::string(f.field), f.from, f.length };
  bitfields[fname2] = newf;

  // open semantics
  for (iclass_set_t::iterator j = parent.usedby_class.begin();
       j != parent.usedby_class.end(); j++) {
    (*j)->bitfields[fname2] = newf;
  }

  if (f.defpresent) {
    fmtfieldlistlist_t fll;
    fmtfieldlist_t fl;
    fmtfield_t f2 = { f.id, f.field, 0, -1, f.defval, loc, f.defpresent };
    fl.push_back(f2);
    fll.push_back(fl);
    updateMatch(pname, fll, loc);
  }
}

void
LIS_isa::updateMatch(const std::string &pname, 
		     const LIS_parsedefs::fmtfieldlistlist_t& fll,
		     const myloc_t &loc, const bool recurse)
{
  if (!iclasses.count(pname)) throw semantic_exception(undefined_iclass, loc);

  iclass_t &parent = iclasses[pname];
  bitfields_t &bitfields = parent.bitfields;

  matchSet_t& originfo = parent.matchinfo;
  matchSet_t newinfo = matchSet_t();

  if (SHOW_SETOPS)
    std::cerr << "Updating " << pname << std::endl;

  for (fmtfieldlistlist_t::const_iterator 
	 i = fll.begin(); i != fll.end(); ++i) { // across the terms
    matchSet_t tmp = originfo;

    for (fmtfieldlist_t::const_iterator
	   j = i->begin(); j != i->end(); j++) {

      const fmtfield_t &f = *j;
      if (!bitfields.count(f.id)) 
	throw semantic_exception(undefined_bitfield, loc);

      bitfield_t &b = bitfields[f.id];

      // need to update the current match set from the bitfield.

      for (matchSet_t::iterator k = tmp.begin(); k != tmp.end(); k++) {

	matchinfo_t &mi = *k;
	
	if (f.defpresent) { // adding to the match 
	  
	  uint64_t bitsaffected, val;
  
	  if (f.length < 0) {
	    bitsaffected = ((static_cast<uint64_t>(1) << b.length) - 1) 
						      << b.from;
	    val = (static_cast<uint64_t>(f.defval) << b.from) & bitsaffected;
	  } else {
	    if (f.from < 0 || (f.length + f.from) > b.length) 
	      throw semantic_exception(invalid_rangespec, loc);

	    bitsaffected = ((static_cast<uint64_t>(1) << f.length) - 1) 
						      << (f.from + b.from);
	    val = (static_cast<uint64_t>(f.defval) << (f.from + b.from)) 
	      & bitsaffected;
	  }

	  matchval_t &mv = mi[b.field];
	  uint64_t conflicts = (mv.mask & bitsaffected) & (mv.val ^ val);
	  if (conflicts) {
	    // remove the conflicted bits so they can be overwritten by
	    // the new match.
	    mv.val &= ~conflicts;
	    //throw semantic_exception(incompatible_decoding, loc);
	  } 
	  // now update
	  mv.mask |= bitsaffected;
	  mv.val |= val;

	} else { // deleting from the match

	  uint64_t bitsaffected;

	  if (f.length < 0) {
	    bitsaffected = ((static_cast<uint64_t>(1) << b.length) - 1) 
						      << b.from;
	  } else {
	    if (f.from < 0 || (f.length + f.from) > b.length) 
	      throw semantic_exception(invalid_rangespec, loc);

	    bitsaffected = ((static_cast<uint64_t>(1) << f.length) - 1) 
						      << (f.from + b.from);
	  }

	  matchval_t &mv = mi[b.field];
	  mv.mask &= ~bitsaffected;
	  mv.val &= ~bitsaffected;
	  
	}
      } // for matchset
    } // for fields
    for (matchSet_t::iterator k = tmp.begin(); k != tmp.end(); k++) {
      add_to_matchset(newinfo, *k); // eliminates duplicates and simplifies
    }
  } // for terms

  parent.matchinfo = newinfo;

  // open semantics
  if (recurse) {
    for (iclass_set_t::iterator
	   i = parent.usedby_class.begin(); i!= parent.usedby_class.end(); 
	 ++i) {
      try {
	updateMatch((*i)->id.c_str(),fll,loc,false);
      } catch (semantic_exception) {}
    }
  }
}

void
LIS_isa::addFrequency(const std::string &iname, const int64_t value,
		      const myloc_t &loc)
{
  if (!iclasses.count(iname)) return;
  iclasses[iname].frequency += value;
}

/***************************** buildsets ****************************/

void
LIS_isa::addBuildSet(const char *bname, const myloc_t &loc) 
{ 
  if (buildsets.count(bname)) return;
  buildset_t b = { bname, "ALL", "unimplemented",
		   capabilities_t(), implcaps_t(), entrypoints_t(), 
		   hiddenFields_t(), decoders_t(), 
		   cvals_t(), cvals_t(), loc };
  buildsets[bname] = b;
}

void
LIS_isa::addHide(const std::string &bname, const std::string &fname, 
		 hide_status_t hidestatus, const myloc_t &loc)
{ 
  if (!buildsets.count(bname)) 
    throw semantic_exception(undefined_buildset,loc);
  if (buildsets[bname].style == "unimplemented") return;

  if (!fields.count(fname)) return; // not an error if not there

  hiddenFields_t &hf = buildsets[bname].hiddenFields;
  hide_t h = { &fields[fname], hidestatus };
  hf[fname] = h;
}

void
LIS_isa::addCapability(const std::string &bname, const std::string &cname,
		       const int actionNo, const std::string &decodetoken,
		       const myloc_t loc)
{ 
  if (!buildsets.count(bname)) 
    throw semantic_exception(undefined_buildset,loc);
  if (buildsets[bname].style == "unimplemented") return;
  capabilities_t &cl = buildsets[bname].capabilities;
  cl.insert(cname);

  if (decodetoken.size()) {
    implcap_t a = { cname, actionNo, decodetoken, loc };
    buildsets[bname].implCapabilities[cname] = a;
  }
}

void
LIS_isa::addEntrypoint(const std::string &bname, 
		       const std::string &rtype,
		       const std::string &ename,
		       const std::string &params,
		       const rangelist_t &befrange,
		       const std::string &dcode,
		       const rangelist_t &aftrange,
		       const myloc_t &loc)
{
  if (!buildsets.count(bname)) 
    throw semantic_exception(undefined_buildset,loc);
  if (buildsets[bname].style == "unimplemented") return;

  entrypoints_t &es = buildsets[bname].entrypoints;
  if (es.count(ename)) throw semantic_exception(redefined_entrypoint,loc);

  // strip whitespace from either end so void and empty can be properly
  // recognized and then form the proper parameter string with LIS_ii
  // The restrict qualifier is a tricky one, as it means that instructions
  // had better not use both reference parameters and instr_info locations
  // simultaneously.
  std::string pstring(params), pstring2("LSE_emu_instr_info_t &__restrict__ LIS_ii");
  while (pstring.size() && isspace(pstring[0])) pstring.erase(0,1);
  while (pstring.size() && isspace(pstring[pstring.size()-1])) 
    pstring.erase(pstring.size()-1,1);

  if (pstring.length() && pstring != "void") {
    pstring2 += ", ";
    pstring2 += pstring;
  }

  es[ename] = new entrypoint_t(ename, std::string(rtype), pstring2, 
			       befrange, dcode, aftrange, 
			       &buildsets[bname], loc);
}

void
LIS_isa::addDecoder(const std::string &bname, 
		    const std::string &dname,
		    const std::string &params,
		    const rangelist_t &arange,
		    const myloc_t &loc)
{
  if (!buildsets.count(bname)) 
    throw semantic_exception(undefined_buildset,loc);
  if (buildsets[bname].style == "unimplemented") return;

  decoders_t &es = buildsets[bname].decoders;

  if (es.count(dname)) throw semantic_exception(redefined_decoder, loc);

  // strip whitespace from either end so void and empty can be properly
  // recognized
  std::string pstring(params), pstring2("LSE_emu_instr_info_t &__restrict__ LIS_ii");
  while (pstring.size() && isspace(pstring[0])) pstring.erase(0,1);
  while (pstring.size() && isspace(pstring[pstring.size()-1])) 
    pstring.erase(pstring.size()-1,1);

  if (pstring.length() && pstring != "void") {
    pstring2 += ", ";
    pstring2 += pstring;
  }

  es[dname] = new decoder_t(dname, pstring2, arange, loc);
}

void
LIS_isa::addStep(const std::string &bname, const std::string &sname,
		 const int sno, const std::string &stype,
		 const rangelist_t &befrange, const std::string &dcode, 
		 const rangelist_t &aftrange, const myloc_t &loc)
{
  if (!buildsets.count(bname)) 
    throw semantic_exception(undefined_buildset,loc);
  if (buildsets[bname].style == "unimplemented") return;

  buildset_t& bs = buildsets[bname];
  //if (steps.count(sname2)) return redefined_step;
  if (sno < 0) throw semantic_exception(invalid_step_number,loc);

  steptypes_t stype2;
  if (stype == "front") stype2 = st_front;
  else if (stype == "back") stype2 = st_back;
  else throw semantic_exception(invalid_step_type, loc);

  step_t s = { bname, sname, sno, stype2, loc };
  steps[sno] = s;

  std::string sname3("LIS_step_"); sname3 += sname;
  bs.entrypoints[sname3] 
    = new entrypoint_t(sname3, "void", 
		       "LSE_emu_instr_info_t &__restrict__ LIS_ii",
		       befrange, dcode, aftrange, &bs, loc);
}

/*********************** Style handling ********************/

void
LIS_isa::addStyle(const std::string &sname, const myloc_t &loc) 
{ 
  if (styles.count(sname)) return;
  style_t s 
    = { sname, entry_t(), generators_t(), hiddenFields_t(), 
	specializedBitfields_t(), rangelist_t(), cvals_t(), cvals_t(), loc };
  styles[sname] = s;
}

void
LIS_isa::addEntry(const std::string &sname, const std::string &code,
		  const std::string &rtype, const std::string &eparms,
		  const myloc_t &loc)
{
  if (!styles.count(sname)) throw semantic_exception(undefined_style,loc);
  entry_t e = { code, strip_space(rtype), strip_space(eparms), loc };
  styles[sname].entry = e;
}


void 
LIS_isa::addGenerator(const std::string &sname, 
		      const std::string &secname,
		      const std::string &code,
		      const myloc_t &loc)
{
  if (!styles.count(sname)) throw semantic_exception(undefined_style,loc);

  std::ostringstream os;
  if (secname != "description" && secname != "makefile") os << "// ";
  else os << "# ";
  os << loc << std::endl;

  generator_t &g = styles[sname].generators[secname];
  g.code += os.str();
  g.code += code;
  g.loc = loc;
  g.section = secname;
}

void
LIS_isa::addSpecialize(const std::string &sname, const std::string &fname, 
		       const myloc_t &loc)
{ 
  if (!styles.count(sname)) 
    throw semantic_exception(undefined_style,loc);

  if (!fields.count(fname)) { // must be a bitfield
    styles[sname].specializedBitfields.insert(fname);
    return; // not an error if not there
  }

  hiddenFields_t &hf = styles[sname].specializedFields;
  hide_t h = { &fields[fname], nothidden };
  hf[fname] = h;
}

void 
LIS_isa::addExclude(const std::string &sname, const rangelist_t &excl,
		    const myloc_t &loc) {
  if (!styles.count(sname)) 
    throw semantic_exception(undefined_style,loc);
  styles[sname].excl = excl;
}

/************************* Error handling ******************************/

namespace LIS {
  const char *
  errToString(const semantic_error_t err) {
    switch (err) {
    case no_error: return "";
    case redefined_symbol: return "Redefined symbol";
    case unknown_type: return "Bad type name";
    case redefined_iclass: return "Duplicate instruction class";
    case undefined_iclass: return "Undefined instruction class";
    case circular_inheritance: return "Circular class inheritance";
    case unknown_class_type: return "Bad class type";
    case redefined_space: return "Duplicate state space";
    case undefined_space: return "Undefined state space";
    case unknown_space_type: return "Bad space type";
    case unknown_oper_type: return "Unknown operand type";
    case inconsistent_oper_type: return "Inconsistent operand type";
    case unknown_accessor_type: return "Unknown accessor type";
    case undefined_accessor: return "Undefined accessor name";
    case redefined_field: return "Duplicate field name";
    case undefined_field: return "Undefined field name";
    case redefined_action: return "Duplication action name";
    case undefined_action: return "Undefined action name";
    case redefined_opername_as_field: 
      return "Field cannot have same name as an operand";
    case redefined_field_as_opername:
      return "Operand cannot have same name as field";
    case invalid_operand_offset: return "Invalid operand mapping";
    case invalid_sequence_number: return "Invalid sequence number";
    case undefined_bitfield: return "Undefined bitfield name";
    case invalid_rangespec: return "Invalid range specification";
    case undefined_opername: return "Undefined operand name";
    case undefined_buildset: return "Undefined buildset name";
    case redefined_entrypoint: return "Duplicate entrypoint name";
    case redefined_step: return "Duplicate step name";
    case redefined_decoder: return "Duplicate decoder name";
    case invalid_step_number: return "Invalid step number";
    case invalid_step_type: return "Invalid step type";
    case incompatible_decoding:
      return "Incompatible decode match specifications";
    case undefined_style: return "Undefined style name";
    case missing_type: 
      return "Either space data type or value type must be given";
    case missing_steps:
      return "No buildset defines instruction steps";
    case duplicate_entrypoint:
      return "More than one buildset defines the same entrypoint";
    case duplicate_decoder:
      return "More than one buildset defines the same decoder";
    case inconsistent_operand_value_type:
      return "Inconsistent operand value field types in accessors";
    case operand_offset_conflict:
      return "Operand offset conflict";
    case must_implement_buildset:
      return "Buildsets nested in styles must have an implementation type";
    case no_decoding_found:
      return "Unable to find a valid decoding function -- should not happen!";
    case instruction_pattern_overlaps:
      return "Instruction patterns were overlapped -- cannot build decoder";

    case duplicate_capability:
      return "More than one buildset defines capability ";

    default: return "Unknown error type";
    }
  }

  namespace {

    // Need to do a sequence-diff on range lists
    //
    //        rrrrrrr            rrrrrrrrrrrr
    //   xxxx          xxxx
    //      xxxxxxxxxxxxx      xxx   xxxx  xxxx

    rangelist_t excludeRange(const rangelist_t &r, const rangelist_t &excl) {

      if (!excl.size()) return r;

      rangelist_t::const_iterator rp = r.begin();
      rangelist_t::const_iterator ep = excl.begin();

      rangelist_t nr;
      int f,l;

      while (rp != r.end()) {

	f = rp->from; 
	l = rp->from + rp->length - 1;

      noupdate:

	// range piece totally before excl piece
	if (ep == excl.end() || l < ep->from) {  
	  range_t n = { f, l - f + 1 };
	  nr.push_back(n);
	  ++rp; 
	  ep = excl.begin();
	  continue;
	}

	// range piece totally after excl piece
	if (ep->from + ep->length - 1 < f) { ++ep; goto noupdate; }

	// excl takes out first part (also covers excl removes all)
	if (f >= ep->from) { 
	  f = ep->from + ep->length; 
	  ep = excl.begin(); 
	  if (f > l) { ++rp; continue; }
	  goto noupdate; 
	}

	if (l >= ep->from) { 
	  range_t n = { f, ep->from - f };
	  nr.push_back(n);
	  f = ep->from + ep->length;
	  ep = excl.begin(); 
	  if (f > l) { ++rp; continue; }
	}
	goto noupdate;

      } // while (range left)

      return nr;
    }

    void get_sequence_text(std::ostream &f, const sequence_t &seq, 
			   const rangelist_t &r, const std::string prefix,
			   const rangelist_t &excl) {

      rangelist_t nr = excludeRange(r, excl);
      sequence_t::const_iterator si = seq.begin();
      rangelist_t::const_iterator ri = nr.begin();
      
      if (ri == nr.end()) return; // at the end
      
      while (true) {
	if (ri == nr.end()) return;
	if (si == seq.end()) { ri++; si = seq.begin(); continue; }
	if (si->seqNo < ri->from) { si++; continue; }
	if (si->seqNo > ri->from + ri->length - 1) 
	  { ri++; si = seq.begin(); continue; }
	
	action_t &act = *(si->action);
	f << prefix << "// action: " << act.id << " from " 
	  << act.loc << std::endl;
	f << prefix << act.code << std::endl;
	si++;
      }
    }

    bool rangeNotEmpty(const rangelist_t &r, const rangelist_t &excl) {
      rangelist_t nr = excludeRange(r, excl);
      return nr.size() != 0;
    }

    bool actionNotEmpty(int actionNo, const rangelist_t &excl) {
      for (rangelist_t::const_iterator ri = excl.begin();
	   ri != excl.end(); ++ri) 
	if (actionNo >= ri->from && actionNo <= ri->from + ri->length - 1)
	  return false;
      return true;
    }

  }

  // Code for entrypoint text manipulation

  bool entrypoint_t::hasBefore(const rangelist_t &excl) const { 
    return rangeNotEmpty(befrange, excl);
  }

  bool entrypoint_t::hasAfter(const rangelist_t &excl) const { 
    return rangeNotEmpty(aftrange, excl);
  }

  std::string entrypoint_t::beforeText(const iclass_t &iclass,
				       const std::string &p,
				       const rangelist_t &excl) const {
    std::stringstream ss;
    get_sequence_text(ss, iclass.sequence, befrange, p, excl);
    return ss.str();
  }

  std::string entrypoint_t::afterText(const iclass_t &iclass,
				      const std::string &p,
				      const rangelist_t &excl) const {
    std::stringstream ss;
    get_sequence_text(ss, iclass.sequence, aftrange, p, excl);
    return ss.str();
  }

  std::ostream& entrypoint_t::dump(std::ostream &os) const {
    os << "\tentry point " << rtype << " " << id << "(" << params 
       << ") befrange:";
    for (rangelist_t::const_iterator k = befrange.begin();
	 k != befrange.end(); k++) {
      if (k->length == 1) os << " " << k->from;
      else os << " " << k->from << ":" << (k->from + k->length - 1);
    }
    os << " dcode: " << dcode << " aftrange:";
    for (rangelist_t::const_iterator k = aftrange.begin();
	 k != aftrange.end(); k++) {
      if (k->length == 1) os << " " << k->from;
      else os << " " << k->from << ":" << (k->from + k->length - 1);
    }
    os << std::endl; 
    return os;
  }

  // Code for decoder text manipulation

  bool decoder_t::hasBefore(const rangelist_t &excl) const { 
    return false;
  }

  bool decoder_t::hasAfter(const rangelist_t &excl) const { 
    return rangeNotEmpty(range,excl);
  }

  std::string decoder_t::beforeText(const iclass_t &iclass,
				    const std::string &p,
				    const rangelist_t &excl) const {
    return "";
  }

  std::string decoder_t::afterText(const iclass_t &iclass,
				   const std::string &p,
				   const rangelist_t &excl) const {
    std::stringstream ss;
    get_sequence_text(ss, iclass.sequence, range, p, excl);
    return ss.str();
  }

  std::ostream& decoder_t::dump(std::ostream &os) const {
    os << "\tdecoder " << id << "(" << params << ") range:";
    for (rangelist_t::const_iterator k = range.begin();
	 k != range.end(); k++) {
      if (k->length == 1) os << " " << k->from;
      else os << " " << k->from << ":" << (k->from + k->length - 1);
    }
    os << std::endl; 
  }

  // Code for operand text manipulation

  bool operacc_t::hasBefore(const rangelist_t &excl) const { 
    return beforeP && actionNotEmpty(actionNo, excl);
  }

  bool operacc_t::hasAfter(const rangelist_t &excl) const { 
    return afterP && actionNotEmpty(actionNo, excl);
  }

  std::string operacc_t::beforeText(const iclass_t &iclass,
				    const std::string &p,
				    const rangelist_t &excl) const {
    if (!hasBefore(excl)) return "";

    for (operands_t::const_iterator 
	   i = iclass.operands.begin(); i != iclass.operands.end(); ++i) {
      const operandname_t &on = *i->second.opername;

      if (on.offset != offset || on.accessNo >= actionNo) continue;

      std::string aname = i->second.id;
      std::string extraprefix, suffix;

      switch (accesstype) {
      case 0: 
      case 4: 
	if (on.opertype == oper_dest) continue;
	aname += ".read"; break;
      case 1: 
      case 2:
	if (on.opertype == oper_src) continue;
	aname += ".write"; break;
      case 3: 
	if (on.opertype == oper_src) continue;
	aname += ".resolve"; break;
      default: continue;
      }

      actions_t::const_iterator ai = iclass.actiondefs.find(aname);
      if (ai != iclass.actiondefs.end() && ai->second) 
	return extraprefix + ai->second->code + suffix;
    }

    // need to put in a dummy return statement for .resolve
    if (accesstype == 3) return "return 0;";
    else return "";
  }

  std::string operacc_t::afterText(const iclass_t &iclass,
				   const std::string &p,
				   const rangelist_t &excl) const {
    if (!hasAfter(excl)) return "";

    for (operands_t::const_iterator 
	   i = iclass.operands.begin(); i != iclass.operands.end(); ++i) {
      const operandname_t &on = *i->second.opername;

      if (on.offset != offset || on.accessNo <= actionNo) continue;

      std::string aname = i->second.id;
      std::string extraprefix, suffix;

      switch (accesstype) {
      case 0: 
      case 4:
	if (on.opertype == oper_dest) continue;
	aname += ".read"; break;
      case 1: 
      case 2:
	if (on.opertype == oper_src) continue;
	aname += ".write"; break;
      case 3: 
	if (on.opertype == oper_src) continue;
	aname += ".resolve"; break;
      default: continue;
      }

      for (sequence_t::const_iterator 
	     j = iclass.sequence.begin(); j != iclass.sequence.end(); ++j) {
	if (j->action->name == aname) 
	  return extraprefix + p + j->action->code + suffix;
      }
	/*
      actions_t::const_iterator ai = iclass.actiondefs.find(aname);
      if (ai != iclass.actiondefs.end() && ai->second) 
	return ai->second->code;
	*/
    }

    // need to put in a dummy return statement for .resolve
    if (accesstype == 3) return "return 0;";
    else return "";
  }

  std::ostream& operacc_t::dump(std::ostream &os) const {
    return os;
  }

  
} // namespace LIS
