/*
 * Copyright (c) 2006-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
%defines
/*%
 *
 * Parser for LIS
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file contains the parser definition for LIS
 *
 */
%{
#define YYDEBUG 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <stack>
#include <string>
#include <iostream>

#include "LIS_parsedefs.h"

using namespace LIS;
using namespace LIS_user; // definitions which user of parser must supply
using namespace LIS_internal;
using namespace LIS_parsedefs;

extern int yylex(void);
bool LIS_internal::suppressed = false;

namespace {
   void yyerror(char const *s);
   IR_t *IR;
   std::stack<IR_t *> IRstack;
}

# define YYLLOC_DEFAULT(Current, Rhs, N)           \
        ((Current).filename     = (Rhs)[1].filename,  \
	 (Current).first_line   = (Rhs)[1].first_line,  \
         (Current).first_column = (Rhs)[1].first_column,        \
         (Current).last_line    = (Rhs)[N].last_line,   \
         (Current).last_column  = (Rhs)[N].last_column)

#if defined (__cplusplus)  
#if defined (__GNUC_MINOR__) && 2093 <= (__GNUC__ * 1000 + __GNUC_MINOR__)
  // fix problem with old bisons and C++
  #define __attribute__(a...)
#endif
#endif

%}
%union {
  char *str;
  LIS_parsedefs::range_t bitrange;
  LIS_parsedefs::identlist_t *identlist;
  LIS_parsedefs::caplist_t *caplist;
  LIS_parsedefs::cap_t cap;
  LIS_parsedefs::rangelist_t *rangelist;
  LIS_parsedefs::strlistvec_t *crosslist;
  LIS_parsedefs::strlist_t *cnamelist;
  LIS_parsedefs::fmtfield_t fmtfield;
  LIS_parsedefs::ctxsp_t ctxsp;
  LIS_parsedefs::cint_t cint;
  LIS_parsedefs::fmtfieldlist_t *fmtfieldlist;
  LIS_parsedefs::fmtfieldlistlist_t *fmtfieldlistlist;
  bool flag;
  struct {
    bool replace;
    bool disconnect;
    int val;  
  } actionops;
}
%verbose

/* constants */
%token <str> CODE
%token <cint> CINT

/* IDs */
%token <str> IDENT

/* keywords */
%token ACCESSOR
%token ACTION
%token BUILDSET
%token CAPABILITY
%token CLASSES
%token CODESECTION
%token CONSTANT
%token CROSS
%token DECODER
%token ELSE
%token ELSEIF
%token ENTRY
%token ENTRYPOINT
%token ENUMVALUE
%token EXCLUDE
%token FIELD
%token FORMAT
%token FREQUENCY
%token GENERATOR
%token HIDE
%token IF
%token IMPLEMENT
%token INSTRCLASS
%token INSTRCLASSLIST
%token INSTRUCTION
%token INSTRUCTIONLIST
%token MATCH
%token OPCODE
%token OPERAND
%token OPERANDNAME
%token OPTION
%token PREDECODE
%token SHOW
%token SPECIALIZE
%token STEP
%token STRUCTFIELD
%token STYLE
%token TEMPLATE
%token TYPEDEF

%token MAYASSIGN
%token GE
%token LE
%token NE
%token EQ
%token SSHL
%token SSHR
%token SLAND
%token SLOR
 
/* precedence for arithmetic operations */
%token NEGOP
%left ':' '?'
%left SLOR
%left SLAND
%left '|'
%left '^'
%left '&'
%left EQ NE
%left '<' '>' LE GE 
%left SSHL SSHR
%left '-' '+'
%left '*' '/' '%'
%left NEGOP '!'

%type <bitrange>      cbitrange
%type <identlist>     identlist
%type <identlist>     identlistnc
%type <fmtfield>      rangeadd
%type <fmtfield>      instrclasslistmatches
%type <cint>          cintexp
%type <actionops>     actionops
%type <str>           codeorid
%type <str>           getcode
%type <str>           getparmcode
%type <str>           getdefcode
%type <rangelist>     seqrangelist
%type <rangelist>     optseqrangelist
%type <str>           id
%type <crosslist>     crosshandler2
%type <cnamelist>     cnamelist
%type <fmtfield>         matchhandlerexpp
%type <fmtfieldlist>     matchhandlerexp
%type <fmtfieldlist>     matchhandlerexp2
%type <fmtfieldlistlist> matchhandler2
%type <flag>             optat
%type <caplist>       capabilitylist
%type <cap>           capabilitydef

%{

#include <map>
#include <string>
#include <vector>
#include "LIS_isa.h"

using namespace LIS;
using namespace LIS_user; // definitions which user of parser must supply
using namespace LIS_internal;
using namespace LIS_parsedefs;

namespace {
  cvalsP_t symbolTable;
  cvalsP_t symbolTableB;

  std::vector<bool> suppress;

  void form_suppressed(void) {
    suppressed = false;
    for (std::vector<bool>::const_iterator 
	   i = suppress.begin(); i != suppress.end(); i++)
      suppressed |= *i;
  }
} // anonymous namespace

%}

%%
desc      : { IR = &theISA->IR; } sections 
	;

sections  : |
          sections section
          ;

section   : 
          topdecls
          | iclassdecls
          | buildsetdecls
          | styledecls
	  | BUILDSET buildsethandler
	  | STYLE stylehandler
	  | CONSTANT { $<ctxsp>$.loc = ctxsp_t::in_top;} constanthandler ';'
	  | OPTION { $<ctxsp>$.loc = ctxsp_t::in_top;} optionhandler ';'
	  | ';' /* empty statement is OK */
	  | iftop 
	  | error
          ;

topdecls: 
	  /* top-level declarations allowed in buildsets and styles */
	  codesectionspec 
	  | typedefspec 
	  | enumvaluespec 
	  | structfieldspec
	  | operandnamespec
  	  | accessorspec
	  | fieldspec
 	  | predecodespec
	  | implementspec
	  ;

iclassdecls:
	  /* iclass definitions allowed in buildsets and styles */
	  iclassspec
	  | instrspec
	  | actionspec
	  | classesspec
	  | formatspec
	  | matchspec
	  | crossspec
	  | operandspec
	  | opcodespec
	  | frequencyspec
	  | instrclasslistspec
	  | instructionlistspec
	  ;

buildsetdecls:
          /* buildset definitions allowed at top level or styles */
          decoderspec
          | capabilityspec
          | entrypointspec
          | stepspec
          | hidespec
          | showspec
	  ;

styledecls:
	  /* style definitions allowed at toplevel */
	  generatorspec
	  | entryspec
	  | specializespec
	  | excludespec
	  ;

iftop:  IF '(' cintexp ')' 
	  { suppress.push_back($3 == 0); form_suppressed(); }
	iftop2

iftop2: '{' sections '}' { suppress.pop_back(); form_suppressed(); }
	| '{' sections '}' elsetop { suppress.pop_back(); form_suppressed(); }
	| '{' sections '}' elseiftop { suppress.pop_back();form_suppressed(); }
	;

elseiftop: ELSEIF '(' cintexp ')' 
	  { bool supp2 = suppress.back();
	    *(suppress.rbegin()) = !supp2;
	    suppress.push_back($3 == 0); 
	    form_suppressed(); 
	  }
	iftop2

elsetop: ELSE 
	  {
	    bool supp2 = suppress.back();
	    *(suppress.rbegin()) = !supp2;
	    form_suppressed();
	  }
	'{' sections '}'

ificlass:  IF '(' cintexp ')' 
	  { suppress.push_back($3 == 0); form_suppressed(); 
	    $<ctxsp>$ = $<ctxsp>0;}
	ificlass2

ificlass2: '{' { $<ctxsp>$ = $<ctxsp>0; } iclasssubspecs '}' ificlass3

ificlass3:     { suppress.pop_back(); form_suppressed(); }
	| elseiclass { suppress.pop_back(); form_suppressed(); }
	| elseificlass { suppress.pop_back();form_suppressed(); }
	;

elseificlass: ELSEIF '(' cintexp ')' 
	  { bool supp2 = suppress.back();
	    *(suppress.rbegin()) = !supp2;
	    suppress.push_back($3 == 0); 
	    form_suppressed(); 
	    $<ctxsp>$ = $<ctxsp>-2;
	  }
	ificlass2

elseiclass: ELSE 
	  {
	    bool supp2 = suppress.back();
	    *(suppress.rbegin()) = !supp2;
	    form_suppressed();
	  }
	'{' { $<ctxsp>$ = $<ctxsp>-2; } iclasssubspecs '}'



ifbuild:  IF '(' cintexp ')' 
	  { suppress.push_back($3 == 0); form_suppressed(); 
	    $<ctxsp>$ = $<ctxsp>0;}
	ifbuild2

ifbuild2: '{' { $<ctxsp>$ = $<ctxsp>0; } buildsetsubspecs '}' ifbuild3

ifbuild3:     { suppress.pop_back(); form_suppressed(); }
	| elsebuild { suppress.pop_back(); form_suppressed(); }
	| elseifbuild { suppress.pop_back();form_suppressed(); }
	;

elseifbuild: ELSEIF '(' cintexp ')' 
	  { bool supp2 = suppress.back();
	    *(suppress.rbegin()) = !supp2;
	    suppress.push_back($3 == 0); 
	    form_suppressed(); 
	    $<ctxsp>$ = $<ctxsp>-2;
	  }
	ifbuild2

elsebuild: ELSE 
	  {
	    bool supp2 = suppress.back();
	    *(suppress.rbegin()) = !supp2;
	    form_suppressed();
	  }
	'{' { $<ctxsp>$ = $<ctxsp>-2; } buildsetsubspecs '}'

ifstyle:  IF '(' cintexp ')' 
	  { suppress.push_back($3 == 0); form_suppressed(); 
	    $<ctxsp>$ = $<ctxsp>0;}
	ifstyle2

ifstyle2: '{' { $<ctxsp>$ = $<ctxsp>0; } stylesubspecs '}' ifstyle3

ifstyle3:     { suppress.pop_back(); form_suppressed(); }
	| elsestyle { suppress.pop_back(); form_suppressed(); }
	| elseifstyle { suppress.pop_back();form_suppressed(); }
	;

elseifstyle: ELSEIF '(' cintexp ')' 
	  { bool supp2 = suppress.back();
	    *(suppress.rbegin()) = !supp2;
	    suppress.push_back($3 == 0); 
	    form_suppressed(); 
	    $<ctxsp>$ = $<ctxsp>-2;
	  }
	ifstyle2

elsestyle: ELSE 
	  {
	    bool supp2 = suppress.back();
	    *(suppress.rbegin()) = !supp2;
	    form_suppressed();
	  }
	'{' { $<ctxsp>$ = $<ctxsp>-2; } stylesubspecs '}'


id: 	IDENT 		{ $$=$1; }
	| ACCESSOR 	{ $$=strdup("accessor"); }
	| ACTION 	{ $$ = strdup("action"); }
	| BUILDSET 	{ $$ = strdup("buildset"); }
	| CAPABILITY 	{ $$ = strdup("capability"); }
	| CLASSES	{ $$ = strdup("classes"); }
	| CODESECTION	{ $$ = strdup("codesection"); }
	| CONSTANT	{ $$ = strdup("constant"); }
	| CROSS 	{ $$ = strdup("cross"); }
	| DECODER	{ $$ = strdup("decoder"); }
	| ELSE   	{ $$ = strdup("else"); }
	| ELSEIF   	{ $$ = strdup("elseif"); }
	| ENTRY 	{ $$ = strdup("entry"); }
	| ENTRYPOINT	{ $$ = strdup("entrypoint"); }
	| ENUMVALUE	{ $$ = strdup("enumvalue"); }
	| FIELD		{ $$ = strdup("field"); }
	| FORMAT	{ $$ = strdup("format"); }
	| GENERATOR 	{ $$ = strdup("generator"); }
	| HIDE		{ $$ = strdup("hide"); }
	| IF	   	{ $$ = strdup("if"); }
        | IMPLEMENT     { $$ = strdup("implement"); }
	| INSTRCLASS	{ $$ = strdup("instrclass"); }
	| INSTRCLASSLIST	{ $$ = strdup("instrclasslist"); }
	| INSTRUCTION	{ $$ = strdup("instruction"); }
	| INSTRUCTIONLIST  { $$ = strdup("instructionlist"); }
	| MATCH		{ $$ = strdup("match"); }
	| OPCODE	{ $$ = strdup("opcode"); }
	| OPERAND	{ $$ = strdup("operand"); }
	| OPERANDNAME	{ $$ = strdup("operandname"); }
	| OPTION	{ $$ = strdup("option"); }
	| PREDECODE	{ $$ = strdup("predecode"); }
	| SHOW		{ $$ = strdup("show"); }
	| STEP		{ $$ = strdup("step"); }
	| STRUCTFIELD	{ $$ = strdup("structfield"); }
	| STYLE 	{ $$ = strdup("style"); }
	| TYPEDEF	{ $$ = strdup("typedef"); }
	;

codesectionspec: CODESECTION id getcode 
	{
	  if (!suppressed) {
	    IR->push_back(new IR_codesection_s($2, $3, true, @$));
 	  }
	  free($2);
	  free($3);
	}
	| CODESECTION '-' id getcode
	{
	  if (!suppressed) {
	    IR->push_back(new IR_codesection_s($3, $4, false, @$));
 	  }
	  free($3);
	  free($4);
	}
	;

typedefspec: TYPEDEF id getdefcode ';'
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_typeadd_s($2, $3, @$));
	    }
	    free($2); free($3);
	  }
	| TYPEDEF '-' id ';'
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_typedel_s($3, @$));
	    }
	    free($3);
	  }

structfieldspec: STRUCTFIELD id getdefcode ';'
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_structadd_s($2, $3, @$));
	    }
	    free($2);  free($3);
	  }
	| STRUCTFIELD '-' id id ';'
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_structdel_s($3, $4, @$));
	    }
	    free($3); free($4);
	  }

enumvaluespec: ENUMVALUE id id ';'
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_enumadd_s($2, $3, "", @$));
	    }
	    free ($2); free($3);
	  }
	| ENUMVALUE id id '=' getdefcode ';'
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_enumadd_s($2, $3, $5, @$));
	    }
	    free($2);  free($3); free($5);
	  }
	| ENUMVALUE '-' id id ';'
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_enumdel_s($3, $4, @$));
	    }
	    free($3); free($4);
	  }

operandnamespec: OPERANDNAME id cintexp '(' cintexp ',' cintexp ')' '=' identlist ';' 
	{
	  if (!suppressed) {
	    for (identlist_t::iterator i = $10->begin(); 
		 i != $10->end(); i++) {
	      IR->push_back(new IR_opername_s(i->first.c_str(), $2, $3, $5, $7,
						  i->second));
	    }
	  }
          free($2);
	  delete $10;
        }
        ;

identlist: id 
		{ 
		  $$ = new identlist_t();
		  $$->push_back(ident_t(std::string($1),@1));
		  free($1); 
		}
           | identlist ',' id 
		{ 
		  $1->push_back(ident_t(std::string($3),@3));
		  $$ = $1;
		  free($3);
		}


identlistnc: id 
		{ 
		  $$ = new identlist_t();
		  $$->push_back(ident_t(std::string($1),@1));
		  free($1); 
		}
           | '-'  
		{ 
		  $$ = new identlist_t();
                  $$->push_back(ident_t(std::string("-"),@1));
		}
           | identlistnc id 
		{ 
		  $1->push_back(ident_t(std::string($2),@2));
		  $$ = $1;
		  free($2);
		}
           | identlistnc '-' 
		{ 
		  $1->push_back(ident_t(std::string("-"),@2));
		  $$ = $1;
		}

accessorspec: ACCESSOR id id '=' id '(' getparmcode ')' 
			'{' accessorlist '}' 
	  { 
	    free($2);
	    free($3);
	    free($5);
	  }
	;

accessorlist : 
	| accessorlist id '=' getcode ';'
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_accessoradd_s($<str>-4, $<str>-7, 
						 $<str>-6, $2, 
						 $<str>-2, $4, @4));
	    }
	    free($2);
	    free($4);
	  }
	;

fieldspec: 
	FIELD id id ';' /* define */
	{
	  if (!suppressed) {
	    IR->push_back(new IR_fieldadd_s($2, $3, "", true, false, @$));
	  }
	  free($2);
	  free($3);
	}
	| FIELD id getcode ';' /* define */
	{
	  if (!suppressed) {
	    IR->push_back(new IR_fieldadd_s($2, $3, "", true, false, @$));
	  }
	  free($2);
	  free($3);
	}
	| FIELD id id '=' getdefcode ';' /* do not define, use accessor */
	{
	  if (!suppressed) {
	    IR->push_back(new IR_fieldadd_s($2, $3, $5, false, false, @$));
	  }
	  free($2);
	  free($3);
	  free($5);
	}
	| FIELD id id ':' getdefcode ';' /* not a reference */
	{
	  if (!suppressed) {
	    IR->push_back(new IR_fieldadd_s($2, $3, $5, false, true, @$));
	  }
	  free($2);
	  free($3);
	  free($5);
	}
	| FIELD '@' id id ';' /* expect field to already be there */ 
	{
	  if (!suppressed) {
	    IR->push_back(new IR_fieldadd_s($3, $4, "", false, false, @$));
	  }
	  free($3);
	  free($4);
	}
	;
   

predecodespec: PREDECODE predecodelist ';'

predecodelist: 
	| predecodelist id 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_predecode_s($2, true, @2));
	    }
	    free($2);
	  }
	| predecodelist '-' id
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_predecode_s($3, false, @3));
	    }
	    free($3);
	  }
	;
actionspec: 
	ACTION id 
		{ 
		  $<ctxsp>$.loc = ctxsp_t::in_iclass; 
		  $<ctxsp>$.str = $2; 
		} 
		actionhandler
		{
		  free($2);
		}
	;

actionops: '+' cintexp { $$.replace = false; $$.disconnect=false;$$.val = $2; }
	| '@' cintexp { $$.replace=true; $$.disconnect=true; $$.val = $2; }
	| '-' cintexp { $$.replace=true; $$.disconnect=false; $$.val = $2; }
	;

actionhandler: actionops '=' getcode 
	{
	  if (!suppressed) {
	    IR->push_back(new IR_action_s($<ctxsp>0.str, $1.val, $3,
					  $1.replace, $1.disconnect, @$));
	  }
	  free($3);
	}
	| actionops
	{
	  if (!suppressed) {
	    IR->push_back(new IR_action_s($<ctxsp>0.str, $1.val, "",
					  $1.replace, $1.disconnect, @$));
	  }
	}
	;

getcode: '{' { startcode('}'); } CODE '}' { $$ = $3; }
	;

getparmcode: { startcode(')'); } CODE { $$ = $2; }
	;

getdefcode: { startcode(';'); } CODE { $$ = $2; }
	;

/* note: while the user is supposed to put public/private before the name,
 * this messes up the parser, so as I'm doing those as identifiers, I'm 
 * just going to rearrange things in here. */
iclassspec: INSTRCLASS id '{'
	{
	  if (!suppressed) {
	    IR->push_back(new IR_iclassadd_s($2, false, false, @$));
	    $<ctxsp>$.loc = ctxsp_t::in_iclass; 
	    $<ctxsp>$.str = $2;
	  }
	}
	    iclasssubspecs '}'
	{
	  free($2);
	}
	| INSTRCLASS id ';'
	{
	  if (!suppressed) {
	    IR->push_back(new IR_iclassadd_s($2, false, false, @$));
	  }
	  free($2);
	}
	;

iclasssubspecs: { }	
        | iclasssubspecs OPCODE { $<ctxsp>$ = $<ctxsp>0; } opcodehandler ';'
	| iclasssubspecs OPERAND { $<ctxsp>$ = $<ctxsp>0; } operandhandler ';'
	| iclasssubspecs ACTION { $<ctxsp>$ = $<ctxsp>0; } actionhandler
	| iclasssubspecs CLASSES { $<ctxsp>$ = $<ctxsp>0; } classlist ';'
	| iclasssubspecs FORMAT { $<ctxsp>$ = $<ctxsp>0; } formathandler ';'
	| iclasssubspecs MATCH { $<ctxsp>$ = $<ctxsp>0; } matchhandler ';'
	| iclasssubspecs FREQUENCY { $<ctxsp>$ = $<ctxsp>0; } 
              frequencyhandler ';'
        | iclasssubspecs { $<ctxsp>$ = $<ctxsp>0; } ificlass
	| iclasssubspecs ';'
	| error ';'
	;

instrspec: INSTRUCTION id '{'
		{
		  $<ctxsp>$.str = $2;
		  $<ctxsp>$.loc = ctxsp_t::in_iclass;
		  if (!suppressed) {
		    IR->push_back(new IR_iclassadd_s($2, true, false, @$));
		  }
		} 
	    iclasssubspecs '}'
		{
		  free($2);
		}
	| INSTRUCTION id ';'
		{
		  if (!suppressed) {
		    IR->push_back(new IR_iclassadd_s($2, true, false, @$));
		  }
		  free($2);
		}
	;

classesspec: CLASSES id 
		{
		  $<ctxsp>$.loc = ctxsp_t::in_iclass;
		  $<ctxsp>$.str = $2; 
		} 
		classlist ';'
		{
		  free($2);
		}
	;

formatspec: FORMAT id
		{
		  $<ctxsp>$.loc = ctxsp_t::in_iclass;
		  $<ctxsp>$.str = $2; 
		} 
		formathandler ';'
		{
		  free($2);
		}
	;

matchspec: MATCH id
		{
		  $<ctxsp>$.loc = ctxsp_t::in_iclass;
		  $<ctxsp>$.str = $2; 
		} 
		matchhandler ';'
		{
		  free($2);
		}
	;

crossspec: CROSS id
		{
		  $<ctxsp>$.loc = ctxsp_t::in_iclass;
		  $<ctxsp>$.str = $2; 
		} 
		crosshandler ';'
		{
		  free($2);
		}
	;


instrclasslistspec: INSTRCLASSLIST id
		{
		  $<ctxsp>$.loc = ctxsp_t::in_iclass;
		  $<ctxsp>$.str = $2; 
		} 
		instrclasslisthandler ';'
		{
		  free($2);
		}
	;

instructionlistspec: INSTRUCTIONLIST id
		{
		  $<ctxsp>$.loc = ctxsp_t::in_iclass;
		  $<ctxsp>$.str = $2; 
		} 
		instructionlisthandler ';'
		{
		  free($2);
		}
	;

operandspec: OPERAND id
		{
		  $<ctxsp>$.loc = ctxsp_t::in_iclass;
		  $<ctxsp>$.str = $2; 
		} 
		operandhandler ';'
		{
		  free($2);
		}
	;


opcodespec: OPCODE id 
	{
	  $<ctxsp>$.loc = ctxsp_t::in_iclass;
	  $<ctxsp>$.str = $2; 
	} 
        opcodehandler ';'
        {
	  free($2);
	}
	;

classlist: { $<ctxsp>$ = $<ctxsp>0; } classlistpiece
	| classlist ',' { $<ctxsp>$ = $<ctxsp>0; } classlistpiece
	;

classlistpiece: id
		{ 
		  if (!suppressed) {
		    IR->push_back(new IR_parentadd_s($<ctxsp>0.str, $1, @$));
		  }
		  free($1);
		}
	| '-' id 
		{
		  if (!suppressed) {
		    IR->push_back(new IR_parentdel_s($<ctxsp>0.str, $2, @$));
		  }
		  free($2);
		}
	;

formathandler:
	id '[' cbitrange ']' rangeadd
		{
		  if (!suppressed) {
		    fmtfield_t f;
		    f.id = strdup($1);
		    f.field = "";
		    f.from = $3.from;
		    f.length = $3.length;
		    f.defval = $5.defval;
		    f.defpresent = $5.defpresent;

		    myloc_t loc = loc_range(@1,@$);
		    IR->push_back(new IR_bitfield_s($<ctxsp>0.str, f, loc));
		  }
		  free($1);
		}
	| id ':' id '[' cbitrange ']' rangeadd
		{
		  if (!suppressed) {
		    fmtfield_t f;
		    f.id = strdup($1);
		    f.field = $3;
		    f.from = $5.from;
		    f.length = $5.length;
		    f.defval = $7.defval;
		    f.defpresent = $7.defpresent;

		    myloc_t loc = loc_range(@1,@$);
		    IR->push_back(new IR_bitfield_s($<ctxsp>0.str, f, loc));
		  }
		  free($1);
		  free($3);
		}
	| formathandler ',' id '[' cbitrange ']' rangeadd
		{
		  if (!suppressed) {
		    fmtfield_t f;
		    f.id = strdup($3);
		    f.field = "";
		    f.from = $5.from;
		    f.length = $5.length;
		    f.defval = $7.defval;
		    f.defpresent = $7.defpresent;

		    myloc_t loc = loc_range(@1,@$);
		    IR->push_back(new IR_bitfield_s($<ctxsp>0.str, f, loc));
		  }
		  free($3);
		}
	| formathandler ',' id ':' id '[' cbitrange ']' rangeadd
		{
		  if (!suppressed) {
		    fmtfield_t f;
		    f.id = strdup($3);
		    f.field = $5;
		    f.from = $7.from;
		    f.length = $7.length;
		    f.defval = $9.defval;
		    f.defpresent = $9.defpresent;

		    myloc_t loc = loc_range(@1,@$);
		    IR->push_back(new IR_bitfield_s($<ctxsp>0.str, f, loc));
		  }
		  free($3);
		  free($5);
		}
	;
	
matchhandler: matchhandler2 
	{ 
	  if (!suppressed) {
	    myloc_t loc = loc_range(@1,@$);
	    IR->push_back(new IR_match_s($<ctxsp>0.str, *$1, loc));
	  } else {
	    for (fmtfieldlistlist_t::iterator 
		   i = $1->begin(); i != $1->end(); i++) {
	      for (fmtfieldlist_t::iterator
		     j = i->begin(); j != i->end(); j++)
		if (j->id) free(j->id);
	    }
	  }
	  delete $1;
	};

matchhandler2: 
        matchhandlerexp 
		{
		  $$ = new fmtfieldlistlist_t();
		  $$->push_back(*$1);
		  delete $1;
		}
	| matchhandler2 '|'  matchhandlerexp
		{
		  $1->push_back(*$3);
		  $$ = $1;
		  delete $3;
		}
	;

matchhandlerexp:
        '+'  { $$ = new fmtfieldlist_t(); }
        | matchhandlerexp2 { $$ = $1; }
	;

matchhandlerexp2:
	matchhandlerexpp
           {
	     $$ = new fmtfieldlist_t();
	     $$->push_back($1);
	   }
	| matchhandlerexp2 ',' matchhandlerexpp
           {
	     $1->push_back($3);
	     $$ = $1;
	   }

matchhandlerexpp: 
        '-' id
		{ 
		  fmtfield_t f;
		  f.id = $2;
		  f.from = 0;
		  f.length = -1;
		  f.defpresent = false; // therefore remove def
		  f.loc = @$;
		  $$ = f;
		}
	| '-' id '[' cbitrange ']'
		{ 
		  fmtfield_t f;
		  f.id = $2;
		  f.from = $4.from;
		  f.length = $4.length;
		  f.defpresent = false; // therefore remove def
		  f.loc = @$;
		  $$ = f;
		}
	| id '[' cbitrange ']' '=' cintexp %prec NEGOP
		{
		  fmtfield_t f;
		  f.id = $1;
		  f.from = $3.from;
		  f.length = $3.length;
		  f.defval = $6;
		  f.defpresent = true;
		  f.loc = @$;
		  $$ = f;
		}
	| id '=' cintexp %prec NEGOP
		{
		  fmtfield_t f;
		  f.id = $1;
		  f.from = 0;
		  f.length = -1;
		  f.defval = $3;
		  f.defpresent = true;
		  f.loc = @$;
		  $$ = f;
		}
	;

cbitrange: cintexp ':' cintexp 
		{ 
		  int first=$1, second=$3;
		  if (first >= second) { 
		    $$.from = second; 
		    $$.length = (first-second+1);
	    	  } else {
		    $$.from = first; 
		    $$.length = (second-first+1);
	    	  }
	  	}
	  | cintexp 
		{ 
		  $$.from = $1; 
		  $$.length = 1;
	  	}
          ;

rangeadd: { $$.defpresent = false; } 
	| '=' cintexp { $$.defpresent = true; $$.defval = $2; }
	;

crosshandler: optat crosshandler2 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_cross_s($<ctxsp>0.str, *$2, $1, @2));
	    }
	    delete $2;
	  }
	;

crosshandler2: '[' cnamelist ']' 
	  { 
	    $$ = new strlistvec_t();
	    $$->push_back(*$2);
	    delete $2;
	  }
	| crosshandler2 '[' cnamelist ']' 
	  { 
	    @$ = loc_range(@2,@4);
	    $3->sort();
	    $3->unique(); // uniquify the list
	    $1->push_back(*$3);
	    delete $3;
	  }

cnamelist: id 
		{ 
		  $$ = new strlist_t();
		  $$->push_back($1);
		  free($1); 
		}
           | cnamelist id 
		{ 
	    	  @$ = loc_range(@1,@2);
		  $1->push_back($2);
		  $$ = $1;
		  free($2);
		}
          ;

optat: 
	 { $$ = false; }
	| '@' { $$ = true; }
	;


instrclasslisthandler: optat '[' identlistnc ']' instrclasslistmatches 
	{ 
	  if (!suppressed) {
	    IR->push_back(new IR_iclassadd_s($<ctxsp>0.str, false, false, @0));
	    int cnt = 0;
	    for (identlist_t::iterator i = $3->begin(); i != $3->end(); 
		 i++,cnt++) {
	      if (i->first == "-") continue;
	      IR->push_back(new IR_iclassadd_s(i->first.c_str(), false, $1,
					       i->second)); 
	      IR->push_back(new IR_parentadd_s(i->first.c_str(), $<ctxsp>0.str,
					       i->second));
	      if ($5.id) {
		fmtfield_t f = $5;
		f.id = f.id ? strdup(f.id): 0; // get another copy
		f.defval = cnt; 
		f.defpresent = true;
		fmtfieldlistlist_t fll;
		fmtfieldlist_t fl;
		fl.push_back(f);
		fll.push_back(fl);
		IR->push_back(new IR_match_s(i->first.c_str(), fll, @5));
	      }
	    }
	  }
	  if ($5.id) free($5.id);
          delete $3; 
	}
	;

instructionlisthandler: optat '[' identlistnc ']' instrclasslistmatches 
	{ 
	  if (!suppressed) {
	    IR->push_back(new IR_iclassadd_s($<ctxsp>0.str, false, false, @0));
	    int cnt = 0;
	    for (identlist_t::iterator i = $3->begin(); i != $3->end(); 
		 i++,cnt++) {
	      if (i->first == "-") continue;
	      IR->push_back(new IR_iclassadd_s(i->first.c_str(), true, $1,
					       i->second)); 
	      IR->push_back(new IR_parentadd_s(i->first.c_str(), $<ctxsp>0.str,
					       i->second));
	      if ($5.id) {
		fmtfield_t f = $5;
		f.id = f.id ? strdup(f.id) : 0;
		f.defval = cnt; 
		f.defpresent = true;
		fmtfieldlistlist_t fll;
		fmtfieldlist_t fl;
		fl.push_back(f);
		fll.push_back(fl);
		IR->push_back(new IR_match_s(i->first.c_str(), fll, @5));
	      }
	    }
	  }
	  if ($5.id) free($5.id);
          delete $3; 
	}
	;

instrclasslistmatches: 
         { fmtfield_t f; 
	   f.id = 0; 
	   $$ = f;
	 }
	| '=' id 
	{  
 	  fmtfield_t f;
	  f.id = $2;
	  f.from = 0;
	  f.length = -1;
	  $$ = f;
	}
	| '=' id '[' cbitrange ']'
	{  
 	  fmtfield_t f;
	  f.id = $2;
	  f.from = $4.from;
	  f.length = $4.length;
	  $$ = f;
	}
	;

operandhandler: id id '(' getparmcode ')'
	{
	  if (!suppressed) {
	    IR->push_back(new IR_operadd_s($<ctxsp>0.str, $1, $2, $4, @$));
	  }
	  free($1);
	  free($2);
	  free($4);
	}
	| '-' id 
        {
	  if (!suppressed) {
	    IR->push_back(new IR_operdel_s($<ctxsp>0.str, $2, @$));
	  }
	  free($2);
	}
	;

opcodehandler: id 
	{
	  if (!suppressed) {
	    IR->push_back(new IR_opcode_s($<ctxsp>0.str, $1, 0, @$));
	  }
	  free($1);
	}
	;

cintexp:  CINT { $$ = $1; }
	| cintexp '+' cintexp	{ $$ = $1 + $3; }
	| cintexp '-' cintexp	{ $$ = $1 - $3; }
	| cintexp '*' cintexp	{ $$ = $1 * $3; }
	| cintexp '/' cintexp	{ $$ = $1 / $3; }
	| cintexp '%' cintexp	{ $$ = $1 % $3; }
	| cintexp '<' cintexp	{ $$ = $1 < $3; }
	| cintexp '>' cintexp	{ $$ = $1 > $3; }
	| cintexp GE cintexp	{ $$ = $1 >= $3; }
	| cintexp LE cintexp	{ $$ = $1 <= $3; }
	| cintexp EQ cintexp	{ $$ = $1 == $3; }
	| cintexp NE cintexp	{ $$ = $1 != $3; }
	| cintexp SSHL cintexp  { $$ = $1 << $3; }
	| cintexp SSHR cintexp  { $$ = $1 >> $3; }
	| cintexp SLAND cintexp  { $$ = $1 && $3; }
	| cintexp SLOR cintexp  { $$ = $1 || $3; }
	| cintexp '&' cintexp  { $$ = $1 & $3; }
	| cintexp '|' cintexp  { $$ = $1 | $3; }
	| cintexp '^' cintexp  { $$ = $1 ^ $3; }
	| '!' cintexp           { $$ = !$2; }
	| '-' cintexp %prec NEGOP { $$ = -$2; }
	| '(' cintexp ')'       { $$ = $2; }
	| cintexp '?' cintexp ':' cintexp { $$ = $1 ? $3 : $5; }
	| id 
	  {
	    if (suppressed) $$ = 0;
 	    else {
		  std::string s($1);
		  if (symbolTableB.count(s)) {
		    $$ = symbolTableB[s]->val;
		    symbolTableB[s]->used = true;
		  } else if (!symbolTable.count(s)) { /* unknown symbol */
		    reportError("Unknown symbol", @1);
		    $$ = 0;
		  } else {
	            $$ = symbolTable[s]->val;
		    symbolTable[s]->used = true;
                  }
	    }
	 }

constanthandler: id '=' cintexp 
	  {
		if (!suppressed) {  
		  semantic_error_t serr;
		  cval_t *cp;
		  switch ($<ctxsp>0.loc) {
		  case ctxsp_t::in_top:
		    serr = theISA->addConstant($1, false, &cp, @$);
		    if (serr) reportError(errToString(serr),@$);
		    else { 
		      symbolTable[$1] = cp;
		      cp->val = $3;
		    }
		    break;
		  case ctxsp_t::in_buildset:
		    serr = theISA->addConstant($<ctxsp>0.str, $1, false,
						&cp, @$);
		    if (serr) reportError(errToString(serr),@$);
		    else {
		      symbolTableB[$1] = cp;
		      cp->val = $3;
		    }
		    break;
		  case ctxsp_t::in_style:
		    serr = theISA->addSConstant($<ctxsp>0.str, $1, false,
						&cp, @$);
		    if (serr) reportError(errToString(serr),@$);
		    else {
		      symbolTableB[$1] = cp;
		      cp->val = $3;
		    }
		    break;
                  default: break; 
		  }
		  free($1);
		}
	  }
	| id MAYASSIGN cintexp 
		{  
	          if (!suppressed) {
		    semantic_error_t serr;
		    cval_t *cp=0;
		    switch ($<ctxsp>0.loc) {
		    case ctxsp_t::in_top:
		      serr = theISA->addConstant($1, false, &cp, @$);
		      if (!serr) {
		        if (!symbolTable.count($1)) {
			  symbolTable[$1] = cp;
			  cp->val = $3;
			}
		      }
		      break;
		    case ctxsp_t::in_buildset:
		      serr = theISA->addConstant($<ctxsp>0.str,$1, false, &cp,
					       @$);
		      if (!serr) {
		        if (!symbolTableB.count($1)) {
			  symbolTableB[$1] = cp;
			  cp->val = $3;
			}
		      }
		      break;
		    case ctxsp_t::in_style:
		      serr = theISA->addSConstant($<ctxsp>0.str,$1, false, &cp,
						  @$);
		      if (!serr) {
		        if (!symbolTable.count($1)) {
			  symbolTableB[$1] = cp;
			  cp->val = $3;
			}
		      }
		      break;
                    default: break; 
		    }
		  }
		  free($1);
		}
	;

optionhandler: id '=' cintexp 
		{  
	          if (!suppressed) {
		    semantic_error_t serr;
		    cval_t *cp=0;
		    switch ($<ctxsp>0.loc) {
		    case ctxsp_t::in_top:
		      serr = theISA->addConstant($1, true, &cp, @$);
		      if (serr) reportError(errToString(serr),@$);
		      else {
			symbolTable[$1] = cp;
			cp->val = $3;
		      }
		      break;
		    case ctxsp_t::in_buildset:
		      serr = theISA->addConstant($<ctxsp>0.str,$1, true, 
						 &cp, @$);
		      if (serr) reportError(errToString(serr),@$);
		      else {
			symbolTableB[$1] = cp;
			cp->val = $3;
		      }
		      break;
		    case ctxsp_t::in_style:
		      serr = theISA->addSConstant($<ctxsp>0.str,$1, true, 
						  &cp, @$);
		      if (serr) reportError(errToString(serr),@$);
		      else {
			symbolTableB[$1] = cp;
			cp->val = $3;
		      }
		      break;
                    default: break; 
		    }
		  }
		  free($1);
		}
	| id MAYASSIGN cintexp 
		{  
	          if (!suppressed) {
		    semantic_error_t serr;
		    cval_t *cp=0;
		    switch ($<ctxsp>0.loc) {
		    case ctxsp_t::in_top:
		      serr = theISA->addConstant($1, true, &cp, @$);
		      if (!serr) {
		        if (!symbolTable.count($1)) {
			  symbolTable[$1] = cp;
			  cp->val = $3;
			}
		      }
		      break;
		    case ctxsp_t::in_buildset:
		      serr = theISA->addConstant($<ctxsp>0.str, $1, true, 
						&cp, @$);
		      if (!serr) {
		        if (!symbolTableB.count($1)) {
			  symbolTableB[$1] = cp;
			  cp->val = $3;
			}
		      }
		      break;
		    case ctxsp_t::in_style:
		      serr = theISA->addSConstant($<ctxsp>0.str, $1, true, 
						  &cp, @$);
		      if (!serr) {
		        if (!symbolTableB.count($1)) {
			  symbolTableB[$1] = cp;
			  cp->val = $3;
			}
		      }
		      break;
                    default: break; 
		    }
		  }
		  free($1);
		}
	;

buildsethandler:
	id '{'
                {
		  if (!suppressed) { 
		    myloc_t loc = loc_range(@1,@$);
		    theISA->addBuildSet($1, loc);
		    symbolTableB.clear();
		    theISA->fillSymbolTable($1, &symbolTableB);

		    IR_buildset_t *ne = new IR_buildset_s($1, "", "", loc);
		    IR->push_back(ne);
		    IRstack.push(IR);
		    IR = &ne->IR;
		  }
		  $<ctxsp>$.loc = ctxsp_t::in_buildset;
		  $<ctxsp>$.str = $1;
		}
           buildsetsubspecs '}'
                {
		  free($1); symbolTableB.clear();
		  if (!suppressed) {
		    IR = IRstack.top();
		    IRstack.pop();
		  }
		}
	| id id '{' 
		{ 
	          if (!suppressed) {
		    myloc_t loc = loc_range(@1,@$);
		    theISA->addBuildSet($1, loc);
		    symbolTableB.clear();
		    theISA->fillSymbolTable($1, &symbolTableB);

		    IR_buildset_t *ne = new IR_buildset_s($1, $2, "", loc);
		    IR->push_back(ne);
		    IRstack.push(IR);
		    IR = &ne->IR;
		  }
		  $<ctxsp>$.loc = ctxsp_t::in_buildset; 
		  $<ctxsp>$.str = $1; 
		  free($2);
		}
	      buildsetsubspecs '}' 
		{ 
		  free($1); symbolTableB.clear();
		  if (!suppressed) {
		    IR = IRstack.top();
		    IRstack.pop();
		  }
		}
	| id id id '{' 
		{ 
	          if (!suppressed) {
		    myloc_t loc = loc_range(@1,@$);
		    theISA->addBuildSet($1, loc);
		    symbolTableB.clear();
		    theISA->fillSymbolTable(std::string($1), &symbolTableB);

		    IR_buildset_t *ne = new IR_buildset_s($1, $2, $3, loc);
		    IR->push_back(ne);
		    IRstack.push(IR);
		    IR = &ne->IR;
		  }
		  $<ctxsp>$.loc = ctxsp_t::in_buildset; 
		  $<ctxsp>$.str = $1; 
		  free($2); free($3);
		}
	      buildsetsubspecs '}' 
		{ 
		  free($1); symbolTableB.clear(); 
		  if (!suppressed) {
		    IR = IRstack.top();
		    IRstack.pop();
		  }
		}
	;

capabilityspec: CAPABILITY id 
		{
		  $<ctxsp>$.loc = ctxsp_t::in_buildset;
		  $<ctxsp>$.str = $2; 
		} capabilityhandler ';'  { free($2); }
	;

decoderspec: DECODER id 
		{
		  $<ctxsp>$.loc = ctxsp_t::in_buildset;
		  $<ctxsp>$.str = $2; 
		} decoderhandler ';'  { free($2); }
	;

entrypointspec: ENTRYPOINT id 
		{
		  $<ctxsp>$.loc = ctxsp_t::in_buildset;
		  $<ctxsp>$.str = $2; 
		} entrypointhandler ';'  { free($2); }
	;

stepspec: STEP id
		{
		  $<ctxsp>$.loc = ctxsp_t::in_buildset;
		  $<ctxsp>$.str = $2; 
		} stephandler ';'  { free($2); }
	;

hidespec: HIDE id 
		{
		  $<ctxsp>$.loc = ctxsp_t::in_buildset;
		  $<ctxsp>$.str = $2; 
		} hidehandler ';' { free($2); }
	;

showspec: SHOW id 
		{
		  $<ctxsp>$.loc = ctxsp_t::in_buildset;
		  $<ctxsp>$.str = $2; 
		} showhandler ';' { free($2); }
	;

buildsetsubspecs: { }	
	| buildsetsubspecs CAPABILITY { $<ctxsp>$ = $<ctxsp>0; } 
		capabilityhandler ';'
	| buildsetsubspecs ENTRYPOINT { $<ctxsp>$ = $<ctxsp>0; } 
		entrypointhandler ';'
	| buildsetsubspecs DECODER { $<ctxsp>$ = $<ctxsp>0; } 
		decoderhandler ';'
	| buildsetsubspecs HIDE { $<ctxsp>$ = $<ctxsp>0; } hidehandler ';'
	| buildsetsubspecs SHOW { $<ctxsp>$ = $<ctxsp>0; } showhandler ';'
	| buildsetsubspecs STEP { $<ctxsp>$ = $<ctxsp>0; } stephandler ';'
	| buildsetsubspecs CONSTANT { $<ctxsp>$ = $<ctxsp>0; } 
		constanthandler ';'
	| buildsetsubspecs OPTION { $<ctxsp>$ = $<ctxsp>0; } 
		optionhandler ';'
	| buildsetsubspecs topdecls
	| buildsetsubspecs iclassdecls
	| buildsetsubspecs BUILDSET buildsethandler 
          { // recover the symbol table because the sub-buildset destroys it
	    theISA->fillSymbolTable($<ctxsp>0.str,&symbolTableB);
	  }
        | buildsetsubspecs { $<ctxsp>$ = $<ctxsp>0; } ifbuild
	| buildsetsubspecs ';'
	| error
	;

capabilityhandler: capabilitylist
	{
	  if (!suppressed) {
	    for (caplist_t::iterator 
		   i = $1->begin(); i != $1->end();i++) {
	      IR->push_back(new IR_capability_s($<ctxsp>0.str, 
						i->id, i->actionNo,
					        i->code ? i->code : "",
						i->loc));
	      free(i->id);
	      free(i->code);
	    }
	  }
	  delete $1;
	}
	;

capabilitylist: capabilitydef
	  {
	    $$ = new caplist_t();
	    $$->push_back($1);
	  }
	| capabilitylist ',' capabilitydef
	  { 
	    $1->push_back($3);
	    $$ = $1;
	  }
	;


capabilitydef: id cintexp getcode
	  {
	    $$.id = $1;
	    $$.actionNo = $2;
	    $$.code = $3;
	    $$.loc = @1;
	  }
	| id 
	  {
	    $$.id = $1;
	    $$.actionNo = -1;
	    $$.code = 0;
	    $$.loc = @1;
	  }
	;

entrypointhandler: codeorid id '(' getparmcode ')' '=' seqrangelist
	  {
	    if (!suppressed) {
	      myloc_t loc = loc_range(@1,@$);
	      IR->push_back(new IR_entrypoint_s($<ctxsp>0.str, $2, $1, 
						$4, *$7, loc));
	    }
	    free($1);
	    free($2);
	    free($4);
	    delete $7;
	  }
	| codeorid id '(' getparmcode ')' '=' getcode seqrangelist
	  {
	    if (!suppressed) {
	      myloc_t loc = loc_range(@1,@$);
	      IR->push_back(new IR_entrypoint_s($<ctxsp>0.str, $2, $1, 
						$4, $7, *$8, loc));
	    }
	    free($1);
	    free($2);
	    free($4);
	    free($7);
	    delete $8;
	  }
	| codeorid id '(' getparmcode ')' '=' seqrangelist getcode 
		seqrangelist
	  {
	    if (!suppressed) {
	      myloc_t loc = loc_range(@1,@$);
	      IR->push_back(new IR_entrypoint_s($<ctxsp>0.str, $2, $1, 
						$4, *$7, $8, *$9, loc));
	    }
	    free($1);
	    free($2);
	    free($4);
	    delete $7;
	    free($8);
	    delete $9;
	  }
	| codeorid id '(' getparmcode ')' '=' seqrangelist getcode 
	  {
	    if (!suppressed) {
	      myloc_t loc = loc_range(@1,@$);
	      IR->push_back(new IR_entrypoint_s($<ctxsp>0.str, $2, $1, 
						$4, *$7, $8, loc));
	    }
	    free($1);
	    free($2);
	    free($4);
	    delete $7;
	    free($8);
	  }
	;

codeorid: id { $$ = $1; }
	| getcode { $$ = $1; }

decoderhandler: id '(' getparmcode ')' optseqrangelist
	  {
	    if (!suppressed) {
	      myloc_t loc = loc_range(@1,@$);
	      if ($5) 
		IR->push_back(new IR_decoder_s($<ctxsp>0.str, $1, $3, 
					       *$5, loc));
	      else 
		IR->push_back(new IR_decoder_s($<ctxsp>0.str, $1, $3, loc));
	    }
	    free($1);
	    free($3);
	    delete $5;
	  }
	;

optseqrangelist: 
	  { $$ = 0; }
	| '=' seqrangelist 
	  {
	    $$ = $2;
	  }
	;

stephandler: id cintexp id '=' seqrangelist
	  {
	    if (!suppressed) {
	      myloc_t loc = loc_range(@1,@$);
	      IR->push_back(new IR_step_s($<ctxsp>0.str, $1, $2, $3, *$5,loc));
	    }
	    free($1);
	    free($3);
	    delete $5;
	  }
	| id cintexp id '=' getcode seqrangelist
	  {
	    if (!suppressed) {
	      myloc_t loc = loc_range(@1,@$);
	      IR->push_back(new IR_step_s($<ctxsp>0.str,$1,$2,$3,$5,*$6,loc));
	    }
	    free($1);
	    free($3);
	    free($5);
	    delete $6;
	  }
	| id cintexp id '=' seqrangelist getcode seqrangelist
	  {
	    if (!suppressed) {
	      myloc_t loc = loc_range(@1,@$);
	      IR->push_back(new IR_step_s($<ctxsp>0.str, $1, $2, $3, *$5, 
					  $6, *$7, loc));
	    }
	    free($1);
	    free($3);
	    delete $5;
	    free($6);
	    delete $7;
	  }
	;

hidehandler: hidelist 
	;

hidelist: id 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $1, hidden, @$));
	    }
	    free($1);
	  }
	| '&' id
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $2, refhidden, @$));
	    }
	    free($2);
	  }
	| '-' id
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $2, nothidden, @$));
	    }
	    free($2);
	  }
	| hidelist ',' id 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $3, hidden, @$));
	    }
	    free($3);
	  }
	| hidelist ',' '&' id 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $4, refhidden, @$));
	    }
	    free($4);
	  }
	| hidelist ',' '-' id 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $4, nothidden, @$));
	    }
	    free($4);
	  }
	;

showhandler: showlist 
	;

showlist: id 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $1, nothidden, @$));
	    }
	    free($1);
	  }
	| '-' id
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $2, hidden, @$));
	    }
	    free($2);
	  }
	| showlist ',' id 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $3, nothidden, @$));
	    }
	    free($3);
	  }
	| showlist ',' '-' id 
	  {
	    if (!suppressed) {
	      IR->push_back(new IR_hide_s($<ctxsp>0.str, $4, hidden, @$));
	    }
	    free($4);
	  }
	;

seqrangelist: cbitrange
		{ 
		  $$ = new rangelist_t();
		  $$->push_back($1);
		}
           | seqrangelist ',' cbitrange
		{ 
		  $1->push_back($3);
		}
	;

frequencyspec: FREQUENCY id { $<ctxsp>$.str = $2; } 
                 frequencyhandler ';' { free($2); } ;

frequencyhandler: cintexp
	{
	  if (!suppressed) {
	    IR->push_back(new IR_frequency_s($<ctxsp>0.str, 
					     static_cast<uint64_t>($1), @$));
	  }
	}
        ;

implementspec: IMPLEMENT identlist '=' id ';'
        {
	  if (!suppressed) {
	    for (LIS_parsedefs::identlist_t::const_iterator
		   i = $2->begin(); i != $2->end(); i++)
	      IR->push_back(new IR_implement_s(i->first.c_str(), $4, 
					       i->second));
	  }
	  delete $2; free($4);
	}
        ;

stylehandler: id '{' 
        {
	  if (!suppressed) { 
	    myloc_t loc = loc_range(@1,@$);
	    theISA->addStyle($1, loc);
	    symbolTableB.clear();
	    theISA->fillSSymbolTable($1, &symbolTableB);
	    IR_style_t *ne = new IR_style_s($1, loc);
	    IR->push_back(ne);
	    IRstack.push(IR);
	    IR = &ne->IR;
	  }
	  $<ctxsp>$.loc = ctxsp_t::in_style;
	  $<ctxsp>$.str = $1;
	} stylesubspecs '}'
        {
	  free($1); symbolTableB.clear();
	  if (!suppressed) {
	    IR = IRstack.top();
	    IRstack.pop();
	  }
	}
        ;

stylesubspecs: {}
	| stylesubspecs ENTRY { $<ctxsp>$ = $<ctxsp>0; } entryhandler 
	| stylesubspecs GENERATOR { $<ctxsp>$ = $<ctxsp>0; } generatorhandler
	| stylesubspecs SPECIALIZE { $<ctxsp>$ = $<ctxsp>0; } specializehandler
	| stylesubspecs EXCLUDE { $<ctxsp>$ = $<ctxsp>0; } excludehandler
	| stylesubspecs topdecls
	| stylesubspecs iclassdecls
	| stylesubspecs buildsetdecls
	| stylesubspecs BUILDSET buildsethandler 
          {
	    theISA->fillSSymbolTable($<ctxsp>0.str, &symbolTableB);
	  }
        | stylesubspecs STYLE stylehandler
          {
	    theISA->fillSSymbolTable($<ctxsp>0.str, &symbolTableB);
	  }
        | stylesubspecs { $<ctxsp>$ = $<ctxsp>0; } ifstyle
	| stylesubspecs ';'
	| error
	;

entryspec: ENTRY id
	{
	  $<ctxsp>$.loc = ctxsp_t::in_style;
	  $<ctxsp>$.str = $2; 
	} entryhandler  { free($2); }
	;

entryhandler: getcode 
        {
	  IR->push_back(new IR_entry_s($<ctxsp>0.str, $1, "", "", @$));
	  free($1);
	}
	|
	'(' getparmcode ')' getcode
	{
	  IR->push_back(new IR_entry_s($<ctxsp>0.str, $4, $2, "", @$));
	  free($2);
	  free($4);
	}
	|
	'(' getparmcode ')' '(' getparmcode ')' getcode
	{
	  IR->push_back(new IR_entry_s($<ctxsp>0.str, $7, $2, $5, @$));
	  free($2);
	  free($5);
	  free($7);
	}
        ;

generatorspec: GENERATOR id
	{
	  $<ctxsp>$.loc = ctxsp_t::in_style;
	  $<ctxsp>$.str = $2; 
	} generatorhandler  { free($2); }
	;

generatorhandler: id getcode 
        { 
	  IR->push_back(new IR_generator_s($<ctxsp>0.str, $1, $2, @$));
	  free($1);
	  free($2);
	}
        ;

specializespec: SPECIALIZE id
	{
	  $<ctxsp>$.loc = ctxsp_t::in_style;
	  $<ctxsp>$.str = $2; 
	} specializehandler  { free($2); }
	;

specializehandler: specializelist
        ;

specializelist: id 
	{
	  if (!suppressed) {
	    IR->push_back(new IR_specialize_s($<ctxsp>0.str, $1, @$));	  
	  }
	  free($1);
	}
      | specializelist ',' id		
	{
	  if (!suppressed) {
	    IR->push_back(new IR_specialize_s($<ctxsp>0.str, $3, @$));	  
	  }
	  free($3);
	}
      ;

excludespec: EXCLUDE id
	{
	  $<ctxsp>$.loc = ctxsp_t::in_style;
	  $<ctxsp>$.str = $2; 
	} excludehandler  { free($2); }
	;

excludehandler: seqrangelist { 
		if (!suppressed) {
		  IR->push_back(new IR_exclude_s($<ctxsp>0.str, *$1, @$));
		}
		delete $1;
	}
	;

%%

namespace {
  void yyerror(const char *str) {
    reportError(str, yylloc);
  }
}
