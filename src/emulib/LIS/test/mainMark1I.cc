/*
 * Copyright (c) 2007-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LIS Mark1 emulator wrapper
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file is the main program of a standalone emulator for Mark1
 *
 */

#include "Mark1I.h"
#include <stdint.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace Mark1I;

namespace {

  std::string progname;

  void usage(const char *message, int errorNum, std::ostream &os=std::cerr) {
    if (message) os << message << std::endl;
    os << "Usage: " << progname << " [options]\n"
       << std::endl
       << "The input binary file is read in text form from stdin\n\n"
       << "  Options are:\n"
       << "  --fast               Use fast method\n"
       << "  --slow               Use slow method\n"
       << "  --steps              Use steps\n"
       << "  --ludicrous          Use ludicrous method\n"
       << "  --templatefind       Use templatefind\n"
       << "  --jitccs             Use a simple JIT-CCS\n"
       << "  --compiled           Compiled code\n"
       << "\n"
       << "  --[no]trace          Control tracing\n"
      ;
    exit(errorNum);
  }

  enum kind_e {
    sim_fast,
    sim_slow,
    sim_rolledback,
    sim_steps,
    sim_ludicrous,
    sim_templatefind,
    sim_jitccs,
    sim_compiled,
  } kind_to_do = sim_fast;

  bool dotrace = false;

} // anonymous namespace

namespace Mark1I {
  extern void EMU_init_mem(Mark1_context_t &);
  extern void EMU_runcompiled(LSE_emu_instr_info_t &ii, char,
			      LSE_emu_addr_t addr);
}

int main(int argc, char *argv[]) {
  Mark1_context_t realct;

  std::ios::sync_with_stdio();
  int optind;
  progname = argv[0];

  for (optind = 1; optind < argc; optind++) {
    char *optarg = argv[optind];
    if (optarg[0]=='-') {
      switch (optarg[1]) {
      case 0: goto donewithargs; // first argument is -
      case 'h': usage(NULL,0,std::cout);
      case '-': /* double-argument */
        if (!strcmp(optarg,"--")) {
          optind++;
          goto donewithargs; // -- argument deleted
        }
        if (!strcmp(optarg,"--help")) usage(NULL,0,std::cout);
        else if (!strcmp(optarg,"--fast")) {
	  kind_to_do = sim_fast;
        }
        else if (!strcmp(optarg,"--slow")) {
	  kind_to_do = sim_slow;
        }
        else if (!strcmp(optarg,"--rollback")) {
	  kind_to_do = sim_rolledback;
        }
        else if (!strcmp(optarg,"--steps")) {
	  kind_to_do = sim_steps;
        }
        else if (!strcmp(optarg,"--ludicrous")) {
	  kind_to_do = sim_ludicrous;
        }
        else if (!strcmp(optarg,"--templatefind")) {
	  kind_to_do = sim_templatefind;
        }
        else if (!strcmp(optarg,"--jitccs")) {
	  kind_to_do = sim_jitccs;
        }
        else if (!strcmp(optarg,"--compiled")) {
	  kind_to_do = sim_compiled;
        }
        else if (!strcmp(optarg,"--trace")) {
	  dotrace = true;
        }
        else if (!strcmp(optarg,"--notrace")) {
	  dotrace = false;
        }
        else usage("Unknown argument",1);
        break;
      default:
        usage("Unknown argument",1);
      }
    } else goto donewithargs;
  }
 donewithargs:
  
  if (argc - optind != 0) 
    usage("Exactly zero arguments required",1);

  for (int i = 0; i < 32; i++) realct.mem[i] = 0; // init mem

  int addr = 0;
  while (!std::cin.eof()) {
    std::string s;
    getline(std::cin, s);
    if (s[0] == '/') continue;
    realct.mem[addr++] = strtoul(s.c_str(), NULL, 0);
  }

  std::cout << "Initial memory contents:\n";
  for (int i=0;i<32;i++) 
    std::cout << "\t" << (i+1) << ":" << std::hex << realct.mem[i] 
	      << std::dec << std::endl;

  switch (kind_to_do) {
  case sim_fast:
    {
      LSE_emu_instr_info_t ii;
      LSE_emu_predecode_info_t pd;
      ii.pre_info = &pd;
      done = false;
      ii.addr = 1;
      ii.swcontexttok = static_cast<LSE_emu_ctoken_t>(&realct);
      
      while (!done) {
	if (dotrace) {
	  std::cout << static_cast<unsigned int>(ii.addr) << ": ";
	  EMU_disassemble_ii_undecoded(ii, std::cout);
	}
	EMU_dofast(ii);
	ii.addr = ii.next_pc;
	if (dotrace) {
	  std::cout << "\t" << "A = " << std::hex << realct.A << std::dec 
		    << std::endl;
	}
      }
    }
    break;
  case sim_templatefind:
    {
      LSE_emu_instr_info_t ii;
      LSE_emu_predecode_info_t pd;
      ii.pre_info = &pd;
      done = false;
      ii.addr = 1;
      ii.swcontexttok = static_cast<LSE_emu_ctoken_t>(&realct);
      
      while (!done) {
	if (dotrace) {
	  std::cout << static_cast<unsigned int>(ii.addr) << ": ";
	  EMU_disassemble_ii_undecoded(ii, std::cout);
	}
	EMU_dotemplatefind(ii);
	ii.addr = ii.next_pc;
	if (dotrace) {
	  std::cout << "\t" << "A = " << std::hex << realct.A << std::dec 
		    << std::endl;
	}
      }
    }
    break;
  case sim_jitccs:
    {
      LSE_emu_instr_info_t ii;
      LSE_emu_predecode_info_t pd;
      ii.pre_info = &pd;
      done = false;
      ii.addr = 1;
      ii.swcontexttok = static_cast<LSE_emu_ctoken_t>(&realct);
      
      while (!done) {
	if (dotrace) {
	  std::cout << static_cast<unsigned int>(ii.addr) << ": ";
	  EMU_disassemble_ii_undecoded(ii, std::cout);
	}
	EMU_dojitccs(ii);
	ii.addr = ii.next_pc;
	if (dotrace) {
	  std::cout << "\t" << "A = " << std::hex << realct.A << std::dec 
		    << std::endl;
	}
      }
    }
    break;
  case sim_slow:
    {
      LSE_emu_instr_info_t ii;
      LSE_emu_predecode_info_t pd;
      ii.pre_info = &pd;
      done = false;
      ii.addr = 1;
      ii.swcontexttok = static_cast<LSE_emu_ctoken_t>(&realct);
      
      while (!done) {
	if (dotrace) {
	  std::cout << static_cast<unsigned int>(ii.addr) << ": ";
	  EMU_disassemble_ii_undecoded(ii, std::cout);
	}
	EMU_doslow(ii);
	ii.addr = ii.next_pc;
	if (dotrace) {
	  std::cout << "\t" << "A = " << std::hex << realct.A << std::dec 
		    << std::endl;
	}
      }
    }
    break;
  case sim_steps:
    {
      LSE_emu_instr_info_t ii;
      LSE_emu_predecode_info_t pd;
      ii.pre_info = &pd;
      done = false;
      ii.addr = 1;
      ii.swcontexttok = static_cast<LSE_emu_ctoken_t>(&realct);
      
      while (!done) {
	if (dotrace) {
	  std::cout << static_cast<unsigned int>(ii.addr) << ": ";
	  EMU_disassemble_ii_undecoded(ii, std::cout);
	}
	for (int i = 0; i < LSE_emu_max_instrstep; i++) 
	  EMU_do_step(&ii, static_cast<LSE_emu_instrstep_name_t>(i), false);

	ii.addr = ii.next_pc;
	if (dotrace) {
	  std::cout << "\t" << "A = " << std::hex << realct.A << std::dec 
		    << std::endl;
	}
      }
    }
    break;

  case sim_rolledback:
    {
      LSE_emu_instr_info_t ii;
      LSE_emu_predecode_info_t pd;
      LSE_emu_iaddr_t iaddr;

      ii.pre_info = &pd;
      done = false;
      iaddr = 1;
      ii.swcontexttok = static_cast<LSE_emu_ctoken_t>(&realct);

      while (!done) {
#if (LSE_emu_emulator_has_speculation)

	ii.addr = iaddr + 1;
	ii.LIS_operand_backed = 0; // since we are not calling clear

	for (int i = 0; i < LSE_emu_max_instrstep-2; i++) 
	  EMU_do_step(&ii, static_cast<LSE_emu_instrstep_name_t>(i), false);

	for (int i = 0; i < LSE_emu_max_operand_dest; i++) {
	  EMU_writeback_operand(&ii, 
				static_cast<LSE_emu_operand_name_t>(i),true);
	}

	for (int i = LSE_emu_max_operand_dest-1; i >= 0; --i) {
	  EMU_resolve_operand(&ii, 
			      static_cast<LSE_emu_operand_name_t>(i), 
			      LSE_emu_resolveOp_rollback);
	}
#endif

	ii.addr = iaddr;
#if (LSE_emu_emulator_has_speculation)
	ii.LIS_operand_backed = 0;
#endif
	if (dotrace) {
	  std::cout << static_cast<unsigned int>(ii.addr) << ": ";
	  EMU_disassemble_ii_undecoded(ii, std::cout);
	}

	for (int i = 0; i < LSE_emu_max_instrstep; i++) 
	  EMU_do_step(&ii, static_cast<LSE_emu_instrstep_name_t>(i), 
		      LSE_emu_resolveOp_commit);

	iaddr = ii.next_pc;
	if (dotrace) {
	  std::cout << "\t" << "A = " << std::hex << realct.A << std::dec 
		    << std::endl;
	}
      }
    }
    break;

  case sim_ludicrous:
    {
      LSE_emu_instr_info_t ii;
      LSE_emu_predecode_info_t pd;
      ii.pre_info = &pd;
      done = false;
      ii.addr = 1;
      ii.swcontexttok = static_cast<LSE_emu_ctoken_t>(&realct);

      Mark1I::EMU_doitall(ii);
    }
    break;

  case sim_compiled:
    EMU_init_mem(realct);
    LSE_emu_instr_info_t ii;
    LSE_emu_predecode_info_t pd;
    ii.pre_info = &pd;
    done = false;
    ii.addr = 1;
    ii.swcontexttok = static_cast<LSE_emu_ctoken_t>(&realct);
    while (!done) {
      if (dotrace) {
	std::cout << static_cast<unsigned int>(ii.addr) << ": ";
	EMU_disassemble_ii_undecoded(ii, std::cout);
      }
      EMU_runcompiled(ii, 0, ii.addr);
      ii.addr = ii.next_pc;
      if (dotrace) {
	std::cout << "\t" << "A = " << std::hex << realct.A << std::dec 
		  << std::endl;
      }
    }
    break;
  }
  std::cout << "Final memory contents:\n";
  for (int i=0;i<32;i++) 
    std::cout << "\t" << (i+1) << ":" << std::hex << realct.mem[i] 
	      << std::dec << std::endl;

  return 0;
}

