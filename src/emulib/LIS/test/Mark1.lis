/* -*-c-*-
 * Copyright (c) 2007-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LIS Mark1 description
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file is the LIS description of the instruction set of the
 * Mark1 baby
 *
 */

/******************** codesections *****************************/

codesection headers {
#include <iostream>
}

codesection description  {
libraries = ""

addrtype="uint8_t"
addrtype_print_format="%d"

max_branch_targets = 2

statespaces = [
	       ("A",   LSE_emu_spacetype_other, 1, 32, "int32_t", [] ),
	       ("mem", LSE_emu_spacetype_reg, 32, 32, "int32_t", [] ),
]

iclasses = [ "cti", "sideeffect" ]
};

codesection public {
  extern bool done;
};

structfield Mark1_context_t int32_t mem[32]; // memory
structfield Mark1_context_t int32_t A;  // A register
typedef Mark1_context_t LSE_emu_isacontext_t;

codesection support {
  bool done = false;
};

/******************** accessor macros ************************/

accessor int32_t val = mem(uint8_t eaddr=uint8_t(0)) {
  decode = {
    oi.spaceid = LSE_emu_spaceid_mem;
    oi.spaceaddr.mem = eaddr;
    oi.uses.reg.bits[0] = ~UINT32_C(0);
  };
  read = { return ctx.mem[eaddr-1]; };
  write = { ctx.mem[eaddr-1] = data; };
}

accessor int32_t val = A() {

  decode = {
    oi.spaceid = LSE_emu_spaceid_A;
    oi.spaceaddr.A = 0;
    oi.uses.reg.bits[0] = ~UINT32_C(0);
  };
  read = { return ctx.A; };
  write = { ctx.A = data; };
}

/********************* field definitions **********************/

field instr            int32_t;
field inline_pc        uint8_t = branch_targets[0];
field target_pc        uint8_t = branch_targets[1];
field opcode           LSE_emu_opcode_t;

predecode iclasses;

/********************** instruction steps ***************************/

constant fetchStep = 100;   
constant findOpcodeStep = 125;
constant changePoint = 150;
constant reportOpcodeStep = 150;
constant decodeStep = 200;
constant requiredDecodeStep = 201;
constant fetchOp1Step = 300;
constant fetchOp2Step = 310;
constant evaluateStep = 400;
constant calcNPCStep = 500;
constant writeResultStep = 600;
constant exceptionStep = 700;

constant disassembleCallStep = 10000;
constant disassembleStep = 10001;

/******************** operand names ********************************/

operandname src 0 (decodeStep, fetchOp1Step) = src_op1;
operandname src 1 (decodeStep, fetchOp2Step) = src_op2;

operandname dest 0 (decodeStep, writeResultStep) = dest_result;

/******************** instruction classes **************************/

// The principal formats
instrclass standard {
  format s[4:0], funcno[15:13];
  action +requiredDecodeStep = { inline_pc = addr + 1; }
  action @calcNPCStep = { next_pc = inline_pc; }
  action @reportOpcodeStep = { opcode = LIS_opcode; }
}
action standard @disassembleStep 
  = { os << LSE_emu_opcode_names[LIS_opcode] << " " << s; }

action ALL @fetchStep = { instr = ctx.mem[addr-1]; }

instrclass cti;
action cti +decodeStep = { iclasses.is_cti = true; }

instrclass sideeffect;
action sideeffect +decodeStep = { iclasses.is_sideeffect = true; }

instrclass standardcti { // note: standardCTI replaces standard!
  classes standard, cti;
  action @calcNPCStep = { next_pc = branch_dir ? target_pc+1 : inline_pc; }
}

/************************* instructions ***********************/

instruction JMP {
  classes standardcti;
  match funcno = 0;

  operand src_op1 mem(s);

  action @evaluateStep = {
    branch_dir = 1;
    target_pc = (src_op1 & 0x1f);
  };
}

instruction JRP {
  classes standardcti;
  match funcno = 1;

  operand src_op1 mem(s);

  action @evaluateStep = {
    branch_dir = 1;
    target_pc = ((src_op1 + addr) & 0x1f);
  };
}

instruction LDN {
  classes standard;
  match funcno = 2;
  operand src_op1 mem(s);
  operand dest_result A();
  action @evaluateStep = { dest_result = -src_op1; };
}

instruction STO {
  classes standard;
  match funcno = 3;
  operand src_op1 A();
  operand dest_result mem(s);
  action @evaluateStep ={  dest_result = src_op1; };
}

instruction SUB {
  classes standard;
  match funcno = 4;
  match + | -funcno,funcno = 5;
  operand src_op1 A();
  operand src_op2 mem(s);
  operand dest_result A();
  action @evaluateStep = { dest_result = src_op1 - src_op2; };
}
/*
instruction SUB2 {
  classes SUB;
  match -funcno, funcno = 5;
}
*/
instruction CMP {
  classes standardcti;
  match funcno = 6;
  operand src_op1 A();
  action @evaluateStep = { 
    branch_dir = (src_op1 < 0);
    target_pc = addr + 1;
  };
  action @disassembleStep = { os << "CMP"; };
}

instruction STOP {
  classes standard, sideeffect;
  match funcno = 7;
  action @exceptionStep = {
    done = true;
  };
  action @disassembleStep = { os << "STOP"; };
}

instruction DEFAULT {
  action @exceptionStep = {
    std::cerr << "Undefined instruction at " << addr << std::endl;
  }
  action @disassembleStep = { os << "undefined instruction"; }
}
