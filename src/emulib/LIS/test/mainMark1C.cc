/*
 * Copyright (c) 2007-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * LIS Mark1 compiled emulator generator wrapper
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file is the main program of a generator for emulators for Mark1
 *
 */

#include "Mark1I.h"
#include <fstream>
// Note: this has to come before because of some weirdness about size_t in
// the header files
#include <iostream>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

using namespace Mark1I;

namespace {

  std::string progname;

  void usage(const char *message, int errorNum, std::ostream &os=std::cerr) {
    if (message) os << message << std::endl;
    os << "Usage: " << progname << " binary\n\n"
       << "The input binary file is read in text form from stdin\n"
       << "The output file is placed on stdout\n"
      ;
    exit(errorNum);
  }

} // anonymous namespace

int main(int argc, char *argv[]) {
  Mark1_context_t realct;

  std::ios::sync_with_stdio();
  int optind;
  progname = argv[0];

  for (optind = 1; optind < argc; optind++) {
    char *optarg = argv[optind];
    if (optarg[0]=='-') {
      switch (optarg[1]) {
      case 0: goto donewithargs; // first argument is -
      case 'h': usage(NULL,0,std::cout);
      case '-': /* double-argument */
        if (!strcmp(optarg,"--")) {
          optind++;
          goto donewithargs; // -- argument deleted
        }
        if (!strcmp(optarg,"--help")) usage(NULL,0,std::cout);
        else usage("Unknown argument",1);
        break;
      default:
        usage("Unknown argument",1);
      }
    } else goto donewithargs;
  }
 donewithargs:
  
  if (argc - optind != 1) 
    usage("Exactly one argument required",1);

  for (int i = 0; i < 32; i++) realct.mem[i] = 0; // init mem

  int addr = 0;
  std::ifstream ist(argv[optind],std::ios_base::in);

  if (!ist) {
    std::cerr << "Unable to open " << argv[optind] << " for reading\n";
    exit(1);
  }

  while (!ist.eof()) {
    std::string s;
    getline(ist, s);
    if (s[0] == '/') continue;
    realct.mem[addr++] = strtoul(s.c_str(), NULL, 0);
  }

  std::cout << "#include \"Mark1I.priv.h\"\n";
  std::cout << "#include \"Mark1I.compiled.cc\"\n";
  std::cout << "namespace Mark1I {\n";

  // output an initializer for the contents of the memory
  std::cout << "\nvoid EMU_init_mem(Mark1_context_t &ctx) {\n";
  for (int i=0;i<32;i++) 
    std::cout << "ctx.mem[" << i << "] = 0x" 
	      << std::hex << realct.mem[i] << std::dec << ";\n";
  std::cout << "} // EMU_init_mem\n";

  LSE_emu_instr_info_t ii;  // really a dummy...
  Mark1I::EMU_docompiled::gen(std::cout, realct, ii, 0);

  std::cout << "}\n";

  return 0;
}

