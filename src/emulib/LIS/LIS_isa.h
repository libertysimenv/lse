/*
 * Copyright (c) 2006-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS. 
 *
 */
/*%
 *
 * Main ISA class definition
 *
 * Authors: David Penry <dpenry@ee.byu.edu>
 *
 * This file has the class definition for the ISA description
 *
 * TODO:
 *
 */
#ifndef LIS_isa_H
#define LIS_isa_H
#include <stdint.h>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include "LIS.h"
#include "LIS_parsedefs.h"

namespace LIS_decoding_support {

  using namespace LIS;

  typedef struct matchval {
    uint64_t mask;
    uint64_t val;
    matchval() { mask = 0; val = 0; }
    bool operator==(const struct matchval &v2) const {
      return mask == v2.mask && val == v2.val;
    }
  } matchval_t;
  typedef std::map<std::string, matchval_t> matchinfo_t;

  inline bool is_empty(const matchinfo_t&mi) {
    for (matchinfo_t::const_iterator j = mi.begin(); j != mi.end(); j++) {
      const matchval_t &mv = j->second;
      if (mv.mask) return false;
    }
    return true;
  }

  inline std::ostream& operator<<(std::ostream& s, const matchinfo_t& mi) {
    bool isfirst = true;
    for (matchinfo_t::const_iterator j = mi.begin(); j != mi.end(); j++) {

      if (!isfirst) s << '/';
      isfirst = false;

      s << j->first << ":" << std::hex;

      const matchval_t &mv = j->second;
      //bool foundfirst = false;
      bool lastdot = false;

      for (int k=15 ; k >= 0; k--) {
	uint64_t tmask = (mv.mask >> (k*4)) & 0xf;
	//if (!tmask && !foundfirst && k > 0) continue;
	//if (tmask) foundfirst = true;
	uint64_t tval = (mv.val >> (k*4)) & 0xf;
	if (tmask == 15) s << tval;
	else if (tmask == 0) s << 'X';
	else {
	  if (!lastdot) s << '.';
	  for (int l = 3; l >= 0; l--) {
	    if (tmask & (1<<l)) s << ((tval >> l) & 1);
	    else s << 'x';
	  }
	  s << '.';
	  lastdot = true;
	  continue;
	}
	lastdot = false;
      }
      s << std::dec;
    }
    return s;
  }

  typedef std::list<matchinfo_t> matchSet_t;

  inline std::ostream& operator<<(std::ostream& s, const matchSet_t& ms) {
    bool isfirst = true;
    s << '{';
    for (matchSet_t::const_iterator i = ms.begin(); i != ms.end(); i++) {
      if (!isfirst) s << ", ";
      isfirst = false;
      s << (*i);
    }
    s << '}';
    return s;
  }

  extern bool SHOW_SETOPS; // debugging variable
  extern bool SHOW_DECODING; // debugging variable
  extern double gammaval;  // gamma to use for decoding
  typedef std::map<uint64_t,int> onebitdiffs_t;
  extern onebitdiffs_t onebitdiffs; // one bit differences

  extern void init_helpers(void);

  inline void normalize_patterns(matchinfo_t& A, matchinfo_t& B) {
    uint64_t any;
    for (matchinfo_t::iterator i = A.begin(); i != A.end(); i++)
      any |= B[i->first].mask;
    for (matchinfo_t::iterator i = B.begin(); i != B.end(); i++)
      any |= A[i->first].mask;
  }

  // attempt to add to a match set, merging with other entries if possible
  // note that this simply tries to make a smaller pattern if it can.
  inline void 
  add_to_matchset(matchSet_t &ms, matchinfo_t mi) {

    if (SHOW_SETOPS)
      std::cerr << "Adding " << mi << " to " << ms << " = ";

    for (matchSet_t::iterator l = ms.begin(); l != ms.end(); ) {
      bool foundone = false, foundmore=false;
      onebitdiffs_t::iterator foundres;
      matchinfo_t::iterator foundm;

      // normalize first against each other!
      normalize_patterns(mi,*l);

      for (matchinfo_t::iterator 
	     i = mi.begin(), j = l->begin(); i != mi.end(); i++, j++) {

	const matchval_t &Av = i->second;
	const matchval_t &Bv = j->second;

	if (Av.mask != Bv.mask) {
	  foundone = false;
	  foundmore = true;
	  break;
	}

	uint64_t diff = (Av.val ^ Bv.val) & Av.mask;
	if (SHOW_SETOPS)
	  std::cerr << "(diff " << std::hex << diff << std::dec << ")";

	if (!diff) continue; // on to next piece because no differences

	onebitdiffs_t::iterator res = onebitdiffs.find(diff);

	if (res == onebitdiffs.end()) { // multiple differences
	  foundone = false; 
	  foundmore = true;
	  break;
	}
	if (SHOW_SETOPS) std::cerr << "(found)";

	if (foundone) { // not a single-bit diff
	  foundone = false; 
	  foundmore = true;
	  break; 
	} else {
	  foundone = true;
	  foundres = res;
	  foundm = i;
	}
      }
      if (foundone) {
	foundm->second.val &= ~foundres->first;
	foundm->second.mask &= ~foundres->first; // make it an X
	if (SHOW_SETOPS) 
	  std::cerr << "(drop " << *l << " for " << mi << "/" 
		    << foundres->first << ") ";
	ms.erase(l);
	l = ms.begin(); // and start over
      } else if (foundmore) l++;
      else { // if we're going to duplicate, delete old from the match set
	if (SHOW_SETOPS) 
	  std::cerr << "(drop duplicate " << *l << " for " << mi << ") ";
	ms.erase(l);
	l = ms.begin();
      }
    }
    ms.push_back(mi);
    if (SHOW_SETOPS) std::cerr << ms << std::endl;
  }

} // namespace LIS_decoding_support

namespace LIS {

  const int DEFAULT_FREQ = 1000;

  // Definitions of types used inside the ISA

  enum opertype_t { oper_src, oper_dest};
  enum spacetype_t { ss_nil, ss_reg, ss_mem, ss_other };

  enum semantic_error_t { 
    no_error,

    redefined_symbol,
    unknown_type,
    redefined_iclass,
    undefined_iclass,
    circular_inheritance,
    unknown_class_type,
    undefined_space,
    redefined_space,
    unknown_space_type,
    unknown_oper_type,
    inconsistent_oper_type,
    unknown_accessor_type,
    undefined_accessor,
    redefined_field,
    undefined_field,
    redefined_action,
    undefined_action,
    redefined_opername_as_field,
    redefined_field_as_opername,
    invalid_operand_offset,
    invalid_sequence_number,
    undefined_bitfield,
    invalid_rangespec,
    undefined_opername,
    undefined_buildset,
    redefined_entrypoint,
    redefined_step,
    redefined_decoder,
    invalid_step_number,
    invalid_step_type,
    incompatible_decoding,
    undefined_style,

    // found when putting things together
    missing_type,
    missing_steps,
    duplicate_entrypoint,
    duplicate_decoder,
    inconsistent_operand_value_type,
    operand_offset_conflict,
    must_implement_buildset,
    duplicate_capability,

    // decoding stuff
    no_decoding_found,
    instruction_pattern_overlaps,

  };

  class semantic_exception {
  public:
    semantic_error_t  err;
    myloc_t loc;
    semantic_exception(const semantic_error_t e, const myloc_t &l) 
      : err(e), loc(l) {}
  };

  typedef struct operandname_t {
    std::string id;
    opertype_t opertype;
    int offset;
    int decodeNo;
    int accessNo;
    myloc_t loc;
  } operandname_t;

  typedef struct field_t { 
    std::string id;
    std::string cname;
    std::string accessorText;
    bool predecode;
    bool toDef;
    bool isExpr;
    myloc_t loc;
  } field_t;
  typedef std::map<std::string, field_t> fields_t;

  typedef std::set<std::string> implicitfields_t;

  typedef struct accessor_code_s {
    std::string rettype;
    std::string params;
    std::string defnparams;
    std::string code;
    implicitfields_t implicitfields;
    myloc_t loc;
  } accessor_code_t;
  typedef std::map<std::string, accessor_code_t> accessor_codes_t;

  typedef struct accessor_s {
    std::string id;
    accessor_codes_t codes;
    std::string ctype;
    std::string ovfield;
    myloc_t loc;
  } accessor_t;
  typedef std::map<std::string, accessor_t> accessors_t;

  typedef struct action_t {
    std::string id;
    std::string name;  /* different from ID so we can match on it */
    std::string code;
    myloc_t loc;
  } action_t;
  typedef std::map<std::string, action_t *> actions_t;
  typedef std::list<action_t *> actionlist_t;

  typedef class sequence_elem {
  public:
    // really could be const, but that would mean making my own assignment
    // and copy constructors, which is silly!
    int seqNo;
    action_t *action;

    sequence_elem(int s=0, action_t *p=0) : seqNo(s), action(p) {}

    struct stableCompare 
      : public std::binary_function<class sequence_elem, 
                                    class sequence_elem, bool>
    {
      bool operator() (class sequence_elem x1, class sequence_elem x2) 
      { 
	return (x2.seqNo > x1.seqNo);
      }
    };

    struct matchesName : public std::unary_function<class sequence_elem, bool> 
    {
      std::string name;
      matchesName() {}
      matchesName(const std::string &n) : name(n) {}
      bool operator() (class sequence_elem x) {
	return name == x.action->name;
      }
    };

    struct matches : public std::unary_function<class sequence_elem, bool> 
    {
      const action_t *ptr;
      matches() {}
      matches(const action_t *p) : ptr(p) {}
      bool operator() (class sequence_elem x) {
	return ptr == x.action;
      }
    };

    bool operator==(class sequence_elem v2) {
      return seqNo == v2.seqNo && action == v2.action;
    }

  } sequence_elem_t;
  typedef class std::list<sequence_elem_t> sequence_t;

  typedef struct bitfield_t {
    std::string id;
    std::string field; // field of instruction type
    int from;
    int length;
  } bitfield_t;
  typedef class std::map<std::string, bitfield_t> bitfields_t;

  typedef struct operand_t {
    std::string id;
    std::string accessor;
    std::string parmstr;
    int decodeNo;
    int actionNo;
    myloc_t loc;
    // set after ISA completes
    accessor_t *acc;
    operandname_t *opername;
  } operand_t;
  typedef class std::map<std::string, operand_t> operands_t;

  struct instruction;

  typedef struct iclass_s {
    std::string id;
    std::string opcode;
    std::set<struct iclass_s *> classes;
    std::set<struct iclass_s *> usedby_class; // keeps transitive set!
    sequence_t sequence;
    actions_t actiondefs;
    bitfields_t bitfields;
    operands_t operands;
    LIS_decoding_support::matchSet_t matchinfo;
    myloc_t loc;
    bool isInstruction;
    unsigned int idno; // used only for instructions; assigned by complete
    uint64_t frequency; // used only for instructions
    bool canDecode;     // used only for instructions; assigned by decode
    LIS_decoding_support::matchinfo_t matchcommon;  // common match information
    void addSeqElement(const action_t *ap, const int seqNo, 
		       const bool replace, const bool sort);
    void removeSeqElement(const std::string &name);

  } iclass_t;

  typedef std::set<iclass_t *> iclass_set_t;
  typedef std::map<std::string, iclass_t> iclasses_t;

  enum steptypes_t { st_front = 0, st_back=1 };
  typedef struct step {
    std::string bsname;
    std::string id;
    int stepNo;
    steptypes_t steptype;
    myloc_t loc;
  } step_t;
  typedef std::map<int, step_t> steps_t;

  typedef std::set<std::string> capabilities_t;

  class codepoint_t {
  public:
    std::string id;
    std::string rtype;
    std::string params, extendedParams;
    std::string dcode;
    myloc_t loc;

    codepoint_t(const std::string &i, const std::string &r, 
		const std::string &p, const std::string &d,
		const myloc_t &l)
      : id(i), rtype(r), params(p), dcode(d), loc(l) {}

    virtual bool hasBefore(const rangelist_t &excl) const = 0;
    virtual bool hasAfter(const rangelist_t &excl) const = 0;
    virtual std::string beforeText(const iclass_t &iclass,
				   const std::string &p, 
				   const rangelist_t &excl) const = 0;
    virtual std::string afterText(const iclass_t &iclass,
				  const std::string &p, 
				  const rangelist_t &excl) const = 0;
    virtual std::ostream& dump(std::ostream &os) const = 0;
  };
  typedef std::map<std::string, codepoint_t *> entrypoints_t;

  class entrypoint_t : public codepoint_t {
  public:
    rangelist_t befrange;
    rangelist_t aftrange;
    class buildset_t *bsp;

    entrypoint_t(const std::string &i, const std::string &r, 
		 const std::string &p, 
		 const rangelist_t &br, const std::string &d, 
		 const rangelist_t &ar,
		 class buildset_t *b, const myloc_t &l) :
      codepoint_t(i,r,p,d,l), befrange(br), aftrange(ar), bsp(b) {}

    bool hasBefore(const rangelist_t &excl) const;
    bool hasAfter(const rangelist_t &excl) const;
    std::string beforeText(const iclass_t &iclass,
			   const std::string &p, const rangelist_t &excl) const;
    std::string afterText(const iclass_t &iclass,
			  const std::string &p, const rangelist_t &excl) const;
    std::ostream& dump(std::ostream &os) const;
  };

  // Operand access code class
  class operacc_t : public codepoint_t {
  public:
    int accesstype;
    int offset;
    int actionNo;
    bool beforeP;
    bool afterP;

    operacc_t(const std::string &i, const std::string &d, 
	      int aty, int of, int an, bool bp, bool ap, const myloc_t &l) :
      codepoint_t(i, "void", "LSE_emu_instr_info_t &LIS_ii", d, l), 
      accesstype(aty), offset(of), actionNo(an), beforeP(bp), afterP(ap) {
      if (aty == 2) params += ", bool LIS_dospeculation"; // write spec
      if (aty == 3) {
	params += ", int LIS_resolveOp"; // resolve
	rtype = "int";
      }
      if (aty == 4) params += ", bool LIS_dospeculation"; // read spec
    }

    bool hasBefore(const rangelist_t &excl) const;
    bool hasAfter(const rangelist_t &excl) const;
    std::string beforeText(const iclass_t &iclass,
			   const std::string &p, const rangelist_t &excl) const;
    std::string afterText(const iclass_t &iclass,
			  const std::string &p, const rangelist_t &excl) const;
    std::ostream& dump(std::ostream &os) const;
  };

  typedef struct hide_t {
    field_t *fp;
    hide_status_t hidestatus;
  } hide_t;
  typedef std::map<std::string, hide_t > hiddenFields_t;

  typedef struct decisionfunc_s {
    enum { dec_pattern, dec_table, dec_leaf, dec_empty } kind;
    std::vector<struct decisionfunc_s *> child;
    LIS_decoding_support::matchinfo_t pattern;
    struct {
      std::string field;
      int from;
      int length;
    } table;
    iclass_t *leaf;
    double freq; // for leaf nodes for reporting purposes

    ~decisionfunc_s() {
      switch (kind) {
      case dec_pattern:
      case dec_table: 
	for (std::vector<struct decisionfunc_s *>::iterator i = child.begin();
	     i != child.end(); i++) {
	  delete *i;
	}
      case dec_leaf: break;
      case dec_empty: break;
      default : break;
      }
    }
  } decisionfunc_t;

  class decoder_t : public codepoint_t {
  public:
    rangelist_t range;
    decisionfunc_t *decodeTree;

    decoder_t(const std::string &i, const std::string &p, 
	      const rangelist_t &r, const myloc_t &l) :
      codepoint_t(i,"LSE_decodetoken_t",p,"",l), range(r), decodeTree(0) {}

    myloc_t loc;
    // decoder_t(){ decodeTree = 0; }
    ~decoder_t() { delete decodeTree; }

    bool hasBefore(const rangelist_t &excl) const;
    bool hasAfter(const rangelist_t &excl) const;
    std::string beforeText(const iclass_t &iclass,
			   const std::string &p, const rangelist_t &excl) const;
    std::string afterText(const iclass_t &iclass,
			  const std::string &p, const rangelist_t &excl) const;
    std::ostream& dump(std::ostream &os) const;
  };
  typedef std::map<std::string, decoder_t *> decoders_t;

  typedef std::map<std::string, std::string> code_t;

  typedef struct cval_t {
    int val;
    bool used;
  } cval_t;
  typedef std::map<std::string, cval_t> cvals_t;
  typedef std::map<std::string, cval_t *> cvalsP_t;

  typedef struct structentry_s {
    std::string name;
    std::string defn;
    myloc_t loc;
  } structentry_t;
  typedef std::map<std::string,structentry_t> structdef_t;

  enum type_type_t { scalar, structdef, enumdef };
  typedef std::list<std::string> typeOrder_t; 
  typedef struct typedef_s {
    std::string id;
    type_type_t ttype;
    structdef_t defn;
    typeOrder_t defnOrder;
    std::string strdef;
    myloc_t loc;
  } typedef_t;
  typedef std::map<std::string,typedef_t> typedefs_t;

  struct implcap_t {
    std::string id;
    int actionNo;
    std::string decodeToken;
    myloc_t loc;
  };
  typedef std::map<std::string, implcap_t> implcaps_t;

  class buildset_t {
  public:
    std::string id;
    std::string baseclass;
    std::string style;
    capabilities_t capabilities;
    implcaps_t implCapabilities;
    entrypoints_t entrypoints;
    hiddenFields_t hiddenFields;
    decoders_t decoders;
    cvals_t constants;
    cvals_t options;
    myloc_t loc;
    ~buildset_t() {
      for (entrypoints_t::iterator 
	     i = entrypoints.begin(); i != entrypoints.end(); ++i) {
	delete i->second;
      }
      for (decoders_t::iterator 
	     i = decoders.begin(); i != decoders.end(); ++i) {
	delete i->second;
      }
    }
  };
  typedef std::map<std::string, buildset_t> buildsets_t;
 
  typedef struct entry_s {
    std::string code;
    std::string rtype;
    std::string eparms;
    myloc_t loc;
  } entry_t;

  struct generator_t {
    std::string section;
    std::string code;
    myloc_t loc;
  };
  typedef std::map<std::string,generator_t> generators_t;
  typedef std::set<std::string> specializedBitfields_t;

  typedef struct style_s {
    std::string id;
    entry_t entry;
    generators_t generators;
    hiddenFields_t specializedFields; 
    specializedBitfields_t specializedBitfields;
    rangelist_t excl;
    cvals_t constants;
    cvals_t options;
    myloc_t loc;
  } style_t;
  typedef std::map<std::string, style_t> styles_t;
  
  extern const char *errToString(semantic_error_t errnum);

  //typedef std::list<LIS_parsedefs::IR_element_t *> IR_t;

  class LIS_isa
    {

    public:
      const static char *otnames[3];
      const static char *stnames[4];
      std::set<std::string> validAccessors;

      /*********** IR *********************************/

      LIS_parsedefs::IR_t IR;

      /********** declared information ****************/

      bool isLSEemu;
      bool isBuildable;

      cvals_t constants;
      cvals_t options;
      code_t code;

      typedefs_t typedefs;
      typeOrder_t typeOrder;

      iclasses_t iclasses;
      std::map<std::string, operandname_t> opernames;
      accessors_t accessors;

      actionlist_t actions; // place to store all the actions we create

      fields_t fields;
      buildsets_t buildsets; // only those in buildsetOrder are valid
      styles_t styles;

      /********* derived information **************/

      typedef std::vector<field_t *> fieldsOrdered_t;
      fieldsOrdered_t fieldsOrdered;

      typedef std::list<std::string> buildsetOrder_t;
      buildsetOrder_t buildsetOrder;
      typedef std::vector<style_t *> styleOrder_t;
      styleOrder_t styleOrder;

      steps_t steps;

      capabilities_t allCapabilities;
      std::set<std::string> instrfields;

      typedef std::vector<iclass_t *> instructionsOrdered_t;
      instructionsOrdered_t instrsOrdered;

      typedef std::map<std::string, std::string> opvaluetype_t;
      opvaluetype_t opvaluetype;

      int max_opers[2];
      std::set<int> oper_used[2];

      bool hasSpeculation;

    private:
      typedef std::pair<bool, std::string> buildi_t;
      typedef std::map<std::string, std::string> stringmap_t;
      typedef std::set<std::string> stringset_t;
      typedef struct buildWork_s {
	stringset_t implBuildsets;
	stringset_t implStyles;
	stringmap_t bs2style;
	std::map<buildi_t, stringset_t > subBuildsets;
	std::map<buildi_t, stringset_t > subStyles;
	stringset_t eps; // entrypoint names
	stringset_t dps; // decoder names
      } buildWork_t;
      void buildpassStyle(const LIS_parsedefs::IR_t &IR, buildWork_t &worker,
			  const buildi_t &parent);
      void buildpass(LIS_parsedefs::IR_t &IR, buildWork_t &worker);

    protected:

      // top level stuff
      void addAccessor(const std::string &ctype, const std::string &ovfield, 
		       const std::string &name, const std::string &atype, 
		       const std::string &params, const std::string &code, 
		       const myloc_t &loc);
      void addCode(const std::string &name, const std::string &value, 
		   bool append, const myloc_t &loc);
      void addCode(const std::string &name, const std::string &value);
      void performCross(const std::string &pname, 
			bool overrideIFlag,
			const LIS_parsedefs::strlistvec_t &sll,
			const myloc_t &loc);
      void addEnumValue(const std::string &tname, const std::string &ename,
			const std::string &edef, const myloc_t &loc);
      void removeEnumValue(const std::string &tname, const std::string &ename,
			   const myloc_t &loc);
      void addStructField(const std::string &tname, const std::string &fdef,
			  const myloc_t &loc);
      void removeStructField(const std::string &tname,
			     const std::string &fname, const myloc_t &loc);
      void addType(const std::string &tdef1, const std::string &tdef2, 
		   const myloc_t &loc);
      void removeType(const std::string &tname, const myloc_t &loc);
      void setPredecode(const std::string &fname, const bool setIt, 
			const myloc_t &loc);

      // iclass stuff
      void addAction(const std::string &cname, const std::string& aname,
		     const bool replace, const bool disconnect, 
		     const bool forceentry,
		     const int seqNo, const std::string &code, 
		     const myloc_t &loc);
      void addBitfield(const std::string &pname, 
		       const LIS_parsedefs::fmtfield_t &f, const myloc_t &loc);
      void addFrequency(const std::string &iname, const int64_t value, 
			const myloc_t &loc);
      void updateMatch(const std::string &pname, 
		       const LIS_parsedefs::fmtfieldlistlist_t& fll,
		       const myloc_t &loc,
		       const bool recurse=true);

      void setOpcode(const std::string &iname, const std::string &nname, 
		     const int piece, const myloc_t &loc);
      void addOperand(const std::string &pname, const std::string &oname,
		      const std::string &accessor, const std::string &parmlist,
		      const myloc_t &loc);
      void removeOperand(const std::string &cname, const std::string &oname,
			 const myloc_t &loc);
      void addOperandName(const std::string &name, const std::string &opertype,
			  const int offset, 
			  const int decodeNo, const int actionNo,
			  const myloc_t &loc);
      void removeParent(const std::string &pname, const std::string &cname,
			const myloc_t &loc);
    public:
      void setIFlag(const std::string &cname, 
		    const bool newFlag, const myloc_t &loc);
    protected:

      // buildpoint stuff
      void addCapability(const std::string &bsname, const std::string &name,
			 const int actionNo, const std::string &decodetoken,
			 const myloc_t loc);
      void addDecoder(const std::string &bname, const std::string &dname,
		      const std::string &params, const rangelist_t &range,
		      const myloc_t &loc);
      void addEntrypoint(const std::string &bname, const std::string &rtype,
			 const std::string &ename, const std::string &params,
			 const rangelist_t &befrange, const std::string &dcode,
			 const rangelist_t &aftrange, const myloc_t &loc);
      void addField(const std::string &fname, const std::string &ctype, 
		    const std::string &code, const bool addDef, 
		    const bool isExpr, const myloc_t &loc);
      void addField(const std::string &fname, const std::string &ctype, 
		    const std::string &code, const bool addDef,
		    const myloc_t &loc);
      void addHide(const std::string &bname, const std::string &fname, 
		   hide_status_t hidestatus, const myloc_t &loc);
      void addStep(const std::string &bname, const std::string &sname,
		   const int sno, const std::string &stype,
		   const rangelist_t &befrange, const std::string &dcode,
		   const rangelist_t &aftrange, const myloc_t &loc);

      // style stuff
      void addEntry(const std::string &sname, const std::string &code,
		    const std::string &rtype, const std::string &eparms,
		    const myloc_t &loc);
      void addGenerator(const std::string &sname, const std::string &secname,
			const std::string &code, const myloc_t &loc);
      void addSpecialize(const std::string &sname, const std::string &fname,
			 const myloc_t &loc);
      void addExclude(const std::string &sname, const rangelist_t &excl,
		      const myloc_t &loc);
    public:
      LIS_isa(bool le = true, bool bld=true);
      ~LIS_isa();

      bool completeISA(void);
      semantic_error_t createDecoding(void);

      semantic_error_t addConstant(const char *name, const bool isOption, 
				   cval_t **cp, const myloc_t loc);
      semantic_error_t addConstant(const char *bname, const char *name, 
				   const bool isOption, cval_t **cp,
				   const myloc_t loc);
      semantic_error_t addSConstant(const char *bname, const char *name, 
				    const bool isOption, cval_t **cp,
				    const myloc_t loc);
      void addIClass(const std::string &name, const bool isInstruction,
		     const myloc_t &loc);
      void addParent(const std::string &cname, const std::string &pname,
		     const myloc_t &loc);
      void addStyle(const std::string &name, const myloc_t &loc);
      void fillSymbolTable(const std::string &bname, cvalsP_t *stab);
      void fillSSymbolTable(const std::string &bname, cvalsP_t *stab);

      void addBuildSet(const char *bname, const myloc_t &loc);
    };

  inline void 
  showDecode(std::ostream &os, const int nspace, const decisionfunc_t *dt,
	     bool recurse=true) {

    for (int i=0;i<nspace;i++) os << ' ';

    switch (dt->kind) {

    case decisionfunc_t::dec_leaf:
      os << "Instr " << (dt->leaf ? dt->leaf->id : "<none>") << " " 
	 << dt->freq << std::endl;
      break;

    case decisionfunc_t::dec_empty:
      os << "Empty decision" << dt->freq << std::endl;
      break;

    case decisionfunc_t::dec_pattern:
      os << "Pattern " ;
      for (LIS_decoding_support::matchinfo_t::const_iterator 
	     j = dt->pattern.begin();
	   j != dt->pattern.end(); j++) {
	os << " (" << j->first << " & " << std::hex << j->second.mask <<
	  ")=" << j->second.val << std::dec << std::endl;
      }
      if (recurse) {
	showDecode(os, nspace+2, dt->child[1]);
	showDecode(os, nspace+2, dt->child[0]);
      }
      break;

    case decisionfunc_t::dec_table:
      os << "Table " << dt->table.field << " " << dt->table.from << "/"
	 << dt->table.length << std::endl;
      if (recurse) {
	for (int i=0; i < (1<<dt->table.length); i++)
	  showDecode(os, nspace+2, dt->child[i]);
      }
      break;
    default: break;
      os << std::endl;
    }
  }

  // And a very useful utility function!

  template <class T> inline std::string to_string (const T& t)
    {
      std::stringstream ss;
      ss << t;
      return ss.str();
    }

  extern std::string find_pnames(std::string p);

} // namespace LIS

#endif /* LIS_isa_H */
