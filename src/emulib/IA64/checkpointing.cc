/*
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Checkpointing functions for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file keeps functions to manipulate checkpoints
 *
 * TODO: 
 * - endianness on register files (if I want portability)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LSE_IA64.h"

namespace LSE_IA64 {

/************************ Table-of-contents **************************/

/*
 * checkpointing: add a table-of-contents item
 */
LSE_chkpt::error_t 
EMU_chkpt_add_toc(LSE_emu_interface_t *intr,
		  LSE_chkpt::file_t *cptFile, const char *emuName, 
		  int step, LSE_emu_chkpt_cntl_t *ctl)
{
  LSE_chkpt::error_t cerr;
  char buf[30];

  cerr = cptFile->add_toc(emuName);
  if (cerr) return cerr;

  sprintf(buf,"STEP=%d",step);
  cerr = cptFile->add_tocparm(buf);
  if (cerr) return cerr;

  sprintf(buf,"recordOS=%s",ctl->recordOS?"TRUE":"FALSE");
  cerr = cptFile->add_tocparm(buf);
  if (cerr) return cerr;

  sprintf(buf,"incrementalMem=%s",ctl->incrementalMem?"TRUE":"FALSE");
  cerr = cptFile->add_tocparm(buf);
  if (cerr) return cerr;

  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t
EMU_chkpt_check_toc(LSE_emu_interface_t *intr,
		    LSE_chkpt::file_t *cptFile,
		    const char *emuName, 
		    int step,
		    unsigned int *position,
		    LSE_emu_chkpt_cntl_t *ctl)
{
  LSE_chkpt::error_t cerr;
  char *fname, *p;
  int fstep = -1;

  cerr = cptFile->get_toc(&fname,position,false);
  if (cerr) return cerr;

  /* is this the expected emulator? */
  if (emuName && strcmp(fname,emuName)) return LSE_chkpt::error_Application;

  /* is this the expected step? */
  cerr = cptFile->get_tocparm(&p,false);
  if (cerr) return cerr;
  if (!p) return LSE_chkpt::error_Application;
  sscanf(p,"STEP=%d",&fstep);
  if (step != fstep) return LSE_chkpt::error_Application;

  cerr = cptFile->get_tocparm(&p,false);
  if (cerr) return cerr;
  else if (!p) return LSE_chkpt::error_Application;
  else if (!strcmp(p,"recordOS=TRUE")) ctl->recordOS = true;
  else if (!strcmp(p,"recordOS=FALSE")) ctl->recordOS = false;
  else return LSE_chkpt::error_Application;

  cerr = cptFile->get_tocparm(&p,false);
  if (cerr) return cerr;
  else if (!p) return LSE_chkpt::error_Application;
  else if (!strcmp(p,"incrementalMem=TRUE")) ctl->incrementalMem = true;
  else if (!strcmp(p,"incrementalMem=FALSE")) ctl->incrementalMem = false;
  else return LSE_chkpt::error_Application;

  return LSE_chkpt::error_None;
}

/*************************** Writing routines *****************************/

/* 
 * write out registers; the trick here is to find a way to do it so 
 * that I am endian and length-neutral but do not have to do conversions
 * very often.  The trick I will use is the following:
 *
 * - register files are ASN.1 octet strings, but the format of the
 *   bytes in those strings is that of an X86 implementation.  If anything
 *   is different, then translation into a buffer must occur.  This can
 *   be detected fairly easily.
 * - buffer copying is avoided by allocating the buffers I need when I 
 *   learn that I need them and deallocating after the write is done.
 * 
 * The order in which registers are dumped is:
 * - low-Rs (1-15 + (16-31 x 2))
 * - Fs
 * - p
 * - b
 * - ar
 * - physr (varies by implementation; num_stacked_phys gives this)
 * - sr
 * - RSE other state
 *
 */

#define RLOW_SIZE (48 * 16)
#define F_SIZE (128 * 16)
#define P_SIZE (1 * 64)
#define B_SIZE (8 * 8)
#define AR_SIZE (128 * 8)
#define PHYSR_SIZE (16)

static LSE_chkpt::error_t
write_regs(LSE_chkpt::file_t *cptFile, IA64_context_t *realct) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp, *dp2;

  /* reuse for better speed */
  dp = LSE_chkpt::build_octetstring(0,0,0,false);
  if (!dp) return LSE_chkpt::error_OutOfMemory;

  /***** handle low Rs *****/

  if (LSE_end_is_big || sizeof(realct->r) != RLOW_SIZE) {
    /* TODO */
  }
  else {
    dp->content.ustringVal = (unsigned char *)realct->r;
  }
  dp->size = RLOW_SIZE;
  cerr = cptFile->write_to_segment(false, dp);
  if (cerr) goto had_error;
  
  /*********** handle Fs **********/

  if (LSE_end_is_big || sizeof(realct->f) != F_SIZE) {
    /* TODO */
  } else {
    dp->content.ustringVal = (unsigned char *)realct->f;
  }
  dp->size = F_SIZE;
  cerr = cptFile->write_to_segment(false, dp);
  if (cerr) goto had_error;
  
  /************ handle Ps ***********/

  if (LSE_end_is_big || sizeof(realct->p) != P_SIZE) {
    /* TODO */
  } else {
    dp->content.ustringVal = (unsigned char *)realct->p;
  }
  dp->size = P_SIZE;
  cerr = cptFile->write_to_segment(false, dp);
  if (cerr) goto had_error;
  
  /************ handle Bs ***********/

  if (LSE_end_is_big || sizeof(realct->b) != B_SIZE) {
    /* TODO */
  } else {
    dp->content.ustringVal = (unsigned char *)realct->b;
  }
  dp->size = B_SIZE;
  cerr = cptFile->write_to_segment(false, dp);
  if (cerr) goto had_error;
  
  /************ handle ARs ***********/

  if (LSE_end_is_big || sizeof(realct->ar) != AR_SIZE) {
    /* TODO */
  } else {
    dp->content.ustringVal = (unsigned char *)realct->ar;
  }
  dp->size = AR_SIZE;
  cerr = cptFile->write_to_segment(false, dp);
  if (cerr) goto had_error;
  
  /************ handle physrs ***********/

  dp2 = LSE_chkpt::build_signed(0, realct->num_stacked_phys);
  if (!dp2) return LSE_chkpt::error_OutOfMemory;
  cerr = cptFile->write_to_segment(true, dp2);
  if (cerr) {
    delete (dp2);
    goto had_error;
  }

  if (LSE_end_is_big || 
      sizeof(realct->physr) != PHYSR_SIZE * IA64_RSE_N_STACKED_IMPL) {
    /* TODO */
  } else {
    dp->content.ustringVal = (unsigned char *)realct->physr;
  }
  dp->size = realct->num_stacked_phys * PHYSR_SIZE;
  cerr = cptFile->write_to_segment(false, dp);
  if (cerr) goto had_error;

  /************ handle SRs ***********/

  dp2 = LSE_chkpt::build_unsigned(0, realct->sr[0].CFM.val);
  if (!dp2) return LSE_chkpt::error_OutOfMemory;
  cerr = cptFile->write_to_segment(true, dp2);
  if (cerr) {
    delete (dp2);
    goto had_error;
  }

  dp2 = LSE_chkpt::build_unsigned(0, (uint64_t)realct->sr[0].CFM.BOF);
  if (!dp2) return LSE_chkpt::error_OutOfMemory;
  cerr = cptFile->write_to_segment(true, dp2);
  if (cerr) {
    delete (dp2);
    goto had_error;
  }

  dp2 = LSE_chkpt::build_unsigned(0, realct->sr[1].PSR);
  if (!dp2) return LSE_chkpt::error_OutOfMemory;
  cerr = cptFile->write_to_segment(true, dp2);
  if (cerr) {
    delete (dp2);
    goto had_error;
  }

  /************ handle RSE ***********/

  dp2 = LSE_chkpt::build_signed(0, (int64_t)realct->RSE_other.NDirty);
  if (!dp2) return LSE_chkpt::error_OutOfMemory;
  cerr = cptFile->write_to_segment(true, dp2);
  if (cerr) {
    delete (dp2);
    goto had_error;
  }

  dp2 = LSE_chkpt::build_signed(0, (int64_t)realct->RSE_other.NClean);
  if (!dp2) return LSE_chkpt::error_OutOfMemory;
  cerr = cptFile->write_to_segment(true, dp2);
  if (cerr) {
    delete (dp2);
    goto had_error;
  }

  delete (dp);
  return LSE_chkpt::error_None;
 had_error:
  delete (dp);
  return cerr;
}

static inline LSE_chkpt::error_t
write_unsigned(LSE_chkpt::file_t *cptFile, 
	       LSE_chkpt::error_t *cerr, uint64_t val) {
  LSE_chkpt::data_t *dp;
  if (*cerr) return *cerr;
  dp=LSE_chkpt::build_unsigned(0,val);
  if (!dp) return LSE_chkpt::error_OutOfMemory;
  *cerr = cptFile->write_to_segment(true, dp);
  if (*cerr) delete (dp);
  return *cerr;
}

static inline LSE_chkpt::error_t
write_boolean(LSE_chkpt::file_t *cptFile, 
	       LSE_chkpt::error_t *cerr, boolean val) {
  LSE_chkpt::data_t *dp;
  if (*cerr) return *cerr;
  dp=LSE_chkpt::build_boolean(0,val);
  if (!dp) return LSE_chkpt::error_OutOfMemory;
  *cerr = cptFile->write_to_segment(true, dp);
  if (*cerr) delete (dp);
  return *cerr;
}

static LSE_chkpt::error_t 
write_alat(LSE_chkpt::file_t *cptFile,IA64_context_t *realct)
{
  /* going to write out the number of entries and then a list with
   * each entry
   */
  LSE_chkpt::data_t *dp,*tdp;
  LSE_chkpt::error_t cerr=LSE_chkpt::error_None;
  int i;

  write_unsigned(cptFile,&cerr,realct->ALAT_size);
  write_unsigned(cptFile,&cerr,realct->ALAT_numentries);
  if (cerr) return cerr;

  tdp = LSE_chkpt::build_sequence(0);
  if (!tdp) return LSE_chkpt::error_OutOfMemory;

  for (i=0;i<realct->ALAT_numentries;i++) {
    LSE_chkpt::data_t *edp;
    edp = LSE_chkpt::build_sequence(tdp);
    if (!edp) goto oom;
    dp = LSE_chkpt::build_signed(edp,realct->ALAT[i].tag);
    if (!dp) goto oom;
    dp = LSE_chkpt::build_signed(edp,realct->ALAT[i].len);
    if (!dp) goto oom;
    dp = LSE_chkpt::build_unsigned(edp,realct->ALAT[i].addr);
    if (!dp) goto oom;
    edp->update_size();
  }

  cerr = cptFile->write_to_segment(true, tdp);
  if (cerr) return cerr;
  return LSE_chkpt::error_None;
 oom:
  delete (tdp);
  return LSE_chkpt::error_OutOfMemory;
}

extern LSE_chkpt::error_t 
Linux_chkpt_write_segment(LSE_emu_interface_t *intr,
			  LSE_chkpt::file_t *cptFile, const char *segmentName, 
			  int step, LSE_emu_chkpt_cntl_t *ctl);

extern LSE_chkpt::error_t 
Linux_chkpt_read_segment(LSE_emu_interface_t *intr,
			 LSE_chkpt::file_t *cptFile, const char *segmentName, 
			 int step, LSE_emu_chkpt_cntl_t *ctl);

enum cptags {
  tag_ISACONTEXT = 1,
};

LSE_chkpt::error_t EMU_write_context(LSE_chkpt::file_t *cptFile, int nmark,
				     IA64_context_t *realct,
				     LSE_emu_chkpt_cntl_t *ctl) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp;

  /* write an indefinite header for the context */
  dp = LSE_chkpt::build_header(0, tag_ISACONTEXT,
			       LSE_chkpt::CLASS_CONTEXTSPECIFIC | 
			       LSE_chkpt::CONSTRUCTED, -1);

  if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;

  write_unsigned(cptFile, &cerr, realct->mem->getOID());
  write_unsigned(cptFile, &cerr, realct->hwcno);
  write_unsigned(cptFile, &cerr, realct->startaddr);
  write_boolean(cptFile, &cerr, realct->done);
  if (cerr) return cerr;

  /* write registers */
  if ((cerr = write_regs(cptFile, realct))) return cerr;

  /* write ALAT */
  if ((cerr = write_alat(cptFile, realct))) return cerr;

  /* and terminate the context, since it was indefinite */
  dp = LSE_chkpt::build_indefinite_end(0);

  return cptFile->write_to_segment(true, dp);
}
					   
/*
 * checkpointing: add a segment to a checkpoint
 */
LSE_chkpt::error_t
EMU_chkpt_write_segment(LSE_emu_interface_t *intr,
			LSE_chkpt::file_t *cptFile, const char *segmentName, 
			int step, LSE_emu_chkpt_cntl_t *ctl)
{
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp,*tdp;

  IA64_dinst_t *di = (IA64_dinst_t *)(intr->etoken);

  cerr = cptFile->begin_segment_write(segmentName);
  if (cerr) return cerr;

  switch (step) {
  case 0:

    /* write out the physical memory */
    if ((cerr = di->mem->writeChkpt(cptFile, ctl->incrementalMem)))
      return cerr;

    // Write out global Linux emulator state; doing so will iterate over
    // the contexts as well.
    if ((cerr = Linux_chkpt_write_segment(intr, cptFile, segmentName, step, 
					  ctl))) return cerr;


    // List of hardware contexts; mappings are implied by what the contexts
    // have said the mappings were.  Of course, I will need to update those...

    if ((cerr = write_ctable(intr, cptFile))) return cerr;

   break;

  case 1:

    // Ask Linux to dump OS records and memory records as it goes along.
    cerr = Linux_chkpt_write_segment(intr, cptFile, segmentName, step, ctl);
    if (cerr) return cerr;

    /* dump incremental device records */
    if (ctl->incrementalMem) 
      if ((cerr = LSE_device::incrWriteChkpt(cptFile))) return cerr;

    break;
  default:
    return LSE_chkpt::error_BadArgument;
  }

  cerr = cptFile->end_segment_write();
  if (cerr) return cerr;
  return LSE_chkpt::error_None;
}

/****************************** Reading routines *************************/

static inline LSE_chkpt::data_t *next(LSE_chkpt::data_t *&p) {
  LSE_chkpt::data_t *t = p;
  p = p->sibling;
  return t;
}

static LSE_chkpt::error_t validate_children(LSE_chkpt::data_t *dp, int num,
                                            bool atleast=false) {
  dp = dp->oldestChild;
  while (num && dp)  { dp=dp->sibling; num--; }
  if (dp && !num && !atleast || num && !dp) return LSE_chkpt::error_FileFormat;
  else return LSE_chkpt::error_None;
}

/*
 * here we must do the reverse of write_regs, taking into account sizing...
 */
static LSE_chkpt::error_t
read_regs(LSE_chkpt::data_t **regs, IA64_context_t *realct) {
  LSE_chkpt::data_t *dp;

  dp = *regs;

  /***** handle low Rs *****/

  if (!dp) return LSE_chkpt::error_FileFormat;
  if (LSE_end_is_big || sizeof(realct->r) != RLOW_SIZE) {
    /* TODO */
  }
  else {
    memcpy((unsigned char *)realct->r, dp->content.ustringVal, dp->size);
  }
  dp = dp->sibling;

  /*********** handle Fs **********/

  if (!dp) return LSE_chkpt::error_FileFormat;
  if (LSE_end_is_big || sizeof(realct->f) != F_SIZE) {
    /* TODO */
  }
  else {
    memcpy((unsigned char *)realct->f, dp->content.ustringVal, dp->size);
  }
  dp = dp->sibling;

  /************ handle Ps ***********/

  if (!dp) return LSE_chkpt::error_FileFormat;
  if (LSE_end_is_big || sizeof(realct->p) != P_SIZE) {
    /* TODO */
  }
  else {
    memcpy((unsigned char *)realct->p, dp->content.ustringVal, dp->size);
  }
  dp = dp->sibling;
  
  /************ handle Bs ***********/

  if (!dp) return LSE_chkpt::error_FileFormat;
  if (LSE_end_is_big || sizeof(realct->b) != B_SIZE) {
    /* TODO */
  }
  else {
    memcpy((unsigned char *)realct->b, dp->content.ustringVal, dp->size);
  }
  dp = dp->sibling;
    
  /************ handle ARs ***********/

  if (!dp) return LSE_chkpt::error_FileFormat;
  if (LSE_end_is_big || sizeof(realct->ar) != AR_SIZE) {
    /* TODO */
  }
  else {
    memcpy((unsigned char *)realct->ar, dp->content.ustringVal, dp->size);
  }
  dp = dp->sibling;
  
  /************ handle physrs ***********/

  if (!dp) return LSE_chkpt::error_FileFormat;
  realct->num_stacked_phys = (int)dp->content.int64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  if (LSE_end_is_big || 
      sizeof(realct->physr) != PHYSR_SIZE * IA64_RSE_N_STACKED_IMPL) {
    /* TODO */
  }
  else {
    memcpy((unsigned char *)realct->physr, dp->content.ustringVal, dp->size);
  }
  dp = dp->sibling;

  /************ handle SRs ***********/

  if (!dp) return LSE_chkpt::error_FileFormat;
  realct->sr[0].CFM.val = dp->content.uint64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  realct->sr[0].CFM.BOF = (unsigned int)dp->content.uint64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  realct->sr[1].PSR = dp->content.uint64Val;
  dp = dp->sibling;

  /************ handle RSE ***********/

  if (!dp) return LSE_chkpt::error_FileFormat;
  realct->RSE_other.NDirty = (long)dp->content.int64Val;
  dp = dp->sibling;

  if (!dp) return LSE_chkpt::error_FileFormat;
  realct->RSE_other.NClean = (long)dp->content.int64Val;
  dp = dp->sibling;

  *regs = dp; /* how far we got... */

  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t 
read_alat(LSE_chkpt::data_t **alat,IA64_context_t *realct)
{
  /* going to write out the number of entries and then a list with
   * each entry
   */
  LSE_chkpt::data_t *dp,*tdp, *edp;
  int i, newsize;

  tdp = *alat;

  /* read size */  
  if (!tdp) return LSE_chkpt::error_FileFormat;
  newsize = tdp->content.uint64Val;

  /* allocate array */
  if (newsize != realct->ALAT_size) {
    if (realct->ALAT) free(realct->ALAT);
    realct->ALAT = (IA64_ALAT_t *)malloc(newsize * sizeof(IA64_ALAT_t));
    if (!realct->ALAT) return LSE_chkpt::error_OutOfMemory;
    realct->ALAT_size = newsize;
  }
  tdp = tdp->sibling;

  /* read num entries */  
  if (!tdp) return LSE_chkpt::error_FileFormat;
  realct->ALAT_numentries = tdp->content.uint64Val;
  tdp = tdp->sibling;

  /* read entries */
  edp = tdp->oldestChild;
  for (i=0;i<realct->ALAT_numentries;i++) {
    if (!edp) return LSE_chkpt::error_FileFormat;
    dp = edp->oldestChild;

    if (!dp) return LSE_chkpt::error_FileFormat;
    realct->ALAT[i].tag = dp->content.int64Val;  
    dp = dp->sibling;

    if (!dp) return LSE_chkpt::error_FileFormat;
    realct->ALAT[i].len = dp->content.int64Val;
    dp = dp->sibling;

    if (!dp) return LSE_chkpt::error_FileFormat;
    realct->ALAT[i].addr = dp->content.uint64Val;
    dp = dp->sibling;
    
    if (dp) return LSE_chkpt::error_FileFormat;
    edp = edp->sibling;
  }
  tdp = tdp->sibling;

  *alat = tdp;

  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t 
test_for_indefinite(LSE_chkpt::file_t *cptFile) {
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *dp;

  /* make sure we got the indefinite end */
  cerr = cptFile->read_from_segment(0,&dp);
  if (cerr) return cerr;
  if (dp) {
    delete (dp);
    return LSE_chkpt::error_FileFormat;
  }
  return LSE_chkpt::error_None;
}

LSE_chkpt::error_t 
EMU_parse_context(LSE_emu_interface_t *intr, 
		  LSE_chkpt::file_t *cptFile,
		  IA64_context_t *&realct,
		  LSE_emu_chkpt_cntl_t *ctl) {
  LSE_chkpt::error_t cerr;
  IA64_dinst_t *di = (IA64_dinst_t *)(intr->etoken);
  LSE_chkpt::data_t *tdp, *dp;
  
  if ((cerr = cptFile->read_taglen_from_segment(0,&tdp))) return cerr;
  if ((cerr = cptFile->read_body_from_segment(0,tdp))) goto deltdp;

  // If no current context, allocate one.
  if (!realct) EMU_context_copy(intr, &realct, 0);

  /* checkpoint information */
  if ((cerr = validate_children(tdp, 3, true))) goto deltdp;
  dp = tdp->oldestChild;

  next(dp);
#ifdef NOMORE
  // This code is no longer with us because the redirection which Linux
  // does is a royal mess.  Eventually we should use a proper soft MMU
  // and then we will just point at physical memories and the reference
  // counting will work.
  memory_t *newm;
  if ((newm = (memory_t *)cptFile->idToPtr[next(dp)->content.uint64Val])
      != realct->mem) {
    if (newm) newm->incr();
    if (realct->mem) realct->mem->decr();
    realct->mem = newm;
  }
#endif
  realct->hwcno = next(dp)->content.uint64Val;
  if (realct->hwcno) di->mappedCtx[realct->hwcno] = realct;
  realct->startaddr = next(dp)->content.uint64Val; 
  realct->done = next(dp)->content.booleanVal;

  cerr = read_regs(&dp,realct);
  if (cerr) return cerr;

  cerr = read_alat(&dp,realct);
  if (cerr) return cerr;

  delete tdp;
  return LSE_chkpt::error_None;
 deltdp:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

static void fixup_cno(LSE_emu_interface_t *intr, int ocno, int ncno) {
  IA64_dinst_t *di = (IA64_dinst_t *)(intr->etoken);
  LSE_emu_update_context_map(ncno, di->mappedCtx[ocno]);
  if (di->mappedCtx[ocno]) di->mappedCtx[ocno]->hwcno = ncno;
}

/*
 * checkpointing: read the physical memory
 */
static LSE_chkpt::error_t
read_memory(LSE_emu_interface_t *intr,
	    LSE_chkpt::file_t *cptFile,
	    LSE_emu_chkpt_cntl_t *ctl) {

  LSE_chkpt::error_t cerr;
  IA64_dinst_t *di = (IA64_dinst_t *)(intr->etoken);
  LSE_chkpt::data_t *tdp, *dp;
  
  if ((cerr = cptFile->read_taglen_from_segment(0,&tdp))) return cerr;

  // There must be a device here.  As a result, different instances had
  // better not share physical memory.

  if (tdp->actualTag != LSE_device::tag_DEVICE) goto deltdp;
  
  { // fix cross-initialization jump via indentation
    memory_t *st;
    if ((cerr = cptFile->read_from_segment(0,&dp))) goto deltdp;
    uint64_t oid = dp->content.uint64Val;
    delete dp;
    if ((cerr = cptFile->read_from_segment(0,&dp))) goto deltdp;
    std::string s(dp->content.stringVal, dp->size);
    delete dp;
    
    if ((cerr = cptFile->read_from_segment(0,&dp))) goto deltdp;
    std::string n(dp->content.stringVal, dp->size);
    delete dp;
    
    // We reload it but do not need to replace it.
    cptFile->idToPtr[oid] = di->mem; 

    cerr = di->mem->readChkptGuts(cptFile, ctl->incrementalMem);
  }

  delete tdp;
  return cerr;

 deltdp:
  delete tdp;
  return LSE_chkpt::error_FileFormat;
}

/*
 * checkpointing: read from a segment
 */
LSE_chkpt::error_t
EMU_chkpt_read_segment(LSE_emu_interface_t *intr,
		       LSE_chkpt::file_t *cptFile,
		       const char *segmentName,
		       int step,
		       LSE_emu_chkpt_cntl_t *ctl) {
  LSE_chkpt::error_t cerr;
  char *sname=0;

  IA64_dinst_t *di = (IA64_dinst_t *)(intr->etoken);

  if (step > 1 || step < 0) return LSE_chkpt::error_BadArgument;

  cerr = cptFile->begin_segment_read(&sname);
  if (cerr) {
    if (sname) free(sname);
    return cerr;
  }

  /* verify that it is the right segment */
  if (segmentName && strcmp(segmentName,sname)) {
    if (sname) free(sname);
    return LSE_chkpt::error_Application;
  }
  if (sname) free(sname);

  di->mappedCtx.clear();

  if (step == 0) 
    if ((cerr = read_memory(intr, cptFile, ctl))) return cerr;

  if ((cerr = Linux_chkpt_read_segment(intr, cptFile, segmentName, step, ctl)))
    return cerr;

  if (step == 0) { /* read hwcno records */
    if ((cerr = read_ctable(intr, cptFile, fixup_cno))) return cerr;
    di->mappedCtx.clear();
  }

  /* read incremental device records */
  if (step == 1 && ctl->incrementalMem) {
    if ((cerr = LSE_device::incrReadChkpt(cptFile))) return cerr;
  } else 
    if ((cerr = test_for_indefinite(cptFile))) return cerr;
    
  cerr = cptFile->end_segment_read(true);
  if (cerr) return cerr;

  return LSE_chkpt::error_None;
}

/*
 * end use of replay mechanisms
 */
void
EMU_chkpt_end_replay(LSE_emu_interface_t *intr) {
}

} // namespace LSE_IA64
