_start:
	fadd	f2 = f1, f1;;
	fadd	f3 = f2, f1
	fadd	f4 = f2, f2 ;;

	frsqrta f20,p10 = f0
	frsqrta f21,p11 = f1
	frsqrta f22,p12 = f2

	frsqrta f23,p13 = f3
	frsqrta f24,p14 = f4 ;;

	fpack	f10 = f0, f0 
	fpack	f11 = f1, f1 
	fpack 	f12 = f2, f2 
	fpack 	f13 = f3, f0 ;;

	fprsqrta f30, p20 = f10
	fprsqrta f31, p21 = f11
	fprsqrta f32, p22 = f12
	fprsqrta f33, p23 = f13 ;;

	getf.sig  r10 = f30
	getf.sig  r11 = f31
	getf.sig  r12 = f32
	getf.sig  r13 = f33 ;;

	shr r20 = r10, 32 
	shr r21 = r11, 32 
	shr r22 = r12, 32
	shr r23 = r13, 32 ;;

	setf.s f40 = r20
	setf.s f41 = r21
	setf.s f42 = r22
	setf.s f43 = r23 
	setf.s f50 = r10
	setf.s f51 = r11
	setf.s f52 = r12
	setf.s f53 = r13 

	break 0	
