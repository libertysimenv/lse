.text
	.align 16
	.global _start#
_start:
	//// ***** A ****

	(p1) add r1= r2, r3  
	(p16) add r1= r2, r3  
	(p1) add r33= r2, r3  
	(p1) add r1= r34, r3  
	(p1) add r1= r2, r35  
	(p2) add r1 = r2, r3, 1 
	(p17) add r1 = r2, r3, 1 
	(p2) add r33 = r2, r3, 1 
	(p2) add r1 = r34, r3, 1 
	(p2) add r1 = r2, r35, 1 
	(p3) add r1 = 1, r3 
	(p18) add r1 = 1, r3 
	(p3) add r33 = 1, r3 
	(p3) add r1 = 1, r35

	(p5) adds r1 = 14, r3 
	(p20) adds r1 = 14, r3 
	(p5) adds r33 = 14, r3 
	(p5) adds r1 = 14, r35

	(p7) addl r1 = 22, r3 
	(p22) addl r1 = 22, r3 
	(p7) addl r33 = 22, r3 

	(p9) addp4 r1 = r2, r3 
	(p24) addp4 r1 = r2, r3 
	(p9) addp4 r33 = r2, r3 
	(p9) addp4 r1 = r34, r3 
	(p9) addp4 r1 = r2, r35 
	(p10) addp4 r1 = 14, r3
	(p25) addp4 r1 = 14, r3
	(p10) addp4 r33 = 14, r3
	(p10) addp4 r1 = 14, r35

	alloc r1 = ar.pfs, 4, 12, 2, 8 
	alloc r33 = ar.pfs, 4, 12, 2, 8 

	(p12) and r1 = r2, r3 
	(p27) and r1 = r2, r3 
	(p12) and r33 = r2, r3 
	(p12) and r1 = r34, r3 
	(p12) and r1 = r2, r35
	(p13) and r1 = 8, r3 
	(p28) and r1 = 8, r3 
	(p13) and r33 = 8, r3 
	(p13) and r1 = 8, r35

.alabel:
	(p15) andcm r1 = r2, r3
	(p30) andcm r1 = r2, r3
	(p15) andcm r33 = r2, r3
	(p15) andcm r1 = r34, r3
	(p15) andcm r1 = r2, r35
	(p1) andcm r1 = 8, r3
	(p16) andcm r1 = 8, r3
	(p1) andcm r33 = 8, r3
	(p1) andcm r1 = 8, r35

	///  ***** B *****

	// B1
	(p2) br.spnt.few	.alabel
	(p18) br.spnt.few	.alabel
	(p3) br.spnt.few.clr 	.alabel
	(p19) br.spnt.few.clr 	.alabel
	(p4) br.spnt		.alabel
	(p20) br.spnt		.alabel
	(p5) br.spnt.clr 	.alabel
	(p21) br.spnt.clr 	.alabel
	(p6) br.spnt.many	.alabel
	(p22) br.spnt.many	.alabel
	(p7) br.spnt.many.clr 	.alabel
	(p23) br.spnt.many.clr 	.alabel
	(p8) br.sptk.few	.alabel
	(p24) br.sptk.few	.alabel
	(p9) br.sptk.few.clr 	.alabel
	(p25) br.sptk.few.clr 	.alabel
	(p10) br.sptk		.alabel
	(p26) br.sptk		.alabel
	(p11) br.sptk.clr 	.alabel
	(p27) br.sptk.clr 	.alabel
	(p12) br.sptk.many	.alabel
	(p28) br.sptk.many	.alabel
	(p13) br.sptk.many.clr 	.alabel
	(p29) br.sptk.many.clr 	.alabel
	(p14) br.dpnt.few	.alabel
	(p30) br.dpnt.few	.alabel
	(p15) br.dpnt.few.clr 	.alabel
	(p31) br.dpnt.few.clr 	.alabel
	(p0) br.dpnt		.alabel
	(p32) br.dpnt		.alabel
	(p1) br.dpnt.clr 	.alabel
	(p33) br.dpnt.clr 	.alabel
	(p2) br.dpnt.many	.alabel
	(p34) br.dpnt.many	.alabel
	(p3) br.dpnt.many.clr 	.alabel
	(p35) br.dpnt.many.clr 	.alabel
	(p4) br.dptk.few	.alabel
	(p36) br.dptk.few	.alabel
	(p5) br.dptk.few.clr 	.alabel
	(p37) br.dptk.few.clr 	.alabel
	(p6) br.dptk		.alabel
	(p38) br.dptk		.alabel
	(p7) br.dptk.clr 	.alabel
	(p39) br.dptk.clr 	.alabel
	(p8) br.dptk.many	.alabel
	(p40) br.dptk.many	.alabel
	(p9) br.dptk.many.clr 	.alabel
	(p41) br.dptk.many.clr 	.alabel
	(p10) br.cond.spnt.few	.alabel
	(p42) br.cond.spnt.few	.alabel
	(p11) br.cond.spnt.few.clr 	.alabel
	(p43) br.cond.spnt.few.clr 	.alabel
	(p12) br.cond.spnt		.alabel
	(p44) br.cond.spnt		.alabel
	(p13) br.cond.spnt.clr 	.alabel
	(p45) br.cond.spnt.clr 	.alabel
	(p14) br.cond.spnt.many	.alabel
	(p46) br.cond.spnt.many	.alabel
	(p15) br.cond.spnt.many.clr 	.alabel
	(p47) br.cond.spnt.many.clr 	.alabel
	(p0) br.cond.sptk.few	.alabel
	(p48) br.cond.sptk.few	.alabel
	(p1) br.cond.sptk.few.clr 	.alabel
	(p49) br.cond.sptk.few.clr 	.alabel
	(p2) br.cond.sptk		.alabel
	(p50) br.cond.sptk		.alabel
	(p3) br.cond.sptk.clr 	.alabel
	(p51) br.cond.sptk.clr 	.alabel
	(p4) br.cond.sptk.many	.alabel
	(p52) br.cond.sptk.many	.alabel
	(p5) br.cond.sptk.many.clr 	.alabel
	(p53) br.cond.sptk.many.clr 	.alabel
	(p6) br.cond.dpnt.few	.alabel
	(p54) br.cond.dpnt.few	.alabel
	(p7) br.cond.dpnt.few.clr 	.alabel
	(p55) br.cond.dpnt.few.clr 	.alabel
	(p8) br.cond.dpnt		.alabel
	(p56) br.cond.dpnt		.alabel
	(p9) br.cond.dpnt.clr 	.alabel
	(p57) br.cond.dpnt.clr 	.alabel
	(p10) br.cond.dpnt.many	.alabel
	(p58) br.cond.dpnt.many	.alabel
	(p11) br.cond.dpnt.many.clr 	.alabel
	(p59) br.cond.dpnt.many.clr 	.alabel
	(p12) br.cond.dptk.few	.alabel
	(p60) br.cond.dptk.few	.alabel
	(p13) br.cond.dptk.few.clr 	.alabel
	(p61) br.cond.dptk.few.clr 	.alabel
	(p14) br.cond.dptk		.alabel
	(p62) br.cond.dptk		.alabel
	(p15) br.cond.dptk.clr 	.alabel
	(p63) br.cond.dptk.clr 	.alabel
	(p0) br.cond.dptk.many	.alabel
	(p16) br.cond.dptk.many	.alabel
	(p1) br.cond.dptk.many.clr 	.alabel
	(p17) br.cond.dptk.many.clr 	.alabel
	(p2) br.wtop.spnt.few	.alabel
	(p18) br.wtop.spnt.few	.alabel
	(p3) br.wtop.spnt.few.clr 	.alabel
	(p19) br.wtop.spnt.few.clr 	.alabel
	(p4) br.wtop.spnt		.alabel
	(p20) br.wtop.spnt		.alabel
	(p5) br.wtop.spnt.clr 	.alabel
	(p21) br.wtop.spnt.clr 	.alabel
	(p6) br.wtop.spnt.many	.alabel
	(p22) br.wtop.spnt.many	.alabel
	(p7) br.wtop.spnt.many.clr 	.alabel
	(p23) br.wtop.spnt.many.clr 	.alabel
	(p8) br.wtop.sptk.few	.alabel
	(p24) br.wtop.sptk.few	.alabel
	(p9) br.wtop.sptk.few.clr 	.alabel
	(p25) br.wtop.sptk.few.clr 	.alabel
	(p10) br.wtop.sptk		.alabel
	(p26) br.wtop.sptk		.alabel
	(p11) br.wtop.sptk.clr 	.alabel
	(p27) br.wtop.sptk.clr 	.alabel
	(p12) br.wtop.sptk.many	.alabel
	(p28) br.wtop.sptk.many	.alabel
	(p13) br.wtop.sptk.many.clr 	.alabel
	(p29) br.wtop.sptk.many.clr 	.alabel
	(p14) br.wtop.dpnt.few	.alabel
	(p30) br.wtop.dpnt.few	.alabel
	(p15) br.wtop.dpnt.few.clr 	.alabel
	(p31) br.wtop.dpnt.few.clr 	.alabel
	(p0) br.wtop.dpnt		.alabel
	(p16) br.wtop.dpnt		.alabel
	(p1) br.wtop.dpnt.clr 	.alabel
	(p17) br.wtop.dpnt.clr 	.alabel
	(p2) br.wtop.dpnt.many	.alabel
	(p18) br.wtop.dpnt.many	.alabel
	(p3) br.wtop.dpnt.many.clr 	.alabel
	(p19) br.wtop.dpnt.many.clr 	.alabel
	(p4) br.wtop.dptk.few	.alabel
	(p20) br.wtop.dptk.few	.alabel
	(p5) br.wtop.dptk.few.clr 	.alabel
	(p21) br.wtop.dptk.few.clr 	.alabel
	(p6) br.wtop.dptk		.alabel
	(p22) br.wtop.dptk		.alabel
	(p7) br.wtop.dptk.clr 	.alabel
	(p23) br.wtop.dptk.clr 	.alabel
	(p8) br.wtop.dptk.many	.alabel	
	(p24) br.wtop.dptk.many	.alabel
	(p9) br.wtop.dptk.many.clr 	.alabel
	(p25) br.wtop.dptk.many.clr 	.alabel
	(p10) br.wexit.spnt.few	.alabel
	(p26) br.wexit.spnt.few	.alabel
	(p11) br.wexit.spnt.few.clr 	.alabel
	(p27) br.wexit.spnt.few.clr 	.alabel
	(p12) br.wexit.spnt		.alabel
	(p28) br.wexit.spnt		.alabel
	(p13) br.wexit.spnt.clr 	.alabel
	(p29) br.wexit.spnt.clr 	.alabel
	(p14) br.wexit.spnt.many	.alabel	
	(p30) br.wexit.spnt.many	.alabel
	(p15) br.wexit.spnt.many.clr 	.alabel
	(p31) br.wexit.spnt.many.clr 	.alabel
	(p0) br.wexit.sptk.few	.alabel
	(p32) br.wexit.sptk.few	.alabel
	(p1) br.wexit.sptk.few.clr 	.alabel
	(p33) br.wexit.sptk.few.clr 	.alabel
	(p2) br.wexit.sptk		.alabel
	(p34) br.wexit.sptk		.alabel
	(p3) br.wexit.sptk.clr 	.alabel	
	(p35) br.wexit.sptk.clr 	.alabel
	(p4) br.wexit.sptk.many	.alabel
	(p36) br.wexit.sptk.many	.alabel
	(p5) br.wexit.sptk.many.clr 	.alabel
	(p37) br.wexit.sptk.many.clr 	.alabel
	(p6) br.wexit.dpnt.few	.alabel
	(p38) br.wexit.dpnt.few	.alabel
	(p7) br.wexit.dpnt.few.clr 	.alabel
	(p39) br.wexit.dpnt.few.clr 	.alabel
	(p8) br.wexit.dpnt		.alabel
	(p40) br.wexit.dpnt		.alabel
	(p9) br.wexit.dpnt.clr 	.alabel	
	(p41) br.wexit.dpnt.clr 	.alabel
	(p10) br.wexit.dpnt.many	.alabel
	(p42) br.wexit.dpnt.many	.alabel
	(p11) br.wexit.dpnt.many.clr 	.alabel
	(p43) br.wexit.dpnt.many.clr 	.alabel
	(p12) br.wexit.dptk.few	.alabel
	(p44) br.wexit.dptk.few	.alabel
	(p13) br.wexit.dptk.few.clr 	.alabel
	(p45) br.wexit.dptk.few.clr 	.alabel
	(p14) br.wexit.dptk		.alabel
	(p46) br.wexit.dptk		.alabel
	(p15) br.wexit.dptk.clr 	.alabel
	(p47) br.wexit.dptk.clr 	.alabel
	(p0) br.wexit.dptk.many	.alabel
	(p48) br.wexit.dptk.many	.alabel
	(p1) br.wexit.dptk.many.clr 	.alabel
	(p49) br.wexit.dptk.many.clr 	.alabel
	// B3
	(p2) br.call.spnt.few	b1 = .alabel
	(p50) br.call.spnt.few	b1 = .alabel
	(p3) br.call.spnt.few.clr 	b1 = .alabel
	(p51) br.call.spnt.few.clr 	b1 = .alabel
	(p4) br.call.spnt		b1 = .alabel
	(p52) br.call.spnt		b1 = .alabel
	(p5) br.call.spnt.clr 	b1 = .alabel
	(p53) br.call.spnt.clr 	b1 = .alabel
	(p6) br.call.spnt.many	b1 = .alabel
	(p54) br.call.spnt.many	b1 = .alabel
	(p7) br.call.spnt.many.clr 	b1 = .alabel	
	(p55) br.call.spnt.many.clr 	b1 = .alabel
	(p8) br.call.sptk.few	b1 = .alabel
	(p56) br.call.sptk.few	b1 = .alabel	
	(p9) br.call.sptk.few.clr 	b1 = .alabel
	(p57) br.call.sptk.few.clr 	b1 = .alabel
	(p10) br.call.sptk		b1 = .alabel
	(p58) br.call.sptk		b1 = .alabel
	(p11) br.call.sptk.clr 	b1 = .alabel
	(p59) br.call.sptk.clr 	b1 = .alabel
	(p12) br.call.sptk.many	b1 = .alabel
	(p60) br.call.sptk.many	b1 = .alabel
	(p13) br.call.sptk.many.clr 	b1 = .alabel
	(p61) br.call.sptk.many.clr 	b1 = .alabel
	(p14) br.call.dpnt.few	b1 = .alabel
	(p62) br.call.dpnt.few	b1 = .alabel
	(p15) br.call.dpnt.few.clr 	b1 = .alabel
	(p63) br.call.dpnt.few.clr 	b1 = .alabel
	(p0) br.call.dpnt		b1 = .alabel	
	(p16) br.call.dpnt		b1 = .alabel
	(p1) br.call.dpnt.clr 	b1 = .alabel
	(p17) br.call.dpnt.clr 	b1 = .alabel
	(p2) br.call.dpnt.many	b1 = .alabel
	(p18) br.call.dpnt.many	b1 = .alabel
	(p3) br.call.dpnt.many.clr 	b1 = .alabel
	(p19) br.call.dpnt.many.clr 	b1 = .alabel
	(p4) br.call.dptk.few	b1 = .alabel
	(p20) br.call.dptk.few	b1 = .alabel
	(p5) br.call.dptk.few.clr 	b1 = .alabel
	(p21) br.call.dptk.few.clr 	b1 = .alabel
	(p6) br.call.dptk		b1 = .alabel
	(p22) br.call.dptk		b1 = .alabel
	(p7) br.call.dptk.clr 	b1 = .alabel
	(p23) br.call.dptk.clr 	b1 = .alabel
	(p8) br.call.dptk.many	b1 = .alabel
	(p24) br.call.dptk.many	b1 = .alabel
	(p9) br.call.dptk.many.clr 	b1 = .alabel
	(p25) br.call.dptk.many.clr 	b1 = .alabel
	// B2
	 br.cloop.spnt.few	.alabel
	 br.cloop.spnt.few.clr 	.alabel
	 br.cloop.spnt		.alabel
	 br.cloop.spnt.clr 	.alabel
	 br.cloop.spnt.many	.alabel
	 br.cloop.spnt.many.clr 	.alabel
	 br.cloop.sptk.few	.alabel
	 br.cloop.sptk.few.clr 	.alabel
	 br.cloop.sptk		.alabel
	 br.cloop.sptk.clr 	.alabel
	 br.cloop.sptk.many	.alabel
	 br.cloop.sptk.many.clr 	.alabel
	 br.cloop.dpnt.few	.alabel
	 br.cloop.dpnt.few.clr 	.alabel
	 br.cloop.dpnt		.alabel
	 br.cloop.dpnt.clr 	.alabel
	 br.cloop.dpnt.many	.alabel
	 br.cloop.dpnt.many.clr 	.alabel
	 br.cloop.dptk.few	.alabel
	 br.cloop.dptk.few.clr 	.alabel
	 br.cloop.dptk		.alabel
	 br.cloop.dptk.clr 	.alabel
	 br.cloop.dptk.many	.alabel
	 br.cloop.dptk.many.clr 	.alabel
	 br.ctop.spnt.few	.alabel
	 br.ctop.spnt.few.clr 	.alabel
	 br.ctop.spnt		.alabel
	 br.ctop.spnt.clr 	.alabel
	 br.ctop.spnt.many	.alabel
	 br.ctop.spnt.many.clr 	.alabel
	 br.ctop.sptk.few	.alabel
	 br.ctop.sptk.few.clr 	.alabel
	 br.ctop.sptk		.alabel
	 br.ctop.sptk.clr 	.alabel
	 br.ctop.sptk.many	.alabel
	 br.ctop.sptk.many.clr 	.alabel
	 br.ctop.dpnt.few	.alabel
	 br.ctop.dpnt.few.clr 	.alabel
	 br.ctop.dpnt		.alabel
	 br.ctop.dpnt.clr 	.alabel
	 br.ctop.dpnt.many	.alabel
	 br.ctop.dpnt.many.clr 	.alabel
	 br.ctop.dptk.few	.alabel
	 br.ctop.dptk.few.clr 	.alabel
	 br.ctop.dptk		.alabel
	 br.ctop.dptk.clr 	.alabel
	 br.ctop.dptk.many	.alabel
	 br.ctop.dptk.many.clr 	.alabel
	 br.cexit.spnt.few	.alabel
	 br.cexit.spnt.few.clr 	.alabel
	 br.cexit.spnt		.alabel
	 br.cexit.spnt.clr 	.alabel
	 br.cexit.spnt.many	.alabel
	 br.cexit.spnt.many.clr 	.alabel
	 br.cexit.sptk.few	.alabel
	 br.cexit.sptk.few.clr 	.alabel
	 br.cexit.sptk		.alabel
	 br.cexit.sptk.clr 	.alabel
	 br.cexit.sptk.many	.alabel
	 br.cexit.sptk.many.clr 	.alabel
	 br.cexit.dpnt.few	.alabel
	 br.cexit.dpnt.few.clr 	.alabel
	 br.cexit.dpnt		.alabel
	 br.cexit.dpnt.clr 	.alabel
	 br.cexit.dpnt.many	.alabel
	 br.cexit.dpnt.many.clr 	.alabel
	 br.cexit.dptk.few	.alabel
	 br.cexit.dptk.few.clr 	.alabel
	 br.cexit.dptk		.alabel
	 br.cexit.dptk.clr 	.alabel
	 br.cexit.dptk.many	.alabel
	 br.cexit.dptk.many.clr 	.alabel
	// pseudo-op
	 br.few		.alabel
	 br.few.clr 	.alabel
	 br		.alabel
	 br.clr 	.alabel
	 br.many	.alabel
	 br.many.clr 	.alabel
	// B4
	(p10) br.cond.spnt.few	b2
	(p26) br.cond.spnt.few	b2
	(p11) br.cond.spnt.few.clr 	b2
	(p27) br.cond.spnt.few.clr 	b2
	(p12) br.cond.spnt		b2
	(p28) br.cond.spnt		b2
	(p13) br.cond.spnt.clr 	b2
	(p29) br.cond.spnt.clr 	b2
	(p14) br.cond.spnt.many	b2
	(p30) br.cond.spnt.many	b2
	(p15) br.cond.spnt.many.clr 	b2
	(p31) br.cond.spnt.many.clr 	b2
	(p0) br.cond.sptk.few	b2
	(p16) br.cond.sptk.few	b2
	(p1) br.cond.sptk.few.clr 	b2
	(p17) br.cond.sptk.few.clr 	b2
	(p2) br.cond.sptk		b2
	(p18) br.cond.sptk		b2
	(p3) br.cond.sptk.clr 	b2
	(p19) br.cond.sptk.clr 	b2
	(p4) br.cond.sptk.many	b2
	(p20) br.cond.sptk.many	b2
	(p5) br.cond.sptk.many.clr 	b2
	(p21) br.cond.sptk.many.clr 	b2
	(p6) br.cond.dpnt.few	b2
	(p22) br.cond.dpnt.few	b2
	(p7) br.cond.dpnt.few.clr 	b2
	(p23) br.cond.dpnt.few.clr 	b2
	(p8) br.cond.dpnt		b2
	(p24) br.cond.dpnt		b2
	(p9) br.cond.dpnt.clr 	b2
	(p25) br.cond.dpnt.clr 	b2
	(p10) br.cond.dpnt.many	b2
	(p26) br.cond.dpnt.many	b2
	(p11) br.cond.dpnt.many.clr 	b2
	(p27) br.cond.dpnt.many.clr 	b2
	(p12) br.cond.dptk.few	b2
	(p28) br.cond.dptk.few	b2
	(p13) br.cond.dptk.few.clr 	b2
	(p29) br.cond.dptk.few.clr 	b2
	(p14) br.cond.dptk		b2
	(p30) br.cond.dptk		b2
	(p15) br.cond.dptk.clr 	b2
	(p31) br.cond.dptk.clr 	b2
	(p16) br.cond.dptk.many	b2
	(p32) br.cond.dptk.many	b2
	(p17) br.cond.dptk.many.clr 	b2
	(p33) br.cond.dptk.many.clr 	b2
	(p34) br.ia.spnt.few	b2
	(p35) br.ia.spnt.few.clr 	b2
	(p36) br.ia.spnt		b2
	(p37) br.ia.spnt.clr 	b2
	(p38) br.ia.spnt.many	b2
	(p39) br.ia.spnt.many.clr 	b2
	(p40) br.ia.sptk.few	b2
	(p41) br.ia.sptk.few.clr 	b2
	(p42) br.ia.sptk		b2
	(p43) br.ia.sptk.clr 	b2
	(p44) br.ia.sptk.many	b2
	(p45) br.ia.sptk.many.clr 	b2
	(p46) br.ia.dpnt.few	b2
	(p47) br.ia.dpnt.few.clr 	b2
	(p48) br.ia.dpnt		b2
	(p49) br.ia.dpnt.clr 	b2
	(p50) br.ia.dpnt.many	b2
	(p51) br.ia.dpnt.many.clr 	b2
	(p52) br.ia.dptk.few	b2
	(p53) br.ia.dptk.few.clr 	b2
	(p54) br.ia.dptk		b2
	(p55) br.ia.dptk.clr 	b2
	(p56) br.ia.dptk.many	b2
	(p57) br.ia.dptk.many.clr 	b2
	(p58) br.ret.spnt.few	b2
	(p59) br.ret.spnt.few.clr 	b2
	(p60) br.ret.spnt		b2
	(p61) br.ret.spnt.clr 	b2
	(p62) br.ret.spnt.many	b2
	(p63) br.ret.spnt.many.clr 	b2
	(p0) br.ret.sptk.few	b2
	(p16) br.ret.sptk.few	b2
	(p1) br.ret.sptk.few.clr 	b2
	(p17) br.ret.sptk.few.clr 	b2
	(p2) br.ret.sptk		b2
	(p18) br.ret.sptk		b2
	(p3) br.ret.sptk.clr 	b2
	(p19) br.ret.sptk.clr 	b2
	(p4) br.ret.sptk.many	b2
	(p20) br.ret.sptk.many	b2
	(p5) br.ret.sptk.many.clr 	b2
	(p21) br.ret.sptk.many.clr 	b2
	(p6) br.ret.dpnt.few	b2
	(p22) br.ret.dpnt.few	b2
	(p7) br.ret.dpnt.few.clr 	b2
	(p23) br.ret.dpnt.few.clr 	b2
	(p8) br.ret.dpnt		b2
	(p24) br.ret.dpnt		b2
	(p9) br.ret.dpnt.clr 	b2
	(p25) br.ret.dpnt.clr 	b2
	(p10) br.ret.dpnt.many	b2
	(p26) br.ret.dpnt.many	b2
	(p11) br.ret.dpnt.many.clr 	b2	
	(p27) br.ret.dpnt.many.clr 	b2
	(p12) br.ret.dptk.few	b2
	(p28) br.ret.dptk.few	b2
	(p13) br.ret.dptk.few.clr 	b2
	(p29) br.ret.dptk.few.clr 	b2
	(p14) br.ret.dptk		b2
	(p30) br.ret.dptk		b2
	(p15) br.ret.dptk.clr 	b2
	(p31) br.ret.dptk.clr 	b2
	(p0) br.ret.dptk.many	b2
	(p16) br.ret.dptk.many	b2
	(p1) br.ret.dptk.many.clr 	b2
	(p17) br.ret.dptk.many.clr 	b2
	// B5
	(p2) br.call.spnt.few	b1 = b2
	(p18) br.call.spnt.few	b1 = b2
	(p3) br.call.spnt.few.clr 	b1 = b2
	(p19) br.call.spnt.few.clr 	b1 = b2
	(p4) br.call.spnt		b1 = b2
	(p20) br.call.spnt		b1 = b2
	(p5) br.call.spnt.clr 	b1 = b2
	(p21) br.call.spnt.clr 	b1 = b2
	(p6) br.call.spnt.many	b1 = b2
	(p22) br.call.spnt.many	b1 = b2
	(p7) br.call.spnt.many.clr 	b1 = b2
	(p23) br.call.spnt.many.clr 	b1 = b2
	(p8) br.call.sptk.few	b1 = b2
	(p24) br.call.sptk.few	b1 = b2
	(p9) br.call.sptk.few.clr 	b1 = b2
	(p25) br.call.sptk.few.clr 	b1 = b2
	(p10) br.call.sptk		b1 = b2
	(p26) br.call.sptk		b1 = b2
	(p11) br.call.sptk.clr 	b1 = b2
	(p27) br.call.sptk.clr 	b1 = b2
	(p12) br.call.sptk.many	b1 = b2
	(p28) br.call.sptk.many	b1 = b2
	(p13) br.call.sptk.many.clr 	b1 = b2
	(p29) br.call.sptk.many.clr 	b1 = b2
	(p14) br.call.dpnt.few	b1 = b2
	(p30) br.call.dpnt.few	b1 = b2
	(p15) br.call.dpnt.few.clr 	b1 = b2
	(p31) br.call.dpnt.few.clr 	b1 = b2
	(p0) br.call.dpnt		b1 = b2
	(p32) br.call.dpnt		b1 = b2
	(p1) br.call.dpnt.clr 	b1 = b2
	(p33) br.call.dpnt.clr 	b1 = b2
	(p2) br.call.dpnt.many	b1 = b2
	(p34) br.call.dpnt.many	b1 = b2
	(p3) br.call.dpnt.many.clr 	b1 = b2
	(p35) br.call.dpnt.many.clr 	b1 = b2
	(p4) br.call.dptk.few	b1 = b2
	(p36) br.call.dptk.few	b1 = b2
	(p5) br.call.dptk.few.clr 	b1 = b2
	(p37) br.call.dptk.few.clr 	b1 = b2
	(p6) br.call.dptk		b1 = b2
	(p38) br.call.dptk		b1 = b2
	(p7) br.call.dptk.clr 	b1 = b2
	(p39) br.call.dptk.clr 	b1 = b2
	(p8) br.call.dptk.many	b1 = b2
	(p40) br.call.dptk.many	b1 = b2
	(p9) br.call.dptk.many.clr 	b1 = b2
	(p41) br.call.dptk.many.clr 	b1 = b2
	// pseudo-op
	 br.few		b2
	 br.few.clr 	b2
	 br		b2
	 br.clr 	b2
	 br.many	b2
	 br.many.clr 	b2

	(p10) break 21
	(p42) break 21
	(p11) break.i 21
	(p43) break.i 21
	(p12) break.b 21
	(p44) break.b 21
	(p13) break.m 21
	(p45) break.m 21
	(p14) break.f 21
	(p46) break.f 21
	(p15) break.x 62 
	(p47) break.x 62 

	// X3	
	(p0) brl.dpnt.many	.alabel
	(p16) brl.dpnt.many	.alabel
	(p1) brl.dpnt.many.clr 	.alabel
	(p17) brl.dpnt.many.clr 	.alabel
	(p2) brl.dptk.few	.alabel
	(p18) brl.dptk.few	.alabel
	(p3) brl.dptk.few.clr 	.alabel
	(p19) brl.dptk.few.clr 	.alabel
	(p4) brl.dptk		.alabel
	(p20) brl.dptk		.alabel
	(p5) brl.dptk.clr 	.alabel
	(p21) brl.dptk.clr 	.alabel
	(p6) brl.dptk.many	.alabel
	(p22) brl.dptk.many	.alabel
	(p7) brl.dptk.many.clr 	.alabel
	(p23) brl.dptk.many.clr 	.alabel
	(p8) brl.cond.spnt.few	.alabel
	(p24) brl.cond.spnt.few	.alabel
	(p9) brl.cond.spnt.few.clr 	.alabel
	(p25) brl.cond.spnt.few.clr 	.alabel
	(p10) brl.cond.spnt		.alabel
	(p26) brl.cond.spnt		.alabel
	(p11) brl.cond.spnt.clr 	.alabel
	(p27) brl.cond.spnt.clr 	.alabel
	(p12) brl.cond.spnt.many	.alabel
	(p28) brl.cond.spnt.many	.alabel
	(p13) brl.cond.spnt.many.clr 	.alabel
	(p29) brl.cond.spnt.many.clr 	.alabel
	(p14) brl.cond.sptk.few	.alabel
	(p30) brl.cond.sptk.few	.alabel
	(p15) brl.cond.sptk.few.clr 	.alabel
	(p31) brl.cond.sptk.few.clr 	.alabel
	(p0) brl.cond.sptk		.alabel
	(p16) brl.cond.sptk		.alabel
	(p1) brl.cond.sptk.clr 	.alabel
	(p17) brl.cond.sptk.clr 	.alabel
	(p2) brl.cond.sptk.many	.alabel
	(p18) brl.cond.sptk.many	.alabel
	(p3) brl.cond.sptk.many.clr 	.alabel
	(p19) brl.cond.sptk.many.clr 	.alabel
	(p4) brl.cond.dpnt.few	.alabel
	(p20) brl.cond.dpnt.few	.alabel
	(p5) brl.cond.dpnt.few.clr 	.alabel
	(p21) brl.cond.dpnt.few.clr 	.alabel
	(p6) brl.cond.dpnt		.alabel
	(p22) brl.cond.dpnt		.alabel
	(p7) brl.cond.dpnt.clr 	.alabel
	(p23) brl.cond.dpnt.clr 	.alabel
	(p8) brl.cond.dpnt.many	.alabel
	(p24) brl.cond.dpnt.many	.alabel
	(p9) brl.cond.dpnt.many.clr 	.alabel
	(p25) brl.cond.dpnt.many.clr 	.alabel
	(p10) brl.cond.dptk.few	.alabel
	(p26) brl.cond.dptk.few	.alabel
	(p11) brl.cond.dptk.few.clr 	.alabel
	(p27) brl.cond.dptk.few.clr 	.alabel
	(p12) brl.cond.dptk		.alabel
	(p28) brl.cond.dptk		.alabel
	(p13) brl.cond.dptk.clr 	.alabel
	(p29) brl.cond.dptk.clr 	.alabel
	(p14) brl.cond.dptk.many	.alabel
	(p30) brl.cond.dptk.many	.alabel
	(p15) brl.cond.dptk.many.clr 	.alabel
	(p31) brl.cond.dptk.many.clr 	.alabel
	// X4
	(p0) brl.call.spnt.few		b1 = .alabel
	(p32) brl.call.spnt.few		b1 = .alabel
	(p1) brl.call.spnt.few.clr 		b1 = .alabel
	(p33) brl.call.spnt.few.clr 		b1 = .alabel
	(p2) brl.call.spnt			b1 = .alabel
	(p34) brl.call.spnt			b1 = .alabel
	(p3) brl.call.spnt.clr 		b1 = .alabel
	(p35) brl.call.spnt.clr 		b1 = .alabel
	(p4) brl.call.spnt.many		b1 = .alabel
	(p36) brl.call.spnt.many		b1 = .alabel
	(p5) brl.call.spnt.many.clr 		b1 = .alabel
	(p37) brl.call.spnt.many.clr 		b1 = .alabel
	(p6) brl.call.sptk.few		b1 = .alabel
	(p38) brl.call.sptk.few		b1 = .alabel
	(p7) brl.call.sptk.few.clr 		b1 = .alabel
	(p39) brl.call.sptk.few.clr 		b1 = .alabel
	(p8) brl.call.sptk			b1 = .alabel
	(p40) brl.call.sptk			b1 = .alabel
	(p9) brl.call.sptk.clr 		b1 = .alabel
	(p41) brl.call.sptk.clr 		b1 = .alabel
	(p10) brl.call.sptk.many		b1 = .alabel
	(p42) brl.call.sptk.many		b1 = .alabel
	(p11) brl.call.sptk.many.clr 		b1 = .alabel
	(p43) brl.call.sptk.many.clr 		b1 = .alabel
	(p12) brl.call.dpnt.few		b1 = .alabel
	(p44) brl.call.dpnt.few		b1 = .alabel
	(p13) brl.call.dpnt.few.clr 		b1 = .alabel
	(p45) brl.call.dpnt.few.clr 		b1 = .alabel
	(p14) brl.call.dpnt			b1 = .alabel
	(p46) brl.call.dpnt			b1 = .alabel
	(p15) brl.call.dpnt.clr 		b1 = .alabel
	(p47) brl.call.dpnt.clr 		b1 = .alabel
	(p0) brl.call.dpnt.many		b1 = .alabel
	(p48) brl.call.dpnt.many		b1 = .alabel
	(p1) brl.call.dpnt.many.clr 		b1 = .alabel
	(p49) brl.call.dpnt.many.clr 		b1 = .alabel
	(p2) brl.call.dptk.few		b1 = .alabel
	(p50) brl.call.dptk.few		b1 = .alabel
	(p3) brl.call.dptk.few.clr 		b1 = .alabel
	(p51) brl.call.dptk.few.clr 		b1 = .alabel
	(p4) brl.call.dptk			b1 = .alabel
	(p52) brl.call.dptk			b1 = .alabel
	(p5) brl.call.dptk.clr 		b1 = .alabel
	(p53) brl.call.dptk.clr 		b1 = .alabel
	(p6) brl.call.dptk.many		b1 = .alabel
	(p54) brl.call.dptk.many		b1 = .alabel
	(p7) brl.call.dptk.many.clr 		b1 = .alabel
	(p55) brl.call.dptk.many.clr 		b1 = .alabel
	// pseudo-op
	 brl.few		.alabel
	 brl.few.clr 	.alabel
	 brl		.alabel
	 brl.clr 	.alabel
	 brl.many	.alabel
	 brl.many.clr 	.alabel

	// B6
	brp.sptk		.alabel, 0x30
	brp.sptk.imp		.alabel, 0x30
	brp.loop		.alabel, 0x30
	brp.loop.imp		.alabel, 0x30
	brp.exit		.alabel, 0x30
	brp.exit.imp		.alabel, 0x30
	brp.dptk		.alabel, 0x30
	brp.dptk.imp		.alabel, 0x30
	// B7
	brp.sptk		b2, 0x30
	brp.sptk.imp		b2, 0x30
	brp.dptk		b2, 0x30
	brp.dptk.imp		b2, 0x30
	brp.ret.sptk		b2, 0x30
	brp.ret.sptk.imp	b2, 0x30
	brp.ret.dptk		b2, 0x30
	brp.ret.dptk.imp	b2, 0x30

	bsw.0 ;;
	bsw.1 ;;

	//// **** C **** 

	(p56) chk.s 	r2, .alabel 
	(p57) chk.s.i 	r2, .alabel 
	(p58) chk.s.m	r2, .alabel 
	(p59) chk.s 	f2, .alabel 
	(p60) chk.a.nc 	r2, .alabel 
	(p61) chk.a.clr 	r2, .alabel 
	(p62) chk.a.nc 	f2, .alabel 
	(p63) chk.a.clr 	f2, .alabel 

	clrrrb 		;;
	clrrrb.pr	;;

	// A6 & A8
	(p0) cmp.eq	p1, p2 = r1, r2
	(p16) cmp.eq	p1, p2 = r1, r2
	(p0) cmp.eq	p17, p2 = r1, r2
	(p0) cmp.eq	p1, p18 = r1, r2
	(p0) cmp.eq	p1, p2 = r33, r2
	(p0) cmp.eq	p1, p2 = r1, r34
	(p1) cmp.eq	p1, p2 = 8, r2
	(p17) cmp.eq	p1, p2 = 8, r2
	(p1) cmp.eq	p17, p2 = 8, r2
	(p1) cmp.eq	p1, p18 = 8, r2
	(p1) cmp.eq	p1, p2 = 8, r34
	(p0) cmp.ne	p1, p2 = r1, r2
	(p16) cmp.ne	p1, p2 = r1, r2
	(p0) cmp.ne	p17, p2 = r1, r2
	(p0) cmp.ne	p1, p18 = r1, r2
	(p0) cmp.ne	p1, p2 = r33, r2
	(p0) cmp.ne	p1, p2 = r1, r34
	(p1) cmp.ne	p1, p2 = 8, r2
	(p17) cmp.ne	p1, p2 = 8, r2
	(p1) cmp.ne	p17, p2 = 8, r2
	(p1) cmp.ne	p1, p18 = 8, r2
	(p1) cmp.ne	p1, p2 = 8, r34
	(p6) cmp.lt	p1, p2 = r1, r2
	(p22) cmp.lt	p1, p2 = r1, r2
	(p6) cmp.lt	p17, p2 = r1, r2
	(p6) cmp.lt	p1, p18 = r1, r2
	(p6) cmp.lt	p1, p2 = r33, r2
	(p6) cmp.lt	p1, p2 = r1, r34
	(p7) cmp.lt	p1, p2 = 8, r2
	(p23) cmp.lt	p1, p2 = 8, r2
	(p7) cmp.lt	p17, p2 = 8, r2
	(p7) cmp.lt	p1, p18 = 8, r2
	(p7) cmp.lt	p1, p2 = 8, r34
	(p9) cmp.le	p1, p2 = r1, r2
	(p25) cmp.le	p1, p2 = r1, r2
	(p9) cmp.le	p17, p2 = r1, r2
	(p9) cmp.le	p1, p18 = r1, r2
	(p9) cmp.le	p1, p2 = r33, r2
	(p9) cmp.le	p1, p2 = r1, r34
	(p10) cmp.le	p1, p2 = 8, r2
	(p26) cmp.le	p1, p2 = 8, r2
	(p10) cmp.le	p17, p2 = 8, r2
	(p10) cmp.le	p1, p18 = 8, r2
	(p10) cmp.le	p1, p2 = 8, r34
	(p12) cmp.gt	p1, p2 = r1, r2
	(p28) cmp.gt	p1, p2 = r1, r2
	(p12) cmp.gt	p17, p2 = r1, r2
	(p12) cmp.gt	p1, p27 = r1, r2
	(p12) cmp.gt	p1, p2 = r33, r2
	(p12) cmp.gt	p1, p2 = r1, r34
	(p13) cmp.gt	p1, p2 = 8, r2
	(p29) cmp.gt	p1, p2 = 8, r2
	(p13) cmp.gt	p17, p2 = 8, r2
	(p13) cmp.gt	p1, p27 = 8, r2
	(p13) cmp.gt	p1, p2 = 8, r34
	(p15) cmp.ge	p1, p2 = r1, r2
	(p31) cmp.ge	p1, p2 = r1, r2
	(p15) cmp.ge	p17, p2 = r1, r2
	(p15) cmp.ge	p1, p27 = r1, r2
	(p15) cmp.ge	p1, p2 = r33, r2
	(p15) cmp.ge	p1, p2 = r1, r34
	(p0) cmp.ge	p1, p2 = 8, r2
	(p16) cmp.ge	p1, p2 = 8, r2
	(p0) cmp.ge	p17, p2 = 8, r2
	(p0) cmp.ge	p1, p18 = 8, r2
	(p0) cmp.ge	p1, p2 = 8, r34
	(p2) cmp.ltu	p1, p2 = r1, r2
	(p18) cmp.ltu	p1, p2 = r1, r2
	(p2) cmp.ltu	p17, p2 = r1, r2
	(p2) cmp.ltu	p1, p27 = r1, r2
	(p2) cmp.ltu	p1, p2 = r33, r2
	(p2) cmp.ltu	p1, p2 = r1, r34
	(p3) cmp.ltu	p1, p2 = 8, r2
	(p19) cmp.ltu	p1, p2 = 8, r2
	(p3) cmp.ltu	p17, p2 = 8, r2
	(p3) cmp.ltu	p1, p27 = 8, r2
	(p3) cmp.ltu	p1, p2 = 8, r34
	(p2) cmp.leu	p1, p2 = r1, r2
	(p18) cmp.leu	p1, p2 = r1, r2
	(p2) cmp.leu	p17, p2 = r1, r2
	(p2) cmp.leu	p1, p27 = r1, r2
	(p2) cmp.leu	p1, p2 = r33, r2
	(p2) cmp.leu	p1, p2 = r1, r34
	(p3) cmp.leu	p1, p2 = 8, r2
	(p19) cmp.leu	p1, p2 = 8, r2
	(p3) cmp.leu	p17, p2 = 8, r2
	(p3) cmp.leu	p1, p27 = 8, r2
	(p3) cmp.leu	p1, p2 = 8, r34
	(p2) cmp.gtu	p1, p2 = r1, r2
	(p18) cmp.gtu	p1, p2 = r1, r2
	(p2) cmp.gtu	p17, p2 = r1, r2
	(p2) cmp.gtu	p1, p27 = r1, r2
	(p2) cmp.gtu	p1, p2 = r33, r2
	(p2) cmp.gtu	p1, p2 = r1, r34
	(p3) cmp.gtu	p1, p2 = 8, r2
	(p19) cmp.gtu	p1, p2 = 8, r2
	(p3) cmp.gtu	p17, p2 = 8, r2
	(p3) cmp.gtu	p1, p27 = 8, r2
	(p3) cmp.gtu	p1, p2 = 8, r34
	(p2) cmp.geu	p1, p2 = r1, r2
	(p18) cmp.geu	p1, p2 = r1, r2
	(p2) cmp.geu	p17, p2 = r1, r2
	(p2) cmp.geu	p1, p27 = r1, r2
	(p2) cmp.geu	p1, p2 = r33, r2
	(p2) cmp.geu	p1, p2 = r1, r34
	(p3) cmp.geu	p1, p2 = 8, r2
	(p19) cmp.geu	p1, p2 = 8, r2
	(p3) cmp.geu	p17, p2 = 8, r2
	(p3) cmp.geu	p1, p27 = 8, r2
	(p3) cmp.geu	p1, p2 = 8, r34
	(p0) cmp.eq.unc	p1, p2 = r1, r2
	(p16) cmp.eq.unc	p1, p2 = r1, r2
	(p0) cmp.eq.unc	p17, p2 = r1, r2
	(p0) cmp.eq.unc	p1, p18 = r1, r2
	(p0) cmp.eq.unc	p1, p2 = r33, r2
	(p0) cmp.eq.unc	p1, p2 = r1, r34
	(p1) cmp.eq.unc	p1, p2 = 8, r2
	(p17) cmp.eq.unc	p1, p2 = 8, r2
	(p1) cmp.eq.unc	p17, p2 = 8, r2
	(p1) cmp.eq.unc	p1, p18 = 8, r2
	(p1) cmp.eq.unc	p1, p2 = 8, r34
	(p0) cmp.ne.unc	p1, p2 = r1, r2
	(p16) cmp.ne.unc	p1, p2 = r1, r2
	(p0) cmp.ne.unc	p17, p2 = r1, r2
	(p0) cmp.ne.unc	p1, p18 = r1, r2
	(p0) cmp.ne.unc	p1, p2 = r33, r2
	(p0) cmp.ne.unc	p1, p2 = r1, r34
	(p1) cmp.ne.unc	p1, p2 = 8, r2
	(p17) cmp.ne.unc	p1, p2 = 8, r2
	(p1) cmp.ne.unc	p17, p2 = 8, r2
	(p1) cmp.ne.unc	p1, p18 = 8, r2
	(p1) cmp.ne.unc	p1, p2 = 8, r34
	(p6) cmp.lt.unc	p1, p2 = r1, r2
	(p22) cmp.lt.unc	p1, p2 = r1, r2
	(p6) cmp.lt.unc	p17, p2 = r1, r2
	(p6) cmp.lt.unc	p1, p18 = r1, r2
	(p6) cmp.lt.unc	p1, p2 = r33, r2
	(p6) cmp.lt.unc	p1, p2 = r1, r34
	(p7) cmp.lt.unc	p1, p2 = 8, r2
	(p23) cmp.lt.unc	p1, p2 = 8, r2
	(p7) cmp.lt.unc	p17, p2 = 8, r2
	(p7) cmp.lt.unc	p1, p18 = 8, r2
	(p7) cmp.lt.unc	p1, p2 = 8, r34
	(p9) cmp.le.unc	p1, p2 = r1, r2
	(p25) cmp.le.unc	p1, p2 = r1, r2
	(p9) cmp.le.unc	p17, p2 = r1, r2
	(p9) cmp.le.unc	p1, p18 = r1, r2
	(p9) cmp.le.unc	p1, p2 = r33, r2
	(p9) cmp.le.unc	p1, p2 = r1, r34
	(p10) cmp.le.unc	p1, p2 = 8, r2
	(p26) cmp.le.unc	p1, p2 = 8, r2
	(p10) cmp.le.unc	p17, p2 = 8, r2
	(p10) cmp.le.unc	p1, p18 = 8, r2
	(p10) cmp.le.unc	p1, p2 = 8, r34
	(p12) cmp.gt.unc	p1, p2 = r1, r2
	(p28) cmp.gt.unc	p1, p2 = r1, r2
	(p12) cmp.gt.unc	p17, p2 = r1, r2
	(p12) cmp.gt.unc	p1, p27 = r1, r2
	(p12) cmp.gt.unc	p1, p2 = r33, r2
	(p12) cmp.gt.unc	p1, p2 = r1, r34
	(p13) cmp.gt.unc	p1, p2 = 8, r2
	(p29) cmp.gt.unc	p1, p2 = 8, r2
	(p13) cmp.gt.unc	p17, p2 = 8, r2
	(p13) cmp.gt.unc	p1, p27 = 8, r2
	(p13) cmp.gt.unc	p1, p2 = 8, r34
	(p15) cmp.ge.unc	p1, p2 = r1, r2
	(p31) cmp.ge.unc	p1, p2 = r1, r2
	(p15) cmp.ge.unc	p17, p2 = r1, r2
	(p15) cmp.ge.unc	p1, p27 = r1, r2
	(p15) cmp.ge.unc	p1, p2 = r33, r2
	(p15) cmp.ge.unc	p1, p2 = r1, r34
	(p0) cmp.ge.unc	p1, p2 = 8, r2
	(p16) cmp.ge.unc	p1, p2 = 8, r2
	(p0) cmp.ge.unc	p17, p2 = 8, r2
	(p0) cmp.ge.unc	p1, p18 = 8, r2
	(p0) cmp.ge.unc	p1, p2 = 8, r34
	(p2) cmp.ltu.unc	p1, p2 = r1, r2
	(p18) cmp.ltu.unc	p1, p2 = r1, r2
	(p2) cmp.ltu.unc	p17, p2 = r1, r2
	(p2) cmp.ltu.unc	p1, p27 = r1, r2
	(p2) cmp.ltu.unc	p1, p2 = r33, r2
	(p2) cmp.ltu.unc	p1, p2 = r1, r34
	(p3) cmp.ltu.unc	p1, p2 = 8, r2
	(p19) cmp.ltu.unc	p1, p2 = 8, r2
	(p3) cmp.ltu.unc	p17, p2 = 8, r2
	(p3) cmp.ltu.unc	p1, p27 = 8, r2
	(p3) cmp.ltu.unc	p1, p2 = 8, r34
	(p2) cmp.leu.unc	p1, p2 = r1, r2
	(p18) cmp.leu.unc	p1, p2 = r1, r2
	(p2) cmp.leu.unc	p17, p2 = r1, r2
	(p2) cmp.leu.unc	p1, p27 = r1, r2
	(p2) cmp.leu.unc	p1, p2 = r33, r2
	(p2) cmp.leu.unc	p1, p2 = r1, r34
	(p3) cmp.leu.unc	p1, p2 = 8, r2
	(p19) cmp.leu.unc	p1, p2 = 8, r2
	(p3) cmp.leu.unc	p17, p2 = 8, r2
	(p3) cmp.leu.unc	p1, p27 = 8, r2
	(p3) cmp.leu.unc	p1, p2 = 8, r34
	(p2) cmp.gtu.unc	p1, p2 = r1, r2
	(p18) cmp.gtu.unc	p1, p2 = r1, r2
	(p2) cmp.gtu.unc	p17, p2 = r1, r2
	(p2) cmp.gtu.unc	p1, p27 = r1, r2
	(p2) cmp.gtu.unc	p1, p2 = r33, r2
	(p2) cmp.gtu.unc	p1, p2 = r1, r34
	(p3) cmp.gtu.unc	p1, p2 = 8, r2
	(p19) cmp.gtu.unc	p1, p2 = 8, r2
	(p3) cmp.gtu.unc	p17, p2 = 8, r2
	(p3) cmp.gtu.unc	p1, p27 = 8, r2
	(p3) cmp.gtu.unc	p1, p2 = 8, r34
	(p2) cmp.geu.unc	p1, p2 = r1, r2
	(p18) cmp.geu.unc	p1, p2 = r1, r2
	(p2) cmp.geu.unc	p17, p2 = r1, r2
	(p2) cmp.geu.unc	p1, p27 = r1, r2
	(p2) cmp.geu.unc	p1, p2 = r33, r2
	(p2) cmp.geu.unc	p1, p2 = r1, r34
	(p3) cmp.geu.unc	p1, p2 = 8, r2
	(p19) cmp.geu.unc	p1, p2 = 8, r2
	(p3) cmp.geu.unc	p17, p2 = 8, r2
	(p3) cmp.geu.unc	p1, p27 = 8, r2
	(p3) cmp.geu.unc	p1, p2 = 8, r34
	// parallel compares in A6 & A8
	(p12) cmp.eq.or	p1, p2 = r1, r2
	(p60) cmp.eq.or	p1, p2 = r1, r2
	(p12) cmp.eq.or	p17, p2 = r1, r2
	(p12) cmp.eq.or	p1, p27 = r1, r2
	(p12) cmp.eq.or	p1, p2 = r33, r2
	(p12) cmp.eq.or	p1, p2 = r1, r34
	(p13) cmp.eq.or	p1, p2 = 8, r2
	(p61) cmp.eq.or	p1, p2 = 8, r2
	(p13) cmp.eq.or	p17, p2 = 8, r2
	(p13) cmp.eq.or	p1, p27 = 8, r2
	(p13) cmp.eq.or	p1, p2 = 8, r34
	(p12) cmp.eq.and	p1, p2 = r1, r2
	(p60) cmp.eq.and	p1, p2 = r1, r2
	(p12) cmp.eq.and	p17, p2 = r1, r2
	(p12) cmp.eq.and	p1, p27 = r1, r2
	(p12) cmp.eq.and	p1, p2 = r33, r2
	(p12) cmp.eq.and	p1, p2 = r1, r34
	(p13) cmp.eq.and	p1, p2 = 8, r2
	(p61) cmp.eq.and	p1, p2 = 8, r2
	(p13) cmp.eq.and	p17, p2 = 8, r2
	(p13) cmp.eq.and	p1, p27 = 8, r2
	(p13) cmp.eq.and	p1, p2 = 8, r34
	(p12) cmp.eq.or.andcm	p1, p2 = r1, r2
	(p60) cmp.eq.or.andcm	p1, p2 = r1, r2
	(p12) cmp.eq.or.andcm	p17, p2 = r1, r2
	(p12) cmp.eq.or.andcm	p1, p27 = r1, r2
	(p12) cmp.eq.or.andcm	p1, p2 = r33, r2
	(p12) cmp.eq.or.andcm	p1, p2 = r1, r34
	(p13) cmp.eq.or.andcm	p1, p2 = 8, r2
	(p61) cmp.eq.or.andcm	p1, p2 = 8, r2
	(p13) cmp.eq.or.andcm	p17, p2 = 8, r2
	(p13) cmp.eq.or.andcm	p1, p27 = 8, r2
	(p13) cmp.eq.or.andcm	p1, p2 = 8, r34
	(p12) cmp.eq.orcm	p1, p2 = r1, r2
	(p60) cmp.eq.orcm	p1, p2 = r1, r2
	(p12) cmp.eq.orcm	p17, p2 = r1, r2
	(p12) cmp.eq.orcm	p1, p27 = r1, r2
	(p12) cmp.eq.orcm	p1, p2 = r33, r2
	(p12) cmp.eq.orcm	p1, p2 = r1, r34
	(p13) cmp.eq.orcm	p1, p2 = 8, r2
	(p61) cmp.eq.orcm	p1, p2 = 8, r2
	(p13) cmp.eq.orcm	p17, p2 = 8, r2
	(p13) cmp.eq.orcm	p1, p27 = 8, r2
	(p13) cmp.eq.orcm	p1, p2 = 8, r34
	(p12) cmp.eq.andcm	p1, p2 = r1, r2
	(p60) cmp.eq.andcm	p1, p2 = r1, r2
	(p12) cmp.eq.andcm	p17, p2 = r1, r2
	(p12) cmp.eq.andcm	p1, p27 = r1, r2
	(p12) cmp.eq.andcm	p1, p2 = r33, r2
	(p12) cmp.eq.andcm	p1, p2 = r1, r34
	(p13) cmp.eq.andcm	p1, p2 = 8, r2
	(p61) cmp.eq.andcm	p1, p2 = 8, r2
	(p13) cmp.eq.andcm	p17, p2 = 8, r2
	(p13) cmp.eq.andcm	p1, p27 = 8, r2
	(p13) cmp.eq.andcm	p1, p2 = 8, r34
	(p12) cmp.eq.and.orcm	p1, p2 = r1, r2
	(p60) cmp.eq.and.orcm	p1, p2 = r1, r2
	(p12) cmp.eq.and.orcm	p17, p2 = r1, r2
	(p12) cmp.eq.and.orcm	p1, p27 = r1, r2
	(p12) cmp.eq.and.orcm	p1, p2 = r33, r2
	(p12) cmp.eq.and.orcm	p1, p2 = r1, r34
	(p13) cmp.eq.and.orcm	p1, p2 = 8, r2
	(p61) cmp.eq.and.orcm	p1, p2 = 8, r2
	(p13) cmp.eq.and.orcm	p17, p2 = 8, r2
	(p13) cmp.eq.and.orcm	p1, p27 = 8, r2
	(p13) cmp.eq.and.orcm	p1, p2 = 8, r34
	(p12) cmp.ne.or	p1, p2 = r1, r2
	(p60) cmp.ne.or	p1, p2 = r1, r2
	(p12) cmp.ne.or	p17, p2 = r1, r2
	(p12) cmp.ne.or	p1, p27 = r1, r2
	(p12) cmp.ne.or	p1, p2 = r33, r2
	(p12) cmp.ne.or	p1, p2 = r1, r34
	(p13) cmp.ne.or	p1, p2 = 8, r2
	(p61) cmp.ne.or	p1, p2 = 8, r2
	(p13) cmp.ne.or	p17, p2 = 8, r2
	(p13) cmp.ne.or	p1, p27 = 8, r2
	(p13) cmp.ne.or	p1, p2 = 8, r34
	(p12) cmp.ne.and	p1, p2 = r1, r2
	(p60) cmp.ne.and	p1, p2 = r1, r2
	(p12) cmp.ne.and	p17, p2 = r1, r2
	(p12) cmp.ne.and	p1, p27 = r1, r2
	(p12) cmp.ne.and	p1, p2 = r33, r2
	(p12) cmp.ne.and	p1, p2 = r1, r34
	(p13) cmp.ne.and	p1, p2 = 8, r2
	(p61) cmp.ne.and	p1, p2 = 8, r2
	(p13) cmp.ne.and	p17, p2 = 8, r2
	(p13) cmp.ne.and	p1, p27 = 8, r2
	(p13) cmp.ne.and	p1, p2 = 8, r34
	(p12) cmp.ne.or.andcm	p1, p2 = r1, r2
	(p60) cmp.ne.or.andcm	p1, p2 = r1, r2
	(p12) cmp.ne.or.andcm	p17, p2 = r1, r2
	(p12) cmp.ne.or.andcm	p1, p27 = r1, r2
	(p12) cmp.ne.or.andcm	p1, p2 = r33, r2
	(p12) cmp.ne.or.andcm	p1, p2 = r1, r34
	(p13) cmp.ne.or.andcm	p1, p2 = 8, r2
	(p61) cmp.ne.or.andcm	p1, p2 = 8, r2
	(p13) cmp.ne.or.andcm	p17, p2 = 8, r2
	(p13) cmp.ne.or.andcm	p1, p27 = 8, r2
	(p13) cmp.ne.or.andcm	p1, p2 = 8, r34
	(p12) cmp.ne.orcm	p1, p2 = r1, r2
	(p60) cmp.ne.orcm	p1, p2 = r1, r2
	(p12) cmp.ne.orcm	p17, p2 = r1, r2
	(p12) cmp.ne.orcm	p1, p27 = r1, r2
	(p12) cmp.ne.orcm	p1, p2 = r33, r2
	(p12) cmp.ne.orcm	p1, p2 = r1, r34
	(p13) cmp.ne.orcm	p1, p2 = 8, r2
	(p61) cmp.ne.orcm	p1, p2 = 8, r2
	(p13) cmp.ne.orcm	p17, p2 = 8, r2
	(p13) cmp.ne.orcm	p1, p27 = 8, r2
	(p13) cmp.ne.orcm	p1, p2 = 8, r34
	(p12) cmp.ne.andcm	p1, p2 = r1, r2
	(p60) cmp.ne.andcm	p1, p2 = r1, r2
	(p12) cmp.ne.andcm	p17, p2 = r1, r2
	(p12) cmp.ne.andcm	p1, p27 = r1, r2
	(p12) cmp.ne.andcm	p1, p2 = r33, r2
	(p12) cmp.ne.andcm	p1, p2 = r1, r34
	(p13) cmp.ne.andcm	p1, p2 = 8, r2
	(p61) cmp.ne.andcm	p1, p2 = 8, r2
	(p13) cmp.ne.andcm	p17, p2 = 8, r2
	(p13) cmp.ne.andcm	p1, p27 = 8, r2
	(p13) cmp.ne.andcm	p1, p2 = 8, r34
	(p12) cmp.ne.and.orcm	p1, p2 = r1, r2
	(p60) cmp.ne.and.orcm	p1, p2 = r1, r2
	(p12) cmp.ne.and.orcm	p17, p2 = r1, r2
	(p12) cmp.ne.and.orcm	p1, p27 = r1, r2
	(p12) cmp.ne.and.orcm	p1, p2 = r33, r2
	(p12) cmp.ne.and.orcm	p1, p2 = r1, r34
	(p13) cmp.ne.and.orcm	p1, p2 = 8, r2
	(p61) cmp.ne.and.orcm	p1, p2 = 8, r2
	(p13) cmp.ne.and.orcm	p17, p2 = 8, r2
	(p13) cmp.ne.and.orcm	p1, p27 = 8, r2
	(p13) cmp.ne.and.orcm	p1, p2 = 8, r34
	// A7 & pseudo-op
	(p0) cmp.eq.or		p1, p2 = r0, r3
	(p16) cmp.eq.or		p1, p2 = r0, r3
	(p0) cmp.eq.or		p17, p2 = r0, r3
	(p0) cmp.eq.or		p1, p27 = r0, r3
	(p0) cmp.eq.or		p1, p2 = r0, r35
	(p0) cmp.eq.or		p1, p2 = r35, r0
	(p0) cmp.eq.and		p1, p2 = r0, r3
	(p16) cmp.eq.and		p1, p2 = r0, r3
	(p0) cmp.eq.and		p17, p2 = r0, r3
	(p0) cmp.eq.and		p1, p27 = r0, r3
	(p0) cmp.eq.and		p1, p2 = r0, r35
	(p0) cmp.eq.and		p1, p2 = r35, r0
	(p0) cmp.eq.or.andcm		p1, p2 = r0, r3
	(p16) cmp.eq.or.andcm		p1, p2 = r0, r3
	(p0) cmp.eq.or.andcm		p17, p2 = r0, r3
	(p0) cmp.eq.or.andcm		p1, p27 = r0, r3
	(p0) cmp.eq.or.andcm		p1, p2 = r0, r35
	(p0) cmp.eq.or.andcm		p1, p2 = r35, r0
	(p0) cmp.eq.orcm		p1, p2 = r0, r3
	(p16) cmp.eq.orcm		p1, p2 = r0, r3
	(p0) cmp.eq.orcm		p17, p2 = r0, r3
	(p0) cmp.eq.orcm		p1, p27 = r0, r3
	(p0) cmp.eq.orcm		p1, p2 = r0, r35
	(p0) cmp.eq.orcm		p1, p2 = r35, r0
	(p0) cmp.eq.andcm		p1, p2 = r0, r3
	(p16) cmp.eq.andcm		p1, p2 = r0, r3
	(p0) cmp.eq.andcm		p17, p2 = r0, r3
	(p0) cmp.eq.andcm		p1, p27 = r0, r3
	(p0) cmp.eq.andcm		p1, p2 = r0, r35
	(p0) cmp.eq.andcm		p1, p2 = r35, r0
	(p0) cmp.eq.and.orcm		p1, p2 = r0, r3
	(p16) cmp.eq.and.orcm		p1, p2 = r0, r3
	(p0) cmp.eq.and.orcm		p17, p2 = r0, r3
	(p0) cmp.eq.and.orcm		p1, p27 = r0, r3
	(p0) cmp.eq.and.orcm		p1, p2 = r0, r35
	(p0) cmp.eq.and.orcm		p1, p2 = r35, r0
	(p0) cmp.ne.or		p1, p2 = r0, r3
	(p16) cmp.ne.or		p1, p2 = r0, r3
	(p0) cmp.ne.or		p17, p2 = r0, r3
	(p0) cmp.ne.or		p1, p27 = r0, r3
	(p0) cmp.ne.or		p1, p2 = r0, r35
	(p0) cmp.ne.or		p1, p2 = r35, r0
	(p0) cmp.ne.and		p1, p2 = r0, r3
	(p16) cmp.ne.and		p1, p2 = r0, r3
	(p0) cmp.ne.and		p17, p2 = r0, r3
	(p0) cmp.ne.and		p1, p27 = r0, r3
	(p0) cmp.ne.and		p1, p2 = r0, r35
	(p0) cmp.ne.and		p1, p2 = r35, r0
	(p0) cmp.ne.or.andcm		p1, p2 = r0, r3
	(p16) cmp.ne.or.andcm		p1, p2 = r0, r3
	(p0) cmp.ne.or.andcm		p17, p2 = r0, r3
	(p0) cmp.ne.or.andcm		p1, p27 = r0, r3
	(p0) cmp.ne.or.andcm		p1, p2 = r0, r35
	(p0) cmp.ne.or.andcm		p1, p2 = r35, r0
	(p0) cmp.ne.orcm		p1, p2 = r0, r3
	(p16) cmp.ne.orcm		p1, p2 = r0, r3
	(p0) cmp.ne.orcm		p17, p2 = r0, r3
	(p0) cmp.ne.orcm		p1, p27 = r0, r3
	(p0) cmp.ne.orcm		p1, p2 = r0, r35
	(p0) cmp.ne.orcm		p1, p2 = r35, r0
	(p0) cmp.ne.andcm		p1, p2 = r0, r3
	(p16) cmp.ne.andcm		p1, p2 = r0, r3
	(p0) cmp.ne.andcm		p17, p2 = r0, r3
	(p0) cmp.ne.andcm		p1, p27 = r0, r3
	(p0) cmp.ne.andcm		p1, p2 = r0, r35
	(p0) cmp.ne.andcm		p1, p2 = r35, r0
	(p0) cmp.ne.and.orcm		p1, p2 = r0, r3
	(p16) cmp.ne.and.orcm		p1, p2 = r0, r3
	(p0) cmp.ne.and.orcm		p17, p2 = r0, r3
	(p0) cmp.ne.and.orcm		p1, p27 = r0, r3
	(p0) cmp.ne.and.orcm		p1, p2 = r0, r35
	(p0) cmp.ne.and.orcm		p1, p2 = r35, r0
	(p0) cmp.lt.or		p1, p2 = r0, r3
	(p16) cmp.lt.or		p1, p2 = r0, r3
	(p0) cmp.lt.or		p17, p2 = r0, r3
	(p0) cmp.lt.or		p1, p27 = r0, r3
	(p0) cmp.lt.or		p1, p2 = r0, r35
	(p0) cmp.lt.or		p1, p2 = r35, r0
	(p0) cmp.lt.and		p1, p2 = r0, r3
	(p16) cmp.lt.and		p1, p2 = r0, r3
	(p0) cmp.lt.and		p17, p2 = r0, r3
	(p0) cmp.lt.and		p1, p27 = r0, r3
	(p0) cmp.lt.and		p1, p2 = r0, r35
	(p0) cmp.lt.and		p1, p2 = r35, r0
	(p0) cmp.lt.or.andcm		p1, p2 = r0, r3
	(p16) cmp.lt.or.andcm		p1, p2 = r0, r3
	(p0) cmp.lt.or.andcm		p17, p2 = r0, r3
	(p0) cmp.lt.or.andcm		p1, p27 = r0, r3
	(p0) cmp.lt.or.andcm		p1, p2 = r0, r35
	(p0) cmp.lt.or.andcm		p1, p2 = r35, r0
	(p0) cmp.lt.orcm		p1, p2 = r0, r3
	(p16) cmp.lt.orcm		p1, p2 = r0, r3
	(p0) cmp.lt.orcm		p17, p2 = r0, r3
	(p0) cmp.lt.orcm		p1, p27 = r0, r3
	(p0) cmp.lt.orcm		p1, p2 = r0, r35
	(p0) cmp.lt.orcm		p1, p2 = r35, r0
	(p0) cmp.lt.andcm		p1, p2 = r0, r3
	(p16) cmp.lt.andcm		p1, p2 = r0, r3
	(p0) cmp.lt.andcm		p17, p2 = r0, r3
	(p0) cmp.lt.andcm		p1, p27 = r0, r3
	(p0) cmp.lt.andcm		p1, p2 = r0, r35
	(p0) cmp.lt.andcm		p1, p2 = r35, r0
	(p0) cmp.lt.and.orcm		p1, p2 = r0, r3
	(p16) cmp.lt.and.orcm		p1, p2 = r0, r3
	(p0) cmp.lt.and.orcm		p17, p2 = r0, r3
	(p0) cmp.lt.and.orcm		p1, p27 = r0, r3
	(p0) cmp.lt.and.orcm		p1, p2 = r0, r35
	(p0) cmp.lt.and.orcm		p1, p2 = r35, r0
	(p0) cmp.le.or		p1, p2 = r0, r3
	(p16) cmp.le.or		p1, p2 = r0, r3
	(p0) cmp.le.or		p17, p2 = r0, r3
	(p0) cmp.le.or		p1, p27 = r0, r3
	(p0) cmp.le.or		p1, p2 = r0, r35
	(p0) cmp.le.or		p1, p2 = r35, r0
	(p0) cmp.le.and		p1, p2 = r0, r3
	(p16) cmp.le.and		p1, p2 = r0, r3
	(p0) cmp.le.and		p17, p2 = r0, r3
	(p0) cmp.le.and		p1, p27 = r0, r3
	(p0) cmp.le.and		p1, p2 = r0, r35
	(p0) cmp.le.and		p1, p2 = r35, r0
	(p0) cmp.le.or.andcm		p1, p2 = r0, r3
	(p16) cmp.le.or.andcm		p1, p2 = r0, r3
	(p0) cmp.le.or.andcm		p17, p2 = r0, r3
	(p0) cmp.le.or.andcm		p1, p27 = r0, r3
	(p0) cmp.le.or.andcm		p1, p2 = r0, r35
	(p0) cmp.le.or.andcm		p1, p2 = r35, r0
	(p0) cmp.le.orcm		p1, p2 = r0, r3
	(p16) cmp.le.orcm		p1, p2 = r0, r3
	(p0) cmp.le.orcm		p17, p2 = r0, r3
	(p0) cmp.le.orcm		p1, p27 = r0, r3
	(p0) cmp.le.orcm		p1, p2 = r0, r35
	(p0) cmp.le.orcm		p1, p2 = r35, r0
	(p0) cmp.le.andcm		p1, p2 = r0, r3
	(p16) cmp.le.andcm		p1, p2 = r0, r3
	(p0) cmp.le.andcm		p17, p2 = r0, r3
	(p0) cmp.le.andcm		p1, p27 = r0, r3
	(p0) cmp.le.andcm		p1, p2 = r0, r35
	(p0) cmp.le.andcm		p1, p2 = r35, r0
	(p0) cmp.le.and.orcm		p1, p2 = r0, r3
	(p16) cmp.le.and.orcm		p1, p2 = r0, r3
	(p0) cmp.le.and.orcm		p17, p2 = r0, r3
	(p0) cmp.le.and.orcm		p1, p27 = r0, r3
	(p0) cmp.le.and.orcm		p1, p2 = r0, r35
	(p0) cmp.le.and.orcm		p1, p2 = r35, r0
	(p0) cmp.gt.or		p1, p2 = r0, r3
	(p16) cmp.gt.or		p1, p2 = r0, r3
	(p0) cmp.gt.or		p17, p2 = r0, r3
	(p0) cmp.gt.or		p1, p27 = r0, r3
	(p0) cmp.gt.or		p1, p2 = r0, r35
	(p0) cmp.gt.or		p1, p2 = r35, r0
	(p0) cmp.gt.and		p1, p2 = r0, r3
	(p16) cmp.gt.and		p1, p2 = r0, r3
	(p0) cmp.gt.and		p17, p2 = r0, r3
	(p0) cmp.gt.and		p1, p27 = r0, r3
	(p0) cmp.gt.and		p1, p2 = r0, r35
	(p0) cmp.gt.and		p1, p2 = r35, r0
	(p0) cmp.gt.or.andcm		p1, p2 = r0, r3
	(p16) cmp.gt.or.andcm		p1, p2 = r0, r3
	(p0) cmp.gt.or.andcm		p17, p2 = r0, r3
	(p0) cmp.gt.or.andcm		p1, p27 = r0, r3
	(p0) cmp.gt.or.andcm		p1, p2 = r0, r35
	(p0) cmp.gt.or.andcm		p1, p2 = r35, r0
	(p0) cmp.gt.orcm		p1, p2 = r0, r3
	(p16) cmp.gt.orcm		p1, p2 = r0, r3
	(p0) cmp.gt.orcm		p17, p2 = r0, r3
	(p0) cmp.gt.orcm		p1, p27 = r0, r3
	(p0) cmp.gt.orcm		p1, p2 = r0, r35
	(p0) cmp.gt.orcm		p1, p2 = r35, r0
	(p0) cmp.gt.andcm		p1, p2 = r0, r3
	(p16) cmp.gt.andcm		p1, p2 = r0, r3
	(p0) cmp.gt.andcm		p17, p2 = r0, r3
	(p0) cmp.gt.andcm		p1, p27 = r0, r3
	(p0) cmp.gt.andcm		p1, p2 = r0, r35
	(p0) cmp.gt.andcm		p1, p2 = r35, r0
	(p0) cmp.gt.and.orcm		p1, p2 = r0, r3
	(p16) cmp.gt.and.orcm		p1, p2 = r0, r3
	(p0) cmp.gt.and.orcm		p17, p2 = r0, r3
	(p0) cmp.gt.and.orcm		p1, p27 = r0, r3
	(p0) cmp.gt.and.orcm		p1, p2 = r0, r35
	(p0) cmp.gt.and.orcm		p1, p2 = r35, r0
	(p0) cmp.ge.or		p1, p2 = r0, r3
	(p16) cmp.ge.or		p1, p2 = r0, r3
	(p0) cmp.ge.or		p17, p2 = r0, r3
	(p0) cmp.ge.or		p1, p27 = r0, r3
	(p0) cmp.ge.or		p1, p2 = r0, r35
	(p0) cmp.ge.or		p1, p2 = r35, r0
	(p0) cmp.ge.and		p1, p2 = r0, r3
	(p16) cmp.ge.and		p1, p2 = r0, r3
	(p0) cmp.ge.and		p17, p2 = r0, r3
	(p0) cmp.ge.and		p1, p27 = r0, r3
	(p0) cmp.ge.and		p1, p2 = r0, r35
	(p0) cmp.ge.and		p1, p2 = r35, r0
	(p0) cmp.ge.or.andcm		p1, p2 = r0, r3
	(p16) cmp.ge.or.andcm		p1, p2 = r0, r3
	(p0) cmp.ge.or.andcm		p17, p2 = r0, r3
	(p0) cmp.ge.or.andcm		p1, p27 = r0, r3
	(p0) cmp.ge.or.andcm		p1, p2 = r0, r35
	(p0) cmp.ge.or.andcm		p1, p2 = r35, r0
	(p0) cmp.ge.orcm		p1, p2 = r0, r3
	(p16) cmp.ge.orcm		p1, p2 = r0, r3
	(p0) cmp.ge.orcm		p17, p2 = r0, r3
	(p0) cmp.ge.orcm		p1, p27 = r0, r3
	(p0) cmp.ge.orcm		p1, p2 = r0, r35
	(p0) cmp.ge.orcm		p1, p2 = r35, r0
	(p0) cmp.ge.andcm		p1, p2 = r0, r3
	(p16) cmp.ge.andcm		p1, p2 = r0, r3
	(p0) cmp.ge.andcm		p17, p2 = r0, r3
	(p0) cmp.ge.andcm		p1, p27 = r0, r3
	(p0) cmp.ge.andcm		p1, p2 = r0, r35
	(p0) cmp.ge.andcm		p1, p2 = r35, r0
	(p0) cmp.ge.and.orcm		p1, p2 = r0, r3
	(p16) cmp.ge.and.orcm		p1, p2 = r0, r3
	(p0) cmp.ge.and.orcm		p17, p2 = r0, r3
	(p0) cmp.ge.and.orcm		p1, p27 = r0, r3
	(p0) cmp.ge.and.orcm		p1, p2 = r0, r35
	(p0) cmp.ge.and.orcm		p1, p2 = r35, r0

	// A6 & A8
	(p0) cmp4.eq	p1, p2 = r1, r2
	(p16) cmp4.eq	p1, p2 = r1, r2
	(p0) cmp4.eq	p17, p2 = r1, r2
	(p0) cmp4.eq	p1, p18 = r1, r2
	(p0) cmp4.eq	p1, p2 = r33, r2
	(p0) cmp4.eq	p1, p2 = r1, r34
	(p1) cmp4.eq	p1, p2 = 8, r2
	(p17) cmp4.eq	p1, p2 = 8, r2
	(p1) cmp4.eq	p17, p2 = 8, r2
	(p1) cmp4.eq	p1, p18 = 8, r2
	(p1) cmp4.eq	p1, p2 = 8, r34
	(p0) cmp4.ne	p1, p2 = r1, r2
	(p16) cmp4.ne	p1, p2 = r1, r2
	(p0) cmp4.ne	p17, p2 = r1, r2
	(p0) cmp4.ne	p1, p18 = r1, r2
	(p0) cmp4.ne	p1, p2 = r33, r2
	(p0) cmp4.ne	p1, p2 = r1, r34
	(p1) cmp4.ne	p1, p2 = 8, r2
	(p17) cmp4.ne	p1, p2 = 8, r2
	(p1) cmp4.ne	p17, p2 = 8, r2
	(p1) cmp4.ne	p1, p18 = 8, r2
	(p1) cmp4.ne	p1, p2 = 8, r34
	(p6) cmp4.lt	p1, p2 = r1, r2
	(p22) cmp4.lt	p1, p2 = r1, r2
	(p6) cmp4.lt	p17, p2 = r1, r2
	(p6) cmp4.lt	p1, p18 = r1, r2
	(p6) cmp4.lt	p1, p2 = r33, r2
	(p6) cmp4.lt	p1, p2 = r1, r34
	(p7) cmp4.lt	p1, p2 = 8, r2
	(p23) cmp4.lt	p1, p2 = 8, r2
	(p7) cmp4.lt	p17, p2 = 8, r2
	(p7) cmp4.lt	p1, p18 = 8, r2
	(p7) cmp4.lt	p1, p2 = 8, r34
	(p9) cmp4.le	p1, p2 = r1, r2
	(p25) cmp4.le	p1, p2 = r1, r2
	(p9) cmp4.le	p17, p2 = r1, r2
	(p9) cmp4.le	p1, p18 = r1, r2
	(p9) cmp4.le	p1, p2 = r33, r2
	(p9) cmp4.le	p1, p2 = r1, r34
	(p10) cmp4.le	p1, p2 = 8, r2
	(p26) cmp4.le	p1, p2 = 8, r2
	(p10) cmp4.le	p17, p2 = 8, r2
	(p10) cmp4.le	p1, p18 = 8, r2
	(p10) cmp4.le	p1, p2 = 8, r34
	(p12) cmp4.gt	p1, p2 = r1, r2
	(p28) cmp4.gt	p1, p2 = r1, r2
	(p12) cmp4.gt	p17, p2 = r1, r2
	(p12) cmp4.gt	p1, p27 = r1, r2
	(p12) cmp4.gt	p1, p2 = r33, r2
	(p12) cmp4.gt	p1, p2 = r1, r34
	(p13) cmp4.gt	p1, p2 = 8, r2
	(p29) cmp4.gt	p1, p2 = 8, r2
	(p13) cmp4.gt	p17, p2 = 8, r2
	(p13) cmp4.gt	p1, p27 = 8, r2
	(p13) cmp4.gt	p1, p2 = 8, r34
	(p15) cmp4.ge	p1, p2 = r1, r2
	(p31) cmp4.ge	p1, p2 = r1, r2
	(p15) cmp4.ge	p17, p2 = r1, r2
	(p15) cmp4.ge	p1, p27 = r1, r2
	(p15) cmp4.ge	p1, p2 = r33, r2
	(p15) cmp4.ge	p1, p2 = r1, r34
	(p0) cmp4.ge	p1, p2 = 8, r2
	(p16) cmp4.ge	p1, p2 = 8, r2
	(p0) cmp4.ge	p17, p2 = 8, r2
	(p0) cmp4.ge	p1, p18 = 8, r2
	(p0) cmp4.ge	p1, p2 = 8, r34
	(p2) cmp4.ltu	p1, p2 = r1, r2
	(p18) cmp4.ltu	p1, p2 = r1, r2
	(p2) cmp4.ltu	p17, p2 = r1, r2
	(p2) cmp4.ltu	p1, p27 = r1, r2
	(p2) cmp4.ltu	p1, p2 = r33, r2
	(p2) cmp4.ltu	p1, p2 = r1, r34
	(p3) cmp4.ltu	p1, p2 = 8, r2
	(p19) cmp4.ltu	p1, p2 = 8, r2
	(p3) cmp4.ltu	p17, p2 = 8, r2
	(p3) cmp4.ltu	p1, p27 = 8, r2
	(p3) cmp4.ltu	p1, p2 = 8, r34
	(p2) cmp4.leu	p1, p2 = r1, r2
	(p18) cmp4.leu	p1, p2 = r1, r2
	(p2) cmp4.leu	p17, p2 = r1, r2
	(p2) cmp4.leu	p1, p27 = r1, r2
	(p2) cmp4.leu	p1, p2 = r33, r2
	(p2) cmp4.leu	p1, p2 = r1, r34
	(p3) cmp4.leu	p1, p2 = 8, r2
	(p19) cmp4.leu	p1, p2 = 8, r2
	(p3) cmp4.leu	p17, p2 = 8, r2
	(p3) cmp4.leu	p1, p27 = 8, r2
	(p3) cmp4.leu	p1, p2 = 8, r34
	(p2) cmp4.gtu	p1, p2 = r1, r2
	(p18) cmp4.gtu	p1, p2 = r1, r2
	(p2) cmp4.gtu	p17, p2 = r1, r2
	(p2) cmp4.gtu	p1, p27 = r1, r2
	(p2) cmp4.gtu	p1, p2 = r33, r2
	(p2) cmp4.gtu	p1, p2 = r1, r34
	(p3) cmp4.gtu	p1, p2 = 8, r2
	(p19) cmp4.gtu	p1, p2 = 8, r2
	(p3) cmp4.gtu	p17, p2 = 8, r2
	(p3) cmp4.gtu	p1, p27 = 8, r2
	(p3) cmp4.gtu	p1, p2 = 8, r34
	(p2) cmp4.geu	p1, p2 = r1, r2
	(p18) cmp4.geu	p1, p2 = r1, r2
	(p2) cmp4.geu	p17, p2 = r1, r2
	(p2) cmp4.geu	p1, p27 = r1, r2
	(p2) cmp4.geu	p1, p2 = r33, r2
	(p2) cmp4.geu	p1, p2 = r1, r34
	(p3) cmp4.geu	p1, p2 = 8, r2
	(p19) cmp4.geu	p1, p2 = 8, r2
	(p3) cmp4.geu	p17, p2 = 8, r2
	(p3) cmp4.geu	p1, p27 = 8, r2
	(p3) cmp4.geu	p1, p2 = 8, r34
	(p0) cmp4.eq.unc	p1, p2 = r1, r2
	(p16) cmp4.eq.unc	p1, p2 = r1, r2
	(p0) cmp4.eq.unc	p17, p2 = r1, r2
	(p0) cmp4.eq.unc	p1, p18 = r1, r2
	(p0) cmp4.eq.unc	p1, p2 = r33, r2
	(p0) cmp4.eq.unc	p1, p2 = r1, r34
	(p1) cmp4.eq.unc	p1, p2 = 8, r2
	(p17) cmp4.eq.unc	p1, p2 = 8, r2
	(p1) cmp4.eq.unc	p17, p2 = 8, r2
	(p1) cmp4.eq.unc	p1, p18 = 8, r2
	(p1) cmp4.eq.unc	p1, p2 = 8, r34
	(p0) cmp4.ne.unc	p1, p2 = r1, r2
	(p16) cmp4.ne.unc	p1, p2 = r1, r2
	(p0) cmp4.ne.unc	p17, p2 = r1, r2
	(p0) cmp4.ne.unc	p1, p18 = r1, r2
	(p0) cmp4.ne.unc	p1, p2 = r33, r2
	(p0) cmp4.ne.unc	p1, p2 = r1, r34
	(p1) cmp4.ne.unc	p1, p2 = 8, r2
	(p17) cmp4.ne.unc	p1, p2 = 8, r2
	(p1) cmp4.ne.unc	p17, p2 = 8, r2
	(p1) cmp4.ne.unc	p1, p18 = 8, r2
	(p1) cmp4.ne.unc	p1, p2 = 8, r34
	(p6) cmp4.lt.unc	p1, p2 = r1, r2
	(p22) cmp4.lt.unc	p1, p2 = r1, r2
	(p6) cmp4.lt.unc	p17, p2 = r1, r2
	(p6) cmp4.lt.unc	p1, p18 = r1, r2
	(p6) cmp4.lt.unc	p1, p2 = r33, r2
	(p6) cmp4.lt.unc	p1, p2 = r1, r34
	(p7) cmp4.lt.unc	p1, p2 = 8, r2
	(p23) cmp4.lt.unc	p1, p2 = 8, r2
	(p7) cmp4.lt.unc	p17, p2 = 8, r2
	(p7) cmp4.lt.unc	p1, p18 = 8, r2
	(p7) cmp4.lt.unc	p1, p2 = 8, r34
	(p9) cmp4.le.unc	p1, p2 = r1, r2
	(p25) cmp4.le.unc	p1, p2 = r1, r2
	(p9) cmp4.le.unc	p17, p2 = r1, r2
	(p9) cmp4.le.unc	p1, p18 = r1, r2
	(p9) cmp4.le.unc	p1, p2 = r33, r2
	(p9) cmp4.le.unc	p1, p2 = r1, r34
	(p10) cmp4.le.unc	p1, p2 = 8, r2
	(p26) cmp4.le.unc	p1, p2 = 8, r2
	(p10) cmp4.le.unc	p17, p2 = 8, r2
	(p10) cmp4.le.unc	p1, p18 = 8, r2
	(p10) cmp4.le.unc	p1, p2 = 8, r34
	(p12) cmp4.gt.unc	p1, p2 = r1, r2
	(p28) cmp4.gt.unc	p1, p2 = r1, r2
	(p12) cmp4.gt.unc	p17, p2 = r1, r2
	(p12) cmp4.gt.unc	p1, p27 = r1, r2
	(p12) cmp4.gt.unc	p1, p2 = r33, r2
	(p12) cmp4.gt.unc	p1, p2 = r1, r34
	(p13) cmp4.gt.unc	p1, p2 = 8, r2
	(p29) cmp4.gt.unc	p1, p2 = 8, r2
	(p13) cmp4.gt.unc	p17, p2 = 8, r2
	(p13) cmp4.gt.unc	p1, p27 = 8, r2
	(p13) cmp4.gt.unc	p1, p2 = 8, r34
	(p15) cmp4.ge.unc	p1, p2 = r1, r2
	(p31) cmp4.ge.unc	p1, p2 = r1, r2
	(p15) cmp4.ge.unc	p17, p2 = r1, r2
	(p15) cmp4.ge.unc	p1, p27 = r1, r2
	(p15) cmp4.ge.unc	p1, p2 = r33, r2
	(p15) cmp4.ge.unc	p1, p2 = r1, r34
	(p0) cmp4.ge.unc	p1, p2 = 8, r2
	(p16) cmp4.ge.unc	p1, p2 = 8, r2
	(p0) cmp4.ge.unc	p17, p2 = 8, r2
	(p0) cmp4.ge.unc	p1, p18 = 8, r2
	(p0) cmp4.ge.unc	p1, p2 = 8, r34
	(p2) cmp4.ltu.unc	p1, p2 = r1, r2
	(p18) cmp4.ltu.unc	p1, p2 = r1, r2
	(p2) cmp4.ltu.unc	p17, p2 = r1, r2
	(p2) cmp4.ltu.unc	p1, p27 = r1, r2
	(p2) cmp4.ltu.unc	p1, p2 = r33, r2
	(p2) cmp4.ltu.unc	p1, p2 = r1, r34
	(p3) cmp4.ltu.unc	p1, p2 = 8, r2
	(p19) cmp4.ltu.unc	p1, p2 = 8, r2
	(p3) cmp4.ltu.unc	p17, p2 = 8, r2
	(p3) cmp4.ltu.unc	p1, p27 = 8, r2
	(p3) cmp4.ltu.unc	p1, p2 = 8, r34
	(p2) cmp4.leu.unc	p1, p2 = r1, r2
	(p18) cmp4.leu.unc	p1, p2 = r1, r2
	(p2) cmp4.leu.unc	p17, p2 = r1, r2
	(p2) cmp4.leu.unc	p1, p27 = r1, r2
	(p2) cmp4.leu.unc	p1, p2 = r33, r2
	(p2) cmp4.leu.unc	p1, p2 = r1, r34
	(p3) cmp4.leu.unc	p1, p2 = 8, r2
	(p19) cmp4.leu.unc	p1, p2 = 8, r2
	(p3) cmp4.leu.unc	p17, p2 = 8, r2
	(p3) cmp4.leu.unc	p1, p27 = 8, r2
	(p3) cmp4.leu.unc	p1, p2 = 8, r34
	(p2) cmp4.gtu.unc	p1, p2 = r1, r2
	(p18) cmp4.gtu.unc	p1, p2 = r1, r2
	(p2) cmp4.gtu.unc	p17, p2 = r1, r2
	(p2) cmp4.gtu.unc	p1, p27 = r1, r2
	(p2) cmp4.gtu.unc	p1, p2 = r33, r2
	(p2) cmp4.gtu.unc	p1, p2 = r1, r34
	(p3) cmp4.gtu.unc	p1, p2 = 8, r2
	(p19) cmp4.gtu.unc	p1, p2 = 8, r2
	(p3) cmp4.gtu.unc	p17, p2 = 8, r2
	(p3) cmp4.gtu.unc	p1, p27 = 8, r2
	(p3) cmp4.gtu.unc	p1, p2 = 8, r34
	(p2) cmp4.geu.unc	p1, p2 = r1, r2
	(p18) cmp4.geu.unc	p1, p2 = r1, r2
	(p2) cmp4.geu.unc	p17, p2 = r1, r2
	(p2) cmp4.geu.unc	p1, p27 = r1, r2
	(p2) cmp4.geu.unc	p1, p2 = r33, r2
	(p2) cmp4.geu.unc	p1, p2 = r1, r34
	(p3) cmp4.geu.unc	p1, p2 = 8, r2
	(p19) cmp4.geu.unc	p1, p2 = 8, r2
	(p3) cmp4.geu.unc	p17, p2 = 8, r2
	(p3) cmp4.geu.unc	p1, p27 = 8, r2
	(p3) cmp4.geu.unc	p1, p2 = 8, r34
	// parallel compares in A6 & A8
	(p12) cmp4.eq.or	p1, p2 = r1, r2
	(p60) cmp4.eq.or	p1, p2 = r1, r2
	(p12) cmp4.eq.or	p17, p2 = r1, r2
	(p12) cmp4.eq.or	p1, p27 = r1, r2
	(p12) cmp4.eq.or	p1, p2 = r33, r2
	(p12) cmp4.eq.or	p1, p2 = r1, r34
	(p13) cmp4.eq.or	p1, p2 = 8, r2
	(p61) cmp4.eq.or	p1, p2 = 8, r2
	(p13) cmp4.eq.or	p17, p2 = 8, r2
	(p13) cmp4.eq.or	p1, p27 = 8, r2
	(p13) cmp4.eq.or	p1, p2 = 8, r34
	(p12) cmp4.eq.and	p1, p2 = r1, r2
	(p60) cmp4.eq.and	p1, p2 = r1, r2
	(p12) cmp4.eq.and	p17, p2 = r1, r2
	(p12) cmp4.eq.and	p1, p27 = r1, r2
	(p12) cmp4.eq.and	p1, p2 = r33, r2
	(p12) cmp4.eq.and	p1, p2 = r1, r34
	(p13) cmp4.eq.and	p1, p2 = 8, r2
	(p61) cmp4.eq.and	p1, p2 = 8, r2
	(p13) cmp4.eq.and	p17, p2 = 8, r2
	(p13) cmp4.eq.and	p1, p27 = 8, r2
	(p13) cmp4.eq.and	p1, p2 = 8, r34
	(p12) cmp4.eq.or.andcm	p1, p2 = r1, r2
	(p60) cmp4.eq.or.andcm	p1, p2 = r1, r2
	(p12) cmp4.eq.or.andcm	p17, p2 = r1, r2
	(p12) cmp4.eq.or.andcm	p1, p27 = r1, r2
	(p12) cmp4.eq.or.andcm	p1, p2 = r33, r2
	(p12) cmp4.eq.or.andcm	p1, p2 = r1, r34
	(p13) cmp4.eq.or.andcm	p1, p2 = 8, r2
	(p61) cmp4.eq.or.andcm	p1, p2 = 8, r2
	(p13) cmp4.eq.or.andcm	p17, p2 = 8, r2
	(p13) cmp4.eq.or.andcm	p1, p27 = 8, r2
	(p13) cmp4.eq.or.andcm	p1, p2 = 8, r34
	(p12) cmp4.eq.orcm	p1, p2 = r1, r2
	(p60) cmp4.eq.orcm	p1, p2 = r1, r2
	(p12) cmp4.eq.orcm	p17, p2 = r1, r2
	(p12) cmp4.eq.orcm	p1, p27 = r1, r2
	(p12) cmp4.eq.orcm	p1, p2 = r33, r2
	(p12) cmp4.eq.orcm	p1, p2 = r1, r34
	(p13) cmp4.eq.orcm	p1, p2 = 8, r2
	(p61) cmp4.eq.orcm	p1, p2 = 8, r2
	(p13) cmp4.eq.orcm	p17, p2 = 8, r2
	(p13) cmp4.eq.orcm	p1, p27 = 8, r2
	(p13) cmp4.eq.orcm	p1, p2 = 8, r34
	(p12) cmp4.eq.andcm	p1, p2 = r1, r2
	(p60) cmp4.eq.andcm	p1, p2 = r1, r2
	(p12) cmp4.eq.andcm	p17, p2 = r1, r2
	(p12) cmp4.eq.andcm	p1, p27 = r1, r2
	(p12) cmp4.eq.andcm	p1, p2 = r33, r2
	(p12) cmp4.eq.andcm	p1, p2 = r1, r34
	(p13) cmp4.eq.andcm	p1, p2 = 8, r2
	(p61) cmp4.eq.andcm	p1, p2 = 8, r2
	(p13) cmp4.eq.andcm	p17, p2 = 8, r2
	(p13) cmp4.eq.andcm	p1, p27 = 8, r2
	(p13) cmp4.eq.andcm	p1, p2 = 8, r34
	(p12) cmp4.eq.and.orcm	p1, p2 = r1, r2
	(p60) cmp4.eq.and.orcm	p1, p2 = r1, r2
	(p12) cmp4.eq.and.orcm	p17, p2 = r1, r2
	(p12) cmp4.eq.and.orcm	p1, p27 = r1, r2
	(p12) cmp4.eq.and.orcm	p1, p2 = r33, r2
	(p12) cmp4.eq.and.orcm	p1, p2 = r1, r34
	(p13) cmp4.eq.and.orcm	p1, p2 = 8, r2
	(p61) cmp4.eq.and.orcm	p1, p2 = 8, r2
	(p13) cmp4.eq.and.orcm	p17, p2 = 8, r2
	(p13) cmp4.eq.and.orcm	p1, p27 = 8, r2
	(p13) cmp4.eq.and.orcm	p1, p2 = 8, r34
	(p12) cmp4.ne.or	p1, p2 = r1, r2
	(p60) cmp4.ne.or	p1, p2 = r1, r2
	(p12) cmp4.ne.or	p17, p2 = r1, r2
	(p12) cmp4.ne.or	p1, p27 = r1, r2
	(p12) cmp4.ne.or	p1, p2 = r33, r2
	(p12) cmp4.ne.or	p1, p2 = r1, r34
	(p13) cmp4.ne.or	p1, p2 = 8, r2
	(p61) cmp4.ne.or	p1, p2 = 8, r2
	(p13) cmp4.ne.or	p17, p2 = 8, r2
	(p13) cmp4.ne.or	p1, p27 = 8, r2
	(p13) cmp4.ne.or	p1, p2 = 8, r34
	(p12) cmp4.ne.and	p1, p2 = r1, r2
	(p60) cmp4.ne.and	p1, p2 = r1, r2
	(p12) cmp4.ne.and	p17, p2 = r1, r2
	(p12) cmp4.ne.and	p1, p27 = r1, r2
	(p12) cmp4.ne.and	p1, p2 = r33, r2
	(p12) cmp4.ne.and	p1, p2 = r1, r34
	(p13) cmp4.ne.and	p1, p2 = 8, r2
	(p61) cmp4.ne.and	p1, p2 = 8, r2
	(p13) cmp4.ne.and	p17, p2 = 8, r2
	(p13) cmp4.ne.and	p1, p27 = 8, r2
	(p13) cmp4.ne.and	p1, p2 = 8, r34
	(p12) cmp4.ne.or.andcm	p1, p2 = r1, r2
	(p60) cmp4.ne.or.andcm	p1, p2 = r1, r2
	(p12) cmp4.ne.or.andcm	p17, p2 = r1, r2
	(p12) cmp4.ne.or.andcm	p1, p27 = r1, r2
	(p12) cmp4.ne.or.andcm	p1, p2 = r33, r2
	(p12) cmp4.ne.or.andcm	p1, p2 = r1, r34
	(p13) cmp4.ne.or.andcm	p1, p2 = 8, r2
	(p61) cmp4.ne.or.andcm	p1, p2 = 8, r2
	(p13) cmp4.ne.or.andcm	p17, p2 = 8, r2
	(p13) cmp4.ne.or.andcm	p1, p27 = 8, r2
	(p13) cmp4.ne.or.andcm	p1, p2 = 8, r34
	(p12) cmp4.ne.orcm	p1, p2 = r1, r2
	(p60) cmp4.ne.orcm	p1, p2 = r1, r2
	(p12) cmp4.ne.orcm	p17, p2 = r1, r2
	(p12) cmp4.ne.orcm	p1, p27 = r1, r2
	(p12) cmp4.ne.orcm	p1, p2 = r33, r2
	(p12) cmp4.ne.orcm	p1, p2 = r1, r34
	(p13) cmp4.ne.orcm	p1, p2 = 8, r2
	(p61) cmp4.ne.orcm	p1, p2 = 8, r2
	(p13) cmp4.ne.orcm	p17, p2 = 8, r2
	(p13) cmp4.ne.orcm	p1, p27 = 8, r2
	(p13) cmp4.ne.orcm	p1, p2 = 8, r34
	(p12) cmp4.ne.andcm	p1, p2 = r1, r2
	(p60) cmp4.ne.andcm	p1, p2 = r1, r2
	(p12) cmp4.ne.andcm	p17, p2 = r1, r2
	(p12) cmp4.ne.andcm	p1, p27 = r1, r2
	(p12) cmp4.ne.andcm	p1, p2 = r33, r2
	(p12) cmp4.ne.andcm	p1, p2 = r1, r34
	(p13) cmp4.ne.andcm	p1, p2 = 8, r2
	(p61) cmp4.ne.andcm	p1, p2 = 8, r2
	(p13) cmp4.ne.andcm	p17, p2 = 8, r2
	(p13) cmp4.ne.andcm	p1, p27 = 8, r2
	(p13) cmp4.ne.andcm	p1, p2 = 8, r34
	(p12) cmp4.ne.and.orcm	p1, p2 = r1, r2
	(p60) cmp4.ne.and.orcm	p1, p2 = r1, r2
	(p12) cmp4.ne.and.orcm	p17, p2 = r1, r2
	(p12) cmp4.ne.and.orcm	p1, p27 = r1, r2
	(p12) cmp4.ne.and.orcm	p1, p2 = r33, r2
	(p12) cmp4.ne.and.orcm	p1, p2 = r1, r34
	(p13) cmp4.ne.and.orcm	p1, p2 = 8, r2
	(p61) cmp4.ne.and.orcm	p1, p2 = 8, r2
	(p13) cmp4.ne.and.orcm	p17, p2 = 8, r2
	(p13) cmp4.ne.and.orcm	p1, p27 = 8, r2
	(p13) cmp4.ne.and.orcm	p1, p2 = 8, r34
	// A7 & pseudo-op
	(p0) cmp4.eq.or		p1, p2 = r0, r3
	(p16) cmp4.eq.or		p1, p2 = r0, r3
	(p0) cmp4.eq.or		p17, p2 = r0, r3
	(p0) cmp4.eq.or		p1, p27 = r0, r3
	(p0) cmp4.eq.or		p1, p2 = r0, r35
	(p0) cmp4.eq.or		p1, p2 = r35, r0
	(p0) cmp4.eq.and		p1, p2 = r0, r3
	(p16) cmp4.eq.and		p1, p2 = r0, r3
	(p0) cmp4.eq.and		p17, p2 = r0, r3
	(p0) cmp4.eq.and		p1, p27 = r0, r3
	(p0) cmp4.eq.and		p1, p2 = r0, r35
	(p0) cmp4.eq.and		p1, p2 = r35, r0
	(p0) cmp4.eq.or.andcm		p1, p2 = r0, r3
	(p16) cmp4.eq.or.andcm		p1, p2 = r0, r3
	(p0) cmp4.eq.or.andcm		p17, p2 = r0, r3
	(p0) cmp4.eq.or.andcm		p1, p27 = r0, r3
	(p0) cmp4.eq.or.andcm		p1, p2 = r0, r35
	(p0) cmp4.eq.or.andcm		p1, p2 = r35, r0
	(p0) cmp4.eq.orcm		p1, p2 = r0, r3
	(p16) cmp4.eq.orcm		p1, p2 = r0, r3
	(p0) cmp4.eq.orcm		p17, p2 = r0, r3
	(p0) cmp4.eq.orcm		p1, p27 = r0, r3
	(p0) cmp4.eq.orcm		p1, p2 = r0, r35
	(p0) cmp4.eq.orcm		p1, p2 = r35, r0
	(p0) cmp4.eq.andcm		p1, p2 = r0, r3
	(p16) cmp4.eq.andcm		p1, p2 = r0, r3
	(p0) cmp4.eq.andcm		p17, p2 = r0, r3
	(p0) cmp4.eq.andcm		p1, p27 = r0, r3
	(p0) cmp4.eq.andcm		p1, p2 = r0, r35
	(p0) cmp4.eq.andcm		p1, p2 = r35, r0
	(p0) cmp4.eq.and.orcm		p1, p2 = r0, r3
	(p16) cmp4.eq.and.orcm		p1, p2 = r0, r3
	(p0) cmp4.eq.and.orcm		p17, p2 = r0, r3
	(p0) cmp4.eq.and.orcm		p1, p27 = r0, r3
	(p0) cmp4.eq.and.orcm		p1, p2 = r0, r35
	(p0) cmp4.eq.and.orcm		p1, p2 = r35, r0
	(p0) cmp4.ne.or		p1, p2 = r0, r3
	(p16) cmp4.ne.or		p1, p2 = r0, r3
	(p0) cmp4.ne.or		p17, p2 = r0, r3
	(p0) cmp4.ne.or		p1, p27 = r0, r3
	(p0) cmp4.ne.or		p1, p2 = r0, r35
	(p0) cmp4.ne.or		p1, p2 = r35, r0
	(p0) cmp4.ne.and		p1, p2 = r0, r3
	(p16) cmp4.ne.and		p1, p2 = r0, r3
	(p0) cmp4.ne.and		p17, p2 = r0, r3
	(p0) cmp4.ne.and		p1, p27 = r0, r3
	(p0) cmp4.ne.and		p1, p2 = r0, r35
	(p0) cmp4.ne.and		p1, p2 = r35, r0
	(p0) cmp4.ne.or.andcm		p1, p2 = r0, r3
	(p16) cmp4.ne.or.andcm		p1, p2 = r0, r3
	(p0) cmp4.ne.or.andcm		p17, p2 = r0, r3
	(p0) cmp4.ne.or.andcm		p1, p27 = r0, r3
	(p0) cmp4.ne.or.andcm		p1, p2 = r0, r35
	(p0) cmp4.ne.or.andcm		p1, p2 = r35, r0
	(p0) cmp4.ne.orcm		p1, p2 = r0, r3
	(p16) cmp4.ne.orcm		p1, p2 = r0, r3
	(p0) cmp4.ne.orcm		p17, p2 = r0, r3
	(p0) cmp4.ne.orcm		p1, p27 = r0, r3
	(p0) cmp4.ne.orcm		p1, p2 = r0, r35
	(p0) cmp4.ne.orcm		p1, p2 = r35, r0
	(p0) cmp4.ne.andcm		p1, p2 = r0, r3
	(p16) cmp4.ne.andcm		p1, p2 = r0, r3
	(p0) cmp4.ne.andcm		p17, p2 = r0, r3
	(p0) cmp4.ne.andcm		p1, p27 = r0, r3
	(p0) cmp4.ne.andcm		p1, p2 = r0, r35
	(p0) cmp4.ne.andcm		p1, p2 = r35, r0
	(p0) cmp4.ne.and.orcm		p1, p2 = r0, r3
	(p16) cmp4.ne.and.orcm		p1, p2 = r0, r3
	(p0) cmp4.ne.and.orcm		p17, p2 = r0, r3
	(p0) cmp4.ne.and.orcm		p1, p27 = r0, r3
	(p0) cmp4.ne.and.orcm		p1, p2 = r0, r35
	(p0) cmp4.ne.and.orcm		p1, p2 = r35, r0
	(p0) cmp4.lt.or		p1, p2 = r0, r3
	(p16) cmp4.lt.or		p1, p2 = r0, r3
	(p0) cmp4.lt.or		p17, p2 = r0, r3
	(p0) cmp4.lt.or		p1, p27 = r0, r3
	(p0) cmp4.lt.or		p1, p2 = r0, r35
	(p0) cmp4.lt.or		p1, p2 = r35, r0
	(p0) cmp4.lt.and		p1, p2 = r0, r3
	(p16) cmp4.lt.and		p1, p2 = r0, r3
	(p0) cmp4.lt.and		p17, p2 = r0, r3
	(p0) cmp4.lt.and		p1, p27 = r0, r3
	(p0) cmp4.lt.and		p1, p2 = r0, r35
	(p0) cmp4.lt.and		p1, p2 = r35, r0
	(p0) cmp4.lt.or.andcm		p1, p2 = r0, r3
	(p16) cmp4.lt.or.andcm		p1, p2 = r0, r3
	(p0) cmp4.lt.or.andcm		p17, p2 = r0, r3
	(p0) cmp4.lt.or.andcm		p1, p27 = r0, r3
	(p0) cmp4.lt.or.andcm		p1, p2 = r0, r35
	(p0) cmp4.lt.or.andcm		p1, p2 = r35, r0
	(p0) cmp4.lt.orcm		p1, p2 = r0, r3
	(p16) cmp4.lt.orcm		p1, p2 = r0, r3
	(p0) cmp4.lt.orcm		p17, p2 = r0, r3
	(p0) cmp4.lt.orcm		p1, p27 = r0, r3
	(p0) cmp4.lt.orcm		p1, p2 = r0, r35
	(p0) cmp4.lt.orcm		p1, p2 = r35, r0
	(p0) cmp4.lt.andcm		p1, p2 = r0, r3
	(p16) cmp4.lt.andcm		p1, p2 = r0, r3
	(p0) cmp4.lt.andcm		p17, p2 = r0, r3
	(p0) cmp4.lt.andcm		p1, p27 = r0, r3
	(p0) cmp4.lt.andcm		p1, p2 = r0, r35
	(p0) cmp4.lt.andcm		p1, p2 = r35, r0
	(p0) cmp4.lt.and.orcm		p1, p2 = r0, r3
	(p16) cmp4.lt.and.orcm		p1, p2 = r0, r3
	(p0) cmp4.lt.and.orcm		p17, p2 = r0, r3
	(p0) cmp4.lt.and.orcm		p1, p27 = r0, r3
	(p0) cmp4.lt.and.orcm		p1, p2 = r0, r35
	(p0) cmp4.lt.and.orcm		p1, p2 = r35, r0
	(p0) cmp4.le.or		p1, p2 = r0, r3
	(p16) cmp4.le.or		p1, p2 = r0, r3
	(p0) cmp4.le.or		p17, p2 = r0, r3
	(p0) cmp4.le.or		p1, p27 = r0, r3
	(p0) cmp4.le.or		p1, p2 = r0, r35
	(p0) cmp4.le.or		p1, p2 = r35, r0
	(p0) cmp4.le.and		p1, p2 = r0, r3
	(p16) cmp4.le.and		p1, p2 = r0, r3
	(p0) cmp4.le.and		p17, p2 = r0, r3
	(p0) cmp4.le.and		p1, p27 = r0, r3
	(p0) cmp4.le.and		p1, p2 = r0, r35
	(p0) cmp4.le.and		p1, p2 = r35, r0
	(p0) cmp4.le.or.andcm		p1, p2 = r0, r3
	(p16) cmp4.le.or.andcm		p1, p2 = r0, r3
	(p0) cmp4.le.or.andcm		p17, p2 = r0, r3
	(p0) cmp4.le.or.andcm		p1, p27 = r0, r3
	(p0) cmp4.le.or.andcm		p1, p2 = r0, r35
	(p0) cmp4.le.or.andcm		p1, p2 = r35, r0
	(p0) cmp4.le.orcm		p1, p2 = r0, r3
	(p16) cmp4.le.orcm		p1, p2 = r0, r3
	(p0) cmp4.le.orcm		p17, p2 = r0, r3
	(p0) cmp4.le.orcm		p1, p27 = r0, r3
	(p0) cmp4.le.orcm		p1, p2 = r0, r35
	(p0) cmp4.le.orcm		p1, p2 = r35, r0
	(p0) cmp4.le.andcm		p1, p2 = r0, r3
	(p16) cmp4.le.andcm		p1, p2 = r0, r3
	(p0) cmp4.le.andcm		p17, p2 = r0, r3
	(p0) cmp4.le.andcm		p1, p27 = r0, r3
	(p0) cmp4.le.andcm		p1, p2 = r0, r35
	(p0) cmp4.le.andcm		p1, p2 = r35, r0
	(p0) cmp4.le.and.orcm		p1, p2 = r0, r3
	(p16) cmp4.le.and.orcm		p1, p2 = r0, r3
	(p0) cmp4.le.and.orcm		p17, p2 = r0, r3
	(p0) cmp4.le.and.orcm		p1, p27 = r0, r3
	(p0) cmp4.le.and.orcm		p1, p2 = r0, r35
	(p0) cmp4.le.and.orcm		p1, p2 = r35, r0
	(p0) cmp4.gt.or		p1, p2 = r0, r3
	(p16) cmp4.gt.or		p1, p2 = r0, r3
	(p0) cmp4.gt.or		p17, p2 = r0, r3
	(p0) cmp4.gt.or		p1, p27 = r0, r3
	(p0) cmp4.gt.or		p1, p2 = r0, r35
	(p0) cmp4.gt.or		p1, p2 = r35, r0
	(p0) cmp4.gt.and		p1, p2 = r0, r3
	(p16) cmp4.gt.and		p1, p2 = r0, r3
	(p0) cmp4.gt.and		p17, p2 = r0, r3
	(p0) cmp4.gt.and		p1, p27 = r0, r3
	(p0) cmp4.gt.and		p1, p2 = r0, r35
	(p0) cmp4.gt.and		p1, p2 = r35, r0
	(p0) cmp4.gt.or.andcm		p1, p2 = r0, r3
	(p16) cmp4.gt.or.andcm		p1, p2 = r0, r3
	(p0) cmp4.gt.or.andcm		p17, p2 = r0, r3
	(p0) cmp4.gt.or.andcm		p1, p27 = r0, r3
	(p0) cmp4.gt.or.andcm		p1, p2 = r0, r35
	(p0) cmp4.gt.or.andcm		p1, p2 = r35, r0
	(p0) cmp4.gt.orcm		p1, p2 = r0, r3
	(p16) cmp4.gt.orcm		p1, p2 = r0, r3
	(p0) cmp4.gt.orcm		p17, p2 = r0, r3
	(p0) cmp4.gt.orcm		p1, p27 = r0, r3
	(p0) cmp4.gt.orcm		p1, p2 = r0, r35
	(p0) cmp4.gt.orcm		p1, p2 = r35, r0
	(p0) cmp4.gt.andcm		p1, p2 = r0, r3
	(p16) cmp4.gt.andcm		p1, p2 = r0, r3
	(p0) cmp4.gt.andcm		p17, p2 = r0, r3
	(p0) cmp4.gt.andcm		p1, p27 = r0, r3
	(p0) cmp4.gt.andcm		p1, p2 = r0, r35
	(p0) cmp4.gt.andcm		p1, p2 = r35, r0
	(p0) cmp4.gt.and.orcm		p1, p2 = r0, r3
	(p16) cmp4.gt.and.orcm		p1, p2 = r0, r3
	(p0) cmp4.gt.and.orcm		p17, p2 = r0, r3
	(p0) cmp4.gt.and.orcm		p1, p27 = r0, r3
	(p0) cmp4.gt.and.orcm		p1, p2 = r0, r35
	(p0) cmp4.gt.and.orcm		p1, p2 = r35, r0
	(p0) cmp4.ge.or		p1, p2 = r0, r3
	(p16) cmp4.ge.or		p1, p2 = r0, r3
	(p0) cmp4.ge.or		p17, p2 = r0, r3
	(p0) cmp4.ge.or		p1, p27 = r0, r3
	(p0) cmp4.ge.or		p1, p2 = r0, r35
	(p0) cmp4.ge.or		p1, p2 = r35, r0
	(p0) cmp4.ge.and		p1, p2 = r0, r3
	(p16) cmp4.ge.and		p1, p2 = r0, r3
	(p0) cmp4.ge.and		p17, p2 = r0, r3
	(p0) cmp4.ge.and		p1, p27 = r0, r3
	(p0) cmp4.ge.and		p1, p2 = r0, r35
	(p0) cmp4.ge.and		p1, p2 = r35, r0
	(p0) cmp4.ge.or.andcm		p1, p2 = r0, r3
	(p16) cmp4.ge.or.andcm		p1, p2 = r0, r3
	(p0) cmp4.ge.or.andcm		p17, p2 = r0, r3
	(p0) cmp4.ge.or.andcm		p1, p27 = r0, r3
	(p0) cmp4.ge.or.andcm		p1, p2 = r0, r35
	(p0) cmp4.ge.or.andcm		p1, p2 = r35, r0
	(p0) cmp4.ge.orcm		p1, p2 = r0, r3
	(p16) cmp4.ge.orcm		p1, p2 = r0, r3
	(p0) cmp4.ge.orcm		p17, p2 = r0, r3
	(p0) cmp4.ge.orcm		p1, p27 = r0, r3
	(p0) cmp4.ge.orcm		p1, p2 = r0, r35
	(p0) cmp4.ge.orcm		p1, p2 = r35, r0
	(p0) cmp4.ge.andcm		p1, p2 = r0, r3
	(p16) cmp4.ge.andcm		p1, p2 = r0, r3
	(p0) cmp4.ge.andcm		p17, p2 = r0, r3
	(p0) cmp4.ge.andcm		p1, p27 = r0, r3
	(p0) cmp4.ge.andcm		p1, p2 = r0, r35
	(p0) cmp4.ge.andcm		p1, p2 = r35, r0
	(p0) cmp4.ge.and.orcm		p1, p2 = r0, r3
	(p16) cmp4.ge.and.orcm		p1, p2 = r0, r3
	(p0) cmp4.ge.and.orcm		p17, p2 = r0, r3
	(p0) cmp4.ge.and.orcm		p1, p27 = r0, r3
	(p0) cmp4.ge.and.orcm		p1, p2 = r0, r35
	(p0) cmp4.ge.and.orcm		p1, p2 = r35, r0

	(p0) cmpxchg1.acq	r1 = [r3], r2, ar.ccv 
	(p16) cmpxchg1.acq	r1 = [r3], r2, ar.ccv 
	(p0) cmpxchg1.acq	r33 = [r3], r2, ar.ccv 
	(p0) cmpxchg1.acq	r1 = [r35], r2, ar.ccv 
	(p0) cmpxchg1.acq	r1 = [r3], r34, ar.ccv
	(p1) cmpxchg1.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p17) cmpxchg1.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p1) cmpxchg1.acq.nt1	r33 = [r3], r2, ar.ccv 
	(p1) cmpxchg1.acq.nt1	r1 = [r35], r2, ar.ccv 
	(p1) cmpxchg1.acq.nt1	r1 = [r3], r34, ar.ccv 
	(p2) cmpxchg1.acq.nta	r1 = [r3], r2, ar.ccv
	(p18) cmpxchg1.acq.nta	r1 = [r3], r2, ar.ccv
	(p2) cmpxchg1.acq.nta	r33 = [r3], r2, ar.ccv
	(p2) cmpxchg1.acq.nta	r1 = [r35], r2, ar.ccv
	(p2) cmpxchg1.acq.nta	r1 = [r3], r34, ar.ccv
	(p3) cmpxchg1.rel	r1 = [r3], r2, ar.ccv 
	(p19) cmpxchg1.rel	r1 = [r3], r2, ar.ccv 
	(p3) cmpxchg1.rel	r33 = [r3], r2, ar.ccv 
	(p3) cmpxchg1.rel	r1 = [r35], r2, ar.ccv 
	(p3) cmpxchg1.rel	r1 = [r3], r34, ar.ccv 
	(p4) cmpxchg1.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p20) cmpxchg1.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p4) cmpxchg1.rel.nt1	r33 = [r3], r2, ar.ccv 
	(p4) cmpxchg1.rel.nt1	r1 = [r35], r2, ar.ccv 
	(p4) cmpxchg1.rel.nt1	r1 = [r3], r34, ar.ccv 
	(p5) cmpxchg1.rel.nta	r1 = [r3], r2, ar.ccv
	(p21) cmpxchg1.rel.nta	r1 = [r3], r2, ar.ccv
	(p5) cmpxchg1.rel.nta	r33 = [r3], r2, ar.ccv
	(p5) cmpxchg1.rel.nta	r1 = [r35], r2, ar.ccv
	(p5) cmpxchg1.rel.nta	r1 = [r3], r34, ar.ccv

	(p0) cmpxchg2.acq	r1 = [r3], r2, ar.ccv 
	(p16) cmpxchg2.acq	r1 = [r3], r2, ar.ccv 
	(p0) cmpxchg2.acq	r33 = [r3], r2, ar.ccv 
	(p0) cmpxchg2.acq	r1 = [r35], r2, ar.ccv 
	(p0) cmpxchg2.acq	r1 = [r3], r34, ar.ccv
	(p1) cmpxchg2.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p17) cmpxchg2.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p1) cmpxchg2.acq.nt1	r33 = [r3], r2, ar.ccv 
	(p1) cmpxchg2.acq.nt1	r1 = [r35], r2, ar.ccv 
	(p1) cmpxchg2.acq.nt1	r1 = [r3], r34, ar.ccv 
	(p2) cmpxchg2.acq.nta	r1 = [r3], r2, ar.ccv
	(p18) cmpxchg2.acq.nta	r1 = [r3], r2, ar.ccv
	(p2) cmpxchg2.acq.nta	r33 = [r3], r2, ar.ccv
	(p2) cmpxchg2.acq.nta	r1 = [r35], r2, ar.ccv
	(p2) cmpxchg2.acq.nta	r1 = [r3], r34, ar.ccv
	(p3) cmpxchg2.rel	r1 = [r3], r2, ar.ccv 
	(p19) cmpxchg2.rel	r1 = [r3], r2, ar.ccv 
	(p3) cmpxchg2.rel	r33 = [r3], r2, ar.ccv 
	(p3) cmpxchg2.rel	r1 = [r35], r2, ar.ccv 
	(p3) cmpxchg2.rel	r1 = [r3], r34, ar.ccv 
	(p4) cmpxchg2.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p20) cmpxchg2.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p4) cmpxchg2.rel.nt1	r33 = [r3], r2, ar.ccv 
	(p4) cmpxchg2.rel.nt1	r1 = [r35], r2, ar.ccv 
	(p4) cmpxchg2.rel.nt1	r1 = [r3], r34, ar.ccv 
	(p5) cmpxchg2.rel.nta	r1 = [r3], r2, ar.ccv
	(p21) cmpxchg2.rel.nta	r1 = [r3], r2, ar.ccv
	(p5) cmpxchg2.rel.nta	r33 = [r3], r2, ar.ccv
	(p5) cmpxchg2.rel.nta	r1 = [r35], r2, ar.ccv
	(p5) cmpxchg2.rel.nta	r1 = [r3], r34, ar.ccv

	(p0) cmpxchg4.acq	r1 = [r3], r2, ar.ccv 
	(p16) cmpxchg4.acq	r1 = [r3], r2, ar.ccv 
	(p0) cmpxchg4.acq	r33 = [r3], r2, ar.ccv 
	(p0) cmpxchg4.acq	r1 = [r35], r2, ar.ccv 
	(p0) cmpxchg4.acq	r1 = [r3], r34, ar.ccv
	(p1) cmpxchg4.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p17) cmpxchg4.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p1) cmpxchg4.acq.nt1	r33 = [r3], r2, ar.ccv 
	(p1) cmpxchg4.acq.nt1	r1 = [r35], r2, ar.ccv 
	(p1) cmpxchg4.acq.nt1	r1 = [r3], r34, ar.ccv 
	(p2) cmpxchg4.acq.nta	r1 = [r3], r2, ar.ccv
	(p18) cmpxchg4.acq.nta	r1 = [r3], r2, ar.ccv
	(p2) cmpxchg4.acq.nta	r33 = [r3], r2, ar.ccv
	(p2) cmpxchg4.acq.nta	r1 = [r35], r2, ar.ccv
	(p2) cmpxchg4.acq.nta	r1 = [r3], r34, ar.ccv
	(p3) cmpxchg4.rel	r1 = [r3], r2, ar.ccv 
	(p19) cmpxchg4.rel	r1 = [r3], r2, ar.ccv 
	(p3) cmpxchg4.rel	r33 = [r3], r2, ar.ccv 
	(p3) cmpxchg4.rel	r1 = [r35], r2, ar.ccv 
	(p3) cmpxchg4.rel	r1 = [r3], r34, ar.ccv 
	(p4) cmpxchg4.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p20) cmpxchg4.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p4) cmpxchg4.rel.nt1	r33 = [r3], r2, ar.ccv 
	(p4) cmpxchg4.rel.nt1	r1 = [r35], r2, ar.ccv 
	(p4) cmpxchg4.rel.nt1	r1 = [r3], r34, ar.ccv 
	(p5) cmpxchg4.rel.nta	r1 = [r3], r2, ar.ccv
	(p21) cmpxchg4.rel.nta	r1 = [r3], r2, ar.ccv
	(p5) cmpxchg4.rel.nta	r33 = [r3], r2, ar.ccv
	(p5) cmpxchg4.rel.nta	r1 = [r35], r2, ar.ccv
	(p5) cmpxchg4.rel.nta	r1 = [r3], r34, ar.ccv

	(p0) cmpxchg8.acq	r1 = [r3], r2, ar.ccv 
	(p16) cmpxchg8.acq	r1 = [r3], r2, ar.ccv 
	(p0) cmpxchg8.acq	r33 = [r3], r2, ar.ccv 
	(p0) cmpxchg8.acq	r1 = [r35], r2, ar.ccv 
	(p0) cmpxchg8.acq	r1 = [r3], r34, ar.ccv
	(p1) cmpxchg8.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p17) cmpxchg8.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p1) cmpxchg8.acq.nt1	r33 = [r3], r2, ar.ccv 
	(p1) cmpxchg8.acq.nt1	r1 = [r35], r2, ar.ccv 
	(p1) cmpxchg8.acq.nt1	r1 = [r3], r34, ar.ccv 
	(p2) cmpxchg8.acq.nta	r1 = [r3], r2, ar.ccv
	(p18) cmpxchg8.acq.nta	r1 = [r3], r2, ar.ccv
	(p2) cmpxchg8.acq.nta	r33 = [r3], r2, ar.ccv
	(p2) cmpxchg8.acq.nta	r1 = [r35], r2, ar.ccv
	(p2) cmpxchg8.acq.nta	r1 = [r3], r34, ar.ccv
	(p3) cmpxchg8.rel	r1 = [r3], r2, ar.ccv 
	(p19) cmpxchg8.rel	r1 = [r3], r2, ar.ccv 
	(p3) cmpxchg8.rel	r33 = [r3], r2, ar.ccv 
	(p3) cmpxchg8.rel	r1 = [r35], r2, ar.ccv 
	(p3) cmpxchg8.rel	r1 = [r3], r34, ar.ccv 
	(p4) cmpxchg8.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p20) cmpxchg8.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p4) cmpxchg8.rel.nt1	r33 = [r3], r2, ar.ccv 
	(p4) cmpxchg8.rel.nt1	r1 = [r35], r2, ar.ccv 
	(p4) cmpxchg8.rel.nt1	r1 = [r3], r34, ar.ccv 
	(p5) cmpxchg8.rel.nta	r1 = [r3], r2, ar.ccv
	(p21) cmpxchg8.rel.nta	r1 = [r3], r2, ar.ccv
	(p5) cmpxchg8.rel.nta	r33 = [r3], r2, ar.ccv
	(p5) cmpxchg8.rel.nta	r1 = [r35], r2, ar.ccv
	(p5) cmpxchg8.rel.nta	r1 = [r3], r34, ar.ccv

	cover ;;

	(p0) czx1.l r1 = r3
	(p40) czx1.l r1 = r3
	(p0) czx1.l r33 = r3
	(p0) czx1.l r1 = r35
	(p11) czx1.r r1 = r3
	(p41) czx1.r r1 = r3
	(p11) czx1.r r33 = r3
	(p11) czx1.r r1 = r35
	(p12) czx2.l r1 = r3
	(p42) czx2.l r1 = r3
	(p12) czx2.l r33 = r3
	(p12) czx2.l r1 = r35
	(p13) czx2.r r1 = r3
	(p43) czx2.r r1 = r3
	(p13) czx2.r r33 = r3
	(p13) czx2.r r1 = r35

	////    ******** D ******


	(p4) dep	r1 = r2, r3, 6, 4
	(p44) dep	r1 = r2, r3, 6, 4
	(p4) dep	r100 = r2, r3, 6, 4
	(p4) dep	r1 = r34, r3, 6, 4
	(p4) dep	r1 = r2, r35, 6, 4
	(p3) dep	r1 = 0, r3, 6, 4
	(p43) dep	r1 = 0, r3, 6, 4
	(p3) dep	r33 = 0, r3, 6, 4
	(p3) dep	r1 = 0, r35, 6, 4
	(p4) dep	r1 = -1, r3, 6, 4
	(p44) dep	r1 = -1, r3, 6, 4
	(p4) dep	r100 = -1, r3, 6, 4
	(p4) dep	r1 = -1, r35, 6, 4
	(p5) dep.z	r1 = r2, 6, 6
	(p45) dep.z	r1 = r2, 6, 6
	(p5) dep.z	r33 = r2, 6, 6
	(p5) dep.z	r1 = r34, 6, 6
	(p6) dep.z	r1 = 8, 6, 6
	(p46) dep.z	r1 = 8, 6, 6
	(p6) dep.z	r100 = 8, 6, 6
	(p7) dep.z	r1 = -8, 6, 6
	(p47) dep.z	r1 = -8, 6, 6
	(p7) dep.z	r100 = -8, 6, 6

	//// ********* E ********

	epc


	(p8) extr	r1 = r3, 6, 16 
	(p48) extr	r1 = r3, 6, 16 
	(p8) extr	r33 = r3, 6, 16 
	(p8) extr	r1 = r35, 6, 16 

	(p9) extr.u	r1 = r3, 6, 16 
	(p49) extr.u	r1 = r3, 6, 16 
	(p9) extr.u	r33 = r3, 6, 16 
	(p9) extr.u	r1 = r35, 6, 16 

	//// ******** F *********

	(p5) fabs f10=f3
	(p50) fabs f10=f3
	(p5) fabs f100=f3
	(p5) fabs f10=f33

	(p1) fadd   f10 = f2, f3 
	(p51) fadd   f10 = f2, f3 
	(p1) fadd   f100 = f2, f3 
	(p1) fadd   f10 = f34, f3 
	(p1) fadd   f10 = f2, f35 
	(p1) fadd.s0   f10 = f2, f3 
	(p51) fadd.s0   f10 = f2, f3 
	(p1) fadd.s0   f100 = f2, f3 
	(p1) fadd.s0   f10 = f34, f3 
	(p1) fadd.s0   f10 = f2, f35 
	(p1) fadd.s1   f10 = f2, f3 
	(p51) fadd.s1   f10 = f2, f3 
	(p1) fadd.s1   f100 = f2, f3 
	(p1) fadd.s1   f10 = f34, f3 
	(p1) fadd.s1   f10 = f2, f35 
	(p1) fadd.s2   f10 = f2, f3 
	(p51) fadd.s2   f10 = f2, f3 
	(p1) fadd.s2   f100 = f2, f3 
	(p1) fadd.s2   f10 = f34, f3 
	(p1) fadd.s2   f10 = f2, f35 
	(p1) fadd.s3   f10 = f2, f3 
	(p51) fadd.s3   f10 = f2, f3 
	(p1) fadd.s3   f100 = f2, f3 
	(p1) fadd.s3   f10 = f34, f3 
	(p1) fadd.s3   f10 = f2, f35 
	(p1) fadd.s   f10 = f2, f3 
	(p51) fadd.s   f10 = f2, f3 
	(p1) fadd.s   f100 = f2, f3 
	(p1) fadd.s   f10 = f34, f3 
	(p1) fadd.s   f10 = f2, f35 
	(p1) fadd.s.s0   f10 = f2, f3 
	(p51) fadd.s.s0   f10 = f2, f3 
	(p1) fadd.s.s0   f100 = f2, f3 
	(p1) fadd.s.s0   f10 = f34, f3 
	(p1) fadd.s.s0   f10 = f2, f35 
	(p1) fadd.s.s1   f10 = f2, f3 
	(p51) fadd.s.s1   f10 = f2, f3 
	(p1) fadd.s.s1   f100 = f2, f3 
	(p1) fadd.s.s1   f10 = f34, f3 
	(p1) fadd.s.s1   f10 = f2, f35 
	(p1) fadd.s.s2   f10 = f2, f3 
	(p51) fadd.s.s2   f10 = f2, f3 
	(p1) fadd.s.s2   f100 = f2, f3 
	(p1) fadd.s.s2   f10 = f34, f3 
	(p1) fadd.s.s2   f10 = f2, f35 
	(p1) fadd.s.s3   f10 = f2, f3 
	(p51) fadd.s.s3   f10 = f2, f3 
	(p1) fadd.s.s3   f100 = f2, f3 
	(p1) fadd.s.s3   f10 = f34, f3 
	(p1) fadd.s.s3   f10 = f2, f35 
	(p1) fadd.d   f10 = f2, f3 
	(p51) fadd.d   f10 = f2, f3 
	(p1) fadd.d   f100 = f2, f3 
	(p1) fadd.d   f10 = f34, f3 
	(p1) fadd.d   f10 = f2, f35 
	(p1) fadd.d.s0   f10 = f2, f3 
	(p51) fadd.d.s0   f10 = f2, f3 
	(p1) fadd.d.s0   f100 = f2, f3 
	(p1) fadd.d.s0   f10 = f34, f3 
	(p1) fadd.d.s0   f10 = f2, f35 
	(p1) fadd.d.s1   f10 = f2, f3 
	(p51) fadd.d.s1   f10 = f2, f3 
	(p1) fadd.d.s1   f100 = f2, f3 
	(p1) fadd.d.s1   f10 = f34, f3 
	(p1) fadd.d.s1   f10 = f2, f35 
	(p1) fadd.d.s2   f10 = f2, f3 
	(p51) fadd.d.s2   f10 = f2, f3 
	(p1) fadd.d.s2   f100 = f2, f3 
	(p1) fadd.d.s2   f10 = f34, f3 
	(p1) fadd.d.s2   f10 = f2, f35 
	(p1) fadd.d.s3   f10 = f2, f3 
	(p51) fadd.d.s3   f10 = f2, f3 
	(p1) fadd.d.s3   f100 = f2, f3 
	(p1) fadd.d.s3   f10 = f34, f3 
	(p1) fadd.d.s3   f10 = f2, f35 

	(p2) famax f10=f3,f4 
	(p20) famax f10=f3,f4 
	(p2) famax f100=f3,f4 
	(p2) famax f10=f35,f4 
	(p2) famax f10=f3,f40 
	(p3) famax.s0 f10=f3,f4 
	(p30) famax.s0 f10=f3,f4 
	(p3) famax.s0 f100=f3,f4 
	(p3) famax.s0 f10=f35,f4 
	(p3) famax.s0 f10=f3,f40 
	(p3) famax.s1 f10=f3,f4 
	(p30) famax.s1 f10=f3,f4 
	(p3) famax.s1 f100=f3,f4 
	(p3) famax.s1 f10=f35,f4 
	(p3) famax.s1 f10=f3,f40 
	(p3) famax.s2 f10=f3,f4 
	(p30) famax.s2 f10=f3,f4 
	(p3) famax.s2 f100=f3,f4 
	(p3) famax.s2 f10=f35,f4 
	(p3) famax.s2 f10=f3,f40 
	(p3) famax.s3 f10=f3,f4 
	(p30) famax.s3 f10=f3,f4 
	(p3) famax.s3 f100=f3,f4 
	(p3) famax.s3 f10=f35,f4 
	(p3) famax.s3 f10=f3,f40 

	(p2) famin f10=f3,f4 
	(p20) famin f10=f3,f4 
	(p2) famin f100=f3,f4 
	(p2) famin f10=f35,f4 
	(p2) famin f10=f3,f40 
	(p3) famin.s0 f10=f3,f4 
	(p30) famin.s0 f10=f3,f4 
	(p3) famin.s0 f100=f3,f4 
	(p3) famin.s0 f10=f35,f4 
	(p3) famin.s0 f10=f3,f40 
	(p3) famin.s1 f10=f3,f4 
	(p30) famin.s1 f10=f3,f4 
	(p3) famin.s1 f100=f3,f4 
	(p3) famin.s1 f10=f35,f4 
	(p3) famin.s1 f10=f3,f40 
	(p3) famin.s2 f10=f3,f4 
	(p30) famin.s2 f10=f3,f4 
	(p3) famin.s2 f100=f3,f4 
	(p3) famin.s2 f10=f35,f4 
	(p3) famin.s2 f10=f3,f40 
	(p3) famin.s3 f10=f3,f4 
	(p30) famin.s3 f10=f3,f4 
	(p3) famin.s3 f100=f3,f4 
	(p3) famin.s3 f10=f35,f4 
	(p3) famin.s3 f10=f3,f40 

	(p12) fand f10 = f2, f3
	(p22) fand f10 = f2, f3
	(p12) fand f100 = f2, f3
	(p12) fand f10 = f34, f3
	(p12) fand f10 = f2, f35

	(p12) fandcm f10 = f2, f3
	(p22) fandcm f10 = f2, f3
	(p12) fandcm f100 = f2, f3
	(p12) fandcm f10 = f34, f3
	(p12) fandcm f10 = f2, f35

	(p14) fc 	r3
	(p24) fc 	r3
	(p14) fc 	r35

	(p15) fchkf	.alabel
	(p31) fchkf	.alabel
	(p0) fchkf.s0	.alabel
	(p16) fchkf.s0	.alabel
	(p1) fchkf.s1	.alabel
	(p17) fchkf.s1	.alabel
	(p2) fchkf.s2	.alabel
	(p18) fchkf.s2	.alabel
	(p3) fchkf.s3	.alabel
	(p19) fchkf.s3	.alabel

	(p2) fclass.m		p1, p2 = f2, @nat
	(p20) fclass.m		p1, p2 = f2, @nat
	(p2) fclass.m		p17, p2 = f2, @nat
	(p2) fclass.m		p1, p20 = f2, @nat
	(p2) fclass.m		p1, p2 = f34, @nat
	(p2) fclass.m.unc	p1, p2 = f2, @nat
	(p20) fclass.m.unc	p1, p2 = f2, @nat
	(p2) fclass.m.unc	p17, p2 = f2, @nat
	(p2) fclass.m.unc	p1, p20 = f2, @nat
	(p2) fclass.m.unc	p1, p2 = f34, @nat
	(p2) fclass.nm		p1, p2 = f2, @nat
	(p20) fclass.nm		p1, p2 = f2, @nat
	(p2) fclass.nm		p17, p2 = f2, @nat
	(p2) fclass.nm		p1, p20 = f2, @nat
	(p2) fclass.nm		p1, p2 = f34, @nat
	(p2) fclass.nm.unc	p1, p2 = f2, @nat
	(p20) fclass.nm.unc	p1, p2 = f2, @nat
	(p2) fclass.nm.unc	p17, p2 = f2, @nat
	(p2) fclass.nm.unc	p1, p20 = f2, @nat
	(p2) fclass.nm.unc	p1, p2 = f34, @nat
	(p2) fclass.m		p1, p2 = f2, @qnan
	(p20) fclass.m		p1, p2 = f2, @qnan
	(p2) fclass.m		p17, p2 = f2, @qnan
	(p2) fclass.m		p1, p20 = f2, @qnan
	(p2) fclass.m		p1, p2 = f34, @qnan
	(p2) fclass.m.unc	p1, p2 = f2, @qnan
	(p20) fclass.m.unc	p1, p2 = f2, @qnan
	(p2) fclass.m.unc	p17, p2 = f2, @qnan
	(p2) fclass.m.unc	p1, p20 = f2, @qnan
	(p2) fclass.m.unc	p1, p2 = f34, @qnan
	(p2) fclass.nm		p1, p2 = f2, @qnan
	(p20) fclass.nm		p1, p2 = f2, @qnan
	(p2) fclass.nm		p17, p2 = f2, @qnan
	(p2) fclass.nm		p1, p20 = f2, @qnan
	(p2) fclass.nm		p1, p2 = f34, @qnan
	(p2) fclass.nm.unc	p1, p2 = f2, @qnan
	(p20) fclass.nm.unc	p1, p2 = f2, @qnan
	(p2) fclass.nm.unc	p17, p2 = f2, @qnan
	(p2) fclass.nm.unc	p1, p20 = f2, @qnan
	(p2) fclass.nm.unc	p1, p2 = f34, @qnan
	(p2) fclass.m		p1, p2 = f2, @snan
	(p20) fclass.m		p1, p2 = f2, @snan
	(p2) fclass.m		p17, p2 = f2, @snan
	(p2) fclass.m		p1, p20 = f2, @snan
	(p2) fclass.m		p1, p2 = f34, @snan
	(p2) fclass.m.unc	p1, p2 = f2, @snan
	(p20) fclass.m.unc	p1, p2 = f2, @snan
	(p2) fclass.m.unc	p17, p2 = f2, @snan
	(p2) fclass.m.unc	p1, p20 = f2, @snan
	(p2) fclass.m.unc	p1, p2 = f34, @snan
	(p2) fclass.nm		p1, p2 = f2, @snan
	(p20) fclass.nm		p1, p2 = f2, @snan
	(p2) fclass.nm		p17, p2 = f2, @snan
	(p2) fclass.nm		p1, p20 = f2, @snan
	(p2) fclass.nm		p1, p2 = f34, @snan
	(p2) fclass.nm.unc	p1, p2 = f2, @snan
	(p20) fclass.nm.unc	p1, p2 = f2, @snan
	(p2) fclass.nm.unc	p17, p2 = f2, @snan
	(p2) fclass.nm.unc	p1, p20 = f2, @snan
	(p2) fclass.nm.unc	p1, p2 = f34, @snan
	(p2) fclass.m		p1, p2 = f2, @pos
	(p20) fclass.m		p1, p2 = f2, @pos
	(p2) fclass.m		p17, p2 = f2, @pos
	(p2) fclass.m		p1, p20 = f2, @pos
	(p2) fclass.m		p1, p2 = f34, @pos
	(p2) fclass.m.unc	p1, p2 = f2, @pos
	(p20) fclass.m.unc	p1, p2 = f2, @pos
	(p2) fclass.m.unc	p17, p2 = f2, @pos
	(p2) fclass.m.unc	p1, p20 = f2, @pos
	(p2) fclass.m.unc	p1, p2 = f34, @pos
	(p2) fclass.nm		p1, p2 = f2, @pos
	(p20) fclass.nm		p1, p2 = f2, @pos
	(p2) fclass.nm		p17, p2 = f2, @pos
	(p2) fclass.nm		p1, p20 = f2, @pos
	(p2) fclass.nm		p1, p2 = f34, @pos
	(p2) fclass.nm.unc	p1, p2 = f2, @pos
	(p20) fclass.nm.unc	p1, p2 = f2, @pos
	(p2) fclass.nm.unc	p17, p2 = f2, @pos
	(p2) fclass.nm.unc	p1, p20 = f2, @pos
	(p2) fclass.nm.unc	p1, p2 = f34, @pos
	(p2) fclass.m		p1, p2 = f2, @neg
	(p20) fclass.m		p1, p2 = f2, @neg
	(p2) fclass.m		p17, p2 = f2, @neg
	(p2) fclass.m		p1, p20 = f2, @neg
	(p2) fclass.m		p1, p2 = f34, @neg
	(p2) fclass.m.unc	p1, p2 = f2, @neg
	(p20) fclass.m.unc	p1, p2 = f2, @neg
	(p2) fclass.m.unc	p17, p2 = f2, @neg
	(p2) fclass.m.unc	p1, p20 = f2, @neg
	(p2) fclass.m.unc	p1, p2 = f34, @neg
	(p2) fclass.nm		p1, p2 = f2, @neg
	(p20) fclass.nm		p1, p2 = f2, @neg
	(p2) fclass.nm		p17, p2 = f2, @neg
	(p2) fclass.nm		p1, p20 = f2, @neg
	(p2) fclass.nm		p1, p2 = f34, @neg
	(p2) fclass.nm.unc	p1, p2 = f2, @neg
	(p20) fclass.nm.unc	p1, p2 = f2, @neg
	(p2) fclass.nm.unc	p17, p2 = f2, @neg
	(p2) fclass.nm.unc	p1, p20 = f2, @neg
	(p2) fclass.nm.unc	p1, p2 = f34, @neg
	(p2) fclass.m		p1, p2 = f2, @zero
	(p20) fclass.m		p1, p2 = f2, @zero
	(p2) fclass.m		p17, p2 = f2, @zero
	(p2) fclass.m		p1, p20 = f2, @zero
	(p2) fclass.m		p1, p2 = f34, @zero
	(p2) fclass.m.unc	p1, p2 = f2, @zero
	(p20) fclass.m.unc	p1, p2 = f2, @zero
	(p2) fclass.m.unc	p17, p2 = f2, @zero
	(p2) fclass.m.unc	p1, p20 = f2, @zero
	(p2) fclass.m.unc	p1, p2 = f34, @zero
	(p2) fclass.nm		p1, p2 = f2, @zero
	(p20) fclass.nm		p1, p2 = f2, @zero
	(p2) fclass.nm		p17, p2 = f2, @zero
	(p2) fclass.nm		p1, p20 = f2, @zero
	(p2) fclass.nm		p1, p2 = f34, @zero
	(p2) fclass.nm.unc	p1, p2 = f2, @zero
	(p20) fclass.nm.unc	p1, p2 = f2, @zero
	(p2) fclass.nm.unc	p17, p2 = f2, @zero
	(p2) fclass.nm.unc	p1, p20 = f2, @zero
	(p2) fclass.nm.unc	p1, p2 = f34, @zero
	(p2) fclass.m		p1, p2 = f2, @unorm
	(p20) fclass.m		p1, p2 = f2, @unorm
	(p2) fclass.m		p17, p2 = f2, @unorm
	(p2) fclass.m		p1, p20 = f2, @unorm
	(p2) fclass.m		p1, p2 = f34, @unorm
	(p2) fclass.m.unc	p1, p2 = f2, @unorm
	(p20) fclass.m.unc	p1, p2 = f2, @unorm
	(p2) fclass.m.unc	p17, p2 = f2, @unorm
	(p2) fclass.m.unc	p1, p20 = f2, @unorm
	(p2) fclass.m.unc	p1, p2 = f34, @unorm
	(p2) fclass.nm		p1, p2 = f2, @unorm
	(p20) fclass.nm		p1, p2 = f2, @unorm
	(p2) fclass.nm		p17, p2 = f2, @unorm
	(p2) fclass.nm		p1, p20 = f2, @unorm
	(p2) fclass.nm		p1, p2 = f34, @unorm
	(p2) fclass.nm.unc	p1, p2 = f2, @unorm
	(p20) fclass.nm.unc	p1, p2 = f2, @unorm
	(p2) fclass.nm.unc	p17, p2 = f2, @unorm
	(p2) fclass.nm.unc	p1, p20 = f2, @unorm
	(p2) fclass.nm.unc	p1, p2 = f34, @unorm
	(p2) fclass.m		p1, p2 = f2, @norm
	(p20) fclass.m		p1, p2 = f2, @norm
	(p2) fclass.m		p17, p2 = f2, @norm
	(p2) fclass.m		p1, p20 = f2, @norm
	(p2) fclass.m		p1, p2 = f34, @norm
	(p2) fclass.m.unc	p1, p2 = f2, @norm
	(p20) fclass.m.unc	p1, p2 = f2, @norm
	(p2) fclass.m.unc	p17, p2 = f2, @norm
	(p2) fclass.m.unc	p1, p20 = f2, @norm
	(p2) fclass.m.unc	p1, p2 = f34, @norm
	(p2) fclass.nm		p1, p2 = f2, @norm
	(p20) fclass.nm		p1, p2 = f2, @norm
	(p2) fclass.nm		p17, p2 = f2, @norm
	(p2) fclass.nm		p1, p20 = f2, @norm
	(p2) fclass.nm		p1, p2 = f34, @norm
	(p2) fclass.nm.unc	p1, p2 = f2, @norm
	(p20) fclass.nm.unc	p1, p2 = f2, @norm
	(p2) fclass.nm.unc	p17, p2 = f2, @norm
	(p2) fclass.nm.unc	p1, p20 = f2, @norm
	(p2) fclass.nm.unc	p1, p2 = f34, @norm
	(p2) fclass.m		p1, p2 = f2, @inf
	(p20) fclass.m		p1, p2 = f2, @inf
	(p2) fclass.m		p17, p2 = f2, @inf
	(p2) fclass.m		p1, p20 = f2, @inf
	(p2) fclass.m		p1, p2 = f34, @inf
	(p2) fclass.m.unc	p1, p2 = f2, @inf
	(p20) fclass.m.unc	p1, p2 = f2, @inf
	(p2) fclass.m.unc	p17, p2 = f2, @inf
	(p2) fclass.m.unc	p1, p20 = f2, @inf
	(p2) fclass.m.unc	p1, p2 = f34, @inf
	(p2) fclass.nm		p1, p2 = f2, @inf
	(p20) fclass.nm		p1, p2 = f2, @inf
	(p2) fclass.nm		p17, p2 = f2, @inf
	(p2) fclass.nm		p1, p20 = f2, @inf
	(p2) fclass.nm		p1, p2 = f34, @inf
	(p2) fclass.nm.unc	p1, p2 = f2, @inf
	(p20) fclass.nm.unc	p1, p2 = f2, @inf
	(p2) fclass.nm.unc	p17, p2 = f2, @inf
	(p2) fclass.nm.unc	p1, p20 = f2, @inf
	(p2) fclass.nm.unc	p1, p2 = f34, @inf

	(p6) fclrf
	(p56) fclrf
	(p7) fclrf.s0
	(p57) fclrf.s0
	(p8) fclrf.s1
	(p58) fclrf.s1
	(p9) fclrf.s2
	(p59) fclrf.s2
	(p10) fclrf.s3
	(p60) fclrf.s3

	(p1) fcmp.gt 		p2,p3 = f2, f3
	(p61) fcmp.gt 		p2,p3 = f2, f3
	(p1) fcmp.gt 		p20,p3 = f2, f3
	(p1) fcmp.gt 		p2,p30 = f2, f3
	(p1) fcmp.gt 		p2,p3 = f34, f3
	(p1) fcmp.gt 		p2,p3 = f2, f35
	(p1) fcmp.eq 		p2,p3 = f2, f3
	(p61) fcmp.eq 		p2,p3 = f2, f3
	(p1) fcmp.eq 		p20,p3 = f2, f3
	(p1) fcmp.eq 		p2,p30 = f2, f3
	(p1) fcmp.eq 		p2,p3 = f34, f3
	(p1) fcmp.eq 		p2,p3 = f2, f35
	(p1) fcmp.lt 		p2,p3 = f2, f3
	(p61) fcmp.lt 		p2,p3 = f2, f3
	(p1) fcmp.lt 		p20,p3 = f2, f3
	(p1) fcmp.lt 		p2,p30 = f2, f3
	(p1) fcmp.lt 		p2,p3 = f34, f3
	(p1) fcmp.lt 		p2,p3 = f2, f35
	(p1) fcmp.ge 		p2,p3 = f2, f3
	(p61) fcmp.ge 		p2,p3 = f2, f3
	(p1) fcmp.ge 		p20,p3 = f2, f3
	(p1) fcmp.ge 		p2,p30 = f2, f3
	(p1) fcmp.ge 		p2,p3 = f34, f3
	(p1) fcmp.ge 		p2,p3 = f2, f35
	(p1) fcmp.le 		p2,p3 = f2, f3
	(p61) fcmp.le 		p2,p3 = f2, f3
	(p1) fcmp.le 		p20,p3 = f2, f3
	(p1) fcmp.le 		p2,p30 = f2, f3
	(p1) fcmp.le 		p2,p3 = f34, f3
	(p1) fcmp.le 		p2,p3 = f2, f35
	(p1) fcmp.unord 		p2,p3 = f2, f3
	(p61) fcmp.unord 		p2,p3 = f2, f3
	(p1) fcmp.unord 		p20,p3 = f2, f3
	(p1) fcmp.unord 		p2,p30 = f2, f3
	(p1) fcmp.unord 		p2,p3 = f34, f3
	(p1) fcmp.unord 		p2,p3 = f2, f35
	(p1) fcmp.ngt 		p2,p3 = f2, f3
	(p61) fcmp.ngt 		p2,p3 = f2, f3
	(p1) fcmp.ngt 		p20,p3 = f2, f3
	(p1) fcmp.ngt 		p2,p30 = f2, f3
	(p1) fcmp.ngt 		p2,p3 = f34, f3
	(p1) fcmp.ngt 		p2,p3 = f2, f35
	(p1) fcmp.neq 		p2,p3 = f2, f3
	(p61) fcmp.neq 		p2,p3 = f2, f3
	(p1) fcmp.neq 		p20,p3 = f2, f3
	(p1) fcmp.neq 		p2,p30 = f2, f3
	(p1) fcmp.neq 		p2,p3 = f34, f3
	(p1) fcmp.neq 		p2,p3 = f2, f35
	(p1) fcmp.nlt 		p2,p3 = f2, f3
	(p61) fcmp.nlt 		p2,p3 = f2, f3
	(p1) fcmp.nlt 		p20,p3 = f2, f3
	(p1) fcmp.nlt 		p2,p30 = f2, f3
	(p1) fcmp.nlt 		p2,p3 = f34, f3
	(p1) fcmp.nlt 		p2,p3 = f2, f35
	(p1) fcmp.nge 		p2,p3 = f2, f3
	(p61) fcmp.nge 		p2,p3 = f2, f3
	(p1) fcmp.nge 		p20,p3 = f2, f3
	(p1) fcmp.nge 		p2,p30 = f2, f3
	(p1) fcmp.nge 		p2,p3 = f34, f3
	(p1) fcmp.nge 		p2,p3 = f2, f35
	(p1) fcmp.nle 		p2,p3 = f2, f3
	(p61) fcmp.nle 		p2,p3 = f2, f3
	(p1) fcmp.nle 		p20,p3 = f2, f3
	(p1) fcmp.nle 		p2,p30 = f2, f3
	(p1) fcmp.nle 		p2,p3 = f34, f3
	(p1) fcmp.nle 		p2,p3 = f2, f35
	(p1) fcmp.ord 		p2,p3 = f2, f3
	(p61) fcmp.ord 		p2,p3 = f2, f3
	(p1) fcmp.ord 		p20,p3 = f2, f3
	(p1) fcmp.ord 		p2,p30 = f2, f3
	(p1) fcmp.ord 		p2,p3 = f34, f3
	(p1) fcmp.ord 		p2,p3 = f2, f35
	(p1) fcmp.gt.unc 		p2,p3 = f2, f3
	(p61) fcmp.gt.unc 		p2,p3 = f2, f3
	(p1) fcmp.gt.unc 		p20,p3 = f2, f3
	(p1) fcmp.gt.unc 		p2,p30 = f2, f3
	(p1) fcmp.gt.unc 		p2,p3 = f34, f3
	(p1) fcmp.gt.unc 		p2,p3 = f2, f35
	(p1) fcmp.eq.unc 		p2,p3 = f2, f3
	(p61) fcmp.eq.unc 		p2,p3 = f2, f3
	(p1) fcmp.eq.unc 		p20,p3 = f2, f3
	(p1) fcmp.eq.unc 		p2,p30 = f2, f3
	(p1) fcmp.eq.unc 		p2,p3 = f34, f3
	(p1) fcmp.eq.unc 		p2,p3 = f2, f35
	(p1) fcmp.lt.unc 		p2,p3 = f2, f3
	(p61) fcmp.lt.unc 		p2,p3 = f2, f3
	(p1) fcmp.lt.unc 		p20,p3 = f2, f3
	(p1) fcmp.lt.unc 		p2,p30 = f2, f3
	(p1) fcmp.lt.unc 		p2,p3 = f34, f3
	(p1) fcmp.lt.unc 		p2,p3 = f2, f35
	(p1) fcmp.ge.unc 		p2,p3 = f2, f3
	(p61) fcmp.ge.unc 		p2,p3 = f2, f3
	(p1) fcmp.ge.unc 		p20,p3 = f2, f3
	(p1) fcmp.ge.unc 		p2,p30 = f2, f3
	(p1) fcmp.ge.unc 		p2,p3 = f34, f3
	(p1) fcmp.ge.unc 		p2,p3 = f2, f35
	(p1) fcmp.le.unc 		p2,p3 = f2, f3
	(p61) fcmp.le.unc 		p2,p3 = f2, f3
	(p1) fcmp.le.unc 		p20,p3 = f2, f3
	(p1) fcmp.le.unc 		p2,p30 = f2, f3
	(p1) fcmp.le.unc 		p2,p3 = f34, f3
	(p1) fcmp.le.unc 		p2,p3 = f2, f35
	(p1) fcmp.unord.unc 		p2,p3 = f2, f3
	(p61) fcmp.unord.unc 		p2,p3 = f2, f3
	(p1) fcmp.unord.unc 		p20,p3 = f2, f3
	(p1) fcmp.unord.unc 		p2,p30 = f2, f3
	(p1) fcmp.unord.unc 		p2,p3 = f34, f3
	(p1) fcmp.unord.unc 		p2,p3 = f2, f35
	(p1) fcmp.ngt.unc 		p2,p3 = f2, f3
	(p61) fcmp.ngt.unc 		p2,p3 = f2, f3
	(p1) fcmp.ngt.unc 		p20,p3 = f2, f3
	(p1) fcmp.ngt.unc 		p2,p30 = f2, f3
	(p1) fcmp.ngt.unc 		p2,p3 = f34, f3
	(p1) fcmp.ngt.unc 		p2,p3 = f2, f35
	(p1) fcmp.neq.unc 		p2,p3 = f2, f3
	(p61) fcmp.neq.unc 		p2,p3 = f2, f3
	(p1) fcmp.neq.unc 		p20,p3 = f2, f3
	(p1) fcmp.neq.unc 		p2,p30 = f2, f3
	(p1) fcmp.neq.unc 		p2,p3 = f34, f3
	(p1) fcmp.neq.unc 		p2,p3 = f2, f35
	(p1) fcmp.nlt.unc 		p2,p3 = f2, f3
	(p61) fcmp.nlt.unc 		p2,p3 = f2, f3
	(p1) fcmp.nlt.unc 		p20,p3 = f2, f3
	(p1) fcmp.nlt.unc 		p2,p30 = f2, f3
	(p1) fcmp.nlt.unc 		p2,p3 = f34, f3
	(p1) fcmp.nlt.unc 		p2,p3 = f2, f35
	(p1) fcmp.nge.unc 		p2,p3 = f2, f3
	(p61) fcmp.nge.unc 		p2,p3 = f2, f3
	(p1) fcmp.nge.unc 		p20,p3 = f2, f3
	(p1) fcmp.nge.unc 		p2,p30 = f2, f3
	(p1) fcmp.nge.unc 		p2,p3 = f34, f3
	(p1) fcmp.nge.unc 		p2,p3 = f2, f35
	(p1) fcmp.nle.unc 		p2,p3 = f2, f3
	(p61) fcmp.nle.unc 		p2,p3 = f2, f3
	(p1) fcmp.nle.unc 		p20,p3 = f2, f3
	(p1) fcmp.nle.unc 		p2,p30 = f2, f3
	(p1) fcmp.nle.unc 		p2,p3 = f34, f3
	(p1) fcmp.nle.unc 		p2,p3 = f2, f35
	(p1) fcmp.ord.unc 		p2,p3 = f2, f3
	(p61) fcmp.ord.unc 		p2,p3 = f2, f3
	(p1) fcmp.ord.unc 		p20,p3 = f2, f3
	(p1) fcmp.ord.unc 		p2,p30 = f2, f3
	(p1) fcmp.ord.unc 		p2,p3 = f34, f3
	(p1) fcmp.ord.unc 		p2,p3 = f2, f35
	(p1) fcmp.gt.s0 		p2,p3 = f2, f3
	(p61) fcmp.gt.s0 		p2,p3 = f2, f3
	(p1) fcmp.gt.s0 		p20,p3 = f2, f3
	(p1) fcmp.gt.s0 		p2,p30 = f2, f3
	(p1) fcmp.gt.s0 		p2,p3 = f34, f3
	(p1) fcmp.gt.s0 		p2,p3 = f2, f35
	(p1) fcmp.eq.s0 		p2,p3 = f2, f3
	(p61) fcmp.eq.s0 		p2,p3 = f2, f3
	(p1) fcmp.eq.s0 		p20,p3 = f2, f3
	(p1) fcmp.eq.s0 		p2,p30 = f2, f3
	(p1) fcmp.eq.s0 		p2,p3 = f34, f3
	(p1) fcmp.eq.s0 		p2,p3 = f2, f35
	(p1) fcmp.lt.s0 		p2,p3 = f2, f3
	(p61) fcmp.lt.s0 		p2,p3 = f2, f3
	(p1) fcmp.lt.s0 		p20,p3 = f2, f3
	(p1) fcmp.lt.s0 		p2,p30 = f2, f3
	(p1) fcmp.lt.s0 		p2,p3 = f34, f3
	(p1) fcmp.lt.s0 		p2,p3 = f2, f35
	(p1) fcmp.ge.s0 		p2,p3 = f2, f3
	(p61) fcmp.ge.s0 		p2,p3 = f2, f3
	(p1) fcmp.ge.s0 		p20,p3 = f2, f3
	(p1) fcmp.ge.s0 		p2,p30 = f2, f3
	(p1) fcmp.ge.s0 		p2,p3 = f34, f3
	(p1) fcmp.ge.s0 		p2,p3 = f2, f35
	(p1) fcmp.le.s0 		p2,p3 = f2, f3
	(p61) fcmp.le.s0 		p2,p3 = f2, f3
	(p1) fcmp.le.s0 		p20,p3 = f2, f3
	(p1) fcmp.le.s0 		p2,p30 = f2, f3
	(p1) fcmp.le.s0 		p2,p3 = f34, f3
	(p1) fcmp.le.s0 		p2,p3 = f2, f35
	(p1) fcmp.unord.s0 		p2,p3 = f2, f3
	(p61) fcmp.unord.s0 		p2,p3 = f2, f3
	(p1) fcmp.unord.s0 		p20,p3 = f2, f3
	(p1) fcmp.unord.s0 		p2,p30 = f2, f3
	(p1) fcmp.unord.s0 		p2,p3 = f34, f3
	(p1) fcmp.unord.s0 		p2,p3 = f2, f35
	(p1) fcmp.ngt.s0 		p2,p3 = f2, f3
	(p61) fcmp.ngt.s0 		p2,p3 = f2, f3
	(p1) fcmp.ngt.s0 		p20,p3 = f2, f3
	(p1) fcmp.ngt.s0 		p2,p30 = f2, f3
	(p1) fcmp.ngt.s0 		p2,p3 = f34, f3
	(p1) fcmp.ngt.s0 		p2,p3 = f2, f35
	(p1) fcmp.neq.s0 		p2,p3 = f2, f3
	(p61) fcmp.neq.s0 		p2,p3 = f2, f3
	(p1) fcmp.neq.s0 		p20,p3 = f2, f3
	(p1) fcmp.neq.s0 		p2,p30 = f2, f3
	(p1) fcmp.neq.s0 		p2,p3 = f34, f3
	(p1) fcmp.neq.s0 		p2,p3 = f2, f35
	(p1) fcmp.nlt.s0 		p2,p3 = f2, f3
	(p61) fcmp.nlt.s0 		p2,p3 = f2, f3
	(p1) fcmp.nlt.s0 		p20,p3 = f2, f3
	(p1) fcmp.nlt.s0 		p2,p30 = f2, f3
	(p1) fcmp.nlt.s0 		p2,p3 = f34, f3
	(p1) fcmp.nlt.s0 		p2,p3 = f2, f35
	(p1) fcmp.nge.s0 		p2,p3 = f2, f3
	(p61) fcmp.nge.s0 		p2,p3 = f2, f3
	(p1) fcmp.nge.s0 		p20,p3 = f2, f3
	(p1) fcmp.nge.s0 		p2,p30 = f2, f3
	(p1) fcmp.nge.s0 		p2,p3 = f34, f3
	(p1) fcmp.nge.s0 		p2,p3 = f2, f35
	(p1) fcmp.nle.s0 		p2,p3 = f2, f3
	(p61) fcmp.nle.s0 		p2,p3 = f2, f3
	(p1) fcmp.nle.s0 		p20,p3 = f2, f3
	(p1) fcmp.nle.s0 		p2,p30 = f2, f3
	(p1) fcmp.nle.s0 		p2,p3 = f34, f3
	(p1) fcmp.nle.s0 		p2,p3 = f2, f35
	(p1) fcmp.ord.s0 		p2,p3 = f2, f3
	(p61) fcmp.ord.s0 		p2,p3 = f2, f3
	(p1) fcmp.ord.s0 		p20,p3 = f2, f3
	(p1) fcmp.ord.s0 		p2,p30 = f2, f3
	(p1) fcmp.ord.s0 		p2,p3 = f34, f3
	(p1) fcmp.ord.s0 		p2,p3 = f2, f35
	(p1) fcmp.gt.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.gt.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.gt.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.gt.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.gt.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.gt.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.eq.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.eq.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.eq.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.eq.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.eq.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.eq.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.lt.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.lt.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.lt.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.lt.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.lt.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.lt.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.ge.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.ge.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.ge.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.ge.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.ge.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.ge.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.le.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.le.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.le.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.le.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.le.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.le.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.unord.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.unord.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.unord.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.unord.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.unord.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.unord.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.ngt.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.ngt.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.ngt.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.ngt.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.ngt.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.ngt.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.neq.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.neq.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.neq.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.neq.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.neq.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.neq.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.nlt.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.nlt.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.nlt.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.nlt.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.nlt.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.nlt.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.nge.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.nge.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.nge.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.nge.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.nge.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.nge.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.nle.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.nle.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.nle.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.nle.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.nle.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.nle.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.ord.unc.s0 		p2,p3 = f2, f3
	(p61) fcmp.ord.unc.s0 		p2,p3 = f2, f3
	(p1) fcmp.ord.unc.s0 		p20,p3 = f2, f3
	(p1) fcmp.ord.unc.s0 		p2,p30 = f2, f3
	(p1) fcmp.ord.unc.s0 		p2,p3 = f34, f3
	(p1) fcmp.ord.unc.s0 		p2,p3 = f2, f35
	(p1) fcmp.gt.s1 		p2,p3 = f2, f3
	(p61) fcmp.gt.s1 		p2,p3 = f2, f3
	(p1) fcmp.gt.s1 		p20,p3 = f2, f3
	(p1) fcmp.gt.s1 		p2,p30 = f2, f3
	(p1) fcmp.gt.s1 		p2,p3 = f34, f3
	(p1) fcmp.gt.s1 		p2,p3 = f2, f35
	(p1) fcmp.eq.s1 		p2,p3 = f2, f3
	(p61) fcmp.eq.s1 		p2,p3 = f2, f3
	(p1) fcmp.eq.s1 		p20,p3 = f2, f3
	(p1) fcmp.eq.s1 		p2,p30 = f2, f3
	(p1) fcmp.eq.s1 		p2,p3 = f34, f3
	(p1) fcmp.eq.s1 		p2,p3 = f2, f35
	(p1) fcmp.lt.s1 		p2,p3 = f2, f3
	(p61) fcmp.lt.s1 		p2,p3 = f2, f3
	(p1) fcmp.lt.s1 		p20,p3 = f2, f3
	(p1) fcmp.lt.s1 		p2,p30 = f2, f3
	(p1) fcmp.lt.s1 		p2,p3 = f34, f3
	(p1) fcmp.lt.s1 		p2,p3 = f2, f35
	(p1) fcmp.ge.s1 		p2,p3 = f2, f3
	(p61) fcmp.ge.s1 		p2,p3 = f2, f3
	(p1) fcmp.ge.s1 		p20,p3 = f2, f3
	(p1) fcmp.ge.s1 		p2,p30 = f2, f3
	(p1) fcmp.ge.s1 		p2,p3 = f34, f3
	(p1) fcmp.ge.s1 		p2,p3 = f2, f35
	(p1) fcmp.le.s1 		p2,p3 = f2, f3
	(p61) fcmp.le.s1 		p2,p3 = f2, f3
	(p1) fcmp.le.s1 		p20,p3 = f2, f3
	(p1) fcmp.le.s1 		p2,p30 = f2, f3
	(p1) fcmp.le.s1 		p2,p3 = f34, f3
	(p1) fcmp.le.s1 		p2,p3 = f2, f35
	(p1) fcmp.unord.s1 		p2,p3 = f2, f3
	(p61) fcmp.unord.s1 		p2,p3 = f2, f3
	(p1) fcmp.unord.s1 		p20,p3 = f2, f3
	(p1) fcmp.unord.s1 		p2,p30 = f2, f3
	(p1) fcmp.unord.s1 		p2,p3 = f34, f3
	(p1) fcmp.unord.s1 		p2,p3 = f2, f35
	(p1) fcmp.ngt.s1 		p2,p3 = f2, f3
	(p61) fcmp.ngt.s1 		p2,p3 = f2, f3
	(p1) fcmp.ngt.s1 		p20,p3 = f2, f3
	(p1) fcmp.ngt.s1 		p2,p30 = f2, f3
	(p1) fcmp.ngt.s1 		p2,p3 = f34, f3
	(p1) fcmp.ngt.s1 		p2,p3 = f2, f35
	(p1) fcmp.neq.s1 		p2,p3 = f2, f3
	(p61) fcmp.neq.s1 		p2,p3 = f2, f3
	(p1) fcmp.neq.s1 		p20,p3 = f2, f3
	(p1) fcmp.neq.s1 		p2,p30 = f2, f3
	(p1) fcmp.neq.s1 		p2,p3 = f34, f3
	(p1) fcmp.neq.s1 		p2,p3 = f2, f35
	(p1) fcmp.nlt.s1 		p2,p3 = f2, f3
	(p61) fcmp.nlt.s1 		p2,p3 = f2, f3
	(p1) fcmp.nlt.s1 		p20,p3 = f2, f3
	(p1) fcmp.nlt.s1 		p2,p30 = f2, f3
	(p1) fcmp.nlt.s1 		p2,p3 = f34, f3
	(p1) fcmp.nlt.s1 		p2,p3 = f2, f35
	(p1) fcmp.nge.s1 		p2,p3 = f2, f3
	(p61) fcmp.nge.s1 		p2,p3 = f2, f3
	(p1) fcmp.nge.s1 		p20,p3 = f2, f3
	(p1) fcmp.nge.s1 		p2,p30 = f2, f3
	(p1) fcmp.nge.s1 		p2,p3 = f34, f3
	(p1) fcmp.nge.s1 		p2,p3 = f2, f35
	(p1) fcmp.nle.s1 		p2,p3 = f2, f3
	(p61) fcmp.nle.s1 		p2,p3 = f2, f3
	(p1) fcmp.nle.s1 		p20,p3 = f2, f3
	(p1) fcmp.nle.s1 		p2,p30 = f2, f3
	(p1) fcmp.nle.s1 		p2,p3 = f34, f3
	(p1) fcmp.nle.s1 		p2,p3 = f2, f35
	(p1) fcmp.ord.s1 		p2,p3 = f2, f3
	(p61) fcmp.ord.s1 		p2,p3 = f2, f3
	(p1) fcmp.ord.s1 		p20,p3 = f2, f3
	(p1) fcmp.ord.s1 		p2,p30 = f2, f3
	(p1) fcmp.ord.s1 		p2,p3 = f34, f3
	(p1) fcmp.ord.s1 		p2,p3 = f2, f35
	(p1) fcmp.gt.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.gt.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.gt.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.gt.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.gt.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.gt.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.eq.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.eq.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.eq.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.eq.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.eq.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.eq.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.lt.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.lt.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.lt.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.lt.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.lt.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.lt.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.ge.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.ge.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.ge.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.ge.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.ge.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.ge.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.le.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.le.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.le.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.le.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.le.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.le.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.unord.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.unord.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.unord.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.unord.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.unord.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.unord.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.ngt.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.ngt.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.ngt.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.ngt.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.ngt.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.ngt.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.neq.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.neq.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.neq.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.neq.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.neq.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.neq.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.nlt.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.nlt.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.nlt.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.nlt.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.nlt.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.nlt.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.nge.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.nge.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.nge.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.nge.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.nge.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.nge.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.nle.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.nle.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.nle.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.nle.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.nle.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.nle.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.ord.unc.s1 		p2,p3 = f2, f3
	(p61) fcmp.ord.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.ord.unc.s1 		p20,p3 = f2, f3
	(p1) fcmp.ord.unc.s1 		p2,p30 = f2, f3
	(p1) fcmp.ord.unc.s1 		p2,p3 = f34, f3
	(p1) fcmp.ord.unc.s1 		p2,p3 = f2, f35
	(p1) fcmp.gt.s2 		p2,p3 = f2, f3
	(p61) fcmp.gt.s2 		p2,p3 = f2, f3
	(p1) fcmp.gt.s2 		p20,p3 = f2, f3
	(p1) fcmp.gt.s2 		p2,p30 = f2, f3
	(p1) fcmp.gt.s2 		p2,p3 = f34, f3
	(p1) fcmp.gt.s2 		p2,p3 = f2, f35
	(p1) fcmp.eq.s2 		p2,p3 = f2, f3
	(p61) fcmp.eq.s2 		p2,p3 = f2, f3
	(p1) fcmp.eq.s2 		p20,p3 = f2, f3
	(p1) fcmp.eq.s2 		p2,p30 = f2, f3
	(p1) fcmp.eq.s2 		p2,p3 = f34, f3
	(p1) fcmp.eq.s2 		p2,p3 = f2, f35
	(p1) fcmp.lt.s2 		p2,p3 = f2, f3
	(p61) fcmp.lt.s2 		p2,p3 = f2, f3
	(p1) fcmp.lt.s2 		p20,p3 = f2, f3
	(p1) fcmp.lt.s2 		p2,p30 = f2, f3
	(p1) fcmp.lt.s2 		p2,p3 = f34, f3
	(p1) fcmp.lt.s2 		p2,p3 = f2, f35
	(p1) fcmp.ge.s2 		p2,p3 = f2, f3
	(p61) fcmp.ge.s2 		p2,p3 = f2, f3
	(p1) fcmp.ge.s2 		p20,p3 = f2, f3
	(p1) fcmp.ge.s2 		p2,p30 = f2, f3
	(p1) fcmp.ge.s2 		p2,p3 = f34, f3
	(p1) fcmp.ge.s2 		p2,p3 = f2, f35
	(p1) fcmp.le.s2 		p2,p3 = f2, f3
	(p61) fcmp.le.s2 		p2,p3 = f2, f3
	(p1) fcmp.le.s2 		p20,p3 = f2, f3
	(p1) fcmp.le.s2 		p2,p30 = f2, f3
	(p1) fcmp.le.s2 		p2,p3 = f34, f3
	(p1) fcmp.le.s2 		p2,p3 = f2, f35
	(p1) fcmp.unord.s2 		p2,p3 = f2, f3
	(p61) fcmp.unord.s2 		p2,p3 = f2, f3
	(p1) fcmp.unord.s2 		p20,p3 = f2, f3
	(p1) fcmp.unord.s2 		p2,p30 = f2, f3
	(p1) fcmp.unord.s2 		p2,p3 = f34, f3
	(p1) fcmp.unord.s2 		p2,p3 = f2, f35
	(p1) fcmp.ngt.s2 		p2,p3 = f2, f3
	(p61) fcmp.ngt.s2 		p2,p3 = f2, f3
	(p1) fcmp.ngt.s2 		p20,p3 = f2, f3
	(p1) fcmp.ngt.s2 		p2,p30 = f2, f3
	(p1) fcmp.ngt.s2 		p2,p3 = f34, f3
	(p1) fcmp.ngt.s2 		p2,p3 = f2, f35
	(p1) fcmp.neq.s2 		p2,p3 = f2, f3
	(p61) fcmp.neq.s2 		p2,p3 = f2, f3
	(p1) fcmp.neq.s2 		p20,p3 = f2, f3
	(p1) fcmp.neq.s2 		p2,p30 = f2, f3
	(p1) fcmp.neq.s2 		p2,p3 = f34, f3
	(p1) fcmp.neq.s2 		p2,p3 = f2, f35
	(p1) fcmp.nlt.s2 		p2,p3 = f2, f3
	(p61) fcmp.nlt.s2 		p2,p3 = f2, f3
	(p1) fcmp.nlt.s2 		p20,p3 = f2, f3
	(p1) fcmp.nlt.s2 		p2,p30 = f2, f3
	(p1) fcmp.nlt.s2 		p2,p3 = f34, f3
	(p1) fcmp.nlt.s2 		p2,p3 = f2, f35
	(p1) fcmp.nge.s2 		p2,p3 = f2, f3
	(p61) fcmp.nge.s2 		p2,p3 = f2, f3
	(p1) fcmp.nge.s2 		p20,p3 = f2, f3
	(p1) fcmp.nge.s2 		p2,p30 = f2, f3
	(p1) fcmp.nge.s2 		p2,p3 = f34, f3
	(p1) fcmp.nge.s2 		p2,p3 = f2, f35
	(p1) fcmp.nle.s2 		p2,p3 = f2, f3
	(p61) fcmp.nle.s2 		p2,p3 = f2, f3
	(p1) fcmp.nle.s2 		p20,p3 = f2, f3
	(p1) fcmp.nle.s2 		p2,p30 = f2, f3
	(p1) fcmp.nle.s2 		p2,p3 = f34, f3
	(p1) fcmp.nle.s2 		p2,p3 = f2, f35
	(p1) fcmp.ord.s2 		p2,p3 = f2, f3
	(p61) fcmp.ord.s2 		p2,p3 = f2, f3
	(p1) fcmp.ord.s2 		p20,p3 = f2, f3
	(p1) fcmp.ord.s2 		p2,p30 = f2, f3
	(p1) fcmp.ord.s2 		p2,p3 = f34, f3
	(p1) fcmp.ord.s2 		p2,p3 = f2, f35
	(p1) fcmp.gt.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.gt.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.gt.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.gt.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.gt.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.gt.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.eq.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.eq.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.eq.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.eq.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.eq.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.eq.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.lt.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.lt.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.lt.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.lt.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.lt.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.lt.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.ge.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.ge.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.ge.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.ge.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.ge.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.ge.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.le.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.le.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.le.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.le.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.le.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.le.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.unord.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.unord.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.unord.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.unord.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.unord.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.unord.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.ngt.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.ngt.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.ngt.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.ngt.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.ngt.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.ngt.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.neq.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.neq.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.neq.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.neq.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.neq.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.neq.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.nlt.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.nlt.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.nlt.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.nlt.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.nlt.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.nlt.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.nge.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.nge.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.nge.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.nge.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.nge.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.nge.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.nle.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.nle.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.nle.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.nle.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.nle.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.nle.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.ord.unc.s2 		p2,p3 = f2, f3
	(p61) fcmp.ord.unc.s2 		p2,p3 = f2, f3
	(p1) fcmp.ord.unc.s2 		p20,p3 = f2, f3
	(p1) fcmp.ord.unc.s2 		p2,p30 = f2, f3
	(p1) fcmp.ord.unc.s2 		p2,p3 = f34, f3
	(p1) fcmp.ord.unc.s2 		p2,p3 = f2, f35
	(p1) fcmp.gt.s3 		p2,p3 = f2, f3
	(p61) fcmp.gt.s3 		p2,p3 = f2, f3
	(p1) fcmp.gt.s3 		p20,p3 = f2, f3
	(p1) fcmp.gt.s3 		p2,p30 = f2, f3
	(p1) fcmp.gt.s3 		p2,p3 = f34, f3
	(p1) fcmp.gt.s3 		p2,p3 = f2, f35
	(p1) fcmp.eq.s3 		p2,p3 = f2, f3
	(p61) fcmp.eq.s3 		p2,p3 = f2, f3
	(p1) fcmp.eq.s3 		p20,p3 = f2, f3
	(p1) fcmp.eq.s3 		p2,p30 = f2, f3
	(p1) fcmp.eq.s3 		p2,p3 = f34, f3
	(p1) fcmp.eq.s3 		p2,p3 = f2, f35
	(p1) fcmp.lt.s3 		p2,p3 = f2, f3
	(p61) fcmp.lt.s3 		p2,p3 = f2, f3
	(p1) fcmp.lt.s3 		p20,p3 = f2, f3
	(p1) fcmp.lt.s3 		p2,p30 = f2, f3
	(p1) fcmp.lt.s3 		p2,p3 = f34, f3
	(p1) fcmp.lt.s3 		p2,p3 = f2, f35
	(p1) fcmp.ge.s3 		p2,p3 = f2, f3
	(p61) fcmp.ge.s3 		p2,p3 = f2, f3
	(p1) fcmp.ge.s3 		p20,p3 = f2, f3
	(p1) fcmp.ge.s3 		p2,p30 = f2, f3
	(p1) fcmp.ge.s3 		p2,p3 = f34, f3
	(p1) fcmp.ge.s3 		p2,p3 = f2, f35
	(p1) fcmp.le.s3 		p2,p3 = f2, f3
	(p61) fcmp.le.s3 		p2,p3 = f2, f3
	(p1) fcmp.le.s3 		p20,p3 = f2, f3
	(p1) fcmp.le.s3 		p2,p30 = f2, f3
	(p1) fcmp.le.s3 		p2,p3 = f34, f3
	(p1) fcmp.le.s3 		p2,p3 = f2, f35
	(p1) fcmp.unord.s3 		p2,p3 = f2, f3
	(p61) fcmp.unord.s3 		p2,p3 = f2, f3
	(p1) fcmp.unord.s3 		p20,p3 = f2, f3
	(p1) fcmp.unord.s3 		p2,p30 = f2, f3
	(p1) fcmp.unord.s3 		p2,p3 = f34, f3
	(p1) fcmp.unord.s3 		p2,p3 = f2, f35
	(p1) fcmp.ngt.s3 		p2,p3 = f2, f3
	(p61) fcmp.ngt.s3 		p2,p3 = f2, f3
	(p1) fcmp.ngt.s3 		p20,p3 = f2, f3
	(p1) fcmp.ngt.s3 		p2,p30 = f2, f3
	(p1) fcmp.ngt.s3 		p2,p3 = f34, f3
	(p1) fcmp.ngt.s3 		p2,p3 = f2, f35
	(p1) fcmp.neq.s3 		p2,p3 = f2, f3
	(p61) fcmp.neq.s3 		p2,p3 = f2, f3
	(p1) fcmp.neq.s3 		p20,p3 = f2, f3
	(p1) fcmp.neq.s3 		p2,p30 = f2, f3
	(p1) fcmp.neq.s3 		p2,p3 = f34, f3
	(p1) fcmp.neq.s3 		p2,p3 = f2, f35
	(p1) fcmp.nlt.s3 		p2,p3 = f2, f3
	(p61) fcmp.nlt.s3 		p2,p3 = f2, f3
	(p1) fcmp.nlt.s3 		p20,p3 = f2, f3
	(p1) fcmp.nlt.s3 		p2,p30 = f2, f3
	(p1) fcmp.nlt.s3 		p2,p3 = f34, f3
	(p1) fcmp.nlt.s3 		p2,p3 = f2, f35
	(p1) fcmp.nge.s3 		p2,p3 = f2, f3
	(p61) fcmp.nge.s3 		p2,p3 = f2, f3
	(p1) fcmp.nge.s3 		p20,p3 = f2, f3
	(p1) fcmp.nge.s3 		p2,p30 = f2, f3
	(p1) fcmp.nge.s3 		p2,p3 = f34, f3
	(p1) fcmp.nge.s3 		p2,p3 = f2, f35
	(p1) fcmp.nle.s3 		p2,p3 = f2, f3
	(p61) fcmp.nle.s3 		p2,p3 = f2, f3
	(p1) fcmp.nle.s3 		p20,p3 = f2, f3
	(p1) fcmp.nle.s3 		p2,p30 = f2, f3
	(p1) fcmp.nle.s3 		p2,p3 = f34, f3
	(p1) fcmp.nle.s3 		p2,p3 = f2, f35
	(p1) fcmp.ord.s3 		p2,p3 = f2, f3
	(p61) fcmp.ord.s3 		p2,p3 = f2, f3
	(p1) fcmp.ord.s3 		p20,p3 = f2, f3
	(p1) fcmp.ord.s3 		p2,p30 = f2, f3
	(p1) fcmp.ord.s3 		p2,p3 = f34, f3
	(p1) fcmp.ord.s3 		p2,p3 = f2, f35
	(p1) fcmp.gt.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.gt.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.gt.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.gt.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.gt.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.gt.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.eq.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.eq.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.eq.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.eq.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.eq.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.eq.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.lt.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.lt.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.lt.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.lt.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.lt.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.lt.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.ge.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.ge.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.ge.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.ge.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.ge.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.ge.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.le.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.le.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.le.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.le.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.le.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.le.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.unord.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.unord.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.unord.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.unord.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.unord.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.unord.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.ngt.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.ngt.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.ngt.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.ngt.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.ngt.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.ngt.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.neq.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.neq.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.neq.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.neq.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.neq.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.neq.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.nlt.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.nlt.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.nlt.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.nlt.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.nlt.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.nlt.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.nge.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.nge.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.nge.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.nge.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.nge.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.nge.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.nle.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.nle.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.nle.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.nle.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.nle.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.nle.unc.s3 		p2,p3 = f2, f35
	(p1) fcmp.ord.unc.s3 		p2,p3 = f2, f3
	(p61) fcmp.ord.unc.s3 		p2,p3 = f2, f3
	(p1) fcmp.ord.unc.s3 		p20,p3 = f2, f3
	(p1) fcmp.ord.unc.s3 		p2,p30 = f2, f3
	(p1) fcmp.ord.unc.s3 		p2,p3 = f34, f3
	(p1) fcmp.ord.unc.s3 		p2,p3 = f2, f35

	(p2) fcvt.fx		f11 = f3
	(p62) fcvt.fx		f11 = f3
	(p2) fcvt.fx		f110 = f3
	(p2) fcvt.fx		f11 = f35
	(p3) fcvt.fx.trunc	f11 = f3
	(p63) fcvt.fx.trunc	f11 = f3
	(p3) fcvt.fx.trunc	f110 = f3
	(p3) fcvt.fx.trunc	f11 = f35
	(p2) fcvt.fx.s0		f11 = f3
	(p62) fcvt.fx.s0	f11 = f3
	(p2) fcvt.fx.s0		f110 = f3
	(p2) fcvt.fx.s0		f11 = f35
	(p3) fcvt.fx.trunc.s0	f11 = f3
	(p63) fcvt.fx.trunc.s0	f11 = f3
	(p3) fcvt.fx.trunc.s0	f110 = f3
	(p3) fcvt.fx.trunc.s0	f11 = f35
	(p2) fcvt.fx.s1		f11 = f3
	(p62) fcvt.fx.s1	f11 = f3
	(p2) fcvt.fx.s1		f110 = f3
	(p2) fcvt.fx.s1		f11 = f35
	(p3) fcvt.fx.trunc.s1	f11 = f3
	(p63) fcvt.fx.trunc.s1	f11 = f3
	(p3) fcvt.fx.trunc.s1	f110 = f3
	(p3) fcvt.fx.trunc.s1	f11 = f35
	(p2) fcvt.fx.s2		f11 = f3
	(p62) fcvt.fx.s2	f11 = f3
	(p2) fcvt.fx.s2		f110 = f3
	(p2) fcvt.fx.s2		f11 = f35
	(p3) fcvt.fx.trunc.s2	f11 = f3
	(p63) fcvt.fx.trunc.s2	f11 = f3
	(p3) fcvt.fx.trunc.s2	f110 = f3
	(p3) fcvt.fx.trunc.s2	f11 = f35
	(p2) fcvt.fx.s3		f11 = f3
	(p62) fcvt.fx.s3	f11 = f3
	(p2) fcvt.fx.s3		f110 = f3
	(p2) fcvt.fx.s3		f11 = f35
	(p3) fcvt.fx.trunc.s3	f11 = f3
	(p63) fcvt.fx.trunc.s3	f11 = f3
	(p3) fcvt.fx.trunc.s3	f110 = f3
	(p3) fcvt.fx.trunc.s3	f11 = f35
	(p2) fcvt.fxu		f11 = f3
	(p62) fcvt.fxu		f11 = f3
	(p2) fcvt.fxu		f110 = f3
	(p2) fcvt.fxu		f11 = f35
	(p3) fcvt.fxu.trunc	f11 = f3
	(p63) fcvt.fxu.trunc	f11 = f3
	(p3) fcvt.fxu.trunc	f110 = f3
	(p3) fcvt.fxu.trunc	f11 = f35
	(p2) fcvt.fxu.s0		f11 = f3
	(p62) fcvt.fxu.s0	f11 = f3
	(p2) fcvt.fxu.s0		f110 = f3
	(p2) fcvt.fxu.s0		f11 = f35
	(p3) fcvt.fxu.trunc.s0	f11 = f3
	(p63) fcvt.fxu.trunc.s0	f11 = f3
	(p3) fcvt.fxu.trunc.s0	f110 = f3
	(p3) fcvt.fxu.trunc.s0	f11 = f35
	(p2) fcvt.fxu.s1		f11 = f3
	(p62) fcvt.fxu.s1	f11 = f3
	(p2) fcvt.fxu.s1		f110 = f3
	(p2) fcvt.fxu.s1		f11 = f35
	(p3) fcvt.fxu.trunc.s1	f11 = f3
	(p63) fcvt.fxu.trunc.s1	f11 = f3
	(p3) fcvt.fxu.trunc.s1	f110 = f3
	(p3) fcvt.fxu.trunc.s1	f11 = f35
	(p2) fcvt.fxu.s2		f11 = f3
	(p62) fcvt.fxu.s2	f11 = f3
	(p2) fcvt.fxu.s2		f110 = f3
	(p2) fcvt.fxu.s2		f11 = f35
	(p3) fcvt.fxu.trunc.s2	f11 = f3
	(p63) fcvt.fxu.trunc.s2	f11 = f3
	(p3) fcvt.fxu.trunc.s2	f110 = f3
	(p3) fcvt.fxu.trunc.s2	f11 = f35
	(p2) fcvt.fxu.s3		f11 = f3
	(p62) fcvt.fxu.s3	f11 = f3
	(p2) fcvt.fxu.s3		f110 = f3
	(p2) fcvt.fxu.s3		f11 = f35
	(p3) fcvt.fxu.trunc.s3	f11 = f3
	(p63) fcvt.fxu.trunc.s3	f11 = f3
	(p3) fcvt.fxu.trunc.s3	f110 = f3
	(p3) fcvt.fxu.trunc.s3	f11 = f35

	(p8) fcvt.xf		f11 = f3
	(p18) fcvt.xf		f11 = f3
	(p8) fcvt.xf		f110 = f3
	(p8) fcvt.xf		f11 = f35

	(p2) fcvt.xuf	f11 = f3
	(p62) fcvt.xuf	f11 = f3
	(p2) fcvt.xuf	f110 = f3
	(p2) fcvt.xuf	f11 = f35
	(p3) fcvt.xuf.s	f11 = f3
	(p63) fcvt.xuf.s	f11 = f3
	(p3) fcvt.xuf.s	f110 = f3
	(p3) fcvt.xuf.s	f11 = f35
	(p3) fcvt.xuf.d	f11 = f3
	(p63) fcvt.xuf.d	f11 = f3
	(p3) fcvt.xuf.d	f110 = f3
	(p3) fcvt.xuf.d	f11 = f35

	(p2) fcvt.xuf.s0	f11 = f3
	(p62) fcvt.xuf.s0	f11 = f3
	(p2) fcvt.xuf.s0	f110 = f3
	(p2) fcvt.xuf.s0	f11 = f35
	(p3) fcvt.xuf.s.s0	f11 = f3
	(p63) fcvt.xuf.s.s0	f11 = f3
	(p3) fcvt.xuf.s.s0	f110 = f3
	(p3) fcvt.xuf.s.s0	f11 = f35
	(p3) fcvt.xuf.d.s0	f11 = f3
	(p63) fcvt.xuf.d.s0	f11 = f3
	(p3) fcvt.xuf.d.s0	f110 = f3
	(p3) fcvt.xuf.d.s0	f11 = f35

	(p2) fcvt.xuf.s1	f11 = f3
	(p62) fcvt.xuf.s1	f11 = f3
	(p2) fcvt.xuf.s1	f110 = f3
	(p2) fcvt.xuf.s1	f11 = f35
	(p3) fcvt.xuf.s.s1	f11 = f3
	(p63) fcvt.xuf.s.s1	f11 = f3
	(p3) fcvt.xuf.s.s1	f110 = f3
	(p3) fcvt.xuf.s.s1	f11 = f35
	(p3) fcvt.xuf.d.s1	f11 = f3
	(p63) fcvt.xuf.d.s1	f11 = f3
	(p3) fcvt.xuf.d.s1	f110 = f3
	(p3) fcvt.xuf.d.s1	f11 = f35

	(p2) fcvt.xuf.s2	f11 = f3
	(p62) fcvt.xuf.s2	f11 = f3
	(p2) fcvt.xuf.s2	f110 = f3
	(p2) fcvt.xuf.s2	f11 = f35
	(p3) fcvt.xuf.s.s2	f11 = f3
	(p63) fcvt.xuf.s.s2	f11 = f3
	(p3) fcvt.xuf.s.s2	f110 = f3
	(p3) fcvt.xuf.s.s2	f11 = f35
	(p3) fcvt.xuf.d.s2	f11 = f3
	(p63) fcvt.xuf.d.s2	f11 = f3
	(p3) fcvt.xuf.d.s2	f110 = f3
	(p3) fcvt.xuf.d.s2	f11 = f35

	(p2) fcvt.xuf.s3	f11 = f3
	(p62) fcvt.xuf.s3	f11 = f3
	(p2) fcvt.xuf.s3	f110 = f3
	(p2) fcvt.xuf.s3	f11 = f35
	(p3) fcvt.xuf.s.s3	f11 = f3
	(p63) fcvt.xuf.s.s3	f11 = f3
	(p3) fcvt.xuf.s.s3	f110 = f3
	(p3) fcvt.xuf.s.s3	f11 = f35
	(p3) fcvt.xuf.d.s3	f11 = f3
	(p63) fcvt.xuf.d.s3	f11 = f3
	(p3) fcvt.xuf.d.s3	f110 = f3
	(p3) fcvt.xuf.d.s3	f11 = f35

	(p4) fetchadd4.acq	r1 = [r3], +8 
	(p34) fetchadd4.acq	r1 = [r3], +8 
	(p4) fetchadd4.acq	r100 = [r3], +8 
	(p4) fetchadd4.acq	r1 = [r35], +8 
	(p5) fetchadd4.acq.nt1	r1 = [r3], +8 
	(p35) fetchadd4.acq.nt1	r1 = [r3], +8 
	(p5) fetchadd4.acq.nt1	r100 = [r3], +8 
	(p5) fetchadd4.acq.nt1	r1 = [r35], +8 
	(p6) fetchadd4.acq.nta	r1 = [r3], +8 
	(p36) fetchadd4.acq.nta	r1 = [r3], +8 
	(p6) fetchadd4.acq.nta	r100 = [r3], +8 
	(p6) fetchadd4.acq.nta	r1 = [r35], +8 
	(p7) fetchadd4.rel	r1 = [r3], +8 
	(p37) fetchadd4.rel	r1 = [r3], +8 
	(p7) fetchadd4.rel	r100 = [r3], +8 
	(p7) fetchadd4.rel	r1 = [r35], +8 
	(p8) fetchadd4.rel.nt1	r1 = [r3], +8 
	(p38) fetchadd4.rel.nt1	r1 = [r3], +8 
	(p8) fetchadd4.rel.nt1	r100 = [r3], +8 
	(p8) fetchadd4.rel.nt1	r1 = [r35], +8 
	(p9) fetchadd4.rel.nta	r1 = [r3], +8 
	(p39) fetchadd4.rel.nta	r1 = [r3], +8 
	(p9) fetchadd4.rel.nta	r100 = [r3], +8 
	(p9) fetchadd4.rel.nta	r1 = [r35], +8 
	(p4) fetchadd8.acq	r1 = [r3], +8 
	(p34) fetchadd8.acq	r1 = [r3], +8 
	(p4) fetchadd8.acq	r100 = [r3], +8 
	(p4) fetchadd8.acq	r1 = [r35], +8 
	(p5) fetchadd8.acq.nt1	r1 = [r3], +8 
	(p35) fetchadd8.acq.nt1	r1 = [r3], +8 
	(p5) fetchadd8.acq.nt1	r100 = [r3], +8 
	(p5) fetchadd8.acq.nt1	r1 = [r35], +8 
	(p6) fetchadd8.acq.nta	r1 = [r3], +8 
	(p36) fetchadd8.acq.nta	r1 = [r3], +8 
	(p6) fetchadd8.acq.nta	r100 = [r3], +8 
	(p6) fetchadd8.acq.nta	r1 = [r35], +8 
	(p7) fetchadd8.rel	r1 = [r3], +8 
	(p37) fetchadd8.rel	r1 = [r3], +8 
	(p7) fetchadd8.rel	r100 = [r3], +8 
	(p7) fetchadd8.rel	r1 = [r35], +8 
	(p8) fetchadd8.rel.nt1	r1 = [r3], +8 
	(p38) fetchadd8.rel.nt1	r1 = [r3], +8 
	(p8) fetchadd8.rel.nt1	r100 = [r3], +8 
	(p8) fetchadd8.rel.nt1	r1 = [r35], +8 
	(p9) fetchadd8.rel.nta	r1 = [r3], +8 
	(p39) fetchadd8.rel.nta	r1 = [r3], +8 
	(p9) fetchadd8.rel.nta	r100 = [r3], +8 
	(p9) fetchadd8.rel.nta	r1 = [r35], +8 

	flushrs

	(p1) fma f10 = f3, f4, f5 
	(p17) fma f10 = f3, f4, f5 
	(p1) fma f100 = f3, f4, f5 
	(p1) fma f10 = f35, f4, f5 
	(p1) fma f10 = f3, f40, f5 
	(p1) fma f10 = f3, f4, f50 
	(p1) fma.s f10 = f3, f4, f5 
	(p17) fma.s f10 = f3, f4, f5 
	(p1) fma.s f100 = f3, f4, f5 
	(p1) fma.s f10 = f35, f4, f5 
	(p1) fma.s f10 = f3, f40, f5 
	(p1) fma.s f10 = f3, f4, f50 
	(p1) fma.d f10 = f3, f4, f5 
	(p17) fma.d f10 = f3, f4, f5 
	(p1) fma.d f100 = f3, f4, f5 
	(p1) fma.d f10 = f35, f4, f5 
	(p1) fma.d f10 = f3, f40, f5 
	(p1) fma.d f10 = f3, f4, f50 
	(p1) fma.s0 f10 = f3, f4, f5 
	(p17) fma.s0 f10 = f3, f4, f5 
	(p1) fma.s0 f100 = f3, f4, f5 
	(p1) fma.s0 f10 = f35, f4, f5 
	(p1) fma.s0 f10 = f3, f40, f5 
	(p1) fma.s0 f10 = f3, f4, f50 
	(p1) fma.s.s0 f10 = f3, f4, f5 
	(p17) fma.s.s0 f10 = f3, f4, f5 
	(p1) fma.s.s0 f100 = f3, f4, f5 
	(p1) fma.s.s0 f10 = f35, f4, f5 
	(p1) fma.s.s0 f10 = f3, f40, f5 
	(p1) fma.s.s0 f10 = f3, f4, f50 
	(p1) fma.d.s0 f10 = f3, f4, f5 
	(p17) fma.d.s0 f10 = f3, f4, f5 
	(p1) fma.d.s0 f100 = f3, f4, f5 
	(p1) fma.d.s0 f10 = f35, f4, f5 
	(p1) fma.d.s0 f10 = f3, f40, f5 
	(p1) fma.d.s0 f10 = f3, f4, f50 
	(p1) fma.s1 f10 = f3, f4, f5 
	(p17) fma.s1 f10 = f3, f4, f5 
	(p1) fma.s1 f100 = f3, f4, f5 
	(p1) fma.s1 f10 = f35, f4, f5 
	(p1) fma.s1 f10 = f3, f40, f5 
	(p1) fma.s1 f10 = f3, f4, f50 
	(p1) fma.s.s1 f10 = f3, f4, f5 
	(p17) fma.s.s1 f10 = f3, f4, f5 
	(p1) fma.s.s1 f100 = f3, f4, f5 
	(p1) fma.s.s1 f10 = f35, f4, f5 
	(p1) fma.s.s1 f10 = f3, f40, f5 
	(p1) fma.s.s1 f10 = f3, f4, f50 
	(p1) fma.d.s1 f10 = f3, f4, f5 
	(p17) fma.d.s1 f10 = f3, f4, f5 
	(p1) fma.d.s1 f100 = f3, f4, f5 
	(p1) fma.d.s1 f10 = f35, f4, f5 
	(p1) fma.d.s1 f10 = f3, f40, f5 
	(p1) fma.d.s1 f10 = f3, f4, f50 
	(p1) fma.s2 f10 = f3, f4, f5 
	(p17) fma.s2 f10 = f3, f4, f5 
	(p1) fma.s2 f100 = f3, f4, f5 
	(p1) fma.s2 f10 = f35, f4, f5 
	(p1) fma.s2 f10 = f3, f40, f5 
	(p1) fma.s2 f10 = f3, f4, f50 
	(p1) fma.s.s2 f10 = f3, f4, f5 
	(p17) fma.s.s2 f10 = f3, f4, f5 
	(p1) fma.s.s2 f100 = f3, f4, f5 
	(p1) fma.s.s2 f10 = f35, f4, f5 
	(p1) fma.s.s2 f10 = f3, f40, f5 
	(p1) fma.s.s2 f10 = f3, f4, f50 
	(p1) fma.d.s2 f10 = f3, f4, f5 
	(p17) fma.d.s2 f10 = f3, f4, f5 
	(p1) fma.d.s2 f100 = f3, f4, f5 
	(p1) fma.d.s2 f10 = f35, f4, f5 
	(p1) fma.d.s2 f10 = f3, f40, f5 
	(p1) fma.d.s2 f10 = f3, f4, f50 
	(p1) fma.s3 f10 = f3, f4, f5 
	(p17) fma.s3 f10 = f3, f4, f5 
	(p1) fma.s3 f100 = f3, f4, f5 
	(p1) fma.s3 f10 = f35, f4, f5 
	(p1) fma.s3 f10 = f3, f40, f5 
	(p1) fma.s3 f10 = f3, f4, f50 
	(p1) fma.s.s3 f10 = f3, f4, f5 
	(p17) fma.s.s3 f10 = f3, f4, f5 
	(p1) fma.s.s3 f100 = f3, f4, f5 
	(p1) fma.s.s3 f10 = f35, f4, f5 
	(p1) fma.s.s3 f10 = f3, f40, f5 
	(p1) fma.s.s3 f10 = f3, f4, f50 
	(p1) fma.d.s3 f10 = f3, f4, f5 
	(p17) fma.d.s3 f10 = f3, f4, f5 
	(p1) fma.d.s3 f100 = f3, f4, f5 
	(p1) fma.d.s3 f10 = f35, f4, f5 
	(p1) fma.d.s3 f10 = f3, f40, f5 
	(p1) fma.d.s3 f10 = f3, f4, f50 

	(p2) fmax f10=f3,f4 
	(p20) fmax f10=f3,f4 
	(p2) fmax f100=f3,f4 
	(p2) fmax f10=f35,f4 
	(p2) fmax f10=f3,f40 
	(p3) fmax.s0 f10=f3,f4 
	(p30) fmax.s0 f10=f3,f4 
	(p3) fmax.s0 f100=f3,f4 
	(p3) fmax.s0 f10=f35,f4 
	(p3) fmax.s0 f10=f3,f40 
	(p3) fmax.s1 f10=f3,f4 
	(p30) fmax.s1 f10=f3,f4 
	(p3) fmax.s1 f100=f3,f4 
	(p3) fmax.s1 f10=f35,f4 
	(p3) fmax.s1 f10=f3,f40 
	(p3) fmax.s2 f10=f3,f4 
	(p30) fmax.s2 f10=f3,f4 
	(p3) fmax.s2 f100=f3,f4 
	(p3) fmax.s2 f10=f35,f4 
	(p3) fmax.s2 f10=f3,f40 
	(p3) fmax.s3 f10=f3,f4 
	(p30) fmax.s3 f10=f3,f4 
	(p3) fmax.s3 f100=f3,f4 
	(p3) fmax.s3 f10=f35,f4 
	(p3) fmax.s3 f10=f3,f40 

	(p10) fmerge.ns		f11 = f3, f4
	(p18) fmerge.ns		f11 = f3, f4
	(p10) fmerge.ns		f110 = f3, f4
	(p10) fmerge.ns		f11 = f35, f4
	(p10) fmerge.ns		f11 = f3, f40
	(p10) fmerge.s		f11 = f3, f4
	(p18) fmerge.s		f11 = f3, f4
	(p10) fmerge.s		f110 = f3, f4
	(p10) fmerge.s		f11 = f35, f4
	(p10) fmerge.s		f11 = f3, f40
	(p10) fmerge.se		f11 = f3, f4
	(p18) fmerge.se		f11 = f3, f4
	(p10) fmerge.se		f110 = f3, f4
	(p10) fmerge.se		f11 = f35, f4
	(p10) fmerge.se		f11 = f3, f40

	(p2) fmin f10=f3,f4 
	(p20) fmin f10=f3,f4 
	(p2) fmin f100=f3,f4 
	(p2) fmin f10=f35,f4 
	(p2) fmin f10=f3,f40 
	(p3) fmin.s0 f10=f3,f4 
	(p30) fmin.s0 f10=f3,f4 
	(p3) fmin.s0 f100=f3,f4 
	(p3) fmin.s0 f10=f35,f4 
	(p3) fmin.s0 f10=f3,f40 
	(p3) fmin.s1 f10=f3,f4 
	(p30) fmin.s1 f10=f3,f4 
	(p3) fmin.s1 f100=f3,f4 
	(p3) fmin.s1 f10=f35,f4 
	(p3) fmin.s1 f10=f3,f40 
	(p3) fmin.s2 f10=f3,f4 
	(p30) fmin.s2 f10=f3,f4 
	(p3) fmin.s2 f100=f3,f4 
	(p3) fmin.s2 f10=f35,f4 
	(p3) fmin.s2 f10=f3,f40 
	(p3) fmin.s3 f10=f3,f4 
	(p30) fmin.s3 f10=f3,f4 
	(p3) fmin.s3 f100=f3,f4 
	(p3) fmin.s3 f10=f35,f4 
	(p3) fmin.s3 f10=f3,f40 

	(p10) fmix.l		f11 = f3, f4
	(p18) fmix.l		f11 = f3, f4
	(p10) fmix.l		f110 = f3, f4
	(p10) fmix.l		f11 = f35, f4
	(p10) fmix.l		f11 = f3, f40
	(p10) fmix.r		f11 = f3, f4
	(p18) fmix.r		f11 = f3, f4
	(p10) fmix.r		f110 = f3, f4
	(p10) fmix.r		f11 = f35, f4
	(p10) fmix.r		f11 = f3, f40
	(p10) fmix.lr		f11 = f3, f4
	(p18) fmix.lr		f11 = f3, f4
	(p10) fmix.lr		f110 = f3, f4
	(p10) fmix.lr		f11 = f35, f4
	(p10) fmix.lr		f11 = f3, f40

	(p1) fmpy f10 = f3, f4 
	(p17) fmpy f10 = f3, f4 
	(p1) fmpy f100 = f3, f4 
	(p1) fmpy f10 = f35, f4 
	(p1) fmpy f10 = f3, f40 
	(p1) fmpy.s f10 = f3, f4 
	(p17) fmpy.s f10 = f3, f4 
	(p1) fmpy.s f100 = f3, f4 
	(p1) fmpy.s f10 = f35, f4 
	(p1) fmpy.s f10 = f3, f40 
	(p1) fmpy.d f10 = f3, f4 
	(p17) fmpy.d f10 = f3, f4 
	(p1) fmpy.d f100 = f3, f4 
	(p1) fmpy.d f10 = f35, f4 
	(p1) fmpy.d f10 = f3, f40 
	(p1) fmpy.s0 f10 = f3, f4 
	(p17) fmpy.s0 f10 = f3, f4 
	(p1) fmpy.s0 f100 = f3, f4 
	(p1) fmpy.s0 f10 = f35, f4 
	(p1) fmpy.s0 f10 = f3, f40 
	(p1) fmpy.s.s0 f10 = f3, f4 
	(p17) fmpy.s.s0 f10 = f3, f4 
	(p1) fmpy.s.s0 f100 = f3, f4 
	(p1) fmpy.s.s0 f10 = f35, f4 
	(p1) fmpy.s.s0 f10 = f3, f40 
	(p1) fmpy.d.s0 f10 = f3, f4 
	(p17) fmpy.d.s0 f10 = f3, f4 
	(p1) fmpy.d.s0 f100 = f3, f4 
	(p1) fmpy.d.s0 f10 = f35, f4 
	(p1) fmpy.d.s0 f10 = f3, f40 
	(p1) fmpy.s1 f10 = f3, f4 
	(p17) fmpy.s1 f10 = f3, f4 
	(p1) fmpy.s1 f100 = f3, f4 
	(p1) fmpy.s1 f10 = f35, f4 
	(p1) fmpy.s1 f10 = f3, f40 
	(p1) fmpy.s.s1 f10 = f3, f4 
	(p17) fmpy.s.s1 f10 = f3, f4 
	(p1) fmpy.s.s1 f100 = f3, f4 
	(p1) fmpy.s.s1 f10 = f35, f4 
	(p1) fmpy.s.s1 f10 = f3, f40 
	(p1) fmpy.d.s1 f10 = f3, f4 
	(p17) fmpy.d.s1 f10 = f3, f4 
	(p1) fmpy.d.s1 f100 = f3, f4 
	(p1) fmpy.d.s1 f10 = f35, f4 
	(p1) fmpy.d.s1 f10 = f3, f40 
	(p1) fmpy.s2 f10 = f3, f4 
	(p17) fmpy.s2 f10 = f3, f4 
	(p1) fmpy.s2 f100 = f3, f4 
	(p1) fmpy.s2 f10 = f35, f4 
	(p1) fmpy.s2 f10 = f3, f40 
	(p1) fmpy.s.s2 f10 = f3, f4 
	(p17) fmpy.s.s2 f10 = f3, f4 
	(p1) fmpy.s.s2 f100 = f3, f4 
	(p1) fmpy.s.s2 f10 = f35, f4 
	(p1) fmpy.s.s2 f10 = f3, f40 
	(p1) fmpy.d.s2 f10 = f3, f4 
	(p17) fmpy.d.s2 f10 = f3, f4 
	(p1) fmpy.d.s2 f100 = f3, f4 
	(p1) fmpy.d.s2 f10 = f35, f4 
	(p1) fmpy.d.s2 f10 = f3, f40 
	(p1) fmpy.s3 f10 = f3, f4 
	(p17) fmpy.s3 f10 = f3, f4 
	(p1) fmpy.s3 f100 = f3, f4 
	(p1) fmpy.s3 f10 = f35, f4 
	(p1) fmpy.s3 f10 = f3, f40 
	(p1) fmpy.s.s3 f10 = f3, f4 
	(p17) fmpy.s.s3 f10 = f3, f4 
	(p1) fmpy.s.s3 f100 = f3, f4 
	(p1) fmpy.s.s3 f10 = f35, f4 
	(p1) fmpy.s.s3 f10 = f3, f40 
	(p1) fmpy.d.s3 f10 = f3, f4 
	(p17) fmpy.d.s3 f10 = f3, f4 
	(p1) fmpy.d.s3 f100 = f3, f4 
	(p1) fmpy.d.s3 f10 = f35, f4 
	(p1) fmpy.d.s3 f10 = f3, f40 

	(p1) fms f10 = f3, f4, f5 
	(p17) fms f10 = f3, f4, f5 
	(p1) fms f100 = f3, f4, f5 
	(p1) fms f10 = f35, f4, f5 
	(p1) fms f10 = f3, f40, f5 
	(p1) fms f10 = f3, f4, f50 
	(p1) fms.s f10 = f3, f4, f5 
	(p17) fms.s f10 = f3, f4, f5 
	(p1) fms.s f100 = f3, f4, f5 
	(p1) fms.s f10 = f35, f4, f5 
	(p1) fms.s f10 = f3, f40, f5 
	(p1) fms.s f10 = f3, f4, f50 
	(p1) fms.d f10 = f3, f4, f5 
	(p17) fms.d f10 = f3, f4, f5 
	(p1) fms.d f100 = f3, f4, f5 
	(p1) fms.d f10 = f35, f4, f5 
	(p1) fms.d f10 = f3, f40, f5 
	(p1) fms.d f10 = f3, f4, f50 
	(p1) fms.s0 f10 = f3, f4, f5 
	(p17) fms.s0 f10 = f3, f4, f5 
	(p1) fms.s0 f100 = f3, f4, f5 
	(p1) fms.s0 f10 = f35, f4, f5 
	(p1) fms.s0 f10 = f3, f40, f5 
	(p1) fms.s0 f10 = f3, f4, f50 
	(p1) fms.s.s0 f10 = f3, f4, f5 
	(p17) fms.s.s0 f10 = f3, f4, f5 
	(p1) fms.s.s0 f100 = f3, f4, f5 
	(p1) fms.s.s0 f10 = f35, f4, f5 
	(p1) fms.s.s0 f10 = f3, f40, f5 
	(p1) fms.s.s0 f10 = f3, f4, f50 
	(p1) fms.d.s0 f10 = f3, f4, f5 
	(p17) fms.d.s0 f10 = f3, f4, f5 
	(p1) fms.d.s0 f100 = f3, f4, f5 
	(p1) fms.d.s0 f10 = f35, f4, f5 
	(p1) fms.d.s0 f10 = f3, f40, f5 
	(p1) fms.d.s0 f10 = f3, f4, f50 
	(p1) fms.s1 f10 = f3, f4, f5 
	(p17) fms.s1 f10 = f3, f4, f5 
	(p1) fms.s1 f100 = f3, f4, f5 
	(p1) fms.s1 f10 = f35, f4, f5 
	(p1) fms.s1 f10 = f3, f40, f5 
	(p1) fms.s1 f10 = f3, f4, f50 
	(p1) fms.s.s1 f10 = f3, f4, f5 
	(p17) fms.s.s1 f10 = f3, f4, f5 
	(p1) fms.s.s1 f100 = f3, f4, f5 
	(p1) fms.s.s1 f10 = f35, f4, f5 
	(p1) fms.s.s1 f10 = f3, f40, f5 
	(p1) fms.s.s1 f10 = f3, f4, f50 
	(p1) fms.d.s1 f10 = f3, f4, f5 
	(p17) fms.d.s1 f10 = f3, f4, f5 
	(p1) fms.d.s1 f100 = f3, f4, f5 
	(p1) fms.d.s1 f10 = f35, f4, f5 
	(p1) fms.d.s1 f10 = f3, f40, f5 
	(p1) fms.d.s1 f10 = f3, f4, f50 
	(p1) fms.s2 f10 = f3, f4, f5 
	(p17) fms.s2 f10 = f3, f4, f5 
	(p1) fms.s2 f100 = f3, f4, f5 
	(p1) fms.s2 f10 = f35, f4, f5 
	(p1) fms.s2 f10 = f3, f40, f5 
	(p1) fms.s2 f10 = f3, f4, f50 
	(p1) fms.s.s2 f10 = f3, f4, f5 
	(p17) fms.s.s2 f10 = f3, f4, f5 
	(p1) fms.s.s2 f100 = f3, f4, f5 
	(p1) fms.s.s2 f10 = f35, f4, f5 
	(p1) fms.s.s2 f10 = f3, f40, f5 
	(p1) fms.s.s2 f10 = f3, f4, f50 
	(p1) fms.d.s2 f10 = f3, f4, f5 
	(p17) fms.d.s2 f10 = f3, f4, f5 
	(p1) fms.d.s2 f100 = f3, f4, f5 
	(p1) fms.d.s2 f10 = f35, f4, f5 
	(p1) fms.d.s2 f10 = f3, f40, f5 
	(p1) fms.d.s2 f10 = f3, f4, f50 
	(p1) fms.s3 f10 = f3, f4, f5 
	(p17) fms.s3 f10 = f3, f4, f5 
	(p1) fms.s3 f100 = f3, f4, f5 
	(p1) fms.s3 f10 = f35, f4, f5 
	(p1) fms.s3 f10 = f3, f40, f5 
	(p1) fms.s3 f10 = f3, f4, f50 
	(p1) fms.s.s3 f10 = f3, f4, f5 
	(p17) fms.s.s3 f10 = f3, f4, f5 
	(p1) fms.s.s3 f100 = f3, f4, f5 
	(p1) fms.s.s3 f10 = f35, f4, f5 
	(p1) fms.s.s3 f10 = f3, f40, f5 
	(p1) fms.s.s3 f10 = f3, f4, f50 
	(p1) fms.d.s3 f10 = f3, f4, f5 
	(p17) fms.d.s3 f10 = f3, f4, f5 
	(p1) fms.d.s3 f100 = f3, f4, f5 
	(p1) fms.d.s3 f10 = f35, f4, f5 
	(p1) fms.d.s3 f10 = f3, f40, f5 
	(p1) fms.d.s3 f10 = f3, f4, f50 

	(p1) fneg f10=f3
	(p51) fneg f10=f3
	(p1) fneg f100=f3
	(p1) fneg f10=f35

	(p2) fnegabs f10=f3
	(p52) fnegabs f10=f3
	(p2) fnegabs f100=f3
	(p2) fnegabs f10=f35
	nop 1

	(p1) fnma f10 = f3, f4, f5 
	(p17) fnma f10 = f3, f4, f5 
	(p1) fnma f100 = f3, f4, f5 
	(p1) fnma f10 = f35, f4, f5 
	(p1) fnma f10 = f3, f40, f5 
	(p1) fnma f10 = f3, f4, f50 
	(p1) fnma.s f10 = f3, f4, f5 
	(p17) fnma.s f10 = f3, f4, f5 
	(p1) fnma.s f100 = f3, f4, f5 
	(p1) fnma.s f10 = f35, f4, f5 
	(p1) fnma.s f10 = f3, f40, f5 
	(p1) fnma.s f10 = f3, f4, f50 
	(p1) fnma.d f10 = f3, f4, f5 
	(p17) fnma.d f10 = f3, f4, f5 
	(p1) fnma.d f100 = f3, f4, f5 
	(p1) fnma.d f10 = f35, f4, f5 
	(p1) fnma.d f10 = f3, f40, f5 
	(p1) fnma.d f10 = f3, f4, f50 
	(p1) fnma.s0 f10 = f3, f4, f5 
	(p17) fnma.s0 f10 = f3, f4, f5 
	(p1) fnma.s0 f100 = f3, f4, f5 
	(p1) fnma.s0 f10 = f35, f4, f5 
	(p1) fnma.s0 f10 = f3, f40, f5 
	(p1) fnma.s0 f10 = f3, f4, f50 
	(p1) fnma.s.s0 f10 = f3, f4, f5 
	(p17) fnma.s.s0 f10 = f3, f4, f5 
	(p1) fnma.s.s0 f100 = f3, f4, f5 
	(p1) fnma.s.s0 f10 = f35, f4, f5 
	(p1) fnma.s.s0 f10 = f3, f40, f5 
	(p1) fnma.s.s0 f10 = f3, f4, f50 
	(p1) fnma.d.s0 f10 = f3, f4, f5 
	(p17) fnma.d.s0 f10 = f3, f4, f5 
	(p1) fnma.d.s0 f100 = f3, f4, f5 
	(p1) fnma.d.s0 f10 = f35, f4, f5 
	(p1) fnma.d.s0 f10 = f3, f40, f5 
	(p1) fnma.d.s0 f10 = f3, f4, f50 
	(p1) fnma.s1 f10 = f3, f4, f5 
	(p17) fnma.s1 f10 = f3, f4, f5 
	(p1) fnma.s1 f100 = f3, f4, f5 
	(p1) fnma.s1 f10 = f35, f4, f5 
	(p1) fnma.s1 f10 = f3, f40, f5 
	(p1) fnma.s1 f10 = f3, f4, f50 
	(p1) fnma.s.s1 f10 = f3, f4, f5 
	(p17) fnma.s.s1 f10 = f3, f4, f5 
	(p1) fnma.s.s1 f100 = f3, f4, f5 
	(p1) fnma.s.s1 f10 = f35, f4, f5 
	(p1) fnma.s.s1 f10 = f3, f40, f5 
	(p1) fnma.s.s1 f10 = f3, f4, f50 
	(p1) fnma.d.s1 f10 = f3, f4, f5 
	(p17) fnma.d.s1 f10 = f3, f4, f5 
	(p1) fnma.d.s1 f100 = f3, f4, f5 
	(p1) fnma.d.s1 f10 = f35, f4, f5 
	(p1) fnma.d.s1 f10 = f3, f40, f5 
	(p1) fnma.d.s1 f10 = f3, f4, f50 
	(p1) fnma.s2 f10 = f3, f4, f5 
	(p17) fnma.s2 f10 = f3, f4, f5 
	(p1) fnma.s2 f100 = f3, f4, f5 
	(p1) fnma.s2 f10 = f35, f4, f5 
	(p1) fnma.s2 f10 = f3, f40, f5 
	(p1) fnma.s2 f10 = f3, f4, f50 
	(p1) fnma.s.s2 f10 = f3, f4, f5 
	(p17) fnma.s.s2 f10 = f3, f4, f5 
	(p1) fnma.s.s2 f100 = f3, f4, f5 
	(p1) fnma.s.s2 f10 = f35, f4, f5 
	(p1) fnma.s.s2 f10 = f3, f40, f5 
	(p1) fnma.s.s2 f10 = f3, f4, f50 
	(p1) fnma.d.s2 f10 = f3, f4, f5 
	(p17) fnma.d.s2 f10 = f3, f4, f5 
	(p1) fnma.d.s2 f100 = f3, f4, f5 
	(p1) fnma.d.s2 f10 = f35, f4, f5 
	(p1) fnma.d.s2 f10 = f3, f40, f5 
	(p1) fnma.d.s2 f10 = f3, f4, f50 
	(p1) fnma.s3 f10 = f3, f4, f5 
	(p17) fnma.s3 f10 = f3, f4, f5 
	(p1) fnma.s3 f100 = f3, f4, f5 
	(p1) fnma.s3 f10 = f35, f4, f5 
	(p1) fnma.s3 f10 = f3, f40, f5 
	(p1) fnma.s3 f10 = f3, f4, f50 
	(p1) fnma.s.s3 f10 = f3, f4, f5 
	(p17) fnma.s.s3 f10 = f3, f4, f5 
	(p1) fnma.s.s3 f100 = f3, f4, f5 
	(p1) fnma.s.s3 f10 = f35, f4, f5 
	(p1) fnma.s.s3 f10 = f3, f40, f5 
	(p1) fnma.s.s3 f10 = f3, f4, f50 
	(p1) fnma.d.s3 f10 = f3, f4, f5 
	(p17) fnma.d.s3 f10 = f3, f4, f5 
	(p1) fnma.d.s3 f100 = f3, f4, f5 
	(p1) fnma.d.s3 f10 = f35, f4, f5 
	(p1) fnma.d.s3 f10 = f3, f40, f5 
	(p1) fnma.d.s3 f10 = f3, f4, f50 

	(p1) fnmpy f10 = f3, f4 
	(p17) fnmpy f10 = f3, f4 
	(p1) fnmpy f100 = f3, f4 
	(p1) fnmpy f10 = f35, f4 
	(p1) fnmpy f10 = f3, f40 
	(p1) fnmpy.s f10 = f3, f4 
	(p17) fnmpy.s f10 = f3, f4 
	(p1) fnmpy.s f100 = f3, f4 
	(p1) fnmpy.s f10 = f35, f4 
	(p1) fnmpy.s f10 = f3, f40 
	(p1) fnmpy.d f10 = f3, f4 
	(p17) fnmpy.d f10 = f3, f4 
	(p1) fnmpy.d f100 = f3, f4 
	(p1) fnmpy.d f10 = f35, f4 
	(p1) fnmpy.d f10 = f3, f40 
	(p1) fnmpy.s0 f10 = f3, f4 
	(p17) fnmpy.s0 f10 = f3, f4 
	(p1) fnmpy.s0 f100 = f3, f4 
	(p1) fnmpy.s0 f10 = f35, f4 
	(p1) fnmpy.s0 f10 = f3, f40 
	(p1) fnmpy.s.s0 f10 = f3, f4 
	(p17) fnmpy.s.s0 f10 = f3, f4 
	(p1) fnmpy.s.s0 f100 = f3, f4 
	(p1) fnmpy.s.s0 f10 = f35, f4 
	(p1) fnmpy.s.s0 f10 = f3, f40 
	(p1) fnmpy.d.s0 f10 = f3, f4 
	(p17) fnmpy.d.s0 f10 = f3, f4 
	(p1) fnmpy.d.s0 f100 = f3, f4 
	(p1) fnmpy.d.s0 f10 = f35, f4 
	(p1) fnmpy.d.s0 f10 = f3, f40 
	(p1) fnmpy.s1 f10 = f3, f4 
	(p17) fnmpy.s1 f10 = f3, f4 
	(p1) fnmpy.s1 f100 = f3, f4 
	(p1) fnmpy.s1 f10 = f35, f4 
	(p1) fnmpy.s1 f10 = f3, f40 
	(p1) fnmpy.s.s1 f10 = f3, f4 
	(p17) fnmpy.s.s1 f10 = f3, f4 
	(p1) fnmpy.s.s1 f100 = f3, f4 
	(p1) fnmpy.s.s1 f10 = f35, f4 
	(p1) fnmpy.s.s1 f10 = f3, f40 
	(p1) fnmpy.d.s1 f10 = f3, f4 
	(p17) fnmpy.d.s1 f10 = f3, f4 
	(p1) fnmpy.d.s1 f100 = f3, f4 
	(p1) fnmpy.d.s1 f10 = f35, f4 
	(p1) fnmpy.d.s1 f10 = f3, f40 
	(p1) fnmpy.s2 f10 = f3, f4 
	(p17) fnmpy.s2 f10 = f3, f4 
	(p1) fnmpy.s2 f100 = f3, f4 
	(p1) fnmpy.s2 f10 = f35, f4 
	(p1) fnmpy.s2 f10 = f3, f40 
	(p1) fnmpy.s.s2 f10 = f3, f4 
	(p17) fnmpy.s.s2 f10 = f3, f4 
	(p1) fnmpy.s.s2 f100 = f3, f4 
	(p1) fnmpy.s.s2 f10 = f35, f4 
	(p1) fnmpy.s.s2 f10 = f3, f40 
	(p1) fnmpy.d.s2 f10 = f3, f4 
	(p17) fnmpy.d.s2 f10 = f3, f4 
	(p1) fnmpy.d.s2 f100 = f3, f4 
	(p1) fnmpy.d.s2 f10 = f35, f4 
	(p1) fnmpy.d.s2 f10 = f3, f40 
	(p1) fnmpy.s3 f10 = f3, f4 
	(p17) fnmpy.s3 f10 = f3, f4 
	(p1) fnmpy.s3 f100 = f3, f4 
	(p1) fnmpy.s3 f10 = f35, f4 
	(p1) fnmpy.s3 f10 = f3, f40 
	(p1) fnmpy.s.s3 f10 = f3, f4 
	(p17) fnmpy.s.s3 f10 = f3, f4 
	(p1) fnmpy.s.s3 f100 = f3, f4 
	(p1) fnmpy.s.s3 f10 = f35, f4 
	(p1) fnmpy.s.s3 f10 = f3, f40 
	(p1) fnmpy.d.s3 f10 = f3, f4 
	(p17) fnmpy.d.s3 f10 = f3, f4 
	(p1) fnmpy.d.s3 f100 = f3, f4 
	(p1) fnmpy.d.s3 f10 = f35, f4 
	(p1) fnmpy.d.s3 f10 = f3, f40 

	(p1) fnorm f10 = f3 
	(p17) fnorm f10 = f3 
	(p1) fnorm f100 = f3 
	(p1) fnorm f10 = f35 
	(p1) fnorm.s f10 = f3 
	(p17) fnorm.s f10 = f3 
	(p1) fnorm.s f100 = f3 
	(p1) fnorm.s f10 = f35 
	(p1) fnorm.d f10 = f3 
	(p17) fnorm.d f10 = f3 
	(p1) fnorm.d f100 = f3 
	(p1) fnorm.d f10 = f35 
	(p1) fnorm.s0 f10 = f3 
	(p17) fnorm.s0 f10 = f3 
	(p1) fnorm.s0 f100 = f3 
	(p1) fnorm.s0 f10 = f35 
	(p1) fnorm.s.s0 f10 = f3 
	(p17) fnorm.s.s0 f10 = f3 
	(p1) fnorm.s.s0 f100 = f3 
	(p1) fnorm.s.s0 f10 = f35 
	(p1) fnorm.d.s0 f10 = f3 
	(p17) fnorm.d.s0 f10 = f3 
	(p1) fnorm.d.s0 f100 = f3 
	(p1) fnorm.d.s0 f10 = f35 
	(p1) fnorm.s1 f10 = f3 
	(p17) fnorm.s1 f10 = f3 
	(p1) fnorm.s1 f100 = f3 
	(p1) fnorm.s1 f10 = f35 
	(p1) fnorm.s.s1 f10 = f3 
	(p17) fnorm.s.s1 f10 = f3 
	(p1) fnorm.s.s1 f100 = f3 
	(p1) fnorm.s.s1 f10 = f35 
	(p1) fnorm.d.s1 f10 = f3 
	(p17) fnorm.d.s1 f10 = f3 
	(p1) fnorm.d.s1 f100 = f3 
	(p1) fnorm.d.s1 f10 = f35 
	(p1) fnorm.s2 f10 = f3 
	(p17) fnorm.s2 f10 = f3 
	(p1) fnorm.s2 f100 = f3 
	(p1) fnorm.s2 f10 = f35 
	(p1) fnorm.s.s2 f10 = f3 
	(p17) fnorm.s.s2 f10 = f3 
	(p1) fnorm.s.s2 f100 = f3 
	(p1) fnorm.s.s2 f10 = f35 
	(p1) fnorm.d.s2 f10 = f3 
	(p17) fnorm.d.s2 f10 = f3 
	(p1) fnorm.d.s2 f100 = f3 
	(p1) fnorm.d.s2 f10 = f35 
	(p1) fnorm.s3 f10 = f3 
	(p17) fnorm.s3 f10 = f3 
	(p1) fnorm.s3 f100 = f3 
	(p1) fnorm.s3 f10 = f35 
	(p1) fnorm.s.s3 f10 = f3 
	(p17) fnorm.s.s3 f10 = f3 
	(p1) fnorm.s.s3 f100 = f3 
	(p1) fnorm.s.s3 f10 = f35 
	(p1) fnorm.d.s3 f10 = f3 
	(p17) fnorm.d.s3 f10 = f3 
	(p1) fnorm.d.s3 f100 = f3 
	(p1) fnorm.d.s3 f10 = f35 

	(p4) for 	f10 = f2, f3
	(p34) for 	f10 = f2, f3
	(p4) for 	f100 = f2, f3
	(p4) for 	f10 = f34, f3
	(p4) for 	f10 = f2, f35

	(p5) fpabs	f10 = f3
	(p35) fpabs	f10 = f3
	(p5) fpabs	f100 = f3
	(p5) fpabs	f10 = f35

	(p6) fpack	f10 = f2, f3
	(p36) fpack	f10 = f2, f3
	(p6) fpack	f100 = f2, f3
	(p6) fpack	f10 = f34, f3
	(p6) fpack	f10 = f2, f35

	(p2) fpamax f10=f3,f4 
	(p20) fpamax f10=f3,f4 
	(p2) fpamax f100=f3,f4 
	(p2) fpamax f10=f35,f4 
	(p2) fpamax f10=f3,f40 
	(p3) fpamax.s0 f10=f3,f4 
	(p30) fpamax.s0 f10=f3,f4 
	(p3) fpamax.s0 f100=f3,f4 
	(p3) fpamax.s0 f10=f35,f4 
	(p3) fpamax.s0 f10=f3,f40 
	(p3) fpamax.s1 f10=f3,f4 
	(p30) fpamax.s1 f10=f3,f4 
	(p3) fpamax.s1 f100=f3,f4 
	(p3) fpamax.s1 f10=f35,f4 
	(p3) fpamax.s1 f10=f3,f40 
	(p3) fpamax.s2 f10=f3,f4 
	(p30) fpamax.s2 f10=f3,f4 
	(p3) fpamax.s2 f100=f3,f4 
	(p3) fpamax.s2 f10=f35,f4 
	(p3) fpamax.s2 f10=f3,f40 
	(p3) fpamax.s3 f10=f3,f4 
	(p30) fpamax.s3 f10=f3,f4 
	(p3) fpamax.s3 f100=f3,f4 
	(p3) fpamax.s3 f10=f35,f4 
	(p3) fpamax.s3 f10=f3,f40 

	(p2) fpamin f10=f3,f4 
	(p20) fpamin f10=f3,f4 
	(p2) fpamin f100=f3,f4 
	(p2) fpamin f10=f35,f4 
	(p2) fpamin f10=f3,f40 
	(p3) fpamin.s0 f10=f3,f4 
	(p30) fpamin.s0 f10=f3,f4 
	(p3) fpamin.s0 f100=f3,f4 
	(p3) fpamin.s0 f10=f35,f4 
	(p3) fpamin.s0 f10=f3,f40 
	(p3) fpamin.s1 f10=f3,f4 
	(p30) fpamin.s1 f10=f3,f4 
	(p3) fpamin.s1 f100=f3,f4 
	(p3) fpamin.s1 f10=f35,f4 
	(p3) fpamin.s1 f10=f3,f40 
	(p3) fpamin.s2 f10=f3,f4 
	(p30) fpamin.s2 f10=f3,f4 
	(p3) fpamin.s2 f100=f3,f4 
	(p3) fpamin.s2 f10=f35,f4 
	(p3) fpamin.s2 f10=f3,f40 
	(p3) fpamin.s3 f10=f3,f4 
	(p30) fpamin.s3 f10=f3,f4 
	(p3) fpamin.s3 f100=f3,f4 
	(p3) fpamin.s3 f10=f35,f4 
	(p3) fpamin.s3 f10=f3,f40 

	(p2) fpcmp.eq 		f10 = f2, f3
	(p62) fpcmp.eq 		f10 = f2, f3
	(p2) fpcmp.eq 		f100 = f2, f3
	(p2) fpcmp.eq 		f10 = f34, f3
	(p2) fpcmp.eq 		f10 = f2, f35
	(p2) fpcmp.lt 		f10 = f2, f3
	(p62) fpcmp.lt 		f10 = f2, f3
	(p2) fpcmp.lt 		f100 = f2, f3
	(p2) fpcmp.lt 		f10 = f34, f3
	(p2) fpcmp.lt 		f10 = f2, f35
	(p2) fpcmp.le 		f10 = f2, f3
	(p62) fpcmp.le 		f10 = f2, f3
	(p2) fpcmp.le 		f100 = f2, f3
	(p2) fpcmp.le 		f10 = f34, f3
	(p2) fpcmp.le 		f10 = f2, f35
	(p2) fpcmp.gt 		f10 = f2, f3
	(p62) fpcmp.gt 		f10 = f2, f3
	(p2) fpcmp.gt 		f100 = f2, f3
	(p2) fpcmp.gt 		f10 = f34, f3
	(p2) fpcmp.gt 		f10 = f2, f35
	(p2) fpcmp.ge 		f10 = f2, f3
	(p62) fpcmp.ge 		f10 = f2, f3
	(p2) fpcmp.ge 		f100 = f2, f3
	(p2) fpcmp.ge 		f10 = f34, f3
	(p2) fpcmp.ge 		f10 = f2, f35
	(p2) fpcmp.unord 		f10 = f2, f3
	(p62) fpcmp.unord 		f10 = f2, f3
	(p2) fpcmp.unord 		f100 = f2, f3
	(p2) fpcmp.unord 		f10 = f34, f3
	(p2) fpcmp.unord 		f10 = f2, f35
	(p2) fpcmp.neq 		f10 = f2, f3
	(p62) fpcmp.neq 		f10 = f2, f3
	(p2) fpcmp.neq 		f100 = f2, f3
	(p2) fpcmp.neq 		f10 = f34, f3
	(p2) fpcmp.neq 		f10 = f2, f35
	(p2) fpcmp.nlt 		f10 = f2, f3
	(p62) fpcmp.nlt 		f10 = f2, f3
	(p2) fpcmp.nlt 		f100 = f2, f3
	(p2) fpcmp.nlt 		f10 = f34, f3
	(p2) fpcmp.nlt 		f10 = f2, f35
	(p2) fpcmp.nle 		f10 = f2, f3
	(p62) fpcmp.nle 		f10 = f2, f3
	(p2) fpcmp.nle 		f100 = f2, f3
	(p2) fpcmp.nle 		f10 = f34, f3
	(p2) fpcmp.nle 		f10 = f2, f35
	(p2) fpcmp.ngt 		f10 = f2, f3
	(p62) fpcmp.ngt 		f10 = f2, f3
	(p2) fpcmp.ngt 		f100 = f2, f3
	(p2) fpcmp.ngt 		f10 = f34, f3
	(p2) fpcmp.ngt 		f10 = f2, f35
	(p2) fpcmp.nge 		f10 = f2, f3
	(p62) fpcmp.nge 		f10 = f2, f3
	(p2) fpcmp.nge 		f100 = f2, f3
	(p2) fpcmp.nge 		f10 = f34, f3
	(p2) fpcmp.nge 		f10 = f2, f35
	(p2) fpcmp.ord 		f10 = f2, f3
	(p62) fpcmp.ord 		f10 = f2, f3
	(p2) fpcmp.ord 		f100 = f2, f3
	(p2) fpcmp.ord 		f10 = f34, f3
	(p2) fpcmp.ord 		f10 = f2, f35
	(p2) fpcmp.eq.s0 		f10 = f2, f3
	(p62) fpcmp.eq.s0 		f10 = f2, f3
	(p2) fpcmp.eq.s0 		f100 = f2, f3
	(p2) fpcmp.eq.s0 		f10 = f34, f3
	(p2) fpcmp.eq.s0 		f10 = f2, f35
	(p2) fpcmp.lt.s0 		f10 = f2, f3
	(p62) fpcmp.lt.s0 		f10 = f2, f3
	(p2) fpcmp.lt.s0 		f100 = f2, f3
	(p2) fpcmp.lt.s0 		f10 = f34, f3
	(p2) fpcmp.lt.s0 		f10 = f2, f35
	(p2) fpcmp.le.s0 		f10 = f2, f3
	(p62) fpcmp.le.s0 		f10 = f2, f3
	(p2) fpcmp.le.s0 		f100 = f2, f3
	(p2) fpcmp.le.s0 		f10 = f34, f3
	(p2) fpcmp.le.s0 		f10 = f2, f35
	(p2) fpcmp.gt.s0 		f10 = f2, f3
	(p62) fpcmp.gt.s0 		f10 = f2, f3
	(p2) fpcmp.gt.s0 		f100 = f2, f3
	(p2) fpcmp.gt.s0 		f10 = f34, f3
	(p2) fpcmp.gt.s0 		f10 = f2, f35
	(p2) fpcmp.ge.s0 		f10 = f2, f3
	(p62) fpcmp.ge.s0 		f10 = f2, f3
	(p2) fpcmp.ge.s0 		f100 = f2, f3
	(p2) fpcmp.ge.s0 		f10 = f34, f3
	(p2) fpcmp.ge.s0 		f10 = f2, f35
	(p2) fpcmp.unord.s0 		f10 = f2, f3
	(p62) fpcmp.unord.s0 		f10 = f2, f3
	(p2) fpcmp.unord.s0 		f100 = f2, f3
	(p2) fpcmp.unord.s0 		f10 = f34, f3
	(p2) fpcmp.unord.s0 		f10 = f2, f35
	(p2) fpcmp.neq.s0 		f10 = f2, f3
	(p62) fpcmp.neq.s0 		f10 = f2, f3
	(p2) fpcmp.neq.s0 		f100 = f2, f3
	(p2) fpcmp.neq.s0 		f10 = f34, f3
	(p2) fpcmp.neq.s0 		f10 = f2, f35
	(p2) fpcmp.nlt.s0 		f10 = f2, f3
	(p62) fpcmp.nlt.s0 		f10 = f2, f3
	(p2) fpcmp.nlt.s0 		f100 = f2, f3
	(p2) fpcmp.nlt.s0 		f10 = f34, f3
	(p2) fpcmp.nlt.s0 		f10 = f2, f35
	(p2) fpcmp.nle.s0 		f10 = f2, f3
	(p62) fpcmp.nle.s0 		f10 = f2, f3
	(p2) fpcmp.nle.s0 		f100 = f2, f3
	(p2) fpcmp.nle.s0 		f10 = f34, f3
	(p2) fpcmp.nle.s0 		f10 = f2, f35
	(p2) fpcmp.ngt.s0 		f10 = f2, f3
	(p62) fpcmp.ngt.s0 		f10 = f2, f3
	(p2) fpcmp.ngt.s0 		f100 = f2, f3
	(p2) fpcmp.ngt.s0 		f10 = f34, f3
	(p2) fpcmp.ngt.s0 		f10 = f2, f35
	(p2) fpcmp.nge.s0 		f10 = f2, f3
	(p62) fpcmp.nge.s0 		f10 = f2, f3
	(p2) fpcmp.nge.s0 		f100 = f2, f3
	(p2) fpcmp.nge.s0 		f10 = f34, f3
	(p2) fpcmp.nge.s0 		f10 = f2, f35
	(p2) fpcmp.ord.s0 		f10 = f2, f3
	(p62) fpcmp.ord.s0 		f10 = f2, f3
	(p2) fpcmp.ord.s0 		f100 = f2, f3
	(p2) fpcmp.ord.s0 		f10 = f34, f3
	(p2) fpcmp.ord.s0 		f10 = f2, f35
	(p2) fpcmp.eq.s1 		f10 = f2, f3
	(p62) fpcmp.eq.s1 		f10 = f2, f3
	(p2) fpcmp.eq.s1 		f100 = f2, f3
	(p2) fpcmp.eq.s1 		f10 = f34, f3
	(p2) fpcmp.eq.s1 		f10 = f2, f35
	(p2) fpcmp.lt.s1 		f10 = f2, f3
	(p62) fpcmp.lt.s1 		f10 = f2, f3
	(p2) fpcmp.lt.s1 		f100 = f2, f3
	(p2) fpcmp.lt.s1 		f10 = f34, f3
	(p2) fpcmp.lt.s1 		f10 = f2, f35
	(p2) fpcmp.le.s1 		f10 = f2, f3
	(p62) fpcmp.le.s1 		f10 = f2, f3
	(p2) fpcmp.le.s1 		f100 = f2, f3
	(p2) fpcmp.le.s1 		f10 = f34, f3
	(p2) fpcmp.le.s1 		f10 = f2, f35
	(p2) fpcmp.gt.s1 		f10 = f2, f3
	(p62) fpcmp.gt.s1 		f10 = f2, f3
	(p2) fpcmp.gt.s1 		f100 = f2, f3
	(p2) fpcmp.gt.s1 		f10 = f34, f3
	(p2) fpcmp.gt.s1 		f10 = f2, f35
	(p2) fpcmp.ge.s1 		f10 = f2, f3
	(p62) fpcmp.ge.s1 		f10 = f2, f3
	(p2) fpcmp.ge.s1 		f100 = f2, f3
	(p2) fpcmp.ge.s1 		f10 = f34, f3
	(p2) fpcmp.ge.s1 		f10 = f2, f35
	(p2) fpcmp.unord.s1 		f10 = f2, f3
	(p62) fpcmp.unord.s1 		f10 = f2, f3
	(p2) fpcmp.unord.s1 		f100 = f2, f3
	(p2) fpcmp.unord.s1 		f10 = f34, f3
	(p2) fpcmp.unord.s1 		f10 = f2, f35
	(p2) fpcmp.neq.s1 		f10 = f2, f3
	(p62) fpcmp.neq.s1 		f10 = f2, f3
	(p2) fpcmp.neq.s1 		f100 = f2, f3
	(p2) fpcmp.neq.s1 		f10 = f34, f3
	(p2) fpcmp.neq.s1 		f10 = f2, f35
	(p2) fpcmp.nlt.s1 		f10 = f2, f3
	(p62) fpcmp.nlt.s1 		f10 = f2, f3
	(p2) fpcmp.nlt.s1 		f100 = f2, f3
	(p2) fpcmp.nlt.s1 		f10 = f34, f3
	(p2) fpcmp.nlt.s1 		f10 = f2, f35
	(p2) fpcmp.nle.s1 		f10 = f2, f3
	(p62) fpcmp.nle.s1 		f10 = f2, f3
	(p2) fpcmp.nle.s1 		f100 = f2, f3
	(p2) fpcmp.nle.s1 		f10 = f34, f3
	(p2) fpcmp.nle.s1 		f10 = f2, f35
	(p2) fpcmp.ngt.s1 		f10 = f2, f3
	(p62) fpcmp.ngt.s1 		f10 = f2, f3
	(p2) fpcmp.ngt.s1 		f100 = f2, f3
	(p2) fpcmp.ngt.s1 		f10 = f34, f3
	(p2) fpcmp.ngt.s1 		f10 = f2, f35
	(p2) fpcmp.nge.s1 		f10 = f2, f3
	(p62) fpcmp.nge.s1 		f10 = f2, f3
	(p2) fpcmp.nge.s1 		f100 = f2, f3
	(p2) fpcmp.nge.s1 		f10 = f34, f3
	(p2) fpcmp.nge.s1 		f10 = f2, f35
	(p2) fpcmp.ord.s1 		f10 = f2, f3
	(p62) fpcmp.ord.s1 		f10 = f2, f3
	(p2) fpcmp.ord.s1 		f100 = f2, f3
	(p2) fpcmp.ord.s1 		f10 = f34, f3
	(p2) fpcmp.ord.s1 		f10 = f2, f35
	(p2) fpcmp.eq.s2 		f10 = f2, f3
	(p62) fpcmp.eq.s2 		f10 = f2, f3
	(p2) fpcmp.eq.s2 		f100 = f2, f3
	(p2) fpcmp.eq.s2 		f10 = f34, f3
	(p2) fpcmp.eq.s2 		f10 = f2, f35
	(p2) fpcmp.lt.s2 		f10 = f2, f3
	(p62) fpcmp.lt.s2 		f10 = f2, f3
	(p2) fpcmp.lt.s2 		f100 = f2, f3
	(p2) fpcmp.lt.s2 		f10 = f34, f3
	(p2) fpcmp.lt.s2 		f10 = f2, f35
	(p2) fpcmp.le.s2 		f10 = f2, f3
	(p62) fpcmp.le.s2 		f10 = f2, f3
	(p2) fpcmp.le.s2 		f100 = f2, f3
	(p2) fpcmp.le.s2 		f10 = f34, f3
	(p2) fpcmp.le.s2 		f10 = f2, f35
	(p2) fpcmp.gt.s2 		f10 = f2, f3
	(p62) fpcmp.gt.s2 		f10 = f2, f3
	(p2) fpcmp.gt.s2 		f100 = f2, f3
	(p2) fpcmp.gt.s2 		f10 = f34, f3
	(p2) fpcmp.gt.s2 		f10 = f2, f35
	(p2) fpcmp.ge.s2 		f10 = f2, f3
	(p62) fpcmp.ge.s2 		f10 = f2, f3
	(p2) fpcmp.ge.s2 		f100 = f2, f3
	(p2) fpcmp.ge.s2 		f10 = f34, f3
	(p2) fpcmp.ge.s2 		f10 = f2, f35
	(p2) fpcmp.unord.s2 		f10 = f2, f3
	(p62) fpcmp.unord.s2 		f10 = f2, f3
	(p2) fpcmp.unord.s2 		f100 = f2, f3
	(p2) fpcmp.unord.s2 		f10 = f34, f3
	(p2) fpcmp.unord.s2 		f10 = f2, f35
	(p2) fpcmp.neq.s2 		f10 = f2, f3
	(p62) fpcmp.neq.s2 		f10 = f2, f3
	(p2) fpcmp.neq.s2 		f100 = f2, f3
	(p2) fpcmp.neq.s2 		f10 = f34, f3
	(p2) fpcmp.neq.s2 		f10 = f2, f35
	(p2) fpcmp.nlt.s2 		f10 = f2, f3
	(p62) fpcmp.nlt.s2 		f10 = f2, f3
	(p2) fpcmp.nlt.s2 		f100 = f2, f3
	(p2) fpcmp.nlt.s2 		f10 = f34, f3
	(p2) fpcmp.nlt.s2 		f10 = f2, f35
	(p2) fpcmp.nle.s2 		f10 = f2, f3
	(p62) fpcmp.nle.s2 		f10 = f2, f3
	(p2) fpcmp.nle.s2 		f100 = f2, f3
	(p2) fpcmp.nle.s2 		f10 = f34, f3
	(p2) fpcmp.nle.s2 		f10 = f2, f35
	(p2) fpcmp.ngt.s2 		f10 = f2, f3
	(p62) fpcmp.ngt.s2 		f10 = f2, f3
	(p2) fpcmp.ngt.s2 		f100 = f2, f3
	(p2) fpcmp.ngt.s2 		f10 = f34, f3
	(p2) fpcmp.ngt.s2 		f10 = f2, f35
	(p2) fpcmp.nge.s2 		f10 = f2, f3
	(p62) fpcmp.nge.s2 		f10 = f2, f3
	(p2) fpcmp.nge.s2 		f100 = f2, f3
	(p2) fpcmp.nge.s2 		f10 = f34, f3
	(p2) fpcmp.nge.s2 		f10 = f2, f35
	(p2) fpcmp.ord.s2 		f10 = f2, f3
	(p62) fpcmp.ord.s2 		f10 = f2, f3
	(p2) fpcmp.ord.s2 		f100 = f2, f3
	(p2) fpcmp.ord.s2 		f10 = f34, f3
	(p2) fpcmp.ord.s2 		f10 = f2, f35
	(p2) fpcmp.eq.s3 		f10 = f2, f3
	(p62) fpcmp.eq.s3 		f10 = f2, f3
	(p2) fpcmp.eq.s3 		f100 = f2, f3
	(p2) fpcmp.eq.s3 		f10 = f34, f3
	(p2) fpcmp.eq.s3 		f10 = f2, f35
	(p2) fpcmp.lt.s3 		f10 = f2, f3
	(p62) fpcmp.lt.s3 		f10 = f2, f3
	(p2) fpcmp.lt.s3 		f100 = f2, f3
	(p2) fpcmp.lt.s3 		f10 = f34, f3
	(p2) fpcmp.lt.s3 		f10 = f2, f35
	(p2) fpcmp.le.s3 		f10 = f2, f3
	(p62) fpcmp.le.s3 		f10 = f2, f3
	(p2) fpcmp.le.s3 		f100 = f2, f3
	(p2) fpcmp.le.s3 		f10 = f34, f3
	(p2) fpcmp.le.s3 		f10 = f2, f35
	(p2) fpcmp.gt.s3 		f10 = f2, f3
	(p62) fpcmp.gt.s3 		f10 = f2, f3
	(p2) fpcmp.gt.s3 		f100 = f2, f3
	(p2) fpcmp.gt.s3 		f10 = f34, f3
	(p2) fpcmp.gt.s3 		f10 = f2, f35
	(p2) fpcmp.ge.s3 		f10 = f2, f3
	(p62) fpcmp.ge.s3 		f10 = f2, f3
	(p2) fpcmp.ge.s3 		f100 = f2, f3
	(p2) fpcmp.ge.s3 		f10 = f34, f3
	(p2) fpcmp.ge.s3 		f10 = f2, f35
	(p2) fpcmp.unord.s3 		f10 = f2, f3
	(p62) fpcmp.unord.s3 		f10 = f2, f3
	(p2) fpcmp.unord.s3 		f100 = f2, f3
	(p2) fpcmp.unord.s3 		f10 = f34, f3
	(p2) fpcmp.unord.s3 		f10 = f2, f35
	(p2) fpcmp.neq.s3 		f10 = f2, f3
	(p62) fpcmp.neq.s3 		f10 = f2, f3
	(p2) fpcmp.neq.s3 		f100 = f2, f3
	(p2) fpcmp.neq.s3 		f10 = f34, f3
	(p2) fpcmp.neq.s3 		f10 = f2, f35
	(p2) fpcmp.nlt.s3 		f10 = f2, f3
	(p62) fpcmp.nlt.s3 		f10 = f2, f3
	(p2) fpcmp.nlt.s3 		f100 = f2, f3
	(p2) fpcmp.nlt.s3 		f10 = f34, f3
	(p2) fpcmp.nlt.s3 		f10 = f2, f35
	(p2) fpcmp.nle.s3 		f10 = f2, f3
	(p62) fpcmp.nle.s3 		f10 = f2, f3
	(p2) fpcmp.nle.s3 		f100 = f2, f3
	(p2) fpcmp.nle.s3 		f10 = f34, f3
	(p2) fpcmp.nle.s3 		f10 = f2, f35
	(p2) fpcmp.ngt.s3 		f10 = f2, f3
	(p62) fpcmp.ngt.s3 		f10 = f2, f3
	(p2) fpcmp.ngt.s3 		f100 = f2, f3
	(p2) fpcmp.ngt.s3 		f10 = f34, f3
	(p2) fpcmp.ngt.s3 		f10 = f2, f35
	(p2) fpcmp.nge.s3 		f10 = f2, f3
	(p62) fpcmp.nge.s3 		f10 = f2, f3
	(p2) fpcmp.nge.s3 		f100 = f2, f3
	(p2) fpcmp.nge.s3 		f10 = f34, f3
	(p2) fpcmp.nge.s3 		f10 = f2, f35
	(p2) fpcmp.ord.s3 		f10 = f2, f3
	(p62) fpcmp.ord.s3 		f10 = f2, f3
	(p2) fpcmp.ord.s3 		f100 = f2, f3
	(p2) fpcmp.ord.s3 		f10 = f34, f3
	(p2) fpcmp.ord.s3 		f10 = f2, f35

	(p2) fpcvt.fx		f11 = f3
	(p62) fpcvt.fx		f11 = f3
	(p2) fpcvt.fx		f110 = f3
	(p2) fpcvt.fx		f11 = f35
	(p3) fpcvt.fx.trunc	f11 = f3
	(p63) fpcvt.fx.trunc	f11 = f3
	(p3) fpcvt.fx.trunc	f110 = f3
	(p3) fpcvt.fx.trunc	f11 = f35
	(p2) fpcvt.fx.s0		f11 = f3
	(p62) fpcvt.fx.s0	f11 = f3
	(p2) fpcvt.fx.s0		f110 = f3
	(p2) fpcvt.fx.s0		f11 = f35
	(p3) fpcvt.fx.trunc.s0	f11 = f3
	(p63) fpcvt.fx.trunc.s0	f11 = f3
	(p3) fpcvt.fx.trunc.s0	f110 = f3
	(p3) fpcvt.fx.trunc.s0	f11 = f35
	(p2) fpcvt.fx.s1		f11 = f3
	(p62) fpcvt.fx.s1	f11 = f3
	(p2) fpcvt.fx.s1		f110 = f3
	(p2) fpcvt.fx.s1		f11 = f35
	(p3) fpcvt.fx.trunc.s1	f11 = f3
	(p63) fpcvt.fx.trunc.s1	f11 = f3
	(p3) fpcvt.fx.trunc.s1	f110 = f3
	(p3) fpcvt.fx.trunc.s1	f11 = f35
	(p2) fpcvt.fx.s2		f11 = f3
	(p62) fpcvt.fx.s2	f11 = f3
	(p2) fpcvt.fx.s2		f110 = f3
	(p2) fpcvt.fx.s2		f11 = f35
	(p3) fpcvt.fx.trunc.s2	f11 = f3
	(p63) fpcvt.fx.trunc.s2	f11 = f3
	(p3) fpcvt.fx.trunc.s2	f110 = f3
	(p3) fpcvt.fx.trunc.s2	f11 = f35
	(p2) fpcvt.fx.s3		f11 = f3
	(p62) fpcvt.fx.s3	f11 = f3
	(p2) fpcvt.fx.s3		f110 = f3
	(p2) fpcvt.fx.s3		f11 = f35
	(p3) fpcvt.fx.trunc.s3	f11 = f3
	(p63) fpcvt.fx.trunc.s3	f11 = f3
	(p3) fpcvt.fx.trunc.s3	f110 = f3
	(p3) fpcvt.fx.trunc.s3	f11 = f35
	(p2) fpcvt.fxu		f11 = f3
	(p62) fpcvt.fxu		f11 = f3
	(p2) fpcvt.fxu		f110 = f3
	(p2) fpcvt.fxu		f11 = f35
	(p3) fpcvt.fxu.trunc	f11 = f3
	(p63) fpcvt.fxu.trunc	f11 = f3
	(p3) fpcvt.fxu.trunc	f110 = f3
	(p3) fpcvt.fxu.trunc	f11 = f35
	(p2) fpcvt.fxu.s0		f11 = f3
	(p62) fpcvt.fxu.s0	f11 = f3
	(p2) fpcvt.fxu.s0		f110 = f3
	(p2) fpcvt.fxu.s0		f11 = f35
	(p3) fpcvt.fxu.trunc.s0	f11 = f3
	(p63) fpcvt.fxu.trunc.s0	f11 = f3
	(p3) fpcvt.fxu.trunc.s0	f110 = f3
	(p3) fpcvt.fxu.trunc.s0	f11 = f35
	(p2) fpcvt.fxu.s1		f11 = f3
	(p62) fpcvt.fxu.s1	f11 = f3
	(p2) fpcvt.fxu.s1		f110 = f3
	(p2) fpcvt.fxu.s1		f11 = f35
	(p3) fpcvt.fxu.trunc.s1	f11 = f3
	(p63) fpcvt.fxu.trunc.s1	f11 = f3
	(p3) fpcvt.fxu.trunc.s1	f110 = f3
	(p3) fpcvt.fxu.trunc.s1	f11 = f35
	(p2) fpcvt.fxu.s2		f11 = f3
	(p62) fpcvt.fxu.s2	f11 = f3
	(p2) fpcvt.fxu.s2		f110 = f3
	(p2) fpcvt.fxu.s2		f11 = f35
	(p3) fpcvt.fxu.trunc.s2	f11 = f3
	(p63) fpcvt.fxu.trunc.s2	f11 = f3
	(p3) fpcvt.fxu.trunc.s2	f110 = f3
	(p3) fpcvt.fxu.trunc.s2	f11 = f35
	(p2) fpcvt.fxu.s3		f11 = f3
	(p62) fpcvt.fxu.s3	f11 = f3
	(p2) fpcvt.fxu.s3		f110 = f3
	(p2) fpcvt.fxu.s3		f11 = f35
	(p3) fpcvt.fxu.trunc.s3	f11 = f3
	(p63) fpcvt.fxu.trunc.s3	f11 = f3
	(p3) fpcvt.fxu.trunc.s3	f110 = f3
	(p3) fpcvt.fxu.trunc.s3	f11 = f35

	(p1) fpma f10 = f3, f4, f5 
	(p17) fpma f10 = f3, f4, f5 
	(p1) fpma f100 = f3, f4, f5 
	(p1) fpma f10 = f35, f4, f5 
	(p1) fpma f10 = f3, f40, f5 
	(p1) fpma f10 = f3, f4, f50 
	(p1) fpma.s0 f10 = f3, f4, f5 
	(p17) fpma.s0 f10 = f3, f4, f5 
	(p1) fpma.s0 f100 = f3, f4, f5 
	(p1) fpma.s0 f10 = f35, f4, f5 
	(p1) fpma.s0 f10 = f3, f40, f5 
	(p1) fpma.s0 f10 = f3, f4, f50 
	(p1) fpma.s1 f10 = f3, f4, f5 
	(p17) fpma.s1 f10 = f3, f4, f5 
	(p1) fpma.s1 f100 = f3, f4, f5 
	(p1) fpma.s1 f10 = f35, f4, f5 
	(p1) fpma.s1 f10 = f3, f40, f5 
	(p1) fpma.s1 f10 = f3, f4, f50 
	(p1) fpma.s2 f10 = f3, f4, f5 
	(p17) fpma.s2 f10 = f3, f4, f5 
	(p1) fpma.s2 f100 = f3, f4, f5 
	(p1) fpma.s2 f10 = f35, f4, f5 
	(p1) fpma.s2 f10 = f3, f40, f5 
	(p1) fpma.s2 f10 = f3, f4, f50 
	(p1) fpma.s3 f10 = f3, f4, f5 
	(p17) fpma.s3 f10 = f3, f4, f5 
	(p1) fpma.s3 f100 = f3, f4, f5 
	(p1) fpma.s3 f10 = f35, f4, f5 
	(p1) fpma.s3 f10 = f3, f40, f5 
	(p1) fpma.s3 f10 = f3, f4, f50 

	(p2) fpmax f10=f3,f4 
	(p20) fpmax f10=f3,f4 
	(p2) fpmax f100=f3,f4 
	(p2) fpmax f10=f35,f4 
	(p2) fpmax f10=f3,f40 
	(p3) fpmax.s0 f10=f3,f4 
	(p30) fpmax.s0 f10=f3,f4 
	(p3) fpmax.s0 f100=f3,f4 
	(p3) fpmax.s0 f10=f35,f4 
	(p3) fpmax.s0 f10=f3,f40 
	(p3) fpmax.s1 f10=f3,f4 
	(p30) fpmax.s1 f10=f3,f4 
	(p3) fpmax.s1 f100=f3,f4 
	(p3) fpmax.s1 f10=f35,f4 
	(p3) fpmax.s1 f10=f3,f40 
	(p3) fpmax.s2 f10=f3,f4 
	(p30) fpmax.s2 f10=f3,f4 
	(p3) fpmax.s2 f100=f3,f4 
	(p3) fpmax.s2 f10=f35,f4 
	(p3) fpmax.s2 f10=f3,f40 
	(p3) fpmax.s3 f10=f3,f4 
	(p30) fpmax.s3 f10=f3,f4 
	(p3) fpmax.s3 f100=f3,f4 
	(p3) fpmax.s3 f10=f35,f4 
	(p3) fpmax.s3 f10=f3,f40 

	(p10) fpmerge.ns		f11 = f3, f4
	(p18) fpmerge.ns		f11 = f3, f4
	(p10) fpmerge.ns		f110 = f3, f4
	(p10) fpmerge.ns		f11 = f35, f4
	(p10) fpmerge.ns		f11 = f3, f40
	(p10) fpmerge.s		f11 = f3, f4
	(p18) fpmerge.s		f11 = f3, f4
	(p10) fpmerge.s		f110 = f3, f4
	(p10) fpmerge.s		f11 = f35, f4
	(p10) fpmerge.s		f11 = f3, f40
	(p10) fpmerge.se		f11 = f3, f4
	(p18) fpmerge.se		f11 = f3, f4
	(p10) fpmerge.se		f110 = f3, f4
	(p10) fpmerge.se		f11 = f35, f4
	(p10) fpmerge.se		f11 = f3, f40

	(p2) fpmin f10=f3,f4 
	(p20) fpmin f10=f3,f4 
	(p2) fpmin f100=f3,f4 
	(p2) fpmin f10=f35,f4 
	(p2) fpmin f10=f3,f40 
	(p3) fpmin.s0 f10=f3,f4 
	(p30) fpmin.s0 f10=f3,f4 
	(p3) fpmin.s0 f100=f3,f4 
	(p3) fpmin.s0 f10=f35,f4 
	(p3) fpmin.s0 f10=f3,f40 
	(p3) fpmin.s1 f10=f3,f4 
	(p30) fpmin.s1 f10=f3,f4 
	(p3) fpmin.s1 f100=f3,f4 
	(p3) fpmin.s1 f10=f35,f4 
	(p3) fpmin.s1 f10=f3,f40 
	(p3) fpmin.s2 f10=f3,f4 
	(p30) fpmin.s2 f10=f3,f4 
	(p3) fpmin.s2 f100=f3,f4 
	(p3) fpmin.s2 f10=f35,f4 
	(p3) fpmin.s2 f10=f3,f40 
	(p3) fpmin.s3 f10=f3,f4 
	(p30) fpmin.s3 f10=f3,f4 
	(p3) fpmin.s3 f100=f3,f4 
	(p3) fpmin.s3 f10=f35,f4 
	(p3) fpmin.s3 f10=f3,f40 

	(p1) fpmpy f10 = f3, f4 
	(p17) fpmpy f10 = f3, f4 
	(p1) fpmpy f100 = f3, f4 
	(p1) fpmpy f10 = f35, f4 
	(p1) fpmpy f10 = f3, f40 
	(p1) fpmpy.s0 f10 = f3, f4 
	(p17) fpmpy.s0 f10 = f3, f4 
	(p1) fpmpy.s0 f100 = f3, f4 
	(p1) fpmpy.s0 f10 = f35, f4 
	(p1) fpmpy.s0 f10 = f3, f40 
	(p1) fpmpy.s1 f10 = f3, f4 
	(p17) fpmpy.s1 f10 = f3, f4 
	(p1) fpmpy.s1 f100 = f3, f4 
	(p1) fpmpy.s1 f10 = f35, f4 
	(p1) fpmpy.s1 f10 = f3, f40 
	(p1) fpmpy.s2 f10 = f3, f4 
	(p17) fpmpy.s2 f10 = f3, f4 
	(p1) fpmpy.s2 f100 = f3, f4 
	(p1) fpmpy.s2 f10 = f35, f4 
	(p1) fpmpy.s2 f10 = f3, f40 
	(p1) fpmpy.s3 f10 = f3, f4 
	(p17) fpmpy.s3 f10 = f3, f4 
	(p1) fpmpy.s3 f100 = f3, f4 
	(p1) fpmpy.s3 f10 = f35, f4 
	(p1) fpmpy.s3 f10 = f3, f40 

	(p1) fpms f10 = f3, f4, f5 
	(p17) fpms f10 = f3, f4, f5 
	(p1) fpms f100 = f3, f4, f5 
	(p1) fpms f10 = f35, f4, f5 
	(p1) fpms f10 = f3, f40, f5 
	(p1) fpms f10 = f3, f4, f50 
	(p1) fpms.s0 f10 = f3, f4, f5 
	(p17) fpms.s0 f10 = f3, f4, f5 
	(p1) fpms.s0 f100 = f3, f4, f5 
	(p1) fpms.s0 f10 = f35, f4, f5 
	(p1) fpms.s0 f10 = f3, f40, f5 
	(p1) fpms.s0 f10 = f3, f4, f50 
	(p1) fpms.s1 f10 = f3, f4, f5 
	(p17) fpms.s1 f10 = f3, f4, f5 
	(p1) fpms.s1 f100 = f3, f4, f5 
	(p1) fpms.s1 f10 = f35, f4, f5 
	(p1) fpms.s1 f10 = f3, f40, f5 
	(p1) fpms.s1 f10 = f3, f4, f50 
	(p1) fpms.s2 f10 = f3, f4, f5 
	(p17) fpms.s2 f10 = f3, f4, f5 
	(p1) fpms.s2 f100 = f3, f4, f5 
	(p1) fpms.s2 f10 = f35, f4, f5 
	(p1) fpms.s2 f10 = f3, f40, f5 
	(p1) fpms.s2 f10 = f3, f4, f50 
	(p1) fpms.s3 f10 = f3, f4, f5 
	(p17) fpms.s3 f10 = f3, f4, f5 
	(p1) fpms.s3 f100 = f3, f4, f5 
	(p1) fpms.s3 f10 = f35, f4, f5 
	(p1) fpms.s3 f10 = f3, f40, f5 
	(p1) fpms.s3 f10 = f3, f4, f50 

	(p3) fpneg	f10 = f3
	(p23) fpneg	f10 = f3
	(p3) fpneg	f100 = f3
	(p3) fpneg	f10 = f35

	(p4) fpnegabs	f10 = f3
	(p24) fpnegabs	f10 = f3
	(p4) fpnegabs	f100 = f3
	(p4) fpnegabs	f10 = f35

	(p1) fpnma f10 = f3, f4, f5 
	(p17) fpnma f10 = f3, f4, f5 
	(p1) fpnma f100 = f3, f4, f5 
	(p1) fpnma f10 = f35, f4, f5 
	(p1) fpnma f10 = f3, f40, f5 
	(p1) fpnma f10 = f3, f4, f50 
	(p1) fpnma.s0 f10 = f3, f4, f5 
	(p17) fpnma.s0 f10 = f3, f4, f5 
	(p1) fpnma.s0 f100 = f3, f4, f5 
	(p1) fpnma.s0 f10 = f35, f4, f5 
	(p1) fpnma.s0 f10 = f3, f40, f5 
	(p1) fpnma.s0 f10 = f3, f4, f50 
	(p1) fpnma.s1 f10 = f3, f4, f5 
	(p17) fpnma.s1 f10 = f3, f4, f5 
	(p1) fpnma.s1 f100 = f3, f4, f5 
	(p1) fpnma.s1 f10 = f35, f4, f5 
	(p1) fpnma.s1 f10 = f3, f40, f5 
	(p1) fpnma.s1 f10 = f3, f4, f50 
	(p1) fpnma.s2 f10 = f3, f4, f5 
	(p17) fpnma.s2 f10 = f3, f4, f5 
	(p1) fpnma.s2 f100 = f3, f4, f5 
	(p1) fpnma.s2 f10 = f35, f4, f5 
	(p1) fpnma.s2 f10 = f3, f40, f5 
	(p1) fpnma.s2 f10 = f3, f4, f50 
	(p1) fpnma.s3 f10 = f3, f4, f5 
	(p17) fpnma.s3 f10 = f3, f4, f5 
	(p1) fpnma.s3 f100 = f3, f4, f5 
	(p1) fpnma.s3 f10 = f35, f4, f5 
	(p1) fpnma.s3 f10 = f3, f40, f5 
	(p1) fpnma.s3 f10 = f3, f4, f50 


	(p1) fpnmpy f10 = f3, f4 
	(p17) fpnmpy f10 = f3, f4 
	(p1) fpnmpy f100 = f3, f4 
	(p1) fpnmpy f10 = f35, f4 
	(p1) fpnmpy f10 = f3, f40 
	(p1) fpnmpy.s0 f10 = f3, f4 
	(p17) fpnmpy.s0 f10 = f3, f4 
	(p1) fpnmpy.s0 f100 = f3, f4 
	(p1) fpnmpy.s0 f10 = f35, f4 
	(p1) fpnmpy.s0 f10 = f3, f40 
	(p1) fpnmpy.s1 f10 = f3, f4 
	(p17) fpnmpy.s1 f10 = f3, f4 
	(p1) fpnmpy.s1 f100 = f3, f4 
	(p1) fpnmpy.s1 f10 = f35, f4 
	(p1) fpnmpy.s1 f10 = f3, f40 
	(p1) fpnmpy.s2 f10 = f3, f4 
	(p17) fpnmpy.s2 f10 = f3, f4 
	(p1) fpnmpy.s2 f100 = f3, f4 
	(p1) fpnmpy.s2 f10 = f35, f4 
	(p1) fpnmpy.s2 f10 = f3, f40 
	(p1) fpnmpy.s3 f10 = f3, f4 
	(p17) fpnmpy.s3 f10 = f3, f4 
	(p1) fpnmpy.s3 f100 = f3, f4 
	(p1) fpnmpy.s3 f10 = f35, f4 
	(p1) fpnmpy.s3 f10 = f3, f40 

	(p3) fprcpa	f10,p2 = f2, f3
	(p23) fprcpa	f10,p2 = f2, f3
	(p3) fprcpa	f100,p2 = f2, f3
	(p3) fprcpa	f10,p20 = f2, f3
	(p3) fprcpa	f10,p2 = f34, f3
	(p3) fprcpa	f10,p2 = f2, f35
	(p3) fprcpa.s0	f10,p2 = f2, f3
	(p23) fprcpa.s0	f10,p2 = f2, f3
	(p3) fprcpa.s0	f100,p2 = f2, f3
	(p3) fprcpa.s0	f10,p20 = f2, f3
	(p3) fprcpa.s0	f10,p2 = f34, f3
	(p3) fprcpa.s0	f10,p2 = f2, f35
	(p3) fprcpa.s1	f10,p2 = f2, f3
	(p23) fprcpa.s1	f10,p2 = f2, f3
	(p3) fprcpa.s1	f100,p2 = f2, f3
	(p3) fprcpa.s1	f10,p20 = f2, f3
	(p3) fprcpa.s1	f10,p2 = f34, f3
	(p3) fprcpa.s1	f10,p2 = f2, f35
	(p3) fprcpa.s2	f10,p2 = f2, f3
	(p23) fprcpa.s2	f10,p2 = f2, f3
	(p3) fprcpa.s2	f100,p2 = f2, f3
	(p3) fprcpa.s2	f10,p20 = f2, f3
	(p3) fprcpa.s2	f10,p2 = f34, f3
	(p3) fprcpa.s2	f10,p2 = f2, f35
	(p3) fprcpa.s3	f10,p2 = f2, f3
	(p23) fprcpa.s3	f10,p2 = f2, f3
	(p3) fprcpa.s3	f100,p2 = f2, f3
	(p3) fprcpa.s3	f10,p20 = f2, f3
	(p3) fprcpa.s3	f10,p2 = f34, f3
	(p3) fprcpa.s3	f10,p2 = f2, f35

	(p3) fprsqrta	f10,p2 = f2
	(p23) fprsqrta	f10,p2 = f2
	(p3) fprsqrta	f100,p2 = f2
	(p3) fprsqrta	f10,p20 = f2
	(p3) fprsqrta	f10,p2 = f34
	(p3) fprsqrta.s0	f10,p2 = f2
	(p23) fprsqrta.s0	f10,p2 = f2
	(p3) fprsqrta.s0	f100,p2 = f2
	(p3) fprsqrta.s0	f10,p20 = f2
	(p3) fprsqrta.s0	f10,p2 = f34
	(p3) fprsqrta.s1	f10,p2 = f2
	(p23) fprsqrta.s1	f10,p2 = f2
	(p3) fprsqrta.s1	f100,p2 = f2
	(p3) fprsqrta.s1	f10,p20 = f2
	(p3) fprsqrta.s1	f10,p2 = f34
	(p3) fprsqrta.s2	f10,p2 = f2
	(p23) fprsqrta.s2	f10,p2 = f2
	(p3) fprsqrta.s2	f100,p2 = f2
	(p3) fprsqrta.s2	f10,p20 = f2
	(p3) fprsqrta.s2	f10,p2 = f34
	(p3) fprsqrta.s3	f10,p2 = f2
	(p23) fprsqrta.s3	f10,p2 = f2
	(p3) fprsqrta.s3	f100,p2 = f2
	(p3) fprsqrta.s3	f10,p20 = f2
	(p3) fprsqrta.s3	f10,p2 = f34

	(p3) frcpa	f10,p2 = f2, f3
	(p23) frcpa	f10,p2 = f2, f3
	(p3) frcpa	f100,p2 = f2, f3
	(p3) frcpa	f10,p20 = f2, f3
	(p3) frcpa	f10,p2 = f34, f3
	(p3) frcpa	f10,p2 = f2, f35
	(p3) frcpa.s0	f10,p2 = f2, f3
	(p23) frcpa.s0	f10,p2 = f2, f3
	(p3) frcpa.s0	f100,p2 = f2, f3
	(p3) frcpa.s0	f10,p20 = f2, f3
	(p3) frcpa.s0	f10,p2 = f34, f3
	(p3) frcpa.s0	f10,p2 = f2, f35
	(p3) frcpa.s1	f10,p2 = f2, f3
	(p23) frcpa.s1	f10,p2 = f2, f3
	(p3) frcpa.s1	f100,p2 = f2, f3
	(p3) frcpa.s1	f10,p20 = f2, f3
	(p3) frcpa.s1	f10,p2 = f34, f3
	(p3) frcpa.s1	f10,p2 = f2, f35
	(p3) frcpa.s2	f10,p2 = f2, f3
	(p23) frcpa.s2	f10,p2 = f2, f3
	(p3) frcpa.s2	f100,p2 = f2, f3
	(p3) frcpa.s2	f10,p20 = f2, f3
	(p3) frcpa.s2	f10,p2 = f34, f3
	(p3) frcpa.s2	f10,p2 = f2, f35
	(p3) frcpa.s3	f10,p2 = f2, f3
	(p23) frcpa.s3	f10,p2 = f2, f3
	(p3) frcpa.s3	f100,p2 = f2, f3
	(p3) frcpa.s3	f10,p20 = f2, f3
	(p3) frcpa.s3	f10,p2 = f34, f3
	(p3) frcpa.s3	f10,p2 = f2, f35

	(p3) frsqrta	f10,p2 = f2
	(p23) frsqrta	f10,p2 = f2
	(p3) frsqrta	f100,p2 = f2
	(p3) frsqrta	f10,p20 = f2
	(p3) frsqrta	f10,p2 = f34
	(p3) frsqrta.s0	f10,p2 = f2
	(p23) frsqrta.s0	f10,p2 = f2
	(p3) frsqrta.s0	f100,p2 = f2
	(p3) frsqrta.s0	f10,p20 = f2
	(p3) frsqrta.s0	f10,p2 = f34
	(p3) frsqrta.s1	f10,p2 = f2
	(p23) frsqrta.s1	f10,p2 = f2
	(p3) frsqrta.s1	f100,p2 = f2
	(p3) frsqrta.s1	f10,p20 = f2
	(p3) frsqrta.s1	f10,p2 = f34
	(p3) frsqrta.s2	f10,p2 = f2
	(p23) frsqrta.s2	f10,p2 = f2
	(p3) frsqrta.s2	f100,p2 = f2
	(p3) frsqrta.s2	f10,p20 = f2
	(p3) frsqrta.s2	f10,p2 = f34
	(p3) frsqrta.s3	f10,p2 = f2
	(p23) frsqrta.s3	f10,p2 = f2
	(p3) frsqrta.s3	f100,p2 = f2
	(p3) frsqrta.s3	f10,p20 = f2
	(p3) frsqrta.s3	f10,p2 = f34


	(p8) fselect	f10 = f3, f4, f2
	(p28) fselect	f10 = f3, f4, f2
	(p8) fselect	f100 = f3, f4, f2
	(p8) fselect	f10 = f35, f4, f2
	(p8) fselect	f10 = f3, f40, f2
	(p8) fselect	f10 = f3, f4, f34

	(p9) fsetc	0x7f, 0x41
	(p29) fsetc	0x7f, 0x41
	(p10) fsetc.s0	0x7f, 0x41
	(p30) fsetc.s0	0x7f, 0x41
	(p11) fsetc.s1	0x7f, 0x41
	(p31) fsetc.s1	0x7f, 0x41
	(p12) fsetc.s2	0x7f, 0x41
	(p32) fsetc.s2	0x7f, 0x41
	(p13) fsetc.s3	0x7f, 0x41
	(p33) fsetc.s3	0x7f, 0x41

	(p1) fsub   f10 = f2, f3 
	(p51) fsub   f10 = f2, f3 
	(p1) fsub   f100 = f2, f3 
	(p1) fsub   f10 = f34, f3 
	(p1) fsub   f10 = f2, f35 
	(p1) fsub.s0   f10 = f2, f3 
	(p51) fsub.s0   f10 = f2, f3 
	(p1) fsub.s0   f100 = f2, f3 
	(p1) fsub.s0   f10 = f34, f3 
	(p1) fsub.s0   f10 = f2, f35 
	(p1) fsub.s1   f10 = f2, f3 
	(p51) fsub.s1   f10 = f2, f3 
	(p1) fsub.s1   f100 = f2, f3 
	(p1) fsub.s1   f10 = f34, f3 
	(p1) fsub.s1   f10 = f2, f35 
	(p1) fsub.s2   f10 = f2, f3 
	(p51) fsub.s2   f10 = f2, f3 
	(p1) fsub.s2   f100 = f2, f3 
	(p1) fsub.s2   f10 = f34, f3 
	(p1) fsub.s2   f10 = f2, f35 
	(p1) fsub.s3   f10 = f2, f3 
	(p51) fsub.s3   f10 = f2, f3 
	(p1) fsub.s3   f100 = f2, f3 
	(p1) fsub.s3   f10 = f34, f3 
	(p1) fsub.s3   f10 = f2, f35 
	(p1) fsub.s   f10 = f2, f3 
	(p51) fsub.s   f10 = f2, f3 
	(p1) fsub.s   f100 = f2, f3 
	(p1) fsub.s   f10 = f34, f3 
	(p1) fsub.s   f10 = f2, f35 
	(p1) fsub.s.s0   f10 = f2, f3 
	(p51) fsub.s.s0   f10 = f2, f3 
	(p1) fsub.s.s0   f100 = f2, f3 
	(p1) fsub.s.s0   f10 = f34, f3 
	(p1) fsub.s.s0   f10 = f2, f35 
	(p1) fsub.s.s1   f10 = f2, f3 
	(p51) fsub.s.s1   f10 = f2, f3 
	(p1) fsub.s.s1   f100 = f2, f3 
	(p1) fsub.s.s1   f10 = f34, f3 
	(p1) fsub.s.s1   f10 = f2, f35 
	(p1) fsub.s.s2   f10 = f2, f3 
	(p51) fsub.s.s2   f10 = f2, f3 
	(p1) fsub.s.s2   f100 = f2, f3 
	(p1) fsub.s.s2   f10 = f34, f3 
	(p1) fsub.s.s2   f10 = f2, f35 
	(p1) fsub.s.s3   f10 = f2, f3 
	(p51) fsub.s.s3   f10 = f2, f3 
	(p1) fsub.s.s3   f100 = f2, f3 
	(p1) fsub.s.s3   f10 = f34, f3 
	(p1) fsub.s.s3   f10 = f2, f35 
	(p1) fsub.d   f10 = f2, f3 
	(p51) fsub.d   f10 = f2, f3 
	(p1) fsub.d   f100 = f2, f3 
	(p1) fsub.d   f10 = f34, f3 
	(p1) fsub.d   f10 = f2, f35 
	(p1) fsub.d.s0   f10 = f2, f3 
	(p51) fsub.d.s0   f10 = f2, f3 
	(p1) fsub.d.s0   f100 = f2, f3 
	(p1) fsub.d.s0   f10 = f34, f3 
	(p1) fsub.d.s0   f10 = f2, f35 
	(p1) fsub.d.s1   f10 = f2, f3 
	(p51) fsub.d.s1   f10 = f2, f3 
	(p1) fsub.d.s1   f100 = f2, f3 
	(p1) fsub.d.s1   f10 = f34, f3 
	(p1) fsub.d.s1   f10 = f2, f35 
	(p1) fsub.d.s2   f10 = f2, f3 
	(p51) fsub.d.s2   f10 = f2, f3 
	(p1) fsub.d.s2   f100 = f2, f3 
	(p1) fsub.d.s2   f10 = f34, f3 
	(p1) fsub.d.s2   f10 = f2, f35 
	(p1) fsub.d.s3   f10 = f2, f3 
	(p51) fsub.d.s3   f10 = f2, f3 
	(p1) fsub.d.s3   f100 = f2, f3 
	(p1) fsub.d.s3   f10 = f34, f3 
	(p1) fsub.d.s3   f10 = f2, f35 

	(p2) fswap	f10 = f2, f3
	(p52) fswap	f10 = f2, f3
	(p2) fswap	f100 = f2, f3
	(p2) fswap	f10 = f34, f3
	(p2) fswap	f10 = f2, f35
	(p3) fswap.nl	f10 = f2, f3
	(p53) fswap.nl	f10 = f2, f3
	(p3) fswap.nl	f100 = f2, f3
	(p3) fswap.nl	f10 = f34, f3
	(p3) fswap.nl	f10 = f2, f35
	(p4) fswap.nr	f10 = f2, f3
	(p54) fswap.nr	f10 = f2, f3
	(p4) fswap.nr	f100 = f2, f3
	(p4) fswap.nr	f10 = f34, f3
	(p4) fswap.nr	f10 = f2, f35

	(p5) fsxt.l f10 = f2, f3
	(p55) fsxt.l f10 = f2, f3
	(p5) fsxt.l f100 = f2, f3
	(p5) fsxt.l f10 = f34, f3
	(p5) fsxt.l f10 = f2, f35
	(p6) fsxt.r f10 = f2, f3
	(p56) fsxt.r f10 = f2, f3
	(p6) fsxt.r f100 = f2, f3
	(p6) fsxt.r f10 = f34, f3
	(p6) fsxt.r f10 = f2, f35

	(p7) fwb
	(p57) fwb

	(p8) fxor	f10 = f2, f3
	(p58) fxor	f10 = f2, f3
	(p8) fxor	f100 = f2, f3
	(p8) fxor	f10 = f34, f3
	(p8) fxor	f10 = f2, f35

	//// ******** G *********

	(p9) getf.s r1 = f2
	(p59) getf.s r1 = f2
	(p9) getf.s r100 = f2
	(p9) getf.s r1 = f34
	(p6) getf.d r1 = f2
	(p60) getf.d r1 = f2
	(p6) getf.d r100 = f2
	(p6) getf.d r1 = f34
	(p1) getf.exp r1 = f2
	(p61) getf.exp r1 = f2
	(p1) getf.exp r100 = f2
	(p1) getf.exp r1 = f34
	(p2) getf.sig r1 = f2
	(p62) getf.sig r1 = f2
	(p2) getf.sig r100 = f2
	(p2) getf.sig r1 = f20

	//// ******** H *********

	//// ******** I *********

	(p3) invala 	;;
	(p63) invala 	;;

	(p0) invala.e	r1 
	(p16) invala.e	r1 
	(p1) invala.e   f1 
	(p17) invala.e   f1 

	(p2) itc.i	r2
	(p20) itc.i	r2
	(p2) itc.i	r34
	(p3) itc.d	r2
	(p30) itc.d	r2
	(p3) itc.d	r34

	(p4) itr.i	itr[r3]=r2
	(p40) itr.i	itr[r3]=r2
	(p4) itr.i	itr[r35]=r2
	(p4) itr.i	itr[r3]=r34
	(p5) itr.d	dtr[r3]=r2
	(p50) itr.d	dtr[r3]=r2
	(p5) itr.d	dtr[r35]=r2
	(p5) itr.d	dtr[r3]=r34

	//// ******** J *********

	//// ******** K *********

	//// ******** L *********

	(p0) ld1		r1 = [r3]
	(p16) ld1		r1 = [r3]
	(p0) ld1		r100 = [r3]
	(p0) ld1		r1 = [r103]
	(p1) ld1		r1 = [r3],r2
	(p41) ld1		r1 = [r3],r2
	(p1) ld1		r110 = [r3],r2
	(p1) ld1		r1 = [r93],r2
	(p1) ld1		r1 = [r3],r92
	(p2) ld1		r1 = [r3],9
	(p32) ld1		r1 = [r3],9
	(p2) ld1		r120 = [r3],9
	(p2) ld1		r1 = [r63],9
	(p0) ld1.nt1		r1 = [r3]
	(p16) ld1.nt1		r1 = [r3]
	(p0) ld1.nt1		r100 = [r3]
	(p0) ld1.nt1		r1 = [r103]
	(p1) ld1.nt1		r1 = [r3],r2
	(p41) ld1.nt1		r1 = [r3],r2
	(p1) ld1.nt1		r110 = [r3],r2
	(p1) ld1.nt1		r1 = [r93],r2
	(p1) ld1.nt1		r1 = [r3],r92
	(p2) ld1.nt1		r1 = [r3],9
	(p32) ld1.nt1		r1 = [r3],9
	(p2) ld1.nt1		r120 = [r3],9
	(p2) ld1.nt1		r1 = [r63],9
	(p0) ld1.nta		r1 = [r3]
	(p16) ld1.nta		r1 = [r3]
	(p0) ld1.nta		r100 = [r3]
	(p0) ld1.nta		r1 = [r103]
	(p1) ld1.nta		r1 = [r3],r2
	(p41) ld1.nta		r1 = [r3],r2
	(p1) ld1.nta		r110 = [r3],r2
	(p1) ld1.nta		r1 = [r93],r2
	(p1) ld1.nta		r1 = [r3],r92
	(p2) ld1.nta		r1 = [r3],9
	(p32) ld1.nta		r1 = [r3],9
	(p2) ld1.nta		r120 = [r3],9
	(p2) ld1.nta		r1 = [r63],9
	(p0) ld1.s		r1 = [r3]
	(p16) ld1.s		r1 = [r3]
	(p0) ld1.s		r100 = [r3]
	(p0) ld1.s		r1 = [r103]
	(p1) ld1.s		r1 = [r3],r2
	(p41) ld1.s		r1 = [r3],r2
	(p1) ld1.s		r110 = [r3],r2
	(p1) ld1.s		r1 = [r93],r2
	(p1) ld1.s		r1 = [r3],r92
	(p2) ld1.s		r1 = [r3],9
	(p32) ld1.s		r1 = [r3],9
	(p2) ld1.s		r120 = [r3],9
	(p2) ld1.s		r1 = [r63],9
	(p0) ld1.s.nt1		r1 = [r3]
	(p16) ld1.s.nt1		r1 = [r3]
	(p0) ld1.s.nt1		r100 = [r3]
	(p0) ld1.s.nt1		r1 = [r103]
	(p1) ld1.s.nt1		r1 = [r3],r2
	(p41) ld1.s.nt1		r1 = [r3],r2
	(p1) ld1.s.nt1		r110 = [r3],r2
	(p1) ld1.s.nt1		r1 = [r93],r2
	(p1) ld1.s.nt1		r1 = [r3],r92
	(p2) ld1.s.nt1		r1 = [r3],9
	(p32) ld1.s.nt1		r1 = [r3],9
	(p2) ld1.s.nt1		r120 = [r3],9
	(p2) ld1.s.nt1		r1 = [r63],9
	(p0) ld1.s.nta		r1 = [r3]
	(p16) ld1.s.nta		r1 = [r3]
	(p0) ld1.s.nta		r100 = [r3]
	(p0) ld1.s.nta		r1 = [r103]
	(p1) ld1.s.nta		r1 = [r3],r2
	(p41) ld1.s.nta		r1 = [r3],r2
	(p1) ld1.s.nta		r110 = [r3],r2
	(p1) ld1.s.nta		r1 = [r93],r2
	(p1) ld1.s.nta		r1 = [r3],r92
	(p2) ld1.s.nta		r1 = [r3],9
	(p32) ld1.s.nta		r1 = [r3],9
	(p2) ld1.s.nta		r120 = [r3],9
	(p2) ld1.s.nta		r1 = [r63],9
	(p0) ld1.a		r1 = [r3]
	(p16) ld1.a		r1 = [r3]
	(p0) ld1.a		r100 = [r3]
	(p0) ld1.a		r1 = [r103]
	(p1) ld1.a		r1 = [r3],r2
	(p41) ld1.a		r1 = [r3],r2
	(p1) ld1.a		r110 = [r3],r2
	(p1) ld1.a		r1 = [r93],r2
	(p1) ld1.a		r1 = [r3],r92
	(p2) ld1.a		r1 = [r3],9
	(p32) ld1.a		r1 = [r3],9
	(p2) ld1.a		r120 = [r3],9
	(p2) ld1.a		r1 = [r63],9
	(p0) ld1.a.nt1		r1 = [r3]
	(p16) ld1.a.nt1		r1 = [r3]
	(p0) ld1.a.nt1		r100 = [r3]
	(p0) ld1.a.nt1		r1 = [r103]
	(p1) ld1.a.nt1		r1 = [r3],r2
	(p41) ld1.a.nt1		r1 = [r3],r2
	(p1) ld1.a.nt1		r110 = [r3],r2
	(p1) ld1.a.nt1		r1 = [r93],r2
	(p1) ld1.a.nt1		r1 = [r3],r92
	(p2) ld1.a.nt1		r1 = [r3],9
	(p32) ld1.a.nt1		r1 = [r3],9
	(p2) ld1.a.nt1		r120 = [r3],9
	(p2) ld1.a.nt1		r1 = [r63],9
	(p0) ld1.a.nta		r1 = [r3]
	(p16) ld1.a.nta		r1 = [r3]
	(p0) ld1.a.nta		r100 = [r3]
	(p0) ld1.a.nta		r1 = [r103]
	(p1) ld1.a.nta		r1 = [r3],r2
	(p41) ld1.a.nta		r1 = [r3],r2
	(p1) ld1.a.nta		r110 = [r3],r2
	(p1) ld1.a.nta		r1 = [r93],r2
	(p1) ld1.a.nta		r1 = [r3],r92
	(p2) ld1.a.nta		r1 = [r3],9
	(p32) ld1.a.nta		r1 = [r3],9
	(p2) ld1.a.nta		r120 = [r3],9
	(p2) ld1.a.nta		r1 = [r63],9
	(p0) ld1.sa		r1 = [r3]
	(p16) ld1.sa		r1 = [r3]
	(p0) ld1.sa		r100 = [r3]
	(p0) ld1.sa		r1 = [r103]
	(p1) ld1.sa		r1 = [r3],r2
	(p41) ld1.sa		r1 = [r3],r2
	(p1) ld1.sa		r110 = [r3],r2
	(p1) ld1.sa		r1 = [r93],r2
	(p1) ld1.sa		r1 = [r3],r92
	(p2) ld1.sa		r1 = [r3],9
	(p32) ld1.sa		r1 = [r3],9
	(p2) ld1.sa		r120 = [r3],9
	(p2) ld1.sa		r1 = [r63],9
	(p0) ld1.sa.nt1		r1 = [r3]
	(p16) ld1.sa.nt1		r1 = [r3]
	(p0) ld1.sa.nt1		r100 = [r3]
	(p0) ld1.sa.nt1		r1 = [r103]
	(p1) ld1.sa.nt1		r1 = [r3],r2
	(p41) ld1.sa.nt1		r1 = [r3],r2
	(p1) ld1.sa.nt1		r110 = [r3],r2
	(p1) ld1.sa.nt1		r1 = [r93],r2
	(p1) ld1.sa.nt1		r1 = [r3],r92
	(p2) ld1.sa.nt1		r1 = [r3],9
	(p32) ld1.sa.nt1		r1 = [r3],9
	(p2) ld1.sa.nt1		r120 = [r3],9
	(p2) ld1.sa.nt1		r1 = [r63],9
	(p0) ld1.sa.nta		r1 = [r3]
	(p16) ld1.sa.nta		r1 = [r3]
	(p0) ld1.sa.nta		r100 = [r3]
	(p0) ld1.sa.nta		r1 = [r103]
	(p1) ld1.sa.nta		r1 = [r3],r2
	(p41) ld1.sa.nta		r1 = [r3],r2
	(p1) ld1.sa.nta		r110 = [r3],r2
	(p1) ld1.sa.nta		r1 = [r93],r2
	(p1) ld1.sa.nta		r1 = [r3],r92
	(p2) ld1.sa.nta		r1 = [r3],9
	(p32) ld1.sa.nta		r1 = [r3],9
	(p2) ld1.sa.nta		r120 = [r3],9
	(p2) ld1.sa.nta		r1 = [r63],9
	(p0) ld1.c.nc		r1 = [r3]
	(p16) ld1.c.nc		r1 = [r3]
	(p0) ld1.c.nc		r100 = [r3]
	(p0) ld1.c.nc		r1 = [r103]
	(p1) ld1.c.nc		r1 = [r3],r2
	(p41) ld1.c.nc		r1 = [r3],r2
	(p1) ld1.c.nc		r110 = [r3],r2
	(p1) ld1.c.nc		r1 = [r93],r2
	(p1) ld1.c.nc		r1 = [r3],r92
	(p2) ld1.c.nc		r1 = [r3],9
	(p32) ld1.c.nc		r1 = [r3],9
	(p2) ld1.c.nc		r120 = [r3],9
	(p2) ld1.c.nc		r1 = [r63],9
	(p0) ld1.c.nc.nt1		r1 = [r3]
	(p16) ld1.c.nc.nt1		r1 = [r3]
	(p0) ld1.c.nc.nt1		r100 = [r3]
	(p0) ld1.c.nc.nt1		r1 = [r103]
	(p1) ld1.c.nc.nt1		r1 = [r3],r2
	(p41) ld1.c.nc.nt1		r1 = [r3],r2
	(p1) ld1.c.nc.nt1		r110 = [r3],r2
	(p1) ld1.c.nc.nt1		r1 = [r93],r2
	(p1) ld1.c.nc.nt1		r1 = [r3],r92
	(p2) ld1.c.nc.nt1		r1 = [r3],9
	(p32) ld1.c.nc.nt1		r1 = [r3],9
	(p2) ld1.c.nc.nt1		r120 = [r3],9
	(p2) ld1.c.nc.nt1		r1 = [r63],9
	(p0) ld1.c.nc.nta		r1 = [r3]
	(p16) ld1.c.nc.nta		r1 = [r3]
	(p0) ld1.c.nc.nta		r100 = [r3]
	(p0) ld1.c.nc.nta		r1 = [r103]
	(p1) ld1.c.nc.nta		r1 = [r3],r2
	(p41) ld1.c.nc.nta		r1 = [r3],r2
	(p1) ld1.c.nc.nta		r110 = [r3],r2
	(p1) ld1.c.nc.nta		r1 = [r93],r2
	(p1) ld1.c.nc.nta		r1 = [r3],r92
	(p2) ld1.c.nc.nta		r1 = [r3],9
	(p32) ld1.c.nc.nta		r1 = [r3],9
	(p2) ld1.c.nc.nta		r120 = [r3],9
	(p2) ld1.c.nc.nta		r1 = [r63],9
	(p0) ld1.c.clr		r1 = [r3]
	(p16) ld1.c.clr		r1 = [r3]
	(p0) ld1.c.clr		r100 = [r3]
	(p0) ld1.c.clr		r1 = [r103]
	(p1) ld1.c.clr		r1 = [r3],r2
	(p41) ld1.c.clr		r1 = [r3],r2
	(p1) ld1.c.clr		r110 = [r3],r2
	(p1) ld1.c.clr		r1 = [r93],r2
	(p1) ld1.c.clr		r1 = [r3],r92
	(p2) ld1.c.clr		r1 = [r3],9
	(p32) ld1.c.clr		r1 = [r3],9
	(p2) ld1.c.clr		r120 = [r3],9
	(p2) ld1.c.clr		r1 = [r63],9
	(p0) ld1.c.clr.nt1		r1 = [r3]
	(p16) ld1.c.clr.nt1		r1 = [r3]
	(p0) ld1.c.clr.nt1		r100 = [r3]
	(p0) ld1.c.clr.nt1		r1 = [r103]
	(p1) ld1.c.clr.nt1		r1 = [r3],r2
	(p41) ld1.c.clr.nt1		r1 = [r3],r2
	(p1) ld1.c.clr.nt1		r110 = [r3],r2
	(p1) ld1.c.clr.nt1		r1 = [r93],r2
	(p1) ld1.c.clr.nt1		r1 = [r3],r92
	(p2) ld1.c.clr.nt1		r1 = [r3],9
	(p32) ld1.c.clr.nt1		r1 = [r3],9
	(p2) ld1.c.clr.nt1		r120 = [r3],9
	(p2) ld1.c.clr.nt1		r1 = [r63],9
	(p0) ld1.c.clr.nta		r1 = [r3]
	(p16) ld1.c.clr.nta		r1 = [r3]
	(p0) ld1.c.clr.nta		r100 = [r3]
	(p0) ld1.c.clr.nta		r1 = [r103]
	(p1) ld1.c.clr.nta		r1 = [r3],r2
	(p41) ld1.c.clr.nta		r1 = [r3],r2
	(p1) ld1.c.clr.nta		r110 = [r3],r2
	(p1) ld1.c.clr.nta		r1 = [r93],r2
	(p1) ld1.c.clr.nta		r1 = [r3],r92
	(p2) ld1.c.clr.nta		r1 = [r3],9
	(p32) ld1.c.clr.nta		r1 = [r3],9
	(p2) ld1.c.clr.nta		r120 = [r3],9
	(p2) ld1.c.clr.nta		r1 = [r63],9
	(p0) ld1.acq		r1 = [r3]
	(p16) ld1.acq		r1 = [r3]
	(p0) ld1.acq		r100 = [r3]
	(p0) ld1.acq		r1 = [r103]
	(p1) ld1.acq		r1 = [r3],r2
	(p41) ld1.acq		r1 = [r3],r2
	(p1) ld1.acq		r110 = [r3],r2
	(p1) ld1.acq		r1 = [r93],r2
	(p1) ld1.acq		r1 = [r3],r92
	(p2) ld1.acq		r1 = [r3],9
	(p32) ld1.acq		r1 = [r3],9
	(p2) ld1.acq		r120 = [r3],9
	(p2) ld1.acq		r1 = [r63],9
	(p0) ld1.acq.nt1		r1 = [r3]
	(p16) ld1.acq.nt1		r1 = [r3]
	(p0) ld1.acq.nt1		r100 = [r3]
	(p0) ld1.acq.nt1		r1 = [r103]
	(p1) ld1.acq.nt1		r1 = [r3],r2
	(p41) ld1.acq.nt1		r1 = [r3],r2
	(p1) ld1.acq.nt1		r110 = [r3],r2
	(p1) ld1.acq.nt1		r1 = [r93],r2
	(p1) ld1.acq.nt1		r1 = [r3],r92
	(p2) ld1.acq.nt1		r1 = [r3],9
	(p32) ld1.acq.nt1		r1 = [r3],9
	(p2) ld1.acq.nt1		r120 = [r3],9
	(p2) ld1.acq.nt1		r1 = [r63],9
	(p0) ld1.acq.nta		r1 = [r3]
	(p16) ld1.acq.nta		r1 = [r3]
	(p0) ld1.acq.nta		r100 = [r3]
	(p0) ld1.acq.nta		r1 = [r103]
	(p1) ld1.acq.nta		r1 = [r3],r2
	(p41) ld1.acq.nta		r1 = [r3],r2
	(p1) ld1.acq.nta		r110 = [r3],r2
	(p1) ld1.acq.nta		r1 = [r93],r2
	(p1) ld1.acq.nta		r1 = [r3],r92
	(p2) ld1.acq.nta		r1 = [r3],9
	(p32) ld1.acq.nta		r1 = [r3],9
	(p2) ld1.acq.nta		r120 = [r3],9
	(p2) ld1.acq.nta		r1 = [r63],9
	(p0) ld1.bias		r1 = [r3]
	(p16) ld1.bias		r1 = [r3]
	(p0) ld1.bias		r100 = [r3]
	(p0) ld1.bias		r1 = [r103]
	(p1) ld1.bias		r1 = [r3],r2
	(p41) ld1.bias		r1 = [r3],r2
	(p1) ld1.bias		r110 = [r3],r2
	(p1) ld1.bias		r1 = [r93],r2
	(p1) ld1.bias		r1 = [r3],r92
	(p2) ld1.bias		r1 = [r3],9
	(p32) ld1.bias		r1 = [r3],9
	(p2) ld1.bias		r120 = [r3],9
	(p2) ld1.bias		r1 = [r63],9
	(p0) ld1.bias.nt1		r1 = [r3]
	(p16) ld1.bias.nt1		r1 = [r3]
	(p0) ld1.bias.nt1		r100 = [r3]
	(p0) ld1.bias.nt1		r1 = [r103]
	(p1) ld1.bias.nt1		r1 = [r3],r2
	(p41) ld1.bias.nt1		r1 = [r3],r2
	(p1) ld1.bias.nt1		r110 = [r3],r2
	(p1) ld1.bias.nt1		r1 = [r93],r2
	(p1) ld1.bias.nt1		r1 = [r3],r92
	(p2) ld1.bias.nt1		r1 = [r3],9
	(p32) ld1.bias.nt1		r1 = [r3],9
	(p2) ld1.bias.nt1		r120 = [r3],9
	(p2) ld1.bias.nt1		r1 = [r63],9
	(p0) ld1.bias.nta		r1 = [r3]
	(p16) ld1.bias.nta		r1 = [r3]
	(p0) ld1.bias.nta		r100 = [r3]
	(p0) ld1.bias.nta		r1 = [r103]
	(p1) ld1.bias.nta		r1 = [r3],r2
	(p41) ld1.bias.nta		r1 = [r3],r2
	(p1) ld1.bias.nta		r110 = [r3],r2
	(p1) ld1.bias.nta		r1 = [r93],r2
	(p1) ld1.bias.nta		r1 = [r3],r92
	(p2) ld1.bias.nta		r1 = [r3],9
	(p32) ld1.bias.nta		r1 = [r3],9
	(p2) ld1.bias.nta		r120 = [r3],9
	(p2) ld1.bias.nta		r1 = [r63],9

	(p0) ld2		r1 = [r3]
	(p16) ld2		r1 = [r3]
	(p0) ld2		r100 = [r3]
	(p0) ld2		r1 = [r103]
	(p1) ld2		r1 = [r3],r2
	(p41) ld2		r1 = [r3],r2
	(p1) ld2		r110 = [r3],r2
	(p1) ld2		r1 = [r93],r2
	(p1) ld2		r1 = [r3],r92
	(p2) ld2		r1 = [r3],9
	(p32) ld2		r1 = [r3],9
	(p2) ld2		r120 = [r3],9
	(p2) ld2		r1 = [r63],9
	(p0) ld2.nt1		r1 = [r3]
	(p16) ld2.nt1		r1 = [r3]
	(p0) ld2.nt1		r100 = [r3]
	(p0) ld2.nt1		r1 = [r103]
	(p1) ld2.nt1		r1 = [r3],r2
	(p41) ld2.nt1		r1 = [r3],r2
	(p1) ld2.nt1		r110 = [r3],r2
	(p1) ld2.nt1		r1 = [r93],r2
	(p1) ld2.nt1		r1 = [r3],r92
	(p2) ld2.nt1		r1 = [r3],9
	(p32) ld2.nt1		r1 = [r3],9
	(p2) ld2.nt1		r120 = [r3],9
	(p2) ld2.nt1		r1 = [r63],9
	(p0) ld2.nta		r1 = [r3]
	(p16) ld2.nta		r1 = [r3]
	(p0) ld2.nta		r100 = [r3]
	(p0) ld2.nta		r1 = [r103]
	(p1) ld2.nta		r1 = [r3],r2
	(p41) ld2.nta		r1 = [r3],r2
	(p1) ld2.nta		r110 = [r3],r2
	(p1) ld2.nta		r1 = [r93],r2
	(p1) ld2.nta		r1 = [r3],r92
	(p2) ld2.nta		r1 = [r3],9
	(p32) ld2.nta		r1 = [r3],9
	(p2) ld2.nta		r120 = [r3],9
	(p2) ld2.nta		r1 = [r63],9
	(p0) ld2.s		r1 = [r3]
	(p16) ld2.s		r1 = [r3]
	(p0) ld2.s		r100 = [r3]
	(p0) ld2.s		r1 = [r103]
	(p1) ld2.s		r1 = [r3],r2
	(p41) ld2.s		r1 = [r3],r2
	(p1) ld2.s		r110 = [r3],r2
	(p1) ld2.s		r1 = [r93],r2
	(p1) ld2.s		r1 = [r3],r92
	(p2) ld2.s		r1 = [r3],9
	(p32) ld2.s		r1 = [r3],9
	(p2) ld2.s		r120 = [r3],9
	(p2) ld2.s		r1 = [r63],9
	(p0) ld2.s.nt1		r1 = [r3]
	(p16) ld2.s.nt1		r1 = [r3]
	(p0) ld2.s.nt1		r100 = [r3]
	(p0) ld2.s.nt1		r1 = [r103]
	(p1) ld2.s.nt1		r1 = [r3],r2
	(p41) ld2.s.nt1		r1 = [r3],r2
	(p1) ld2.s.nt1		r110 = [r3],r2
	(p1) ld2.s.nt1		r1 = [r93],r2
	(p1) ld2.s.nt1		r1 = [r3],r92
	(p2) ld2.s.nt1		r1 = [r3],9
	(p32) ld2.s.nt1		r1 = [r3],9
	(p2) ld2.s.nt1		r120 = [r3],9
	(p2) ld2.s.nt1		r1 = [r63],9
	(p0) ld2.s.nta		r1 = [r3]
	(p16) ld2.s.nta		r1 = [r3]
	(p0) ld2.s.nta		r100 = [r3]
	(p0) ld2.s.nta		r1 = [r103]
	(p1) ld2.s.nta		r1 = [r3],r2
	(p41) ld2.s.nta		r1 = [r3],r2
	(p1) ld2.s.nta		r110 = [r3],r2
	(p1) ld2.s.nta		r1 = [r93],r2
	(p1) ld2.s.nta		r1 = [r3],r92
	(p2) ld2.s.nta		r1 = [r3],9
	(p32) ld2.s.nta		r1 = [r3],9
	(p2) ld2.s.nta		r120 = [r3],9
	(p2) ld2.s.nta		r1 = [r63],9
	(p0) ld2.a		r1 = [r3]
	(p16) ld2.a		r1 = [r3]
	(p0) ld2.a		r100 = [r3]
	(p0) ld2.a		r1 = [r103]
	(p1) ld2.a		r1 = [r3],r2
	(p41) ld2.a		r1 = [r3],r2
	(p1) ld2.a		r110 = [r3],r2
	(p1) ld2.a		r1 = [r93],r2
	(p1) ld2.a		r1 = [r3],r92
	(p2) ld2.a		r1 = [r3],9
	(p32) ld2.a		r1 = [r3],9
	(p2) ld2.a		r120 = [r3],9
	(p2) ld2.a		r1 = [r63],9
	(p0) ld2.a.nt1		r1 = [r3]
	(p16) ld2.a.nt1		r1 = [r3]
	(p0) ld2.a.nt1		r100 = [r3]
	(p0) ld2.a.nt1		r1 = [r103]
	(p1) ld2.a.nt1		r1 = [r3],r2
	(p41) ld2.a.nt1		r1 = [r3],r2
	(p1) ld2.a.nt1		r110 = [r3],r2
	(p1) ld2.a.nt1		r1 = [r93],r2
	(p1) ld2.a.nt1		r1 = [r3],r92
	(p2) ld2.a.nt1		r1 = [r3],9
	(p32) ld2.a.nt1		r1 = [r3],9
	(p2) ld2.a.nt1		r120 = [r3],9
	(p2) ld2.a.nt1		r1 = [r63],9
	(p0) ld2.a.nta		r1 = [r3]
	(p16) ld2.a.nta		r1 = [r3]
	(p0) ld2.a.nta		r100 = [r3]
	(p0) ld2.a.nta		r1 = [r103]
	(p1) ld2.a.nta		r1 = [r3],r2
	(p41) ld2.a.nta		r1 = [r3],r2
	(p1) ld2.a.nta		r110 = [r3],r2
	(p1) ld2.a.nta		r1 = [r93],r2
	(p1) ld2.a.nta		r1 = [r3],r92
	(p2) ld2.a.nta		r1 = [r3],9
	(p32) ld2.a.nta		r1 = [r3],9
	(p2) ld2.a.nta		r120 = [r3],9
	(p2) ld2.a.nta		r1 = [r63],9
	(p0) ld2.sa		r1 = [r3]
	(p16) ld2.sa		r1 = [r3]
	(p0) ld2.sa		r100 = [r3]
	(p0) ld2.sa		r1 = [r103]
	(p1) ld2.sa		r1 = [r3],r2
	(p41) ld2.sa		r1 = [r3],r2
	(p1) ld2.sa		r110 = [r3],r2
	(p1) ld2.sa		r1 = [r93],r2
	(p1) ld2.sa		r1 = [r3],r92
	(p2) ld2.sa		r1 = [r3],9
	(p32) ld2.sa		r1 = [r3],9
	(p2) ld2.sa		r120 = [r3],9
	(p2) ld2.sa		r1 = [r63],9
	(p0) ld2.sa.nt1		r1 = [r3]
	(p16) ld2.sa.nt1		r1 = [r3]
	(p0) ld2.sa.nt1		r100 = [r3]
	(p0) ld2.sa.nt1		r1 = [r103]
	(p1) ld2.sa.nt1		r1 = [r3],r2
	(p41) ld2.sa.nt1		r1 = [r3],r2
	(p1) ld2.sa.nt1		r110 = [r3],r2
	(p1) ld2.sa.nt1		r1 = [r93],r2
	(p1) ld2.sa.nt1		r1 = [r3],r92
	(p2) ld2.sa.nt1		r1 = [r3],9
	(p32) ld2.sa.nt1		r1 = [r3],9
	(p2) ld2.sa.nt1		r120 = [r3],9
	(p2) ld2.sa.nt1		r1 = [r63],9
	(p0) ld2.sa.nta		r1 = [r3]
	(p16) ld2.sa.nta		r1 = [r3]
	(p0) ld2.sa.nta		r100 = [r3]
	(p0) ld2.sa.nta		r1 = [r103]
	(p1) ld2.sa.nta		r1 = [r3],r2
	(p41) ld2.sa.nta		r1 = [r3],r2
	(p1) ld2.sa.nta		r110 = [r3],r2
	(p1) ld2.sa.nta		r1 = [r93],r2
	(p1) ld2.sa.nta		r1 = [r3],r92
	(p2) ld2.sa.nta		r1 = [r3],9
	(p32) ld2.sa.nta		r1 = [r3],9
	(p2) ld2.sa.nta		r120 = [r3],9
	(p2) ld2.sa.nta		r1 = [r63],9
	(p0) ld2.c.nc		r1 = [r3]
	(p16) ld2.c.nc		r1 = [r3]
	(p0) ld2.c.nc		r100 = [r3]
	(p0) ld2.c.nc		r1 = [r103]
	(p1) ld2.c.nc		r1 = [r3],r2
	(p41) ld2.c.nc		r1 = [r3],r2
	(p1) ld2.c.nc		r110 = [r3],r2
	(p1) ld2.c.nc		r1 = [r93],r2
	(p1) ld2.c.nc		r1 = [r3],r92
	(p2) ld2.c.nc		r1 = [r3],9
	(p32) ld2.c.nc		r1 = [r3],9
	(p2) ld2.c.nc		r120 = [r3],9
	(p2) ld2.c.nc		r1 = [r63],9
	(p0) ld2.c.nc.nt1		r1 = [r3]
	(p16) ld2.c.nc.nt1		r1 = [r3]
	(p0) ld2.c.nc.nt1		r100 = [r3]
	(p0) ld2.c.nc.nt1		r1 = [r103]
	(p1) ld2.c.nc.nt1		r1 = [r3],r2
	(p41) ld2.c.nc.nt1		r1 = [r3],r2
	(p1) ld2.c.nc.nt1		r110 = [r3],r2
	(p1) ld2.c.nc.nt1		r1 = [r93],r2
	(p1) ld2.c.nc.nt1		r1 = [r3],r92
	(p2) ld2.c.nc.nt1		r1 = [r3],9
	(p32) ld2.c.nc.nt1		r1 = [r3],9
	(p2) ld2.c.nc.nt1		r120 = [r3],9
	(p2) ld2.c.nc.nt1		r1 = [r63],9
	(p0) ld2.c.nc.nta		r1 = [r3]
	(p16) ld2.c.nc.nta		r1 = [r3]
	(p0) ld2.c.nc.nta		r100 = [r3]
	(p0) ld2.c.nc.nta		r1 = [r103]
	(p1) ld2.c.nc.nta		r1 = [r3],r2
	(p41) ld2.c.nc.nta		r1 = [r3],r2
	(p1) ld2.c.nc.nta		r110 = [r3],r2
	(p1) ld2.c.nc.nta		r1 = [r93],r2
	(p1) ld2.c.nc.nta		r1 = [r3],r92
	(p2) ld2.c.nc.nta		r1 = [r3],9
	(p32) ld2.c.nc.nta		r1 = [r3],9
	(p2) ld2.c.nc.nta		r120 = [r3],9
	(p2) ld2.c.nc.nta		r1 = [r63],9
	(p0) ld2.c.clr		r1 = [r3]
	(p16) ld2.c.clr		r1 = [r3]
	(p0) ld2.c.clr		r100 = [r3]
	(p0) ld2.c.clr		r1 = [r103]
	(p1) ld2.c.clr		r1 = [r3],r2
	(p41) ld2.c.clr		r1 = [r3],r2
	(p1) ld2.c.clr		r110 = [r3],r2
	(p1) ld2.c.clr		r1 = [r93],r2
	(p1) ld2.c.clr		r1 = [r3],r92
	(p2) ld2.c.clr		r1 = [r3],9
	(p32) ld2.c.clr		r1 = [r3],9
	(p2) ld2.c.clr		r120 = [r3],9
	(p2) ld2.c.clr		r1 = [r63],9
	(p0) ld2.c.clr.nt1		r1 = [r3]
	(p16) ld2.c.clr.nt1		r1 = [r3]
	(p0) ld2.c.clr.nt1		r100 = [r3]
	(p0) ld2.c.clr.nt1		r1 = [r103]
	(p1) ld2.c.clr.nt1		r1 = [r3],r2
	(p41) ld2.c.clr.nt1		r1 = [r3],r2
	(p1) ld2.c.clr.nt1		r110 = [r3],r2
	(p1) ld2.c.clr.nt1		r1 = [r93],r2
	(p1) ld2.c.clr.nt1		r1 = [r3],r92
	(p2) ld2.c.clr.nt1		r1 = [r3],9
	(p32) ld2.c.clr.nt1		r1 = [r3],9
	(p2) ld2.c.clr.nt1		r120 = [r3],9
	(p2) ld2.c.clr.nt1		r1 = [r63],9
	(p0) ld2.c.clr.nta		r1 = [r3]
	(p16) ld2.c.clr.nta		r1 = [r3]
	(p0) ld2.c.clr.nta		r100 = [r3]
	(p0) ld2.c.clr.nta		r1 = [r103]
	(p1) ld2.c.clr.nta		r1 = [r3],r2
	(p41) ld2.c.clr.nta		r1 = [r3],r2
	(p1) ld2.c.clr.nta		r110 = [r3],r2
	(p1) ld2.c.clr.nta		r1 = [r93],r2
	(p1) ld2.c.clr.nta		r1 = [r3],r92
	(p2) ld2.c.clr.nta		r1 = [r3],9
	(p32) ld2.c.clr.nta		r1 = [r3],9
	(p2) ld2.c.clr.nta		r120 = [r3],9
	(p2) ld2.c.clr.nta		r1 = [r63],9
	(p0) ld2.acq		r1 = [r3]
	(p16) ld2.acq		r1 = [r3]
	(p0) ld2.acq		r100 = [r3]
	(p0) ld2.acq		r1 = [r103]
	(p1) ld2.acq		r1 = [r3],r2
	(p41) ld2.acq		r1 = [r3],r2
	(p1) ld2.acq		r110 = [r3],r2
	(p1) ld2.acq		r1 = [r93],r2
	(p1) ld2.acq		r1 = [r3],r92
	(p2) ld2.acq		r1 = [r3],9
	(p32) ld2.acq		r1 = [r3],9
	(p2) ld2.acq		r120 = [r3],9
	(p2) ld2.acq		r1 = [r63],9
	(p0) ld2.acq.nt1		r1 = [r3]
	(p16) ld2.acq.nt1		r1 = [r3]
	(p0) ld2.acq.nt1		r100 = [r3]
	(p0) ld2.acq.nt1		r1 = [r103]
	(p1) ld2.acq.nt1		r1 = [r3],r2
	(p41) ld2.acq.nt1		r1 = [r3],r2
	(p1) ld2.acq.nt1		r110 = [r3],r2
	(p1) ld2.acq.nt1		r1 = [r93],r2
	(p1) ld2.acq.nt1		r1 = [r3],r92
	(p2) ld2.acq.nt1		r1 = [r3],9
	(p32) ld2.acq.nt1		r1 = [r3],9
	(p2) ld2.acq.nt1		r120 = [r3],9
	(p2) ld2.acq.nt1		r1 = [r63],9
	(p0) ld2.acq.nta		r1 = [r3]
	(p16) ld2.acq.nta		r1 = [r3]
	(p0) ld2.acq.nta		r100 = [r3]
	(p0) ld2.acq.nta		r1 = [r103]
	(p1) ld2.acq.nta		r1 = [r3],r2
	(p41) ld2.acq.nta		r1 = [r3],r2
	(p1) ld2.acq.nta		r110 = [r3],r2
	(p1) ld2.acq.nta		r1 = [r93],r2
	(p1) ld2.acq.nta		r1 = [r3],r92
	(p2) ld2.acq.nta		r1 = [r3],9
	(p32) ld2.acq.nta		r1 = [r3],9
	(p2) ld2.acq.nta		r120 = [r3],9
	(p2) ld2.acq.nta		r1 = [r63],9
	(p0) ld2.bias		r1 = [r3]
	(p16) ld2.bias		r1 = [r3]
	(p0) ld2.bias		r100 = [r3]
	(p0) ld2.bias		r1 = [r103]
	(p1) ld2.bias		r1 = [r3],r2
	(p41) ld2.bias		r1 = [r3],r2
	(p1) ld2.bias		r110 = [r3],r2
	(p1) ld2.bias		r1 = [r93],r2
	(p1) ld2.bias		r1 = [r3],r92
	(p2) ld2.bias		r1 = [r3],9
	(p32) ld2.bias		r1 = [r3],9
	(p2) ld2.bias		r120 = [r3],9
	(p2) ld2.bias		r1 = [r63],9
	(p0) ld2.bias.nt1		r1 = [r3]
	(p16) ld2.bias.nt1		r1 = [r3]
	(p0) ld2.bias.nt1		r100 = [r3]
	(p0) ld2.bias.nt1		r1 = [r103]
	(p1) ld2.bias.nt1		r1 = [r3],r2
	(p41) ld2.bias.nt1		r1 = [r3],r2
	(p1) ld2.bias.nt1		r110 = [r3],r2
	(p1) ld2.bias.nt1		r1 = [r93],r2
	(p1) ld2.bias.nt1		r1 = [r3],r92
	(p2) ld2.bias.nt1		r1 = [r3],9
	(p32) ld2.bias.nt1		r1 = [r3],9
	(p2) ld2.bias.nt1		r120 = [r3],9
	(p2) ld2.bias.nt1		r1 = [r63],9
	(p0) ld2.bias.nta		r1 = [r3]
	(p16) ld2.bias.nta		r1 = [r3]
	(p0) ld2.bias.nta		r100 = [r3]
	(p0) ld2.bias.nta		r1 = [r103]
	(p1) ld2.bias.nta		r1 = [r3],r2
	(p41) ld2.bias.nta		r1 = [r3],r2
	(p1) ld2.bias.nta		r110 = [r3],r2
	(p1) ld2.bias.nta		r1 = [r93],r2
	(p1) ld2.bias.nta		r1 = [r3],r92
	(p2) ld2.bias.nta		r1 = [r3],9
	(p32) ld2.bias.nta		r1 = [r3],9
	(p2) ld2.bias.nta		r120 = [r3],9
	(p2) ld2.bias.nta		r1 = [r63],9
	(p0) ld4		r1 = [r3]
	(p16) ld4		r1 = [r3]
	(p0) ld4		r100 = [r3]
	(p0) ld4		r1 = [r103]
	(p1) ld4		r1 = [r3],r2
	(p41) ld4		r1 = [r3],r2
	(p1) ld4		r110 = [r3],r2
	(p1) ld4		r1 = [r93],r2
	(p1) ld4		r1 = [r3],r92
	(p2) ld4		r1 = [r3],9
	(p32) ld4		r1 = [r3],9
	(p2) ld4		r120 = [r3],9
	(p2) ld4		r1 = [r63],9
	(p0) ld4.nt1		r1 = [r3]
	(p16) ld4.nt1		r1 = [r3]
	(p0) ld4.nt1		r100 = [r3]
	(p0) ld4.nt1		r1 = [r103]
	(p1) ld4.nt1		r1 = [r3],r2
	(p41) ld4.nt1		r1 = [r3],r2
	(p1) ld4.nt1		r110 = [r3],r2
	(p1) ld4.nt1		r1 = [r93],r2
	(p1) ld4.nt1		r1 = [r3],r92
	(p2) ld4.nt1		r1 = [r3],9
	(p32) ld4.nt1		r1 = [r3],9
	(p2) ld4.nt1		r120 = [r3],9
	(p2) ld4.nt1		r1 = [r63],9
	(p0) ld4.nta		r1 = [r3]
	(p16) ld4.nta		r1 = [r3]
	(p0) ld4.nta		r100 = [r3]
	(p0) ld4.nta		r1 = [r103]
	(p1) ld4.nta		r1 = [r3],r2
	(p41) ld4.nta		r1 = [r3],r2
	(p1) ld4.nta		r110 = [r3],r2
	(p1) ld4.nta		r1 = [r93],r2
	(p1) ld4.nta		r1 = [r3],r92
	(p2) ld4.nta		r1 = [r3],9
	(p32) ld4.nta		r1 = [r3],9
	(p2) ld4.nta		r120 = [r3],9
	(p2) ld4.nta		r1 = [r63],9
	(p0) ld4.s		r1 = [r3]
	(p16) ld4.s		r1 = [r3]
	(p0) ld4.s		r100 = [r3]
	(p0) ld4.s		r1 = [r103]
	(p1) ld4.s		r1 = [r3],r2
	(p41) ld4.s		r1 = [r3],r2
	(p1) ld4.s		r110 = [r3],r2
	(p1) ld4.s		r1 = [r93],r2
	(p1) ld4.s		r1 = [r3],r92
	(p2) ld4.s		r1 = [r3],9
	(p32) ld4.s		r1 = [r3],9
	(p2) ld4.s		r120 = [r3],9
	(p2) ld4.s		r1 = [r63],9
	(p0) ld4.s.nt1		r1 = [r3]
	(p16) ld4.s.nt1		r1 = [r3]
	(p0) ld4.s.nt1		r100 = [r3]
	(p0) ld4.s.nt1		r1 = [r103]
	(p1) ld4.s.nt1		r1 = [r3],r2
	(p41) ld4.s.nt1		r1 = [r3],r2
	(p1) ld4.s.nt1		r110 = [r3],r2
	(p1) ld4.s.nt1		r1 = [r93],r2
	(p1) ld4.s.nt1		r1 = [r3],r92
	(p2) ld4.s.nt1		r1 = [r3],9
	(p32) ld4.s.nt1		r1 = [r3],9
	(p2) ld4.s.nt1		r120 = [r3],9
	(p2) ld4.s.nt1		r1 = [r63],9
	(p0) ld4.s.nta		r1 = [r3]
	(p16) ld4.s.nta		r1 = [r3]
	(p0) ld4.s.nta		r100 = [r3]
	(p0) ld4.s.nta		r1 = [r103]
	(p1) ld4.s.nta		r1 = [r3],r2
	(p41) ld4.s.nta		r1 = [r3],r2
	(p1) ld4.s.nta		r110 = [r3],r2
	(p1) ld4.s.nta		r1 = [r93],r2
	(p1) ld4.s.nta		r1 = [r3],r92
	(p2) ld4.s.nta		r1 = [r3],9
	(p32) ld4.s.nta		r1 = [r3],9
	(p2) ld4.s.nta		r120 = [r3],9
	(p2) ld4.s.nta		r1 = [r63],9
	(p0) ld4.a		r1 = [r3]
	(p16) ld4.a		r1 = [r3]
	(p0) ld4.a		r100 = [r3]
	(p0) ld4.a		r1 = [r103]
	(p1) ld4.a		r1 = [r3],r2
	(p41) ld4.a		r1 = [r3],r2
	(p1) ld4.a		r110 = [r3],r2
	(p1) ld4.a		r1 = [r93],r2
	(p1) ld4.a		r1 = [r3],r92
	(p2) ld4.a		r1 = [r3],9
	(p32) ld4.a		r1 = [r3],9
	(p2) ld4.a		r120 = [r3],9
	(p2) ld4.a		r1 = [r63],9
	(p0) ld4.a.nt1		r1 = [r3]
	(p16) ld4.a.nt1		r1 = [r3]
	(p0) ld4.a.nt1		r100 = [r3]
	(p0) ld4.a.nt1		r1 = [r103]
	(p1) ld4.a.nt1		r1 = [r3],r2
	(p41) ld4.a.nt1		r1 = [r3],r2
	(p1) ld4.a.nt1		r110 = [r3],r2
	(p1) ld4.a.nt1		r1 = [r93],r2
	(p1) ld4.a.nt1		r1 = [r3],r92
	(p2) ld4.a.nt1		r1 = [r3],9
	(p32) ld4.a.nt1		r1 = [r3],9
	(p2) ld4.a.nt1		r120 = [r3],9
	(p2) ld4.a.nt1		r1 = [r63],9
	(p0) ld4.a.nta		r1 = [r3]
	(p16) ld4.a.nta		r1 = [r3]
	(p0) ld4.a.nta		r100 = [r3]
	(p0) ld4.a.nta		r1 = [r103]
	(p1) ld4.a.nta		r1 = [r3],r2
	(p41) ld4.a.nta		r1 = [r3],r2
	(p1) ld4.a.nta		r110 = [r3],r2
	(p1) ld4.a.nta		r1 = [r93],r2
	(p1) ld4.a.nta		r1 = [r3],r92
	(p2) ld4.a.nta		r1 = [r3],9
	(p32) ld4.a.nta		r1 = [r3],9
	(p2) ld4.a.nta		r120 = [r3],9
	(p2) ld4.a.nta		r1 = [r63],9
	(p0) ld4.sa		r1 = [r3]
	(p16) ld4.sa		r1 = [r3]
	(p0) ld4.sa		r100 = [r3]
	(p0) ld4.sa		r1 = [r103]
	(p1) ld4.sa		r1 = [r3],r2
	(p41) ld4.sa		r1 = [r3],r2
	(p1) ld4.sa		r110 = [r3],r2
	(p1) ld4.sa		r1 = [r93],r2
	(p1) ld4.sa		r1 = [r3],r92
	(p2) ld4.sa		r1 = [r3],9
	(p32) ld4.sa		r1 = [r3],9
	(p2) ld4.sa		r120 = [r3],9
	(p2) ld4.sa		r1 = [r63],9
	(p0) ld4.sa.nt1		r1 = [r3]
	(p16) ld4.sa.nt1		r1 = [r3]
	(p0) ld4.sa.nt1		r100 = [r3]
	(p0) ld4.sa.nt1		r1 = [r103]
	(p1) ld4.sa.nt1		r1 = [r3],r2
	(p41) ld4.sa.nt1		r1 = [r3],r2
	(p1) ld4.sa.nt1		r110 = [r3],r2
	(p1) ld4.sa.nt1		r1 = [r93],r2
	(p1) ld4.sa.nt1		r1 = [r3],r92
	(p2) ld4.sa.nt1		r1 = [r3],9
	(p32) ld4.sa.nt1		r1 = [r3],9
	(p2) ld4.sa.nt1		r120 = [r3],9
	(p2) ld4.sa.nt1		r1 = [r63],9
	(p0) ld4.sa.nta		r1 = [r3]
	(p16) ld4.sa.nta		r1 = [r3]
	(p0) ld4.sa.nta		r100 = [r3]
	(p0) ld4.sa.nta		r1 = [r103]
	(p1) ld4.sa.nta		r1 = [r3],r2
	(p41) ld4.sa.nta		r1 = [r3],r2
	(p1) ld4.sa.nta		r110 = [r3],r2
	(p1) ld4.sa.nta		r1 = [r93],r2
	(p1) ld4.sa.nta		r1 = [r3],r92
	(p2) ld4.sa.nta		r1 = [r3],9
	(p32) ld4.sa.nta		r1 = [r3],9
	(p2) ld4.sa.nta		r120 = [r3],9
	(p2) ld4.sa.nta		r1 = [r63],9
	(p0) ld4.c.nc		r1 = [r3]
	(p16) ld4.c.nc		r1 = [r3]
	(p0) ld4.c.nc		r100 = [r3]
	(p0) ld4.c.nc		r1 = [r103]
	(p1) ld4.c.nc		r1 = [r3],r2
	(p41) ld4.c.nc		r1 = [r3],r2
	(p1) ld4.c.nc		r110 = [r3],r2
	(p1) ld4.c.nc		r1 = [r93],r2
	(p1) ld4.c.nc		r1 = [r3],r92
	(p2) ld4.c.nc		r1 = [r3],9
	(p32) ld4.c.nc		r1 = [r3],9
	(p2) ld4.c.nc		r120 = [r3],9
	(p2) ld4.c.nc		r1 = [r63],9
	(p0) ld4.c.nc.nt1		r1 = [r3]
	(p16) ld4.c.nc.nt1		r1 = [r3]
	(p0) ld4.c.nc.nt1		r100 = [r3]
	(p0) ld4.c.nc.nt1		r1 = [r103]
	(p1) ld4.c.nc.nt1		r1 = [r3],r2
	(p41) ld4.c.nc.nt1		r1 = [r3],r2
	(p1) ld4.c.nc.nt1		r110 = [r3],r2
	(p1) ld4.c.nc.nt1		r1 = [r93],r2
	(p1) ld4.c.nc.nt1		r1 = [r3],r92
	(p2) ld4.c.nc.nt1		r1 = [r3],9
	(p32) ld4.c.nc.nt1		r1 = [r3],9
	(p2) ld4.c.nc.nt1		r120 = [r3],9
	(p2) ld4.c.nc.nt1		r1 = [r63],9
	(p0) ld4.c.nc.nta		r1 = [r3]
	(p16) ld4.c.nc.nta		r1 = [r3]
	(p0) ld4.c.nc.nta		r100 = [r3]
	(p0) ld4.c.nc.nta		r1 = [r103]
	(p1) ld4.c.nc.nta		r1 = [r3],r2
	(p41) ld4.c.nc.nta		r1 = [r3],r2
	(p1) ld4.c.nc.nta		r110 = [r3],r2
	(p1) ld4.c.nc.nta		r1 = [r93],r2
	(p1) ld4.c.nc.nta		r1 = [r3],r92
	(p2) ld4.c.nc.nta		r1 = [r3],9
	(p32) ld4.c.nc.nta		r1 = [r3],9
	(p2) ld4.c.nc.nta		r120 = [r3],9
	(p2) ld4.c.nc.nta		r1 = [r63],9
	(p0) ld4.c.clr		r1 = [r3]
	(p16) ld4.c.clr		r1 = [r3]
	(p0) ld4.c.clr		r100 = [r3]
	(p0) ld4.c.clr		r1 = [r103]
	(p1) ld4.c.clr		r1 = [r3],r2
	(p41) ld4.c.clr		r1 = [r3],r2
	(p1) ld4.c.clr		r110 = [r3],r2
	(p1) ld4.c.clr		r1 = [r93],r2
	(p1) ld4.c.clr		r1 = [r3],r92
	(p2) ld4.c.clr		r1 = [r3],9
	(p32) ld4.c.clr		r1 = [r3],9
	(p2) ld4.c.clr		r120 = [r3],9
	(p2) ld4.c.clr		r1 = [r63],9
	(p0) ld4.c.clr.nt1		r1 = [r3]
	(p16) ld4.c.clr.nt1		r1 = [r3]
	(p0) ld4.c.clr.nt1		r100 = [r3]
	(p0) ld4.c.clr.nt1		r1 = [r103]
	(p1) ld4.c.clr.nt1		r1 = [r3],r2
	(p41) ld4.c.clr.nt1		r1 = [r3],r2
	(p1) ld4.c.clr.nt1		r110 = [r3],r2
	(p1) ld4.c.clr.nt1		r1 = [r93],r2
	(p1) ld4.c.clr.nt1		r1 = [r3],r92
	(p2) ld4.c.clr.nt1		r1 = [r3],9
	(p32) ld4.c.clr.nt1		r1 = [r3],9
	(p2) ld4.c.clr.nt1		r120 = [r3],9
	(p2) ld4.c.clr.nt1		r1 = [r63],9
	(p0) ld4.c.clr.nta		r1 = [r3]
	(p16) ld4.c.clr.nta		r1 = [r3]
	(p0) ld4.c.clr.nta		r100 = [r3]
	(p0) ld4.c.clr.nta		r1 = [r103]
	(p1) ld4.c.clr.nta		r1 = [r3],r2
	(p41) ld4.c.clr.nta		r1 = [r3],r2
	(p1) ld4.c.clr.nta		r110 = [r3],r2
	(p1) ld4.c.clr.nta		r1 = [r93],r2
	(p1) ld4.c.clr.nta		r1 = [r3],r92
	(p2) ld4.c.clr.nta		r1 = [r3],9
	(p32) ld4.c.clr.nta		r1 = [r3],9
	(p2) ld4.c.clr.nta		r120 = [r3],9
	(p2) ld4.c.clr.nta		r1 = [r63],9
	(p0) ld4.acq		r1 = [r3]
	(p16) ld4.acq		r1 = [r3]
	(p0) ld4.acq		r100 = [r3]
	(p0) ld4.acq		r1 = [r103]
	(p1) ld4.acq		r1 = [r3],r2
	(p41) ld4.acq		r1 = [r3],r2
	(p1) ld4.acq		r110 = [r3],r2
	(p1) ld4.acq		r1 = [r93],r2
	(p1) ld4.acq		r1 = [r3],r92
	(p2) ld4.acq		r1 = [r3],9
	(p32) ld4.acq		r1 = [r3],9
	(p2) ld4.acq		r120 = [r3],9
	(p2) ld4.acq		r1 = [r63],9
	(p0) ld4.acq.nt1		r1 = [r3]
	(p16) ld4.acq.nt1		r1 = [r3]
	(p0) ld4.acq.nt1		r100 = [r3]
	(p0) ld4.acq.nt1		r1 = [r103]
	(p1) ld4.acq.nt1		r1 = [r3],r2
	(p41) ld4.acq.nt1		r1 = [r3],r2
	(p1) ld4.acq.nt1		r110 = [r3],r2
	(p1) ld4.acq.nt1		r1 = [r93],r2
	(p1) ld4.acq.nt1		r1 = [r3],r92
	(p2) ld4.acq.nt1		r1 = [r3],9
	(p32) ld4.acq.nt1		r1 = [r3],9
	(p2) ld4.acq.nt1		r120 = [r3],9
	(p2) ld4.acq.nt1		r1 = [r63],9
	(p0) ld4.acq.nta		r1 = [r3]
	(p16) ld4.acq.nta		r1 = [r3]
	(p0) ld4.acq.nta		r100 = [r3]
	(p0) ld4.acq.nta		r1 = [r103]
	(p1) ld4.acq.nta		r1 = [r3],r2
	(p41) ld4.acq.nta		r1 = [r3],r2
	(p1) ld4.acq.nta		r110 = [r3],r2
	(p1) ld4.acq.nta		r1 = [r93],r2
	(p1) ld4.acq.nta		r1 = [r3],r92
	(p2) ld4.acq.nta		r1 = [r3],9
	(p32) ld4.acq.nta		r1 = [r3],9
	(p2) ld4.acq.nta		r120 = [r3],9
	(p2) ld4.acq.nta		r1 = [r63],9
	(p0) ld4.bias		r1 = [r3]
	(p16) ld4.bias		r1 = [r3]
	(p0) ld4.bias		r100 = [r3]
	(p0) ld4.bias		r1 = [r103]
	(p1) ld4.bias		r1 = [r3],r2
	(p41) ld4.bias		r1 = [r3],r2
	(p1) ld4.bias		r110 = [r3],r2
	(p1) ld4.bias		r1 = [r93],r2
	(p1) ld4.bias		r1 = [r3],r92
	(p2) ld4.bias		r1 = [r3],9
	(p32) ld4.bias		r1 = [r3],9
	(p2) ld4.bias		r120 = [r3],9
	(p2) ld4.bias		r1 = [r63],9
	(p0) ld4.bias.nt1		r1 = [r3]
	(p16) ld4.bias.nt1		r1 = [r3]
	(p0) ld4.bias.nt1		r100 = [r3]
	(p0) ld4.bias.nt1		r1 = [r103]
	(p1) ld4.bias.nt1		r1 = [r3],r2
	(p41) ld4.bias.nt1		r1 = [r3],r2
	(p1) ld4.bias.nt1		r110 = [r3],r2
	(p1) ld4.bias.nt1		r1 = [r93],r2
	(p1) ld4.bias.nt1		r1 = [r3],r92
	(p2) ld4.bias.nt1		r1 = [r3],9
	(p32) ld4.bias.nt1		r1 = [r3],9
	(p2) ld4.bias.nt1		r120 = [r3],9
	(p2) ld4.bias.nt1		r1 = [r63],9
	(p0) ld4.bias.nta		r1 = [r3]
	(p16) ld4.bias.nta		r1 = [r3]
	(p0) ld4.bias.nta		r100 = [r3]
	(p0) ld4.bias.nta		r1 = [r103]
	(p1) ld4.bias.nta		r1 = [r3],r2
	(p41) ld4.bias.nta		r1 = [r3],r2
	(p1) ld4.bias.nta		r110 = [r3],r2
	(p1) ld4.bias.nta		r1 = [r93],r2
	(p1) ld4.bias.nta		r1 = [r3],r92
	(p2) ld4.bias.nta		r1 = [r3],9
	(p32) ld4.bias.nta		r1 = [r3],9
	(p2) ld4.bias.nta		r120 = [r3],9
	(p2) ld4.bias.nta		r1 = [r63],9
	(p0) ld8		r1 = [r3]
	(p16) ld8		r1 = [r3]
	(p0) ld8		r100 = [r3]
	(p0) ld8		r1 = [r103]
	(p1) ld8		r1 = [r3],r2
	(p41) ld8		r1 = [r3],r2
	(p1) ld8		r110 = [r3],r2
	(p1) ld8		r1 = [r93],r2
	(p1) ld8		r1 = [r3],r92
	(p2) ld8		r1 = [r3],9
	(p32) ld8		r1 = [r3],9
	(p2) ld8		r120 = [r3],9
	(p2) ld8		r1 = [r63],9
	(p0) ld8.nt1		r1 = [r3]
	(p16) ld8.nt1		r1 = [r3]
	(p0) ld8.nt1		r100 = [r3]
	(p0) ld8.nt1		r1 = [r103]
	(p1) ld8.nt1		r1 = [r3],r2
	(p41) ld8.nt1		r1 = [r3],r2
	(p1) ld8.nt1		r110 = [r3],r2
	(p1) ld8.nt1		r1 = [r93],r2
	(p1) ld8.nt1		r1 = [r3],r92
	(p2) ld8.nt1		r1 = [r3],9
	(p32) ld8.nt1		r1 = [r3],9
	(p2) ld8.nt1		r120 = [r3],9
	(p2) ld8.nt1		r1 = [r63],9
	(p0) ld8.nta		r1 = [r3]
	(p16) ld8.nta		r1 = [r3]
	(p0) ld8.nta		r100 = [r3]
	(p0) ld8.nta		r1 = [r103]
	(p1) ld8.nta		r1 = [r3],r2
	(p41) ld8.nta		r1 = [r3],r2
	(p1) ld8.nta		r110 = [r3],r2
	(p1) ld8.nta		r1 = [r93],r2
	(p1) ld8.nta		r1 = [r3],r92
	(p2) ld8.nta		r1 = [r3],9
	(p32) ld8.nta		r1 = [r3],9
	(p2) ld8.nta		r120 = [r3],9
	(p2) ld8.nta		r1 = [r63],9
	(p0) ld8.s		r1 = [r3]
	(p16) ld8.s		r1 = [r3]
	(p0) ld8.s		r100 = [r3]
	(p0) ld8.s		r1 = [r103]
	(p1) ld8.s		r1 = [r3],r2
	(p41) ld8.s		r1 = [r3],r2
	(p1) ld8.s		r110 = [r3],r2
	(p1) ld8.s		r1 = [r93],r2
	(p1) ld8.s		r1 = [r3],r92
	(p2) ld8.s		r1 = [r3],9
	(p32) ld8.s		r1 = [r3],9
	(p2) ld8.s		r120 = [r3],9
	(p2) ld8.s		r1 = [r63],9
	(p0) ld8.s.nt1		r1 = [r3]
	(p16) ld8.s.nt1		r1 = [r3]
	(p0) ld8.s.nt1		r100 = [r3]
	(p0) ld8.s.nt1		r1 = [r103]
	(p1) ld8.s.nt1		r1 = [r3],r2
	(p41) ld8.s.nt1		r1 = [r3],r2
	(p1) ld8.s.nt1		r110 = [r3],r2
	(p1) ld8.s.nt1		r1 = [r93],r2
	(p1) ld8.s.nt1		r1 = [r3],r92
	(p2) ld8.s.nt1		r1 = [r3],9
	(p32) ld8.s.nt1		r1 = [r3],9
	(p2) ld8.s.nt1		r120 = [r3],9
	(p2) ld8.s.nt1		r1 = [r63],9
	(p0) ld8.s.nta		r1 = [r3]
	(p16) ld8.s.nta		r1 = [r3]
	(p0) ld8.s.nta		r100 = [r3]
	(p0) ld8.s.nta		r1 = [r103]
	(p1) ld8.s.nta		r1 = [r3],r2
	(p41) ld8.s.nta		r1 = [r3],r2
	(p1) ld8.s.nta		r110 = [r3],r2
	(p1) ld8.s.nta		r1 = [r93],r2
	(p1) ld8.s.nta		r1 = [r3],r92
	(p2) ld8.s.nta		r1 = [r3],9
	(p32) ld8.s.nta		r1 = [r3],9
	(p2) ld8.s.nta		r120 = [r3],9
	(p2) ld8.s.nta		r1 = [r63],9
	(p0) ld8.a		r1 = [r3]
	(p16) ld8.a		r1 = [r3]
	(p0) ld8.a		r100 = [r3]
	(p0) ld8.a		r1 = [r103]
	(p1) ld8.a		r1 = [r3],r2
	(p41) ld8.a		r1 = [r3],r2
	(p1) ld8.a		r110 = [r3],r2
	(p1) ld8.a		r1 = [r93],r2
	(p1) ld8.a		r1 = [r3],r92
	(p2) ld8.a		r1 = [r3],9
	(p32) ld8.a		r1 = [r3],9
	(p2) ld8.a		r120 = [r3],9
	(p2) ld8.a		r1 = [r63],9
	(p0) ld8.a.nt1		r1 = [r3]
	(p16) ld8.a.nt1		r1 = [r3]
	(p0) ld8.a.nt1		r100 = [r3]
	(p0) ld8.a.nt1		r1 = [r103]
	(p1) ld8.a.nt1		r1 = [r3],r2
	(p41) ld8.a.nt1		r1 = [r3],r2
	(p1) ld8.a.nt1		r110 = [r3],r2
	(p1) ld8.a.nt1		r1 = [r93],r2
	(p1) ld8.a.nt1		r1 = [r3],r92
	(p2) ld8.a.nt1		r1 = [r3],9
	(p32) ld8.a.nt1		r1 = [r3],9
	(p2) ld8.a.nt1		r120 = [r3],9
	(p2) ld8.a.nt1		r1 = [r63],9
	(p0) ld8.a.nta		r1 = [r3]
	(p16) ld8.a.nta		r1 = [r3]
	(p0) ld8.a.nta		r100 = [r3]
	(p0) ld8.a.nta		r1 = [r103]
	(p1) ld8.a.nta		r1 = [r3],r2
	(p41) ld8.a.nta		r1 = [r3],r2
	(p1) ld8.a.nta		r110 = [r3],r2
	(p1) ld8.a.nta		r1 = [r93],r2
	(p1) ld8.a.nta		r1 = [r3],r92
	(p2) ld8.a.nta		r1 = [r3],9
	(p32) ld8.a.nta		r1 = [r3],9
	(p2) ld8.a.nta		r120 = [r3],9
	(p2) ld8.a.nta		r1 = [r63],9
	(p0) ld8.sa		r1 = [r3]
	(p16) ld8.sa		r1 = [r3]
	(p0) ld8.sa		r100 = [r3]
	(p0) ld8.sa		r1 = [r103]
	(p1) ld8.sa		r1 = [r3],r2
	(p41) ld8.sa		r1 = [r3],r2
	(p1) ld8.sa		r110 = [r3],r2
	(p1) ld8.sa		r1 = [r93],r2
	(p1) ld8.sa		r1 = [r3],r92
	(p2) ld8.sa		r1 = [r3],9
	(p32) ld8.sa		r1 = [r3],9
	(p2) ld8.sa		r120 = [r3],9
	(p2) ld8.sa		r1 = [r63],9
	(p0) ld8.sa.nt1		r1 = [r3]
	(p16) ld8.sa.nt1		r1 = [r3]
	(p0) ld8.sa.nt1		r100 = [r3]
	(p0) ld8.sa.nt1		r1 = [r103]
	(p1) ld8.sa.nt1		r1 = [r3],r2
	(p41) ld8.sa.nt1		r1 = [r3],r2
	(p1) ld8.sa.nt1		r110 = [r3],r2
	(p1) ld8.sa.nt1		r1 = [r93],r2
	(p1) ld8.sa.nt1		r1 = [r3],r92
	(p2) ld8.sa.nt1		r1 = [r3],9
	(p32) ld8.sa.nt1		r1 = [r3],9
	(p2) ld8.sa.nt1		r120 = [r3],9
	(p2) ld8.sa.nt1		r1 = [r63],9
	(p0) ld8.sa.nta		r1 = [r3]
	(p16) ld8.sa.nta		r1 = [r3]
	(p0) ld8.sa.nta		r100 = [r3]
	(p0) ld8.sa.nta		r1 = [r103]
	(p1) ld8.sa.nta		r1 = [r3],r2
	(p41) ld8.sa.nta		r1 = [r3],r2
	(p1) ld8.sa.nta		r110 = [r3],r2
	(p1) ld8.sa.nta		r1 = [r93],r2
	(p1) ld8.sa.nta		r1 = [r3],r92
	(p2) ld8.sa.nta		r1 = [r3],9
	(p32) ld8.sa.nta		r1 = [r3],9
	(p2) ld8.sa.nta		r120 = [r3],9
	(p2) ld8.sa.nta		r1 = [r63],9
	(p0) ld8.c.nc		r1 = [r3]
	(p16) ld8.c.nc		r1 = [r3]
	(p0) ld8.c.nc		r100 = [r3]
	(p0) ld8.c.nc		r1 = [r103]
	(p1) ld8.c.nc		r1 = [r3],r2
	(p41) ld8.c.nc		r1 = [r3],r2
	(p1) ld8.c.nc		r110 = [r3],r2
	(p1) ld8.c.nc		r1 = [r93],r2
	(p1) ld8.c.nc		r1 = [r3],r92
	(p2) ld8.c.nc		r1 = [r3],9
	(p32) ld8.c.nc		r1 = [r3],9
	(p2) ld8.c.nc		r120 = [r3],9
	(p2) ld8.c.nc		r1 = [r63],9
	(p0) ld8.c.nc.nt1		r1 = [r3]
	(p16) ld8.c.nc.nt1		r1 = [r3]
	(p0) ld8.c.nc.nt1		r100 = [r3]
	(p0) ld8.c.nc.nt1		r1 = [r103]
	(p1) ld8.c.nc.nt1		r1 = [r3],r2
	(p41) ld8.c.nc.nt1		r1 = [r3],r2
	(p1) ld8.c.nc.nt1		r110 = [r3],r2
	(p1) ld8.c.nc.nt1		r1 = [r93],r2
	(p1) ld8.c.nc.nt1		r1 = [r3],r92
	(p2) ld8.c.nc.nt1		r1 = [r3],9
	(p32) ld8.c.nc.nt1		r1 = [r3],9
	(p2) ld8.c.nc.nt1		r120 = [r3],9
	(p2) ld8.c.nc.nt1		r1 = [r63],9
	(p0) ld8.c.nc.nta		r1 = [r3]
	(p16) ld8.c.nc.nta		r1 = [r3]
	(p0) ld8.c.nc.nta		r100 = [r3]
	(p0) ld8.c.nc.nta		r1 = [r103]
	(p1) ld8.c.nc.nta		r1 = [r3],r2
	(p41) ld8.c.nc.nta		r1 = [r3],r2
	(p1) ld8.c.nc.nta		r110 = [r3],r2
	(p1) ld8.c.nc.nta		r1 = [r93],r2
	(p1) ld8.c.nc.nta		r1 = [r3],r92
	(p2) ld8.c.nc.nta		r1 = [r3],9
	(p32) ld8.c.nc.nta		r1 = [r3],9
	(p2) ld8.c.nc.nta		r120 = [r3],9
	(p2) ld8.c.nc.nta		r1 = [r63],9
	(p0) ld8.c.clr		r1 = [r3]
	(p16) ld8.c.clr		r1 = [r3]
	(p0) ld8.c.clr		r100 = [r3]
	(p0) ld8.c.clr		r1 = [r103]
	(p1) ld8.c.clr		r1 = [r3],r2
	(p41) ld8.c.clr		r1 = [r3],r2
	(p1) ld8.c.clr		r110 = [r3],r2
	(p1) ld8.c.clr		r1 = [r93],r2
	(p1) ld8.c.clr		r1 = [r3],r92
	(p2) ld8.c.clr		r1 = [r3],9
	(p32) ld8.c.clr		r1 = [r3],9
	(p2) ld8.c.clr		r120 = [r3],9
	(p2) ld8.c.clr		r1 = [r63],9
	(p0) ld8.c.clr.nt1		r1 = [r3]
	(p16) ld8.c.clr.nt1		r1 = [r3]
	(p0) ld8.c.clr.nt1		r100 = [r3]
	(p0) ld8.c.clr.nt1		r1 = [r103]
	(p1) ld8.c.clr.nt1		r1 = [r3],r2
	(p41) ld8.c.clr.nt1		r1 = [r3],r2
	(p1) ld8.c.clr.nt1		r110 = [r3],r2
	(p1) ld8.c.clr.nt1		r1 = [r93],r2
	(p1) ld8.c.clr.nt1		r1 = [r3],r92
	(p2) ld8.c.clr.nt1		r1 = [r3],9
	(p32) ld8.c.clr.nt1		r1 = [r3],9
	(p2) ld8.c.clr.nt1		r120 = [r3],9
	(p2) ld8.c.clr.nt1		r1 = [r63],9
	(p0) ld8.c.clr.nta		r1 = [r3]
	(p16) ld8.c.clr.nta		r1 = [r3]
	(p0) ld8.c.clr.nta		r100 = [r3]
	(p0) ld8.c.clr.nta		r1 = [r103]
	(p1) ld8.c.clr.nta		r1 = [r3],r2
	(p41) ld8.c.clr.nta		r1 = [r3],r2
	(p1) ld8.c.clr.nta		r110 = [r3],r2
	(p1) ld8.c.clr.nta		r1 = [r93],r2
	(p1) ld8.c.clr.nta		r1 = [r3],r92
	(p2) ld8.c.clr.nta		r1 = [r3],9
	(p32) ld8.c.clr.nta		r1 = [r3],9
	(p2) ld8.c.clr.nta		r120 = [r3],9
	(p2) ld8.c.clr.nta		r1 = [r63],9
	(p0) ld8.acq		r1 = [r3]
	(p16) ld8.acq		r1 = [r3]
	(p0) ld8.acq		r100 = [r3]
	(p0) ld8.acq		r1 = [r103]
	(p1) ld8.acq		r1 = [r3],r2
	(p41) ld8.acq		r1 = [r3],r2
	(p1) ld8.acq		r110 = [r3],r2
	(p1) ld8.acq		r1 = [r93],r2
	(p1) ld8.acq		r1 = [r3],r92
	(p2) ld8.acq		r1 = [r3],9
	(p32) ld8.acq		r1 = [r3],9
	(p2) ld8.acq		r120 = [r3],9
	(p2) ld8.acq		r1 = [r63],9
	(p0) ld8.acq.nt1		r1 = [r3]
	(p16) ld8.acq.nt1		r1 = [r3]
	(p0) ld8.acq.nt1		r100 = [r3]
	(p0) ld8.acq.nt1		r1 = [r103]
	(p1) ld8.acq.nt1		r1 = [r3],r2
	(p41) ld8.acq.nt1		r1 = [r3],r2
	(p1) ld8.acq.nt1		r110 = [r3],r2
	(p1) ld8.acq.nt1		r1 = [r93],r2
	(p1) ld8.acq.nt1		r1 = [r3],r92
	(p2) ld8.acq.nt1		r1 = [r3],9
	(p32) ld8.acq.nt1		r1 = [r3],9
	(p2) ld8.acq.nt1		r120 = [r3],9
	(p2) ld8.acq.nt1		r1 = [r63],9
	(p0) ld8.acq.nta		r1 = [r3]
	(p16) ld8.acq.nta		r1 = [r3]
	(p0) ld8.acq.nta		r100 = [r3]
	(p0) ld8.acq.nta		r1 = [r103]
	(p1) ld8.acq.nta		r1 = [r3],r2
	(p41) ld8.acq.nta		r1 = [r3],r2
	(p1) ld8.acq.nta		r110 = [r3],r2
	(p1) ld8.acq.nta		r1 = [r93],r2
	(p1) ld8.acq.nta		r1 = [r3],r92
	(p2) ld8.acq.nta		r1 = [r3],9
	(p32) ld8.acq.nta		r1 = [r3],9
	(p2) ld8.acq.nta		r120 = [r3],9
	(p2) ld8.acq.nta		r1 = [r63],9
	(p0) ld8.bias		r1 = [r3]
	(p16) ld8.bias		r1 = [r3]
	(p0) ld8.bias		r100 = [r3]
	(p0) ld8.bias		r1 = [r103]
	(p1) ld8.bias		r1 = [r3],r2
	(p41) ld8.bias		r1 = [r3],r2
	(p1) ld8.bias		r110 = [r3],r2
	(p1) ld8.bias		r1 = [r93],r2
	(p1) ld8.bias		r1 = [r3],r92
	(p2) ld8.bias		r1 = [r3],9
	(p32) ld8.bias		r1 = [r3],9
	(p2) ld8.bias		r120 = [r3],9
	(p2) ld8.bias		r1 = [r63],9
	(p0) ld8.bias.nt1		r1 = [r3]
	(p16) ld8.bias.nt1		r1 = [r3]
	(p0) ld8.bias.nt1		r100 = [r3]
	(p0) ld8.bias.nt1		r1 = [r103]
	(p1) ld8.bias.nt1		r1 = [r3],r2
	(p41) ld8.bias.nt1		r1 = [r3],r2
	(p1) ld8.bias.nt1		r110 = [r3],r2
	(p1) ld8.bias.nt1		r1 = [r93],r2
	(p1) ld8.bias.nt1		r1 = [r3],r92
	(p2) ld8.bias.nt1		r1 = [r3],9
	(p32) ld8.bias.nt1		r1 = [r3],9
	(p2) ld8.bias.nt1		r120 = [r3],9
	(p2) ld8.bias.nt1		r1 = [r63],9
	(p0) ld8.bias.nta		r1 = [r3]
	(p16) ld8.bias.nta		r1 = [r3]
	(p0) ld8.bias.nta		r100 = [r3]
	(p0) ld8.bias.nta		r1 = [r103]
	(p1) ld8.bias.nta		r1 = [r3],r2
	(p41) ld8.bias.nta		r1 = [r3],r2
	(p1) ld8.bias.nta		r110 = [r3],r2
	(p1) ld8.bias.nta		r1 = [r93],r2
	(p1) ld8.bias.nta		r1 = [r3],r92
	(p2) ld8.bias.nta		r1 = [r3],9
	(p32) ld8.bias.nta		r1 = [r3],9
	(p2) ld8.bias.nta		r120 = [r3],9
	(p2) ld8.bias.nta		r1 = [r63],9
	(p0) ld8.fill		r1 = [r3]
	(p16) ld8.fill		r1 = [r3]
	(p0) ld8.fill		r100 = [r3]
	(p0) ld8.fill		r1 = [r103]
	(p1) ld8.fill		r1 = [r3],r2
	(p41) ld8.fill		r1 = [r3],r2
	(p1) ld8.fill		r110 = [r3],r2
	(p1) ld8.fill		r1 = [r93],r2
	(p1) ld8.fill		r1 = [r3],r92
	(p2) ld8.fill		r1 = [r3],9
	(p32) ld8.fill		r1 = [r3],9
	(p2) ld8.fill		r120 = [r3],9
	(p2) ld8.fill		r1 = [r63],9
	(p0) ld8.fill.nt1		r1 = [r3]
	(p16) ld8.fill.nt1		r1 = [r3]
	(p0) ld8.fill.nt1		r100 = [r3]
	(p0) ld8.fill.nt1		r1 = [r103]
	(p1) ld8.fill.nt1		r1 = [r3],r2
	(p41) ld8.fill.nt1		r1 = [r3],r2
	(p1) ld8.fill.nt1		r110 = [r3],r2
	(p1) ld8.fill.nt1		r1 = [r93],r2
	(p1) ld8.fill.nt1		r1 = [r3],r92
	(p2) ld8.fill.nt1		r1 = [r3],9
	(p32) ld8.fill.nt1		r1 = [r3],9
	(p2) ld8.fill.nt1		r120 = [r3],9
	(p2) ld8.fill.nt1		r1 = [r63],9
	(p0) ld8.fill.nta		r1 = [r3]
	(p16) ld8.fill.nta		r1 = [r3]
	(p0) ld8.fill.nta		r100 = [r3]
	(p0) ld8.fill.nta		r1 = [r103]
	(p1) ld8.fill.nta		r1 = [r3],r2
	(p41) ld8.fill.nta		r1 = [r3],r2
	(p1) ld8.fill.nta		r110 = [r3],r2
	(p1) ld8.fill.nta		r1 = [r93],r2
	(p1) ld8.fill.nta		r1 = [r3],r92
	(p2) ld8.fill.nta		r1 = [r3],9
	(p32) ld8.fill.nta		r1 = [r3],9
	(p2) ld8.fill.nta		r120 = [r3],9
	(p2) ld8.fill.nta		r1 = [r63],9

	(p0) ldfs		f10 = [r3]
	(p16) ldfs		f10 = [r3]
	(p0) ldfs		f100 = [r3]
	(p0) ldfs		f10 = [r103]
	(p1) ldfs		f10 = [r3],r2
	(p41) ldfs		f10 = [r3],r2
	(p1) ldfs		f110 = [r3],r2
	(p1) ldfs		f10 = [r93],r2
	(p1) ldfs		f10 = [r3],r92
	(p2) ldfs		f10 = [r3],9
	(p32) ldfs		f10 = [r3],9
	(p2) ldfs		f120 = [r3],9
	(p2) ldfs		f10 = [r63],9
	(p0) ldfs.nt1		f10 = [r3]
	(p16) ldfs.nt1		f10 = [r3]
	(p0) ldfs.nt1		f100 = [r3]
	(p0) ldfs.nt1		f10 = [r103]
	(p1) ldfs.nt1		f10 = [r3],r2
	(p41) ldfs.nt1		f10 = [r3],r2
	(p1) ldfs.nt1		f110 = [r3],r2
	(p1) ldfs.nt1		f10 = [r93],r2
	(p1) ldfs.nt1		f10 = [r3],r92
	(p2) ldfs.nt1		f10 = [r3],9
	(p32) ldfs.nt1		f10 = [r3],9
	(p2) ldfs.nt1		f120 = [r3],9
	(p2) ldfs.nt1		f10 = [r63],9
	(p0) ldfs.nta		f10 = [r3]
	(p16) ldfs.nta		f10 = [r3]
	(p0) ldfs.nta		f100 = [r3]
	(p0) ldfs.nta		f10 = [r103]
	(p1) ldfs.nta		f10 = [r3],r2
	(p41) ldfs.nta		f10 = [r3],r2
	(p1) ldfs.nta		f110 = [r3],r2
	(p1) ldfs.nta		f10 = [r93],r2
	(p1) ldfs.nta		f10 = [r3],r92
	(p2) ldfs.nta		f10 = [r3],9
	(p32) ldfs.nta		f10 = [r3],9
	(p2) ldfs.nta		f120 = [r3],9
	(p2) ldfs.nta		f10 = [r63],9
	(p0) ldfs.s		f10 = [r3]
	(p16) ldfs.s		f10 = [r3]
	(p0) ldfs.s		f100 = [r3]
	(p0) ldfs.s		f10 = [r103]
	(p1) ldfs.s		f10 = [r3],r2
	(p41) ldfs.s		f10 = [r3],r2
	(p1) ldfs.s		f110 = [r3],r2
	(p1) ldfs.s		f10 = [r93],r2
	(p1) ldfs.s		f10 = [r3],r92
	(p2) ldfs.s		f10 = [r3],9
	(p32) ldfs.s		f10 = [r3],9
	(p2) ldfs.s		f120 = [r3],9
	(p2) ldfs.s		f10 = [r63],9
	(p0) ldfs.s.nt1		f10 = [r3]
	(p16) ldfs.s.nt1		f10 = [r3]
	(p0) ldfs.s.nt1		f100 = [r3]
	(p0) ldfs.s.nt1		f10 = [r103]
	(p1) ldfs.s.nt1		f10 = [r3],r2
	(p41) ldfs.s.nt1		f10 = [r3],r2
	(p1) ldfs.s.nt1		f110 = [r3],r2
	(p1) ldfs.s.nt1		f10 = [r93],r2
	(p1) ldfs.s.nt1		f10 = [r3],r92
	(p2) ldfs.s.nt1		f10 = [r3],9
	(p32) ldfs.s.nt1		f10 = [r3],9
	(p2) ldfs.s.nt1		f120 = [r3],9
	(p2) ldfs.s.nt1		f10 = [r63],9
	(p0) ldfs.s.nta		f10 = [r3]
	(p16) ldfs.s.nta		f10 = [r3]
	(p0) ldfs.s.nta		f100 = [r3]
	(p0) ldfs.s.nta		f10 = [r103]
	(p1) ldfs.s.nta		f10 = [r3],r2
	(p41) ldfs.s.nta		f10 = [r3],r2
	(p1) ldfs.s.nta		f110 = [r3],r2
	(p1) ldfs.s.nta		f10 = [r93],r2
	(p1) ldfs.s.nta		f10 = [r3],r92
	(p2) ldfs.s.nta		f10 = [r3],9
	(p32) ldfs.s.nta		f10 = [r3],9
	(p2) ldfs.s.nta		f120 = [r3],9
	(p2) ldfs.s.nta		f10 = [r63],9
	(p0) ldfs.a		f10 = [r3]
	(p16) ldfs.a		f10 = [r3]
	(p0) ldfs.a		f100 = [r3]
	(p0) ldfs.a		f10 = [r103]
	(p1) ldfs.a		f10 = [r3],r2
	(p41) ldfs.a		f10 = [r3],r2
	(p1) ldfs.a		f110 = [r3],r2
	(p1) ldfs.a		f10 = [r93],r2
	(p1) ldfs.a		f10 = [r3],r92
	(p2) ldfs.a		f10 = [r3],9
	(p32) ldfs.a		f10 = [r3],9
	(p2) ldfs.a		f120 = [r3],9
	(p2) ldfs.a		f10 = [r63],9
	(p0) ldfs.a.nt1		f10 = [r3]
	(p16) ldfs.a.nt1		f10 = [r3]
	(p0) ldfs.a.nt1		f100 = [r3]
	(p0) ldfs.a.nt1		f10 = [r103]
	(p1) ldfs.a.nt1		f10 = [r3],r2
	(p41) ldfs.a.nt1		f10 = [r3],r2
	(p1) ldfs.a.nt1		f110 = [r3],r2
	(p1) ldfs.a.nt1		f10 = [r93],r2
	(p1) ldfs.a.nt1		f10 = [r3],r92
	(p2) ldfs.a.nt1		f10 = [r3],9
	(p32) ldfs.a.nt1		f10 = [r3],9
	(p2) ldfs.a.nt1		f120 = [r3],9
	(p2) ldfs.a.nt1		f10 = [r63],9
	(p0) ldfs.a.nta		f10 = [r3]
	(p16) ldfs.a.nta		f10 = [r3]
	(p0) ldfs.a.nta		f100 = [r3]
	(p0) ldfs.a.nta		f10 = [r103]
	(p1) ldfs.a.nta		f10 = [r3],r2
	(p41) ldfs.a.nta		f10 = [r3],r2
	(p1) ldfs.a.nta		f110 = [r3],r2
	(p1) ldfs.a.nta		f10 = [r93],r2
	(p1) ldfs.a.nta		f10 = [r3],r92
	(p2) ldfs.a.nta		f10 = [r3],9
	(p32) ldfs.a.nta		f10 = [r3],9
	(p2) ldfs.a.nta		f120 = [r3],9
	(p2) ldfs.a.nta		f10 = [r63],9
	(p0) ldfs.sa		f10 = [r3]
	(p16) ldfs.sa		f10 = [r3]
	(p0) ldfs.sa		f100 = [r3]
	(p0) ldfs.sa		f10 = [r103]
	(p1) ldfs.sa		f10 = [r3],r2
	(p41) ldfs.sa		f10 = [r3],r2
	(p1) ldfs.sa		f110 = [r3],r2
	(p1) ldfs.sa		f10 = [r93],r2
	(p1) ldfs.sa		f10 = [r3],r92
	(p2) ldfs.sa		f10 = [r3],9
	(p32) ldfs.sa		f10 = [r3],9
	(p2) ldfs.sa		f120 = [r3],9
	(p2) ldfs.sa		f10 = [r63],9
	(p0) ldfs.sa.nt1		f10 = [r3]
	(p16) ldfs.sa.nt1		f10 = [r3]
	(p0) ldfs.sa.nt1		f100 = [r3]
	(p0) ldfs.sa.nt1		f10 = [r103]
	(p1) ldfs.sa.nt1		f10 = [r3],r2
	(p41) ldfs.sa.nt1		f10 = [r3],r2
	(p1) ldfs.sa.nt1		f110 = [r3],r2
	(p1) ldfs.sa.nt1		f10 = [r93],r2
	(p1) ldfs.sa.nt1		f10 = [r3],r92
	(p2) ldfs.sa.nt1		f10 = [r3],9
	(p32) ldfs.sa.nt1		f10 = [r3],9
	(p2) ldfs.sa.nt1		f120 = [r3],9
	(p2) ldfs.sa.nt1		f10 = [r63],9
	(p0) ldfs.sa.nta		f10 = [r3]
	(p16) ldfs.sa.nta		f10 = [r3]
	(p0) ldfs.sa.nta		f100 = [r3]
	(p0) ldfs.sa.nta		f10 = [r103]
	(p1) ldfs.sa.nta		f10 = [r3],r2
	(p41) ldfs.sa.nta		f10 = [r3],r2
	(p1) ldfs.sa.nta		f110 = [r3],r2
	(p1) ldfs.sa.nta		f10 = [r93],r2
	(p1) ldfs.sa.nta		f10 = [r3],r92
	(p2) ldfs.sa.nta		f10 = [r3],9
	(p32) ldfs.sa.nta		f10 = [r3],9
	(p2) ldfs.sa.nta		f120 = [r3],9
	(p2) ldfs.sa.nta		f10 = [r63],9
	(p0) ldfs.c.nc		f10 = [r3]
	(p16) ldfs.c.nc		f10 = [r3]
	(p0) ldfs.c.nc		f100 = [r3]
	(p0) ldfs.c.nc		f10 = [r103]
	(p1) ldfs.c.nc		f10 = [r3],r2
	(p41) ldfs.c.nc		f10 = [r3],r2
	(p1) ldfs.c.nc		f110 = [r3],r2
	(p1) ldfs.c.nc		f10 = [r93],r2
	(p1) ldfs.c.nc		f10 = [r3],r92
	(p2) ldfs.c.nc		f10 = [r3],9
	(p32) ldfs.c.nc		f10 = [r3],9
	(p2) ldfs.c.nc		f120 = [r3],9
	(p2) ldfs.c.nc		f10 = [r63],9
	(p0) ldfs.c.nc.nt1		f10 = [r3]
	(p16) ldfs.c.nc.nt1		f10 = [r3]
	(p0) ldfs.c.nc.nt1		f100 = [r3]
	(p0) ldfs.c.nc.nt1		f10 = [r103]
	(p1) ldfs.c.nc.nt1		f10 = [r3],r2
	(p41) ldfs.c.nc.nt1		f10 = [r3],r2
	(p1) ldfs.c.nc.nt1		f110 = [r3],r2
	(p1) ldfs.c.nc.nt1		f10 = [r93],r2
	(p1) ldfs.c.nc.nt1		f10 = [r3],r92
	(p2) ldfs.c.nc.nt1		f10 = [r3],9
	(p32) ldfs.c.nc.nt1		f10 = [r3],9
	(p2) ldfs.c.nc.nt1		f120 = [r3],9
	(p2) ldfs.c.nc.nt1		f10 = [r63],9
	(p0) ldfs.c.nc.nta		f10 = [r3]
	(p16) ldfs.c.nc.nta		f10 = [r3]
	(p0) ldfs.c.nc.nta		f100 = [r3]
	(p0) ldfs.c.nc.nta		f10 = [r103]
	(p1) ldfs.c.nc.nta		f10 = [r3],r2
	(p41) ldfs.c.nc.nta		f10 = [r3],r2
	(p1) ldfs.c.nc.nta		f110 = [r3],r2
	(p1) ldfs.c.nc.nta		f10 = [r93],r2
	(p1) ldfs.c.nc.nta		f10 = [r3],r92
	(p2) ldfs.c.nc.nta		f10 = [r3],9
	(p32) ldfs.c.nc.nta		f10 = [r3],9
	(p2) ldfs.c.nc.nta		f120 = [r3],9
	(p2) ldfs.c.nc.nta		f10 = [r63],9
	(p0) ldfs.c.clr		f10 = [r3]
	(p16) ldfs.c.clr		f10 = [r3]
	(p0) ldfs.c.clr		f100 = [r3]
	(p0) ldfs.c.clr		f10 = [r103]
	(p1) ldfs.c.clr		f10 = [r3],r2
	(p41) ldfs.c.clr		f10 = [r3],r2
	(p1) ldfs.c.clr		f110 = [r3],r2
	(p1) ldfs.c.clr		f10 = [r93],r2
	(p1) ldfs.c.clr		f10 = [r3],r92
	(p2) ldfs.c.clr		f10 = [r3],9
	(p32) ldfs.c.clr		f10 = [r3],9
	(p2) ldfs.c.clr		f120 = [r3],9
	(p2) ldfs.c.clr		f10 = [r63],9
	(p0) ldfs.c.clr.nt1		f10 = [r3]
	(p16) ldfs.c.clr.nt1		f10 = [r3]
	(p0) ldfs.c.clr.nt1		f100 = [r3]
	(p0) ldfs.c.clr.nt1		f10 = [r103]
	(p1) ldfs.c.clr.nt1		f10 = [r3],r2
	(p41) ldfs.c.clr.nt1		f10 = [r3],r2
	(p1) ldfs.c.clr.nt1		f110 = [r3],r2
	(p1) ldfs.c.clr.nt1		f10 = [r93],r2
	(p1) ldfs.c.clr.nt1		f10 = [r3],r92
	(p2) ldfs.c.clr.nt1		f10 = [r3],9
	(p32) ldfs.c.clr.nt1		f10 = [r3],9
	(p2) ldfs.c.clr.nt1		f120 = [r3],9
	(p2) ldfs.c.clr.nt1		f10 = [r63],9
	(p0) ldfs.c.clr.nta		f10 = [r3]
	(p16) ldfs.c.clr.nta		f10 = [r3]
	(p0) ldfs.c.clr.nta		f100 = [r3]
	(p0) ldfs.c.clr.nta		f10 = [r103]
	(p1) ldfs.c.clr.nta		f10 = [r3],r2
	(p41) ldfs.c.clr.nta		f10 = [r3],r2
	(p1) ldfs.c.clr.nta		f110 = [r3],r2
	(p1) ldfs.c.clr.nta		f10 = [r93],r2
	(p1) ldfs.c.clr.nta		f10 = [r3],r92
	(p2) ldfs.c.clr.nta		f10 = [r3],9
	(p32) ldfs.c.clr.nta		f10 = [r3],9
	(p2) ldfs.c.clr.nta		f120 = [r3],9
	(p2) ldfs.c.clr.nta		f10 = [r63],9
	(p0) ldfd		f10 = [r3]
	(p16) ldfd		f10 = [r3]
	(p0) ldfd		f100 = [r3]
	(p0) ldfd		f10 = [r103]
	(p1) ldfd		f10 = [r3],r2
	(p41) ldfd		f10 = [r3],r2
	(p1) ldfd		f110 = [r3],r2
	(p1) ldfd		f10 = [r93],r2
	(p1) ldfd		f10 = [r3],r92
	(p2) ldfd		f10 = [r3],9
	(p32) ldfd		f10 = [r3],9
	(p2) ldfd		f120 = [r3],9
	(p2) ldfd		f10 = [r63],9
	(p0) ldfd.nt1		f10 = [r3]
	(p16) ldfd.nt1		f10 = [r3]
	(p0) ldfd.nt1		f100 = [r3]
	(p0) ldfd.nt1		f10 = [r103]
	(p1) ldfd.nt1		f10 = [r3],r2
	(p41) ldfd.nt1		f10 = [r3],r2
	(p1) ldfd.nt1		f110 = [r3],r2
	(p1) ldfd.nt1		f10 = [r93],r2
	(p1) ldfd.nt1		f10 = [r3],r92
	(p2) ldfd.nt1		f10 = [r3],9
	(p32) ldfd.nt1		f10 = [r3],9
	(p2) ldfd.nt1		f120 = [r3],9
	(p2) ldfd.nt1		f10 = [r63],9
	(p0) ldfd.nta		f10 = [r3]
	(p16) ldfd.nta		f10 = [r3]
	(p0) ldfd.nta		f100 = [r3]
	(p0) ldfd.nta		f10 = [r103]
	(p1) ldfd.nta		f10 = [r3],r2
	(p41) ldfd.nta		f10 = [r3],r2
	(p1) ldfd.nta		f110 = [r3],r2
	(p1) ldfd.nta		f10 = [r93],r2
	(p1) ldfd.nta		f10 = [r3],r92
	(p2) ldfd.nta		f10 = [r3],9
	(p32) ldfd.nta		f10 = [r3],9
	(p2) ldfd.nta		f120 = [r3],9
	(p2) ldfd.nta		f10 = [r63],9
	(p0) ldfd.s		f10 = [r3]
	(p16) ldfd.s		f10 = [r3]
	(p0) ldfd.s		f100 = [r3]
	(p0) ldfd.s		f10 = [r103]
	(p1) ldfd.s		f10 = [r3],r2
	(p41) ldfd.s		f10 = [r3],r2
	(p1) ldfd.s		f110 = [r3],r2
	(p1) ldfd.s		f10 = [r93],r2
	(p1) ldfd.s		f10 = [r3],r92
	(p2) ldfd.s		f10 = [r3],9
	(p32) ldfd.s		f10 = [r3],9
	(p2) ldfd.s		f120 = [r3],9
	(p2) ldfd.s		f10 = [r63],9
	(p0) ldfd.s.nt1		f10 = [r3]
	(p16) ldfd.s.nt1		f10 = [r3]
	(p0) ldfd.s.nt1		f100 = [r3]
	(p0) ldfd.s.nt1		f10 = [r103]
	(p1) ldfd.s.nt1		f10 = [r3],r2
	(p41) ldfd.s.nt1		f10 = [r3],r2
	(p1) ldfd.s.nt1		f110 = [r3],r2
	(p1) ldfd.s.nt1		f10 = [r93],r2
	(p1) ldfd.s.nt1		f10 = [r3],r92
	(p2) ldfd.s.nt1		f10 = [r3],9
	(p32) ldfd.s.nt1		f10 = [r3],9
	(p2) ldfd.s.nt1		f120 = [r3],9
	(p2) ldfd.s.nt1		f10 = [r63],9
	(p0) ldfd.s.nta		f10 = [r3]
	(p16) ldfd.s.nta		f10 = [r3]
	(p0) ldfd.s.nta		f100 = [r3]
	(p0) ldfd.s.nta		f10 = [r103]
	(p1) ldfd.s.nta		f10 = [r3],r2
	(p41) ldfd.s.nta		f10 = [r3],r2
	(p1) ldfd.s.nta		f110 = [r3],r2
	(p1) ldfd.s.nta		f10 = [r93],r2
	(p1) ldfd.s.nta		f10 = [r3],r92
	(p2) ldfd.s.nta		f10 = [r3],9
	(p32) ldfd.s.nta		f10 = [r3],9
	(p2) ldfd.s.nta		f120 = [r3],9
	(p2) ldfd.s.nta		f10 = [r63],9
	(p0) ldfd.a		f10 = [r3]
	(p16) ldfd.a		f10 = [r3]
	(p0) ldfd.a		f100 = [r3]
	(p0) ldfd.a		f10 = [r103]
	(p1) ldfd.a		f10 = [r3],r2
	(p41) ldfd.a		f10 = [r3],r2
	(p1) ldfd.a		f110 = [r3],r2
	(p1) ldfd.a		f10 = [r93],r2
	(p1) ldfd.a		f10 = [r3],r92
	(p2) ldfd.a		f10 = [r3],9
	(p32) ldfd.a		f10 = [r3],9
	(p2) ldfd.a		f120 = [r3],9
	(p2) ldfd.a		f10 = [r63],9
	(p0) ldfd.a.nt1		f10 = [r3]
	(p16) ldfd.a.nt1		f10 = [r3]
	(p0) ldfd.a.nt1		f100 = [r3]
	(p0) ldfd.a.nt1		f10 = [r103]
	(p1) ldfd.a.nt1		f10 = [r3],r2
	(p41) ldfd.a.nt1		f10 = [r3],r2
	(p1) ldfd.a.nt1		f110 = [r3],r2
	(p1) ldfd.a.nt1		f10 = [r93],r2
	(p1) ldfd.a.nt1		f10 = [r3],r92
	(p2) ldfd.a.nt1		f10 = [r3],9
	(p32) ldfd.a.nt1		f10 = [r3],9
	(p2) ldfd.a.nt1		f120 = [r3],9
	(p2) ldfd.a.nt1		f10 = [r63],9
	(p0) ldfd.a.nta		f10 = [r3]
	(p16) ldfd.a.nta		f10 = [r3]
	(p0) ldfd.a.nta		f100 = [r3]
	(p0) ldfd.a.nta		f10 = [r103]
	(p1) ldfd.a.nta		f10 = [r3],r2
	(p41) ldfd.a.nta		f10 = [r3],r2
	(p1) ldfd.a.nta		f110 = [r3],r2
	(p1) ldfd.a.nta		f10 = [r93],r2
	(p1) ldfd.a.nta		f10 = [r3],r92
	(p2) ldfd.a.nta		f10 = [r3],9
	(p32) ldfd.a.nta		f10 = [r3],9
	(p2) ldfd.a.nta		f120 = [r3],9
	(p2) ldfd.a.nta		f10 = [r63],9
	(p0) ldfd.sa		f10 = [r3]
	(p16) ldfd.sa		f10 = [r3]
	(p0) ldfd.sa		f100 = [r3]
	(p0) ldfd.sa		f10 = [r103]
	(p1) ldfd.sa		f10 = [r3],r2
	(p41) ldfd.sa		f10 = [r3],r2
	(p1) ldfd.sa		f110 = [r3],r2
	(p1) ldfd.sa		f10 = [r93],r2
	(p1) ldfd.sa		f10 = [r3],r92
	(p2) ldfd.sa		f10 = [r3],9
	(p32) ldfd.sa		f10 = [r3],9
	(p2) ldfd.sa		f120 = [r3],9
	(p2) ldfd.sa		f10 = [r63],9
	(p0) ldfd.sa.nt1		f10 = [r3]
	(p16) ldfd.sa.nt1		f10 = [r3]
	(p0) ldfd.sa.nt1		f100 = [r3]
	(p0) ldfd.sa.nt1		f10 = [r103]
	(p1) ldfd.sa.nt1		f10 = [r3],r2
	(p41) ldfd.sa.nt1		f10 = [r3],r2
	(p1) ldfd.sa.nt1		f110 = [r3],r2
	(p1) ldfd.sa.nt1		f10 = [r93],r2
	(p1) ldfd.sa.nt1		f10 = [r3],r92
	(p2) ldfd.sa.nt1		f10 = [r3],9
	(p32) ldfd.sa.nt1		f10 = [r3],9
	(p2) ldfd.sa.nt1		f120 = [r3],9
	(p2) ldfd.sa.nt1		f10 = [r63],9
	(p0) ldfd.sa.nta		f10 = [r3]
	(p16) ldfd.sa.nta		f10 = [r3]
	(p0) ldfd.sa.nta		f100 = [r3]
	(p0) ldfd.sa.nta		f10 = [r103]
	(p1) ldfd.sa.nta		f10 = [r3],r2
	(p41) ldfd.sa.nta		f10 = [r3],r2
	(p1) ldfd.sa.nta		f110 = [r3],r2
	(p1) ldfd.sa.nta		f10 = [r93],r2
	(p1) ldfd.sa.nta		f10 = [r3],r92
	(p2) ldfd.sa.nta		f10 = [r3],9
	(p32) ldfd.sa.nta		f10 = [r3],9
	(p2) ldfd.sa.nta		f120 = [r3],9
	(p2) ldfd.sa.nta		f10 = [r63],9
	(p0) ldfd.c.nc		f10 = [r3]
	(p16) ldfd.c.nc		f10 = [r3]
	(p0) ldfd.c.nc		f100 = [r3]
	(p0) ldfd.c.nc		f10 = [r103]
	(p1) ldfd.c.nc		f10 = [r3],r2
	(p41) ldfd.c.nc		f10 = [r3],r2
	(p1) ldfd.c.nc		f110 = [r3],r2
	(p1) ldfd.c.nc		f10 = [r93],r2
	(p1) ldfd.c.nc		f10 = [r3],r92
	(p2) ldfd.c.nc		f10 = [r3],9
	(p32) ldfd.c.nc		f10 = [r3],9
	(p2) ldfd.c.nc		f120 = [r3],9
	(p2) ldfd.c.nc		f10 = [r63],9
	(p0) ldfd.c.nc.nt1		f10 = [r3]
	(p16) ldfd.c.nc.nt1		f10 = [r3]
	(p0) ldfd.c.nc.nt1		f100 = [r3]
	(p0) ldfd.c.nc.nt1		f10 = [r103]
	(p1) ldfd.c.nc.nt1		f10 = [r3],r2
	(p41) ldfd.c.nc.nt1		f10 = [r3],r2
	(p1) ldfd.c.nc.nt1		f110 = [r3],r2
	(p1) ldfd.c.nc.nt1		f10 = [r93],r2
	(p1) ldfd.c.nc.nt1		f10 = [r3],r92
	(p2) ldfd.c.nc.nt1		f10 = [r3],9
	(p32) ldfd.c.nc.nt1		f10 = [r3],9
	(p2) ldfd.c.nc.nt1		f120 = [r3],9
	(p2) ldfd.c.nc.nt1		f10 = [r63],9
	(p0) ldfd.c.nc.nta		f10 = [r3]
	(p16) ldfd.c.nc.nta		f10 = [r3]
	(p0) ldfd.c.nc.nta		f100 = [r3]
	(p0) ldfd.c.nc.nta		f10 = [r103]
	(p1) ldfd.c.nc.nta		f10 = [r3],r2
	(p41) ldfd.c.nc.nta		f10 = [r3],r2
	(p1) ldfd.c.nc.nta		f110 = [r3],r2
	(p1) ldfd.c.nc.nta		f10 = [r93],r2
	(p1) ldfd.c.nc.nta		f10 = [r3],r92
	(p2) ldfd.c.nc.nta		f10 = [r3],9
	(p32) ldfd.c.nc.nta		f10 = [r3],9
	(p2) ldfd.c.nc.nta		f120 = [r3],9
	(p2) ldfd.c.nc.nta		f10 = [r63],9
	(p0) ldfd.c.clr		f10 = [r3]
	(p16) ldfd.c.clr		f10 = [r3]
	(p0) ldfd.c.clr		f100 = [r3]
	(p0) ldfd.c.clr		f10 = [r103]
	(p1) ldfd.c.clr		f10 = [r3],r2
	(p41) ldfd.c.clr		f10 = [r3],r2
	(p1) ldfd.c.clr		f110 = [r3],r2
	(p1) ldfd.c.clr		f10 = [r93],r2
	(p1) ldfd.c.clr		f10 = [r3],r92
	(p2) ldfd.c.clr		f10 = [r3],9
	(p32) ldfd.c.clr		f10 = [r3],9
	(p2) ldfd.c.clr		f120 = [r3],9
	(p2) ldfd.c.clr		f10 = [r63],9
	(p0) ldfd.c.clr.nt1		f10 = [r3]
	(p16) ldfd.c.clr.nt1		f10 = [r3]
	(p0) ldfd.c.clr.nt1		f100 = [r3]
	(p0) ldfd.c.clr.nt1		f10 = [r103]
	(p1) ldfd.c.clr.nt1		f10 = [r3],r2
	(p41) ldfd.c.clr.nt1		f10 = [r3],r2
	(p1) ldfd.c.clr.nt1		f110 = [r3],r2
	(p1) ldfd.c.clr.nt1		f10 = [r93],r2
	(p1) ldfd.c.clr.nt1		f10 = [r3],r92
	(p2) ldfd.c.clr.nt1		f10 = [r3],9
	(p32) ldfd.c.clr.nt1		f10 = [r3],9
	(p2) ldfd.c.clr.nt1		f120 = [r3],9
	(p2) ldfd.c.clr.nt1		f10 = [r63],9
	(p0) ldfd.c.clr.nta		f10 = [r3]
	(p16) ldfd.c.clr.nta		f10 = [r3]
	(p0) ldfd.c.clr.nta		f100 = [r3]
	(p0) ldfd.c.clr.nta		f10 = [r103]
	(p1) ldfd.c.clr.nta		f10 = [r3],r2
	(p41) ldfd.c.clr.nta		f10 = [r3],r2
	(p1) ldfd.c.clr.nta		f110 = [r3],r2
	(p1) ldfd.c.clr.nta		f10 = [r93],r2
	(p1) ldfd.c.clr.nta		f10 = [r3],r92
	(p2) ldfd.c.clr.nta		f10 = [r3],9
	(p32) ldfd.c.clr.nta		f10 = [r3],9
	(p2) ldfd.c.clr.nta		f120 = [r3],9
	(p2) ldfd.c.clr.nta		f10 = [r63],9
	(p0) ldfe		f10 = [r3]
	(p16) ldfe		f10 = [r3]
	(p0) ldfe		f100 = [r3]
	(p0) ldfe		f10 = [r103]
	(p1) ldfe		f10 = [r3],r2
	(p41) ldfe		f10 = [r3],r2
	(p1) ldfe		f110 = [r3],r2
	(p1) ldfe		f10 = [r93],r2
	(p1) ldfe		f10 = [r3],r92
	(p2) ldfe		f10 = [r3],9
	(p32) ldfe		f10 = [r3],9
	(p2) ldfe		f120 = [r3],9
	(p2) ldfe		f10 = [r63],9
	(p0) ldfe.nt1		f10 = [r3]
	(p16) ldfe.nt1		f10 = [r3]
	(p0) ldfe.nt1		f100 = [r3]
	(p0) ldfe.nt1		f10 = [r103]
	(p1) ldfe.nt1		f10 = [r3],r2
	(p41) ldfe.nt1		f10 = [r3],r2
	(p1) ldfe.nt1		f110 = [r3],r2
	(p1) ldfe.nt1		f10 = [r93],r2
	(p1) ldfe.nt1		f10 = [r3],r92
	(p2) ldfe.nt1		f10 = [r3],9
	(p32) ldfe.nt1		f10 = [r3],9
	(p2) ldfe.nt1		f120 = [r3],9
	(p2) ldfe.nt1		f10 = [r63],9
	(p0) ldfe.nta		f10 = [r3]
	(p16) ldfe.nta		f10 = [r3]
	(p0) ldfe.nta		f100 = [r3]
	(p0) ldfe.nta		f10 = [r103]
	(p1) ldfe.nta		f10 = [r3],r2
	(p41) ldfe.nta		f10 = [r3],r2
	(p1) ldfe.nta		f110 = [r3],r2
	(p1) ldfe.nta		f10 = [r93],r2
	(p1) ldfe.nta		f10 = [r3],r92
	(p2) ldfe.nta		f10 = [r3],9
	(p32) ldfe.nta		f10 = [r3],9
	(p2) ldfe.nta		f120 = [r3],9
	(p2) ldfe.nta		f10 = [r63],9
	(p0) ldfe.s		f10 = [r3]
	(p16) ldfe.s		f10 = [r3]
	(p0) ldfe.s		f100 = [r3]
	(p0) ldfe.s		f10 = [r103]
	(p1) ldfe.s		f10 = [r3],r2
	(p41) ldfe.s		f10 = [r3],r2
	(p1) ldfe.s		f110 = [r3],r2
	(p1) ldfe.s		f10 = [r93],r2
	(p1) ldfe.s		f10 = [r3],r92
	(p2) ldfe.s		f10 = [r3],9
	(p32) ldfe.s		f10 = [r3],9
	(p2) ldfe.s		f120 = [r3],9
	(p2) ldfe.s		f10 = [r63],9
	(p0) ldfe.s.nt1		f10 = [r3]
	(p16) ldfe.s.nt1		f10 = [r3]
	(p0) ldfe.s.nt1		f100 = [r3]
	(p0) ldfe.s.nt1		f10 = [r103]
	(p1) ldfe.s.nt1		f10 = [r3],r2
	(p41) ldfe.s.nt1		f10 = [r3],r2
	(p1) ldfe.s.nt1		f110 = [r3],r2
	(p1) ldfe.s.nt1		f10 = [r93],r2
	(p1) ldfe.s.nt1		f10 = [r3],r92
	(p2) ldfe.s.nt1		f10 = [r3],9
	(p32) ldfe.s.nt1		f10 = [r3],9
	(p2) ldfe.s.nt1		f120 = [r3],9
	(p2) ldfe.s.nt1		f10 = [r63],9
	(p0) ldfe.s.nta		f10 = [r3]
	(p16) ldfe.s.nta		f10 = [r3]
	(p0) ldfe.s.nta		f100 = [r3]
	(p0) ldfe.s.nta		f10 = [r103]
	(p1) ldfe.s.nta		f10 = [r3],r2
	(p41) ldfe.s.nta		f10 = [r3],r2
	(p1) ldfe.s.nta		f110 = [r3],r2
	(p1) ldfe.s.nta		f10 = [r93],r2
	(p1) ldfe.s.nta		f10 = [r3],r92
	(p2) ldfe.s.nta		f10 = [r3],9
	(p32) ldfe.s.nta		f10 = [r3],9
	(p2) ldfe.s.nta		f120 = [r3],9
	(p2) ldfe.s.nta		f10 = [r63],9
	(p0) ldfe.a		f10 = [r3]
	(p16) ldfe.a		f10 = [r3]
	(p0) ldfe.a		f100 = [r3]
	(p0) ldfe.a		f10 = [r103]
	(p1) ldfe.a		f10 = [r3],r2
	(p41) ldfe.a		f10 = [r3],r2
	(p1) ldfe.a		f110 = [r3],r2
	(p1) ldfe.a		f10 = [r93],r2
	(p1) ldfe.a		f10 = [r3],r92
	(p2) ldfe.a		f10 = [r3],9
	(p32) ldfe.a		f10 = [r3],9
	(p2) ldfe.a		f120 = [r3],9
	(p2) ldfe.a		f10 = [r63],9
	(p0) ldfe.a.nt1		f10 = [r3]
	(p16) ldfe.a.nt1		f10 = [r3]
	(p0) ldfe.a.nt1		f100 = [r3]
	(p0) ldfe.a.nt1		f10 = [r103]
	(p1) ldfe.a.nt1		f10 = [r3],r2
	(p41) ldfe.a.nt1		f10 = [r3],r2
	(p1) ldfe.a.nt1		f110 = [r3],r2
	(p1) ldfe.a.nt1		f10 = [r93],r2
	(p1) ldfe.a.nt1		f10 = [r3],r92
	(p2) ldfe.a.nt1		f10 = [r3],9
	(p32) ldfe.a.nt1		f10 = [r3],9
	(p2) ldfe.a.nt1		f120 = [r3],9
	(p2) ldfe.a.nt1		f10 = [r63],9
	(p0) ldfe.a.nta		f10 = [r3]
	(p16) ldfe.a.nta		f10 = [r3]
	(p0) ldfe.a.nta		f100 = [r3]
	(p0) ldfe.a.nta		f10 = [r103]
	(p1) ldfe.a.nta		f10 = [r3],r2
	(p41) ldfe.a.nta		f10 = [r3],r2
	(p1) ldfe.a.nta		f110 = [r3],r2
	(p1) ldfe.a.nta		f10 = [r93],r2
	(p1) ldfe.a.nta		f10 = [r3],r92
	(p2) ldfe.a.nta		f10 = [r3],9
	(p32) ldfe.a.nta		f10 = [r3],9
	(p2) ldfe.a.nta		f120 = [r3],9
	(p2) ldfe.a.nta		f10 = [r63],9
	(p0) ldfe.sa		f10 = [r3]
	(p16) ldfe.sa		f10 = [r3]
	(p0) ldfe.sa		f100 = [r3]
	(p0) ldfe.sa		f10 = [r103]
	(p1) ldfe.sa		f10 = [r3],r2
	(p41) ldfe.sa		f10 = [r3],r2
	(p1) ldfe.sa		f110 = [r3],r2
	(p1) ldfe.sa		f10 = [r93],r2
	(p1) ldfe.sa		f10 = [r3],r92
	(p2) ldfe.sa		f10 = [r3],9
	(p32) ldfe.sa		f10 = [r3],9
	(p2) ldfe.sa		f120 = [r3],9
	(p2) ldfe.sa		f10 = [r63],9
	(p0) ldfe.sa.nt1		f10 = [r3]
	(p16) ldfe.sa.nt1		f10 = [r3]
	(p0) ldfe.sa.nt1		f100 = [r3]
	(p0) ldfe.sa.nt1		f10 = [r103]
	(p1) ldfe.sa.nt1		f10 = [r3],r2
	(p41) ldfe.sa.nt1		f10 = [r3],r2
	(p1) ldfe.sa.nt1		f110 = [r3],r2
	(p1) ldfe.sa.nt1		f10 = [r93],r2
	(p1) ldfe.sa.nt1		f10 = [r3],r92
	(p2) ldfe.sa.nt1		f10 = [r3],9
	(p32) ldfe.sa.nt1		f10 = [r3],9
	(p2) ldfe.sa.nt1		f120 = [r3],9
	(p2) ldfe.sa.nt1		f10 = [r63],9
	(p0) ldfe.sa.nta		f10 = [r3]
	(p16) ldfe.sa.nta		f10 = [r3]
	(p0) ldfe.sa.nta		f100 = [r3]
	(p0) ldfe.sa.nta		f10 = [r103]
	(p1) ldfe.sa.nta		f10 = [r3],r2
	(p41) ldfe.sa.nta		f10 = [r3],r2
	(p1) ldfe.sa.nta		f110 = [r3],r2
	(p1) ldfe.sa.nta		f10 = [r93],r2
	(p1) ldfe.sa.nta		f10 = [r3],r92
	(p2) ldfe.sa.nta		f10 = [r3],9
	(p32) ldfe.sa.nta		f10 = [r3],9
	(p2) ldfe.sa.nta		f120 = [r3],9
	(p2) ldfe.sa.nta		f10 = [r63],9
	(p0) ldfe.c.nc		f10 = [r3]
	(p16) ldfe.c.nc		f10 = [r3]
	(p0) ldfe.c.nc		f100 = [r3]
	(p0) ldfe.c.nc		f10 = [r103]
	(p1) ldfe.c.nc		f10 = [r3],r2
	(p41) ldfe.c.nc		f10 = [r3],r2
	(p1) ldfe.c.nc		f110 = [r3],r2
	(p1) ldfe.c.nc		f10 = [r93],r2
	(p1) ldfe.c.nc		f10 = [r3],r92
	(p2) ldfe.c.nc		f10 = [r3],9
	(p32) ldfe.c.nc		f10 = [r3],9
	(p2) ldfe.c.nc		f120 = [r3],9
	(p2) ldfe.c.nc		f10 = [r63],9
	(p0) ldfe.c.nc.nt1		f10 = [r3]
	(p16) ldfe.c.nc.nt1		f10 = [r3]
	(p0) ldfe.c.nc.nt1		f100 = [r3]
	(p0) ldfe.c.nc.nt1		f10 = [r103]
	(p1) ldfe.c.nc.nt1		f10 = [r3],r2
	(p41) ldfe.c.nc.nt1		f10 = [r3],r2
	(p1) ldfe.c.nc.nt1		f110 = [r3],r2
	(p1) ldfe.c.nc.nt1		f10 = [r93],r2
	(p1) ldfe.c.nc.nt1		f10 = [r3],r92
	(p2) ldfe.c.nc.nt1		f10 = [r3],9
	(p32) ldfe.c.nc.nt1		f10 = [r3],9
	(p2) ldfe.c.nc.nt1		f120 = [r3],9
	(p2) ldfe.c.nc.nt1		f10 = [r63],9
	(p0) ldfe.c.nc.nta		f10 = [r3]
	(p16) ldfe.c.nc.nta		f10 = [r3]
	(p0) ldfe.c.nc.nta		f100 = [r3]
	(p0) ldfe.c.nc.nta		f10 = [r103]
	(p1) ldfe.c.nc.nta		f10 = [r3],r2
	(p41) ldfe.c.nc.nta		f10 = [r3],r2
	(p1) ldfe.c.nc.nta		f110 = [r3],r2
	(p1) ldfe.c.nc.nta		f10 = [r93],r2
	(p1) ldfe.c.nc.nta		f10 = [r3],r92
	(p2) ldfe.c.nc.nta		f10 = [r3],9
	(p32) ldfe.c.nc.nta		f10 = [r3],9
	(p2) ldfe.c.nc.nta		f120 = [r3],9
	(p2) ldfe.c.nc.nta		f10 = [r63],9
	(p0) ldfe.c.clr		f10 = [r3]
	(p16) ldfe.c.clr		f10 = [r3]
	(p0) ldfe.c.clr		f100 = [r3]
	(p0) ldfe.c.clr		f10 = [r103]
	(p1) ldfe.c.clr		f10 = [r3],r2
	(p41) ldfe.c.clr		f10 = [r3],r2
	(p1) ldfe.c.clr		f110 = [r3],r2
	(p1) ldfe.c.clr		f10 = [r93],r2
	(p1) ldfe.c.clr		f10 = [r3],r92
	(p2) ldfe.c.clr		f10 = [r3],9
	(p32) ldfe.c.clr		f10 = [r3],9
	(p2) ldfe.c.clr		f120 = [r3],9
	(p2) ldfe.c.clr		f10 = [r63],9
	(p0) ldfe.c.clr.nt1		f10 = [r3]
	(p16) ldfe.c.clr.nt1		f10 = [r3]
	(p0) ldfe.c.clr.nt1		f100 = [r3]
	(p0) ldfe.c.clr.nt1		f10 = [r103]
	(p1) ldfe.c.clr.nt1		f10 = [r3],r2
	(p41) ldfe.c.clr.nt1		f10 = [r3],r2
	(p1) ldfe.c.clr.nt1		f110 = [r3],r2
	(p1) ldfe.c.clr.nt1		f10 = [r93],r2
	(p1) ldfe.c.clr.nt1		f10 = [r3],r92
	(p2) ldfe.c.clr.nt1		f10 = [r3],9
	(p32) ldfe.c.clr.nt1		f10 = [r3],9
	(p2) ldfe.c.clr.nt1		f120 = [r3],9
	(p2) ldfe.c.clr.nt1		f10 = [r63],9
	(p0) ldfe.c.clr.nta		f10 = [r3]
	(p16) ldfe.c.clr.nta		f10 = [r3]
	(p0) ldfe.c.clr.nta		f100 = [r3]
	(p0) ldfe.c.clr.nta		f10 = [r103]
	(p1) ldfe.c.clr.nta		f10 = [r3],r2
	(p41) ldfe.c.clr.nta		f10 = [r3],r2
	(p1) ldfe.c.clr.nta		f110 = [r3],r2
	(p1) ldfe.c.clr.nta		f10 = [r93],r2
	(p1) ldfe.c.clr.nta		f10 = [r3],r92
	(p2) ldfe.c.clr.nta		f10 = [r3],9
	(p32) ldfe.c.clr.nta		f10 = [r3],9
	(p2) ldfe.c.clr.nta		f120 = [r3],9
	(p2) ldfe.c.clr.nta		f10 = [r63],9
	(p0) ldf8		f10 = [r3]
	(p16) ldf8		f10 = [r3]
	(p0) ldf8		f100 = [r3]
	(p0) ldf8		f10 = [r103]
	(p1) ldf8		f10 = [r3],r2
	(p41) ldf8		f10 = [r3],r2
	(p1) ldf8		f110 = [r3],r2
	(p1) ldf8		f10 = [r93],r2
	(p1) ldf8		f10 = [r3],r92
	(p2) ldf8		f10 = [r3],9
	(p32) ldf8		f10 = [r3],9
	(p2) ldf8		f120 = [r3],9
	(p2) ldf8		f10 = [r63],9
	(p0) ldf8.nt1		f10 = [r3]
	(p16) ldf8.nt1		f10 = [r3]
	(p0) ldf8.nt1		f100 = [r3]
	(p0) ldf8.nt1		f10 = [r103]
	(p1) ldf8.nt1		f10 = [r3],r2
	(p41) ldf8.nt1		f10 = [r3],r2
	(p1) ldf8.nt1		f110 = [r3],r2
	(p1) ldf8.nt1		f10 = [r93],r2
	(p1) ldf8.nt1		f10 = [r3],r92
	(p2) ldf8.nt1		f10 = [r3],9
	(p32) ldf8.nt1		f10 = [r3],9
	(p2) ldf8.nt1		f120 = [r3],9
	(p2) ldf8.nt1		f10 = [r63],9
	(p0) ldf8.nta		f10 = [r3]
	(p16) ldf8.nta		f10 = [r3]
	(p0) ldf8.nta		f100 = [r3]
	(p0) ldf8.nta		f10 = [r103]
	(p1) ldf8.nta		f10 = [r3],r2
	(p41) ldf8.nta		f10 = [r3],r2
	(p1) ldf8.nta		f110 = [r3],r2
	(p1) ldf8.nta		f10 = [r93],r2
	(p1) ldf8.nta		f10 = [r3],r92
	(p2) ldf8.nta		f10 = [r3],9
	(p32) ldf8.nta		f10 = [r3],9
	(p2) ldf8.nta		f120 = [r3],9
	(p2) ldf8.nta		f10 = [r63],9
	(p0) ldf8.s		f10 = [r3]
	(p16) ldf8.s		f10 = [r3]
	(p0) ldf8.s		f100 = [r3]
	(p0) ldf8.s		f10 = [r103]
	(p1) ldf8.s		f10 = [r3],r2
	(p41) ldf8.s		f10 = [r3],r2
	(p1) ldf8.s		f110 = [r3],r2
	(p1) ldf8.s		f10 = [r93],r2
	(p1) ldf8.s		f10 = [r3],r92
	(p2) ldf8.s		f10 = [r3],9
	(p32) ldf8.s		f10 = [r3],9
	(p2) ldf8.s		f120 = [r3],9
	(p2) ldf8.s		f10 = [r63],9
	(p0) ldf8.s.nt1		f10 = [r3]
	(p16) ldf8.s.nt1		f10 = [r3]
	(p0) ldf8.s.nt1		f100 = [r3]
	(p0) ldf8.s.nt1		f10 = [r103]
	(p1) ldf8.s.nt1		f10 = [r3],r2
	(p41) ldf8.s.nt1		f10 = [r3],r2
	(p1) ldf8.s.nt1		f110 = [r3],r2
	(p1) ldf8.s.nt1		f10 = [r93],r2
	(p1) ldf8.s.nt1		f10 = [r3],r92
	(p2) ldf8.s.nt1		f10 = [r3],9
	(p32) ldf8.s.nt1		f10 = [r3],9
	(p2) ldf8.s.nt1		f120 = [r3],9
	(p2) ldf8.s.nt1		f10 = [r63],9
	(p0) ldf8.s.nta		f10 = [r3]
	(p16) ldf8.s.nta		f10 = [r3]
	(p0) ldf8.s.nta		f100 = [r3]
	(p0) ldf8.s.nta		f10 = [r103]
	(p1) ldf8.s.nta		f10 = [r3],r2
	(p41) ldf8.s.nta		f10 = [r3],r2
	(p1) ldf8.s.nta		f110 = [r3],r2
	(p1) ldf8.s.nta		f10 = [r93],r2
	(p1) ldf8.s.nta		f10 = [r3],r92
	(p2) ldf8.s.nta		f10 = [r3],9
	(p32) ldf8.s.nta		f10 = [r3],9
	(p2) ldf8.s.nta		f120 = [r3],9
	(p2) ldf8.s.nta		f10 = [r63],9
	(p0) ldf8.a		f10 = [r3]
	(p16) ldf8.a		f10 = [r3]
	(p0) ldf8.a		f100 = [r3]
	(p0) ldf8.a		f10 = [r103]
	(p1) ldf8.a		f10 = [r3],r2
	(p41) ldf8.a		f10 = [r3],r2
	(p1) ldf8.a		f110 = [r3],r2
	(p1) ldf8.a		f10 = [r93],r2
	(p1) ldf8.a		f10 = [r3],r92
	(p2) ldf8.a		f10 = [r3],9
	(p32) ldf8.a		f10 = [r3],9
	(p2) ldf8.a		f120 = [r3],9
	(p2) ldf8.a		f10 = [r63],9
	(p0) ldf8.a.nt1		f10 = [r3]
	(p16) ldf8.a.nt1		f10 = [r3]
	(p0) ldf8.a.nt1		f100 = [r3]
	(p0) ldf8.a.nt1		f10 = [r103]
	(p1) ldf8.a.nt1		f10 = [r3],r2
	(p41) ldf8.a.nt1		f10 = [r3],r2
	(p1) ldf8.a.nt1		f110 = [r3],r2
	(p1) ldf8.a.nt1		f10 = [r93],r2
	(p1) ldf8.a.nt1		f10 = [r3],r92
	(p2) ldf8.a.nt1		f10 = [r3],9
	(p32) ldf8.a.nt1		f10 = [r3],9
	(p2) ldf8.a.nt1		f120 = [r3],9
	(p2) ldf8.a.nt1		f10 = [r63],9
	(p0) ldf8.a.nta		f10 = [r3]
	(p16) ldf8.a.nta		f10 = [r3]
	(p0) ldf8.a.nta		f100 = [r3]
	(p0) ldf8.a.nta		f10 = [r103]
	(p1) ldf8.a.nta		f10 = [r3],r2
	(p41) ldf8.a.nta		f10 = [r3],r2
	(p1) ldf8.a.nta		f110 = [r3],r2
	(p1) ldf8.a.nta		f10 = [r93],r2
	(p1) ldf8.a.nta		f10 = [r3],r92
	(p2) ldf8.a.nta		f10 = [r3],9
	(p32) ldf8.a.nta		f10 = [r3],9
	(p2) ldf8.a.nta		f120 = [r3],9
	(p2) ldf8.a.nta		f10 = [r63],9
	(p0) ldf8.sa		f10 = [r3]
	(p16) ldf8.sa		f10 = [r3]
	(p0) ldf8.sa		f100 = [r3]
	(p0) ldf8.sa		f10 = [r103]
	(p1) ldf8.sa		f10 = [r3],r2
	(p41) ldf8.sa		f10 = [r3],r2
	(p1) ldf8.sa		f110 = [r3],r2
	(p1) ldf8.sa		f10 = [r93],r2
	(p1) ldf8.sa		f10 = [r3],r92
	(p2) ldf8.sa		f10 = [r3],9
	(p32) ldf8.sa		f10 = [r3],9
	(p2) ldf8.sa		f120 = [r3],9
	(p2) ldf8.sa		f10 = [r63],9
	(p0) ldf8.sa.nt1		f10 = [r3]
	(p16) ldf8.sa.nt1		f10 = [r3]
	(p0) ldf8.sa.nt1		f100 = [r3]
	(p0) ldf8.sa.nt1		f10 = [r103]
	(p1) ldf8.sa.nt1		f10 = [r3],r2
	(p41) ldf8.sa.nt1		f10 = [r3],r2
	(p1) ldf8.sa.nt1		f110 = [r3],r2
	(p1) ldf8.sa.nt1		f10 = [r93],r2
	(p1) ldf8.sa.nt1		f10 = [r3],r92
	(p2) ldf8.sa.nt1		f10 = [r3],9
	(p32) ldf8.sa.nt1		f10 = [r3],9
	(p2) ldf8.sa.nt1		f120 = [r3],9
	(p2) ldf8.sa.nt1		f10 = [r63],9
	(p0) ldf8.sa.nta		f10 = [r3]
	(p16) ldf8.sa.nta		f10 = [r3]
	(p0) ldf8.sa.nta		f100 = [r3]
	(p0) ldf8.sa.nta		f10 = [r103]
	(p1) ldf8.sa.nta		f10 = [r3],r2
	(p41) ldf8.sa.nta		f10 = [r3],r2
	(p1) ldf8.sa.nta		f110 = [r3],r2
	(p1) ldf8.sa.nta		f10 = [r93],r2
	(p1) ldf8.sa.nta		f10 = [r3],r92
	(p2) ldf8.sa.nta		f10 = [r3],9
	(p32) ldf8.sa.nta		f10 = [r3],9
	(p2) ldf8.sa.nta		f120 = [r3],9
	(p2) ldf8.sa.nta		f10 = [r63],9
	(p0) ldf8.c.nc		f10 = [r3]
	(p16) ldf8.c.nc		f10 = [r3]
	(p0) ldf8.c.nc		f100 = [r3]
	(p0) ldf8.c.nc		f10 = [r103]
	(p1) ldf8.c.nc		f10 = [r3],r2
	(p41) ldf8.c.nc		f10 = [r3],r2
	(p1) ldf8.c.nc		f110 = [r3],r2
	(p1) ldf8.c.nc		f10 = [r93],r2
	(p1) ldf8.c.nc		f10 = [r3],r92
	(p2) ldf8.c.nc		f10 = [r3],9
	(p32) ldf8.c.nc		f10 = [r3],9
	(p2) ldf8.c.nc		f120 = [r3],9
	(p2) ldf8.c.nc		f10 = [r63],9
	(p0) ldf8.c.nc.nt1		f10 = [r3]
	(p16) ldf8.c.nc.nt1		f10 = [r3]
	(p0) ldf8.c.nc.nt1		f100 = [r3]
	(p0) ldf8.c.nc.nt1		f10 = [r103]
	(p1) ldf8.c.nc.nt1		f10 = [r3],r2
	(p41) ldf8.c.nc.nt1		f10 = [r3],r2
	(p1) ldf8.c.nc.nt1		f110 = [r3],r2
	(p1) ldf8.c.nc.nt1		f10 = [r93],r2
	(p1) ldf8.c.nc.nt1		f10 = [r3],r92
	(p2) ldf8.c.nc.nt1		f10 = [r3],9
	(p32) ldf8.c.nc.nt1		f10 = [r3],9
	(p2) ldf8.c.nc.nt1		f120 = [r3],9
	(p2) ldf8.c.nc.nt1		f10 = [r63],9
	(p0) ldf8.c.nc.nta		f10 = [r3]
	(p16) ldf8.c.nc.nta		f10 = [r3]
	(p0) ldf8.c.nc.nta		f100 = [r3]
	(p0) ldf8.c.nc.nta		f10 = [r103]
	(p1) ldf8.c.nc.nta		f10 = [r3],r2
	(p41) ldf8.c.nc.nta		f10 = [r3],r2
	(p1) ldf8.c.nc.nta		f110 = [r3],r2
	(p1) ldf8.c.nc.nta		f10 = [r93],r2
	(p1) ldf8.c.nc.nta		f10 = [r3],r92
	(p2) ldf8.c.nc.nta		f10 = [r3],9
	(p32) ldf8.c.nc.nta		f10 = [r3],9
	(p2) ldf8.c.nc.nta		f120 = [r3],9
	(p2) ldf8.c.nc.nta		f10 = [r63],9
	(p0) ldf8.c.clr		f10 = [r3]
	(p16) ldf8.c.clr		f10 = [r3]
	(p0) ldf8.c.clr		f100 = [r3]
	(p0) ldf8.c.clr		f10 = [r103]
	(p1) ldf8.c.clr		f10 = [r3],r2
	(p41) ldf8.c.clr		f10 = [r3],r2
	(p1) ldf8.c.clr		f110 = [r3],r2
	(p1) ldf8.c.clr		f10 = [r93],r2
	(p1) ldf8.c.clr		f10 = [r3],r92
	(p2) ldf8.c.clr		f10 = [r3],9
	(p32) ldf8.c.clr		f10 = [r3],9
	(p2) ldf8.c.clr		f120 = [r3],9
	(p2) ldf8.c.clr		f10 = [r63],9
	(p0) ldf8.c.clr.nt1		f10 = [r3]
	(p16) ldf8.c.clr.nt1		f10 = [r3]
	(p0) ldf8.c.clr.nt1		f100 = [r3]
	(p0) ldf8.c.clr.nt1		f10 = [r103]
	(p1) ldf8.c.clr.nt1		f10 = [r3],r2
	(p41) ldf8.c.clr.nt1		f10 = [r3],r2
	(p1) ldf8.c.clr.nt1		f110 = [r3],r2
	(p1) ldf8.c.clr.nt1		f10 = [r93],r2
	(p1) ldf8.c.clr.nt1		f10 = [r3],r92
	(p2) ldf8.c.clr.nt1		f10 = [r3],9
	(p32) ldf8.c.clr.nt1		f10 = [r3],9
	(p2) ldf8.c.clr.nt1		f120 = [r3],9
	(p2) ldf8.c.clr.nt1		f10 = [r63],9
	(p0) ldf8.c.clr.nta		f10 = [r3]
	(p16) ldf8.c.clr.nta		f10 = [r3]
	(p0) ldf8.c.clr.nta		f100 = [r3]
	(p0) ldf8.c.clr.nta		f10 = [r103]
	(p1) ldf8.c.clr.nta		f10 = [r3],r2
	(p41) ldf8.c.clr.nta		f10 = [r3],r2
	(p1) ldf8.c.clr.nta		f110 = [r3],r2
	(p1) ldf8.c.clr.nta		f10 = [r93],r2
	(p1) ldf8.c.clr.nta		f10 = [r3],r92
	(p2) ldf8.c.clr.nta		f10 = [r3],9
	(p32) ldf8.c.clr.nta		f10 = [r3],9
	(p2) ldf8.c.clr.nta		f120 = [r3],9
	(p2) ldf8.c.clr.nta		f10 = [r63],9
	(p0) ldf.fill		f10 = [r3]
	(p16) ldf.fill		f10 = [r3]
	(p0) ldf.fill		f100 = [r3]
	(p0) ldf.fill		f10 = [r103]
	(p1) ldf.fill		f10 = [r3],r2
	(p41) ldf.fill		f10 = [r3],r2
	(p1) ldf.fill		f110 = [r3],r2
	(p1) ldf.fill		f10 = [r93],r2
	(p1) ldf.fill		f10 = [r3],r92
	(p2) ldf.fill		f10 = [r3],9
	(p32) ldf.fill		f10 = [r3],9
	(p2) ldf.fill		f120 = [r3],9
	(p2) ldf.fill		f10 = [r63],9
	(p0) ldf.fill.nt1		f10 = [r3]
	(p16) ldf.fill.nt1		f10 = [r3]
	(p0) ldf.fill.nt1		f100 = [r3]
	(p0) ldf.fill.nt1		f10 = [r103]
	(p1) ldf.fill.nt1		f10 = [r3],r2
	(p41) ldf.fill.nt1		f10 = [r3],r2
	(p1) ldf.fill.nt1		f110 = [r3],r2
	(p1) ldf.fill.nt1		f10 = [r93],r2
	(p1) ldf.fill.nt1		f10 = [r3],r92
	(p2) ldf.fill.nt1		f10 = [r3],9
	(p32) ldf.fill.nt1		f10 = [r3],9
	(p2) ldf.fill.nt1		f120 = [r3],9
	(p2) ldf.fill.nt1		f10 = [r63],9
	(p0) ldf.fill.nta		f10 = [r3]
	(p16) ldf.fill.nta		f10 = [r3]
	(p0) ldf.fill.nta		f100 = [r3]
	(p0) ldf.fill.nta		f10 = [r103]
	(p1) ldf.fill.nta		f10 = [r3],r2
	(p41) ldf.fill.nta		f10 = [r3],r2
	(p1) ldf.fill.nta		f110 = [r3],r2
	(p1) ldf.fill.nta		f10 = [r93],r2
	(p1) ldf.fill.nta		f10 = [r3],r92
	(p2) ldf.fill.nta		f10 = [r3],9
	(p32) ldf.fill.nta		f10 = [r3],9
	(p2) ldf.fill.nta		f120 = [r3],9
	(p2) ldf.fill.nta		f10 = [r63],9

	(p0) ldfps		f10,f11 = [r3]
	(p16) ldfps		f10,f11 = [r3]
	(p0) ldfps		f110,f11 = [r3]
	(p0) ldfps		f10,f111 = [r3]
	(p0) ldfps		f10,f11 = [r53]
	(p18) ldfps		f10,f11 = [r3],8
	(p2) ldfps		f120,f11 = [r3],8
	(p2) ldfps		f10,f121 = [r3],8
	(p2) ldfps		f10,f11 = [r123],8
	(p0) ldfps.nt1		f10,f11 = [r3]
	(p16) ldfps.nt1		f10,f11 = [r3]
	(p0) ldfps.nt1		f110,f11 = [r3]
	(p0) ldfps.nt1		f10,f111 = [r3]
	(p0) ldfps.nt1		f10,f11 = [r53]
	(p18) ldfps.nt1		f10,f11 = [r3],8
	(p2) ldfps.nt1		f120,f11 = [r3],8
	(p2) ldfps.nt1		f10,f121 = [r3],8
	(p2) ldfps.nt1		f10,f11 = [r123],8
	(p0) ldfps.nta		f10,f11 = [r3]
	(p16) ldfps.nta		f10,f11 = [r3]
	(p0) ldfps.nta		f110,f11 = [r3]
	(p0) ldfps.nta		f10,f111 = [r3]
	(p0) ldfps.nta		f10,f11 = [r53]
	(p18) ldfps.nta		f10,f11 = [r3],8
	(p2) ldfps.nta		f120,f11 = [r3],8
	(p2) ldfps.nta		f10,f121 = [r3],8
	(p2) ldfps.nta		f10,f11 = [r123],8
	(p0) ldfps.s		f10,f11 = [r3]
	(p16) ldfps.s		f10,f11 = [r3]
	(p0) ldfps.s		f110,f11 = [r3]
	(p0) ldfps.s		f10,f111 = [r3]
	(p0) ldfps.s		f10,f11 = [r53]
	(p18) ldfps.s		f10,f11 = [r3],8
	(p2) ldfps.s		f120,f11 = [r3],8
	(p2) ldfps.s		f10,f121 = [r3],8
	(p2) ldfps.s		f10,f11 = [r123],8
	(p0) ldfps.s.nt1		f10,f11 = [r3]
	(p16) ldfps.s.nt1		f10,f11 = [r3]
	(p0) ldfps.s.nt1		f110,f11 = [r3]
	(p0) ldfps.s.nt1		f10,f111 = [r3]
	(p0) ldfps.s.nt1		f10,f11 = [r53]
	(p18) ldfps.s.nt1		f10,f11 = [r3],8
	(p2) ldfps.s.nt1		f120,f11 = [r3],8
	(p2) ldfps.s.nt1		f10,f121 = [r3],8
	(p2) ldfps.s.nt1		f10,f11 = [r123],8
	(p0) ldfps.s.nta		f10,f11 = [r3]
	(p16) ldfps.s.nta		f10,f11 = [r3]
	(p0) ldfps.s.nta		f110,f11 = [r3]
	(p0) ldfps.s.nta		f10,f111 = [r3]
	(p0) ldfps.s.nta		f10,f11 = [r53]
	(p18) ldfps.s.nta		f10,f11 = [r3],8
	(p2) ldfps.s.nta		f120,f11 = [r3],8
	(p2) ldfps.s.nta		f10,f121 = [r3],8
	(p2) ldfps.s.nta		f10,f11 = [r123],8
	(p0) ldfps.a		f10,f11 = [r3]
	(p16) ldfps.a		f10,f11 = [r3]
	(p0) ldfps.a		f110,f11 = [r3]
	(p0) ldfps.a		f10,f111 = [r3]
	(p0) ldfps.a		f10,f11 = [r53]
	(p18) ldfps.a		f10,f11 = [r3],8
	(p2) ldfps.a		f120,f11 = [r3],8
	(p2) ldfps.a		f10,f121 = [r3],8
	(p2) ldfps.a		f10,f11 = [r123],8
	(p0) ldfps.a.nt1		f10,f11 = [r3]
	(p16) ldfps.a.nt1		f10,f11 = [r3]
	(p0) ldfps.a.nt1		f110,f11 = [r3]
	(p0) ldfps.a.nt1		f10,f111 = [r3]
	(p0) ldfps.a.nt1		f10,f11 = [r53]
	(p18) ldfps.a.nt1		f10,f11 = [r3],8
	(p2) ldfps.a.nt1		f120,f11 = [r3],8
	(p2) ldfps.a.nt1		f10,f121 = [r3],8
	(p2) ldfps.a.nt1		f10,f11 = [r123],8
	(p0) ldfps.a.nta		f10,f11 = [r3]
	(p16) ldfps.a.nta		f10,f11 = [r3]
	(p0) ldfps.a.nta		f110,f11 = [r3]
	(p0) ldfps.a.nta		f10,f111 = [r3]
	(p0) ldfps.a.nta		f10,f11 = [r53]
	(p18) ldfps.a.nta		f10,f11 = [r3],8
	(p2) ldfps.a.nta		f120,f11 = [r3],8
	(p2) ldfps.a.nta		f10,f121 = [r3],8
	(p2) ldfps.a.nta		f10,f11 = [r123],8
	(p0) ldfps.sa		f10,f11 = [r3]
	(p16) ldfps.sa		f10,f11 = [r3]
	(p0) ldfps.sa		f110,f11 = [r3]
	(p0) ldfps.sa		f10,f111 = [r3]
	(p0) ldfps.sa		f10,f11 = [r53]
	(p18) ldfps.sa		f10,f11 = [r3],8
	(p2) ldfps.sa		f120,f11 = [r3],8
	(p2) ldfps.sa		f10,f121 = [r3],8
	(p2) ldfps.sa		f10,f11 = [r123],8
	(p0) ldfps.sa.nt1		f10,f11 = [r3]
	(p16) ldfps.sa.nt1		f10,f11 = [r3]
	(p0) ldfps.sa.nt1		f110,f11 = [r3]
	(p0) ldfps.sa.nt1		f10,f111 = [r3]
	(p0) ldfps.sa.nt1		f10,f11 = [r53]
	(p18) ldfps.sa.nt1		f10,f11 = [r3],8
	(p2) ldfps.sa.nt1		f120,f11 = [r3],8
	(p2) ldfps.sa.nt1		f10,f121 = [r3],8
	(p2) ldfps.sa.nt1		f10,f11 = [r123],8
	(p0) ldfps.sa.nta		f10,f11 = [r3]
	(p16) ldfps.sa.nta		f10,f11 = [r3]
	(p0) ldfps.sa.nta		f110,f11 = [r3]
	(p0) ldfps.sa.nta		f10,f111 = [r3]
	(p0) ldfps.sa.nta		f10,f11 = [r53]
	(p18) ldfps.sa.nta		f10,f11 = [r3],8
	(p2) ldfps.sa.nta		f120,f11 = [r3],8
	(p2) ldfps.sa.nta		f10,f121 = [r3],8
	(p2) ldfps.sa.nta		f10,f11 = [r123],8
	(p0) ldfps.c.nc		f10,f11 = [r3]
	(p16) ldfps.c.nc		f10,f11 = [r3]
	(p0) ldfps.c.nc		f110,f11 = [r3]
	(p0) ldfps.c.nc		f10,f111 = [r3]
	(p0) ldfps.c.nc		f10,f11 = [r53]
	(p18) ldfps.c.nc		f10,f11 = [r3],8
	(p2) ldfps.c.nc		f120,f11 = [r3],8
	(p2) ldfps.c.nc		f10,f121 = [r3],8
	(p2) ldfps.c.nc		f10,f11 = [r123],8
	(p0) ldfps.c.nc.nt1		f10,f11 = [r3]
	(p16) ldfps.c.nc.nt1		f10,f11 = [r3]
	(p0) ldfps.c.nc.nt1		f110,f11 = [r3]
	(p0) ldfps.c.nc.nt1		f10,f111 = [r3]
	(p0) ldfps.c.nc.nt1		f10,f11 = [r53]
	(p18) ldfps.c.nc.nt1		f10,f11 = [r3],8
	(p2) ldfps.c.nc.nt1		f120,f11 = [r3],8
	(p2) ldfps.c.nc.nt1		f10,f121 = [r3],8
	(p2) ldfps.c.nc.nt1		f10,f11 = [r123],8
	(p0) ldfps.c.nc.nta		f10,f11 = [r3]
	(p16) ldfps.c.nc.nta		f10,f11 = [r3]
	(p0) ldfps.c.nc.nta		f110,f11 = [r3]
	(p0) ldfps.c.nc.nta		f10,f111 = [r3]
	(p0) ldfps.c.nc.nta		f10,f11 = [r53]
	(p18) ldfps.c.nc.nta		f10,f11 = [r3],8
	(p2) ldfps.c.nc.nta		f120,f11 = [r3],8
	(p2) ldfps.c.nc.nta		f10,f121 = [r3],8
	(p2) ldfps.c.nc.nta		f10,f11 = [r123],8
	(p0) ldfps.c.clr		f10,f11 = [r3]
	(p16) ldfps.c.clr		f10,f11 = [r3]
	(p0) ldfps.c.clr		f110,f11 = [r3]
	(p0) ldfps.c.clr		f10,f111 = [r3]
	(p0) ldfps.c.clr		f10,f11 = [r53]
	(p18) ldfps.c.clr		f10,f11 = [r3],8
	(p2) ldfps.c.clr		f120,f11 = [r3],8
	(p2) ldfps.c.clr		f10,f121 = [r3],8
	(p2) ldfps.c.clr		f10,f11 = [r123],8
	(p0) ldfps.c.clr.nt1		f10,f11 = [r3]
	(p16) ldfps.c.clr.nt1		f10,f11 = [r3]
	(p0) ldfps.c.clr.nt1		f110,f11 = [r3]
	(p0) ldfps.c.clr.nt1		f10,f111 = [r3]
	(p0) ldfps.c.clr.nt1		f10,f11 = [r53]
	(p18) ldfps.c.clr.nt1		f10,f11 = [r3],8
	(p2) ldfps.c.clr.nt1		f120,f11 = [r3],8
	(p2) ldfps.c.clr.nt1		f10,f121 = [r3],8
	(p2) ldfps.c.clr.nt1		f10,f11 = [r123],8
	(p0) ldfps.c.clr.nta		f10,f11 = [r3]
	(p16) ldfps.c.clr.nta		f10,f11 = [r3]
	(p0) ldfps.c.clr.nta		f110,f11 = [r3]
	(p0) ldfps.c.clr.nta		f10,f111 = [r3]
	(p0) ldfps.c.clr.nta		f10,f11 = [r53]
	(p18) ldfps.c.clr.nta		f10,f11 = [r3],8
	(p2) ldfps.c.clr.nta		f120,f11 = [r3],8
	(p2) ldfps.c.clr.nta		f10,f121 = [r3],8
	(p2) ldfps.c.clr.nta		f10,f11 = [r123],8
	(p0) ldfpd		f10,f11 = [r3]
	(p16) ldfpd		f10,f11 = [r3]
	(p0) ldfpd		f110,f11 = [r3]
	(p0) ldfpd		f10,f111 = [r3]
	(p0) ldfpd		f10,f11 = [r53]
	(p18) ldfpd		f10,f11 = [r3],16
	(p2) ldfpd		f120,f11 = [r3],16
	(p2) ldfpd		f10,f121 = [r3],16
	(p2) ldfpd		f10,f11 = [r123],16
	(p0) ldfpd.nt1		f10,f11 = [r3]
	(p16) ldfpd.nt1		f10,f11 = [r3]
	(p0) ldfpd.nt1		f110,f11 = [r3]
	(p0) ldfpd.nt1		f10,f111 = [r3]
	(p0) ldfpd.nt1		f10,f11 = [r53]
	(p18) ldfpd.nt1		f10,f11 = [r3],16
	(p2) ldfpd.nt1		f120,f11 = [r3],16
	(p2) ldfpd.nt1		f10,f121 = [r3],16
	(p2) ldfpd.nt1		f10,f11 = [r123],16
	(p0) ldfpd.nta		f10,f11 = [r3]
	(p16) ldfpd.nta		f10,f11 = [r3]
	(p0) ldfpd.nta		f110,f11 = [r3]
	(p0) ldfpd.nta		f10,f111 = [r3]
	(p0) ldfpd.nta		f10,f11 = [r53]
	(p18) ldfpd.nta		f10,f11 = [r3],16
	(p2) ldfpd.nta		f120,f11 = [r3],16
	(p2) ldfpd.nta		f10,f121 = [r3],16
	(p2) ldfpd.nta		f10,f11 = [r123],16
	(p0) ldfpd.s		f10,f11 = [r3]
	(p16) ldfpd.s		f10,f11 = [r3]
	(p0) ldfpd.s		f110,f11 = [r3]
	(p0) ldfpd.s		f10,f111 = [r3]
	(p0) ldfpd.s		f10,f11 = [r53]
	(p18) ldfpd.s		f10,f11 = [r3],16
	(p2) ldfpd.s		f120,f11 = [r3],16
	(p2) ldfpd.s		f10,f121 = [r3],16
	(p2) ldfpd.s		f10,f11 = [r123],16
	(p0) ldfpd.s.nt1		f10,f11 = [r3]
	(p16) ldfpd.s.nt1		f10,f11 = [r3]
	(p0) ldfpd.s.nt1		f110,f11 = [r3]
	(p0) ldfpd.s.nt1		f10,f111 = [r3]
	(p0) ldfpd.s.nt1		f10,f11 = [r53]
	(p18) ldfpd.s.nt1		f10,f11 = [r3],16
	(p2) ldfpd.s.nt1		f120,f11 = [r3],16
	(p2) ldfpd.s.nt1		f10,f121 = [r3],16
	(p2) ldfpd.s.nt1		f10,f11 = [r123],16
	(p0) ldfpd.s.nta		f10,f11 = [r3]
	(p16) ldfpd.s.nta		f10,f11 = [r3]
	(p0) ldfpd.s.nta		f110,f11 = [r3]
	(p0) ldfpd.s.nta		f10,f111 = [r3]
	(p0) ldfpd.s.nta		f10,f11 = [r53]
	(p18) ldfpd.s.nta		f10,f11 = [r3],16
	(p2) ldfpd.s.nta		f120,f11 = [r3],16
	(p2) ldfpd.s.nta		f10,f121 = [r3],16
	(p2) ldfpd.s.nta		f10,f11 = [r123],16
	(p0) ldfpd.a		f10,f11 = [r3]
	(p16) ldfpd.a		f10,f11 = [r3]
	(p0) ldfpd.a		f110,f11 = [r3]
	(p0) ldfpd.a		f10,f111 = [r3]
	(p0) ldfpd.a		f10,f11 = [r53]
	(p18) ldfpd.a		f10,f11 = [r3],16
	(p2) ldfpd.a		f120,f11 = [r3],16
	(p2) ldfpd.a		f10,f121 = [r3],16
	(p2) ldfpd.a		f10,f11 = [r123],16
	(p0) ldfpd.a.nt1		f10,f11 = [r3]
	(p16) ldfpd.a.nt1		f10,f11 = [r3]
	(p0) ldfpd.a.nt1		f110,f11 = [r3]
	(p0) ldfpd.a.nt1		f10,f111 = [r3]
	(p0) ldfpd.a.nt1		f10,f11 = [r53]
	(p18) ldfpd.a.nt1		f10,f11 = [r3],16
	(p2) ldfpd.a.nt1		f120,f11 = [r3],16
	(p2) ldfpd.a.nt1		f10,f121 = [r3],16
	(p2) ldfpd.a.nt1		f10,f11 = [r123],16
	(p0) ldfpd.a.nta		f10,f11 = [r3]
	(p16) ldfpd.a.nta		f10,f11 = [r3]
	(p0) ldfpd.a.nta		f110,f11 = [r3]
	(p0) ldfpd.a.nta		f10,f111 = [r3]
	(p0) ldfpd.a.nta		f10,f11 = [r53]
	(p18) ldfpd.a.nta		f10,f11 = [r3],16
	(p2) ldfpd.a.nta		f120,f11 = [r3],16
	(p2) ldfpd.a.nta		f10,f121 = [r3],16
	(p2) ldfpd.a.nta		f10,f11 = [r123],16
	(p0) ldfpd.sa		f10,f11 = [r3]
	(p16) ldfpd.sa		f10,f11 = [r3]
	(p0) ldfpd.sa		f110,f11 = [r3]
	(p0) ldfpd.sa		f10,f111 = [r3]
	(p0) ldfpd.sa		f10,f11 = [r53]
	(p18) ldfpd.sa		f10,f11 = [r3],16
	(p2) ldfpd.sa		f120,f11 = [r3],16
	(p2) ldfpd.sa		f10,f121 = [r3],16
	(p2) ldfpd.sa		f10,f11 = [r123],16
	(p0) ldfpd.sa.nt1		f10,f11 = [r3]
	(p16) ldfpd.sa.nt1		f10,f11 = [r3]
	(p0) ldfpd.sa.nt1		f110,f11 = [r3]
	(p0) ldfpd.sa.nt1		f10,f111 = [r3]
	(p0) ldfpd.sa.nt1		f10,f11 = [r53]
	(p18) ldfpd.sa.nt1		f10,f11 = [r3],16
	(p2) ldfpd.sa.nt1		f120,f11 = [r3],16
	(p2) ldfpd.sa.nt1		f10,f121 = [r3],16
	(p2) ldfpd.sa.nt1		f10,f11 = [r123],16
	(p0) ldfpd.sa.nta		f10,f11 = [r3]
	(p16) ldfpd.sa.nta		f10,f11 = [r3]
	(p0) ldfpd.sa.nta		f110,f11 = [r3]
	(p0) ldfpd.sa.nta		f10,f111 = [r3]
	(p0) ldfpd.sa.nta		f10,f11 = [r53]
	(p18) ldfpd.sa.nta		f10,f11 = [r3],16
	(p2) ldfpd.sa.nta		f120,f11 = [r3],16
	(p2) ldfpd.sa.nta		f10,f121 = [r3],16
	(p2) ldfpd.sa.nta		f10,f11 = [r123],16
	(p0) ldfpd.c.nc		f10,f11 = [r3]
	(p16) ldfpd.c.nc		f10,f11 = [r3]
	(p0) ldfpd.c.nc		f110,f11 = [r3]
	(p0) ldfpd.c.nc		f10,f111 = [r3]
	(p0) ldfpd.c.nc		f10,f11 = [r53]
	(p18) ldfpd.c.nc		f10,f11 = [r3],16
	(p2) ldfpd.c.nc		f120,f11 = [r3],16
	(p2) ldfpd.c.nc		f10,f121 = [r3],16
	(p2) ldfpd.c.nc		f10,f11 = [r123],16
	(p0) ldfpd.c.nc.nt1		f10,f11 = [r3]
	(p16) ldfpd.c.nc.nt1		f10,f11 = [r3]
	(p0) ldfpd.c.nc.nt1		f110,f11 = [r3]
	(p0) ldfpd.c.nc.nt1		f10,f111 = [r3]
	(p0) ldfpd.c.nc.nt1		f10,f11 = [r53]
	(p18) ldfpd.c.nc.nt1		f10,f11 = [r3],16
	(p2) ldfpd.c.nc.nt1		f120,f11 = [r3],16
	(p2) ldfpd.c.nc.nt1		f10,f121 = [r3],16
	(p2) ldfpd.c.nc.nt1		f10,f11 = [r123],16
	(p0) ldfpd.c.nc.nta		f10,f11 = [r3]
	(p16) ldfpd.c.nc.nta		f10,f11 = [r3]
	(p0) ldfpd.c.nc.nta		f110,f11 = [r3]
	(p0) ldfpd.c.nc.nta		f10,f111 = [r3]
	(p0) ldfpd.c.nc.nta		f10,f11 = [r53]
	(p18) ldfpd.c.nc.nta		f10,f11 = [r3],16
	(p2) ldfpd.c.nc.nta		f120,f11 = [r3],16
	(p2) ldfpd.c.nc.nta		f10,f121 = [r3],16
	(p2) ldfpd.c.nc.nta		f10,f11 = [r123],16
	(p0) ldfpd.c.clr		f10,f11 = [r3]
	(p16) ldfpd.c.clr		f10,f11 = [r3]
	(p0) ldfpd.c.clr		f110,f11 = [r3]
	(p0) ldfpd.c.clr		f10,f111 = [r3]
	(p0) ldfpd.c.clr		f10,f11 = [r53]
	(p18) ldfpd.c.clr		f10,f11 = [r3],16
	(p2) ldfpd.c.clr		f120,f11 = [r3],16
	(p2) ldfpd.c.clr		f10,f121 = [r3],16
	(p2) ldfpd.c.clr		f10,f11 = [r123],16
	(p0) ldfpd.c.clr.nt1		f10,f11 = [r3]
	(p16) ldfpd.c.clr.nt1		f10,f11 = [r3]
	(p0) ldfpd.c.clr.nt1		f110,f11 = [r3]
	(p0) ldfpd.c.clr.nt1		f10,f111 = [r3]
	(p0) ldfpd.c.clr.nt1		f10,f11 = [r53]
	(p18) ldfpd.c.clr.nt1		f10,f11 = [r3],16
	(p2) ldfpd.c.clr.nt1		f120,f11 = [r3],16
	(p2) ldfpd.c.clr.nt1		f10,f121 = [r3],16
	(p2) ldfpd.c.clr.nt1		f10,f11 = [r123],16
	(p0) ldfpd.c.clr.nta		f10,f11 = [r3]
	(p16) ldfpd.c.clr.nta		f10,f11 = [r3]
	(p0) ldfpd.c.clr.nta		f110,f11 = [r3]
	(p0) ldfpd.c.clr.nta		f10,f111 = [r3]
	(p0) ldfpd.c.clr.nta		f10,f11 = [r53]
	(p18) ldfpd.c.clr.nta		f10,f11 = [r3],16
	(p2) ldfpd.c.clr.nta		f120,f11 = [r3],16
	(p2) ldfpd.c.clr.nta		f10,f121 = [r3],16
	(p2) ldfpd.c.clr.nta		f10,f11 = [r123],16
	(p0) ldfp8		f10,f11 = [r3]
	(p16) ldfp8		f10,f11 = [r3]
	(p0) ldfp8		f110,f11 = [r3]
	(p0) ldfp8		f10,f111 = [r3]
	(p0) ldfp8		f10,f11 = [r53]
	(p18) ldfp8		f10,f11 = [r3],16
	(p2) ldfp8		f120,f11 = [r3],16
	(p2) ldfp8		f10,f121 = [r3],16
	(p2) ldfp8		f10,f11 = [r123],16
	(p0) ldfp8.nt1		f10,f11 = [r3]
	(p16) ldfp8.nt1		f10,f11 = [r3]
	(p0) ldfp8.nt1		f110,f11 = [r3]
	(p0) ldfp8.nt1		f10,f111 = [r3]
	(p0) ldfp8.nt1		f10,f11 = [r53]
	(p18) ldfp8.nt1		f10,f11 = [r3],16
	(p2) ldfp8.nt1		f120,f11 = [r3],16
	(p2) ldfp8.nt1		f10,f121 = [r3],16
	(p2) ldfp8.nt1		f10,f11 = [r123],16
	(p0) ldfp8.nta		f10,f11 = [r3]
	(p16) ldfp8.nta		f10,f11 = [r3]
	(p0) ldfp8.nta		f110,f11 = [r3]
	(p0) ldfp8.nta		f10,f111 = [r3]
	(p0) ldfp8.nta		f10,f11 = [r53]
	(p18) ldfp8.nta		f10,f11 = [r3],16
	(p2) ldfp8.nta		f120,f11 = [r3],16
	(p2) ldfp8.nta		f10,f121 = [r3],16
	(p2) ldfp8.nta		f10,f11 = [r123],16
	(p0) ldfp8.s		f10,f11 = [r3]
	(p16) ldfp8.s		f10,f11 = [r3]
	(p0) ldfp8.s		f110,f11 = [r3]
	(p0) ldfp8.s		f10,f111 = [r3]
	(p0) ldfp8.s		f10,f11 = [r53]
	(p18) ldfp8.s		f10,f11 = [r3],16
	(p2) ldfp8.s		f120,f11 = [r3],16
	(p2) ldfp8.s		f10,f121 = [r3],16
	(p2) ldfp8.s		f10,f11 = [r123],16
	(p0) ldfp8.s.nt1		f10,f11 = [r3]
	(p16) ldfp8.s.nt1		f10,f11 = [r3]
	(p0) ldfp8.s.nt1		f110,f11 = [r3]
	(p0) ldfp8.s.nt1		f10,f111 = [r3]
	(p0) ldfp8.s.nt1		f10,f11 = [r53]
	(p18) ldfp8.s.nt1		f10,f11 = [r3],16
	(p2) ldfp8.s.nt1		f120,f11 = [r3],16
	(p2) ldfp8.s.nt1		f10,f121 = [r3],16
	(p2) ldfp8.s.nt1		f10,f11 = [r123],16
	(p0) ldfp8.s.nta		f10,f11 = [r3]
	(p16) ldfp8.s.nta		f10,f11 = [r3]
	(p0) ldfp8.s.nta		f110,f11 = [r3]
	(p0) ldfp8.s.nta		f10,f111 = [r3]
	(p0) ldfp8.s.nta		f10,f11 = [r53]
	(p18) ldfp8.s.nta		f10,f11 = [r3],16
	(p2) ldfp8.s.nta		f120,f11 = [r3],16
	(p2) ldfp8.s.nta		f10,f121 = [r3],16
	(p2) ldfp8.s.nta		f10,f11 = [r123],16
	(p0) ldfp8.a		f10,f11 = [r3]
	(p16) ldfp8.a		f10,f11 = [r3]
	(p0) ldfp8.a		f110,f11 = [r3]
	(p0) ldfp8.a		f10,f111 = [r3]
	(p0) ldfp8.a		f10,f11 = [r53]
	(p18) ldfp8.a		f10,f11 = [r3],16
	(p2) ldfp8.a		f120,f11 = [r3],16
	(p2) ldfp8.a		f10,f121 = [r3],16
	(p2) ldfp8.a		f10,f11 = [r123],16
	(p0) ldfp8.a.nt1		f10,f11 = [r3]
	(p16) ldfp8.a.nt1		f10,f11 = [r3]
	(p0) ldfp8.a.nt1		f110,f11 = [r3]
	(p0) ldfp8.a.nt1		f10,f111 = [r3]
	(p0) ldfp8.a.nt1		f10,f11 = [r53]
	(p18) ldfp8.a.nt1		f10,f11 = [r3],16
	(p2) ldfp8.a.nt1		f120,f11 = [r3],16
	(p2) ldfp8.a.nt1		f10,f121 = [r3],16
	(p2) ldfp8.a.nt1		f10,f11 = [r123],16
	(p0) ldfp8.a.nta		f10,f11 = [r3]
	(p16) ldfp8.a.nta		f10,f11 = [r3]
	(p0) ldfp8.a.nta		f110,f11 = [r3]
	(p0) ldfp8.a.nta		f10,f111 = [r3]
	(p0) ldfp8.a.nta		f10,f11 = [r53]
	(p18) ldfp8.a.nta		f10,f11 = [r3],16
	(p2) ldfp8.a.nta		f120,f11 = [r3],16
	(p2) ldfp8.a.nta		f10,f121 = [r3],16
	(p2) ldfp8.a.nta		f10,f11 = [r123],16
	(p0) ldfp8.sa		f10,f11 = [r3]
	(p16) ldfp8.sa		f10,f11 = [r3]
	(p0) ldfp8.sa		f110,f11 = [r3]
	(p0) ldfp8.sa		f10,f111 = [r3]
	(p0) ldfp8.sa		f10,f11 = [r53]
	(p18) ldfp8.sa		f10,f11 = [r3],16
	(p2) ldfp8.sa		f120,f11 = [r3],16
	(p2) ldfp8.sa		f10,f121 = [r3],16
	(p2) ldfp8.sa		f10,f11 = [r123],16
	(p0) ldfp8.sa.nt1		f10,f11 = [r3]
	(p16) ldfp8.sa.nt1		f10,f11 = [r3]
	(p0) ldfp8.sa.nt1		f110,f11 = [r3]
	(p0) ldfp8.sa.nt1		f10,f111 = [r3]
	(p0) ldfp8.sa.nt1		f10,f11 = [r53]
	(p18) ldfp8.sa.nt1		f10,f11 = [r3],16
	(p2) ldfp8.sa.nt1		f120,f11 = [r3],16
	(p2) ldfp8.sa.nt1		f10,f121 = [r3],16
	(p2) ldfp8.sa.nt1		f10,f11 = [r123],16
	(p0) ldfp8.sa.nta		f10,f11 = [r3]
	(p16) ldfp8.sa.nta		f10,f11 = [r3]
	(p0) ldfp8.sa.nta		f110,f11 = [r3]
	(p0) ldfp8.sa.nta		f10,f111 = [r3]
	(p0) ldfp8.sa.nta		f10,f11 = [r53]
	(p18) ldfp8.sa.nta		f10,f11 = [r3],16
	(p2) ldfp8.sa.nta		f120,f11 = [r3],16
	(p2) ldfp8.sa.nta		f10,f121 = [r3],16
	(p2) ldfp8.sa.nta		f10,f11 = [r123],16
	(p0) ldfp8.c.nc		f10,f11 = [r3]
	(p16) ldfp8.c.nc		f10,f11 = [r3]
	(p0) ldfp8.c.nc		f110,f11 = [r3]
	(p0) ldfp8.c.nc		f10,f111 = [r3]
	(p0) ldfp8.c.nc		f10,f11 = [r53]
	(p18) ldfp8.c.nc		f10,f11 = [r3],16
	(p2) ldfp8.c.nc		f120,f11 = [r3],16
	(p2) ldfp8.c.nc		f10,f121 = [r3],16
	(p2) ldfp8.c.nc		f10,f11 = [r123],16
	(p0) ldfp8.c.nc.nt1		f10,f11 = [r3]
	(p16) ldfp8.c.nc.nt1		f10,f11 = [r3]
	(p0) ldfp8.c.nc.nt1		f110,f11 = [r3]
	(p0) ldfp8.c.nc.nt1		f10,f111 = [r3]
	(p0) ldfp8.c.nc.nt1		f10,f11 = [r53]
	(p18) ldfp8.c.nc.nt1		f10,f11 = [r3],16
	(p2) ldfp8.c.nc.nt1		f120,f11 = [r3],16
	(p2) ldfp8.c.nc.nt1		f10,f121 = [r3],16
	(p2) ldfp8.c.nc.nt1		f10,f11 = [r123],16
	(p0) ldfp8.c.nc.nta		f10,f11 = [r3]
	(p16) ldfp8.c.nc.nta		f10,f11 = [r3]
	(p0) ldfp8.c.nc.nta		f110,f11 = [r3]
	(p0) ldfp8.c.nc.nta		f10,f111 = [r3]
	(p0) ldfp8.c.nc.nta		f10,f11 = [r53]
	(p18) ldfp8.c.nc.nta		f10,f11 = [r3],16
	(p2) ldfp8.c.nc.nta		f120,f11 = [r3],16
	(p2) ldfp8.c.nc.nta		f10,f121 = [r3],16
	(p2) ldfp8.c.nc.nta		f10,f11 = [r123],16
	(p0) ldfp8.c.clr		f10,f11 = [r3]
	(p16) ldfp8.c.clr		f10,f11 = [r3]
	(p0) ldfp8.c.clr		f110,f11 = [r3]
	(p0) ldfp8.c.clr		f10,f111 = [r3]
	(p0) ldfp8.c.clr		f10,f11 = [r53]
	(p18) ldfp8.c.clr		f10,f11 = [r3],16
	(p2) ldfp8.c.clr		f120,f11 = [r3],16
	(p2) ldfp8.c.clr		f10,f121 = [r3],16
	(p2) ldfp8.c.clr		f10,f11 = [r123],16
	(p0) ldfp8.c.clr.nt1		f10,f11 = [r3]
	(p16) ldfp8.c.clr.nt1		f10,f11 = [r3]
	(p0) ldfp8.c.clr.nt1		f110,f11 = [r3]
	(p0) ldfp8.c.clr.nt1		f10,f111 = [r3]
	(p0) ldfp8.c.clr.nt1		f10,f11 = [r53]
	(p18) ldfp8.c.clr.nt1		f10,f11 = [r3],16
	(p2) ldfp8.c.clr.nt1		f120,f11 = [r3],16
	(p2) ldfp8.c.clr.nt1		f10,f121 = [r3],16
	(p2) ldfp8.c.clr.nt1		f10,f11 = [r123],16
	(p0) ldfp8.c.clr.nta		f10,f11 = [r3]
	(p16) ldfp8.c.clr.nta		f10,f11 = [r3]
	(p0) ldfp8.c.clr.nta		f110,f11 = [r3]
	(p0) ldfp8.c.clr.nta		f10,f111 = [r3]
	(p0) ldfp8.c.clr.nta		f10,f11 = [r53]
	(p18) ldfp8.c.clr.nta		f10,f11 = [r3],16
	(p2) ldfp8.c.clr.nta		f120,f11 = [r3],16
	(p2) ldfp8.c.clr.nta		f10,f121 = [r3],16
	(p2) ldfp8.c.clr.nta		f10,f11 = [r123],16

	(p0) lfetch		[r3]
	(p16) lfetch		[r3]
	(p0) lfetch		[r43]
	(p1) lfetch		[r3],r2
	(p17) lfetch		[r3],r2
	(p1) lfetch		[r53],r2
	(p1) lfetch		[r3],r52
	(p2) lfetch		[r3],9
	(p22) lfetch		[r3],9
	(p2) lfetch		[r63],9
	(p0) lfetch.nt1		[r3]
	(p16) lfetch.nt1		[r3]
	(p0) lfetch.nt1		[r43]
	(p1) lfetch.nt1		[r3],r2
	(p17) lfetch.nt1		[r3],r2
	(p1) lfetch.nt1		[r53],r2
	(p1) lfetch.nt1		[r3],r52
	(p2) lfetch.nt1		[r3],9
	(p22) lfetch.nt1		[r3],9
	(p2) lfetch.nt1		[r63],9
	(p0) lfetch.nt2		[r3]
	(p16) lfetch.nt2		[r3]
	(p0) lfetch.nt2		[r43]
	(p1) lfetch.nt2		[r3],r2
	(p17) lfetch.nt2		[r3],r2
	(p1) lfetch.nt2		[r53],r2
	(p1) lfetch.nt2		[r3],r52
	(p2) lfetch.nt2		[r3],9
	(p22) lfetch.nt2		[r3],9
	(p2) lfetch.nt2		[r63],9
	(p0) lfetch.nta		[r3]
	(p16) lfetch.nta		[r3]
	(p0) lfetch.nta		[r43]
	(p1) lfetch.nta		[r3],r2
	(p17) lfetch.nta		[r3],r2
	(p1) lfetch.nta		[r53],r2
	(p1) lfetch.nta		[r3],r52
	(p2) lfetch.nta		[r3],9
	(p22) lfetch.nta		[r3],9
	(p2) lfetch.nta		[r63],9
	(p0) lfetch.excl		[r3]
	(p16) lfetch.excl		[r3]
	(p0) lfetch.excl		[r43]
	(p1) lfetch.excl		[r3],r2
	(p17) lfetch.excl		[r3],r2
	(p1) lfetch.excl		[r53],r2
	(p1) lfetch.excl		[r3],r52
	(p2) lfetch.excl		[r3],9
	(p22) lfetch.excl		[r3],9
	(p2) lfetch.excl		[r63],9
	(p0) lfetch.excl.nt1		[r3]
	(p16) lfetch.excl.nt1		[r3]
	(p0) lfetch.excl.nt1		[r43]
	(p1) lfetch.excl.nt1		[r3],r2
	(p17) lfetch.excl.nt1		[r3],r2
	(p1) lfetch.excl.nt1		[r53],r2
	(p1) lfetch.excl.nt1		[r3],r52
	(p2) lfetch.excl.nt1		[r3],9
	(p22) lfetch.excl.nt1		[r3],9
	(p2) lfetch.excl.nt1		[r63],9
	(p0) lfetch.excl.nt2		[r3]
	(p16) lfetch.excl.nt2		[r3]
	(p0) lfetch.excl.nt2		[r43]
	(p1) lfetch.excl.nt2		[r3],r2
	(p17) lfetch.excl.nt2		[r3],r2
	(p1) lfetch.excl.nt2		[r53],r2
	(p1) lfetch.excl.nt2		[r3],r52
	(p2) lfetch.excl.nt2		[r3],9
	(p22) lfetch.excl.nt2		[r3],9
	(p2) lfetch.excl.nt2		[r63],9
	(p0) lfetch.excl.nta		[r3]
	(p16) lfetch.excl.nta		[r3]
	(p0) lfetch.excl.nta		[r43]
	(p1) lfetch.excl.nta		[r3],r2
	(p17) lfetch.excl.nta		[r3],r2
	(p1) lfetch.excl.nta		[r53],r2
	(p1) lfetch.excl.nta		[r3],r52
	(p2) lfetch.excl.nta		[r3],9
	(p22) lfetch.excl.nta		[r3],9
	(p2) lfetch.excl.nta		[r63],9
	(p0) lfetch.fault		[r3]
	(p16) lfetch.fault		[r3]
	(p0) lfetch.fault		[r43]
	(p1) lfetch.fault		[r3],r2
	(p17) lfetch.fault		[r3],r2
	(p1) lfetch.fault		[r53],r2
	(p1) lfetch.fault		[r3],r52
	(p2) lfetch.fault		[r3],9
	(p22) lfetch.fault		[r3],9
	(p2) lfetch.fault		[r63],9
	(p0) lfetch.fault.nt1		[r3]
	(p16) lfetch.fault.nt1		[r3]
	(p0) lfetch.fault.nt1		[r43]
	(p1) lfetch.fault.nt1		[r3],r2
	(p17) lfetch.fault.nt1		[r3],r2
	(p1) lfetch.fault.nt1		[r53],r2
	(p1) lfetch.fault.nt1		[r3],r52
	(p2) lfetch.fault.nt1		[r3],9
	(p22) lfetch.fault.nt1		[r3],9
	(p2) lfetch.fault.nt1		[r63],9
	(p0) lfetch.fault.nt2		[r3]
	(p16) lfetch.fault.nt2		[r3]
	(p0) lfetch.fault.nt2		[r43]
	(p1) lfetch.fault.nt2		[r3],r2
	(p17) lfetch.fault.nt2		[r3],r2
	(p1) lfetch.fault.nt2		[r53],r2
	(p1) lfetch.fault.nt2		[r3],r52
	(p2) lfetch.fault.nt2		[r3],9
	(p22) lfetch.fault.nt2		[r3],9
	(p2) lfetch.fault.nt2		[r63],9
	(p0) lfetch.fault.nta		[r3]
	(p16) lfetch.fault.nta		[r3]
	(p0) lfetch.fault.nta		[r43]
	(p1) lfetch.fault.nta		[r3],r2
	(p17) lfetch.fault.nta		[r3],r2
	(p1) lfetch.fault.nta		[r53],r2
	(p1) lfetch.fault.nta		[r3],r52
	(p2) lfetch.fault.nta		[r3],9
	(p22) lfetch.fault.nta		[r3],9
	(p2) lfetch.fault.nta		[r63],9
	(p0) lfetch.fault.excl		[r3]
	(p16) lfetch.fault.excl		[r3]
	(p0) lfetch.fault.excl		[r43]
	(p1) lfetch.fault.excl		[r3],r2
	(p17) lfetch.fault.excl		[r3],r2
	(p1) lfetch.fault.excl		[r53],r2
	(p1) lfetch.fault.excl		[r3],r52
	(p2) lfetch.fault.excl		[r3],9
	(p22) lfetch.fault.excl		[r3],9
	(p2) lfetch.fault.excl		[r63],9
	(p0) lfetch.fault.excl.nt1		[r3]
	(p16) lfetch.fault.excl.nt1		[r3]
	(p0) lfetch.fault.excl.nt1		[r43]
	(p1) lfetch.fault.excl.nt1		[r3],r2
	(p17) lfetch.fault.excl.nt1		[r3],r2
	(p1) lfetch.fault.excl.nt1		[r53],r2
	(p1) lfetch.fault.excl.nt1		[r3],r52
	(p2) lfetch.fault.excl.nt1		[r3],9
	(p22) lfetch.fault.excl.nt1		[r3],9
	(p2) lfetch.fault.excl.nt1		[r63],9
	(p0) lfetch.fault.excl.nt2		[r3]
	(p16) lfetch.fault.excl.nt2		[r3]
	(p0) lfetch.fault.excl.nt2		[r43]
	(p1) lfetch.fault.excl.nt2		[r3],r2
	(p17) lfetch.fault.excl.nt2		[r3],r2
	(p1) lfetch.fault.excl.nt2		[r53],r2
	(p1) lfetch.fault.excl.nt2		[r3],r52
	(p2) lfetch.fault.excl.nt2		[r3],9
	(p22) lfetch.fault.excl.nt2		[r3],9
	(p2) lfetch.fault.excl.nt2		[r63],9
	(p0) lfetch.fault.excl.nta		[r3]
	(p16) lfetch.fault.excl.nta		[r3]
	(p0) lfetch.fault.excl.nta		[r43]
	(p1) lfetch.fault.excl.nta		[r3],r2
	(p17) lfetch.fault.excl.nta		[r3],r2
	(p1) lfetch.fault.excl.nta		[r53],r2
	(p1) lfetch.fault.excl.nta		[r3],r52
	(p2) lfetch.fault.excl.nta		[r3],9
	(p22) lfetch.fault.excl.nta		[r3],9
	(p2) lfetch.fault.excl.nta		[r63],9

	loadrs

	//// ******** M *********
	
	(p6) mf
	(p16) mf
	(p7) mf.a
	(p17) mf.a

	(p8) mix1.l	r1 = r2, r3
	(p18) mix1.l	r1 = r2, r3
	(p8) mix1.l	r81 = r2, r3
	(p8) mix1.l	r1 = r82, r3
	(p8) mix1.l	r1 = r2, r83
	(p9) mix2.l	r1 = r2, r3
	(p19) mix2.l	r1 = r2, r3
	(p9) mix2.l	r91 = r2, r3
	(p9) mix2.l	r1 = r92, r3
	(p9) mix2.l	r1 = r2, r93
	(p2) mix4.l	r1 = r2, r3
	(p20) mix4.l	r1 = r2, r3
	(p2) mix4.l	r71 = r2, r3
	(p2) mix4.l	r1 = r72, r3
	(p2) mix4.l	r1 = r2, r73
	(p1) mix1.r	r1 = r2, r3
	(p21) mix1.r	r1 = r2, r3
	(p1) mix1.r	r61 = r2, r3
	(p1) mix1.r	r1 = r62, r3
	(p1) mix1.r	r1 = r2, r63
	(p2) mix2.r	r1 = r2, r3
	(p22) mix2.r	r1 = r2, r3
	(p2) mix2.r	r41 = r2, r3
	(p2) mix2.r	r1 = r42, r3
	(p2) mix2.r	r1 = r2, r43
	(p3) mix4.r	r1 = r2, r3
	(p23) mix4.r	r1 = r2, r3
	(p3) mix4.r	r51 = r2, r3
	(p3) mix4.r	r1 = r52, r3
	(p3) mix4.r	r1 = r2, r53

	// moves
	mov.i r1 = ar.bsp
	mov.i r1 = ar.bspstore
	mov.i r1 = ar.ccv
	mov.i r1 = ar.ec
	mov.i r1 = ar.fpsr
	mov.i r1 = ar.itc
	mov.i r1 = ar.lc
	mov.i r1 = ar.pfs
	mov.i r1 = ar.rnat
	mov.i r1 = ar.rsc
	mov.i r1 = ar.unat
	mov.i r1 = ar1
	mov.i r1 = ar22
	mov.i r1 = ar48
	mov.i r1 = ar67
	mov.i r1 = ar112
	mov.i ar.bspstore = r2
	mov.i ar.bspstore = 15
	mov.i ar.ccv = r2
	mov.i ar.ccv = 15
	mov.i ar.ec = r2
	mov.i ar.ec = 15
	mov.i ar.fpsr = r2
	mov.i ar.fpsr = 15
	mov.i ar.itc = r2
	mov.i ar.itc = 15
	mov.i ar.lc = r2
	mov.i ar.lc = 15
	mov.i ar.pfs = r2
	mov.i ar.pfs = 15
	mov.i ar.rnat = r2
	mov.i ar.rnat = 15
	mov.i ar.rsc = r2
	mov.i ar.rsc = 15
	mov.i ar.unat = r2
	mov.i ar.unat = 15
	mov.i ar1 = r2
	mov.i ar1 = 15
	mov.i ar22 = r2
	mov.i ar22 = 15
	mov.i ar48 = r2
	mov.i ar48 = 15
	mov.i ar67 = r2
	mov.i ar67 = 15
	mov.i ar112 = r2
	mov.i ar112 = 15
	mov.m r1 = ar.bsp
	mov.m r1 = ar.bspstore
	mov.m r1 = ar.ccv
	mov.m r1 = ar.ec
	mov.m r1 = ar.fpsr
	mov.m r1 = ar.itc
	mov.m r1 = ar.lc
	mov.m r1 = ar.pfs
	mov.m r1 = ar.rnat
	mov.m r1 = ar.rsc
	mov.m r1 = ar.unat
	mov.m r1 = ar1
	mov.m r1 = ar22
	mov.m r1 = ar48
	mov.m r1 = ar67
	mov.m r1 = ar112
	mov.m ar.bspstore = r2
	mov.m ar.bspstore = 15
	mov.m ar.ccv = r2
	mov.m ar.ccv = 15
	mov.m ar.ec = r2
	mov.m ar.ec = 15
	mov.m ar.fpsr = r2
	mov.m ar.fpsr = 15
	mov.m ar.itc = r2
	mov.m ar.itc = 15
	mov.m ar.lc = r2
	mov.m ar.lc = 15
	mov.m ar.pfs = r2
	mov.m ar.pfs = 15
	mov.m ar.rnat = r2
	mov.m ar.rnat = 15
	mov.m ar.rsc = r2
	mov.m ar.rsc = 15
	mov.m ar.unat = r2
	mov.m ar.unat = 15
	mov.m ar1 = r2
	mov.m ar1 = 15
	mov.m ar22 = r2
	mov.m ar22 = 15
	mov.m ar48 = r2
	mov.m ar48 = 15
	mov.m ar67 = r2
	mov.m ar67 = 15
	mov.m ar112 = r2
	mov.m ar112 = 15
	
	(p3) mov r1 = ar3
	(p30) mov r1 = ar3
	(p3) mov r41 = ar3
	(p1) mov ar3 = r2
	(p31) mov ar3 = r2
	(p1) mov ar3 = r52
	(p2) mov ar3 = 15
	(p32) mov ar3 = 15
	(p3) mov.i r1 = ar3
	(p30) mov.i r1 = ar3
	(p3) mov.i r41 = ar3
	(p1) mov.i ar3 = r2
	(p31) mov.i ar3 = r2
	(p1) mov.i ar3 = r52
	(p2) mov.i ar3 = 15
	(p32) mov.i ar3 = 15
	(p3) mov.m r1 = ar3
	(p30) mov.m r1 = ar3
	(p3) mov.m r41 = ar3
	(p1) mov.m ar3 = r2
	(p31) mov.m ar3 = r2
	(p1) mov.m ar3 = r52
	(p2) mov.m ar3 = 15
	(p32) mov.m ar3 = 15

.blabel:
	(p3) mov r1 = b1
	(p33) mov r1 = b1
	(p3) mov r51 = b1
	(p4) mov b1 = r2
	(p34) mov b1 = r2
	(p4) mov b1 = r52
	(p5) mov		b1 = r2, .blabel
	(p35) mov		b1 = r2, .blabel
	(p5) mov		b1 = r72, .blabel
	(p7) mov.imp		b1 = r2, .blabel
	(p37) mov.imp		b1 = r2, .blabel
	(p7) mov.imp		b1 = r82, .blabel
	(p5) mov.sptk		b1 = r2, .blabel
	(p35) mov.sptk		b1 = r2, .blabel
	(p5) mov.sptk		b1 = r72, .blabel
	(p7) mov.sptk.imp		b1 = r2, .blabel
	(p37) mov.sptk.imp		b1 = r2, .blabel
	(p7) mov.sptk.imp		b1 = r82, .blabel
	(p5) mov.dptk		b1 = r2, .blabel
	(p35) mov.dptk		b1 = r2, .blabel
	(p5) mov.dptk		b1 = r72, .blabel
	(p7) mov.dptk.imp		b1 = r2, .blabel
	(p37) mov.dptk.imp		b1 = r2, .blabel
	(p7) mov.dptk.imp		b1 = r82, .blabel
	(p5) mov.ret		b1 = r2, .blabel
	(p35) mov.ret		b1 = r2, .blabel
	(p5) mov.ret		b1 = r72, .blabel
	(p7) mov.ret.imp		b1 = r2, .blabel
	(p37) mov.ret.imp		b1 = r2, .blabel
	(p7) mov.ret.imp		b1 = r82, .blabel
	(p5) mov.ret.sptk		b1 = r2, .blabel
	(p35) mov.ret.sptk		b1 = r2, .blabel
	(p5) mov.ret.sptk		b1 = r72, .blabel
	(p7) mov.ret.sptk.imp		b1 = r2, .blabel
	(p37) mov.ret.sptk.imp		b1 = r2, .blabel
	(p7) mov.ret.sptk.imp		b1 = r82, .blabel
	(p5) mov.ret.dptk		b1 = r2, .blabel
	(p35) mov.ret.dptk		b1 = r2, .blabel
	(p5) mov.ret.dptk		b1 = r72, .blabel
	(p7) mov.ret.dptk.imp		b1 = r2, .blabel
	(p37) mov.ret.dptk.imp		b1 = r2, .blabel
	(p7) mov.ret.dptk.imp		b1 = r82, .blabel

	mov r1 = cr.dcr
	mov r1 = cr.itm
	mov r1 = cr.iva
	mov r1 = cr.pta
	mov r1 = cr.ipsr
	mov r1 = cr.isr
	mov r1 = cr.iip
	mov r1 = cr.ifa
	mov r1 = cr.itir
	mov r1 = cr.iipa
	mov r1 = cr.ifs
	mov r1 = cr.iim
	mov r1 = cr.iha
	mov r1 = cr.lid
	mov r1 = cr.ivr
	mov r1 = cr.tpr
	mov r1 = cr.eoi
	mov r1 = cr.irr0
	mov r1 = cr.irr1
	mov r1 = cr.irr2
	mov r1 = cr.irr3
	mov r1 = cr.itv
	mov r1 = cr.pmv
	mov r1 = cr.cmcv
	mov r1 = cr.lrr0
	mov r1 = cr.lrr1

	mov  cr.dcr = r2
	mov  cr.itm = r2
	mov  cr.iva = r2
	mov  cr.pta = r2
	mov  cr.ipsr = r2
	mov  cr.isr = r2
	mov  cr.iip = r2
	mov  cr.ifa = r2
	mov  cr.itir = r2
	mov  cr.iipa = r2
	mov  cr.ifs = r2
	mov  cr.iim = r2
	mov  cr.iha = r2
	mov  cr.lid = r2
	mov  cr.ivr = r2
	mov  cr.tpr = r2
	mov  cr.eoi = r2
	mov  cr.irr0 = r2
	mov  cr.irr1 = r2
	mov  cr.irr2 = r2
	mov  cr.irr3 = r2
	mov  cr.itv = r2
	mov  cr.pmv = r2
	mov  cr.cmcv = r2
	mov  cr.lrr0 = r2
	mov  cr.lrr1 = r2


	(p6) mov r1 = cr2
	(p46) mov r1 = cr2
	(p6) mov r41 = cr2
	(p7) mov cr2 = r2
	(p47) mov cr2 = r2
	(p7) mov cr2 = r42

	(p8) mov f11 = f3
	(p48) mov f11 = f3
	(p8) mov f110 = f3
	(p8) mov f11 = f37

	(p9) mov r1 = r3
	(p49) mov r1 = r3
	(p9) mov r91 = r3
	(p9) mov r1 = r93

	(p5) mov r1 = 3000
	(p50) mov r1 = 3000
	(p5) mov r101 = 3000

	(p5) mov r1 = rr[r3]
	(p51) mov r1 = rr[r3]
	(p5) mov r81 = rr[r3]
	(p5) mov r1 = rr[r83]
	(p5) mov r1 = dbr[r3]
	(p51) mov r1 = dbr[r3]
	(p5) mov r81 = dbr[r3]
	(p5) mov r1 = dbr[r83]
	(p5) mov r1 = ibr[r3]
	(p51) mov r1 = ibr[r3]
	(p5) mov r81 = ibr[r3]
	(p5) mov r1 = ibr[r83]
	(p5) mov r1 = pkr[r3]
	(p51) mov r1 = pkr[r3]
	(p5) mov r81 = pkr[r3]
	(p5) mov r1 = pkr[r83]
	(p5) mov r1 = pmc[r3]
	(p51) mov r1 = pmc[r3]
	(p5) mov r81 = pmc[r3]
	(p5) mov r1 = pmc[r83]
	(p5) mov r1 = pmd[r3]
	(p51) mov r1 = pmd[r3]
	(p5) mov r81 = pmd[r3]
	(p5) mov r1 = pmd[r83]
	(p5) mov r1 = cpuid[r3]
	(p51) mov r1 = cpuid[r3]
	(p5) mov r81 = cpuid[r3]
	(p5) mov r1 = cpuid[r83]

	//(p0) mov cpuid[r3] = r2
	//(p16) mov cpuid[r3] = r2
	// (p0) mov cpuid[r53] = r2
	// (p0) mov cpuid[r3] = r52
	(p0) mov dbr[r3] = r2
	(p16) mov dbr[r3] = r2
	(p0) mov dbr[r53] = r2
	(p0) mov dbr[r3] = r52
	(p0) mov ibr[r3] = r2
	(p16) mov ibr[r3] = r2
	(p0) mov ibr[r53] = r2
	(p0) mov ibr[r3] = r52
	(p0) mov pkr[r3] = r2
	(p16) mov pkr[r3] = r2
	(p0) mov pkr[r53] = r2
	(p0) mov pkr[r3] = r52
	(p0) mov pmc[r3] = r2
	(p16) mov pmc[r3] = r2
	(p0) mov pmc[r53] = r2
	(p0) mov pmc[r3] = r52
	(p0) mov pmd[r3] = r2
	(p16) mov pmd[r3] = r2
	(p0) mov pmd[r53] = r2
	(p0) mov pmd[r3] = r52
	(p0) mov rr[r3] = r2
	(p16) mov rr[r3] = r2
	(p0) mov rr[r53] = r2
	(p0) mov rr[r3] = r52
	
	(p1) mov r2 = ip
	(p16) mov r2 = ip
	(p1) mov r42 = ip

	(p2) mov r1 = pr
	(p42) mov r1 = pr
	(p2) mov r91 = pr
	(p3) mov pr = r2, 34
	(p43) mov pr = r2, 34
	(p3) mov pr = r52, 34
	(p4) mov pr.rot = 0x3450000
	(p44) mov pr.rot = 0x3450000

	(p5) mov r1 = psr
	(p45) mov r1 = psr
	(p5) mov r101 = psr
	(p6) mov psr.l = r2
	(p46) mov psr.l = r2
	(p6) mov psr.l = r92
	
	(p7) mov r1 = psr.um
	(p37) mov r1 = psr.um
	(p7) mov r51 = psr.um
	(p8) mov psr.um = r2
	(p38) mov psr.um = r2
	(p8) mov psr.um = r52

	(p9) movl r1 = 0xfedcba9876543210
	(p59) movl r1 = 0xfedcba9876543210

	(p10) mux1	r1 = r2, @rev
	(p16) mux1	r1 = r2, @rev
	(p10) mux1	r101 = r2, @rev
	(p10) mux1	r1 = r102, @rev
	(p11) mux1	r1 = r2, @mix
	(p17) mux1	r1 = r2, @mix
	(p11) mux1	r111 = r2, @mix
	(p11) mux1	r1 = r112, @mix
	(p12) mux1	r1 = r2, @shuf
	(p18) mux1	r1 = r2, @shuf
	(p12) mux1	r81 = r2, @shuf
	(p12) mux1	r1 = r82, @shuf
	(p13) mux1	r1 = r2, @alt
	(p19) mux1	r1 = r2, @alt
	(p13) mux1	r91 = r2, @alt
	(p13) mux1	r1 = r92, @alt
	(p14) mux1	r1 = r2, @brcst
	(p44) mux1	r1 = r2, @brcst
	(p14) mux1	r61 = r2, @brcst
	(p14) mux1	r1 = r62, @brcst
	(p45) mux2	r1 = r2, 0x96
	(p15) mux2	r1 = r2, 0x96
	(p15) mux2	r51 = r2, 0x96
	(p15) mux2	r1 = r52, 0x96

	//// ******** N *********

	(p6) nop 3
	(p16) nop 3
	(p7) nop.i 4
	(p17) nop.i 4
	(p8) nop.b 5
	(p18) nop.b 5
	(p9) nop.m 6
	(p19) nop.m 6
	(p10) nop.f 0x100007
	(p20) nop.f 0x100007
	(p11) nop.x 0x3edcba9876543210
	(p21) nop.x 0x3edcba9876543210

	//// ******** O *********

	(p2) or r1 = r2, r3
	(p22) or r1 = r2, r3
	(p2) or r121 = r2, r3
	(p2) or r1 = r122, r3
	(p2) or r1 = r2, r123
	(p3) or r1 = 7, r3
	(p23) or r1 = 7, r3
	(p3) or r41 = 7, r3
	(p3) or r1 = 7, r43

	//// ******** P *********

	(p5) pack2.sss	r1 = r2, r3
	(p25) pack2.sss	r1 = r2, r3
	(p5) pack2.sss	r51 = r2, r3
	(p5) pack2.sss	r1 = r52, r3
	(p5) pack2.sss	r1 = r2, r53
	(p6) pack2.uss	r1 = r2, r3
	(p26) pack2.uss	r1 = r2, r3
	(p6) pack2.uss	r61 = r2, r3
	(p6) pack2.uss	r1 = r62, r3
	(p6) pack2.uss	r1 = r2, r63
	(p7) pack4.sss	r1 = r2, r3
	(p27) pack4.sss	r1 = r2, r3
	(p7) pack4.sss	r71 = r2, r3
	(p7) pack4.sss	r1 = r72, r3
	(p7) pack4.sss	r1 = r2, r73

	(p2) padd1	r1 = r2, r3
	(p22) padd1	r1 = r2, r3
	(p2) padd1	r101 = r2, r3
	(p2) padd1	r1 = r68, r3
	(p2) padd1	r1 = r2, r35
	(p2) padd1.sss	r1 = r2, r3
	(p22) padd1.sss	r1 = r2, r3
	(p2) padd1.sss	r101 = r2, r3
	(p2) padd1.sss	r1 = r68, r3
	(p2) padd1.sss	r1 = r2, r35
	(p2) padd1.uus	r1 = r2, r3
	(p22) padd1.uus	r1 = r2, r3
	(p2) padd1.uus	r101 = r2, r3
	(p2) padd1.uus	r1 = r68, r3
	(p2) padd1.uus	r1 = r2, r35
	(p2) padd1.uuu	r1 = r2, r3
	(p22) padd1.uuu	r1 = r2, r3
	(p2) padd1.uuu	r101 = r2, r3
	(p2) padd1.uuu	r1 = r68, r3
	(p2) padd1.uuu	r1 = r2, r35
	(p2) padd2	r1 = r2, r3
	(p22) padd2	r1 = r2, r3
	(p2) padd2	r101 = r2, r3
	(p2) padd2	r1 = r68, r3
	(p2) padd2	r1 = r2, r35
	(p2) padd2.sss	r1 = r2, r3
	(p22) padd2.sss	r1 = r2, r3
	(p2) padd2.sss	r101 = r2, r3
	(p2) padd2.sss	r1 = r68, r3
	(p2) padd2.sss	r1 = r2, r35
	(p2) padd2.uus	r1 = r2, r3
	(p22) padd2.uus	r1 = r2, r3
	(p2) padd2.uus	r101 = r2, r3
	(p2) padd2.uus	r1 = r68, r3
	(p2) padd2.uus	r1 = r2, r35
	(p2) padd2.uuu	r1 = r2, r3
	(p22) padd2.uuu	r1 = r2, r3
	(p2) padd2.uuu	r101 = r2, r3
	(p2) padd2.uuu	r1 = r68, r3
	(p2) padd2.uuu	r1 = r2, r35
	(p2) padd4	r1 = r2, r3
	(p22) padd4	r1 = r2, r3
	(p2) padd4	r101 = r2, r3
	(p2) padd4	r1 = r68, r3
	(p2) padd4	r1 = r2, r35

	(p7) pavg1	r1 = r2, r3
	(p37) pavg1	r1 = r2, r3
	(p7) pavg1	r51 = r2, r3
	(p7) pavg1	r1 = r52, r3
	(p7) pavg1	r1 = r2, r53
	(p8) pavg1.raz	r1 = r2, r3
	(p38) pavg1.raz	r1 = r2, r3
	(p8) pavg1.raz	r61 = r2, r3
	(p8) pavg1.raz	r1 = r62, r3
	(p8) pavg1.raz	r1 = r2, r63
	(p7) pavg2	r1 = r2, r3
	(p37) pavg2	r1 = r2, r3
	(p7) pavg2	r51 = r2, r3
	(p7) pavg2	r1 = r52, r3
	(p7) pavg2	r1 = r2, r53
	(p8) pavg2.raz	r1 = r2, r3
	(p38) pavg2.raz	r1 = r2, r3
	(p8) pavg2.raz	r61 = r2, r3
	(p8) pavg2.raz	r1 = r62, r3
	(p8) pavg2.raz	r1 = r2, r63

	(p1) pavgsub1	r1 = r2, r3
	(p41) pavgsub1	r1 = r2, r3
	(p1) pavgsub1	r81 = r2, r3
	(p1) pavgsub1	r1 = r82, r3
	(p1) pavgsub1	r1 = r2, r83
	(p2) pavgsub2	r1 = r2, r3
	(p42) pavgsub2	r1 = r2, r3
	(p2) pavgsub2	r111 = r2, r3
	(p2) pavgsub2	r1 = r112, r3
	(p2) pavgsub2	r1 = r2, r113

	(p3) pcmp1.eq	r1 = r2, r3
	(p43) pcmp1.eq	r1 = r2, r3
	(p3) pcmp1.eq	r81 = r2, r3
	(p3) pcmp1.eq	r1 = r82, r3
	(p3) pcmp1.eq	r1 = r2, r83
	(p4) pcmp1.gt	r1 = r2, r3
	(p44) pcmp1.gt	r1 = r2, r3
	(p4) pcmp1.gt	r121 = r2, r3
	(p4) pcmp1.gt	r1 = r122, r3
	(p4) pcmp1.gt	r1 = r2, r123
	(p3) pcmp2.eq	r1 = r2, r3
	(p43) pcmp2.eq	r1 = r2, r3
	(p3) pcmp2.eq	r81 = r2, r3
	(p3) pcmp2.eq	r1 = r82, r3
	(p3) pcmp2.eq	r1 = r2, r83
	(p4) pcmp2.gt	r1 = r2, r3
	(p44) pcmp2.gt	r1 = r2, r3
	(p4) pcmp2.gt	r121 = r2, r3
	(p4) pcmp2.gt	r1 = r122, r3
	(p4) pcmp2.gt	r1 = r2, r123
	(p3) pcmp4.eq	r1 = r2, r3
	(p43) pcmp4.eq	r1 = r2, r3
	(p3) pcmp4.eq	r81 = r2, r3
	(p3) pcmp4.eq	r1 = r82, r3
	(p3) pcmp4.eq	r1 = r2, r83
	(p4) pcmp4.gt	r1 = r2, r3
	(p44) pcmp4.gt	r1 = r2, r3
	(p4) pcmp4.gt	r121 = r2, r3
	(p4) pcmp4.gt	r1 = r122, r3
	(p4) pcmp4.gt	r1 = r2, r123

	(p9) pmax1.u	r1 = r2, r3
	(p49) pmax1.u	r1 = r2, r3
	(p9) pmax1.u	r41 = r2, r3
	(p9) pmax1.u	r1 = r42, r3
	(p9) pmax1.u	r1 = r2, r43
	(p5) pmax2	r1 = r2, r3
	(p50) pmax2	r1 = r2, r3
	(p5) pmax2	r91 = r2, r3
	(p5) pmax2	r1 = r92, r3
	(p5) pmax2	r1 = r2, r93

	(p1) pmin1.u	r1 = r2, r3
	(p51) pmin1.u	r1 = r2, r3
	(p1) pmin1.u	r61 = r2, r3
	(p1) pmin1.u	r1 = r62, r3
	(p1) pmin1.u	r1 = r2, r63
	(p2) pmin2	r1 = r2, r3
	(p52) pmin2	r1 = r2, r3
	(p2) pmin2	r51 = r2, r3
	(p2) pmin2	r1 = r52, r3
	(p2) pmin2	r1 = r2, r53

	(p3) pmpy2.r	r1 = r2, r3
	(p53) pmpy2.r	r1 = r2, r3
	(p3) pmpy2.r	r81 = r2, r3
	(p3) pmpy2.r	r1 = r82, r3
	(p3) pmpy2.r	r1 = r2, r83
	(p4) pmpy2.l	r1 = r2, r3
	(p54) pmpy2.l	r1 = r2, r3
	(p4) pmpy2.l	r71 = r2, r3
	(p4) pmpy2.l	r1 = r72, r3
	(p4) pmpy2.l	r1 = r2, r73

	(p5) pmpyshr2	r1 = r2, r3, 0
	(p55) pmpyshr2	r1 = r2, r3, 0
	(p5) pmpyshr2	r101 = r2, r3, 0
	(p5) pmpyshr2	r1 = r112, r3, 0
	(p5) pmpyshr2	r1 = r2, r123, 0
	(p9) pmpyshr2.u	r1 = r2, r3, 0
	(p59) pmpyshr2.u	r1 = r2, r3, 0
	(p9) pmpyshr2.u	r111 = r2, r3, 0
	(p9) pmpyshr2.u	r1 = r112, r3, 0
	(p9) pmpyshr2.u	r1 = r2, r93, 0

	(p3) popcnt	r1 = r3
	(p63) popcnt	r1 = r3
	(p3) popcnt	r101 = r3
	(p3) popcnt	r1 = r103

	(p0) probe.r	r1 = r3, r2
	(p16) probe.r	r1 = r3, r2
	(p0) probe.r	r101 = r3, r2
	(p0) probe.r	r1 = r96, r2
	(p0) probe.r	r1 = r3, r64
	(p0) probe.w	r1 = r3, r2
	(p16) probe.w	r1 = r3, r2
	(p0) probe.w	r101 = r3, r2
	(p0) probe.w	r1 = r96, r2
	(p0) probe.w	r1 = r3, r64
	(p0) probe.r	r1 = r3, 2
	(p16) probe.r	r1 = r3, 2
	(p0) probe.r	r101 = r3, 2
	(p0) probe.r	r1 = r35, 2
	(p0) probe.w	r1 = r3, 2
	(p16) probe.w	r1 = r3, 2
	(p0) probe.w	r101 = r3, 2
	(p0) probe.w	r1 = r35, 2
	(p4) probe.r.fault	r3, 2
	(p24) probe.r.fault	r3, 2
	(p4) probe.r.fault	r33, 2
	(p5) probe.w.fault	r3, 2
	(p25) probe.w.fault	r3, 2
	(p5) probe.w.fault	r33, 2
	(p6) probe.rw.fault	r3, 2
	(p26) probe.rw.fault	r3, 2
	(p6) probe.rw.fault	r63, 2

	(p7) psad1	r1 = r2, r3
	(p37) psad1	r1 = r2, r3
	(p7) psad1	r101 = r2, r3
	(p7) psad1	r1 = r102, r3
	(p7) psad1	r1 = r2, r103

	(p8) pshl2	r1 = r2, r3
	(p28) pshl2	r1 = r2, r3
	(p8) pshl2	r101 = r2, r3
	(p8) pshl2	r1 = r102, r3
	(p8) pshl2	r1 = r2, r103
	(p9) pshl2	r1 = r2, 5
	(p59) pshl2	r1 = r2, 5
	(p9) pshl2	r101 = r2, 5
	(p9) pshl2	r1 = r102, 5
	(p10) pshl4	r1 = r2, r3
	(p50) pshl4	r1 = r2, r3
	(p10) pshl4	r101 = r2, r3
	(p10) pshl4	r1 = r102, r3
	(p10) pshl4	r1 = r2, r103
	(p11) pshl4	r1 = r2, 5
	(p41) pshl4	r1 = r2, 5
	(p11) pshl4	r37 = r2, 5
	(p11) pshl4	r1 = r67, 5

	(p12) pshladd2	r1 = r2, 2, r3
	(p22) pshladd2	r1 = r2, 2, r3
	(p12) pshladd2	r101 = r2, 2, r3
	(p12) pshladd2	r1 = r102, 2, r3
	(p12) pshladd2	r1 = r2, 2, r103

	(p3) pshr2	r1 = r3, r2
	(p23) pshr2	r1 = r3, r2
	(p3) pshr2	r101 = r3, r2
	(p3) pshr2	r1 = r103, r2
	(p3) pshr2	r1 = r3, r102
	(p14) pshr2	r1 = r3, 5
	(p24) pshr2	r1 = r3, 5
	(p14) pshr2	r100 = r3, 5
	(p14) pshr2	r1 = r35, 5
	(p3) pshr2.u	r1 = r3, r2
	(p23) pshr2.u	r1 = r3, r2
	(p3) pshr2.u	r101 = r3, r2
	(p3) pshr2.u	r1 = r103, r2
	(p3) pshr2.u	r1 = r3, r102
	(p14) pshr2.u	r1 = r3, 5
	(p24) pshr2.u	r1 = r3, 5
	(p14) pshr2.u	r100 = r3, 5
	(p14) pshr2.u	r1 = r35, 5
	(p3) pshr4	r1 = r3, r2
	(p23) pshr4	r1 = r3, r2
	(p3) pshr4	r101 = r3, r2
	(p3) pshr4	r1 = r103, r2
	(p3) pshr4	r1 = r3, r102
	(p14) pshr4	r1 = r3, 5
	(p24) pshr4	r1 = r3, 5
	(p14) pshr4	r100 = r3, 5
	(p14) pshr4	r1 = r35, 5
	(p3) pshr4.u	r1 = r3, r2
	(p23) pshr4.u	r1 = r3, r2
	(p3) pshr4.u	r101 = r3, r2
	(p3) pshr4.u	r1 = r103, r2
	(p3) pshr4.u	r1 = r3, r102
	(p14) pshr4.u	r1 = r3, 5
	(p24) pshr4.u	r1 = r3, 5
	(p14) pshr4.u	r100 = r3, 5
	(p14) pshr4.u	r1 = r35, 5

	(p1) pshradd2	r1 = r2, 2, r3
	(p21) pshradd2	r1 = r2, 2, r3
	(p1) pshradd2	r97 = r2, 2, r3
	(p1) pshradd2	r1 = r66, 2, r3
	(p1) pshradd2	r1 = r2, 2, r35

	(p2) psub1	r1 = r2, r3
	(p22) psub1	r1 = r2, r3
	(p2) psub1	r101 = r2, r3
	(p2) psub1	r1 = r68, r3
	(p2) psub1	r1 = r2, r35
	(p2) psub1.sss	r1 = r2, r3
	(p22) psub1.sss	r1 = r2, r3
	(p2) psub1.sss	r101 = r2, r3
	(p2) psub1.sss	r1 = r68, r3
	(p2) psub1.sss	r1 = r2, r35
	(p2) psub1.uus	r1 = r2, r3
	(p22) psub1.uus	r1 = r2, r3
	(p2) psub1.uus	r101 = r2, r3
	(p2) psub1.uus	r1 = r68, r3
	(p2) psub1.uus	r1 = r2, r35
	(p2) psub1.uuu	r1 = r2, r3
	(p22) psub1.uuu	r1 = r2, r3
	(p2) psub1.uuu	r101 = r2, r3
	(p2) psub1.uuu	r1 = r68, r3
	(p2) psub1.uuu	r1 = r2, r35
	(p2) psub2	r1 = r2, r3
	(p22) psub2	r1 = r2, r3
	(p2) psub2	r101 = r2, r3
	(p2) psub2	r1 = r68, r3
	(p2) psub2	r1 = r2, r35
	(p2) psub2.sss	r1 = r2, r3
	(p22) psub2.sss	r1 = r2, r3
	(p2) psub2.sss	r101 = r2, r3
	(p2) psub2.sss	r1 = r68, r3
	(p2) psub2.sss	r1 = r2, r35
	(p2) psub2.uus	r1 = r2, r3
	(p22) psub2.uus	r1 = r2, r3
	(p2) psub2.uus	r101 = r2, r3
	(p2) psub2.uus	r1 = r68, r3
	(p2) psub2.uus	r1 = r2, r35
	(p2) psub2.uuu	r1 = r2, r3
	(p22) psub2.uuu	r1 = r2, r3
	(p2) psub2.uuu	r101 = r2, r3
	(p2) psub2.uuu	r1 = r68, r3
	(p2) psub2.uuu	r1 = r2, r35
	(p2) psub4	r1 = r2, r3
	(p22) psub4	r1 = r2, r3
	(p2) psub4	r101 = r2, r3
	(p2) psub4	r1 = r68, r3
	(p2) psub4	r1 = r2, r35

	(p1) ptc.e	r3
	(p31) ptc.e	r3
	(p1) ptc.e	r33
	(p2) ptc.g	r3, r2
	(p32) ptc.g	r3, r2
	(p2) ptc.g	r103, r2
	(p2) ptc.g	r3, r34
	(p3) ptc.ga	r3, r2
	(p33) ptc.ga	r3, r2
	(p3) ptc.ga	r103, r2
	(p3) ptc.ga	r3, r32

	(p4) ptc.l	r3, r2
	(p34) ptc.l	r3, r2
	(p4) ptc.l	r103, r2
	(p4) ptc.l	r3, r102

	(p5) ptr.d	r3, r2
	(p35) ptr.d	r3, r2
	(p5) ptr.d	r103, r2
	(p5) ptr.d	r3, r102
	(p6) ptr.i	r3, r2
	(p36) ptr.i	r3, r2
	(p6) ptr.i	r103, r2
	(p6) ptr.i	r3, r102
	
	//// ******** Q *********

	//// ******** R *********

	rfi
	
        rsm 0x000000
        rsm 0x000001
        rsm 0x000002
        rsm 0x000004
        rsm 0x000008
        rsm 0x000010
        rsm 0x000020
        rsm 0x000040
        rsm 0x000080
        rsm 0x000100
        rsm 0x000200
        rsm 0x000400
        rsm 0x000800
        rsm 0x001000
        rsm 0x002000
        rsm 0x004000
        rsm 0x008000
        rsm 0x010000
        rsm 0x020000
        rsm 0x040000
        rsm 0x080000
        rsm 0x100000
        rsm 0x200000
        rsm 0x400000
        rsm 0x800000


	(p7) rsm  0xaaaaaa
	(p37) rsm  0xaaaaaa
	(p5) rsm 0x555555
	(p37) rsm 0x555555

	(p8) rum 0x800000
	(p8) rum 0x800001
	(p8) rum 0x800002
	(p8) rum 0x800004
	(p8) rum 0x800008
	(p8) rum 0x800010
	(p8) rum 0x800020
	(p38) rum 0x800000
	(p38) rum 0x800001
	(p38) rum 0x800002
	(p38) rum 0x800004
	(p38) rum 0x800008
	(p38) rum 0x800010
	(p38) rum 0x800020

	//// ******** S *********

	(p9) setf.s f10 = r2
	(p39) setf.s f10 = r2
	(p9) setf.s f100 = r2
	(p9) setf.s f10 = r34
	(p9) setf.d f10 = r2
	(p39) setf.d f10 = r2
	(p9) setf.d f100 = r2
	(p9) setf.d f10 = r34
	(p9) setf.exp f10 = r2
	(p39) setf.exp f10 = r2
	(p9) setf.exp f100 = r2
	(p9) setf.exp f10 = r34
	(p9) setf.sig f10 = r2
	(p39) setf.sig f10 = r2
	(p9) setf.sig f100 = r2
	(p9) setf.sig f10 = r34

	(p3) shl	r1 = r2, r3
	(p43) shl	r1 = r2, r3
	(p3) shl	r101 = r2, r3
	(p3) shl	r1 = r102, r3
	(p3) shl	r1 = r2, r103
	(p4) shl	r1 = r2, 6
	(p44) shl	r1 = r2, 6
	(p4) shl	r101 = r2, 6
	(p4) shl	r1 = r102, 6

	(p5) shladd	r1 = r2, 2, r3
	(p45) shladd	r1 = r2, 2, r3
	(p5) shladd	r101 = r2, 2, r3
	(p5) shladd	r1 = r102, 2, r3
	(p5) shladd	r1 = r2, 2, r103

	(p6) shladdp4	r1 = r2, 2, r3
	(p46) shladdp4	r1 = r2, 2, r3
	(p6) shladdp4	r101 = r2, 2, r3
	(p6) shladdp4	r1 = r102, 2, r3
	(p6) shladdp4	r1 = r2, 2, r103

	(p7) shr	r1 = r3, r2
	(p47) shr	r1 = r3, r2
	(p7) shr	r101 = r3, r2
	(p7) shr	r1 = r103, r2
	(p7) shr	r1 = r3, r102
	(p8) shr.u	r1 = r3, r2
	(p48) shr.u	r1 = r3, r2
	(p8) shr.u	r101 = r3, r2
	(p8) shr.u	r1 = r103, r2
	(p8) shr.u	r1 = r3, r102
	(p9) shr	r1 = r3, 6
	(p49) shr	r1 = r3, 6
	(p9) shr	r101 = r3, 6
	(p9) shr	r1 = r35, 6
	(p5) shr.u	r1 = r3, 6
	(p50) shr.u	r1 = r3, 6
	(p5) shr.u	r101 = r3, 6
	(p5) shr.u	r1 = r103, 6

	(p1) shrp	r1 = r2, r3, 6
	(p51) shrp	r1 = r2, r3, 6
	(p1) shrp	r101 = r2, r3, 6
	(p1) shrp	r1 = r102, r3, 6
	(p1) shrp	r1 = r2, r103, 6
	
	(p2) srlz.i
	(p52) srlz.i
	(p53) srlz.d
	(p3) srlz.d

        ssm 0x000000
        ssm 0x000001
        ssm 0x000002
        ssm 0x000004
        ssm 0x000008
        ssm 0x000010
        ssm 0x000020
        ssm 0x000040
        ssm 0x000080
        ssm 0x000100
        ssm 0x000200
        ssm 0x000400
        ssm 0x000800
        ssm 0x001000
        ssm 0x002000
        ssm 0x004000
        ssm 0x008000
        ssm 0x010000
        ssm 0x020000
        ssm 0x040000
        ssm 0x080000
        ssm 0x100000
        ssm 0x200000
        ssm 0x400000
        ssm 0x800000

	(p7) ssm  0xaaaaaa
	(p37) ssm  0xaaaaaa
	(p5) ssm 0x555555
	(p37) ssm 0x555555

	(p0) st1 		[r3] = r2
	(p16) st1 		[r3] = r2
	(p0) st1 		[r103] = r2
	(p0) st1 		[r3] = r102
	(p1) st1 		[r3] = r2, 9
	(p17) st1 		[r3] = r2, 9
	(p1) st1 		[r103] = r2, 9
	(p1) st1 		[r3] = r102, 9
	(p0) st1.nta 		[r3] = r2
	(p16) st1.nta 		[r3] = r2
	(p0) st1.nta 		[r103] = r2
	(p0) st1.nta 		[r3] = r102
	(p1) st1.nta 		[r3] = r2, 9
	(p17) st1.nta 		[r3] = r2, 9
	(p1) st1.nta 		[r103] = r2, 9
	(p1) st1.nta 		[r3] = r102, 9
	(p0) st1.rel 		[r3] = r2
	(p16) st1.rel 		[r3] = r2
	(p0) st1.rel 		[r103] = r2
	(p0) st1.rel 		[r3] = r102
	(p1) st1.rel 		[r3] = r2, 9
	(p17) st1.rel 		[r3] = r2, 9
	(p1) st1.rel 		[r103] = r2, 9
	(p1) st1.rel 		[r3] = r102, 9
	(p0) st1.rel.nta 		[r3] = r2
	(p16) st1.rel.nta 		[r3] = r2
	(p0) st1.rel.nta 		[r103] = r2
	(p0) st1.rel.nta 		[r3] = r102
	(p1) st1.rel.nta 		[r3] = r2, 9
	(p17) st1.rel.nta 		[r3] = r2, 9
	(p1) st1.rel.nta 		[r103] = r2, 9
	(p1) st1.rel.nta 		[r3] = r102, 9
	(p0) st2 		[r3] = r2
	(p16) st2 		[r3] = r2
	(p0) st2 		[r103] = r2
	(p0) st2 		[r3] = r102
	(p1) st2 		[r3] = r2, 9
	(p17) st2 		[r3] = r2, 9
	(p1) st2 		[r103] = r2, 9
	(p1) st2 		[r3] = r102, 9
	(p0) st2.nta 		[r3] = r2
	(p16) st2.nta 		[r3] = r2
	(p0) st2.nta 		[r103] = r2
	(p0) st2.nta 		[r3] = r102
	(p1) st2.nta 		[r3] = r2, 9
	(p17) st2.nta 		[r3] = r2, 9
	(p1) st2.nta 		[r103] = r2, 9
	(p1) st2.nta 		[r3] = r102, 9
	(p0) st2.rel 		[r3] = r2
	(p16) st2.rel 		[r3] = r2
	(p0) st2.rel 		[r103] = r2
	(p0) st2.rel 		[r3] = r102
	(p1) st2.rel 		[r3] = r2, 9
	(p17) st2.rel 		[r3] = r2, 9
	(p1) st2.rel 		[r103] = r2, 9
	(p1) st2.rel 		[r3] = r102, 9
	(p0) st2.rel.nta 		[r3] = r2
	(p16) st2.rel.nta 		[r3] = r2
	(p0) st2.rel.nta 		[r103] = r2
	(p0) st2.rel.nta 		[r3] = r102
	(p1) st2.rel.nta 		[r3] = r2, 9
	(p17) st2.rel.nta 		[r3] = r2, 9
	(p1) st2.rel.nta 		[r103] = r2, 9
	(p1) st2.rel.nta 		[r3] = r102, 9
	(p0) st4 		[r3] = r2
	(p16) st4 		[r3] = r2
	(p0) st4 		[r103] = r2
	(p0) st4 		[r3] = r102
	(p1) st4 		[r3] = r2, 9
	(p17) st4 		[r3] = r2, 9
	(p1) st4 		[r103] = r2, 9
	(p1) st4 		[r3] = r102, 9
	(p0) st4.nta 		[r3] = r2
	(p16) st4.nta 		[r3] = r2
	(p0) st4.nta 		[r103] = r2
	(p0) st4.nta 		[r3] = r102
	(p1) st4.nta 		[r3] = r2, 9
	(p17) st4.nta 		[r3] = r2, 9
	(p1) st4.nta 		[r103] = r2, 9
	(p1) st4.nta 		[r3] = r102, 9
	(p0) st4.rel 		[r3] = r2
	(p16) st4.rel 		[r3] = r2
	(p0) st4.rel 		[r103] = r2
	(p0) st4.rel 		[r3] = r102
	(p1) st4.rel 		[r3] = r2, 9
	(p17) st4.rel 		[r3] = r2, 9
	(p1) st4.rel 		[r103] = r2, 9
	(p1) st4.rel 		[r3] = r102, 9
	(p0) st4.rel.nta 		[r3] = r2
	(p16) st4.rel.nta 		[r3] = r2
	(p0) st4.rel.nta 		[r103] = r2
	(p0) st4.rel.nta 		[r3] = r102
	(p1) st4.rel.nta 		[r3] = r2, 9
	(p17) st4.rel.nta 		[r3] = r2, 9
	(p1) st4.rel.nta 		[r103] = r2, 9
	(p1) st4.rel.nta 		[r3] = r102, 9
	(p0) st8 		[r3] = r2
	(p16) st8 		[r3] = r2
	(p0) st8 		[r103] = r2
	(p0) st8 		[r3] = r102
	(p1) st8 		[r3] = r2, 9
	(p17) st8 		[r3] = r2, 9
	(p1) st8 		[r103] = r2, 9
	(p1) st8 		[r3] = r102, 9
	(p0) st8.nta 		[r3] = r2
	(p16) st8.nta 		[r3] = r2
	(p0) st8.nta 		[r103] = r2
	(p0) st8.nta 		[r3] = r102
	(p1) st8.nta 		[r3] = r2, 9
	(p17) st8.nta 		[r3] = r2, 9
	(p1) st8.nta 		[r103] = r2, 9
	(p1) st8.nta 		[r3] = r102, 9
	(p0) st8.rel 		[r3] = r2
	(p16) st8.rel 		[r3] = r2
	(p0) st8.rel 		[r103] = r2
	(p0) st8.rel 		[r3] = r102
	(p1) st8.rel 		[r3] = r2, 9
	(p17) st8.rel 		[r3] = r2, 9
	(p1) st8.rel 		[r103] = r2, 9
	(p1) st8.rel 		[r3] = r102, 9
	(p0) st8.rel.nta 		[r3] = r2
	(p16) st8.rel.nta 		[r3] = r2
	(p0) st8.rel.nta 		[r103] = r2
	(p0) st8.rel.nta 		[r3] = r102
	(p1) st8.rel.nta 		[r3] = r2, 9
	(p17) st8.rel.nta 		[r3] = r2, 9
	(p1) st8.rel.nta 		[r103] = r2, 9
	(p1) st8.rel.nta 		[r3] = r102, 9
	(p0) st8.spill 		[r3] = r2
	(p16) st8.spill 		[r3] = r2
	(p0) st8.spill 		[r103] = r2
	(p0) st8.spill 		[r3] = r102
	(p1) st8.spill 		[r3] = r2, 9
	(p17) st8.spill 		[r3] = r2, 9
	(p1) st8.spill 		[r103] = r2, 9
	(p1) st8.spill 		[r3] = r102, 9
	(p0) st8.spill.nta 		[r3] = r2
	(p16) st8.spill.nta 		[r3] = r2
	(p0) st8.spill.nta 		[r103] = r2
	(p0) st8.spill.nta 		[r3] = r102
	(p1) st8.spill.nta 		[r3] = r2, 9
	(p17) st8.spill.nta 		[r3] = r2, 9
	(p1) st8.spill.nta 		[r103] = r2, 9
	(p1) st8.spill.nta 		[r3] = r102, 9

	(p0) stfs [r3] = f2
	(p16) stfs [r3] = f2
	(p0) stfs [r39] = f2
	(p0) stfs [r3] = f34
	(p1) stfs [r3] = f2, 9
	(p17) stfs [r3] = f2, 9
	(p1) stfs [r39] = f2, 9
	(p1) stfs [r3] = f102, 9
	(p0) stfs.nta [r3] = f2
	(p16) stfs.nta [r3] = f2
	(p0) stfs.nta [r39] = f2
	(p0) stfs.nta [r3] = f34
	(p1) stfs.nta [r3] = f2, 9
	(p17) stfs.nta [r3] = f2, 9
	(p1) stfs.nta [r39] = f2, 9
	(p1) stfs.nta [r3] = f102, 9
	(p0) stfd [r3] = f2
	(p16) stfd [r3] = f2
	(p0) stfd [r39] = f2
	(p0) stfd [r3] = f34
	(p1) stfd [r3] = f2, 9
	(p17) stfd [r3] = f2, 9
	(p1) stfd [r39] = f2, 9
	(p1) stfd [r3] = f102, 9
	(p0) stfd.nta [r3] = f2
	(p16) stfd.nta [r3] = f2
	(p0) stfd.nta [r39] = f2
	(p0) stfd.nta [r3] = f34
	(p1) stfd.nta [r3] = f2, 9
	(p17) stfd.nta [r3] = f2, 9
	(p1) stfd.nta [r39] = f2, 9
	(p1) stfd.nta [r3] = f102, 9
	(p0) stfe [r3] = f2
	(p16) stfe [r3] = f2
	(p0) stfe [r39] = f2
	(p0) stfe [r3] = f34
	(p1) stfe [r3] = f2, 9
	(p17) stfe [r3] = f2, 9
	(p1) stfe [r39] = f2, 9
	(p1) stfe [r3] = f102, 9
	(p0) stfe.nta [r3] = f2
	(p16) stfe.nta [r3] = f2
	(p0) stfe.nta [r39] = f2
	(p0) stfe.nta [r3] = f34
	(p1) stfe.nta [r3] = f2, 9
	(p17) stfe.nta [r3] = f2, 9
	(p1) stfe.nta [r39] = f2, 9
	(p1) stfe.nta [r3] = f102, 9
	(p0) stf8 [r3] = f2
	(p16) stf8 [r3] = f2
	(p0) stf8 [r39] = f2
	(p0) stf8 [r3] = f34
	(p1) stf8 [r3] = f2, 9
	(p17) stf8 [r3] = f2, 9
	(p1) stf8 [r39] = f2, 9
	(p1) stf8 [r3] = f102, 9
	(p0) stf8.nta [r3] = f2
	(p16) stf8.nta [r3] = f2
	(p0) stf8.nta [r39] = f2
	(p0) stf8.nta [r3] = f34
	(p1) stf8.nta [r3] = f2, 9
	(p17) stf8.nta [r3] = f2, 9
	(p1) stf8.nta [r39] = f2, 9
	(p1) stf8.nta [r3] = f102, 9
	(p0) stf.spill [r3] = f2
	(p16) stf.spill [r3] = f2
	(p0) stf.spill [r39] = f2
	(p0) stf.spill [r3] = f34
	(p1) stf.spill [r3] = f2, 9
	(p17) stf.spill [r3] = f2, 9
	(p1) stf.spill [r39] = f2, 9
	(p1) stf.spill [r3] = f102, 9
	(p0) stf.spill.nta [r3] = f2
	(p16) stf.spill.nta [r3] = f2
	(p0) stf.spill.nta [r39] = f2
	(p0) stf.spill.nta [r3] = f34
	(p1) stf.spill.nta [r3] = f2, 9
	(p17) stf.spill.nta [r3] = f2, 9
	(p1) stf.spill.nta [r39] = f2, 9
	(p1) stf.spill.nta [r3] = f102, 9

	(p6) sub	r1 = r2, r3
	(p16) sub	r1 = r2, r3
	(p6) sub	r33 = r2, r3
	(p6) sub	r1 = r34, r3
	(p6) sub	r1 = r2, r34
	(p7) sub	r1 = r2, r3, 1
	(p17) sub	r1 = r2, r3, 1
	(p7) sub	r33 = r2, r3, 1
	(p7) sub	r1 = r34, r3, 1
	(p7) sub	r1 = r2, r35, 1
	(p8) sub	r1 = 8, r3
	(p18) sub	r1 = 8, r3
	(p8) sub	r111 = 8, r3
	(p8) sub	r1 = 8, r113

	(p8) sum 0x800000
	(p8) sum 0x800001
	(p8) sum 0x800002
	(p8) sum 0x800004
	(p8) sum 0x800008
	(p8) sum 0x800010
	(p8) sum 0x800020
	(p38) sum 0x800000
	(p38) sum 0x800001
	(p38) sum 0x800002
	(p38) sum 0x800004
	(p38) sum 0x800008
	(p38) sum 0x800010
	(p38) sum 0x800020

	(p1) sxt1	r1 = r2
	(p17) sxt1	r1 = r2
	(p1) sxt1	r100 = r2
	(p1) sxt1	r1 = r34
	(p12) sxt2	r1 = r2
	(p24) sxt2	r1 = r2
	(p12) sxt2	r100 = r2
	(p12) sxt2	r1 = r34
	(p13) sxt4	r1 = r2
	(p23) sxt4	r1 = r2
	(p13) sxt4	r100 = r2
	(p13) sxt4	r1 = r34

	(p14) sync.i
	(p39) sync.i

	//// ******** T *********

	(p5) tak	r1 = r3
	(p31) tak	r1 = r3
	(p5) tak	r42 = r3
	(p5) tak	r1 = r36

	(p6) tbit.z		p1, p2 = r3, 6
	(p16) tbit.z		p1, p2 = r3, 6
	(p6) tbit.z		p17, p2 = r3, 6
	(p6) tbit.z		p1, p37 = r3, 6
	(p6) tbit.z		p1, p2 = r37, 6
	(p7) tbit.nz		p1, p2 = r3, 6
	(p17) tbit.nz		p1, p2 = r3, 6
	(p7) tbit.nz		p17, p2 = r3, 6
	(p7) tbit.nz		p1, p37 = r3, 6
	(p7) tbit.nz		p1, p2 = r37, 6
	(p6) tbit.z.unc		p1, p2 = r3, 6
	(p16) tbit.z.unc		p1, p2 = r3, 6
	(p6) tbit.z.unc		p17, p2 = r3, 6
	(p6) tbit.z.unc		p1, p37 = r3, 6
	(p6) tbit.z.unc		p1, p2 = r37, 6
	(p7) tbit.nz.unc		p1, p2 = r3, 6
	(p17) tbit.nz.unc		p1, p2 = r3, 6
	(p7) tbit.nz.unc		p17, p2 = r3, 6
	(p7) tbit.nz.unc		p1, p37 = r3, 6
	(p7) tbit.nz.unc		p1, p2 = r37, 6
	(p6) tbit.z.or		p1, p2 = r3, 6
	(p16) tbit.z.or		p1, p2 = r3, 6
	(p6) tbit.z.or		p17, p2 = r3, 6
	(p6) tbit.z.or		p1, p37 = r3, 6
	(p6) tbit.z.or		p1, p2 = r37, 6
	(p7) tbit.nz.or		p1, p2 = r3, 6
	(p17) tbit.nz.or		p1, p2 = r3, 6
	(p7) tbit.nz.or		p17, p2 = r3, 6
	(p7) tbit.nz.or		p1, p37 = r3, 6
	(p7) tbit.nz.or		p1, p2 = r37, 6
	(p6) tbit.z.and		p1, p2 = r3, 6
	(p16) tbit.z.and		p1, p2 = r3, 6
	(p6) tbit.z.and		p17, p2 = r3, 6
	(p6) tbit.z.and		p1, p37 = r3, 6
	(p6) tbit.z.and		p1, p2 = r37, 6
	(p7) tbit.nz.and		p1, p2 = r3, 6
	(p17) tbit.nz.and		p1, p2 = r3, 6
	(p7) tbit.nz.and		p17, p2 = r3, 6
	(p7) tbit.nz.and		p1, p37 = r3, 6
	(p7) tbit.nz.and		p1, p2 = r37, 6
	(p6) tbit.z.or.andcm		p1, p2 = r3, 6
	(p16) tbit.z.or.andcm		p1, p2 = r3, 6
	(p6) tbit.z.or.andcm		p17, p2 = r3, 6
	(p6) tbit.z.or.andcm		p1, p37 = r3, 6
	(p6) tbit.z.or.andcm		p1, p2 = r37, 6
	(p7) tbit.nz.or.andcm		p1, p2 = r3, 6
	(p17) tbit.nz.or.andcm		p1, p2 = r3, 6
	(p7) tbit.nz.or.andcm		p17, p2 = r3, 6
	(p7) tbit.nz.or.andcm		p1, p37 = r3, 6
	(p7) tbit.nz.or.andcm		p1, p2 = r37, 6
	(p6) tbit.z.andcm		p1, p2 = r3, 6
	(p16) tbit.z.andcm		p1, p2 = r3, 6
	(p6) tbit.z.andcm		p17, p2 = r3, 6
	(p6) tbit.z.andcm		p1, p37 = r3, 6
	(p6) tbit.z.andcm		p1, p2 = r37, 6
	(p7) tbit.nz.andcm		p1, p2 = r3, 6
	(p17) tbit.nz.andcm		p1, p2 = r3, 6
	(p7) tbit.nz.andcm		p17, p2 = r3, 6
	(p7) tbit.nz.andcm		p1, p37 = r3, 6
	(p7) tbit.nz.andcm		p1, p2 = r37, 6
	(p6) tbit.z.orcm		p1, p2 = r3, 6
	(p16) tbit.z.orcm		p1, p2 = r3, 6
	(p6) tbit.z.orcm		p17, p2 = r3, 6
	(p6) tbit.z.orcm		p1, p37 = r3, 6
	(p6) tbit.z.orcm		p1, p2 = r37, 6
	(p7) tbit.nz.orcm		p1, p2 = r3, 6
	(p17) tbit.nz.orcm		p1, p2 = r3, 6
	(p7) tbit.nz.orcm		p17, p2 = r3, 6
	(p7) tbit.nz.orcm		p1, p37 = r3, 6
	(p7) tbit.nz.orcm		p1, p2 = r37, 6
	(p6) tbit.z.and.orcm		p1, p2 = r3, 6
	(p16) tbit.z.and.orcm		p1, p2 = r3, 6
	(p6) tbit.z.and.orcm		p17, p2 = r3, 6
	(p6) tbit.z.and.orcm		p1, p37 = r3, 6
	(p6) tbit.z.and.orcm		p1, p2 = r37, 6
	(p7) tbit.nz.and.orcm		p1, p2 = r3, 6
	(p17) tbit.nz.and.orcm		p1, p2 = r3, 6
	(p7) tbit.nz.and.orcm		p17, p2 = r3, 6
	(p7) tbit.nz.and.orcm		p1, p37 = r3, 6
	(p7) tbit.nz.and.orcm		p1, p2 = r37, 6

	(p2) thash	r1 = r3
	(p32) thash	r1 = r3
	(p2) thash	r100 = r3
	(p2) thash	r1 = r35
	
	(p6) tnat.z		p1, p2 = r3
	(p16) tnat.z		p1, p2 = r3
	(p6) tnat.z		p17, p2 = r3
	(p6) tnat.z		p1, p37 = r3
	(p6) tnat.z		p1, p2 = r37
	(p7) tnat.nz		p1, p2 = r3
	(p17) tnat.nz		p1, p2 = r3
	(p7) tnat.nz		p17, p2 = r3
	(p7) tnat.nz		p1, p37 = r3
	(p7) tnat.nz		p1, p2 = r37
	(p6) tnat.z.unc		p1, p2 = r3
	(p16) tnat.z.unc		p1, p2 = r3
	(p6) tnat.z.unc		p17, p2 = r3
	(p6) tnat.z.unc		p1, p37 = r3
	(p6) tnat.z.unc		p1, p2 = r37
	(p7) tnat.nz.unc		p1, p2 = r3
	(p17) tnat.nz.unc		p1, p2 = r3
	(p7) tnat.nz.unc		p17, p2 = r3
	(p7) tnat.nz.unc		p1, p37 = r3
	(p7) tnat.nz.unc		p1, p2 = r37
	(p6) tnat.z.or		p1, p2 = r3
	(p16) tnat.z.or		p1, p2 = r3
	(p6) tnat.z.or		p17, p2 = r3
	(p6) tnat.z.or		p1, p37 = r3
	(p6) tnat.z.or		p1, p2 = r37
	(p7) tnat.nz.or		p1, p2 = r3
	(p17) tnat.nz.or		p1, p2 = r3
	(p7) tnat.nz.or		p17, p2 = r3
	(p7) tnat.nz.or		p1, p37 = r3
	(p7) tnat.nz.or		p1, p2 = r37
	(p6) tnat.z.and		p1, p2 = r3
	(p16) tnat.z.and		p1, p2 = r3
	(p6) tnat.z.and		p17, p2 = r3
	(p6) tnat.z.and		p1, p37 = r3
	(p6) tnat.z.and		p1, p2 = r37
	(p7) tnat.nz.and		p1, p2 = r3
	(p17) tnat.nz.and		p1, p2 = r3
	(p7) tnat.nz.and		p17, p2 = r3
	(p7) tnat.nz.and		p1, p37 = r3
	(p7) tnat.nz.and		p1, p2 = r37
	(p6) tnat.z.or.andcm		p1, p2 = r3
	(p16) tnat.z.or.andcm		p1, p2 = r3
	(p6) tnat.z.or.andcm		p17, p2 = r3
	(p6) tnat.z.or.andcm		p1, p37 = r3
	(p6) tnat.z.or.andcm		p1, p2 = r37
	(p7) tnat.nz.or.andcm		p1, p2 = r3
	(p17) tnat.nz.or.andcm		p1, p2 = r3
	(p7) tnat.nz.or.andcm		p17, p2 = r3
	(p7) tnat.nz.or.andcm		p1, p37 = r3
	(p7) tnat.nz.or.andcm		p1, p2 = r37
	(p6) tnat.z.andcm		p1, p2 = r3
	(p16) tnat.z.andcm		p1, p2 = r3
	(p6) tnat.z.andcm		p17, p2 = r3
	(p6) tnat.z.andcm		p1, p37 = r3
	(p6) tnat.z.andcm		p1, p2 = r37
	(p7) tnat.nz.andcm		p1, p2 = r3
	(p17) tnat.nz.andcm		p1, p2 = r3
	(p7) tnat.nz.andcm		p17, p2 = r3
	(p7) tnat.nz.andcm		p1, p37 = r3
	(p7) tnat.nz.andcm		p1, p2 = r37
	(p6) tnat.z.orcm		p1, p2 = r3
	(p16) tnat.z.orcm		p1, p2 = r3
	(p6) tnat.z.orcm		p17, p2 = r3
	(p6) tnat.z.orcm		p1, p37 = r3
	(p6) tnat.z.orcm		p1, p2 = r37
	(p7) tnat.nz.orcm		p1, p2 = r3
	(p17) tnat.nz.orcm		p1, p2 = r3
	(p7) tnat.nz.orcm		p17, p2 = r3
	(p7) tnat.nz.orcm		p1, p37 = r3
	(p7) tnat.nz.orcm		p1, p2 = r37
	(p6) tnat.z.and.orcm		p1, p2 = r3
	(p16) tnat.z.and.orcm		p1, p2 = r3
	(p6) tnat.z.and.orcm		p17, p2 = r3
	(p6) tnat.z.and.orcm		p1, p37 = r3
	(p6) tnat.z.and.orcm		p1, p2 = r37
	(p7) tnat.nz.and.orcm		p1, p2 = r3
	(p17) tnat.nz.and.orcm		p1, p2 = r3
	(p7) tnat.nz.and.orcm		p17, p2 = r3
	(p7) tnat.nz.and.orcm		p1, p37 = r3
	(p7) tnat.nz.and.orcm		p1, p2 = r37

	(p2) tpa	r1 = r3
	(p32) tpa	r1 = r3
	(p2) tpa	r100 = r3
	(p2) tpa	r1 = r35
	
	(p3) ttag	r1 = r3
	(p33) ttag	r1 = r3
	(p3) ttag	r100 = r3
	(p3) ttag	r1 = r35

	//// ******** U *********

	(p4) unpack1.h		r1 = r2, r3
	(p34) unpack1.h		r1 = r2, r3
	(p4) unpack1.h		r100 = r2, r3
	(p4) unpack1.h		r1 = r34, r3
	(p4) unpack1.h		r1 = r2, r35
	(p5) unpack2.h		r1 = r2, r3
	(p35) unpack2.h		r1 = r2, r3
	(p5) unpack2.h		r100 = r2, r3
	(p5) unpack2.h		r1 = r35, r3
	(p5) unpack2.h		r1 = r2, r35
	(p6) unpack4.h		r1 = r2, r3
	(p36) unpack4.h		r1 = r2, r3
	(p6) unpack4.h		r100 = r2, r3
	(p6) unpack4.h		r1 = r36, r3
	(p6) unpack4.h		r1 = r2, r36
	(p4) unpack1.h		r1 = r2, r3
	(p34) unpack1.l		r1 = r2, r3
	(p4) unpack1.l		r100 = r2, r3
	(p4) unpack1.l		r1 = r34, r3
	(p4) unpack1.l		r1 = r2, r35
	(p5) unpack2.l		r1 = r2, r3
	(p35) unpack2.l		r1 = r2, r3
	(p5) unpack2.l		r100 = r2, r3
	(p5) unpack2.l		r1 = r35, r3
	(p5) unpack2.l		r1 = r2, r35
	(p6) unpack4.l		r1 = r2, r3
	(p36) unpack4.l		r1 = r2, r3
	(p6) unpack4.l		r100 = r2, r3
	(p6) unpack4.l		r1 = r36, r3
	(p6) unpack4.l		r1 = r2, r36

	//// ******** V *********
	//// ******** W *********
	//// ******** X *********

	(p4) xchg1		r1 = [r3], r2
	(p40) xchg1		r1 = [r3], r2
	(p4) xchg1		r100 = [r3], r2
	(p4) xchg1		r1 = [r35], r2
	(p4) xchg1		r1 = [r3], r34
	(p1) xchg1.nt1		r1 = [r3], r2
	(p41) xchg1.nt1		r1 = [r3], r2
	(p1) xchg1.nt1		r120 = [r3], r2
	(p1) xchg1.nt1		r1 = [r35], r2
	(p1) xchg1.nt1		r1 = [r3], r95
	(p2) xchg1.nta		r1 = [r3], r2
	(p42) xchg1.nta		r1 = [r3], r2
	(p2) xchg1.nta		r101 = [r3], r2
	(p2) xchg1.nta		r1 = [r35], r2
	(p2) xchg1.nta		r1 = [r3], r85
	(p4) xchg2		r1 = [r3], r2
	(p40) xchg2		r1 = [r3], r2
	(p4) xchg2		r100 = [r3], r2
	(p4) xchg2		r1 = [r35], r2
	(p4) xchg2		r1 = [r3], r34
	(p1) xchg2.nt1		r1 = [r3], r2
	(p41) xchg2.nt1		r1 = [r3], r2
	(p1) xchg2.nt1		r120 = [r3], r2
	(p1) xchg2.nt1		r1 = [r35], r2
	(p1) xchg2.nt1		r1 = [r3], r95
	(p2) xchg2.nta		r1 = [r3], r2
	(p42) xchg2.nta		r1 = [r3], r2
	(p2) xchg2.nta		r101 = [r3], r2
	(p2) xchg2.nta		r1 = [r35], r2
	(p2) xchg2.nta		r1 = [r3], r85
	(p4) xchg4		r1 = [r3], r2
	(p40) xchg4		r1 = [r3], r2
	(p4) xchg4		r100 = [r3], r2
	(p4) xchg4		r1 = [r35], r2
	(p4) xchg4		r1 = [r3], r34
	(p1) xchg4.nt1		r1 = [r3], r2
	(p41) xchg4.nt1		r1 = [r3], r2
	(p1) xchg4.nt1		r120 = [r3], r2
	(p1) xchg4.nt1		r1 = [r35], r2
	(p1) xchg4.nt1		r1 = [r3], r95
	(p2) xchg4.nta		r1 = [r3], r2
	(p42) xchg4.nta		r1 = [r3], r2
	(p2) xchg4.nta		r101 = [r3], r2
	(p2) xchg4.nta		r1 = [r35], r2
	(p2) xchg4.nta		r1 = [r3], r85
	(p4) xchg8		r1 = [r3], r2
	(p40) xchg8		r1 = [r3], r2
	(p4) xchg8		r100 = [r3], r2
	(p4) xchg8		r1 = [r35], r2
	(p4) xchg8		r1 = [r3], r34
	(p1) xchg8.nt1		r1 = [r3], r2
	(p41) xchg8.nt1		r1 = [r3], r2
	(p1) xchg8.nt1		r120 = [r3], r2
	(p1) xchg8.nt1		r1 = [r35], r2
	(p1) xchg8.nt1		r1 = [r3], r95
	(p2) xchg8.nta		r1 = [r3], r2
	(p42) xchg8.nta		r1 = [r3], r2
	(p2) xchg8.nta		r101 = [r3], r2
	(p2) xchg8.nta		r1 = [r35], r2
	(p2) xchg8.nta		r1 = [r3], r85

	(p6) xma.l	f10 = f3, f4, f2
	(p56) xma.l	f10 = f3, f4, f2
	(p6) xma.l	f100 = f3, f4, f2
	(p6) xma.l	f10 = f35, f4, f2
	(p6) xma.l	f10 = f3, f40, f2
	(p6) xma.l	f10 = f3, f4, f34
	(p7) xma.lu	f10 = f3, f4, f2
	(p57) xma.lu	f10 = f3, f4, f2
	(p7) xma.lu	f100 = f3, f4, f2
	(p7) xma.lu	f10 = f35, f4, f2
	(p7) xma.lu	f10 = f3, f40, f2
	(p7) xma.lu	f10 = f3, f4, f34
	(p8) xma.h	f10 = f3, f4, f2
	(p58) xma.h	f10 = f3, f4, f2
	(p8) xma.h	f100 = f3, f4, f2
	(p8) xma.h	f10 = f35, f4, f2
	(p8) xma.h	f10 = f3, f40, f2
	(p8) xma.h	f10 = f3, f4, f34
	(p9) xma.hu	f10 = f3, f4, f2
	(p59) xma.hu	f10 = f3, f4, f2
	(p9) xma.hu	f100 = f3, f4, f2
	(p9) xma.hu	f10 = f35, f4, f2
	(p9) xma.hu	f10 = f3, f40, f2
	(p9) xma.hu	f10 = f3, f4, f34

	(p6) xmpy.l	f10 = f3, f4
	(p56) xmpy.l	f10 = f3, f4
	(p6) xmpy.l	f100 = f3, f4
	(p6) xmpy.l	f10 = f35, f4
	(p6) xmpy.l	f10 = f3, f40
	(p7) xmpy.lu	f10 = f3, f4
	(p57) xmpy.lu	f10 = f3, f4
	(p7) xmpy.lu	f100 = f3, f4
	(p7) xmpy.lu	f10 = f35, f4
	(p7) xmpy.lu	f10 = f3, f40
	(p8) xmpy.h	f10 = f3, f4
	(p58) xmpy.h	f10 = f3, f4
	(p8) xmpy.h	f100 = f3, f4
	(p8) xmpy.h	f10 = f35, f4
	(p8) xmpy.h	f10 = f3, f40
	(p9) xmpy.hu	f10 = f3, f4
	(p59) xmpy.hu	f10 = f3, f4
	(p9) xmpy.hu	f100 = f3, f4
	(p9) xmpy.hu	f10 = f35, f4
	(p9) xmpy.hu	f10 = f3, f40

	(p6) xor	r1 = r2, r3
	(p60) xor	r1 = r2, r3
	(p6) xor	r100 = r2, r3
	(p6) xor	r1 = r34, r3
	(p6) xor	r1 = r2, r35
	(p1) xor	r1 = 8, r3
	(p61) xor	r1 = 8, r3
	(p1) xor	r103 = 8, r3
	(p1) xor	r1 = 8, r35
	
	//// ******** Y *********
	//// ******** Z *********

	(p3) zxt1	r1 = r3
	(p63) zxt1	r1 = r3
	(p3) zxt1	r100 = r3
	(p3) zxt1	r1 = r35
	(p0) zxt2	r1 = r3
	(p20) zxt2	r1 = r3
	(p0) zxt2	r100 = r3
	(p0) zxt2	r1 = r35
	(p1) zxt4	r1 = r3
	(p21) zxt4	r1 = r3
	(p1) zxt4	r100 = r3
	(p1) zxt4	r1 = r35

	.endp _start#
