.text
_start:
	//movl r3=0x9804c0270033f ;;
	movl r3 = 0b0000001111110000000110111000000010111100000000001110111111 ;;

	mov.m ar.fpsr = r3 ;;
.if 0
	// test singles

	movl r10 = 0x80000e0000000000 
	movl r11 = 0x80000e4000000000 
	movl r12 = 0x80000e8000000000 
	movl r13 = 0x80000ec000000000
	movl r14 = 0x80000f0000000000 
	movl r15 = 0x80000f4000000000 
	movl r16 = 0x80000f8000000000
	movl r17 = 0x80000fc000000000 
	movl r18 = 0x80000e8000000001 
	movl r19 = 0x80000f8000000001 ;;

	setf.sig f10 = r10
	setf.sig f11 = r11
	setf.sig f12 = r12
	setf.sig f13 = r13
	setf.sig f14 = r14
	setf.sig f15 = r15
	setf.sig f16 = r16
	setf.sig f17 = r17 
	setf.sig f18 = r18 
	setf.sig f19 = r19  ;;

	fnorm.s.s3 f30 = f10
	fnorm.s.s3 f31 = f11
	fnorm.s.s3 f32 = f12
	fnorm.s.s3 f33 = f13
	fnorm.s.s3 f34 = f14
	fnorm.s.s3 f35 = f15
	fnorm.s.s3 f36 = f16
	fnorm.s.s3 f37 = f17
	fnorm.s.s3 f38 = f18
	fnorm.s.s3 f39 = f19

	fnorm.s.s2 f40 = f10
	fnorm.s.s2 f41 = f11
	fnorm.s.s2 f42 = f12
	fnorm.s.s2 f43 = f13
	fnorm.s.s2 f44 = f14
	fnorm.s.s2 f45 = f15
	fnorm.s.s2 f46 = f16
	fnorm.s.s2 f47 = f17
	fnorm.s.s2 f48 = f18
	fnorm.s.s2 f49 = f19

	fnorm.s.s1 f50 = f10
	fnorm.s.s1 f51 = f11
	fnorm.s.s1 f52 = f12
	fnorm.s.s1 f53 = f13
	fnorm.s.s1 f54 = f14
	fnorm.s.s1 f55 = f15
	fnorm.s.s1 f56 = f16
	fnorm.s.s1 f57 = f17
	fnorm.s.s1 f58 = f18
	fnorm.s.s1 f59 = f19

	fnorm.s.s0 f60 = f10
	fnorm.s.s0 f61 = f11
	fnorm.s.s0 f62 = f12
	fnorm.s.s0 f63 = f13
	fnorm.s.s0 f64 = f14
	fnorm.s.s0 f65 = f15
	fnorm.s.s0 f66 = f16
	fnorm.s.s0 f67 = f17
	fnorm.s.s0 f68 = f18
	fnorm.s.s0 f69 = f19 ;;

	// test doubles

	movl r10 = 0x80000e000000f000 
	movl r11 = 0x80000e400000f200 
	movl r12 = 0x80000e800000f400 
	movl r13 = 0x80000ec00000f600
	movl r14 = 0x80000f000000f800 
	movl r15 = 0x80000f400000fa00 
	movl r16 = 0x80000f800000fc00
	movl r17 = 0x80000fc00000fe00 
	movl r18 = 0x80000e800000f401 
	movl r19 = 0x80000f800000fc01 ;;

	setf.sig f10 = r10
	setf.sig f11 = r11
	setf.sig f12 = r12
	setf.sig f13 = r13
	setf.sig f14 = r14
	setf.sig f15 = r15
	setf.sig f16 = r16
	setf.sig f17 = r17 
	setf.sig f18 = r18 
	setf.sig f19 = r19  ;;

	fnorm.d.s3 f30 = f10
	fnorm.d.s3 f31 = f11
	fnorm.d.s3 f32 = f12
	fnorm.d.s3 f33 = f13
	fnorm.d.s3 f34 = f14
	fnorm.d.s3 f35 = f15
	fnorm.d.s3 f36 = f16
	fnorm.d.s3 f37 = f17
	fnorm.d.s3 f38 = f18
	fnorm.d.s3 f39 = f19

	fnorm.d.s2 f40 = f10
	fnorm.d.s2 f41 = f11
	fnorm.d.s2 f42 = f12
	fnorm.d.s2 f43 = f13
	fnorm.d.s2 f44 = f14
	fnorm.d.s2 f45 = f15
	fnorm.d.s2 f46 = f16
	fnorm.d.s2 f47 = f17
	fnorm.d.s2 f48 = f18
	fnorm.d.s2 f49 = f19

	fnorm.d.s1 f50 = f10
	fnorm.d.s1 f51 = f11
	fnorm.d.s1 f52 = f12
	fnorm.d.s1 f53 = f13
	fnorm.d.s1 f54 = f14
	fnorm.d.s1 f55 = f15
	fnorm.d.s1 f56 = f16
	fnorm.d.s1 f57 = f17
	fnorm.d.s1 f58 = f18
	fnorm.d.s1 f59 = f19

	fnorm.d.s0 f60 = f10
	fnorm.d.s0 f61 = f11
	fnorm.d.s0 f62 = f12
	fnorm.d.s0 f63 = f13
	fnorm.d.s0 f64 = f14
	fnorm.d.s0 f65 = f15
	fnorm.d.s0 f66 = f16
	fnorm.d.s0 f67 = f17
	fnorm.d.s0 f68 = f18
	fnorm.d.s0 f69 = f19 ;;
.endif
	// test double-extended
	// 2^66 + 0-15 
	mov r2 = 0x10041

	movl r10 = 0x0000000000000000 
	movl r11 = 0x0000000000000002 
	movl r12 = 0x0000000000000004 
	movl r13 = 0x0000000000000006 
	movl r14 = 0x0000000000000008 
	movl r15 = 0x000000000000000a 
	movl r16 = 0x000000000000000c 
	movl r17 = 0x000000000000000e 
	movl r18 = 0x0000000000000005 
	movl r19 = 0x000000000000000d  ;;

	setf.exp f2 = r2 
	setf.sig f10 = r10
	setf.sig f11 = r11
	setf.sig f12 = r12
	setf.sig f13 = r13
	setf.sig f14 = r14
	setf.sig f15 = r15
	setf.sig f16 = r16
	setf.sig f17 = r17 
	setf.sig f18 = r18
	setf.sig f19 = r19 ;;

	fadd.s3 f30 = f10, f2
	fadd.s3 f31 = f11, f2
	fadd.s3 f32 = f12, f2
	fadd.s3 f33 = f13, f2
	fadd.s3 f34 = f14, f2
	fadd.s3 f35 = f15, f2
	fadd.s3 f36 = f16, f2
	fadd.s3 f37 = f17, f2
	fadd.s3 f38 = f18, f2
	fadd.s3 f39 = f19, f2

	fadd.s2 f40 = f10, f2
	fadd.s2 f41 = f11, f2
	fadd.s2 f42 = f12, f2
	fadd.s2 f43 = f13, f2
	fadd.s2 f44 = f14, f2
	fadd.s2 f45 = f15, f2
	fadd.s2 f46 = f16, f2
	fadd.s2 f47 = f17, f2
	fadd.s2 f48 = f18, f2
	fadd.s2 f49 = f19, f2

	fadd.s1 f50 = f10, f2
	fadd.s1 f51 = f11, f2
	fadd.s1 f52 = f12, f2
	fadd.s1 f53 = f13, f2
	fadd.s1 f54 = f14, f2
	fadd.s1 f55 = f15, f2
	fadd.s1 f56 = f16, f2
	fadd.s1 f57 = f17, f2
	fadd.s1 f58 = f18, f2
	fadd.s1 f59 = f19, f2

	fadd.s0 f60 = f10, f2
	fadd.s0 f61 = f11, f2
	fadd.s0 f62 = f12, f2
	fadd.s0 f63 = f13, f2
	fadd.s0 f64 = f14, f2
	fadd.s0 f65 = f15, f2
	fadd.s0 f66 = f16, f2
	fadd.s0 f67 = f17, f2
	fadd.s0 f68 = f18, f2
	fadd.s0 f69 = f19, f2

