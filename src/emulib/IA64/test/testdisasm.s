.text
	.align 16
	.global _start#
_start:
	//// ***** A ****

	(p1) add r1= r2, r3  
	(p2) add r1 = r2, r3, 1 
	(p3) add r1 = 1, r3 
	(p4) add r1 = -1, r3 
	(p5) adds r1 = 14, r3 
	(p6) adds r1 = -14, r3 
	(p7) addl r1 = 22, r3 
	(p8) addl r1 = -22, r3
	nop 1 

	(p9) addp4 r1 = r2, r3 
	(p10) addp4 r1 = 14, r3
	(p11) addp4 r1 = -14, r3
	nop 1

	alloc r1 = ar.pfs, 4, 12, 2, 8 
	nop 1

	(p12) and r1 = r2, r3 
	(p13) and r1 = 8, r3 
	(p14) and r1 = -8, r3 
	nop 1

.alabel:
	(p15) andcm r1 = r2, r3
	(p16) andcm r1 = 8, r3
	(p17) andcm r1 = -8, r3
	nop 1

	///  ***** B *****

	// B1
	(p18) br.spnt.few	.alabel
	(p19) br.spnt.few.clr 	.alabel
	(p20) br.spnt		.alabel
	(p21) br.spnt.clr 	.alabel
	(p22) br.spnt.many	.alabel
	(p23) br.spnt.many.clr 	.alabel
	(p24) br.sptk.few	.alabel
	(p25) br.sptk.few.clr 	.alabel
	(p26) br.sptk		.alabel
	(p27) br.sptk.clr 	.alabel
	(p28) br.sptk.many	.alabel
	(p29) br.sptk.many.clr 	.alabel
	(p30) br.dpnt.few	.alabel
	(p31) br.dpnt.few.clr 	.alabel
	(p32) br.dpnt		.alabel
	(p33) br.dpnt.clr 	.alabel
	(p34) br.dpnt.many	.alabel
	(p35) br.dpnt.many.clr 	.alabel
	(p36) br.dptk.few	.alabel
	(p37) br.dptk.few.clr 	.alabel
	(p38) br.dptk		.alabel
	(p39) br.dptk.clr 	.alabel
	(p40) br.dptk.many	.alabel
	(p41) br.dptk.many.clr 	.alabel
	(p42) br.cond.spnt.few	.alabel
	(p43) br.cond.spnt.few.clr 	.alabel
	(p44) br.cond.spnt		.alabel
	(p45) br.cond.spnt.clr 	.alabel
	(p46) br.cond.spnt.many	.alabel
	(p47) br.cond.spnt.many.clr 	.alabel
	(p48) br.cond.sptk.few	.alabel
	(p49) br.cond.sptk.few.clr 	.alabel
	(p50) br.cond.sptk		.alabel
	(p51) br.cond.sptk.clr 	.alabel
	(p52) br.cond.sptk.many	.alabel
	(p53) br.cond.sptk.many.clr 	.alabel
	(p54) br.cond.dpnt.few	.alabel
	(p55) br.cond.dpnt.few.clr 	.alabel
	(p56) br.cond.dpnt		.alabel
	(p57) br.cond.dpnt.clr 	.alabel
	(p58) br.cond.dpnt.many	.alabel
	(p59) br.cond.dpnt.many.clr 	.alabel
	(p60) br.cond.dptk.few	.alabel
	(p61) br.cond.dptk.few.clr 	.alabel
	(p62) br.cond.dptk		.alabel
	(p63) br.cond.dptk.clr 	.alabel
	(p0) br.cond.dptk.many	.alabel
	(p1) br.cond.dptk.many.clr 	.alabel
	(p2) br.wtop.spnt.few	.alabel
	(p3) br.wtop.spnt.few.clr 	.alabel
	(p4) br.wtop.spnt		.alabel
	(p5) br.wtop.spnt.clr 	.alabel
	(p6) br.wtop.spnt.many	.alabel
	(p7) br.wtop.spnt.many.clr 	.alabel
	(p8) br.wtop.sptk.few	.alabel
	(p9) br.wtop.sptk.few.clr 	.alabel
	(p10) br.wtop.sptk		.alabel
	(p11) br.wtop.sptk.clr 	.alabel
	(p12) br.wtop.sptk.many	.alabel
	(p13) br.wtop.sptk.many.clr 	.alabel
	(p14) br.wtop.dpnt.few	.alabel
	(p15) br.wtop.dpnt.few.clr 	.alabel
	(p16) br.wtop.dpnt		.alabel
	(p17) br.wtop.dpnt.clr 	.alabel
	(p18) br.wtop.dpnt.many	.alabel
	(p19) br.wtop.dpnt.many.clr 	.alabel
	(p20) br.wtop.dptk.few	.alabel
	(p21) br.wtop.dptk.few.clr 	.alabel
	(p22) br.wtop.dptk		.alabel
	(p23) br.wtop.dptk.clr 	.alabel
	(p24) br.wtop.dptk.many	.alabel
	(p25) br.wtop.dptk.many.clr 	.alabel
	(p26) br.wexit.spnt.few	.alabel
	(p27) br.wexit.spnt.few.clr 	.alabel
	(p28) br.wexit.spnt		.alabel
	(p29) br.wexit.spnt.clr 	.alabel
	(p30) br.wexit.spnt.many	.alabel
	(p31) br.wexit.spnt.many.clr 	.alabel
	(p32) br.wexit.sptk.few	.alabel
	(p33) br.wexit.sptk.few.clr 	.alabel
	(p34) br.wexit.sptk		.alabel
	(p35) br.wexit.sptk.clr 	.alabel
	(p36) br.wexit.sptk.many	.alabel
	(p37) br.wexit.sptk.many.clr 	.alabel
	(p38) br.wexit.dpnt.few	.alabel
	(p39) br.wexit.dpnt.few.clr 	.alabel
	(p40) br.wexit.dpnt		.alabel
	(p41) br.wexit.dpnt.clr 	.alabel
	(p42) br.wexit.dpnt.many	.alabel
	(p43) br.wexit.dpnt.many.clr 	.alabel
	(p44) br.wexit.dptk.few	.alabel
	(p45) br.wexit.dptk.few.clr 	.alabel
	(p46) br.wexit.dptk		.alabel
	(p47) br.wexit.dptk.clr 	.alabel
	(p48) br.wexit.dptk.many	.alabel
	(p49) br.wexit.dptk.many.clr 	.alabel
	// B3
	(p50) br.call.spnt.few	b1 = .alabel
	(p51) br.call.spnt.few.clr 	b1 = .alabel
	(p52) br.call.spnt		b1 = .alabel
	(p53) br.call.spnt.clr 	b1 = .alabel
	(p54) br.call.spnt.many	b1 = .alabel
	(p55) br.call.spnt.many.clr 	b1 = .alabel
	(p56) br.call.sptk.few	b1 = .alabel
	(p57) br.call.sptk.few.clr 	b1 = .alabel
	(p58) br.call.sptk		b1 = .alabel
	(p59) br.call.sptk.clr 	b1 = .alabel
	(p60) br.call.sptk.many	b1 = .alabel
	(p61) br.call.sptk.many.clr 	b1 = .alabel
	(p62) br.call.dpnt.few	b1 = .alabel
	(p63) br.call.dpnt.few.clr 	b1 = .alabel
	(p0) br.call.dpnt		b1 = .alabel
	(p1) br.call.dpnt.clr 	b1 = .alabel
	(p2) br.call.dpnt.many	b1 = .alabel
	(p3) br.call.dpnt.many.clr 	b1 = .alabel
	(p4) br.call.dptk.few	b1 = .alabel
	(p5) br.call.dptk.few.clr 	b1 = .alabel
	(p6) br.call.dptk		b1 = .alabel
	(p7) br.call.dptk.clr 	b1 = .alabel
	(p8) br.call.dptk.many	b1 = .alabel
	(p9) br.call.dptk.many.clr 	b1 = .alabel
	// B2
	 br.cloop.spnt.few	.alabel
	 br.cloop.spnt.few.clr 	.alabel
	 br.cloop.spnt		.alabel
	 br.cloop.spnt.clr 	.alabel
	 br.cloop.spnt.many	.alabel
	 br.cloop.spnt.many.clr 	.alabel
	 br.cloop.sptk.few	.alabel
	 br.cloop.sptk.few.clr 	.alabel
	 br.cloop.sptk		.alabel
	 br.cloop.sptk.clr 	.alabel
	 br.cloop.sptk.many	.alabel
	 br.cloop.sptk.many.clr 	.alabel
	 br.cloop.dpnt.few	.alabel
	 br.cloop.dpnt.few.clr 	.alabel
	 br.cloop.dpnt		.alabel
	 br.cloop.dpnt.clr 	.alabel
	 br.cloop.dpnt.many	.alabel
	 br.cloop.dpnt.many.clr 	.alabel
	 br.cloop.dptk.few	.alabel
	 br.cloop.dptk.few.clr 	.alabel
	 br.cloop.dptk		.alabel
	 br.cloop.dptk.clr 	.alabel
	 br.cloop.dptk.many	.alabel
	 br.cloop.dptk.many.clr 	.alabel
	 br.ctop.spnt.few	.alabel
	 br.ctop.spnt.few.clr 	.alabel
	 br.ctop.spnt		.alabel
	 br.ctop.spnt.clr 	.alabel
	 br.ctop.spnt.many	.alabel
	 br.ctop.spnt.many.clr 	.alabel
	 br.ctop.sptk.few	.alabel
	 br.ctop.sptk.few.clr 	.alabel
	 br.ctop.sptk		.alabel
	 br.ctop.sptk.clr 	.alabel
	 br.ctop.sptk.many	.alabel
	 br.ctop.sptk.many.clr 	.alabel
	 br.ctop.dpnt.few	.alabel
	 br.ctop.dpnt.few.clr 	.alabel
	 br.ctop.dpnt		.alabel
	 br.ctop.dpnt.clr 	.alabel
	 br.ctop.dpnt.many	.alabel
	 br.ctop.dpnt.many.clr 	.alabel
	 br.ctop.dptk.few	.alabel
	 br.ctop.dptk.few.clr 	.alabel
	 br.ctop.dptk		.alabel
	 br.ctop.dptk.clr 	.alabel
	 br.ctop.dptk.many	.alabel
	 br.ctop.dptk.many.clr 	.alabel
	 br.cexit.spnt.few	.alabel
	 br.cexit.spnt.few.clr 	.alabel
	 br.cexit.spnt		.alabel
	 br.cexit.spnt.clr 	.alabel
	 br.cexit.spnt.many	.alabel
	 br.cexit.spnt.many.clr 	.alabel
	 br.cexit.sptk.few	.alabel
	 br.cexit.sptk.few.clr 	.alabel
	 br.cexit.sptk		.alabel
	 br.cexit.sptk.clr 	.alabel
	 br.cexit.sptk.many	.alabel
	 br.cexit.sptk.many.clr 	.alabel
	 br.cexit.dpnt.few	.alabel
	 br.cexit.dpnt.few.clr 	.alabel
	 br.cexit.dpnt		.alabel
	 br.cexit.dpnt.clr 	.alabel
	 br.cexit.dpnt.many	.alabel
	 br.cexit.dpnt.many.clr 	.alabel
	 br.cexit.dptk.few	.alabel
	 br.cexit.dptk.few.clr 	.alabel
	 br.cexit.dptk		.alabel
	 br.cexit.dptk.clr 	.alabel
	 br.cexit.dptk.many	.alabel
	 br.cexit.dptk.many.clr 	.alabel
	// pseudo-op
	 br.few		.alabel
	 br.few.clr 	.alabel
	 br		.alabel
	 br.clr 	.alabel
	 br.many	.alabel
	 br.many.clr 	.alabel
	// B4
	(p10) br.cond.spnt.few	b2
	(p11) br.cond.spnt.few.clr 	b2
	(p12) br.cond.spnt		b2
	(p13) br.cond.spnt.clr 	b2
	(p14) br.cond.spnt.many	b2
	(p15) br.cond.spnt.many.clr 	b2
	(p16) br.cond.sptk.few	b2
	(p17) br.cond.sptk.few.clr 	b2
	(p18) br.cond.sptk		b2
	(p19) br.cond.sptk.clr 	b2
	(p20) br.cond.sptk.many	b2
	(p21) br.cond.sptk.many.clr 	b2
	(p22) br.cond.dpnt.few	b2
	(p23) br.cond.dpnt.few.clr 	b2
	(p24) br.cond.dpnt		b2
	(p25) br.cond.dpnt.clr 	b2
	(p26) br.cond.dpnt.many	b2
	(p27) br.cond.dpnt.many.clr 	b2
	(p28) br.cond.dptk.few	b2
	(p29) br.cond.dptk.few.clr 	b2
	(p30) br.cond.dptk		b2
	(p31) br.cond.dptk.clr 	b2
	(p32) br.cond.dptk.many	b2
	(p33) br.cond.dptk.many.clr 	b2
	(p34) br.ia.spnt.few	b2
	(p35) br.ia.spnt.few.clr 	b2
	(p36) br.ia.spnt		b2
	(p37) br.ia.spnt.clr 	b2
	(p38) br.ia.spnt.many	b2
	(p39) br.ia.spnt.many.clr 	b2
	(p40) br.ia.sptk.few	b2
	(p41) br.ia.sptk.few.clr 	b2
	(p42) br.ia.sptk		b2
	(p43) br.ia.sptk.clr 	b2
	(p44) br.ia.sptk.many	b2
	(p45) br.ia.sptk.many.clr 	b2
	(p46) br.ia.dpnt.few	b2
	(p47) br.ia.dpnt.few.clr 	b2
	(p48) br.ia.dpnt		b2
	(p49) br.ia.dpnt.clr 	b2
	(p50) br.ia.dpnt.many	b2
	(p51) br.ia.dpnt.many.clr 	b2
	(p52) br.ia.dptk.few	b2
	(p53) br.ia.dptk.few.clr 	b2
	(p54) br.ia.dptk		b2
	(p55) br.ia.dptk.clr 	b2
	(p56) br.ia.dptk.many	b2
	(p57) br.ia.dptk.many.clr 	b2
	(p58) br.ret.spnt.few	b2
	(p59) br.ret.spnt.few.clr 	b2
	(p60) br.ret.spnt		b2
	(p61) br.ret.spnt.clr 	b2
	(p62) br.ret.spnt.many	b2
	(p63) br.ret.spnt.many.clr 	b2
	(p0) br.ret.sptk.few	b2
	(p1) br.ret.sptk.few.clr 	b2
	(p2) br.ret.sptk		b2
	(p3) br.ret.sptk.clr 	b2
	(p4) br.ret.sptk.many	b2
	(p5) br.ret.sptk.many.clr 	b2
	(p6) br.ret.dpnt.few	b2
	(p7) br.ret.dpnt.few.clr 	b2
	(p8) br.ret.dpnt		b2
	(p9) br.ret.dpnt.clr 	b2
	(p10) br.ret.dpnt.many	b2
	(p11) br.ret.dpnt.many.clr 	b2
	(p12) br.ret.dptk.few	b2
	(p13) br.ret.dptk.few.clr 	b2
	(p14) br.ret.dptk		b2
	(p15) br.ret.dptk.clr 	b2
	(p16) br.ret.dptk.many	b2
	(p17) br.ret.dptk.many.clr 	b2
	// B5
	(p18) br.call.spnt.few	b1 = b2
	(p19) br.call.spnt.few.clr 	b1 = b2
	(p20) br.call.spnt		b1 = b2
	(p21) br.call.spnt.clr 	b1 = b2
	(p22) br.call.spnt.many	b1 = b2
	(p23) br.call.spnt.many.clr 	b1 = b2
	(p24) br.call.sptk.few	b1 = b2
	(p25) br.call.sptk.few.clr 	b1 = b2
	(p26) br.call.sptk		b1 = b2
	(p27) br.call.sptk.clr 	b1 = b2
	(p28) br.call.sptk.many	b1 = b2
	(p29) br.call.sptk.many.clr 	b1 = b2
	(p30) br.call.dpnt.few	b1 = b2
	(p31) br.call.dpnt.few.clr 	b1 = b2
	(p32) br.call.dpnt		b1 = b2
	(p33) br.call.dpnt.clr 	b1 = b2
	(p34) br.call.dpnt.many	b1 = b2
	(p35) br.call.dpnt.many.clr 	b1 = b2
	(p36) br.call.dptk.few	b1 = b2
	(p37) br.call.dptk.few.clr 	b1 = b2
	(p38) br.call.dptk		b1 = b2
	(p39) br.call.dptk.clr 	b1 = b2
	(p40) br.call.dptk.many	b1 = b2
	(p41) br.call.dptk.many.clr 	b1 = b2
	// pseudo-op
	 br.few		b2
	 br.few.clr 	b2
	 br		b2
	 br.clr 	b2
	 br.many	b2
	 br.many.clr 	b2
	 nop 1 

	(p42) break 21
	(p43) break.i 21
	(p44) break.b 21
	(p45) break.m 21
	(p46) break.f 21
	(p47) break.x 62 
	 nop 1 

	// X3	
	(p48) brl.spnt.few	.alabel
	(p49) brl.spnt.few.clr 	.alabel
	(p50) brl.spnt		.alabel
	(p51) brl.spnt.clr 	.alabel
	(p52) brl.spnt.many	.alabel
	(p53) brl.spnt.many.clr 	.alabel
	(p54) brl.sptk.few	.alabel
	(p55) brl.sptk.few.clr 	.alabel
	(p56) brl.sptk		.alabel
	(p57) brl.sptk.clr 	.alabel
	(p58) brl.sptk.many	.alabel
	(p59) brl.sptk.many.clr 	.alabel
	(p60) brl.dpnt.few	.alabel
	(p61) brl.dpnt.few.clr 	.alabel
	(p62) brl.dpnt		.alabel
	(p63) brl.dpnt.clr 	.alabel
	(p0) brl.dpnt.many	.alabel
	(p1) brl.dpnt.many.clr 	.alabel
	(p2) brl.dptk.few	.alabel
	(p3) brl.dptk.few.clr 	.alabel
	(p4) brl.dptk		.alabel
	(p5) brl.dptk.clr 	.alabel
	(p6) brl.dptk.many	.alabel
	(p7) brl.dptk.many.clr 	.alabel
	(p8) brl.cond.spnt.few	.alabel
	(p9) brl.cond.spnt.few.clr 	.alabel
	(p10) brl.cond.spnt		.alabel
	(p11) brl.cond.spnt.clr 	.alabel
	(p12) brl.cond.spnt.many	.alabel
	(p13) brl.cond.spnt.many.clr 	.alabel
	(p14) brl.cond.sptk.few	.alabel
	(p15) brl.cond.sptk.few.clr 	.alabel
	(p16) brl.cond.sptk		.alabel
	(p17) brl.cond.sptk.clr 	.alabel
	(p18) brl.cond.sptk.many	.alabel
	(p19) brl.cond.sptk.many.clr 	.alabel
	(p20) brl.cond.dpnt.few	.alabel
	(p21) brl.cond.dpnt.few.clr 	.alabel
	(p22) brl.cond.dpnt		.alabel
	(p23) brl.cond.dpnt.clr 	.alabel
	(p24) brl.cond.dpnt.many	.alabel
	(p25) brl.cond.dpnt.many.clr 	.alabel
	(p26) brl.cond.dptk.few	.alabel
	(p27) brl.cond.dptk.few.clr 	.alabel
	(p28) brl.cond.dptk		.alabel
	(p29) brl.cond.dptk.clr 	.alabel
	(p30) brl.cond.dptk.many	.alabel
	(p31) brl.cond.dptk.many.clr 	.alabel
	// X4
	(p32) brl.call.spnt.few		b1 = .alabel
	(p33) brl.call.spnt.few.clr 		b1 = .alabel
	(p34) brl.call.spnt			b1 = .alabel
	(p35) brl.call.spnt.clr 		b1 = .alabel
	(p36) brl.call.spnt.many		b1 = .alabel
	(p37) brl.call.spnt.many.clr 		b1 = .alabel
	(p38) brl.call.sptk.few		b1 = .alabel
	(p39) brl.call.sptk.few.clr 		b1 = .alabel
	(p40) brl.call.sptk			b1 = .alabel
	(p41) brl.call.sptk.clr 		b1 = .alabel
	(p42) brl.call.sptk.many		b1 = .alabel
	(p43) brl.call.sptk.many.clr 		b1 = .alabel
	(p44) brl.call.dpnt.few		b1 = .alabel
	(p45) brl.call.dpnt.few.clr 		b1 = .alabel
	(p46) brl.call.dpnt			b1 = .alabel
	(p47) brl.call.dpnt.clr 		b1 = .alabel
	(p48) brl.call.dpnt.many		b1 = .alabel
	(p49) brl.call.dpnt.many.clr 		b1 = .alabel
	(p50) brl.call.dptk.few		b1 = .alabel
	(p51) brl.call.dptk.few.clr 		b1 = .alabel
	(p52) brl.call.dptk			b1 = .alabel
	(p53) brl.call.dptk.clr 		b1 = .alabel
	(p54) brl.call.dptk.many		b1 = .alabel
	(p55) brl.call.dptk.many.clr 		b1 = .alabel
	// pseudo-op
	 brl.few		.alabel
	 brl.few.clr 	.alabel
	 brl		.alabel
	 brl.clr 	.alabel
	 brl.many	.alabel
	 brl.many.clr 	.alabel
	 nop 1 

	// B6
	brp.sptk		.alabel, 0x30
	brp.sptk.imp		.alabel, 0x30
	brp.loop		.alabel, 0x30
	brp.loop.imp		.alabel, 0x30
	brp.exit		.alabel, 0x30
	brp.exit.imp		.alabel, 0x30
	brp.dptk		.alabel, 0x30
	brp.dptk.imp		.alabel, 0x30
	// B7
	brp.sptk		b2, 0x30
	brp.sptk.imp		b2, 0x30
	brp.dptk		b2, 0x30
	brp.dptk.imp		b2, 0x30
	brp.ret.sptk		b2, 0x30
	brp.ret.sptk.imp	b2, 0x30
	brp.ret.dptk		b2, 0x30
	brp.ret.dptk.imp	b2, 0x30
	nop 1

	bsw.0 ;;
	bsw.1 ;;
	nop 1

	//// **** C **** 

	(p56) chk.s 	r2, .alabel 
	(p57) chk.s.i 	r2, .alabel 
	(p58) chk.s.m	r2, .alabel 
	(p59) chk.s 	f2, .alabel 
	(p60) chk.a.nc 	r2, .alabel 
	(p61) chk.a.clr 	r2, .alabel 
	(p62) chk.a.nc 	f2, .alabel 
	(p63) chk.a.clr 	f2, .alabel 
	nop 1

	clrrrb 		;;
	clrrrb.pr	;;
	nop 1

	// A6 & A8
	(p0) cmp.eq	p1, p2 = r1, r2
	(p1) cmp.eq	p1, p2 = 8, r2
	(p2) cmp.eq	p1, p2 = -8, r2
	(p3) cmp.ne	p1, p2 = r1, r2
	(p4) cmp.ne	p1, p2 = 8, r2
	(p5) cmp.ne	p1, p2 = -8, r2
	(p6) cmp.lt	p1, p2 = r1, r2
	(p7) cmp.lt	p1, p2 = 8, r2
	(p8) cmp.lt	p1, p2 = -8, r2
	(p9) cmp.le	p1, p2 = r1, r2
	(p10) cmp.le	p1, p2 = 8, r2
	(p11) cmp.le	p1, p2 = -8, r2
	(p12) cmp.gt	p1, p2 = r1, r2
	(p13) cmp.gt	p1, p2 = 8, r2
	(p14) cmp.gt	p1, p2 = -8, r2
	(p15) cmp.ge	p1, p2 = r1, r2
	(p16) cmp.ge	p1, p2 = 8, r2
	(p17) cmp.ge	p1, p2 = -8, r2
	(p18) cmp.ltu	p1, p2 = r1, r2
	(p19) cmp.ltu	p1, p2 = 8, r2
	(p20) cmp.ltu	p1, p2 = -8, r2
	(p21) cmp.leu	p1, p2 = r1, r2
	(p22) cmp.leu	p1, p2 = 8, r2
	(p23) cmp.leu	p1, p2 = -8, r2
	(p24) cmp.gtu	p1, p2 = r1, r2
	(p25) cmp.gtu	p1, p2 = 8, r2
	(p26) cmp.gtu	p1, p2 = -8, r2
	(p27) cmp.geu	p1, p2 = r1, r2
	(p28) cmp.geu	p1, p2 = 8, r2
	(p29) cmp.geu	p1, p2 = -8, r2
	(p30) cmp.eq.unc	p1, p2 = r1, r2
	(p31) cmp.eq.unc	p1, p2 = 8, r2
	(p32) cmp.eq.unc	p1, p2 = -8, r2
	(p33) cmp.ne.unc	p1, p2 = r1, r2
	(p34) cmp.ne.unc	p1, p2 = 8, r2
	(p35) cmp.ne.unc	p1, p2 = -8, r2
	(p36) cmp.lt.unc	p1, p2 = r1, r2
	(p37) cmp.lt.unc	p1, p2 = 8, r2
	(p38) cmp.lt.unc	p1, p2 = -8, r2
	(p39) cmp.le.unc	p1, p2 = r1, r2
	(p40) cmp.le.unc	p1, p2 = 8, r2
	(p41) cmp.le.unc	p1, p2 = -8, r2
	(p42) cmp.gt.unc	p1, p2 = r1, r2
	(p43) cmp.gt.unc	p1, p2 = 8, r2
	(p44) cmp.gt.unc	p1, p2 = -8, r2
	(p45) cmp.ge.unc	p1, p2 = r1, r2
	(p46) cmp.ge.unc	p1, p2 = 8, r2
	(p47) cmp.ge.unc	p1, p2 = -8, r2
	(p48) cmp.ltu.unc	p1, p2 = r1, r2
	(p49) cmp.ltu.unc	p1, p2 = 8, r2
	(p50) cmp.ltu.unc	p1, p2 = -8, r2
	(p51) cmp.leu.unc	p1, p2 = r1, r2
	(p52) cmp.leu.unc	p1, p2 = 8, r2
	(p53) cmp.leu.unc	p1, p2 = -8, r2
	(p54) cmp.gtu.unc	p1, p2 = r1, r2
	(p55) cmp.gtu.unc	p1, p2 = 8, r2
	(p56) cmp.gtu.unc	p1, p2 = -8, r2
	(p57) cmp.geu.unc	p1, p2 = r1, r2
	(p58) cmp.geu.unc	p1, p2 = 8, r2
	(p59) cmp.geu.unc	p1, p2 = -8, r2
	// parallel compares in A6 & A8
	(p60) cmp.eq.or	p1, p2 = r1, r2
	(p61) cmp.eq.or	p1, p2 = 8, r2
	(p62) cmp.eq.or	p1, p2 = -8, r2
	(p63) cmp.eq.and	p1, p2 = r1, r2
	(p0) cmp.eq.and	p1, p2 = 8, r2
	(p1) cmp.eq.and	p1, p2 = -8, r2
	(p2) cmp.eq.or.andcm	p1, p2 = r1, r2
	(p3) cmp.eq.or.andcm	p1, p2 = 8, r2
	(p4) cmp.eq.or.andcm	p1, p2 = -8, r2
	(p5) cmp.eq.orcm	p1, p2 = r1, r2
	(p6) cmp.eq.orcm	p1, p2 = 8, r2
	(p7) cmp.eq.orcm	p1, p2 = -8, r2
	(p8) cmp.eq.andcm	p1, p2 = r1, r2
	(p9) cmp.eq.andcm	p1, p2 = 8, r2
	(p10) cmp.eq.andcm	p1, p2 = -8, r2
	(p11) cmp.eq.and.orcm	p1, p2 = r1, r2
	(p12) cmp.eq.and.orcm	p1, p2 = 8, r2
	(p13) cmp.eq.and.orcm	p1, p2 = -8, r2
	(p14) cmp.ne.or	p1, p2 = r1, r2
	(p15) cmp.ne.or	p1, p2 = 8, r2
	(p16) cmp.ne.or	p1, p2 = -8, r2
	(p17) cmp.ne.and	p1, p2 = r1, r2
	(p18) cmp.ne.and	p1, p2 = 8, r2
	(p19) cmp.ne.and	p1, p2 = -8, r2
	(p20) cmp.ne.or.andcm	p1, p2 = r1, r2
	(p21) cmp.ne.or.andcm	p1, p2 = 8, r2
	(p22) cmp.ne.or.andcm	p1, p2 = -8, r2
	(p23) cmp.ne.orcm	p1, p2 = r1, r2
	(p24) cmp.ne.orcm	p1, p2 = 8, r2
	(p25) cmp.ne.orcm	p1, p2 = -8, r2
	(p26) cmp.ne.andcm	p1, p2 = r1, r2
	(p27) cmp.ne.andcm	p1, p2 = 8, r2
	(p28) cmp.ne.andcm	p1, p2 = -8, r2
	(p29) cmp.ne.and.orcm	p1, p2 = r1, r2
	(p30) cmp.ne.and.orcm	p1, p2 = 8, r2
	(p31) cmp.ne.and.orcm	p1, p2 = -8, r2
	// A7 & pseudo-op
	(p32) cmp.eq.or		p1, p2 = r0, r3
	(p33) cmp.eq.or		p1, p2 = r3, r0
	(p34) cmp.eq.and	p1, p2 = r0, r3
	(p35) cmp.eq.and	p1, p2 = r3, r0
	(p36) cmp.eq.or.andcm	p1, p2 = r0, r3
	(p37) cmp.eq.or.andcm	p1, p2 = r3, r0
	(p38) cmp.eq.orcm	p1, p2 = r0, r3
	(p39) cmp.eq.orcm	p1, p2 = r3, r0
	(p40) cmp.eq.andcm	p1, p2 = r0, r3
	(p41) cmp.eq.andcm	p1, p2 = r3, r0
	(p42) cmp.eq.and.orcm	p1, p2 = r0, r3
	(p43) cmp.eq.and.orcm	p1, p2 = r3, r0
	(p44) cmp.ne.or		p1, p2 = r0, r3
	(p45) cmp.ne.or		p1, p2 = r3, r0
	(p46) cmp.ne.and	p1, p2 = r0, r3
	(p47) cmp.ne.and	p1, p2 = r3, r0
	(p48) cmp.ne.or.andcm	p1, p2 = r0, r3
	(p49) cmp.ne.or.andcm	p1, p2 = r3, r0
	(p50) cmp.ne.orcm	p1, p2 = r0, r3
	(p51) cmp.ne.orcm	p1, p2 = r3, r0
	(p52) cmp.ne.andcm	p1, p2 = r0, r3
	(p53) cmp.ne.andcm	p1, p2 = r3, r0
	(p54) cmp.ne.and.orcm	p1, p2 = r0, r3
	(p55) cmp.ne.and.orcm	p1, p2 = r3, r0
	(p56) cmp.lt.or		p1, p2 = r0, r3
	(p57) cmp.lt.or		p1, p2 = r3, r0
	(p58) cmp.lt.and	p1, p2 = r0, r3
	(p59) cmp.lt.and	p1, p2 = r3, r0
	(p60) cmp.lt.or.andcm	p1, p2 = r0, r3
	(p61) cmp.lt.or.andcm	p1, p2 = r3, r0
	(p62) cmp.lt.orcm	p1, p2 = r0, r3
	(p63) cmp.lt.orcm	p1, p2 = r3, r0
	(p0) cmp.lt.andcm	p1, p2 = r0, r3
	(p1) cmp.lt.andcm	p1, p2 = r3, r0
	(p2) cmp.lt.and.orcm	p1, p2 = r0, r3
	(p3) cmp.lt.and.orcm	p1, p2 = r3, r0
	(p4) cmp.le.or		p1, p2 = r0, r3
	(p5) cmp.le.or		p1, p2 = r3, r0
	(p6) cmp.le.and		p1, p2 = r0, r3
	(p7) cmp.le.and		p1, p2 = r3, r0
	(p8) cmp.le.or.andcm	p1, p2 = r0, r3
	(p9) cmp.le.or.andcm	p1, p2 = r3, r0
	(p10) cmp.le.orcm	p1, p2 = r0, r3
	(p11) cmp.le.orcm	p1, p2 = r3, r0
	(p12) cmp.le.andcm	p1, p2 = r0, r3
	(p13) cmp.le.andcm	p1, p2 = r3, r0
	(p14) cmp.le.and.orcm	p1, p2 = r0, r3
	(p15) cmp.le.and.orcm	p1, p2 = r3, r0
	(p16) cmp.gt.or		p1, p2 = r0, r3
	(p17) cmp.gt.or		p1, p2 = r3, r0
	(p18) cmp.gt.and		p1, p2 = r0, r3
	(p19) cmp.gt.and		p1, p2 = r3, r0
	(p20) cmp.gt.or.andcm	p1, p2 = r0, r3
	(p21) cmp.gt.or.andcm	p1, p2 = r3, r0
	(p22) cmp.gt.orcm	p1, p2 = r0, r3
	(p23) cmp.gt.orcm	p1, p2 = r3, r0
	(p24) cmp.gt.andcm	p1, p2 = r0, r3
	(p25) cmp.gt.andcm	p1, p2 = r3, r0
	(p26) cmp.gt.and.orcm	p1, p2 = r0, r3
	(p27) cmp.gt.and.orcm	p1, p2 = r3, r0
	(p28) cmp.ge.or		p1, p2 = r0, r3
	(p29) cmp.ge.or		p1, p2 = r3, r0
	(p30) cmp.ge.and		p1, p2 = r0, r3
	(p31) cmp.ge.and		p1, p2 = r3, r0
	(p32) cmp.ge.or.andcm	p1, p2 = r0, r3
	(p33) cmp.ge.or.andcm	p1, p2 = r3, r0
	(p34) cmp.ge.orcm	p1, p2 = r0, r3
	(p35) cmp.ge.orcm	p1, p2 = r3, r0
	(p36) cmp.ge.andcm	p1, p2 = r0, r3
	(p37) cmp.ge.andcm	p1, p2 = r3, r0
	(p38) cmp.ge.and.orcm	p1, p2 = r0, r3
	(p39) cmp.ge.and.orcm	p1, p2 = r3, r0
	nop 1

	// A6 & A8
	(p40) cmp4.eq	p1, p2 = r1, r2
	(p41) cmp4.eq	p1, p2 = 8, r2
	(p42) cmp4.eq	p1, p2 = -8, r2
	(p43) cmp4.ne	p1, p2 = r1, r2
	(p44) cmp4.ne	p1, p2 = 8, r2
	(p45) cmp4.ne	p1, p2 = -8, r2
	(p46) cmp4.lt	p1, p2 = r1, r2
	(p47) cmp4.lt	p1, p2 = 8, r2
	(p48) cmp4.lt	p1, p2 = -8, r2
	(p49) cmp4.le	p1, p2 = r1, r2
	(p50) cmp4.le	p1, p2 = 8, r2
	(p51) cmp4.le	p1, p2 = -8, r2
	(p52) cmp4.gt	p1, p2 = r1, r2
	(p53) cmp4.gt	p1, p2 = 8, r2
	(p54) cmp4.gt	p1, p2 = -8, r2
	(p55) cmp4.ge	p1, p2 = r1, r2
	(p56) cmp4.ge	p1, p2 = 8, r2
	(p57) cmp4.ge	p1, p2 = -8, r2
	(p58) cmp4.ltu	p1, p2 = r1, r2
	(p59) cmp4.ltu	p1, p2 = 8, r2
	(p60) cmp4.ltu	p1, p2 = -8, r2
	(p61) cmp4.leu	p1, p2 = r1, r2
	(p62) cmp4.leu	p1, p2 = 8, r2
	(p63) cmp4.leu	p1, p2 = -8, r2
	(p0) cmp4.gtu	p1, p2 = r1, r2
	(p1) cmp4.gtu	p1, p2 = 8, r2
	(p2) cmp4.gtu	p1, p2 = -8, r2
	(p3) cmp4.geu	p1, p2 = r1, r2
	(p4) cmp4.geu	p1, p2 = 8, r2
	(p5) cmp4.geu	p1, p2 = -8, r2
	(p6) cmp4.eq.unc	p1, p2 = r1, r2
	(p7) cmp4.eq.unc	p1, p2 = 8, r2
	(p8) cmp4.eq.unc	p1, p2 = -8, r2
	(p9) cmp4.ne.unc	p1, p2 = r1, r2
	(p10) cmp4.ne.unc	p1, p2 = 8, r2
	(p11) cmp4.ne.unc	p1, p2 = -8, r2
	(p12) cmp4.lt.unc	p1, p2 = r1, r2
	(p13) cmp4.lt.unc	p1, p2 = 8, r2
	(p14) cmp4.lt.unc	p1, p2 = -8, r2
	(p15) cmp4.le.unc	p1, p2 = r1, r2
	(p16) cmp4.le.unc	p1, p2 = 8, r2
	(p17) cmp4.le.unc	p1, p2 = -8, r2
	(p18) cmp4.gt.unc	p1, p2 = r1, r2
	(p19) cmp4.gt.unc	p1, p2 = 8, r2
	(p20) cmp4.gt.unc	p1, p2 = -8, r2
	(p21) cmp4.ge.unc	p1, p2 = r1, r2
	(p22) cmp4.ge.unc	p1, p2 = 8, r2
	(p23) cmp4.ge.unc	p1, p2 = -8, r2
	(p24) cmp4.ltu.unc	p1, p2 = r1, r2
	(p25) cmp4.ltu.unc	p1, p2 = 8, r2
	(p26) cmp4.ltu.unc	p1, p2 = -8, r2
	(p27) cmp4.leu.unc	p1, p2 = r1, r2
	(p28) cmp4.leu.unc	p1, p2 = 8, r2
	(p29) cmp4.leu.unc	p1, p2 = -8, r2
	(p30) cmp4.gtu.unc	p1, p2 = r1, r2
	(p31) cmp4.gtu.unc	p1, p2 = 8, r2
	(p32) cmp4.gtu.unc	p1, p2 = -8, r2
	(p33) cmp4.geu.unc	p1, p2 = r1, r2
	(p34) cmp4.geu.unc	p1, p2 = 8, r2
	(p35) cmp4.geu.unc	p1, p2 = -8, r2
	// parallel compares in A6 & A8
	(p36) cmp4.eq.or	p1, p2 = r1, r2
	(p37) cmp4.eq.or	p1, p2 = 8, r2
	(p38) cmp4.eq.or	p1, p2 = -8, r2
	(p39) cmp4.eq.and	p1, p2 = r1, r2
	(p40) cmp4.eq.and	p1, p2 = 8, r2
	(p41) cmp4.eq.and	p1, p2 = -8, r2
	(p42) cmp4.eq.or.andcm	p1, p2 = r1, r2
	(p43) cmp4.eq.or.andcm	p1, p2 = 8, r2
	(p44) cmp4.eq.or.andcm	p1, p2 = -8, r2
	(p45) cmp4.eq.orcm	p1, p2 = r1, r2
	(p46) cmp4.eq.orcm	p1, p2 = 8, r2
	(p47) cmp4.eq.orcm	p1, p2 = -8, r2
	(p48) cmp4.eq.andcm	p1, p2 = r1, r2
	(p49) cmp4.eq.andcm	p1, p2 = 8, r2
	(p50) cmp4.eq.andcm	p1, p2 = -8, r2
	(p51) cmp4.eq.and.orcm	p1, p2 = r1, r2
	(p52) cmp4.eq.and.orcm	p1, p2 = 8, r2
	(p53) cmp4.eq.and.orcm	p1, p2 = -8, r2
	(p54) cmp4.ne.or	p1, p2 = r1, r2
	(p55) cmp4.ne.or	p1, p2 = 8, r2
	(p56) cmp4.ne.or	p1, p2 = -8, r2
	(p57) cmp4.ne.and	p1, p2 = r1, r2
	(p58) cmp4.ne.and	p1, p2 = 8, r2
	(p59) cmp4.ne.and	p1, p2 = -8, r2
	(p60) cmp4.ne.or.andcm	p1, p2 = r1, r2
	(p61) cmp4.ne.or.andcm	p1, p2 = 8, r2
	(p62) cmp4.ne.or.andcm	p1, p2 = -8, r2
	(p63) cmp4.ne.orcm	p1, p2 = r1, r2
	(p0) cmp4.ne.orcm	p1, p2 = 8, r2
	(p1) cmp4.ne.orcm	p1, p2 = -8, r2
	(p2) cmp4.ne.andcm	p1, p2 = r1, r2
	(p3) cmp4.ne.andcm	p1, p2 = 8, r2
	(p4) cmp4.ne.andcm	p1, p2 = -8, r2
	(p5) cmp4.ne.and.orcm	p1, p2 = r1, r2
	(p6) cmp4.ne.and.orcm	p1, p2 = 8, r2
	(p7) cmp4.ne.and.orcm	p1, p2 = -8, r2
	// A7 & pseudo-op
	(p8) cmp4.eq.or		p1, p2 = r0, r3
	(p9) cmp4.eq.or		p1, p2 = r3, r0
	(p10) cmp4.eq.and	p1, p2 = r0, r3
	(p11) cmp4.eq.and	p1, p2 = r3, r0
	(p12) cmp4.eq.or.andcm	p1, p2 = r0, r3
	(p13) cmp4.eq.or.andcm	p1, p2 = r3, r0
	(p14) cmp4.eq.orcm	p1, p2 = r0, r3
	(p15) cmp4.eq.orcm	p1, p2 = r3, r0
	(p16) cmp4.eq.andcm	p1, p2 = r0, r3
	(p17) cmp4.eq.andcm	p1, p2 = r3, r0
	(p18) cmp4.eq.and.orcm	p1, p2 = r0, r3
	(p19) cmp4.eq.and.orcm	p1, p2 = r3, r0
	(p20) cmp4.ne.or	p1, p2 = r0, r3
	(p21) cmp4.ne.or	p1, p2 = r3, r0
	(p22) cmp4.ne.and	p1, p2 = r0, r3
	(p23) cmp4.ne.and	p1, p2 = r3, r0
	(p24) cmp4.ne.or.andcm	p1, p2 = r0, r3
	(p25) cmp4.ne.or.andcm	p1, p2 = r3, r0
	(p26) cmp4.ne.orcm	p1, p2 = r0, r3
	(p27) cmp4.ne.orcm	p1, p2 = r3, r0
	(p28) cmp4.ne.andcm	p1, p2 = r0, r3
	(p29) cmp4.ne.andcm	p1, p2 = r3, r0
	(p30) cmp4.ne.and.orcm	p1, p2 = r0, r3
	(p31) cmp4.ne.and.orcm	p1, p2 = r3, r0
	(p32) cmp4.lt.or	p1, p2 = r0, r3
	(p33) cmp4.lt.or	p1, p2 = r3, r0
	(p34) cmp4.lt.and	p1, p2 = r0, r3
	(p35) cmp4.lt.and	p1, p2 = r3, r0
	(p36) cmp4.lt.or.andcm	p1, p2 = r0, r3
	(p37) cmp4.lt.or.andcm	p1, p2 = r3, r0
	(p38) cmp4.lt.orcm	p1, p2 = r0, r3
	(p39) cmp4.lt.orcm	p1, p2 = r3, r0
	(p40) cmp4.lt.andcm	p1, p2 = r0, r3
	(p41) cmp4.lt.andcm	p1, p2 = r3, r0
	(p42) cmp4.lt.and.orcm	p1, p2 = r0, r3
	(p43) cmp4.lt.and.orcm	p1, p2 = r3, r0
	(p44) cmp4.le.or	p1, p2 = r0, r3
	(p45) cmp4.le.or	p1, p2 = r3, r0
	(p46) cmp4.le.and	p1, p2 = r0, r3
	(p47) cmp4.le.and	p1, p2 = r3, r0
	(p48) cmp4.le.or.andcm	p1, p2 = r0, r3
	(p49) cmp4.le.or.andcm	p1, p2 = r3, r0
	(p50) cmp4.le.orcm	p1, p2 = r0, r3
	(p51) cmp4.le.orcm	p1, p2 = r3, r0
	(p52) cmp4.le.andcm	p1, p2 = r0, r3
	(p53) cmp4.le.andcm	p1, p2 = r3, r0
	(p54) cmp4.le.and.orcm	p1, p2 = r0, r3
	(p55) cmp4.le.and.orcm	p1, p2 = r3, r0
	(p56) cmp4.gt.or	p1, p2 = r0, r3
	(p57) cmp4.gt.or	p1, p2 = r3, r0
	(p58) cmp4.gt.and	p1, p2 = r0, r3
	(p59) cmp4.gt.and	p1, p2 = r3, r0
	(p60) cmp4.gt.or.andcm	p1, p2 = r0, r3
	(p61) cmp4.gt.or.andcm	p1, p2 = r3, r0
	(p62) cmp4.gt.orcm	p1, p2 = r0, r3
	(p63) cmp4.gt.orcm	p1, p2 = r3, r0
	(p0) cmp4.gt.andcm	p1, p2 = r0, r3
	(p1) cmp4.gt.andcm	p1, p2 = r3, r0
	(p2) cmp4.gt.and.orcm	p1, p2 = r0, r3
	(p3) cmp4.gt.and.orcm	p1, p2 = r3, r0
	(p4) cmp4.ge.or		p1, p2 = r0, r3
	(p5) cmp4.ge.or		p1, p2 = r3, r0
	(p6) cmp4.ge.and	p1, p2 = r0, r3
	(p7) cmp4.ge.and	p1, p2 = r3, r0
	(p8) cmp4.ge.or.andcm	p1, p2 = r0, r3
	(p9) cmp4.ge.or.andcm	p1, p2 = r3, r0
	(p10) cmp4.ge.orcm	p1, p2 = r0, r3
	(p11) cmp4.ge.orcm	p1, p2 = r3, r0
	(p12) cmp4.ge.andcm	p1, p2 = r0, r3
	(p13) cmp4.ge.andcm	p1, p2 = r3, r0
	(p14) cmp4.ge.and.orcm	p1, p2 = r0, r3
	(p15) cmp4.ge.and.orcm	p1, p2 = r3, r0
	nop 1

	(p16) cmpxchg1.acq	r1 = [r3], r2, ar.ccv 
	(p17) cmpxchg1.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p18) cmpxchg1.acq.nta	r1 = [r3], r2, ar.ccv
	(p19) cmpxchg1.rel	r1 = [r3], r2, ar.ccv 
	(p20) cmpxchg1.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p21) cmpxchg1.rel.nta	r1 = [r3], r2, ar.ccv
	(p22) cmpxchg2.acq	r1 = [r3], r2, ar.ccv 
	(p23) cmpxchg2.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p24) cmpxchg2.acq.nta	r1 = [r3], r2, ar.ccv
	(p25) cmpxchg2.rel	r1 = [r3], r2, ar.ccv 
	(p26) cmpxchg2.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p27) cmpxchg2.rel.nta	r1 = [r3], r2, ar.ccv
	(p28) cmpxchg4.acq	r1 = [r3], r2, ar.ccv 
	(p29) cmpxchg4.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p30) cmpxchg4.acq.nta	r1 = [r3], r2, ar.ccv
	(p31) cmpxchg4.rel	r1 = [r3], r2, ar.ccv 
	(p32) cmpxchg4.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p33) cmpxchg4.rel.nta	r1 = [r3], r2, ar.ccv
	(p34) cmpxchg8.acq	r1 = [r3], r2, ar.ccv 
	(p35) cmpxchg8.acq.nt1	r1 = [r3], r2, ar.ccv 
	(p36) cmpxchg8.acq.nta	r1 = [r3], r2, ar.ccv
	(p37) cmpxchg8.rel	r1 = [r3], r2, ar.ccv 
	(p38) cmpxchg8.rel.nt1	r1 = [r3], r2, ar.ccv 
	(p39) cmpxchg8.rel.nta	r1 = [r3], r2, ar.ccv
	nop 1

	cover ;;
	nop 1

	(p40) czx1.l r1 = r3
	(p41) czx1.r r1 = r3
	(p42) czx2.l r1 = r3
	(p43) czx2.r r1 = r3
	nop 1

	////    ******** D ******


	(p44) dep	r1 = r2, r3, 6, 4
	(p43) dep	r1 = 0, r3, 6, 4
	(p44) dep	r1 = -1, r3, 6, 4
	(p45) dep.z	r1 = r2, 6, 6
	(p46) dep.z	r1 = 8, 6, 6
	(p47) dep.z	r1 = -8, 6, 6
	nop 1

	//// ********* E ********

	epc
	nop 1

	(p48) extr	r1 = r3, 6, 16 
	(p49) extr.u	r1 = r3, 6, 16 
	nop 1

	//// ******** F *********

	(p50) fabs f10=f3
	nop 1

	(p51) fadd   f10 = f2, f3 
	(p52) fadd.s0   f10 = f2, f3 
	(p53) fadd.s1   f10 = f2, f3
	(p54) fadd.s2   f10 = f2, f3 
	(p55) fadd.s3   f10 = f2, f3 
	(p56) fadd.s f10 = f2, f3 
	(p57) fadd.s.s0 f10 = f2, f3 
	(p58) fadd.s.s1 f10 = f2, f3 
	(p59) fadd.s.s2 f10 = f2, f3 
	(p60) fadd.s.s3 f10 = f2, f3 
	(p61) fadd.d f10 = f2, f3 
	(p62) fadd.d.s0 f10 = f2, f3 
	(p63) fadd.d.s1 f10 = f2, f3 
	(p0) fadd.d.s2 f10 = f2, f3 
	(p1) fadd.d.s3 f10 = f2, f3 
	nop 1

	(p2) famax f10=f3,f4 
	(p3) famax.s0 f10=f3,f4 
	(p4) famax.s1 f10=f3,f4 
	(p5) famax.s2 f10=f3,f4 
	(p6) famax.s3 f10=f3,f4 
	nop 1

	(p7) famin f10=f3,f4 
	(p8) famin.s0 f10=f3,f4 
	(p9) famin.s1 f10=f3,f4 
	(p10) famin.s2 f10=f3,f4 
	(p11) famin.s3 f10=f3,f4 
	nop 1

	(p12) fand f10 = f2, f3
	nop 1

	(p13) fandcm f10 = f2, f3
	nop 1

	(p14) fc 	r3
	nop 1

	(p15) fchkf	.alabel
	(p16) fchkf.s0	.alabel
	(p17) fchkf.s1	.alabel
	(p18) fchkf.s2	.alabel
	(p19) fchkf.s3	.alabel
	nop 1

	(p20) fclass.m		p1, p2 = f2, @nat
	(p21) fclass.m.unc	p1, p2 = f2, @nat
	(p22) fclass.nm		p1, p2 = f2, @nat
	(p23) fclass.nm.unc	p1, p2 = f2, @nat
	(p24) fclass.m		p1, p2 = f2, @qnan
	(p25) fclass.m.unc	p1, p2 = f2, @qnan
	(p26) fclass.nm		p1, p2 = f2, @qnan
	(p27) fclass.nm.unc	p1, p2 = f2, @qnan
	(p28) fclass.m		p1, p2 = f2, @snan
	(p29) fclass.m.unc	p1, p2 = f2, @snan
	(p30) fclass.nm		p1, p2 = f2, @snan
	(p31) fclass.nm.unc	p1, p2 = f2, @snan
	(p32) fclass.m		p1, p2 = f2, @pos
	(p33) fclass.m.unc	p1, p2 = f2, @pos
	(p34) fclass.nm		p1, p2 = f2, @pos
	(p35) fclass.nm.unc	p1, p2 = f2, @pos
	(p36) fclass.m		p1, p2 = f2, @neg
	(p37) fclass.m.unc	p1, p2 = f2, @neg
	(p38) fclass.nm		p1, p2 = f2, @neg
	(p39) fclass.nm.unc	p1, p2 = f2, @neg
	(p40) fclass.m		p1, p2 = f2, @zero
	(p41) fclass.m.unc	p1, p2 = f2, @zero
	(p42) fclass.nm		p1, p2 = f2, @zero
	(p43) fclass.nm.unc	p1, p2 = f2, @zero
	(p44) fclass.m		p1, p2 = f2, @unorm
	(p45) fclass.m.unc	p1, p2 = f2, @unorm
	(p46) fclass.nm		p1, p2 = f2, @unorm
	(p47) fclass.nm.unc	p1, p2 = f2, @unorm
	(p48) fclass.m		p1, p2 = f2, @norm
	(p49) fclass.m.unc	p1, p2 = f2, @norm
	(p50) fclass.nm		p1, p2 = f2, @norm
	(p51) fclass.nm.unc	p1, p2 = f2, @norm
	(p52) fclass.m		p1, p2 = f2, @inf
	(p53) fclass.m.unc	p1, p2 = f2, @inf
	(p54) fclass.nm		p1, p2 = f2, @inf
	(p55) fclass.nm.unc	p1, p2 = f2, @inf
	nop 1

	(p56) fclrf
	(p57) fclrf.s0
	(p58) fclrf.s1
	(p59) fclrf.s2
	(p60) fclrf.s3
	nop 1

	(p61) fcmp.gt 		p2,p3 = f2, f3
	(p62) fcmp.eq 		p2,p3 = f2, f3
	(p63) fcmp.lt 		p2,p3 = f2, f3
	(p0) fcmp.le 		p2,p3 = f2, f3
	(p1) fcmp.gt 		p2,p3 = f2, f3
	(p2) fcmp.ge 		p2,p3 = f2, f3
	(p3) fcmp.unord 	p2,p3 = f2, f3
	(p4) fcmp.neq		p2,p3 = f2, f3
	(p5) fcmp.nlt		p2,p3 = f2, f3
	(p6) fcmp.nle		p2,p3 = f2, f3
	(p7) fcmp.ngt		p2,p3 = f2, f3
	(p8) fcmp.nge		p2,p3 = f2, f3
	(p9) fcmp.ord		p2,p3 = f2, f3
	(p10) fcmp.gt.unc 		p2,p3 = f2, f3
	(p11) fcmp.eq.unc 		p2,p3 = f2, f3
	(p12) fcmp.lt.unc 		p2,p3 = f2, f3
	(p13) fcmp.le.unc 		p2,p3 = f2, f3
	(p14) fcmp.gt.unc 		p2,p3 = f2, f3
	(p15) fcmp.ge.unc 		p2,p3 = f2, f3
	(p16) fcmp.unord.unc 		p2,p3 = f2, f3
	(p17) fcmp.neq.unc		p2,p3 = f2, f3
	(p18) fcmp.nlt.unc		p2,p3 = f2, f3
	(p19) fcmp.nle.unc		p2,p3 = f2, f3
	(p20) fcmp.ngt.unc		p2,p3 = f2, f3
	(p21) fcmp.nge.unc		p2,p3 = f2, f3
	(p22) fcmp.ord.unc		p2,p3 = f2, f3
	(p23) fcmp.gt.s0 		p2,p3 = f2, f3
	(p24) fcmp.eq.s0 		p2,p3 = f2, f3
	(p25) fcmp.lt.s0 		p2,p3 = f2, f3
	(p26) fcmp.le.s0 		p2,p3 = f2, f3
	(p27) fcmp.gt.s0 		p2,p3 = f2, f3
	(p28) fcmp.ge.s0 		p2,p3 = f2, f3
	(p29) fcmp.unord.s0 		p2,p3 = f2, f3
	(p30) fcmp.neq.s0		p2,p3 = f2, f3
	(p31) fcmp.nlt.s0		p2,p3 = f2, f3
	(p32) fcmp.nle.s0		p2,p3 = f2, f3
	(p33) fcmp.ngt.s0		p2,p3 = f2, f3
	(p34) fcmp.nge.s0		p2,p3 = f2, f3
	(p34) fcmp.ord.s0		p2,p3 = f2, f3
	(p35) fcmp.gt.unc.s0 		p2,p3 = f2, f3
	(p36) fcmp.eq.unc.s0 		p2,p3 = f2, f3
	(p37) fcmp.lt.unc.s0 		p2,p3 = f2, f3
	(p38) fcmp.le.unc.s0 		p2,p3 = f2, f3
	(p39) fcmp.gt.unc.s0 		p2,p3 = f2, f3
	(p40) fcmp.ge.unc.s0 		p2,p3 = f2, f3
	(p41) fcmp.unord.unc.s0 	p2,p3 = f2, f3
	(p42) fcmp.neq.unc.s0		p2,p3 = f2, f3
	(p43) fcmp.nlt.unc.s0		p2,p3 = f2, f3
	(p44) fcmp.nle.unc.s0		p2,p3 = f2, f3
	(p45) fcmp.ngt.unc.s0		p2,p3 = f2, f3
	(p46) fcmp.nge.unc.s0		p2,p3 = f2, f3
	(p47) fcmp.ord.unc.s0		p2,p3 = f2, f3
	(p48) fcmp.gt.s1 		p2,p3 = f2, f3
	(p49) fcmp.eq.s1 		p2,p3 = f2, f3
	(p50) fcmp.lt.s1 		p2,p3 = f2, f3
	(p51) fcmp.le.s1 		p2,p3 = f2, f3
	(p52) fcmp.gt.s1 		p2,p3 = f2, f3
	(p53) fcmp.ge.s1 		p2,p3 = f2, f3
	(p54) fcmp.unord.s1 		p2,p3 = f2, f3
	(p55) fcmp.neq.s1		p2,p3 = f2, f3
	(p56) fcmp.nlt.s1		p2,p3 = f2, f3
	(p57) fcmp.nle.s1		p2,p3 = f2, f3
	(p58) fcmp.ngt.s1		p2,p3 = f2, f3
	(p59) fcmp.nge.s1		p2,p3 = f2, f3
	(p60) fcmp.ord.s1		p2,p3 = f2, f3
	(p61) fcmp.gt.unc.s1 		p2,p3 = f2, f3
	(p62) fcmp.eq.unc.s1 		p2,p3 = f2, f3
	(p63) fcmp.lt.unc.s1 		p2,p3 = f2, f3
	(p0) fcmp.le.unc.s1 		p2,p3 = f2, f3
	(p1) fcmp.gt.unc.s1 		p2,p3 = f2, f3
	(p2) fcmp.ge.unc.s1 		p2,p3 = f2, f3
	(p3) fcmp.unord.unc.s1 		p2,p3 = f2, f3
	(p4) fcmp.neq.unc.s1		p2,p3 = f2, f3
	(p5) fcmp.nlt.unc.s1		p2,p3 = f2, f3
	(p6) fcmp.nle.unc.s1		p2,p3 = f2, f3
	(p7) fcmp.ngt.unc.s1		p2,p3 = f2, f3
	(p8) fcmp.nge.unc.s1		p2,p3 = f2, f3
	(p9) fcmp.ord.unc.s1		p2,p3 = f2, f3
	(p10) fcmp.gt.s2 		p2,p3 = f2, f3
	(p11) fcmp.eq.s2 		p2,p3 = f2, f3
	(p12) fcmp.lt.s2 		p2,p3 = f2, f3
	(p13) fcmp.le.s2 		p2,p3 = f2, f3
	(p14) fcmp.gt.s2 		p2,p3 = f2, f3
	(p15) fcmp.ge.s2 		p2,p3 = f2, f3
	(p16) fcmp.unord.s2 		p2,p3 = f2, f3
	(p17) fcmp.neq.s2		p2,p3 = f2, f3
	(p18) fcmp.nlt.s2		p2,p3 = f2, f3
	(p19) fcmp.nle.s2		p2,p3 = f2, f3
	(p20) fcmp.ngt.s2		p2,p3 = f2, f3
	(p21) fcmp.nge.s2		p2,p3 = f2, f3
	(p22) fcmp.ord.s2		p2,p3 = f2, f3
	(p23) fcmp.gt.unc.s2 		p2,p3 = f2, f3
	(p24) fcmp.eq.unc.s2 		p2,p3 = f2, f3
	(p25) fcmp.lt.unc.s2 		p2,p3 = f2, f3
	(p26) fcmp.le.unc.s2 		p2,p3 = f2, f3
	(p27) fcmp.gt.unc.s2 		p2,p3 = f2, f3
	(p28) fcmp.ge.unc.s2 		p2,p3 = f2, f3
	(p29) fcmp.unord.unc.s2 	p2,p3 = f2, f3
	(p30) fcmp.neq.unc.s2		p2,p3 = f2, f3
	(p31) fcmp.nlt.unc.s2		p2,p3 = f2, f3
	(p32) fcmp.nle.unc.s2		p2,p3 = f2, f3
	(p33) fcmp.ngt.unc.s2		p2,p3 = f2, f3
	(p34) fcmp.nge.unc.s2		p2,p3 = f2, f3
	(p35) fcmp.ord.unc.s2		p2,p3 = f2, f3
	(p36) fcmp.gt.s3 		p2,p3 = f2, f3
	(p37) fcmp.eq.s3 		p2,p3 = f2, f3
	(p38) fcmp.lt.s3 		p2,p3 = f2, f3
	(p39) fcmp.le.s3 		p2,p3 = f2, f3
	(p40) fcmp.gt.s3 		p2,p3 = f2, f3
	(p41) fcmp.ge.s3 		p2,p3 = f2, f3
	(p42) fcmp.unord.s3 		p2,p3 = f2, f3
	(p43) fcmp.neq.s3		p2,p3 = f2, f3
	(p44) fcmp.nlt.s3		p2,p3 = f2, f3
	(p45) fcmp.nle.s3		p2,p3 = f2, f3
	(p46) fcmp.ngt.s3		p2,p3 = f2, f3
	(p47) fcmp.nge.s3		p2,p3 = f2, f3
	(p48) fcmp.ord.s3		p2,p3 = f2, f3
	(p49) fcmp.gt.unc.s3 		p2,p3 = f2, f3
	(p50) fcmp.eq.unc.s3 		p2,p3 = f2, f3
	(p51) fcmp.lt.unc.s3 		p2,p3 = f2, f3
	(p52) fcmp.le.unc.s3 		p2,p3 = f2, f3
	(p53) fcmp.gt.unc.s3 		p2,p3 = f2, f3
	(p54) fcmp.ge.unc.s3 		p2,p3 = f2, f3
	(p55) fcmp.unord.unc.s3 	p2,p3 = f2, f3
	(p56) fcmp.neq.unc.s3		p2,p3 = f2, f3
	(p57) fcmp.nlt.unc.s3		p2,p3 = f2, f3
	(p58) fcmp.nle.unc.s3		p2,p3 = f2, f3
	(p59) fcmp.ngt.unc.s3		p2,p3 = f2, f3
	(p60) fcmp.nge.unc.s3		p2,p3 = f2, f3
	(p61) fcmp.ord.unc.s3		p2,p3 = f2, f3
	nop 1

	(p62) fcvt.fx		f11 = f3
	(p63) fcvt.fx.trunc	f11 = f3
	(p0) fcvt.fx.s0		f11 = f3
	(p1) fcvt.fx.trunc.s0	f11 = f3
	(p2) fcvt.fx.s1		f11 = f3
	(p3) fcvt.fx.trunc.s1	f11 = f3
	(p4) fcvt.fx.s2		f11 = f3
	(p5) fcvt.fx.trunc.s2	f11 = f3
	(p6) fcvt.fx.s3		f11 = f3
	(p7) fcvt.fx.trunc.s3	f11 = f3
	(p8) fcvt.fxu		f11 = f3
	(p9) fcvt.fxu.trunc	f11 = f3
	(p10) fcvt.fxu.s0	f11 = f3
	(p11) fcvt.fxu.trunc.s0	f11 = f3
	(p12) fcvt.fxu.s1	f11 = f3
	(p13) fcvt.fxu.trunc.s1	f11 = f3
	(p14) fcvt.fxu.s2	f11 = f3
	(p15) fcvt.fxu.trunc.s2	f11 = f3
	(p16) fcvt.fxu.s3	f11 = f3
	(p17) fcvt.fxu.trunc.s3	f11 = f3
	nop 1

	(p18) fcvt.xf		f11 = f3
	nop 1

	(p19) fcvt.xuf		f11 = f3
	(p20) fcvt.xuf.s	f11 = f3
	(p21) fcvt.xuf.d	f11 = f3
	(p22) fcvt.xuf.s0	f11 = f3
	(p23) fcvt.xuf.s.s0	f11 = f3
	(p24) fcvt.xuf.d.s0	f11 = f3
	(p25) fcvt.xuf.s1	f11 = f3
	(p26) fcvt.xuf.s.s1	f11 = f3
	(p27) fcvt.xuf.d.s1	f11 = f3
	(p28) fcvt.xuf.s2	f11 = f3
	(p29) fcvt.xuf.s.s2	f11 = f3
	(p30) fcvt.xuf.d.s2	f11 = f3
	(p31) fcvt.xuf.s3	f11 = f3
	(p32) fcvt.xuf.s.s3	f11 = f3
	(p33) fcvt.xuf.d.s3	f11 = f3
	nop 1

	(p34) fetchadd4.acq	r1 = [r3], +8 
	(p35) fetchadd4.acq.nt1	r1 = [r3], +8 
	(p36) fetchadd4.acq.nta	r1 = [r3], +8 
	(p37) fetchadd4.rel	r1 = [r3], +8 
	(p38) fetchadd4.rel.nt1	r1 = [r3], +8 
	(p39) fetchadd4.rel.nta	r1 = [r3], +8 
	(p40) fetchadd8.acq	r1 = [r3], +8 
	(p41) fetchadd8.acq.nt1	r1 = [r3], +8 
	(p42) fetchadd8.acq.nta	r1 = [r3], +8 
	(p43) fetchadd8.rel	r1 = [r3], +8 
	(p44) fetchadd8.rel.nt1	r1 = [r3], +8 
	(p45) fetchadd8.rel.nta	r1 = [r3], +8 

	(p46) fetchadd8.rel.nta	r1 = [r3], -16
	(p47) fetchadd8.rel.nta	r1 = [r3], -8
	(p48) fetchadd8.rel.nta	r1 = [r3], -4
	(p49) fetchadd8.rel.nta	r1 = [r3], -1
	(p50) fetchadd8.rel.nta	r1 = [r3], 1
	(p51) fetchadd8.rel.nta	r1 = [r3], 4
	(p52) fetchadd8.rel.nta	r1 = [r3], 8
	(p53) fetchadd8.rel.nta	r1 = [r3], 16
	nop 1

	flushrs
	nop 1

	(p54) fma   f10 = f3, f4, f5 
	(p55) fma.s0   f10 = f3, f4, f5 
	(p56) fma.s1   f10 = f3, f4, f5
	(p57) fma.s2   f10 = f3, f4, f5 
	(p58) fma.s3   f10 = f3, f4, f5 
	(p59) fma.s f10 = f3, f4, f5 
	(p60) fma.s.s0 f10 = f3, f4, f5 
	(p61) fma.s.s1 f10 = f3, f4, f5 
	(p62) fma.s.s2 f10 = f3, f4, f5 
	(p63) fma.s.s3 f10 = f3, f4, f5 
	(p0) fma.d f10 = f3, f4, f5 
	(p1) fma.d.s0 f10 = f3, f4, f5 
	(p2) fma.d.s1 f10 = f3, f4, f5 
	(p3) fma.d.s2 f10 = f3, f4, f5 
	(p4) fma.d.s3 f10 = f3, f4, f5 
	nop 1

	(p5) fmax		f10 = f3, f4
	(p6) fmax.s0		f10 = f3, f4
	(p7) fmax.s1		f10 = f3, f4
	(p8) fmax.s2		f10 = f3, f4
	(p9) fmax.s3		f10 = f3, f4
	nop 1

	(p10) fmerge.ns		f11 = f3, f4
	(p11) fmerge.s		f11 = f3, f4
	(p12) fmerge.se		f11 = f3, f4
	nop 1

	(p13) fmin		f10 = f3, f4
	(p14) fmin.s0		f10 = f3, f4
	(p15) fmin.s1		f10 = f3, f4
	(p16) fmin.s2		f10 = f3, f4
	(p17) fmin.s3		f10 = f3, f4
	nop 1

	(p18) fmix.l		f10 = f2, f3 
	(p19) fmix.r		f10 = f2, f3 
	(p20) fmix.lr		f10 = f2, f3 
	nop 1

	(p21) fmpy   f10 = f3, f4 
	(p22) fmpy.s0   f10 = f3, f4 
	(p23) fmpy.s1   f10 = f3, f4
	(p24) fmpy.s2   f10 = f3, f4 
	(p25) fmpy.s3   f10 = f3, f4 
	(p26) fmpy.s f10 = f3, f4 
	(p27) fmpy.s.s0 f10 = f3, f4 
	(p28) fmpy.s.s1 f10 = f3, f4 
	(p29) fmpy.s.s2 f10 = f3, f4 
	(p30) fmpy.s.s3 f10 = f3, f4 
	(p31) fmpy.d f10 = f3, f4 
	(p32) fmpy.d.s0 f10 = f3, f4 
	(p33) fmpy.d.s1 f10 = f3, f4 
	(p34) fmpy.d.s2 f10 = f3, f4 
	(p35) fmpy.d.s3 f10 = f3, f4 
	nop 1

	(p36) fms   f10 = f3, f4, f2 
	(p37) fms.s0   f10 = f3, f4, f2 
	(p38) fms.s1   f10 = f3, f4, f2
	(p39) fms.s2   f10 = f3, f4, f2 
	(p40) fms.s3   f10 = f3, f4, f2 
	(p41) fms.s f10 = f3, f4, f2 
	(p42) fms.s.s0 f10 = f3, f4, f2 
	(p43) fms.s.s1 f10 = f3, f4, f2 
	(p44) fms.s.s2 f10 = f3, f4, f2 
	(p45) fms.s.s3 f10 = f3, f4, f2 
	(p46) fms.d f10 = f3, f4, f2 
	(p47) fms.d.s0 f10 = f3, f4, f2 
	(p48) fms.d.s1 f10 = f3, f4, f2 
	(p49) fms.d.s2 f10 = f3, f4, f2 
	(p50) fms.d.s3 f10 = f3, f4, f2 
	nop 1

	(p51) fneg f10=f3
	nop 1

	(p52) fnegabs f10=f3
	nop 1

	(p53) fnma   f10 = f3, f4, f2 
	(p54) fnma.s0   f10 = f3, f4, f2 
	(p55) fnma.s1   f10 = f3, f4, f2
	(p56) fnma.s2   f10 = f3, f4, f2 
	(p57) fnma.s3   f10 = f3, f4, f2 
	(p58) fnma.s f10 = f3, f4, f2 
	(p59) fnma.s.s0 f10 = f3, f4, f2 
	(p60) fnma.s.s1 f10 = f3, f4, f2 
	(p61) fnma.s.s2 f10 = f3, f4, f2 
	(p62) fnma.s.s3 f10 = f3, f4, f2 
	(p63) fnma.d f10 = f3, f4, f2 
	(p0) fnma.d.s0 f10 = f3, f4, f2 
	(p1) fnma.d.s1 f10 = f3, f4, f2 
	(p2) fnma.d.s2 f10 = f3, f4, f2 
	(p3) fnma.d.s3 f10 = f3, f4, f2 
	nop 1

	(p4) fnmpy   f10 = f3, f4 
	(p5) fnmpy.s0   f10 = f3, f4 
	(p6) fnmpy.s1   f10 = f3, f4
	(p7) fnmpy.s2   f10 = f3, f4 
	(p8) fnmpy.s3   f10 = f3, f4 
	(p9) fnmpy.s f10 = f3, f4 
	(p10) fnmpy.s.s0 f10 = f3, f4 
	(p11) fnmpy.s.s1 f10 = f3, f4 
	(p12) fnmpy.s.s2 f10 = f3, f4 
	(p13) fnmpy.s.s3 f10 = f3, f4 
	(p14) fnmpy.d f10 = f3, f4 
	(p15) fnmpy.d.s0 f10 = f3, f4 
	(p16) fnmpy.d.s1 f10 = f3, f4 
	(p17) fnmpy.d.s2 f10 = f3, f4 
	(p18) fnmpy.d.s3 f10 = f3, f4 
	nop 1

	(p19) fnorm   	f10 = f3 
	(p20) fnorm.s0   f10 = f3 
	(p21) fnorm.s1   f10 = f3
	(p22) fnorm.s2   f10 = f3 
	(p23) fnorm.s3   f10 = f3 
	(p24) fnorm.s 	f10 = f3 
	(p25) fnorm.s.s0 f10 = f3 
	(p26) fnorm.s.s1 f10 = f3 
	(p27) fnorm.s.s2 f10 = f3 
	(p28) fnorm.s.s3 f10 = f3 
	(p29) fnorm.d 	f10 = f3 
	(p30) fnorm.d.s0 f10 = f3 
	(p31) fnorm.d.s1 f10 = f3 
	(p32) fnorm.d.s2 f10 = f3 
	(p33) fnorm.d.s3 f10 = f3 
	nop 1

	(p34) for 	f10 = f2, f3
	nop 1

	(p35) fpabs	f10 = f3
	nop 1

	(p36) fpack	f10 = f2, f3
	nop 1

	(p37) fpamax 	f10=f2,f3 
	(p38) fpamax.s0 f10=f2,f3 
	(p39) fpamax.s1	f10=f2,f3 
	(p40) fpamax.s2 f10=f2,f3 
	(p41) fpamax.s3 f10=f2,f3 
	nop 1

	(p42) fpamin 	f10=f2,f3 
	(p43) fpamin.s0 f10=f2,f3 
	(p44) fpamin.s1	f10=f2,f3 
	(p45) fpamin.s2 f10=f2,f3 
	(p46) fpamin.s3 f10=f2,f3 
	nop 1

	(p62) fpcmp.eq 		f10 = f2, f3
	(p63) fpcmp.lt 		f10 = f2, f3
	(p0) fpcmp.le 		f10 = f2, f3
	(p1) fpcmp.gt 		f10 = f2, f3
	(p2) fpcmp.ge 		f10 = f2, f3
	(p3) fpcmp.unord 	f10 = f2, f3
	(p4) fpcmp.neq		f10 = f2, f3
	(p5) fpcmp.nlt		f10 = f2, f3
	(p6) fpcmp.nle		f10 = f2, f3
	(p7) fpcmp.ngt		f10 = f2, f3
	(p8) fpcmp.nge		f10 = f2, f3
	(p9) fpcmp.ord		f10 = f2, f3
	(p24) fpcmp.eq.s0 		f10 = f2, f3
	(p25) fpcmp.lt.s0 		f10 = f2, f3
	(p26) fpcmp.le.s0 		f10 = f2, f3
	(p27) fpcmp.gt.s0 		f10 = f2, f3
	(p28) fpcmp.ge.s0 		f10 = f2, f3
	(p29) fpcmp.unord.s0 		f10 = f2, f3
	(p30) fpcmp.neq.s0		f10 = f2, f3
	(p31) fpcmp.nlt.s0		f10 = f2, f3
	(p32) fpcmp.nle.s0		f10 = f2, f3
	(p33) fpcmp.ngt.s0		f10 = f2, f3
	(p34) fpcmp.nge.s0		f10 = f2, f3
	(p34) fpcmp.ord.s0		f10 = f2, f3
	(p49) fpcmp.eq.s1 		f10 = f2, f3
	(p50) fpcmp.lt.s1 		f10 = f2, f3
	(p51) fpcmp.le.s1 		f10 = f2, f3
	(p52) fpcmp.gt.s1 		f10 = f2, f3
	(p53) fpcmp.ge.s1 		f10 = f2, f3
	(p54) fpcmp.unord.s1 		f10 = f2, f3
	(p55) fpcmp.neq.s1		f10 = f2, f3
	(p56) fpcmp.nlt.s1		f10 = f2, f3
	(p57) fpcmp.nle.s1		f10 = f2, f3
	(p58) fpcmp.ngt.s1		f10 = f2, f3
	(p59) fpcmp.nge.s1		f10 = f2, f3
	(p60) fpcmp.ord.s1		f10 = f2, f3
	(p11) fpcmp.eq.s2 		f10 = f2, f3
	(p12) fpcmp.lt.s2 		f10 = f2, f3
	(p13) fpcmp.le.s2 		f10 = f2, f3
	(p14) fpcmp.gt.s2 		f10 = f2, f3
	(p15) fpcmp.ge.s2 		f10 = f2, f3
	(p16) fpcmp.unord.s2 		f10 = f2, f3
	(p17) fpcmp.neq.s2		f10 = f2, f3
	(p18) fpcmp.nlt.s2		f10 = f2, f3
	(p19) fpcmp.nle.s2		f10 = f2, f3
	(p20) fpcmp.ngt.s2		f10 = f2, f3
	(p21) fpcmp.nge.s2		f10 = f2, f3
	(p22) fpcmp.ord.s2		f10 = f2, f3
	(p37) fpcmp.eq.s3 		f10 = f2, f3
	(p38) fpcmp.lt.s3 		f10 = f2, f3
	(p39) fpcmp.le.s3 		f10 = f2, f3
	(p40) fpcmp.gt.s3 		f10 = f2, f3
	(p41) fpcmp.ge.s3 		f10 = f2, f3
	(p42) fpcmp.unord.s3 		f10 = f2, f3
	(p43) fpcmp.neq.s3		f10 = f2, f3
	(p44) fpcmp.nlt.s3		f10 = f2, f3
	(p45) fpcmp.nle.s3		f10 = f2, f3
	(p46) fpcmp.ngt.s3		f10 = f2, f3
	(p47) fpcmp.nge.s3		f10 = f2, f3
	(p48) fpcmp.ord.s3		f10 = f2, f3
	nop 1

	(p62) fpcvt.fx		f11 = f3
	(p63) fpcvt.fx.trunc	f11 = f3
	(p0) fpcvt.fx.s0		f11 = f3
	(p1) fpcvt.fx.trunc.s0	f11 = f3
	(p2) fpcvt.fx.s1		f11 = f3
	(p3) fpcvt.fx.trunc.s1	f11 = f3
	(p4) fpcvt.fx.s2		f11 = f3
	(p5) fpcvt.fx.trunc.s2	f11 = f3
	(p6) fpcvt.fx.s3		f11 = f3
	(p7) fpcvt.fx.trunc.s3	f11 = f3
	(p8) fpcvt.fxu		f11 = f3
	(p9) fpcvt.fxu.trunc	f11 = f3
	(p10) fpcvt.fxu.s0	f11 = f3
	(p11) fpcvt.fxu.trunc.s0	f11 = f3
	(p12) fpcvt.fxu.s1	f11 = f3
	(p13) fpcvt.fxu.trunc.s1	f11 = f3
	(p14) fpcvt.fxu.s2	f11 = f3
	(p15) fpcvt.fxu.trunc.s2	f11 = f3
	(p16) fpcvt.fxu.s3	f11 = f3
	(p17) fpcvt.fxu.trunc.s3	f11 = f3
	nop 1

	(p18) fpma	f10 = f3, f4, f2
	(p19) fpma.s0	f10 = f3, f4, f2
	(p20) fpma.s1	f10 = f3, f4, f2
	(p21) fpma.s2	f10 = f3, f4, f2
	(p22) fpma.s3	f10 = f3, f4, f2
	nop 1

	(p5) fpmax		f10 = f3, f4
	(p6) fpmax.s0		f10 = f3, f4
	(p7) fpmax.s1		f10 = f3, f4
	(p8) fpmax.s2		f10 = f3, f4
	(p9) fpmax.s3		f10 = f3, f4
	nop 1

	(p10) fpmerge.ns	f11 = f3, f4
	(p11) fpmerge.s		f11 = f3, f4
	(p12) fpmerge.se	f11 = f3, f4
	nop 1

	(p5) fpmin		f10 = f3, f4
	(p6) fpmin.s0		f10 = f3, f4
	(p7) fpmin.s1		f10 = f3, f4
	(p8) fpmin.s2		f10 = f3, f4
	(p9) fpmin.s3		f10 = f3, f4
	nop 1

	(p18) fpmpy	f10 = f3, f4
	(p19) fpmpy.s0	f10 = f3, f4
	(p20) fpmpy.s1	f10 = f3, f4
	(p21) fpmpy.s2	f10 = f3, f4
	(p22) fpmpy.s3	f10 = f3, f4
	nop 1

	(p18) fpms	f10 = f3, f4, f2
	(p19) fpms.s0	f10 = f3, f4, f2
	(p20) fpms.s1	f10 = f3, f4, f2
	(p21) fpms.s2	f10 = f3, f4, f2
	(p22) fpms.s3	f10 = f3, f4, f2
	nop 1

	(p23) fpneg	f10 = f3
	nop 1

	(p24) fpnegabs	f10 = f3
	nop 1

	(p18) fpnma	f10 = f3, f4, f2
	(p19) fpnma.s0	f10 = f3, f4, f2
	(p20) fpnma.s1	f10 = f3, f4, f2
	(p21) fpnma.s2	f10 = f3, f4, f2
	(p22) fpnma.s3	f10 = f3, f4, f2
	nop 1

	(p18) fpnmpy	f10 = f3, f4
	(p19) fpnmpy.s0	f10 = f3, f4
	(p20) fpnmpy.s1	f10 = f3, f4
	(p21) fpnmpy.s2	f10 = f3, f4
	(p22) fpnmpy.s3	f10 = f3, f4
	nop 1

	(p23) fprcpa	f1,p2 = f2, f3
	(p24) fprcpa.s0	f1,p2 = f2, f3
	(p25) fprcpa.s1	f1,p2 = f2, f3
	(p26) fprcpa.s2	f1,p2 = f2, f3
	(p27) fprcpa.s3	f1,p2 = f2, f3
	nop 1

	(p23) fprsqrta		f1,p2 = f2
	(p24) fprsqrta.s0	f1,p2 = f2
	(p25) fprsqrta.s1	f1,p2 = f2
	(p26) fprsqrta.s2	f1,p2 = f2
	(p27) fprsqrta.s3	f1,p2 = f2
	nop 1

	(p23) frcpa	f1,p2 = f2, f3
	(p24) frcpa.s0	f1,p2 = f2, f3
	(p25) frcpa.s1	f1,p2 = f2, f3
	(p26) frcpa.s2	f1,p2 = f2, f3
	(p27) frcpa.s3	f1,p2 = f2, f3
	nop 1

	(p23) frsqrta		f1,p2 = f2
	(p24) frsqrta.s0	f1,p2 = f2
	(p25) frsqrta.s1	f1,p2 = f2
	(p26) frsqrta.s2	f1,p2 = f2
	(p27) frsqrta.s3	f1,p2 = f2
	nop 1

	(p28) fselect	f10 = f3, f4, f2
	nop 1

	(p29) fsetc	0x7f, 0x41
	(p30) fsetc.s0	0x7f, 0x41
	(p31) fsetc.s1	0x7f, 0x41
	(p32) fsetc.s2	0x7f, 0x41
	(p33) fsetc.s3	0x7f, 0x41
	nop 1

	(p51) fsub   f10 = f2, f3 
	(p52) fsub.s0   f10 = f2, f3 
	(p53) fsub.s1   f10 = f2, f3
	(p54) fsub.s2   f10 = f2, f3 
	(p55) fsub.s3   f10 = f2, f3 
	(p56) fsub.s f10 = f2, f3 
	(p57) fsub.s.s0 f10 = f2, f3 
	(p58) fsub.s.s1 f10 = f2, f3 
	(p59) fsub.s.s2 f10 = f2, f3 
	(p60) fsub.s.s3 f10 = f2, f3 
	(p61) fsub.d f10 = f2, f3 
	(p62) fsub.d.s0 f10 = f2, f3 
	(p63) fsub.d.s1 f10 = f2, f3 
	(p0) fsub.d.s2 f10 = f2, f3 
	(p1) fsub.d.s3 f10 = f2, f3 
	nop 1

	(p52) fswap	f10 = f2, f3
	(p53) fswap.nl	f10 = f2, f3
	(p54) fswap.nr	f10 = f2, f3
	nop 1

	(p55) fsxt.l f10 = f2, f3
	(p56) fsxt.r f10 = f2, f3
	nop 1

	(p57) fwb
	nop 1

	(p58) fxor	f10 = f2, f3
	nop 1

	//// ******** G *********

	(p59) getf.s r1 = f2
	(p60) getf.d r1 = f2
	(p61) getf.exp r1 = f2
	(p62) getf.sig r1 = f2
	nop 1

	//// ******** H *********

	//// ******** I *********

	(p63) invala 	;;
	(p0) invala.e	r1
	(p1) invala.e   f1
	nop 1

	(p2) itc.i	r2
	(p3) itc.d	r2
	nop 1
	
	(p4) itr.i	itr[r3]=r2
	(p5) itr.d	dtr[r3]=r2
	nop 1

	//// ******** J *********

	//// ******** K *********

	//// ******** L *********

	(p0) ld1		r1 = [r3]
	(p1) ld1		r1 = [r3],r2
	(p2) ld1		r1 = [r3],9
	(p3) ld1		r1 = [r3],-9
	(p4) ld1.nt1		r1 = [r3]
	(p5) ld1.nt1		r1 = [r3],r2
	(p6) ld1.nt1		r1 = [r3],9
	(p7) ld1.nt1		r1 = [r3],-9
	(p8) ld1.nta		r1 = [r3]
	(p9) ld1.nta		r1 = [r3],r2
	(p10) ld1.nta		r1 = [r3],9
	(p11) ld1.nta		r1 = [r3],-9
	(p20) ld1.s		r1 = [r3]
	(p21) ld1.s		r1 = [r3],r2
	(p22) ld1.s		r1 = [r3],9
	(p23) ld1.s		r1 = [r3],-9
	(p24) ld1.s.nt1		r1 = [r3]
	(p25) ld1.s.nt1		r1 = [r3],r2
	(p26) ld1.s.nt1		r1 = [r3],9
	(p27) ld1.s.nt1		r1 = [r3],-9
	(p28) ld1.s.nta		r1 = [r3]
	(p29) ld1.s.nta		r1 = [r3],r2
	(p30) ld1.s.nta		r1 = [r3],9
	(p31) ld1.s.nta		r1 = [r3],-9
	(p40) ld1.a		r1 = [r3]
	(p41) ld1.a		r1 = [r3],r2
	(p42) ld1.a		r1 = [r3],9
	(p43) ld1.a		r1 = [r3],-9
	(p44) ld1.a.nt1		r1 = [r3]
	(p45) ld1.a.nt1		r1 = [r3],r2
	(p46) ld1.a.nt1		r1 = [r3],9
	(p47) ld1.a.nt1		r1 = [r3],-9
	(p48) ld1.a.nta		r1 = [r3]
	(p49) ld1.a.nta		r1 = [r3],r2
	(p50) ld1.a.nta		r1 = [r3],9
	(p51) ld1.a.nta		r1 = [r3],-9
	(p20) ld1.sa		r1 = [r3]
	(p21) ld1.sa		r1 = [r3],r2
	(p22) ld1.sa		r1 = [r3],9
	(p23) ld1.sa		r1 = [r3],-9
	(p24) ld1.sa.nt1		r1 = [r3]
	(p25) ld1.sa.nt1		r1 = [r3],r2
	(p26) ld1.sa.nt1		r1 = [r3],9
	(p27) ld1.sa.nt1		r1 = [r3],-9
	(p28) ld1.sa.nta		r1 = [r3]
	(p29) ld1.sa.nta		r1 = [r3],r2
	(p30) ld1.sa.nta		r1 = [r3],9
	(p31) ld1.sa.nta		r1 = [r3],-9
	(p40) ld1.c.nc		r1 = [r3]
	(p41) ld1.c.nc		r1 = [r3],r2
	(p42) ld1.c.nc		r1 = [r3],9
	(p43) ld1.c.nc		r1 = [r3],-9
	(p44) ld1.c.nc.nt1		r1 = [r3]
	(p45) ld1.c.nc.nt1		r1 = [r3],r2
	(p46) ld1.c.nc.nt1		r1 = [r3],9
	(p47) ld1.c.nc.nt1		r1 = [r3],-9
	(p48) ld1.c.nc.nta		r1 = [r3]
	(p49) ld1.c.nc.nta		r1 = [r3],r2
	(p50) ld1.c.nc.nta		r1 = [r3],9
	(p51) ld1.c.nc.nta		r1 = [r3],-9
	(p20) ld1.c.clr		r1 = [r3]
	(p21) ld1.c.clr		r1 = [r3],r2
	(p22) ld1.c.clr		r1 = [r3],9
	(p23) ld1.c.clr		r1 = [r3],-9
	(p24) ld1.c.clr.nt1		r1 = [r3]
	(p25) ld1.c.clr.nt1		r1 = [r3],r2
	(p26) ld1.c.clr.nt1		r1 = [r3],9
	(p27) ld1.c.clr.nt1		r1 = [r3],-9
	(p28) ld1.c.clr.nta		r1 = [r3]
	(p29) ld1.c.clr.nta		r1 = [r3],r2
	(p30) ld1.c.clr.nta		r1 = [r3],9
	(p31) ld1.c.clr.nta		r1 = [r3],-9
	(p40) ld1.c.clr.acq		r1 = [r3]
	(p41) ld1.c.clr.acq		r1 = [r3],r2
	(p42) ld1.c.clr.acq		r1 = [r3],9
	(p43) ld1.c.clr.acq		r1 = [r3],-9
	(p44) ld1.c.clr.acq.nt1		r1 = [r3]
	(p45) ld1.c.clr.acq.nt1		r1 = [r3],r2
	(p46) ld1.c.clr.acq.nt1		r1 = [r3],9
	(p47) ld1.c.clr.acq.nt1		r1 = [r3],-9
	(p48) ld1.c.clr.acq.nta		r1 = [r3]
	(p49) ld1.c.clr.acq.nta		r1 = [r3],r2
	(p50) ld1.c.clr.acq.nta		r1 = [r3],9
	(p51) ld1.c.clr.acq.nta		r1 = [r3],-9
	(p20) ld1.acq		r1 = [r3]
	(p21) ld1.acq		r1 = [r3],r2
	(p22) ld1.acq		r1 = [r3],9
	(p23) ld1.acq		r1 = [r3],-9
	(p24) ld1.acq.nt1		r1 = [r3]
	(p25) ld1.acq.nt1		r1 = [r3],r2
	(p26) ld1.acq.nt1		r1 = [r3],9
	(p27) ld1.acq.nt1		r1 = [r3],-9
	(p28) ld1.acq.nta		r1 = [r3]
	(p29) ld1.acq.nta		r1 = [r3],r2
	(p30) ld1.acq.nta		r1 = [r3],9
	(p31) ld1.acq.nta		r1 = [r3],-9
	(p40) ld1.bias		r1 = [r3]
	(p41) ld1.bias		r1 = [r3],r2
	(p42) ld1.bias		r1 = [r3],9
	(p43) ld1.bias		r1 = [r3],-9
	(p44) ld1.bias.nt1		r1 = [r3]
	(p45) ld1.bias.nt1		r1 = [r3],r2
	(p46) ld1.bias.nt1		r1 = [r3],9
	(p47) ld1.bias.nt1		r1 = [r3],-9
	(p48) ld1.bias.nta		r1 = [r3]
	(p49) ld1.bias.nta		r1 = [r3],r2
	(p50) ld1.bias.nta		r1 = [r3],9
	(p51) ld1.bias.nta		r1 = [r3],-9
	(p0) ld2		r1 = [r3]
	(p1) ld2		r1 = [r3],r2
	(p2) ld2		r1 = [r3],9
	(p3) ld2		r1 = [r3],-9
	(p4) ld2.nt1		r1 = [r3]
	(p5) ld2.nt1		r1 = [r3],r2
	(p6) ld2.nt1		r1 = [r3],9
	(p7) ld2.nt1		r1 = [r3],-9
	(p8) ld2.nta		r1 = [r3]
	(p9) ld2.nta		r1 = [r3],r2
	(p10) ld2.nta		r1 = [r3],9
	(p11) ld2.nta		r1 = [r3],-9
	(p20) ld2.s		r1 = [r3]
	(p21) ld2.s		r1 = [r3],r2
	(p22) ld2.s		r1 = [r3],9
	(p23) ld2.s		r1 = [r3],-9
	(p24) ld2.s.nt1		r1 = [r3]
	(p25) ld2.s.nt1		r1 = [r3],r2
	(p26) ld2.s.nt1		r1 = [r3],9
	(p27) ld2.s.nt1		r1 = [r3],-9
	(p28) ld2.s.nta		r1 = [r3]
	(p29) ld2.s.nta		r1 = [r3],r2
	(p30) ld2.s.nta		r1 = [r3],9
	(p31) ld2.s.nta		r1 = [r3],-9
	(p40) ld2.a		r1 = [r3]
	(p41) ld2.a		r1 = [r3],r2
	(p42) ld2.a		r1 = [r3],9
	(p43) ld2.a		r1 = [r3],-9
	(p44) ld2.a.nt1		r1 = [r3]
	(p45) ld2.a.nt1		r1 = [r3],r2
	(p46) ld2.a.nt1		r1 = [r3],9
	(p47) ld2.a.nt1		r1 = [r3],-9
	(p48) ld2.a.nta		r1 = [r3]
	(p49) ld2.a.nta		r1 = [r3],r2
	(p50) ld2.a.nta		r1 = [r3],9
	(p51) ld2.a.nta		r1 = [r3],-9
	(p20) ld2.sa		r1 = [r3]
	(p21) ld2.sa		r1 = [r3],r2
	(p22) ld2.sa		r1 = [r3],9
	(p23) ld2.sa		r1 = [r3],-9
	(p24) ld2.sa.nt1		r1 = [r3]
	(p25) ld2.sa.nt1		r1 = [r3],r2
	(p26) ld2.sa.nt1		r1 = [r3],9
	(p27) ld2.sa.nt1		r1 = [r3],-9
	(p28) ld2.sa.nta		r1 = [r3]
	(p29) ld2.sa.nta		r1 = [r3],r2
	(p30) ld2.sa.nta		r1 = [r3],9
	(p31) ld2.sa.nta		r1 = [r3],-9
	(p40) ld2.c.nc		r1 = [r3]
	(p41) ld2.c.nc		r1 = [r3],r2
	(p42) ld2.c.nc		r1 = [r3],9
	(p43) ld2.c.nc		r1 = [r3],-9
	(p44) ld2.c.nc.nt1		r1 = [r3]
	(p45) ld2.c.nc.nt1		r1 = [r3],r2
	(p46) ld2.c.nc.nt1		r1 = [r3],9
	(p47) ld2.c.nc.nt1		r1 = [r3],-9
	(p48) ld2.c.nc.nta		r1 = [r3]
	(p49) ld2.c.nc.nta		r1 = [r3],r2
	(p50) ld2.c.nc.nta		r1 = [r3],9
	(p51) ld2.c.nc.nta		r1 = [r3],-9
	(p20) ld2.c.clr		r1 = [r3]
	(p21) ld2.c.clr		r1 = [r3],r2
	(p22) ld2.c.clr		r1 = [r3],9
	(p23) ld2.c.clr		r1 = [r3],-9
	(p24) ld2.c.clr.nt1		r1 = [r3]
	(p25) ld2.c.clr.nt1		r1 = [r3],r2
	(p26) ld2.c.clr.nt1		r1 = [r3],9
	(p27) ld2.c.clr.nt1		r1 = [r3],-9
	(p28) ld2.c.clr.nta		r1 = [r3]
	(p29) ld2.c.clr.nta		r1 = [r3],r2
	(p30) ld2.c.clr.nta		r1 = [r3],9
	(p31) ld2.c.clr.nta		r1 = [r3],-9
	(p40) ld2.c.clr.acq		r1 = [r3]
	(p41) ld2.c.clr.acq		r1 = [r3],r2
	(p42) ld2.c.clr.acq		r1 = [r3],9
	(p43) ld2.c.clr.acq		r1 = [r3],-9
	(p44) ld2.c.clr.acq.nt1		r1 = [r3]
	(p45) ld2.c.clr.acq.nt1		r1 = [r3],r2
	(p46) ld2.c.clr.acq.nt1		r1 = [r3],9
	(p47) ld2.c.clr.acq.nt1		r1 = [r3],-9
	(p48) ld2.c.clr.acq.nta		r1 = [r3]
	(p49) ld2.c.clr.acq.nta		r1 = [r3],r2
	(p50) ld2.c.clr.acq.nta		r1 = [r3],9
	(p51) ld2.c.clr.acq.nta		r1 = [r3],-9
	(p20) ld2.acq		r1 = [r3]
	(p21) ld2.acq		r1 = [r3],r2
	(p22) ld2.acq		r1 = [r3],9
	(p23) ld2.acq		r1 = [r3],-9
	(p24) ld2.acq.nt1		r1 = [r3]
	(p25) ld2.acq.nt1		r1 = [r3],r2
	(p26) ld2.acq.nt1		r1 = [r3],9
	(p27) ld2.acq.nt1		r1 = [r3],-9
	(p28) ld2.acq.nta		r1 = [r3]
	(p29) ld2.acq.nta		r1 = [r3],r2
	(p30) ld2.acq.nta		r1 = [r3],9
	(p31) ld2.acq.nta		r1 = [r3],-9
	(p40) ld2.bias		r1 = [r3]
	(p41) ld2.bias		r1 = [r3],r2
	(p42) ld2.bias		r1 = [r3],9
	(p43) ld2.bias		r1 = [r3],-9
	(p44) ld2.bias.nt1		r1 = [r3]
	(p45) ld2.bias.nt1		r1 = [r3],r2
	(p46) ld2.bias.nt1		r1 = [r3],9
	(p47) ld2.bias.nt1		r1 = [r3],-9
	(p48) ld2.bias.nta		r1 = [r3]
	(p49) ld2.bias.nta		r1 = [r3],r2
	(p50) ld2.bias.nta		r1 = [r3],9
	(p51) ld2.bias.nta		r1 = [r3],-9
	(p0) ld4		r1 = [r3]
	(p1) ld4		r1 = [r3],r2
	(p2) ld4		r1 = [r3],9
	(p3) ld4		r1 = [r3],-9
	(p4) ld4.nt1		r1 = [r3]
	(p5) ld4.nt1		r1 = [r3],r2
	(p6) ld4.nt1		r1 = [r3],9
	(p7) ld4.nt1		r1 = [r3],-9
	(p8) ld4.nta		r1 = [r3]
	(p9) ld4.nta		r1 = [r3],r2
	(p10) ld4.nta		r1 = [r3],9
	(p11) ld4.nta		r1 = [r3],-9
	(p20) ld4.s		r1 = [r3]
	(p21) ld4.s		r1 = [r3],r2
	(p22) ld4.s		r1 = [r3],9
	(p23) ld4.s		r1 = [r3],-9
	(p24) ld4.s.nt1		r1 = [r3]
	(p25) ld4.s.nt1		r1 = [r3],r2
	(p26) ld4.s.nt1		r1 = [r3],9
	(p27) ld4.s.nt1		r1 = [r3],-9
	(p28) ld4.s.nta		r1 = [r3]
	(p29) ld4.s.nta		r1 = [r3],r2
	(p30) ld4.s.nta		r1 = [r3],9
	(p31) ld4.s.nta		r1 = [r3],-9
	(p40) ld4.a		r1 = [r3]
	(p41) ld4.a		r1 = [r3],r2
	(p42) ld4.a		r1 = [r3],9
	(p43) ld4.a		r1 = [r3],-9
	(p44) ld4.a.nt1		r1 = [r3]
	(p45) ld4.a.nt1		r1 = [r3],r2
	(p46) ld4.a.nt1		r1 = [r3],9
	(p47) ld4.a.nt1		r1 = [r3],-9
	(p48) ld4.a.nta		r1 = [r3]
	(p49) ld4.a.nta		r1 = [r3],r2
	(p50) ld4.a.nta		r1 = [r3],9
	(p51) ld4.a.nta		r1 = [r3],-9
	(p20) ld4.sa		r1 = [r3]
	(p21) ld4.sa		r1 = [r3],r2
	(p22) ld4.sa		r1 = [r3],9
	(p23) ld4.sa		r1 = [r3],-9
	(p24) ld4.sa.nt1		r1 = [r3]
	(p25) ld4.sa.nt1		r1 = [r3],r2
	(p26) ld4.sa.nt1		r1 = [r3],9
	(p27) ld4.sa.nt1		r1 = [r3],-9
	(p28) ld4.sa.nta		r1 = [r3]
	(p29) ld4.sa.nta		r1 = [r3],r2
	(p30) ld4.sa.nta		r1 = [r3],9
	(p31) ld4.sa.nta		r1 = [r3],-9
	(p40) ld4.c.nc		r1 = [r3]
	(p41) ld4.c.nc		r1 = [r3],r2
	(p42) ld4.c.nc		r1 = [r3],9
	(p43) ld4.c.nc		r1 = [r3],-9
	(p44) ld4.c.nc.nt1		r1 = [r3]
	(p45) ld4.c.nc.nt1		r1 = [r3],r2
	(p46) ld4.c.nc.nt1		r1 = [r3],9
	(p47) ld4.c.nc.nt1		r1 = [r3],-9
	(p48) ld4.c.nc.nta		r1 = [r3]
	(p49) ld4.c.nc.nta		r1 = [r3],r2
	(p50) ld4.c.nc.nta		r1 = [r3],9
	(p51) ld4.c.nc.nta		r1 = [r3],-9
	(p20) ld4.c.clr		r1 = [r3]
	(p21) ld4.c.clr		r1 = [r3],r2
	(p22) ld4.c.clr		r1 = [r3],9
	(p23) ld4.c.clr		r1 = [r3],-9
	(p24) ld4.c.clr.nt1		r1 = [r3]
	(p25) ld4.c.clr.nt1		r1 = [r3],r2
	(p26) ld4.c.clr.nt1		r1 = [r3],9
	(p27) ld4.c.clr.nt1		r1 = [r3],-9
	(p28) ld4.c.clr.nta		r1 = [r3]
	(p29) ld4.c.clr.nta		r1 = [r3],r2
	(p30) ld4.c.clr.nta		r1 = [r3],9
	(p31) ld4.c.clr.nta		r1 = [r3],-9
	(p40) ld4.c.clr.acq		r1 = [r3]
	(p41) ld4.c.clr.acq		r1 = [r3],r2
	(p42) ld4.c.clr.acq		r1 = [r3],9
	(p43) ld4.c.clr.acq		r1 = [r3],-9
	(p44) ld4.c.clr.acq.nt1		r1 = [r3]
	(p45) ld4.c.clr.acq.nt1		r1 = [r3],r2
	(p46) ld4.c.clr.acq.nt1		r1 = [r3],9
	(p47) ld4.c.clr.acq.nt1		r1 = [r3],-9
	(p48) ld4.c.clr.acq.nta		r1 = [r3]
	(p49) ld4.c.clr.acq.nta		r1 = [r3],r2
	(p50) ld4.c.clr.acq.nta		r1 = [r3],9
	(p51) ld4.c.clr.acq.nta		r1 = [r3],-9
	(p20) ld4.acq		r1 = [r3]
	(p21) ld4.acq		r1 = [r3],r2
	(p22) ld4.acq		r1 = [r3],9
	(p23) ld4.acq		r1 = [r3],-9
	(p24) ld4.acq.nt1		r1 = [r3]
	(p25) ld4.acq.nt1		r1 = [r3],r2
	(p26) ld4.acq.nt1		r1 = [r3],9
	(p27) ld4.acq.nt1		r1 = [r3],-9
	(p28) ld4.acq.nta		r1 = [r3]
	(p29) ld4.acq.nta		r1 = [r3],r2
	(p30) ld4.acq.nta		r1 = [r3],9
	(p31) ld4.acq.nta		r1 = [r3],-9
	(p40) ld4.bias		r1 = [r3]
	(p41) ld4.bias		r1 = [r3],r2
	(p42) ld4.bias		r1 = [r3],9
	(p43) ld4.bias		r1 = [r3],-9
	(p44) ld4.bias.nt1		r1 = [r3]
	(p45) ld4.bias.nt1		r1 = [r3],r2
	(p46) ld4.bias.nt1		r1 = [r3],9
	(p47) ld4.bias.nt1		r1 = [r3],-9
	(p48) ld4.bias.nta		r1 = [r3]
	(p49) ld4.bias.nta		r1 = [r3],r2
	(p50) ld4.bias.nta		r1 = [r3],9
	(p51) ld4.bias.nta		r1 = [r3],-9
	(p0) ld8		r1 = [r3]
	(p1) ld8		r1 = [r3],r2
	(p2) ld8		r1 = [r3],9
	(p3) ld8		r1 = [r3],-9
	(p4) ld8.nt1		r1 = [r3]
	(p5) ld8.nt1		r1 = [r3],r2
	(p6) ld8.nt1		r1 = [r3],9
	(p7) ld8.nt1		r1 = [r3],-9
	(p8) ld8.nta		r1 = [r3]
	(p9) ld8.nta		r1 = [r3],r2
	(p10) ld8.nta		r1 = [r3],9
	(p11) ld8.nta		r1 = [r3],-9
	(p20) ld8.s		r1 = [r3]
	(p21) ld8.s		r1 = [r3],r2
	(p22) ld8.s		r1 = [r3],9
	(p23) ld8.s		r1 = [r3],-9
	(p24) ld8.s.nt1		r1 = [r3]
	(p25) ld8.s.nt1		r1 = [r3],r2
	(p26) ld8.s.nt1		r1 = [r3],9
	(p27) ld8.s.nt1		r1 = [r3],-9
	(p28) ld8.s.nta		r1 = [r3]
	(p29) ld8.s.nta		r1 = [r3],r2
	(p30) ld8.s.nta		r1 = [r3],9
	(p31) ld8.s.nta		r1 = [r3],-9
	(p40) ld8.a		r1 = [r3]
	(p41) ld8.a		r1 = [r3],r2
	(p42) ld8.a		r1 = [r3],9
	(p43) ld8.a		r1 = [r3],-9
	(p44) ld8.a.nt1		r1 = [r3]
	(p45) ld8.a.nt1		r1 = [r3],r2
	(p46) ld8.a.nt1		r1 = [r3],9
	(p47) ld8.a.nt1		r1 = [r3],-9
	(p48) ld8.a.nta		r1 = [r3]
	(p49) ld8.a.nta		r1 = [r3],r2
	(p50) ld8.a.nta		r1 = [r3],9
	(p51) ld8.a.nta		r1 = [r3],-9
	(p20) ld8.sa		r1 = [r3]
	(p21) ld8.sa		r1 = [r3],r2
	(p22) ld8.sa		r1 = [r3],9
	(p23) ld8.sa		r1 = [r3],-9
	(p24) ld8.sa.nt1		r1 = [r3]
	(p25) ld8.sa.nt1		r1 = [r3],r2
	(p26) ld8.sa.nt1		r1 = [r3],9
	(p27) ld8.sa.nt1		r1 = [r3],-9
	(p28) ld8.sa.nta		r1 = [r3]
	(p29) ld8.sa.nta		r1 = [r3],r2
	(p30) ld8.sa.nta		r1 = [r3],9
	(p31) ld8.sa.nta		r1 = [r3],-9
	(p40) ld8.c.nc		r1 = [r3]
	(p41) ld8.c.nc		r1 = [r3],r2
	(p42) ld8.c.nc		r1 = [r3],9
	(p43) ld8.c.nc		r1 = [r3],-9
	(p44) ld8.c.nc.nt1		r1 = [r3]
	(p45) ld8.c.nc.nt1		r1 = [r3],r2
	(p46) ld8.c.nc.nt1		r1 = [r3],9
	(p47) ld8.c.nc.nt1		r1 = [r3],-9
	(p48) ld8.c.nc.nta		r1 = [r3]
	(p49) ld8.c.nc.nta		r1 = [r3],r2
	(p50) ld8.c.nc.nta		r1 = [r3],9
	(p51) ld8.c.nc.nta		r1 = [r3],-9
	(p20) ld8.c.clr		r1 = [r3]
	(p21) ld8.c.clr		r1 = [r3],r2
	(p22) ld8.c.clr		r1 = [r3],9
	(p23) ld8.c.clr		r1 = [r3],-9
	(p24) ld8.c.clr.nt1		r1 = [r3]
	(p25) ld8.c.clr.nt1		r1 = [r3],r2
	(p26) ld8.c.clr.nt1		r1 = [r3],9
	(p27) ld8.c.clr.nt1		r1 = [r3],-9
	(p28) ld8.c.clr.nta		r1 = [r3]
	(p29) ld8.c.clr.nta		r1 = [r3],r2
	(p30) ld8.c.clr.nta		r1 = [r3],9
	(p31) ld8.c.clr.nta		r1 = [r3],-9
	(p40) ld8.c.clr.acq		r1 = [r3]
	(p41) ld8.c.clr.acq		r1 = [r3],r2
	(p42) ld8.c.clr.acq		r1 = [r3],9
	(p43) ld8.c.clr.acq		r1 = [r3],-9
	(p44) ld8.c.clr.acq.nt1		r1 = [r3]
	(p45) ld8.c.clr.acq.nt1		r1 = [r3],r2
	(p46) ld8.c.clr.acq.nt1		r1 = [r3],9
	(p47) ld8.c.clr.acq.nt1		r1 = [r3],-9
	(p48) ld8.c.clr.acq.nta		r1 = [r3]
	(p49) ld8.c.clr.acq.nta		r1 = [r3],r2
	(p50) ld8.c.clr.acq.nta		r1 = [r3],9
	(p51) ld8.c.clr.acq.nta		r1 = [r3],-9
	(p20) ld8.acq		r1 = [r3]
	(p21) ld8.acq		r1 = [r3],r2
	(p22) ld8.acq		r1 = [r3],9
	(p23) ld8.acq		r1 = [r3],-9
	(p24) ld8.acq.nt1		r1 = [r3]
	(p25) ld8.acq.nt1		r1 = [r3],r2
	(p26) ld8.acq.nt1		r1 = [r3],9
	(p27) ld8.acq.nt1		r1 = [r3],-9
	(p28) ld8.acq.nta		r1 = [r3]
	(p29) ld8.acq.nta		r1 = [r3],r2
	(p30) ld8.acq.nta		r1 = [r3],9
	(p31) ld8.acq.nta		r1 = [r3],-9
	(p40) ld8.bias		r1 = [r3]
	(p41) ld8.bias		r1 = [r3],r2
	(p42) ld8.bias		r1 = [r3],9
	(p43) ld8.bias		r1 = [r3],-9
	(p44) ld8.bias.nt1		r1 = [r3]
	(p45) ld8.bias.nt1		r1 = [r3],r2
	(p46) ld8.bias.nt1		r1 = [r3],9
	(p47) ld8.bias.nt1		r1 = [r3],-9
	(p48) ld8.bias.nta		r1 = [r3]
	(p49) ld8.bias.nta		r1 = [r3],r2
	(p50) ld8.bias.nta		r1 = [r3],9
	(p51) ld8.bias.nta		r1 = [r3],-9
	(p0) ld8.fill		r1 = [r3]
	(p1) ld8.fill		r1 = [r3],r2
	(p2) ld8.fill		r1 = [r3],9
	(p3) ld8.fill		r1 = [r3],-9
	(p4) ld8.fill.nt1		r1 = [r3]
	(p5) ld8.fill.nt1		r1 = [r3],r2
	(p6) ld8.fill.nt1		r1 = [r3],9
	(p7) ld8.fill.nt1		r1 = [r3],-9
	(p8) ld8.fill.nta		r1 = [r3]
	(p9) ld8.fill.nta		r1 = [r3],r2
	(p10) ld8.fill.nta		r1 = [r3],9
	(p11) ld8.fill.nta		r1 = [r3],-9
	nop 1

	(p0) ldfs		f10 = [r3]
	(p1) ldfs		f10 = [r3],r2
	(p2) ldfs		f10 = [r3],9
	(p3) ldfs		f10 = [r3],-9
	(p4) ldfs.nt1		f10 = [r3]
	(p5) ldfs.nt1		f10 = [r3],r2
	(p6) ldfs.nt1		f10 = [r3],9
	(p7) ldfs.nt1		f10 = [r3],-9
	(p8) ldfs.nta		f10 = [r3]
	(p9) ldfs.nta		f10 = [r3],r2
	(p10) ldfs.nta		f10 = [r3],9
	(p11) ldfs.nta		f10 = [r3],-9
	(p20) ldfs.s		f10 = [r3]
	(p21) ldfs.s		f10 = [r3],r2
	(p22) ldfs.s		f10 = [r3],9
	(p23) ldfs.s		f10 = [r3],-9
	(p24) ldfs.s.nt1		f10 = [r3]
	(p25) ldfs.s.nt1		f10 = [r3],r2
	(p26) ldfs.s.nt1		f10 = [r3],9
	(p27) ldfs.s.nt1		f10 = [r3],-9
	(p28) ldfs.s.nta		f10 = [r3]
	(p29) ldfs.s.nta		f10 = [r3],r2
	(p30) ldfs.s.nta		f10 = [r3],9
	(p31) ldfs.s.nta		f10 = [r3],-9
	(p40) ldfs.a		f10 = [r3]
	(p41) ldfs.a		f10 = [r3],r2
	(p42) ldfs.a		f10 = [r3],9
	(p43) ldfs.a		f10 = [r3],-9
	(p44) ldfs.a.nt1		f10 = [r3]
	(p45) ldfs.a.nt1		f10 = [r3],r2
	(p46) ldfs.a.nt1		f10 = [r3],9
	(p47) ldfs.a.nt1		f10 = [r3],-9
	(p48) ldfs.a.nta		f10 = [r3]
	(p49) ldfs.a.nta		f10 = [r3],r2
	(p50) ldfs.a.nta		f10 = [r3],9
	(p51) ldfs.a.nta		f10 = [r3],-9
	(p20) ldfs.sa		f10 = [r3]
	(p21) ldfs.sa		f10 = [r3],r2
	(p22) ldfs.sa		f10 = [r3],9
	(p23) ldfs.sa		f10 = [r3],-9
	(p24) ldfs.sa.nt1		f10 = [r3]
	(p25) ldfs.sa.nt1		f10 = [r3],r2
	(p26) ldfs.sa.nt1		f10 = [r3],9
	(p27) ldfs.sa.nt1		f10 = [r3],-9
	(p28) ldfs.sa.nta		f10 = [r3]
	(p29) ldfs.sa.nta		f10 = [r3],r2
	(p30) ldfs.sa.nta		f10 = [r3],9
	(p31) ldfs.sa.nta		f10 = [r3],-9
	(p40) ldfs.c.nc		f10 = [r3]
	(p41) ldfs.c.nc		f10 = [r3],r2
	(p42) ldfs.c.nc		f10 = [r3],9
	(p43) ldfs.c.nc		f10 = [r3],-9
	(p44) ldfs.c.nc.nt1		f10 = [r3]
	(p45) ldfs.c.nc.nt1		f10 = [r3],r2
	(p46) ldfs.c.nc.nt1		f10 = [r3],9
	(p47) ldfs.c.nc.nt1		f10 = [r3],-9
	(p48) ldfs.c.nc.nta		f10 = [r3]
	(p49) ldfs.c.nc.nta		f10 = [r3],r2
	(p50) ldfs.c.nc.nta		f10 = [r3],9
	(p51) ldfs.c.nc.nta		f10 = [r3],-9
	(p20) ldfs.c.clr		f10 = [r3]
	(p21) ldfs.c.clr		f10 = [r3],r2
	(p22) ldfs.c.clr		f10 = [r3],9
	(p23) ldfs.c.clr		f10 = [r3],-9
	(p24) ldfs.c.clr.nt1		f10 = [r3]
	(p25) ldfs.c.clr.nt1		f10 = [r3],r2
	(p26) ldfs.c.clr.nt1		f10 = [r3],9
	(p27) ldfs.c.clr.nt1		f10 = [r3],-9
	(p28) ldfs.c.clr.nta		f10 = [r3]
	(p29) ldfs.c.clr.nta		f10 = [r3],r2
	(p30) ldfs.c.clr.nta		f10 = [r3],9
	(p31) ldfs.c.clr.nta		f10 = [r3],-9
	(p0) ldfd		f10 = [r3]
	(p1) ldfd		f10 = [r3],r2
	(p2) ldfd		f10 = [r3],9
	(p3) ldfd		f10 = [r3],-9
	(p4) ldfd.nt1		f10 = [r3]
	(p5) ldfd.nt1		f10 = [r3],r2
	(p6) ldfd.nt1		f10 = [r3],9
	(p7) ldfd.nt1		f10 = [r3],-9
	(p8) ldfd.nta		f10 = [r3]
	(p9) ldfd.nta		f10 = [r3],r2
	(p10) ldfd.nta		f10 = [r3],9
	(p11) ldfd.nta		f10 = [r3],-9
	(p20) ldfd.s		f10 = [r3]
	(p21) ldfd.s		f10 = [r3],r2
	(p22) ldfd.s		f10 = [r3],9
	(p23) ldfd.s		f10 = [r3],-9
	(p24) ldfd.s.nt1		f10 = [r3]
	(p25) ldfd.s.nt1		f10 = [r3],r2
	(p26) ldfd.s.nt1		f10 = [r3],9
	(p27) ldfd.s.nt1		f10 = [r3],-9
	(p28) ldfd.s.nta		f10 = [r3]
	(p29) ldfd.s.nta		f10 = [r3],r2
	(p30) ldfd.s.nta		f10 = [r3],9
	(p31) ldfd.s.nta		f10 = [r3],-9
	(p40) ldfd.a		f10 = [r3]
	(p41) ldfd.a		f10 = [r3],r2
	(p42) ldfd.a		f10 = [r3],9
	(p43) ldfd.a		f10 = [r3],-9
	(p44) ldfd.a.nt1		f10 = [r3]
	(p45) ldfd.a.nt1		f10 = [r3],r2
	(p46) ldfd.a.nt1		f10 = [r3],9
	(p47) ldfd.a.nt1		f10 = [r3],-9
	(p48) ldfd.a.nta		f10 = [r3]
	(p49) ldfd.a.nta		f10 = [r3],r2
	(p50) ldfd.a.nta		f10 = [r3],9
	(p51) ldfd.a.nta		f10 = [r3],-9
	(p20) ldfd.sa		f10 = [r3]
	(p21) ldfd.sa		f10 = [r3],r2
	(p22) ldfd.sa		f10 = [r3],9
	(p23) ldfd.sa		f10 = [r3],-9
	(p24) ldfd.sa.nt1		f10 = [r3]
	(p25) ldfd.sa.nt1		f10 = [r3],r2
	(p26) ldfd.sa.nt1		f10 = [r3],9
	(p27) ldfd.sa.nt1		f10 = [r3],-9
	(p28) ldfd.sa.nta		f10 = [r3]
	(p29) ldfd.sa.nta		f10 = [r3],r2
	(p30) ldfd.sa.nta		f10 = [r3],9
	(p31) ldfd.sa.nta		f10 = [r3],-9
	(p40) ldfd.c.nc		f10 = [r3]
	(p41) ldfd.c.nc		f10 = [r3],r2
	(p42) ldfd.c.nc		f10 = [r3],9
	(p43) ldfd.c.nc		f10 = [r3],-9
	(p44) ldfd.c.nc.nt1		f10 = [r3]
	(p45) ldfd.c.nc.nt1		f10 = [r3],r2
	(p46) ldfd.c.nc.nt1		f10 = [r3],9
	(p47) ldfd.c.nc.nt1		f10 = [r3],-9
	(p48) ldfd.c.nc.nta		f10 = [r3]
	(p49) ldfd.c.nc.nta		f10 = [r3],r2
	(p50) ldfd.c.nc.nta		f10 = [r3],9
	(p51) ldfd.c.nc.nta		f10 = [r3],-9
	(p20) ldfd.c.clr		f10 = [r3]
	(p21) ldfd.c.clr		f10 = [r3],r2
	(p22) ldfd.c.clr		f10 = [r3],9
	(p23) ldfd.c.clr		f10 = [r3],-9
	(p24) ldfd.c.clr.nt1		f10 = [r3]
	(p25) ldfd.c.clr.nt1		f10 = [r3],r2
	(p26) ldfd.c.clr.nt1		f10 = [r3],9
	(p27) ldfd.c.clr.nt1		f10 = [r3],-9
	(p28) ldfd.c.clr.nta		f10 = [r3]
	(p29) ldfd.c.clr.nta		f10 = [r3],r2
	(p30) ldfd.c.clr.nta		f10 = [r3],9
	(p31) ldfd.c.clr.nta		f10 = [r3],-9
	(p0) ldfe		f10 = [r3]
	(p1) ldfe		f10 = [r3],r2
	(p2) ldfe		f10 = [r3],9
	(p3) ldfe		f10 = [r3],-9
	(p4) ldfe.nt1		f10 = [r3]
	(p5) ldfe.nt1		f10 = [r3],r2
	(p6) ldfe.nt1		f10 = [r3],9
	(p7) ldfe.nt1		f10 = [r3],-9
	(p8) ldfe.nta		f10 = [r3]
	(p9) ldfe.nta		f10 = [r3],r2
	(p10) ldfe.nta		f10 = [r3],9
	(p11) ldfe.nta		f10 = [r3],-9
	(p20) ldfe.s		f10 = [r3]
	(p21) ldfe.s		f10 = [r3],r2
	(p22) ldfe.s		f10 = [r3],9
	(p23) ldfe.s		f10 = [r3],-9
	(p24) ldfe.s.nt1		f10 = [r3]
	(p25) ldfe.s.nt1		f10 = [r3],r2
	(p26) ldfe.s.nt1		f10 = [r3],9
	(p27) ldfe.s.nt1		f10 = [r3],-9
	(p28) ldfe.s.nta		f10 = [r3]
	(p29) ldfe.s.nta		f10 = [r3],r2
	(p30) ldfe.s.nta		f10 = [r3],9
	(p31) ldfe.s.nta		f10 = [r3],-9
	(p40) ldfe.a		f10 = [r3]
	(p41) ldfe.a		f10 = [r3],r2
	(p42) ldfe.a		f10 = [r3],9
	(p43) ldfe.a		f10 = [r3],-9
	(p44) ldfe.a.nt1		f10 = [r3]
	(p45) ldfe.a.nt1		f10 = [r3],r2
	(p46) ldfe.a.nt1		f10 = [r3],9
	(p47) ldfe.a.nt1		f10 = [r3],-9
	(p48) ldfe.a.nta		f10 = [r3]
	(p49) ldfe.a.nta		f10 = [r3],r2
	(p50) ldfe.a.nta		f10 = [r3],9
	(p51) ldfe.a.nta		f10 = [r3],-9
	(p20) ldfe.sa		f10 = [r3]
	(p21) ldfe.sa		f10 = [r3],r2
	(p22) ldfe.sa		f10 = [r3],9
	(p23) ldfe.sa		f10 = [r3],-9
	(p24) ldfe.sa.nt1		f10 = [r3]
	(p25) ldfe.sa.nt1		f10 = [r3],r2
	(p26) ldfe.sa.nt1		f10 = [r3],9
	(p27) ldfe.sa.nt1		f10 = [r3],-9
	(p28) ldfe.sa.nta		f10 = [r3]
	(p29) ldfe.sa.nta		f10 = [r3],r2
	(p30) ldfe.sa.nta		f10 = [r3],9
	(p31) ldfe.sa.nta		f10 = [r3],-9
	(p40) ldfe.c.nc		f10 = [r3]
	(p41) ldfe.c.nc		f10 = [r3],r2
	(p42) ldfe.c.nc		f10 = [r3],9
	(p43) ldfe.c.nc		f10 = [r3],-9
	(p44) ldfe.c.nc.nt1		f10 = [r3]
	(p45) ldfe.c.nc.nt1		f10 = [r3],r2
	(p46) ldfe.c.nc.nt1		f10 = [r3],9
	(p47) ldfe.c.nc.nt1		f10 = [r3],-9
	(p48) ldfe.c.nc.nta		f10 = [r3]
	(p49) ldfe.c.nc.nta		f10 = [r3],r2
	(p50) ldfe.c.nc.nta		f10 = [r3],9
	(p51) ldfe.c.nc.nta		f10 = [r3],-9
	(p20) ldfe.c.clr		f10 = [r3]
	(p21) ldfe.c.clr		f10 = [r3],r2
	(p22) ldfe.c.clr		f10 = [r3],9
	(p23) ldfe.c.clr		f10 = [r3],-9
	(p24) ldfe.c.clr.nt1		f10 = [r3]
	(p25) ldfe.c.clr.nt1		f10 = [r3],r2
	(p26) ldfe.c.clr.nt1		f10 = [r3],9
	(p27) ldfe.c.clr.nt1		f10 = [r3],-9
	(p28) ldfe.c.clr.nta		f10 = [r3]
	(p29) ldfe.c.clr.nta		f10 = [r3],r2
	(p30) ldfe.c.clr.nta		f10 = [r3],9
	(p31) ldfe.c.clr.nta		f10 = [r3],-9
	(p0) ldf8		f10 = [r3]
	(p1) ldf8		f10 = [r3],r2
	(p2) ldf8		f10 = [r3],9
	(p3) ldf8		f10 = [r3],-9
	(p4) ldf8.nt1		f10 = [r3]
	(p5) ldf8.nt1		f10 = [r3],r2
	(p6) ldf8.nt1		f10 = [r3],9
	(p7) ldf8.nt1		f10 = [r3],-9
	(p8) ldf8.nta		f10 = [r3]
	(p9) ldf8.nta		f10 = [r3],r2
	(p10) ldf8.nta		f10 = [r3],9
	(p11) ldf8.nta		f10 = [r3],-9
	(p20) ldf8.s		f10 = [r3]
	(p21) ldf8.s		f10 = [r3],r2
	(p22) ldf8.s		f10 = [r3],9
	(p23) ldf8.s		f10 = [r3],-9
	(p24) ldf8.s.nt1		f10 = [r3]
	(p25) ldf8.s.nt1		f10 = [r3],r2
	(p26) ldf8.s.nt1		f10 = [r3],9
	(p27) ldf8.s.nt1		f10 = [r3],-9
	(p28) ldf8.s.nta		f10 = [r3]
	(p29) ldf8.s.nta		f10 = [r3],r2
	(p30) ldf8.s.nta		f10 = [r3],9
	(p31) ldf8.s.nta		f10 = [r3],-9
	(p40) ldf8.a		f10 = [r3]
	(p41) ldf8.a		f10 = [r3],r2
	(p42) ldf8.a		f10 = [r3],9
	(p43) ldf8.a		f10 = [r3],-9
	(p44) ldf8.a.nt1		f10 = [r3]
	(p45) ldf8.a.nt1		f10 = [r3],r2
	(p46) ldf8.a.nt1		f10 = [r3],9
	(p47) ldf8.a.nt1		f10 = [r3],-9
	(p48) ldf8.a.nta		f10 = [r3]
	(p49) ldf8.a.nta		f10 = [r3],r2
	(p50) ldf8.a.nta		f10 = [r3],9
	(p51) ldf8.a.nta		f10 = [r3],-9
	(p20) ldf8.sa		f10 = [r3]
	(p21) ldf8.sa		f10 = [r3],r2
	(p22) ldf8.sa		f10 = [r3],9
	(p23) ldf8.sa		f10 = [r3],-9
	(p24) ldf8.sa.nt1		f10 = [r3]
	(p25) ldf8.sa.nt1		f10 = [r3],r2
	(p26) ldf8.sa.nt1		f10 = [r3],9
	(p27) ldf8.sa.nt1		f10 = [r3],-9
	(p28) ldf8.sa.nta		f10 = [r3]
	(p29) ldf8.sa.nta		f10 = [r3],r2
	(p30) ldf8.sa.nta		f10 = [r3],9
	(p31) ldf8.sa.nta		f10 = [r3],-9
	(p40) ldf8.c.nc		f10 = [r3]
	(p41) ldf8.c.nc		f10 = [r3],r2
	(p42) ldf8.c.nc		f10 = [r3],9
	(p43) ldf8.c.nc		f10 = [r3],-9
	(p44) ldf8.c.nc.nt1		f10 = [r3]
	(p45) ldf8.c.nc.nt1		f10 = [r3],r2
	(p46) ldf8.c.nc.nt1		f10 = [r3],9
	(p47) ldf8.c.nc.nt1		f10 = [r3],-9
	(p48) ldf8.c.nc.nta		f10 = [r3]
	(p49) ldf8.c.nc.nta		f10 = [r3],r2
	(p50) ldf8.c.nc.nta		f10 = [r3],9
	(p51) ldf8.c.nc.nta		f10 = [r3],-9
	(p20) ldf8.c.clr		f10 = [r3]
	(p21) ldf8.c.clr		f10 = [r3],r2
	(p22) ldf8.c.clr		f10 = [r3],9
	(p23) ldf8.c.clr		f10 = [r3],-9
	(p24) ldf8.c.clr.nt1		f10 = [r3]
	(p25) ldf8.c.clr.nt1		f10 = [r3],r2
	(p26) ldf8.c.clr.nt1		f10 = [r3],9
	(p27) ldf8.c.clr.nt1		f10 = [r3],-9
	(p28) ldf8.c.clr.nta		f10 = [r3]
	(p29) ldf8.c.clr.nta		f10 = [r3],r2
	(p30) ldf8.c.clr.nta		f10 = [r3],9
	(p31) ldf8.c.clr.nta		f10 = [r3],-9
	(p0) ldf.fill		f10 = [r3]
	(p1) ldf.fill		f10 = [r3],r2
	(p2) ldf.fill		f10 = [r3],9
	(p3) ldf.fill		f10 = [r3],-9
	(p4) ldf.fill.nt1		f10 = [r3]
	(p5) ldf.fill.nt1		f10 = [r3],r2
	(p6) ldf.fill.nt1		f10 = [r3],9
	(p7) ldf.fill.nt1		f10 = [r3],-9
	(p8) ldf.fill.nta		f10 = [r3]
	(p9) ldf.fill.nta		f10 = [r3],r2
	(p10) ldf.fill.nta		f10 = [r3],9
	(p11) ldf.fill.nta		f10 = [r3],-9
	nop 1

	(p0) ldfps		f10,f11 = [r3]
	(p2) ldfps		f10,f11 = [r3],8
	(p4) ldfps.nt1		f10,f11 = [r3]
	(p6) ldfps.nt1		f10,f11 = [r3],8
	(p8) ldfps.nta		f10,f11 = [r3]
	(p10) ldfps.nta		f10,f11 = [r3],8
	(p20) ldfps.s		f10,f11 = [r3]
	(p22) ldfps.s		f10,f11 = [r3],8
	(p24) ldfps.s.nt1		f10,f11 = [r3]
	(p26) ldfps.s.nt1		f10,f11 = [r3],8
	(p28) ldfps.s.nta		f10,f11 = [r3]
	(p30) ldfps.s.nta		f10,f11 = [r3],8
	(p40) ldfps.a		f10,f11 = [r3]
	(p42) ldfps.a		f10,f11 = [r3],8
	(p44) ldfps.a.nt1		f10,f11 = [r3]
	(p46) ldfps.a.nt1		f10,f11 = [r3],8
	(p48) ldfps.a.nta		f10,f11 = [r3]
	(p50) ldfps.a.nta		f10,f11 = [r3],8
	(p20) ldfps.sa		f10,f11 = [r3]
	(p22) ldfps.sa		f10,f11 = [r3],8
	(p24) ldfps.sa.nt1		f10,f11 = [r3]
	(p26) ldfps.sa.nt1		f10,f11 = [r3],8
	(p28) ldfps.sa.nta		f10,f11 = [r3]
	(p30) ldfps.sa.nta		f10,f11 = [r3],8
	(p40) ldfps.c.nc		f10,f11 = [r3]
	(p42) ldfps.c.nc		f10,f11 = [r3],8
	(p44) ldfps.c.nc.nt1		f10,f11 = [r3]
	(p46) ldfps.c.nc.nt1		f10,f11 = [r3],8
	(p48) ldfps.c.nc.nta		f10,f11 = [r3]
	(p50) ldfps.c.nc.nta		f10,f11 = [r3],8
	(p20) ldfps.c.clr		f10,f11 = [r3]
	(p22) ldfps.c.clr		f10,f11 = [r3],8
	(p24) ldfps.c.clr.nt1		f10,f11 = [r3]
	(p26) ldfps.c.clr.nt1		f10,f11 = [r3],8
	(p28) ldfps.c.clr.nta		f10,f11 = [r3]
	(p30) ldfps.c.clr.nta		f10,f11 = [r3],8
	(p0) ldfpd		f10,f11 = [r3]
	(p2) ldfpd		f10,f11 = [r3],16
	(p4) ldfpd.nt1		f10,f11 = [r3]
	(p6) ldfpd.nt1		f10,f11 = [r3],16
	(p16) ldfpd.nta		f10,f11 = [r3]
	(p10) ldfpd.nta		f10,f11 = [r3],16
	(p20) ldfpd.s		f10,f11 = [r3]
	(p22) ldfpd.s		f10,f11 = [r3],16
	(p24) ldfpd.s.nt1		f10,f11 = [r3]
	(p26) ldfpd.s.nt1		f10,f11 = [r3],16
	(p28) ldfpd.s.nta		f10,f11 = [r3]
	(p30) ldfpd.s.nta		f10,f11 = [r3],16
	(p40) ldfpd.a		f10,f11 = [r3]
	(p42) ldfpd.a		f10,f11 = [r3],16
	(p44) ldfpd.a.nt1		f10,f11 = [r3]
	(p46) ldfpd.a.nt1		f10,f11 = [r3],16
	(p48) ldfpd.a.nta		f10,f11 = [r3]
	(p50) ldfpd.a.nta		f10,f11 = [r3],16
	(p20) ldfpd.sa		f10,f11 = [r3]
	(p22) ldfpd.sa		f10,f11 = [r3],16
	(p24) ldfpd.sa.nt1		f10,f11 = [r3]
	(p26) ldfpd.sa.nt1		f10,f11 = [r3],16
	(p28) ldfpd.sa.nta		f10,f11 = [r3]
	(p30) ldfpd.sa.nta		f10,f11 = [r3],16
	(p40) ldfpd.c.nc		f10,f11 = [r3]
	(p42) ldfpd.c.nc		f10,f11 = [r3],16
	(p44) ldfpd.c.nc.nt1		f10,f11 = [r3]
	(p46) ldfpd.c.nc.nt1		f10,f11 = [r3],16
	(p48) ldfpd.c.nc.nta		f10,f11 = [r3]
	(p50) ldfpd.c.nc.nta		f10,f11 = [r3],16
	(p20) ldfpd.c.clr		f10,f11 = [r3]
	(p22) ldfpd.c.clr		f10,f11 = [r3],16
	(p24) ldfpd.c.clr.nt1		f10,f11 = [r3]
	(p26) ldfpd.c.clr.nt1		f10,f11 = [r3],16
	(p28) ldfpd.c.clr.nta		f10,f11 = [r3]
	(p30) ldfpd.c.clr.nta		f10,f11 = [r3],16
	(p0) ldfp8		f10,f11 = [r3]
	(p2) ldfp8		f10,f11 = [r3],16
	(p4) ldfp8.nt1		f10,f11 = [r3]
	(p6) ldfp8.nt1		f10,f11 = [r3],16
	(p16) ldfp8.nta		f10,f11 = [r3]
	(p10) ldfp8.nta		f10,f11 = [r3],16
	(p20) ldfp8.s		f10,f11 = [r3]
	(p22) ldfp8.s		f10,f11 = [r3],16
	(p24) ldfp8.s.nt1		f10,f11 = [r3]
	(p26) ldfp8.s.nt1		f10,f11 = [r3],16
	(p28) ldfp8.s.nta		f10,f11 = [r3]
	(p30) ldfp8.s.nta		f10,f11 = [r3],16
	(p40) ldfp8.a		f10,f11 = [r3]
	(p42) ldfp8.a		f10,f11 = [r3],16
	(p44) ldfp8.a.nt1		f10,f11 = [r3]
	(p46) ldfp8.a.nt1		f10,f11 = [r3],16
	(p48) ldfp8.a.nta		f10,f11 = [r3]
	(p50) ldfp8.a.nta		f10,f11 = [r3],16
	(p20) ldfp8.sa		f10,f11 = [r3]
	(p22) ldfp8.sa		f10,f11 = [r3],16
	(p24) ldfp8.sa.nt1		f10,f11 = [r3]
	(p26) ldfp8.sa.nt1		f10,f11 = [r3],16
	(p28) ldfp8.sa.nta		f10,f11 = [r3]
	(p30) ldfp8.sa.nta		f10,f11 = [r3],16
	(p40) ldfp8.c.nc		f10,f11 = [r3]
	(p42) ldfp8.c.nc		f10,f11 = [r3],16
	(p44) ldfp8.c.nc.nt1		f10,f11 = [r3]
	(p46) ldfp8.c.nc.nt1		f10,f11 = [r3],16
	(p48) ldfp8.c.nc.nta		f10,f11 = [r3]
	(p50) ldfp8.c.nc.nta		f10,f11 = [r3],16
	(p20) ldfp8.c.clr		f10,f11 = [r3]
	(p22) ldfp8.c.clr		f10,f11 = [r3],16
	(p24) ldfp8.c.clr.nt1		f10,f11 = [r3]
	(p26) ldfp8.c.clr.nt1		f10,f11 = [r3],16
	(p28) ldfp8.c.clr.nta		f10,f11 = [r3]
	(p30) ldfp8.c.clr.nta		f10,f11 = [r3],16
	nop 1

	(p0) lfetch		[r3]
	(p1) lfetch		[r3],r2
	(p2) lfetch		[r3],9
	(p3) lfetch		[r3],-9
	(p4) lfetch.nt1		[r3]
	(p5) lfetch.nt1		[r3],r2
	(p6) lfetch.nt1		[r3],9
	(p7) lfetch.nt1		[r3],-9
	(p8) lfetch.nt2		[r3]
	(p9) lfetch.nt2		[r3],r2
	(p10) lfetch.nt2	[r3],9
	(p11) lfetch.nt2	[r3],-9
	(p12) lfetch.nta	[r3]
	(p13) lfetch.nta	[r3],r2
	(p14) lfetch.nta	[r3],9
	(p15) lfetch.nta	[r3],-9
	(p0) lfetch.fault		[r3]
	(p1) lfetch.fault		[r3],r2
	(p2) lfetch.fault		[r3],9
	(p3) lfetch.fault		[r3],-9
	(p4) lfetch.fault.nt1		[r3]
	(p5) lfetch.fault.nt1		[r3],r2
	(p6) lfetch.fault.nt1		[r3],9
	(p7) lfetch.fault.nt1		[r3],-9
	(p8) lfetch.fault.nt2		[r3]
	(p9) lfetch.fault.nt2		[r3],r2
	(p10) lfetch.fault.nt2	[r3],9
	(p11) lfetch.fault.nt2	[r3],-9
	(p12) lfetch.fault.nta	[r3]
	(p13) lfetch.fault.nta	[r3],r2
	(p14) lfetch.fault.nta	[r3],9
	(p15) lfetch.fault.nta	[r3],-9
	(p0) lfetch.excl		[r3]
	(p1) lfetch.excl		[r3],r2
	(p2) lfetch.excl		[r3],9
	(p3) lfetch.excl		[r3],-9
	(p4) lfetch.excl.nt1		[r3]
	(p5) lfetch.excl.nt1		[r3],r2
	(p6) lfetch.excl.nt1		[r3],9
	(p7) lfetch.excl.nt1		[r3],-9
	(p8) lfetch.excl.nt2		[r3]
	(p9) lfetch.excl.nt2		[r3],r2
	(p10) lfetch.excl.nt2	[r3],9
	(p11) lfetch.excl.nt2	[r3],-9
	(p12) lfetch.excl.nta	[r3]
	(p13) lfetch.excl.nta	[r3],r2
	(p14) lfetch.excl.nta	[r3],9
	(p15) lfetch.excl.nta	[r3],-9
	(p0) lfetch.fault.excl		[r3]
	(p1) lfetch.fault.excl		[r3],r2
	(p2) lfetch.fault.excl		[r3],9
	(p3) lfetch.fault.excl		[r3],-9
	(p4) lfetch.fault.excl.nt1		[r3]
	(p5) lfetch.fault.excl.nt1		[r3],r2
	(p6) lfetch.fault.excl.nt1		[r3],9
	(p7) lfetch.fault.excl.nt1		[r3],-9
	(p8) lfetch.fault.excl.nt2		[r3]
	(p9) lfetch.fault.excl.nt2		[r3],r2
	(p10) lfetch.fault.excl.nt2	[r3],9
	(p11) lfetch.fault.excl.nt2	[r3],-9
	(p12) lfetch.fault.excl.nta	[r3]
	(p13) lfetch.fault.excl.nta	[r3],r2
	(p14) lfetch.fault.excl.nta	[r3],9
	(p15) lfetch.fault.excl.nta	[r3],-9
	nop 1

	loadrs
	nop 1

	//// ******** M *********
	
	(p16) mf
	(p17) mf.a
	nop 1

	(p18) mix1.l	r1 = r2, r3
	(p19) mix2.l	r1 = r2, r3
	(p20) mix4.l	r1 = r2, r3
	(p21) mix1.r	r1 = r2, r3
	(p22) mix2.r	r1 = r2, r3
	(p23) mix4.r	r1 = r2, r3
	nop 1

	(p24) mov r1 = ar3
	(p25) mov ar3 = r2
	(p26) mov ar3 = 15
	(p27) mov.i r1 = ar3
	(p28) mov.i ar3 = r2
	(p29) mov.i ar3 = 15
	(p30) mov.m r1 = ar3
	(p31) mov.m ar3 = r2
	(p32) mov.m ar3 = 15
	nop 1

.blabel:
	(p33) mov r1 = b1
	(p34) mov b1 = r2
	(p35) mov		b1 = r2, .blabel
	(p37) mov.imp		b1 = r2, .blabel
	(p39) mov.sptk		b1 = r2, .blabel
	(p41) mov.sptk.imp 	b1 = r2, .blabel
	(p43) mov.dptk		b1 = r2, .blabel
	(p45) mov.dptk.imp 	b1 = r2, .blabel
	(p35) mov.ret		b1 = r2, .blabel
	(p37) mov.ret.imp		b1 = r2, .blabel
	(p39) mov.ret.sptk		b1 = r2, .blabel
	(p41) mov.ret.sptk.imp 	b1 = r2, .blabel
	(p43) mov.ret.dptk		b1 = r2, .blabel
	(p45) mov.ret.dptk.imp 	b1 = r2, .blabel
	nop 1

	(p46) mov r1 = cr3
	(p47) mov cr3 = r2
	nop 1

	(p48) mov f11 = f3
	nop 1

	(p49) mov r1 = r3
	nop 1

	(p50) mov r1 = 3000
	nop 1

	(p51) mov r1 = cpuid[r3]
	nop 0
	// (p52) mov cpuid[r3] = r2  // because you really cannot encode this
	(p53) mov r1 = dbr[r3]
	(p54) mov dbr[r3] = r2
	(p55) mov r1 = ibr[r3]
	(p56) mov ibr[r3] = r2
	(p57) mov r1 = pkr[r3]
	(p58) mov pkr[r3] = r2
	(p59) mov r1 = pmc[r3]
	(p60) mov pmc[r3] = r2
	(p61) mov r1 = pmd[r3]
	(p62) mov pmd[r3] = r2
	(p63) mov r1 = rr[r3]
	(p0) mov rr[r3] = r2
	nop 1

	(p1) mov r2 = ip
	nop 1

	(p2) mov r1 = pr
	(p3) mov pr = r2, 34
	(p4) mov pr.rot = 0x3450000
	nop 1

	(p5) mov r1 = psr
	(p6) mov psr.l = r2
	nop 1

	(p7) mov r1 = psr.um
	(p8) mov psr.um = r2
	nop 1

	(p9) movl r1 = 0xfedcba9876543210
	nop 1

	(p10) mux1	r1 = r2, @rev
	(p11) mux1	r1 = r2, @mix
	(p12) mux1	r1 = r2, @shuf
	(p13) mux1	r1 = r2, @alt
	(p14) mux1	r1 = r2, @brcst
	(p15) mux2	r1 = r2, 0x96
	nop 1

	//// ******** N *********

	(p16) nop 3
	(p17) nop.i 4
	(p18) nop.b 5
	(p19) nop.m 6
	(p20) nop.f 0x100007
	(p21) nop.x 0x3edcba9876543210
	nop 1

	//// ******** O *********

	(p22) or r1 = r2, r3
	(p23) or r1 = 7, r3
	(p24) or r1 = -7, r3
	nop 1

	//// ******** P *********

	(p25) pack2.sss	r1 = r2, r3
	(p26) pack2.uss	r1 = r2, r3
	(p27) pack4.sss	r1 = r2, r3
	nop 1

	(p28) padd1	r1 = r2, r3
	(p29) padd1.sss	r1 = r2, r3
	(p30) padd1.uus	r1 = r2, r3
	(p31) padd1.uuu	r1 = r2, r3
	(p32) padd2	r1 = r2, r3
	(p33) padd2.sss	r1 = r2, r3
	(p34) padd2.uus	r1 = r2, r3
	(p35) padd2.uuu	r1 = r2, r3
	(p36) padd4	r1 = r2, r3
	nop 1

	(p37) pavg1	r1 = r2, r3
	(p38) pavg1.raz	r1 = r2, r3
	(p39) pavg2	r1 = r2, r3
	(p40) pavg2.raz	r1 = r2, r3
	nop 1

	(p41) pavgsub1	r1 = r2, r3
	(p42) pavgsub2	r1 = r2, r3
	nop 1

	(p43) pcmp1.eq	r1 = r2, r3
	(p44) pcmp1.gt	r1 = r2, r3
	(p45) pcmp2.eq	r1 = r2, r3
	(p46) pcmp2.gt	r1 = r2, r3
	(p47) pcmp4.eq	r1 = r2, r3
	(p48) pcmp4.gt	r1 = r2, r3
	nop 1

	(p49) pmax1.u	r1 = r2, r3
	(p50) pmax2	r1 = r2, r3
	nop 1

	(p51) pmin1.u	r1 = r2, r3
	(p52) pmin2	r1 = r2, r3
	nop 1

	(p53) pmpy2.r	r1 = r2, r3
	(p54) pmpy2.l	r1 = r2, r3
	nop 1

	(p55) pmpyshr2	r1 = r2, r3, 0
	(p56) pmpyshr2	r1 = r2, r3, 7
	(p57) pmpyshr2	r1 = r2, r3, 15
	(p58) pmpyshr2	r1 = r2, r3, 16
	(p59) pmpyshr2.u	r1 = r2, r3, 0
	(p60) pmpyshr2.u	r1 = r2, r3, 7
	(p61) pmpyshr2.u	r1 = r2, r3, 15
	(p62) pmpyshr2.u	r1 = r2, r3, 16
	nop 1

	(p63) popcnt	r1 = r3
	nop 1

	(p0) probe.r	r1 = r3, r2
	(p1) probe.w	r1 = r3, r2
	(p2) probe.r	r1 = r3, 2
	(p3) probe.w	r1 = r3, 2
	(p4) probe.r.fault	r3, 2
	(p5) probe.w.fault	r3, 2
	(p6) probe.rw.fault	r3, 2
	nop 1

	(p7) psad1	r1 = r2, r3
	nop 1

	(p8) pshl2	r1 = r2, r3
	(p9) pshl2	r1 = r2, 5
	(p10) pshl4	r1 = r2, r3
	(p11) pshl4	r1 = r2, 5
	nop 1

	(p12) pshladd2	r1 = r2, 2, r3
	nop 1

	(p13) pshr2	r1 = r3, r2
	(p14) pshr2	r1 = r3, 5
	(p15) pshr2.u	r1 = r3, r2
	(p16) pshr2.u	r1 = r3, 5
	(p17) pshr4	r1 = r3, r2
	(p18) pshr4	r1 = r3, 5
	(p19) pshr4.u	r1 = r3, r2
	(p20) pshr4.u	r1 = r3, 5
	nop 1

	(p21) pshradd2	r1 = r2, 2, r3
	nop 1

	(p22) psub1	r1 = r2, r3
	(p23) psub1.sss	r1 = r2, r3
	(p24) psub1.uus	r1 = r2, r3
	(p25) psub1.uuu	r1 = r2, r3
	(p26) psub2	r1 = r2, r3
	(p27) psub2.sss	r1 = r2, r3
	(p28) psub2.uus	r1 = r2, r3
	(p29) psub2.uuu	r1 = r2, r3
	(p30) psub4	r1 = r2, r3
	nop 1

	(p31) ptc.e	r3
	nop 1

	(p32) ptc.g	r3, r2
	(p33) ptc.ga	r3, r2
	nop 1

	(p34) ptc.l	r3, r2
	nop 1

	(p35) ptr.d	r3, r2
	(p36) ptr.i	r3, r2
	nop 1

	//// ******** Q *********

	//// ******** R *********

	rfi
	nop 1

	(p37) rsm 0x800001
	nop 1

	(p38) rum 0x800001
	nop 1

	//// ******** S *********

	(p39) setf.s f10 = r2
	(p40) setf.d f10 = r2
	(p41) setf.exp f10 = r2
	(p42) setf.sig f10 = r2
	nop 1

	(p43) shl	r1 = r2, r3
	(p44) shl	r1 = r2, 6
	nop 1

	(p45) shladd	r1 = r2, 2, r3
	nop 1

	(p46) shladdp4	r1 = r2, 2, r3
	nop 1

	(p47) shr	r1 = r3, r2
	(p48) shr.u	r1 = r3, r2
	(p49) shr	r1 = r3, 6
	(p50) shr.u	r1 = r3, 6
	nop 1

	(p51) shrp	r1 = r2, r3, 6
	nop 1
	
	(p52) srlz.i
	(p53) srlz.d
	nop 1

	(p54) ssm 0x800001
	nop 1

	(p0) st1 		[r3] = r2
	(p1) st1 		[r3] = r2, 9
	(p2) st1 		[r3] = r2, -9
	(p3) st1.nta		[r3] = r2
	(p4) st1.nta		[r3] = r2, 9
	(p5) st1.nta		[r3] = r2, -9
	(p6) st1.rel 		[r3] = r2
	(p7) st1.rel 		[r3] = r2, 9
	(p8) st1.rel 		[r3] = r2, -9
	(p9) st1.rel.nta 	[r3] = r2
	(p10) st1.rel.nta	[r3] = r2, 9
	(p11) st1.rel.nta	[r3] = r2, -9
	(p0) st2 		[r3] = r2
	(p1) st2 		[r3] = r2, 9
	(p2) st2 		[r3] = r2, -9
	(p3) st2.nta		[r3] = r2
	(p4) st2.nta		[r3] = r2, 9
	(p5) st2.nta		[r3] = r2, -9
	(p6) st2.rel 		[r3] = r2
	(p7) st2.rel 		[r3] = r2, 9
	(p8) st2.rel 		[r3] = r2, -9
	(p9) st2.rel.nta 	[r3] = r2
	(p10) st2.rel.nta	[r3] = r2, 9
	(p11) st2.rel.nta	[r3] = r2, -9
	(p0) st4 		[r3] = r2
	(p1) st4 		[r3] = r2, 9
	(p2) st4 		[r3] = r2, -9
	(p3) st4.nta		[r3] = r2
	(p4) st4.nta		[r3] = r2, 9
	(p5) st4.nta		[r3] = r2, -9
	(p6) st4.rel 		[r3] = r2
	(p7) st4.rel 		[r3] = r2, 9
	(p8) st4.rel 		[r3] = r2, -9
	(p9) st4.rel.nta 	[r3] = r2
	(p10) st4.rel.nta	[r3] = r2, 9
	(p11) st4.rel.nta	[r3] = r2, -9
	(p0) st8 		[r3] = r2
	(p1) st8 		[r3] = r2, 9
	(p2) st8 		[r3] = r2, -9
	(p3) st8.nta		[r3] = r2
	(p4) st8.nta		[r3] = r2, 9
	(p5) st8.nta		[r3] = r2, -9
	(p6) st8.rel 		[r3] = r2
	(p7) st8.rel 		[r3] = r2, 9
	(p8) st8.rel 		[r3] = r2, -9
	(p9) st8.rel.nta 	[r3] = r2
	(p10) st8.rel.nta	[r3] = r2, 9
	(p11) st8.rel.nta	[r3] = r2, -9
	(p0) st8.spill 		[r3] = r2
	(p1) st8.spill 		[r3] = r2, 9
	(p2) st8.spill 		[r3] = r2, -9
	(p3) st8.spill.nta		[r3] = r2
	(p4) st8.spill.nta		[r3] = r2, 9
	(p5) st8.spill.nta		[r3] = r2, -9
	nop 1

	(p0) stfs [r3] = f2
	(p1) stfs [r3] = f2, 9
	(p2) stfs [r3] = f2, -9
	(p3) stfs.nta [r3] = f2
	(p4) stfs.nta [r3] = f2, 9
	(p5) stfs.nta [r3] = f2, -9
	(p0) stfd [r3] = f2
	(p1) stfd [r3] = f2, 9
	(p2) stfd [r3] = f2, -9
	(p3) stfd.nta [r3] = f2
	(p4) stfd.nta [r3] = f2, 9
	(p5) stfd.nta [r3] = f2, -9
	(p0) stfe [r3] = f2
	(p1) stfe [r3] = f2, 9
	(p2) stfe [r3] = f2, -9
	(p3) stfe.nta [r3] = f2
	(p4) stfe.nta [r3] = f2, 9
	(p5) stfe.nta [r3] = f2, -9
	(p0) stf8 [r3] = f2
	(p1) stf8 [r3] = f2, 9
	(p2) stf8 [r3] = f2, -9
	(p3) stf8.nta [r3] = f2
	(p4) stf8.nta [r3] = f2, 9
	(p5) stf8.nta [r3] = f2, -9
	(p0) stf.spill [r3] = f2
	(p1) stf.spill [r3] = f2, 9
	(p2) stf.spill [r3] = f2, -9
	(p3) stf.spill.nta [r3] = f2
	(p4) stf.spill.nta [r3] = f2, 9
	(p5) stf.spill.nta [r3] = f2, -9
	nop 1

	(p6) sub	r1 = r2, r3
	(p7) sub	r1 = r2, r3, 1
	(p8) sub	r1 = 8, r3
	(p9) sub	r1 = -8, r3
	nop 1

	(p10) ssm	0x800001
	nop 1
	
	(p11) sxt1	r1 = r2
	(p12) sxt2	r1 = r2
	(p13) sxt4	r1 = r2
	nop 1

	(p14) sync.i
	nop 1

	//// ******** T *********
	
	(p15) tak	r1 = r3
	nop 1

	nop 2 // align
	(p16) tbit.z		p1, p2 = r3, 6
	(p17) tbit.nz		p1, p2 = r3, 6
	(p18) tbit.z.unc	p1, p2 = r3, 6
	(p19) tbit.nz.unc	p1, p2 = r3, 6
	(p20) tbit.z.and	p1, p2 = r3, 6
	(p21) tbit.nz.and	p1, p2 = r3, 6
	(p22) tbit.z.or		p1, p2 = r3, 6
	(p23) tbit.nz.or	p1, p2 = r3, 6
	(p24) tbit.z.or.andcm	p1, p2 = r3, 6
	(p25) tbit.nz.or.andcm	p1, p2 = r3, 6
	(p26) tbit.z.andcm	p1, p2 = r3, 6
	(p27) tbit.nz.andcm	p1, p2 = r3, 6
	(p28) tbit.z.orcm	p1, p2 = r3, 6
	(p29) tbit.nz.orcm	p1, p2 = r3, 6
	(p30) tbit.z.and.orcm	p1, p2 = r3, 6
	(p31) tbit.nz.and.orcm	p1, p2 = r3, 6
	nop 1

	(p32) thash	r1 = r3
	nop 1

	(p16) tnat.z		p1, p2 = r3
	(p17) tnat.nz		p1, p2 = r3
	(p18) tnat.z.unc	p1, p2 = r3
	(p19) tnat.nz.unc	p1, p2 = r3
	(p20) tnat.z.and	p1, p2 = r3
	(p21) tnat.nz.and	p1, p2 = r3
	(p22) tnat.z.or		p1, p2 = r3
	(p23) tnat.nz.or	p1, p2 = r3
	(p24) tnat.z.or.andcm	p1, p2 = r3
	(p25) tnat.nz.or.andcm	p1, p2 = r3
	(p26) tnat.z.andcm	p1, p2 = r3
	(p27) tnat.nz.andcm	p1, p2 = r3
	(p28) tnat.z.orcm	p1, p2 = r3
	(p29) tnat.nz.orcm	p1, p2 = r3
	(p30) tnat.z.and.orcm	p1, p2 = r3
	(p31) tnat.nz.and.orcm	p1, p2 = r3
	nop 1

	(p32) tpa	r1 = r3
	nop 1

	(p33) ttag	r1 = r3
	nop 1

	//// ******** U *********

	(p34) unpack1.h		r1 = r2, r3
	(p35) unpack2.h		r1 = r2, r3
	(p36) unpack4.h		r1 = r2, r3
	(p37) unpack1.l		r1 = r2, r3
	(p38) unpack2.l		r1 = r2, r3
	(p39) unpack4.l		r1 = r2, r3
	nop 1

	//// ******** V *********
	//// ******** W *********
	//// ******** X *********

	(p40) xchg1		r1 = [r3], r2
	(p41) xchg1.nt1		r1 = [r3], r2
	(p42) xchg1.nta		r1 = [r3], r2
	(p43) xchg2		r1 = [r3], r2
	(p44) xchg2.nt1		r1 = [r3], r2
	(p45) xchg2.nta		r1 = [r3], r2
	(p46) xchg4		r1 = [r3], r2
	(p47) xchg4.nt1		r1 = [r3], r2
	(p48) xchg4.nta		r1 = [r3], r2
	(p49) xchg8		r1 = [r3], r2
	(p50) xchg8.nt1		r1 = [r3], r2
	(p51) xchg8.nta		r1 = [r3], r2
	nop 1

	(p52) xma.l	f10 = f3, f4, f2
	(p53) xma.lu	f10 = f3, f4, f2
	(p54) xma.h	f10 = f3, f4, f2
	(p55) xma.hu	f10 = f3, f4, f2
	nop 1

	(p56) xmpy.l	f10 = f3, f4
	(p57) xmpy.lu	f10 = f3, f4
	(p58) xmpy.h	f10 = f3, f4
	(p59) xmpy.hu	f10 = f3, f4
	nop 1

	(p60) xor	r1 = r2, r3
	(p61) xor	r1 = 8, r3
	(p62) xor	r1 = -8, r3
	nop 1
	
	//// ******** Y *********
	//// ******** Z *********

	(p63) zxt1	r1 = r3
	(p0) zxt2	r1 = r3
	(p1) zxt4	r1 = r3

	.endp _start#
