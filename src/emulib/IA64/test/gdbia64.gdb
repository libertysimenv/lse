define starttarg 
  target remote 127.0.0.1:$arg0 
end
document starttarg 
Attach to an emulator on localhost.  The port number must be supplied as an
argument.
end

define showreg 
set $r0++
flushregs
end
document showreg
Make the emulator dump registers, minus FP.
end

define showallreg
set $f0++
flushregs
end
document showallreg
Make the emulator dump registers, including FP.
end

set stop-on-solib-events 1

