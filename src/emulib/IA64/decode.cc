/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Decode function for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file holds the front end functions for the emulator.  They include
 * fetching and decoding the instruction.
 *
 * TO DO: 
 *        - set up the host endianness properly in the configure.in file
 *          so we set IA64_HOST_LE
 *        - M28: ptc.e will have VM destinations
 *        - M41, M42: will have RR/VM destinations
 *        - M45: ptc/ptr will have VM destinations
 *        - loads/probes have VM sources
 */

#include <stdio.h>
#include <stdlib.h>
#include "LSE_IA64.h"

namespace LSE_IA64 {

#define bits64 (uint64_t)UINT64_C(0xFfffFfffFfffFfff)
#define all_cfm_bits ((INT64_C(1)<<38)-1)
#define all_gr_bits bits64,1
#define all_br_bits bits64
#define all_ar_bits bits64
#define all_fr_bits bits64,0x3ffff

#define set_src_regop2(ii,i,t,a,m0,m1) { \
    ii->operand_src[i].uses.reg.bits[0] = m0; \
    ii->operand_src[i].uses.reg.bits[1] = m1; \
    ii->operand_src[i].spaceid = LSE_emu_spaceid_ ## t; \
    ii->operand_src[i].spaceaddr.t = a; \
  }
#define set_src_regop1(ii,i,t,a,m0) { \
    ii->operand_src[i].uses.reg.bits[0] = m0; \
    ii->operand_src[i].spaceid = LSE_emu_spaceid_ ## t; \
    ii->operand_src[i].spaceaddr.t = a; \
  }
#define merge_src_regop2(ii,i,t,a,m0,m1) { \
    ii->operand_src[i].uses.reg.bits[0] |= m0; \
    ii->operand_src[i].uses.reg.bits[1] |= m1; \
  }
#define merge_src_regop1(ii,i,t,a,m0) { \
    ii->operand_src[i].uses.reg.bits[0] |= m0; \
  }
#define clear_src_regop_bitsused(ii,i) { \
    ii->operand_src[i].uses.reg.bits[0] = 0; \
    ii->operand_src[i].uses.reg.bits[1] = 0; \
  }
#define set_src_memop(ii,i,t,m0,s) { \
    ii->operand_src[i].uses.mem.flags = m0; \
    ii->operand_src[i].uses.mem.size = s; \
    ii->operand_src[i].spaceid = LSE_emu_spaceid_ ## t; \
  }

/* allow one parameter to become 2 */
#define set_src_regop(ii,i,t,a,d) set_src_regop2(ii,i,t,a,d)
#define merge_src_regop(ii,i,t,a,d) merge_src_regop2(ii,i,t,a,d)

#define set_src_regop_imm(ii,i) { \
    ii->operand_src[i].spaceid = (LSE_emu_spaceid_t)0;	\
    ii->operand_src[i].spaceaddr.LSE = 1;	\
  }
#define set_dest_regop2(ii,i,t,a,m0,m1) { \
    ii->operand_dest[i].uses.reg.bits[0] = m0; \
    ii->operand_dest[i].uses.reg.bits[1] = m1; \
    ii->operand_dest[i].spaceid = LSE_emu_spaceid_ ## t; \
    ii->operand_dest[i].spaceaddr.t = a; \
  }
#define set_dest_regop1(ii,i,t,a,m0) { \
    ii->operand_dest[i].uses.reg.bits[0] = m0; \
    ii->operand_dest[i].spaceid = LSE_emu_spaceid_ ## t; \
    ii->operand_dest[i].spaceaddr.t = a; \
  }
#define set_dest_memop(ii,i,t,m0,s) { \
    ii->operand_dest[i].uses.mem.flags = m0; \
    ii->operand_dest[i].uses.mem.size = s; \
    ii->operand_dest[i].spaceid = LSE_emu_spaceid_ ## t; \
  }

#define merge_dest_regop2(ii,i,t,a,m0,m1) { \
    ii->operand_dest[i].uses.reg.bits[0] |= m0; \
    ii->operand_dest[i].uses.reg.bits[1] |= m1; \
  }
#define merge_dest_regop1(ii,i,t,a,m0) { \
    ii->operand_dest[i].uses.reg.bits[0] |= m0; \
  }
#define clear_dest_regop_bitsused(ii,i) { \
    ii->operand_dest[i].uses.reg.bits[0] = 0; \
    ii->operand_dest[i].uses.reg.bits[1] = 0; \
  }
/* allow one parameter to become 2 */
#define set_dest_regop(ii,i,t,a,d) set_dest_regop2(ii,i,t,a,d)
#define merge_dest_regop(ii,i,t,a,d) merge_dest_regop2(ii,i,t,a,d)

#define qp_mask16   0x00000030UL
#define r1_mask32   0x00001800UL
#define r2_mask32   0x000c0000UL
#define r3_mask32   0x06000000UL
#define f1_mask32   0x00001800UL
#define f2_mask32   0x000c0000UL
#define f3_mask32   0x06000000UL
#define f4_mask32  UINT64_C(0x300000000)
#define p1_mask16   0x00000c00UL
#define p2_mask16  UINT64_C(0x180000000)
#define r1_bit16    0x00000400UL
#define r2_bit16    0x00020000UL
#define r3_bit16    0x01000000UL

/* This incredibly nasty thing figures out how to set cfm/psr for 
 * use of rotating registers and low vs. high floating point stuff...
 */
#define do_rotate(ii,instr,t,cfmmask,bnmask,dhmask,dlmask)           \
  { t ubits=instr & (cfmmask);                                       \
    t ubitshift=(~ubits>>2)&(~ubits>>1);                             \
    if (ubits)                                                       \
      set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);      \
    if (ubitshift & instr & (bnmask))                                \
      merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_bn)); \
    if ((dhmask) & (instr))                                          \
      merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_dfh));\
    if ( (((dlmask) & 1) &&                                          \
             (instr & 0x00001f80UL) && (ubitshift & 0x00000400UL)) ||\
	 (((dlmask) & 2) &&                                          \
             (instr & 0x000fc000UL) && (ubitshift & 0x00020000UL)) ||\
	 (((dlmask) & 4) &&                                          \
             (instr & 0x07e00000UL) && (ubitshift & 0x01000000UL)) ||\
	 (((dlmask) & 8) &&                                          \
             (instr &UINT64_C(0x3f0000000)) && (ubitshift& 0x80000000UL)) ) \
      merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_dfl));\
  }
								     
#define set_src_rotate_qp_r1_r2_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long,                      \
	    qp_mask16|r1_mask32|r2_mask32|r3_mask32,     \
	    r1_bit16|r2_bit16|r3_bit16,0,0)

#define set_src_rotate_qp_r2_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long,       \
	    qp_mask16|r2_mask32|r3_mask32, r2_bit16|r3_bit16,0,0)

#define set_src_rotate_qp_r1_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long,                      \
	    qp_mask16|r1_mask32|r3_mask32, r1_bit16|r3_bit16,0,0)

#define set_src_rotate_qp_r1_r2(ii,instr) \
  do_rotate(ii,instr,unsigned long,       \
            qp_mask16|r1_mask32|r2_mask32, r1_bit16|r2_bit16,0,0)

#define set_src_rotate_qp_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long, qp_mask16|r3_mask32, r3_bit16,0,0)

#define set_src_rotate_qp_r2(ii,instr) \
  do_rotate(ii,instr,unsigned long, qp_mask16|r2_mask32, r2_bit16,0,0)

#define set_src_rotate_qp_r1(ii,instr) \
  do_rotate(ii,instr,unsigned long, qp_mask16|r1_mask32, r1_bit16,0,0)

#define set_src_rotate_qp(ii,instr) \
  do_rotate(ii,instr,unsigned long, qp_mask16, 0,0,0)

#define set_src_rotate_qp_r2_r3_p1_p2(ii,instr) \
  do_rotate(ii,instr,uint64_t,                    \
	    qp_mask16|r2_mask32|r3_mask32|p1_mask16|p2_mask16, \
	    r2_bit16|r3_bit16,0,0)

#define set_src_rotate_qp_r3_p1_p2(ii,instr) \
  do_rotate(ii,instr,uint64_t,                    \
	    qp_mask16|r3_mask32|p1_mask16|p2_mask16, \
	    r3_bit16,0,0)

#define set_src_rotate_qp_f2_f3_p1_p2(ii,instr) \
  do_rotate(ii,instr,uint64_t,                    \
	    qp_mask16|f3_mask32|f2_mask32|p1_mask16|p2_mask16, \
	    0,f3_mask32|f2_mask32,6)

#define set_src_rotate_qp_f2_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f2_mask32|r3_mask32, \
	    r3_bit16,f2_mask32,2)

#define set_src_rotate_qp_f2_r1(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f2_mask32|r1_mask32, \
	    r1_bit16,f2_mask32,2)

#define set_src_rotate_qp_f2_p1_p2(ii,instr) \
  do_rotate(ii,instr,uint64_t,                    \
	    qp_mask16|f2_mask32|p1_mask16|p2_mask16, \
	    0,f2_mask32,2)

#define set_src_rotate_qp_f2(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f2_mask32, \
	    0,f2_mask32,2)

#define set_src_rotate_qp_f1_f3_p2(ii,instr) \
  do_rotate(ii,instr,uint64_t,                    \
	    qp_mask16|f1_mask32|f3_mask32|p2_mask16,   \
	    0,f1_mask32|f3_mask32,0x5)

#define set_src_rotate_qp_f1_r2_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32|r2_mask32|r3_mask32, \
	    r2_bit16|r3_bit16,f1_mask32,1)

#define set_src_rotate_qp_f1_f2_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32|f2_mask32|r3_mask32, \
	    r3_bit16,f1_mask32|f2_mask32,3)

#define set_src_rotate_qp_f1_r2(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32|r2_mask32, \
	    r2_bit16,f1_mask32,1)

#define set_src_rotate_qp_f1_f2_f3_f4(ii,instr) \
  do_rotate(ii,instr,uint64_t,                    \
	    qp_mask16|f1_mask32|f2_mask32|f3_mask32|f4_mask32, \
	    0,f1_mask32|f2_mask32|f3_mask32|f4_mask32,0xf)

#define set_src_rotate_qp_f1_f2_f3_p2(ii,instr) \
  do_rotate(ii,instr,uint64_t,                    \
	    qp_mask16|f1_mask32|f2_mask32|f3_mask32|p2_mask16, \
	    0,f1_mask32|f2_mask32|f3_mask32,0x7)

#define set_src_rotate_qp_f1_f2_f3(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32|f2_mask32|f3_mask32, \
	    0,f1_mask32|f2_mask32|f3_mask32,0x7)

#define set_src_rotate_qp_f1_f2(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32|f2_mask32,             \
	    0,f1_mask32|f2_mask32,0x3)

#define set_src_rotate_qp_f1_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32|r3_mask32, \
	    r3_bit16,f1_mask32,1)

#define set_src_rotate_qp_f1_r3(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32|r3_mask32, \
	    r3_bit16,f1_mask32,1)

#define set_src_rotate_qp_f1(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32, \
	    0,f1_mask32,2)

#define set_src_rotate_nopsr_qp_f1(ii,instr) \
  do_rotate(ii,instr,unsigned long,                    \
	    qp_mask16|f1_mask32, \
	    0,0,0)

#define set_src_gr_rotate(ii,rno)                      \
    if (rno>=32) {                                     \
      set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits); \
    } else if (rno >= 16) {                            \
      merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_bn)); \
    }
#define set_src_pr_rotate(ii,rno)                      \
    if (rno>=16) {                                     \
      set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits); \
    } 
#define set_src_fr_rotate(ii,rno)                      \
    if (rno>=32) {                                     \
      set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits); \
      merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_dfh)); \
    } else if (rno >=2) {                              \
      merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_dfl)); \
    }

#define set_qp(ii,pno) if (pno) \
  set_src_regop1(ii,src_qp,PR,pno,((INT64_C(1)<<pno)&~INT64_C(1)))

#define fp_check_target_register(di,rno) { \
    if (rno==0 || rno==1) { \
      report_potential_intr(di,IA64_intr_illegaloperation); \
    } \
  }

#define clear_dest_fr_psr(ii) \
   clear_dest_regop_bitsused(ii,dest_psr);

#define set_dest_fr_psr(ii,rno) \
  if (rno>=32) { \
    ii->operand_dest[dest_psr].spaceid = LSE_emu_spaceid_SR; \
    ii->operand_dest[dest_psr].spaceaddr.SR = IA64_ADDR_PSR; \
    merge_dest_regop1(ii,dest_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_mfh)); \
  } else if (rno>=2) { \
    ii->operand_dest[dest_psr].spaceid = LSE_emu_spaceid_SR; \
    ii->operand_dest[dest_psr].spaceaddr.SR = IA64_ADDR_PSR; \
    merge_dest_regop1(ii,dest_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_mfl)); \
  }

static int format2int_tab[604+IA64_NUM_USER_FORMATS];
static int last_user_format = 0;

/*** Initialize the translation between format number and integer ****/
void 
decode_init(void)
{
  int counter, i;
  counter=3; /* -1, -2, -3 are reserved */

  for (i=0;i<516;i++) format2int_tab[i] = 0; /* mark as reserved */
  format2int_tab[0] = 0;
  format2int_tab[1] = 1;
  format2int_tab[2] = 2;

  for (i=1;i<=29;i++) /* I */
    format2int_tab[i+3] = counter++;
  for (i=101;i<=148;i++) /* M */
    format2int_tab[i+3] = counter++;
  for (i=201;i<=216;i++) /* F */
    format2int_tab[i+3] = counter++;
  for (i=301;i<=309;i++) /* B */
    format2int_tab[i+3] = counter++;
  for (i=401;i<=405;i++) /* X */
    format2int_tab[i+3] = counter++;
  for (i=501;i<=510;i++) /* A */
    format2int_tab[i+3] = counter++;

  for (i=601;i<601+IA64_NUM_USER_FORMATS;i++) /* user */
    format2int_tab[i+3] = 0; /* mark as reserved to begin */

  last_user_format = 0;
}

/********** Define a new instruction  ************
 * these instructions will have format numbers starting with 601.  There
 * can be no more than IA64_NUM_USER_FORMATS of them.  We check that the
 * number doesn't go over and that the offset doesn't go over...
 */

int IA64_instr_define(IA64_instr_hooks_t *hooks) {
  int formatno = last_user_format+601;
  int offset = IA64_NUM_BASE_FORMATS+last_user_format;

  /* check that we haven't run out... */
  if (formatno > 600+IA64_NUM_USER_FORMATS || offset > IA64_NUM_FORMATS) 
    return -1;

  last_user_format++;

  format2int_tab[formatno] = offset;
  decode_details[offset].funcs.decode_func = hooks->decode;
  decode_details[offset].funcs.opfetch_func = hooks->opfetch;
  decode_details[offset].funcs.evaluate_func = hooks->evaluate;
  decode_details[offset].funcs.memory_func = hooks->memory;
  decode_details[offset].funcs.format_func = hooks->format;
  decode_details[offset].funcs.writeback_func = hooks->writeback;
  decode_details[offset].funcs.all_backend_func = NULL;
  decode_details[offset].funcs.disassemble_func = hooks->disassemble;
  return formatno; /* want to return the format number... */
}

void 
do_decode(LSE_emu_instr_info_t *ii) 
{
  LSE_emu_addr_t addr = ii->addr;
  uint64_t instr,instrcont;
  int btemplate, unit;
  IA64_decode_t *dt;
  int format;
  int detno;

  /* default stuff... */
  ii->size=16;
#ifdef IA64_INIT_INDIVIDUALLY
  ii->iclasses.is_cti = 0;
  ii->iclasses.is_indirect_cti = 0;
  ii->iclasses.is_unconditional_cti = 0;
  ii->iclasses.is_load = 0;
  ii->iclasses.is_store = 0;
  ii->iclasses.is_sideeffect = 0;
  ii->iclasses.is_nop = 0;
#endif
  ii->branch_num_targets = 1;

  ii->extra.btemplate = btemplate = ii->extra.bundle.bytes[0] & 0x1f;
  ii->extra.unit = unit = bundle_types[btemplate][addr&0x3];
  ii->extra.serialization = bundle_stops[btemplate][addr&0x3];

#ifndef WORDS_BIGENDIAN
#define IA64_HOST_LE
#endif

  switch (addr & 0x3) {
    /* crazy casting to long is so that 32-bit machines do not do 64-bit
     * shifts when it is not necessary, but 64-bit LP machines do not have
     * to truncate to 32 bits (as they would for int)
     * 
     */
  case 0:

#ifdef IA64_HOST_LE
    ii->extra.instr = instr =
      (ii->extra.bundle.words64[0]>>5)&UINT64_C(0x1ffFfffFfff);
#else
    ii->extra.instr = instr = (ii->extra.bundle.bytes[0]>>5) | 
      ((unsigned long)ii->extra.bundle.bytes[1]<<3) | 
      ((unsigned long)ii->extra.bundle.bytes[2]<<11) |
      ((unsigned long)ii->extra.bundle.bytes[3] << 19) | 
      ((uint64_t)ii->extra.bundle.bytes[4]<<27) | 
      ((uint64_t)(ii->extra.bundle.bytes[5]&0x3f)<<35);
#endif
    if ((btemplate>>1)==2) ii->branch_targets[0] = ii->addr + 2;
    else ii->branch_targets[0] = ii->addr + 1;
    break;

  case 1:
#ifdef IA64_HOST_LE
    ii->extra.instr = instr =
      ( ((ii->extra.bundle.words64[0]>>46)&UINT64_C(0x0000003Ffff)) |
	((ii->extra.bundle.words64[1]<<18)&UINT64_C(0x1FFFFFC0000)));
#else
    ii->extra.instr = instr = (ii->extra.bundle.bytes[5]>>6) | 
      ((unsigned long)ii->extra.bundle.bytes[6]<<2) | 
      ((unsigned long)ii->extra.bundle.bytes[7]<<10) |
      ((unsigned long)ii->extra.bundle.bytes[8] << 18) | 
      ((uint64_t)ii->extra.bundle.bytes[9]<<26) | 
      ((uint64_t)(ii->extra.bundle.bytes[10]&0x7f)<<34);
#endif
    ii->branch_targets[0] = ii->addr + 1;
    break;

  case 2 :
#ifdef IA64_HOST_LE
    ii->extra.instr = instr 
      = (ii->extra.bundle.words64[1]>>23)&UINT64_C(0x1ffFffffFfff);
#else
    ii->extra.instr = instr = (ii->extra.bundle.bytes[10]>>7) | 
      ((unsigned long)ii->extra.bundle.bytes[11]<<1) | 
      ((unsigned long)ii->extra.bundle.bytes[12]<<9) |
      ((unsigned long)ii->extra.bundle.bytes[13] << 17) | 
      ((uint64_t)ii->extra.bundle.bytes[14]<<25) | 
      ((uint64_t)ii->extra.bundle.bytes[15]<<33);
#endif
    ii->branch_targets[0] = ii->addr + 14;

    if (unit==4) { /* L+X */
#ifdef IA64_HOST_LE
      ii->extra.instrcont = instrcont =
	( ((ii->extra.bundle.words64[0]>>46)&UINT64_C(0x0000003Ffff)) |
	  ((ii->extra.bundle.words64[1]<<18)&UINT64_C(0x1FFFFFC0000)));
#else
      ii->extra.instrcont = instrcont = (ii->extra.bundle.bytes[5]>>6) | 
	((unsigned long)ii->extra.bundle.bytes[6]<<2) | 
	((unsigned long)ii->extra.bundle.bytes[7]<<10) |
	((unsigned long)ii->extra.bundle.bytes[8] << 18) | 
	((uint64_t)ii->extra.bundle.bytes[9]<<26) | 
	((uint64_t)(ii->extra.bundle.bytes[10]&0x7f)<<34);
#endif
    }

    break;
  default:
    ii->extra.instr = instr = 0;
    ii->branch_targets[0] = (ii->addr + 15) & ~INT64_C(15);
  }
  if ((unit==5) /* a continuation */
      || ((addr & 15) > 2) /* bad address */
      || (unit > 4) /* bad header */
      ){
    /* build up a nop */
    ii->extra.instr = ii->extra.instrcont = 0;
    ii->extra.formatno = 0;
    ii->privatef.formatint = 0;
    if (unit > 5) /* bad bundle encoding */
      {
	report_actual_intr(ii,IA64_intr_illegaloperation);
      }
    if ((addr & 15) > 2) /* bad address */
      {
	report_actual_intr(ii,IA64_intr_illegaloperation);
      }
    return;
  }

  /* Now, find the instruction format */
  format = 0;
  dt = decode_tables[unit];
  while (format==0 && dt != NULL) {
    int ino=(instr>>dt->shift)&dt->mask;
    format = dt->info[ino].formatno;
    dt = dt->info[ino].nextlevelp;
  }
  ii->extra.formatno = format;

  set_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_ss));

  detno = format2int_tab[format+3];
  ii->privatef.formatint = detno;

  (*(decode_details[detno].funcs.decode_func))(ii);

}

/******** Do the decoding of a user instruction....
 * if it is a purely internal instruction, the user's code should initialize
 * the instruction info structure as needed.  If it's going to be an 
 * architectural instruction, the instruction should be decoded normally 
 * first to set up the template, unit, serialization, etc....  Then call
 * this function to get to the user function to change things...
 *******/
void 
IA64_decode_user_instr(LSE_emu_instr_info_t *ii, void *dynid, int itok) {
  int detno;

  set_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_ss));

  ii->extra.formatno = itok;
  detno = format2int_tab[itok];
  ii->privatef.formatint = detno;

  memset((char*)ii->extra.bundle.bytes, 0, 16);
  (*(decode_details[detno].funcs.decode_func))(ii);
}

void 
decode_res(LSE_emu_instr_info_t *ii) 
{ /* -1 reserved */
  report_actual_intr(ii,IA64_intr_illegaloperation);
}

void 
decode_pred_res(LSE_emu_instr_info_t *ii) 
{ /* -2 predicated reserved */
  uint64_t instr = ii->extra.instr;
  report_potential_intr(ii,IA64_intr_illegaloperation);
  if (all_qp(instr) != 0) {
    set_qp(ii,all_qp(instr));
    set_src_pr_rotate(ii,all_qp(instr));
  }
}

void 
decode_bpred_res(LSE_emu_instr_info_t *ii) 
{ /* -3 predicated reserved */
  report_potential_intr(ii,IA64_intr_illegaloperation);
}

void 
decode_I1(LSE_emu_instr_info_t *ii) 
{ /* I1 multimedia multiply and shift */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op3);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I2(LSE_emu_instr_info_t *ii) 
{ /* I2 multimedia multiply mix/pack */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I3(LSE_emu_instr_info_t *ii) 
{ /* I3 multimedia mux1 */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r1_r2(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I4(LSE_emu_instr_info_t *ii) 
{ /* I4 multimedia mux2 */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r1_r2(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I5(LSE_emu_instr_info_t *ii) 
{ /* I5 shift right-variable */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r2(instr),all_gr_bits);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I6(LSE_emu_instr_info_t *ii) 
{ /* I6 multimedia shift right-fixed */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r1_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I7(LSE_emu_instr_info_t *ii) 
{ /* I7 shift left-variable */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I8(LSE_emu_instr_info_t *ii) 
{ /* I8 multimedia shift left-fixed */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r1_r2(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I9(LSE_emu_instr_info_t *ii) 
{ /* I9 population count */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I10(LSE_emu_instr_info_t *ii) 
{ /* I10 shift right set */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op3);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}
    
void 
decode_I11(LSE_emu_instr_info_t *ii) 
{ /* I11 extract (also shr by immediate) */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_regop_imm(ii,src_op3);
  set_src_rotate_qp_r1_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I12(LSE_emu_instr_info_t *ii) 
{ /* I12 zero and deposite */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_regop_imm(ii,src_op3);
  set_src_rotate_qp_r1_r2(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I13(LSE_emu_instr_info_t *ii) 
{ /* I13 zero and deposit immediate */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_regop_imm(ii,src_op2);
  set_src_regop_imm(ii,src_op3);
  set_src_rotate_qp_r1(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I14(LSE_emu_instr_info_t *ii) 
{ /* I14 - deposit immediate */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op3);
  set_src_regop_imm(ii,src_op4);
  set_src_rotate_qp_r1_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I15(LSE_emu_instr_info_t *ii) 
{ /* I15 - deposit */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op3);
  set_src_regop_imm(ii,src_op4);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I16(LSE_emu_instr_info_t *ii) 
{ /* I16 test bit */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r3_p1_p2(ii,instr);
  set_dest_regop1(ii,dest_istrue,PR,I16_p1(instr),
		  (I16_p1(instr)>0?(INT64_C(1)<<(I16_p1(instr))):0));
  set_dest_regop1(ii,dest_isfalse,PR,I16_p2(instr),
		  (I16_p2(instr)>0?(INT64_C(1)<<(I16_p2(instr))):0));
}

void 
decode_I17(LSE_emu_instr_info_t *ii) 
{ /* I17 test nat */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r3_p1_p2(ii,instr);
  set_dest_regop1(ii,dest_istrue,PR,I17_p1(instr),
		  (I17_p1(instr)>0?(INT64_C(1)<<(I17_p1(instr))):0));
  set_dest_regop1(ii,dest_isfalse,PR,I17_p2(instr),
		  (I17_p2(instr)>0?(INT64_C(1)<<(I17_p2(instr))):0));
}

void 
decode_I18(LSE_emu_instr_info_t *ii) 
{ /* I18 - nop/hint */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  ii->iclasses.is_nop = !I18_y(instr);
}

void 
decode_I19(LSE_emu_instr_info_t *ii) 
{ /* I19 - break */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
    
  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  ii->iclasses.is_sideeffect 
    = Linux_is_os(realct,ii,ii->extra.unit,
		  I19_imm20a(instr) | (I19_i(instr) << 20));
  ii->extra.decode.cti.btype = 11;
  ii->iclasses.is_cti = 1; /* exit, jumps */
  ii->branch_num_targets = 2;
}

void 
decode_I20(LSE_emu_instr_info_t *ii) 
{ /* I20 - Integer speculation check (I-unit) */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop2(ii,src_op1,GR,all_r2(instr),0,1);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r2(ii,instr);
  ii->extra.decode.cti.btype = 10;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)I20_imm7a(instr);
    tmp_IP |= ((uint64_t)I20_imm13c(instr))<<7;
    if (I20_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void 
decode_I21(LSE_emu_instr_info_t *ii) 
{ /* I21 - mov to br */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r2(ii,instr);
  set_dest_regop1(ii,dest_result,BR,I21_b1(instr),bits64);
}

void 
decode_I22(LSE_emu_instr_info_t *ii) 
{ /* I22 - mov from br */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop1(ii,src_op1,BR,I22_b2(instr),bits64);
  set_src_rotate_qp_r1(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I23(LSE_emu_instr_info_t *ii) 
{ /* I23 - mov to predicates - register */
  uint64_t instr = ii->extra.instr;
  uint64_t imm64;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r2(ii,instr);
  imm64 = I23_mask7a(instr)<<1;
  imm64 |= (I23_mask8c(instr)<<8);
  if (I23_s(instr)) imm64 |= ~INT64_C(0xFFFF);
  /* The bits actually affected are those not masked */
  set_dest_regop1(ii,dest_result,PR,64,imm64);
}

void 
decode_I24(LSE_emu_instr_info_t *ii) 
{ /* I24 - mov to predicates - immediate */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  set_dest_regop1(ii,dest_result,PR,64,(bits64 << 16)&bits64);
}

void 
decode_I25(LSE_emu_instr_info_t *ii) 
{ /* I25 - mov from pr/ip */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  if (I25_x6(instr)==0x30) {
    /* IP is *not* given as a source because it's automagic */
  }
  else {
    set_src_regop1(ii,src_op1,PR,64,bits64 << 1);
  }
  set_src_rotate_qp_r1(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I26(LSE_emu_instr_info_t *ii) 
{ /* I26 - mov.i to AR (can only be ar48 or greater) */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_rotate_qp_r2(ii,instr);
  if (!IA64_ar_reserved[I26_ar3(instr)] && (I26_ar3(instr)>=48))
    set_dest_regop1(ii,dest_result,AR,I26_ar3(instr),bits64);
}

void 
decode_I27(LSE_emu_instr_info_t *ii) 
{ /* I27 - mov.i to AR immediate (can only be ar48 or greater) */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  if (!IA64_ar_reserved[I27_ar3(instr)] && (I27_ar3(instr)>=48))
    set_dest_regop1(ii,dest_result,AR,I27_ar3(instr),bits64);
}

void 
decode_I28(LSE_emu_instr_info_t *ii) 
{ /* I28 - mov.i from AR (can only be ar48 or greater) */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  if (!IA64_ar_reserved[I28_ar3(instr)] && I28_ar3(instr)>=48)
    set_src_regop1(ii,src_op1,AR,I28_ar3(instr),bits64);
  set_src_rotate_qp_r1(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_I29(LSE_emu_instr_info_t *ii) 
{ /* I29 - sign/zero extensions */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

#define PSR_r_mem_rd (rf_mask(PSR_be)|rf_mask(PSR_ac)|rf_mask(PSR_cpl)|\
                      rf_mask(PSR_da)|rf_mask(PSR_db)|rf_mask(PSR_dd)|\
                      rf_mask(PSR_dt)|rf_mask(PSR_pk))
#define PSR_r_mem_wr (rf_mask(PSR_be)|rf_mask(PSR_ac)|rf_mask(PSR_cpl)|\
                      rf_mask(PSR_da)|rf_mask(PSR_db)|rf_mask(PSR_dd)|\
                      rf_mask(PSR_dt)|rf_mask(PSR_pk))

void 
decode_M1(LSE_emu_instr_info_t *ii) 
{ /* M1 - integer load */
  uint64_t instr = ii->extra.instr;
  int comple = M1_x6(instr)>>2;
  int len = (1<<(M1_x6(instr)&3));
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8 || comple==10;
  int cnclear = comple==9;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  if (comple==6) {
    /* have to mark all as potential sources since we do
     * do not know which is actually the case until after 
     * address is opfetched
     */
    set_src_regop1(ii,src_unat,AR,IA64_ADDR_UNAT,bits64);
  }
  set_src_rotate_qp_r1_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   PSR_r_mem_rd | (speculative ? rf_mask(PSR_ed):0));
  if (speculative)
    set_src_regop1(ii,src_dcr,CR,IA64_ADDR_DCR,bits64);

  set_src_memop(ii,src_load,mem,LSE_emu_memaccess_read,len);

  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
  if (advanced || cclear || cnclear) {
    set_dest_regop1(ii,dest_alatupdate,ALAT,all_r1(instr),1);
    set_src_regop1(ii,src_alat,ALAT,all_r1(instr),1);    
  }

  ii->iclasses.is_load = 1;
  ii->extra.serialization |= ( (((M1_x6(instr) & ~3) == 0x14) ||
				((M1_x6(instr) & ~3) == 0x28)) ?
			       IA64_serialize_acquire : 0);
}

void 
decode_M2(LSE_emu_instr_info_t *ii) 
{ /* M2 - integer load/increment by register */
  uint64_t instr = ii->extra.instr;
  int len = (1<<(M2_x6(instr)&3));
  int comple = M2_x6(instr)>>2;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8 || comple==10;
  int cnclear = comple==9;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop(ii,src_update_amt,GR,all_r2(instr),all_gr_bits);
  if ((M2_x6(instr)>>2)==6) {
    /* have to mark all as potential sources since we do
     * do not know which is actually the case until after 
     * address is opfetched
     */
    set_src_regop1(ii,src_unat,AR,IA64_ADDR_UNAT,bits64);
  }
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   PSR_r_mem_rd | (speculative ? rf_mask(PSR_ed):0));
  if (speculative)
    set_src_regop1(ii,src_dcr,CR,IA64_ADDR_DCR,bits64);
  set_src_memop(ii,src_load,mem,LSE_emu_memaccess_read,len);

  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);

  if (advanced || cclear || cnclear) {
    set_dest_regop1(ii,dest_alatupdate,ALAT,all_r1(instr),1);
    set_src_regop1(ii,src_alat,ALAT,all_r1(instr),1);    
  }

  ii->iclasses.is_load = 1;
  /* register update and destination the same */
  if (all_r1(instr) == all_r3(instr)) 
    report_potential_intr(ii,IA64_intr_illegaloperation);
  ii->extra.serialization |= ( (((M2_x6(instr) & ~3) == 0x14) ||
				((M2_x6(instr) & ~3) == 0x28)) ?
			       IA64_serialize_acquire : 0);
}

void 
decode_M3(LSE_emu_instr_info_t *ii) 
{ /* M3 - integer load/increment by immediate */
  uint64_t instr = ii->extra.instr;
  int len = (1<<(M2_x6(instr)&3));
  int comple = M1_x6(instr)>>2;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8 || comple==10;
  int cnclear = comple==9;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_update_amt);
  if ((M3_x6(instr)>>2)==6) {
    /* have to mark all as potential sources since we do
     * do not know which is actually the case until after 
     * address is opfetched
     */
    set_src_regop1(ii,src_unat,AR,IA64_ADDR_UNAT,bits64);
  }
  set_src_rotate_qp_r1_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   PSR_r_mem_rd | (speculative ? rf_mask(PSR_ed):0));
  if (speculative)
    set_src_regop1(ii,src_dcr,CR,IA64_ADDR_DCR,bits64);
  set_src_memop(ii,src_load,mem,LSE_emu_memaccess_read,len);

  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);

  if (advanced || cclear || cnclear) {
    set_dest_regop1(ii,dest_alatupdate,ALAT,all_r1(instr),1);
    set_src_regop1(ii,src_alat,ALAT,all_r1(instr),1);    
  }

  ii->iclasses.is_load = 1;
  /* register update and destination the same */
  if (all_r1(instr) == all_r3(instr)) 
    report_potential_intr(ii,IA64_intr_illegaloperation);
  ii->extra.serialization |= ( (((M3_x6(instr) & ~3) == 0x14) ||
				((M3_x6(instr) & ~3) == 0x28)) ?
			       IA64_serialize_acquire : 0);
}

void 
decode_M4(LSE_emu_instr_info_t *ii) 
{ /* M4 - integer store */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop(ii,src_store_data,GR,all_r2(instr),all_gr_bits);
  set_src_rotate_qp_r2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,PSR_r_mem_wr);
  if ((M4_x6(instr)>>2)==14) {
    /* have to mark all as potential destinations since we do
     * do not know which is actually the case until after 
     * address is opfetched
     */
    set_dest_regop1(ii,dest_unat,AR,IA64_ADDR_UNAT,bits64);
  }
  set_dest_regop1(ii,dest_alatupdate,ALAT,0,1);
  set_dest_memop(ii,dest_store,mem,LSE_emu_memaccess_write,
		 (1<<(M4_x6(instr)&3)));
  ii->iclasses.is_store = 1;
  ii->extra.serialization |= ( (((M4_x6(instr) & ~3) == 0x34)) ?
			       IA64_serialize_release : 0);
}

void 
decode_M5(LSE_emu_instr_info_t *ii) 
{ /* M5 - integer store - increment by immediate */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_update_amt);
  set_src_regop(ii,src_store_data,GR,all_r2(instr),all_gr_bits);
  set_src_rotate_qp_r2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,PSR_r_mem_wr);
  if ((M5_x6(instr)>>2)==14) {
    /* have to mark all as potential destinations since we do
     * do not know which is actually the case until after 
     * address is opfetched
     */
    set_dest_regop1(ii,dest_unat,AR,IA64_ADDR_UNAT,bits64);
  }
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);
  set_dest_regop1(ii,dest_alatupdate,ALAT,0,1);
  set_dest_memop(ii,dest_store,mem,LSE_emu_memaccess_write,
		 (1<<(M4_x6(instr)&3)));
  ii->iclasses.is_store = 1;
  ii->extra.serialization |= ( (((M5_x6(instr) & ~3) == 0x34)) ?
			       IA64_serialize_release : 0);
}

void 
decode_M6(LSE_emu_instr_info_t *ii) 
{ /* M6 - FP load */
  uint64_t instr = ii->extra.instr;
  static int sizes[] = { 
      10, 8, 4, 8
    };
  uint64_t len = (M6_x6(instr) != 0x1b) ? sizes[M6_x6(instr) & 3] : 16;
  int comple = M6_x6(instr)>>2;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8;
  int cnclear = comple==9;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_f1_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   PSR_r_mem_rd | (speculative ? rf_mask(PSR_ed):0));
  if (speculative)
    set_src_regop1(ii,src_dcr,CR,IA64_ADDR_DCR,bits64);
  set_src_memop(ii,src_load,mem,LSE_emu_memaccess_read,len);

  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));

  if (advanced || cclear || cnclear) {
    set_dest_regop1(ii,dest_alatupdate,ALAT,128+all_f1(instr),1);
    set_src_regop1(ii,src_alat,ALAT,128+all_f1(instr),1);    
  }

  ii->iclasses.is_load = 1;
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_M7(LSE_emu_instr_info_t *ii) 
{ /* M7 - FP load/increment by register */
  uint64_t instr = ii->extra.instr;
  static int sizes[] = { 
      10, 8, 4, 8
    };
  uint64_t len = (M7_x6(instr) != 0x1b) ? sizes[M7_x6(instr) & 3] : 16;
  int comple = M7_x6(instr)>>2;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8;
  int cnclear = comple==9;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop(ii,src_update_amt,GR,all_r2(instr),all_gr_bits);
  set_src_rotate_qp_f1_r2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   PSR_r_mem_rd | (speculative ? rf_mask(PSR_ed):0));
  if (speculative)
    set_src_regop1(ii,src_dcr,CR,IA64_ADDR_DCR,bits64);
  set_src_memop(ii,src_load,mem,LSE_emu_memaccess_read,len);

  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);

  if (advanced || cclear || cnclear) {
    set_dest_regop1(ii,dest_alatupdate,ALAT,128+all_f1(instr),1);
    set_src_regop1(ii,src_alat,ALAT,128+all_f1(instr),1);    
  }

  ii->iclasses.is_load = 1;
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_M8(LSE_emu_instr_info_t *ii) 
{ /* M8 - FP load/increment by immediate */
  uint64_t instr = ii->extra.instr;
  static int sizes[] = { 
      10, 8, 4, 8
    };
  uint64_t len = (M8_x6(instr) != 0x1b) ? sizes[M8_x6(instr) & 3] : 16;
  int comple = M8_x6(instr)>>2;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8;
  int cnclear = comple==9;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_update_amt);
  set_src_rotate_qp_f1_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   PSR_r_mem_rd | (speculative ? rf_mask(PSR_ed):0));
  if (speculative)
    set_src_regop1(ii,src_dcr,CR,IA64_ADDR_DCR,bits64);
  set_src_memop(ii,src_load,mem,LSE_emu_memaccess_read,len);

  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);

  if (advanced || cclear || cnclear) {
    set_dest_regop1(ii,dest_alatupdate,ALAT,128+all_f1(instr),1);
    set_src_regop1(ii,src_alat,ALAT,128+all_f1(instr),1);    
  }

  ii->iclasses.is_load = 1;
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_M9(LSE_emu_instr_info_t *ii) 
{ /* M9 - FP Store */
  uint64_t instr = ii->extra.instr;
  static int lens[] = { 10, 8, 4, 8 };
  uint64_t len = (M9_x6(instr) < 0x34) ? lens[M9_x6(instr)&3] : 16;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop(ii,src_store_data,FR,all_f2(instr),all_fr_bits);
  set_src_rotate_qp_f2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,PSR_r_mem_wr);
  set_dest_regop1(ii,dest_alatupdate,ALAT,0,1);
  set_dest_memop(ii,dest_store,mem,LSE_emu_memaccess_write,len);


  ii->iclasses.is_store = 1;
}

void 
decode_M10(LSE_emu_instr_info_t *ii) 
{ /* M10 - FP store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  static int lens[] = { 10, 8, 4, 8 };
  uint64_t len = (M10_x6(instr) < 0x34) ? lens[M10_x6(instr)&3] : 16;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_update_amt);
  set_src_regop(ii,src_store_data,FR,all_f2(instr),all_fr_bits);
  set_src_rotate_qp_f2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,PSR_r_mem_wr);
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);
  set_dest_regop1(ii,dest_alatupdate,ALAT,0,1);
  set_dest_memop(ii,dest_store,mem,LSE_emu_memaccess_write,len);
  ii->iclasses.is_store = 1;
}

void 
decode_M11(LSE_emu_instr_info_t *ii) 
{ /* M11 - FP load pair */
  uint64_t instr = ii->extra.instr;
  int comple = M1_x6(instr)>>2;
  uint64_t len = ((M11_x6(instr) &3)==2) ? 8 : 16;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8;
  int cnclear = comple==9;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_f1_f2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   PSR_r_mem_rd | (speculative ? rf_mask(PSR_ed):0));
  if (speculative)
    set_src_regop1(ii,src_dcr,CR,IA64_ADDR_DCR,bits64);
  set_src_memop(ii,src_load,mem,LSE_emu_memaccess_read,len);

  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  set_dest_regop(ii,dest_result2,FR,all_f2(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_fr_psr(ii,all_f2(instr));

  if (advanced || cclear || cnclear) {
    set_dest_regop1(ii,dest_alatupdate,ALAT,128+all_f1(instr),2);
    set_src_regop1(ii,src_alat,ALAT,128+all_f1(instr),2);
  }

  fp_check_target_register(ii,all_f1(instr));
  fp_check_target_register(ii,all_f2(instr));
}

void 
decode_M12(LSE_emu_instr_info_t *ii) 
{ /* M12 - FP load pair - increment by immediate */
  uint64_t instr = ii->extra.instr;
  int comple = M1_x6(instr)>>2;
  uint64_t len = ((M12_x6(instr) &3)==2) ? 8 : 16;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8;
  int cnclear = comple==9;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_update_amt);
  set_src_rotate_qp_f1_f2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   PSR_r_mem_rd | (speculative ? rf_mask(PSR_ed):0));
  if (speculative)
    set_src_regop1(ii,src_dcr,CR,IA64_ADDR_DCR,bits64);
  set_src_memop(ii,src_load,mem,LSE_emu_memaccess_read,len);

  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  set_dest_regop(ii,dest_result2,FR,all_f2(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_fr_psr(ii,all_f2(instr));
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);

  if (advanced || cclear || cnclear) {
    set_dest_regop1(ii,dest_alatupdate,ALAT,128+all_f1(instr),2);
    set_src_regop1(ii,src_alat,ALAT,128+all_f1(instr),2);
  }

  fp_check_target_register(ii,all_f1(instr));
  fp_check_target_register(ii,all_f2(instr));
}

void 
decode_M13(LSE_emu_instr_info_t *ii) 
{ /* M13 - line prefetch */
  uint64_t instr = ii->extra.instr;
  int comple = M13_x6(instr)&3;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_cpl) | rf_mask(PSR_ed) |
		   ((comple&2) ? (rf_mask(PSR_da)|rf_mask(PSR_dd)):0));
  set_src_memop(ii,src_load,mem,
		LSE_emu_memaccess_read|LSE_emu_memaccess_noaccess,1);
}

void 
decode_M14(LSE_emu_instr_info_t *ii) 
{ /* M14 - line prefetch/increment by register */
  uint64_t instr = ii->extra.instr;
  int comple = M14_x6(instr)&3;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop(ii,src_update_amt,GR,all_r2(instr),all_gr_bits);
  set_src_rotate_qp_r2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_cpl) | rf_mask(PSR_ed) |
		   ((comple&2) ? (rf_mask(PSR_da)|rf_mask(PSR_dd)):0));
  set_src_memop(ii,src_load,mem,
		LSE_emu_memaccess_read|LSE_emu_memaccess_noaccess,1);
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);
}

void 
decode_M15(LSE_emu_instr_info_t *ii) 
{ /* M15 - line prefetch/increment by immediate */
  uint64_t instr = ii->extra.instr;
  int comple = M14_x6(instr)&3;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_update_amt);
  set_src_rotate_qp_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_cpl) | rf_mask(PSR_ed) |
		   ((comple&2) ? (rf_mask(PSR_da)|rf_mask(PSR_dd)):0));
  set_src_memop(ii,src_load,mem,
		LSE_emu_memaccess_read|LSE_emu_memaccess_noaccess,1);
  set_dest_regop(ii,dest_baseupdate,GR,all_r3(instr),all_gr_bits);
}

void 
decode_M16(LSE_emu_instr_info_t *ii) 
{ /* M16 - Exchange/Compare and Exchange */
  uint64_t instr = ii->extra.instr;
  int len = (1<<(M16_x6(instr)&3));

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop(ii,src_store_data,GR,all_r2(instr),all_gr_bits);
  if (M1_x6(instr)<8) {
    set_src_regop1(ii,src_ccv,AR,IA64_ADDR_CCV,bits64);
  }
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   (PSR_r_mem_rd | PSR_r_mem_wr));
  ii->iclasses.is_load = 1;
  ii->iclasses.is_store = 1;
  ii->extra.serialization |= ( ((M1_x6(instr) & 4) ? IA64_serialize_release :
				IA64_serialize_acquire));
  set_src_memop(ii,src_load,mem,
		 LSE_emu_memaccess_read | LSE_emu_memaccess_atomic,len);

  set_dest_regop1(ii,dest_alatupdate,ALAT,all_r1(instr),1);
  set_dest_memop(ii,dest_store,mem,
		 LSE_emu_memaccess_write | LSE_emu_memaccess_atomic,len);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_M17(LSE_emu_instr_info_t *ii) 
{ /* M17 - Fetch and Add - Immediate */
  uint64_t instr = ii->extra.instr;
  int len = (1<<(M17_x6(instr)&3));

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_regop_imm(ii,src_update_amt);
  set_src_rotate_qp_r1_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   (PSR_r_mem_rd | PSR_r_mem_wr));
  set_src_memop(ii,src_load,mem,
		 LSE_emu_memaccess_read | LSE_emu_memaccess_atomic,len);
  ii->iclasses.is_load = 1;
  ii->iclasses.is_store = 1;
  ii->extra.serialization |= ( ((M1_x6(instr) & 4) ? IA64_serialize_release :
				IA64_serialize_acquire));
  set_dest_regop1(ii,dest_alatupdate,ALAT,all_r1(instr),1);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
  set_dest_memop(ii,dest_store,mem,
		 LSE_emu_memaccess_write | LSE_emu_memaccess_atomic,len);
}

void 
decode_M18(LSE_emu_instr_info_t *ii) 
{ /* M18 - Set FR */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_rotate_qp_f1_r2(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
}

void 
decode_M19(LSE_emu_instr_info_t *ii) 
{ /* M19 - Get FR */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  set_src_rotate_qp_f2_r1(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_M20(LSE_emu_instr_info_t *ii) 
{ /* M20 - Integer speculation check (M-unit) */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop2(ii,src_op1,GR,all_r2(instr),0,1);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_r2(ii,instr);
  ii->extra.decode.cti.btype = 10;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)M20_imm7a(instr);
    tmp_IP |= ((uint64_t)M20_imm13c(instr))<<7;
    if (M20_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void 
decode_M21(LSE_emu_instr_info_t *ii) 
{ /* M21 - FP speculation check */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_f2(ii,instr);
  ii->extra.decode.cti.btype = 10;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)M21_imm7a(instr);
    tmp_IP |= ((uint64_t)M21_imm13c(instr))<<7;
    if (M21_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void 
decode_M22(LSE_emu_instr_info_t *ii) 
{ /* M22 - Integer advanced load check */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op2);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));
  set_src_rotate_qp_r1(ii,instr);
  set_src_regop1(ii,src_alat,ALAT,all_r1(instr),1);

  if (M22_x3(instr)==5) 
    set_dest_regop1(ii,dest_alatupdate,ALAT,all_r1(instr),1);
  ii->extra.decode.cti.btype = 10;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)M22_imm20b(instr);
    if (M22_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void 
decode_M23(LSE_emu_instr_info_t *ii) 
{ /* M23 - FP advanced load check */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op2);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));
  set_src_rotate_nopsr_qp_f1(ii,instr);
  set_src_regop1(ii,src_alat,ALAT,128+all_f1(instr),1);

  if (M23_x3(instr)==7)
    set_dest_regop1(ii,dest_alatupdate,ALAT,128+all_f1(instr),1);
  ii->extra.decode.cti.btype = 10;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)M23_imm20b(instr);
    if (M23_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void
decode_M24(LSE_emu_instr_info_t *ii)
{ /* M24 - Sync/Fence/Serialize/ALAT control */
  uint64_t instr = ii->extra.instr;
  static int serializations[8] = {
    IA64_serialize_flushwb, IA64_serialize_data,
    0, IA64_serialize_instruction,
    (IA64_serialize_acquire|IA64_serialize_release), 0,
    IA64_serialize_acceptance, IA64_serialize_flushc
  };

  set_qp(ii,all_qp(instr));
  set_src_rotate_qp(ii,instr);
  if (M24_x2(instr)==1) { /* invala */
    set_dest_regop1(ii,dest_alatupdate,ALAT,-1,1);
  } else {
    int index = M24_x4(instr)<<2 | (M24_x2(instr)&1);
    /* We bounds check here since simulators may want to hijack unused
       encodings and don't want to get surprised by out-of-bounds
       array accesses */
    if (index >= 0 && index < 8)
      ii->extra.serialization |= serializations[index];
  }
}

void 
decode_M25(LSE_emu_instr_info_t *ii) 
{ /* M25 - RSE Control */
  uint64_t instr = ii->extra.instr;

  if (M25_x4(instr)&0x4) { /* flushrs */
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		     rf_mask(PSR_rt));
    set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
    set_src_regop1(ii,src_bspstore,AR,IA64_ADDR_BSPSTORE,bits64);
    set_src_regop1(ii,src_rnat,AR,IA64_ADDR_RNAT,bits64);
    set_src_regop1(ii,src_rsc,AR,IA64_ADDR_RSC,bits64);
    set_src_regop1(ii,src_rse,RSE,0,1);

    set_dest_regop1(ii,dest_bspstore,AR,IA64_ADDR_BSPSTORE,bits64);
    set_dest_regop1(ii,dest_rnat,AR,IA64_ADDR_RNAT,bits64);
    set_dest_regop1(ii,dest_rse,RSE,0,1); 
    set_dest_regop1(ii,dest_rsemem,LSE,2,1);
  } else { /* loadrs */
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		     rf_mask(PSR_rt));
    set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
    set_src_regop1(ii,src_rnat,AR,IA64_ADDR_RNAT,bits64);
    set_src_regop1(ii,src_rsc,AR,IA64_ADDR_RSC,bits64);
    set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_src_regop1(ii,src_rse,RSE,0,1);

    set_dest_regop1(ii,dest_bspstore,AR,IA64_ADDR_BSPSTORE,bits64);
    set_dest_regop1(ii,dest_rnat,AR,IA64_ADDR_RNAT,bits64);
    set_dest_regop1(ii,dest_rse,RSE,0,1); 
    set_dest_regop1(ii,dest_rsemem,RSE,2,1);
  }
}

void
decode_M26(LSE_emu_instr_info_t *ii)
{ /* M26 - Integer ALAT Entry Invalidate */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_rotate_qp_r1(ii,instr);
  set_src_regop1(ii,src_alat,ALAT,all_r1(instr),1); /* weird, but true. */
  set_dest_regop1(ii,dest_alatupdate,ALAT,all_r1(instr),1);
}

void
decode_M27(LSE_emu_instr_info_t *ii)
{ /* M27 - FP ALAT Entry Invalidate */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_rotate_qp_f1(ii,instr);
  set_src_regop1(ii,src_alat,ALAT,128+all_f1(instr),1); /* weird, but true. */
  set_dest_regop1(ii,dest_alatupdate,ALAT,128+all_f1(instr),1);
}

void
decode_M28(LSE_emu_instr_info_t *ii)
{ /* M28 - Flush Cache/Purge Translation Cache Entry */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r3(ii,instr);
  if (M28_x6(instr)&0x4) { /* ptc.e */
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));    
    /* TODO: ptc.e needs to affect translation resources... */
  }
}

void 
decode_M29(LSE_emu_instr_info_t *ii) 
{ /* M29 - mov.m to ar */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  switch (M29_ar3(instr)) {
  case IA64_ADDR_BSPSTORE:
    set_src_regop1(ii,src_rsc,AR,IA64_ADDR_RSC,bits64);
    set_src_regop1(ii,src_rse,RSE,0,1); /* is needed */
    set_dest_regop1(ii,dest_rnat,AR,IA64_ADDR_RNAT,bits64);
    set_dest_regop1(ii,dest_bsp,AR,IA64_ADDR_BSP,bits64);
    set_dest_regop1(ii,dest_rse,RSE,0,1);
    break;
  case IA64_ADDR_RNAT:
    set_src_regop1(ii,src_rsc,AR,IA64_ADDR_RSC,bits64);
    /* writing to RNAT doesn't look at hidden state, so no RSE read */
    break;
  case IA64_ADDR_ITC:
  case IA64_ADDR_RSC:
  case 0:
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
  case 6:
  case 7:
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    break;
  }
  set_src_rotate_qp_r2(ii,instr);
  if (!IA64_ar_reserved[M29_ar3(instr)] && ((M29_ar3(instr)<64) ||
					    (M29_ar3(instr)>111)))
    set_dest_regop1(ii,dest_result,AR,M29_ar3(instr),bits64);
}

void 
decode_M30(LSE_emu_instr_info_t *ii) 
{ /* M30 - mov.m to ar immediate */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  switch (M29_ar3(instr)) {
  case IA64_ADDR_BSPSTORE:
    set_src_regop1(ii,src_rsc,AR,IA64_ADDR_RSC,bits64);
    set_src_regop1(ii,src_rse,RSE,0,1); /* is needed */
    set_dest_regop1(ii,dest_rnat,AR,IA64_ADDR_RNAT,bits64);
    set_dest_regop1(ii,dest_bsp,AR,IA64_ADDR_BSP,bits64);
    set_dest_regop1(ii,dest_rse,RSE,0,1);
    break;
  case IA64_ADDR_RNAT:
    set_src_regop1(ii,src_rsc,AR,IA64_ADDR_RSC,bits64);
    /* writing to RNAT doesn't look at hidden state, so no RSE read */
    break;
  case IA64_ADDR_ITC:
  case IA64_ADDR_RSC:
  case 0:
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
  case 6:
  case 7:
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    break;
  }
  set_src_rotate_qp_r2(ii,instr);
  if (!IA64_ar_reserved[M30_ar3(instr)] && ((M30_ar3(instr)<64) ||
					    (M30_ar3(instr)>111)))
    set_dest_regop1(ii,dest_result,AR,M30_ar3(instr),bits64);
}

void 
decode_M31(LSE_emu_instr_info_t *ii) 
{ /* M31 - mov.m from ar */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  if (!IA64_ar_reserved[M31_ar3(instr)] && ((M31_ar3(instr)<64) ||
					    (M31_ar3(instr)>111)))
    set_src_regop1(ii,src_op1,AR,M31_ar3(instr),bits64);
  if (M31_ar3(instr) == IA64_ADDR_BSPSTORE || 
      M31_ar3(instr) == IA64_ADDR_RNAT) {
    set_src_regop1(ii,src_rsc,AR,IA64_ADDR_RSC,bits64);
    /* we do not access hidden state for these, so no need to read RSE other */
  } else if (M31_ar3(instr)==IA64_ADDR_ITC) {
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		     rf_mask(PSR_cpl)|rf_mask(PSR_si));
  }
  set_src_rotate_qp_r1(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_M32(LSE_emu_instr_info_t *ii) 
{ /* M32 - mov to cr */
  uint64_t instr = ii->extra.instr;
  int regno = M32_cr3(instr);

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_rotate_qp_r2(ii,instr);

  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));

  /* access to interrupt registers need to look at ic */
  if (regno>=16 && regno<=25 && regno != 18)
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_ic));
  
  /* no dest if reserved or read only register */
  if (!IA64_cr_reserved[regno] && !(regno>=68 && regno<=71) && regno != 65)
    set_dest_regop1(ii,dest_result,CR,regno,bits64);
}

void 
decode_M33(LSE_emu_instr_info_t *ii) 
{ /* M33 - mov from cr */
  uint64_t instr = ii->extra.instr;
  int regno = M33_cr3(instr);

  set_qp(ii,all_qp(instr));
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
  set_src_rotate_qp_r1(ii,instr);

  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));

  /* access to interrupt registers need to look at ic */
  if (regno>=16 && regno<=25 && regno != 18)
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_ic));
  
  /* no src if reserved or EOI (which always returns 0 on read) */
  if (!IA64_cr_reserved[regno] && regno != IA64_ADDR_EOI)
    set_src_regop1(ii,src_op1,CR,regno,bits64);

  if (regno == IA64_ADDR_IVR) {
    set_dest_regop1(ii,dest_irr0,CR,IA64_ADDR_IRR0,bits64);
    set_dest_regop1(ii,dest_irr1,CR,IA64_ADDR_IRR1,bits64);
    set_dest_regop1(ii,dest_irr2,CR,IA64_ADDR_IRR2,bits64);
    set_dest_regop1(ii,dest_irr3,CR,IA64_ADDR_IRR3,bits64);
    set_src_regop1(ii,src_tpr,CR,IA64_ADDR_TPR,bits64);
  }

}

void 
decode_M34(LSE_emu_instr_info_t *ii) 
{ /* M34 - alloc */
  uint64_t instr = ii->extra.instr;

  set_src_regop1(ii,src_op1,AR,IA64_ADDR_PFS,bits64);
  set_src_regop_imm(ii,src_op2);  /* all the values */
  set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
  set_src_regop1(ii,src_bspstore,AR,IA64_ADDR_BSPSTORE,bits64);
  set_src_regop1(ii,src_rnat,AR,IA64_ADDR_RNAT,bits64);
  set_src_regop1(ii,src_rsc,AR,IA64_ADDR_RSC,bits64);
  set_src_regop1(ii,src_rse,RSE,0,1);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_rt));
  set_dest_regop(ii,dest_result2,GR,all_r1(instr),all_gr_bits);
  set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits); 
  set_dest_regop1(ii,dest_bspstore,AR,IA64_ADDR_BSPSTORE,bits64);
  set_dest_regop1(ii,dest_rnat,AR,IA64_ADDR_RNAT,bits64);
  set_dest_regop1(ii,dest_rse,RSE,0,1); 
  set_dest_regop1(ii,dest_rsemem,LSE,2,1);
  /*set_dest_regop1(ii,dest_alatupdate,ALAT,-1,1);*/ /* can affect alat */
}

void 
decode_M35(LSE_emu_instr_info_t *ii) 
{ /* M35 - Move to PSR */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits); /* data */
  set_src_rotate_qp_r2(ii,instr);
  if (M35_x6(instr)==0x2d) { /* mov psr.l */
    /* This register is not directly read, but is read as a side effect
     * of the PSR.i bit being set...
     */
    set_src_regop1(ii,src_tpr,CR,IA64_ADDR_TPR,bits64);

    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    set_dest_regop1(ii,dest_result,SR,IA64_ADDR_PSR,UINT64_C(0xffffffff));
  } else {
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_sp));
    set_dest_regop1(ii,dest_result,SR,IA64_ADDR_PSR,UINT64_C(0x3f));
  }
}

void 
decode_M36(LSE_emu_instr_info_t *ii) 
{ /* M36 - Move from PSR */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits); /* data */
  set_src_rotate_qp_r1(ii,instr);
  if (M36_x6(instr)==0x25) { /* mov psr */
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,UINT64_C(0x18FfffFfff));
  } else {
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,UINT64_C(0x3f));
  }
}


void 
decode_M37(LSE_emu_instr_info_t *ii) 
{ /* M37 - break */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  ii->iclasses.is_sideeffect 
    = Linux_is_os(realct,ii,ii->extra.unit,
		  M37_imm20a(instr) | (M37_i(instr) << 20));
  ii->extra.decode.cti.btype = 11;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
}

void 
decode_M38(LSE_emu_instr_info_t *ii) 
{ /* M38 - probe - register */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_store_data,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r2_r3(ii,instr);

  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_cpl)|rf_mask(PSR_dt)|rf_mask(PSR_pk));

  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_M39(LSE_emu_instr_info_t *ii) 
{ /* M39 - probe - immediate */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_store_data);
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r3(ii,instr);

  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_cpl)|rf_mask(PSR_dt)|rf_mask(PSR_pk));

  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_M40(LSE_emu_instr_info_t *ii) 
{ /* M40 - probe fault - immediate */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_store_data);
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r3(ii,instr);

  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_cpl)|rf_mask(PSR_dt)|rf_mask(PSR_pk)|
		   rf_mask(PSR_da)|rf_mask(PSR_db)|rf_mask(PSR_dd));
}

void 
decode_M41(LSE_emu_instr_info_t *ii) 
{ /* M41 - Translation Cache Insert */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop1(ii,src_itir,CR,IA64_ADDR_ITIR,bits64);
  set_src_regop1(ii,src_ifa,CR,IA64_ADDR_IFA,bits64);
  /* other ops:  RR */
  set_src_rotate_qp_r2(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_ic)|rf_mask(PSR_cpl));

  /* behavior if no stop bit present is undefined, so we will do nothing */
  if (!(ii->extra.serialization & IA64_serialize_stopafter));

  /* The acquire is with respect to externally generated purges.... */
  ii->extra.serialization |= IA64_serialize_acquire;
}

void 
decode_M42(LSE_emu_instr_info_t *ii) 
{ /* M42 - Move to Indirect Register/Translation Register Insert */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op3,GR,all_r2(instr),all_gr_bits); /* data */
  set_src_regop(ii,src_op1,GR,all_r3(instr),all_gr_bits); /* base */
  set_src_rotate_qp_r2_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
  if ((M42_x6(instr)&0xe)==0xe) { /* itr.d, itr.i */
    /* other ops:  RR */
    set_src_regop1(ii,src_itir,CR,IA64_ADDR_ITIR,bits64);
    set_src_regop1(ii,src_ifa,CR,IA64_ADDR_IFA,bits64);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_ic));
  }
  /* set_dest_regop1(ii,dest_result,?,1,1); the space will depend upon the
   * selected register...
   */
}

void 
decode_M43(LSE_emu_instr_info_t *ii) 
{ /* M43 - Move from Indirect Register */
  uint64_t instr = ii->extra.instr;
  int ireg = M42_x6(instr);

  set_qp(ii,all_qp(instr));
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits); /* data */
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits); /* base */
  set_src_rotate_qp_r1_r3(ii,instr);
  if (ireg != 0x17)
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
  if (ireg == 0x15)
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_sp));
  /* set_src_regop1(ii,src_?,?,1,1); the space will depend upon the
   * selected register...
   */
}

void 
decode_M44(LSE_emu_instr_info_t *ii) 
{ /* M44 - Set/Reset User/System Mask */
  uint64_t instr = ii->extra.instr;
  int imm24 = ( (M44_i(instr)<<23) | (M44_i2d(instr)<<21) | 
		(M44_imm21a(instr)));

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  if (M44_x4(instr)<6) { /* rum/sum */
    set_dest_regop1(ii,dest_result,SR,IA64_ADDR_PSR,0x3fUL & imm24);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_sp));
  } else { /* rsm/ssm */
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    set_src_regop1(ii,src_tpr,CR,IA64_ADDR_TPR,bits64);
    set_dest_regop1(ii,dest_result,SR,IA64_ADDR_PSR,0xffFfffUL & imm24);
  }
}

void 
decode_M45(LSE_emu_instr_info_t *ii) 
{ /* M45 - translation purge */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_store_data,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r2_r3(ii,instr);

  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
}

void 
decode_M46(LSE_emu_instr_info_t *ii) 
{ /* M46 - translation access */
  uint64_t instr = ii->extra.instr;
  
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
  switch (M45_x6(instr)) {
  case 0x1a: /* thash */
    set_src_regop1(ii,src_pta,CR,IA64_ADDR_PTA,bits64);
    break;
  case 0x1b: /* ttag */
    break;
  case 0x1e: /* tpa */
  case 0x1f: /* tak */
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    break;
  }
}

void 
decode_M47(LSE_emu_instr_info_t *ii) 
{ /* M47 - purge tranlation cache entry */
  uint64_t instr = ii->extra.instr;
  
  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_base,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r3(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
}

void 
decode_M48(LSE_emu_instr_info_t *ii) 
{ /* M48 - nop/hint */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  ii->iclasses.is_nop = !M48_y(instr);
}


void 
decode_F1(LSE_emu_instr_info_t *ii) 
{ /* F1 - floating-point multiply-add */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f3(instr),all_fr_bits);
  set_src_regop(ii,src_op2,FR,all_f4(instr),all_fr_bits);
  set_src_regop(ii,src_op3,FR,all_f2(instr),all_fr_bits);
  set_src_regop1(ii,src_fpsr,AR,IA64_ADDR_FPSR,
		 rf_mask(FPSR_traps)|rf_mask(FPSR_rv)|
		 (rf_mask(FPSR_sf_controls)<<(13*F1_sf(instr)+6)));
  set_src_rotate_qp_f1_f2_f3_f4(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_regop1(ii,dest_fpsr,AR,IA64_ADDR_FPSR,
		  (rf_mask(FPSR_sf_flags)<<(13*F1_sf(instr)+6)));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F2(LSE_emu_instr_info_t *ii) 
{ /* F2 - fixed-point multiply-add */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f3(instr),all_fr_bits);
  set_src_regop(ii,src_op2,FR,all_f4(instr),all_fr_bits);
  set_src_regop(ii,src_op3,FR,all_f2(instr),all_fr_bits);
  set_src_rotate_qp_f1_f2_f3_f4(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F3(LSE_emu_instr_info_t *ii) 
{ /* F3 - floating-point select */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f3(instr),all_fr_bits);
  set_src_regop(ii,src_op2,FR,all_f4(instr),all_fr_bits);
  set_src_regop(ii,src_op3,FR,all_f2(instr),all_fr_bits);
  set_src_rotate_qp_f1_f2_f3_f4(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F4(LSE_emu_instr_info_t *ii) 
{ /* F4 - floating-point compare */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  set_src_regop(ii,src_op2,FR,all_f3(instr),all_fr_bits);
  set_src_regop1(ii,src_fpsr,AR,IA64_ADDR_FPSR,
		 rf_mask(FPSR_traps)|rf_mask(FPSR_rv)|
		 (rf_mask(FPSR_sf_controls)<<(13*F4_sf(instr)+6)));
  set_src_rotate_qp_f2_f3_p1_p2(ii,instr);
  set_dest_regop1(ii,dest_istrue,PR,F4_p1(instr),
		  (INT64_C(1)<<F4_p1(instr))&~INT64_C(1));
  set_dest_regop1(ii,dest_isfalse,PR,F4_p2(instr),
		  (INT64_C(1)<<F4_p2(instr))&~INT64_C(1));
  set_dest_regop1(ii,dest_fpsr,AR,IA64_ADDR_FPSR,
		  (rf_mask(FPSR_sf_flags)<<(13*F4_sf(instr)+6)));
}

void 
decode_F5(LSE_emu_instr_info_t *ii) 
{ /* F5 - floating-point class */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_rotate_qp_f2_p1_p2(ii,instr);
  set_dest_regop1(ii,dest_istrue,PR,F4_p1(instr),
		  (INT64_C(1)<<F4_p1(instr))&~INT64_C(1));
  set_dest_regop1(ii,dest_isfalse,PR,F4_p2(instr),
		  (INT64_C(1)<<F4_p2(instr))&~INT64_C(1));
}

void 
decode_F6(LSE_emu_instr_info_t *ii) 
{ /* F6 - floating-point reciprocal approximation */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  set_src_regop(ii,src_op2,FR,all_f3(instr),all_fr_bits);
  set_src_regop1(ii,src_fpsr,AR,IA64_ADDR_FPSR,
		 rf_mask(FPSR_traps)|rf_mask(FPSR_rv)|
		 (rf_mask(FPSR_sf_controls)<<(13*F6_sf(instr)+6)));
  set_src_rotate_qp_f1_f2_f3_p2(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  set_dest_regop1(ii,dest_isfalse,PR,F6_p2(instr),
		  (INT64_C(1)<<(F6_p2(instr)))&~INT64_C(1));
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_regop1(ii,dest_fpsr,AR,IA64_ADDR_FPSR,
		  (rf_mask(FPSR_sf_flags)<<(13*F6_sf(instr)+6)));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F7(LSE_emu_instr_info_t *ii) 
{ /* F7 - floating-point reciprocal sqrt approximation */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f3(instr),all_fr_bits);
  set_src_regop1(ii,src_fpsr,AR,IA64_ADDR_FPSR,
		 rf_mask(FPSR_traps)|rf_mask(FPSR_rv)|
		 (rf_mask(FPSR_sf_controls)<<(13*F7_sf(instr)+6)));
  set_src_rotate_qp_f1_f3_p2(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  set_dest_regop1(ii,dest_isfalse,PR,F7_p2(instr),
		  (INT64_C(1)<<(F7_p2(instr)))&~INT64_C(1));
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_regop1(ii,dest_fpsr,AR,IA64_ADDR_FPSR,
		  (rf_mask(FPSR_sf_flags)<<(13*F7_sf(instr)+6)));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F8(LSE_emu_instr_info_t *ii) 
{ /* F8 - minimum/maximum and parallel compare */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  set_src_regop(ii,src_op2,FR,all_f3(instr),all_fr_bits);
  set_src_regop1(ii,src_fpsr,AR,IA64_ADDR_FPSR,
		 rf_mask(FPSR_traps)|rf_mask(FPSR_rv)|
		 (rf_mask(FPSR_sf_controls)<<(13*F8_sf(instr)+6)));
  set_src_rotate_qp_f1_f2_f3(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_regop1(ii,dest_fpsr,AR,IA64_ADDR_FPSR,
		  (rf_mask(FPSR_sf_flags)<<(13*F8_sf(instr)+6)));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F9(LSE_emu_instr_info_t *ii) 
{ /* F9 - merge and logical */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  set_src_regop(ii,src_op2,FR,all_f3(instr),all_fr_bits);
  set_src_rotate_qp_f1_f2_f3(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F10(LSE_emu_instr_info_t *ii) 
{ /* F10 - convert floating-point to fixed-point */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  set_src_regop1(ii,src_fpsr,AR,IA64_ADDR_FPSR,
		 rf_mask(FPSR_traps)|rf_mask(FPSR_rv)|
		 (rf_mask(FPSR_sf_controls)<<(13*F10_sf(instr)+6)));
  set_src_rotate_qp_f1_f2(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  set_dest_regop1(ii,dest_fpsr,AR,IA64_ADDR_FPSR,
		  (rf_mask(FPSR_sf_flags)<<(13*F10_sf(instr)+6)));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F11(LSE_emu_instr_info_t *ii) 
{ /* F11 - convert fixed-point to floating-point */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,FR,all_f2(instr),all_fr_bits);
  set_src_rotate_qp_f1_f2(ii,instr);
  set_dest_regop(ii,dest_result,FR,all_f1(instr),all_fr_bits);
  clear_dest_fr_psr(ii);
  set_dest_fr_psr(ii,all_f1(instr));
  fp_check_target_register(ii,all_f1(instr));
}

void 
decode_F12(LSE_emu_instr_info_t *ii) 
{ /* F12 - floating-point set controls */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_regop_imm(ii,src_op2);
  set_src_regop1(ii,src_fpsr,AR,IA64_ADDR_FPSR,
		 rf_mask(FPSR_sf0_controls));
  set_src_rotate_qp(ii,instr);
  set_dest_regop1(ii,dest_fpsr,AR,IA64_ADDR_FPSR,
		  (rf_mask(FPSR_sf_controls)<<(13*F12_sf(instr)+6)));
}

void 
decode_F13(LSE_emu_instr_info_t *ii) 
{ /* F12 - floating-point clear flags */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_rotate_qp(ii,instr);
  set_dest_regop1(ii,dest_fpsr,AR,IA64_ADDR_FPSR,
		  (rf_mask(FPSR_sf_flags)<<(13*F12_sf(instr)+6)));
}

void 
decode_F14(LSE_emu_instr_info_t *ii) 
{ /* F14 - floating-point check flags */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_rotate_qp(ii,instr);
  set_src_regop1(ii,src_fpsr,AR,IA64_ADDR_FPSR,
		 rf_mask(FPSR_traps)|rf_mask(FPSR_rv)|
		 rf_mask(FPSR_sf0_flags)|
		 (rf_mask(FPSR_sf_flags)<<(13*F4_sf(instr)+6)));
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));
  ii->extra.decode.cti.btype = 10;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)F14_imm20a(instr);
    if (F14_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void 
decode_F15(LSE_emu_instr_info_t *ii) 
{ /* F15 - nop/break */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  ii->iclasses.is_sideeffect 
    = Linux_is_os(realct,ii,ii->extra.unit,
		  F15_imm20a(instr) | (F15_i(instr) << 20));
  ii->extra.decode.cti.btype = 11;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
}

void 
decode_F16(LSE_emu_instr_info_t *ii) 
{ /* F16 - nop/hint */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  ii->iclasses.is_nop = !F16_y(instr);
}


void 
decode_B1(LSE_emu_instr_info_t *ii) 
{ /* B1 - IP-relative branch */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_branchoff);
  set_src_rotate_qp(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));

  if (B1_btype(instr)>0) { /* wexit/wtop */
    set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_src_regop1(ii,src_ec,AR,IA64_ADDR_EC,bits64);
    set_dest_regop1(ii,dest_result,PR,63,(INT64_C(1)<<63));
    set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_dest_regop1(ii,dest_ec,AR,IA64_ADDR_EC,bits64);
    if ((ii->addr & INT64_C(3)) != 2) { /* check slot */
      report_actual_intr(ii,IA64_intr_illegaloperation);
    }
  }
  ii->iclasses.is_cti = 1;
  ii->extra.decode.cti.btype = B1_btype(instr);
  ii->branch_num_targets = 2;
  ii->iclasses.is_unconditional_cti = ((all_qp(instr)==0) && 
				       (B1_btype(instr)==0));
  if (ii->iclasses.is_unconditional_cti) ii->branch_dir = 1;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)B1_imm20b(instr);
    if (B1_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
    }
}
  
void 
decode_B2(LSE_emu_instr_info_t *ii) 
{ /* B2 - IP-relative counted branch */
  uint64_t instr = ii->extra.instr;

  set_src_regop1(ii,src_lc,AR,IA64_ADDR_LC,bits64);
  set_src_regop_imm(ii,src_branchoff);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));
  if (B2_btype(instr)>5) { /* cexit/ctop */
    set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_src_regop1(ii,src_ec,AR,IA64_ADDR_EC,bits64);
    set_dest_regop1(ii,dest_istrue,PR,63,(INT64_C(1)<<63));
    set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_dest_regop1(ii,dest_ec,AR,IA64_ADDR_EC,bits64);
  }
  set_dest_regop1(ii,dest_lc,AR,IA64_ADDR_LC,bits64);
  ii->extra.decode.cti.btype = B2_btype(instr);
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  ii->iclasses.is_unconditional_cti = 0;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)B2_imm20b(instr);
    if (B2_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
  if ((ii->addr & INT64_C(3)) != 2) { /* check slot */
    report_actual_intr(ii,IA64_intr_illegaloperation);
  }
}
  
void 
decode_B3(LSE_emu_instr_info_t *ii) 
{ /* B3 - IP-relative call */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_branchoff);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb)|rf_mask(PSR_cpl));
  set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
  set_src_regop1(ii,src_ec,AR,IA64_ADDR_EC,all_cfm_bits);
  set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
  set_src_regop1(ii,src_rse,RSE,0,1);
  set_dest_regop1(ii,dest_result,BR,B3_b1(instr),bits64);
  set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
  set_dest_regop1(ii,dest_pfs,AR,IA64_ADDR_PFS,bits64);
  set_dest_regop1(ii,dest_bsp,AR,IA64_ADDR_BSP,bits64);
  set_dest_regop1(ii,dest_rse,RSE,0,1);
  ii->extra.decode.cti.btype = 8;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  ii->iclasses.is_unconditional_cti = (all_qp(instr)==0);
  if (ii->iclasses.is_unconditional_cti) ii->branch_dir = 1;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)B3_imm20b(instr);
    if (B3_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void 
decode_B4(LSE_emu_instr_info_t *ii) 
{ /* B4 - indirect branch */
  uint64_t instr = ii->extra.instr;

  /* note that br.ia should not be predicated and does *not* read CFM if
   * you do happen to predicate it... */
  set_src_regop1(ii,src_branchoff,BR,B4_b2(instr),bits64);
  if (B2_btype(instr)==4) { /* ret */
    set_qp(ii,all_qp(instr));
    set_src_rotate_qp(ii,instr);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		     rf_mask(PSR_it)|rf_mask(PSR_tb)|rf_mask(PSR_lp));
    set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_src_regop1(ii,src_pfs,AR,IA64_ADDR_PFS,bits64);
    set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
    set_src_regop1(ii,src_rse,RSE,0,1);
    set_dest_regop1(ii,dest_bsp,AR,IA64_ADDR_BSP,bits64);
    set_dest_regop1(ii,dest_ec,AR,IA64_ADDR_EC,bits64);
    set_dest_regop1(ii,dest_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_dest_regop1(ii,dest_rse,RSE,0,1);
    set_dest_regop1(ii,dest_rsemem,LSE,2,1);
    /*set_dest_regop1(ii,dest_alatupdate,ALAT,-1,1);*/ /* notify ALAT */
  } else if (B2_btype(instr)==1) { /* ia */
    /* Note that the hazard charts in the manual
     * describe a large assortment of registers that br.ia reads.  These
     * are not actually used by br.ia, but could be used by IA32 
     * instructions.  Rather than have dozens of source operands, we treat
     * br.ia as a special case; if you plan to simulate using it, you need
     * to check for it and flush the pipe appropriately.  We are tempted
     * to just call it a side-effecting instruction....
     *
     * However, we will note ops that are actually used...
     */
    set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
    set_src_regop1(ii,src_bspstore,AR,IA64_ADDR_BSPSTORE,bits64);
    set_src_regop1(ii,src_rse,RSE,0,1);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		     rf_mask(PSR_it)|rf_mask(PSR_tb)|rf_mask(PSR_di));
    set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_dest_regop1(ii,dest_rse,RSE,0,1);
    set_dest_regop1(ii,dest_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_is));
  } else {
    set_qp(ii,all_qp(instr));
    set_src_rotate_qp(ii,instr);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		     rf_mask(PSR_it)|rf_mask(PSR_tb));
  }
  ii->extra.decode.cti.btype = B4_btype(instr);
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  ii->iclasses.is_unconditional_cti = (all_qp(instr)==0);
  if (ii->iclasses.is_unconditional_cti) ii->branch_dir = 1;
  ii->iclasses.is_indirect_cti = 1;
}

void 
decode_B5(LSE_emu_instr_info_t *ii) 
{ /* B5 - indirect call */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop1(ii,src_branchoff,BR,B5_b2(instr),bits64);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb)|rf_mask(PSR_cpl));
  set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
  set_src_regop1(ii,src_ec,AR,IA64_ADDR_EC,bits64);
  set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
  set_src_regop1(ii,src_rse,RSE,0,1);
  set_dest_regop1(ii,dest_result,BR,B3_b1(instr),all_br_bits);
  set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
  set_dest_regop1(ii,dest_pfs,AR,IA64_ADDR_PFS,bits64);
  set_dest_regop1(ii,dest_bsp,AR,IA64_ADDR_BSP,bits64);
  set_dest_regop1(ii,dest_rse,RSE,0,1);
  ii->extra.decode.cti.btype = 8;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  ii->iclasses.is_unconditional_cti = (all_qp(instr)==0);
  ii->iclasses.is_indirect_cti = 1;
}

void 
decode_B6(LSE_emu_instr_info_t *ii) 
{ /* B6 - IP-relative predict */
  uint64_t instr = ii->extra.instr;

  set_src_regop_imm(ii,src_op1); /* tag */
  set_src_regop_imm(ii,src_branchoff);
  ii->iclasses.is_cti = 0;
  ii->branch_num_targets = 2;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)B6_imm20b(instr);
    if (B6_s(instr)) tmp_IP |= ~INT64_C(0xFFFFF);
    tmp_IP <<= 4;
    tmp_IP += (ii->addr & ~INT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}
  
void 
decode_B7(LSE_emu_instr_info_t *ii) 
{ /* B7 - indirect predict */
  uint64_t instr = ii->extra.instr;

  set_src_regop_imm(ii,src_op1); /* tag */
  set_src_regop1(ii,src_branchoff,BR,B7_b2(instr),bits64);
  ii->iclasses.is_cti = 0;
  ii->branch_num_targets = 2;
}
  
void 
decode_B8(LSE_emu_instr_info_t *ii) 
{ /* B8 - miscellaneous B-unit */
  uint64_t instr = ii->extra.instr;

  /* not predicated */
  
  switch (B8_x6(instr)) {
    
  case 2 : /* cover */
    if (!(ii->extra.serialization & IA64_serialize_stopafter))
      report_actual_intr(ii,IA64_intr_illegaloperation);
    set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
    set_src_regop1(ii,src_rse,RSE,0,1);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_ic));
    set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_dest_regop1(ii,dest_bsp,AR,IA64_ADDR_BSP,bits64);
    set_dest_regop1(ii,dest_ifs,CR,IA64_ADDR_IFS,bits64);
    set_dest_regop1(ii,dest_rse,RSE,0,1);
    break;

  case 4 : /* clrrb */
  case 5 : /* clrrb.pr */
    if (!(ii->extra.serialization & IA64_serialize_stopafter))
      report_actual_intr(ii,IA64_intr_illegaloperation);
    set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    break;

  case 12 : /* bsw.0 */
  case 13 : /* bsw.1 */
    if (!(ii->extra.serialization & IA64_serialize_stopafter))
      report_actual_intr(ii,IA64_intr_illegaloperation);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    set_dest_regop1(ii,dest_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_bn));
    break;
    
  case 0x10 : /* epc */
    set_src_regop1(ii,src_pfs,AR,IA64_ADDR_PFS,bits64);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		     (rf_mask(PSR_cpl)|rf_mask(PSR_it)));
    set_dest_regop1(ii,dest_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    break;

  case 8 : /* rfi */
    /* This one reads CR[IFS], CR[IIP], CR[IPSR], CR[TPR]
     * and writes CFM, all of PSR, BSP */
    set_qp(ii,all_qp(instr));
    set_src_rotate_qp(ii,instr);
    set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
    set_src_regop1(ii,src_ifs,CR,IA64_ADDR_IFS,bits64);
    set_src_regop1(ii,src_iip,CR,IA64_ADDR_IIP,bits64);
    set_src_regop1(ii,src_ipsr,CR,IA64_ADDR_IPSR,bits64);
    set_src_regop1(ii,src_tpr,CR,IA64_ADDR_TPR,bits64);
    set_src_regop1(ii,src_rse,RSE,0,1);
    merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,rf_mask(PSR_cpl));
    set_dest_regop1(ii,dest_bsp,AR,IA64_ADDR_BSP,bits64);
    set_dest_regop1(ii,dest_psr,SR,IA64_ADDR_PSR,bits64);
    set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_dest_regop1(ii,dest_rse,RSE,0,1);
    set_dest_regop1(ii,dest_rsemem,LSE,2,1);
    /*set_dest_regop1(ii,dest_alatupdate,ALAT,-1,1);*/ /* notify ALAT */
    ii->branch_num_targets = 2;
    ii->branch_dir = 1;
    ii->extra.decode.cti.btype = 9;
    ii->iclasses.is_cti = 1;
    ii->iclasses.is_unconditional_cti = 1;
    ii->iclasses.is_indirect_cti = 1;
    break;

  default:
    report_potential_intr(ii,IA64_intr_notimplemented);
  }
}

void 
decode_B9(LSE_emu_instr_info_t *ii) 
{ /* B9 - nop/break */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  if (!all_majorop(instr)) { /* break */
    ii->iclasses.is_sideeffect 
      = Linux_is_os(realct,ii,ii->extra.unit,
		    B9_imm20a(instr) | (B9_i(instr) << 20));
    ii->extra.decode.cti.btype = 11;
    ii->iclasses.is_cti = 1;
    ii->branch_num_targets = 2;
  } else ii->iclasses.is_nop = 1;
}


void 
decode_X1(LSE_emu_instr_info_t *ii) 
{ /* X1 - nop/break */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t temp;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  temp = (uint64_t)X1_imm20a(instr);
  temp |= (uint64_t)X1_i(instr) << 20;
  temp |= (ii->extra.instrcont << 21);
  ii->iclasses.is_sideeffect 
    = Linux_is_os(realct,ii,ii->extra.unit,temp);
  ii->extra.decode.cti.btype = 11;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
}

void 
decode_X2(LSE_emu_instr_info_t *ii) 
{ /* X2 - movl */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp_r1(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_X3(LSE_emu_instr_info_t *ii) 
{ /* X3 - long branch */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_branchoff);
  set_src_rotate_qp(ii,instr);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb));
  if (X3_btype(instr)>0) { /* wexit/wtop */
    set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_src_regop1(ii,src_ec,AR,IA64_ADDR_EC,bits64);
    set_dest_regop1(ii,dest_result,PR,63,(INT64_C(1)<<63));
    set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
    set_dest_regop1(ii,dest_ec,AR,IA64_ADDR_EC,bits64);
    if ((ii->addr & INT64_C(3)) != 2) { /* check slot */
      report_actual_intr(ii,IA64_intr_illegaloperation);
    }
  }
  ii->extra.decode.cti.btype = X3_btype(instr);
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  ii->iclasses.is_unconditional_cti = ((all_qp(instr)==0) && 
				       (B1_btype(instr)==0));
  if (ii->iclasses.is_unconditional_cti) ii->branch_dir = 1;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)X4_imm20b(instr) << 4;
    tmp_IP |= ((ii->extra.instrcont & ~UINT64_C(3))<< 22);
    tmp_IP |= (uint64_t)X4_i(instr) << 63;
    tmp_IP += (ii->addr & ~UINT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void 
decode_X4(LSE_emu_instr_info_t *ii) 
{ /* X4 - long call */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_branchoff);
  merge_src_regop1(ii,src_psr,SR,IA64_ADDR_PSR,
		   rf_mask(PSR_it)|rf_mask(PSR_tb)|rf_mask(PSR_cpl));
  set_src_regop1(ii,src_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
  set_src_regop1(ii,src_ec,AR,IA64_ADDR_EC,all_cfm_bits);
  set_src_regop1(ii,src_bsp,AR,IA64_ADDR_BSP,bits64);
  set_src_regop1(ii,src_rse,RSE,0,1);
  set_dest_regop1(ii,dest_result,BR,X4_b1(instr),bits64);
  set_dest_regop1(ii,dest_cfm,SR,IA64_ADDR_CFM,all_cfm_bits);
  set_dest_regop1(ii,dest_pfs,AR,IA64_ADDR_PFS,bits64);
  set_dest_regop1(ii,dest_bsp,AR,IA64_ADDR_BSP,bits64);
  set_dest_regop1(ii,dest_rse,RSE,0,1);
  ii->extra.decode.cti.btype = 8;
  ii->iclasses.is_cti = 1;
  ii->branch_num_targets = 2;
  ii->iclasses.is_unconditional_cti = (all_qp(instr)==0);
  if (ii->iclasses.is_unconditional_cti) ii->branch_dir = 1;
  {
    uint64_t tmp_IP;
    tmp_IP = (uint64_t)X4_imm20b(instr) << 4;
    tmp_IP |= ((ii->extra.instrcont & ~UINT64_C(3))<< 22);
    tmp_IP |= (uint64_t)X4_i(instr) << 63;
    tmp_IP += (ii->addr & ~UINT64_C(3));
    ii->branch_targets[1] = tmp_IP;
  }
}

void 
decode_X5(LSE_emu_instr_info_t *ii) 
{ /* F16 - nop/hint */
  uint64_t instr = ii->extra.instr;
    
  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_rotate_qp(ii,instr);
  ii->iclasses.is_nop = !X5_y(instr);
}

void 
decode_A1(LSE_emu_instr_info_t *ii) 
{ /* A1 - add/sub/addp4/and/andcm/or/xor */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  if (A1_x2b(instr) + A1_x4(instr) == 1) {
    set_src_regop_imm(ii,src_op3);
  }
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_A2(LSE_emu_instr_info_t *ii) 
{ /* A2 shladd/shladdp4 */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_regop(ii,src_op3,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_A3(LSE_emu_instr_info_t *ii) 
{ /* A3 Integer ALU - Immediate8-Register */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_A4(LSE_emu_instr_info_t *ii) 
{ /* A4 adds/addp4 */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_A5(LSE_emu_instr_info_t *ii) 
{ /* A5 - addl */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_regop(ii,src_op2,GR,A5_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1(ii,instr); /* not r3 because it is restricted to 0-3 */
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_A6(LSE_emu_instr_info_t *ii) 
{ /* A6 integer compare - register-register */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r2_r3_p1_p2(ii,instr);
  set_dest_regop1(ii,dest_istrue,PR,A6_p1(instr),
		  (INT64_C(1)<<(A6_p1(instr)))&~INT64_C(1));
  set_dest_regop1(ii,dest_isfalse,PR,A6_p2(instr),
		  (INT64_C(1)<<(A6_p2(instr)))&~INT64_C(1));
  if (all_p1(instr) == all_p2(instr)) 
    ii->privatef.potential_intr = (int)IA64_intr_illegaloperation;
}

void 
decode_A7(LSE_emu_instr_info_t *ii) 
{ /* A7 integer compare to zero */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r3_p1_p2(ii,instr);
  set_dest_regop1(ii,dest_istrue,PR,A7_p1(instr),
		  (INT64_C(1)<<(A7_p1(instr)))&~INT64_C(1));
  set_dest_regop1(ii,dest_isfalse,PR,A7_p2(instr),
		  (INT64_C(1)<<(A7_p2(instr)))&~INT64_C(1));
  if (all_p1(instr) == all_p2(instr)) 
    ii->privatef.potential_intr = (int)IA64_intr_illegaloperation;
}

void 
decode_A8(LSE_emu_instr_info_t *ii) 
{ /* A8 integer compare immediate-register */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop_imm(ii,src_op1);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r3_p1_p2(ii,instr);
  set_dest_regop1(ii,dest_istrue,PR,A8_p1(instr),
		  (INT64_C(1)<<(A8_p1(instr)))&~INT64_C(1));
  set_dest_regop1(ii,dest_isfalse,PR,A8_p2(instr),
		  (INT64_C(1)<<(A8_p2(instr)))&~INT64_C(1));
  if (all_p1(instr) == all_p2(instr)) 
    ii->privatef.potential_intr = (int)IA64_intr_illegaloperation;
}

void 
decode_A9(LSE_emu_instr_info_t *ii) 
{ /* A9 - multimedia ALU */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop(ii,src_op2,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
}

void 
decode_A10(LSE_emu_instr_info_t *ii) 
{ /* A10 - multimedia shift and add */
  uint64_t instr = ii->extra.instr;

  set_qp(ii,all_qp(instr));
  set_src_regop(ii,src_op1,GR,all_r2(instr),all_gr_bits);
  set_src_regop_imm(ii,src_op2);
  set_src_regop(ii,src_op3,GR,all_r3(instr),all_gr_bits);
  set_src_rotate_qp_r1_r2_r3(ii,instr);
  set_dest_regop(ii,dest_result,GR,all_r1(instr),all_gr_bits);
  if (A10_ct2d(instr) > 2) {
    report_potential_intr(ii,IA64_intr_illegaloperation);
  }
}

} // namespace LSE_IA64
