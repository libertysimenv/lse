/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header file for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file contains common type and constant definitions that the IA64
 * emulator uses internally.  It does *not* use these definitions elsewhere.
 *
 * TODO: 
 *       - complete the register definitions
 *       - make implementation dependencies variables?
 *
 */
#ifndef _LSE_IA64_H_
#define _LSE_IA64_H_
#include <math.h>
#include <stdio.h>
#include <LSE_endianness.h>
#include <LSE_chkpt.h>
#include <LSE_emu.h>
#include "LSE_IA64_SIM_isa.h"
#include <emulib/LSE_env.h>
#include <LSE_memory.h>

/* #define DEBUG_RSE */
namespace LSE_IA64 {
/**************************** Context structures ********************/

/* Field operators */
#define exf(i,l,b) (((i)>>(b))&((UINT64_C(1)<<(l))-1))
#define putf(i,l,b,v) (i = ((~(((UINT64_C(1)<<(l))-1)<<(b)) & (i)) \
                       | (((uint64_t)(v))<<(b))))
#define maskf(l,b) (((UINT64_C(1)<<(l))-1)<<b)

#define exfw(i,l,b) (((i)>>(b))&((1UL<<(l))-1))
#define putfw(i,l,b,v) (i = ((~(((1UL<<(l))-1)<<(b)) & (i)) \
                      | (((unsigned long)(v))<<(b))))
#define maskfw(l,b) (((1UL<<(l))-1)<<b)

#define rf_get(i,j) exf(i,j)
#define rf_set(i,j,v) putf(i,j,v)
#define rf_mask(i) maskf(i) 

typedef LSE_emu_space_GR_t IA64_GR_t;
typedef LSE_emu_space_FR_t IA64_FR_t;
typedef LSE_emu_space_AR_t IA64_AR_t;
typedef LSE_emu_space_BR_t IA64_BR_t;
typedef LSE_emu_space_SR_t IA64_SR_t;

#define CFM_sof 7,0
#define CFM_sol 7,7
#define CFM_sor 4,14
#define CFM_rrb 21,18
#define CFM_rrb_gr 7,18
#define CFM_rrb_fr 7,25
#define CFM_rrb_pr 7,32

#define PSR_umask 6,0
#define PSR_sysmask 24,0
#define PSR_be    1,1
#define PSR_up    1,2
#define PSR_ac    1,3
#define PSR_mfl   1,4
#define PSR_mfh   1,5
#define PSR_mfboth 2,4
#define PSR_ic    1,13
#define PSR_i     1,14
#define PSR_pk    1,15
#define PSR_dt    1,17
#define PSR_dfl   1,18
#define PSR_dfh   1,19
#define PSR_sp    1,20
#define PSR_pp    1,21
#define PSR_di    1,22
#define PSR_si    1,23
#define PSR_db    1,24
#define PSR_lp    1,25
#define PSR_tb    1,26
#define PSR_rt    1,27
#define PSR_cpl   2,32
#define PSR_is    1,34
#define PSR_mc    1,35
#define PSR_it    1,36
#define PSR_id    1,37
#define PSR_da    1,38
#define PSR_dd    1,39
#define PSR_ss    1,40
#define PSR_ri    2,41
#define PSR_ed    1,43
#define PSR_bn    1,44
#define PSR_ia    1,45

#define IA64_ADDR_CFM 0
#define IA64_ADDR_PSR 1

#define PFS_pfm 38,0
#define PFS_pfm_sof 7,0
#define PFS_pfm_sol 7,7
#define PFS_pfm_sor 4,14
#define PFS_rv1 14,38
#define PFS_pec 6,52
#define PFS_rv2 4,58
#define PFS_ppl 2,62

#define RSC_mode 2,0
#define RSC_pl 2,2
#define RSC_be 1,4
#define RSC_rv 11,5
#define RSC_loadrs 14,16
#define RSC_rv2 34,30

#define FPSR_traps         6,0
#define FPSR_vd      1,0
#define FPSR_dd      1,1
#define FPSR_zd      1,2
#define FPSR_od      1,3
#define FPSR_ud      1,4
#define FPSR_id      1,5
#define FPSR_sf_controls   7,0
#define FPSR_ftz     1,0
#define FPSR_wre     1,1
#define FPSR_pc      2,2
#define FPSR_rc      2,4
#define FPSR_td      1,6
#define FPSR_sf_flags      6,7
#define FPSR_v       1,7
#define FPSR_d       1,8
#define FPSR_z       1,9
#define FPSR_o       1,10
#define FPSR_u       1,11
#define FPSR_i       1,12
#define FPSR_f_v     1,0
#define FPSR_f_d     1,1
#define FPSR_f_z     1,2
#define FPSR_f_o     1,3
#define FPSR_f_u     1,4
#define FPSR_f_i     1,5
#define FPSR_sf0          13,6
#define FPSR_sf0_controls  7,6
#define FPSR_sf0_flags    6,13
#define FPSR_sf1         13,19
#define FPSR_sf2         13,32
#define FPSR_sf3         13,45
#define FPSR_rv           6,58

#define IA64_ADDR_RSC 16
#define IA64_ADDR_BSP 17
#define IA64_ADDR_BSPSTORE 18
#define IA64_ADDR_RNAT 19
#define IA64_ADDR_CCV 32
#define IA64_ADDR_UNAT 36
#define IA64_ADDR_FPSR 40
#define IA64_ADDR_ITC 44
#define IA64_ADDR_PFS 64
#define IA64_ADDR_LC 65
#define IA64_ADDR_EC 66

#define IA64_ADDR_DCR 0
#define IA64_ADDR_ITM 1
#define IA64_ADDR_IVA 2
#define IA64_ADDR_PTA 8
#define IA64_ADDR_IPSR 16
#define IA64_ADDR_ISR 17
#define IA64_ADDR_IIP 19
#define IA64_ADDR_IFA 20
#define IA64_ADDR_ITIR 21
#define IA64_ADDR_IIPA 22
#define IA64_ADDR_IFS 23
#define IA64_ADDR_IIM 24
#define IA64_ADDR_IHA 25
#define IA64_ADDR_LID 64
#define IA64_ADDR_IVR 65
#define IA64_ADDR_TPR 66
#define IA64_ADDR_EOI 67
#define IA64_ADDR_IRR0 68
#define IA64_ADDR_IRR1 69
#define IA64_ADDR_IRR2 70
#define IA64_ADDR_IRR3 71
#define IA64_ADDR_ITV 72
#define IA64_ADDR_PMV 73
#define IA64_ADDR_CMCV 74
#define IA64_ADDR_LRR0 80
#define IA64_ADDR_LRR1 81

extern int IA64_ar_reserved[];
extern int IA64_ar_ignored[];
extern int IA64_cr_reserved[];

extern IA64_FR_t IA64_NatVAL;
extern IA64_FR_t IA64_QNaN;
extern IA64_FR_t IA64_Inf;
extern IA64_FR_t IA64_FPZero;
extern IA64_FR_t IA64_FPIntZero;
extern IA64_FR_t IA64_FPOne;

/* Maximum number of stacked registers */
#define IA64_RSE_N_STACKED_IMPL 2048

typedef struct {
  int tag;
  int len;
  LSE_emu_addr_t addr;
} IA64_ALAT_t;

typedef struct IA64_context_s {
  LSE_emu_contextno_t hwcno;
  class IA64_dinst_s *di;
  void *osinfo;
  LSE_device::device_t *mem;
  int memID;
  LSE_emu_addr_t startaddr;
  //  LSE_emu_addr_t psect_start;
  //  LSE_emu_addr_t psect_end;
  boolean done;

  /* used in checkpointing */
  unsigned int cpmemid;

  IA64_GR_t r[32+16]; /* fixed and banked registers */
  IA64_FR_t f[128];   /* floating point registers */
  char p[64];         /* predicates */
  uint64_t b[8];       /* branch registers */
  IA64_AR_t ar[128];  /* application registers */

  IA64_SR_t sr[2];    /* specials (see above for indices) */
  IA64_GR_t physr[IA64_RSE_N_STACKED_IMPL];
  int num_stacked_phys;
  
  /* ALAT */
  IA64_ALAT_t *ALAT;
  int ALAT_size;
  int ALAT_numentries;

  /* not part of CFM because interlocks differently... note that BOF 
   * is actually part of CFM structure; this makes it possible to 
   * modify the state for it directly.
   */
  struct {
    long NDirty;
    long NClean;
  } RSE_other;

} IA64_context_t;

extern int EMU_context_copy(LSE_emu_interface_t *ifc,
			    IA64_context_t **,IA64_context_t *);

/****************** Serialization flags *****************/

#define IA64_serialize_stopafter 1
#define IA64_serialize_instruction 2
#define IA64_serialize_data 4
#define IA64_serialize_flushwb 8
#define IA64_serialize_flushc 16
#define IA64_serialize_acceptance 32
#define IA64_serialize_acquire 64
#define IA64_serialize_release 128

/****************** Rollback types **********************/

#define IA64_rollback_ignore -1
#define IA64_rollback_empty 0
#define IA64_rollback_copy 1
#define IA64_rollback_bits 2
#define IA64_rollback_mem 3
#define IA64_rollback_preds 4
#define IA64_rollback_alat 5
#define IA64_rollback_bof 6

/****************** Interruptions (intr) *****************/

typedef enum {
  IA64_intr_none = 0,

  /* Architecture-defined */
  IA64_intr_instructionpagenotpresent = 23,

  IA64_intr_illegaloperation = 33,
  IA64_intr_breakinstruction = 35,
  IA64_intr_privilegedoperation = 36,
  IA64_intr_disabledfpregister = 37,

  IA64_intr_registernatconsumption = 41,
  IA64_intr_reservedregisterfield = 42,
  IA64_intr_privilegedregister = 44,
  IA64_intr_speculativeoperation = 45, 

  IA64_intr_unaligneddatareference = 60,

  IA64_intr_floatingpointfault = 68,

  IA64_intr_unimplementedinstructionaddress = 69,
  IA64_intr_floatingpointtrap = 70,
  IA64_intr_lowerprivilegetransfer = 71,
  IA64_intr_takenbranch = 72,
  IA64_intr_singlestep = 73,

  /* bounds */
  IA64_intr_lowest_interrupt = 2,
  IA64_intr_lowest_fault = 6,
  IA64_intr_lowest_trap = 69,

  /* emulator-defined stuff */
  IA64_intr_memory = 51,
  IA64_intr_notimplemented = 2, /* machine check */
  IA64_intr_rsenotimplemented = 6, /* IR unimplemented data address */

} IA64_intr_t;

/*extern const char *EMU_intr_string(int fault);*/

/* DO NOT CALL THE REPORT MACROS TO REPORT IA64_intr_none Also: if we
 * call report_potential_intr for some interrupt, need to call
 * report_intr or report_actual_intr later after checking same
 * condition if the predicate might be found to be true later.  If we
 * set potential in an early stage to a high priority interrupt but
 * then only attempt to set lower-priority interrupts in later stages,
 * we get an actual interrupt of lower priority than the potential.
 */
static inline void
report_potential_intr(LSE_emu_instr_info_t *ii, IA64_intr_t fno)
{
  if ((100 - fno) > ii->privatef.potential_intr)
    ii->privatef.potential_intr = fno;
}

static inline void
report_actual_intr(LSE_emu_instr_info_t *ii, IA64_intr_t fno)
{
  if ((100 - fno) > ii->privatef.actual_intr)
    ii->privatef.potential_intr = ii->privatef.actual_intr = fno;
}

/* should only call this after fetching predicate */
static void inline
report_intr(LSE_emu_instr_info_t *ii, IA64_intr_t nf) {
  if (ii->operand_val_src[LSE_emu_operand_name_src_qp].data.PR)
    report_actual_intr(ii,nf);
  else
    report_potential_intr(ii,nf);
}

extern int Linux_call_os(IA64_context_t *,
		         LSE_emu_instr_info_t *,
			 LSE_emu_iaddr_t,
			 LSE_emu_iaddr_t&,
		         int, uint64_t);

extern boolean Linux_is_os(IA64_context_t *,
			   LSE_emu_instr_info_t *,
			   int, uint64_t);

/*extern void EMU_do_all_steps(LSE_emu_instr_info_t *);*/

/*************** Register accessors *****************/

#define GR_FILE_SIZE 128
#define FR_FILE_SIZE 128
#define PR_FILE_SIZE 64
#define BR_FILE_SIZE 8
#define RRB_PR_ii(ii) ((long)rf_get((ii)->operand_val_src[0].data.SR.CFM.val,\
				    CFM_rrb_pr))
#define RRB_FR_ii(ii) ((long)rf_get((ii)->operand_val_src[0].data.SR.CFM.val,\
				    CFM_rrb_fr))
#define RRB_GR_ii(ii) ((long)rf_get((ii)->operand_val_src[0].data.SR.CFM.val,\
				     CFM_rrb_gr))
#define SOR_ii(ii)  ((long)rf_get((ii)->operand_val_src[0].data.SR.CFM.val, \
                                     CFM_sor))
#define SOL_ii(ii)  ((long)rf_get((ii)->operand_val_src[0].data.SR.CFM.val, \
                                     CFM_sol))
#define RRB_PR_ct(ct) ((long)rf_get((ct)->sr[IA64_ADDR_CFM].CFM.val,\
                                     CFM_rrb_pr))
#define RRB_FR_ct(ct)  ((long)rf_get((ct)->sr[IA64_ADDR_CFM].CFM.val,\
                                     CFM_rrb_fr))
#define RRB_GR_ct(ct)  ((long)rf_get((ct)->sr[IA64_ADDR_CFM].CFM.val,\
                                     CFM_rrb_gr))
#define SOR_ct(ct)  ((long)rf_get((ct)->sr[IA64_ADDR_CFM].CFM.val,CFM_sor))
#define SOL_ct(ct)  ((long)rf_get((ct)->sr[IA64_ADDR_CFM].CFM.val,CFM_sol))

#define MAX_FR_ROT_REGS 96
#define MAX_PR_ROT_REGS 48

#define IA64_RT_PR_NO(N,rrbpr) ((N < 16) ? N : \
    (((N + rrbpr) > (15+MAX_PR_ROT_REGS)) ? \
      (N + rrbpr - MAX_PR_ROT_REGS) : (N + rrbpr)))
#define IA64_RT_FR_NO(N,rrbfr)  ((N < 32) ? N : \
    (((N + rrbfr) > (31+MAX_FR_ROT_REGS)) ? \
     (N + rrbfr - MAX_FR_ROT_REGS) : (N + rrbfr)))
#define IA64_RT_GR_NO(N,rrbgr,sor) ((N < 32 || N > 31 + sor) ? N : \
   (((N + rrbgr) > (31 + sor)) ? (N + rrbgr - sor) : (N + rrbgr)))
#define IA64_RT_GRR_NO(N,rrbgr,sor) ((N > 31 + sor) ? N : \
   (((N + rrbgr) > (31 + sor)) ? (N + rrbgr - sor) : (N + rrbgr)))

#define get_p(ct,di,rno) ((ct)->p[IA64_RT_PR_NO((long)rno,RRB_PR_ii(di))])

#define IA64_GR_FAST_ROT(N,NR) ((N)>=(NR)?(N)-(NR):(N))

static inline void 
set_p(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno, char v) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  long erno;
  if (rno==0) return;
  erno = IA64_RT_PR_NO(rno,RRB_PR_ii(ii));
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback[opno].rtype = IA64_rollback_copy;
    ii->privatef.rollback[opno].data.copy.size=1;
    ii->privatef.rollback[opno].data.copy.host_addr=&ct->p[erno];
    ii->privatef.rollback[opno].data.copy.data[0] = ct->p[erno];
    ii->privatef.rollback_saved |= (1<<opno);
  }
  ct->p[erno]=v;
}

static inline void 
set_p_noroll(LSE_emu_instr_info_t *ii, long rno, char v) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  long erno;
  if (rno==0) return;
  erno = IA64_RT_PR_NO(rno,RRB_PR_ii(ii));
  ct->p[erno]=v;
}

#define get_p_norot(ct,di,rno) ((ct)->p[rno])
#define set_p_norot(ct,di,rno,v) if (rno>0) (ct)->p[rno]=(v)

static inline IA64_GR_t *get_r_point(IA64_context_t *ct, int rno,
					 long rrbgr, long sor, long bn, 
					 long bof) {
  if (rno < 32) return &ct->r[rno+(bn ? (rno&16) : 0)];
  else {
    /* must subtract 32 to maintain 0<=rno<num_phys*2 */
    rno = IA64_RT_GRR_NO(rno, rrbgr, sor)+bof-32; 
    rno = IA64_GR_FAST_ROT(rno,ct->num_stacked_phys);
    return &ct->physr[rno];
  }
}

// Used for operating system access to the general registers
static inline IA64_GR_t *get_r_ptr_ctx(IA64_context_t *ct, int rno) {
  long rrbgr = RRB_GR_ct(ct);
  long sor = SOR_ct(ct);
  long bn = rf_get(ct->sr[IA64_ADDR_PSR].PSR,PSR_bn);
  long bof = ct->sr[IA64_ADDR_CFM].CFM.BOF;
  return get_r_point(ct, rno, rrbgr, sor, bn, bof);
}

static inline int
get_r_pno(int rno, long rrbgr, long sor, long bof,
	  long bno, long numstacked) {
  if (rno < 32) {
    /* Oddly enough, this seems to be faster than shifting bn by 4 and
     * anding with rno ... probably because the
     * branch is strongly biased towards adding 0 */
    rno = rno + (bno ? (rno&16) : 0);
    return rno;
  } else {
    /* must subtract 32 to maintain 0<=rno<num_phys*2 */
    rno = (IA64_RT_GRR_NO(rno, rrbgr, sor)+ bof-32);
    rno = IA64_GR_FAST_ROT(rno,numstacked);
    return rno + 48;
  }
}

static inline int
get_r_pno_ii(LSE_emu_instr_info_t *ii, int rno) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  return get_r_pno(rno, RRB_GR_ii(ii), SOR_ii(ii)<<3,
		   ii->operand_val_src[0].data.SR.CFM.BOF,
		   rf_get(ii->operand_val_src[1].data.SR.PSR,PSR_bn),
		   realct->num_stacked_phys);
}

static inline IA64_GR_t *
get_r_ptr(IA64_context_t *ct, LSE_emu_instr_info_t *ii, long rno) {
  if (rno < 32) {
    /* Oddly enough, this seems to be faster than shifting bn by 4 and
     * anding with rno ... probably because the
     * branch is strongly biased towards adding 0 */
    rno = rno + (rf_get(ii->operand_val_src[1].data.SR.PSR,PSR_bn) ? 
		 (rno&16) : 0);
    return &ct->r[rno];
  } else {
    long rrbgr = RRB_GR_ii(ii);
    long sor = SOR_ii(ii)<<3;
    /* must subtract 32 to maintain 0<=rno<num_phys*2 */
    rno = (IA64_RT_GRR_NO(rno, rrbgr, sor)+
	   ii->operand_val_src[0].data.SR.CFM.BOF-32);
    rno = IA64_GR_FAST_ROT(rno,ct->num_stacked_phys);
    return &ct->physr[rno];
  }
}

#define get_r_nat(ct,ii,rno) (get_r_ptr(ct,ii,rno)->nat)
#define get_r_val(ct,ii,rno) (get_r_ptr(ct,ii,rno)->val)

static inline void 
set_r(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii,
      long rno, uint64_t v, char n) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *rptr;
  if (rno==0) return;
  rptr = get_r_ptr(ct,ii,rno);

  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_copy;
    ii->privatef.rollback[opno].data.copy.size=sizeof(IA64_GR_t);
    ii->privatef.rollback[opno].data.copy.host_addr=(void *)rptr;
    memcpy(ii->privatef.rollback[opno].data.copy.data,
	   rptr,sizeof(IA64_GR_t));
  }
  rptr->val = v;
  rptr->nat = n;
}

static inline void 
set_r_noroll(LSE_emu_instr_info_t *ii,
	     long rno, uint64_t v, char n) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *rptr;
  if (rno==0) return;
  rptr = get_r_ptr(ct,ii,rno);
  rptr->val = v;
  rptr->nat = n;
}

#define get_f(ct,di,rno) ((ct)->f[IA64_RT_FR_NO((long)rno,RRB_FR_ii(di))])

#define get_f_ptr(ct,di,rno) (&((ct)->f[IA64_RT_FR_NO((long)rno,\
						      RRB_FR_ii(di))]))

static inline void 
set_f(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno, 
      IA64_FR_t v) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  long erno = IA64_RT_FR_NO(rno,RRB_FR_ii(ii));
  if (rno<2) return;
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_copy;
    ii->privatef.rollback[opno].data.copy.size=sizeof(IA64_FR_t);
    ii->privatef.rollback[opno].data.copy.host_addr=&ct->f[erno];
    memcpy(ii->privatef.rollback[opno].data.copy.data,
	   &ct->f[erno],sizeof(IA64_FR_t));
  }
  ct->f[erno]=v;
}

#define get_sr(ct,di,rno) ((ct)->sr[rno])

static inline void 
set_sr(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno,
       IA64_SR_t v) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_copy;
    ii->privatef.rollback[opno].data.copy.size=sizeof(IA64_SR_t);
    ii->privatef.rollback[opno].data.copy.host_addr=&ct->sr[rno];
    memcpy(ii->privatef.rollback[opno].data.copy.data,
	   &ct->sr[rno],sizeof(IA64_SR_t));
  }
  ct->sr[rno] =v;
}

static inline void 
set_sr_bits(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno,
	    uint64_t v,
	    uint64_t mask) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t ov;
  ov = ct->sr[rno].PSR;
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_bits;
    ii->privatef.rollback[opno].data.bits.old = ov;
    ii->privatef.rollback[opno].data.bits.mask = mask;
    ii->privatef.rollback[opno].data.bits.host_addr=(uint64_t *)&ct->sr[rno];
  }
  ct->sr[rno].PSR =(v & mask) | (ov & ~mask);
}

/* TODO: these are sticky bits, and really do not roll back out of order */
static inline void 
set_sr_sbits(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno, 
	     uint64_t v, uint64_t mask) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t ov;
  ov = ct->sr[rno].PSR;
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_bits;
    ii->privatef.rollback[opno].data.bits.old = ov;
    ii->privatef.rollback[opno].data.bits.mask = mask;
    ii->privatef.rollback[opno].data.bits.host_addr=(uint64_t *)&ct->sr[rno];
  }
  ct->sr[rno].PSR=(v & mask) | ov;
}

#define get_ar(ct,di,rno) ((ct)->ar[rno])

static inline void 
set_ar(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno, uint64_t v) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_copy;
    ii->privatef.rollback[opno].data.copy.size=sizeof(IA64_AR_t);
    ii->privatef.rollback[opno].data.copy.host_addr=&ct->ar[rno];
    memcpy(ii->privatef.rollback[opno].data.copy.data,
	   &ct->ar[rno],sizeof(IA64_AR_t));
  }
  ct->ar[rno]=v;
}

static inline void 
set_ar_bits(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno, uint64_t v,
	    uint64_t mask) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t ov;

  ov = ct->ar[rno];
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_bits;
    ii->privatef.rollback[opno].data.bits.old = ov;
    ii->privatef.rollback[opno].data.bits.mask = mask;
    ii->privatef.rollback[opno].data.bits.host_addr=&ct->ar[rno];
  }
  ct->ar[rno]=(v & mask) | (ov & ~mask);
}

/* TODO: these are sticky bits, and really do not roll back out of order */
static inline void 
set_ar_sbits(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno, 
	     uint64_t v, uint64_t mask) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t ov;
  ov = ct->ar[rno];
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_bits;
    ii->privatef.rollback[opno].data.bits.old = ov;
    ii->privatef.rollback[opno].data.bits.mask = mask;
    ii->privatef.rollback[opno].data.bits.host_addr=&ct->ar[rno];
  }
  ct->ar[rno]=(v & mask) | ov;
}


#define get_b(ct,di,rno) ((ct)->b[rno])

static inline void 
set_b(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii, long rno, uint64_t v) 
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_copy;
    ii->privatef.rollback[opno].data.copy.size=8;
    ii->privatef.rollback[opno].data.copy.host_addr=&ct->b[rno];
    memcpy(ii->privatef.rollback[opno].data.copy.data,&ct->b[rno],8);
  }
  ct->b[rno]=v;
}

static inline void
save_rse(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii)
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_copy;
    ii->privatef.rollback[opno].data.copy.host_addr=&ct->RSE_other;
    ii->privatef.rollback[opno].data.copy.size = sizeof(ct->RSE_other);
    memcpy(ii->privatef.rollback[opno].data.copy.data,&ct->RSE_other,
	   sizeof(ct->RSE_other));
  }
}

static inline void
save_bof(LSE_emu_operand_name_t opno, LSE_emu_instr_info_t *ii)
{
  IA64_context_t *ct = (IA64_context_t *)(ii->swcontexttok);
  if (~ii->privatef.rollback_saved & (1<<opno)) {
    ii->privatef.rollback_saved |= (1<<opno);
    ii->privatef.rollback[opno].rtype = IA64_rollback_bof;
    /* also saves CFM */
    ii->privatef.rollback[opno].data.BOF.CFM = ct->sr[IA64_ADDR_CFM].CFM.val;
    ii->privatef.rollback[opno].data.BOF.BOF = ct->sr[IA64_ADDR_CFM].CFM.BOF;
  }
}

/********************* Decode information *****************************/

extern int bundle_types[32][3];
extern int bundle_stops[32][3];

#define IA64_MAX_DECODE_INFO 64

/* 3 extra, 10 A, 29 I, 48 M, 9 B, 16 F, 5 X, user */
#define IA64_NUM_USER_FORMATS 32
#define IA64_NUM_BASE_FORMATS (3+10+29+48+9+16+5)
#define IA64_NUM_FORMATS (IA64_NUM_BASE_FORMATS+IA64_NUM_USER_FORMATS)

typedef struct IA64_decode_s {
  int mask;
  int shift;
  struct {
    int formatno;  /* 0 = next level,
		      1   -  29  :  I formats I1 - I29
		      101 - 148  :  M formats M1 - M48
		      201 - 216  :  F formats F1 - F16
		      301 - 309  :  B formats B1 - B9
		      401 - 405  :  X formats X1 - X5
		      501 - 510  :  A formats A1 - A10
		      601 - 600+IA64_NUM_USER_FORMATS : U formats U1  - U..
		      -1 = reserved,
		      -2 = reserved PR,
		      -3 = reserved PR in B-unit
		      -4 = nop
		   */
    struct IA64_decode_s *nextlevelp;
  } info[IA64_MAX_DECODE_INFO];
} IA64_decode_t;

typedef void (*IA64_exec_step_t)(LSE_emu_instr_info_t *);
typedef void (*IA64_disasm_t)(LSE_emu_instr_info_t *,FILE *);
typedef void (*IA64_exec_step1_t)(LSE_emu_instr_info_t *, LSE_emu_operand_name_t, int);

typedef struct IA64_decode_details_s {
  struct {
    IA64_exec_step_t decode_func;
    IA64_exec_step1_t opfetch_func;
    IA64_exec_step_t evaluate_func;
    IA64_exec_step_t memory_func;
    IA64_exec_step_t format_func;
    IA64_exec_step1_t writeback_func;
    IA64_exec_step_t all_backend_func;
    IA64_disasm_t disassemble_func;
  } funcs;
} IA64_decode_details_t;

extern IA64_decode_t Itable,Mtable,Ftable,Btable,Ltable;
extern IA64_decode_t *decode_tables[];
extern IA64_decode_details_t decode_details[];

/* Any which do not pass beyond 32 bits can be cast to unsigned long and
 * use the "word" variety of macros; this will make the code faster on a 
 * 32-bit machine....
 */
#define all_qp(i) ((int)exfw((unsigned long)i,6,0))
#define all_r1(i) ((int)exfw((unsigned long)i,7,6))
#define all_r2(i) ((int)exfw((unsigned long)i,7,13))
#define all_r3(i) ((int)exfw((unsigned long)i,7,20))
#define all_f1(i) ((int)exfw((unsigned long)i,7,6))
#define all_f2(i) ((int)exfw((unsigned long)i,7,13))
#define all_f3(i) ((int)exfw((unsigned long)i,7,20))
#define all_f4(i) ((int)exf(i,7,27))
#define all_p1(i) ((int)exfw((unsigned long)i,6,6))
#define all_p2(i) ((int)exf(i,6,27))
#define all_sf(i) ((int)exf(i,2,34))
#define all_majorop(i) exf(i,4,37)
#define IA64_FPSR_FMASK (INT64_C(0x03f01f80fc07e000))

#define A1_x2b(i) exf(i,2,27)
#define A1_x4(i) exf(i,4,29)
#define A1_ve(i) exf(i,1,33)
#define A1_x2a(i) exf(i,2,34)

#define A2_ct2d(i) exf(i,2,27)
#define A2_x4(i) exf(i,4,29)
#define A2_ve(i) exf(i,1,33)
#define A2_x2a(i) exf(i,2,34)

#define A3_imm7b(i) exf(i,7,13)
#define A3_x2b(i) exf(i,2,27)
#define A3_x4(i) exf(i,4,29)
#define A3_ve(i) exf(i,1,33)
#define A3_x2a(i) exf(i,2,34)
#define A3_s(i) exf(i,1,36)

#define A4_imm7b(i) exf(i,7,13)
#define A4_imm6d(i) exf(i,6,27)
#define A4_ve(i) exf(i,1,33)
#define A4_x2a(i) exf(i,2,34)
#define A4_s(i) exf(i,1,36)

#define A5_imm7b(i) exf(i,7,13)
#define A5_r3(i) ((int)exf(i,2,20))
#define A5_imm5c(i) exf(i,5,22)
#define A5_imm9d(i) exf(i,9,27)
#define A5_s(i) exf(i,1,36)

#define A6_p1(i) ((int)exf(i,6,6))
#define A6_c(i) exf(i,1,12)
#define A6_p2(i) ((int)exf(i,6,27))
#define A6_ta(i) exf(i,1,33)
#define A6_x2(i) exf(i,2,34)
#define A6_tb(i) exf(i,1,36)

#define A7_p1(i) ((int)exf(i,6,6))
#define A7_c(i) exf(i,1,12)
#define A7_p2(i) ((int)exf(i,6,27))
#define A7_ta(i) exf(i,1,33)
#define A7_x2(i) exf(i,2,34)
#define A7_tb(i) exf(i,1,36)

#define A8_p1(i) ((int)exf(i,6,6))
#define A8_c(i) exf(i,1,12)
#define A8_imm7b(i) exf(i,7,13)
#define A8_p2(i) ((int)exf(i,6,27))
#define A8_ta(i) exf(i,1,33)
#define A8_x2(i) exf(i,2,34)
#define A8_s(i) exf(i,1,36)

#define A9_x2b(i) exf(i,2,27)
#define A9_x4(i) exf(i,4,29)
#define A9_zb(i) exf(i,1,33)
#define A9_x2a(i) exf(i,2,34)
#define A9_za(i) exf(i,1,36)

#define A10_ct2d(i) exf(i,2,27)
#define A10_x4(i) exf(i,4,29)
#define A10_zb(i) exf(i,1,33)
#define A10_x2a(i) exf(i,2,34)
#define A10_za(i) exf(i,1,36)


#define I1_x2b(i) exf(i,2,28)
#define I1_ct2d(i) exf(i,2,30)
#define I1_ve(i) exf(i,1,32)
#define I1_zb(i) exf(i,1,33)
#define I1_x2a(i) exf(i,2,34)
#define I1_za(i) exf(i,1,36)

#define I2_x2b(i) exf(i,2,28)
#define I2_x2c(i) exf(i,2,30)
#define I2_ve(i) exf(i,1,32)
#define I2_zb(i) exf(i,1,33)
#define I2_x2a(i) exf(i,2,34)
#define I2_za(i) exf(i,1,36)

#define I3_mbt4c(i) exf(i,4,20)
#define I3_x2b(i) exf(i,2,28)
#define I3_x2c(i) exf(i,2,30)
#define I3_ve(i) exf(i,1,32)
#define I3_zb(i) exf(i,1,33)
#define I3_x2a(i) exf(i,2,34)
#define I3_za(i) exf(i,1,36)

#define I4_mht8c(i) exf(i,8,20)
#define I4_x2b(i) exf(i,2,28)
#define I4_x2c(i) exf(i,2,30)
#define I4_ve(i) exf(i,1,32)
#define I4_zb(i) exf(i,1,33)
#define I4_x2a(i) exf(i,2,34)
#define I4_za(i) exf(i,1,36)

#define I5_x2b(i) exf(i,2,28)
#define I5_x2c(i) exf(i,2,30)
#define I5_ve(i) exf(i,1,32)
#define I5_zb(i) exf(i,1,33)
#define I5_x2a(i) exf(i,2,34)
#define I5_za(i) exf(i,1,36)

#define I6_count5b(i) exf(i,5,14)
#define I6_x2b(i) exf(i,2,28)
#define I6_x2c(i) exf(i,2,30)
#define I6_ve(i) exf(i,1,32)
#define I6_zb(i) exf(i,1,33)
#define I6_x2a(i) exf(i,2,34)
#define I6_za(i) exf(i,1,36)

#define I7_x2b(i) exf(i,2,28)
#define I7_x2c(i) exf(i,2,30)
#define I7_ve(i) exf(i,1,32)
#define I7_zb(i) exf(i,1,33)
#define I7_x2a(i) exf(i,2,34)
#define I7_za(i) exf(i,1,36)

#define I8_ccount5c(i) exf(i,5,20)
#define I8_x2b(i) exf(i,2,28)
#define I8_x2c(i) exf(i,2,30)
#define I8_ve(i) exf(i,1,32)
#define I8_zb(i) exf(i,1,33)
#define I8_x2a(i) exf(i,2,34)
#define I8_za(i) exf(i,1,36)

#define I9_x2b(i) exf(i,2,28)
#define I9_x2c(i) exf(i,2,30)
#define I9_ve(i) exf(i,1,32)
#define I9_zb(i) exf(i,1,33)
#define I9_x2a(i) exf(i,2,34)
#define I9_za(i) exf(i,1,36)

#define I10_count6d(i) exf(i,6,27)
#define I10_x(i) exf(i,1,33)
#define I10_x2(i) exf(i,2,34)

#define I11_y(i) exf(i,1,13)
#define I11_pos6b(i) exf(i,6,14)
#define I11_len6d(i) exf(i,6,27)
#define I11_x(i) exf(i,1,33)
#define I11_x2(i) exf(i,2,34)

#define I12_cpos6c(i) exf(i,6,20)
#define I12_y(i) exf(i,1,26)
#define I12_len6d(i) exf(i,6,27)
#define I12_x(i) exf(i,1,33)
#define I12_x2(i) exf(i,2,34)

#define I13_imm7b(i) exf(i,7,13)
#define I13_cpos6c(i) exf(i,6,20)
#define I13_y(i) exf(i,1,26)
#define I13_len6d(i) exf(i,6,27)
#define I13_x(i) exf(i,1,33)
#define I13_x2(i) exf(i,2,34)
#define I13_s(i) exf(i,1,36)

#define I14_cpos6b(i) exf(i,6,14)
#define I14_len6d(i) exf(i,6,27)
#define I14_x(i) exf(i,1,33)
#define I14_x2(i) exf(i,2,34)
#define I14_s(i) exf(i,1,36)

#define I15_len4d(i) exf(i,4,27)
#define I15_cpos6d(i) exf(i,6,31)

#define I16_p1(i) ((int)exf(i,6,6))
#define I16_c(i) exf(i,1,12)
#define I16_y(i) exf(i,1,13)
#define I16_pos6b(i) exf(i,6,14)
#define I16_p2(i) ((int)exf(i,6,27))
#define I16_ta(i) exf(i,1,33)
#define I16_x2(i) exf(i,2,34)
#define I16_tb(i) exf(i,1,36)

#define I17_p1(i) ((int)exf(i,6,6))
#define I17_c(i) exf(i,1,12)
#define I17_y(i) exf(i,1,13)
#define I17_p2(i) ((int)exf(i,6,27))
#define I17_ta(i) exf(i,1,33)
#define I17_x2(i) exf(i,2,34)
#define I17_tb(i) exf(i,1,36)

#define I18_imm20a(i) exf(i,20,6)
#define I18_x6(i) exf(i,6,27)
#define I18_x3(i) exf(i,3,33)
#define I18_y(i) exf(i,1,26)
#define I18_i(i) exf(i,1,36)

#define I19_imm20a(i) exf(i,20,6)
#define I19_x6(i) exf(i,6,27)
#define I19_x3(i) exf(i,3,33)
#define I19_i(i) exf(i,1,36)

#define I20_imm7a(i) exf(i,7,6)
#define I20_imm13c(i) exf(i,13,20)
#define I20_x3(i) exf(i,3,33)
#define I20_s(i) exf(i,1,36)

#define I21_b1(i) ((int)exf(i,3,6))
#define I21_wh(i) exf(i,2,20)
#define I21_x(i) exf(i,1,22)
#define I21_ih(i) exf(i,1,23)
#define I21_timm9c(i) exf(i,9,24)
#define I21_x3(i) exf(i,3,33)

#define I22_b2(i) ((int)exf(i,3,13))
#define I22_x6(i) exf(i,6,27)
#define I22_x3(i) exf(i,3,33)

#define I23_mask7a(i) exf(i,7,6)
#define I23_mask8c(i) exf(i,8,24)
#define I23_x3(i) exf(i,3,33)
#define I23_s(i) exf(i,1,36)

#define I24_imm27a(i) exf(i,27,6)
#define I24_x3(i) exf(i,3,33)
#define I24_s(i) exf(i,1,36)

#define I25_x6(i) exf(i,6,27)
#define I25_x3(i) exf(i,3,33)

#define I26_ar3(i) ((int)exf(i,7,20))
#define I26_x6(i) exf(i,6,27)
#define I26_x3(i) exf(i,3,33)

#define I27_imm7b(i) exf(i,7,13)
#define I27_ar3(i) ((int)exf(i,7,20))
#define I27_x6(i) exf(i,6,27)
#define I27_x3(i) exf(i,3,33)
#define I27_s(i) exf(i,1,36)

#define I28_ar3(i) ((int)exf(i,7,20))
#define I28_x6(i) exf(i,6,27)
#define I28_x3(i) exf(i,3,33)

#define I29_x6(i) exf(i,6,27)
#define I29_x3(i) exf(i,3,33)


#define M1_x(i) exf(i,1,27)
#define M1_hint(i) exf(i,2,28)
#define M1_x6(i) exf(i,6,30)
#define M1_m(i) exf(i,1,36)

#define M2_x(i) exf(i,1,27)
#define M2_hint(i) exf(i,2,28)
#define M2_x6(i) exf(i,6,30)
#define M2_m(i) exf(i,1,36)

#define M3_imm7b(i) exf(i,7,13)
#define M3_i(i) exf(i,1,27)
#define M3_hint(i) exf(i,2,28)
#define M3_x6(i) exf(i,6,30)
#define M3_s(i) exf(i,1,36)

#define M4_x(i) exf(i,1,27)
#define M4_hint(i) exf(i,2,28)
#define M4_x6(i) exf(i,6,30)
#define M4_m(i) exf(i,1,36)

#define M5_imm7a(i) exf(i,7,6)
#define M5_i(i) exf(i,1,27)
#define M5_hint(i) exf(i,2,28)
#define M5_x6(i) exf(i,6,30)
#define M5_s(i) exf(i,1,36)

#define M6_x(i) exf(i,1,27)
#define M6_hint(i) exf(i,2,28)
#define M6_x6(i) exf(i,6,30)
#define M6_m(i) exf(i,1,36)

#define M7_x(i) exf(i,1,27)
#define M7_hint(i) exf(i,2,28)
#define M7_x6(i) exf(i,6,30)
#define M7_m(i) exf(i,1,36)

#define M8_imm7b(i) exf(i,7,13)
#define M8_i(i) exf(i,1,27)
#define M8_hint(i) exf(i,2,28)
#define M8_x6(i) exf(i,6,30)
#define M8_s(i) exf(i,1,36)

#define M9_x(i) exf(i,1,27)
#define M9_hint(i) exf(i,2,28)
#define M9_x6(i) exf(i,6,30)
#define M9_m(i) exf(i,1,36)

#define M10_imm7a(i) exf(i,7,6)
#define M10_i(i) exf(i,1,27)
#define M10_hint(i) exf(i,2,28)
#define M10_x6(i) exf(i,6,30)
#define M10_s(i) exf(i,1,36)

#define M11_x(i) exf(i,1,27)
#define M11_hint(i) exf(i,2,28)
#define M11_x6(i) exf(i,6,30)
#define M11_m(i) exf(i,1,36)

#define M12_x(i) exf(i,1,27)
#define M12_hint(i) exf(i,2,28)
#define M12_x6(i) exf(i,6,30)
#define M12_m(i) exf(i,1,36)

#define M13_x(i) exf(i,1,27)
#define M13_hint(i) exf(i,2,28)
#define M13_x6(i) exf(i,6,30)
#define M13_m(i) exf(i,1,36)

#define M14_x(i) exf(i,1,27)
#define M14_hint(i) exf(i,2,28)
#define M14_x6(i) exf(i,6,30)
#define M14_m(i) exf(i,1,36)

#define M15_imm7b(i) exf(i,7,13)
#define M15_i(i) exf(i,1,27)
#define M15_hint(i) exf(i,2,28)
#define M15_x6(i) exf(i,6,30)
#define M15_s(i) exf(i,1,36)

#define M16_x(i) exf(i,1,27)
#define M16_hint(i) exf(i,2,28)
#define M16_x6(i) exf(i,6,30)
#define M16_m(i) exf(i,1,36)

#define M17_i2b(i) exf(i,2,13)
#define M17_s(i) exf(i,1,15)
#define M17_x(i) exf(i,1,27)
#define M17_hint(i) exf(i,2,28)
#define M17_x6(i) exf(i,6,30)
#define M17_m(i) exf(i,1,36)

#define M18_x(i) exf(i,1,27)
#define M18_x6(i) exf(i,6,30)
#define M18_m(i) exf(i,1,36)

#define M19_x(i) exf(i,1,27)
#define M19_x6(i) exf(i,6,30)
#define M19_m(i) exf(i,1,36)

#define M20_imm7a(i) exf(i,7,6)
#define M20_imm13c(i) exf(i,13,20)
#define M20_x3(i) exf(i,3,33)
#define M20_s(i) exf(i,1,36)

#define M21_imm7a(i) exf(i,7,6)
#define M21_imm13c(i) exf(i,13,20)
#define M21_x3(i) exf(i,3,33)
#define M21_s(i) exf(i,1,36)

#define M22_imm20b(i) exf(i,20,13)
#define M22_x3(i) exf(i,3,33)
#define M22_s(i) exf(i,1,36)

#define M23_imm20b(i) exf(i,20,13)
#define M23_x3(i) exf(i,3,33)
#define M23_s(i) exf(i,1,36)

#define M24_x4(i) exf(i,4,27)
#define M24_x2(i) exf(i,2,31)
#define M24_x3(i) exf(i,3,33)

#define M25_x4(i) exf(i,4,27)
#define M25_x2(i) exf(i,2,31)
#define M25_x3(i) exf(i,3,33)

#define M26_x4(i) exf(i,4,27)
#define M26_x2(i) exf(i,2,31)
#define M26_x3(i) exf(i,3,33)

#define M27_x4(i) exf(i,4,27)
#define M27_x2(i) exf(i,2,31)
#define M27_x3(i) exf(i,3,33)

#define M28_x6(i) exf(i,6,27)
#define M28_x3(i) exf(i,3,33)

#define M29_ar3(i) ((int)exf(i,7,20))
#define M29_x6(i) exf(i,6,27)
#define M29_x3(i) exf(i,3,33)

#define M30_imm7b(i) exf(i,7,13)
#define M30_ar3(i) ((int)exf(i,7,20))
#define M30_x6(i) exf(i,6,27)
#define M30_x3(i) exf(i,3,33)
#define M30_s(i) exf(i,1,36)

#define M31_ar3(i) ((int)exf(i,7,20))
#define M31_x6(i) exf(i,6,27)
#define M31_x3(i) exf(i,3,33)

#define M32_cr3(i) ((int)exf(i,7,20))
#define M32_x6(i) exf(i,6,27)
#define M32_x3(i) exf(i,3,33)

#define M33_cr3(i) ((int)exf(i,7,20))
#define M33_x6(i) exf(i,6,27)
#define M33_x3(i) exf(i,3,33)

#define M34_sof(i) ((int)exf(i,7,13))
#define M34_sol(i) ((int)exf(i,7,20))
#define M34_sor(i) ((int)exf(i,4,27))
#define M34_x3(i) exf(i,3,33)

#define M35_x6(i) exf(i,6,27)
#define M35_x3(i) exf(i,3,33)

#define M36_x6(i) exf(i,6,27)
#define M36_x3(i) exf(i,3,33)

#define M37_imm20a(i) exf(i,20,6)
#define M37_x4(i) exf(i,4,27)
#define M37_x2(i) exf(i,2,31)
#define M37_x3(i) exf(i,3,33)
#define M37_i(i) exf(i,1,36)

#define M38_x6(i) exf(i,6,27)
#define M38_x3(i) exf(i,3,33)

#define M39_i2b(i) exf(i,2,13)
#define M39_x6(i) exf(i,6,27)
#define M39_x3(i) exf(i,3,33)

#define M40_i2b(i) exf(i,2,13)
#define M40_x6(i) exf(i,6,27)
#define M40_x3(i) exf(i,3,33)

#define M41_x6(i) exf(i,6,27)
#define M41_x3(i) exf(i,3,33)

#define M42_x6(i) exf(i,6,27)
#define M42_x3(i) exf(i,3,33)

#define M43_x6(i) exf(i,6,27)
#define M43_x3(i) exf(i,3,33)

#define M44_imm21a(i) exf(i,21,6)
#define M44_x4(i) exf(i,4,27)
#define M44_i2d(i) exf(i,2,31)
#define M44_x3(i) exf(i,3,33)
#define M44_i(i) exf(i,1,36)

#define M45_x6(i) exf(i,6,27)
#define M45_x3(i) exf(i,3,33)

#define M46_x6(i) exf(i,6,27)
#define M46_x3(i) exf(i,3,33)

#define M48_imm20a(i) exf(i,20,6)
#define M48_x4(i) exf(i,4,27)
#define M48_x2(i) exf(i,2,31)
#define M48_x3(i) exf(i,3,33)
#define M48_i(i) exf(i,1,36)
#define M48_y(i) exf(i,1,26)

#define B1_btype(i) exf(i,3,6)
#define B1_p(i) exf(i,1,12)
#define B1_imm20b(i) exf(i,20,13)
#define B1_wh(i) exf(i,2,33)
#define B1_d(i) exf(i,1,35)
#define B1_s(i) exf(i,1,36)

#define B2_btype(i) exf(i,3,6)
#define B2_p(i) exf(i,1,12)
#define B2_imm20b(i) exf(i,20,13)
#define B2_wh(i) exf(i,2,33)
#define B2_d(i) exf(i,1,35)
#define B2_s(i) exf(i,1,36)

#define B3_b1(i) ((int)exf(i,3,6))
#define B3_p(i) exf(i,1,12)
#define B3_imm20b(i) exf(i,20,13)
#define B3_wh(i) exf(i,2,33)
#define B3_d(i) exf(i,1,35)
#define B3_s(i) exf(i,1,36)

#define B4_btype(i) exf(i,3,6)
#define B4_p(i) exf(i,1,12)
#define B4_b2(i) ((int)exf(i,3,13))
#define B4_x6(i) exf(i,6,27)
#define B4_wh(i) exf(i,2,33)
#define B4_d(i) exf(i,1,35)

#define B5_b1(i) ((int)exf(i,3,6))
#define B5_p(i) exf(i,1,12)
#define B5_b2(i) ((int)exf(i,3,13))
#define B5_wh(i) exf(i,3,32)
#define B5_d(i) exf(i,1,35)

#define B6_wh(i) exf(i,2,3)
#define B6_timm7a(i) exf(i,7,6)
#define B6_imm20b(i) exf(i,20,13)
#define B6_t2e(i) exf(i,2,33)
#define B6_ih(i) exf(i,1,35)
#define B6_s(i) exf(i,1,36)

#define B7_wh(i) exf(i,2,3)
#define B7_timm7a(i) exf(i,7,6)
#define B7_b2(i) ((int)exf(i,3,13))
#define B7_x6(i) exf(i,6,27)
#define B7_t2e(i) exf(i,2,33)
#define B7_ih(i) exf(i,1,35)

#define B8_x6(i) exf(i,6,27)

#define B9_imm20a(i) exf(i,20,6)
#define B9_x6(i) exf(i,6,27)
#define B9_i(i) exf(i,1,33)

 
#define F1_sf(i) exf(i,2,34)
#define F1_x(i) exf(i,1,36)

#define F2_x2(i) exf(i,2,34)
#define F2_x(i) exf(i,1,36)

#define F3_x(i) exf(i,1,36)
 
#define F4_p1(i) ((int)exf(i,6,6))
#define F4_ta(i) exf(i,1,12)
#define F4_p2(i) ((int)exf(i,6,27))
#define F4_ra(i) exf(i,1,33)
#define F4_sf(i) exf(i,2,34)
#define F4_rb(i) exf(i,1,36)

#define F5_p1(i) ((int)exf(i,6,6))
#define F5_ta(i) exf(i,1,12)
#define F5_fclass7c(i) exf(i,7,20)
#define F5_p2(i) ((int)exf(i,6,27))
#define F5_fc2(i) exf(i,2,33)

#define F6_p2(i) ((int)exf(i,6,27))
#define F6_x(i) exf(i,1,33)
#define F6_sf(i) exf(i,2,34)
#define F6_q(i) exf(i,1,36)

#define F7_p2(i) ((int)exf(i,6,27))
#define F7_x(i) exf(i,1,33)
#define F7_sf(i) exf(i,2,34)
#define F7_q(i) exf(i,1,36)

#define F8_x6(i) exf(i,6,27)
#define F8_x(i) exf(i,1,33)
#define F8_sf(i) exf(i,2,34)

#define F9_x6(i) exf(i,6,27)
#define F9_x(i) exf(i,1,33)

#define F10_x6(i) exf(i,6,27)
#define F10_x(i) exf(i,1,33)
#define F10_sf(i) exf(i,2,34)

#define F11_x6(i) exf(i,6,27)
#define F11_x(i) exf(i,1,33)

#define F12_amask7b(i) exf(i,7,13)
#define F12_omask7c(i) exf(i,7,20)
#define F12_x6(i) exf(i,6,27)
#define F12_x(i) exf(i,1,33)
#define F12_sf(i) exf(i,2,34)

#define F13_x6(i) exf(i,6,27)
#define F13_x(i) exf(i,1,33)
#define F13_sf(i) exf(i,2,34)

#define F14_imm20a(i) exf(i,20,6)
#define F14_x6(i) exf(i,6,27)
#define F14_x(i) exf(i,1,33)
#define F14_sf(i) exf(i,2,34)
#define F14_s(i) exf(i,1,36)

#define F15_imm20a(i) exf(i,20,6)
#define F15_x6(i) exf(i,6,27)
#define F15_x(i) exf(i,1,33)
#define F15_i(i) exf(i,1,36)

#define F16_imm20a(i) exf(i,20,6)
#define F16_x6(i) exf(i,6,27)
#define F16_x(i) exf(i,1,33)
#define F16_i(i) exf(i,1,36)
#define F16_y(i) exf(i,1,26)

#define X1_imm20a(i) exf(i,20,6)
#define X1_x6(i) exf(i,6,27)
#define X1_x3(i) exf(i,3,33)
#define X1_i(i) exf(i,1,36)

#define X2_imm7b(i) exf(i,7,13)
#define X2_vc(i) exf(i,1,20)
#define X2_ic(i) exf(i,1,21)
#define X2_imm5c(i) exf(i,5,22)
#define X2_imm9d(i) exf(i,9,27)
#define X2_i(i) exf(i,1,36)

#define X3_btype(i) exf(i,3,6)
#define X3_p(i) exf(i,1,12)
#define X3_imm20b(i) exf(i,20,13)
#define X3_wh(i) exf(i,2,33)
#define X3_d(i) exf(i,1,35)
#define X3_i(i) exf(i,1,36)

#define X4_b1(i) ((int)exf(i,3,6))
#define X4_p(i) exf(i,1,12)
#define X4_imm20b(i) exf(i,20,13)
#define X4_wh(i) exf(i,2,33)
#define X4_d(i) exf(i,1,35)
#define X4_i(i) exf(i,1,36)

#define X5_imm20a(i) exf(i,20,6)
#define X5_x6(i) exf(i,6,27)
#define X5_x(i) exf(i,1,33)
#define X5_i(i) exf(i,1,36)
#define X5_y(i) exf(i,1,26)

/************* operand definitions **********/

#define src_cfm        LSE_emu_operand_name_src_cfm
#define src_psr        LSE_emu_operand_name_src_psr
#define src_qp         LSE_emu_operand_name_src_qp

#define src_op1        LSE_emu_operand_name_src_op1
#define src_base       LSE_emu_operand_name_src_base
#define src_iip        LSE_emu_operand_name_src_iip

#define src_op2        LSE_emu_operand_name_src_op2
#define src_update_amt LSE_emu_operand_name_src_update_amt
#define src_branchoff  LSE_emu_operand_name_src_branchoff
#define src_ifs        LSE_emu_operand_name_src_ifs

#define src_op3        LSE_emu_operand_name_src_op3
#define src_store_data LSE_emu_operand_name_src_store_data
#define src_bspstore   LSE_emu_operand_name_src_bspstore
#define src_ipsr       LSE_emu_operand_name_src_ipsr
#define src_load       LSE_emu_operand_name_src_load

#define src_op4        LSE_emu_operand_name_src_op4
#define src_rnat       LSE_emu_operand_name_src_rnat
#define src_lc         LSE_emu_operand_name_src_lc
#define src_unat       LSE_emu_operand_name_src_unat
#define src_ccv        LSE_emu_operand_name_src_ccv
#define src_itir       LSE_emu_operand_name_src_itir
#define src_tpr        LSE_emu_operand_name_src_tpr

#define src_fpsr       LSE_emu_operand_name_src_fpsr
#define src_ec         LSE_emu_operand_name_src_ec
#define src_rsc        LSE_emu_operand_name_src_rsc
#define src_pfs        LSE_emu_operand_name_src_pfs
#define src_ifa        LSE_emu_operand_name_src_ifa
#define src_dcr        LSE_emu_operand_name_src_dcr
#define src_pta        LSE_emu_operand_name_src_pta

#define src_bsp        LSE_emu_operand_name_src_bsp
#define src_alat       LSE_emu_operand_name_src_alat

#define src_rse        LSE_emu_operand_name_src_rse

#define dest_result     LSE_emu_operand_name_dest_result
#define dest_istrue     LSE_emu_operand_name_dest_istrue

#define dest_result2    LSE_emu_operand_name_dest_result2
#define dest_isfalse    LSE_emu_operand_name_dest_isfalse
#define dest_rsemem     LSE_emu_operand_name_dest_rsemem
#define dest_store      LSE_emu_operand_name_dest_store

#define dest_unat       LSE_emu_operand_name_dest_unat
#define dest_bspstore   LSE_emu_operand_name_dest_bspstore
#define dest_irr0       LSE_emu_operand_name_dest_irr0

#define dest_bsp        LSE_emu_operand_name_dest_bsp
#define dest_lc         LSE_emu_operand_name_dest_lc
#define dest_fpsr       LSE_emu_operand_name_dest_fpsr
#define dest_irr1       LSE_emu_operand_name_dest_irr1

#define dest_alatupdate LSE_emu_operand_name_dest_alatupdate
#define dest_irr2       LSE_emu_operand_name_dest_irr2

#define dest_baseupdate LSE_emu_operand_name_dest_baseupdate
#define dest_rnat       LSE_emu_operand_name_dest_rnat
#define dest_ec         LSE_emu_operand_name_dest_ec
#define dest_pfs        LSE_emu_operand_name_dest_pfs
#define dest_irr3       LSE_emu_operand_name_dest_irr3
#define dest_ifs        LSE_emu_operand_name_dest_ifs

#define dest_cfm        LSE_emu_operand_name_dest_cfm
#define dest_tpr        LSE_emu_operand_name_dest_tpr

#define dest_psr        LSE_emu_operand_name_dest_psr

#define dest_rse        LSE_emu_operand_name_dest_rse
#define dest_bof        LSE_emu_operand_name_dest_cfm

#define int_defer       LSE_emu_operand_name_int_defer
#define int_updatepred  LSE_emu_operand_name_int_updatepred

#define int_memerr      LSE_emu_operand_name_int_memerr

#define int_alatcheck   LSE_emu_operand_name_int_alatcheck
#define int_comparison  LSE_emu_operand_name_int_comparison
#define int_semupdate   LSE_emu_operand_name_int_semupdate

/************* implementation dependencies (that aren't variables) **********/

#define IA64_implemented_long_branch (1)
#define IA64_swa_on_input_unnorms (0)
#define IA64_swa_on_recip_range (0)
#define IA64_swa_on_output_huge (0)
#define IA64_swa_on_output_tiny (0)
#define IA64_implemented_check_branch_fchkf (1)
#define IA64_implemented_check_branch_float (1)
#define IA64_implemented_check_branch_general (1)

#define IA64_unimplemented_va(ip) (0)
#define IA64_unimplemented_pa(ip) (0)

typedef LSE_device::devmemory<20,3> memory_t;

typedef class IA64_dinst_s {
 public:
  /** interface information **/
  LSE_emu_interface_t *einterface;
  void *ostoken;  /* os link */
  memory_t *mem;

  /* checkpointing information */
  std::map<int, IA64_context_t *> mappedCtx;

  /** execution options **/

  int EMU_trace_syscalls;
  int EMU_debugcontextload;
  bool IA64_rsedebugprint;
  bool EMU_do_rollback;

  LSE_env::env_t *evars;
  IA64_dinst_s(LSE_emu_interface_t *i) 
    : einterface(i), EMU_trace_syscalls(false), 
    EMU_debugcontextload(false), IA64_rsedebugprint(false),
    EMU_do_rollback(true)
  {
    evars = new LSE_env::env_t();
    mem = new memory_t;
    ostoken = 0;
  }
  ~IA64_dinst_s() {
    mem->decr();
    delete evars;
  }

} IA64_dinst_t;

} // namespace LSE_IA64

#endif /* _LSE_IA64_H_ */
