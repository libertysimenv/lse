/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Execution functions for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Operand fetch, evaluation, and writeback steps for the IA64 emulator
 *
 * TODO: 
 *      - missing entirely: M28, M32, M33, M35, M36, M44
 *      - probes: M38, M39, M40
 *      - VM-related: M28, M41, M42, M43, M45, M46
 *      - M25: loadrs
 *      - M29/M30/M31: additional privilege level checks
 *      - B4: br.ia - are we going to support IA32 instructions? NOT FOR NOW
 *      - B8: rfi, TLB stuff for epc, CR[IFS] stuff for cover
 *      - all loads: opfetch for src_dcr
 *      - F6-F7: correct operand limits causing SWA in both parallel and normal
 *               reciprocal operations.
 *               This information is not in manuals..... need to ask Intel
 *      - memops: bias stuff/cache hints in memory ops
 *                figure out the mysterious natd_gr_write/read
 *                alignment checks
 *                endianness controlled by UM.be
 *                ma_is_speculative function
 *                virtual memory work?
 *      - RSE:    endianness
 *                undoing speculation which messes up clean registers?
 *      - extra information fields on interruptions
 *      - interrupt-related CRs (actually, all CRs)
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "LSE_IA64.h"
#include "IA64_fp.h"

namespace LSE_IA64 {
/* #define DEBUG_ALAT */

/****************** Various functionality macros ******************/

/* Always check target before any FP stuff... */
#define check_target_register(di,rno) { \
    if ((rno) > 32+((int)rf_get(get_src_op(di,src_cfm,SR.CFM.val),\
        CFM_sof))-1) { \
      report_intr(di,IA64_intr_illegaloperation); \
    } \
  }

#define fp_check_target_register(ct,di,rno) { \
    if (rno==0 || rno==1) { \
      report_intr(di,IA64_intr_illegaloperation); \
    } \
  }

#define fp_check_disabled_register(r,d,p,rno) { \
  if ((rf_get(p,PSR_dfh) && rno>31) ||          \
      (rf_get(p,PSR_dfl) && rno<32 && rno>2)) { \
    report_intr(d,IA64_intr_disabledfpregister); \
  } \
}

#define get_src_op(di,op,f) (di->operand_val_src[op].data.f)

#define set_src_op(di,op,f,v) (di->operand_val_src[op].valid=1, \
                               di->operand_val_src[op].data.f = v)

#define get_src_op_f(i,o,t) \
   ( t.significand = i->operand_val_src[o].data.FR.significand, \
     t.exponent    = i->operand_val_src[o].data.FR.exponent,    \
     t.sign        = i->operand_val_src[o].data.FR.sign, t )

#define get_src_op_psr(i,o,t) \
   ( t.significand = i->operand_val_src[o].data.FR.significand, \
     t.exponent    = i->operand_val_src[o].data.FR.exponent,    \
     t.sign        = i->operand_val_src[o].data.FR.sign, t )

#define set_src_op_f(i,o,t) \
   ( i->operand_val_src[o].valid=1, \
     i->operand_val_src[o].data.FR.significand = (t).significand, \
     i->operand_val_src[o].data.FR.exponent    = (t).exponent,    \
     i->operand_val_src[o].data.FR.sign        = (t).sign)

#define set_src_op_r(i,o,t) \
   ( i->operand_val_src[o].valid=1, \
     i->operand_val_src[o].data.GR.val = (t).val, \
     i->operand_val_src[o].data.GR.nat    = (t).nat)

#define set_src_op_rse(i,o,d,c) \
   ( i->operand_val_src[o].valid=1, \
     i->operand_val_src[o].data.RSE_other.NDirty      = d,      \
     i->operand_val_src[o].data.RSE_other.NClean      = c       )

#define get_dest_op(di,op,f) (di->operand_val_dest[op].data.f)

#define set_dest_op(di,op,f,v) (di->operand_val_dest[op].valid=1, \
                                di->operand_val_dest[op].data.f = v)
#define set_dest_op_rsechange(di,op,r,w) (di->operand_val_dest[op].valid=1, \
                        di->operand_val_dest[op].data.rsechange.numregs = r, \
                      di->operand_val_dest[op].data.rsechange.numwords = w)

#define get_dest_op_f(i,o,t) \
   ( t.significand = i->operand_val_dest[o].data.FR.significand, \
     t.exponent    = i->operand_val_dest[o].data.FR.exponent,    \
     t.sign        = i->operand_val_dest[o].data.FR.sign, t )

#define set_dest_op_f(i,o,t) \
   ( i->operand_val_dest[o].valid=1, \
     i->operand_val_dest[o].data.FR.significand = t.significand, \
     i->operand_val_dest[o].data.FR.exponent    = t.exponent,    \
     i->operand_val_dest[o].data.FR.sign        = t.sign, t)

#define set_dest_op_rse(i,o,d,c) \
   ( i->operand_val_dest[o].valid=1, \
     i->operand_val_dest[o].data.RSE_other.NDirty      = d,      \
     i->operand_val_dest[o].data.RSE_other.NClean      = c,      d)

#define get_int_op(di,op,f) (di->operand_val_int[op].data.f)

#define set_int_op(di,op,f,v) (di->operand_val_int[op].valid=1, \
                                di->operand_val_int[op].data.f = v)

#define get_int_op_f(i,o,t) \
   ( t.significand = i->operand_val_int[o].data.FR.significand, \
     t.exponent    = i->operand_val_int[o].data.FR.exponent,    \
     t.sign        = i->operand_val_int[o].data.FR.sign, t )

#define set_int_op_f(i,o,t) \
   ( i->operand_val_int[o].valid=1, \
     i->operand_val_int[o].data.FR.significand = t.significand, \
     i->operand_val_int[o].data.FR.exponent    = t.exponent,    \
     i->operand_val_int[o].data.FR.sign        = t.sign, t)

#define intr_cancels_instr(ii) \
   (ii->privatef.actual_intr && ii->privatef.actual_intr<69)

/*********************** ALAT functions ***************************/
/* our ALAT uses the same number of physical registers as the RSE for
 * general registers.  This makes it much easier to manage...
 */

static inline 
int alat_get_gr_tag(LSE_emu_instr_info_t *ii, int rno) {
  return 128 + get_r_pno_ii(ii,rno); /* just use physical # + 128 */
}

static inline 
int alat_get_fr_tag(LSE_emu_instr_info_t *ii, int rno) {
  return IA64_RT_FR_NO(rno,RRB_FR_ii(ii));
}

static int 
alat_check_gr(LSE_emu_instr_info_t *ii, int rno, LSE_emu_addr_t *addr, 
	      int *len) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int p = alat_get_gr_tag(ii,rno);
  int i;
  for (i=realct->ALAT_numentries-1;i>=0;i--) {
    if (realct->ALAT[i].tag == p) {
      *addr = realct->ALAT[i].addr;
      *len = realct->ALAT[i].len;
      return 1;
    }
  }
  return 0;
}

static int 
alat_check_fr(LSE_emu_instr_info_t *ii, int rno, LSE_emu_addr_t *addr,
	      int *len) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int p = alat_get_fr_tag(ii,rno);
  int i;
  for (i=realct->ALAT_numentries-1;i>=0;i--) {
    if (realct->ALAT[i].tag == p) {
      *addr = realct->ALAT[i].addr;
      *len = realct->ALAT[i].len;
      return 1;
    }
  }
  return 0;
}

static void 
alat_inval_gr(LSE_emu_instr_info_t *ii, int rno) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int p = alat_get_gr_tag(ii,rno);
  int i;
  /* no undo of invalidations */
  for (i=realct->ALAT_numentries-1;i>=0;i--) {
    if (realct->ALAT[i].tag == p) {
      realct->ALAT[i] = realct->ALAT[realct->ALAT_numentries-1];
      realct->ALAT_numentries--;
#ifdef DEBUG_ALAT
      fprintf(stderr,"ALAT: Remove ALAT entry for GR %d/%d\n",
	      rno,p);
#endif
      return;
    }
  }
  return;
}

static void 
alat_inval_fr(LSE_emu_instr_info_t *ii, int rno) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int p = alat_get_fr_tag(ii,rno);
  int i;
  /* no undo of invalidations */
  for (i=realct->ALAT_numentries-1;i>=0;i--) {
    if (realct->ALAT[i].tag == p) {
      realct->ALAT[i] = realct->ALAT[realct->ALAT_numentries-1];
      realct->ALAT_numentries--;
#ifdef DEBUG_ALAT
      fprintf(stderr,"ALAT: Remove ALAT entry for FR %d/%d\n",
	      rno,p);
#endif
      return;
    }
  }
  return;
}

static void
alat_inval_all(LSE_emu_instr_info_t *ii) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  /* no undo of invalidations */
  realct->ALAT_numentries = 0;
#ifdef DEBUG_ALAT
  fprintf(stderr,"ALAT: Remove all ALAT entries\n");
#endif
}

static void
alat_inval_many(LSE_emu_instr_info_t *ii, LSE_emu_addr_t saddr, int len) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int i;
  LSE_emu_addr_t faddr = saddr + len - 1;

  /* no undo of invalidations */
  for (i=realct->ALAT_numentries-1;i>=0;i--) {
    LSE_emu_addr_t ALAT_faddr, ALAT_saddr;

    /* cases:
     * access              <--->
     * ALAT         <---->                NO match
     * ALAT             <---->             match
     * ALAT                 <->            match
     * ALAT              <-------->        match
     * ALAT                  <----->       match
     * ALAT                        <----> NO match
     */

    ALAT_saddr = realct->ALAT[i].addr;
    ALAT_faddr = realct->ALAT[i].addr + realct->ALAT[i].len - 1;

    if (!(ALAT_faddr < saddr || ALAT_saddr > faddr)) {
#ifdef DEBUG_ALAT
      fprintf(stderr, "ALAT: Remove ALAT entry for "
	      "register /%d due to addr=%"PRIX64" len=%d\n",
	      realct->ALAT[i].tag,saddr,len);
#endif
      realct->ALAT[i] = realct->ALAT[realct->ALAT_numentries-1];
      realct->ALAT_numentries--;
    }
  }
}

static void
alat_insert_gr(LSE_emu_instr_info_t *ii, int rno, LSE_emu_addr_t addr, int len) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int p = alat_get_gr_tag(ii,rno);
  int i;

  /* get rid of old one */
  for (i=realct->ALAT_numentries-1;i>=0;i--) {
    if (realct->ALAT[i].tag == p) {
#ifdef DEBUG_ALAT
      fprintf(stderr,
	      "ALAT: Replacing ALAT entry for GR %d/%d addr=%"PRIX64" len=%d\n",
	      rno,p,realct->ALAT[i].addr,realct->ALAT[i].len);
#endif
      break;
    }
  }
  if (realct->ALAT_numentries == realct->ALAT_size && i == -1) i=0;

  if (i >= 0) {
#ifdef ALAT_LRU
    memmove(realct->ALAT+i,realct->ALAT+i+1,
	    (realct->ALAT_numentries-i-1) * sizeof(realct->ALAT[0]));
#else
    realct->ALAT[i] = realct->ALAT[realct->ALAT_numentries-1];
#endif
    realct->ALAT_numentries--;
  }

  realct->ALAT[realct->ALAT_numentries].tag = p;
  realct->ALAT[realct->ALAT_numentries].addr = addr;
  realct->ALAT[realct->ALAT_numentries].len = len;
  realct->ALAT_numentries++;
  if (~ii->privatef.rollback_saved & (1<<dest_alatupdate)) {
    ii->privatef.rollback_saved |= (1<<dest_alatupdate);
    ii->privatef.rollback[dest_alatupdate].rtype = IA64_rollback_alat;
    ii->privatef.rollback[dest_alatupdate].data.alat.tag = p;
  }
#ifdef DEBUG_ALAT
  fprintf(stderr,
	  "ALAT: Insert ALAT entry for GR %d/%d addr=%"PRIX64" len=%d\n",
	  rno,p,addr,len);
#endif
  return;
}

static void 
alat_insert_fr(LSE_emu_instr_info_t *ii, int rno, LSE_emu_addr_t addr, int len) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int p = alat_get_fr_tag(ii,rno);
  int i;
  /* get rid of old one */
  for (i=realct->ALAT_numentries-1;i>=0;i--) {
    if (realct->ALAT[i].tag == p) {
#ifdef DEBUG_ALAT
      fprintf(stderr,
	      "ALAT: Replacing ALAT entry for FR %d/%d addr=%"PRIX64" len=%d\n",
	      rno,p,realct->ALAT[i].addr,realct->ALAT[i].len);
#endif
      break;
    }
  }
  if (realct->ALAT_numentries == realct->ALAT_size && i == -1) i=0;

  if (i >= 0) {
#ifdef ALAT_LRU
    memmove(realct->ALAT+i,realct->ALAT+i+1,
	    (realct->ALAT_numentries-i-1) * sizeof(realct->ALAT[0]));
#else
    realct->ALAT[i] = realct->ALAT[realct->ALAT_numentries-1];
#endif
    realct->ALAT_numentries--;
  }

  realct->ALAT[realct->ALAT_numentries].tag = p;
  realct->ALAT[realct->ALAT_numentries].addr = addr;
  realct->ALAT[realct->ALAT_numentries].len = len;
  realct->ALAT_numentries++;
  if (~ii->privatef.rollback_saved & (1<<dest_alatupdate)) {
    ii->privatef.rollback_saved |= (1<<dest_alatupdate);
    ii->privatef.rollback[dest_alatupdate].rtype = IA64_rollback_alat;
    ii->privatef.rollback[dest_alatupdate].data.alat.tag = p;
  }
#ifdef DEBUG_ALAT
  fprintf(stderr,
	  "ALAT: Replace ALAT entry for FR %d/%d addr=%"PRIX64" len=%d\n",
	  rno,p,addr,len);
#endif
  return;
}

/************************ generic writeback functions ******************/

#define OPFETCH_PREFIX \
  case 0:                                                             \
    set_src_op(ii,src_cfm,SR,get_sr(realct,ii,IA64_ADDR_CFM));        \
    if (!fallthrough) break;                                          \
  case 1:                                                             \
    set_src_op(ii,src_psr,SR,get_sr(realct,ii,IA64_ADDR_PSR));        \
    if (!fallthrough) break;                                          

#define ALLBACK_PREFIX \
    set_src_op(ii,src_cfm,SR,get_sr(realct,ii,IA64_ADDR_CFM));        \
    set_src_op(ii,src_psr,SR,get_sr(realct,ii,IA64_ADDR_PSR));

void 
writeback_r1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname, int fallthrough) 
{
  uint64_t instr = ii->extra.instr;

  if (get_src_op(ii,src_qp,PR) && !intr_cancels_instr(ii))
    switch (opname) {
    case dest_result:
      set_r(dest_result,ii,all_r1(instr),
	    get_dest_op(ii,dest_result,GR.val),
	    get_dest_op(ii,dest_result,GR.nat));
      if (!fallthrough) break;
    default: break;
    }
}

void
writeback_f1_psr(LSE_emu_instr_info_t *ii, 
		 LSE_emu_operand_name_t opname, int fallthrough)
{
  uint64_t instr = ii->extra.instr;
  IA64_FR_t f1;

  if (get_src_op(ii,src_qp,PR) && !intr_cancels_instr(ii)) {
    switch (opname) {
    case dest_result:
      get_dest_op_f(ii,dest_result,f1);
      set_f(dest_result,ii,all_f1(instr),f1);
      if (!fallthrough) break;
    case dest_psr:
      set_sr_sbits(dest_psr,ii,IA64_ADDR_PSR,get_dest_op(ii,dest_psr,SR.PSR),
		   rf_mask(PSR_mfboth));
      if (!fallthrough) break;
    default: break;
    }
  }
}

void
writeback_f1_psr_fpsr(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,
		      int fallthrough)
{
  uint64_t instr = ii->extra.instr;
  IA64_FR_t f1;
  uint64_t mask;

  if (get_src_op(ii,src_qp,PR) && !intr_cancels_instr(ii)) {
    switch (opname) {
    case dest_result:
      get_dest_op_f(ii,dest_result,f1);
      set_f(dest_result,ii,all_f1(instr),f1);
      if (!fallthrough) break;
    case dest_fpsr:
      mask = INT64_C(0x3f) << (6 + 7 + 13 * (all_sf(instr)));
      set_ar_sbits(dest_fpsr,ii,IA64_ADDR_FPSR,get_dest_op(ii,dest_fpsr,AR),
		   mask);
      if (!fallthrough) break;
    case dest_psr:
      set_sr_sbits(dest_psr,ii,IA64_ADDR_PSR,get_dest_op(ii,dest_psr,SR.PSR),
		   rf_mask(PSR_mfboth));
      if (!fallthrough) break;
    default: break;
    }
  }
}

void 
writeback_recip(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,
		int fallthrough) 
{ 
  uint64_t instr = ii->extra.instr;
  IA64_FR_t f1;
  uint64_t mask;

  if (!intr_cancels_instr(ii)) {
    switch (opname) {
    case dest_result:
      if (get_src_op(ii,src_qp,PR)) {
	get_dest_op_f(ii,dest_result,f1);
	set_f(dest_result,ii,all_f1(instr),f1);
      }
      if (!fallthrough) break;
    case dest_isfalse:
      /* we always update the output predicate */
      set_p(dest_isfalse,ii,all_p2(instr),get_dest_op(ii,dest_isfalse,PR));
      if (!fallthrough) break;
    case dest_fpsr:
      if (get_src_op(ii,src_qp,PR)) {
	mask = INT64_C(0x3f) << (6 + 7 + 13 * (all_sf(instr)));
	set_ar_sbits(dest_fpsr,ii,IA64_ADDR_FPSR,get_dest_op(ii,dest_fpsr,AR),
		     mask);
      }
      if (!fallthrough) break;
    case dest_psr:
      if (get_src_op(ii,src_qp,PR)) {
	set_sr_sbits(dest_psr,ii,IA64_ADDR_PSR,get_dest_op(ii,dest_psr,SR.PSR),
		     rf_mask(PSR_mfboth));
      }
      if (!fallthrough) break;
    default: break;
    }
  }
}

/*************************** actual instruction stuff *******************/

void 
opfetch_I1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I1 multimedia multiply and shift */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;
  static int counts[]={0,7,15,16};

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  case src_op3:
    set_src_op(ii,src_op3,imm64,counts[I1_ct2d(instr)]);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I1(LSE_emu_instr_info_t *ii) 
{ /* I1 multimedia multiply and shift */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val,r3val,newval;
  int count;
  int i;

  check_target_register(ii,all_r1(instr));

  r2val = get_src_op(ii,src_op1,GR.val);
  r3val = get_src_op(ii,src_op2,GR.val);
  count = get_src_op(ii,src_op3,imm64);
  newval = 0;

  for (i=0;i<4;i++) {
    if (I1_x2b(instr)&2) { /* signed */
      newval |= (( ((int32_t)(int16_t)((r2val>>(16*i))&0xffffUL)) * 
		   ((int32_t)(int16_t)((r3val>>(16*i))&0xffffUL)))>>count) 
	& 0xffff;
    } else { /* unsigned */
      newval |= ((((r2val>>(16*i))&0xffffUL) * 
		  ((r3val>>(16*i))&0xffffUL))>>count) & 0xffff;
    }
  }

  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat)|get_src_op(ii,src_op2,GR.nat));
  return;
}

void 
opfetch_I2(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I2 multimedia multiply mix/pack */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I2(LSE_emu_instr_info_t *ii) 
{ /* I2 multimediate multiply/mix/pack/unpack */
  uint64_t instr = ii->extra.instr;
  /*
      static char *i2op[] = {
	"pack","unpack","mix","",
	"pmin","pmax","","pmpy",
	"pack","unpack","mix","",
	"pmin","pmax","psad","pmpy"
      };
      static char *i2ext[] = {
	".uss",".h",".r","",
	".u",".u","",".r",
	".sss",".l",".l","",
	"","","",".l"
      };
  */
  int opno = (I2_x2b(instr)<<2)|I2_x2c(instr);
  int size = 1<<(int)((I2_za(instr)<<1)|I2_zb(instr));
  uint64_t r2val,newval,r3val;
  int i;

  check_target_register(ii,all_r1(instr));

  r2val = get_src_op(ii,src_op1,GR.val);
  r3val = get_src_op(ii,src_op2,GR.val);
    
  switch (opno) {

  case 0: /* pack2.uss */
    newval = 0;
    for (i=0;i<4;i++) {
      uint16_t val1,val2;
      val1 = r2val & 0xffffL;
      if (val1 > 0xff) val1 = 0xff;
      newval |= val1 << i*8;
      r2val >>= 16;

      val2 = r3val & 0xffffL;
      if (val2 > 0xff) val2 = 0xff;
      newval |= val2 << (i*8+32);
      r3val >>= 16;
    }
    break;

  case 1: /* unpack[124].h */
    switch (size) {
    case 1:
      newval = (((r3val >>  32) & INT64_C(0x00000000000000FF)) |
		((r3val >>  24) & INT64_C(0x0000000000FF0000)) |
		((r3val >>  16) & INT64_C(0x000000FF00000000)) |
		((r3val >>  8 ) & INT64_C(0x00FF000000000000)) |
		((r2val >>  0 ) & INT64_C(0xFF00000000000000)) |
		((r2val >>  8 ) & INT64_C(0x0000FF0000000000)) |
		((r2val >>  16) & INT64_C(0x00000000FF000000)) |
		((r2val >>  24) & INT64_C(0xFF0000000000FF00)));
      break;
    case 2:
      newval = (((r3val >>  32) & INT64_C(0x000000000000FFFF)) |
		((r3val >>  16) & INT64_C(0x0000FFFF00000000)) |
		((r2val >>  0 ) & INT64_C(0xFFFF000000000000)) |
		((r2val >>  16) & INT64_C(0x00000000FFFF0000)));
      break;
    case 4:
      newval = (((r3val >>  32) & INT64_C(0x00000000FFFFFFFF)) |
		((r2val >>  0 ) & INT64_C(0xFFFFFFFF00000000)));
      break;
    default:
      newval = 0; /* shut up warnings */
      break;
    }
    break;

  case 2:/* mix[1-4].r */
    switch (size) {
    case 1:
      newval = (((r3val <<  0) & INT64_C(0x00FF00FF00FF00FF)) |
		((r2val <<  8) & INT64_C(0xFF00FF00FF00FF00)));
      break;
    case 2:
      newval = (((r3val <<  0) & INT64_C(0x0000FFFF0000FFFF)) |
		((r2val << 16) & INT64_C(0xFFFF0000FFFF0000)));
      break;
    case 4:
      newval = (((r3val <<  0) & INT64_C(0x00000000FFFFFFFF)) |
		((r2val << 32) & INT64_C(0xFFFFFFFF00000000)));
      break;
    default:
      newval = 0; /* shut up warnings */
      break;
    }
    break;

  case 4: /* pmin1.u */
    newval = 0;
    for (i=0;i<8;i++) {
      uint8_t val1, val2;
      val1 = r2val & 0xffL;
      val2 = r3val & 0xffL;
      if (val1 > val2) newval |= (val2 & 0xff) << i*8;
      else newval |= (val1 & 0xff) << i*8;
      r2val >>= 8;
      r3val >>= 8;
    }
    break;

  case 5: /* pmax1.u */
    newval = 0;
    for (i=0;i<8;i++) {
      uint8_t val1, val2;
      val1 = r2val & 0xffL;
      val2 = r3val & 0xffL;
      if (val1 < val2) newval |= (val2 & 0xff) << i*8;
      else newval |= (val1 & 0xff) << i*8;
      r2val >>= 8;
      r3val >>= 8;
    }
    break;

  case 7: /* pmpy2.r */
    { /* the trick here is to get the sign extension right */
      int64_t a1,a2,b1,b2;
      a1 = (((int64_t)(r2val << 16)) >> 48);
      a2 = (((int64_t)(r2val << 48)) >> 48);
      b1 = (((int64_t)(r3val << 16)) >> 48);
      b2 = (((int64_t)(r3val << 48)) >> 48);
      newval = ((a1 * b1) << 32) | ((a2 * b2) & INT64_C(0xFfffFfff));
    }
    break;

  case 8: /* pack[24].sss */
    if (I2_zb(instr)) {
      newval = 0;
      for (i=0;i<4;i++) {
	int16_t val1,val2;
	val1 = r2val & 0xffffL;
	if (r2val & 0x8000L) val1 |= ~0xffffL; /* in case int16_t > 16 bits */
	if (val1 > 127) val1 = 127;
	else if (val1 < -128) val1 = -128;
	newval |= val1 << i*8;
	r2val >>= 16;
	
	val2 = r3val & 0xffffL;
	if (r3val & 0x8000L) val2 |= ~0xffffL; /* in case int16_t > 16 bits */
	if (val2 > 127) val2 = 127;
	else if (val2 < -128) val2 = -128;
	newval |= val2 << (i*8+32);
	r3val >>= 16;
      }
    } else {
      newval = 0;
      for (i=0;i<2;i++) {
	int32_t val1,val2;
	val1 = r2val & 0xffffffffL;
	if (r2val & 0x80000000L) val1 |= ~INT64_C(0xffffffff);/*if int32_t > 32 bits */
	if (val1 > 32767) val1 = 32767;
	else if (val1 < -32768) val1 = -32768;
	newval |= val1 << i*16;
	r2val >>= 32;
	
	val2 = r3val & 0xffffffffL;
	if (r3val & 0x80000000L) val2 |= ~INT64_C(0xffffffff);/*if int16_t > 16 bits */
	if (val2 > 32767) val2 = 32767;
	else if (val2 < -32768) val2 = -32768;
	newval |= val2 << (i*16+32);
	r3val >>= 32;
      }
    }
    break;

  case 9: /* unpack[124].l */
    switch (size) {
    case 1:
      newval = (((r3val <<   0) & INT64_C(0x00000000000000FF)) |
		((r3val <<   8) & INT64_C(0x0000000000FF0000)) |
		((r3val <<  16) & INT64_C(0x000000FF00000000)) |
		((r3val <<  24) & INT64_C(0x00FF000000000000)) |
		((r2val <<  32) & INT64_C(0xFF00000000000000)) |
		((r2val <<  24) & INT64_C(0x0000FF0000000000)) |
		((r2val <<  16) & INT64_C(0x00000000FF000000)) |
		((r2val <<   8) & INT64_C(0xFF0000000000FF00)));
      break;
    case 2:
      newval = (((r3val <<   0) & INT64_C(0x000000000000FFFF)) |
		((r3val <<  16) & INT64_C(0x0000FFFF00000000)) |
		((r2val <<  32) & INT64_C(0xFFFF000000000000)) |
		((r2val <<  16) & INT64_C(0x00000000FFFF0000)));
      break;
    case 4:
      newval = (((r3val <<   0) & INT64_C(0x00000000FFFFFFFF)) |
		((r2val <<  32) & INT64_C(0xFFFFFFFF00000000)));
      break;
    default:
      newval = 0; /* shut up warnings */
      break;
    }
    break;


  case 10: /* mix[1-4].l */
    switch (size) {
    case 1:
      newval = (((r3val >>  8) & INT64_C(0x00FF00FF00FF00FF)) |
		((r2val >>  0) & INT64_C(0xFF00FF00FF00FF00)));
      break;
    case 2:
      newval = (((r3val >> 16) & INT64_C(0x0000FFFF0000FFFF)) |
		((r2val >>  0) & INT64_C(0xFFFF0000FFFF0000)));
      break;
    case 4:
      newval = (((r3val >> 32) & INT64_C(0x00000000FFFFFFFF)) |
		((r2val >>  0) & INT64_C(0xFFFFFFFF00000000)));
      break;
    default:
      newval = 0; /* shut up warnings */
      break;
    }
    break;

  case 12: /* pmin2 */
    newval = 0;
    for (i=0;i<4;i++) {
      int16_t val1, val2;
      val1 = r2val & 0xffffL;
      if (r2val & 0x8000L) val1 |= ~0xffffL; /* in case int16_t > 16 bits */
      val2 = r3val & 0xffffL;
      if (r3val & 0x8000L) val2 |= ~0xffffL; /* in case int16_t > 16 bits */
      if (val1 > val2) newval |= (val2 & 0xffff) << i*16;
      else newval |= (val1 & 0xffff) << i*16;
      r2val >>= 16;
      r3val >>= 16;
    }
    break;

  case 13: /* pmax2 */
    newval = 0;
    for (i=0;i<4;i++) {
      int16_t val1, val2;
      val1 = r2val & 0xffffL;
      if (r2val & 0x8000L) val1 |= ~0xffffL; /* in case int16_t > 16 bits */
      val2 = r3val & 0xffffL;
      if (r3val & 0x8000L) val2 |= ~0xffffL; /* in case int16_t > 16 bits */
      if (val1 < val2) newval |= (val2 & 0xffff) << i*16;
      else newval |= (val1 & 0xffff) << i*16;
      r2val >>= 16;
      r3val >>= 16;
    }
    break;

  case 14: /* psad1 */
    newval = 0;
    for (i=0;i<8;i++) {
      long tmp = (r3val&0xff) - (r2val&0xff);
      newval += (tmp<0)? -tmp : tmp;
      r2val >>= 8;
      r3val >>= 8;
    }
    break;

  case 15: /* pmpy2.l */
    { /* the trick here is to get the sign extension right */
      int64_t a1,a2,b1,b2;
      a1 = (((int64_t)(r2val << 0)) >> 48);
      a2 = (((int64_t)(r2val << 32)) >> 48);
      b1 = (((int64_t)(r3val << 0)) >> 48);
      b2 = (((int64_t)(r3val << 32)) >> 48);
      newval = ((a1 * b1) << 32) | ((a2 * b2) & INT64_C(0xFfffFfff));
    }
    break;

  default: /* never executed -- keeps compiler warnings happy */
    report_actual_intr(ii,IA64_intr_notimplemented);
    newval = 0;
  }
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) || get_src_op(ii,src_op2,GR.nat));
  return;
}

void 
opfetch_I3(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I3 multimedia mux1 */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;
    
  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,I3_mbt4c(instr));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I3(LSE_emu_instr_info_t *ii) 
{ /* I3 multimedia mux1 */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val,newval;

  check_target_register(ii,all_r1(instr));

  r2val = get_src_op(ii,src_op1,GR.val);
    
  switch (get_src_op(ii,src_op2,imm64)) {
  case 0: /* brcst */
    newval = r2val & INT64_C(0x0FF);
    newval = (newval<<8) | newval;
    newval = (newval<<16) | newval;
    newval = (newval<<32) | newval;
    break;
  case 8: /* mix */
    newval = (((r2val)     & INT64_C(0xFF00FF0000FF00FF)) |
	      ((r2val<<24) & INT64_C(0x00FF00FF00000000)) |
	      ((r2val>>24) & INT64_C(0x00000000FF00FF00)));
    break;
  case 9 : /* shuf */
    newval = (((r2val)     & INT64_C(0xFF000000000000FF)) | 
	      ((r2val>>24) & INT64_C(0x000000000000FF00)) | 
	      ((r2val<<8)  & INT64_C(0x0000000000FF0000)) | 
	      ((r2val>>16) & INT64_C(0x00000000FF000000)) | 
	      ((r2val<<16) & INT64_C(0x000000FF00000000)) | 
	      ((r2val>>8)  & INT64_C(0x0000FF0000000000)) | 
	      ((r2val<<24) & INT64_C(0x00FF000000000000)));
    break;
  case 10 : /* alt */
    newval = (((r2val)     & INT64_C(0xFF000000000000FF)) | 
	      ((r2val>>8)  & INT64_C(0x000000000000FF00)) | 
	      ((r2val>>16) & INT64_C(0x0000000000FF0000)) | 
	      ((r2val>>24) & INT64_C(0x00000000FF000000)) | 
	      ((r2val<<24) & INT64_C(0x000000FF00000000)) | 
	      ((r2val<<16) & INT64_C(0x0000FF0000000000)) | 
	      ((r2val<<8)  & INT64_C(0x00FF000000000000)));
    break;
  case 11 : /* rev */
    newval = (((r2val>>56) & INT64_C(0x00000000000000FF)) | 
	      ((r2val>>40) & INT64_C(0x000000000000FF00)) | 
	      ((r2val>>24) & INT64_C(0x0000000000FF0000)) | 
	      ((r2val>>8)  & INT64_C(0x00000000FF000000)) | 
	      ((r2val<<8)  & INT64_C(0x000000FF00000000)) | 
	      ((r2val<<24) & INT64_C(0x0000FF0000000000)) | 
	      ((r2val<<40) & INT64_C(0x00FF000000000000)) | 
	      ((r2val<<56) & INT64_C(0xFF00000000000000)));
    break;
  default:
    newval = 0;
    report_intr(ii,IA64_intr_illegaloperation);
  }
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
  return;
}

void 
opfetch_I4(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I4 multimedia mux2 */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;
    
  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,I4_mht8c(instr));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I4(LSE_emu_instr_info_t *ii) 
{ /* I4 multimedia mux2 */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val,newval;
  int sources;
  uint16_t vals[4];

  check_target_register(ii,all_r1(instr));

  r2val = get_src_op(ii,src_op1,GR.val);
  sources = get_src_op(ii,src_op2,imm64);
  vals[0] = r2val & 0xffff;
  vals[1] = (r2val >> 16) & 0xffff;
  vals[2] = (r2val >> 32) & 0xffff;
  vals[3] = (r2val >> 48) & 0xffff;

  newval = ( vals[sources&0x3] |
	     (((uint64_t)vals[(sources>>2)&0x3]) << 16) |
	     (((uint64_t)vals[(sources>>4)&0x3]) << 32) |
	     (((uint64_t)vals[(sources>>6)&0x3]) << 48));

  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
  return;
}

void 
opfetch_I5(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I5 shift right-variable */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I5(LSE_emu_instr_info_t *ii) 
{ /* I5 shift right-variable */
  uint64_t instr = ii->extra.instr;
  uint64_t r3val, r2val,newval,tmask,tmask2;
  int count;

  check_target_register(ii,all_r1(instr));
  
  r3val = get_src_op(ii,src_op1,GR.val);
  r2val = get_src_op(ii,src_op2,GR.val);
    
  switch ((I5_za(instr)<<1)+I5_zb(instr)) {
  case 1: /* pshr2 */
    count = (r2val > 16) ? 16 : r2val;
    if (I5_x2b(instr)) { /* signed */
      newval = r3val >> count;
      tmask = (INT64_C(0x0ffff) >> count);
      newval &= (tmask | (tmask<<16) | (tmask<<32) | (tmask<<48));
      tmask2 = (~tmask) & INT64_C(0x0ffff);
      if (r3val & (INT64_C(1)<<63)) newval |= tmask2 << 48;
      if (r3val & (INT64_C(1)<<47)) newval |= tmask2 << 32;
      if (r3val & (INT64_C(1)<<31)) newval |= tmask2 << 16;
      if (r3val & (INT64_C(1)<<15)) newval |= tmask2;
    } else { /* unsigned */
      newval = r3val >> count;
      tmask = (INT64_C(0x0ffff) >> count);
      newval &= (tmask | (tmask<<16) | (tmask<<32) | (tmask<<48));
    }
    break;
  case 2: /* pshr4 */
    count = (r3val > 32) ? 32 : r2val;
    if (I5_x2b(instr)) { /* signed */
      newval = r3val >> count;
      tmask = (INT64_C(0x0ffffffff) >> count);
      newval &= (tmask | (tmask<<32));
      tmask2 = (~tmask) & INT64_C(0x0ffffffff);
      if (r3val & (INT64_C(1)<<63)) newval |= tmask2 << 32;
      if (r3val & (INT64_C(1)<<31)) newval |= tmask2;
    } else { /* unsigned */
      newval = r3val >> count;
      tmask = (INT64_C(0x0ffffffff) >> count);
      newval &= (tmask | (tmask<<32));
    }
    break;
  case 3: /* shr */
    if (I5_x2b(instr)) { /* signed */
      /* clip because C mod's the shift count */
      count = (r2val > 63) ? 63 : r2val;  
      newval = (int64_t)r3val >> count;
    } else {
      count = r2val;
      newval = (r2val > 63) ? 0 : (r3val >> r2val);
    }
    break;
  default:
    newval = 0; /* shut up warnings */
    break;
  }
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) || get_src_op(ii,src_op2,GR.nat));
  return;
}

void 
opfetch_I6(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I6 multimedia shift right-fixed */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,I6_count5b(instr));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I6(LSE_emu_instr_info_t *ii) 
{ /* I6 multimedia shift right-fixed */
  uint64_t instr = ii->extra.instr;
  uint64_t r3val, newval,tmask,tmask2;
  int count;

  check_target_register(ii,all_r1(instr));
  
  r3val = get_src_op(ii,src_op1,GR.val);
  count = get_src_op(ii,src_op2,imm64);
  
  if (I6_zb(instr)) { /* pshr2 */
    if (count > 16) count = 16;
    if (I6_x2b(instr)&2) { /* signed */
      newval = r3val >> count;
      tmask = (INT64_C(0x0ffff) >> count);
      newval &= (tmask | (tmask<<16) | (tmask<<32) | (tmask<<48));
      tmask2 = (~tmask) & INT64_C(0x0ffff);
      if (r3val & (INT64_C(1)<<63)) newval |= tmask2 << 48;
      if (r3val & (INT64_C(1)<<47)) newval |= tmask2 << 32;
      if (r3val & (INT64_C(1)<<31)) newval |= tmask2 << 16;
      if (r3val & (INT64_C(1)<<15)) newval |= tmask2;
    } else { /* unsigned */
      newval = r3val >> count;
      tmask = (INT64_C(0x0ffff) >> count);
      newval &= (tmask | (tmask<<16) | (tmask<<32) | (tmask<<48));
    }
  } else { /* pshr4 */
    if (count > 32) count = 32;
    if (I5_x2b(instr)) { /* signed */
      newval = r3val >> count;
      tmask = (INT64_C(0x0ffffffff) >> count);
      newval &= (tmask | (tmask<<32));
      tmask2 = (~tmask) & INT64_C(0x0ffffffff);
      if (r3val & (INT64_C(1)<<63)) newval |= tmask2 << 32;
      if (r3val & (INT64_C(1)<<31)) newval |= tmask2;
    } else { /* unsigned */
      newval = r3val >> count;
      tmask = (INT64_C(0x0ffffffff) >> count);
      newval &= (tmask | (tmask<<32));
    }
  }
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
  return;
}

void 
opfetch_I7(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I7 shift left-variable */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr)); 
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I7(LSE_emu_instr_info_t *ii) 
{ /* I7 shift left-variable */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val, count, tmask;    

  check_target_register(ii,all_r1(instr));
  
  r2val = get_src_op(ii,src_op1,GR.val);
  count = get_src_op(ii,src_op2,GR.val);
    
  switch ((I7_za(instr)<<1)+I7_zb(instr)) {
  case 1: /* pshl2 */
    if (count > 16) count=16;
    r2val <<= count;
    tmask = (INT64_C(0x0ffff) << count) & INT64_C(0x0FFFF);
    r2val &= (tmask | (tmask<<16) | (tmask<<32) | (tmask<<48));
    break;
  case 2: /* pshl4 */
    if (count > 32) count = 32;
    r2val <<= count;
    tmask = (INT64_C(0x0ffffffff) << count) & INT64_C(0x0ffffffff);
    r2val &= (tmask | (tmask<<32));
    break;
  default: /* shl */
    if (count > 63) r2val = 0;
    else r2val <<= count;
  }
  set_dest_op(ii,dest_result,GR.val,r2val);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) || get_src_op(ii,src_op2,GR.nat));
  return;
}

void 
opfetch_I8(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I8 multimedia shift left-fixed */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,31-I8_ccount5c(instr));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I8(LSE_emu_instr_info_t *ii) 
{ /* I8 multimedia shift left-fixed */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val, count, tmask;    

  check_target_register(ii,all_r1(instr));
  
  r2val = get_src_op(ii,src_op1,GR.val);
  count = get_src_op(ii,src_op2,GR.val);
    
  if (I8_zb(instr)) { /* pshl2 */
    if (count > 16) count=16;
    r2val <<= count;
    tmask = (INT64_C(0x0ffff) << count) & INT64_C(0x0FFFF);
    r2val &= (tmask | (tmask<<16) | (tmask<<32) | (tmask<<48));
  } else { /* pshl4 */
    if (count > 32) count = 32;
    r2val <<= count;
    tmask = (INT64_C(0x0ffffffff) << count) & INT64_C(0x0ffffffff);
    r2val &= (tmask | (tmask<<32));
  }
  set_dest_op(ii,dest_result,GR.val,r2val);
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
  return;
}

void 
opfetch_I9(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I9 population count */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I9(LSE_emu_instr_info_t *ii) 
{ /* I9 population count */
  uint64_t instr = ii->extra.instr;
  uint64_t r3val;
  int count, i;

  check_target_register(ii,all_r1(instr));
  
  r3val = get_src_op(ii,src_op1,GR.val);

  count = 0;
  for (i=0;i<64;i++) {
    if (r3val & 1) count++;
    r3val >>= 1;
  }
  set_dest_op(ii,dest_result,GR.val,count);
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
  return;
}

void 
opfetch_I10(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I10 shift right set */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1: 
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  case src_op3:
    set_src_op(ii,src_op3,imm64,I10_count6d(instr));
    if (!fallthrough) break;
  default: break;
  }
}
 
void
evaluate_I10(LSE_emu_instr_info_t *ii) 
{ /* I10 shift right pair */
  uint64_t instr = ii->extra.instr;
  uint64_t newval;
  int count = get_src_op(ii,src_op3,imm64);

  check_target_register(ii,all_r1(instr));    
    
  newval = get_src_op(ii,src_op2,GR.val) >> count;
  newval |= get_src_op(ii,src_op1,GR.val) << (64-count);
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) || get_src_op(ii,src_op2,GR.nat));
  return;
}

void 
opfetch_I11(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I11 extract (also shr by immediate) */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,(I11_pos6b(instr)));
    if (!fallthrough) break;
  case src_op3:
    set_src_op(ii,src_op3,imm64,(I11_len6d(instr)+1));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I11(LSE_emu_instr_info_t *ii) 
{ /* I11 extract (also shr by immediate) */
  uint64_t instr = ii->extra.instr;
  uint64_t r3val = get_src_op(ii,src_op1,GR.val);
  int len = get_src_op(ii,src_op3,imm64);

  check_target_register(ii,all_r1(instr));
    
  r3val >>= get_src_op(ii,src_op2,imm64);
  if (I11_y(instr) && (r3val & (INT64_C(1)<<(len-1)))) 
    r3val |= ~((INT64_C(1)<<len)-1); /* sign extend a negative */
  else /* zero-extend or sign extend a positive */
    r3val &= (INT64_C(1)<<len)-1;
  
  set_dest_op(ii,dest_result,GR.val,r3val);
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
  return;
}

void 
opfetch_I12(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I12 dep.z */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,(63-I12_cpos6c(instr)));
    if (!fallthrough) break;
  case src_op3:
    set_src_op(ii,src_op3,imm64,(I12_len6d(instr)+1));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I12(LSE_emu_instr_info_t *ii) 
{ /* I12 dep.z */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val;
  int len;

  check_target_register(ii,all_r1(instr));

  r2val = get_src_op(ii,src_op1,GR.val);
  len = get_src_op(ii,src_op3,imm64);
  set_dest_op(ii,dest_result,GR.val,
	      (r2val & ((INT64_C(1)<<len)-1))<<get_src_op(ii,src_op2,imm64));
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat));
  return;
}

void 
opfetch_I13(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I13 zero and deposit immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int64_t imm8;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    imm8 = I13_imm7b(instr);
    if (I13_s(instr)) imm8 |= ~(0x7f);
    set_src_op(ii,src_op1,imm64,imm8);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,(63-I13_cpos6c(instr)));
    if (!fallthrough) break;
  case src_op3:
    set_src_op(ii,src_op3,imm64,(I13_len6d(instr)+1));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I13(LSE_emu_instr_info_t *ii) 
{ /* I13 zero and deposit immediate */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val;
  int len;

  check_target_register(ii,all_r1(instr));

  r2val = get_src_op(ii,src_op1,GR.val);
  len = get_src_op(ii,src_op3,imm64);
  set_dest_op(ii,dest_result,GR.val,
	      (r2val & ((INT64_C(1)<<len)-1))<<get_src_op(ii,src_op2,imm64));
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat));
  return;
}

void 
opfetch_I14(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I14 - deposit immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    set_src_op(ii,src_op1,imm64,(I14_s(instr) ? -INT64_C(1) : INT64_C(0)));
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  case src_op3:
    set_src_op(ii,src_op3,imm64,(63-I14_cpos6b(instr)));
    if (!fallthrough) break;
  case src_op4:
    set_src_op(ii,src_op4,imm64,(I14_len6d(instr)+1));    
    if (!fallthrough) break;
  default: break; 
  }
}

void
evaluate_I14(LSE_emu_instr_info_t *ii) 
{ /* I14 - deposit immediate */
  uint64_t instr = ii->extra.instr;
  int len = get_src_op(ii,src_op4,imm64);
  int pos = get_src_op(ii,src_op3,imm64);
  uint64_t mask1 = ((INT64_C(1)<<len)-1);
  uint64_t mask = mask1 << pos;

  check_target_register(ii,all_r1(instr));

  set_dest_op(ii,dest_result,GR.val,
	      ((get_src_op(ii,src_op2,GR.val) & ~mask) |
	       (get_src_op(ii,src_op1,imm64) & mask1)<<pos));
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op2,GR.nat));
  return;
}
   
void 
opfetch_I15(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I15 - deposit */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  case src_op3:
    set_src_op(ii,src_op3,imm64,(63-I15_cpos6d(instr)));
    if (!fallthrough) break;
  case src_op4:
    set_src_op(ii,src_op4,imm64,I15_len4d(instr)+1);    
    if (!fallthrough) break;
  default: break;
  }
}
 
void
evaluate_I15(LSE_emu_instr_info_t *ii) 
{ /* I15 - deposit */
  uint64_t instr = ii->extra.instr;
  int len = get_src_op(ii,src_op4,imm64);
  int pos = get_src_op(ii,src_op3,imm64);
  uint64_t mask1 = ((INT64_C(1)<<len)-1);
  uint64_t mask = mask1<<pos;

  check_target_register(ii,all_r1(instr));

  set_dest_op(ii,dest_result,GR.val,
	      ((get_src_op(ii,src_op2,GR.val) & ~mask) |
	       ((get_src_op(ii,src_op1,GR.val) & mask1)<< pos)));
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) || get_src_op(ii,src_op2,GR.nat));
  return;
}

void 
opfetch_I16(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I16 test bit */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,I16_pos6b(instr));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I16(LSE_emu_instr_info_t *ii) 
{ /* I16 - test bit */
  uint64_t instr = ii->extra.instr;

  static int comptype[]= { 0, 4, 1, 1, 2, 2, 3, 3 };
  int comptypeind = I16_ta(instr)*4 + I16_tb(instr)*2+I16_c(instr);
  int tmp_nat, istrue;
  uint64_t r3val;
  
  set_int_op(ii,int_updatepred,PR,0);
  if (!get_src_op(ii,src_qp,PR)) {
    if (comptypeind==1) {
      if (all_p1(instr) == all_p2(instr)) 
	report_actual_intr(ii,IA64_intr_illegaloperation);
      set_dest_op(ii,dest_istrue,PR,0);
      set_dest_op(ii,dest_isfalse,PR,0);
      set_int_op(ii,int_updatepred,PR,1);
      return;
    }
    if (all_p1(instr) == all_p2(instr)) 
      report_potential_intr(ii,IA64_intr_illegaloperation);
  } else if (all_p1(instr) == all_p2(instr)) 
    report_actual_intr(ii,IA64_intr_illegaloperation);

  tmp_nat = get_src_op(ii,src_op1,GR.nat);
  r3val = get_src_op(ii,src_op1,GR.val);
  
  if (!(comptypeind & 1) || comptypeind==1) 
    istrue = !(r3val & (INT64_C(1)<<get_src_op(ii,src_op2,imm64)));
  else
    istrue = !!(r3val & (INT64_C(1)<<get_src_op(ii,src_op2,imm64)));
  
  switch (comptype[comptypeind]) {
  case 1:  /* and */
    if (tmp_nat || !istrue) {
      set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));
      set_dest_op(ii,dest_istrue,PR,0);
      set_dest_op(ii,dest_isfalse,PR,0);
    }
    break;
  case 2:  /* or */
    if (!tmp_nat && istrue) {
      set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));
      set_dest_op(ii,dest_istrue,PR,1);
      set_dest_op(ii,dest_isfalse,PR,1);
    }
    break;
  case 3:  /* or.andcm */
    if (!tmp_nat && istrue) {
      set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));
      set_dest_op(ii,dest_istrue,PR,1);
      set_dest_op(ii,dest_isfalse,PR,0);
    }
    break;
  case 4:  /* unc */
    /* intentionally runs through */
  default:
    set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));
    set_dest_op(ii,dest_istrue,PR,!tmp_nat && istrue);
    set_dest_op(ii,dest_isfalse,PR,!tmp_nat && !istrue);
  }
  return;
}

void 
opfetch_I17(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I17 test NaT */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I17(LSE_emu_instr_info_t *ii) 
{ /* I17 - test NaT */
  uint64_t instr = ii->extra.instr;

  static int comptype[]= { 0, 4, 1, 1, 2, 2, 3, 3 };
  int comptypeind = I16_ta(instr)*4 + I16_tb(instr)*2+I16_c(instr);
  int tmp_nat, istrue;
  
  set_int_op(ii,int_updatepred,PR,0);
  if (!get_src_op(ii,src_qp,PR)) {
    if (comptypeind==1) {
      if (all_p1(instr) == all_p2(instr)) 
	report_actual_intr(ii,IA64_intr_illegaloperation);
      set_dest_op(ii,dest_istrue,PR,0);
      set_dest_op(ii,dest_isfalse,PR,0);
      set_int_op(ii,int_updatepred,PR,1);
      return;
    }
    if (all_p1(instr) == all_p2(instr)) 
      report_potential_intr(ii,IA64_intr_illegaloperation);
  } else if (all_p1(instr) == all_p2(instr)) 
    report_actual_intr(ii,IA64_intr_illegaloperation);

  tmp_nat = get_src_op(ii,src_op1,GR.nat);
  
  if (!(comptypeind & 1) || comptypeind==1) 
    istrue = !tmp_nat;
  else
    istrue = tmp_nat;

  switch (comptype[comptypeind]) {
  case 1:  /* and */
    if (!istrue) {
      set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));
      set_dest_op(ii,dest_istrue,PR,0);
      set_dest_op(ii,dest_isfalse,PR,0);
    }
    break;
  case 2:  /* or */
    if (istrue) {
      set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));
      set_dest_op(ii,dest_istrue,PR,1);
      set_dest_op(ii,dest_isfalse,PR,1);
    }
    break;
  case 3:  /* or.andcm */
    if (istrue) {
      set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));
      set_dest_op(ii,dest_istrue,PR,1);
      set_dest_op(ii,dest_isfalse,PR,0);
    }
    break;
  case 4:  /* unc */
    /* intentionally runs through */
  default:
    set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));
    set_dest_op(ii,dest_istrue,PR,istrue);
    set_dest_op(ii,dest_isfalse,PR,!istrue);
  }
  return;
}

void 
opfetch_I18(LSE_emu_instr_info_t *ii, 
	    LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I18 nop/hint */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t temp;
  temp = I18_imm20a(instr) | (I18_i(instr) << 20);


  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    set_src_op(ii,src_op1,imm64,temp);
    if (!fallthrough) break;
  default: break;
  }
}

/* I19 - break (I-unit) with M37 */

/* I20 - Integer speculation check (I-unit) with M20 */

void 
opfetch_I21(LSE_emu_instr_info_t *ii, 
	    LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I21 - mov to br */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t imm64;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    imm64 = I21_timm9c(instr) & 0xFF;
    if (imm64 & 0x100) imm64 |= ~INT64_C(0xFF);
    imm64 <<= 4;
    set_src_op(ii,src_op2,imm64,imm64);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I21(LSE_emu_instr_info_t *ii) 
{  /* I21 - mov to br */   
  
  if (get_src_op(ii,src_op1,GR.nat)) 
    report_intr(ii,IA64_intr_registernatconsumption);
  set_dest_op(ii,dest_result,BR,get_src_op(ii,src_op1,GR.val));
  return;
}

void
writeback_I21(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{  /* I21 - mov to br */   
  uint64_t instr = ii->extra.instr;

  if (get_src_op(ii,src_qp,PR) && !intr_cancels_instr(ii)) {
    switch (opname) {
    case dest_result:
      set_b(dest_result,ii,I21_b1(instr),get_dest_op(ii,dest_result,BR));
      if (!fallthrough) break;
    default: break;
    }
  }
}

void 
opfetch_I22(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I22 - mov from br */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    set_src_op(ii,src_op1,BR,get_b(realct,ii,I22_b2(instr)));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I22(LSE_emu_instr_info_t *ii) { /* I22 - mov from BR */
  uint64_t instr = ii->extra.instr;

  check_target_register(ii,all_r1(instr));
  set_dest_op(ii,dest_result,GR.val,get_src_op(ii,src_op1,BR));
  set_dest_op(ii,dest_result,GR.nat,0);
  return;
}

void 
opfetch_I23(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I23 - mov to predicates - register */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t imm64;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    imm64 = I23_mask7a(instr)<<1;
    imm64 |= (I23_mask8c(instr)<<8);
    if (I23_s(instr)) imm64 |= ~INT64_C(0xFFFF);
    set_src_op(ii,src_op2,imm64,imm64);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I23(LSE_emu_instr_info_t *ii) 
{ /* I23 - mov to predicates - register */
  if (get_src_op(ii,src_op1,GR.nat)) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }
  set_dest_op(ii,dest_result,imm64,
	      (get_src_op(ii,src_op1,GR.val) & get_src_op(ii,src_op2,imm64)));
  return;
}

void
writeback_I23(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I23 - mov to predicates - register */
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t imm64, val64, oval;
  int i;

  if (get_src_op(ii,src_qp,PR) && !intr_cancels_instr(ii)) {
    switch (opname) {
    case dest_result:
      imm64 = get_src_op(ii,src_op2,imm64);
      val64 = get_dest_op(ii,dest_result,imm64);
      oval = 1;
      val64 >>= 1;
      imm64 >>= 1;
      for (i=1;i<64;i++) {
	oval |= (((uint64_t)realct->p[i]) << i);
	if (imm64 & 1)
	  set_p_norot(realct,ii,i, (char)(val64 & 1));
	imm64 >>= 1;
	val64 >>= 1;
      }
      /* deal with rollback */
      if (~ii->privatef.rollback_saved & (1<<dest_result)) {
	ii->privatef.rollback_saved |= (1<<dest_result);
	ii->privatef.rollback[dest_result].rtype = IA64_rollback_preds;
	ii->privatef.rollback[dest_result].data.preds.old = oval;
	ii->privatef.rollback[dest_result].data.preds.mask 
	  = get_src_op(ii,src_op2,imm64) & ~INT64_C(1);
      }
      if (!fallthrough) break;
    default: break;
    }
  }
}

void 
opfetch_I24(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I24 - mov to rotating predicates - immediate */
  uint64_t instr = ii->extra.instr;
  uint64_t imm64;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    imm64 = I24_imm27a(instr) << 16;
    if (I24_s(instr)) imm64 |= ~INT64_C(0x7FFFFFFFFFF);

    set_src_op(ii,src_op1,imm64,imm64);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I24(LSE_emu_instr_info_t *ii) 
{ /* I24 - mov to rotating predicates - immediate */
  set_dest_op(ii,dest_result,imm64,get_src_op(ii,src_op1,imm64));
  return;
}

void
writeback_I24(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I24 - mov to rotating predicates - immediate */
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t rval,oval;
  int i;

  if (get_src_op(ii,src_qp,PR) && !intr_cancels_instr(ii)) {
    switch (opname) {
    case dest_result:
      rval = get_dest_op(ii,dest_result,imm64);
      oval = 1;
      /* This rollback is OK */
      rval >>= 16;
      for (i=16;i<64;i++) {
	oval |= (((uint64_t)realct->p[i]) << i);
	set_p_norot(realct,ii,i,(char)(rval&1));
	rval >>= 1;
      }
      /* deal with rollback */
      if (~ii->privatef.rollback_saved & (1<<dest_result)) {
	ii->privatef.rollback_saved |= (1<<dest_result);
	ii->privatef.rollback[dest_result].rtype = IA64_rollback_preds;
	ii->privatef.rollback[dest_result].data.preds.old = oval;
	ii->privatef.rollback[dest_result].data.preds.mask = ~INT64_C(0xffff);
      }
      if (!fallthrough) break;
    default: break;
    }
  }
  return;
}

void
opfetch_I25(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I25 - mov from pred/IP */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    if (I25_x6(instr)==0x30) {
      /* IP is *not* given as a source because it's automagic, therefore
	 there is no opfetch here either...
      */
    }
    else {
      uint64_t tmpval=0;
      int i;
      for (i=0;i<64;i++) {
	tmpval |= ((unsigned long long)get_p_norot(realct,ii,i))<<i;
      }
      set_src_op(ii,src_op1,imm64,tmpval);
    }
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I25(LSE_emu_instr_info_t *ii) 
{ /* I25 - mov from pred/IP */
  uint64_t instr = ii->extra.instr;

  check_target_register(ii,all_r1(instr));
  if (I25_x6(instr)==0x30) {
    set_dest_op(ii,dest_result,GR.val,ii->addr & ~INT64_C(3));
    set_dest_op(ii,dest_result,GR.nat,0);
  } else {
    set_dest_op(ii,dest_result,GR.val,get_src_op(ii,src_op1,imm64));
    set_dest_op(ii,dest_result,GR.nat,0);
  }
  return;
}

    /* case 26: put with M29 */ /* I26 - mov.i to ar */
    /* case 27: put with M30 */ /* I27 - mov.i to ar immediate */
    /* case 28: put with M31 */ /* I28 - mov.i from ar */

void
opfetch_I29(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* I29 - sign/zero extensions */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_I29(LSE_emu_instr_info_t *ii) 
{ 
  uint64_t instr = ii->extra.instr;
  int op = (I29_x6(instr) >> 2) & 3;
  int size = I29_x6(instr) & 3;
  int i;
  uint64_t mask,tval;

  check_target_register(ii,all_r1(instr));
  
  switch (op) {
  case 0: /* zxt */
    mask = (INT64_C(1)<<(8*(1<<size)))-1;
    set_dest_op(ii,dest_result,GR.val,
		mask & get_src_op(ii,src_op1,GR.val));
    set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
    break;
  case 1: /* sxt */
    mask = (INT64_C(1)<<(8*(1<<size)-1));
    tval = get_src_op(ii,src_op1,GR.val);
    set_dest_op(ii,dest_result,GR.val,
		(mask & tval) ? 
		( (tval & (mask-1)) | ~(mask-1)) : (tval & (mask-1)));
    set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
    break;
    
  case 2: /* czxl */
    mask = (size?INT64_C(0xFFFF) : INT64_C(0xFF00))<<56;
    tval = get_src_op(ii,src_op1,GR.val);
    i=0;
    while (mask) {
      if (!(mask & tval)) break;
      i=i+1;
      mask >>= (size?16:8);
    }
    set_dest_op(ii,dest_result,GR.val,i);
    set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
    break;
    
  case 3: /* czxr */
    mask = (size?INT64_C(0xFFFF) : INT64_C(0x00FF));
    tval = get_src_op(ii,src_op1,GR.val);
    i=0;
    while (mask) {
      if (!(mask & tval)) break;
      i=i+1;
      mask <<= (size?16:8);
    }
    set_dest_op(ii,dest_result,GR.val,i);
    set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op1,GR.nat));
    break;
  }
  return;
}

void
opfetch_M1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M1 - integer load */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:    
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_unat:
    if ((M3_x6(instr)>>2)==6) {
      set_src_op(ii,src_unat,AR,get_ar(realct,ii,IA64_ADDR_UNAT));
    }
    if (!fallthrough) break;
  default: break;
  }
}

void
opfetch_M2(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M2 - integer load, increment by register */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:    
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_update_amt,*ival);
    if (!fallthrough) break;
  case src_unat:
    if ((M3_x6(instr)>>2)==6) {
      set_src_op(ii,src_unat,AR,get_ar(realct,ii,IA64_ADDR_UNAT));
    }
    if (!fallthrough) break;
  default: break;
  }
}

void
opfetch_M3(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M3 - integer load, increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int64_t imm9;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    imm9 = M3_imm7b(instr);
    imm9 |= (M3_i(instr) << 7);
    if (M3_s(instr)) imm9 |= ~(0xff);
    set_src_op(ii,src_update_amt,imm64, imm9);
    if (!fallthrough) break;
  case src_unat:
    if ((M3_x6(instr)>>2)==6) {
      set_src_op(ii,src_unat,AR,get_ar(realct,ii,IA64_ADDR_UNAT));
    }
    if (!fallthrough) break;
  default: break;
  }
}

static inline void
dest_alatupdate_M1M2M3(LSE_emu_instr_info_t *ii) 
{ /* M1 - integer load */
  /* M2 - integer load, increment by register */
  /* M3 - integer load, increment by immediate */
  uint64_t instr = ii->extra.instr;
  int comple;
  boolean advanced, cclear, cnclear;

  comple = M3_x6(instr)>>2;
  advanced = comple==2 || comple == 3;
  cclear = comple==8 || comple==10;
  cnclear = comple==9;

  if (cclear && get_int_op(ii,int_alatcheck,alatcheck.present)) 
    set_dest_op(ii,dest_alatupdate,alatchange,-1);
  else if ((advanced || (cnclear && 
			 !get_int_op(ii,int_alatcheck,alatcheck.present))) && 
	   1 /* speculative page */)
    set_dest_op(ii,dest_alatupdate,alatchange,1);
  else 
    set_dest_op(ii,dest_alatupdate,alatchange,0);
}

void
evaluate_M_int_ld(LSE_emu_instr_info_t *ii) 
{ /* M1 - integer load */
  /* M2 - integer load, increment by register */
  /* M3 - integer load, increment by immediate */
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;
  uint64_t len;
  int comple;
  uint64_t addr,r2val;
  int speculative, advanced, cclear, cnclear;
  char r3nat, r2nat;

  /* 
   * just compute memory access information and get faults relating to
   * register access
   */
  comple= M3_x6(instr) >> 2;
  len = (1<<(M3_x6(instr)&3));
  speculative = comple==1 || comple==3;
  advanced = comple==2 || comple == 3;
  cclear = comple==8 || comple==10;
  cnclear = comple==9;

  /* Check for a bunch of bad conditions we can know now, but only report
   * them if the predicate is true...
   */

  /* check for target register update and destination the same */
  if (format != 101 && all_r1(instr) == all_r3(instr)) {
    report_intr(ii,IA64_intr_illegaloperation);
  }
  check_target_register(ii,all_r1(instr));
  if (format != 101) { 
    check_target_register(ii,all_r3(instr));
  }

  if (format == 102) {
    r2val = get_src_op(ii,src_update_amt,GR.val);
    r2nat = get_src_op(ii,src_update_amt,GR.nat);
  } else {
    r2val = 0; r2nat = 0; /* shut up warnings */
  }
  r3nat = get_src_op(ii,src_base,GR.nat);
  
  if (!speculative && r3nat) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }

  addr = get_src_op(ii,src_base,GR.val);

  ii->operand_src[src_load].spaceaddr.mem = addr;
  
  /* Now do the ALAT checking... */
  if ((cclear || cnclear)) {
    LSE_emu_addr_t addr;
    int alen;
    boolean check = alat_check_gr(ii,all_r1(instr),&addr,&alen);
    set_int_op(ii,int_alatcheck,alatcheck.present,check);
    set_int_op(ii,int_alatcheck,alatcheck.addr,addr);
    set_int_op(ii,int_alatcheck,alatcheck.len,alen);
  }
  dest_alatupdate_M1M2M3(ii);

  if (format == 102) {
    set_dest_op(ii,dest_baseupdate,GR.val,addr+r2val);
    set_dest_op(ii,dest_baseupdate,GR.nat,r3nat || r2nat);
  } else if (format == 103) {
    set_dest_op(ii,dest_baseupdate,GR.val,
		addr + get_src_op(ii,src_update_amt,imm64));
    set_dest_op(ii,dest_baseupdate,GR.nat,r3nat);
  }
  return;
}

void
memory_M_int_ld(LSE_emu_instr_info_t *ii) 
{ /* M1 - integer load */
  /* M2 - integer load, increment by register */
  /* M3 - integer load, increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t len;
  int comple;
  uint64_t addr;
  int speculative, advanced, cclear, cnclear, defer ;
  uint64_t psr;
  char r3nat;
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;

  comple= M3_x6(instr) >> 2;
  len = (1<<(M3_x6(instr)&3));
  speculative = comple==1 || comple==3;
  advanced = comple==2 || comple == 3;
  cclear = comple==8 || comple==10;
  cnclear = comple==9;
  
  r3nat = get_src_op(ii,src_base,GR.nat);
  psr = get_src_op(ii,src_psr,SR.PSR);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));
  
  addr = get_src_op(ii,src_base,GR.val);
  if ((cclear || cnclear) && get_int_op(ii,int_alatcheck,alatcheck.present)) {
  }
  else {
    /* Now do the memory access and look for/report errors */
    if (!defer) {
      /* should check for memory translation problems here */
      try {
	realct->mem->read(addr,
			  (unsigned char *)get_src_op(ii,src_load,memdata),
			  len);
      } catch (LSE_device::deverror_t e) {
	memerr = e.err;
      }

      defer = memerr && speculative ? 1 : 0;
      set_int_op(ii,int_memerr,error,memerr);
    }
  }
  set_int_op(ii,int_defer,PR,defer);
  return;
}

void 
format_M_int_ld(LSE_emu_instr_info_t *ii) 
{ /* M1 - integer load */
  /* M2 - integer load, increment by register */
  /* M3 - integer load, increment by immediate */
  uint64_t instr = ii->extra.instr;
  int len = (1<<(M3_x6(instr)&3));
  int comple;
  uint64_t newval, addr;
  int speculative, advanced, cclear, cnclear, defer ;
  uint64_t psr;
  char r3nat, newnat;
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;

  comple= M3_x6(instr) >> 2;
  len = (1<<(M3_x6(instr)&3));
  speculative = comple==1 || comple==3;
  advanced = comple==2 || comple == 3;
  cclear = comple==8 || comple==10;
  cnclear = comple==9;
  
  r3nat = get_src_op(ii,src_base,GR.nat);
  psr = get_src_op(ii,src_psr,SR.PSR);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));
  
  addr = get_src_op(ii,src_base,GR.val);

  if ((cclear || cnclear) && get_int_op(ii,int_alatcheck,alatcheck.present)) {
  }
  else {
    if (!defer) {

      memerr=(LSE_device::deverror_code_t)get_int_op(ii,int_memerr,error);
      /* This is more complicated somehow, having to do with deferred
       * exceptions while doing an advanced load; basically an advanced
       * load to a sequential page
       */
      if (memerr) {
	if (speculative) {
	  defer = 1;
	} else {
	  report_intr(ii,IA64_intr_memory);
	}
      }
    }
    if (defer) {
      if (speculative) {
	newval = 0; /* We have a choice what to do with 
		     * NaT'd speculative loads; we'll go with load 0
		     */
	newnat = 1;
      } else { /* we would get here after a deferred exception 
		  on advanced ld */
	newval = 0;
	newnat = 0; 
      }
    } else {
      /* Report the NaT bit we would get if there were no errors...*/
      if (comple==6) { /* fill */
	uint64_t unat;
	unat = get_src_op(ii,src_unat,AR);
	newnat = (unat>>((addr>>3) & 0x3f))&1;
      }
      else newnat = 0;
      newval = 0;
      if (rf_get(psr,PSR_be)) {
	/* newval is now big-endian with 0 in lower (in address) bytes */
	memcpy(((char *)&newval)+(8-len),
	       get_src_op(ii,src_load,memdata),len);
	newval = LSE_end_be2h_ll(newval);
      } else {
	memcpy((void *)&newval,get_src_op(ii,src_load,memdata),len);
	/* newval is now little-endian with 0 in upper (in addres) bytes */
	newval = LSE_end_le2h_ll(newval);
      }
    }
    set_dest_op(ii,dest_result,GR.nat,newnat);
    set_dest_op(ii,dest_result,GR.val,newval);
  }
  set_int_op(ii,int_defer,PR,defer);
  return;
}

void 
writeback_M_int_ld(LSE_emu_instr_info_t *ii, 
		   LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M1 - integer load */
  /* M2 - integer load, increment by register */
  /* M3 - integer load, increment by immediate */
  uint64_t instr = ii->extra.instr;
  int len = (1<<(M3_x6(instr)&3));
  int comple;
  uint64_t newval;
  int speculative, advanced, cclear, cnclear, defer ;
  uint64_t psr;
  char r3nat;

  if (!get_src_op(ii,src_qp,PR)) return;
  
  comple= M3_x6(instr) >> 2;
  len = (1<<(M3_x6(instr)&3));
  speculative = comple==1 || comple==3;
  advanced = comple==2 || comple == 3;
  cclear = comple==8 || comple==10;
  cnclear = comple==9;
  
  r3nat = get_src_op(ii,src_base,GR.nat);
  psr = get_src_op(ii,src_psr,SR.PSR);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));
  
  newval = 0;

  switch (opname) {
  case dest_result:
    if (!intr_cancels_instr(ii)) { 
      /* if there was no fault reported, we can update most state */
      if ((cclear || cnclear) && 
	  get_int_op(ii,int_alatcheck,alatcheck.present)) {
      } 
      else set_r(dest_result,ii,all_r1(instr),
		 get_dest_op(ii,dest_result,GR.val),
		 get_dest_op(ii,dest_result,GR.nat));
    }
    if (!fallthrough) break;
  case dest_alatupdate:
    if (advanced && speculative && get_int_op(ii,int_defer,PR)) {
      /* invalidating old ALAT entry */
      alat_inval_gr(ii,all_r1(instr));
    } else if (!intr_cancels_instr(ii)) { 
      if (get_dest_op(ii,dest_alatupdate,alatchange)>0) {
	/* insert new ALAT entry, invalidating old */
	alat_insert_gr(ii,all_r1(instr),
		       ii->operand_src[src_load].spaceaddr.mem,
		       ii->operand_src[src_load].uses.mem.size);
      }
      else if (get_dest_op(ii,dest_alatupdate,alatchange)<0) {
	/* invalidating old ALAT entry */
	alat_inval_gr(ii,all_r1(instr));
      }
    }
    else if (ii->privatef.actual_intr>=IA64_intr_memory) {
      if (get_dest_op(ii,dest_alatupdate,alatchange)<0) {
	/* invalidate old ALAT entry */
	alat_inval_gr(ii,all_r1(instr));
      }
    }
    if (!fallthrough) break;
  case dest_baseupdate:
    if (!intr_cancels_instr(ii) && ii->extra.formatno > 101) {
      set_r(dest_baseupdate,ii,all_r3(instr),
	    get_dest_op(ii,dest_baseupdate,GR.val),
	    get_dest_op(ii,dest_baseupdate,GR.nat));
    }
    if (!fallthrough) break;
  default: break;
  } /* switch */
  return;
}

void 
opfetch_M4(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M4 - integer store */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_store_data:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_store_data,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_M5(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M5 - integer store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t imm9;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    imm9 = M5_imm7a(instr);
    imm9 |= (M5_i(instr) << 7);
    if (M5_s(instr)) imm9 |= ~(0xff);
    set_src_op(ii,src_update_amt,imm64,imm9);
    if (!fallthrough) break;
  case src_store_data:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_store_data,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_M_int_st(LSE_emu_instr_info_t *ii) 
{ /* M4 - integer store */
  /* M5 - integer store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;
  int comple = (M4_x6(instr)>>2) & 3;
  uint64_t addr;

  addr = get_src_op(ii,src_base,GR.val);

  if (format == 105) {
    check_target_register(ii,all_r3(instr));
  }

  ii->operand_dest[dest_store].spaceaddr.mem = addr;
    
  if (comple==2) { /* spill */
    set_dest_op(ii,dest_unat,AR,
		(get_src_op(ii,src_store_data,GR.nat) ? 
		 (INT64_C(1)<<((addr>>3) & 0x3f)) : 0));
  }

  set_dest_op(ii,dest_alatupdate,alatchange,-1);

  if (format == 105) {
    set_dest_op(ii,dest_baseupdate,
		GR.val,addr+get_src_op(ii,src_update_amt,imm64));
    set_dest_op(ii,dest_baseupdate,GR.nat,0);
  }
  return;
}

void
format_M_int_st(LSE_emu_instr_info_t *ii) 
{ /* M4 - integer store */
  /* M5 - integer store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  int comple = (M4_x6(instr)>>2) & 3;
  uint64_t len = (1<<(M4_x6(instr)&3));
  uint64_t newval;
  uint64_t psr = get_src_op(ii,src_psr,SR.PSR);

  if (get_src_op(ii,src_base,GR.nat) ||
      (comple!=2 && get_src_op(ii,src_store_data,GR.nat))) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }

  if (comple==2 && get_src_op(ii,src_store_data,GR.nat)) { /* spill a NaT bit*/
    newval = 0; /* One of the possible options when spilling a register
		   with NaT */
    memcpy(get_dest_op(ii,dest_store,memdata),&newval,len);
  } else {
    newval = get_src_op(ii,src_store_data,GR.val);

    if (rf_get(psr,PSR_be)) {
      newval = LSE_end_h2be_ll(newval);
      memcpy(get_dest_op(ii,dest_store,memdata),
	     ((char *)&newval)+8-len,len);
    } else {
      newval = LSE_end_h2le_ll(newval);
      memcpy(get_dest_op(ii,dest_store,memdata),&newval,len);
    }
  }
  return;
}

void 
writeback_M_int_st(LSE_emu_instr_info_t *ii, 
		   LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M4 - integer store */
  /* M5 - integer store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t len = (1<<(M4_x6(instr)&3));
  uint64_t addr;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case 0: 
    if (!fallthrough) break;
  case dest_store: /* so we can fall through */
    addr = get_src_op(ii,src_base,GR.val);
    
    if (~ii->privatef.rollback_saved & (1<<dest_store)) {
      ii->privatef.rollback_saved |= (1<<dest_store);
      ii->privatef.rollback[dest_store].rtype = IA64_rollback_mem;
      ii->privatef.rollback[dest_store].data.mem.len = len;
      ii->privatef.rollback[dest_store].data.mem.addr = addr;
      /*fixme: jc1*/
      try {
	realct->mem->read(addr,
			  ii->privatef.rollback[dest_store].data.mem.data, len);
      } catch (LSE_device::deverror_t) {}
    }
    
    try {
      realct->mem->write(addr, 
			 (unsigned char *)get_dest_op(ii,dest_store,memdata),
			 len);
    } catch (LSE_device::deverror_t) {
      report_intr(ii,IA64_intr_memory);
      break; /* do not fall through on error... */
    }
    if (!fallthrough) break;
  case dest_unat:
    if ((M5_x6(instr)>>2)==14) {
      uint64_t mask;
      mask = (INT64_C(1)<<((get_src_op(ii,src_base,GR.val)>>3) & 0x3f));
      set_ar_bits(dest_unat,ii,IA64_ADDR_UNAT,
		  get_dest_op(ii,dest_unat,AR), mask);
    }
    if (!fallthrough) break;
  case dest_alatupdate:
    /* invalidate the ALAT */
    alat_inval_many(ii,ii->operand_dest[dest_store].spaceaddr.mem,
		    ii->operand_dest[dest_store].uses.mem.size);
    if (!fallthrough) break;
  case dest_baseupdate:
    if (ii->extra.formatno > 104) {
      set_r(dest_baseupdate,ii,all_r3(instr),
	    get_dest_op(ii,dest_baseupdate,GR.val),
	    get_dest_op(ii,dest_baseupdate,GR.nat));
    }
    if (!fallthrough) break;
  default: break;
  } /* switch */
  return;
}

void
opfetch_M6(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M6 - FP load */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
opfetch_M7(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M7 - FP load, increment by register */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_update_amt,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
opfetch_M8(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M8 - FP load, increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int64_t imm9;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    imm9 = M8_imm7b(instr);
    imm9 |= (M8_i(instr) << 7);
    if (M8_s(instr)) imm9 |= ~(0xff);
    set_src_op(ii,src_update_amt,imm64, imm9);
    if (!fallthrough) break;
  default: break;
  }
}

#define dest_alatupdate_M6M7M8(ii) dest_alatupdate_M1M2M3(ii)

void
evaluate_M_fp_ld(LSE_emu_instr_info_t *ii) 
{ /* M6 - FP load */
  /* M7 - FP load/increment by register */
  /* M8 - FP load/increment by immediate */
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;
  int comple = M6_x6(instr)>>2;
  uint64_t addr,r2val;
  int speculative = comple==1 || comple==3;
  /*  int advanced = comple==2 || comple == 3; */
  int cclear = comple==8;
  int cnclear = comple==9;
  uint64_t psr;
  char r3nat, r2nat;

  psr = get_src_op(ii,src_psr,SR.PSR);
  
  if (format != 106) { 
    check_target_register(ii,all_r3(instr));
  }
  fp_check_target_register(realct,ii,all_f1(instr));
    
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
    
  r3nat = get_src_op(ii,src_base,GR.nat);
  if (!speculative && r3nat) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }
    
  addr = get_src_op(ii,src_base,GR.val);

  ii->operand_src[src_load].spaceaddr.mem = addr;

  /* Now do the ALAT checking */
  if ((cclear || cnclear)) {
    LSE_emu_addr_t addr;
    int alen;
    boolean check = alat_check_fr(ii,all_f1(instr),&addr,&alen);
    set_int_op(ii,int_alatcheck,alatcheck.present,check);
    set_int_op(ii,int_alatcheck,alatcheck.addr,addr);
    set_int_op(ii,int_alatcheck,alatcheck.len,alen);
  }
  dest_alatupdate_M6M7M8(ii);

  if (format == 107) {
    r2val = get_src_op(ii,src_update_amt,GR.val);
    r2nat = get_src_op(ii,src_update_amt,GR.nat);
    set_dest_op(ii,dest_baseupdate,GR.val,addr+r2val);
    set_dest_op(ii,dest_baseupdate,GR.nat,r3nat || r2nat);
    /* implicit prefetch possible in uarch */
  } else if (format == 108) {
    set_dest_op(ii,dest_baseupdate,GR.val,
		addr+get_src_op(ii,src_update_amt,imm64));
    set_dest_op(ii,dest_baseupdate,GR.nat,r3nat);
  }
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  return;
}

void
memory_M_fp_ld(LSE_emu_instr_info_t *ii) 
{ /* M6 - FP load */
  /* M7 - FP load/increment by register */
  /* M8 - FP load/increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  static int sizes[] = { 
    10, 8, 4, 8
  };
  uint64_t len = (M6_x6(instr) != 0x1b) ? sizes[M6_x6(instr) & 3] : 16;
  int comple = M6_x6(instr)>>2;
  uint64_t addr;
  int speculative = comple==1 || comple==3;
  /*  int advanced = comple==2 || comple == 3; */
  int cclear = comple==8;
  int cnclear = comple==9;
  int defer;
  uint64_t psr;
  char r3nat;
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;
  
  r3nat = get_src_op(ii,src_base,GR.nat);
  psr = get_src_op(ii,src_psr,SR.PSR);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));
    
  addr = get_src_op(ii,src_base,GR.val);

  if ((cclear || cnclear) && get_int_op(ii,int_alatcheck,alatcheck.present)) {
  }
  else {
    if (!defer) {
      
      /* should check for memory translation problems here */
      try {
	realct->mem->read(addr,
			  (unsigned char *)get_src_op(ii,src_load,memdata),
			  len);
      } catch (LSE_device::deverror_t e) {
	memerr = e.err;
      }

      defer = memerr && speculative ? 1 : 0;
      set_int_op(ii,int_memerr,error,memerr);
    }
  }
  return;
}

void 
format_M_fp_ld(LSE_emu_instr_info_t *ii) 
{ /* M6 - FP load */
  /* M7 - FP load/increment by register */
  /* M8 - FP load/increment by immediate */
  uint64_t instr = ii->extra.instr;
  static int sizes[] = { 
    10, 8, 4, 8
  };
  uint64_t len = (M6_x6(instr) != 0x1b) ? sizes[M6_x6(instr) & 3] : 16;
  int comple = M6_x6(instr)>>2;
  uint64_t addr;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8;
  int cnclear = comple==9;
  int defer;
  uint64_t psr;
  char r3nat;
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;
  uint64_t v1, v2;
  struct {
      uint64_t lowhalf;
    uint64_t highhalf;
  } buffer;
  IA64_FR_t newval;
  
  r3nat = get_src_op(ii,src_base,GR.nat);
  psr = get_src_op(ii,src_psr,SR.PSR);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));
    
  addr = get_src_op(ii,src_base,GR.val);

  if ((cclear || cnclear) && get_int_op(ii,int_alatcheck,alatcheck.present)) {
  }
  else {
    if (!defer) {
      
      memerr = (LSE_device::deverror_code_t)get_int_op(ii,int_memerr,error);
      /* This is more complicated somehow, having to do with deferred
       * exceptions while doing an advanced load, but I can't find
       * when this would occur...
       */
      if (memerr) {
	if (speculative) {
	  defer = 1;
	} else {
	  report_intr(ii,IA64_intr_memory);
	}
      }
      /* do the access (already done) */
    }
    if (speculative && defer) {
      newval = IA64_NatVAL;
    } else if (advanced && !speculative && defer) {
      newval = ((M6_x6(instr) & 3)==1) ? IA64_FPIntZero : IA64_FPZero;
    } else {
      memcpy((void *)&buffer,get_src_op(ii,src_load,memdata),len);
      if (!(rf_get(psr,PSR_be))) {
	v1 = LSE_end_le2h_ll(buffer.lowhalf);
	v2 = LSE_end_le2h_ll(buffer.highhalf);
      } else {
	if (len <= 8) {
	  v1 = LSE_end_be2h_ll(buffer.lowhalf>>(64-8*len));
	  v2 = 0; /* shut up warnings */
	} else if (len==16) {
	  v1 = LSE_end_be2h_ll(buffer.highhalf);
	  v2 = LSE_end_be2h_ll(buffer.lowhalf);
	} else {
	  v2 = LSE_end_be2h_ll(buffer.lowhalf);
	  v1 = LSE_end_be2h_ll(buffer.highhalf);
	  v1 = (v1 << 16) | (v2 >> 48);
	  v2 = LSE_end_be2h_ll(buffer.highhalf) >> 48;
	}
      }
	
      if (len == 16) { /* fill */
	newval.significand = v1;
	newval.exponent = v2 & INT64_C(0x1ffff);
	newval.sign = (v2 >> 17) & 1;
      } else switch (M6_x6(instr) & 3) {
      case 0 : /* ldfe */
	fp_mem2reg_e(v1,v2,&newval);
	break;
      case 1 : /* ldf8 */
	fp_mem2reg_8(v1,&newval);
	break;
      case 2 : /* ldfs */
	fp_mem2reg_s(v1,&newval);
	break;
      case 3 : /* ldfd */
	fp_mem2reg_d(v1,&newval);
	break;
      } /* switch */
      
    }
    set_dest_op_f(ii,dest_result,newval);
  }
  return;
}

void 
writeback_M_fp_ld(LSE_emu_instr_info_t *ii, 
		  LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M6 - FP load */
  /* M7 - FP load/increment by register */
  /* M8 - FP load/increment by immediate */
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;
  int comple = M6_x6(instr)>>2;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8;
  int cnclear = comple==9;
  int defer;
  uint64_t psr;
  char r3nat;
  IA64_FR_t newval;
  
  if (!get_src_op(ii,src_qp,PR)) return;
 
  r3nat = get_src_op(ii,src_base,GR.nat);
  psr = get_src_op(ii,src_psr,SR.PSR);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));

  switch (opname) {
  case dest_result:
    if (!intr_cancels_instr(ii)) { 
      /* if there was no fault reported, we can update most state */
      if ((cclear || cnclear) && 
	  get_int_op(ii,int_alatcheck,alatcheck.present)) {
      } 
      else {
	get_dest_op_f(ii,dest_result,newval);
	set_f(dest_result,ii,all_f1(instr),newval);
      }
    }
    if (!fallthrough) break;
  case dest_alatupdate:
    if (advanced && speculative && get_int_op(ii,int_defer,PR)) {
      /* invalidating old ALAT entry */
      alat_inval_fr(ii,all_f1(instr));
    } else if (!intr_cancels_instr(ii)) { 
      /* if there was no fault reported, we can update most state */
      
      if (get_dest_op(ii,dest_alatupdate,alatchange)>0) {
	/* insert new ALAT entry, invalidating old */
	alat_insert_fr(ii,all_f1(instr),
		       ii->operand_src[src_load].spaceaddr.mem,
		       ii->operand_src[src_load].uses.mem.size);
      }
      else if (get_dest_op(ii,dest_alatupdate,alatchange)<0) {
	/* invalidating old ALAT entry */
	alat_inval_fr(ii,all_f1(instr));
      }
    } else if (ii->privatef.actual_intr>=IA64_intr_memory) {
      if (get_dest_op(ii,dest_alatupdate,alatchange)<0) {
	/* invalidate old ALAT entry */
	alat_inval_fr(ii,all_f1(instr));
      }
    }
    if (!fallthrough) break;
  case dest_baseupdate:
    if (!intr_cancels_instr(ii) && format > 106)
      set_r(dest_baseupdate,ii,all_r3(instr),
	    get_dest_op(ii,dest_baseupdate,GR.val),
	    get_dest_op(ii,dest_baseupdate,GR.nat));
    if (!fallthrough) break;
  case dest_psr:
    if (!intr_cancels_instr(ii)) { 
      set_sr_sbits(dest_psr,ii,IA64_ADDR_PSR,get_dest_op(ii,dest_psr,SR.PSR),
		   rf_mask(PSR_mfboth));
    }
    if (!fallthrough) break;
  default: break;
  } /* switch */
  return;
}

void 
opfetch_M9(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M9 - FP store */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base: 
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_store_data:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_store_data,*tval);
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_M10(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M10 - FP store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t imm9;
  IA64_FR_t *tval;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    imm9 = M10_imm7a(instr);
    imm9 |= (M10_i(instr) << 7);
    if (M10_s(instr)) imm9 |= ~(0xff);
    set_src_op(ii,src_update_amt,imm64,imm9);
    if (!fallthrough) break;
  case src_store_data:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_store_data,*tval);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_M_fp_st(LSE_emu_instr_info_t *ii) 
{ /* M9 - FP store */
  /* M10 - FP store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;
  uint64_t psr;
  uint64_t addr;

  psr = get_src_op(ii,src_psr,SR.PSR);
  if (format == 110) {
    check_target_register(ii,all_r3(instr));
  }
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  addr = get_src_op(ii,src_base,GR.val);

  ii->operand_dest[dest_store].spaceaddr.mem = addr;

  set_dest_op(ii,dest_alatupdate,alatchange,-1);
    
  if (format == 110) {
    set_dest_op(ii,dest_baseupdate,
		GR.val,addr+get_src_op(ii,src_update_amt,imm64));
    set_dest_op(ii,dest_baseupdate,GR.nat,0);
  }
  return;
}

void
format_M_fp_st(LSE_emu_instr_info_t *ii) 
{ /* M9 - FP store */
  /* M10 - FP store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  uint64_t len;
  IA64_FR_t f2val;
  uint64_t v1,v2=0;
  struct {
    uint64_t lowhalf;
    uint64_t highhalf;
  } buffer;
  uint64_t psr;

  psr = get_src_op(ii,src_psr,SR.PSR);
  get_src_op_f(ii,src_store_data,f2val);

  if (get_src_op(ii,src_base,GR.nat) ||
      (M9_x6(instr)!= 0x3b && fp_is_natval(f2val))) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }
    
  switch (M9_x6(instr)) {
    /* NOTE: casts to uint64_t are just to be safe because gcc occasionally
     * complains about the shift counts and I do not trust it to actually
     * do the right thing with these bit fields.
     */
  case 0x3b : /* stf.spill */
    len = 16;
    v1 = f2val.significand;
    v2 = ((uint64_t)f2val.exponent | 
	  ((uint64_t)f2val.sign << 17));
    break;
  case 0x30 : /* stfe */
    len = 10; /* double-extended precision */
    v1 = f2val.significand;
    v2 = (((uint64_t)f2val.exponent & 0x3fff) |
	  (((uint64_t)f2val.exponent>>2) &0x40)|
	  ((uint64_t)f2val.sign << 15));
    break;
  case 0x31 : /* stf8 */
    len = 8;
    v1 = f2val.significand;
    break;
  case 0x32 : /* stfs */
    len = 4;
    fp_reg2mem_s(&f2val,&v1);
    break;
  case 0x33 : /* stfd */
    len = 8;
    fp_reg2mem_d(&f2val,&v1);
    break;
  default: /* to shut up warnings */
    len = 0;
    v1 = 0;
  }
  
  if (!rf_get(psr,PSR_be)) {
    buffer.lowhalf = LSE_end_h2le_ll(v1);
    buffer.highhalf = LSE_end_h2le_ll(v2);
  } else {
    if (len <= 8) {
      buffer.lowhalf = LSE_end_h2be_ll(v1<<(64-8*len));
    } else if (len==16) {
      buffer.highhalf = LSE_end_h2be_ll(v1);
      buffer.lowhalf = LSE_end_h2be_ll(v2);
    } else {
      buffer.lowhalf = LSE_end_h2be_ll((v2 << 48) |
				       (v1 >> 16));
      buffer.highhalf = LSE_end_h2be_ll(v1 << 48);
    }
  }

  memcpy(get_dest_op(ii,dest_store,memdata),(void *)&buffer,len);
  return;
}

void 
writeback_M_fp_st(LSE_emu_instr_info_t *ii, 
		  LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M9 - FP store  */
  /* M10 - FP store - increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int format = ii->extra.formatno;
  uint64_t addr;
  uint64_t len=ii->operand_dest[dest_store].uses.mem.size;

  if (get_src_op(ii,src_qp,PR) && !intr_cancels_instr(ii)) {
    switch (opname) {
    case 0: /* so we can fall through */
      if (!fallthrough) break;
    case dest_store: 
      addr = get_src_op(ii,src_base,GR.val);

      if (~ii->privatef.rollback_saved & (1<<dest_store)) {
	ii->privatef.rollback_saved |= (1<<dest_store);
	ii->privatef.rollback[dest_store].rtype = IA64_rollback_mem;
	ii->privatef.rollback[dest_store].data.mem.len = len;
	ii->privatef.rollback[dest_store].data.mem.addr = addr;
	/*fixme: jc1*/
	realct->mem->read(addr,
			  ii->privatef.rollback[dest_store].data.mem.data,
			  len);
      }

      /* should check for memory translation problems here */
      try {
	realct->mem->write(addr,
			   (unsigned char *)get_dest_op(ii,dest_store,memdata),
			   len);
      } catch (LSE_device::deverror_t) {
	report_intr(ii,IA64_intr_memory);
	break; /* do not want to fall through on memory error */
      }
      if (!fallthrough) break;
    case dest_alatupdate:
      /* invalidate the ALAT */
      alat_inval_many(ii,ii->operand_dest[dest_store].spaceaddr.mem,
		      ii->operand_dest[dest_store].uses.mem.size);
      if (!fallthrough) break;
    case dest_baseupdate:
      if (format > 109)
	set_r(dest_baseupdate,ii,all_r3(instr),
	      get_dest_op(ii,dest_baseupdate,GR.val),
	      get_dest_op(ii,dest_baseupdate,GR.nat));
      if (!fallthrough) break;
    default: break;
    }
  }
}

void 
opfetch_M11(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M11 - FP load pair */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_M12(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M12 - FP load pair - increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    set_src_op(ii,src_update_amt,imm64, (M12_x6(instr) & 1)?16:8);
    if (!fallthrough) break;
  default: break;
  }
}

#define dest_alatupdate_M11M12(ii) dest_alatupdate_M1M2M3(ii)

void
evaluate_M_fp_ld_pair(LSE_emu_instr_info_t *ii) 
{ /* M11 - FP load pair  */
  /* M12 - FP load pair - increment by immediate */
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;
  int comple = M11_x6(instr)>>2;
  uint64_t addr;
  int speculative = comple==1 || comple==3;
  int cclear = comple==8;
  int cnclear = comple==9;
  uint64_t psr;
  char r3nat;
  int cbit;

  psr = get_src_op(ii,src_psr,SR.PSR);

  if (format == 111) { 
    check_target_register(ii,all_r3(instr));
  }

  /* figure out bank conflict */
  cbit = (all_f1(instr) ^ all_f2(instr)) & 1;
  if ((all_f1(instr) < 32 && all_f2(instr) < 32) ||
      (all_f1(instr) >= 32 && all_f2(instr) >= 32)) {
    if (!cbit) { /* bank conflict */
      report_intr(ii,IA64_intr_illegaloperation);
    }
  } else {
    IA64_SR_t cfm;
    cfm=get_src_op(ii,src_cfm,SR);
    if (cbit == (rf_get(cfm.CFM.val,CFM_rrb_fr) & 1)) {
      report_intr(ii,IA64_intr_illegaloperation);
    }
  }

  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_target_register(realct,ii,all_f2(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));
      
  r3nat = get_src_op(ii,src_base,GR.nat);
  if (!speculative && r3nat) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }

  addr = get_src_op(ii,src_base,GR.val);

  ii->operand_src[src_load].spaceaddr.mem = addr;
      
  /* Now do the ALAT checking */
  if ((cclear || cnclear)) {
    LSE_emu_addr_t addr;
    int alen;
    boolean check = alat_check_fr(ii,all_f1(instr),&addr,&alen);
    set_int_op(ii,int_alatcheck,alatcheck.present,check);
    set_int_op(ii,int_alatcheck,alatcheck.addr,addr);
    set_int_op(ii,int_alatcheck,alatcheck.len,alen);
  }

  dest_alatupdate_M11M12(ii);

  if (format == 112) {
    set_dest_op(ii,dest_baseupdate,GR.val,
		addr+get_src_op(ii,src_update_amt,imm64));
    set_dest_op(ii,dest_baseupdate,GR.nat,r3nat);
  }

  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  return;
}

void
memory_M_fp_ld_pair(LSE_emu_instr_info_t *ii) 
{ /* M11 - FP load pair  */
  /* M12 - FP load pair - increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t len = ((M6_x6(instr) &3)==2) ? 8 : 16;
  int comple = M11_x6(instr)>>2;
  uint64_t addr;
  int speculative = comple==1 || comple==3;
  /*  int advanced = comple==2 || comple == 3; */
  int cclear = comple==8;
  int cnclear = comple==9;
  int defer;
  uint64_t psr;
  char r3nat;
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;

  psr = get_src_op(ii,src_psr,SR.PSR);
  r3nat = get_src_op(ii,src_base,GR.nat);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));
      
  addr = get_src_op(ii,src_base,GR.val);

  if ((cclear || cnclear) && get_int_op(ii,int_alatcheck,alatcheck.present)) {
  }
  else {
    if (!defer) {
      
      /* should check for memory translation problems here */
      try {
	realct->mem->read(addr,
			  (unsigned char *)get_src_op(ii,src_load,memdata),
			  len);
      } catch (LSE_device::deverror_t e) {
	memerr = e.err;
      }

      defer = memerr && speculative ? 1 : 0;
      set_int_op(ii,int_memerr,error,memerr);
    }
  }
  return;
}


void 
format_M_fp_ld_pair(LSE_emu_instr_info_t *ii) 
{ /* M11 - FP load pair */
  /* M12 - FP load pair - increment by immediate */
  uint64_t instr = ii->extra.instr;
  uint64_t len = ((M6_x6(instr) &3)==2) ? 8 : 16;
  int comple = M11_x6(instr)>>2;
  uint64_t addr;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==7 || comple==9;
  int cnclear = comple==8;
  int defer;
  uint64_t psr;
  char r3nat;
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;
  uint64_t v1, v2;
  struct {
    uint64_t lowhalf;
    uint64_t highhalf;
  } buffer;
  IA64_FR_t newval1, newval2;

  psr = get_src_op(ii,src_psr,SR.PSR);
  r3nat = get_src_op(ii,src_base,GR.nat);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));
      
  addr = get_src_op(ii,src_base,GR.val);

  if ((cclear || cnclear) && get_int_op(ii,int_alatcheck,alatcheck.present)) {
  }
  else {
    if (!defer) {
      
      memerr = (LSE_device::deverror_code_t)get_int_op(ii,int_memerr,error);
      /* This is more complicated somehow, having to do with deferred
       * exceptions while doing an advanced load, but I can't find
       * when this would occur...
       */
      if (memerr) {
	if (speculative) {
	  defer = 1;
	} else {
	  report_intr(ii,IA64_intr_memory);
	}
      }
      /* do the access (already done) */
    }
    if (speculative && defer) {
      newval1 = newval2 = IA64_NatVAL;
    } else if (advanced && !speculative && defer) {
      newval1 = newval2 = ((M6_x6(instr) & 3)==1) ? 
	IA64_FPIntZero : IA64_FPZero;
    } else {
      memcpy((void *)&buffer,get_src_op(ii,src_load,memdata),len);
      if (!rf_get(psr,PSR_be)) {
	v1 = LSE_end_le2h_ll(buffer.lowhalf);
	v2 = LSE_end_le2h_ll(buffer.highhalf);
      } else {
	if (len == 8) {
	  v1 = LSE_end_be2h_ll(buffer.lowhalf);
	  v1 = (v1 >> 32) | (v1 << 32); /* swap words */
	  v2 = 0; /* shut up warnings */
	} else {
	  v1 = LSE_end_be2h_ll(buffer.highhalf);
	  v2 = LSE_end_be2h_ll(buffer.lowhalf);
	}
      }
      
      switch (M6_x6(instr) & 3) {
      case 1 : /* ldfp8 */
	fp_mem2reg_8(v1,&newval1);
	fp_mem2reg_8(v2,&newval2);
	break;
      case 2 : /* ldfps */
	fp_mem2reg_s(v1&0xffffffffU,&newval1);
	fp_mem2reg_s(v1>>32,&newval2);
	break;
      case 3 : /* ldfpd */
	fp_mem2reg_d(v1,&newval1);
	fp_mem2reg_d(v2,&newval2);
	break;
      } /* switch of formats */
      
    }
    set_dest_op_f(ii,dest_result,newval1);
    set_dest_op_f(ii,dest_result2,newval2);
  } /* else clear check */
}

void 
writeback_M_fp_ld_pair(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M11 - FP load pair */
  /* M12 - FP load pair - increment by immediate */
  uint64_t instr = ii->extra.instr;
  int comple = M11_x6(instr)>>2;
  int speculative = comple==1 || comple==3;
  int advanced = comple==2 || comple == 3;
  int cclear = comple==8;
  int cnclear = comple==9;
  int defer;
  uint64_t psr;
  char r3nat;
  IA64_FR_t newval;

  if (!get_src_op(ii,src_qp,PR)) return;

  psr = get_src_op(ii,src_psr,SR.PSR);
  r3nat = get_src_op(ii,src_base,GR.nat);
  defer = speculative && (r3nat || rf_get(psr,PSR_ed));

  switch (opname) {
  case dest_result:
    if (!intr_cancels_instr(ii)) { 
      /* if there was no fault reported, we can update most state */
      if ((cclear || cnclear) && 
	  get_int_op(ii,int_alatcheck,alatcheck.present)) {
      } 
      else {
	get_dest_op_f(ii,dest_result,newval);
	set_f(dest_result,ii,all_f1(instr),newval);
      }
    }
    if (!fallthrough) break;
  case dest_result2:
    if (!intr_cancels_instr(ii)) { 
      /* if there was no fault reported, we can update most state */
      if ((cclear || cnclear) && 
	  get_int_op(ii,int_alatcheck,alatcheck.present)) {
      } 
      else {
	get_dest_op_f(ii,dest_result2,newval);
	set_f(dest_result2,ii,all_f2(instr),newval);
      }
    }
    if (!fallthrough) break;
  case dest_alatupdate:
    if (advanced && speculative && get_int_op(ii,int_defer,PR)) {
      /* invalidating old ALAT entry */
      alat_inval_fr(ii,all_f1(instr));
    } else if (!intr_cancels_instr(ii)) { 
      if (get_dest_op(ii,dest_alatupdate,alatchange)>0) {
	/* insert new ALAT entry, invalidating old */
	alat_insert_fr(ii,all_f1(instr),
		       ii->operand_src[src_load].spaceaddr.mem,
		       ii->operand_src[src_load].uses.mem.size);
      }
      else if (get_dest_op(ii,dest_alatupdate,alatchange)<0) {
	/* invalidating old ALAT entry */
	alat_inval_fr(ii,all_f1(instr));
      }
    } else if (ii->privatef.actual_intr>=IA64_intr_memory) {
      if (get_dest_op(ii,dest_alatupdate,alatchange)<0) {
	/* invalidate old ALAT entry */
	alat_inval_fr(ii,all_f1(instr));
      }
    }
    if (!fallthrough) break;
  case dest_baseupdate:
    if (!intr_cancels_instr(ii) && ii->extra.formatno > 111) {
      set_r(dest_baseupdate,ii,all_r3(instr),
	    get_dest_op(ii,dest_baseupdate,GR.val),
	    get_dest_op(ii,dest_baseupdate,GR.nat));
    }
    if (!fallthrough) break;
  case dest_psr:
    if (!intr_cancels_instr(ii)) { 
      set_sr_sbits(dest_psr,ii,IA64_ADDR_PSR,get_dest_op(ii,dest_psr,SR.PSR),
		   rf_mask(PSR_mfboth));
    }
    if (!fallthrough) break;
  default: break;
  }
}

void
opfetch_M13(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,
	    int fallthrough) 
{ /* M13 - line prefetch */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:    
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
  default: break;
  }
}

void
opfetch_M14(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,
	    int fallthrough) 
{ /* M14 - line prefetch, increment by register */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:    
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_update_amt,*ival);
  default: break;
  }
}

void
opfetch_M15(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,
	    int fallthrough) 
{ /* M15 - line prefetch, increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int64_t imm9;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_update_amt:
    imm9 = M15_imm7b(instr);
    imm9 |= (M15_i(instr) << 7);
    if (M15_s(instr)) imm9 |= ~(0xff);
    set_src_op(ii,src_update_amt,imm64, imm9);
  default: break;
  }
}

void
evaluate_M_lfetch(LSE_emu_instr_info_t *ii) 
{ /* M13 - line prefetch */
  /* M14 - line prefetch, increment by register */
  /* M15 - line prefetch, increment by immediate */
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;
  int comple;
  uint64_t addr,r2val;
  char r3nat, r2nat;
  uint64_t psr;

  /* 
   * just compute memory access information and get faults relating to
   * register access
   */
  comple= M3_x6(instr) & 3;

  psr = get_src_op(ii,src_psr,SR.PSR);

  if (format != 113) { 
    check_target_register(ii,all_r3(instr));
  }

  r3nat = get_src_op(ii,src_base,GR.nat);
  
  if ((comple&2) && r3nat && !rf_get(psr,PSR_ed)) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }

  addr = get_src_op(ii,src_base,GR.val);

  ii->operand_src[src_load].spaceaddr.mem = addr;
  
  if (format == 114) {
    r2val = get_src_op(ii,src_update_amt,GR.val);
    r2nat = get_src_op(ii,src_update_amt,GR.nat);
    set_dest_op(ii,dest_baseupdate,GR.val,addr+r2val);
    set_dest_op(ii,dest_baseupdate,GR.nat,r3nat || r2nat);
  } else if (format == 115) {
    set_dest_op(ii,dest_baseupdate,GR.val,
		addr + get_src_op(ii,src_update_amt,imm64));
    set_dest_op(ii,dest_baseupdate,GR.nat,r3nat);
  }
  return;
}

void
memory_M_lfetch(LSE_emu_instr_info_t *ii) 
{ /* M13 - line prefetch */
  /* M14 - line prefetch, increment by register */
  /* M15 - line prefetch, increment by immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int comple;
  uint64_t addr;
  uint64_t len;
  uint64_t psr;
  char r3nat;
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;

  comple= M3_x6(instr) & 3;

  psr = get_src_op(ii,src_psr,SR.PSR);

  r3nat = get_src_op(ii,src_base,GR.nat);
  if (!r3nat && !rf_get(psr,PSR_ed)) {
    
    addr = get_src_op(ii,src_base,GR.val);
    len = 1;

    /* check for memory translation problems here */
    try {
      LSE_device::devdata_t *hp;
      len = realct->mem->translate(addr, &hp);
    } catch (LSE_device::deverror_t e) {
      memerr = e.err;
    }
    set_int_op(ii,int_memerr,error,memerr);
    if (memerr && (comple&2)) {
      report_intr(ii,IA64_intr_memory);
    } else {
      report_potential_intr(ii,IA64_intr_memory);
    }
  }
  return;
}

void 
writeback_M_lfetch(LSE_emu_instr_info_t *ii, 
		   LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M14 - line prefetch, increment by register */
  /* M15 - line prefetch, increment by immediate */
  uint64_t instr = ii->extra.instr;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case 0:
    if (!fallthrough) break;
  case dest_baseupdate:
    if (!intr_cancels_instr(ii)) {
      set_r(dest_baseupdate,ii,all_r3(instr),
	    get_dest_op(ii,dest_baseupdate,GR.val),
	    get_dest_op(ii,dest_baseupdate,GR.nat));
    }
    if (!fallthrough) break;
  default: break;
  } /* switch */
  return;
}

void
opfetch_M16(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M16 - Exchange/Compare and Exchange */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:    
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_ccv:
    set_src_op(ii,src_ccv,AR,get_ar(realct,ii,IA64_ADDR_CCV));
    if (!fallthrough) break;
  case src_store_data:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_store_data,*ival);
  default: break;
  }
}

void
evaluate_M16(LSE_emu_instr_info_t *ii) 
{ /* M16 - Exchange/Compare and Exchange */
  uint64_t instr = ii->extra.instr;
  int len = (1<<(M16_x6(instr)&3));
  uint64_t addr = get_src_op(ii,src_base,GR.val);

  check_target_register(ii,all_r1(instr));

  if (get_src_op(ii,src_base,GR.nat) || 
      get_src_op(ii,src_store_data,GR.nat)) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }
  
  ii->operand_src[src_load].spaceaddr.mem = addr;
  ii->operand_dest[dest_store].spaceaddr.mem = addr;

  if (addr & (len-1)) {
    report_intr(ii,IA64_intr_unaligneddatareference);
  }

  set_dest_op(ii,dest_alatupdate,alatchange,-1);
}

void
memory_M16(LSE_emu_instr_info_t *ii)
{ /* M16 - Exchange/Compare and Exchange */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;
  uint64_t addr, len;
  addr = get_src_op(ii,src_base,GR.val);
  len = (1<<(M16_x6(instr)&3));

  try {
    realct->mem->read(addr,
		      (unsigned char *)get_src_op(ii,src_load,memdata),
		      len);
  } catch (LSE_device::deverror_t e) {
    memerr = e.err;
  }
  set_int_op(ii,int_memerr,error,memerr);
}

void
format_M16(LSE_emu_instr_info_t *ii)
{ /* M16 - Exchange/Compare and Exchange */
  uint64_t instr = ii->extra.instr;
  uint64_t psr = get_src_op(ii,src_psr,SR.PSR);
  uint64_t newval;
  int len = (1<<(M16_x6(instr)&3));
  int isxchg = M16_x6(instr) > 7;

  /* format incoming data.... */
  newval = 0;
  if (rf_get(psr,PSR_be)) {
    /* newval is now big-endian with 0 in lower (in address) bytes */
    memcpy(((char *)&newval)+(8-len),
	   get_src_op(ii,src_load,memdata),len);
    newval = LSE_end_be2h_ll(newval);
  } else {
    memcpy((void *)&newval,get_src_op(ii,src_load,memdata),len);
    /* newval is now little-endian with 0 in upper (in addres) bytes */
    newval = LSE_end_le2h_ll(newval);
  }
  set_dest_op(ii,dest_result,GR.nat,0);
  set_dest_op(ii,dest_result,GR.val,newval);

  /* do the comparison.... */
  set_int_op(ii,int_comparison,PR,
	     (isxchg || (get_src_op(ii,src_ccv,AR) == newval)));

  if (get_int_op(ii,int_memerr,error)) {
    report_intr(ii,IA64_intr_memory);
  }

  /* format outgoing data.... */
  newval = get_src_op(ii,src_store_data,GR.val);
  if (rf_get(psr,PSR_be)) {
    newval = LSE_end_h2be_ll(newval);
    memcpy(get_dest_op(ii,dest_store,memdata),((char *)&newval)+8-len,len);
  } else {
    newval = LSE_end_h2le_ll(newval);
    memcpy(get_dest_op(ii,dest_store,memdata),&newval,len);
  }
}

void
writeback_M16(LSE_emu_instr_info_t *ii, 
	      LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M16 - Exchange/Compare and Exchange */
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t instr = ii->extra.instr;
  uint64_t len = (1<<(M16_x6(instr)&3));
  uint64_t addr;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case dest_result:
    set_r(dest_result,ii,all_r1(instr),
	  get_dest_op(ii,dest_result,GR.val),
	  get_dest_op(ii,dest_result,GR.nat));
    if (!fallthrough) break;
  case dest_store: 
    if (get_int_op(ii,int_comparison,PR)) {
      addr = get_src_op(ii,src_base,GR.val);
      
      if (~ii->privatef.rollback_saved & (1<<dest_store)) {
	ii->privatef.rollback_saved |= (1<<dest_store);
	ii->privatef.rollback[dest_store].rtype = IA64_rollback_mem;
	ii->privatef.rollback[dest_store].data.mem.len = len;
	ii->privatef.rollback[dest_store].data.mem.addr = addr;
	/*fixme: jc1*/
	realct->mem->read(addr,
			  ii->privatef.rollback[dest_store].data.mem.data,
			  len);
      }
      
      try {
	realct->mem->write(addr,
			   (unsigned char *)get_dest_op(ii,dest_store,memdata),
			   len);
      } catch (LSE_device::deverror_t) {
	report_intr(ii,IA64_intr_memory);
	break; /* do not fall through on error... */
      }
    }
    if (!fallthrough) break;
  case dest_alatupdate:
    if (get_int_op(ii,int_comparison,PR)) {
      /* invalidate the ALAT */
      alat_inval_many(ii,ii->operand_dest[dest_store].spaceaddr.mem,
		      ii->operand_dest[dest_store].uses.mem.size);
    }
  default: break;
  } /* switch */
  return;
}

void
opfetch_M17(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M17 - Fetch and Add - immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;
  static int incs[]={16,8,4,1};
 
  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_base:    
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_base,*ival);
    if (!fallthrough) break;
  case src_ccv:
    set_src_op(ii,src_ccv,AR,get_ar(realct,ii,IA64_ADDR_CCV));
    if (!fallthrough) break;
  case src_update_amt:
    set_src_op(ii,src_update_amt,imm64,
	       (M17_s(instr)? -1 : 1) * incs[M17_i2b(instr)]);
  default: break;
  }
}

void
evaluate_M17(LSE_emu_instr_info_t *ii) 
{ /* M17 - Fetch and Add - immediate */
  uint64_t instr = ii->extra.instr;
  int len = (1<<(M17_x6(instr)&3));
  uint64_t addr = get_src_op(ii,src_base,GR.val);

  check_target_register(ii,all_r1(instr));

  if (get_src_op(ii,src_base,GR.nat) || 
      get_src_op(ii,src_store_data,GR.nat)) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }
  
  ii->operand_src[src_load].spaceaddr.mem = addr;
  ii->operand_dest[dest_store].spaceaddr.mem = addr;

  if (addr & (len-1)) {
    report_intr(ii,IA64_intr_unaligneddatareference);
  }

  set_dest_op(ii,dest_alatupdate,alatchange,-1);
}

void
memory_M17(LSE_emu_instr_info_t *ii)
{ /* M17 - Fetch and Add - immediate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;
  uint64_t addr, len;

  addr = get_src_op(ii,src_base,GR.val);
  len = (1<<(M17_x6(instr)&3));

  try {
    realct->mem->read(addr,
		      (unsigned char *)get_src_op(ii,src_load,memdata),
		      len);
  } catch (LSE_device::deverror_t e) { 
    memerr = e.err; 
  }

  set_int_op(ii,int_memerr,error,memerr);
}

void
format_M17(LSE_emu_instr_info_t *ii)
{ /* M17 - Fetch and Add - immediate */
  uint64_t instr = ii->extra.instr;
  uint64_t psr = get_src_op(ii,src_psr,SR.PSR);
  uint64_t newval;
  int len = (1<<(M17_x6(instr)&3));

  /* format incoming data.... */
  newval = 0;
  if (rf_get(psr,PSR_be)) {
    /* newval is now big-endian with 0 in lower (in address) bytes */
    memcpy(((char *)&newval)+(8-len),
	   get_src_op(ii,src_load,memdata),len);
    newval = LSE_end_be2h_ll(newval);
  } else {
    memcpy((void *)&newval,get_src_op(ii,src_load,memdata),len);
    /* newval is now little-endian with 0 in upper (in addres) bytes */
    newval = LSE_end_le2h_ll(newval);
  }
  set_dest_op(ii,dest_result,GR.nat,0);
  set_dest_op(ii,dest_result,GR.val,newval);

  if (get_int_op(ii,int_memerr,error)) {
    report_intr(ii,IA64_intr_memory);
  }

  /* format outgoing data.... */
  newval += get_src_op(ii,src_update_amt,GR.val);
  if (rf_get(psr,PSR_be)) {
    newval = LSE_end_h2be_ll(newval);
    memcpy(get_dest_op(ii,dest_store,memdata),((char *)&newval)+8-len,len);
  } else {
    newval = LSE_end_h2le_ll(newval);
    memcpy(get_dest_op(ii,dest_store,memdata),&newval,len);
  }
}

void
writeback_M17(LSE_emu_instr_info_t *ii, 
	      LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M17 - Fetch and Add - immediate */
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t instr = ii->extra.instr;
  uint64_t len = (1<<(M17_x6(instr)&3));
  uint64_t addr;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case dest_result:
    set_r(dest_result,ii,all_r1(instr),
	  get_dest_op(ii,dest_result,GR.val),
	  get_dest_op(ii,dest_result,GR.nat));
    if (!fallthrough) break;
  case dest_store: 
    addr = get_src_op(ii,src_base,GR.val);
      
    if (~ii->privatef.rollback_saved & (1<<dest_store)) {
      ii->privatef.rollback_saved |= (1<<dest_store);
      ii->privatef.rollback[dest_store].rtype = IA64_rollback_mem;
      ii->privatef.rollback[dest_store].data.mem.len = len;
      ii->privatef.rollback[dest_store].data.mem.addr = addr;
      /*fixme: jc1*/
      realct->mem->read(addr,
			ii->privatef.rollback[dest_store].data.mem.data, len);
    }
    
    try {
      realct->mem->write(addr,
			 (unsigned char *)get_dest_op(ii,dest_store,memdata),
			 len);
    } catch (LSE_device::deverror_t) {
      report_intr(ii,IA64_intr_memory);
      break; /* do not fall through on error... */
    }
    if (!fallthrough) break;
  case dest_alatupdate:
    /* invalidate the ALAT */
    alat_inval_many(ii,ii->operand_dest[dest_store].spaceaddr.mem,
		    ii->operand_dest[dest_store].uses.mem.size);
  default: break;
  } /* switch */
  return;
}

void 
opfetch_M18(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M18 - Set FR */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_M18(LSE_emu_instr_info_t *ii) 
{ /* M18 - Set FR */
  uint64_t instr = ii->extra.instr;
  IA64_FR_t newval;
  uint64_t psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));

  if (get_src_op(ii,src_op1,GR.nat)) {
    newval = IA64_NatVAL;
  } else { /* do real work */
    uint64_t r2val = get_src_op(ii,src_op1,GR.val);
    
    switch (M18_x6(instr) & 3) {
    case 0 : /* setf.sig */
      fp_mem2reg_8(r2val,&newval);
      break;
    case 1 : /* setf.exp */
      newval.significand = (INT64_C(1)<<63);
      newval.exponent = r2val & 0x1ffff;
      newval.sign = (r2val >> 17) & 1;
      break;
    case 2 : /* setf.s */
      fp_mem2reg_s(r2val,&newval);
      break;
    case 3 : /* setf.d */
      fp_mem2reg_d(r2val,&newval);
      break;
    }
  }
  set_dest_op_f(ii,dest_result,newval);

  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  return;
}

void 
opfetch_M19(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M19 - Get FR */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_M19(LSE_emu_instr_info_t *ii) 
{ /* M19 - Get FR */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t r2val;
  uint64_t newval;
    
  psr = get_src_op(ii,src_psr,SR.PSR);

  check_target_register(ii,all_r1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  get_src_op_f(ii,src_op1,r2val);
  
  switch (M19_x6(instr) & 3) {
  case 0 : /* getf.sig */
    newval = r2val.significand;
    break;
  case 1 : /* getf.exp */
    newval = ((uint64_t)r2val.sign << 17) | (uint64_t)r2val.exponent;
    break;
  case 2 : /* getf.s */
    newval = (
	      ( (r2val.significand>>40) & 0x7fFfff) |
	      ( ((int64_t)r2val.significand>>63) & 
		((((uint64_t)r2val.exponent & INT64_C(0x7f)) << 23) |
		 (((uint64_t)r2val.exponent & INT64_C(0x10000))<<14))) |
	      ( (uint64_t)r2val.sign<<31));
    break;
  case 3 : /* getf.d */
    newval 
      = (
	 ( ((uint64_t)r2val.significand >>11) 
	   & INT64_C(0x000fFfffFfffFfff)) |
	 ( ((int64_t)r2val.significand>>63) &
	   ( (((uint64_t)r2val.exponent & INT64_C(0x3ff)) << 52) |
	     (((uint64_t)r2val.exponent & INT64_C(0x10000)) << 46))) |
	 ( (uint64_t)r2val.sign << 63)
	 );
    break;
  default:
    newval = 0; /* shut up warnings */
    break;
  }
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,fp_is_natval(r2val));
  return;
}

void 
opfetch_M20(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M20 - Integer speculation check (M-unit) */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,ii->branch_targets[1]); /* already done */
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_M20(LSE_emu_instr_info_t *ii) 
{ /* I20, M20 - Integer speculation check */
  uint64_t psr;
  int taken = get_src_op(ii,src_qp,PR) && get_src_op(ii,src_op1,GR.nat);
  
  psr = get_src_op(ii,src_psr,SR.PSR);
  if (taken) {
    if (IA64_implemented_check_branch_general) {
      uint64_t tmp_IP = get_src_op(ii,src_op2,imm64);
      if (get_src_op(ii,src_qp,PR)) {
	ii->branch_dir = 1;
	ii->next_pc = tmp_IP;
      }

      if ((rf_get(psr,PSR_it) && IA64_unimplemented_va(tmp_IP)) ||
	  (!rf_get(psr,PSR_it) && IA64_unimplemented_pa(tmp_IP))) {
	report_intr(ii,IA64_intr_unimplementedinstructionaddress);
      }
      if (rf_get(psr,PSR_tb)) {
	report_intr(ii,IA64_intr_takenbranch);
      }
    } else {
      report_intr(ii,IA64_intr_speculativeoperation);
    }
  } else {
    ii->branch_dir = 0;
    /* not needed because bookkeeping.c does it... */
    /* ii->next_pc = ii->branch_targets[0]; */ 
  }
  return;
}

void 
opfetch_M21(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M21 - FP speculation check */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,ii->branch_targets[1]); /* already done */
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_M21(LSE_emu_instr_info_t *ii) 
{ /* M21 - FP speculation check */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t f2val;
  int taken;

  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  get_src_op_f(ii,src_op1,f2val);
  taken = get_src_op(ii,src_qp,PR) && fp_is_natval(f2val);  
  ii->branch_dir = 0;
  if (taken) {
    if (IA64_implemented_check_branch_float) {
      uint64_t tmp_IP = get_src_op(ii,src_op2,imm64);
      if (get_src_op(ii,src_qp,PR)) {
	ii->branch_dir = 1;
	ii->next_pc = tmp_IP;
      }

      if ((rf_get(psr,PSR_it) && IA64_unimplemented_va(tmp_IP)) ||
	  (!rf_get(psr,PSR_it) && IA64_unimplemented_pa(tmp_IP))) {
	report_intr(ii,IA64_intr_unimplementedinstructionaddress);
      }
      if (rf_get(psr,PSR_tb)) {
	report_intr(ii,IA64_intr_takenbranch);
      }
    } else {
      report_intr(ii,IA64_intr_speculativeoperation);
    }
  }
  return;
}

void 
opfetch_M_chk_a(LSE_emu_instr_info_t *ii, 
		LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M22 - Integer advanced load check */
  /* M23 - FP advanced load check */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,ii->branch_targets[1]); /* already done */
    if (!fallthrough) break;
  default: break;
  }
}

static inline void
dest_alatupdate_M22M23(LSE_emu_instr_info_t *ii) 
{ /* M22 - Integer advanced load check */
  /* M23 - FP advanced load check */
  uint64_t instr = ii->extra.instr;
  boolean taken, impl;

  taken = (ii->extra.formatno == 122 ? (all_r1(instr)==0) :
	   (all_f1(instr) == 0 || all_f1(instr) == 1));
  taken = taken || !get_int_op(ii,int_alatcheck,alatcheck.present);
  impl = (ii->extra.formatno == 122 ? IA64_implemented_check_branch_general:
	  IA64_implemented_check_branch_float);

  if (taken) {
    if (impl) {
      set_dest_op(ii,dest_alatupdate,alatchange,0);
    } else {
      set_dest_op(ii,dest_alatupdate,alatchange,0);
    }
  } else 
    if (M22_x3(instr) & 1)
      set_dest_op(ii,dest_alatupdate,alatchange,-1);
    else
      set_dest_op(ii,dest_alatupdate,alatchange,0);

  return;
}

void
evaluate_M_chk_a(LSE_emu_instr_info_t *ii) 
{ /* M22 - Integer advanced load check */
  /* M23 - FP advanced load check */
  uint64_t instr = ii->extra.instr;
  int check, alen;
  LSE_emu_addr_t addr;

  check = ((ii->extra.formatno==122) ? 
	   alat_check_gr(ii,all_r1(instr),&addr,&alen) : 
	   alat_check_fr(ii,all_f1(instr),&addr,&alen));
  set_int_op(ii,int_alatcheck,alatcheck.present,check);
  set_int_op(ii,int_alatcheck,alatcheck.addr,addr);
  set_int_op(ii,int_alatcheck,alatcheck.len,alen);
  dest_alatupdate_M22M23(ii);
  return;
}

void
format_M_chk_a(LSE_emu_instr_info_t *ii) 
{ /* M22 - Integer advanced load check */
  /* M23 - FP advanced load check */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  int taken;
  int impl;

  taken = (ii->extra.formatno == 122 ? (all_r1(instr)==0) :
	   (all_f1(instr) == 0 || all_f1(instr) == 1));
  taken = taken || !get_int_op(ii,int_alatcheck,alatcheck.present);
  psr = get_src_op(ii,src_psr,SR.PSR);
  impl = (ii->extra.formatno == 122 ? IA64_implemented_check_branch_general:
	  IA64_implemented_check_branch_float);

  ii->branch_dir = 0;
  if (taken) {
    if (impl) {
      uint64_t tmp_IP = get_src_op(ii,src_op2,imm64);
      if (get_src_op(ii,src_qp,PR)) {
	ii->branch_dir = 1;
	ii->next_pc = tmp_IP;
      }

      if ((rf_get(psr,PSR_it) && IA64_unimplemented_va(tmp_IP)) ||
	  (!rf_get(psr,PSR_it) && IA64_unimplemented_pa(tmp_IP))) {
	report_intr(ii,IA64_intr_unimplementedinstructionaddress);
      }
      if (rf_get(psr,PSR_tb)) {
	report_intr(ii,IA64_intr_takenbranch);
      }
     } else {
      report_intr(ii,IA64_intr_speculativeoperation);
    }
  }

  return;
}

void
writeback_M_chk_a(LSE_emu_instr_info_t *ii, 
		  LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M22 - Integer advanced load check */
  /* M23 - FP advanced load check */
  uint64_t instr = ii->extra.instr;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case 0: /* for fallthrough */
    if (!fallthrough) break;
  case dest_alatupdate:
    if (get_dest_op(ii,dest_alatupdate,alatchange)<0) {
      /* invalidate an ALAT entry as instructed */
      if (ii->extra.formatno == 122) {
	alat_inval_gr(ii,all_r1(instr));
      } else {
	alat_inval_fr(ii,all_f1(instr));
      }
    }
    if (!fallthrough) break;
  default: break;
  }
  return;
}

static void inline
dest_alatupdate_M24(LSE_emu_instr_info_t *ii) 
{ /* M24 - Sync/Fence/Serialize/ALAT control */
  uint64_t instr = ii->extra.instr;
  switch (M24_x2(instr)) {
  case 1:
    set_dest_op(ii,dest_alatupdate,alatchange,-1);
    break;
  }
}

void 
opfetch_M24(LSE_emu_instr_info_t *ii, 
	    LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M25 - RSE Control */
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  default: break;
  }
}

void 
evaluate_M24(LSE_emu_instr_info_t *ii) 
{ /* M24 - Sync/Fence/Serialize/ALAT control */
  dest_alatupdate_M24(ii);
}

void 
writeback_M24(LSE_emu_instr_info_t *ii, 
	    LSE_emu_operand_name_t opname, int fallthrough) 
{  /* M24 - Sync/Fence/Serialize/ALAT control */
  uint64_t instr = ii->extra.instr;

  /* only invala has any work to do here at all */

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
    case 0: 
      if (!fallthrough) break;
    case dest_alatupdate:
      if (M24_x2(instr)==1 && get_dest_op(ii,dest_alatupdate,alatchange)<0) {
	/* invalidating old ALAT entry */
	alat_inval_all(ii);
      }
      break;
  default: break;
  }
}

void 
opfetch_M25(LSE_emu_instr_info_t *ii, 
	    LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M25 - RSE Control */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  if (M25_x4(instr)&0x4) { /* flushrs */
    switch (opname) {
      OPFETCH_PREFIX;
    case src_bspstore:
      set_src_op(ii,src_bspstore,AR,get_ar(realct,ii,IA64_ADDR_BSPSTORE));
      if (!fallthrough) break;
    case src_bsp:
      set_src_op(ii,src_bsp,AR,get_ar(realct,ii,IA64_ADDR_BSP));
      if (!fallthrough) break;
    case src_rnat:
      set_src_op(ii,src_rnat,AR,get_ar(realct,ii,IA64_ADDR_RNAT));
      if (!fallthrough) break;
    case src_rsc:
      set_src_op(ii,src_rsc,AR,get_ar(realct,ii,IA64_ADDR_RSC));
      if (!fallthrough) break;
    case src_rse:
      set_src_op_rse(ii,src_rse,
		     realct->RSE_other.NDirty,realct->RSE_other.NClean);
      if (!fallthrough) break;
    default: break;
    }
  } else { /* loadrs */
    switch (opname) {
      OPFETCH_PREFIX;
    case src_bsp:
      set_src_op(ii,src_bspstore,AR,get_ar(realct,ii,IA64_ADDR_BSP));
      if (!fallthrough) break;
    case src_rnat:
      set_src_op(ii,src_rnat,AR,get_ar(realct,ii,IA64_ADDR_RNAT));
      if (!fallthrough) break;
    case src_rsc:
      set_src_op(ii,src_rsc,AR,get_ar(realct,ii,IA64_ADDR_RSC));
      if (!fallthrough) break;
    case src_rse:
      set_src_op_rse(ii,src_rse,
		     realct->RSE_other.NDirty,realct->RSE_other.NClean);
      if (!fallthrough) break;
    default: break;
    }
  }
}

void 
evaluate_M25(LSE_emu_instr_info_t *ii) 
{ /* M25 - RSE Control */
  uint64_t instr = ii->extra.instr;
  uint64_t bspstore, rnat, bsp;
  int ndirty, nclean, natcols;

  /*  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);*/

  if (M25_x4(instr)&0x4) { /* flushrs */
    
    /******** RSE stuff ************/
    bspstore = get_src_op(ii,src_bspstore,AR);
    bsp = get_src_op(ii,src_bsp,AR);
    rnat = get_src_op(ii,src_rnat,AR);
    ndirty = get_src_op(ii,src_rse,RSE_other.NDirty);
    nclean = get_src_op(ii,src_rse,RSE_other.NClean);
    
    /* NOTE: we're reporting only the number of dirty registers, not the
     * number of words; if there are 0 dirty registers, there might
     * still be an RNAT collection out there...
     */
    natcols = (((bspstore>>3)&0x3f)+ndirty)/63;
    set_dest_op_rsechange(ii,dest_rsemem,ndirty,ndirty+natcols);
    bspstore += 8*(ndirty + natcols);

    set_dest_op_rse(ii,dest_rse,0,nclean+ndirty);
    set_dest_op(ii,dest_bspstore,AR,bspstore);
    /* actually the starting NAT, not the final, but the final can't be done 
       at this point */
    set_dest_op(ii,dest_rnat,AR,rnat);

  } else { /* loadrs */
    report_actual_intr(ii,IA64_intr_notimplemented);
  }
  return;
}

void 
writeback_M25(LSE_emu_instr_info_t *ii, 
	    LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M25 - RSE Control */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

    /* not predicated */
  if (intr_cancels_instr(ii)) return;

  if (M25_x4(instr)&0x4) { /* flushrs */
    switch (opname) {
    case dest_rsemem:

      if (get_dest_op(ii,dest_rsemem,rsechange.numwords)) { /* do a spill */
	/* this may change RNAT dest operands */
	LSE_emu_addr_t bspstore = get_src_op(ii,src_bspstore,AR);
	int bitnum = (bspstore >> 3) & 0x3f;
	int numtodo = get_dest_op(ii,dest_rsemem,rsechange.numwords);
	uint64_t mask = INT64_C(1) << bitnum;
	uint64_t rnat = get_dest_op(ii,dest_rnat,AR);
	uint64_t len = 8;

	int rno = (get_src_op(ii,src_cfm,SR.CFM.BOF) -
		   get_src_op(ii,src_rse,RSE_other.NDirty));
	rno = IA64_GR_FAST_ROT(rno + realct->num_stacked_phys,
			       realct->num_stacked_phys);

#ifdef DEBUG_RSE
	if (realct->di->IA64_rsedebugprint) {
	  fprintf(stderr,
		  "RSE: flushing %d registers\n",numtodo);
	}
#endif
	while (numtodo) {
	  if (bitnum==63) {
#ifdef DEBUG_RSE
	    if (realct->di->IA64_rsedebugprint) {
	      /* store here */
	      fprintf(stderr,
		      "RSE: flushing RNAT %016"PRIX64" to %016"PRIX64"\n", 
		      rnat,bspstore);
	    }
#endif
	    /* TODO: endianness 
	       jc1: fixed?
	     */
	    rnat = LSE_end_le2h_ll(rnat);

	    try {
	      realct->mem->write(bspstore, (unsigned char *)&rnat, len);
	    } catch (LSE_device::deverror_t) {
	      report_intr(ii,IA64_intr_memory);
	      break; /* do not fall-through on error */
	    }
	    bitnum = 0;
	    bspstore += 8;
	    mask = INT64_C(1);
	    numtodo--;
	    continue;
	  }
	  rnat = (rnat & ~mask) | 
	    (((uint64_t)realct->physr[rno].nat) << bitnum);
	  /* store here */
#ifdef DEBUG_RSE
	  if (realct->di->IA64_rsedebugprint) {
	    fprintf(stderr,
		    "RSE: flushing %d %c %016"PRIX64" to %016"PRIX64"\n", 
		    rno,
		    realct->physr[rno].nat ? 'N' : '-',
		    realct->physr[rno].val,
		    bspstore);
	  }
#endif
	  /* TODO: endianness 
	     jc1: fixed?
	   */
	  realct->physr[rno].val = LSE_end_le2h_ll(realct->physr[rno].val);

	  try {
	    realct->mem->write(bspstore,
			       (unsigned char *)&realct->physr[rno].val, len);
	  } catch (LSE_device::deverror_t) {
	    report_intr(ii,IA64_intr_memory);
	    break; /* do not fall-through on error */
	  }

	  numtodo--;
	  bspstore += 8;
	  bitnum++;
	  rno = IA64_GR_FAST_ROT(rno + 1,realct->num_stacked_phys);
	  mask <<= 1;
	}
#ifdef DEBUG_RSE
	if (realct->di->IA64_rsedebugprint) {
	  fprintf(stderr,"RSE: rnat is now %016"PRIX64"\n",rnat);
	  fprintf(stderr,"RSE: BSPSTORE now %016"PRIX64"\n",
		  get_dest_op(ii,dest_bspstore,AR));
	}
#endif
	set_dest_op(ii,dest_rnat,AR,rnat);
      }
      if (!fallthrough) break;
    case dest_bspstore:
      set_ar(dest_bspstore,ii,IA64_ADDR_BSPSTORE,
	     get_dest_op(ii,dest_bspstore,AR));
      if (!fallthrough) break;
    case dest_rnat:
      set_ar(dest_rnat,ii,IA64_ADDR_RNAT,get_dest_op(ii,dest_rnat,AR));
      if (!fallthrough) break;
    case dest_rse:
      save_rse(dest_rse,ii); 
      realct->RSE_other.NDirty = get_dest_op(ii,dest_rse,RSE_other.NDirty);
      realct->RSE_other.NClean = get_dest_op(ii,dest_rse,RSE_other.NClean);
      if (!fallthrough) break;
    default: break;
    }
  } else { /* loadrs */
    report_actual_intr(ii,IA64_intr_notimplemented);
  }
}

void 
opfetch_M_invala_e(LSE_emu_instr_info_t *ii, 
		   LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M26 - Integer ALAT invalidate */
  /* M27 - FP ALAT invalidate */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  default: break;
  }
}

static inline void
dest_alatupdate_M26M27(LSE_emu_instr_info_t *ii) 
{ /* M26 - Integer ALAT invalidate */
  /* M27 - FP ALAT invalidate */
  if (get_int_op(ii,int_alatcheck,alatcheck.present)) {
    set_dest_op(ii,dest_alatupdate,alatchange,-1);
  } else {
    set_dest_op(ii,dest_alatupdate,alatchange,0);
  }
  return;
}

void
evaluate_M_invala_e(LSE_emu_instr_info_t *ii) 
{ /* M26 - Integer ALAT invalidate */
  /* M27 - FP ALAT invalidate */
  uint64_t instr = ii->extra.instr;
  int check, alen;
  LSE_emu_addr_t addr;

  check = ((ii->extra.formatno==126) ? 
	   alat_check_gr(ii,all_r1(instr),&addr,&alen) : 
	   alat_check_fr(ii,all_f1(instr),&addr,&alen));
  set_int_op(ii,int_alatcheck,alatcheck.present,check);
  set_int_op(ii,int_alatcheck,alatcheck.addr,addr);
  set_int_op(ii,int_alatcheck,alatcheck.len,alen);
  dest_alatupdate_M26M27(ii);
  return;
}

void
writeback_M_invala_e(LSE_emu_instr_info_t *ii, 
		     LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M26 - Integer ALAT invalidate */
  /* M27 - FP ALAT invalidate */
  uint64_t instr = ii->extra.instr;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case 0: /* for fallthrough */
    if (!fallthrough) break;
  case dest_alatupdate:
    if (get_dest_op(ii,dest_alatupdate,alatchange)<0) {
      /* invalidate an ALAT entry as instructed */
      if (ii->extra.formatno == 126) {
	alat_inval_gr(ii,all_r1(instr));
      } else {
	alat_inval_fr(ii,all_f1(instr));
      }
    }
    if (!fallthrough) break;
  default: break;
  }
  return;
}


void
opfetch_mov_to_AR(LSE_emu_instr_info_t *ii, 
		  LSE_emu_operand_name_t opname, int fallthrough) 
{ /* I26 - Move to Application Register (I form) */
  /* I27 - Move to Application Register immediate (I form) */
  /* M29 - Move to Application Register (M form) */
  /* M30 - Move to Application Register immediate (M form) */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int format = ii->extra.formatno;
  int isimm = (format==27 || format==130);
  uint64_t newval;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    if (isimm) {
      newval = I27_imm7b(instr);
      if (I27_s(instr)) newval |= (~INT64_C(0x7f));
      set_src_op(ii,src_op1,imm64,newval);
    } else {
      ival = get_r_ptr(realct,ii,all_r2(instr));
      set_src_op_r(ii,src_op1,*ival);
    }
    if (!fallthrough) break;
  case src_rsc:
    if (M29_ar3(instr) == IA64_ADDR_BSPSTORE) {
      set_src_op(ii,src_rsc,AR,get_ar(realct,ii,IA64_ADDR_RSC));
    } else if (M29_ar3(instr) == IA64_ADDR_RNAT) {
      set_src_op(ii,src_rsc,AR,get_ar(realct,ii,IA64_ADDR_RSC));
    }
    if (!fallthrough) break;
  case src_rse:
    if (M29_ar3(instr) == IA64_ADDR_BSPSTORE) {
      set_src_op_rse(ii,src_rse,
		     realct->RSE_other.NDirty,realct->RSE_other.NClean);
    } 
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_mov_to_AR(LSE_emu_instr_info_t *ii) /* I26, I27, M29, M30 */
{ /* I26 - Move to Application Register (I form) */
  /* I27 - Move to Application Register immediate (I form) */
  /* M29 - Move to Application Register (M form) */
  /* M30 - Move to Application Register immediate (M form) */
  int unit = ii->extra.unit;
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;

  IA64_AR_t newval;
  int isimm = (format==27 || format==130);

  newval = (isimm ? get_src_op(ii,src_op1,imm64) : 
	    get_src_op(ii,src_op1,GR.val));

  /* reserved register check */
  if ((unit==0 && M29_ar3(instr) < 48) ||
      (unit==1 && M29_ar3(instr) > 63 &&
       M29_ar3(instr) < 112) ||
      IA64_ar_reserved[M29_ar3(instr)]) {
    report_intr(ii,IA64_intr_illegaloperation);
  }

  /* read-only register check */
  if (M29_ar3(instr) == IA64_ADDR_BSP) {
    report_intr(ii,IA64_intr_illegaloperation);
  } else if (M29_ar3(instr) == IA64_ADDR_BSPSTORE || 
	     M29_ar3(instr) == IA64_ADDR_RNAT) {
    IA64_AR_t rsc;
    rsc = get_src_op(ii,src_rsc,AR);
    if (rf_get(rsc,RSC_mode) != 0) {
      report_intr(ii,IA64_intr_illegaloperation);
    }
  }
  
  if (!isimm && get_src_op(ii,src_op1,GR.nat)) {
    report_intr(ii,IA64_intr_registernatconsumption);
  }
  
  /* reserved field check/clear ignored bits */
    
  switch (M29_ar3(instr)) {
  case IA64_ADDR_RSC:
    {
      uint64_t psr = get_src_op(ii,src_psr,SR.PSR);
      if (rf_get(newval,RSC_rv) || rf_get(newval,RSC_rv2)) {
	report_intr(ii,IA64_intr_reservedregisterfield);
      }
      if (rf_get(newval,RSC_pl) < rf_get(psr,PSR_cpl)) /* illegal promotion */
	rf_set(newval,RSC_pl,rf_get(psr,PSR_cpl));
    }
    break;
  case IA64_ADDR_BSPSTORE:
    {
      int ndirty;
      uint64_t newbsp;
      newval &= ~INT64_C(0x7);

      ndirty = get_src_op(ii,src_rse,RSE_other.NDirty);
      newbsp = newval + 8*(ndirty + (((newval>>3)&0x3f) + ndirty)/63);
      set_dest_op(ii,dest_rnat,AR,~0);
      set_dest_op(ii,dest_bsp,AR,newbsp);
      set_dest_op_rse(ii,dest_rse,ndirty,0);
    }
    break;
  case IA64_ADDR_RNAT:
    newval &= ~(INT64_C(1) << 63);
    break;
  case IA64_ADDR_PFS:
    if (rf_get(newval,PFS_rv1) || rf_get(newval,PFS_rv2)) {
      report_intr(ii,IA64_intr_reservedregisterfield);
    }
    break;
  case IA64_ADDR_FPSR:
    if (rf_get(newval,FPSR_rv)) {
      report_intr(ii,IA64_intr_reservedregisterfield);
    }
    break;
  case IA64_ADDR_EC:
    newval &= 0x3f;
    break;
  } /* switch */
  
  if (M29_ar3(instr) < 8 || M29_ar3(instr) == IA64_ADDR_ITC) {
    uint64_t psr = get_src_op(ii,src_psr,SR.PSR);
    if (rf_get(psr,PSR_cpl) != 0) {
      report_intr(ii,IA64_intr_privilegedregister);
    }
  }

  set_dest_op(ii,dest_result,AR,newval);
  return;
}

void
writeback_mov_to_AR(LSE_emu_instr_info_t *ii, 
		    LSE_emu_operand_name_t opname, int fallthrough) /* I28, M29 */
{ /* I26 - Move to Application Register (I form) */
  /* I27 - Move to Application Register immediate (I form) */
  /* M29 - Move to Application Register (M form) */
  /* M30 - Move to Application Register immediate (M form) */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case dest_result:
    if (!IA64_ar_ignored[M29_ar3(instr)]) {
      set_ar(dest_result,ii,M29_ar3(instr),get_dest_op(ii,dest_result,AR));
#ifdef DEBUG_RSE
      if (realct->di->IA64_rsedebugprint) {
	  if (M29_ar3(instr)==IA64_ADDR_BSPSTORE) {
	    fprintf(stderr,
		    "RSE: BSPSTORE written with %016"PRIX64"\n",
		    get_dest_op(ii,dest_result,AR));
	  } else if (M29_ar3(instr)==IA64_ADDR_RNAT) {
	    fprintf(stderr,"RSE: RNAT written with %016"PRIX64"\n",
		    get_dest_op(ii,dest_result,AR));
	  }
      }
#endif

    }
    if (!fallthrough) break;
  case dest_bsp:
    if (M29_ar3(instr) == IA64_ADDR_BSPSTORE) {
      set_ar(dest_bsp,ii,IA64_ADDR_BSP,get_dest_op(ii,dest_bsp,AR));
#ifdef DEBUG_RSE
      if (realct->di->IA64_rsedebugprint) {
	fprintf(stderr,
		"RSE: BSPSTORE write sets BSP to %016"PRIX64"\n",
		get_dest_op(ii,dest_bsp,AR));
      }
#endif
    }
    if (!fallthrough) break;
  case dest_rnat:
    if (M29_ar3(instr) == IA64_ADDR_BSPSTORE) {
      set_ar(dest_rnat,ii,IA64_ADDR_RNAT,get_dest_op(ii,dest_rnat,AR));
#ifdef DEBUG_RSE
      if (realct->di->IA64_rsedebugprint) {
	fprintf(stderr,
		"RSE: BSPSTORE write sets RNAT to %016"PRIX64"\n",
		get_dest_op(ii,dest_rnat,AR));
      }
#endif
    }
    if (!fallthrough) break;
  case dest_rse:
    if (M29_ar3(instr) == IA64_ADDR_BSPSTORE) {
      save_rse(dest_rse,ii);
      realct->RSE_other.NDirty = get_dest_op(ii,dest_rse,RSE_other.NDirty);
      realct->RSE_other.NClean = get_dest_op(ii,dest_rse,RSE_other.NClean);
    }
    if (!fallthrough) break;
  default: break;
  }
}

void
opfetch_mov_from_AR(LSE_emu_instr_info_t *ii, 
		    LSE_emu_operand_name_t opname, int fallthrough)
{ /* I28 - Move from AR (I-unit) */
  /* M31 - Move from AR (M-unit) */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    set_src_op(ii,src_op1,AR,(IA64_ar_ignored[M31_ar3(instr)] ? 0 :
			      get_ar(realct,ii,M31_ar3(instr))));
    if (!fallthrough) break;
  case src_rsc:
    if (M31_ar3(instr) == IA64_ADDR_BSPSTORE || 
	M31_ar3(instr) == IA64_ADDR_RNAT) {
      set_src_op(ii,src_rsc,AR,get_ar(realct,ii,IA64_ADDR_RSC));
    }
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_mov_from_AR(LSE_emu_instr_info_t *ii)
{ /* I28 - Move from AR (I-unit) */
  /* M31 - Move from AR (M-unit) */
  int unit = ii->extra.unit;
  uint64_t instr = ii->extra.instr;

  if (get_src_op(ii,src_qp,PR)) {

    /* reserved register check */
    if ((unit==0 && M31_ar3(instr) < 48) ||
	(unit==1 && M31_ar3(instr) > 63 &&
	 M31_ar3(instr) < 112) ||
	IA64_ar_reserved[M31_ar3(instr)]) {
      report_intr(ii,IA64_intr_illegaloperation);
    }
    else check_target_register(ii,all_r1(instr));
    
    if ((M31_ar3(instr) == IA64_ADDR_BSPSTORE || 
	 M31_ar3(instr) == IA64_ADDR_RNAT)) {
      IA64_AR_t rsc;
      rsc = get_src_op(ii,src_rsc,AR);
      if (rf_get(rsc,RSC_mode)) {
	report_intr(ii,IA64_intr_illegaloperation);
      }
    }
    /* need to check for privileged access to ITC */
    else if (M31_ar3(instr) == IA64_ADDR_ITC) {
      uint64_t psr = get_src_op(ii,src_psr,SR.PSR);
      if (rf_get(psr,PSR_si) && rf_get(psr,PSR_cpl) != 0) {
	report_intr(ii,IA64_intr_privilegedregister);
      }
    }
  }
  set_dest_op(ii,dest_result,GR.val,get_src_op(ii,src_op1,AR));
  set_dest_op(ii,dest_result,GR.nat,0);
  return;
}

void 
opfetch_M34(LSE_emu_instr_info_t *ii, 
	    LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M34 - alloc */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_op1:
    set_src_op(ii,src_op1,AR,get_ar(realct,ii,IA64_ADDR_PFS));
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64, ((instr >> 13) & INT64_C(0x3ffff)));
    if (!fallthrough) break;
  case src_bspstore:
    set_src_op(ii,src_bspstore,AR,get_ar(realct,ii,IA64_ADDR_BSPSTORE));
    if (!fallthrough) break;
  case src_rnat:
    set_src_op(ii,src_rnat,AR,get_ar(realct,ii,IA64_ADDR_RNAT));
    if (!fallthrough) break;
  case src_rsc:
    set_src_op(ii,src_rsc,AR,get_ar(realct,ii,IA64_ADDR_RSC));
    if (!fallthrough) break;
  case src_rse:
    set_src_op_rse(ii,src_rse,
		   realct->RSE_other.NDirty,realct->RSE_other.NClean);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_M34(LSE_emu_instr_info_t *ii) 
{ /* M34 - alloc */
  uint64_t cfm;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t instr = ii->extra.instr;
  uint64_t imm64 = get_src_op(ii,src_op2,imm64) << 13;
  int NInvalid, NNeeded, ndirty, nclean, nmove;
  uint64_t bspstore, rnat;

  if (all_r1(instr) > 32+M34_sof(imm64)-1) { 
    report_actual_intr(ii,IA64_intr_illegaloperation);
  }
  if (M34_sol(imm64) > 96 || (M34_sor(imm64)<<3) > M34_sof(instr)
      || M34_sol(imm64) > M34_sof(imm64)) {
    report_actual_intr(ii,IA64_intr_illegaloperation);
  }
  cfm=get_src_op(ii,src_cfm,SR.CFM.val);
  if (M34_sor(imm64) != rf_get(cfm,CFM_sor) &&
      (rf_get(cfm,CFM_rrb_gr) != 0 || rf_get(cfm,CFM_rrb_fr) != 0 
       || rf_get(cfm,CFM_rrb_pr) != 0)) {
    report_actual_intr(ii,IA64_intr_reservedregisterfield);
  }

  /******** RSE stuff ************/
  bspstore = get_src_op(ii,src_bspstore,AR);
  rnat = get_src_op(ii,src_rnat,AR);

  ndirty = get_src_op(ii,src_rse,RSE_other.NDirty);
  nclean = get_src_op(ii,src_rse,RSE_other.NClean);
  
  /* Figure out what needs to be spilled or filled */
  NInvalid = (realct->num_stacked_phys - ndirty
	      - nclean - rf_get(cfm,CFM_sof));
  NNeeded = M34_sof(imm64) - rf_get(cfm,CFM_sof);

  /* set_dest_op(ii,dest_alatupdate,alatchange,0); */
  set_dest_op_rsechange(ii,dest_rsemem,0,0);

  if (NNeeded > NInvalid) {
    NNeeded -= NInvalid;
    if (NNeeded > nclean) {
      NNeeded -= nclean;
      nclean = 0;
      ndirty -= NNeeded;

      nmove = NNeeded + (((bspstore>>3)&0x3f)+NNeeded)/63;
      bspstore += 8*nmove;
      /* when the last thing written would be an RNAT spill, it is
       * not necessary to spill it.  Real RSEs know whether they
       * did so or not.  However, the fake RSE in the emulator
       * does NOT know because br.ret does not read bspstore as
       * it attempts to fill...  So, for safety's sake, we
       * will just write out that final RNAT so br.ret can assume
       * that it was written.
       */
      set_dest_op_rsechange(ii,dest_rsemem,NNeeded,nmove);
    }
    else nclean -= NNeeded;
  }

  set_dest_op_rse(ii,dest_rse,ndirty,nclean);

  /* ALAT notification of physical register wraparound is solely to
   * give hints for reclamation of entries
   */

  rf_set(cfm,CFM_sof,M34_sof(imm64));
  rf_set(cfm,CFM_sol,M34_sol(imm64));
  rf_set(cfm,CFM_sor,M34_sor(imm64));
  set_dest_op(ii,dest_cfm,SR.CFM.val,cfm);
  set_dest_op(ii,dest_cfm,SR.CFM.BOF,get_src_op(ii,src_cfm,SR.CFM.BOF));
  set_dest_op(ii,dest_result2,GR.val,get_src_op(ii,src_op1,AR));
  set_dest_op(ii,dest_result2,GR.nat,0);
  set_dest_op(ii,dest_bspstore,AR,bspstore);
  /* actually the starting NAT, not the final, but the final can't be done 
     at this point
   */
  set_dest_op(ii,dest_rnat,AR,rnat);

  /* NOTE: do not need to check for a bad predicate because a bad
   * predicate here has undefined behavior (p. 2-5)
   */
  return;
}

void 
writeback_M34(LSE_emu_instr_info_t *ii, 
	      LSE_emu_operand_name_t opname, int fallthrough) 
{ /* M34 - alloc */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *rp;
  uint64_t newcfm;
    
    /* not predicated */
  if (intr_cancels_instr(ii)) return;

  switch (opname) {
  case dest_rsemem:
    if (get_dest_op(ii,dest_rsemem,rsechange.numwords)) { /* do a spill */
      /* this may change ALAT and RNAT dest operands */
      LSE_emu_addr_t bspstore = get_src_op(ii,src_bspstore,AR);
      int bitnum = (bspstore >> 3) & 0x3f;
      int numtodo = get_dest_op(ii,dest_rsemem,rsechange.numwords);
      uint64_t mask = INT64_C(1) << bitnum;
      uint64_t rnat = get_dest_op(ii,dest_rnat,AR);
      uint64_t len = 8;

      int rno = (get_src_op(ii,src_cfm,SR.CFM.BOF) -
		 get_src_op(ii,src_rse,RSE_other.NDirty));
      rno = IA64_GR_FAST_ROT(rno + realct->num_stacked_phys,
			     realct->num_stacked_phys);

#ifdef DEBUG_RSE
      if (realct->di->IA64_rsedebugprint) {
	fprintf(stderr,"RSE: spilling %d registers (instr @ 0x%016"PRIX64")\n",numtodo, ii->addr);
      }
#endif
      while (numtodo) {
	if (bitnum==63) {
	  /* store here */
#ifdef DEBUG_RSE
	  if (realct->di->IA64_rsedebugprint) {
	    fprintf(stderr,
		    "RSE: spilling RNAT %016"PRIX64" to %016"PRIX64"\n", 
		    rnat,bspstore);
	  }
#endif
	  /* TODO: endianness 
	     jc1: fixed?
	   */
	  rnat = LSE_end_le2h_ll(rnat);

	  try {
	    realct->mem->write(bspstore, (unsigned char *)&rnat, len);
	  } catch (LSE_device::deverror_t) {
	    report_intr(ii,IA64_intr_memory);
	    break; /* do not fall-through on error */
	  }
	  bitnum = 0;
	  bspstore += 8;
	  mask = INT64_C(1);
	  numtodo--;
	  continue;
	}
	rnat = (rnat & ~mask) | 
	  (((uint64_t)realct->physr[rno].nat) << bitnum);
	/* store here */
#ifdef DEBUG_RSE
	if (realct->di->IA64_rsedebugprint) {
	  fprintf(stderr,
		  "RSE: spilling %d %c %016"PRIX64" to %016"PRIX64"\n", 
		  rno+48,
		  (realct->physr[rno].nat ? 'N' : '-'),
		  realct->physr[rno].val,
		  bspstore);
	}
#endif
	/* TODO: endianness 
	   jc1: fixed?
	*/
	realct->physr[rno].val = LSE_end_le2h_ll(realct->physr[rno].val);
	try {
	  realct->mem->write(bspstore,
			     (unsigned char *)&realct->physr[rno].val, len);
	} catch (LSE_device::deverror_t) {
	  report_intr(ii,IA64_intr_memory);
	  break; /* do not fall-through on error */
	}
	numtodo--;
	bspstore += 8;
	bitnum++;
	rno = IA64_GR_FAST_ROT(rno + 1,realct->num_stacked_phys);
	mask <<= 1;
      }
#ifdef DEBUG_RSE
      if (realct->di->IA64_rsedebugprint) {
	fprintf(stderr,"RSE: RNAT is now %016"PRIX64"\n",rnat);
	fprintf(stderr,"RSE: BSPSTORE now %016"PRIX64"\n",
		get_dest_op(ii,dest_bspstore,AR));
      }
#endif
      set_dest_op(ii,dest_rnat,AR,rnat);
    }
    if (!fallthrough) break;
  case dest_result2:
    /* have to write this one back specially because CFM values come
     *  from the new values, not the old ones! */
    if (all_r1(instr)!=0) {
      newcfm = get_dest_op(ii,dest_cfm,SR.CFM.val);
      rp = get_r_point(realct,all_r1(instr),
		     rf_get(newcfm,CFM_rrb_gr),
		     rf_get(newcfm,CFM_sor)<<3,
		     rf_get(get_src_op(ii,src_psr,SR.PSR),PSR_bn),
		     get_src_op(ii,src_cfm,SR.CFM.BOF));

      if (~ii->privatef.rollback_saved & (1<<dest_result2)) {
        ii->privatef.rollback_saved |= (1<<dest_result2);
        ii->privatef.rollback[dest_result2].rtype = IA64_rollback_copy;
        ii->privatef.rollback[dest_result2].data.copy.size=sizeof(IA64_GR_t);
        ii->privatef.rollback[dest_result2].data.copy.host_addr=rp;
        memcpy(ii->privatef.rollback[dest_result2].data.copy.data,
	       rp,sizeof(IA64_GR_t));
      }
      rp->val = get_dest_op(ii,dest_result2,GR.val);
      rp->nat = get_dest_op(ii,dest_result2,GR.nat);
    }
    if (!fallthrough) break;
  case dest_cfm:
    set_sr(dest_cfm,ii,IA64_ADDR_CFM,get_dest_op(ii,dest_cfm,SR));
    if (!fallthrough) break;
  case dest_bspstore:
    set_ar(dest_bspstore,ii,IA64_ADDR_BSPSTORE,
	   get_dest_op(ii,dest_bspstore,AR));
    if (!fallthrough) break;
  case dest_rnat:
    set_ar(dest_rnat,ii,IA64_ADDR_RNAT,get_dest_op(ii,dest_rnat,AR));
    if (!fallthrough) break;
  case dest_rse:
    save_rse(dest_rse,ii); 
    realct->RSE_other.NDirty = get_dest_op(ii,dest_rse,RSE_other.NDirty);
    realct->RSE_other.NClean = get_dest_op(ii,dest_rse,RSE_other.NClean);
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_M37(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* M37 - break */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t temp;
  temp = M37_imm20a(instr) | (M37_i(instr) << 20);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    set_src_op(ii,src_op1,imm64,temp);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_M37(LSE_emu_instr_info_t *ii) {
  int unit = ii->extra.unit;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  ii->branch_dir = 0;
  if (!get_src_op(ii,src_qp,PR)) return;
  ii->branch_dir = 1;
  ii->branch_num_targets = 2;
  ii->branch_targets[1] = ii->next_pc; /* jump to following */
  if (Linux_call_os(realct,ii,ii->addr, ii->next_pc, unit,
		    get_src_op(ii,src_op1,imm64))) goto wasfault;
  return;
 wasfault:
  return;
}

void opfetch_M43(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname, int fallthrough) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  /* int ireg = M42_x6(instr); */
  IA64_GR_t *ival;
  uint64_t psr;

  psr = get_src_op(ii,src_psr,SR.PSR);

  /* The to form requires privleged access */
  if (rf_get(psr,PSR_cpl)!=0) {
    report_actual_intr(ii,IA64_intr_privilegedoperation);
  }
  
  switch (opname) {
      OPFETCH_PREFIX;
  case src_qp:
      set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
      if (!fallthrough) break;
  case src_base:
      ival = get_r_ptr(realct,ii,all_r3(instr));
      set_src_op_r(ii,src_base, *ival);
      if (!fallthrough) break;
  default: break;
  }
}

#define CPUID 0x1f000704

void evaluate_M43(LSE_emu_instr_info_t *ii) {
  uint64_t instr = ii->extra.instr;
  uint64_t r3val, ireg;
  
  r3val = get_src_op(ii,src_base,GR.val);
  ireg = M42_x6(instr);
  
  if (ireg == 0x17) {
      if (r3val == 0x3) {
	  set_dest_op(ii,dest_result,GR.val, CPUID);
      } else {
	  report_actual_intr(ii,IA64_intr_notimplemented);
      }
  } else {
      report_actual_intr(ii,IA64_intr_notimplemented);
  }
}

void writeback_M43(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) {
  uint64_t instr = ii->extra.instr;
  
  if (get_src_op(ii,src_qp,PR) && !intr_cancels_instr(ii)) {
    switch (opname) {
    case dest_result:
      set_r(dest_result, ii, all_r1(instr), get_dest_op(ii, dest_result, GR.val), 0);
      if (!fallthrough) break;
    default: break;
    }
  }
}

void 
opfetch_F1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F1 - floating-point multiply-add */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f3(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    tval = get_f_ptr(realct,ii,all_f4(instr));
    set_src_op_f(ii,src_op2,*tval);
    if (!fallthrough) break;
  case src_op3:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op3,*tval);
    if (!fallthrough) break;
  case src_fpsr:
    set_src_op(ii,src_fpsr,AR,get_ar(realct,ii,IA64_ADDR_FPSR));
    if (!fallthrough) break;
  default: break;
  }
}

/* Routine for doing full-precision multiply-adds.  We'll use it once
 * for a normal muladd, and twice for parallel muladds....
 */
/* #define DC_CHECK_ADD */
/* #define DC_CHECK_MUL */

static inline int 
do_muladd(IA64_FR_t *newval,  /* will be normalized but not range-limited */
	  int *flags,
	  IA64_FR_t f2val, IA64_FR_t f3val, 
	  IA64_FR_t f4val, 
	  int doadd,
	  int optodo, /* 0 = fma, 1 = fnma, 2 = fms */
	  int rmode,  /* rounding modes as in pfsr */
	  int *round,  /* place to put the 65th significand bit */
	  int *sticky  /* or of 66th and higher bits */
	  ) {
  
  fp_internal newvalint;
  uint64_t extra3;
  *flags = 0;

  /* I think I can check all the fma/fms/fnma faults together since they
   * are so similar; I just have to watch out for the differences in
   * sign handling in each case when checking -inf + inf = NaN
   */
  if (optodo == 1) f2val.sign = f2val.sign ^ 1;  /* fms */

  /* look for NaNs, zeros, and infinities
   * Any NaN = NaN       (invalid if any snan)
   * 0*inf   = QNan      (invalid)
   * 0*finite = 0
   * finite*0 = 0
   * inf*any = inf
   * any * inf = inf
   *
   * additions:
   * 0+0 = 0 (interesting signs)
   * 0+non-zero = SRC
   * non-zero+0 = src
   * inf + -inf = QNan   (invalid)
   * inf + inf  = inf
   * inf + finite = inf
   */
  if (fp_is_nan(f4val) || fp_is_nan(f3val) || fp_is_nan(f2val)) {
    if (fp_is_nan(f4val)) *newval = f4val;
    else if (fp_is_nan(f2val)) *newval = f2val;
    else *newval = f3val;
    if (fp_is_snan(f4val) || fp_is_snan(f3val) || fp_is_snan(f2val))
      *flags |= rf_mask(FPSR_f_v);
    return 1;
  }
  if (fp_is_inf(f3val)) {
    if (fp_is_true_zero(f4val)) { /* true 0*inf = QNan */
      *newval = IA64_QNaN;
      *flags |= rf_mask(FPSR_f_v);
    } else { /* infinity is the result */
      int sign = f3val.sign ^ f4val.sign;
      if (fp_is_inf(f2val) && sign != f2val.sign) { /* inf-inf */
	*newval = IA64_QNaN;
	*flags |= rf_mask(FPSR_f_v);
      } else {
	*newval = IA64_Inf;
	newval->sign = sign;
	if (fp_is_unorm(f2val)) *flags |= rf_mask(FPSR_f_d);
      }
    }
    return 1;
  }
  if (fp_is_inf(f4val)) {
    if (fp_is_true_zero(f3val)) { /* true 0*inf = QNan */
      *newval = IA64_QNaN;
      *flags |= rf_mask(FPSR_f_v);
    } else { /* infinity is the result */
      int sign = f3val.sign ^ f4val.sign;
      if (fp_is_inf(f2val) && sign != f2val.sign) { /* inf-inf */
	*newval = IA64_QNaN;
	*flags |= rf_mask(FPSR_f_v);
      } else {
	*newval = IA64_Inf;
	newval->sign = sign;
	if (fp_is_unorm(f2val)) *flags |= rf_mask(FPSR_f_d);
      }
    }
    return 1;
  }
  /* We do not check for zeros explicitly: the multiplier/adder code 
   * handles them correctly without need of a special case....
   */
  if (fp_is_unorm(f3val)) {
    *flags |= rf_mask(FPSR_f_d);
    fp_fixweirdexp(&f3val);
    /* overnormalization is not needed here.... */
  }
  if (fp_is_unorm(f4val)) {
    *flags |= rf_mask(FPSR_f_d);
    fp_fixweirdexp(&f4val);
    /* overnormalization is not needed here.... */
  }

  newvalint.sign = f3val.sign ^ f4val.sign;  /* that's easy, at least */

  fp_fixweirdexp(&f4val);

      /* Do the multiplication */
  {
    /* result(128 bits) = (A*2^32 + B) * (C*2^32 + D)
     *         A    B
     *   x     C    D
     * ----------------
     *         BDh  BDl
     *     ADh ADl
     *     BCh BCl
     * ACh ACl
     */
    uint64_t a, b, c, d, bc, ad, bits32_63;
    a = f3val.significand >> 32; /* any extra bits are zero */
    b = f3val.significand & bits32;
    c = f4val.significand >>32;
    d = f4val.significand & bits32;
	
    /* native multiply to get 64-bit results */
    bc = b * c;
    ad = a * d;
    newvalint.significandl 
      = (f3val.significand * f4val.significand) & bits64;
    /* now figure out the middle bits to get carry out */
    bits32_63 = ((b * d)>>32) + (ad & bits32) + (bc & bits32);
    newvalint.significandh = a*c + (bc>>32) + (ad>>32) +
      (bits32_63>>32);
    newvalint.exponent = f3val.exponent + f4val.exponent - 65535 + 1;
    if (optodo == 2) newvalint.sign ^= 1;

#ifdef DC_CHECK_MUL
    fprintf(stderr,"%016llX %016llX %016llX%016llX "
	    "sc sb sa lc lb la * !=x lc lb la * =y "
	    "[%d*%d=%d\n]P\n",
	    f3val.significand, f4val.significand,
	    newvalint.significandh, newvalint.significandl,
	    f3val.exponent, f4val.exponent, newvalint.exponent);
#endif

  } /* do the multiplication.... */
      
  extra3 = 0;

  if (doadd) {
    int d;
    int carryout;
    int topsign;
    fp_internal op2;

    if (fp_is_unorm(f2val)) {
      *flags |= rf_mask(FPSR_f_d);
      fp_fixweirdexp(&f2val);
      /* overnormalization is not needed here.... */
    }

    /* We normalize the product before add; this is because unnormalized 
     * inputs can cause a seriously unnormalized result and that would
     * be a problem with the add.  Think about:
     * (e=63+bias m=0....1) * (e=63+bias m=0....1) + (e=-10+bias m=1000)=
     * (e=127+bias m=0......1) + (e=-10+bias m=1000)
     */
    /* set to 0 if it really is 0.... */
    if (!newvalint.significandh && !newvalint.significandl) {
      newvalint.exponent = 0;
    } else {
      while (!(newvalint.significandh & UINT64_C(0x8000000000000000)) 
	     && newvalint.exponent > 1) {
	newvalint.significandh <<= 1;
	newvalint.significandh |= (newvalint.significandl>>63) & bits64;
	newvalint.significandl <<= 1;
	newvalint.significandl &= bits64;
	newvalint.exponent --;
      }
    } /* else normalize */

#ifdef DC_CHECK_ADD
    { 
      int dci, dci2;
      uint64_t ts;

      /*
      fprintf(stderr,"[(%c%016llX %d)*(%c%016llX %d)%c(%016llX %d)\n]P\n",
	      (f3val.sign ^ (optodo==2)) ? '-' : '+',
	      f3val.significand, f3val.exponent,
	      f4val.sign ? '-' : '+',
	      f4val.significand, f4val.exponent,
	      f2val.sign ? '-' : '+',
	      f2val.significand, f2val.exponent);
      */

      if (f3val.sign ^ (optodo==2)) fputc('_',stderr);
      if (f3val.exponent == 0 && f3val.significand == 0) 
	fprintf(stderr,"0\n");
      else {
	dci = f3val.exponent - 65535;
	if (dci < -1) {
	  fputc('.',stderr);
	  for (dci2 = -1;dci2!=dci;dci2--) fputc('0',stderr);
	}
	ts = f3val.significand;
	if (dci == -1)  fputc('.',stderr);
	for (dci2 = 0; dci2 < 64; dci2++) {
	  fputc((ts & (UINT64_C(1)<<63))?'1':'0',stderr);
	  ts<<=1;
	  if (dci==dci2) fputc('.',stderr);
	}
	if (dci > 63) {
	  dci2--;
	  for (;dci2<dci;dci2++) fputc('0',stderr);
	}
	fputc('\n',stderr);
      }
      if (f4val.sign) fputc('_',stderr);
      if (f4val.exponent == 0 && f4val.significand == 0) 
	fprintf(stderr,"0\n");
      else {
	dci = f4val.exponent - 65535;
	if (dci < -1) {
	  fputc('.',stderr);
	  for (dci2 = -1;dci2!=dci;dci2--) fputc('0',stderr);
	}
	ts = f4val.significand;
	if (dci == -1)  fputc('.',stderr);
	for (dci2 = 0; dci2 < 64; dci2++) {
	  fputc((ts & (UINT64_C(1)<<63))?'1':'0',stderr);
	  ts<<=1;
	  if (dci==dci2) fputc('.',stderr);
	}
	if (dci > 63) {
	  dci2--;
	  for (;dci2<dci;dci2++) fputc('0',stderr);
	}
	fputc('\n',stderr);
      }
      if (f2val.sign) fputc('_',stderr);
      if (f2val.exponent == 0 && f2val.significand == 0) 
	fprintf(stderr,"0\n");
      else {
	dci = f2val.exponent - 65535;
	if (dci < -1) {
	  fputc('.',stderr);
	  for (dci2 = -1;dci2!=dci;dci2--) fputc('0',stderr);
	}
	ts = f2val.significand;
	if (dci == -1)  fputc('.',stderr);
	for (dci2 = 0; dci2 < 64; dci2++) {
	  fputc((ts & (UINT64_C(1)<<63))?'1':'0',stderr);
	  ts<<=1;
	  if (dci==dci2) fputc('.',stderr);
	}
	if (dci > 63) {
	  dci2--;
	  for (;dci2<dci;dci2++) fputc('0',stderr);
	}
	fputc('\n',stderr);
      }
    }
#endif

    /* We do not need to pre-normalize the second addend because it's at
     * most 64 bits and we're doing our addition as 128 bits and rounding
     * to 64 bits (or less).  What happens is that we do the addition
     * of significant bits with the binary point somewhere lower than
     * usual, but never more than 64 bits lower
     * (e=0 m=1foo) + (e=0  m=0...1) : (true denorms not an issue)
     * (e=0 m=1foo) + (e=1  m=0...1) : one bit of foo shifted off, but
     *                                 would have been part of anyway...
     * (e=0 m=1foo) + (e=63 m=0...1) : 63 bits of foo shifted off, but
     *                                 ditto....
     * (e=0 m=1foo) + (e=64 m=0...1) : 
     * (e=0 m=1foo) + (e=68 m=0...1) : 
     * (e=0 m=1foo) + (e=127 m=0...1) : wow, shift off almost everything,
     *                                  but only 1 bit was part of it 
     *                                  anyway...
     */

    /* Swap operands so first operand exponent >= second op exponent 
     * NOTE: signs are not swapped.... newvalint.sign remains the 
     * sign from the multiply and f2val.sign the sign from the addend
     */
    d = newvalint.exponent - f2val.exponent;
    if (d < 0) {
      d = -d;
      op2 = newvalint;
      newvalint.significandl = 0;
      newvalint.significandh = f2val.significand;
      newvalint.exponent = f2val.exponent;
      newvalint.sign = f2val.sign;
    } else {
      op2.significandl = 0;
      op2.significandh = f2val.significand;
      op2.exponent = f2val.exponent;
      op2.sign = f2val.sign;
    }

    /* align decimal points for 128-bit addition + g r s 
     * Actually, we keep 61 more bits between r and s, which seems kind
     * of silly, but actually takes advantage of host hardware better
     * than a lot of bit masking as we shift...
     */
    while (d >= 64) {
      extra3 = (extra3 ? 1 : 0) | op2.significandl;
      op2.significandl = op2.significandh;
      op2.significandh = 0;
      d -= 64;
    } /* exit condition: d < 64 */
    if (d > 0) {
      extra3 = (  ((d>1) ? (extra3 ? UINT64_C(1) : 0) : ((extra3>>1)|(extra3&1)))|
		  ((op2.significandl & ((UINT64_C(1)<<d)-1)) << (64-d))     );
      op2.significandl = (  ((op2.significandh & ((UINT64_C(1)<<d)-1)) << (64-d))|
			    (op2.significandl >> d));
      op2.significandh = (op2.significandh >> d);
    }

    /* 2's complement easiest operand, which is always the first,
     * if signs do not match.  Logic is:
     *   Let A = abs(a), B = abs(b)
     *   a+b:   if a>0, b>0, a+b = A+B
     *          if a<0, b<0, a+b = -A-B = -(A+B)
     *          if a<0, b>0, a+b = -A+B
     *          if a>0, b<0, a+b = A-B = -(-A+B)
     * There is some trickiness with zeros....
     */

    if ((newvalint.sign ^ op2.sign)) {
      if (newvalint.significandl == INT64_C(0)) {
	topsign = !(newvalint.significandh == INT64_C(0));
	newvalint.significandh = -newvalint.significandh & bits64;
      } else {
	topsign = 1;
	newvalint.significandh = (~newvalint.significandh & bits64);
	newvalint.significandl = (-newvalint.significandl) & bits64;
      }
    } else topsign=0;

    /* Add.... loads of fun.... however, we can recover the carry out
     * because for adding two unsigned numbers, a carry out means that
     * the the sum is smaller than either addend.  For the high
     * half, the previous carry can cause the sum to equal an addend
     */
    newvalint.significandl += op2.significandl;
    newvalint.significandl &= bits64;
    carryout = (newvalint.significandl < op2.significandl);
    newvalint.significandh += op2.significandh + carryout;
    newvalint.significandh &= bits64;
    carryout= ((newvalint.significandh < op2.significandh) ||
	       (carryout && (op2.significandh == newvalint.significandh)));

    /*** Two's complement result if needed, 
     *     otherwise shift right as necessary 
     *     Relevant examples:
     *
     *   -1.000   1000   +0.111   1001    +1.000   1000   -0.111   1001
     *   +0.111   0111   -1.000   1000    -0.111   0111   +1.000   1000
     *          ------          -------           ------          ------
     *           01111           10001            01111           10001
     *           -0001           -0001            +0001           +0001
     *
     *   -1.001   0111   +0.111   1001    +1.001   0111   -0.111   1001
     *   +0.111   0111   -1.001   1001    -0.111   0111   +1.001   1001
     *          ------          -------           ------          ------
     *           01110           10010            01110           10010
     *           -0010           -0010            +0010           +0001
     *
     *   -0.000   0000   +0.111   1001    +0.000   0000   -0.111   1001
     *   +0.111   0111   -0.000   0000    -0.111   0111   +0.000   0000
     *          ------          -------           ------          ------
     *           00111           01001            00111           01001
     *           +0111           +0111            -0111           -0111
     *
     *   -0.000   0000   +0.000   0000    +1.111   0001   -1.111   0001
     *   +0.000   0000   -0.000   0000    -1.111   1111   +1.111   1111
     *          ------          -------           ------          ------
     *           00000           00000            10000           10000
     *           +0000           -0000!           -0000!          +0000
     *
     * ! cases must be checked for specially....
     ***/
    if (newvalint.sign ^ op2.sign) { /* signs didn't match */
      if (carryout != topsign) { /* 2's compl. result */
	if (extra3 == 0) {
	  if (newvalint.significandl == INT64_C(0)) 
	    newvalint.significandh = -newvalint.significandh & bits64;
	  else {
	    newvalint.significandl = (-newvalint.significandl) & bits64;
	    newvalint.significandh = (~newvalint.significandh & bits64);
	  }
	} else {
	  extra3 = (-extra3) & bits64;
	  newvalint.significandl = (~newvalint.significandl & bits64);
	  newvalint.significandh = (~newvalint.significandh & bits64);
	}
      } /* if (!carryout) */

      /* Now, when signs didn't match and we ended up with zero,
       * the result is +, except when rounding is -inf, when it is -
       * When we don't end up with zero, calculate properly...
       * TODO: check IEE754 spec
       */
      if (newvalint.significandh || newvalint.significandl ||
	  extra3)
	newvalint.sign = (carryout ^ op2.sign ^ topsign);
      else newvalint.sign = (rmode==1);

    } else {

      newvalint.sign = op2.sign;
      if (carryout) { /* shift right */
	extra3 = ((extra3 & UINT64_C(1)) | (extra3>>1) | 
		  ((newvalint.significandl & UINT64_C(1))<<63));
	newvalint.significandl = ( (newvalint.significandl>>1) |
				   ((newvalint.significandh&UINT64_C(1))<<63) );
	newvalint.significandh = ( (newvalint.significandh>>1) |
				   (UINT64_C(1) << 63) );
	newvalint.exponent++;
      }
    } /* else signs didn't match */

  } /* if (doing the add) */

  /* do final overnormalize */
  if (!newvalint.significandh && !newvalint.significandl && !extra3) {
    newvalint.exponent = 0;
  } else {
    while (!(newvalint.significandh & UINT64_C(0x8000000000000000))) {
      newvalint.significandh <<= 1;
      newvalint.significandh |= (newvalint.significandl>>63);
      newvalint.significandh &= bits64;
      newvalint.significandl <<= 1;
      newvalint.significandl |= (extra3>>63);
      newvalint.significandl &= bits64;
      extra3 <<= 1;
      extra3 &= bits64;
      newvalint.exponent --;
    }
  }

#ifdef DC_CHECK_ADD
  if (doadd) { 
    int dci2, dci;
    uint64_t ts;

    if (newvalint.sign) fputc('_',stderr);
    if (newvalint.exponent == 0 && newvalint.significandh == 0 &&
	newvalint.significandl ==0 && !extra3) 
      fprintf(stderr,"0\n");
    else {
      dci = newvalint.exponent - 65535;
      if (dci < -1) {
	fputc('.',stderr);
	for (dci2 = -1;dci2!=dci;dci2--) fputc('0',stderr);
      }
      ts = newvalint.significandh;
      if (dci == -1)  fputc('.',stderr);
      for (dci2 = 0; dci2 < 64; dci2++) {
	fputc((ts & (UINT64_C(1)<<63))?'1':'0',stderr);
	ts<<=1;
	if (dci==dci2) fputc('.',stderr);
      }
      ts = newvalint.significandl;
      for (dci2 = 64; dci2 < 128; dci2++) {
	fputc((ts & (UINT64_C(1)<<63))?'1':'0',stderr);
	ts<<=1;
	if (dci==dci2) fputc('.',stderr);
      }
      ts = extra3;
      for (dci2 = 128; dci2 < 192; dci2++) {
	fputc((ts & (UINT64_C(1)<<63))?'1':'0',stderr);
	ts<<=1;
	if (dci==dci2) fputc('.',stderr);
      }
      if (dci > 192) {
	dci2--;
	for (;dci2<dci;dci2++) fputc('0',stderr);
      }
      fputc('\n',stderr);
    }
    fprintf(stderr,
	    "sd sc sb sa ld lc lb la * + !=x ld lc lb la * + =y ");
    /*
	    "[%d*%d=%d\n]P\n",
	    f3val.exponent, f4val.exponent, newvalint.exponent);
    */
  }
#endif

  newval->significand = newvalint.significandh;
  newval->sign = newvalint.sign;
  newval->exponent = newvalint.exponent;
  /* make all the bits beyond 64 simply round and sticky */
  *round = (newvalint.significandl>>63)&1;
  *sticky = extra3 || (newvalint.significandl & bits63);

  return 0;
}

void 
evaluate_F1(LSE_emu_instr_info_t *ii)
{ /* F1 - floating-point multiply-add */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t f2val, f3val, f4val, newval;
  IA64_AR_t fpsr;

  psr = get_src_op(ii,src_psr,SR.PSR);
  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f3(instr));
  fp_check_disabled_register(realct,ii,psr,all_f4(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  get_src_op_f(ii,src_op1,f3val);
  get_src_op_f(ii,src_op2,f4val);
  get_src_op_f(ii,src_op3,f2val);
  fpsr = get_src_op(ii,src_fpsr,AR);
  fpsr &= ~IA64_FPSR_FMASK; /* remove flag bits */

  if (fp_is_natval(f2val) || fp_is_natval(f4val) || fp_is_natval(f3val)) {
    set_dest_op_f(ii,dest_result, IA64_NatVAL);
  } else { /* do real work */
    int precis, optodo, flags=0, controls, traps;

    controls = (fpsr >> (6+13*F1_sf(instr))) & 0x7f;
    traps = rf_get(fpsr,FPSR_traps) | (rf_get(controls,FPSR_td)?~0:0);

    precis = ((all_majorop(instr) & 1) << 1) | F1_x(instr);
    optodo = (all_majorop(instr) >> 1) & 3;

    if (precis==3) { /* parallel opcodes */
      IA64_FR_t f3left, f4left, f2left, f3right, f4right, f2right;
      IA64_FR_t savevalleft, savevalright, newvalleft, newvalright;
      int inexactleft, roundleft, stickyleft, fpaleft, finishedleft;
      int inexactright, roundright, stickyright, fparight, finishedright;
      int flagsleft, flagsright;
      IA64_intr_t intrleft, intrright;
      uint64_t vleft, vright;

      fp_mem2reg_s(f3val.significand & bits32, &f3right);
      fp_mem2reg_s((f3val.significand>>32) & bits32, &f3left);
      fp_mem2reg_s(f4val.significand & bits32, &f4right);
      fp_mem2reg_s((f4val.significand>>32) & bits32, &f4left);

      if (all_f2(instr)) {
	fp_mem2reg_s(f2val.significand & bits32, &f2right);
	fp_mem2reg_s((f2val.significand>>32) & bits32, &f2left);
      }

      finishedleft = do_muladd(&savevalleft, &flagsleft, f2left,
			       f3left, f4left, all_f2(instr)!=0,
			       optodo, rf_get(controls,FPSR_rc), &roundleft,
			       &stickyleft);

      finishedright = do_muladd(&savevalright, &flagsright, f2right,
			       f3right, f4right, all_f2(instr)!=0,
			       optodo, rf_get(controls,FPSR_rc), &roundright,
			       &stickyright);

      flags = flagsleft | flagsright;

      if (flags & ~traps) {
	/* only one of [dv] possible for this op, so fault */
	/* real IEEE fp fault (d or v) */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
	/* denorm input going to SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else {
	rf_set(controls,FPSR_pc,0); /* set precision */
	rf_set(controls,FPSR_wre,0); /* set range mode to 8-bit */
	if (!finishedleft) {
	  inexactleft = fp_round(&newvalleft,&savevalleft,&fpaleft,
				 0,rf_get(controls,FPSR_rc),
				 roundleft, stickyleft);
	  intrleft = fp_find_trap(&newvalleft,&savevalleft,&flagsleft,&fpaleft,
				  roundleft,stickyleft,
				  controls, traps, inexactleft);
	  if (intrleft) report_intr(ii,intrleft);
	} else {
	  newvalleft = savevalleft;
	}
	if (!finishedright) {
	  inexactright = fp_round(&newvalright,&savevalright,&fparight,
				 0,rf_get(controls,FPSR_rc),
				 roundright, stickyright);
	  intrright = fp_find_trap(&newvalright,&savevalright,&flagsright,
				   &fparight,
				   roundright,stickyright,
				   controls, traps, inexactright);
	  if (intrright) report_intr(ii,intrright);
	} else {
	  newvalright = savevalright;
	}
      }

      fp_reg2mem_s(&newvalleft, &vleft);
      fp_reg2mem_s(&newvalright, &vright);

      newval.exponent = 0x1003e;
      newval.sign = 0;
      newval.significand = ((vleft & bits32) << 32) | (vright & bits32);

    } else {  /* normal form */

      IA64_FR_t saveval;
      int inexact, round, sticky, fpa, finished;
      IA64_intr_t intr;

      switch (precis) {
      case 0: precis = rf_get(controls, FPSR_pc); break;
      case 1: precis = 0; break;
      case 2: precis = 2; break;
      }

      finished = do_muladd(&saveval, &flags, f2val, f3val, f4val, 
			all_f2(instr)!=0, 
			optodo, rf_get(controls,FPSR_rc), &round, &sticky);

      if (flags & ~traps) {
	/* only one of [dv] possible for this op, so fault */
	/* real IEEE fp fault (d or v) */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
	/* denorm input going to SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (!finished) {
	inexact = fp_round(&newval,&saveval,&fpa,
			   precis,rf_get(controls,FPSR_rc),
			   round, sticky);
	rf_set(controls,FPSR_pc,precis); /* set precision */
	intr = fp_find_trap(&newval,&saveval,&flags,&fpa,
			    round,sticky,
			    controls, traps, inexact);
	if (intr) report_intr(ii,intr);
      } else {
	newval = saveval;
      }
    } /* else not parallel */

    fpsr |= flags << (13 * F1_sf(instr)+6+7);

    set_dest_op_f(ii,dest_result,newval);
  }
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  set_dest_op(ii,dest_fpsr,AR,fpsr);
  return;
}

void 
opfetch_F2(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F2 - fixed-point multiply-add */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f3(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    tval = get_f_ptr(realct,ii,all_f4(instr));
    set_src_op_f(ii,src_op2,*tval);
    if (!fallthrough) break;
  case src_op3:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op3,*tval);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_F2(LSE_emu_instr_info_t *ii)
{ /* F2 - fixed-point multiply-add */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t f2val,f3val,f4val;

  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f3(instr));
  fp_check_disabled_register(realct,ii,psr,all_f4(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  get_src_op_f(ii,src_op1,f3val);
  get_src_op_f(ii,src_op2,f4val);
  get_src_op_f(ii,src_op3,f2val);
      
  if (fp_is_natval(f2val) || fp_is_natval(f4val) || fp_is_natval(f3val)) {
    set_dest_op_f(ii,dest_result,IA64_NatVAL);
  } else { /* do real work */
    IA64_FR_t newval;
    newval.exponent = 0x1003e;
    newval.sign = 0;
    /* NOTE: instruction is defined as ignoring NaNs and all that jazz..*/
    switch (F2_x2(instr) & 3) {
    case 0 : /* .l */
      newval.significand =
	f3val.significand * f4val.significand;
      newval.significand += f2val.significand;
      break;
    case 2 : /* .hu */
      { /* do as (a*2^32 + b)(c*2^32 + d) + (e*2^32 + f) */
	uint64_t a, b, c, d, e, f, bc, ad, bits0_31, bits32_63;
	a = f3val.significand >>32;
	b = f3val.significand & INT64_C(0x0ffffffff);
	c = f4val.significand >>32;
	d = f4val.significand & INT64_C(0x0ffffffff);
	e = f2val.significand >> 32;
	f = f2val.significand & INT64_C(0x0ffffffff);
	bc = b * c;
	ad = a * d;
	bits0_31 = f + b * d; /* lower 32 bits and carry out are good */
	/* mask is so we just get next 32bits + carry */
	bits32_63 = (bits0_31>>32)+ e + (ad&INT64_C(0x0ffffffff)) + 
	  (bc&INT64_C(0x0ffffffff));
	newval.significand = a*c + (bc>>32) + (ad>>32) +
	  (bits32_63>>32);
      }
      break;
    case 3 : /* .h */
      { /* I'm sure there's a clever way to do this, but I'm not all
	 * that clever and this I can reason through...
	 */
	uint64_t a, b, c, d, e, f, bc, ad, bits0_31, bits32_63;
	int s=0;
	if ((int64_t)f3val.significand < 0) {
	  a = (-(int64_t)f3val.significand) >>32;
	  b = (-(int64_t)f3val.significand) & INT64_C(0xffffffff);
	  s = 1;
	} else {
	  a = f3val.significand >>32;
	  b = f3val.significand & INT64_C(0xffffffff);
	}
	if ((int64_t)f4val.significand < 0) {
	  c = (-(int64_t)f4val.significand) >>32;
	  d = (-(int64_t)f4val.significand) & INT64_C(0xffffffff);
	  s = 1-s;
	} else {
	  c = f4val.significand >>32;
	  d = f4val.significand & INT64_C(0xffffffff);
	}
	/* note that we do not sign-extend f2 */
	e = f2val.significand >> 32;
	f = f2val.significand & INT64_C(0x0ffffffff);
	bc = b * c;
	ad = a * d;
	/* First we'll multiply */
	/* lower 32 bits and carry out are good */
	bits0_31 = b * d;
	/* mask is so we just get next 32bits + carry */
	bits32_63 = 
	  (bits0_31>>32) + (ad&INT64_C(0x0ffffffff)) + (bc&INT64_C(0x0ffffffff));
	newval.significand = 
	  a*c + (bc>>32) + (ad>>32) + (bits32_63>>32);
	/* Now, negate and add f3 */
	bits0_31 = s ?
	  (~bits0_31 & INT64_C(0x0ffffffff)) + 1 + f :
	  (bits0_31 & INT64_C(0x0ffffffff)) + f ;
	bits32_63 = s ?
	  (bits0_31>>32) + e + (~bits32_63 & INT64_C(0x0ffffffff)) :
	  (bits0_31>>32) + e + (bits32_63 & INT64_C(0x0ffffffff));
	newval.significand = s ?
	  (~newval.significand) +(bits32_63>>32):
	  (newval.significand) + (bits32_63>>32);
      }
    }
    set_dest_op_f(ii,dest_result,newval);
  }
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  return;
}

void 
opfetch_F3(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F3 - floating-point select */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f3(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    tval = get_f_ptr(realct,ii,all_f4(instr));
    set_src_op_f(ii,src_op2,*tval);
    if (!fallthrough) break;
  case src_op3:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op3,*tval);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_F3(LSE_emu_instr_info_t *ii)
{ /* F3 - floating-point select */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t f2val,f3val,f4val;

  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f3(instr));
  fp_check_disabled_register(realct,ii,psr,all_f4(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  get_src_op_f(ii,src_op1,f3val);
  get_src_op_f(ii,src_op2,f4val);
  get_src_op_f(ii,src_op3,f2val);
      
  if (fp_is_natval(f2val) || fp_is_natval(f4val) || fp_is_natval(f3val)) {
    set_dest_op_f(ii,dest_result,IA64_NatVAL);
  } else { /* do real work */
    IA64_FR_t newval;
    newval.exponent = 0x1003e;
    newval.sign = 0;
    newval.significand = ( (f3val.significand & f2val.significand) |
			   (f4val.significand & ~f2val.significand) );
    set_dest_op_f(ii,dest_result,newval);
  }
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  return;
}

void 
opfetch_F4(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F4 - floating-point compare */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    tval = get_f_ptr(realct,ii,all_f3(instr));
    set_src_op_f(ii,src_op2,*tval);
    if (!fallthrough) break;
  case src_fpsr:
    set_src_op(ii,src_fpsr,AR,get_ar(realct,ii,IA64_ADDR_FPSR));
    if (!fallthrough) break;
  default: break;
  }
}

static int 
do_fpcompare(int *flags, IA64_FR_t f2val, IA64_FR_t f3val, int comp,
	     int nanval) 
{
  int cond;

  if (fp_is_nan(f2val) || fp_is_nan(f3val)) { 
    /* NaN's are unordered and cause invalid exceptions when they
     * are signalling NaN's or we were not doing unordered/equal checks
     */
    if (fp_is_snan(f2val) || fp_is_snan(f3val) || (comp!=3 && comp!=0)) {
      *flags |= rf_mask(FPSR_f_v);
    }
    /* condition is true if unordered was wanted, otherwise it depends upon
     * what we are to do with nans (for proper max/min)
     */
    cond = (comp==3) ? 1 : nanval;
  } else if (fp_is_zero(f2val) && fp_is_zero(f3val)) {
    /* Zeroes are a special case.... */
    cond = (comp == 0 || comp==2); /* condition is true if equal or 
				      >= was wanted */
    if (fp_is_unorm(f2val) || fp_is_unorm(f3val)) {
      *flags |= rf_mask(FPSR_f_d);
    }
  } else if (fp_is_inf(f2val) && fp_is_inf(f3val)) {
    /* infinity vs. infinity is special; infinity vs. finite is handled
     * properly by the finite comparison code...
     */
    cond = (((comp==0 || comp==2) && f2val.sign == f3val.sign) ||
	    ((comp==1 || comp==2) && f2val.sign && !f3val.sign));
  } else {
    if (fp_is_unorm(f2val)) {
      *flags |= rf_mask(FPSR_f_d);
      fp_fixweirdexp(&f2val);
      fp_overnormalize(&f2val);
    }
    if (fp_is_unorm(f3val)) {
      *flags |= rf_mask(FPSR_f_d);
      fp_fixweirdexp(&f3val);
      fp_overnormalize(&f3val);
    }
    
    switch (comp) {
    case 0: /* .eq */
      cond = (f2val.sign == f3val.sign && f2val.exponent==f3val.exponent &&
	      f2val.significand == f3val.significand);
      break;
    case 1: /* .lt */
      /* cases:
	 f2 < 0, f3 > 0:  yes
	 f2 < 0, f3 < 0: abs(f2) > abs(f3)
	 f2 > 0, f3 < 0:  no
	 f2 > 0, f3 > 0: abs(f2) < abs(f3)
      */
      cond = (f2val.sign &&  
	      (!f3val.sign ||
	       (f2val.exponent > f3val.exponent ||
		((f2val.exponent == f3val.exponent) &&
		 (f2val.significand > f3val.significand))))) ||
	(!f2val.sign && !f3val.sign &&
	 (f2val.exponent < f3val.exponent ||
	  ((f2val.exponent == f3val.exponent) && 
	   (f2val.significand < f3val.significand))));
      break;
    case 2: /* .le */
      cond = (f2val.sign && 
	      (!f3val.sign || 
	       (f2val.exponent > f3val.exponent ||
		((f2val.exponent == f3val.exponent) &&
		 (f2val.significand >= f3val.significand))))) ||
	(!f2val.sign && !f3val.sign &&
	 (f2val.exponent < f3val.exponent ||
	  ((f2val.exponent == f3val.exponent) && 
	   (f2val.significand <= f3val.significand))));
      break;
    case 3: /* .unord */
    default:
      cond = 0;
      break;
    }
  }
  return cond;
}

void 
evaluate_F4(LSE_emu_instr_info_t *ii)
{ /* F4 - floating-point compare */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_AR_t fpsr;
  fpsr = get_src_op(ii,src_fpsr,AR);
  fpsr &= ~IA64_FPSR_FMASK; /* remove flag bits */

  set_int_op(ii,int_updatepred,PR,0);
  if (!get_src_op(ii,src_qp,PR)) {
    if (F4_ta(instr)) {
      if (all_p1(instr) == all_p2(instr)) 
	report_actual_intr(ii,IA64_intr_illegaloperation);
      set_dest_op(ii,dest_istrue,PR,0);
      set_dest_op(ii,dest_isfalse,PR,0);
      set_int_op(ii,int_updatepred,PR,1);
      set_dest_op(ii,dest_fpsr,AR,fpsr);
      return;
    }
    if (all_p1(instr) == all_p2(instr)) 
      report_potential_intr(ii,IA64_intr_illegaloperation);
  } else if (all_p1(instr) == all_p2(instr)) 
    report_actual_intr(ii,IA64_intr_illegaloperation);

  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_disabled_register(realct,ii,psr,all_f2(instr));
  fp_check_disabled_register(realct,ii,psr,all_f3(instr));

  set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));

  if (!get_src_op(ii,src_qp,PR) && F4_ta(instr)) {
    set_dest_op(ii,dest_istrue,PR,0);
    set_dest_op(ii,dest_isfalse,PR,0);
  } else {
    int cond, comp;
    IA64_FR_t f2val, f3val;

    comp = (F4_ra(instr) << 1) | F4_rb(instr);

    get_src_op_f(ii,src_op1,f2val);
    get_src_op_f(ii,src_op2,f3val);

    if (fp_is_natval(f2val) || fp_is_natval(f3val)) {
      set_dest_op(ii,dest_istrue,PR,0);
      set_dest_op(ii,dest_isfalse,PR,0);
    } else {

      int flags=0, traps, controls;

      controls = (fpsr >> (6+13*F4_sf(instr))) & 0x7f;
      traps = rf_get(fpsr,FPSR_traps) | (rf_get(controls,FPSR_td)?~0:0);

      cond = do_fpcompare(&flags, f2val, f3val, comp, 0);

      if (flags & ~traps) {
	/* only one of [dv] possible for this op, so fault */
	/* real IEEE fp fault (d or v) */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
	/* denorm input going to SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      } 
      set_dest_op(ii,dest_istrue,PR,cond);
      set_dest_op(ii,dest_isfalse,PR,!cond);

      fpsr |= flags << (13 * F4_sf(instr)+6+7);

    }
  }
  set_dest_op(ii,dest_fpsr,AR,fpsr);
  return;
}

void 
opfetch_F5(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F5 - floating-point class */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,(F5_fclass7c(instr) << 2) | F5_fc2(instr));
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_F5(LSE_emu_instr_info_t *ii)
{ /* F5 - floating-point class */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  int cond, class9, nat;
  IA64_FR_t f2val;

  set_int_op(ii,int_updatepred,PR,0);
  if (!get_src_op(ii,src_qp,PR)) {
    if (F5_ta(instr)) {
      if (all_p1(instr) == all_p2(instr)) 
	report_actual_intr(ii,IA64_intr_illegaloperation);
      set_dest_op(ii,dest_istrue,PR,0);
      set_dest_op(ii,dest_isfalse,PR,0);
      set_int_op(ii,int_updatepred,PR,1);
      return;
    }
    if (all_p1(instr) == all_p2(instr)) 
      report_potential_intr(ii,IA64_intr_illegaloperation);
  } else if (all_p1(instr) == all_p2(instr)) 
    report_actual_intr(ii,IA64_intr_illegaloperation);

  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  set_int_op(ii,int_updatepred,PR,get_src_op(ii,src_qp,PR));

  class9 = get_src_op(ii,src_op2,imm64);
  
  get_src_op_f(ii,src_op1,f2val);
  
  cond = ( ( ((class9 & 1) && !f2val.sign) ||
	     ((class9 & 2) && f2val.sign)      )
	   && ( ( (class9 & 4) && fp_is_true_zero(f2val)) ||
		( (class9 & 8) && fp_is_unorm(f2val)) ||
		( (class9 & 16) && fp_is_normal(f2val)) ||
		( (class9 & 32) && fp_is_inf(f2val))
		  )
	   )
    ||  ((class9 & 64) && fp_is_snan(f2val)) 
    ||  ((class9 & 128) && fp_is_qnan(f2val))
    ||  ((class9 & 256) && fp_is_natval(f2val));
  
  nat = fp_is_natval(f2val) && !(class9 & 256);
  
  if (nat) {
    set_dest_op(ii,dest_istrue,PR,0);
    set_dest_op(ii,dest_isfalse,PR,0);
  } else {
    set_dest_op(ii,dest_istrue,PR,cond);
    set_dest_op(ii,dest_isfalse,PR,!cond);
  }
  return;
}

void 
opfetch_F6(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F6 - floating-point reciprocal approximation */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    tval = get_f_ptr(realct,ii,all_f3(instr));
    set_src_op_f(ii,src_op2,*tval);
    if (!fallthrough) break;
  case src_fpsr:
    set_src_op(ii,src_fpsr,AR,get_ar(realct,ii,IA64_ADDR_FPSR));
    if (!fallthrough) break;
  default: break;
  }
}

static uint64_t reciptable[256] = {
  0x3fc, 0x3f4, 0x3ec, 0x3e4, 0x3dd, 0x3d5, 0x3cd, 0x3c6,
  0x3be, 0x3b7, 0x3af, 0x3a8, 0x3a1, 0x399, 0x392, 0x38b,
  0x384, 0x37d, 0x376, 0x36f, 0x368, 0x361, 0x35b, 0x354,
  0x34d, 0x346, 0x340, 0x339, 0x333, 0x32c, 0x326, 0x320,
  0x319, 0x313, 0x30d, 0x307, 0x300, 0x2fa, 0x2f4, 0x2ee,
  0x2e8, 0x2e2, 0x2dc, 0x2d7, 0x2d1, 0x2cb, 0x2c5, 0x2bf,
  0x2ba, 0x2b4, 0x2af, 0x2a9, 0x2a3, 0x29e, 0x299, 0x293,
  0x28e, 0x288, 0x283, 0x27e, 0x279, 0x273, 0x26e, 0x269,
  0x264, 0x25f, 0x25a, 0x255, 0x250, 0x24b, 0x246, 0x241,
  0x23c, 0x237, 0x232, 0x22e, 0x229, 0x224, 0x21f, 0x21b,
  0x216, 0x211, 0x20d, 0x208, 0x204, 0x1ff, 0x1fb, 0x1f6,
  0x1f2, 0x1ed, 0x1e9, 0x1e5, 0x1e0, 0x1dc, 0x1d8, 0x1d4,
  0x1cf, 0x1cb, 0x1c7, 0x1c3, 0x1bf, 0x1bb, 0x1b6, 0x1b2,
  0x1ae, 0x1aa, 0x1a6, 0x1a2, 0x19e, 0x19a, 0x197, 0x193,
  0x18f, 0x18b, 0x187, 0x183, 0x17f, 0x17c, 0x178, 0x174,
  0x171, 0x16d, 0x169, 0x166, 0x162, 0x15e, 0x15b, 0x157,
  0x154, 0x150, 0x14d, 0x149, 0x146, 0x142, 0x13f, 0x13b,
  0x138, 0x134, 0x131, 0x12e, 0x12a, 0x127, 0x124, 0x120,
  0x11d, 0x11a, 0x117, 0x113, 0x110, 0x10d, 0x10a, 0x107,
  0x103, 0x100, 0x0fd, 0x0fa, 0x0f7, 0x0f4, 0x0f1, 0x0ee,
  0x0eb, 0x0e8, 0x0e5, 0x0e2, 0x0df, 0x0dc, 0x0d9, 0x0d6,
  0x0d3, 0x0d0, 0x0cd, 0x0ca, 0x0c8, 0x0c5, 0x0c2, 0x0bf,
  0x0bc, 0x0b9, 0x0b7, 0x0b4, 0x0b1, 0x0ae, 0x0ac, 0x0a9,
  0x0a6, 0x0a4, 0x0a1, 0x09e, 0x09c, 0x099, 0x096, 0x094,
  0x091, 0x08e, 0x08c, 0x089, 0x087, 0x084, 0x082, 0x07f,
  0x07c, 0x07a, 0x077, 0x075, 0x073, 0x070, 0x06e, 0x06b,
  0x069, 0x066, 0x064, 0x061, 0x05f, 0x05d, 0x05a, 0x058,
  0x056, 0x053, 0x051, 0x04f, 0x04c, 0x04a, 0x048, 0x045,
  0x043, 0x041, 0x03f, 0x03c, 0x03a, 0x038, 0x036, 0x033,
  0x031, 0x02f, 0x02d, 0x02b, 0x029, 0x026, 0x024, 0x022,
  0x020, 0x01e, 0x01c, 0x01a, 0x018, 0x015, 0x013, 0x011,
  0x00f, 0x00d, 0x00b, 0x009, 0x007, 0x005, 0x003, 0x001,
};

/* returns new flag value, < 0 for SWA */
static inline int
do_rcpa(IA64_FR_t *newval, char *newp, IA64_FR_t *numer, IA64_FR_t *denom,
	int isparallel)
{
  int flags = 0;

  /* look for NaNs, zeros, and infinities
   * Any NaN = NaN        (invalid if any snan)
   * 0/0 = QNaN           (invalid)
   * inf/0 = inf          (NOTE: no divbyzero)
   * finite/0 = inf       (divbyzero)
   * inf/inf=QNaN         (invalid)
   * inf/finite = inf
   * finite/inf = 0 
   * 0/non-zero = 0
   */
  if (fp_is_nan(*numer)) { 
    *newval = *numer;
    *newp = 0;
    if (fp_is_snan(*numer) || fp_is_snan(*denom)) flags |= rf_mask(FPSR_f_v);
  } else if (fp_is_nan(*denom)) {
    *newval = *denom;
    *newp = 0;
    if (fp_is_snan(*denom)) flags |= rf_mask(FPSR_f_v);
  } else if (fp_is_zero(*denom)) {
    if (fp_is_zero(*numer)) { /* 0/0 */
      *newval = IA64_QNaN;
      *newp = 0;
      flags |= rf_mask(FPSR_f_v);
      if (denom->exponent || numer->exponent)
	flags |= rf_mask(FPSR_f_d); /* pseudo-zero, so really an unnorm */
    } else if (fp_is_inf(*numer)) { /* inf/0 */
      *newval = IA64_Inf;
      newval->sign = denom->sign ^ numer->sign;
      *newp = 0;
      if (denom->exponent)
	flags |= rf_mask(FPSR_f_d); /* pseudo-zero, so really an unnorm */
    } else { /* finite/0 */
      *newval = IA64_Inf;
      newval->sign = denom->sign ^ numer->sign;
      *newp = 0;
      flags |= rf_mask(FPSR_f_z);
      if (denom->exponent)
	flags |= rf_mask(FPSR_f_d); /* pseudo-zero, so really an unnorm */
    }
  } else if (fp_is_inf(*denom)) {
    if (fp_is_inf(*numer)) { /* inf/inf */
      *newval = IA64_QNaN;
      *newp = 0;
      flags |= rf_mask(FPSR_f_v);
    } else { /* finite/inf */
      *newval = IA64_FPZero;
      newval->sign = denom->sign ^ numer->sign;
      *newp = 0;
    }
  } else if (fp_is_inf(*numer)) { /* inf/fin */
    *newval = IA64_Inf;
    newval->sign = denom->sign ^ numer->sign;
    *newp = 0;
  } else if (fp_is_zero(*numer)) { /* 0/non-zero */
    *newval = IA64_FPZero;
    newval->sign = denom->sign ^ numer->sign;
    *newp = 0;
    if (numer->exponent)
      flags |= rf_mask(FPSR_f_d); /* pseudo-zero, so it is really an unnorm */
  } else {
    int exponent;

    if (fp_is_unorm(*denom)) {
      flags |= rf_mask(FPSR_f_d);
      fp_fixweirdexp(denom);
      fp_overnormalize(denom); /* allow negative exponents... */
    }
    exponent = denom->exponent;

    /* This is a range check, but I do not know whether it is the right
     * range check....  I'm going to figure that the limit is half the
     * normal....
     */
    if (isparallel && 
	(denom->exponent > 65535+63 || denom->exponent < 65535-64 ||
	 numer->exponent > 65535+63 || numer->exponent < 65535-64)) {
	*newval = IA64_FPZero;
      *newp = 0;
      return -1;
    }

    if (denom->exponent > 65535+16383 || denom->exponent < 65535-16382 ||
	numer->exponent > 65535+16383 || numer->exponent < 65535-16382) {
      
      if (IA64_swa_on_recip_range) {
	*newval = IA64_FPZero;
	*newp=0;
	return -1;
      }

      /* Create approximation of mantissa by using host division (which is
       * accurate to double and then throwing away some bits; can't
       * just cast to float because of range issues....
       *
       */
      denom->exponent = 65535;
      fp_host2reg_good(newval,(1.0 / fp_reg2host(denom)));
      /* set the true exponent, but must correct it if the approximation 
       * does not have the exponent we expected (which would be 65534)
       */
      newval->exponent = 0x1ffff-2-exponent+(newval->exponent-65534);

      /* maximum exponent was 0x1fffe: this can give a -1 exponent: a denormal
       * result.  minimum exponent (after overnormalization) is -63.  That 
       * translates to 0x2003c: an overflow.  For parallel, a similar problem
       * can occur w.r.t the smaller range.  Thus, the caller must check for
       * overflow/underflow.  Note that there normally is not any because SWA
       * gets called before.
       */
      *newp = 1;
      /* give only 24 bits good */
      newval->significand &= UINT64_C(0xFfffFf0000000000); 
      newval->sign = denom->sign;
    } else { /* look up from table */
      newval->exponent = 0x1ffff-2-denom->exponent;
      newval->sign = denom->sign;
      newval->significand =( (UINT64_C(1) << 63) |
			     (reciptable[(denom->significand>>55)&0xff]<<53));
      *newp = 1;
    }
  }
  return flags;
}

void 
evaluate_F6(LSE_emu_instr_info_t *ii)
{ /* F6 - floating-point reciprocal approximation */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t f2val, f3val, newval;
  IA64_AR_t fpsr;
  char newp;

  psr = get_src_op(ii,src_psr,SR.PSR);
  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f3(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  if (!get_src_op(ii,src_qp,PR)) set_dest_op(ii,dest_isfalse,PR,0);

  get_src_op_f(ii,src_op2,f3val);
  get_src_op_f(ii,src_op1,f2val);
  fpsr = get_src_op(ii,src_fpsr,AR);
  fpsr &= ~IA64_FPSR_FMASK; /* remove flag bits */
  
  if (fp_is_natval(f2val) || fp_is_natval(f3val)) {
    set_dest_op_f(ii,dest_result,IA64_NatVAL);
    set_dest_op(ii,dest_isfalse,PR,0);
  } else { /* do real work */
    int flags=0, controls, traps;

    controls = (fpsr >> (6+13*F6_sf(instr))) & 0x7f;
    traps = rf_get(fpsr,FPSR_traps) | (rf_get(controls,FPSR_td)?~0:0);

    if (all_majorop(instr)) { /* parallel */

      IA64_FR_t leftnum, rightnum, leftden, rightden, newleft, newright;
      uint64_t vleft, vright;
      int flagsleft, flagsright;
      char pleft, pright;

      fp_mem2reg_s(f2val.significand & bits32, &rightnum);
      fp_mem2reg_s((f2val.significand>>32) & bits32, &leftnum); 
      fp_mem2reg_s(f3val.significand & bits32, &rightden);
      fp_mem2reg_s((f3val.significand>>32) & bits32, &leftden); 

      flagsleft = do_rcpa(&newleft, &pleft, &leftnum, &leftden,1);
      flagsright = do_rcpa(&newright, &pright, &rightnum, &rightden,1);
      flags = flagsleft | flagsright;

      if (IA64_swa_on_recip_range && flags < 0) { /* do_rcpa wanted an SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (flags & ~traps) { 
	/* only one of [dvz] possible for this op, so fault */
	/* real IEEE fp fault (d or v or z) */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
	/* denorm input going to SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (!IA64_swa_on_recip_range && (pleft || pright)) { 
	/* not needed if SWA possible */
	IA64_intr_t intrleft=IA64_intr_none, intrright=IA64_intr_none;
	int fpa=0;
	rf_set(controls,FPSR_pc,0); /* set precision mode to 24-bit */
	rf_set(controls,FPSR_wre,0); /* set range mode to 8-bit */
	if (pleft)
	  intrleft=fp_find_trap(&newleft,&newleft,&flags,&fpa,0,0,
				controls,traps,0);
	fpa = 0;
	if (pright)
	  intrright=fp_find_trap(&newright,&newright,&flags,&fpa,0,0,
				 controls,traps,0);
	if (intrleft || intrright) 
	  report_intr(ii,intrleft<intrright?intrleft:intrright);
      }

      fp_reg2mem_s(&newleft, &vleft);
      fp_reg2mem_s(&newright, &vright);

      newp = pleft && pright;
      newval.exponent = 0x1003e;
      newval.sign = 0;
      newval.significand = ((vleft & bits32) << 32) | (vright & bits32);

    } else { /* not parallel */

      flags=do_rcpa(&newval,&newp,&f2val,&f3val,0);

      if (IA64_swa_on_recip_range && flags < 0) { /* do_rcpa wanted an SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (flags & ~traps) { 
	/* only one of [dvz] possible for this op, so fault */
	/* real IEEE fp fault (d or v or z) */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
	/* denorm input going to SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (!IA64_swa_on_recip_range && newp) { 
	/* not needed if SWA possible */
	IA64_intr_t intr;
	int fpa=0;
	rf_set(controls,FPSR_pc,3); /* set precision to 64-bit */
	rf_set(controls,FPSR_wre,1); /* set range mode to 17-bit */
	intr=fp_find_trap(&newval,&newval,&flags,&fpa,0,0,
			  controls,traps,0);
	if (intr) report_intr(ii,intr);
      }
    } /* else parallel form */

    fpsr |= flags << (13 * F6_sf(instr)+6+7);

    set_dest_op_f(ii,dest_result,newval);
    set_dest_op(ii,dest_isfalse,PR,newp);
  }
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  set_dest_op(ii,dest_fpsr,AR,fpsr);
  /* make certain we get the destination predicate right */
  if (!get_src_op(ii,src_qp,PR)) set_dest_op(ii,dest_isfalse,PR,0);
  return;
}

void 
opfetch_F7(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F7 - floating-point reciprocal sqrt approximation */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f3(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_fpsr:
    set_src_op(ii,src_fpsr,AR,get_ar(realct,ii,IA64_ADDR_FPSR));
    if (!fallthrough) break;
  default: break;
  }
}

static uint64_t sqrttable[256] = {
  0x1a5, 0x1a0, 0x19a, 0x195, 0x18f, 0x18a, 0x185, 0x180,
  0x17a, 0x175, 0x170, 0x16b, 0x166, 0x161, 0x15d, 0x158,
  0x153, 0x14e, 0x14a, 0x145, 0x140, 0x13c, 0x138, 0x133,
  0x12f, 0x12a, 0x126, 0x122, 0x11e, 0x11a, 0x115, 0x111,
  0x10d, 0x109, 0x105, 0x101, 0x0fd, 0x0fa, 0x0f6, 0x0f2,
  0x0ee, 0x0ea, 0x0e7, 0x0e3, 0x0df, 0x0dc, 0x0d8, 0x0d5,
  0x0d1, 0x0ce, 0x0ca, 0x0c7, 0x0c3, 0x0c0, 0x0bd, 0x0b9,
  0x0b6, 0x0b3, 0x0b0, 0x0ad, 0x0a9, 0x0a6, 0x0a3, 0x0a0,
  0x09d, 0x09a, 0x097, 0x094, 0x091, 0x08e, 0x08b, 0x088,
  0x085, 0x082, 0x07f, 0x07d, 0x07a, 0x077, 0x074, 0x071,
  0x06f, 0x06c, 0x069, 0x067, 0x064, 0x061, 0x05f, 0x05c,
  0x05a, 0x057, 0x054, 0x052, 0x04f, 0x04d, 0x04a, 0x048,
  0x045, 0x043, 0x041, 0x03e, 0x03c, 0x03a, 0x037, 0x035,
  0x033, 0x030, 0x02e, 0x02c, 0x029, 0x027, 0x025, 0x023,
  0x020, 0x01e, 0x01c, 0x01a, 0x018, 0x016, 0x014, 0x011,
  0x00f, 0x00d, 0x00b, 0x009, 0x007, 0x005, 0x003, 0x001,
  0x3fc, 0x3f4, 0x3ec, 0x3e5, 0x3dd, 0x3d5, 0x3ce, 0x3c7,
  0x3bf, 0x3b8, 0x3b1, 0x3aa, 0x3a3, 0x39c, 0x395, 0x38e,
  0x388, 0x381, 0x37a, 0x374, 0x36d, 0x367, 0x361, 0x35a,
  0x354, 0x34e, 0x348, 0x342, 0x33c, 0x336, 0x330, 0x32b,
  0x325, 0x31f, 0x31a, 0x314, 0x30f, 0x309, 0x304, 0x2fe,
  0x2f9, 0x2f4, 0x2ee, 0x2e9, 0x2e4, 0x2df, 0x2da, 0x2d5,
  0x2d0, 0x2cb, 0x2c6, 0x2c1, 0x2bd, 0x2b8, 0x2b3, 0x2ae,
  0x2aa, 0x2a5, 0x2a1, 0x29c, 0x298, 0x293, 0x28f, 0x28a,
  0x286, 0x282, 0x27d, 0x279, 0x275, 0x271, 0x26d, 0x268,
  0x264, 0x260, 0x25c, 0x258, 0x254, 0x250, 0x24c, 0x249,
  0x245, 0x241, 0x23d, 0x239, 0x235, 0x232, 0x22e, 0x22a,
  0x227, 0x223, 0x220, 0x21c, 0x218, 0x215, 0x211, 0x20e,
  0x20a, 0x207, 0x204, 0x200, 0x1fd, 0x1f9, 0x1f6, 0x1f3,
  0x1f0, 0x1ec, 0x1e9, 0x1e6, 0x1e3, 0x1df, 0x1dc, 0x1d9,
  0x1d6, 0x1d3, 0x1d0, 0x1cd, 0x1ca, 0x1c7, 0x1c4, 0x1c1,
  0x1be, 0x1bb, 0x1b8, 0x1b5, 0x1b2, 0x1af, 0x1ac, 0x1aa,
};

static inline int 
do_rsqrt(IA64_FR_t *newval, char *newp, IA64_FR_t *f3val, int isparallel)
{
  int flags=0;

  /* look for NaNs, zeros, and infinities
   * NaN = NaN        (invalid if snan)
   * -0 = -0          (unnorm if pseudo-zero)
   * +0 = 0           (unnorm if pseudo-zero)
   * -non-zero = QNaN (invalid)
   * inf = inf
   */
  if (fp_is_nan(*f3val)) {
    *newval = *f3val;
    *newp = 0;
    if (fp_is_snan(*f3val)) flags |= rf_mask(FPSR_f_v);
  } else if (fp_is_zero(*f3val)) {
    /* remember, when newp=0, we give the value for the sqrt, *NOT* the
     * reciprocal */
    *newval = isparallel ? IA64_Inf : *f3val;
    *newp = 0;
    if (f3val->exponent)
      flags |= rf_mask(FPSR_f_d); /* pseudo-zero, so it is really an unnorm */
  } else if (f3val->sign) { /* unsupported values are the negative ones */
    *newval = IA64_QNaN; 
    *newp = 0; 
    flags |= rf_mask(FPSR_f_v);
  } else if (fp_is_inf(*f3val)) {
    /* remember, when newp=0, we give the value for the sqrt, *NOT* the
     * reciprocal */
    *newval = isparallel ? IA64_FPZero : *f3val;  
    *newp = 0;
  } else {
    int exponent;
    
    /* Check for unnorms */
    if (fp_is_unorm(*f3val)) {
      flags |= rf_mask(FPSR_f_d);
      fp_fixweirdexp(f3val);
      fp_overnormalize(f3val); /* allow negative exponents here... */
    }
    exponent = f3val->exponent;

    /* This is a range check, but I do not know whether it is the right
     * range check....  I'm going to figure that the limit is half the
     * normal....
     */
    if (isparallel && 
	(f3val->exponent > 65535+63 || f3val->exponent < 65535-64)) {
      *newval = IA64_FPZero;
      *newp = 0;
      return -1;
    }

    if (f3val->exponent > 65535+16383 || f3val->exponent < 65535-16382) {
      
      if (IA64_swa_on_recip_range) {
	*newval = IA64_FPZero;
	*newp=1;
	return -1;
      }

      /* Create approximation of mantissa by using host sqrt
       * accurate to double and then throwing away some bits; can't
       * just cast to float because of range issues....
       */
      f3val->exponent = 65536 - (exponent&1);
      fp_host2reg_good(newval,(1.0 / sqrt(fp_reg2host(f3val))));
      /* set the true exponent, but must correct it if the approximation 
       * does not have the exponent we expected (which would be 65534)
       */
      newval->exponent = ( 65534-((exponent - 65535)>>1) + 
			   (newval->exponent-65534));
      
      /* Maximum exponent was 0x1fffe.  That becomes 0x07fff, which
       * is clearly not a denorm; therefore there are no denorm outputs.
       * Minimum exponent was (after overnormalization) -62.  That becomes
       * 1801c, which is not an overflow, therefore there is no overflow
       * possible on output.
       */
      /* give only 24 bits good */
      newval->significand &= UINT64_C(0xFfffFf0000000000);
      newval->sign = 0;
      *newp = 1;
      
    } else { /* look up from table */
      newval->exponent = 65534-((exponent-65535)>>1);
      newval->sign = 0;
      newval->significand =( (UINT64_C(1) << 63) |
			     (sqrttable[((exponent&1)<<7) |
				       ((f3val->significand>>56)&0x7f)]<<53));
      *newp = 1;
    }
  }
  return flags;
}

void 
evaluate_F7(LSE_emu_instr_info_t *ii)
{ 
  uint64_t instr = ii->extra.instr;
  IA64_FR_t f3val, newval;
  IA64_AR_t fpsr;
  char newp;
  uint64_t psr;

  psr = get_src_op(ii,src_psr,SR.PSR);
  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f3(instr));

  if (!get_src_op(ii,src_qp,PR)) set_dest_op(ii,dest_isfalse,PR,0);

  get_src_op_f(ii,src_op1,f3val);
  fpsr = get_src_op(ii,src_fpsr,AR);
  fpsr &= ~IA64_FPSR_FMASK; /* remove flag bits */

  if (fp_is_natval(f3val)) {
    set_dest_op_f(ii,dest_result,IA64_NatVAL);
    set_dest_op(ii,dest_isfalse,PR,0);
  } else { /* do real work */
    int flags=0, controls, traps;

    controls = (fpsr >> (6+13*F7_sf(instr))) & 0x7f;
    traps = rf_get(fpsr,FPSR_traps) | (rf_get(controls,FPSR_td)?~0:0);

    if (all_majorop(instr)) { /* parallel */

      IA64_FR_t left, right, newleft, newright;
      uint64_t vleft, vright;
      int flagsleft, flagsright;
      char pleft, pright;

      fp_mem2reg_s(f3val.significand & bits32, &right);
      fp_mem2reg_s((f3val.significand>>32) & bits32, &left); 

      flagsleft = do_rsqrt(&newleft, &pleft, &left, 1);
      flagsright = do_rsqrt(&newright, &pright, &right, 1);
      flags = flagsleft | flagsright;

      fp_reg2mem_s(&newleft, &vleft);
      fp_reg2mem_s(&newright, &vright);

      newp = pleft && pright;
      newval.significand = ((vleft & bits32) << 32) | (vright & bits32);
      newval.sign = 0;
      newval.exponent = 0x1003e;

    } else { /* not parallel */

      flags = do_rsqrt(&newval,&newp,&f3val,0);

    } /* else parallel form */

    if (IA64_swa_on_recip_range && flags < 0) { /* do_rsqrt wanted an SWA */
      report_intr(ii,IA64_intr_floatingpointfault);
    } else if (flags & ~traps) { 
      /* real IEEE fp fault (d or v) */
      report_intr(ii,IA64_intr_floatingpointfault);
    } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
      /* denorm input going to SWA */
      report_intr(ii,IA64_intr_floatingpointfault);
    }

    /* As there is no way in which an FP trap can
     * occur for this instruction, there is no need to look
     */

    fpsr |= flags << (13 * F7_sf(instr)+6+7);

    set_dest_op_f(ii,dest_result,newval);
    set_dest_op(ii,dest_isfalse,PR,newp);
  }
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  set_dest_op(ii,dest_fpsr,AR,fpsr);
  /* make certain we get the destination predicate right */
  if (!get_src_op(ii,src_qp,PR)) set_dest_op(ii,dest_isfalse,PR,0);
  return;
}

void 
opfetch_F8(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F8 - minimum/maximum and parallel compare */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    tval = get_f_ptr(realct,ii,all_f3(instr));
    set_src_op_f(ii,src_op2,*tval);
    if (!fallthrough) break;
  case src_fpsr:
    set_src_op(ii,src_fpsr,AR,get_ar(realct,ii,IA64_ADDR_FPSR));
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_F8(LSE_emu_instr_info_t *ii)
{ /* F8 - minimum/maximum and parallel compare */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t f2val, newval, f3val;
  IA64_AR_t fpsr;

  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));
  fp_check_disabled_register(realct,ii,psr,all_f3(instr));
    
  get_src_op_f(ii,src_op1,f2val);
  get_src_op_f(ii,src_op2,f3val);
  fpsr = get_src_op(ii,src_fpsr,AR);
  fpsr &= ~IA64_FPSR_FMASK; /* remove flag bits */

  if (fp_is_natval(f2val) || fp_is_natval(f3val)) {
    set_dest_op_f(ii,dest_result,IA64_NatVAL);
  } else { /* do real work */
    int controls, traps;
    unsigned int cond;
    int flags=0;

    controls = (fpsr >> (6+13*F1_sf(instr))) & 0x7f;
    traps = rf_get(fpsr,FPSR_traps) | (rf_get(controls,FPSR_td)?~0:0);

    if (all_majorop(instr)) { /* parallel forms */
      uint64_t vleft, vright;
      IA64_FR_t f2valleft, f2valright, f3valleft, f3valright;
      IA64_FR_t newvalleft, newvalright;
      int flags;

      fp_mem2reg_s(f3val.significand & bits32, &f3valright);
      fp_mem2reg_s((f3val.significand>>32) & bits32, &f3valleft);
      fp_mem2reg_s(f2val.significand & bits32, &f2valright);
      fp_mem2reg_s((f2val.significand>>32) & bits32, &f2valleft);

      if (F8_x6(instr)&0x20) { /* fpcmp */

	cond = do_fpcompare(&flags,f2valleft, f3valleft, F8_x6(instr)&3,0);
	if (F8_x6(instr)&4) { /* negative forms */
	  vleft = cond ? 0 : ~0;
	} else {
	  vleft = cond ? ~0 : 0;
	}
	cond = do_fpcompare(&flags,f2valright, f3valright, F8_x6(instr)&3,0);
	if (F8_x6(instr)&4) { /* negative forms */
	  vright = cond ? 0 : ~0;
	} else {
	  vright = cond ? ~0 : 0;
	}

      } else { /* fpmin/pfmax/fpmin/fpmax */

	if (F8_x6(instr)&2) {
	  f2valleft.sign = f2valright.sign = 0;
	  f3valleft.sign = f3valright.sign = 0;
	}
	cond = do_fpcompare(&flags,f2valleft,f3valleft,1, 
			    F8_x6(instr)&1); /* use .lt */
	if (F8_x6(instr)&1) newvalleft = cond ? f3valleft : f2valleft;
	else newvalleft = cond ? f2valleft : f3valleft;

	cond = do_fpcompare(&flags,f2valright,f3valright,1,
			    F8_x6(instr)&1); /* use .lt */
	if (F8_x6(instr)&1) newvalright = cond ? f3valright : f2valright;
	else newvalright = cond ? f2valright : f3valright;	

	fp_reg2mem_s(&newvalleft, &vleft);
	fp_reg2mem_s(&newvalright, &vright);
      
      }

      if (flags & ~traps) {
	/* only one of [dv] possible for this op, so fault */
	/* real IEEE fp fault (d or v) */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
	/* denorm input going to SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      }

      newval.exponent = 0x1003e;
      newval.sign = 0;
      newval.significand = ((vleft & bits32) << 32) | (vright & bits32);

    } else { /* fmin/fmax/famin/famax */

      /* TODO: figure out what are real exceptions here.... overflow, 
       * obviously.  Is underflow an issue?  What about denorms?  
       * Signaling NaN's? unnormalized numbers?
       */
      if (F8_x6(instr)&2) {
	f2val.sign = 0;
	f3val.sign = 0;
      }
      cond = do_fpcompare(&flags,f2val,f3val,1,
			  F8_x6(instr)&1); /* use .lt */
      if (F8_x6(instr)&1) newval = cond ? f3val : f2val;
      else newval = cond ? f2val : f3val;

      if (flags & ~traps) {
	/* only one of [dv] possible for this op, so fault */
	/* real IEEE fp fault (d or v) */
	report_intr(ii,IA64_intr_floatingpointfault);
      } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
	/* denorm input going to SWA */
	report_intr(ii,IA64_intr_floatingpointfault);
      }


    } /* else not parallel */

    fpsr |= flags << (13 * F8_sf(instr)+6+7);

    set_dest_op_f(ii,dest_result,newval);
  } /* else not a natval */
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  set_dest_op(ii,dest_fpsr,AR,fpsr);
  return;
}

void 
opfetch_F9(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F9 - merge and logical */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_op2:
    tval = get_f_ptr(realct,ii,all_f3(instr));
    set_src_op_f(ii,src_op2,*tval);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_F9(LSE_emu_instr_info_t *ii)
{ /* F9 - merge and logical */
  uint64_t instr = ii->extra.instr;
  IA64_FR_t f2val, f3val, newval;
  uint64_t psr;

  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  get_src_op_f(ii,src_op1,f2val);
  get_src_op_f(ii,src_op2,f3val);

  if (fp_is_natval(f2val) || fp_is_natval(f3val)) {
    set_dest_op_f(ii,dest_result,IA64_NatVAL);
  } else { /* real work */

    switch (F9_x6(instr) & 0x1f) {

    case 0x8: /* fpack */
      {
	uint64_t v1, v2;
	fp_reg2mem_s(&f2val,&v1);
	fp_reg2mem_s(&f3val,&v2);
	newval.significand = (v1<<32) | v2;
	newval.exponent = 0x1003e;
	newval.sign = 0;
      }
      break;

    case 0xc : /* fand */
      newval.significand = f2val.significand & f3val.significand;
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0xd : /* fandcm */
      newval.significand = f2val.significand & ~f3val.significand;
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0xe : /* for */
      newval.significand = f2val.significand | f3val.significand;
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0xf : /* fxor */
      newval.significand = f2val.significand ^ f3val.significand;
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0x10 : /* f[p]merge.s */

      if (all_majorop(instr)) { /* fpmerge.s */
	newval.significand = ( (f3val.significand   & UINT64_C(0x7fffFfff7fffFfff)) |
			       ( (f2val.significand & UINT64_C(0x8000000080000000))));
	newval.exponent = 0x1003e;
	newval.sign = 0;
      } else { /* fmerge.s */
	newval.significand = f3val.significand;
	newval.exponent = f3val.exponent;
	newval.sign = f2val.sign;
      }
      break;

    case 0x11 : /* f[p]merge.ns */

      if (all_majorop(instr)) { /* fpmerge.ns */
	newval.significand = ( (f3val.significand   & UINT64_C(0x7fffFfff7fffFfff)) |
			       ( (f2val.significand & UINT64_C(0x8000000080000000)) ^
				 UINT64_C(0x8000000080000000)));
	newval.exponent = 0x1003e;
	newval.sign = 0;
      } else { /* fmerge.s n*/
	newval.significand = f3val.significand;
	newval.exponent = f3val.exponent;
	newval.sign = 1-f2val.sign;
      }
      break;

    case 0x12 : /* f[p]merge.se */

      if (all_majorop(instr)) { /* fpmerge.se */
	newval.significand = ( (f3val.significand   & UINT64_C(0x007fFfff007fFfff)) |
			       ( (f2val.significand & UINT64_C(0xff800000ff800000))));
	newval.exponent = 0x1003e;
	newval.sign = 0;
      } else { /* fmerge.se n*/
	newval.significand = f3val.significand;
	newval.exponent = f2val.exponent;
	newval.sign = f2val.sign;
      }
      break;

    case 0x14: /* fswap */
      newval.significand = (((f3val.significand<<32) & UINT64_C(0xFfffFfff00000000))|
			    ((f2val.significand>>32) & UINT64_C(0x00000000FfffFfff)));
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0x15: /* fswap.nl */
      newval.significand = (((f3val.significand<<32) & UINT64_C(0xFfffFfff00000000))|
			    ((f2val.significand>>32) & UINT64_C(0x00000000FfffFfff)));
      newval.significand ^= UINT64_C(0x8000000000000000);
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0x16: /* fswap.nr */
      newval.significand = (((f3val.significand<<32) & UINT64_C(0xFfffFfff00000000))|
			    ((f2val.significand>>32) & UINT64_C(0x00000000FfffFfff)));
      newval.significand ^= UINT64_C(0x0000000080000000);
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0x19: /* fmix.lr */
      newval.significand = (((f3val.significand    ) & UINT64_C(0x00000000FfffFfff))|
			    ((f2val.significand    ) & UINT64_C(0xFfffFfff00000000)));
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0x1a: /* fmix.r */
      newval.significand = (((f3val.significand    ) & UINT64_C(0x00000000FfffFfff))|
			    ((f2val.significand<<32) & UINT64_C(0xFfffFfff00000000)));
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0x1b: /* fmix.l */
      newval.significand = (((f3val.significand>>32) & UINT64_C(0x00000000FfffFfff))|
			    ((f2val.significand    ) & UINT64_C(0xFfffFfff00000000)));
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0x1c: /* fsxt.r */
      newval.significand 
	= ((f3val.significand & INT64_C(0xFfffFfff) ) |
	   ((f2val.significand & (INT64_C(1)<<31)) 
	    ? (INT64_C(0xFfffFfff) << 32) : 0));
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    case 0x1d: /* fsxt.l */
      newval.significand 
	= ((f3val.significand >> 32) |
	   ((f2val.significand & (INT64_C(1)<<63)) 
	    ? (INT64_C(0xFfffFfff) << 32) : 0));
      newval.exponent = 0x1003e;
      newval.sign = 0;
      break;

    default:
      newval = IA64_NatVAL; /* make compiler happy */
      report_actual_intr(ii,IA64_intr_notimplemented);
    }
    set_dest_op_f(ii,dest_result,newval);
  }
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  return;
}

void 
opfetch_F10(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F10 - convert floating-point to fixed-point */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  case src_fpsr:
    set_src_op(ii,src_fpsr,AR,get_ar(realct,ii,IA64_ADDR_FPSR));
    if (!fallthrough) break;
  default: break;
  }
}

static inline int
do_fcvt(uint64_t *val, int *flags,
	IA64_FR_t f2val, int rmode, int issigned, int isparallel)
{
  int inexact=0;

  if (fp_is_nan(f2val) || fp_is_inf(f2val)) {
    *val = UINT64_C(0x1) << 63;
    *flags |= rf_mask(FPSR_f_v);
  } else {
    int shift_amt = (63-(f2val.exponent-65535));

    if (fp_is_unorm(f2val)) {
      *flags |= rf_mask(FPSR_f_d);
      fp_fixweirdexp(&f2val);
      fp_overnormalize(&f2val);
    }

    if (shift_amt < 0) { /* unsigned overflow */
      *val = UINT64_C(0x1) << 63;
      *flags |= rf_mask(FPSR_f_v);
    } else { 
      int round, sticky, fpa;
      IA64_FR_t newval;
      
      newval = f2val;

      switch (shift_amt) {
      case 0: /* no round or guard */
	newval.significand = f2val.significand;
	round = sticky = 0;
	break;
      case 1:
	newval.significand = f2val.significand>>1;
	round = f2val.significand & 1;
	sticky = 0;
	break;
      case 64:
	round = (f2val.significand & (INT64_C(1)<<63)) ? 1 : 0;
	sticky = (f2val.significand & bits63)?1:0;
	newval.significand = 0;
	break;
      default:
	if (shift_amt > 64) { /* 0 < abs(number) < 0.5 */
	  newval.significand = 0;
	  round = 0; sticky = f2val.significand ? 1 : 0;
	} else {
	  newval.significand = (f2val.significand
				>> shift_amt);
	  round = ((f2val.significand >> (shift_amt-1)) & 1);
	  sticky = ((f2val.significand & 
		     ((INT64_C(1)<<(shift_amt-1))-1)) ? 1:0);
	}
      } /* switch (shift_amt) */
      newval.sign = f2val.sign;
      /* newval.exponent = 63+65535; */
      inexact = fp_round(&newval,&newval,&fpa,4,rmode,round,sticky);

      /* now figure out if we overflowed due to rounding */
      if ((isparallel && (UINT64_C(0xFfffFfff00000000) & newval.significand)) ||
	  (fpa && !newval.significand) /* rolled over*/
	  ) {
	*val = UINT64_C(0x1) << 63;
	*flags |= rf_mask(FPSR_f_v); 
      }
      else 
      /* now deal with signs, including the possibility of signed overflow */
      if (issigned) {
	if (isparallel) {

	  if ( (newval.significand & (UINT64_C(1)<<31)) &&
	       (!newval.sign || (newval.significand & ((UINT64_C(1)<<31)-1)))) {
	    *val = UINT64_C(0x1) << 63;
	    *flags |= rf_mask(FPSR_f_v);
	  } else {
	    if (newval.sign) *val = (-newval.significand) & bits64;
	    else *val = newval.significand;
	  }

	} else { /* ifparallel */

	  if ( (newval.significand & (UINT64_C(1)<<63)) &&
	       (!newval.sign || (newval.significand & ((UINT64_C(1)<<63)-1)))) {
	    *val = UINT64_C(0x1) << 63;
	    *flags |= rf_mask(FPSR_f_v);
	  } else {
	    if (newval.sign) *val = (-newval.significand) & bits64;
	    else *val = newval.significand;
	  }

	} /* else ifparallel */
      } /* if issigned */
      else *val = newval.significand;

    } /* else  shift amount */
  } /* else not nan/inf */
  return inexact; 
}

void 
evaluate_F10(LSE_emu_instr_info_t *ii)
{ /* F10 - convert floating-point to fixed-point */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t f2val, newval;
  IA64_AR_t fpsr;
  unsigned int rmode, controls, traps;

  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));
    
  get_src_op_f(ii,src_op1,f2val);
  fpsr = get_src_op(ii,src_fpsr,AR);
  fpsr &= ~IA64_FPSR_FMASK; /* remove flag bits */
  controls = (fpsr >> (6+13*F10_sf(instr))) & 0x7f;
  traps = rf_get(fpsr,FPSR_traps) | (rf_get(controls,FPSR_td)?~0:0);
  rmode = (F10_x6(instr) & 2) ? 3 : rf_get(controls,FPSR_rc);
    
  if (fp_is_natval(f2val)) {
    set_dest_op_f(ii,dest_result,IA64_NatVAL);
  } else { /* do real work */
    int inexact, flags=0;
  
    if (all_majorop(instr)) { /* parallel form */
      IA64_FR_t leftnum, rightnum;
      uint64_t vleft, vright;

      fp_mem2reg_s(f2val.significand & bits32, &rightnum);
      fp_mem2reg_s((f2val.significand>>32) & bits32, &leftnum); 

      inexact = do_fcvt(&vleft, &flags, leftnum, rmode,
			!(F10_x6(instr) & 1),1);
      inexact |= do_fcvt(&vright, &flags, rightnum, rmode,
			!(F10_x6(instr) & 1),1);

      newval.significand = ((vleft & bits32) << 32) | (vright & bits32);
      
    } else {

      inexact = do_fcvt(&newval.significand, &flags, f2val, rmode,
			!(F10_x6(instr) & 1),0);
    } /* else parallel form */

    if (flags & ~traps) { 
      /* only one of [dv] possible for this op, so fault */
      /* real IEEE fp fault (d or v) */
      report_intr(ii,IA64_intr_floatingpointfault);
    } else if (IA64_swa_on_input_unnorms && rf_get(flags,FPSR_f_d)) { 
      /* denorm input going to SWA */
      report_intr(ii,IA64_intr_floatingpointfault);
    } else if (inexact) { /* inexactness */
      flags |= rf_mask(FPSR_f_i);
      if (~traps & rf_mask(FPSR_f_i)) {
	report_intr(ii,IA64_intr_floatingpointtrap);
      }
    }

    newval.exponent = 0x1003e;
    newval.sign = 0;

    fpsr |= flags << (13 * F10_sf(instr)+6+7);
    set_dest_op_f(ii,dest_result,newval);

  } /* else not a natval */

  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  set_dest_op(ii,dest_fpsr,AR,fpsr);
  return;
}

void 
opfetch_F11(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F11 - convert fixed-point to floating-point */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_FR_t *tval;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tval = get_f_ptr(realct,ii,all_f2(instr));
    set_src_op_f(ii,src_op1,*tval);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_F11(LSE_emu_instr_info_t *ii) 
{ /* F11 - convert fixed-point to floating-point */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_FR_t f2val;
  psr = get_src_op(ii,src_psr,SR.PSR);

  fp_check_target_register(realct,ii,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f1(instr));
  fp_check_disabled_register(realct,ii,psr,all_f2(instr));

  get_src_op_f(ii,src_op1,f2val);
    
  if (fp_is_natval(f2val)) {
    set_dest_op_f(ii,dest_result,IA64_NatVAL);
  } else { /* do real work */
    IA64_FR_t newval;
    newval.exponent = 0x1003e;
    if (f2val.significand & (INT64_C(1)<<63)) {
      newval.significand = ~f2val.significand + 1;
      newval.sign = 1;
    } else {
      newval.significand = f2val.significand;
      newval.sign = 0;
    }
    fp_normalize(&newval);
    set_dest_op_f(ii,dest_result,newval);
  }
  psr |= ii->operand_dest[dest_psr].uses.reg.bits[0] & rf_mask(PSR_mfboth);
  set_dest_op(ii,dest_psr,SR.PSR,psr);
  return;
}

void 
opfetch_F12(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F12 - floating-point set controls */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    set_src_op(ii,src_op1,imm64,F12_amask7b(instr));
    if (!fallthrough) break;
  case src_op2:
    if ((F12_omask7c(instr) & 0x40) && F12_sf(instr)==0) {
      report_potential_intr(ii,IA64_intr_reservedregisterfield);
    }
    set_src_op(ii,src_op2,imm64,F12_omask7c(instr));
    if (!fallthrough) break;
  case src_fpsr:
    set_src_op(ii,src_fpsr,AR,get_ar(realct,ii,IA64_ADDR_FPSR));
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_F12(LSE_emu_instr_info_t *ii) 
{ /* F12 - floating-point set controls */
  uint64_t instr = ii->extra.instr;
  IA64_AR_t fpsr;
  uint64_t newval;

  if ((F12_omask7c(instr) & 0x40) && F12_sf(instr)==0) {
    report_intr(ii,IA64_intr_reservedregisterfield);
  }

  fpsr = get_src_op(ii,src_fpsr,AR);
  newval = ((rf_get(fpsr,FPSR_sf0) & 0x7f & get_src_op(ii,src_op1,imm64)) |
		get_src_op(ii,src_op2,imm64));
  newval <<= 6 + 13*F12_sf(instr);
  fpsr = (fpsr & ~(0x7f<<(6+13*F12_sf(instr)))) | newval;
  set_dest_op(ii,dest_fpsr,AR,fpsr);
  return;
}

void 
writeback_F12(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F12 - floating-point set controls */
  uint64_t instr = ii->extra.instr;
  uint64_t mask;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case 0: /* for fallthrough */
    if (!fallthrough) break;
  case dest_fpsr:
    mask = 0x7f<<(6+13*F12_sf(instr));
    set_ar_sbits(dest_fpsr,ii,IA64_ADDR_FPSR,get_dest_op(ii,dest_fpsr,AR),
		   mask);
    if (!fallthrough) break;
  default: 
    break;
  }
}

void 
opfetch_F13(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F13 - floating-point clear flags */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_F13(LSE_emu_instr_info_t *ii) 
{ /* F13 - floating-point clear flags */
  uint64_t instr = ii->extra.instr;
  IA64_AR_t fpsr;

  fpsr = 0;  /* do not look at the rest of fpsr since we do not fetch it */
  fpsr &= ~(rf_mask(FPSR_sf_flags)<<(13*F12_sf(instr)+6));
  set_dest_op(ii,dest_fpsr,AR,fpsr);
  return;
}

void 
writeback_F13(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F13 - floating-point clear flags */
  uint64_t instr = ii->extra.instr;
  uint64_t mask;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case 0: /* for fallthrough */
    if (!fallthrough) break;
  case dest_fpsr:
    mask = rf_mask(FPSR_sf_flags)<<(13*F12_sf(instr)+6);
    set_ar_sbits(dest_fpsr,ii,IA64_ADDR_FPSR,get_dest_op(ii,dest_fpsr,AR),
		 mask);
    if (!fallthrough) break;
  default: 
    break;
  }
}

void 
opfetch_F14(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* F14 - floating-point check flags */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_fpsr:
    set_src_op(ii,src_fpsr,AR,get_ar(realct,ii,IA64_ADDR_FPSR));
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_F14(LSE_emu_instr_info_t *ii) 
{ /* F14 - floating-point check flags */
  uint64_t instr = ii->extra.instr;
  uint64_t psr;
  IA64_AR_t fpsr;
  int flags;

  psr = get_src_op(ii,src_psr,SR.PSR);
  fpsr = get_src_op(ii,src_fpsr,AR);
  flags = fpsr >> (13*F12_sf(instr)+13); /* get the flags */

  ii->branch_dir = 0;
  if ((flags & ~fpsr & 0x3f) || (flags & ~(fpsr>>13) & 0x3f)) { 
    if (IA64_implemented_check_branch_fchkf) {
      uint64_t tmp_IP = get_src_op(ii,src_op2,imm64);

      if (get_src_op(ii,src_qp,PR)) {
	ii->branch_dir = 1;
	ii->next_pc = tmp_IP;
      }

      if ((rf_get(psr,PSR_it) && IA64_unimplemented_va(tmp_IP)) ||
	  (!rf_get(psr,PSR_it) && IA64_unimplemented_pa(tmp_IP))) {
	report_intr(ii,IA64_intr_unimplementedinstructionaddress);
      }
      if (rf_get(psr,PSR_tb)) {
	report_intr(ii,IA64_intr_takenbranch);
      }
    } else {
      report_intr(ii,IA64_intr_speculativeoperation);
    }
  }
  return;
}

/* F15 - break is the same as M37 */
/* F16 - nop/hint is the same as I18 */

void 
opfetch_B1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B1 - IP-relative branch */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_branchoff:
    set_src_op(ii,src_branchoff,BR,ii->branch_targets[1]); 
    if (!fallthrough) break;
  case src_ec:
    if (B1_btype(instr)>0) { /* wexit/wtop */
      set_src_op(ii,src_ec,AR,get_ar(realct,ii,IA64_ADDR_EC));
    }
    if (!fallthrough) break;
  default: break;
  }
}
  
void 
opfetch_B2(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B2 - IP-relative counted branch */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_branchoff:
    set_src_op(ii,src_branchoff,BR,ii->branch_targets[1]);
    if (!fallthrough) break;
  case src_lc:
    set_src_op(ii,src_lc,AR,get_ar(realct,ii,IA64_ADDR_LC));
    if (!fallthrough) break;
  case src_ec:
    if (B2_btype(instr)>5) { /* cexit/ctop */
      set_src_op(ii,src_ec,AR,get_ar(realct,ii,IA64_ADDR_EC));
    }
    if (!fallthrough) break;
  default: break;
  }
}
  
void 
opfetch_B3(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B3 - IP-relative call */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_branchoff:
    set_src_op(ii,src_branchoff,BR,ii->branch_targets[1]);
    if (!fallthrough) break;
  case src_bsp:
    set_src_op(ii,src_bsp,AR,get_ar(realct,ii,IA64_ADDR_BSP));
    if (!fallthrough) break;
  case src_ec:
    set_src_op(ii,src_ec,AR,get_ar(realct,ii,IA64_ADDR_EC));
    if (!fallthrough) break;
  case src_rse:
    set_src_op_rse(ii,src_rse,
		   realct->RSE_other.NDirty,realct->RSE_other.NClean);
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_B4(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B4 - indirect branch */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_branchoff:
    set_src_op(ii,src_branchoff,BR,get_b(realct,ii,B4_b2(instr)));
    if (!fallthrough) break;
  case src_bsp:
    if (B2_btype(instr)==4) { /* ret */
      set_src_op(ii,src_bsp,AR,get_ar(realct,ii,IA64_ADDR_BSP));
    }
    if (!fallthrough) break;
  case src_pfs:
    if (B2_btype(instr)==4) { /* ret */
      set_src_op(ii,src_pfs,AR,get_ar(realct,ii,IA64_ADDR_PFS));
    }
    if (!fallthrough) break;
  case src_rse:
    if (B2_btype(instr)==4) { /* ret */
      set_src_op_rse(ii,src_rse,
		     realct->RSE_other.NDirty,realct->RSE_other.NClean);
    }
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_B5(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B5 - indirect call */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_branchoff:
    set_src_op(ii,src_branchoff,BR,get_b(realct,ii,B5_b2(instr)));
    if (!fallthrough) break;
  case src_bsp:
    set_src_op(ii,src_bsp,AR,get_ar(realct,ii,IA64_ADDR_BSP));
    if (!fallthrough) break;
  case src_ec:
    set_src_op(ii,src_ec,AR,get_ar(realct,ii,IA64_ADDR_EC));
    if (!fallthrough) break;
  case src_rse:
    set_src_op_rse(ii,src_rse,
		   realct->RSE_other.NDirty,realct->RSE_other.NClean);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_br(LSE_emu_instr_info_t *ii) { /* B1, B2, B3, B4, B5 */
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;
  uint64_t tmp_IP;
  int lpt, taken;
  unsigned int bof;
  int btype= (format==303 || format==305 || format==404) ? 8 : B4_btype(instr);
  uint64_t psr,ndirty,nclean;
  IA64_AR_t pfs, bsp;
  IA64_SR_t cfm;

  psr = get_src_op(ii,src_psr,SR.PSR);
  tmp_IP = get_src_op(ii,src_branchoff,BR);
  ii->branch_targets[1] = tmp_IP;
  ii->branch_num_targets = 2;
  
  /* check for switch-to-IA32 mode */
  
  lpt = 0;
  
  if (format > 400) {  /* Yes, X3 and X4 end up here, too... */
    /* if there is no stop bit, behavior is undefined, but our definition
     * of undefined is to "do the right thing"....
     */
    if (!IA64_implemented_long_branch) {
      report_intr(ii,IA64_intr_illegaloperation);
    }
    if (btype != 0 && btype != 8) { /* only conditional and call supported */
      report_intr(ii,IA64_intr_illegaloperation);
    }
  }

  /* set_dest_op(ii,dest_alatupdate,alatchange,0); */

  switch (btype) {
  case 0: /* cond */
    taken = get_src_op(ii,src_qp,PR);
    break;
    
  case 2: /* wexit */
  case 3: /* wtop */
    {
      uint64_t ec;
      int pred;
      /* slot check at decode */
      pred = get_src_op(ii,src_qp,PR);
      ec = get_src_op(ii,src_ec,AR);
      cfm = get_src_op(ii,src_cfm,SR);
      
      if (btype==3) taken = (pred || ec > 1);
      else taken = !(pred || ec > 1);
      
      if (pred) {
	set_dest_op(ii,dest_ec,AR,ec);
	set_dest_op(ii,dest_result,PR,0);
	if (rf_get(cfm.CFM.val,CFM_sor)) 
	  rf_set(cfm.CFM.val,CFM_rrb_gr,
		 ((rf_get(cfm.CFM.val,CFM_rrb_gr) + 
		   (rf_get(cfm.CFM.val,CFM_sor)<<3)-1) %
		  (rf_get(cfm.CFM.val,CFM_sor)<<3)));
	rf_set(cfm.CFM.val,CFM_rrb_fr,
	       ((rf_get(cfm.CFM.val,CFM_rrb_fr) + MAX_FR_ROT_REGS-1) % 
		MAX_FR_ROT_REGS));
	rf_set(cfm.CFM.val,CFM_rrb_pr,
	       ((rf_get(cfm.CFM.val,CFM_rrb_pr) + MAX_PR_ROT_REGS-1) % 
		MAX_PR_ROT_REGS));
      } else if (ec != 0) {
	set_dest_op(ii,dest_ec,AR,ec-1);
	set_dest_op(ii,dest_result,PR,0);
	if (rf_get(cfm.CFM.val,CFM_sor)) 
	  rf_set(cfm.CFM.val,CFM_rrb_gr,
		 ((rf_get(cfm.CFM.val,CFM_rrb_gr) + 
		   (rf_get(cfm.CFM.val,CFM_sor)<<3)-1) %
		  (rf_get(cfm.CFM.val,CFM_sor)<<3)));
	rf_set(cfm.CFM.val,CFM_rrb_fr,
	       ((rf_get(cfm.CFM.val,CFM_rrb_fr) + MAX_FR_ROT_REGS-1) % 
		MAX_FR_ROT_REGS));
	rf_set(cfm.CFM.val,CFM_rrb_pr,
	       ((rf_get(cfm.CFM.val,CFM_rrb_pr) + MAX_PR_ROT_REGS-1) % 
		MAX_PR_ROT_REGS));
      } else {
	set_dest_op(ii,dest_ec,AR,ec);
	set_dest_op(ii,dest_result,PR,0);
	/* leave CFM alone */
      }
      /* important: do it here so p[63] rotates in correctly */
      set_dest_op(ii,dest_cfm,SR,cfm);
    }
    break;
    
  case 4: /* ret */
    taken = get_src_op(ii,src_qp,PR);
    {
      int growth;
      uint64_t newbsp;

      pfs = get_src_op(ii,src_pfs,AR);
      cfm = get_src_op(ii,src_cfm,SR);
      growth = (rf_get(pfs,PFS_pfm_sof) - rf_get(pfs,PFS_pfm_sol) 
		- rf_get(cfm.CFM.val,CFM_sof));
      
      bsp = get_src_op(ii,src_bsp,AR);
      /* Basically, BSP is never allowed to point at 63 */
      newbsp = (bsp&~INT64_C(7)) + 8 * (- rf_get(pfs,PFS_pfm_sol) - 
				 (62 - ((bsp>>3)&0x3f) +
				  rf_get(pfs,PFS_pfm_sol))/63);
      set_dest_op(ii,dest_bsp,AR,newbsp);

      bof = cfm.CFM.BOF;
      bof = IA64_GR_FAST_ROT((bof + realct->num_stacked_phys 
			      - rf_get(pfs,PFS_pfm_sol)),
			     realct->num_stacked_phys);
      set_dest_op(ii,dest_cfm,SR.CFM.BOF,bof);

      ndirty = get_src_op(ii,src_rse,RSE_other.NDirty);
      nclean = get_src_op(ii,src_rse,RSE_other.NClean);

      set_dest_op_rsechange(ii,dest_rsemem,0,0);
      if (rf_get(pfs,PFS_pfm_sof) - rf_get(pfs,PFS_pfm_sol) > 
	  (realct->num_stacked_phys - ndirty)) { /* bad PFS */
	set_dest_op(ii,dest_cfm,SR.CFM.val,0);
	ndirty -= rf_get(pfs,PFS_pfm_sol);
      } else {
	/* figure out fills */
	int NInvalid, NNeeded;

	NInvalid = (realct->num_stacked_phys - ndirty - nclean
		    - rf_get(cfm.CFM.val,CFM_sof));

	if (growth > 0) { /* very weird... entering from right */
	  if (growth > NInvalid) {
	    nclean -= (growth - NInvalid);
	    NInvalid = 0; /* eat all invalid */
	  } else NInvalid -= growth;
	} else NInvalid -= growth; /* add child frame into invalid */

	/* number of registers entering current frame from left */
	NNeeded = rf_get(pfs,PFS_pfm_sol);

	if (NNeeded > ndirty) { /* BSPSTORE > newBSP now */
	  NNeeded -= ndirty;
	  ndirty = 0;
	  if (NNeeded > nclean) { /* will need to fill */
	    int rnatcols;
	    NNeeded -= nclean;
	    nclean = 0;
	    /* Need to fill and make BSPSTORE equal BSP; fill need not happen
	     * for registers which are already clean... real machine does this
	     * autonomously, but the machine is stalled in the meanwhile.
	     * Interrupts should be delivered on next instruction.
	     *
  	     * Also, a real machine might not have spilled RNAT if that was
	     * the last thing to spill.  But since we don't look at bspstore,
	     * we can't know this.  As a result, we'll just assume it did,
	     * and our emulated RSE will do final RNAT spills.
	     */
	    rnatcols = (((newbsp>>3)&0x3f)+NNeeded)/63;
	    set_dest_op_rsechange(ii,dest_rsemem,-NNeeded,-NNeeded-rnatcols);
	  } else { 
	    /* just need to make BSPSTORE equal BSP; real machine does this
	     * autonomously, but the machine is stalled in the meanwhile
	     */
	    nclean -= NNeeded;
	  }
	} else ndirty -= NNeeded;

	set_dest_op(ii,dest_cfm,SR.CFM.val,rf_get(pfs,PFS_pfm));
      }

      set_dest_op_rse(ii,dest_rse,ndirty,nclean);

      set_dest_op(ii,dest_ec,AR,rf_get(pfs,PFS_pec));
      if (rf_get(psr,PSR_cpl) < rf_get(pfs,PFS_ppl)) {
	rf_set(psr,PSR_cpl,rf_get(pfs,PFS_ppl));
	lpt = 1;
      }
      set_dest_op(ii,dest_psr,SR.PSR,psr);
    }
    break;
    
  case 5: /* cloop */
    {
      uint64_t lc;
      /* slot check in decode */
      lc = get_src_op(ii,src_lc,AR);
      taken = lc != 0;
      set_dest_op(ii,dest_lc,AR,taken ? lc-1 : lc);
    }
    break;
    
  case 6: /* cexit */
  case 7: /* ctop */
    {
      uint64_t lc,ec;
      /* slot check in decode */
      lc = get_src_op(ii,src_lc,AR);
      ec = get_src_op(ii,src_ec,AR);
      cfm = get_src_op(ii,src_cfm,SR);
      if (btype==7) 
	taken = (lc != 0) || (ec > 1);
      else
	taken = !((lc != 0) || (ec > 1));
      if (lc != 0) {
	set_dest_op(ii,dest_lc,AR,lc-1);
	set_dest_op(ii,dest_ec,AR,ec);
	set_dest_op(ii,dest_istrue,PR,1);
	if (rf_get(cfm.CFM.val,CFM_sor)) 
	  rf_set(cfm.CFM.val,CFM_rrb_gr,
		 ((rf_get(cfm.CFM.val,CFM_rrb_gr) + 
		   (rf_get(cfm.CFM.val,CFM_sor)<<3)-1) %
		  (rf_get(cfm.CFM.val,CFM_sor)<<3)));
	rf_set(cfm.CFM.val,CFM_rrb_fr,
	       ((rf_get(cfm.CFM.val,CFM_rrb_fr) + MAX_FR_ROT_REGS-1) % 
		MAX_FR_ROT_REGS));
	rf_set(cfm.CFM.val,CFM_rrb_pr,
	       ((rf_get(cfm.CFM.val,CFM_rrb_pr) + MAX_PR_ROT_REGS-1) % 
		MAX_PR_ROT_REGS));
      } else if (ec != 0) {
	set_dest_op(ii,dest_lc,AR,lc);
	set_dest_op(ii,dest_ec,AR,ec-1);
	set_dest_op(ii,dest_istrue,PR,0);
	if (rf_get(cfm.CFM.val,CFM_sor)) 
	  rf_set(cfm.CFM.val,CFM_rrb_gr,
		 ((rf_get(cfm.CFM.val,CFM_rrb_gr) + 
		   (rf_get(cfm.CFM.val,CFM_sor)<<3)-1) % 
		  (rf_get(cfm.CFM.val,CFM_sor)<<3)));
	rf_set(cfm.CFM.val,CFM_rrb_fr, 
	       ((rf_get(cfm.CFM.val,CFM_rrb_fr) + MAX_FR_ROT_REGS-1) % 
		MAX_FR_ROT_REGS));
	rf_set(cfm.CFM.val,CFM_rrb_pr, 
	       ((rf_get(cfm.CFM.val,CFM_rrb_pr) + MAX_PR_ROT_REGS-1) % 
		MAX_PR_ROT_REGS));
      } else {
	set_dest_op(ii,dest_lc,AR,lc);
	set_dest_op(ii,dest_ec,AR,ec);
	set_dest_op(ii,dest_istrue,PR,0);
	/* leave CFM alone */
      }
      /* important: do it here so p[63] rotates in correctly */
      set_dest_op(ii,dest_cfm,SR,cfm); 
    }
    break;
    
  case 8: /* call */
    taken = get_src_op(ii,src_qp,PR);
    {
      set_dest_op(ii,dest_result,BR,((ii->addr & ~INT64_C(3)) + 16));
      pfs = 0;/*get_src_op(ii,src_pfs,AR);*/
      cfm = get_src_op(ii,src_cfm,SR);
      rf_set(pfs,PFS_pfm,cfm.CFM.val);
      rf_set(pfs,PFS_pec,get_src_op(ii,src_ec,AR));
      rf_set(pfs,PFS_ppl,rf_get(psr,PSR_cpl));
      set_dest_op(ii,dest_pfs,AR,pfs);

      /* real ALATs have work to do; we don't */
      bof = cfm.CFM.BOF;
      bof = IA64_GR_FAST_ROT(bof + rf_get(cfm.CFM.val,CFM_sol),
			     realct->num_stacked_phys);
      cfm.CFM.BOF = bof;

      ndirty = get_src_op(ii,src_rse,RSE_other.NDirty);
      ndirty += rf_get(cfm.CFM.val,CFM_sol);
      nclean = get_src_op(ii,src_rse,RSE_other.NClean);

      set_dest_op_rse(ii,dest_rse,ndirty,nclean);

      bsp = get_src_op(ii,src_bsp,AR);
      set_dest_op(ii,dest_bsp,AR,
	     (bsp&~INT64_C(7)) + 8*(rf_get(cfm.CFM.val,CFM_sol) + 
		 (((bsp>>3)&0x3f)+rf_get(cfm.CFM.val,CFM_sol))/63));

      rf_set(cfm.CFM.val,CFM_sof,
	     rf_get(cfm.CFM.val,CFM_sof)-rf_get(cfm.CFM.val,CFM_sol));
      rf_set(cfm.CFM.val,CFM_sol,0);
      rf_set(cfm.CFM.val,CFM_sor,0);
      rf_set(cfm.CFM.val,CFM_rrb,0);
      set_dest_op(ii,dest_cfm,SR,cfm);
    }
    break;
    
  default:
    taken = 0;
    report_actual_intr(ii,IA64_intr_notimplemented);
  }
  
  if (taken) {
    ii->next_pc = tmp_IP;
    ii->branch_dir = 1;
    ii->branch_num_targets = 2;

    if ((rf_get(psr,PSR_it) && IA64_unimplemented_va(tmp_IP)) ||
	(!rf_get(psr,PSR_it) && IA64_unimplemented_pa(tmp_IP))) {
      report_intr(ii,IA64_intr_unimplementedinstructionaddress);
    }
    if (lpt && rf_get(psr,PSR_lp)) 
      report_actual_intr(ii,IA64_intr_lowerprivilegetransfer);
    if (rf_get(psr,PSR_tb))
      report_actual_intr(ii,IA64_intr_takenbranch);

  } /* if (taken) */
  else { /* need to restore this */
    ii->branch_dir = 0;
    ii->next_pc = ii->branch_targets[0];
  }
  
  return;
}

void 
opfetch_B6(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B6 - IP-relative predict */
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t instr = ii->extra.instr;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_op1: /* tag */
    {
      uint64_t tmp_IP;
      tmp_IP = (uint64_t)B6_t2e(instr)<<7 | B6_timm7a(instr);
      if (B6_t2e(instr) & 2) tmp_IP |= ~INT64_C(0x1FF);
      tmp_IP <<= 4;
      tmp_IP += (ii->addr & ~UINT64_C(15));
      set_src_op(ii,src_op1,imm64,tmp_IP);
    }
    if (!fallthrough) break;
  case src_branchoff:
    set_src_op(ii,src_branchoff,BR,ii->branch_targets[1]); 
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_B6(LSE_emu_instr_info_t *ii)
{ /* B6 - IP-relative predict */
  uint64_t tmp_IP;

  tmp_IP = get_src_op(ii,src_branchoff,BR);
  ii->branch_targets[1] = tmp_IP;
  ii->branch_num_targets = 2;
}  

void 
opfetch_B7(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B7 - indirect predict */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_op1: /* tag */
    {
      uint64_t tmp_IP;
      tmp_IP = (uint64_t)B6_t2e(instr)<<7 | B6_timm7a(instr);
      if (B6_t2e(instr) & 2) tmp_IP |= ~INT64_C(0x1FF);
      tmp_IP <<= 4;
      tmp_IP += (ii->addr & ~UINT64_C(15));
      set_src_op(ii,src_op1,imm64,tmp_IP);
    }
    if (!fallthrough) break;
  case src_branchoff:
    set_src_op(ii,src_branchoff,BR,get_b(realct,ii,B7_b2(instr)));
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_B7(LSE_emu_instr_info_t *ii)
{ /* B7 - indirect predict */
  uint64_t tmp_IP;

  tmp_IP = get_src_op(ii,src_branchoff,BR);
  ii->branch_targets[1] = tmp_IP;
  ii->branch_num_targets = 2;
}

void 
writeback_B1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B1 - IP-relative branch */
  uint64_t instr = ii->extra.instr;

  if (intr_cancels_instr(ii) || B1_btype(instr)==0) return;
  switch (opname) {
  case dest_result:
    set_p(dest_result,ii,63,get_dest_op(ii,dest_result,PR));
    if (!fallthrough) break;
  case dest_ec:
    set_ar(dest_ec,ii,IA64_ADDR_EC,get_dest_op(ii,dest_ec,AR));
    if (!fallthrough) break;
  case dest_cfm:
    set_sr_bits(dest_cfm,ii,IA64_ADDR_CFM,
		get_dest_op(ii,dest_cfm,SR.CFM.val),
		rf_mask(CFM_rrb));
    if (!fallthrough) break;
  default: break;
  }
}
  
void 
writeback_B2(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B2 - IP-relative counted branch */
  uint64_t instr = ii->extra.instr;

  if (intr_cancels_instr(ii)) return;
  switch (opname) {
  case dest_istrue:
    if (B2_btype(instr)>5) { /* cexit/ctop */
      set_p(dest_istrue,ii,63,get_dest_op(ii,dest_istrue,PR));
    }
    if (!fallthrough) break;
  case dest_lc:
    set_ar(dest_lc,ii,IA64_ADDR_LC,get_dest_op(ii,dest_lc,AR));
    if (!fallthrough) break;
  case dest_ec:
    if (B2_btype(instr)>5) { /* cexit/ctop */
      set_ar(dest_ec,ii,IA64_ADDR_EC,get_dest_op(ii,dest_ec,AR));
    }
    if (!fallthrough) break;
  case dest_cfm:
    if (B2_btype(instr)>5) { /* cexit/ctop */
      set_sr_bits(dest_cfm,ii,IA64_ADDR_CFM,
		  get_dest_op(ii,dest_cfm,SR.CFM.val),
		  rf_mask(CFM_rrb));
    }
    if (!fallthrough) break;
  default: break;
  }
}
  
void 
writeback_B3(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B3 - IP-relative call */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case dest_result:
    set_b(dest_result,ii,B3_b1(instr),get_dest_op(ii,dest_result,BR));
    if (!fallthrough) break;
  case dest_bsp:
    set_ar(dest_bsp,ii,IA64_ADDR_BSP,get_dest_op(ii,dest_bsp,AR));
    if (!fallthrough) break;
  case dest_pfs:
    set_ar(dest_pfs,ii,IA64_ADDR_PFS,get_dest_op(ii,dest_pfs,AR));
    if (!fallthrough) break;
  case dest_cfm:
    set_sr(dest_cfm,ii,IA64_ADDR_CFM,get_dest_op(ii,dest_cfm,SR));
    if (!fallthrough) break;
  case dest_rse:
    save_rse(dest_rse,ii);
    realct->RSE_other.NDirty = get_dest_op(ii,dest_rse,RSE_other.NDirty);
    realct->RSE_other.NClean = get_dest_op(ii,dest_rse,RSE_other.NClean);
    if (!fallthrough) break;
  default: break;
  }
}

void 
writeback_B4(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B4 - indirect branch */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t psr,psrnew;
  IA64_AR_t pfs;

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case dest_rsemem:
    if (B4_btype(instr)==4) { /* ret */
      if (get_dest_op(ii,dest_rsemem,rsechange.numwords)) { /* do a fill */

	uint64_t loadp = get_dest_op(ii,dest_bsp,AR);
	int numtodo = -get_dest_op(ii,dest_rsemem,rsechange.numwords);
	int numregtodo = -get_dest_op(ii,dest_rsemem,rsechange.numregs);
	uint64_t rnat = get_ar(realct,ii,IA64_ADDR_RNAT);
	int bitnum;
	uint64_t len = 8;
	uint64_t mask;
	LSE_device::deverror_code_t memerr = LSE_device::deverror_none;
	unsigned int rno;

	/* this uses the current rnat */

	rno = get_dest_op(ii,dest_cfm,SR.CFM.BOF) + numregtodo;
	rno = IA64_GR_FAST_ROT(rno,realct->num_stacked_phys);

	loadp += 8*numtodo;
	bitnum = (loadp>>3) & 0x3f;
	mask = INT64_C(1) << bitnum;

#ifdef DEBUG_RSE
	if (realct->di->IA64_rsedebugprint) {
	  fprintf(stderr,
		  "RSE: filling %d registers (instr @ 0x%016"PRIX64")\n",
		  numtodo, ii->addr);
	}
#endif 

	while (numtodo) {

	  loadp -= 8; /* pre-decrement */
	  bitnum--;
	  numtodo --;
	  mask >>= 1;

	  if (bitnum == -1) { /* Get RNAT */

	    /* TODO: endianness 
	       jc1: fixed?
	    */
	    try { 
	      realct->mem->read(loadp, (unsigned char *)&rnat, len);
	    } catch (LSE_device::deverror_t e) { 
	      memerr = e.err;
	    }

	    rnat = LSE_end_le2h_ll(rnat);

	    if (memerr) {
	      /* TODO: set up fault state correctly */
	      report_intr(ii,IA64_intr_memory);
	      break; /* do not fall-through on error */
	    }
	    /* load here */
#ifdef DEBUG_RSE
	    if (realct->di->IA64_rsedebugprint) {
	      fprintf(stderr,
		      "RSE: filling RNAT %016"PRIX64" from %016"PRIX64"\n", 
		      rnat,loadp);
	    }
#endif
	    bitnum = 63;
	    mask = INT64_C(1) << 63;
	    continue;
	  }

	  rno = IA64_GR_FAST_ROT(rno - 1 + realct->num_stacked_phys,
				 realct->num_stacked_phys);

	  /* TODO: endianness */
	  try {
	    realct->mem->read(loadp,
			      (unsigned char *)&realct->physr[rno].val, len);
	  } catch (LSE_device::deverror_t e) { 
	    memerr = e.err; 
	  }

	  realct->physr[rno].val = LSE_end_le2h_ll(realct->physr[rno].val);

	  if (memerr) {
	    /* TODO: set up fault state correctly */
	    report_intr(ii,IA64_intr_memory);
	    break; /* do not fall-through on error */
	  }
	  realct->physr[rno].nat = (mask & rnat) ? 1:0;

#ifdef DEBUG_RSE
	  if (realct->di->IA64_rsedebugprint) {
	    fprintf(stderr,
		    "RSE: filling %d %016"PRIX64" %c from %016"PRIX64"\n",
		    rno+48,
		    realct->physr[rno].val,realct->physr[rno].nat?
		    'N':'-',loadp);
	  }
#endif

	} /* while(numtodo) */

	/* ugly side effect that cannot be rolled back! */
	set_ar(dest_rsemem,ii,IA64_ADDR_RNAT,rnat);
      }

      /* And this is another ugly side effect */

      if (get_dest_op(ii,dest_bsp,AR)<get_ar(realct,ii,IA64_ADDR_BSPSTORE)) {
	set_ar(dest_rsemem,ii,IA64_ADDR_BSPSTORE,
	       get_dest_op(ii,dest_bsp,AR));
#ifdef DEBUG_RSE
	if (realct->di->IA64_rsedebugprint) {
	  fprintf(stderr,"RSE: BSPSTORE now %016"PRIX64"\n",
		  get_ar(realct,ii,IA64_ADDR_BSPSTORE));
	}
#endif 
      }
    }
    if (!fallthrough) break;
  case dest_bsp:
    if (B4_btype(instr)==4) { /* ret */
      set_ar(dest_bsp,ii,IA64_ADDR_BSP,get_dest_op(ii,dest_bsp,AR));
    }
    if (!fallthrough) break;
#ifdef LATER
  case dest_alatupdate:
    /* TODO: haven't figured this out yet -- only on RSE wraparound */
    if (!fallthrough) break;
#endif
  case dest_ec:
    if (B4_btype(instr)==4) { /* ret */
      set_ar(dest_ec,ii,IA64_ADDR_EC,get_dest_op(ii,dest_ec,AR));
    }
    if (!fallthrough) break;
  case dest_cfm:
    if (B4_btype(instr)==4) { /* ret */
      set_sr(dest_cfm,ii,IA64_ADDR_CFM,get_dest_op(ii,dest_cfm,SR));
    }
    if (!fallthrough) break;
  case dest_psr:
    if (B4_btype(instr)==4) { /* ret */
      psr = get_src_op(ii,src_psr,SR.PSR);
      psrnew = get_dest_op(ii,dest_psr,SR.PSR);
      pfs = get_src_op(ii,src_pfs,AR);
      /* must check for enable condition -- oddly, PSR.cpl does not list
       * the return as a reader, though it most certainly does read it.
       * Perhaps this should be done vs. current value, but I don't trust
       * that....
       */
      if (rf_get(psr,PSR_cpl) < rf_get(pfs,PFS_ppl)) {
	rf_set(psr,PSR_cpl,rf_get(get_dest_op(ii,dest_psr,SR.PSR),PSR_cpl));
	set_sr_bits(dest_psr,ii,IA64_ADDR_PSR,psrnew,rf_mask(PSR_cpl));
      }
    }
    if (!fallthrough) break;
  case dest_rse:
    if (B4_btype(instr)==4) { /* ret */
      save_rse(dest_rse,ii);
      realct->RSE_other.NDirty = get_dest_op(ii,dest_rse,RSE_other.NDirty);
      realct->RSE_other.NClean = get_dest_op(ii,dest_rse,RSE_other.NClean);
    }
    if (!fallthrough) break;
  default: break;
  }
}

void 
writeback_B5(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B5 - indirect call */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  if (!get_src_op(ii,src_qp,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case dest_result:
    set_b(dest_result,ii,B5_b1(instr),get_dest_op(ii,dest_result,BR));
    if (!fallthrough) break;
  case dest_bsp:
    set_ar(dest_bsp,ii,IA64_ADDR_BSP,get_dest_op(ii,dest_bsp,AR));
    if (!fallthrough) break;
  case dest_pfs:
    set_ar(dest_pfs,ii,IA64_ADDR_PFS,get_dest_op(ii,dest_pfs,AR));
    if (!fallthrough) break;
  case dest_cfm:
    set_sr(dest_cfm,ii,IA64_ADDR_CFM,get_dest_op(ii,dest_cfm,SR));
    if (!fallthrough) break;
  case dest_rse:
    save_rse(dest_rse,ii);
    realct->RSE_other.NDirty = get_dest_op(ii,dest_rse,RSE_other.NDirty);
    realct->RSE_other.NClean = get_dest_op(ii,dest_rse,RSE_other.NClean);
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_B8(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B8 - miscellaneous B-unit */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_bsp:
    if (B8_x6(instr)==2) {
      set_src_op(ii,src_bsp,AR,get_ar(realct,ii,IA64_ADDR_BSP));
    }
    if (!fallthrough) break;
  case src_pfs:
    set_src_op(ii,src_pfs,AR,get_ar(realct,ii,IA64_ADDR_PFS));
    if (!fallthrough) break;
  case src_rse: /* will be needed for cover eventually */
    if (B8_x6(instr)==2) {
      set_src_op_rse(ii,src_rse,
		     realct->RSE_other.NDirty,realct->RSE_other.NClean);
    }
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_B8(LSE_emu_instr_info_t *ii) 
{ /* B8 - miscellaneous B-unit */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  /* not predicated */
  IA64_SR_t cfm;
  uint64_t psr;
  IA64_AR_t pfs, bsp;
  int ndirty, nclean;
  unsigned int bof;

  switch (B8_x6(instr)) {
    
  case 2 : /* cover */
    psr = get_src_op(ii,src_psr,SR.PSR);
    cfm = get_src_op(ii,src_cfm,SR);
    if (rf_get(psr,PSR_ic) == 0) {
      /* TODO: CR[IFS] stuff.... */
    }
    /* real ALATs have work to do; we don't */
    bof = cfm.CFM.BOF;
    bof = IA64_GR_FAST_ROT(bof + rf_get(cfm.CFM.val,CFM_sof),
			   realct->num_stacked_phys);
    cfm.CFM.BOF = bof;
    
    ndirty = get_src_op(ii,src_rse,RSE_other.NDirty);
    ndirty += rf_get(cfm.CFM.val,CFM_sof);
    nclean = get_src_op(ii,src_rse,RSE_other.NClean);
    
    set_dest_op_rse(ii,dest_rse,ndirty,nclean);
    
    bsp = get_src_op(ii,src_bsp,AR);
    set_dest_op(ii,dest_bsp,AR,
		(bsp&~INT64_C(7)) + 8*(rf_get(cfm.CFM.val,CFM_sof) + 
				(((bsp>>3)&0x3f)+rf_get(cfm.CFM.val,CFM_sof))
				/63));
    
    rf_set(cfm.CFM.val,CFM_sof,0);
    rf_set(cfm.CFM.val,CFM_sol,0);
    rf_set(cfm.CFM.val,CFM_sor,0);
    rf_set(cfm.CFM.val,CFM_rrb,0);
    set_dest_op(ii,dest_cfm,SR,cfm);
    break;

  case 4 : /* clrrb */
    /* check for end of group in decode */
    cfm = get_src_op(ii,src_cfm,SR);
    rf_set(cfm.CFM.val,CFM_rrb,0);
    set_dest_op(ii,dest_cfm,SR,cfm);
    break;
    
  case 5 : /* clrrb.pr */
    /* check for end of group in decode */
    cfm = get_src_op(ii,src_cfm,SR);
    rf_set(cfm.CFM.val,CFM_rrb_pr,0);
    set_dest_op(ii,dest_cfm,SR,cfm);
    break;
    
  case 12 : /* bsw.0 */
    psr = get_src_op(ii,src_psr,SR.PSR);
    if (rf_get(psr,PSR_cpl)!=0) {
      report_actual_intr(ii,IA64_intr_privilegedoperation);
    }
    rf_set(psr,PSR_bn,0);
    set_dest_op(ii,dest_psr,SR.PSR,psr);
    break;

  case 13 : /* bsw.1 */
    psr = get_src_op(ii,src_psr,SR.PSR);
    if (rf_get(psr,PSR_cpl)!=0) {
      report_actual_intr(ii,IA64_intr_privilegedoperation);
    }
    rf_set(psr,PSR_bn,1);
    set_dest_op(ii,dest_psr,SR.PSR,psr);
    break;

  case 16 : /* epc */
    pfs = get_src_op(ii,src_pfs,AR);
    psr = get_src_op(ii,src_psr,SR.PSR);
    if (rf_get(pfs,PFS_ppl) < rf_get(psr,PSR_cpl)) {
      report_actual_intr(ii,IA64_intr_illegaloperation);
    } else {
      if (rf_get(psr,PSR_it)) {
	/* TODO: TLB stuff... */
	report_actual_intr(ii,IA64_intr_notimplemented);
      }
      else rf_set(psr,PSR_cpl,0);
    }
    set_dest_op(ii,dest_psr,SR.PSR,psr);
    break;

  case 8 : /* rfi */
  default:
    report_actual_intr(ii,IA64_intr_notimplemented);
  }
  return;
}

void 
writeback_B8(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B8 - miscellaneous B-unit */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  /* not predicated */
  if (intr_cancels_instr(ii)) return;

  switch (opname) {
  case 0: /* allow fallthrough */
    if (!fallthrough) break;

  case dest_bsp:
    if (B8_x6(instr)==2) { /* cover */
      set_ar(dest_bsp,ii,IA64_ADDR_BSP,get_dest_op(ii,dest_bsp,AR));
    }
    if (!fallthrough) break;

  case dest_cfm:
    switch (B8_x6(instr)) {
    case 2 : /* cover */
      set_sr(dest_cfm,ii,IA64_ADDR_CFM,get_dest_op(ii,dest_cfm,SR));
      break;      
    case 4 : /* clrrb */
      set_sr_bits(dest_cfm,ii,IA64_ADDR_CFM,
		  get_dest_op(ii,dest_cfm,SR.CFM.val),
		  rf_mask(CFM_rrb));
      break;
    case 5 : /* clrrb.pr */
      set_sr_bits(dest_cfm,ii,IA64_ADDR_CFM,
		  get_dest_op(ii,dest_cfm,SR.CFM.val),
		  rf_mask(CFM_rrb_pr));
      break;
    case 8 : /* rfi */
    default:
      break;
    }
    if (!fallthrough) break;

  case dest_psr:
    switch (B8_x6(instr)) {
    case 12 : /* bsw.0 */
    case 13 : /* bsw.1 */
      set_sr_bits(dest_psr,ii,IA64_ADDR_PSR,get_dest_op(ii,dest_psr,SR.PSR),
		  rf_mask(PSR_bn));
      break;

    case 16 : /* epc */
      set_sr_bits(dest_psr,ii,IA64_ADDR_PSR,get_dest_op(ii,dest_psr,SR.PSR),
		  rf_mask(PSR_cpl));
      break;

    case 8 : /* rfi */
    default: break;

    }
    if (!fallthrough) break;

  case dest_rse:
    if (B8_x6(instr)==2) { /* cover */
      save_rse(dest_rse,ii);
      realct->RSE_other.NDirty = get_dest_op(ii,dest_rse,RSE_other.NDirty);
      realct->RSE_other.NClean = get_dest_op(ii,dest_rse,RSE_other.NClean);
    }
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_B9(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* B9 - nop/break */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t temp;
  temp = B9_imm20a(instr) | (B9_i(instr) << 20);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    set_src_op(ii,src_op1,imm64,temp);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_B9(LSE_emu_instr_info_t *ii) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int unit = ii->extra.unit;

  ii->branch_dir = 0;
  if (!get_src_op(ii,src_qp,PR)) return;
  if (all_majorop(instr)) {
    return;
  } else { /* how a break is handled */
    ii->branch_dir = 1;
    ii->branch_num_targets = 2;
    ii->branch_targets[1] = ii->next_pc; /* jump to following */
    if (Linux_call_os(realct,ii,ii->addr, ii->next_pc,unit,
		      get_src_op(ii,src_op1,imm64))) goto wasfault;
  }
  return;
 wasfault:
  return;
}

void 
opfetch_X1(LSE_emu_instr_info_t *ii, 
	   LSE_emu_operand_name_t opname,int fallthrough) 
{ /* X1 - break */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    {
      uint64_t temp;
      temp = (uint64_t)X1_imm20a(instr);
      temp |= (uint64_t)X1_i(instr) << 20;
      temp |= (ii->extra.instrcont << 21);
      set_src_op(ii,src_op1,imm64,temp);
    }
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_X2(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t imm64;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    imm64 = (uint64_t)X2_imm7b(instr);
    imm64 |= ((uint64_t)X2_imm9d(instr) << 7);
    imm64 |= ((uint64_t)X2_imm5c(instr) << 16);
    imm64 |= ((uint64_t)X2_ic(instr) << 21);
    imm64 |= (ii->extra.instrcont << 22);
    imm64 |= ((uint64_t)X2_i(instr) << 63);
    set_src_op(ii,src_op1,imm64,imm64);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_X2(LSE_emu_instr_info_t *ii) {
  uint64_t instr = ii->extra.instr;

  check_target_register(ii,all_r1(instr));
  set_dest_op(ii,dest_result,GR.val,get_src_op(ii,src_op1,imm64));
  set_dest_op(ii,dest_result,GR.nat,0);
  return;
}

/* X3 : same as B1 */

/* X4 : same as B3 */

void 
opfetch_X5(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* X5 - nop/hint */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    {
      uint64_t temp;
      temp = (uint64_t)X5_imm20a(instr);
      temp |= (uint64_t)X5_i(instr) << 20;
      temp |= (ii->extra.instrcont << 21);
      set_src_op(ii,src_op1,imm64,temp);
    }
    if (!fallthrough) break;
  default: break;
  }
}


void 
opfetch_A1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  case src_op3:
    if ((A1_x2b(instr) + A1_x4(instr)) == 1) {
      set_src_op(ii,src_op3,imm64,1);
    }
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_A1(LSE_emu_instr_info_t *ii) {
  uint64_t instr = ii->extra.instr;
  uint64_t tres;

  check_target_register(ii,all_r1(instr));

  switch (A1_x4(instr)) {
  case 0: /* add */
    tres = (get_src_op(ii,src_op1,GR.val) +
	    get_src_op(ii,src_op2,GR.val) +
	    (A1_x2b(instr) ? INT64_C(1) : INT64_C(0)));
    break;
  case 1: /* sub */
    tres = (get_src_op(ii,src_op1,GR.val) -
	    get_src_op(ii,src_op2,GR.val) -
	    (!A1_x2b(instr) ? INT64_C(1) : INT64_C(0)));
    break;
  case 2: /* addp4 */
    {
      uint64_t r3val = get_src_op(ii,src_op2,GR.val);
      tres = ((get_src_op(ii,src_op1,GR.val) + r3val) & INT64_C(0xFFFFFFFF)) |
	((r3val & INT64_C(0xC0000000))<<31);
    }
    break;
  case 3:
    switch (A1_x2b(instr)) {
    case 0: /* and */
      tres = (get_src_op(ii,src_op1,GR.val) &
	      get_src_op(ii,src_op2,GR.val));
      break;
    case 1: /* andcm */
      tres = (get_src_op(ii,src_op1,GR.val) &
	      ~get_src_op(ii,src_op2,GR.val));
      break;
    case 2: /* or */
      tres = (get_src_op(ii,src_op1,GR.val) |
	      get_src_op(ii,src_op2,GR.val));
      break;
    case 3: /* xor */
      tres = (get_src_op(ii,src_op1,GR.val) ^
	      get_src_op(ii,src_op2,GR.val));
      break;
    default:
      tres = 0; /* shut up warnings */
      break;
    }
    break;
  default:
    tres = 0; /* shut up warnings */
    break;
  }
  set_dest_op(ii,dest_result,GR.val,tres);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) || get_src_op(ii,src_op2,GR.nat));
  return;
}

void 
opfetch_A2(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* A2 shladd/shladdp4 */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,(A2_ct2d(instr)+1));
    if (!fallthrough) break;
  case src_op3:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op3,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_A2(LSE_emu_instr_info_t *ii)
{ /* A2 shladd/shladdp4 */
  uint64_t instr = ii->extra.instr;
  uint64_t tres, r3val, r2val;

  check_target_register(ii,all_r1(instr));

  r3val = get_src_op(ii,src_op3,GR.val);
  r2val = get_src_op(ii,src_op1,GR.val);

  if (A2_x4(instr) & 2) { /* shladdp4 */
    tres = (r2val << get_src_op(ii,src_op2,imm64)) + r3val;
    tres &= INT64_C(0xFFFFFFFF);
    tres |= (r3val & INT64_C(0xC0000000))<<31;
  }
  else { /* shladd */
    tres = (r2val << get_src_op(ii,src_op2,imm64)) + r3val;
  }
  set_dest_op(ii,dest_result,GR.val,tres);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) || get_src_op(ii,src_op3,GR.nat));
  return;
}

void 
opfetch_A3(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* A3 Integer ALU - Immediate8-Register */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int64_t imm8;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    imm8 = A3_imm7b(instr);
    if (A3_s(instr)) imm8 |= ~(0x7f);
    set_src_op(ii,src_op1,imm64,imm8);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}


void 
evaluate_A3(LSE_emu_instr_info_t *ii) 
{ /* A3 Integer ALU - Immediate8-Register */
  uint64_t instr = ii->extra.instr;
  uint64_t newval, imm8, r3val;
  char r3nat;
  
  check_target_register(ii,all_r1(instr));

  r3val = get_src_op(ii,src_op2,GR.val);
  r3nat = get_src_op(ii,src_op2,GR.nat);
  imm8 = get_src_op(ii,src_op1,imm64);
  if (A3_x4(instr) == 9) { /* sub */
    newval = imm8 - r3val;
  } else {
    switch (A3_x2b(instr)) {
    case 0: /* and */
      newval = imm8 & r3val;
      break;
    case 1 : /* andcm */
      newval = imm8 & ~r3val;
      break;
    case 2 : /* or */
      newval = imm8 | r3val;
      break;
    case 3 : /* xor */
      newval = imm8 ^ r3val;
      break;
    default:
      newval = 0; /* shut up warnings */
      break;
    }
  }
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,r3nat);
  return;
}

void 
opfetch_A4(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,
	   int fallthrough) 
{ /* A4 - Add Immediate14 */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int64_t imm14;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp :
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    imm14 = A4_imm7b(instr) | (A4_imm6d(instr)<<7);
    if (A4_s(instr)) imm14 |= ~(0x1FFF);
    set_src_op(ii,src_op1,imm64,imm14);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_A4(LSE_emu_instr_info_t *ii) 
{ /* A4 - Add Immediate14 */
  uint64_t instr = ii->extra.instr;
  int64_t imm14 = (int64_t)get_src_op(ii,src_op1,imm64);
  uint64_t tres;

  check_target_register(ii,all_r1(instr));

  if (A4_x2a(instr)==2) { /* adds */
    tres = imm14 + get_src_op(ii,src_op2,GR.val);
  } else { /* addp4 */
    uint64_t r3val = get_src_op(ii,src_op2,GR.val);
    tres = ((imm14 + r3val) & INT64_C(0xFFFFFFFF)) |
      ((r3val & INT64_C(0xC0000000))<<31);
  }
  set_dest_op(ii,dest_result,GR.val,tres);
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op2,GR.nat));
  return;
}

void
allbackend_A4(LSE_emu_instr_info_t *ii) {
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t instr = ii->extra.instr;
  int64_t imm14;
  IA64_GR_t *op3val;
  uint64_t tres;

  ALLBACK_PREFIX; /* CFM and PSR */

  if (!get_p(realct,ii,all_qp(instr))) return;
  
  check_target_register(ii,all_r1(instr));
  if (intr_cancels_instr(ii)) return;

  imm14 = A4_imm7b(instr) | (A4_imm6d(instr)<<7);
  if (A4_s(instr)) imm14 |= ~(0x1FFF);

  op3val = get_r_ptr(realct,ii,all_r3(instr));
  if (A4_x2a(instr)==2) { /* adds */
    tres = imm14 + op3val->val;
  } else {
    uint64_t r3val = op3val->val;
    tres = ( ((imm14 + r3val) & INT64_C(0xFFFFFFFF)) |
	     ((r3val & INT64_C(0xC0000000))<<31) );
  }
  
  set_r_noroll(ii,all_r1(instr),tres,op3val->nat);
}

void 
opfetch_A5(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int64_t imm22;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    imm22 = A5_imm7b(instr);
    imm22 |= (A5_imm9d(instr) << 7);
    imm22 |= (A5_imm5c(instr) << 16);
    if (A5_s(instr)) imm22 |= ~(0x1FFFFF);
    set_src_op(ii,src_op1,imm64,imm22);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,A5_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void 
evaluate_A5(LSE_emu_instr_info_t *ii) {
  uint64_t instr = ii->extra.instr;
  uint64_t tres;

  check_target_register(ii,all_r1(instr));

  tres = (get_src_op(ii,src_op1,GR.val) + get_src_op(ii,src_op2,GR.val));
  set_dest_op(ii,dest_result,GR.val,tres);
  set_dest_op(ii,dest_result,GR.nat,get_src_op(ii,src_op2,GR.nat));
  return;
}

void 
opfetch_A6(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* A6 integer compare - register-register */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_A7(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* A7 integer compare to zero */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void 
opfetch_A8(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* A8 integer compare immediate-register */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  uint64_t tmp_src;
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    tmp_src = A8_imm7b(instr);
    if (A8_s(instr)) tmp_src |= ~(INT64_C(0x7f));
    set_src_op(ii,src_op1,imm64,tmp_src);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

static inline int
do_int_cmp(int comptypeind2, 
	   uint64_t tmp_src, uint64_t r3val, int qp, int tmp_nat,
	   char *p1, char *p2) {

  static int relations[]= {
    2, 6, 0, -1, /* lt, ltu, eq */
    2, 6, 0, -1, 
    0, 0, 0, -1, /* eq, eq, eq */
    1, 1, 1, -1, /* ne, ne, ne */
    
    4, 4, 4, -1, /* gt, gt, gt */
    3, 3, 3, -1, /* le, le, le */
    5, 5, 5, -1, /* ge, ge, ge */
    2, 2, 2, -1, /* lt, lt, lt */
  };
  static int comptype[]= {
    0, 0, 0, 0, /* default */
    4, 4, 4, 4, /* unc */
    1, 2, 3, 0, /* and, or, or.andcm */
    1, 2, 3, 0,
    
    1, 2, 3, 0,
    1, 2, 3, 0,
    1, 2, 3, 0,
    1, 2, 3, 0,
  };
  int comptypeind = comptypeind2 & 0x1f;
  boolean isp4 = comptypeind2 & 0x20;
  int istrue;

  if (!qp) {
    if (comptype[comptypeind] == 4) {
      *p1 = *p2 = 0;
      return 1;
    }
  }

  qp = !!qp; /* force to single bit */
  tmp_nat = !!tmp_nat; /* force to single bit */

  switch (relations[comptypeind]) {
  case 0 : /* eq */
    istrue = isp4 ? 
      ((uint32_t)tmp_src == (uint32_t)r3val) :
	(tmp_src == r3val);
    break;
  case 1 : /* ne */
    istrue = isp4 ? 
      ((uint32_t)tmp_src != (uint32_t)r3val) :
	(tmp_src != r3val);
    break;
  case 2 : /* lt */
    istrue = isp4 ? 
      ((int32_t)tmp_src < (int32_t)r3val) :
	((int64_t) tmp_src < (int64_t)r3val);
    break;
  case 3 : /* le */
    istrue = isp4 ? 
      ((int32_t)tmp_src <= (int32_t)r3val) :
	((int64_t)tmp_src <= (int64_t)r3val);
    break;
  case 4 : /* gt */
    istrue = isp4 ? 
      ((int32_t)tmp_src > (int32_t)r3val) :
	((int64_t)tmp_src > (int64_t)r3val);
    break;
  case 5 : /* ge */ 
    istrue = isp4 ? 
      ((int32_t)tmp_src >= (int32_t)r3val) :
	((int64_t)tmp_src >= (int64_t)r3val);
    break;
  default : /* ltu */
    istrue = isp4 ? 
      ((uint32_t)tmp_src < (uint32_t)r3val) :
	((uint64_t)tmp_src < (uint64_t)r3val);
    break;
  }

  switch (comptype[comptypeind]) {
  case 1:  /* and */
    *p1 = *p2 = 0;
    return qp & (tmp_nat | ~istrue);
    break;
  case 2:  /* or */
    *p1 = *p2 = 1;
    return (qp & (~tmp_nat & istrue));
    break;
  case 3:  /* or.andcm */
    *p1 = 1;
    *p2 = 0;
    return (qp & (~tmp_nat & istrue));
    break;
  case 4:  /* unc */
    *p1 = ~tmp_nat & istrue & qp;
    *p2 = ~tmp_nat & ~istrue & qp;
    return 1;
    /* intentionally runs through */
  default:
    *p1 = ~tmp_nat & istrue & 1;
    *p2 = ~tmp_nat & ~istrue & 1;
    return qp;
  }
  return 0;
}

void
evaluate_A_int_cmp(LSE_emu_instr_info_t *ii) { /* A6, A7, A8 */
  uint64_t instr = ii->extra.instr;
  int format = ii->extra.formatno;

  int comptypeind = ((all_majorop(instr) &3)) |
    (A6_c(instr)<<2) |
    (A6_ta(instr)<<3) |
    (((A6_x2(instr) & 2) || !A6_tb(instr)) ? 0 : 16) |
    ( (A6_x2(instr) & 1) ? 0x20 : 0);
  int tmp_nat;
  uint64_t tmp_src,r3val;
  boolean updated;
  char p1, p2, qp;
  
  qp = get_src_op(ii,src_qp,PR);
  if (!qp) {
    if ((comptypeind & 0x1c) == 4) {
      if (all_p1(instr) == all_p2(instr)) 
	report_actual_intr(ii,IA64_intr_illegaloperation);
    } else if (all_p1(instr) == all_p2(instr)) 
      report_potential_intr(ii,IA64_intr_illegaloperation);
  } else 
    if (all_p1(instr) == all_p2(instr)) 
      report_actual_intr(ii,IA64_intr_illegaloperation);
  
  tmp_nat = (( (format==506)? get_src_op(ii,src_op1,GR.nat):0) ||
	     get_src_op(ii,src_op2,GR.nat));
  
  if (format==506) tmp_src = get_src_op(ii,src_op1,GR.val);
  else if (format == 507) tmp_src = 0;
  else tmp_src = get_src_op(ii,src_op1,imm64);
  
  r3val = get_src_op(ii,src_op2,GR.val);
  
  updated = do_int_cmp(comptypeind, tmp_src, r3val, qp,
		       tmp_nat, &p1, &p2);

  set_int_op(ii,int_updatepred,PR,updated);
  set_dest_op(ii,dest_istrue,PR,p1);
  set_dest_op(ii,dest_isfalse,PR,p2);
  return;
}

void 
allbackend_A6(LSE_emu_instr_info_t *ii) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int comptypeind = ((all_majorop(instr) &3)) |
    (A6_c(instr)<<2) |
    (A6_ta(instr)<<3) |
    (((A6_x2(instr) & 2) || !A6_tb(instr)) ? 0 : 16) |
    ( (A6_x2(instr) & 1) ? 0x20 : 0);
  boolean updated;
  char p1, p2, qp;
  IA64_GR_t *r3, *r2;

  ALLBACK_PREFIX; /* CFM and PSR */

  qp = get_p(realct,ii,all_qp(instr));

  if (!qp) {
    if ((comptypeind & 0x1c) == 4) {
      if (all_p1(instr) == all_p2(instr)) 
	report_actual_intr(ii,IA64_intr_illegaloperation);
    } else if (all_p1(instr) == all_p2(instr)) 
      report_potential_intr(ii,IA64_intr_illegaloperation);
  } else 
    if (all_p1(instr) == all_p2(instr)) 
      report_actual_intr(ii,IA64_intr_illegaloperation);
  
  r3 = get_r_ptr(realct,ii,all_r3(instr));
  r2 = get_r_ptr(realct,ii,all_r2(instr));

  updated = do_int_cmp(comptypeind, r2->val, r3->val, qp, 
		       r3->nat || r2->nat, &p1, &p2);

  if (updated && !intr_cancels_instr(ii)) {
    set_p_noroll(ii,all_p1(instr),p1);
    set_p_noroll(ii,all_p2(instr),p2);
  }
  
  return;
}

void 
allbackend_A7(LSE_emu_instr_info_t *ii) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int comptypeind = ((all_majorop(instr) &3)) |
    (A6_c(instr)<<2) |
    (A6_ta(instr)<<3) |
    (((A6_x2(instr) & 2) || !A6_tb(instr)) ? 0 : 16) |
    ( (A6_x2(instr) & 1) ? 0x20 : 0);
  uint64_t tmp_src;
  boolean updated;
  char p1, p2, qp;
  IA64_GR_t *r3;

  ALLBACK_PREFIX; /* CFM and PSR */

  qp = get_p(realct,ii,all_qp(instr));

  if (!qp) {
    if ((comptypeind & 0x1c) == 4) {
      if (all_p1(instr) == all_p2(instr)) 
	report_actual_intr(ii,IA64_intr_illegaloperation);
    } else if (all_p1(instr) == all_p2(instr)) 
      report_potential_intr(ii,IA64_intr_illegaloperation);
  } else 
    if (all_p1(instr) == all_p2(instr)) 
      report_actual_intr(ii,IA64_intr_illegaloperation);
  
  r3 = get_r_ptr(realct,ii,all_r3(instr));
  tmp_src = A8_imm7b(instr);
  if (A8_s(instr)) tmp_src |= ~(INT64_C(0x7f));

  updated = do_int_cmp(comptypeind, 0, r3->val, qp, 
		       r3->nat, &p1, &p2);

  if (updated && !intr_cancels_instr(ii)) {
    set_p_noroll(ii,all_p1(instr),p1);
    set_p_noroll(ii,all_p2(instr),p2);
  }
  
  return;
}

void 
allbackend_A8(LSE_emu_instr_info_t *ii) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int comptypeind = ((all_majorop(instr) &3)) |
    (A6_c(instr)<<2) |
    (A6_ta(instr)<<3) |
    (((A6_x2(instr) & 2) || !A6_tb(instr)) ? 0 : 16) |
    ( (A6_x2(instr) & 1) ? 0x20 : 0);
  uint64_t tmp_src;
  boolean updated;
  char p1, p2, qp;
  IA64_GR_t *r3;

  ALLBACK_PREFIX; /* CFM and PSR */

  qp = get_p(realct,ii,all_qp(instr));

  if (!qp) {
    if ((comptypeind & 0x1c) == 4) {
      if (all_p1(instr) == all_p2(instr)) 
	report_actual_intr(ii,IA64_intr_illegaloperation);
    } else if (all_p1(instr) == all_p2(instr)) 
      report_potential_intr(ii,IA64_intr_illegaloperation);
  } else 
    if (all_p1(instr) == all_p2(instr)) 
      report_actual_intr(ii,IA64_intr_illegaloperation);
  
  r3 = get_r_ptr(realct,ii,all_r3(instr));
  tmp_src = A8_imm7b(instr);
  if (A8_s(instr)) tmp_src |= ~(INT64_C(0x7f));

  updated = do_int_cmp(comptypeind, tmp_src, r3->val, qp, 
		       r3->nat, &p1, &p2);

  if (updated && !intr_cancels_instr(ii)) {
    set_p_noroll(ii,all_p1(instr),p1);
    set_p_noroll(ii,all_p2(instr),p2);
  }
  
  return;
}

void writeback_preds(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) {
  uint64_t instr = ii->extra.instr;
  
  if (!get_int_op(ii,int_updatepred,PR) || intr_cancels_instr(ii)) return;

  switch (opname) {
  case dest_istrue:
    set_p(dest_istrue,ii,all_p1(instr),get_dest_op(ii,dest_istrue,PR));
    if (!fallthrough) break;
  case dest_isfalse:
    set_p(dest_isfalse,ii,all_p2(instr),get_dest_op(ii,dest_isfalse,PR));
    if (!fallthrough) break;
  default: break;
  }
}


void 
opfetch_A9(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) 
{ /* A9 - multimedia ALU */
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op2,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_A9(LSE_emu_instr_info_t *ii) 
{ /* A9 - multimedia ALU */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val, r3val, newval;
  int i;
  int size = (1<<(A9_za(instr)*2+A9_zb(instr)));

  check_target_register(ii,all_r1(instr));
  
  r2val = get_src_op(ii,src_op1,GR.val);
  r3val = get_src_op(ii,src_op2,GR.val);

  switch (A9_x4(instr)) {

  case 0: /* padd */

    if (size == 1) {

      switch (A9_x2b(instr)) {

      case 0 : /* modulo form */
	newval = 0;
	for (i=0;i<8;i++) {
	  uint8_t tmp;
	  tmp = (r2val & 0xff) + (r3val & 0xff);
	  newval |= ((uint64_t)tmp)<<(i*8);
	  r2val >>= 8;
	  r3val >>= 8;
	}
	break;

      case 1: /* sss form */
	newval = 0;
	for (i=0;i<8;i++) {
	  int16_t tmp;
	  tmp = ((r2val & 0xff) + (r3val & 0xff) 
		 + ((r3val & 0x80) ? ~0xff : 0) 
		 + ((r2val & 0x80) ? ~0xff : 0));
	  if (tmp > 127) tmp = 127;
	  else if (tmp < -128) tmp = -128;
	  newval |= ((uint64_t)tmp & 0xff)<<(i*8);
	  r2val >>= 8;
	  r3val >>= 8;
	}
	break;

      case 2: /* uuu form */
	newval = 0;
	for (i=0;i<8;i++) {
	  uint16_t tmp;
	  tmp = (r2val & 0xff) + (r3val & 0xff);
	  if (tmp > 0xff) tmp = 0xff;
	  newval |= ((uint64_t)tmp)<<(i*8);
	  r2val >>= 8;
	  r3val >>= 8;
	}
	break;

      case 3: /* uus form */
	newval = 0;
	for (i=0;i<8;i++) {
	  uint16_t tmp;
	  tmp = ((r2val & 0xff) + (r3val & 0xff) 
		 + ((r3val & 0x80) ? ~0xff : 0));
	  if (tmp > 0xff) tmp = 0xff;
	  newval |= ((uint64_t)tmp)<<(i*8);
	  r2val >>= 8;
	  r3val >>= 8;
	}
	break;

      default:
	newval = 0; /* make compiler happy */
      }

    } else if (size == 2) {

      switch (A9_x2b(instr)) {

      case 0 : /* modulo form */
	newval = 0;
	for (i=0;i<4;i++) {
	  uint16_t tmp;
	  tmp = (r2val + r3val) & 0xffff;
	  newval |= ((uint64_t)tmp)<<(i*16);
	  r2val >>= 16;
	  r3val >>= 16;
	}
	break;

      case 1: /* sss form */
	newval = 0;
	for (i=0;i<4;i++) {
	  int32_t tmp;
	  tmp = ((r2val & 0xffff) + (r3val & 0xffff) 
		 + ((r3val & 0x8000) ? ~0xffff : 0) 
		 + ((r2val & 0x8000) ? ~0xffff : 0));
	  if (tmp > 32767) tmp = 32767;
	  else if (tmp < -32768) tmp = -32768;
	  newval |= ((uint64_t)tmp & 0xffff)<<(i*16);
	  r2val >>= 16;
	  r3val >>= 16;
	}
	break;

      case 2: /* uuu form */
	newval = 0;
	for (i=0;i<4;i++) {
	  uint32_t tmp;
	  tmp = (r2val & 0xffff) + (r3val & 0xffff);
	  if (tmp > 0xffff) tmp = 0xffff;
	  newval |= ((uint64_t)tmp)<<(i*16);
	  r2val >>= 16;
	  r3val >>= 16;
	}
	break;

      case 3: /* uus form */
	newval = 0;
	for (i=0;i<4;i++) {
	  uint32_t tmp;
	  tmp = ((r2val & 0xffff) + (r3val & 0xffff) 
		 + ((r3val & 0x8000) ? ~0xffff : 0));
	  if (tmp > 0xffff) tmp = 0xffff;
	  newval |= ((uint64_t)tmp)<<(i*16);
	  r2val >>= 16;
	  r3val >>= 16;
	}
	break;

      default:
	newval = 0; /* make compiler happy */
      }

    } else {
      newval = (((r2val + r3val) & UINT64_C(0xffffffff)) |
		((((r2val>>32) + (r3val>>32)) << 32) & UINT64_C(0xFfffFfff00000000)));
    }
    break;

  case 1 : /* psub */

    if (size == 1) {

      switch (A9_x2b(instr)) {

      case 0 : /* modulo form */
	newval = 0;
	for (i=0;i<8;i++) {
	  uint8_t tmp;
	  tmp = (r2val & 0xff) - (r3val & 0xff);
	  newval |= ((uint64_t)tmp)<<(i*8);
	  r2val >>= 8;
	  r3val >>= 8;
	}
	break;

      case 1: /* sss form */
	newval = 0;
	for (i=0;i<8;i++) {
	  int16_t tmp;
	  tmp = ((r2val & 0xff) - (r3val & 0xff) 
		 - ((r3val & 0x80) ? ~0xff : 0) 
		 + ((r2val & 0x80) ? ~0xff : 0));
	  if (tmp > 127) tmp = 127;
	  else if (tmp < -128) tmp = -128;
	  newval |= ((uint64_t)tmp & 0xff)<<(i*8);
	  r2val >>= 8;
	  r3val >>= 8;
	}
	break;

      case 2: /* uuu form */
	newval = 0;
	for (i=0;i<8;i++) {
	  uint16_t tmp;
	  tmp = (r2val & 0xff) - (r3val & 0xff);
	  if (tmp > 0xff) tmp = 0xff;
	  newval |= ((uint64_t)tmp)<<(i*8);
	  r2val >>= 8;
	  r3val >>= 8;
	}
	break;

      case 3: /* uus form */
	newval = 0;
	for (i=0;i<8;i++) {
	  uint16_t tmp;
	  tmp = ((r2val & 0xff) - (r3val & 0xff) 
		 - ((r3val & 0x80) ? ~0xff : 0));
	  if (tmp > 0xff) tmp = 0xff;
	  newval |= ((uint64_t)tmp)<<(i*8);
	  r2val >>= 8;
	  r3val >>= 8;
	}
	break;

      default:
	newval = 0; /* make compiler happy */
      }

    } else if (size == 2) {

      switch (A9_x2b(instr)) {

      case 0 : /* modulo form */
	newval = 0;
	for (i=0;i<4;i++) {
	  uint16_t tmp;
	  tmp = (r2val - r3val) & 0xffff;
	  newval |= ((uint64_t)tmp)<<(i*16);
	  r2val >>= 16;
	  r3val >>= 16;
	}
	break;

      case 1: /* sss form */
	newval = 0;
	for (i=0;i<4;i++) {
	  int32_t tmp;
	  tmp = ((r2val & 0xffff) - (r3val & 0xffff) 
		 - ((r3val & 0x8000) ? ~0xffff : 0) 
		 + ((r2val & 0x8000) ? ~0xffff : 0));
	  if (tmp > 32767) tmp = 32767;
	  else if (tmp < -32768) tmp = -32768;
	  newval |= ((uint64_t)tmp & 0xffff)<<(i*16);
	  r2val >>= 16;
	  r3val >>= 16;
	}
	break;

      case 2: /* uuu form */
	newval = 0;
	for (i=0;i<4;i++) {
	  uint32_t tmp;
	  tmp = (r2val & 0xffff) - (r3val & 0xffff);
	  if (tmp > 0xffff) tmp = 0xffff;
	  newval |= ((uint64_t)tmp)<<(i*16);
	  r2val >>= 16;
	  r3val >>= 16;
	}
	break;

      case 3: /* uus form */
	newval = 0;
	for (i=0;i<4;i++) {
	  uint32_t tmp;
	  tmp = ((r2val & 0xffff) - (r3val & 0xffff) 
		 - ((r3val & 0x8000) ? ~0xffff : 0));
	  if (tmp > 0xffff) tmp = 0xffff;
	  newval |= ((uint64_t)tmp)<<(i*16);
	  r2val >>= 16;
	  r3val >>= 16;
	}
	break;

      default:
	newval = 0; /* make compiler happy */
      }

    } else {
      newval = (((r2val + r3val) & UINT64_C(0xffffffff)) |
		((((r2val>>32) + (r3val>>32)) << 32) & UINT64_C(0xFfffFfff00000000)));
    }
    break;

  case 2 : /* pavg */
    if (size==1) {
      int razform = A9_x2b(instr)&1;
      newval = 0;
      for (i=0;i<8;i++) {
	uint16_t tmp;
	if (razform) {
	  tmp = (r2val & 0xff) + (r3val & 0xff) + 1;
	  tmp = (tmp >> 1) & 0xff;
	} else {
	  tmp = (r2val & 0xff) + (r3val & 0xff);
	  tmp = ((tmp >> 1) & 0xff) | (tmp & 1);
	}
	newval |= tmp << (8*i);
	r2val >>= 8;
	r3val >>= 8;
      }
    } else {
      int razform = A9_x2b(instr)&1;
      newval = 0;
      for (i=0;i<4;i++) {
	uint32_t tmp;
	if (razform) {
	  tmp = (r2val & 0xffff) + (r3val & 0xffff) + 1;
	  tmp = (tmp >> 1) & 0xffff;
	} else {
	  tmp = (r2val & 0xffff) + (r3val & 0xffff);
	  tmp = ((tmp >> 1) & 0xffff) | (tmp & 1);
	}
	newval |= tmp << (16*i);
	r2val >>= 16;
	r3val >>= 16;
      }
    }
    break;

  case 3 : /* pavgsub */
    if (size==1) {
      newval = 0;
      for (i=0;i<8;i++) {
	uint16_t tmp;
	tmp = (r2val & 0xff) - (r3val & 0xff);
	tmp = ((tmp >> 1) & 0xff ) | (tmp & 1);
	newval |= tmp << (8*i);
	r2val >>= 8;
	r3val >>= 8;
      }
    } else {
      newval = 0;
      for (i=0;i<4;i++) {
	uint32_t tmp;
	tmp = (r2val & 0xffff) - (r3val & 0xffff);
	tmp = ((tmp >> 1) & 0xffff) | (tmp & 1);
	newval |= tmp << (16*i);
	r2val >>= 16;
	r3val >>= 16;
      }
    }
    break;

  case 9: /* pcmp */
    if (size==1) {
      newval = 0;
      for (i=0;i<8;i++) {
	int tmp;
	tmp = ( (r2val & 0xff) - (r3val & 0xff)
		 - ((r3val & 0x80) ? ~0xff : 0) 
		 + ((r2val & 0x80) ? ~0xff : 0));
	if (A9_x2b(instr)) {
	  if (tmp > 0) newval |= UINT64_C(0xff) << (8*i);
	} else {
	  if (!tmp) newval |= UINT64_C(0xff) << (8*i);
	}
	r2val >>= 8;
	r3val >>= 8;
      }
    } else if (size==2) {
      newval = 0;
      for (i=0;i<4;i++) {
	int tmp;
	tmp = ( (r2val & 0xffff) - (r3val & 0xffff)
		 - ((r3val & 0x8000) ? ~0xffff : 0) 
		 + ((r2val & 0x8000) ? ~0xffff : 0));
	if (A9_x2b(instr)) {
	  if (tmp > 0) newval |= UINT64_C(0xffff) << (16*i);
	} else {
	  if (!tmp) newval |= UINT64_C(0xffff) << (16*i);
	}
	r2val >>= 16;
	r3val >>= 16;
      }
    } else {
      newval = 0;
      for (i=0;i<2;i++) {
	int64_t tmp;
	tmp = ( (r2val & 0xffff) - (r3val & UINT64_C(0xffffffff))
		 - ((r3val & 0x8000) ? ~UINT64_C(0xffffffff) : 0) 
		 + ((r2val & 0x8000) ? ~UINT64_C(0xffffffff) : 0));
	if (A9_x2b(instr)) {
	  if (tmp > 0) newval |= UINT64_C(0xffffffff) << (32*i);
	} else {
	  if (!tmp) newval |= UINT64_C(0xffffffff) << (32*i);
	}
	r2val >>= 32;
	r3val >>= 32;
      }
    }
    break;

  default:
    newval = 0; /* make compiler happy */
  }
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) | get_src_op(ii,src_op3,GR.nat));
  return;
}

void 
opfetch_A10(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,int fallthrough) {
  uint64_t instr = ii->extra.instr;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  IA64_GR_t *ival;

  switch (opname) {
    OPFETCH_PREFIX;
  case src_qp:
    set_src_op(ii,src_qp,PR,get_p(realct,ii,all_qp(instr)));
    if (!fallthrough) break;
  case src_op1:
    ival = get_r_ptr(realct,ii,all_r2(instr));
    set_src_op_r(ii,src_op1,*ival);
    if (!fallthrough) break;
  case src_op2:
    set_src_op(ii,src_op2,imm64,A10_ct2d(instr)+1);
    if (!fallthrough) break;
  case src_op3:
    ival = get_r_ptr(realct,ii,all_r3(instr));
    set_src_op_r(ii,src_op3,*ival);
    if (!fallthrough) break;
  default: break;
  }
}

void
evaluate_A10(LSE_emu_instr_info_t *ii) 
{ /* A10 multimedia shift and add */
  uint64_t instr = ii->extra.instr;
  uint64_t r2val, r3val, newval;
  int32_t tmp, tmask;
  int i, count;

  check_target_register(ii,all_r1(instr));
  
  r2val = get_src_op(ii,src_op1,GR.val);
  r3val = get_src_op(ii,src_op3,GR.val);
  count = get_src_op(ii,src_op2,GR.val);

  if (count > 3) {
    report_intr(ii,IA64_intr_illegaloperation);
  }

  if (A10_x4(instr) & 2) { /* pshradd2 */
    newval = 0;
    tmask = (0x0ffff >> count);
    for (i=0;i<4;i++) {
      tmp = ((r2val>>count) & 0x0FFFF) | ((r2val & 0x8000)?~tmask:0);
      tmp += (int32_t)(((r3val & 0x8000) ? ~0xffff : 0) | (r3val & 0x0ffff));
      if (tmp > 32767) tmp = 32767;
      else if (tmp < -32768) tmp = -32768;
      newval |= (tmp & 0xFFFF) << (16 * i);
      r2val >>= 16;
      r3val >>= 16;
    }
  } else { /* pshladd2 */
    newval = 0;
    for (i=0;i<4;i++) {
      tmp = (r2val & 0x0FFFF);
      if (tmp & 0x8000) tmp |= ~0x0FFFF;
      tmp >>= count;
      tmp += (int32_t)(((r3val & 0x8000) ? ~0xffff : 0) | (r3val & 0x0ffff));
      if (tmp > 32767) tmp = 32767;
      else if (tmp < -32768) tmp = -32768;
      newval |= (tmp & 0xFFFF) << (16 * i);
      r2val >>= 16;
      r3val >>= 16;
    }
  }
  set_dest_op(ii,dest_result,GR.val,newval);
  set_dest_op(ii,dest_result,GR.nat,
	      get_src_op(ii,src_op1,GR.nat) | get_src_op(ii,src_op3,GR.nat));
  return;
}

void pred_illegal_op(LSE_emu_instr_info_t *ii) {
  report_intr(ii,IA64_intr_illegaloperation);
  return;
}

/************************* Unified operand stuff ************************/

void 
IA64_calc_dest_alatupdate(LSE_emu_instr_info_t *ii) {

  switch (ii->extra.formatno) {

  case 101: /* M1 - integer load */
  case 102: /* M2 - integer load, increment by register */
  case 103: /* M3 - integer load, increment by immediate */
  case 106: /* M6 - FP load */
  case 107: /* M7 - FP load/increment by register */
  case 108: /* M8 - FP load/increment by immediate */
  case 111: /* M11 - FP load pair  */
  case 112: /* M12 - FP load pair - increment by immediate */
    dest_alatupdate_M1M2M3(ii);
    break;

  case 104: /* M4 - integer store */
  case 105: /* M5 - integer store - increment by immediate */
  case 109: /* M9 - FP store */
  case 110: /* M10 - FP store - increment by immediate */

  case 116: /* M16 - Exchange/Compare and Exchange */
  case 117: /* M17 - Fetch and Add - immediate */
    set_dest_op(ii,dest_alatupdate,alatchange,-1);
    break;
    
  case 122: /* M22 - Integer advanced load check */
  case 123: /* M23 - FP advanced load check */
    dest_alatupdate_M22M23(ii);
    break;

  case 124: /* M24 - Sync/Fence/Serialize/ALAT control */
    dest_alatupdate_M24(ii);
    break;

  case 126: /* M26 - Invalidate integer entry */
  case 127: /* M27 - Invalidate FP entry */
    dest_alatupdate_M26M27(ii);
    break;

  case 134: /* M34 - alloc */
    /* set_dest_op(ii,dest_alatupdate,alatchange,0); */
    break;

  case 304: /* B4 - indirect branch */
    /* set_dest_op(ii,dest_alatupdate,alatchange,0); */
    break;

  case 308: /* TODO: B8 - miscellaneous B-unit (rfi does stuff) */
    break;

    /* default is to do nothing at all, as the instruction might not have
     * this kind of operand...
     */

  } /* switch (ii->extra.formatno) */
}

int IA64_RSE_fill(LSE_emu_instr_info_t *ii,
		  LSE_emu_addr_t loadp,
		  int numtodo,
		  uint64_t rnat,
		  uint64_t *newrnat,
		  LSE_emu_operand_val_t *valarr) {

  int bitnum;
  uint64_t len = 8;
  uint64_t mask;
  LSE_device::deverror_code_t memerr = LSE_device::deverror_none;
  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);

  unsigned int rno = numtodo;

  loadp = (loadp&~INT64_C(7)) + 8*(numtodo+(((loadp>>3)&0x3f)+numtodo)/63);
  bitnum = (loadp>>3) & 0x3f;
  mask = INT64_C(1) << bitnum;

  while (numtodo) {

    loadp -= 8; /* pre-decrement */
    bitnum--;
    numtodo --;
    mask >>= 1;
    rno = rno - 1;

    if (bitnum == -1) { /* Get RNAT */

      /* TODO: endianness 
	 jc1: fixed?
       */
      try {
	realct->mem->read(loadp, (unsigned char *)&rnat, len);
      } catch (LSE_device::deverror_t e) {
	memerr = e.err;
      }
      
      rnat = LSE_end_le2h_ll(rnat);

      if (memerr) break;

      bitnum = 62;
      loadp -= 8;
      mask = INT64_C(1) << 62;
    }

    /* TODO: endianness 
       jc1: fixed?
    */
    try {
      realct->mem->read(loadp,
			(unsigned char *)&valarr[rno].data.GR.val, len);
    } catch (LSE_device::deverror_t e) {
      memerr = e.err;
    }

    valarr[rno].data.GR.val = LSE_end_le2h_ll(valarr[rno].data.GR.val);

    if (memerr) break;

    valarr[rno].data.GR.nat = (mask & rnat) ? 1:0;

  } /* while (numtodo) */
  
  *newrnat = rnat;
  return memerr;
}
} // namespace LSE_IA64
