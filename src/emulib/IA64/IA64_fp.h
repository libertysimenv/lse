/* 
 * Copyright (c) 2000-2007 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header file for floating point the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file contains common type and constant definitions that the IA64
 * emulator uses internally.  It does *not* use these definitions elsewhere.
 *
 * TODO: 
 *
 */
#ifndef _IA64_FP_H_
#define _IA64_FP_H_
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "LSE_IA64.h"

#define bits64 (uint64_t)UINT64_C(0xFfffFfffFfffFfff)
#define bits63 (uint64_t)UINT64_C(0x7fffFfffFfffFfff)
#define bits32 (uint64_t)UINT64_C(0xFfffFfff)

namespace LSE_IA64 {

static inline int fp_is_inf(IA64_FR_t v) {
  return (v.exponent==0x1ffff && !(v.significand & bits63));
}

static inline int fp_is_nan(IA64_FR_t v) {
  return (v.exponent==0x1ffff && (v.significand & bits63));
}

static inline int fp_is_natval(IA64_FR_t v) {
  return (v.exponent==0x1fffe && !v.sign && 
	  v.significand==0);
}

static inline int fp_is_normal(IA64_FR_t v) {
  if (v.exponent == 0x1ffff) return 0;
  else return (v.exponent != 0 && 
	       (v.significand & (INT64_C(1)<<63)));
}

static inline int fp_is_qnan(IA64_FR_t v) {
  return (v.exponent==0x1ffff && (v.significand & (INT64_C(1)<<62)));
}

static inline int fp_is_snan(IA64_FR_t v) {
  return (v.exponent==0x1ffff && (v.significand & INT64_C(1)<<63) && 
	  v.significand != (INT64_C(1)<<63) &&
	  ( (v.significand & (INT64_C(3)<<62)) == (INT64_C(2)<<62)));
}

static inline int fp_is_unorm(IA64_FR_t v) {
  if (v.exponent == 0x1ffff) return 0;
  else if (v.exponent==0) return (v.significand!=0); /* not a zero */
  else if (v.significand==0) return (v.sign || v.exponent!=0x1fffe); /*not NaT*/
  else return !(v.significand & (UINT64_C(1)<<63));
}

static inline int fp_is_zero(IA64_FR_t v) {
  return (v.significand==0 && (v.exponent < 0x1fffe || 
			       (v.sign && v.exponent != 0x1ffff)));
}

static inline int fp_is_true_zero(IA64_FR_t v) {
  return (v.significand==0 && v.exponent == 0);
}

static inline double
fp_reg2host(IA64_FR_t *v) 
{
  if (v->exponent>1023+65535) return 0.0;
  else return ((v->sign?-1.0:1.0)*
	       ldexp((double)v->significand,
		     (double)v->exponent-65535-63));
}

static inline void
fp_host2reg_good(IA64_FR_t *v, double host_form)
{
  double fraction;
  int exponent;
  fraction = frexp(host_form, &exponent);
  v->sign = host_form < 0.0;
  if (v->sign) fraction = -fraction;
  v->exponent = exponent + 65535 - 1;
  v->significand = (uint64_t) ldexp(fraction,64);
}

static inline void 
fp_normalize(IA64_FR_t *v) 
{ /* Assumes not a NatVal */
  if (v->significand == 0) { /* make pseudo-zeros true zeros */
    v->exponent = 0;
  } else {
    while (!(v->significand & (INT64_C(1)<<63)) && v->exponent>1) {
      v->exponent--;
      v->significand <<= 1;
    }
    v->significand &= UINT64_C(0xFfffFfffFfffFfff);
  }
}

/* This is because an exponent of 0 is really an exponent of 0xc001 (unless
 * it's a zero) ; why they did that, I do not know, but it is *really* 
 * annoying....
 */
static inline void 
fp_fixweirdexp(IA64_FR_t *v) 
{ 
  if (v->exponent == 0 && v->significand != 0) v->exponent = 0xc001;
}

static inline void 
fp_overnormalize(IA64_FR_t *v) 
{ /* Assumes no NatVal, infinity, or NaN */
  /* normalize, not restricting range; makes denorms look "real" */
  if (v->significand == 0) {
    v->exponent = 0;
  } else { 
    while (!(v->significand & (INT64_C(1)<<63))) {
      v->exponent--;
      v->significand <<= 1;
    }
    v->significand &= UINT64_C(0xFfffFfffFfffFfff);
  }
}

static inline void fp_mem2reg_8(uint64_t v1, IA64_FR_t *v) {
  v->significand = v1;
  v->exponent = 0x1003e;
  v->sign = 0;
}

static inline void fp_mem2reg_s(uint64_t v1, IA64_FR_t *v) {
  v->sign = (v1 >> 31)&1;
  if ((v1 & 0x7f800000)==0x7f800000) { /* Infinity/NaN */
    v->exponent = 0x1ffff;
    v->significand = ( (INT64_C(1)<<63) |
				( (v1 <<40) & ~(INT64_C(1)<<63) ));
  } else if ((v1 & 0x7f800000)==0) { /* denorm/zeros */
    v->significand = ( (v1 <<40) & ~(INT64_C(1)<<63) );
    v->exponent = (v->significand ? 
			    0x0ff81 : 0);
  } else {
    v->significand = ( (INT64_C(1)<<63) |
				( (v1 <<40) & ~(INT64_C(1)<<63) ));
    v->exponent = ( ((v1 >> 23) & 0x7f) |
			     ((v1 & 0x40000000) ? 0x10000 
			      : 0x0ff80));
  }
}

static inline void fp_mem2reg_d(uint64_t v1, IA64_FR_t *v) {
  v->sign = v1 >> 63;
  if ((v1 & (INT64_C(0x7ff0) << 48))==(INT64_C(0x7ff0) << 48)) {
    /* Infinity/NaN */
    v->exponent = 0x1ffff;
    v->significand = ( (INT64_C(1)<<63) |
				((v1 << 11) & ~(INT64_C(1)<<63) ));
  } else if ((v1 & (INT64_C(0x7ff0) << 48))==0) { /* denorm/zeros */
    v->significand = ( ((v1 << 11) & ~(INT64_C(1)<<63) ));
    v->exponent = (v->significand ? 
			    0x0fc01 : 0);
  } else {
    v->significand = ( (INT64_C(1)<<63) |
				((v1 << 11) & ~(INT64_C(1)<<63) ));
    v->exponent = ( ((v1 >> 52) & 0x03ff) |
			     ((v1 & (INT64_C(0x4000) << 48)) ? 0x10000 
			      : 0x0fc00));
  }
}

static inline void fp_mem2reg_e(uint64_t v1, uint64_t v2, IA64_FR_t *v) {
  v->significand = v1;
  v->sign = (v2 >> 15) & 1;
  if ((v2 & 0x7fff)==0x7fff) { /* Infinity/NaN */
    v->exponent = 0x1ffff;
  } else if ((v2 & 0x7fff)==0) { /* denorm/zeros */
    v->exponent = 0;
  } else {
    v->exponent = ((v2 & 0x3fff) | 
			    ((v2 & 0x4000) ? 0x10000 
			     : 0x0c000));
  }
}

static inline void fp_reg2mem_s(IA64_FR_t *v, uint64_t *v1)
{
  *v1 = ( 
	 ( (v->significand>>40) & 0x7fFfff) |
	 ( ((int64_t)v->significand>>63) & 
	   ((((uint64_t)v->exponent & INT64_C(0x7f)) << 23) |
	    (((uint64_t)v->exponent & INT64_C(0x10000))<<14))) |
	 ( (uint64_t)v->sign<<31)
	 );
}

static inline void fp_reg2mem_d(IA64_FR_t *v, uint64_t *v1)
{
  *v1 = ( 
	 ( ((uint64_t)v->significand >>11) 
	   & INT64_C(0x000fFfffFfffFfff)) |
	 ( ((int64_t)v->significand>>63) &
	   ( (((uint64_t)v->exponent & INT64_C(0x3ff)) << 52) |
	     (((uint64_t)v->exponent & INT64_C(0x10000)) << 46))) |
	 ( (uint64_t)v->sign << 63)
	 );
}

typedef struct {
  uint64_t significandh, significandl;
  int exponent;
  int sign;
} fp_internal;

/* Returns overflow/underflow/etc. status */
static inline int
fp_round(IA64_FR_t *v, IA64_FR_t *iv, int *fpa,
	 int precis, int rmode, int roundbit, 
	 int stickybit) 
{
  unsigned int bits;
  static int roundtable[8][8] = {
    /*        nearest:
       0.00 - no   (2.00 -> 2)
       0.01 - no   (2.25 -> 2)
       0.10 - no   (2.50 -> 2)
       0.11 - yes  (2.75 -> 3)
       1.00 - no   (3.00 -> 3)
       1.01 - no   (3.25 -> 3)
       1.10 - yes  (3.50 -> 4)
       1.11 - yes  (3.75 -> 4)
    */
    { 0, 0, 0, 1, 0, 0, 1, 1 },
    { 0, 0, 0, 1, 0, 0, 1, 1 },
    /*        down:
       0.00 - no   (-2.00 -> 2)
       0.01 - yes  (-2.25 -> -3)
       0.10 - yes  (-2.50 -> -3)
       0.11 - yes  (-2.75 -> -3)
       1.00 - no   (-3.00 -> 3)
       1.01 - yes  (-3.25 -> -4)
       1.10 - yes  (-3.50 -> -4)
       1.11 - yes  (-3.75 -> -4)
    */
    { 0, 0, 0, 0, 0, 0, 0, 0 },
    { 0, 1, 1, 1, 0, 1, 1, 1 },
    /*        up:
       0.00 - no   (2.00 -> 2)
       0.01 - yes  (2.25 -> 3)
       0.10 - yes  (2.50 -> 3)
       0.11 - yes  (2.75 -> 3)
       1.00 - no   (3.00 -> 3)
       1.01 - yes  (3.25 -> 4)
       1.10 - yes  (3.50 -> 4)
       1.11 - yes  (3.75 -> 4)
    */
    { 0, 1, 1, 1, 0, 1, 1, 1 },
    { 0, 0, 0, 0, 0, 0, 0, 0 },
    /*        trunc: */
    { 0, 0, 0, 0, 0, 0, 0, 0 },
    { 0, 0, 0, 0, 0, 0, 0, 0 },
  };

  static uint64_t stickybits[] = { (UINT64_C(1)<<39)-1, 0, (UINT64_C(1)<<10)-1, 0, 0};
  static uint64_t roundbits[] = { UINT64_C(1) << 39, 0, UINT64_C(1) << 10, 0, 0};
  static uint64_t addbits[] = { UINT64_C(1) << 40, 0, UINT64_C(1) << 11, 1, 1};
  static uint64_t sigmask[] = { 
    UINT64_C(0xffffff) << 40, 0, UINT64_C(0xFfffFfffFfffF800),
    UINT64_C(0xFfffFfffFfffFfff), UINT64_C(0xFfffFfffFfffFfff)
  };
  static int bitcount[] = { 38, 0, 9, 0, 0};

  if (precis < 3) {
    stickybit = ((iv->significand & stickybits[precis])) ? 1 : 0;
    roundbit = (iv->significand & roundbits[precis]) ? 1 : 0;
    bits = (iv->significand >> bitcount[precis]) & 4;
  }
  else bits = (iv->significand << 2) & 4;
  bits |= (roundbit<<1) | stickybit;

  v->significand = iv->significand & sigmask[precis];
  v->sign = iv->sign;
  v->exponent = iv->exponent;
  *fpa = roundtable[(rmode<<1)|(v->sign&1)][bits&7];
  if (*fpa) { /* add one */
    if (v->significand == sigmask[precis]) {
      if (precis != 4) v->exponent++;
      v->significand = (INT64_C(1)<<63);
    }
    else v->significand += addbits[precis];
  } /* add one */

  return bits & 3; /* inexact */
}

static inline IA64_intr_t
fp_find_trap(IA64_FR_t *newval,  /* unbound-range-rounded normalized value */
	     IA64_FR_t *savedval, /* unrounded normalized value */
	     int *flags, /* mysf so far, shifted down */
	     int *fpa, /* had to add when rounding */
	     int guard, int sticky,
	     int controls, 
	     int traps, /* traps (with the td included) */
	     int i /* inexact bit so far */
	     )
{
  int limittable[8][2] = {
    { 0x0ff81, 0x1007e }, /* single */
    { 0, 0x1fffe}, /* reserved */
    { 0x0fc01, 0x103fe }, /* double */
    { 0x0c001, 0x13ffe }, /* double-extended */
    { 1, 0x1fffe}, /* wre */
    { 1, 0x1fffe}, /* reserved */
    { 1, 0x1fffe}, /* wre */
    { 1, 0x1fffe}, /* wre */

  };
  int maxe, mine;
  int wre = rf_get(controls,FPSR_wre);
  int precis = rf_get(controls,FPSR_pc);

  mine = limittable[(wre<<2)|precis][0];
  maxe = limittable[(wre<<2)|precis][1];

  if (newval->exponent == 0 && newval->significand == 0 && !i)
    return IA64_intr_none;
 
  if (newval->exponent > maxe) { /* overflow situation... */
#ifdef IA64_COMPLAIN_ABOUT_RANGE
    fprintf(stderr,"%d %x %016"PRIx64" %x %d %d\n",newval->sign,newval->exponent,
	    newval->significand, maxe, controls, precis);
    exit(1);
#endif 
    if (!rf_get(traps,FPSR_od)) {
      *flags |= rf_mask(FPSR_o) | (i ? rf_mask(FPSR_i) : 0);
      newval->exponent &= (1<<17)-1; /* wrap */
      return IA64_intr_floatingpointtrap;
    } else {
      int sign = newval->sign;
      if (IA64_swa_on_output_huge) {
	*flags |= rf_mask(FPSR_i) | rf_mask(FPSR_o);
	newval->exponent &= (1<<17)-1; /* wrap */
	return IA64_intr_floatingpointtrap; 
     } else {
	i = 1;
	*flags |= rf_mask(FPSR_o);
	*newval = IA64_Inf; /* overflow to infinity */
	newval->sign = sign;
      }
    }
  } else if (newval->exponent < mine) { /* underflow situation */
#ifdef IA64_COMPLAIN_ABOUT_RANGE
    fprintf(stderr,"%d %x %016"PRIx64"\n",newval->sign,newval->exponent,
	    newval->significand);
    exit(2);
#endif 
    if (!rf_get(traps,FPSR_ud)) { 
      *flags |= (rf_mask(FPSR_u) | rf_mask(FPSR_i));
      newval->exponent &= (1<<17)-1; /* wrap */
      return IA64_intr_floatingpointtrap;
    } else if (rf_get(controls,FPSR_ftz)) { /* flush-to-zero */
      i = 1;
      *flags |= rf_mask(FPSR_u);
      newval->exponent = 0;
      newval->significand = 0;
    } else { /* denormalize */
      /* 
       * Would you believe it?  This is another potential
       * software assist place....
       */
      int d = mine - savedval->exponent;
      uint64_t extra3 = ((uint64_t)guard << 63) | (uint64_t)sticky;
      IA64_FR_t myval;

      if (IA64_swa_on_output_tiny) {
	*flags |= rf_mask(FPSR_u) | rf_mask(FPSR_i);
	newval->exponent &= (1<<17)-1; /* wrap */
	return IA64_intr_floatingpointtrap;
      }

      myval = *savedval;
      while (d > 64) {
	extra3 = (extra3 ? 1 : 0) | myval.significand;
	myval.significand = 0;
	d -= 64;
      }
      if (d > 0) {
	extra3 = (  ((d>1) ? (extra3 ? UINT64_C(1) : 0) : ((extra3>>1)|(extra3&1)))|
		    ((myval.significand & ((UINT64_C(1)<<d)-1)) << (64-d))     );
	myval.significand >>= d;
      }
      myval.exponent = mine;
      /* re-round the denormalized number */
      i = fp_round(newval,&myval,fpa,precis,rf_get(*flags,FPSR_rc),
		   (extra3>>63)&1,(extra3 & bits63) ? 1:0);
      /* need to get zero back.... */
      if (newval->significand == 0) newval->exponent = 0;
      /* weirdness! double-extended real denormals are mapped to exponent 0 */
      if (newval->exponent == 0x0c001) newval->exponent = 0;

      if (i) {
	*flags |= rf_mask(FPSR_u); /* both tiny and inexact */
      }
    } /* denormalize */
  } /* underflow */

  if (i) {
    *flags |= rf_mask(FPSR_i);
    if (!rf_get(traps,FPSR_id)) return IA64_intr_floatingpointtrap;
  }
  return IA64_intr_none;
}

} // namespace LSE_IA64

#endif /* _LSE_IA64_H_ */

