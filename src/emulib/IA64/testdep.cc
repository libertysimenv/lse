/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Test dependency information
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This program tests whether the decoder got all the source and
 * destination stuff right.  It uses a different algorithm than the
 * decoder does so as to not make the same mistakes.
 *
 * The way it works is that each instruction in a program is compared against
 * each state element.  State elements are defined as a list of patterns to
 * match against the instruction.  Lots of fun....
 *
 * TODO:
 *    - Add testing for:
 *      ALAT, CPUID#, DBR#, DTC, DTC_LIMIT, DTR, IBR#, InService, ITC,
 *      ITC_LIMIT, ITR, memory?, PKR#, PMC#, PMD#, RR#, RSE
 *    - Figure out if my additions marked ADD are really errors in the manual
 */
#include "LSE_IA64.h"
using namespace LSE_IA64;
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

static char *pname;
#include "LSE_emu_alonesupp.c"  /* yes, the .c file */

static void usage() {
  fprintf(stderr,
          "\n%s <options>\n",
          pname);
  fprintf(stderr,"\t--decode\t\tDump decode information #\n");
  exit(1);
}

static char *cleanenvp[]={0};

int errors = 0;

static void build_instruction_lists(LSE_emu_ctoken_t tok);
static int do_dependency_checks(LSE_emu_instr_info_t *ii);
static void start_dep_checks(void);
static void end_dep_checks(void);

int main(int argc, char *argv[], char **envp)
{
  int rval;
  int i;
  char *rargs[2];
  int rcnt = 0;
  int count=0;

  pname = argv[0];
  LSE_sim_exit_status = 0;
  EMU_init(&LSE_emu_interface);

  for (i=1;i<argc;i++) {
    if (!strcmp(argv[i],"--help")) usage();
    else if (!strcmp(argv[i],"-help")) usage();
    else if (rcnt < 1) rargs[rcnt++] = argv[i];
    else {
      fprintf(stderr,"Error: unrecognized argument\n");
      usage();
    }
  }

  LSE_emu_standalone_init();

  LSE_emu_create_context(1);

  rval = EMU_context_load(&LSE_emu_interface, 1, 1, rargs, cleanenvp);

  build_instruction_lists(LSE_emu_hwcontexts_table[1].ctok);

  if (rval) {
    fprintf(stderr,"Error while loading program\n");
    exit(1);
  }

  start_dep_checks();

  {
    LSE_emu_addr_t addr;
    LSE_emu_instr_info_t ii;
    LSE_emu_ctoken_t ctx = LSE_emu_hwcontexts_table[1].ctok;

    addr = EMU_get_start_addr(ctx);
    ii.hwcontextno = 1;
    ii.swcontexttok= ctx;

    fprintf(stdout,"One '.' equals 32 instructions checked\n");
    while (1) {
      EMU_init_instr(&ii,1,ctx,addr);      
      EMU_do_step(&ii, (LSE_emu_instrstep_name_t)0, FALSE);  /* ifetch */
      if (ii.privatef.actual_intr || ii.privatef.potential_intr) break;
      /* presumably fell off end.... */

#ifdef DEBUGDEP      
      EMU_disassemble_addr(ctx,addr,stdout);
#endif
	
      errors += do_dependency_checks(&ii);

      count ++;
      if (!(count&0x1f)) {
	fprintf(stdout,".");
	fflush(stdout);
	if (!(count & 0x1ff)) {
	  fprintf(stdout,"\n");
	  fflush(stdout);
	}
      }

      addr = addr + 1;
      if ((addr & 3) == 3) addr += 13;

    } /* while 1 */
  }

  end_dep_checks();

  fprintf(stdout,"There were %d errors\n",errors);
  EMU_finish(&LSE_emu_interface);
  LSE_emu_standalone_finalize();
  return errors;
}

struct itype {
  char name[50];
  int unit;
  uint64_t masklo, bitslo;
  int formatno;
  const char *rnames[10];
  int notaninst;
  int mark;
  int matches;
  struct rtype *rtests[10];
};
typedef struct itype Itype;

struct ltype {
  char name[50];
  const char *cnames[60];
  int conjunctive;
  int mark;
  struct {
    int itype;
    union {
      struct itype *inst;
      struct ltype *list;
      struct rtype *rtest;
      struct utype *utest;
    } ptr;
  } contents[60];
};
typedef struct ltype Ltype;

struct rtype { /* register tests: test for address value */
  char name[50];
  int marktype; /* 1 = read, 2 = write */
  int id;
  int shift, maxregno;
};
typedef struct rtype Rtype;

struct utype { /* ugly tests.... */
  char name[50];
  int (*func)(LSE_emu_instr_info_t *, int, LSE_emu_spaceid_t, int, uint64_t,
	      Itype **);
  int extra;
};
typedef struct utype Utype;

Itype instructions[] = { 

  { "add",           5,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
			  (UINT64_C(0xf)<<29)|(UINT64_C(0x2)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x0)<<29)|(UINT64_C(0x0)<<27)),
    501, { "qp", "r1", "r2", "r3" } },
  { "addp4 A1",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
			 (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2)<<29)|(UINT64_C(0x0)<<27)),
    501, { "qp", "r1", "r2", "r3" } },
  { "addp4 A4",   5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x2)<<34)|(UINT64_C(0x0)<<33)),
    504, { "qp", "r1", "r3" } },
  { "adds",   5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x0)<<33)),
    504, { "qp", "r1", "r3" } },
  { "addl",   5, ((UINT64_C(0xf)<<37)),
    ((UINT64_C(0x9)<<37)),
    505, { "qp", "r1", "r3small" } },
  { "alloc",      1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(1)<<37)|(UINT64_C(0x6)<<33)),
    134, { "r1" } },
  { "and A1",     5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		      (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x3)<<29)|(UINT64_C(0x0)<<27)),
    501, { "qp", "r1", "r2", "r3" } },
  { "and A3",     5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		      (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0xb)<<29)|(UINT64_C(0x0)<<27)),
    503, { "qp", "r1", "r3" } },
  { "andcm A1",   5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		      (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x3)<<29)|(UINT64_C(0x1)<<27)),
    501, { "qp", "r1", "r2", "r3" } },
  { "andcm A3",   5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		      (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0xb)<<29)|(UINT64_C(0x1)<<27)),
    503, { "qp", "r1", "r3" } },

  { "br.call B3", 3, ((UINT64_C(0xf)<<37)),(UINT64_C(5)<<37),
    303, { "qp", "b1" } },
  { "br.call B5", 3, ((UINT64_C(0xf)<<37)),(UINT64_C(1)<<37),
    305, { "qp", "b1", "b2" } },
  { "br.cexit", 3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<6)), ((UINT64_C(0x4)<<37)|(UINT64_C(0x6)<<6)),
    302,{} },
  { "br.cloop", 3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<6)), ((UINT64_C(0x4)<<37)|(UINT64_C(0x5)<<6)),
    302, {} },
  { "br.cond B1", 3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<6)), ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<6)),
    301, { "qp" } },
  { "br.cond B4", 3, ((UINT64_C(0xf)<<37)|(0x7<<6)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0)<<37)|(UINT64_C(0x20)<<27)|(0<<6)),
    304, { "qp", "b2" } },
  { "br.ctop" , 3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<6)), ((UINT64_C(0x4)<<37)|(UINT64_C(0x7)<<6)),
    302, {} },
  { "br.ia",      3, ((UINT64_C(0xf)<<37)|(0x7<<6)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0)<<37)|(UINT64_C(0x20)<<27)|(1<<6)),
    304, {} },
  { "br.ret",     3, ((UINT64_C(0xf)<<37)|(0x7<<6)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0)<<37)|(UINT64_C(0x21)<<27)|(4<<6)),
    304, { "qp" } },
  { "br.wexit", 3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<6)), ((UINT64_C(0x4)<<37)|(UINT64_C(0x2)<<6)),
    301, { "qp" } },
  { "br.wtop",  3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<6)), ((UINT64_C(0x4)<<37)|(UINT64_C(0x3)<<6)),
    301, { "qp" } },
  { "break.b",  3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<27)), 0,
    309, { "qp" } },
  { "break.i",  0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)), 0,
    19, { "qp" } },
  { "break.f",  2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)), 0,
    215, { "qp" } },
  { "break.m",  1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3)<<31)), 0,
    137, { "qp" } },
  { "break.x",  4, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)), 0,
    401, { "qp" } },
  { "brl.call", 4, ((UINT64_C(0xf)<<37)), (UINT64_C(0xd)<<37),
    404, { "qp" } },
  { "brl.cond", 4, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<6)), ((UINT64_C(0xc)<<37)|(UINT64_C(0x0)<<6)),
    403, { "qp" } },
  { "brp B6",   3, ((UINT64_C(0xf)<<37)), (UINT64_C(0x7)<<37),
    306, { "qp" } },
  { "brp B7",   3, ((UINT64_C(0xf)<<37) | (UINT64_C(0x3e)<<27)),
    ((UINT64_C(0x2)<<37)|(UINT64_C(0x10)<<27)),
    307, { "b2" } },
  { "bsw",      3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3e)<<27)), 
    ((UINT64_C(0x0)<<37)|UINT64_C(0x0c)<<27),
    308, {} },

  { "chk.a.clr M22",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(0)<<37)|(UINT64_C(5)<<33)),
    122, { "qp", "r1 s" } },
  { "chk.a.clr M23",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(0)<<37)|(UINT64_C(7)<<33)),
    123, { "qp", "f1 s" } },
  { "chk.a.nc M22",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(0)<<37)|(UINT64_C(4)<<33)),
    122, { "qp", "r1 s" } },
  { "chk.a.nc M23",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(0)<<37)|(UINT64_C(6)<<33)),
    123, { "qp", "f1 s" } },
  { "chk.s I20",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(0)<<37)|(UINT64_C(1)<<33)),
    20, { "qp", "r2" } },
  { "chk.s M20",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(1)<<37)|(UINT64_C(1)<<33)),
    120, { "qp", "r2" } },
  { "chk.s M21",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(1)<<37)|(UINT64_C(3)<<33)),
    121, { "qp", "f2" } },
  { "clrrrb",      3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3e)<<27)), 
    ((UINT64_C(0x0)<<37)|UINT64_C(0x04)<<27),
    308, {} },
  { "cmp C A6",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)), 
    ((UINT64_C(0xc)<<37)|(UINT64_C(0)<<34)|(UINT64_C(0)<<36)), 
    506, { "qp", "p1", "p2", "r2", "r3" } },
  { "cmp D A6",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xd)<<37)|(UINT64_C(0)<<34)|(UINT64_C(0)<<36)), 
    506, { "qp", "p1", "p2", "r2", "r3" } },
  { "cmp E A6",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xe)<<37)|(UINT64_C(0)<<34)|(UINT64_C(0)<<36)), 
    506, { "qp", "p1", "p2", "r2", "r3" } },
  { "cmp4 C A6",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xc)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)), 
    506, { "qp", "p1", "p2", "r2", "r3" } },
  { "cmp4 D A6",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xd)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)), 
    506, { "qp", "p1", "p2", "r2", "r3" } },
  { "cmp4 E A6",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xe)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)), 
    506, { "qp", "p1", "p2", "r2", "r3" } },
  { "cmp C A7",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)), 
    ((UINT64_C(0xc)<<37)|(UINT64_C(0)<<34)|(UINT64_C(1)<<36)), 
    507, { "qp", "p1", "p2", "r3" } },
  { "cmp D A7",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xd)<<37)|(UINT64_C(0)<<34)|(UINT64_C(1)<<36)), 
    507, { "qp", "p1", "p2", "r3" } },
  { "cmp E A7",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xe)<<37)|(UINT64_C(0)<<34)|(UINT64_C(1)<<36)), 
    507, { "qp", "p1", "p2", "r3" } },
  { "cmp4 C A7",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xc)<<37)|(UINT64_C(1)<<34)|(UINT64_C(1)<<36)), 
    507, { "qp", "p1", "p2", "r3" } },
  { "cmp4 D A7",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xd)<<37)|(UINT64_C(1)<<34)|(UINT64_C(1)<<36)), 
    507, { "qp", "p1", "p2", "r3" } },
  { "cmp4 E A7",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xe)<<37)|(UINT64_C(1)<<34)|(UINT64_C(1)<<36)), 
    507, { "qp", "p1", "p2", "r3" } },
  { "cmp C A8",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)), 
    ((UINT64_C(0xc)<<37)|(UINT64_C(2)<<34)), 
    508, { "qp", "p1", "p2", "r3" } },
  { "cmp D A8",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)),
    ((UINT64_C(0xd)<<37)|(UINT64_C(2)<<34)), 
    508, { "qp", "p1", "p2", "r3" } },
  { "cmp E A8",       5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)),
    ((UINT64_C(0xe)<<37)|(UINT64_C(2)<<34)),
    508, { "qp", "p1", "p2", "r3" } },
  { "cmp4 C A8",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)),
    ((UINT64_C(0xc)<<37)|(UINT64_C(3)<<34)),
    508, { "qp", "p1", "p2", "r3" } },
  { "cmp4 D A8",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)),
    ((UINT64_C(0xd)<<37)|(UINT64_C(3)<<34)),
    508, { "qp", "p1", "p2", "r3" } },
  { "cmp4 E A8",      5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)),
    ((UINT64_C(0xe)<<37)|(UINT64_C(3)<<34)),
    508, { "qp", "p1", "p2", "r3" } },
  { "cmpxchg",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(4)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<27)|(UINT64_C(0)<<30)),
    116, { "qp", "r1", "r3", "r2" } },
  { "cover",      3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0x0)<<37)|UINT64_C(0x02)<<27),
    308, {} },
  { "czx1",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3b)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x18)<<27)),
    29, { "qp", "r1", "r3" } },
  { "czx2",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3b)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x19)<<27)),
    29, { "qp", "r1", "r3" } },

  { "dep.z I12",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(1)<<33)|(UINT64_C(1)<<26)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x1)<<34)|(UINT64_C(0x1)<<33)|(UINT64_C(0x0)<<26)),
    12, { "qp", "r1", "r2" } },
  { "dep.z I13",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(1)<<33)|(UINT64_C(1)<<26)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x1)<<34)|(UINT64_C(0x1)<<33)|(UINT64_C(0x1)<<26)),
    13, { "qp", "r1" } },
  { "dep I14",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(1)<<33)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)),
    14, { "qp", "r1", "r3" } },
  { "dep I15",      0, ((UINT64_C(0xf)<<37)),
    ((UINT64_C(0x4)<<37)),
    15, { "qp", "r1", "r2", "r3" } },

  { "epc",      3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0x0)<<37)|UINT64_C(0x10)<<27),
    308, {} },
  { "extr",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(1)<<33)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x1)<<34)|(UINT64_C(0x0)<<33)),
    11, { "qp", "r1", "r3" } },

  { "famax", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x17)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "famin", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x16)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "fand", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2c)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fandcm", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2d)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fc", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x30)<<27)),
    128, { "qp", "r3" } },
  { "fchkf", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x08)<<27)),
    214, { "qp" } },
  { "fclass", 2,  ((UINT64_C(0xf)<<37)),
    (UINT64_C(0x5)<<37),
    205, { "qp", "p1", "p2", "f2" } },
  { "fclrf", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x05)<<27)),
    213, { "qp" } },
  { "fcmp", 2,  (UINT64_C(0xf)<<37), 
    (UINT64_C(0x4)<<37),
    204, { "qp", "p1", "p2", "f2", "f3" } },
  { "fcvt.fx", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3d)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x18)<<27)),
    210, { "qp", "f1", "f2" } },
  { "fcvt.fxu", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3d)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x19)<<27)),
    210, { "qp", "f1", "f2" } },
  { "fcvt.xf", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1c)<<27)),
    211, { "qp", "f1", "f2" } },
  { "fetchadd4",  1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3b)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x12)<<30)),
    117, { "qp", "r3", "r1" } },
  { "fetchadd8",  1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3b)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x13)<<30)),
    117, { "qp", "r3", "r1" } },
  { "flushrs",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3)<<31)|(UINT64_C(0xf)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0xc)<<27)|(UINT64_C(0x0)<<31)),
    125, {} },
  { "fma 8", 2, ((UINT64_C(0xf)<<37)),
    ((UINT64_C(0x8)<<37)),
    201, { "qp", "f1", "f3", "f4", "f2" } },
  { "fma 9", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0x9)<<37)|(UINT64_C(0x0)<<36)),
    201, { "qp", "f1", "f3", "f4", "f2" } },
  { "fmax", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x15)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "fmerge", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3c)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x10)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fmin", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x14)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "fmix", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3c)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x38)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fms a", 2, ((UINT64_C(0xf)<<37)),
    ((UINT64_C(0xa)<<37)),
    201, { "qp", "f1", "f3", "f4", "f2" } },
  { "fms b", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xb)<<37)|(UINT64_C(0x0)<<36)),
    201, { "qp", "f1", "f3", "f4", "f2" } },
  { "fnma c", 2, ((UINT64_C(0xf)<<37)),
    ((UINT64_C(0xc)<<37)),
    201, { "qp", "f1", "f3", "f4", "f2" } },
  { "fnma d", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xd)<<37)|(UINT64_C(0x0)<<36)),
    201, { "qp", "f1", "f3", "f4", "f2" } },
  { "for", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2e)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fpack", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x28)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fpamax", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x17)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "fpamin", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x16)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "fpcmp", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x38)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x30)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "fpcvt.fx", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3d)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x18)<<27)),
    210, { "qp", "f1", "f2" } },
  { "fpcvt.fxu", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3d)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x19)<<27)),
    210, { "qp", "f1", "f2" } },
  { "fpma", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0x9)<<37)|(UINT64_C(0x1)<<36)),
    201, { "qp", "f1", "f2", "f3", "f4" } },
  { "fpmax", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x15)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "fpmerge", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3c)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x10)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fpmin", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x14)<<27)),
    208, { "qp", "f1", "f2", "f3" } },
  { "fpms", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xb)<<37)|(UINT64_C(0x1)<<36)),
    201, { "qp", "f1", "f2", "f3", "f4" } },
  { "fpnma", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xd)<<37)|(UINT64_C(0x1)<<36)),
    201, { "qp", "f1", "f2", "f3", "f4" } },
  { "fprcpa", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x0)<<36)),
    206, { "qp", "p2", "f1", "f2", "f3" } },
  { "fprsqrta", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x1)<<36)),
    207, { "qp", "p2", "f1", "f3" } },
  { "frcpa", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x0)<<36)),
    206, { "qp", "p2", "f1", "f2", "f3" } },
  { "frsqrta", 2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x1)<<36)),
    207, { "qp", "p2", "f1", "f3" } },
  { "fselect", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)),
    ((UINT64_C(0xe)<<37)|(UINT64_C(0x0)<<36)),
    203, { "qp", "f1", "f3", "f4", "f2" } },
  { "fsetc", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x04)<<27)),
    212, { "qp" } },
  { "fswap", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3c)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x34)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fsxt", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3e)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x3c)<<27)),
    209, { "qp", "f1", "f2", "f3" } },
  { "fwb",       1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7f)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3f)<<31)),
    ((UINT64_C(0)<<37)|(UINT64_C(0)<<33)|(UINT64_C(0)<<27)|(UINT64_C(2)<<31)),
    124, { "qp" } },
  { "fxor", 2,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2f)<<27)),
    209, { "qp", "f1", "f2", "f3" } },

  { "getf",     1, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<27)|(UINT64_C(0x3c)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(1)<<27)|(UINT64_C(0x1c)<<30)),
    119, { "qp", "r1", "f2" } },

  { "invala M24",  1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7f)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3f)<<31)),
    ((UINT64_C(0)<<37)|(UINT64_C(0)<<33)|(UINT64_C(0)<<27)|(UINT64_C(1)<<31)),
    124, { "qp" } },
  { "invala.e M26", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3)<<31)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2)<<27)|(UINT64_C(0x1)<<31)),
    126, { "qp", "r1 s" } },
  { "invala.e M27", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3)<<31)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x3)<<27)|(UINT64_C(0x1)<<31)),
    127, { "qp", "f1 s" } },
  { "itc.d", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2e)<<27)),
    141, { "qp", "r2" } },
  { "itc.i", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2f)<<27)),
    141, { "qp", "r2" } },
  { "itr.d", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x0e)<<27)),
    142, { "qp", "r2", "r3" } },
  { "itr.i", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x0f)<<27)),
    142, { "qp", "r2", "r3" } },

  { "ld M1p1",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x30)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x00)<<30)),
    101, { "qp", "r1", "r3" } },
  { "ld M1p2",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x10)<<30)),
    101, { "qp", "r1", "r3" } },
  { "ld8.fill M1", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x1b)<<30)),
    101, { "qp", "r1", "r3" } },
  { "ld.c M1p1",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x20)<<30)),
    101, { "qp", "r1", "r3" } },
  { "ld.c M1p2",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3c)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x28)<<30)),
    101, { "qp", "r1", "r3" } },
  { "ld M2p1",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x30)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x00)<<30)),
    102, { "qp", "r1", "r3 u", "r2" } },
  { "ld M2p2",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x10)<<30)),
    102, { "qp", "r1", "r3 u" , "r2" } },
  { "ld8.fill M2", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x1b)<<30)),
    102, { "qp", "r1", "r3 u", "r2" } },
  { "ld.c M2p1",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x20)<<30)),
    102, { "qp", "r1", "r3 u", "r2" } },
  { "ld.c M2p2",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3c)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x28)<<30)),
    102, { "qp", "r1", "r3 u", "r2" } },
  { "ld M3p1",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x30)<<30)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x00)<<30)),
    103, { "qp", "r1", "r3 u" } },
  { "ld M3p2",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x10)<<30)),
    103, { "qp", "r1", "r3 u" } },
  { "ld8.fill M3", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x1b)<<30)),
    103, { "qp", "r1", "r3 u" } },
  { "ld.c M3p1",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x20)<<30)),
    103, { "qp", "r1", "r3 u" } },
  { "ld.c M3p2",   1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3c)<<30)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x28)<<30)),
    103, { "qp", "r1", "r3 u" } },

  { "ldf M6p1",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x30)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x00)<<30)),
    106, { "qp", "f1", "r3" } },
  { "ldf.fill M6",  1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x1b)<<30)),
    106, { "qp", "f1", "r3" } },
  { "ldf.c M6",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x20)<<30)),
    106, { "qp", "f1", "r3" } },
  { "ldf M7p1",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x30)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x00)<<30)),
    107, { "qp", "f1", "r3 u", "r2" } },
  { "ldf.fill M7",  1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x1b)<<30)),
    107, { "qp", "f1", "r3 u", "r2" } },
  { "ldf.c M7",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x20)<<30)),
    107, { "qp", "f1", "r3 u", "r2" } },
  { "ldf M8p1",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x30)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0x00)<<30)),
    108, { "qp", "f1", "r3 u" } },
  { "ldf.fill M8",  1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0x1b)<<30)),
    108, { "qp", "f1", "r3 u" } },
  { "ldf.c M8",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0x20)<<30)),
    108, { "qp", "f1", "r3 u" } },
  { "ldfp M11p1",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x30)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x00)<<30)),
    111, { "qp", "f1", "f2 d", "r3" } },
  { "ldfp.c M11",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x20)<<30)),
    111, { "qp", "f1", "f2 d", "r3" } },
  { "ldfp M12p1",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x30)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x00)<<30)),
    112, { "qp", "f1", "f2 d", "r3 u" } },
  { "ldfp.c M12",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x20)<<30)),
    112, { "qp", "f1", "f2 d", "r3 u" } },
  { "lfetch M13",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3c)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x2c)<<30)),
    113, { "qp", "r3" } },
  { "lfetch M14",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3c)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x0)<<27)|(UINT64_C(0x2c)<<30)),
    114, { "qp", "r3 u", "r2" } },
  { "lfetch M15",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3c)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0x2c)<<30)),
    115, { "qp", "r3 u" } },
  { "loadrs",     1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3)<<31)|(UINT64_C(0xf)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0xa)<<27)|(UINT64_C(0x0)<<31)),
    125, {} },

  { "mix1",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(0<<28)|(UINT64_C(2)<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "mix2",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(0<<28)|(UINT64_C(2)<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "mix4",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(0<<28)|(UINT64_C(2)<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "mf.",        1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7f)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3f)<<31)),
    ((UINT64_C(0)<<37)|(UINT64_C(0)<<33)|(UINT64_C(2)<<27)|(UINT64_C(2)<<31)),
    124, { "qp" } },
  { "mf.a",      1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7f)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3f)<<31)),
    ((UINT64_C(0)<<37)|(UINT64_C(0)<<33)|(UINT64_C(3)<<27)|(UINT64_C(2)<<31)),
    124, { "qp" } },
  { "mov-from-AR.m", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x22)<<27)),
    131, { "qp", "r1", "ar3" } },
  { "mov-from-AR.i", 0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x32)<<27)),
    28, { "qp", "r1", "ar3" } },
  { "mov-from-BR", 0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x31)<<27)),
    22, { "qp", "r1" , "b2" } },
  { "mov-from-CR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x24)<<27)),
    133, { "qp", "cr3", "r1" } },
  { "mov-from-IND-RR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x10)<<27)),
    143, { "qp", "r1", "r3" } },
  { "mov-from-IND-DBR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x11)<<27)),
    143, { "qp", "r1", "r3" } },
  { "mov-from-IND-IBR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x12)<<27)),
    143, { "qp", "r1", "r3" } },
  { "mov-from-IND-PKR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x13)<<27)),
    143, { "qp", "r1", "r3" } },
  { "mov-from-IND-PMC", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x14)<<27)),
    143, { "qp", "r1", "r3" } },
  { "mov-from-IND-PMD", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x15)<<27)),
    143, { "qp", "r1", "r3" } },
  { "mov-from-IND-CPUID", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x17)<<27)),
    143, { "qp", "r1", "r3" } },
  { "mov-from-PR", 0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x33)<<27)), 
    25, { "qp", "r1" } },
  { "mov-from-IP", 0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x30)<<27)), 
    25, { "qp", "r1" } },
  { "mov-from-PSR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x25)<<27)),
    136, { "qp", "r1" } },
  { "mov-from-PSR-um", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x21)<<27)),
    136, { "qp", "r1" } },
  { "mov-to-AR.m M29", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2a)<<27)),
    129, { "qp", "r2", "ar3 d" } }, 
  { "mov-to-AR.m M30", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|
			    (UINT64_C(0x3)<<31)|(UINT64_C(0xf)<<27)),
    ((UINT64_C(0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2)<<31)|(UINT64_C(0x8)<<27)),
    130, { "qp", "r1", "ar3 d" } }, 
  { "mov-to-AR.i I26", 0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2a)<<27)),
    26, { "qp", "r2", "ar3 d" } }, 
  { "mov-to-AR.i I27", 0,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0xa)<<27)),
    27, { "qp", "ar3 d" } }, 
  { "mov-to-BR", 0,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x7)<<33)),
    21, { "qp", "r2", "b1" } },
  { "mov-to-CR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2c)<<27)),
    132, { "qp", "cr3 d", "r2" } },
  { "mov-to-IND-RR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x00)<<27)),
    142, { "qp", "r2", "r3" } },
  { "mov-to-IND-DBR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x01)<<27)),
    142, { "qp", "r2", "r3" } },
  { "mov-to-IND-IBR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x02)<<27)),
    142, { "qp", "r2", "r3" } },
  { "mov-to-IND-PKR", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x03)<<27)),
    142, { "qp", "r2", "r3" } },
  { "mov-to-IND-PMC", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x04)<<27)),
    142, { "qp", "r2", "r3" } },
  { "mov-to-IND-PMD", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x05)<<27)),
    142, { "qp", "r2", "r3" } },
  { "mov-to-PR I23", 0,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x3)<<33)),
    23, { "qp", "r2" } },
  { "mov-to-PR I24", 0,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x2)<<33)),
    24, { "qp" } },
  { "mov-to-PSR-l", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x2d)<<27)),
    135, { "qp", "r2" } },
  { "mov-to-PSR-um", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x29)<<27)),
    135, { "qp", "r2" } },
  { "movl",  4, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<20)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x0)<<20)),
    402, { "qp", "r1" } },
  { "mux1",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(3)<<34)|(INT64_C(2)<<28)|(INT64_C(2)<<30)),
    3, { "qp", "r1", "r2" } },
  { "mux2",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(3)<<34)|(INT64_C(2)<<28)|(INT64_C(2)<<30)),
    4, { "qp", "r1", "r2" } },

  { "nop.b",  3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0x2)<<37)|(UINT64_C(0)<<27)),
    309, { "qp" } },
  { "nop.f",  2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1)<<27)),
    215, { "qp" } },
  { "nop.i",  0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1)<<27)),
    19, { "qp" } },
  { "nop.m",  1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3)<<31)), 
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1)<<27)|(UINT64_C(0x0)<<31)),
    137, { "qp" } },
  { "nop.x",  4, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1)<<27)),
    401, { "qp" } },

  { "or A1",     5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		     (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x3)<<29)|(UINT64_C(0x2)<<27)),
    501, { "qp", "r1", "r2", "r3" } },
  { "or A3",     5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		     (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0xb)<<29)|(UINT64_C(0x2)<<27)),
    503, { "qp", "r1", "r3" } },

  { "pack2.uss",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(0<<28)|(0<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "pack2.sss",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(UINT64_C(2)<<28)|(0<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "pack4.sss",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(UINT64_C(2)<<28)|(0<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "padd1.",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(0)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "padd1.sss", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(0)<<29)|(UINT64_C(1)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "padd1.uuu", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(0)<<29)|(UINT64_C(2)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "padd1.uus", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(0)<<29)|(UINT64_C(3)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "padd2.",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(0)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "padd2.sss", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(0)<<29)|(UINT64_C(1)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "padd2.uuu", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(0)<<29)|(UINT64_C(2)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "padd2.uus", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(0)<<29)|(UINT64_C(3)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "padd4",     5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|(UINT64_C(0)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pavg1.",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(2)<<29)|(UINT64_C(2)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pavg1.raz", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(2)<<29)|(UINT64_C(3)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pavg2.",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(2)<<29)|(UINT64_C(2)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pavg2.raz", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(2)<<29)|(UINT64_C(3)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pavgsub1",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(3)<<29)|(UINT64_C(2)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pavgsub2",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(3)<<29)|(UINT64_C(2)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pcmp1.eq",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(9)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pcmp1.gt",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(9)<<29)|(UINT64_C(1)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pcmp2.eq",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(9)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pcmp2.gt",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(9)<<29)|(UINT64_C(1)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pcmp4.eq",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|(UINT64_C(9)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pcmp4.gt",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|(UINT64_C(9)<<29)|(UINT64_C(1)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "pmax1.u",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(1<<28)|(1<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "pmax2",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(3<<28)|(1<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "pmin1.u",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(1<<28)|(0<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "pmin2",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(UINT64_C(3)<<28)|(0<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "pmpy2",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "pmpyshr2",  0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(0)<<34)|(UINT64_C(1)<<28)),
    1, { "qp", "r1", "r2", "r3" } },
  { "popcnt",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(1)<<34)|(INT64_C(1)<<28)|(INT64_C(2)<<30)),
    9, { "qp", "r1", "r3" } },
  { "probe M38", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3e)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x38)<<27)),
    138, { "qp", "r1", "r2", "r3" } },
  { "probe M39", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3e)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x18)<<27)),
    139, { "qp", "r1", "r3" } },
  { "probe M40p1", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x31)<<27)),
    140, { "qp", "r3" } },
  { "probe M40p2", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3e)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x32)<<27)),
    140, { "qp", "r3" } },
  { "psad1",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(INT64_C(3)<<28)|(INT64_C(2)<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "pshl2 I7",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(0)<<34)|(INT64_C(0)<<28)|(INT64_C(1)<<30)),
    7, { "qp", "r1", "r2", "r3" } },
  { "pshl2 I8",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(3)<<34)|(INT64_C(1)<<28)|(INT64_C(1)<<30)),
    8, { "qp", "r1", "r2" } },
  { "pshl4 I7",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(0)<<34)|(INT64_C(0)<<28)|(INT64_C(1)<<30)),
    7, { "qp", "r1", "r2", "r3" } },
  { "pshl4 I8",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(3)<<34)|(INT64_C(1)<<28)|(INT64_C(1)<<30)),
    8, { "qp", "r1", "r2" } },
  { "pshladd2",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(4)<<29)),
    510, { "qp", "r1", "r2", "r3" } },
  { "pshr2 I5",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(0)<<34)|(INT64_C(0)<<28)|(INT64_C(0)<<30)),
    5, { "qp", "r1", "r2", "r3" } },
  { "pshr2 I6",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(1)<<34)|(INT64_C(1)<<28)|(INT64_C(0)<<30)),
    6, { "qp", "r1", "r3" } },
  { "pshr4 I5",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(0)<<34)|(INT64_C(0)<<28)|(INT64_C(0)<<30)),
    5, { "qp", "r1", "r2", "r3" } },
  { "pshr4 I6",    0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(1)<<34)|(INT64_C(1)<<28)|(INT64_C(0)<<30)),
    6, { "qp", "r1", "r3" } },
  { "pshradd2",  5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(6)<<29)),
    510, { "qp", "r1", "r2", "r3" } },
  { "psub1.",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(1)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "psub1.sss", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(1)<<29)|(UINT64_C(1)<<27)),
    509, { "qp", "r1", "r2", "r3" } }, 
  { "psub1.uuu", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(1)<<29)|(UINT64_C(2)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "psub1.uus", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|(UINT64_C(1)<<29)|(UINT64_C(3)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "psub2.",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(1)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "psub2.sss", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(1)<<29)|(UINT64_C(1)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "psub2.uuu", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(1)<<29)|(UINT64_C(2)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "psub2.uus", 5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|(UINT64_C(1)<<29)|(UINT64_C(3)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "psub4",     5, ((UINT64_C(0xf)<<37)|(UINT64_C(3)<<34)|(UINT64_C(1)<<36)|
		     (UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)|(UINT64_C(3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(1)<<34)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|(UINT64_C(1)<<29)|(UINT64_C(0)<<27)),
    509, { "qp", "r1", "r2", "r3" } },
  { "ptc.e", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x34)<<27)),
    128, { "qp", "r3" } },
  { "ptc.g", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x0a)<<27)),
    145, { "qp", "r3", "r2" } },
  { "ptc.ga", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x0b)<<27)),
    145, { "qp", "r3", "r2" } },
  { "ptc.l", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x09)<<27)),
    145, { "qp", "r3", "r2" } },
  { "ptr.d", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x0c)<<27)),
    145, { "qp", "r3", "r2" } },
  { "ptr.i", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x0d)<<27)),
    145, { "qp", "r3", "r2" } },

  { "rfi",       3, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<27)), 
    ((UINT64_C(0x0)<<37)|UINT64_C(0x08)<<27),
    308, {} },
  { "rsm", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0xf)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x7)<<27)),
    144, { "qp" } },
  { "rum", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0xf)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x5)<<27)),
    144, { "qp" } },

  { "setf",     1, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<27)|(UINT64_C(0x3c)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(1)<<27)|(UINT64_C(0x1c)<<30)),
    118, { "qp", "f1", "r2" } },
  { "shl",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(3)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(0)<<34)|(INT64_C(0)<<28)|(INT64_C(1)<<30)),
    7, { "qp", "r1", "r2", "r3" } },
  { "shladd",   5, ((0xfuLL<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x4)<<29)),
    502, { "qp", "r1", "r2", "r3" } },
  { "shladdp4",   5, ((0xfuLL<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(1)<<33)|(UINT64_C(0xf)<<29)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x6)<<29)),
    502, { "qp", "r1", "r2", "r3" } },
  { "shr",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(0)<<34)|(INT64_C(0)<<28)|(INT64_C(0)<<30)),
    5, { "qp", "r1", "r2", "r3" } },
  { "shrp",     0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(1)<<33)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x0)<<33)),
    10, { "qp", "r1", "r2", "r3" } },
  { "srlz.d",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7f)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3f)<<31)),
    ((UINT64_C(0)<<37)|(UINT64_C(0)<<33)|(UINT64_C(0)<<27)|(UINT64_C(3)<<31)),
    124, { "qp" } },
  { "srlz.i",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7f)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3f)<<31)),
    ((UINT64_C(0)<<37)|(UINT64_C(0)<<33)|(UINT64_C(1)<<27)|(UINT64_C(3)<<31)),
    124, { "qp" } },
  { "ssm", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0xf)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x6)<<27)),
    144, { "qp" } },
  { "st. M4",  1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0)<<36)|(0x0<<27)|(UINT64_C(0x30)<<30)),
    104, { "qp", "r3", "r2" } },
  { "st8.spill M4", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0)<<36)|(0x0<<27)|(UINT64_C(0x3b)<<30)),
    104, { "qp", "r3", "r2" } },
  { "st. M5", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x30)<<30)),
    105, { "qp", "r3 u", "r2" } },
  { "st8.spill M5", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0x3b)<<30)),
    105, { "qp", "r3 u", "r2" } },
  { "stf. M9",  1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0)<<36)|(0x0<<27)|(UINT64_C(0x30)<<30)),
    109, { "qp", "r3", "f2" } },
  { "stf.spill M9", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x6)<<37)|(UINT64_C(0)<<36)|(0x0<<27)|(UINT64_C(0x3b)<<30)),
    109, { "qp", "r3", "f2" } },
  { "stf. M10", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x38)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0x30)<<30)),
    110, { "qp", "r3 u", "f2" } },
  { "stf.spill M10", 1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0x3b)<<30)),
    110, { "qp", "r3 u", "f2" } },
  { "sub A1",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		     (UINT64_C(0xf)<<29)|(UINT64_C(0x2)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1)<<29)|(UINT64_C(0x0)<<27)),
    501, { "qp", "r1", "r2", "r3" } },
  { "sub A3",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		     (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x9)<<29)|(UINT64_C(0x1)<<27)),
    503, { "qp", "r1", "r3" } },
  { "sum", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0xf)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x4)<<27)),
    144, { "qp" } },
  { "sxt1",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x14)<<27)),
    29, { "qp", "r1", "r3" } },
  { "sxt2",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x15)<<27)),
    29, { "qp", "r1", "r3" } },
  { "sxt4",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x16)<<27)),
    29, { "qp", "r1", "r3" } },
  { "sync.i",    1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7f)<<33)|(UINT64_C(0xf)<<27)|(UINT64_C(0x3f)<<31)),
    ((UINT64_C(0)<<37)|(UINT64_C(0)<<33)|(UINT64_C(3)<<27)|(UINT64_C(3)<<31)),
    124, { "qp" } },


  { "tak", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1f)<<27)),
    146, { "qp", "r1", "r3" } },
  { "tbit",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<13)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0)<<34)|(UINT64_C(0)<<13)),
    16, { "qp", "p1", "p2", "r3" } },
  { "thash", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1a)<<27)),
    146, { "qp", "r1", "r3" } },
  { "tnat",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<13)),
    ((UINT64_C(0x5)<<37)|(UINT64_C(0)<<34)|(UINT64_C(1)<<13)),
    17, { "qp", "p1", "p2", "r3" } },
  { "tpa", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1e)<<27)),
    146, { "qp", "r1", "r3" } },
  { "ttag", 1,  ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x1)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x1b)<<27)),
    146, { "qp", "r1", "r3" } },

  { "unpack1",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(0<<28)|(1<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "unpack2",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(0)<<36)|(UINT64_C(1)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(0<<28)|(1<<30)),
    2, { "qp", "r1", "r2", "r3" } },
  { "unpack4",   0, ((UINT64_C(0xf)<<37)|(UINT64_C(1)<<36)|(UINT64_C(1)<<33)|
		     (UINT64_C(1)<<32)|(UINT64_C(3)<<34)|(UINT64_C(1)<<28)|(UINT64_C(3)<<30)),
    ((UINT64_C(0x7)<<37)|(UINT64_C(1)<<36)|(UINT64_C(0)<<33)|
     (UINT64_C(0)<<32)|(UINT64_C(2)<<34)|(0<<28)|(1<<30)),
    2, { "qp", "r1", "r2", "r3" } },


  { "xchg1",     1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x08)<<30)),
    116, { "qp", "r1", "r3", "r2" } },
  { "xchg2",     1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x09)<<30)),
    116, { "qp", "r1", "r3", "r2" } },
  { "xchg4",     1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x0a)<<30)),
    116, { "qp", "r1", "r3", "r2" } },
  { "xchg8",     1, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x3f)<<30)),
    ((UINT64_C(0x4)<<37)|(UINT64_C(0x0)<<36)|(UINT64_C(0x1)<<27)|(UINT64_C(0x0b)<<30)),
    116, { "qp", "r1", "r3", "r2" } },
  { "xma",       2, ((UINT64_C(0xf)<<37)|(UINT64_C(0x1)<<36)), 
    ((UINT64_C(0xe)<<37)|(UINT64_C(1)<<36)),
    202, { "qp", "f1", "f3", "f4", "f2" } },
  { "xor A1",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		     (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0x3)<<29)|(UINT64_C(0x3)<<27)),
    501, { "qp", "r1", "r2", "r3" } },
  { "xor A3",    5, ((UINT64_C(0xf)<<37)|(UINT64_C(0x3)<<34)|(UINT64_C(0x1)<<33)|
		     (UINT64_C(0xf)<<29)|(UINT64_C(0x3)<<27)),
    ((UINT64_C(0x8)<<37)|(UINT64_C(0x0)<<34)|(UINT64_C(0x0)<<33)|(UINT64_C(0xb)<<29)|(UINT64_C(0x3)<<27)),
    503, { "qp", "r1", "r3" } },

  { "zxt1",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x10)<<27)),
    29, { "qp", "r1", "r3" } },
  { "zxt2",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x11)<<27)),
    29, { "qp", "r1", "r3" } },
  { "zxt4",      0, ((UINT64_C(0xf)<<37)|(UINT64_C(0x7)<<33)|(UINT64_C(0x3f)<<27)),
    ((UINT64_C(0x0)<<37)|(UINT64_C(0x0)<<33)|(UINT64_C(0x12)<<27)),
    29, { "qp", "r1", "r3" } },

  /* additional qualifiers */

  { "field(sf)==s0", 2, ((UINT64_C(0x3)<<34)), (UINT64_C(0)<<34), 0, {}, 1 },
  { "field(sf)==s1", 2, ((UINT64_C(0x3)<<34)), (UINT64_C(1)<<34), 0, {}, 1 },
  { "field(sf)==s2", 2, ((UINT64_C(0x3)<<34)), (UINT64_C(2)<<34), 0, {}, 1 },
  { "field(sf)==s3", 2, ((UINT64_C(0x3)<<34)), (UINT64_C(3)<<34), 0, {}, 1 },

  { "field(ar3)==BSP", -1, (UINT64_C(0x7f)<<20), (UINT64_C(17)<<20), 0, {}, 1 },
  { "field(ar3)==BSPSTORE", -1, (UINT64_C(0x7f)<<20), (UINT64_C(18)<<20), 0, {}, 1 },
  { "field(ar3)==CCV", -1, (UINT64_C(0x7f)<<20), (UINT64_C(32)<<20), 0, {}, 1 },
  { "field(ar3)==EC", -1, (UINT64_C(0x7f)<<20), (UINT64_C(66)<<20), 0, {}, 1 },
  { "field(ar3)==FPSR", -1, (UINT64_C(0x7f)<<20), (UINT64_C(40)<<20), 0, {}, 1 },
  { "field(ar3)==ITC", -1, (UINT64_C(0x7f)<<20), (UINT64_C(44)<<20), 0, {}, 1 },
  { "field(ar3)==K%", -1, (UINT64_C(0x78)<<20), (UINT64_C(0)<<20), 0, {}, 1 },
  { "field(ar3)==LC", -1, (UINT64_C(0x7f)<<20), (UINT64_C(65)<<20), 0, {}, 1 },
  { "field(ar3)==PFS", -1, (UINT64_C(0x7f)<<20), (UINT64_C(64)<<20), 0, {}, 1 },
  { "field(ar3)==RNAT", -1, (UINT64_C(0x7f)<<20), (UINT64_C(19)<<20), 0, {}, 1 },
  { "field(ar3)==RSC", -1, (UINT64_C(0x7f)<<20), (UINT64_C(16)<<20), 0, {}, 1 },
  { "field(ar3)==UNAT", -1, (UINT64_C(0x7f)<<20), (UINT64_C(36)<<20), 0, {}, 1 },

  { "field(cr3)==CMCV", -1, (UINT64_C(0x7f)<<20), (UINT64_C(74)<<20), 0, {}, 1 },
  { "field(cr3)==DCR", -1, (UINT64_C(0x7f)<<20), (UINT64_C(0)<<20), 0, {}, 1 },
  { "field(cr3)==EOI", -1, (UINT64_C(0x7f)<<20), (UINT64_C(67)<<20), 0, {}, 1 },
  { "field(cr3)==GPTA", -1, 0/*(UINT64_C(0x7f)<<20)*/, (UINT64_C(1)<<20), 0, {}, 1 },/*?*/
  { "field(cr3)==IFA", -1, (UINT64_C(0x7f)<<20), (UINT64_C(20)<<20), 0, {}, 1 },
  { "field(cr3)==IFS", -1, (UINT64_C(0x7f)<<20), (UINT64_C(23)<<20), 0, {}, 1 },
  { "field(cr3)==IHA", -1, (UINT64_C(0x7f)<<20), (UINT64_C(25)<<20), 0, {}, 1 },
  { "field(cr3)==IIM", -1, (UINT64_C(0x7f)<<20), (UINT64_C(24)<<20), 0, {}, 1 },
  { "field(cr3)==IIP", -1, (UINT64_C(0x7f)<<20), (UINT64_C(19)<<20), 0, {}, 1 },
  { "field(cr3)==IIPA", -1, (UINT64_C(0x7f)<<20), (UINT64_C(22)<<20), 0, {}, 1 },
  { "field(cr3)==IPSR", -1, (UINT64_C(0x7f)<<20), (UINT64_C(16)<<20), 0, {}, 1 },
  { "field(cr3)==IRR", -1, (UINT64_C(0x7c)<<20), (UINT64_C(68)<<20), 0, {}, 1 },
  { "field(cr3)==ISR", -1, (UINT64_C(0x7f)<<20), (UINT64_C(17)<<20), 0, {}, 1 },
  { "field(cr3)==ITIR", -1, (UINT64_C(0x7f)<<20), (UINT64_C(21)<<20), 0, {}, 1 },
  { "field(cr3)==ITM", -1, (UINT64_C(0x7f)<<20), (UINT64_C(1)<<20), 0, {}, 1 },
  { "field(cr3)==ITV", -1, (UINT64_C(0x7f)<<20), (UINT64_C(72)<<20), 0, {}, 1 },
  { "field(cr3)==IVA", -1, (UINT64_C(0x7f)<<20), (UINT64_C(2)<<20), 0, {}, 1 },
  { "field(cr3)==IVR", -1, (UINT64_C(0x7f)<<20), (UINT64_C(65)<<20), 0, {}, 1 },
  { "field(cr3)==LID", -1, (UINT64_C(0x7f)<<20), (UINT64_C(64)<<20), 0, {}, 1 },
  { "field(cr3)==LRR", -1, (UINT64_C(0x7e)<<20), (UINT64_C(80)<<20), 0, {}, 1 },
  { "field(cr3)==PMV", -1, (UINT64_C(0x7f)<<20), (UINT64_C(73)<<20), 0, {}, 1 },
  { "field(cr3)==PTA", -1, (UINT64_C(0x7f)<<20), (UINT64_C(8)<<20), 0, {}, 1 },
  { "field(cr3)==TPR", -1, (UINT64_C(0x7f)<<20), (UINT64_C(66)<<20), 0, {}, 1 },

  { "field(lftype)==fault", -1, (UINT64_C(0x3e)<<30), (UINT64_C(0x2e)<<30), 0, {}, 1},

  { "fp s", -1, (UINT64_C(3)<<30), (UINT64_C(2)<<30), 0, {}, 1 },
  { "fp d", -1, (UINT64_C(3)<<30), (UINT64_C(3)<<30), 0, {}, 1 },
  { "fp 8", -1, (UINT64_C(3)<<30), (UINT64_C(1)<<30), 0, {}, 1 },
  { "fp e", -1, (UINT64_C(3)<<30), (UINT64_C(0)<<30), 0, {}, 1 },

  { "int 1", -1, (UINT64_C(3)<<30), (UINT64_C(0)<<30), 0, {}, 1 },
  { "int 2", -1, (UINT64_C(3)<<30), (UINT64_C(1)<<30), 0, {}, 1 },
  { "int 4", -1, (UINT64_C(3)<<30), (UINT64_C(2)<<30), 0, {}, 1 },
  { "int 8", -1, (UINT64_C(3)<<30), (UINT64_C(3)<<30), 0, {}, 1 },

  { "ld .s", -1, (UINT64_C(0x3c)<<30), (UINT64_C(0x04)<<30), 0, {}, 1 },
  { "ld .sa", -1, (UINT64_C(0x3c)<<30), (UINT64_C(0x0c)<<30), 0, {}, 1 },
};

Rtype regtests[] = {
  { "qp", 1, LSE_emu_spaceid_PR, 0, 63 },
  { "p1", 2, LSE_emu_spaceid_PR, 6, 63 },
  { "p2", 2, LSE_emu_spaceid_PR, 27, 63 },
  { "ar3", 1, LSE_emu_spaceid_AR, 20, 127 },
  { "ar3 d", 2, LSE_emu_spaceid_AR, 20, 127 },
  { "b1", 2, LSE_emu_spaceid_BR, 6, 7 },
  { "b2", 1, LSE_emu_spaceid_BR, 13, 7 },
  { "cr3", 1, LSE_emu_spaceid_CR, 20, 127 },
  { "cr3 d", 2, LSE_emu_spaceid_CR, 20, 127 },
  { "r1", 2, LSE_emu_spaceid_GR, 6, 127 },
  { "r1 s", 1, LSE_emu_spaceid_GR, 6, 127 },
  { "r2", 1, LSE_emu_spaceid_GR, 13, 127 }, 
  { "r3", 1, LSE_emu_spaceid_GR, 20, 127 },
  { "r3 u", 3, LSE_emu_spaceid_GR, 20, 127 },
  { "r3small", 1, LSE_emu_spaceid_GR, 20, 3 },
  { "f1", 2, LSE_emu_spaceid_FR, 6, 127 },
  { "f1 s", 1, LSE_emu_spaceid_FR, 6, 127 },
  { "f2", 1, LSE_emu_spaceid_FR, 13, 127 },
  { "f2 d", 2, LSE_emu_spaceid_FR, 13, 127 },
  { "f3", 1, LSE_emu_spaceid_FR, 20, 127 },
  { "f4", 1, LSE_emu_spaceid_FR, 27, 127 },
};

Ltype lists[] = {
  /* our own funny lists... */
  { "addp4",         { "addp4 A1", "addp4 A4" } },
  { "and",           { "and A1", "and A3" } },
  { "andcm",         { "andcm A1", "andcm A3" } },
  { "br.call",       { "br.call B3", "br.call B5" }},
  { "br.cond",       { "br.cond B1", "br.cond B4" }},
  { "brp",           { "brp B6", "brp B7" }},
  { "chk",           { "chk.a.clr", "chk.a.nc", "chk.s" } },
  { "chk.a.clr",     { "chk.a.clr M22", "chk.a.clr M23" }},
  { "chk.a.nc",      { "chk.a.nc M22", "chk.a.nc M23" }},
  { "chk.s",         { "chk.s I20", "chk.s M20", "chk.s M21" }},
  { "cmp",           { "cmp C A6", "cmp D A6", "cmp E A6",
		       "cmp C A7", "cmp D A7", "cmp E A7",
		       "cmp C A8", "cmp D A8", "cmp E A8" }},
  { "cmp4",          { "cmp4 C A6", "cmp4 D A6", "cmp4 E A6",
		       "cmp4 C A7", "cmp4 D A7", "cmp4 E A7",
		       "cmp4 C A8", "cmp4 D A8", "cmp4 E A8" }},
  { "dep",           { "dep.z I12", "dep.z I13", "dep I14", "dep I15" } },
  { "dep\\dep I13",  { "dep.z I12", "dep I14", "dep I15" } },
  /* { "fchkf.s0",       { "fchkf", "field(sf)==s0" }, 1 }, */
  { "fchkf.s1",       { "fchkf", "field(sf)==s1" }, 1 },
  { "fchkf.s2",       { "fchkf", "field(sf)==s2" }, 1 },
  { "fchkf.s3",       { "fchkf", "field(sf)==s3" }, 1 },
  { "fclrf.s0",       { "fclrf", "field(sf)==s0" }, 1 },
  { "fclrf.s1",       { "fclrf", "field(sf)==s1" }, 1 },
  { "fclrf.s2",       { "fclrf", "field(sf)==s2" }, 1 },
  { "fclrf.s3",       { "fclrf", "field(sf)==s3" }, 1 },
  { "fma",           { "fma 8", "fma 9" }},
  { "fms",           { "fms a", "fms b" }},
  { "fnma",          { "fnma c", "fnma d" }}, 
  { "fsetc.s0",      { "fsetc", "field(sf)==s0" }, 1 },
  { "fsetc.s1",      { "fsetc", "field(sf)==s1" }, 1 },
  { "fsetc.s2",      { "fsetc", "field(sf)==s2" }, 1 },
  { "fsetc.s3",      { "fsetc", "field(sf)==s3" }, 1 },
  { "invala.e",      { "invala.e M26", "invala.e M27" } },
  { "ld1",           { "ld nofill", "int 1" }, 1},
  { "ld1.s",         { "ld nofill", "int 1", "ld .s" }, 1},
  { "ld1.sa",        { "ld nofill", "int 1", "ld .sa" }, 1},
  { "ld2",           { "ld nofill", "int 2" }, 1},
  { "ld2.s",         { "ld nofill", "int 2", "ld .s" }, 1},
  { "ld2.sa",        { "ld nofill", "int 2", "ld .sa" }, 1},
  { "ld4",           { "ld nofill", "int 4" }, 1},
  { "ld4.s",         { "ld nofill", "int 4", "ld .s" }, 1},
  { "ld4.sa",        { "ld nofill", "int 4", "ld .sa" }, 1},
  { "ld8",           { "ld nofill", "int 8" }, 1},
  { "ld8.s",         { "ld nofill", "int 8", "ld .s" }, 1},
  { "ld8.sa",        { "ld nofill", "int 8", "ld .sa" }, 1},
  { "ld M1",        { "ld M1p1", "ld M1p2", "ld8.fill M1", "ld.c M1p1", 
		      "ld.c M1p2" } },
  { "ld M2",        { "ld M2p1", "ld M2p2", "ld8.fill M2", "ld.c M2p1", 
		      "ld.c M2p2" } },
  { "ld M3",        { "ld M3p1", "ld M3p2", "ld8.fill M3", "ld.c M3p1", 
		      "ld.c M3p2" } },
  { "ld nofill",     { "ld M1p1", "ld M2p1", "ld M3p1", 
		       "ld M1p2", "ld M2p2", "ld M3p2", "ld.c" } },
  { "ld.c",          { "ld.c M1p1", "ld.c M1p2", "ld.c M2p1", "ld.c M2p2",
		       "ld.c M3p1", "ld.c M3p2" } },
  { "ld8.fill",      { "ld8.fill M1", "ld8.fill M2", "ld8.fill M3" } },
  { "ldf M6",        { "ldf M6p1", "ldf.fill M6", "ldf.c M6" }},
  { "ldf M7",        { "ldf M7p1", "ldf.fill M7", "ldf.c M7" }},
  { "ldf M8",        { "ldf M8p1", "ldf.fill M8", "ldf.c M8" }},
  { "ldf.fill",      { "ldf.fill M6", "ldf.fill M7", "ldf.fill M8" } },
  { "ldf.c",         { "ldf.c M6", "ldf.c M7", "ldf.c M8" } },
  { "ldf nofill",    { "ldf M6p1", "ldf M7p1", "ldf M8p1", "ldf.c" } },
  { "ldfs",          { "ldf nofill", "fp s" }, 1},
  { "ldfs.s",        { "ldf nofill", "fp s", "ld .s" }, 1},
  { "ldfs.sa",       { "ldf nofill", "fp s", "ld .sa" }, 1},
  { "ldfd",          { "ldf nofill", "fp d" }, 1},
  { "ldfd.s",        { "ldf nofill", "fp d", "ld .s" }, 1},
  { "ldfd.sa",       { "ldf nofill", "fp d", "ld .sa" }, 1},
  { "ldf8",          { "ldf nofill", "fp 8" }, 1},
  { "ldf8.s",        { "ldf nofill", "fp 8", "ld .s" }, 1},
  { "ldf8.sa",       { "ldf nofill", "fp 8", "ld .sa" }, 1},
  { "ldfe",          { "ldf nofill", "fp e" }, 1},
  { "ldfe.s",        { "ldf nofill", "fp e", "ld .s" }, 1},
  { "ldfe.sa",       { "ldf nofill", "fp e", "ld .sa" }, 1},
  { "ldfp M11",      { "ldfp M11p1", "ldfp.c M11" }},
  { "ldfp M12",      { "ldfp M12p1", "ldfp.c M12" }},
  { "ldfp.c",        { "ldfp.c M11", "ldfp.c M12" } },
  { "ldfp",          { "ldfp M11", "ldfp M12"} },
  { "ldfps",         { "ldfp", "fp s" }, 1},
  { "ldfps.s",       { "ldfp", "fp s", "ld .s" }, 1},
  { "ldfps.sa",      { "ldfp", "fp s", "ld .sa" }, 1},
  { "ldfpd",         { "ldfp", "fp d" }, 1},
  { "ldfpd.s",       { "ldfp", "fp d", "ld .s" }, 1},
  { "ldfpd.sa",      { "ldfp", "fp d", "ld .sa" }, 1},
  { "ldfp8",         { "ldfp", "fp 8" }, 1},
  { "ldfp8.s",       { "ldfp", "fp 8", "ld .s" }, 1},
  { "ldfp8.sa",      { "ldfp", "fp 8", "ld .sa" }, 1},
  { "mf",            { "mf.", "mf.a" } },
  { "mov-to-PR",     { "mov-to-PR I23", "mov-to-PR I24" }},
  { "or",            { "or A1", "or A3" } },
  { "pack2",         { "pack2.uss", "pack2.sss" } },
  { "pack4",         { "pack4.sss" } },
  { "padd1",         { "padd1.", "padd1.sss", "padd1.uus", "padd1.uuu" } },
  { "padd2",         { "padd2.", "padd2.sss", "padd2.uus", "padd2.uuu" } },
  { "pavg1",         { "pavg1.", "pavg1.raz" } },
  { "pavg2",         { "pavg2.", "pavg2.raz" } },
  { "pcmp1",         { "pcmp1.eq", "pcmp1.gt" } },
  { "pcmp2",         { "pcmp2.eq", "pcmp2.gt" } },
  { "pcmp4",         { "pcmp4.eq", "pcmp4.gt" } },
  { "pmax1",         { "pmax1.u" } },
  { "pmin1",         { "pmin1.u" } },
  { "probe M40",     { "probe M40p1", "probe M40p2" } },
  { "pshl2",         { "pshl2 I7", "pshl2 I8" } },
  { "pshl4",         { "pshl4 I7", "pshl4 I8" } },
  { "pshr2",         { "pshr2 I5", "pshr2 I6" } },
  { "pshr4",         { "pshr4 I5", "pshr4 I6" } },
  { "psub1",         { "psub1.", "psub1.sss", "psub1.uus", "psub1.uuu" } },
  { "psub2",         { "psub2.", "psub2.sss", "psub2.uus", "psub2.uuu" } },
  { "st M4",         { "st. M4", "st8.spill M4" }},
  { "st M5",         { "st. M5", "st8.spill M5" }},
  { "st nospill",    { "st. M4", "st. M5" } },
  { "st8.spill",     { "st8.spill M4", "st8.spill M5" } },
  { "st1",           { "st nospill", "int 1" }, 1},
  { "st2",           { "st nospill", "int 2" }, 1},
  { "st4",           { "st nospill", "int 4" }, 1},
  { "st8",           { "st nospill", "int 8" }, 1},
  { "stfs",          { "stf nospill", "fp s" }, 1},
  { "stfd",          { "stf nospill", "fp d" }, 1},
  { "stf8",          { "stf nospill", "fp 8" }, 1},
  { "stfe",          { "stf nospill", "fp e" }, 1},
  { "stf M9",        { "stf. M9", "stf.spill M9" }},
  { "stf M10",       { "stf. M10", "stf.spill M10" }},
  { "stf nospill",   { "stf. M9", "stf. M10" } },
  { "stf.spill",     { "stf.spill M9", "stf.spill M10" } },

  { "sub",           { "sub A1", "sub A3" } },
  { "sync",          { "sync.i" } },
  { "xor",           { "xor A1", "xor A3" } },

  /* lists from book */
  { "all",           { "predicatable-instructions", 
		       "unpredicatable-instructions" } },
  { "branches",      { "indirect-brs", "ip-rel-brs" } },
  { "cfm-readers",   { "fr-readers", "fr-writers", "gr-readers", "gr-writers",
		       "mod-sched-brs", "predicatable-instructions",
		       "pr-writers","alloc", "br.call", "brl.call", 
		       "br.ret", "cover", "loadrs", "rfi", "chk-a", "invala.e",
  }},
  { "chk-a" ,        { "chk.a.clr", "chk.a.nc" }},
  { "czx",           { "czx1", "czx2" } },
  { "fcmp-s0",       { "fcmp", "field(sf)==s0" }, 1 },
  { "fcmp-s1",       { "fcmp", "field(sf)==s1" }, 1 },
  { "fcmp-s2",       { "fcmp", "field(sf)==s2" }, 1 },
  { "fcmp-s3",       { "fcmp", "field(sf)==s3" }, 1 },
  { "fetchadd",      { "fetchadd4", "fetchadd8" } },
  { "fp-arith",      { "famax", "famin", "fcvt.fx", 
		       "fcvt.fxu", "fma", "fmax", 
		       "fmin", "fms", "fnma", 
		       "fpamax", "fpamin", "fpcvt.fx", 
		       "fpcvt.fxu", "fpma", "fpmax", "fpmin", 
		       "fpms", "fpnma", 
		       "fprcpa", "fprsqrta", "frcpa", "frsqrta",
		       /* "fnorm", "fadd", "fmpy", "fnmpy", "fpmpy", "fpnmpy",
			  "fsub" are psuedo-ops */ }},
  { "fp-arith-s0",   { "fp-arith", "field(sf)==s0" }, 1 },
  { "fp-arith-s1",   { "fp-arith", "field(sf)==s1" }, 1 },
  { "fp-arith-s2",   { "fp-arith", "field(sf)==s2" }, 1 },
  { "fp-arith-s3",   { "fp-arith", "field(sf)==s3" }, 1 },
  { "fp-non-arith",  { "fand", "fandcm", "fclass", "fcvt.xf", 
		       "fmerge", "fmix", "for", "fpmerge", "fpack", 
		       "fselect", "fswap", "fsxt", "fxor", "xma",
		       /* fabs, fneg, fnegabs, fpabs, fpneg, fpnegabs 
			  are pseudo-ops */ }},
  { "fpcmp-s0",      { "fpcmp", "field(sf)==s0" }, 1 },
  { "fpcmp-s1",      { "fpcmp", "field(sf)==s1" }, 1 },
  { "fpcmp-s2",      { "fpcmp", "field(sf)==s2" }, 1 },
  { "fpcmp-s3",      { "fpcmp", "field(sf)==s3" }, 1 },
  { "fr-readers",    { "fp-arith", "fp-non-arith", "pr-writers-fp",
		       "chk.s M21", "getf", 
		       /* omitted ones.... */
		       "fpcmp", "stf"  } },
  { "fr-writers",    { "fp-arith", /* copy fp-non-arith - fclass */
		       "fand", "fandcm", "fclass", "fcvt.xf", 
		       "fmerge", "fmix", "for", "fpmerge", "fpack", 
		       "fselect", "fswap", "fsxt", "fxor", "xma",
		       "mem-readers-fp", 
		       "fpcmp", "setf" /* ADD: omitted? */ } },
  { "gr-readers",    { "gr-readers-writers", "mem-readers", "mem-writers",
		       "chk.s", "cmp", "cmp4", "fc", "itc.i", "itc.d", "itr.i",
		       "itr.d", "mov-to-AR-gr", "mov-to-BR", "mov-to-CR",
		       "mov-to-IND", "mov-from-IND", "mov-to-PR-allreg",
		       "mov-to-PSR-l", "mov-to-PSR-um", "probe-all", "ptc.e",
		       "ptc.g", "ptc.ga", "ptc.l", "ptr.i", "ptr.d", 
		       "setf", "tbit", "tnat",
		       "lfetch-all" /* left out? */} },
  { "gr-readers-writers", { "mov-from-IND", "add", "addl", "addp4", "adds",
			    "and", "andcm", "czx", "dep\\dep I13", "extr",
			    "mem-readers-int", "ld-all-postinc", 
			    "lfetch-postinc", "mix", "mux", "or", "pack",
			    "padd", "pavg", "pavgsub", "pcmp", "pmax",
			    "pmin", "pmpy", "pmpyshr", "popcnt", 
			    "probe-nofault", "psad", "pshl", "pshladd",
			    "pshr", "pshradd", "psub", "shl", "shladd",
			    "shladdp4", "shr", "shrp", "st-postinc",
			    "sub", "sxt", "tak", "thash", "tpa", "ttag",
			    "unpack", "xor", "zxt" } },
  { "gr-writers",    { "alloc", "dep", "getf", "gr-readers-writers", 
		       "mem-readers-int", "mov-from-AR", "mov-from-BR",
		       "mov-from-CR", "mov-from-PR", "mov-immediate",
		       "mov-from-PSR", "mov-from-PSR-um", "mov-ip", "movl" } },
  { "indirect-brp",  { "brp B7" } },
  { "indirect-brs",  { "br.call B5", "br.cond B4", "br.ia", "br.ret"}},
  { "invala-all",    { "invala M24", "invala.e" } },
  { "ip-rel-brs",    { "mod-sched-brs", "br.call B3", "brl.call", "brl.cond",
		       "br.cond B1", "br.cloop" } },
  { "ld",            { "ld1", "ld2", "ld4", "ld8", "ld8.fill" }},
  { "ld-all-postinc", { "ld M2", "ld M3", "ldfp M12", "ldf M7", "ldf M8"}},
  { "ld-s",          { "ld1.s", "ld2.s", "ld4.s", "ld8.s" } },
  { "ld-sa",         { "ld1.sa", "ld2.sa", "ld4.sa", "ld8.sa" } },
  { "ldf",           { "ldfs", "ldfd", "ldfe", "ldf8", "ldf.fill" }},
  { "ldf-s",         { "ldfs.s", "ldfd.s", "ldfe.s", "ldf8.s" } },
  { "ldf-sa",        { "ldfs.sa", "ldfd.sa", "ldfe.sa", "ldf8.sa" } },
  { "ldfp",          { "ldfps", "ldfpd", "ldfp8" }},
  { "ldfp-s",        { "ldfps.s", "ldfpd.s", "ldfp8.s" }},
  { "ldfp-sa",       { "ldfps.sa", "ldfpd.sa", "ldfp8.sa" }},
  { "lfetch-all",    { "lfetch M13", "lfetch M14", "lfetch M15" }},
  { "lfetch-fault",  { "lfetch-all", "field(lftype)==fault"},1},
  { "lfetch-postinc",  { "lfetch M14", "lfetch M15" }},
  { "mem-readers",   { "mem-readers-fp", "mem-readers-int" }},
  { "mem-readers-spec", { "ld-s", "ld-sa", "ldf-s", "ldf-sa", "ldfp-s", 
			  "ldfp-sa" } },
  { "mem-readers-fp", { "ldf", "ldfp" }},
  { "mem-readers-int", { "cmpxchg", "fetchadd", "xchg", "ld" }},
  { "mem-writers",   { "mem-writers-fp", "mem-writers-int" }},
  { "mem-writers-fp", { "stf" }},
  { "mem-writers-int", { "cmpxchg", "fetchadd", "xchg", "st" }},
  { "mix",           { "mix1", "mix2", "mix4" } },
  { "mod-sched-brs", { "br.cexit", "br.ctop", "br.wexit", "br.wtop" }},
  { "mod-sched-brs-counted", { "br.cexit", "br.cloop", "br.ctop" }},
  { "mov-from-AR",      { "mov-from-AR-M", "mov-from-AR-I" }},
  { "mov-from-AR-BSP",  { "mov-from-AR.m", "field(ar3)==BSP"}, 1},
  { "mov-from-AR-BSPSTORE", { "mov-from-AR.m", "field(ar3)==BSPSTORE"}, 1},
  { "mov-from-AR-CCV",  { "mov-from-AR.m", "field(ar3)==CCV"}, 1},
  { "mov-from-AR-EC",   { "mov-from-AR.i", "field(ar3)==EC"}, 1},
  { "mov-from-AR-FPSR", { "mov-from-AR.m", "field(ar3)==FPSR"}, 1},
  { "mov-from-AR-I",    { "mov-from-AR.i" } },
  { "mov-from-AR-ig",   { "mov-from-AR", "ar3"}, 1},
  { "mov-from-AR-IM",   { "mov-from-AR.i", "mov-from-AR.m" } },
  { "mov-from-AR-ITC",  { "mov-from-AR.m", "field(ar3)==ITC"}, 1},
  { "mov-from-AR-K",    { "mov-from-AR.m", "field(ar3)==K%"}, 1},
  { "mov-from-AR-LC",   { "mov-from-AR.i", "field(ar3)==LC"}, 1},
  { "mov-from-AR-M",    { "mov-from-AR.m" } },
  { "mov-from-AR-PFS",  { "mov-from-AR.i", "field(ar3)==PFS"}, 1},
  { "mov-from-AR-RNAT", { "mov-from-AR.m", "field(ar3)==RNAT"}, 1},
  { "mov-from-AR-RSC",  { "mov-from-AR.m", "field(ar3)==RSC"}, 1},
  { "mov-from-AR-rv",   { } },
  { "mov-from-AR-UNAT", { "mov-from-AR.m", "field(ar3)==UNAT"}, 1},
  { "mov-from-CR-CMCV", { "mov-from-CR", "field(cr3)==CMCV"}, 1},
  { "mov-from-CR-DCR", { "mov-from-CR", "field(cr3)==DCR"}, 1},
  { "mov-from-CR-EOI", { "mov-from-CR", "field(cr3)==EOI"}, 1},
  { "mov-from-CR-GPTA", { "mov-from-CR", "field(cr3)==GPTA"}, 1},
  { "mov-from-CR-IFA", { "mov-from-CR", "field(cr3)==IFA"}, 1},
  { "mov-from-CR-IFS", { "mov-from-CR", "field(cr3)==IFS"}, 1},
  { "mov-from-CR-IHA", { "mov-from-CR", "field(cr3)==IHA"}, 1},
  { "mov-from-CR-IIM", { "mov-from-CR", "field(cr3)==IIM"}, 1},
  { "mov-from-CR-IIP", { "mov-from-CR", "field(cr3)==IIP"}, 1},
  { "mov-from-CR-IIPA", { "mov-from-CR", "field(cr3)==IIPA"}, 1},
  { "mov-from-CR-IPSR", { "mov-from-CR", "field(cr3)==IPSR"}, 1},
  { "mov-from-CR-IRR", { "mov-from-CR", "field(cr3)==IRR"}, 1},
  { "mov-from-CR-ISR", { "mov-from-CR", "field(cr3)==ISR"}, 1},
  { "mov-from-CR-ITIR", { "mov-from-CR", "field(cr3)==ITIR"}, 1},
  { "mov-from-CR-ITM", { "mov-from-CR", "field(cr3)==ITM"}, 1},
  { "mov-from-CR-ITV", { "mov-from-CR", "field(cr3)==ITV"}, 1},
  { "mov-from-CR-IVA", { "mov-from-CR", "field(cr3)==IVA"}, 1},
  { "mov-from-CR-IVR", { "mov-from-CR", "field(cr3)==IVR"}, 1},
  { "mov-from-CR-LID", { "mov-from-CR", "field(cr3)==LID"}, 1},
  { "mov-from-CR-LRR", { "mov-from-CR", "field(cr3)==LRR"}, 1},
  { "mov-from-CR-PMV", { "mov-from-CR", "field(cr3)==PMV"}, 1},
  { "mov-from-CR-PTA", { "mov-from-CR", "field(cr3)==PTA"}, 1},
  { "mov-from-CR-rv", { } },
  { "mov-from-CR-TPR", { "mov-from-CR", "field(cr3)==TPR"}, 1},
  { "mov-from-IND",     { "mov-from-IND-RR", "mov-from-IND-DBR", 
			  "mov-from-IND-IBR", "mov-from-IND-PKR", 
			  "mov-from-IND-PMC", "mov-from-IND-PMD",
			  "mov-from-IND-CPUID" }},
  { "mov-from-IND-priv", { "mov-from-IND-RR", "mov-from-IND-DBR", 
			  "mov-from-IND-IBR", "mov-from-IND-PKR", 
			  "mov-from-IND-PMC" }},
  { "mov-immediate",    { "addl"/*in A5 */ }},
  { "mov-ip",           { "mov-from-IP" } },
  { "mov-to-AR",        { "mov-to-AR-I", "mov-to-AR-M"} },
  { "mov-to-AR-BSPSTORE", { "mov-to-AR-M", "field(ar3)==BSPSTORE"}, 1},
  { "mov-to-AR-CCV",  { "mov-to-AR-M", "field(ar3)==CCV"}, 1},
  { "mov-to-AR-EC",   { "mov-to-AR-I", "field(ar3)==EC"}, 1},
  { "mov-to-AR-FPSR", { "mov-to-AR-M", "field(ar3)==FPSR"}, 1},
  { "mov-to-AR-gr",   { "mov-to-AR.m M29", "mov-to-AR.i I26"} },
  { "mov-to-AR-I",      { "mov-to-AR.i I26", "mov-to-AR.i I27" } },
  { "mov-to-AR-ig",   { "mov-to-AR", "ar3"}, 1},
  { "mov-to-AR-IM",   { "mov-to-AR.i I26", "mov-to-AR.i I27",
			"mov-to-AR.m M29", "mov-to-AR.m M30" } },
  { "mov-to-AR-ITC",  { "mov-to-AR-M", "field(ar3)==ITC"}, 1},
  { "mov-to-AR-K",    { "mov-to-AR-M", "field(ar3)==K%"}, 1},
  { "mov-to-AR-LC",   { "mov-to-AR-I", "field(ar3)==LC"}, 1}, 
  { "mov-to-AR-M",    { "mov-to-AR.m M29", "mov-to-AR.m M30" }},
  { "mov-to-AR-PFS",  { "mov-to-AR-I", "field(ar3)==PFS"}, 1},
  { "mov-to-AR-RNAT", { "mov-to-AR-M", "field(ar3)==RNAT"}, 1},
  { "mov-to-AR-RSC",  { "mov-to-AR-M", "field(ar3)==RSC"}, 1},
  { "mov-to-AR-UNAT", { "mov-to-AR-M", "field(ar3)==UNAT"}, 1},
  { "mov-to-CR-CMCV", { "mov-to-CR", "field(cr3)==CMCV"}, 1},
  { "mov-to-CR-DCR", { "mov-to-CR", "field(cr3)==DCR"}, 1},
  { "mov-to-CR-EOI", { "mov-to-CR", "field(cr3)==EOI"}, 1},
  { "mov-to-CR-GPTA", { "mov-to-CR", "field(cr3)==GPTA"}, 1},
  { "mov-to-CR-IFA", { "mov-to-CR", "field(cr3)==IFA"}, 1},
  { "mov-to-CR-IFS", { "mov-to-CR", "field(cr3)==IFS"}, 1},
  { "mov-to-CR-IHA", { "mov-to-CR", "field(cr3)==IHA"}, 1},
  { "mov-to-CR-IIM", { "mov-to-CR", "field(cr3)==IIM"}, 1},
  { "mov-to-CR-IIP", { "mov-to-CR", "field(cr3)==IIP"}, 1},
  { "mov-to-CR-IIPA", { "mov-to-CR", "field(cr3)==IIPA"}, 1},
  { "mov-to-CR-IPSR", { "mov-to-CR", "field(cr3)==IPSR"}, 1},
  { "mov-to-CR-IRR", { "mov-to-CR", "field(cr3)==IRR"}, 1},
  { "mov-to-CR-ISR", { "mov-to-CR", "field(cr3)==ISR"}, 1},
  { "mov-to-CR-ITIR", { "mov-to-CR", "field(cr3)==ITIR"}, 1},
  { "mov-to-CR-ITM", { "mov-to-CR", "field(cr3)==ITM"}, 1},
  { "mov-to-CR-ITV", { "mov-to-CR", "field(cr3)==ITV"}, 1},
  { "mov-to-CR-IVA", { "mov-to-CR", "field(cr3)==IVA"}, 1},
  { "mov-to-CR-IVR", { "mov-to-CR", "field(cr3)==IVR"}, 1},
  { "mov-to-CR-LID", { "mov-to-CR", "field(cr3)==LID"}, 1},
  { "mov-to-CR-LRR", { "mov-to-CR", "field(cr3)==LRR"}, 1},
  { "mov-to-CR-PMV", { "mov-to-CR", "field(cr3)==PMV"}, 1},
  { "mov-to-CR-PTA", { "mov-to-CR", "field(cr3)==PTA"}, 1},
  { "mov-to-CR-rv", { } },
  { "mov-to-CR-TPR", { "mov-to-CR", "field(cr3)==TPR"}, 1},
  { "mov-to-IND",     { "mov-to-IND-RR", "mov-to-IND-DBR", "mov-to-IND-IBR",
			"mov-to-IND-PKR", "mov-to-IND-PMC", "mov-to-IND-PMD"}},
  { "mov-to-IND-priv", { "mov-to-IND"}},
  { "mov-to-PR-allreg", { "mov-to-PR I23" }},
  { "mov-to-PR-rotreg", { "mov-to-PR I24" }},
  { "mux",            { "mux1", "mux2" } },
  { "pack",           { "pack2", "pack4" } },
  { "padd",           { "padd1", "padd2", "padd4" } },
  { "pavg",           { "pavg1", "pavg2" } },
  { "pavgsub",        { "pavgsub1", "pavgsub2" } },
  { "pcmp",           { "pcmp1", "pcmp2", "pcmp4" } },
  { "pmax",           { "pmax1", "pmax2" } },
  { "pmin",           { "pmin1", "pmin2" } },
  { "pmpy",           { "pmpy2" } },
  { "pmpyshr",        { "pmpyshr2" } },
  { "pr-and-writers",  { "pr-gen-writers-int", "field(ctype) is and" },1},
  { "pr-gen-writers-fp", { "fclass", "fcmp" } },
  { "pr-gen-writers-int", { "cmp", "cmp4", "tbit", "tnat" } },
  { "pr-norm-writers-fp", { "pr-gen-writers-fp", "field(ctype)=="}, 1},
  { "pr-norm-writers-int", { "pr-gen-writers-int", "field(ctype)=="}, 1},
  { "pr-or-writers",  { "pr-gen-writers-int", "field(ctype) is or" },1},
  { "pr-readers-br",  { "br.call", "br.cond", "brl.call", "brl.cond",
			"br.ret", "br.wexit", "br.wtop", "break.b", "nop.b",
			"ReservedBQP" } },
  { "pr-readers-nobr-nomovpr:1",  { "add", "addp4", "and", "andcm", "break.f",
				    "break.i", "break.m", "break.x", "chk.s",
				    "chk-a", "cmp", "cmp4", "cmpxchg",
				    "czx", "dep", "extr", "fp-arith",
				    "fp-non-arith", "fc", "fchkf", "fclrf",
				    "fcmp", "fetchadd", "fpcmp", "fsetc",
				    "fwb", "getf", "invala-all", "itc.i",
				    "itc.d", "itr.i", "itr.d", "ld", "ldf",
				    "ldfp", "lfetch-all", "mf", "mix" } },
  { "pr-readers-nobr-nomovpr:2",  { "mov-from-AR-M", "mov-from-AR-IM",
				    "mov-from-AR-I", "mov-to-AR-M",
				    "mov-to-AR-I", "mov-to-AR-IM",
				    "mov-to-BR", "mov-from-BR",
				    "mov-to-CR", "mov-from-CR",
				    "mov-to-IND", "mov-from-IND",
				    "mov-ip", "mov-immediate", 
				    "mov-to-PSR-l", "mov-to-PSR-um",
				    "mov-from-PSR", "mov-from-PSR-um",
				    "movl", "mux", "nop.f", "nop.i", "nop.m",
				    "nop.x", 
				    "adds" /* ADD: missing? */
				    } },
  { "pr-readers-nobr-nomovpr:3",  { "or", "pack", "padd", "pavg",
				    "pavgsub", "pcmp", "pmax", "pmin", 
				    "pmpy", "pmpyshr", "popcnt",
				    "probe-all", "psad", "pshl",
				    "pshladd", "pshr", "pshradd", "psub",
				    "ptc.e", "ptc.g", "ptc.ga", "ptc.l",
				    "ptr.d", "ptr.i", "ReservedQP", "rsm",
				    "rum", /* ADD: missing? */
				    "setf", "shl", "shladd", "shladdp4", 
				    "shr", "shrp", "srlz.i", "srlz.d", 
				    "ssm", "st", "stf", "sub", "sum",
				    "sxt", "sync", "tak", "tbit", "thash",
				    "tnat", "tpa", "ttag", "unpack", "xchg",
				    "xma", "xor", "zxt"
				    /* "xmpy" is a pseudo-op */ } },
  { "pr-readers-nobr-nomovpr", { "pr-readers-nobr-nomovpr:1",
				 "pr-readers-nobr-nomovpr:2",
				 "pr-readers-nobr-nomovpr:3" } },
  { "pr-unc-writers-fp:1", { "pr-gen-writers-fp", "field(ctype)==unc"}, 1},
  { "pr-unc-writers-fp", { "pr-unc-writers-fp:1", "fprcpa", "fprsqrta",
			   "frcpa", "frsqrta" } },
  { "pr-unc-writers-int", { "pr-gen-writers-int", "field(ctype)==unc"},1},
  { "pr-writers",     { "pr-writers-int", "pr-writers-fp"} },
  { "pr-writers-fp",  { "pr-norm-writers-fp", "pr-unc-writers-fp"} },
  { "pr-writers-int", { "pr-norm-writers-int", "pr-unc-writers-int",
			"pr-or-writers","pr-and-writers" /* ADD:left out?*/} },
  { "predicatable-instructions", { "mov-from-PR", "mov-to-PR", 
				   "pr-readers-br",
				   "pr-readers-nobr-nomovpr" } },
  { "priv-ops",       { "mov-to-IND-priv", "bsw", "itc.i", "itc.d",
			"itr.i", "itr.d", "mov-to-CR", "mov-from-CR",
			"mov-to-PSR-l", "mov-from-PSR", "mov-from-IND-priv",
			"ptc.e", "ptc.g", "ptc.ga", "ptc.l", "ptr.i",
			"ptr.d", "rfi", "rsm", "ssm", "tak", "tpa" } },
  { "probe-all",      { "probe-fault", "probe-nofault" } },
  { "probe-fault",    { "probe M40" } },
  { "probe-nofault",  { "probe M38", "probe M39" } },
  { "psad",           { "psad1" } },
  { "pshl",           { "pshl2", "pshl4" } },
  { "pshladd",        { "pshladd2" } },
  { "pshr",           { "pshr2", "pshr4" } },
  { "pshradd",        { "pshradd2" } },
  { "psub",           { "psub1", "psub2", "psub4" } },
  { "st",             { "st1", "st2", "st4", "st8", "st8.spill" } },
  { "st-postinc",     { "stf M10", "st M5" }},
  { "stf",            { "stfs", "stfd", "stfe", "stf8", "stf.spill" } },
  { "sxt",            { "sxt1", "sxt2", "sxt4" } },
  { "sys-mask-writers-partial", { "rsm", "ssm" } },
  { "unpack",         { "unpack1", "unpack2", "unpack4" } },
  { "user-mask-writers-partial", { "rum", "sum" } },
  { "xchg",           { "xchg1", "xchg2", "xchg4", "xchg8" } },
  { "zxt",            { "zxt1", "zxt2", "zxt4" } },

  { "ReservedBQP", {}}, /* not really doable here... */
  { "ReservedQP", {}}, /* not really doable here... */

  /* Our sneakiness to check register values (superscript 1 in tables) */
  { "br.call^1",      { "br.call", "b1"}, 1},
  { "brl.call^1",     { "brl.call", "b1"}, 1},
  { "cfm-readers^2",  { "cfm-readers", "using-rotating"}, 1}, 
  { "indirect-brs^1", { "indirect-brs", "b2"}, 1},
  { "indirect-brp^1", { "indirect-brp", "b2"}, 1},
  { "mov-from-BR^1",  { "mov-from-BR", "b2"}, 1},
  { "mov-to-BR^1",    { "mov-to-BR", "b1"}, 1},
  { "fr-readers^1",   { "fr-readers", "using-regs-read"}, 1}, 
  { "fr-readers^8-low",   { "fr-readers", "using-low-FR-read" }, 1},
  { "fr-readers^8-high",   { "fr-readers", "using-high-FR-read" }, 1},
  { "fr-writers^1",   { "fr-writers", "using-regs-write"}, 1},
  { "fr-writers^8-low",   { "fr-writers", "using-low-FR-write"}, 1},
  { "fr-writers^8-high",   { "fr-writers", "using-high-FR-write"}, 1},
  { "fr-writers^9-low", { "fr-writers", "using-low-FR-write" }, 1},
  { "fr-writers^9-high", { "fr-writers", "using-high-FR-write" }, 1},
  { "gr-readers^1",   { "gr-readers", "using-regs-read"}, 1}, 
  { "gr-readers^10",   { "gr-readers", "using-banked-GR"}, 1}, 
  { "gr-writers^1",   { "gr-writers", "using-regs-write"}, 1},
  { "gr-writers^10",   { "gr-readers", "using-banked-GR"}, 1}, 
  { "pr-readers-br^1", { "pr-readers-br", "using-pregs-read"}, 1}, 
  { "pr-readers-nobr-nomovpr^1", { "pr-readers-nobr-nomovpr", 
				   "using-pregs-read"}, 1}, 
  { "pr-writers^1",   { "pr-writers", "using-pregs-write"}, 1},
  { "pr-writers-fp^1",{ "pr-writers-fp", "using-pregs-write"}, 1},
  { "pr-writers-int^1",{ "pr-writers-int", "using-pregs-write"}, 1},
  { "mov-from-AR-K^1",    { "mov-from-AR-K", "using-regs-read"}, 1},
  { "mov-from-AR-ig^1",    { "mov-from-AR-ig", "using-regs-read"}, 1},
  { "mov-from-AR-rv^1",    { "mov-from-AR-rv", "using-regs-read"}, 1},
  { "mov-from-CR-IRR^1",   { "mov-from-CR-IRR", "using-regs-read"}, 1},
  { "mov-from-CR-LRR^1",   { "mov-from-CR-LRR", "using-regs-read"}, 1},
  { "mov-from-CR-rv^1",    { "mov-from-CR-rv", "using-regs-read"}, 1},
  { "mov-from-PR^12", { "mov-from-PR","using-pregs-qp"}, 1},
  { "mov-to-AR-K^1",    { "mov-to-AR-K", "using-regs-write"}, 1},
  { "mov-to-AR-ig^1",    { "mov-to-AR-ig", "using-regs-write"}, 1},
  { "mov-to-CR-LRR^1",   { "mov-to-CR-LRR", "using-regs-write"}, 1},
  { "mov-to-PR^12",   { "mov-to-PR",  "using-pregs-qp"}, 1},
  { "mov-to-PR-allreg^7", { "mov-to-PR-allreg", "PR-allreg-masking"}, 1}, 
  { "unpredicatable-instructions", { "alloc", "br.cloop", "br.ctop",
				     "br.cexit", "brp", "bsw", "clrrrb",
				     "cover", "epc", "flushrs", "loadrs",
				     "rfi",
				     "br.ia" /* ADD: left out? */} },
  { "user-mask-writers-partial^7", { "user-mask-writers-partial",
				     "PSR-partial-masking"}, 1},
  { "sys-mask-writers-partial^7", { "sys-mask-writers-partial",
				    "PSR-partial-masking"}, 1},
};

Ltype writers[] = {
  { "AR[BSP]", 
    { "br.call", "brl.call", "br.ret", "cover",
    "mov-to-AR-BSPSTORE","rfi" }},
  { "AR[BSPSTORE]",
    { "alloc", "loadrs", "flushrs", "mov-to-AR-BSPSTORE" }},
  { "AR[CCV]",
    { "mov-to-AR-CCV" }},
  { "AR[EC]",
    { "mov-to-AR-EC", "br.ret", "mod-sched-brs" }},
  { "AR[FPSR].sf0.controls",
    { "mov-to-AR-FPSR", "fsetc.s0" }},
  { "AR[FPSR].sf1.controls",
    { "mov-to-AR-FPSR", "fsetc.s1" }},
  { "AR[FPSR].sf2.controls",
    { "mov-to-AR-FPSR", "fsetc.s2" }},
  { "AR[FPSR].sf3.controls",
    { "mov-to-AR-FPSR", "fsetc.s3" }},
  { "AR[FPSR].sf0.flags",
    { "fp-arith-s0", "fclrf.s0", "fcmp-s0", "fpcmp-s0", "mov-to-AR-FPSR"}},
  { "AR[FPSR].sf1.flags",
    { "fp-arith-s1", "fclrf.s1", "fcmp-s1", "fpcmp-s1", "mov-to-AR-FPSR"}},
  { "AR[FPSR].sf2.flags",
    { "fp-arith-s2", "fclrf.s2", "fcmp-s2", "fpcmp-s2", "mov-to-AR-FPSR"}},
  { "AR[FPSR].sf3.flags",
    { "fp-arith-s3", "fclrf.s3", "fcmp-s3", "fpcmp-s3", "mov-to-AR-FPSR"}},
  { "AR[FPSR].traps",
    { "mov-to-AR-FPSR"}},
  { "AR[FPSR].rv",
    { "mov-to-AR-FPSR"}},
  { "AR[K%]", 
    { "mov-to-AR-K^1" }},
  { "AR[ITC]",
    { "mov-to-AR-ITC" }},
  { "AR[LC]",
    { "mod-sched-brs-counted", "mov-to-AR-LC" }},
  { "AR[PFS]",
    { "br.call", "brl.call", "mov-to-AR-PFS" }},
  { "AR[RNAT]",
    { "alloc", "flushrs", "loadrs", "mov-to-AR-RNAT", "mov-to-AR-BSPSTORE"}},
  { "AR[RSC]",
    { "mov-to-AR-RSC" }},
  { "AR[UNAT]",
    { "mov-to-AR-UNAT", "st8.spill"}},
  { "AR[rv%]", 
    {  } },
  { "AR[ig%]", 
    { "mov-to-AR-ig^1" }},

  { "BR[%]",
    { "br.call^1", "brl.call^1", "mov-to-BR^1" } },

  { "CFM",
    { "mod-sched-brs", "br.call", "brl.call", "br.ret", "clrrrb",
      "cover", "rfi", "alloc",
      "br.ia" /* ADD: left out? */ } },

  { "CR[CMCV]",
    { "mov-to-CR-CMCV" }},
  { "CR[DCR]",
    { "mov-to-CR-DCR" } },
  { "CR[EOI]", 
    { "mov-to-CR-EOI" } },
  { "CR[GPTA]", 
    { "mov-to-CR-GPTA" } },
  { "CR[IFA]",
    { "mov-to-CR-IFA" } },
  { "CR[IFS]",
    { "mov-to-CR-IFS", "cover" } },
  { "CR[IHA]",
    { "mov-to-CR-IHA" } },
  { "CR[IIM]",
    { "mov-to-CR-IIM" } },
  { "CR[IIP]",
    { "mov-to-CR-IIP" } },
  { "CR[IIPA]",
    { "mov-to-CR-IIPA" } },
  { "CR[IPSR]",
    { "mov-to-CR-IPSR" } },
  { "CR[IRR%]",
    { "mov-from-CR-IVR" }},
  { "CR[ISR]",
    { "mov-to-CR-ISR" } },
  { "CR[ITIR]",
    { "mov-to-CR-ITIR" } },
  { "CR[ITM]",
    { "mov-to-CR-ITM" } },
  { "CR[ITV]",
    { "mov-to-CR-ITV" } },
  { "CR[IVA]",
    { "mov-to-CR-IVA" } },
  { "CR[IVR]",
    {  } },
  { "CR[LID]",
    { "mov-to-CR-LID" } },
  { "CR[LRR%]",
    { "mov-to-CR-LRR^1" } },
  { "CR[PMV]",
    { "mov-to-CR-PMV" } },
  { "CR[PTA]",
    { "mov-to-CR-PTA" } },
  { "CR[TPR]",
    { "mov-to-CR-TPR" } },
  { "CR[rv%]",
    {  } },

  { "FR[0]", {} },
  { "FR[1]", {} },
  { "FR[%]",
    { "fr-writers^1", 
      /*"ldf-c", "ldfp-c", "ldf-c", "ldfp-c" both removed and added */ }},

  { "GR[0]", {} },
  { "GR[%]",
    { "gr-writers^1", /* "ld-c" removed and added */ }},

  { "PR[0]",
    /*{ "pr-writers^1" } we never report PR0 as a destination */},
  { "PR[%]",
    { "pr-writers^1", "mov-to-PR-allreg^7", "mov-to-PR-rotreg",
      "pr-writers-fp^1", "pr-writers-int^1" } },
  { "PR[63]", 
    { "mod-sched-brs",
      "pr-writers^1", "mov-to-PR-allreg^7", "mov-to-PR-rotreg",
      "pr-writers-fp^1", "pr-writers-int^1" } },

  { "PSR.ac",
    { "user-mask-writers-partial^7", "mov-to-PSR-um",
      "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.be",
    { "user-mask-writers-partial^7", "mov-to-PSR-um",
      "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.bn",
    { "bsw", "rfi" } },
  { "PSR.cpl",
    { "epc", "br.ret", "rfi" } },
  { "PSR.da", 
    { "rfi" } },
  { "PSR.db", 
    { "rfi", "mov-to-PSR-l" } },
  { "PSR.dd", 
    { "rfi" } },
  { "PSR.dfh", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.dfl", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.di", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.dt", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.ed", 
    { "rfi" } },
  { "PSR.i", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.ia", 
    { "rfi" } },
  { "PSR.ic", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.id", 
    { "rfi" } },
  { "PSR.is", 
    { "br.ia", "rfi" } },
  { "PSR.it", 
    { "rfi" } },
  { "PSR.lp", 
    { "mov-to-PSR-l", "rfi" } },
  { "PSR.mc", 
    { "rfi" } },
  { "PSR.mfl",
    { "fr-writers^9-low",
      "user-mask-writers-partial^7", "mov-to-PSR-um",
      "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.mfh",
    { "fr-writers^9-high",
      "user-mask-writers-partial^7", "mov-to-PSR-um",
      "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.pk", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.pp", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.ri", 
    { "rfi" } },
  { "PSR.rt", 
    { "mov-to-PSR-l", "rfi" } },
  { "PSR.si", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.sp", 
    { "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
  { "PSR.ss", 
    { "rfi" } },
  { "PSR.tb", 
    { "mov-to-PSR-l", "rfi" } },
  { "PSR.up",
    { "user-mask-writers-partial^7", "mov-to-PSR-um",
      "sys-mask-writers-partial^7", "mov-to-PSR-l", "rfi" } },
    
};

Ltype readers[] = {
  { "AR[BSP]", 
    { "br.call", "brl.call", "br.ia", "br.ret", "cover", "flushrs",
    "loadrs", "mov-from-AR-BSP","rfi" }},
  { "AR[BSPSTORE]",
    { "alloc", "br.ia", "flushrs", "mov-from-AR-BSPSTORE" }},
  { "AR[CCV]",
    { "br.ia", "mov-from-AR-CCV", "cmpxchg" }},
  { "AR[EC]",
    { "mov-from-AR-EC", "br.call", "brl.call", "br.ia", "mod-sched-brs" }},
  { "AR[FPSR].sf0.controls",
    { "br.ia", "fp-arith-s0", "fcmp-s0", "fpcmp-s0", "fsetc",
      "mov-from-AR-FPSR" }},
  { "AR[FPSR].sf1.controls",
    { "br.ia", "fp-arith-s1", "fcmp-s1", "fpcmp-s1", 
      "mov-from-AR-FPSR" }},
  { "AR[FPSR].sf2.controls",
    { "br.ia", "fp-arith-s2", "fcmp-s2", "fpcmp-s2", 
      "mov-from-AR-FPSR" }},
  { "AR[FPSR].sf3.controls",
    { "br.ia", "fp-arith-s3", "fcmp-s3", "fpcmp-s3", 
      "mov-from-AR-FPSR" }},
  { "AR[FPSR].sf0.flags",
    { "br.ia", "fchkf", "mov-from-AR-FPSR"}},
  { "AR[FPSR].sf1.flags",
    { "br.ia", "fchkf.s1", "mov-from-AR-FPSR"}},
  { "AR[FPSR].sf2.flags",
    { "br.ia", "fchkf.s2", "mov-from-AR-FPSR"}},
  { "AR[FPSR].sf3.flags",
    { "br.ia", "fchkf.s3", "mov-from-AR-FPSR"}},
  { "AR[FPSR].traps",
    { "br.ia", "fp-arith", "fchkf", "fcmp", "fpcmp", "mov-from-AR-FPSR"}},
  { "AR[FPSR].rv",
    { "br.ia", "fp-arith", "fchkf", "fcmp", "fpcmp", "mov-from-AR-FPSR"}},
  { "AR[ITC]",
    { "br.ia", "mov-from-AR-ITC" }},
  { "AR[K%]", 
    { "br.ia", "mov-from-AR-K^1" }},
  { "AR[LC]",
    { "br.ia", "mod-sched-brs-counted", "mov-from-AR-LC" }},
  { "AR[PFS]",
    { "alloc", "br.ia", "br.ret", "epc", "mov-from-AR-PFS" }},
  { "AR[RNAT]",
    { "alloc", "br.ia", "flushrs", "loadrs", "mov-from-AR-RNAT" }},
  { "AR[RSC]",
    { "alloc", "br.ia", "flushrs", "loadrs", "mov-from-AR-RSC",
      "mov-from-AR-BSPSTORE", "mov-to-AR-RNAT", "mov-from-AR-RNAT",
      "mov-to-AR-BSPSTORE" }},
  { "AR[UNAT]",
    { "br.ia", "ld8.fill", "mov-from-AR-UNAT" }},
  { "AR[rv%]", 
    { "br.ia", "mov-from-AR-rv^1" }},
  { "AR[ig%]", 
    { "br.ia", "mov-from-AR-ig^1" }},

  { "BR[%]",
    { "indirect-brs^1", "indirect-brp^1", "mov-from-BR^1" } },

  { "CFM",
    { "mod-sched-brs", "cover", "alloc", "rfi", "loadrs", "br.ret",
      "br.call", "brl.call", "cfm-readers^2" }},

  { "CR[CMCV]",
    { "mov-from-CR-CMCV" }},
  { "CR[DCR]",
    { "mov-from-CR-DCR", "mem-readers-spec" }},
  { "CR[EOI]", {} },
  { "CR[GPTA]", 
    { "mov-from-CR-GPTA", "thash" } },
  { "CR[IFA]",
    { "itc.i", "itc.d", "itr.i", "itr.d", "mov-from-CR-IFA" } },
  { "CR[IFS]",
    { "mov-from-CR-IFS", "rfi" } },
  { "CR[IHA]",
    { "mov-from-CR-IHA" } },
  { "CR[IIM]",
    { "mov-from-CR-IIM" } },
  { "CR[IIP]",
    { "mov-from-CR-IIP", "rfi" } },
  { "CR[IIPA]",
    { "mov-from-CR-IIPA" } },
  { "CR[IPSR]",
    { "mov-from-CR-IPSR", "rfi" } },
  { "CR[IRR%]",
    { "mov-from-CR-IRR^1"}},
  { "CR[ISR]",
    { "mov-from-CR-ISR" } },
  { "CR[ITIR]",
    { "mov-from-CR-ITIR", "itc.i", "itc.d", "itr.i", "itr.d" } },
  { "CR[ITM]",
    { "mov-from-CR-ITM" } },
  { "CR[ITV]",
    { "mov-from-CR-ITV" } },
  { "CR[IVA]",
    { "mov-from-CR-IVA" } },
  { "CR[IVR]",
    { "mov-from-CR-IVR" } },
  { "CR[LID]",
    { "mov-from-CR-LID" } },
  { "CR[LRR%]",
    { "mov-from-CR-LRR^1" } },
  { "CR[PMV]",
    { "mov-from-CR-PMV" } },
  { "CR[PTA]",
    { "mov-from-CR-PTA", "thash" } },
  { "CR[TPR]",
    { "mov-from-CR-TPR", "mov-from-CR-IVR", "mov-to-PSR-l", 
      "rfi", "rsm", "ssm" } },
  { "CR[rv%]",
    { "mov-from-CR-rv^1" } },

  { "FR[0]",
    { "fr-readers^1" }},
  { "FR[1]",
    { "fr-readers^1" }},
  { "FR[%]",
    { "fr-readers^1" }},

  { "GR[0]",
    { "gr-readers^1", "mov-immediate" }},
  { "GR[%]",
    { "gr-readers^1" }},

  { "PR[0]",
    /*  { "pr-readers-br^1", "pr-readers-nobr-nomovpr^1", "mov-from-PR^12",
	"mov-to-PR^12" } we never report PR[0] as a source */ },
  { "PR[%]",
    { "pr-readers-br^1", "pr-readers-nobr-nomovpr^1", "mov-from-PR",
      "mov-to-PR^12" } },
  { "PR[63]",
    { "pr-readers-br^1", "pr-readers-nobr-nomovpr^1", "mov-from-PR",
      "mov-to-PR^12" } },

  { "PSR.ac",
    { "mem-readers", "mem-writers", "mov-from-PSR", "mov-from-PSR-um" } },
  { "PSR.be",
    { "mem-readers", "mem-writers", "mov-from-PSR", "mov-from-PSR-um" } },
  { "PSR.bn",
    { "gr-readers^10", "gr-writers^10" } },
  { "PSR.cpl", 
    { "priv-ops", "br.call", "brl.call", "epc", "mov-from-AR-ITC",
      "mov-to-AR-ITC", "mov-to-AR-RSC", "mov-to-AR-K", 
      "mov-from-IND-PMD", "probe-all", "mem-readers", "mem-writers",
      "lfetch-all" } },
  { "PSR.da",
    { "mem-readers", "lfetch-fault", "mem-writers", "probe-fault" } },
  { "PSR.db",
    { "mem-readers", "mem-writers", "probe-fault", "mov-from-PSR" } },
  { "PSR.dd",
    { "mem-readers", "lfetch-fault", "mem-writers", "probe-fault" } },
  { "PSR.dfh",
    { "fr-readers^8-high", "fr-writers^8-high", "mov-from-PSR" } },
  { "PSR.dfl",
    { "fr-readers^8-low", "fr-writers^8-low", "mov-from-PSR" } },
  { "PSR.di", 
    { "br.ia", "mov-from-PSR" } },
  { "PSR.dt", 
    { "mem-readers", "mem-writers", "mov-from-PSR",
      "probe-all" /* ADD: was this left out??? */ } },
  { "PSR.ed", 
    { "lfetch-all", "mem-readers-spec" } },
  { "PSR.i",
    { "mov-from-PSR" } },
  { "PSR.ia", {} },
  { "PSR.ic",
    { "mov-from-PSR", "cover", "itc.i", "itc.d", "itr.i", "itr.d",
      "mov-from-CR-ITIR", "mov-from-CR-IFS", "mov-from-CR-IIM",
      "mov-from-CR-IIP", "mov-from-CR-IPSR", "mov-from-CR-ISR",
      "mov-from-CR-IFA", "mov-from-CR-IHA", "mov-from-CR-IIPA", 
      "mov-to-CR-ITIR", "mov-to-CR-IFS", "mov-to-CR-IIM", 
      "mov-to-CR-IIP", "mov-to-CR-IPSR", "mov-to-CR-ISR", 
      "mov-to-CR-IFA", "mov-to-CR-IHA", "mov-to-CR-IIPA" } },
  { "PSR.id", {} },
  { "PSR.is", {} },
  { "PSR.it", 
    { "branches", "mov-from-PSR", "chk", "epc", "fchkf" } },
  { "PSR.lp", 
    { "mov-from-PSR", "br.ret" } },
  { "PSR.mc",
    { "mov-from-PSR" } },
  { "PSR.mfh",
    { "mov-from-PSR", "mov-from-PSR-um" } },
  { "PSR.mfl",
    { "mov-from-PSR", "mov-from-PSR-um" } },
  { "PSR.pk",
    { "mem-readers", "mem-writers", "probe-all", "mov-from-PSR" } },
  { "PSR.pp", 
    { "mov-from-PSR" } },
  { "PSR.ri", {} },
  { "PSR.rt", 
    { "mov-from-PSR", "alloc", "flushrs", "loadrs" } },
  { "PSR.si",
    { "mov-from-PSR", "mov-from-AR-ITC" } },
  { "PSR.sp",
    { "mov-from-PSR", "mov-from-IND-PMD", "mov-to-PSR-um", "rum", "sum" } },
  { "PSR.ss",
    { "all" } },
  { "PSR.tb", 
    { "branches", "chk", "fchkf", "mov-from-PSR" } },
  { "PSR.up",
    { "mov-from-PSR-um", "mov-from-PSR" } },

};

/*  Figure out whether a register is used; extra is read/write to check */
static int 
using_regs(LSE_emu_instr_info_t *ii, int extra,
	   LSE_emu_spaceid_t id, int addr, uint64_t mask0,
	   Itype **itp) 
{
  uint64_t instr = ii->extra.instr;
  int raddr;
  Rtype **rtp, *rt;

  rtp = &(*itp)->rtests[0];
  while ((rt = *rtp)) {
    /* we have a register match.... */
    raddr = ((instr>>rt->shift)&rt->maxregno);
    if (rt->id == id && raddr == addr && (extra&rt->marktype)) {
      return 1;
    }
    rtp++;
  }
  return 0;
}


/*  Figure out whether the CFM is in use for rotation.... */
static int 
using_rotating(LSE_emu_instr_info_t *ii, int extra,
	       LSE_emu_spaceid_t id, int addr, uint64_t mask0,
	       Itype **itp) 
{
  uint64_t instr = ii->extra.instr;
  int raddr;
  Rtype **rtp, *rt;

  rtp = &(*itp)->rtests[0];
  while ((rt = *rtp)) {
    /* we have a register match.... */
    raddr = ((instr>>rt->shift)&rt->maxregno);
    switch (rt->id) {
    case LSE_emu_spaceid_PR:
      if (raddr > 15) return 1;
      break;
    case LSE_emu_spaceid_GR:
    case LSE_emu_spaceid_FR:
      if (raddr > 31) return 1;
      break;
    }
    rtp++;
  }
  return 0;
}

/* Figure out the completer for things setting predicates */
static int 
ctype_equals(LSE_emu_instr_info_t *ii, int extra,
	     LSE_emu_spaceid_t id, int addr, uint64_t mask0,
	     Itype **itp) 
{
  uint64_t instr = ii->extra.instr;
  int opno = (instr>>37)&0xf;
  int thiskind = 0;
  if (ii->extra.unit==2) { /* fp */
    if ((UINT64_C(1)<<12)&instr) thiskind = 1;
    else thiskind = 8;
  } else if (opno == 5) { /* tbit/tnan */
    thiskind = ((instr>>35)&2)|((instr>>31)&4);
    if (!thiskind) thiskind=((instr>>12)&1) ? 1 : 8;
  } else { /* compare */
    if ((((instr>>34)&3)>=2)|| !((instr>>36)&1)) { /* can have unc */
      if ((instr>>33)&1) { /* evil ones */
	thiskind = ((opno+1)&3)<<1;
      } else {
	thiskind = ((instr>>12)&1) ? 1 : 8;
      }
    } else {
      thiskind = ((opno+1)&3)<<1;
    }
  }
  /* printf("Extra is %d thiskind=%d\n",extra,thiskind); */
  return ((thiskind&extra)!=0);
}

/*  Figure out whether a predicate register is used; 
    extra is read/write to check */
static int 
using_pregs(LSE_emu_instr_info_t *ii, int extra,
	    LSE_emu_spaceid_t id, int addr, uint64_t mask0, Itype **itp) 
{
  uint64_t instr = ii->extra.instr;
  int raddr;
  Rtype **rtp, *rt;

  rtp = &(*itp)->rtests[0];
  while ((rt = *rtp)) {
    /* we have a register match.... */
    raddr = ((instr>>rt->shift)&rt->maxregno);
    if (rt->id == id && ((UINT64_C(1)<<raddr)&mask0) && (extra&rt->marktype)) {
      return 1;
    }
    rtp++;
  }
  return 0;
}

/*  Figure out whether a predicate register is used; 
    extra is read/write to check; restrict to qp */
static int 
using_pregs_qp(LSE_emu_instr_info_t *ii, int extra,
	       LSE_emu_spaceid_t id, int addr, uint64_t mask0, Itype **itp) 
{
  uint64_t instr = ii->extra.instr;
  int raddr;
  Rtype **rtp, *rt;

  rtp = &(*itp)->rtests[0];
  while ((rt = *rtp)) {
    /* we have a register match.... */
    raddr = ((instr>>rt->shift)&rt->maxregno);
    if (rt->id == id && ((UINT64_C(1)<<raddr)&mask0) && (extra&rt->marktype) &&
	!strcmp(rt->name,"qp")) {
      return 1;
    }
    rtp++;
  }
  return 0;
}

/*  Figure out whether a PR register is masked out on mov pr */
static int 
pr_allreg_masking(LSE_emu_instr_info_t *ii, int extra,
		  LSE_emu_spaceid_t id, int addr, uint64_t mask0, Itype **itp) 
{
  uint64_t instr = ii->extra.instr, imm64;

  imm64 = ((instr >> 6) & UINT64_C(0x3f))<<1;
  imm64 |= ((instr >> 24) & UINT64_C(0xff))<<8;
  if ((instr>>36)&1) imm64 |= ~INT64_C(0xFFFF);

  if (imm64 & mask0) return 1;
  else return 0;
}

/*  Figure out whether a PSR bit is masked on sum/rum/ssm/rsm */
static int 
psr_partial_masking(LSE_emu_instr_info_t *ii, int extra,
		    LSE_emu_spaceid_t id, int addr, uint64_t mask0, Itype **itp) 
{
  uint64_t instr = ii->extra.instr, imm64;

  imm64 = ((instr >> 6) & UINT64_C(0x1fffff));
  imm64 |= ((instr >> 31) & UINT64_C(0x3))<<21;
  imm64 |= ((instr >> 36) & UINT64_C(0x1))<<23;

  if (imm64 & mask0) return 1;
  else return 0;
}

/*  Figure out whether FR low/high registers are used; extra = 0 for low */
static int 
using_fr(LSE_emu_instr_info_t *ii, int extra,
	 LSE_emu_spaceid_t id, int addr, uint64_t mask0,
	 Itype **itp) 
{
  uint64_t instr = ii->extra.instr;
  int raddr;
  Rtype **rtp, *rt;
  int ishigh = extra & 8;

  rtp = &(*itp)->rtests[0];
  while ((rt = *rtp)) {
    if (rt->id == LSE_emu_spaceid_FR) {
      raddr = ((instr>>rt->shift)&rt->maxregno);
      if (((ishigh && raddr > 31) || (!ishigh && raddr < 32 && raddr>1)) &&
	  (extra & 3 & rt->marktype))
	return 1;
    }
    rtp++;
  }
  return 0;
}

/*  Figure out whether banked GR registers are used */
static int 
using_banked_gr(LSE_emu_instr_info_t *ii, int extra,
		LSE_emu_spaceid_t id, int addr, uint64_t mask0,
		Itype **itp) 
{
  uint64_t instr = ii->extra.instr;
  int raddr;
  Rtype **rtp, *rt;

  rtp = &(*itp)->rtests[0];
  while ((rt = *rtp)) {
    if (rt->id == LSE_emu_spaceid_GR) {
      raddr = ((instr>>rt->shift)&rt->maxregno);
      if (raddr >= 16 && raddr <= 31) return 1;
    }
    rtp++;
  }
  return 0;
}

Utype uglytests[] = {
  { "using-regs-read", using_regs, 1 },
  { "using-regs-write", using_regs, 2 },
  { "using-rotating", using_rotating, 0 },
  { "field(ctype)==", ctype_equals, 8 },
  { "field(ctype)==unc", ctype_equals, 1 },
  { "field(ctype) is and", ctype_equals, 2 },
  { "field(ctype) is or", ctype_equals, 4 },
  { "using-pregs-qp", using_pregs_qp, 1 },
  { "using-pregs-read", using_pregs, 1 },
  { "using-pregs-write", using_pregs, 2 },
  { "PR-allreg-masking", pr_allreg_masking, 0 },
  { "PSR-partial-masking", psr_partial_masking, 0 },
  { "using-low-FR-read", using_fr, 1 },
  { "using-high-FR-read", using_fr, 1 | 8 },
  { "using-low-FR-write", using_fr, 2 },
  { "using-high-FR-write", using_fr, 2 | 8 },
  { "using-banked-GR", using_banked_gr, 0 },
};

#define NUMINSTS (sizeof(instructions)/sizeof(Itype))
#define NUMLISTS (sizeof(lists)/sizeof(Ltype))
#define NUMREADERS (sizeof(readers)/sizeof(Ltype))
#define NUMWRITERS (sizeof(writers)/sizeof(Ltype))
#define NUMREGTESTS (sizeof(regtests)/sizeof(Rtype))
#define NUMUGLYTESTS (sizeof(uglytests)/sizeof(Utype))

static void
init_list(Ltype *lt, const char *lclass) {
  int i,j;
#ifdef DEBUGDEP
  fprintf(stdout,"Linking %s %s to:\n", lclass, lt->name);
#endif
  i=0;
  while (lt->cnames[i]) {
    for (j=0;j<NUMINSTS;j++) {
      if (!strcmp(instructions[j].name,lt->cnames[i])) {
	lt->contents[i].ptr.inst = &instructions[j];
	lt->contents[i].itype = 0;
#ifdef DEBUGDEP
	fprintf(stdout,"\tInstr %s\n",lt->cnames[i]);
#endif
	goto found;
      }
    } /* for j */
    for (j=0;j<NUMLISTS;j++) {
      if (!strcmp(lists[j].name,lt->cnames[i])) {
	lt->contents[i].ptr.list = &lists[j];
	lt->contents[i].itype = 1;
#ifdef DEBUGDEP
	fprintf(stdout,"\tList  %s\n",lt->cnames[i]);
#endif
	goto found;
      }
    } /* for j */
    for (j=0;j<NUMREGTESTS;j++) {
      if (!strcmp(regtests[j].name,lt->cnames[i])) {
	lt->contents[i].ptr.rtest = &regtests[j];
	lt->contents[i].itype = 2;
#ifdef DEBUGDEP
	fprintf(stdout,"\tRegister %s\n",lt->cnames[i]);
#endif
	goto found;
      }
    } /* for j */
    for (j=0;j<NUMUGLYTESTS;j++) {
      if (!strcmp(uglytests[j].name,lt->cnames[i])) {
	lt->contents[i].ptr.utest = &uglytests[j];
	lt->contents[i].itype = 3;
#ifdef DEBUGDEP
	fprintf(stdout,"\tRegister %s\n",lt->cnames[i]);
#endif
	goto found;
      }
    } /* for j */
    fprintf(stdout,"\tUnable to link %s\n",lt->cnames[i]);
    exit(1);
  found:
    i++;
    continue;
  }
}

static void
init_instr(Itype *it) {
  int i,j;
#ifdef DEBUGDEP
  fprintf(stdout,"Linking instr %s:\n", it->name);
#endif
  i=0;
  while (it->rnames[i]) {
    for (j=0;j<NUMREGTESTS;j++) {
      if (!strcmp(regtests[j].name,it->rnames[i])) {
	it->rtests[i] = &regtests[j];
#ifdef DEBUGDEP
	fprintf(stdout,"\tRegtest %s\n",it->rnames[i]);
#endif
	goto found;
      }
    } /* for j */
    fprintf(stdout,"\tUnable to link %s\n",it->rnames[i]);
    exit(1);
  found:
    i++;
    continue;
  }
}

static void 
build_instruction_lists(LSE_emu_ctoken_t tok) 
{
  int i;

  fprintf(stdout,"*** Creating lists ***\n");
  for (i=0;i<NUMLISTS;i++) {
    init_list(&lists[i],"list");
  }
  fprintf(stdout,"*** Creating readers ***\n");
  for (i=0;i<NUMREADERS;i++) {
    init_list(&readers[i],"reader");
  }
  fprintf(stdout,"*** Creating writers ***\n");
  for (i=0;i<NUMWRITERS;i++) {
    init_list(&writers[i],"writer");
  }
  for (i=0;i<NUMINSTS;i++) {
    init_instr(&instructions[i]);
  }
}

static void
traverse_and_mark(LSE_emu_instr_info_t *ii, Ltype *l, int m, int *outmp,
		  int printit, LSE_emu_spaceid_t id, int addr, uint64_t mask0,
		  Itype **newit)
{
  int i;
  int found=0,newf;
  int runningmark=0,newmark;
  Itype *saveit;

  if (0 && printit) 
    fprintf(stdout, "Trying list %s\n", l->name);

  runningmark = l->conjunctive ? m : 0;
  found = l->conjunctive ? 1 : 0;

  saveit = *newit;

  i=0;  
  while (l->cnames[i]) {
    newmark = 0;
    newf = 0;
    switch (l->contents[i].itype) {
    case 1:
      traverse_and_mark(ii,l->contents[i].ptr.list,m,&newmark,printit,
			id, addr, mask0, newit);
      if (newmark) newf=1;
      break;
    case 0:
      {
	Itype *it = l->contents[i].ptr.inst;
	/* just look whether it matched before... */
	if (it->matches) {
	  newmark = m;
	  it->mark |= 1; /* found matching instruction.... */
	  newf = 1;
	  if (printit) 
	    fprintf(stdout,"  Marking %s %d\n", it->name,m);
	  if (!it->notaninst) *newit = it;
	}
      }
      break;
    case 2:
      {
	Rtype *rt = l->contents[i].ptr.rtest;
	if (addr == ((ii->extra.instr>>rt->shift)&rt->maxregno)) {
	  newmark = m;
	  newf = 1;
	  if (printit)
	    fprintf(stdout, "Marking %s %d %d\n", rt->name, addr, m);
	}
      }
      break;
    case 3:
      {
	Utype *ut = l->contents[i].ptr.utest;
	if ((*ut->func)(ii,ut->extra,id,addr,mask0,newit)) {
	  newmark = m;
	  newf = 1;
	  if (printit)
	    fprintf(stdout, "Marking %s %d %d\n", ut->name,addr,m);
	}
      }
      break;
    }
    if (l->conjunctive) {
      runningmark &= newmark;
      found &= newf;
      if (!newf) goto shortcircuit;
    } else { /* disjunctive (or) */
      runningmark |= newmark;
      found |= newf;
      if (newf) goto shortcircuit;
    }
    i++;
  }
 shortcircuit:
  *outmp |= runningmark;
  l->mark |= found;
  if (printit && found) 
    fprintf(stdout, "Marking list %s\n", l->name);
  if (!found) *newit = saveit;
}

static int
check_dep(LSE_emu_instr_info_t *ii, const char *lname,
	  LSE_emu_spaceid_t id, int addr,
	  uint64_t mask0, uint64_t mask1) 
{
  int reader=-1, writer=-1;
  int i,j, expmark=0, repmark=0;
  int ecount=0;
  Itype *foundinstr=NULL, *foundinstw=NULL;

#ifdef DEBUGDEP
  fprintf(stdout,"Checking %s ",lname);
  fprintf(stdout,"%d %d %"PRIx64" %"PRIx64"\n",id,addr,mask0,mask1);
#endif

  /* Figure out expected dependencies */

  for (i=0;i<NUMREADERS;i++)
    if (!strcmp(readers[i].name,lname)) {
      reader = i;
      break;
    }
#ifdef DEBUGDEP
  if (reader>=0) traverse_and_mark(ii,&readers[reader],1,&expmark,1,id,addr,
				   mask0,
				   &foundinstr);
#else
  if (reader>=0) traverse_and_mark(ii,&readers[reader],1,&expmark,0,id,addr,
				   mask0,
				   &foundinstr);
#endif
  for (i=0;i<NUMWRITERS;i++)
    if (!strcmp(writers[i].name,lname)) {
      writer = i;
      break;
    }
#ifdef DEBUGDEP
  if (writer>=0) traverse_and_mark(ii,&writers[writer],2,&expmark,1,id,addr,
				   mask0,
				   &foundinstw);
#else
  if (writer>=0) traverse_and_mark(ii,&writers[writer],2,&expmark,0,id,addr,
				   mask0,
				   &foundinstw);
#endif

  /* look for reported dependencies */

  /* special cases */
  if (id==LSE_emu_spaceid_AR && ii->extra.unit == 3 && 
      ((ii->extra.instr & ((UINT64_C(0xf)<<37)|(UINT64_C(0x3f)<<27)|(0x7<<6)))==
       ((UINT64_C(0x0)<<37)|(UINT64_C(0x20)<<27)|(0x1<<6)))) { /* br.ia reads all ARs */
    repmark|=1;
  }

  for (j=0;j<LSE_emu_max_operand_src;j++) {
    /*    fprintf(stdout,"%d %d %d %"PRIx64" %"PRIx64"\n",
	    j,ii->operand_src[j],
	    ii->operand_src[j].spaceaddr.LSE,
	    ii->operand_src[j].uses.reg.bits[0],	      
	    ii->operand_src[j].uses.reg.bits[1]);*/
    if (ii->operand_src[j].spaceid == id &&
	ii->operand_src[j].spaceaddr.LSE == addr &&
	((ii->operand_src[j].uses.reg.bits[0] & mask0) ||
	 (ii->operand_src[j].uses.reg.bits[1] & mask1))) {
      repmark |= 1;
    }
  }
  for (j=0;j<LSE_emu_max_operand_dest;j++) {
    if (ii->operand_dest[j].spaceid == id &&
	ii->operand_dest[j].spaceaddr.LSE == addr &&
	((ii->operand_dest[j].uses.reg.bits[0] & mask0) ||
	 (ii->operand_dest[j].uses.reg.bits[1] & mask1))) {
      repmark |= 2;
    }
  }
  if (expmark ^ repmark) {
    if (id==LSE_emu_spaceid_PR) {
      fprintf(stdout,"!! Error: resource %s %016"PRIx64": expected %d was %d",
	      lname,mask0,expmark,repmark);
    } else {
      fprintf(stdout,"!! Error: resource %s %d: expected %d was %d",
	      lname,addr,expmark,repmark);
    }
    fprintf(stdout,"\t");
    EMU_disassemble_instr(ii,stdout);
#ifndef DEBUGDEP
    /* report instructions which it was thought to be... */
    if (reader>=0) traverse_and_mark(ii,&readers[reader],1,&expmark,1,id,addr,
				     mask0,
				     &foundinstr);
    if (writer>=0) traverse_and_mark(ii,&writers[writer],2,&expmark,1,id,addr,
				     mask0,
				     &foundinstw);
#endif
    ecount++;
  }
  return ecount;
}

#define bits64 UINT64_C(0xFfffFfffFfffFfff)

static int 
do_dependency_checks(LSE_emu_instr_info_t *ii) 
{
  int ecount = 0, i;
  int matchcnt, matchno=-1;

#ifdef nomore
  fprintf(stdout,"****** Beginning dependency checks ******\n");
#endif

  EMU_do_step(ii, (LSE_emu_instrstep_name_t)1, FALSE);  /* decode */

  /* Find out which instructions match */
  matchcnt = 0;
  for (i=0;i<NUMINSTS;i++) {
    Itype *it = &instructions[i];
    if ( (it->unit<0 || (it->unit == ii->extra.unit) ||
	  (it->unit == 5 && (ii->extra.unit<2))) &&
	 ((it->masklo & ii->extra.instr)==it->bitslo)) {
      it->matches = 1;
      if (!it->notaninst) {
	matchcnt++;
	matchno = i;
	if (it->formatno != ii->extra.formatno) {
	  fprintf(stdout,"--Format number mismatch: %s(%d) and %d ",
		  it->name, it->formatno, ii->extra.formatno);
	  EMU_disassemble_instr(ii,stdout);
	}
      }
    } else it->matches = 0;
  }
  if (matchcnt == 0) {
    /*    fprintf(stdout,"--No match found for: ");   
    EMU_disassemble_instr(ii,stdout);
    */
  } else if (matchcnt>1) {
    fprintf(stdout,"--Multiple matches found for: ");   
    EMU_disassemble_instr(ii,stdout);
    for (i=0;i<NUMINSTS;i++)
      if (instructions[i].matches && !instructions[i].notaninst) 
	fprintf(stdout,"--\t%s\n",instructions[i].name);
    return 1;
  }

  if (ii->privatef.potential_intr) {
    /* must have been unimplemented... */
    fprintf(stdout,"--Decode error: %s ",
	    EMU_intr_string(ii->privatef.potential_intr));
    EMU_disassemble_instr(ii,stdout);
    if (matchno >= 0) instructions[matchno].mark = 1;
    if (ii->privatef.potential_intr == IA64_intr_notimplemented) return 0;
  }

  /* TODO: ALAT */

  /***** ARs *****/

  ecount+=check_dep(ii,"AR[BSP]",LSE_emu_spaceid_AR, IA64_ADDR_BSP, bits64, 0);
  ecount+=check_dep(ii,"AR[BSPSTORE]",
		    LSE_emu_spaceid_AR, IA64_ADDR_BSPSTORE, bits64, 0);
  ecount+=check_dep(ii,"AR[CCV]",
		    LSE_emu_spaceid_AR, IA64_ADDR_CCV, bits64, 0);
  ecount+=check_dep(ii,"AR[EC]",
		    LSE_emu_spaceid_AR, IA64_ADDR_EC, bits64, 0);
  ecount+=check_dep(ii,"AR[FPSR].sf0.controls",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_sf0_controls),0);
  ecount+=check_dep(ii,"AR[FPSR].sf1.controls",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_sf0_controls)<<13,0);
  ecount+=check_dep(ii,"AR[FPSR].sf2.controls",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_sf0_controls)<<26,0);
  ecount+=check_dep(ii,"AR[FPSR].sf3.controls",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_sf0_controls)<<39,0);
  ecount+=check_dep(ii,"AR[FPSR].sf0.flags",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_sf0_flags),0);
  ecount+=check_dep(ii,"AR[FPSR].sf1.flags",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_sf0_flags)<<13,0);
  ecount+=check_dep(ii,"AR[FPSR].sf2.flags",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_sf0_flags)<<26,0);
  ecount+=check_dep(ii,"AR[FPSR].sf3.flags",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_sf0_flags)<<39,0);
  ecount+=check_dep(ii,"AR[FPSR].traps",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_traps),0);
  ecount+=check_dep(ii,"AR[FPSR].rv",
		    LSE_emu_spaceid_AR, IA64_ADDR_FPSR, 
		    rf_mask(FPSR_rv),0);
  ecount+=check_dep(ii,"AR[ITC]",
		    LSE_emu_spaceid_AR, IA64_ADDR_ITC, bits64, 0);
  for (i=0;i<8;i++) {
    ecount+=check_dep(ii,"AR[K%]",
		      LSE_emu_spaceid_AR, i, bits64, 0);
  }
  ecount+=check_dep(ii,"AR[LC]",
		    LSE_emu_spaceid_AR, IA64_ADDR_LC, bits64, 0);
  ecount+=check_dep(ii,"AR[PFS]",
		    LSE_emu_spaceid_AR, IA64_ADDR_PFS, bits64, 0);
  ecount+=check_dep(ii,"AR[RNAT]",
		    LSE_emu_spaceid_AR, IA64_ADDR_RNAT, bits64, 0);
  ecount+=check_dep(ii,"AR[RSC]",
		    LSE_emu_spaceid_AR, IA64_ADDR_RSC, bits64, 0);
  ecount+=check_dep(ii,"AR[UNAT]",
		    LSE_emu_spaceid_AR, IA64_ADDR_UNAT, bits64, 0);
  for (i=0;i<128;i++) if (IA64_ar_reserved[i])
    ecount+=check_dep(ii,"AR[rv%]",
		      LSE_emu_spaceid_AR, i, bits64, 0);
  for (i=0;i<128;i++) if (IA64_ar_ignored[i])
    ecount+=check_dep(ii,"AR[ig%]",
		      LSE_emu_spaceid_AR, i, bits64, 0);

  for (i=0;i<8;i++) 
    ecount+=check_dep(ii,"BR[%]",
		      LSE_emu_spaceid_BR, i, bits64, 0);

  ecount+=check_dep(ii,"CFM",
		    LSE_emu_spaceid_SR, IA64_ADDR_CFM, bits64, 0);

  ecount+=check_dep(ii,"CR[CMCV]",
		    LSE_emu_spaceid_CR, IA64_ADDR_CMCV, bits64, 0);
  ecount+=check_dep(ii,"CR[DCR]",
		    LSE_emu_spaceid_CR, IA64_ADDR_DCR, bits64, 0);
  ecount+=check_dep(ii,"CR[EOI]",
		    LSE_emu_spaceid_CR, IA64_ADDR_EOI, bits64, 0);
  /* There is a mysterious GPTA in the book.... */
  ecount+=check_dep(ii,"CR[IFA]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IFA, bits64, 0);
  ecount+=check_dep(ii,"CR[IFS]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IFS, bits64, 0);
  ecount+=check_dep(ii,"CR[IHA]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IHA, bits64, 0);
  ecount+=check_dep(ii,"CR[IIM]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IIM, bits64, 0);
  ecount+=check_dep(ii,"CR[IIP]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IIP, bits64, 0);
  ecount+=check_dep(ii,"CR[IIPA]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IIPA, bits64, 0);
  ecount+=check_dep(ii,"CR[IPSR]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IPSR, bits64, 0);
  ecount+=check_dep(ii,"CR[IRR%]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IRR0, bits64, 0);
  ecount+=check_dep(ii,"CR[IRR%]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IRR1, bits64, 0);
  ecount+=check_dep(ii,"CR[IRR%]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IRR2, bits64, 0);
  ecount+=check_dep(ii,"CR[IRR%]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IRR3, bits64, 0);
  ecount+=check_dep(ii,"CR[ISR]",
		    LSE_emu_spaceid_CR, IA64_ADDR_ISR, bits64, 0);
  ecount+=check_dep(ii,"CR[ITIR]",
		    LSE_emu_spaceid_CR, IA64_ADDR_ITIR, bits64, 0);
  ecount+=check_dep(ii,"CR[ITM]",
		    LSE_emu_spaceid_CR, IA64_ADDR_ITM, bits64, 0);
  ecount+=check_dep(ii,"CR[ITV]",
		    LSE_emu_spaceid_CR, IA64_ADDR_ITV, bits64, 0);
  ecount+=check_dep(ii,"CR[IVA]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IVA, bits64, 0);
  ecount+=check_dep(ii,"CR[IVR]",
		    LSE_emu_spaceid_CR, IA64_ADDR_IVR, bits64, 0);
  ecount+=check_dep(ii,"CR[LID]",
		    LSE_emu_spaceid_CR, IA64_ADDR_LID, bits64, 0);
  ecount+=check_dep(ii,"CR[LRR%]",
		    LSE_emu_spaceid_CR, IA64_ADDR_LRR0, bits64, 0);
  ecount+=check_dep(ii,"CR[LRR%]",
		    LSE_emu_spaceid_CR, IA64_ADDR_LRR1, bits64, 0);
  ecount+=check_dep(ii,"CR[PMV]",
		    LSE_emu_spaceid_CR, IA64_ADDR_PMV, bits64, 0);
  ecount+=check_dep(ii,"CR[TPR]",
		    LSE_emu_spaceid_CR, IA64_ADDR_TPR, bits64, 0);
  for (i=0;i<128;i++) if (IA64_cr_reserved[i])
    ecount+=check_dep(ii,"CR[rv%]",
		      LSE_emu_spaceid_CR, i, bits64, 0);

  ecount+=check_dep(ii,"FR[0]",
		    LSE_emu_spaceid_FR, 0, bits64, bits64);
  ecount+=check_dep(ii,"FR[1]",
		    LSE_emu_spaceid_FR, 1, bits64, bits64);
  for (i=2;i<128;i++) 
    ecount+=check_dep(ii,"FR[%]",
		      LSE_emu_spaceid_FR, i, bits64, bits64);
  ecount+=check_dep(ii,"GR[0]",
		    LSE_emu_spaceid_GR, 0, bits64, 1);
  for (i=1;i<128;i++) 
    ecount+=check_dep(ii,"GR[%]",
		      LSE_emu_spaceid_GR, i, bits64, 1);


  ecount+=check_dep(ii,"PR[0]",
		    LSE_emu_spaceid_PR, 0, 1, 0);
  for (i=1;i<63;i++) 
    ecount+=check_dep(ii,"PR[%]",
		      LSE_emu_spaceid_PR, 0, UINT64_C(1)<<i, 1);
  ecount+=check_dep(ii,"PR[63]",
		    LSE_emu_spaceid_PR, 0, UINT64_C(1)<<63, 0);

  ecount+=check_dep(ii,"PSR.ac",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_ac),0);
  ecount+=check_dep(ii,"PSR.be",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_be),0);
  ecount+=check_dep(ii,"PSR.bn",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_bn),0);
  ecount+=check_dep(ii,"PSR.cpl",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_cpl),0);
  ecount+=check_dep(ii,"PSR.da",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_da),0);
  ecount+=check_dep(ii,"PSR.db",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_db),0);
  ecount+=check_dep(ii,"PSR.dd",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_dd),0);
  ecount+=check_dep(ii,"PSR.dfh",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_dfh),0);
  ecount+=check_dep(ii,"PSR.dfl",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_dfl),0);
  ecount+=check_dep(ii,"PSR.di",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_di),0);
  ecount+=check_dep(ii,"PSR.dt",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_dt),0);
  ecount+=check_dep(ii,"PSR.ed",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_ed),0);
  ecount+=check_dep(ii,"PSR.i",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_i),0);
  ecount+=check_dep(ii,"PSR.ia",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_ia),0);
  ecount+=check_dep(ii,"PSR.ic",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_ic),0);
  ecount+=check_dep(ii,"PSR.id",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_id),0);
  ecount+=check_dep(ii,"PSR.is",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_is),0);
  ecount+=check_dep(ii,"PSR.it",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_it),0);
  ecount+=check_dep(ii,"PSR.lp",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_lp),0);
  ecount+=check_dep(ii,"PSR.mc",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_mc),0);
  ecount+=check_dep(ii,"PSR.mfh",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_mfh),0);
  ecount+=check_dep(ii,"PSR.mfl",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_mfl),0);
  ecount+=check_dep(ii,"PSR.pk",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_pk),0);
  ecount+=check_dep(ii,"PSR.pp",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_pp),0);
  ecount+=check_dep(ii,"PSR.ri",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_ri),0);
  ecount+=check_dep(ii,"PSR.rt",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_rt),0);
  ecount+=check_dep(ii,"PSR.si",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_si),0);
  ecount+=check_dep(ii,"PSR.sp",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_sp),0);
  ecount+=check_dep(ii,"PSR.ss",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_ss),0);
  ecount+=check_dep(ii,"PSR.tb",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_tb),0);
  ecount+=check_dep(ii,"PSR.up",
		    LSE_emu_spaceid_SR, IA64_ADDR_PSR, 
		    rf_mask(PSR_up),0);

  /* TODO: RSE state */

  return ecount;
}

static void
start_dep_checks() {
  int i;
  for (i=0;i<NUMINSTS;i++) 
    instructions[i].mark = 0;
  for (i=0;i<NUMLISTS;i++) 
    lists[i].mark = 0;
  for (i=0;i<NUMREADERS;i++) 
    readers[i].mark = 0;
  for (i=0;i<NUMWRITERS;i++) 
    writers[i].mark = 0;

}

static void
end_dep_checks() {
  /*
  int i;
  for (i=0;i<NUMINSTS;i++) 
    if (!instructions[i].mark && !instructions[i].notaninst) {
      fprintf(stdout, "--No instance of instruction %s used\n",
	      instructions[i].name);
    }
  for (i=0;i<NUMLISTS;i++) 
    if (!lists[i].mark && lists[i].cnames[0]) {
      fprintf(stdout, "--No instruction in list %s used\n",
	      lists[i].name);
    }
  for (i=0;i<NUMREADERS;i++) 
    if (!readers[i].mark && readers[i].cnames[0]) {
      fprintf(stdout, "--No instruction in readers of %s used\n",
	      readers[i].name);
    }
  for (i=0;i<NUMWRITERS;i++) 
    if (!writers[i].mark && writers[i].cnames[0]) {
      fprintf(stdout, "--No instruction in writers of %s used\n",
	      writers[i].name);
    }
  */
}
