/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Book-keeping functions for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file keeps book-keeping functions for the emulator, such as the
 * the routines to manipulate contexts.
 *
 * TODO: 
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LSE_IA64.h"

namespace LSE_IA64 {

extern void Linux_schedule(LSE_emu_interface_t *);
extern int Linux_init(LSE_emu_interface_t *);
extern void Linux_finish(LSE_emu_interface_t *);
extern int Linux_context_load(IA64_context_t *,int,char **,char **);
extern int Linux_context_clone(LSE_emu_interface_t *,
			       IA64_context_t **, IA64_context_t *);

IA64_FR_t IA64_NatVAL = { 
  0, 0x1fffe, 0,
};

IA64_FR_t IA64_QNaN = { 
  (UINT64_C(3)<<62), 0x1ffff, 0,
};

IA64_FR_t IA64_Inf = { 
  (UINT64_C(1)<<63), 0x1ffff, 0,
};

IA64_FR_t IA64_FPZero = { 
  0, 0, 0, 
};

IA64_FR_t IA64_FPIntZero = { 
  0, 0x1003e, 0,
};

IA64_FR_t IA64_FPOne = { 
  UINT64_C(1)<<63, 0x0ffff, 0,
};

/*********************** Context bookkeeping *******************/

void EMU_context_destroy(IA64_context_t *&realct) {
  if (!realct) return;
  if (realct->ALAT) free(realct->ALAT);
  free(realct);
  realct = 0;
}

/*
 * Copy a context.  We do an "almost exact" copy... the ALAT state is
 * not copied.  However, the physical registers are, so we do not need
 * to flush them to the register stack.
 */
int EMU_context_copy(LSE_emu_interface_t *ifc,
		     IA64_context_t **ctpp, IA64_context_t *oldct) {
  IA64_context_t *realct;
  IA64_dinst_t *di = (IA64_dinst_t *)(ifc->etoken);
  int i;

  /* using calloc so valgrind won't complain */
  realct = (IA64_context_t *)calloc(1,sizeof(IA64_context_t));
  if (!realct) {
    fprintf(stderr, "Out of memory creating LSE IA64 emulator context\n");
    return 1;
  }
  if (oldct) *realct = *oldct;
  else {
    realct->done = FALSE;
    realct->di = di;
    realct->mem = di->mem;

    /* Constant values */
    realct->p[0] = 1;
    realct->r[0].nat = 0;
    realct->r[0].val = 0;
    realct->f[0] = IA64_FPZero;
    realct->f[1] = IA64_FPOne;

    /* Create some initial values ... this is the "hardware" reset value. */
    realct->RSE_other.NDirty = 0;
    realct->RSE_other.NClean = 0;

    uint64_t newcfm = 0;
    rf_set(newcfm,CFM_sof,96);
    realct->sr[IA64_ADDR_CFM].CFM.val = newcfm; /* hardware reset value */
    realct->sr[IA64_ADDR_CFM].CFM.BOF = 0;      /* bottom of frame */

    /* can't set the nat bits to 1 because code might spill the 
       registers and then screw up UNAT (this actually happens
       in glibc! */
    for (i=1;i<48;i++) realct->r[i].nat = 0;
    for (i=1;i<64;i++) realct->p[i] = 0;
    realct->num_stacked_phys = 96;
    for (i=0;i<96;i++)
      realct->physr[i].nat = 0;
    for (i=2;i<128;i++)
      realct->f[i] = IA64_NatVAL;
    for (i=0;i<128;i++)
      realct->ar[i] = 0;
  }
  realct->hwcno = 0; // initially unmapped

  /* initialize ALAT */
  realct->ALAT_numentries = 0;
  realct->ALAT_size = 1024;
  realct->ALAT = (IA64_ALAT_t *)malloc(1024 * sizeof(IA64_ALAT_t));
  if (!realct->ALAT) {
    fprintf(stderr, "Out of memory creating LSE IA64 emulator context\n");
    free(realct);
    return 1;
  }

#ifdef LATER
    realct->softmmu.clear();
#endif

  *ctpp = realct;
  return 0;
}

/* 
 * Called by simulator, but not emulators to create software contexts 
 */
int EMU_context_create(LSE_emu_interface_t *ifc, 
		       LSE_emu_ctoken_t *ctp, int cno) {
  if (cno >= 0) {
    *ctp = 0; // not mapped.
    return 0; 
  } else return 1;
}

/*
 * execve, really.... 
 */
  int EMU_context_load(LSE_emu_interface_t *ifc, int cno,
		     int argc, char *argv[], char **envp) {

  if (cno <= 0 || cno > LSE_emu_hwcontexts_total ||
      !LSE_emu_hwcontexts_table[cno].valid) return 1;

  IA64_context_t *realctp 
        = (IA64_context_t *)(LSE_emu_hwcontexts_table[cno].ctok);

  // loading into an empty context is the same as running execve off of
  // a process forked from init.  Thus we need to call Linux_context_clone.
  if (!realctp) {
    int ret = Linux_context_clone(ifc, &realctp, 0);
    if (ret) return ret;
    realctp->hwcno = cno;
    LSE_emu_update_context_map(cno, realctp);
  }

  LSE_env::env_t tempenv((const char **)envp);
  tempenv.setenv(*realctp->di->evars, true);
  char **newenvp = tempenv.getenvbuf();

  int retval = Linux_context_load(realctp, argc, argv, newenvp);

  if (retval) fprintf(stderr, "LSE_IA64 emulator:Unable to read executable "
		      "\"%s\", errno was %d\n",
		      argv[0],-retval);
  if (newenvp) ::free(newenvp);
  return retval;
}

/*
 * get the starting address of a context
 */
LSE_emu_addr_t EMU_get_start_addr(LSE_emu_ctoken_t ctx) { 
  IA64_context_t *realct = (IA64_context_t *)ctx;
  return realct->startaddr;
}

/*
 * set the starting address of a context
 */
void EMU_set_start_addr(LSE_emu_ctoken_t ctx, LSE_emu_addr_t addr) { 
  IA64_context_t *realct = (IA64_context_t *)ctx;
  realct->startaddr = addr;
}

/************************ start and finish *****************************/
extern void decode_init(void);

void 
EMU_init(LSE_emu_interface_t *ifc) 
{
  IA64_dinst_t *di = new IA64_dinst_t(ifc);
  ifc->etoken = static_cast<void *>(di);
  decode_init();
  Linux_init(ifc);
}

void EMU_finish(LSE_emu_interface_t *intr) {
  Linux_finish(intr);
  if (intr->etoken) delete static_cast<IA64_dinst_t *>(intr->etoken);
  intr->etoken = 0;
}

int 
EMU_parse_arg(LSE_emu_interface_t *ifc, int argc, char *arg, char *argv[]) 
{
  IA64_dinst_t *di = (IA64_dinst_t *)(ifc->etoken);
  if (!strcmp(arg,"debugload")) di->EMU_debugcontextload = 1;
  else if (!strcmp(arg,"tracesyscalls")) di->EMU_trace_syscalls = 1;
  else if (!strncmp(arg,"env:",4)) {
    di->evars->setenv(arg+4, true);
  } else if (!strcmp(arg,"cleanenv")) {
    di->evars->clearenv();
  }
  else return 0;
  return 1;
}

void 
EMU_print_usage(LSE_emu_interface_t *ifc) 
{
  fprintf(stderr, "\tdebugload\t\tPrint debug messages when loading\n");
  fprintf(stderr, "\ttracesyscalls\t\tTrace system calls\n");
  return;
}

/************************ miscellaneous *****************************/

boolean 
EMU_spaceaddr_is_constant(LSE_emu_ctoken_t ctx,
			  LSE_emu_spaceid_t sid,
			  LSE_emu_spaceaddr_t *addr) 
{
  return ((sid == LSE_emu_spaceid_GR && addr->GR == 0) ||
	  (sid == LSE_emu_spaceid_FR && addr->FR == 0) ||
	  (sid == LSE_emu_spaceid_FR && addr->FR == 1) ||
	  (sid == LSE_emu_spaceid_PR && addr->PR == 0));
}

/*********************** instruction running ************************/

extern void EMU_dofront(LSE_emu_instr_info_t *ii);
extern void do_decode(LSE_emu_instr_info_t *ii);

void do_nothing(LSE_emu_instr_info_t *ii) {
  return;
}

void do_nothing1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname, 
		 int fallthrough) {
  return;
}

void not_implemented(LSE_emu_instr_info_t *ii) {
  /*  fprintf(stderr,"%d\n",ii->extra.formatno);*/
  ii->privatef.actual_intr = 
    ii->privatef.potential_intr = (int)IA64_intr_notimplemented;
  return;
}

void 
not_implemented_of1(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname, 
		    int fallthrough) 
{
  /*  fprintf(stderr,"%d\n",ii->extra.formatno);*/
  ii->privatef.actual_intr = 
    ii->privatef.potential_intr = (int)IA64_intr_notimplemented;
}

void 
EMU_do_step(LSE_emu_instr_info_t *ii, 
	    LSE_emu_instrstep_name_t sname, boolean isSpeculative) {
  int i;

  switch (sname) {
  case 0: /* fetch */
    {
      LSE_emu_addr_t addr = ii->addr & ~0xf;
      IA64_context_t *realct = (IA64_context_t *)ii->swcontexttok;

#ifdef LATER
      void *ha = realct->softmmu.translate(addr, softmmu_t::Ifetch,
                                           MMUhandler(ctx, LIS_ii.fault));
      instr = LIS_ii.fault ? 0 : LSE_b2h(*reinterpret_cast<uint32_t *>(ha));
#endif
      try {
        realct->mem->read(addr, (unsigned char *)ii->extra.bundle.bytes, 16);
      } catch (LSE_device::deverror_t) {
	report_actual_intr(ii,IA64_intr_instructionpagenotpresent);
	/* For consistency, set the bundle bytes to something
	   constant.  This ensures that simulators have consistent
	   behavior even when attempting to execute instructions from
	   bad pages (due to speculation) */
	memset((char*)ii->extra.bundle.bytes, 0, 16);
      }
    }
    break;
  case 1: /* decode */
    do_decode(ii);
    break;
  case 2 : /* opfetch */
    (*(decode_details[ii->privatef.formatint].funcs.opfetch_func))(ii,
						(LSE_emu_operand_name_t)0,1);
    break;
  case 3 : /* evaluate */
    ii->next_pc = ii->branch_targets[0];
    if (rf_get(ii->operand_val_src[src_psr].data.SR.PSR,PSR_ss)) {
      report_actual_intr(ii,IA64_intr_singlestep);
    }
    (*(decode_details[ii->privatef.formatint].funcs.evaluate_func))(ii);
    break;
  case 4 : /* memory (named for ld .. for store this is actually format) */
    (*(decode_details[ii->privatef.formatint].funcs.memory_func))(ii);
    break;
  case 5 : /* format (named for ld .. for store this is actually memory */
    (*(decode_details[ii->privatef.formatint].funcs.format_func))(ii);
    break;
  case 6 : /* writeback */
    (*(decode_details[ii->privatef.formatint].funcs.writeback_func))(ii,
					(LSE_emu_operand_name_t)0,1);
    for (i=0;i<LSE_emu_max_operand_dest;i++) ii->operand_written_dest[i] = 1;
    break;
  default : /* ignore */
    break;
  }
}

void 
EMU_do_all_steps(LSE_emu_instr_info_t *ii) {
  int formatint;

  LSE_emu_addr_t addr = ii->addr & ~0xf;
  IA64_context_t *realct = (IA64_context_t *)ii->swcontexttok;

  /* do light-weight init 
   * MUST KEEP UP TO DATE WITH EMU_init_instr!
   */
  ii->privatef.rollback_saved = ~0; /* no rollbacks! */

  try {
    realct->mem->read(addr, (unsigned char *)ii->extra.bundle.bytes, 16);
  } catch (LSE_device::deverror_t) {
    report_actual_intr(ii,IA64_intr_instructionpagenotpresent);
    return;
  }

  do_decode(ii);
  formatint = ii->privatef.formatint;

  if (decode_details[formatint].funcs.all_backend_func) {
    ii->next_pc = ii->branch_targets[0];
    (*(decode_details[formatint].funcs.all_backend_func))(ii);
  } else {
    (*(decode_details[formatint].funcs.opfetch_func))(ii,
						    (LSE_emu_operand_name_t)0,
						      1);

    ii->next_pc = ii->branch_targets[0];
    if (rf_get(ii->operand_val_src[src_psr].data.SR.PSR,PSR_ss)) {
      report_actual_intr(ii,IA64_intr_singlestep);
    }
    (*(decode_details[formatint].funcs.evaluate_func))(ii);
    (*(decode_details[formatint].funcs.memory_func))(ii);
    (*(decode_details[formatint].funcs.format_func))(ii);
    (*(decode_details[formatint].funcs.writeback_func))\
      (ii,(LSE_emu_operand_name_t)0,1);
  }
}

void 
EMU_fetch_operand(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,
		  boolean isSpeculative) 
{
  (*(decode_details[ii->privatef.formatint].funcs.opfetch_func))(ii,opname,0);
}

void 
EMU_writeback_operand(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t opname,
		      boolean isSpeculative) 
{
  (*(decode_details[ii->privatef.formatint].funcs.writeback_func))
    (ii,opname,0);
  ii->operand_written_dest[opname] = 1;
}

void EMU_init_instr(LSE_emu_instr_info_t *ii, int hwcno, 
		    LSE_emu_ctoken_t swcontexttok, const LSE_emu_iaddr_t &addr) {
  /* Profiling shows this function to be a bottleneck.  The things which
   * must be zeroed are:
   *   privatef.rollback_saved
   *   privatef.potential_intr, privatef.actual_intr
   *   extra.formatno (for special decode cases)
   *   branch_dir
   *   operand_val_src[].valid
   *   operand_val_dest[].valid
   *   operand_val_int[].valid
   *   operand_written_dest[]
   *   operand_src[]
   *   operand_dest[]
   *
   *   With this amount of stuff to zero, it turns out that it is faster
   *   just to save the four fields that are valid on entry, zero everything,
   *   and restore four fields.
   */
#ifdef IA64_INIT_INDIVIDUALLY /* As of 14 Sep 04, individual is much slower */
  int i;
  ii->privatef.potential_intr = ii->privatef.actual_intr = 0;
  ii->branch_dir = 0;
  for (i=0;i<LSE_emu_max_operand_src;i++)
    ii->operand_val_src[i].valid = 0;
  for (i=0;i<LSE_emu_max_operand_dest;i++) {
    ii->operand_val_dest[i].valid = 0;
    ii->operand_written_dest[i] = FALSE;
  }
  for (i=0;i<LSE_emu_max_operand_int;i++)
    ii->operand_val_int[i].valid = 0;
  memset(ii->operand_src,0,sizeof(ii->operand_src));
  memset(ii->operand_dest,0,sizeof(ii->operand_dest));
  ii->extra.formatno = 0;
  ii->privatef.rollback_saved = 0;
#else

  /* clear all the standard stuff */
  memset(ii,0,sizeof(*ii) - sizeof(ii->extra) - sizeof(ii->privatef));
  /* and hit the things in extra and private we need to hit */
  ii->extra.formatno = 0; 
  ii->privatef.rollback_saved = 0;
  ii->privatef.potential_intr = ii->privatef.actual_intr = 0;

#endif
  /* set valid fields */
  ii->addr = addr;
  ii->swcontexttok = swcontexttok;
  ii->hwcontextno = hwcno;
}

int
EMU_resolve_operand(LSE_emu_instr_info_t *ii, LSE_emu_operand_name_t i, int resolveOp) {

  IA64_context_t *realct = (IA64_context_t *)(ii->swcontexttok);
  int j;
  uint64_t tval,mask;

  if (resolveOp != LSE_emu_resolveOp_rollback) return 0;

  if (~ii->privatef.rollback_saved & (1<<i)) return 0;
  switch (ii->privatef.rollback[i].rtype) {
  case IA64_rollback_copy:
    memcpy(ii->privatef.rollback[i].data.copy.host_addr,
	   ii->privatef.rollback[i].data.copy.data,
	   ii->privatef.rollback[i].data.copy.size);
    break;
  case IA64_rollback_bits:
    tval = *ii->privatef.rollback[i].data.bits.host_addr;
    tval = ((tval & ~ii->privatef.rollback[i].data.bits.mask) |
	    (ii->privatef.rollback[i].data.bits.old & 
	     ii->privatef.rollback[i].data.bits.mask));
    *ii->privatef.rollback[i].data.bits.host_addr = tval;
    break;
  case IA64_rollback_mem:
    realct->mem->write(ii->privatef.rollback[i].data.mem.addr,
		       ii->privatef.rollback[i].data.mem.data,
		       ii->privatef.rollback[i].data.mem.len);
    break;
  case IA64_rollback_preds:
    tval = ii->privatef.rollback[i].data.preds.old;
    mask = ii->privatef.rollback[i].data.preds.mask;
    mask >>= 1;
    tval >>= 1;
    for (j=1;j<64;j++) {
      if (mask & 1) realct->p[j] = tval & 1;
      mask >>= 1;
      tval >>= 1;
    }
    break;
  case IA64_rollback_alat:
    /* Just invalidate an inserted entry; we cannot roll back
     * speculative invalidations
     */
    for (j=realct->ALAT_numentries-1;j>=0;j--) {
      if (realct->ALAT[j].tag == ii->privatef.rollback[i].data.alat.tag) {
	realct->ALAT[j] = realct->ALAT[realct->ALAT_numentries--];
	break;
      }
    }
    break;
  case IA64_rollback_bof:
    /* copy back CFM and BOF stuff */
    realct->sr[IA64_ADDR_CFM].CFM.val 
      = ii->privatef.rollback[i].data.BOF.CFM;
    realct->sr[IA64_ADDR_CFM].CFM.BOF
      = ii->privatef.rollback[i].data.BOF.BOF;
    break;

  default: break;
  }   
  ii->privatef.rollback[i].rtype = IA64_rollback_empty;
  return 0;
}

/************************* Extra functions ****************************/

/*
 * Perform register rotation, banking, and stacking calculations.  What
 * it does is change the register into a "physical" register number
 * according to the normal IA64 rules, including banking.  So, the GR
 * assignments are:
 *    r0-15:   0-15
 *    r16-31, bank 0:  16-31
 *    r16-31, bank 1:  32-47
 *    r32+ : 48+ 
 *
 */
void
IA64_remap_regaddr(LSE_emu_operand_info_t *oi,
		   LSE_emu_operand_val_t *CFM,
		   int bankno,
		   uint32_t numStackedRegs) {

  switch (oi->spaceid) {
  case LSE_emu_spaceid_GR :
    {
      long rrb = rf_get(CFM->data.SR.CFM.val,CFM_rrb_gr);
      long sor = rf_get(CFM->data.SR.CFM.val,CFM_sor)*8;
      long bof = CFM->data.SR.CFM.BOF;
      oi->spaceaddr.GR = get_r_pno(oi->spaceaddr.GR,rrb,sor,bof,
				   bankno,numStackedRegs);
    }
    break;
  case LSE_emu_spaceid_PR :
    {
      int rrb = rf_get(CFM->data.SR.CFM.val,CFM_rrb_pr);
      if (oi->spaceaddr.PR != 64) 
	oi->spaceaddr.PR = IA64_RT_PR_NO(oi->spaceaddr.PR,rrb);
    }
    break;
  case LSE_emu_spaceid_FR :  
    {
      int rrb = rf_get(CFM->data.SR.CFM.val,CFM_rrb_fr);
      oi->spaceaddr.FR = IA64_RT_FR_NO(oi->spaceaddr.FR,rrb);
    }
    break;
  case LSE_emu_spaceid_ALAT :
    { /* duplicate the tag calculation... */
      if (oi->spaceaddr.ALAT > 0) {
	if (oi->spaceaddr.ALAT < 128) { /* a GR location */
	  int rrb = rf_get(CFM->data.SR.CFM.val,CFM_rrb_gr);
	  int sor = rf_get(CFM->data.SR.CFM.val,CFM_sor)*8;
	  long bof = CFM->data.SR.CFM.BOF;
	  oi->spaceaddr.GR = 128 + get_r_pno(oi->spaceaddr.GR,rrb,sor,bof,
					     bankno,numStackedRegs);
	} else { /* FR location... */
	  int rrb = rf_get(CFM->data.SR.CFM.val,CFM_rrb_fr);
	  oi->spaceaddr.FR = IA64_RT_FR_NO(oi->spaceaddr.FR-128,rrb);
	}
      }
    }
    break;
  default:
    break;
  }
  return;
}

void
IA64_print_instr_oper_vals(FILE *fp, const char *prefix,
			   LSE_emu_instr_info_t *ii) {
  int k;
  LSE_emu_operand_info_t opinf;

  /* destinations */

  fprintf(fp,"%s",prefix);
  if (ii->iclasses.is_load || ii->iclasses.is_store) {
    LSE_emu_addr_t eaddr;
    if(ii->iclasses.is_load)
      eaddr = ii->operand_src[LSE_emu_operand_name_src_load].\
	spaceaddr.mem;
    else
      eaddr = ii->operand_dest[LSE_emu_operand_name_dest_store].\
	spaceaddr.mem;
    fprintf(fp, "EA 0x%"PRIx64", ",eaddr);
  }
  for(k=0;k<LSE_emu_max_operand_dest;k++) {
    switch(ii->operand_dest[k].spaceid) {
    case LSE_emu_spaceid_LSE:
      continue;
    case LSE_emu_spaceid_GR:
      fprintf(fp,"G%d", k);
      fprintf(fp,":0x%"PRIx64"%c ",ii->operand_val_dest[k].data.GR.val,
		ii->operand_val_dest[k].data.GR.nat?'N':' ');
      break;
    case LSE_emu_spaceid_FR:
      fprintf(fp,"F%d", k);
      fprintf(fp,":%c0x%05x:0x%"PRIx64" ",
	      (ii->operand_val_dest[k].data.FR.sign ?'-':'+'),
	      ii->operand_val_dest[k].data.FR.exponent,
	      ii->operand_val_dest[k].data.FR.significand);
      break;
    case LSE_emu_spaceid_BR:
      fprintf(fp,"B%d", k);
      fprintf(fp,":0x%"PRIx64" ",ii->operand_val_dest[k].data.BR);
      break;
    case LSE_emu_spaceid_PR:
      fprintf(fp,"P%d", k);
      fprintf(fp,":%d ", ii->operand_val_dest[k].data.PR);
      break;
    case LSE_emu_spaceid_SR:
      fprintf(fp,"S%d", k);
      if (ii->operand_dest[k].spaceaddr.SR == IA64_ADDR_CFM) {
	fprintf(fp,":0x%"PRIx64"/%d ",ii->operand_val_dest[k].data.SR.CFM.val,
		ii->operand_val_dest[k].data.SR.CFM.BOF);
      } else {
	fprintf(fp,":0x%"PRIx64" ",(ii->operand_val_dest[k].data.SR.PSR & 
			       ii->operand_dest[k].uses.reg.bits[0]));
      }
      break;
    case LSE_emu_spaceid_AR:
      fprintf(fp,"A%d", k);
      fprintf(fp,":0x%"PRIx64" ",ii->operand_val_dest[k].data.AR);
      break;
    case LSE_emu_spaceid_CR:
      fprintf(fp,"C%d", k);
      fprintf(fp,":0x%"PRIx64" ",ii->operand_val_dest[k].data.CR);
      break;
    case LSE_emu_spaceid_ALAT:
      fprintf(fp,"ALAT%d", k);
      fprintf(fp,":%c ",ii->operand_val_dest[k].data.alatchange?
	      (ii->operand_val_dest[k].data.alatchange>0?'+':'-'):'.');
      break;
    case LSE_emu_spaceid_RSE:
      fprintf(fp,"RSE%d", k);
      fprintf(fp,":d%d/c%d ",ii->operand_val_dest[k].data.RSE_other.NDirty,
	      ii->operand_val_dest[k].data.RSE_other.NClean);
      break;

    default:
      break;
    }

  }

  /* RSE nonsense... */
  opinf = ii->operand_dest[LSE_emu_operand_name_dest_rsemem];
  if (opinf.spaceid==LSE_emu_spaceid_LSE && opinf.spaceaddr.LSE==2) {
    int chg = ii->operand_val_dest[LSE_emu_operand_name_dest_rsemem].data.\
      rsechange.numregs;
    int chg2 = ii->operand_val_dest[LSE_emu_operand_name_dest_rsemem].data.\
      rsechange.numwords;
    if (chg < 0 )
      fprintf(fp,"RSEfill %d/%d", -chg, -chg2);
    else if (chg)
      fprintf(fp,"RSEspill %d/%d", chg, chg2);
  }

  /* sources */

  fprintf(fp,"\n%s",prefix);

  fprintf(fp, "CFM 0x%"PRIx64"/%d ", ii->operand_val_src[0].data.SR.CFM.val,
	  ii->operand_val_src[0].data.SR.CFM.BOF);
  for(k=1;k<LSE_emu_max_operand_src;k++) {
    switch(ii->operand_src[k].spaceid) {
    case LSE_emu_spaceid_LSE:
      continue;
    case LSE_emu_spaceid_GR:
      fprintf(fp,"G%d", k);
      fprintf(fp,"-0x%"PRIx64"%c ",ii->operand_val_src[k].data.GR.val,
	ii->operand_val_src[k].data.GR.nat?'N':' ');
      break;
    case LSE_emu_spaceid_FR:
      fprintf(fp,"F%d", k);
      fprintf(fp,":%c0x%05x:0x%"PRIx64" ",
	      (ii->operand_val_src[k].data.FR.sign ?'-':'+'),
	      ii->operand_val_src[k].data.FR.exponent,
	      ii->operand_val_src[k].data.FR.significand);
      break;
    case LSE_emu_spaceid_BR:
      fprintf(fp,"B%d", k);
      fprintf(fp,":0x%"PRIx64" ",ii->operand_val_src[k].data.BR);
      break;
    case LSE_emu_spaceid_PR:
      fprintf(fp,"P%d", k);
      fprintf(fp,":%d ", ii->operand_val_src[k].data.PR);
      break;
    case LSE_emu_spaceid_SR:
      fprintf(fp,"S%d", k);
      fprintf(fp,":0x%"PRIx64" ",(ii->operand_val_src[k].data.SR.PSR & 
			     ii->operand_src[k].uses.reg.bits[0]));
      break;
    case LSE_emu_spaceid_AR:
      fprintf(fp,"A%d", k);
      fprintf(fp,":0x%"PRIx64" ",ii->operand_val_src[k].data.AR);
      break;
    case LSE_emu_spaceid_CR:
      fprintf(fp,"C%d", k);
      fprintf(fp,":0x%"PRIx64" ",ii->operand_val_src[k].data.CR);
      break;
    case LSE_emu_spaceid_RSE:
      fprintf(fp,"RSE%d", k);
      fprintf(fp,":d%d/c%d ",ii->operand_val_src[k].data.RSE_other.NDirty,
	      ii->operand_val_src[k].data.RSE_other.NClean);
      break;
    default:
      break;
    }
  }
  fprintf(fp,"\n");
  
}

/************* access capability support ******************/
/* NOTE: these functions access *rotated* internal register addresses;
 */

void 
EMU_space_read(LSE_emu_spacedata_t *sdata, 
	       LSE_emu_ctoken_t ctoken, 
	       LSE_emu_spaceid_t sid, LSE_emu_spaceaddr_t *saddr,
	       int flags)
{
  IA64_context_t *realct = (IA64_context_t *)ctoken;

  switch (sid) {
  case LSE_emu_spaceid_GR:
    sdata->GR = (saddr->GR < 48 ? realct->r[saddr->GR] : 
		 realct->physr[saddr->GR - 48]);
    break;
  case LSE_emu_spaceid_FR:
    sdata->FR = realct->f[saddr->FR];
    break;
  case LSE_emu_spaceid_BR:
    sdata->BR = realct->b[saddr->BR];
    break;
  case LSE_emu_spaceid_PR:
    if (saddr->PR == 64) {
      uint64_t tval=0;
      int i;
      for (i=0;i<64;i++) 
	tval |= (realct->p[i] ? (UINT64_C(1)<<i) : 0);
    } else sdata->PR = realct->p[saddr->PR];
    break;
  case LSE_emu_spaceid_SR:
    sdata->SR = realct->sr[saddr->SR];
    break;
  case LSE_emu_spaceid_AR:
    sdata->AR = realct->ar[saddr->AR];
    break;
  case LSE_emu_spaceid_CR:
    sdata->CR = 0; /* CRs not implemented yet... */
    break;
  case LSE_emu_spaceid_mem:
    { 
      uint64_t len2=flags;
      realct->mem->read(saddr->mem,(LSE_device::devdata_t *)&sdata->mem[0], 
			len2);
    }
    break;
  case LSE_emu_spaceid_RSE:
  case LSE_emu_spaceid_LSE:
  default:
    break;
  } /* switch (sid) */
}

void 
EMU_space_write(LSE_emu_ctoken_t ctoken, 
		LSE_emu_spaceid_t sid, LSE_emu_spaceaddr_t *saddr, 
		LSE_emu_spacedata_t *sdata, int flags)
{
  IA64_context_t *realct = (IA64_context_t *)ctoken;

  switch (sid) {
  case LSE_emu_spaceid_GR:
    if (saddr->GR < 48) realct->r[saddr->GR] = sdata->GR;
    else realct->physr[saddr->GR-48] = sdata->GR;
    break;
  case LSE_emu_spaceid_FR:
    if (saddr->FR >= 2) realct->f[saddr->FR] = sdata->FR;
    break;
  case LSE_emu_spaceid_BR:
    realct->b[saddr->BR] = sdata->BR;
    break;
  case LSE_emu_spaceid_PR:
    if (saddr->PR == 64) {
      int i;
      for (i=1;i<64;i++)
	realct->p[i] = (sdata->PR >> i) & 1;
    } else realct->p[saddr->PR] = sdata->PR ? 1 : 0;
    break;
  case LSE_emu_spaceid_SR:
    realct->sr[saddr->SR] = sdata->SR;
    break;
  case LSE_emu_spaceid_AR:
    realct->ar[saddr->AR] = sdata->AR;
    break;
  case LSE_emu_spaceid_CR:
    /* CRs not implemented yet... */
    break;
  case LSE_emu_spaceid_mem:
    { 
      uint64_t len2=flags;
      realct->mem->write(saddr->mem, (LSE_device::devdata_t *)&sdata->mem[0], 
			 len2);
    }
    break;
  case LSE_emu_spaceid_RSE:
  case LSE_emu_spaceid_LSE:
  default:
    break;
  } /* switch (sid) */
}

} // namespace LSE_IA64
