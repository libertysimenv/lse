/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Decode tables for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file holds the decode tables.
 *
 * Table references are to the Intel(R) IA-64 Architecture Software
 *  Developer's Manual
 *
 */
#include "LSE_IA64.h"
namespace LSE_IA64 {
extern IA64_decode_t Table_4_8;
extern IA64_decode_t Table_4_9;
extern IA64_decode_t Table_4_10_and_11;
extern IA64_decode_t Table_4_12_0;
extern IA64_decode_t Table_4_12_1;
extern IA64_decode_t Table_4_13;
extern IA64_decode_t Table_4_14;
extern IA64_decode_t Table_4_15;
extern IA64_decode_t Table_4_16;
extern IA64_decode_t Table_4_17_2;
extern IA64_decode_t Table_4_17_3;
extern IA64_decode_t Table_4_18_0;
extern IA64_decode_t Table_4_18_1;
extern IA64_decode_t Table_4_18_2;
extern IA64_decode_t Table_4_18_3;
extern IA64_decode_t Table_4_19_0;
extern IA64_decode_t Table_4_19_1;
extern IA64_decode_t Table_4_19_2;
extern IA64_decode_t Table_4_19_3;
extern IA64_decode_t Table_4_20_0;
extern IA64_decode_t Table_4_21_and_22;
extern IA64_decode_t Table_4_22_extra;
extern IA64_decode_t Table_4_23;
extern IA64_decode_t Table_4_24;
extern IA64_decode_t Table_4_25;
extern IA64_decode_t Table_4_27_0;
extern IA64_decode_t Table_4_27_1;
extern IA64_decode_t Table_4_28_0;
extern IA64_decode_t Table_4_28_1;
extern IA64_decode_t Table_4_29;
extern IA64_decode_t Table_4_30;
extern IA64_decode_t Table_4_31;
extern IA64_decode_t Table_4_32;
extern IA64_decode_t Table_4_33;
extern IA64_decode_t Table_4_34;
extern IA64_decode_t Table_4_35;
extern IA64_decode_t Table_4_36;
extern IA64_decode_t Table_4_37;
extern IA64_decode_t Table_4_41;
extern IA64_decode_t Table_4_42;
extern IA64_decode_t Table_4_43;
extern IA64_decode_t Table_4_44;
extern IA64_decode_t Table_4_45;
extern IA64_decode_t Table_4_46;
extern IA64_decode_t Table_4_47;
extern IA64_decode_t Table_4_48;
extern IA64_decode_t Table_4_53;
extern IA64_decode_t Table_4_57_0;
extern IA64_decode_t Table_4_57_1;
extern IA64_decode_t Table_4_58;
extern IA64_decode_t Table_4_59;
extern IA64_decode_t Table_4_60;
extern IA64_decode_t Table_4_63;
extern IA64_decode_t Table_4_66;
extern IA64_decode_t Table_4_67;
extern IA64_decode_t Table_4_68;

/******************** Table 4-3 *********************/

IA64_decode_t Itable = {
  0xf, /* mask - 4 bits for major opcode */
  37,  /* location of major opcode field */
  {
    { 0, &Table_4_24}, /* Misc */
    {-2}, {-2}, {-2},
    { 15 }, /* Deposit */
    { 0, &Table_4_21_and_22}, /* shift/test bit */
    { -2 },
    { 0, &Table_4_16} , /* MM Mpy/Shift */

    { 0, &Table_4_8} , /* ALU/MM ALU (table 4-8) */
    { 505 }, /* Add immediate22 */
    { -2 }, { -2 },
    { 0, &Table_4_10_and_11} , /* compare */
    { 0, &Table_4_10_and_11} , /* compare */
    { 0, &Table_4_10_and_11} , /* compare */
    { -2 }
  }
};

IA64_decode_t Mtable = {
  0x1f, /* mask - 5 bits for major opcode+m */
  36,  /* location of major opcode field */
  {
    { 0, &Table_4_41}, { 0, &Table_4_41}, /* Sys/Mem Mgmt */
    { 0, &Table_4_43}, { 0, &Table_4_43}, /* Sys/Mem Mgmt */
    { -2 }, { -2 },
    { -2 }, { -2 },

    { 0, &Table_4_27_0}, { 0, &Table_4_27_1}, /* Int Ld/St + Reg/getf */
    { 0, &Table_4_31}, { 0, &Table_4_31}, /* Int Ld/St + imm */
    { 0, &Table_4_28_0}, { 0, &Table_4_28_1}, /* FP Ld/St + Reg/setf */
    { 0, &Table_4_35}, { 0, &Table_4_35}, /* FP Ld/St + imm */

    { 0, &Table_4_8}, { 0, &Table_4_8}, /* ALU/MM ALU (table 4-8) */
    { 505 }, { 505 }, /* Add immediate22 */
    { -2 }, { -2 },
    { -2 }, { -2 },

    { 0, &Table_4_10_and_11}, { 0, &Table_4_10_and_11} , /* compare */
    { 0, &Table_4_10_and_11}, { 0, &Table_4_10_and_11} , /* compare */
    { 0, &Table_4_10_and_11}, { 0, &Table_4_10_and_11} , /* compare */
    { -2 }, { -2 }
  }
};

IA64_decode_t Ftable = {
  0xf, /* mask - 4 bits for major opcode */
  37,  /* location of major opcode field */
  {
    { 0, &Table_4_57_0},
    { 0, &Table_4_57_1},
    { -2 }, 
    { -2 }, 
    { 204 },
    { 205 },
    { -2 }, 
    { -2 }, 
    { 201 },
    { 201 },
    { 201 },
    { 201 },
    { 201 },
    { 201 },
    { 0, &Table_4_63 }, /* Not yet */
    { -2 }, 
  }
};

IA64_decode_t Btable = {
  0xf, /* mask - 4 bits for major opcode */
  37,  /* location of major opcode field */
  {
    { 0, &Table_4_46},
    { 305 }, 
    { 0, &Table_4_53},
    { -4 },
    { 0, &Table_4_45},
    { 303 },
    { -4 }, 
    { 306 },
    { -3 }, 
    { -3 }, 
    { -3 }, 
    { -3 }, 
    { -3 }, 
    { -3 }, 
    { -3 }, 
    { -3 }, 
  }
};

IA64_decode_t Ltable = {
  0xf, /* mask - 4 bits for major opcode */
  37,  /* location of major opcode field */
  {
    { 0, &Table_4_66}, /* Misc */
    { -2 }, { -2 }, { -2 }, { -2 }, { -2 }, 
    { 0, &Table_4_68}, /* movl */
    { -2 },

    { -3 }, { -3 }, { -3 }, { -3 },
    { 403 }, /* long branch */
    { 404 }, /* long call */
    { -3 }, { -3 }, 
  }
};

IA64_decode_t Table_4_8 = {
  0x7,  /* 3 bits */
  33,   /* x2a+ve field */
  {
    { 0, &Table_4_9 }, /* Integer ALU 4-bit+2-bit */
    { -2 }, 
    { 0, &Table_4_12_0 }, /* Multi-media */
    { 0, &Table_4_12_1 }, /* Multi-media */
    { 504 }, /* adds imm14 */
    { -2 }, 
    { 504 }, /* addp4 imm14 */
    { -2 }, 
  }
};

IA64_decode_t Table_4_9 = { 
  0x3f,  /* 4 bits */
  27,   /* x4:x2b field */
  {
    { 501 }, { 501 }, { -2 } , { -2 },
    { 501 }, { 501 }, { -2 } , { -2 },
    { 501 }, { -2 }, { -2 } , { -2 },
    { 501 }, { 501 }, { 501 } , { 501 },

    { 502 }, { 502 }, { 502 } , { 502 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 502 }, { 502 }, { 502 } , { 502 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { 503 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 503 }, { 503 }, { 503 } , { 503 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_10_and_11 = {  /* compressed */
  0x7,  /* 3 bits */
  34,   /* x4:x2b field */
  {
    { 506 }, { 506 }, { 508 }, { 508 },
    { 507 }, { 507 }, { 508 }, { 508 },
  }
};

IA64_decode_t Table_4_12_0 = { 
  0x1,  /* 1 bits */
  36,   /* za field */
  {
    { 0, &Table_4_13},
    { 0, &Table_4_15},
  }
};

IA64_decode_t Table_4_12_1 = { 
  0x1,  /* 1 bits */
  36,   /* za field */
  {
    { 0, &Table_4_14 },
    { -2 },
  }
};

IA64_decode_t Table_4_13 = { 
  0x3f,  /* 1 bits */
  27,   /* x4:x2b field */
  {
    { 509 }, { 509 }, { 509 }, { 509 },
    { 509 }, { 509 }, { 509 }, { 509 },
    { -2 }, { -2 }, { 509 }, { 509 },
    { -2 }, { -2 }, { 509 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { 509 }, { 509 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};


IA64_decode_t Table_4_14 = { 
  0x3f,  /* 6 bits */
  27,   /* x4:x2b field */
  {
    { 509 }, { 509 }, { 509 }, { 509 },
    { 509 }, { 509 }, { 509 }, { 509 },
    { -2 }, { -2 }, { 509 }, { 509 },
    { -2 }, { -2 }, { 509 }, { -2 },

    { 510 }, { 510 }, { 510 }, { 510 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 510 }, { 510 }, { 510 }, { 510 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { 509 }, { 509 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_15 = { 
  0x3f,  /* 6 bits */
  27,   /* x4:x2b field */
  {
    { 509 }, { -2 }, { -2 }, { -2 },
    { 509 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { 509 }, { 509 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_16 = {
  0x1f,  /* 5 bits */
  32,   /* za:..:zb:ve fields */
  {
    { -2 }, { -2 }, { 0, &Table_4_18_0}, { -2 },
    { -2 }, { -2 }, { 0, &Table_4_18_1}, { -2 },
    { 0, &Table_4_17_2}, { -2 }, { 0, &Table_4_18_2}, { -2 },
    { 0, &Table_4_17_3}, { -2 }, { 0, &Table_4_18_3}, { -2 },

    { 0, &Table_4_19_0}, { -2 }, { 0, &Table_4_20_0}, { -2 },
    { 0, &Table_4_19_1}, { -2 }, { -2 }, { -2 },
    { 0, &Table_4_19_2}, { -2 }, { -2 }, { -2 },
    { 0, &Table_4_19_3}, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_17_2 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { -2 }, { 2 }, { -2 }, { -2 },
    { 2 }, { 2 }, { 2 }, { -2 },
    { 2 }, { -2 }, { 2 }, { 2 },
    { -2 }, { -2 }, { -2 }, { -2 }, 
  }
};

IA64_decode_t Table_4_17_3 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { -2 }, { -2 }, { -2 }, { -2 }, 
    { -2 }, { -2 }, { -2 }, { -2 }, 
    { -2 }, { -2 }, { 3 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 }, 
  }
};

IA64_decode_t Table_4_18_0 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { 5 }, { 1 }, { 5 }, { 1 },
    { 7 }, { 1 }, { -2 }, { 1 },
    { -2 }, { 1 }, { -2 }, { 1 },
    { -2 }, { 1 }, { -2 }, { 1 },
  }
};

IA64_decode_t Table_4_18_1 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { -2 }, { 6 }, { -2 }, { 6 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { 9 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_18_2 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { 2 }, { -2 }, { 2 }, { 2 },
    { 2 }, { -2 }, { 2 }, { 2 },
    { 2 }, { -2 }, { 2 }, { -2 },
    { -2 }, { 2 }, { -2 }, { 2 },
  }
};

IA64_decode_t Table_4_18_3 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { 8 }, { -2 }, { -2 },
    { -2 }, { -2 }, { 4 }, { -2 }, 
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_19_0 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { 5 }, { -2 }, { 5 }, { -2 },
    { 7 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_19_1 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { -2 }, { 6 }, { -2 }, { 6 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_19_2 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { -2 }, { -2 }, { 2 }, { -2 },
    { 2 }, { -2 }, { 2 }, { -2 },
    { 2 }, { -2 }, { 2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_19_3 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { 8 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_20_0 = {
  0xf,  /* 4 bits */
  28,   /* x2c:x2b fields */
  {
    { 5 }, { -2 }, { 5 }, { -2 },
    { 7 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_21_and_22 = { 
  0x7,  /* 3 bits */
  33,   /* x2:x fields */
  { 
    { 0, &Table_4_23},
    { 0, &Table_4_23},
    { 11 },
    { 0, &Table_4_22_extra},
    { -2 },
    { -2 },
    { 10 },
    { 14 },
  }
};

IA64_decode_t Table_4_22_extra = { 
  0x1,  /* 1 bits */
  26,   /* y field */
  { 
    { 12 },
    { 13 },
  }
};

IA64_decode_t Table_4_23 = { 
  0x1,  /* 1 bits */
  13,   /* y field */
  { 
    { 16 },
    { 17 },
  }
};

IA64_decode_t Table_4_24 = { 
  0x7,  /* 3 bits */
  33,   /* x3 field */
  { 
    { 0, &Table_4_25},
    { 20 },
    { 24 },
    { 23 },
    { -2 },
    { -2 },
    { -2 },
    { 21 },
  }
};

IA64_decode_t Table_4_25 = { 
  0x3f,  /* 6 bits */
  27,   /* x6 field */
  { 
    { 19 }, { 18 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { 27 }, { -2,      },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 29 }, { 29 }, { 29 }, { -2 },
    { 29 }, { 29 }, { 29 }, { -2 },
    { 29 }, { 29 }, { -2 }, { -2 },
    { 29 }, { 29 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { 26 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 25 }, { 22 }, { 28 }, { 25 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_27_0 = { 
  0x1,  /* 1 bits */
  27,   /* x field */
  {
    { 0, &Table_4_29 },
    { 0, &Table_4_32 },
  }
};

IA64_decode_t Table_4_27_1 = { 
  0x1,  /* 1 bits */
  27,   /* x field */
  {
    { 0, &Table_4_30 }, 
    { -2 },
  }
};

IA64_decode_t Table_4_28_0 = { 
  0x1,  /* 1 bits */
  27,   /* x field */
  {
    { 0, &Table_4_33 },
    { 0, &Table_4_36 },
  }
};

IA64_decode_t Table_4_28_1 = { 
  0x1,  /* 1 bits */
  27,   /* x field */
  {
    { 0, &Table_4_34 }, 
    { 0, &Table_4_37 }, 
  }
};

IA64_decode_t Table_4_29 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { 101 }, { 101 }, { 101 }, { 101 },
    { 101 }, { 101 }, { 101 }, { 101 },
    { 101 }, { 101 }, { 101 }, { 101 },
    { 101 }, { 101 }, { 101 }, { 101 },

    { 101 }, { 101 }, { 101 }, { 101 },
    { 101 }, { 101 }, { 101 }, { 101 },
    { -2 }, { -2 }, { -2 }, { 101 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 101 }, { 101 }, { 101 }, { 101 },
    { 101 }, { 101 }, { 101 }, { 101 },
    { 101 }, { 101 }, { 101 }, { 101 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 104 }, { 104 }, { 104 }, { 104 },
    { 104 }, { 104 }, { 104 }, { 104 },
    { -2 }, { -2 }, { -2 }, { 104 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_30 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { 102 }, { 102 }, { 102 }, { 102 },
    { 102 }, { 102 }, { 102 }, { 102 },
    { 102 }, { 102 }, { 102 }, { 102 },
    { 102 }, { 102 }, { 102 }, { 102 },

    { 102 }, { 102 }, { 102 }, { 102 },
    { 102 }, { 102 }, { 102 }, { 102 },
    { -2 }, { -2 }, { -2 }, { 102 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 102 }, { 102 }, { 102 }, { 102 },
    { 102 }, { 102 }, { 102 }, { 102 },
    { 102 }, { 102 }, { 102 }, { 102 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_31 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { 103 }, { 103 }, { 103 }, { 103 },
    { 103 }, { 103 }, { 103 }, { 103 },
    { 103 }, { 103 }, { 103 }, { 103 },
    { 103 }, { 103 }, { 103 }, { 103 },

    { 103 }, { 103 }, { 103 }, { 103 },
    { 103 }, { 103 }, { 103 }, { 103 },
    { -2 }, { -2 }, { -2 }, { 103 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 103 }, { 103 }, { 103 }, { 103 },
    { 103 }, { 103 }, { 103 }, { 103 },
    { 103 }, { 103 }, { 103 }, { 103 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 105 }, { 105 }, { 105 }, { 105 },
    { 105 }, { 105 }, { 105 }, { 105 },
    { -2 }, { -2 }, { -2 }, { 105 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_32 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { 116 }, { 116 }, { 116 }, { 116 },
    { 116 }, { 116 }, { 116 }, { 116 },
    { 116 }, { 116 }, { 116 }, { 116 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { 117 }, { 117 },
    { -2 }, { -2 }, { 117 }, { 117 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 119 }, { 119 }, { 119 }, { 119 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_33 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { 106 }, { 106 }, { 106 }, { 106 },
    { 106 }, { 106 }, { 106 }, { 106 },
    { 106 }, { 106 }, { 106 }, { 106 },
    { 106 }, { 106 }, { 106 }, { 106 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { 106 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 106 }, { 106 }, { 106 }, { 106 },
    { 106 }, { 106 }, { 106 }, { 106 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 113 }, { 113 }, { 113 }, { 113 },

    { 109 }, { 109 }, { 109 }, { 109 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { 109 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_34 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { 107 }, { 107 }, { 107 }, { 107 },
    { 107 }, { 107 }, { 107 }, { 107 },
    { 107 }, { 107 }, { 107 }, { 107 },
    { 107 }, { 107 }, { 107 }, { 107 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { 107 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 107 }, { 107 }, { 107 }, { 107 },
    { 107 }, { 107 }, { 107 }, { 107 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 114 }, { 114 }, { 114 }, { 114 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_35 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { 108 }, { 108 }, { 108 }, { 108 },
    { 108 }, { 108 }, { 108 }, { 108 },
    { 108 }, { 108 }, { 108 }, { 108 },
    { 108 }, { 108 }, { 108 }, { 108 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { 108 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 108 }, { 108 }, { 108 }, { 108 },
    { 108 }, { 108 }, { 108 }, { 108 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 115 }, { 115 }, { 115 }, { 115 },

    { 110 }, { 110 }, { 110 }, { 110 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { 110 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_36 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { -2 }, { 111 }, { 111 }, { 111 },
    { -2 }, { 111 }, { 111 }, { 111 },
    { -2 }, { 111 }, { 111 }, { 111 },
    { -2 }, { 111 }, { 111 }, { 111 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 118 }, { 118 }, { 118 }, { 118 },

    { -2 }, { 111 }, { 111 }, { 111 },
    { -2 }, { 111 }, { 111 }, { 111 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_37 = { 
  0x3f,  /* 6 bits */
  30,   /* x6 field */
  { 
    { -2 }, { 112 }, { 112 }, { 112 },
    { -2 }, { 112 }, { 112 }, { 112 },
    { -2 }, { 112 }, { 112 }, { 112 },
    { -2 }, { 112 }, { 112 }, { 112 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { 112 }, { 112 }, { 112 },
    { -2 }, { 112 }, { 112 }, { 112 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_41 = { 
  0x7,  /* 3 bits */
  33,   /* x3 field */
  { 
    { 0, &Table_4_42},
    { -2 },
    { -2 },
    { -2 },
    { 122 },
    { 122 },
    { 123 },
    { 123 },
  }
};

IA64_decode_t Table_4_42 = { 
  0x3f,  /* 6 bits */
  27,   /* x2:x4 field */
  { 
    { 137 }, { 148 }, { -2 }, { -2 },
    { 144 }, { 144 }, { 144 }, { 144 },
    { -2 }, { -2 }, { 125 }, { -2 },
    { 125 }, { -2 }, { -2 }, { -2 },

    { 124 }, { -2 }, { 126 }, { 127 },
    { 144 }, { 144 }, { 144 }, { 144 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 124 }, { -2 }, { 124 }, { 124 },
    { 144 }, { 144 }, { 144 }, { 144 },
    { 130 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 124 }, { 124 }, { -2 }, { 124 },
    { 144 }, { 144 }, { 144 }, { 144 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_43 = { 
  0x7,  /* 3 bits */
  33,   /* x3 field */
  { 
    { 0, &Table_4_44},
    { 120 },
    { -2 },
    { 121 },
    { -2 }, { -2 },
    { 134 },
    { -2 }
  }
};

IA64_decode_t Table_4_44 = { 
  0x3f,  /* 5 bits */
  27,   /* x6 field */
  { 
    { 142 }, { 142 }, { 142 }, { 142 }, 
    { 142 }, { 142 }, { -2},         { -2} ,        
    { -2} ,        { 145 }, { 145 }, { 145 }, 
    { 145 }, { 145 }, { 142 }, { 142 }, 

    { 143 }, { 143 }, { 143 }, { 143 }, 
    { 143 }, { 143 }, { -2 },        { 143 }, 
    { 139 }, { 139 }, { 146 }, { 146 }, 
    { -2 },        { -2 },        { 146 }, { 146 }, 

    { -2 },        { 136 }, { 131 }, { -2 },         
    { 133 }, { 136 }, { -2 },         { -2 },     
    { -2 },        { 135 }, { 129 }, { -2 },        
    { 132 }, { 135 }, { 141 }, { 141 },

    { 128 }, { 140 }, { 140 }, { 140 },
    { 128 }, { -2 }, { -2 }, { -2 },
    { 138 }, { 138 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    
  }
};

IA64_decode_t Table_4_45 = {
  0x7, /* 3 bits */
  6,  /* btype field */
  {
    { 301 }, 
    { -3 },
    { 301 },
    { 301 },
    { -3 },
    { 302 },
    { 302 },
    { 302 },
  }
};

IA64_decode_t Table_4_46 = {
  0x3f, /* 6 bits */
  27,  /* btype field */
  {
    { 309 }, { -4 }, { 308 } , { -3 },
    { 308 }, { 308 }, { -3 }, { -3 },
    { 308 }, { -2 }, { -3 }, { -3 },
    { 308 }, { 308 }, { -3 }, { -3 },

    { 308 }, { -3 }, { -3 }, { -3 },
    { -3 }, { -3 }, { -3 }, { -3 },
    { -3 }, { -3 }, { -3 }, { -3 },
    { -3 }, { -3 }, { -3 }, { -3 },

    { 0, &Table_4_47}, { 0, &Table_4_48}, { -1 }, { -1 },
    { -1 }, { -1 }, { -1 }, { -1 },
    { -1 }, { -1 }, { -1 }, { -1 },
    { -1 }, { -1 }, { -1 }, { -1 },

    { -1 }, { -1 }, { -1 }, { -1 },
    { -1 }, { -1 }, { -1 }, { -1 },
    { -1 }, { -1 }, { -1 }, { -1 },
    { -1 }, { -1 }, { -1 }, { -1 },
  }
};

IA64_decode_t Table_4_47 = {
  0x7, /* 3 bits */
  6,  /* btype field */
  {
    { 304 },
    { 304 },
    { -1 },
    { -1 },
    { -1 },
    { -1 },
    { -1 },
    { -1 },
  }
};

IA64_decode_t Table_4_48 = {
  0x7, /* 3 bits */
  6,  /* btype field */
  {
    { -1 },
    { -1 },
    { -1 },
    { -1 },
    { 304 },
    { -1 },
    { -1 },
    { -1 },
  }
};

IA64_decode_t Table_4_53 = {
  0x3f, /* 6 bits */
  27,  /* btype field */
  {
    { 309 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },

    { 307 }, { 307 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },

    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },

    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
    { -4 }, { -4 }, { -4 }, { -4 },
  }
};

IA64_decode_t Table_4_57_0 = {
  0x1, /* 1 bits */
  33,  /* x field */
  {
    { 0, &Table_4_58},
    { 0, &Table_4_60},
  }
};

IA64_decode_t Table_4_57_1 = {
  0x1, /* 1 bits */
  33,  /* x field */
  {
    { 0, &Table_4_59},
    { 0, &Table_4_60},
  }
};

IA64_decode_t Table_4_58 = {
  0x3f, /* 6 bits */
  27,  /* x6 field */
  {
    { 215 }, { 216 }, { -2 }, { -2 },
    { 212 }, { 213 }, { -2 }, { -2 },
    { 214 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 209 }, { 209 }, { 209 }, { -2 },
    { 208 }, { 208 }, { 208 }, { 208 },
    { 210 }, { 210 }, { 210 }, { 210 },
    { 211 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { 209 }, { -2 }, { -2 }, { -2 },
    { 209 }, { 209 }, { 209 }, { 209 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { 209 }, { 209 }, { 209 }, { -2 },
    { -2 }, { 209 }, { 209 }, { 209 },
    { 209 }, { 209 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_59 = {
  0x3f, /* 6 bits */
  27,  /* x6 field */
  {
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 209 }, { 209 }, { 209 }, { -2 },
    { 208 }, { 208 }, { 208 }, { 208 },
    { 210 }, { 210 }, { 210 }, { 210 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { 208 }, { 208 }, { 208 }, { 208 },
    { 208 }, { 208 }, { 208 }, { 208 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

  }
};

IA64_decode_t Table_4_60 = {
  0x1, /* 1 bits */
  36,  /* q field */
  {
    { 206 },
    { 207 },
  }
};

IA64_decode_t Table_4_63 = {
  0x7, /* 3 bits */
  34,  /* x:x2 field */
  {
    { 203 }, { 203 }, { 203 }, { 203 },
    { 202 }, { -2 }, { 202 }, { 202 },
  }
};

IA64_decode_t Table_4_66 = {
  0x7, /* 3 bits */
  33,  /* x3 field */
  {
    { 0, &Table_4_67 }, /* movl */
    { -2 },
    { -2 },
    { -2 },
    { -2 },
    { -2 },
    { -2 },
    { -2 },
  }
};

IA64_decode_t Table_4_67 = {
  0x3f, /* 6 bits */
  27,  /* x6 field */
  {
    { 401 }, { 405 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },

    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
    { -2 }, { -2 }, { -2 }, { -2 },
  }
};

IA64_decode_t Table_4_68 = {
  0x1, /* 1 bit */
  20,  /* vc field */
  {
    { 402 }, /* movl */
    { -2 },
  }
};


IA64_decode_t *decode_tables[]={&Itable,
				&Mtable,
				&Ftable,
				&Btable,
				&Ltable};

int bundle_types[32][3] = {
  {1,0,0},{1,0,0},{1,0,0},{1,0,0},
  {1,5,4},{1,5,4},{6,7,8},{6,7,8},
  {1,1,0},{1,1,0},{1,1,0},{1,1,0},
  {1,2,0},{1,2,0},{1,1,2},{1,1,2},
  {1,0,3},{1,0,3},{1,3,3},{1,3,3},
  {6,7,8},{6,7,8},{3,3,3},{3,3,3},
  {1,1,3},{1,1,3},{6,7,8},{6,7,8},
  {1,2,3},{1,2,3},{6,7,8},{6,7,8},
};

int bundle_stops[32][3] = {
  {0,0,0},{0,0,1},{0,1,0},{0,1,1},
  {0,0,0},{0,0,1},{0,0,0},{0,0,0},
  {0,0,0},{0,0,1},{1,0,0},{1,0,1},
  {0,0,0},{0,0,1},{0,0,0},{0,0,1},
  {0,0,0},{0,0,1},{0,0,0},{0,0,1},
  {0,0,0},{0,0,0},{0,0,0},{0,0,1},
  {0,0,0},{0,0,1},{0,0,0},{0,0,0},
  {0,0,0},{0,0,1},{0,0,0},{0,0,0},
};

extern void do_nothing(LSE_emu_instr_info_t *ii);
extern void do_nothing1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void not_implemented(LSE_emu_instr_info_t *ii);

extern void decode_res(LSE_emu_instr_info_t *ii);
extern void decode_pred_res(LSE_emu_instr_info_t *ii);
extern void decode_bpred_res(LSE_emu_instr_info_t *ii);
extern void decode_I1(LSE_emu_instr_info_t *ii);
extern void decode_I2(LSE_emu_instr_info_t *ii);
extern void decode_I3(LSE_emu_instr_info_t *ii);
extern void decode_I4(LSE_emu_instr_info_t *ii);
extern void decode_I5(LSE_emu_instr_info_t *ii);
extern void decode_I6(LSE_emu_instr_info_t *ii);
extern void decode_I7(LSE_emu_instr_info_t *ii);
extern void decode_I8(LSE_emu_instr_info_t *ii);
extern void decode_I9(LSE_emu_instr_info_t *ii);
extern void decode_I10(LSE_emu_instr_info_t *ii);
extern void decode_I11(LSE_emu_instr_info_t *ii);
extern void decode_I12(LSE_emu_instr_info_t *ii);
extern void decode_I13(LSE_emu_instr_info_t *ii);
extern void decode_I14(LSE_emu_instr_info_t *ii);
extern void decode_I15(LSE_emu_instr_info_t *ii);
extern void decode_I16(LSE_emu_instr_info_t *ii);
extern void decode_I17(LSE_emu_instr_info_t *ii);
extern void decode_I18(LSE_emu_instr_info_t *ii);
extern void decode_I19(LSE_emu_instr_info_t *ii);
extern void decode_I20(LSE_emu_instr_info_t *ii);
extern void decode_I21(LSE_emu_instr_info_t *ii);
extern void decode_I22(LSE_emu_instr_info_t *ii);
extern void decode_I23(LSE_emu_instr_info_t *ii);
extern void decode_I24(LSE_emu_instr_info_t *ii);
extern void decode_I25(LSE_emu_instr_info_t *ii);
extern void decode_I26(LSE_emu_instr_info_t *ii);
extern void decode_I27(LSE_emu_instr_info_t *ii);
extern void decode_I28(LSE_emu_instr_info_t *ii);
extern void decode_I29(LSE_emu_instr_info_t *ii);
extern void decode_M1(LSE_emu_instr_info_t *ii);
extern void decode_M2(LSE_emu_instr_info_t *ii);
extern void decode_M3(LSE_emu_instr_info_t *ii);
extern void decode_M4(LSE_emu_instr_info_t *ii);
extern void decode_M5(LSE_emu_instr_info_t *ii);
extern void decode_M6(LSE_emu_instr_info_t *ii);
extern void decode_M7(LSE_emu_instr_info_t *ii);
extern void decode_M8(LSE_emu_instr_info_t *ii);
extern void decode_M9(LSE_emu_instr_info_t *ii);
extern void decode_M10(LSE_emu_instr_info_t *ii);
extern void decode_M11(LSE_emu_instr_info_t *ii);
extern void decode_M12(LSE_emu_instr_info_t *ii);
extern void decode_M13(LSE_emu_instr_info_t *ii);
extern void decode_M14(LSE_emu_instr_info_t *ii);
extern void decode_M15(LSE_emu_instr_info_t *ii);
extern void decode_M16(LSE_emu_instr_info_t *ii);
extern void decode_M17(LSE_emu_instr_info_t *ii);
extern void decode_M18(LSE_emu_instr_info_t *ii);
extern void decode_M19(LSE_emu_instr_info_t *ii);
extern void decode_M20(LSE_emu_instr_info_t *ii);
extern void decode_M21(LSE_emu_instr_info_t *ii);
extern void decode_M22(LSE_emu_instr_info_t *ii);
extern void decode_M23(LSE_emu_instr_info_t *ii);
extern void decode_M24(LSE_emu_instr_info_t *ii);
extern void decode_M25(LSE_emu_instr_info_t *ii);
extern void decode_M26(LSE_emu_instr_info_t *ii);
extern void decode_M27(LSE_emu_instr_info_t *ii);
extern void decode_M28(LSE_emu_instr_info_t *ii);
extern void decode_M29(LSE_emu_instr_info_t *ii);
extern void decode_M30(LSE_emu_instr_info_t *ii);
extern void decode_M31(LSE_emu_instr_info_t *ii);
extern void decode_M32(LSE_emu_instr_info_t *ii);
extern void decode_M33(LSE_emu_instr_info_t *ii);
extern void decode_M34(LSE_emu_instr_info_t *ii);
extern void decode_M35(LSE_emu_instr_info_t *ii);
extern void decode_M36(LSE_emu_instr_info_t *ii);
extern void decode_M37(LSE_emu_instr_info_t *ii);
extern void decode_M38(LSE_emu_instr_info_t *ii);
extern void decode_M39(LSE_emu_instr_info_t *ii);
extern void decode_M40(LSE_emu_instr_info_t *ii);
extern void decode_M41(LSE_emu_instr_info_t *ii);
extern void decode_M42(LSE_emu_instr_info_t *ii);
extern void decode_M43(LSE_emu_instr_info_t *ii);
extern void decode_M44(LSE_emu_instr_info_t *ii);
extern void decode_M45(LSE_emu_instr_info_t *ii);
extern void decode_M46(LSE_emu_instr_info_t *ii);
extern void decode_M47(LSE_emu_instr_info_t *ii);
extern void decode_M48(LSE_emu_instr_info_t *ii);
extern void decode_F1(LSE_emu_instr_info_t *ii);
extern void decode_F2(LSE_emu_instr_info_t *ii);
extern void decode_F3(LSE_emu_instr_info_t *ii);
extern void decode_F4(LSE_emu_instr_info_t *ii);
extern void decode_F5(LSE_emu_instr_info_t *ii);
extern void decode_F6(LSE_emu_instr_info_t *ii);
extern void decode_F7(LSE_emu_instr_info_t *ii);
extern void decode_F8(LSE_emu_instr_info_t *ii);
extern void decode_F9(LSE_emu_instr_info_t *ii);
extern void decode_F10(LSE_emu_instr_info_t *ii);
extern void decode_F11(LSE_emu_instr_info_t *ii);
extern void decode_F12(LSE_emu_instr_info_t *ii);
extern void decode_F13(LSE_emu_instr_info_t *ii);
extern void decode_F14(LSE_emu_instr_info_t *ii);
extern void decode_F15(LSE_emu_instr_info_t *ii);
extern void decode_F16(LSE_emu_instr_info_t *ii);
extern void decode_B1(LSE_emu_instr_info_t *ii);
extern void decode_B2(LSE_emu_instr_info_t *ii);
extern void decode_B3(LSE_emu_instr_info_t *ii);
extern void decode_B4(LSE_emu_instr_info_t *ii);
extern void decode_B5(LSE_emu_instr_info_t *ii);
extern void decode_B6(LSE_emu_instr_info_t *ii);
extern void decode_B7(LSE_emu_instr_info_t *ii);
extern void decode_B8(LSE_emu_instr_info_t *ii);
extern void decode_B9(LSE_emu_instr_info_t *ii);
extern void decode_X1(LSE_emu_instr_info_t *ii);
extern void decode_X2(LSE_emu_instr_info_t *ii);
extern void decode_X3(LSE_emu_instr_info_t *ii);
extern void decode_X4(LSE_emu_instr_info_t *ii);
extern void decode_X5(LSE_emu_instr_info_t *ii);
extern void decode_A1(LSE_emu_instr_info_t *ii);
extern void decode_A2(LSE_emu_instr_info_t *ii);
extern void decode_A3(LSE_emu_instr_info_t *ii);
extern void decode_A4(LSE_emu_instr_info_t *ii);
extern void decode_A5(LSE_emu_instr_info_t *ii);
extern void decode_A6(LSE_emu_instr_info_t *ii);
extern void decode_A7(LSE_emu_instr_info_t *ii);
extern void decode_A8(LSE_emu_instr_info_t *ii);
extern void decode_A9(LSE_emu_instr_info_t *ii);
extern void decode_A10(LSE_emu_instr_info_t *ii);

extern void not_implemented_of1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_mov_to_AR(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int); /* I26, I27, M29, M30 */
extern void opfetch_mov_from_AR(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int); /* I28, M31 */
extern void opfetch_M_chk_a(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M_invala_e(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);

extern void opfetch_I1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I2(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I3(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I4(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I5(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I6(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I7(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I8(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I9(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I10(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I11(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I12(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I13(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I14(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I15(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I16(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I17(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I18(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* I19 same as M37 (nop/brk) */
/* I20 same as M20 */
extern void opfetch_I21(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I22(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I23(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I24(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_I25(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* I26 : opfetch_mov_to_AR */
/* I27 : opfetch_mov_to_AR */
/* I28 : opfetch_mov_from_AR */
extern void opfetch_I29(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M2(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M3(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M4(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M5(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M6(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M7(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M8(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M9(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M10(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M11(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M12(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M13(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M14(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M15(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M16(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M17(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M18(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M19(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M20(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M21(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M22 - M23 : opfetch_M_chk_a */
extern void opfetch_M24(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M25(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M26 - M27 : opfetch_M_invala_e */
/* M28 : ni */
/* M29 : opfetch_mov_to_AR */
/* M31 : opfetch_mov_from_AR */
/* M32 - M33 : ni */
extern void opfetch_M34(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M35 - M36 : ni */
extern void opfetch_M37(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_M43(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M38 - M47 : ni */
/* M48 - same as I18 */
extern void opfetch_F1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F2(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F3(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F4(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F5(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F6(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F7(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F8(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F9(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F10(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F11(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F12(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F13(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_F14(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* F15 same as M37 (nop/brk) */
extern void opfetch_B1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_B2(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_B3(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_B4(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_B5(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_B6(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_B7(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_B8(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_B9(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_X1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_X2(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* X3 : same as B1 (since target was done at decode) */
/* X4 : same as B3 (since target was done at decode) */
extern void opfetch_X5(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A2(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A3(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A4(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A5(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A6(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A7(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A8(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A9(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void opfetch_A10(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);

extern void evaluate_reserved(LSE_emu_instr_info_t *ii);
extern void pred_illegal_op(LSE_emu_instr_info_t *ii);
extern void evaluate_mov_to_AR(LSE_emu_instr_info_t *ii); /* I26, I27, M29, M30 */
extern void evaluate_mov_from_AR(LSE_emu_instr_info_t *ii); /* I28, M31 */
extern void evaluate_I1(LSE_emu_instr_info_t *ii);
extern void evaluate_I2(LSE_emu_instr_info_t *ii);
extern void evaluate_I3(LSE_emu_instr_info_t *ii);
extern void evaluate_I4(LSE_emu_instr_info_t *ii);
extern void evaluate_I5(LSE_emu_instr_info_t *ii);
extern void evaluate_I6(LSE_emu_instr_info_t *ii);
extern void evaluate_I7(LSE_emu_instr_info_t *ii);
extern void evaluate_I8(LSE_emu_instr_info_t *ii);
extern void evaluate_I9(LSE_emu_instr_info_t *ii);
extern void evaluate_I10(LSE_emu_instr_info_t *ii);
extern void evaluate_I11(LSE_emu_instr_info_t *ii);
extern void evaluate_I12(LSE_emu_instr_info_t *ii);
extern void evaluate_I13(LSE_emu_instr_info_t *ii);
extern void evaluate_I14(LSE_emu_instr_info_t *ii);
extern void evaluate_I15(LSE_emu_instr_info_t *ii);
extern void evaluate_I16(LSE_emu_instr_info_t *ii);
extern void evaluate_I17(LSE_emu_instr_info_t *ii);
extern void evaluate_I18(LSE_emu_instr_info_t *ii);
/* I19 same as M37 (nop/brk) */
/* I20 same as M20 (nop/brk) */
extern void evaluate_I21(LSE_emu_instr_info_t *ii);
extern void evaluate_I22(LSE_emu_instr_info_t *ii);
extern void evaluate_I23(LSE_emu_instr_info_t *ii);
extern void evaluate_I24(LSE_emu_instr_info_t *ii);
extern void evaluate_I25(LSE_emu_instr_info_t *ii);
/* I26 : evaluate_mov_to_AR */
/* I27 : evaluate_mov_to_AR */
/* I28 : evaluate_mov_from_AR */
extern void evaluate_I29(LSE_emu_instr_info_t *ii);
extern void evaluate_M_int_ld(LSE_emu_instr_info_t *ii); /* M1,M2,M3 */
extern void evaluate_M_int_st(LSE_emu_instr_info_t *ii); /* M4,M5 */
extern void evaluate_M_fp_ld(LSE_emu_instr_info_t *ii); /* M6,M7,M8 */
extern void evaluate_M_fp_st(LSE_emu_instr_info_t *ii); /* M9,M10 */
extern void evaluate_M_fp_ld_pair(LSE_emu_instr_info_t *ii); /* M11,M12 */
extern void evaluate_M_lfetch(LSE_emu_instr_info_t *ii); /* M13,M14,M15 */
extern void evaluate_M16(LSE_emu_instr_info_t *ii);
extern void evaluate_M17(LSE_emu_instr_info_t *ii);
extern void evaluate_M18(LSE_emu_instr_info_t *ii);
extern void evaluate_M19(LSE_emu_instr_info_t *ii);
extern void evaluate_M20(LSE_emu_instr_info_t *ii);
extern void evaluate_M21(LSE_emu_instr_info_t *ii);
extern void evaluate_M_chk_a(LSE_emu_instr_info_t *ii); /* M22, M23 */
extern void evaluate_M24(LSE_emu_instr_info_t *ii);
extern void evaluate_M25(LSE_emu_instr_info_t *ii);
extern void evaluate_M_invala_e(LSE_emu_instr_info_t *ii);
/* M28 : ni */
/* M29 - M30 : evaluate_mov_to_AR */
/* M31 : evaluate_mov_from_AR */
/* M32 - M33 : ni */
extern void evaluate_M34(LSE_emu_instr_info_t *ii);
/* M35 - M36 : ni */
extern void evaluate_M37(LSE_emu_instr_info_t *ii);
extern void evaluate_M43(LSE_emu_instr_info_t *ii);
/* M38 - M46 : ni */
extern void evaluate_F1(LSE_emu_instr_info_t *ii);
extern void evaluate_F2(LSE_emu_instr_info_t *ii);
extern void evaluate_F3(LSE_emu_instr_info_t *ii);
extern void evaluate_F4(LSE_emu_instr_info_t *ii);
extern void evaluate_F5(LSE_emu_instr_info_t *ii);
extern void evaluate_F6(LSE_emu_instr_info_t *ii);
extern void evaluate_F7(LSE_emu_instr_info_t *ii);
extern void evaluate_F8(LSE_emu_instr_info_t *ii);
extern void evaluate_F9(LSE_emu_instr_info_t *ii);
extern void evaluate_F10(LSE_emu_instr_info_t *ii);
extern void evaluate_F11(LSE_emu_instr_info_t *ii);
extern void evaluate_F12(LSE_emu_instr_info_t *ii);
extern void evaluate_F13(LSE_emu_instr_info_t *ii);
extern void evaluate_F14(LSE_emu_instr_info_t *ii);
/* F15 same as M37 brk) */
/* F16 - same as I18 (nop) */
extern void evaluate_br(LSE_emu_instr_info_t *ii); /* B1-B5 */
extern void evaluate_B6(LSE_emu_instr_info_t *ii);
extern void evaluate_B7(LSE_emu_instr_info_t *ii);
extern void evaluate_B8(LSE_emu_instr_info_t *ii);
extern void evaluate_B9(LSE_emu_instr_info_t *ii);
/* X1 : same as M37 (brk) */
extern void evaluate_X2(LSE_emu_instr_info_t *ii);
/* X2 - X3 : evaluate_br */
/* X5 : same as I18 (nop */
extern void evaluate_A1(LSE_emu_instr_info_t *ii);
extern void evaluate_A2(LSE_emu_instr_info_t *ii);
extern void evaluate_A3(LSE_emu_instr_info_t *ii);
extern void evaluate_A4(LSE_emu_instr_info_t *ii);
extern void evaluate_A5(LSE_emu_instr_info_t *ii);
extern void evaluate_A_int_cmp(LSE_emu_instr_info_t *ii); /* A6, A7, A8 */
extern void evaluate_A9(LSE_emu_instr_info_t *ii);
extern void evaluate_A10(LSE_emu_instr_info_t *ii);

extern void memory_M_int_ld(LSE_emu_instr_info_t *ii); /* M1,M2,M3 */
extern void memory_M_fp_ld(LSE_emu_instr_info_t *ii); /* M6,M7,M8 */
extern void memory_M_fp_ld_pair(LSE_emu_instr_info_t *ii); /* M11,M12 */
extern void memory_M_lfetch(LSE_emu_instr_info_t *ii); /* M13,M14,M15 */
extern void memory_M16(LSE_emu_instr_info_t *ii);
extern void memory_M17(LSE_emu_instr_info_t *ii);

extern void format_M_int_ld(LSE_emu_instr_info_t *ii); /* M1,M2,M3 */
extern void format_M_int_st(LSE_emu_instr_info_t *ii); /* M4,M5 */
extern void format_M_fp_ld(LSE_emu_instr_info_t *ii); /* M6,M7,M8 */
extern void format_M_fp_st(LSE_emu_instr_info_t *ii); /* M9,M10 */
extern void format_M_fp_ld_pair(LSE_emu_instr_info_t *ii); /* M11,M12 */
extern void format_M16(LSE_emu_instr_info_t *ii);
extern void format_M17(LSE_emu_instr_info_t *ii);
extern void format_M_chk_a(LSE_emu_instr_info_t *ii); /* M22, M23 */

extern void writeback_r1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_f1_psr(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_f1_psr_fpsr(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_recip(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int); 
extern void writeback_mov_to_AR(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_preds(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_M_chk_a(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_M_invala_e(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_M_lfetch(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);

/* I1 - I15 : writeback_r1 */
/* I16 - I17 : writeback_preds */
/* I18 : non-existent */
/* I19 : do_nothing (nop/break) */
/* I20 : do_nothing (integer spec check) */
extern void writeback_I21(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* I22 : writeback_r1 */
extern void writeback_I23(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_I24(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* I24 - I25 : writeback_r1 */
/* I26 - I27 : writeback_move_to_AR */
/* I28 - I29 : writeback_r1 */
extern void writeback_M_int_ld(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int); 
/* M1 - M3 : writeback_M_int_ld */
extern void writeback_M_int_st(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M4 - M5 : writeback_M_int_st */
extern void writeback_M_fp_ld(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M6 - M8 : writeback_M_fp_ld */
extern void writeback_M_fp_st(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M9 - M10 : writeback_M_fp_st */
extern void writeback_M_fp_ld_pair(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M11 - M12 : writeback_M_fp_ld_pair */
/* M13 : do_nothing */
/* M14 - M15 : writeback_M_lfetch */
extern void writeback_M16(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_M17(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M18 : writeback_f1_psr */
/* M19 : writeback_r1 */
/* M20 - M21 : do_nothing (speculation checks) */
/* M22 - M23 : writeback_M_chk_a */
extern void writeback_M24(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_M25(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M26 - M27 : writeback_M_invala_e */
/* M28 : ni */
/* M29 - M30 : writeback_mov_to_AR */
/* M31 : writeback_r1 */
/* M32 - M33 : ni */
extern void writeback_M34(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_M43(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* M35 - M36 : ni */
/* M37 : do_nothing (nop/break) */
/* M38 - M46 : ni */
/* F1 : writeback_f1_psr_fpsr */
/* F2 - F3 : writeback_f1_psr */
/* F4 - F5 : writeback_preds */
/* F6 - F7 : writeback_recip */
/* F8 : writeback_f1_psr_fpsr */
/* F9 : writeback_f1_psr */
/* F10 : writeback_f1_psr_fpsr */
/* F11 : writeback_f1_psr */
extern void writeback_F12(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_F13(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* F14 : do_nothing (fchkf) */
/* F15 : do_nothing (nop/break) */
extern void writeback_B1(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_B2(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_B3(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_B4(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
extern void writeback_B5(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* B6 - B7 : do_nothing (brp) */
extern void writeback_B8(LSE_emu_instr_info_t *,LSE_emu_operand_name_t,int);
/* B9 : do_nothing (nop/break) */
/* X1 : do_nothing (nop/break) */
/* X2 : writeback_r1 */
/* X3 : same as B1 (since target was done at decode) */
/* X4 : same as B3 (since target was done at decode) */
/* A1 - A5 : writeback_r1 */
/* A6 - A8 : writeback_preds */
/* A9 - A10 : writeback_r1 */

/* only the most popular formats have an allbackend, and then only if
 * it doesn't create a hassle.  For example, loads and stores are a hassle!
 */
extern void allbackend_A4(LSE_emu_instr_info_t *);
extern void allbackend_A6(LSE_emu_instr_info_t *);
extern void allbackend_A7(LSE_emu_instr_info_t *);
extern void allbackend_A8(LSE_emu_instr_info_t *);

IA64_decode_details_t decode_details[IA64_NUM_FORMATS] = {
  { /* reserved (fault is reported at decode) */
    { decode_res, do_nothing1, do_nothing, do_nothing, 
      do_nothing, do_nothing1 },
  },
  { /* reserved if pr=1 (nothing fetched, written back, etc.) */
    { decode_pred_res, do_nothing1, pred_illegal_op, do_nothing, 
      do_nothing, do_nothing1 },
  },
  { /* reserved b-type if pr=1 (nothing fetched, written back, etc.) */
    { decode_bpred_res, do_nothing1, pred_illegal_op, do_nothing, 
      do_nothing, do_nothing1 },
  },


  { /* I1 */
    { decode_I1, opfetch_I1, evaluate_I1, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I2 */
    { decode_I2, opfetch_I2, evaluate_I2, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I3 */
    { decode_I3, opfetch_I3, evaluate_I3, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I4 */
    { decode_I4, opfetch_I4, evaluate_I4, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* I5 */
    { decode_I5, opfetch_I5, evaluate_I5, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I6 */
    { decode_I6, opfetch_I6, evaluate_I6, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I7 */
    { decode_I7, opfetch_I7, evaluate_I7, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I8 */
    { decode_I8, opfetch_I8, evaluate_I8, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I9 */
    { decode_I9, opfetch_I9, evaluate_I9, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I10 */
    { decode_I10, opfetch_I10, evaluate_I10, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I11 */
    { decode_I11, opfetch_I11, evaluate_I11, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I12 */
    { decode_I12, opfetch_I12, evaluate_I12, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I13 */
    { decode_I13, opfetch_I13, evaluate_I13, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I14 */
    { decode_I14, opfetch_I14, evaluate_I14, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I15 */
    { decode_I15, opfetch_I15, evaluate_I15, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I16 */
    { decode_I16, opfetch_I16, evaluate_I16, do_nothing, 
      do_nothing, writeback_preds },
  },
  { /* I17 */
    { decode_I17, opfetch_I17, evaluate_I17, do_nothing, 
      do_nothing, writeback_preds },
  },
  { /* I18 */
    { decode_I18, opfetch_I18, do_nothing, do_nothing, 
      do_nothing, do_nothing1, do_nothing },
  },
  { /* I19 */
    { decode_I19, opfetch_M37, evaluate_M37, do_nothing, 
      do_nothing, do_nothing1},
  },
  { /* I20 (same as M20) */
    { decode_I20, opfetch_M20, evaluate_M20, do_nothing, 
      do_nothing, do_nothing1 },
  },
  { /* I21 */
    { decode_I21, opfetch_I21, evaluate_I21, do_nothing, 
      do_nothing, writeback_I21 },
  },
  { /* I22 */
    { decode_I22, opfetch_I22, evaluate_I22, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I23 */
    { decode_I23, opfetch_I23, evaluate_I23, do_nothing, 
      do_nothing, writeback_I23 },
  },
  { /* I24 */
    { decode_I24, opfetch_I24, evaluate_I24, do_nothing, 
      do_nothing, writeback_I24 },
  },
  { /* I25 */
    { decode_I25, opfetch_I25, evaluate_I25, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I26 */
    { decode_I26, opfetch_mov_to_AR, evaluate_mov_to_AR, do_nothing, 
      do_nothing, writeback_mov_to_AR },
  },
  { /* I27 */
    { decode_I27, opfetch_mov_to_AR, evaluate_mov_to_AR, do_nothing, 
      do_nothing, writeback_mov_to_AR },
  },
  { /* I28 */
    { decode_I28, opfetch_mov_from_AR, evaluate_mov_from_AR, do_nothing, 
      do_nothing, writeback_r1 },
  },
  { /* I29 */
    { decode_I29, opfetch_I29, evaluate_I29, do_nothing, 
      do_nothing, writeback_r1 },
  },

  { /* M1 */
    { decode_M1, opfetch_M1, evaluate_M_int_ld, memory_M_int_ld,
     format_M_int_ld, writeback_M_int_ld },
  },
  { /* M2 */
    { decode_M2, opfetch_M2, evaluate_M_int_ld, memory_M_int_ld,
      format_M_int_ld, writeback_M_int_ld },
  },
  { /* M3 */
    { decode_M3, opfetch_M3, evaluate_M_int_ld, memory_M_int_ld,
      format_M_int_ld, writeback_M_int_ld },
  },
  { /* M4 */
    { decode_M4, opfetch_M4, evaluate_M_int_st, do_nothing,
      format_M_int_st, writeback_M_int_st },
  },
  { /* M5 */
    { decode_M5, opfetch_M5, evaluate_M_int_st, do_nothing,
      format_M_int_st, writeback_M_int_st },
  },
  { /* M6 */
    { decode_M6, opfetch_M6, evaluate_M_fp_ld, memory_M_fp_ld,
      format_M_fp_ld, writeback_M_fp_ld },
  },
  { /* M7 */
    { decode_M7, opfetch_M7, evaluate_M_fp_ld, memory_M_fp_ld,
      format_M_fp_ld, writeback_M_fp_ld },
  },
  { /* M8 */
    { decode_M8, opfetch_M8, evaluate_M_fp_ld, memory_M_fp_ld,
      format_M_fp_ld, writeback_M_fp_ld },
  },
  { /* M9 */
    { decode_M9, opfetch_M9, evaluate_M_fp_st, do_nothing,
      format_M_fp_st, writeback_M_fp_st },
  },
  { /* M10 */
    { decode_M10, opfetch_M10, evaluate_M_fp_st, do_nothing,
      format_M_fp_st, writeback_M_fp_st },
  },
  { /* M11 */
    { decode_M11, opfetch_M11, evaluate_M_fp_ld_pair, memory_M_fp_ld_pair,
      format_M_fp_ld_pair, writeback_M_fp_ld_pair },
  },
  { /* M12 */
    { decode_M12, opfetch_M12, evaluate_M_fp_ld_pair, memory_M_fp_ld_pair,
      format_M_fp_ld_pair, writeback_M_fp_ld_pair },
  },
  { /* M13 */
    { decode_M13, opfetch_M13, evaluate_M_lfetch, memory_M_lfetch,
      do_nothing, do_nothing1 },
  },
  { /* M14 */
    { decode_M14, opfetch_M14, evaluate_M_lfetch, memory_M_lfetch,
      do_nothing, writeback_M_lfetch },
  },
  { /* M15 */
    { decode_M15, opfetch_M15, evaluate_M_lfetch, memory_M_lfetch,
      do_nothing, writeback_M_lfetch },
  },
  { /* M16 */
    { decode_M16, opfetch_M16, evaluate_M16, memory_M16,
      format_M16, writeback_M16 },
  },
  { /* M17 */
    { decode_M17, opfetch_M17, evaluate_M17, memory_M17,
      format_M17, writeback_M17 },
  },
  { /* M18 */
    { decode_M18, opfetch_M18, evaluate_M18, do_nothing,
      do_nothing, writeback_f1_psr },
  },
  { /* M19 */
    { decode_M19, opfetch_M19, evaluate_M19, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* M20 */
    { decode_M20, opfetch_M20, evaluate_M20, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M21 */
    { decode_M21, opfetch_M21, evaluate_M21, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M22 */
    { decode_M22, opfetch_M_chk_a, evaluate_M_chk_a, do_nothing,
      format_M_chk_a, writeback_M_chk_a },
  },
  { /* M23 */
    { decode_M23, opfetch_M_chk_a, evaluate_M_chk_a, do_nothing,
      format_M_chk_a, writeback_M_chk_a },
  },
  { /* M24 */
    { decode_M24, opfetch_M24, evaluate_M24, do_nothing,
      do_nothing, writeback_M24 },
  },
  { /* M25 */
    { decode_M25, opfetch_M25, evaluate_M25, do_nothing,
      do_nothing, writeback_M25 },
  },
  { /* M26 */
    { decode_M26, opfetch_M_invala_e, evaluate_M_invala_e, do_nothing,
      do_nothing, writeback_M_invala_e },
  },
  { /* M27 */
    { decode_M27, opfetch_M_invala_e, evaluate_M_invala_e, do_nothing,
      do_nothing, writeback_M_invala_e },
  },
  { /* M28 */
    { decode_M28, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M29 */
    { decode_M29, opfetch_mov_to_AR, evaluate_mov_to_AR, do_nothing,
      do_nothing, writeback_mov_to_AR },
  },
  { /* M30 */
    { decode_M30, opfetch_mov_to_AR, evaluate_mov_to_AR, do_nothing,
      do_nothing, writeback_mov_to_AR },
  },
  { /* M31 */
    { decode_M31, opfetch_mov_from_AR, evaluate_mov_from_AR, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* M32 */
    { decode_M32, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M33 */
    { decode_M33, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M34 */
    { decode_M34, opfetch_M34, evaluate_M34, do_nothing,
      do_nothing, writeback_M34 },
  },
  { /* M35 */
    { decode_M35, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M36 */
    { decode_M36, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M37 */
    { decode_M37, opfetch_M37, evaluate_M37, do_nothing,
      do_nothing, do_nothing1, },
  },
  { /* M38 */
    { decode_M38, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M39 */
    { decode_M39, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M40 */
    { decode_M40, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M41 */
    { decode_M41, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M42 */
    { decode_M42, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M43 */
    { decode_M43, opfetch_M43, evaluate_M43, do_nothing,
      do_nothing, writeback_M43 },
  },
  { /* M44 */
    { decode_M44, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M45 */
    { decode_M45, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M46 */
    { decode_M46, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M47 */
    { decode_M47, do_nothing1, not_implemented, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* M48 */
    { decode_M48, opfetch_I18, do_nothing, do_nothing, 
      do_nothing, do_nothing1, do_nothing },
  },

  { /* F1 */
    { decode_F1, opfetch_F1, evaluate_F1, do_nothing,
      do_nothing, writeback_f1_psr_fpsr },
  },
  { /* F2 */
    { decode_F2, opfetch_F2, evaluate_F2, do_nothing,
      do_nothing, writeback_f1_psr },
  },
  { /* F3 */
    { decode_F3, opfetch_F3, evaluate_F3, do_nothing,
      do_nothing, writeback_f1_psr },
  },
  { /* F4 */
    { decode_F4, opfetch_F4, evaluate_F4, do_nothing,
      do_nothing, writeback_preds },
  },
  { /* F5 */
    { decode_F5, opfetch_F5, evaluate_F5, do_nothing,
      do_nothing, writeback_preds },
  },
  { /* F6 */
    { decode_F6, opfetch_F6, evaluate_F6, do_nothing,
      do_nothing, writeback_recip },
  },
  { /* F7 */
    { decode_F7, opfetch_F7, evaluate_F7, do_nothing,
      do_nothing, writeback_recip },
  },
  { /* F8 */
    { decode_F8, opfetch_F8, evaluate_F8, do_nothing,
      do_nothing, writeback_f1_psr_fpsr },
  },
  { /* F9 */
    { decode_F9, opfetch_F9, evaluate_F9, do_nothing,
      do_nothing, writeback_f1_psr },
  },
  { /* F10 */
    { decode_F10, opfetch_F10, evaluate_F10, do_nothing,
      do_nothing, writeback_f1_psr_fpsr },
  },
  { /* F11 */
    { decode_F11, opfetch_F11, evaluate_F11, do_nothing,
      do_nothing, writeback_f1_psr },
  },
  { /* F12 */
    { decode_F12, opfetch_F12, evaluate_F12, do_nothing,
      do_nothing, writeback_F12 },
  },
  { /* F13 */
    { decode_F13, opfetch_F13, evaluate_F13, do_nothing,
      do_nothing, writeback_F13 },
  },
  { /* F14 */
    { decode_F14, opfetch_F14, evaluate_F14, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* F15 */
    { decode_F15, opfetch_M37, evaluate_M37, do_nothing,
      do_nothing, do_nothing1, },
  },
  { /* F16 */
    { decode_F16, opfetch_I18, do_nothing, do_nothing, 
      do_nothing, do_nothing1, do_nothing },
  },

  { /* B1 */
    { decode_B1, opfetch_B1, evaluate_br, do_nothing,
      do_nothing, writeback_B1 },
  },
  { /* B2 */
    { decode_B2, opfetch_B2, evaluate_br, do_nothing,
      do_nothing, writeback_B2 },
  },
  { /* B3 */
    { decode_B3, opfetch_B3, evaluate_br, do_nothing,
      do_nothing, writeback_B3 },
  },
  { /* B4 */
    { decode_B4, opfetch_B4, evaluate_br, do_nothing,
      do_nothing, writeback_B4 },
  },
  { /* B5 */
    { decode_B5, opfetch_B5, evaluate_br, do_nothing,
      do_nothing, writeback_B5 },
  },
  { /* B6 */
    { decode_B6, opfetch_B6, evaluate_B6, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* B7 */
    { decode_B7, opfetch_B7, evaluate_B7, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* B8 */
    { decode_B8, opfetch_B8, evaluate_B8, do_nothing,
      do_nothing, writeback_B8 },
  },
  { /* B9 */
    { decode_B9, opfetch_B9, evaluate_B9, do_nothing,
      do_nothing, do_nothing1 },
  },

  { /* X1 */
    { decode_X1, opfetch_X1, evaluate_M37, do_nothing,
      do_nothing, do_nothing1 },
  },
  { /* X2 */
    { decode_X2, opfetch_X2, evaluate_X2, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* X3 */
    { decode_X3, opfetch_B1, evaluate_br, do_nothing,
      do_nothing, writeback_B1 },
  },
  { /* X4 */
    { decode_X4, opfetch_B3, evaluate_br, do_nothing,
      do_nothing, writeback_B3 },
  },
  { /* X5 */
    { decode_X5, opfetch_X5, do_nothing, do_nothing, 
      do_nothing, do_nothing1, do_nothing },
  },

  { /* A1 */
    { decode_A1, opfetch_A1, evaluate_A1, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* A2 */
    { decode_A2, opfetch_A2, evaluate_A2, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* A3 */
    { decode_A3, opfetch_A3, evaluate_A3, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* A4 */
    { decode_A4, opfetch_A4, evaluate_A4, do_nothing,
      do_nothing, writeback_r1, allbackend_A4 },
  },
  { /* A5 */
    { decode_A5, opfetch_A5, evaluate_A5, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* A6 */
    { decode_A6, opfetch_A6, evaluate_A_int_cmp, do_nothing,
      do_nothing, writeback_preds, allbackend_A6 },
  },
  { /* A7 */
    { decode_A7, opfetch_A7, evaluate_A_int_cmp, do_nothing,
      do_nothing, writeback_preds, allbackend_A7 },
  },
  { /* A8 */
    { decode_A8, opfetch_A8, evaluate_A_int_cmp, do_nothing,
      do_nothing, writeback_preds, allbackend_A8 },
  },
  { /* A9 */
    { decode_A9, opfetch_A9, evaluate_A9, do_nothing,
      do_nothing, writeback_r1 },
  },
  { /* A10 */
    { decode_A10, opfetch_A10, evaluate_A10, do_nothing,
      do_nothing, writeback_r1 },
  },
};


int IA64_ar_reserved[] = {
  0,0,0,0,0,0,0,0,
  1,1,1,1,1,1,1,1,
  0,0,0,0,1,0,1,1,
  0,0,0,0,0,0,0,1,
  0,1,1,1,0,1,1,1,
  0,1,1,1,0,1,1,1,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,

  0,0,0,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
};

int IA64_ar_ignored[] = {
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,

  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
};

int IA64_cr_reserved[] = {
  0,0,0,1,1,1,1,1,
  0,1,1,1,1,1,1,1,
  0,0,1,0,0,0,0,0,
  0,0,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,

  0,0,0,0,0,0,0,0,
  0,0,0,1,1,1,1,1,
  0,0,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,
};

} // namespace LSE_IA64
