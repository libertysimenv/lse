/* 
 * Copyright (c) 2000-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Linux OS template file for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *          David August <august@cs.princeton.edu>
 * 	    Ram Rangan <ram@cs.princeton.edu>
 *
 * TODO: 
 *      - extra information fields on faults
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LSE_IA64.h"

#define LSE_LINUX_CHECKPOINT
#define LSE_LINUX_HEADERS
#include "OS/LSE_Linux.h"

namespace LSE_IA64 {

extern LSE_chkpt::error_t EMU_write_context(LSE_chkpt::file_t *cptFile,
					    int nmark, IA64_context_t *ctx,
					    LSE_emu_chkpt_cntl_t *ctl);

extern LSE_chkpt::error_t EMU_parse_context(LSE_emu_interface_t *intr,
       			  		    LSE_chkpt::file_t *cptFile,
                                            IA64_context_t *&realct,
					    LSE_emu_chkpt_cntl_t *ctl);

void EMU_allocate_memory(IA64_context_t *realct);

#define IA64_PAGE_SIZE UINT64_C(0x4000)
#define PAGE_BITS 14

#define IA64_USTACK_TOP \
    (UINT64_C(0x8000000000000000) + (UINT64_C(1)<< (4*14-12)) - IA64_PAGE_SIZE)
#define IA64_USTACK_BASE \
    (IA64_USTACK_TOP - UINT64_C(0x80000000) + IA64_PAGE_SIZE)

/* helper functions for checkpointing from/to target memory */

extern int EMU_context_copy(LSE_emu_interface_t *,
       	   	 	    IA64_context_t **, IA64_context_t *);

extern void EMU_context_destroy(IA64_context_t *&realct);

#define LSE_LINUX_BASE
#define CC_isacontext_t     IA64_context_t
const bool Linux_bigendian = false;
#include "OS/LSE_Linux.h"

struct OSBDefs : public OSBaseDefs {
  // termbits
  static const int oNCCS = 32;

  // termios
  static const int oNCC = 32;

  // ========== Other stuff ==============

  static const int oNUM_DLINFO_ENTRIES = 2;
  static const int oHWCAP              = 0;
  static const int oCLKTCK             = 1024;

  static const unsigned oPAGE_SIZE   = IA64_PAGE_SIZE;
  static const unsigned oPAGE_BITS   = PAGE_BITS;
  static const int oCLONE_STACK_SIZE = 16;
  static const uint64_t oRLIMIT_INFINITY = ~uint64_t(0);

  static LSE_emu_addr_t oUTASK_SIZE(bool)    { return  IA64_USTACK_TOP; }
  static LSE_emu_addr_t oUNMAPPED_BASE(bool) { return uint64_t(0x2000) << 48; }
  static LSE_emu_addr_t oUSTACK_BASE(bool)   { return IA64_USTACK_BASE; }
  static LSE_emu_addr_t oUSTACK_TOP(bool)    { return IA64_USTACK_TOP; }
  static const LSE_emu_addr_t oGATE_AREA = INT64_C(0xa000000000000000);

  static const char *oDYNLOADER;
  static const char *oSYSNAME;
  static const char *oNODENAME;
  static const char *oRELEASE;
  static const char *oVERSION;
  static const char *oMACHINE;
  static const char *oDOMAINNAME;

  typedef  uint8_t L_cc_t;
  typedef  int64_t L_clock_t;
  typedef  int64_t L_long;
  typedef uint64_t L_ptr_t;
  typedef uint64_t L_size_t;
  typedef uint32_t L_speed_t;
  typedef  int64_t L_statfs_word;
  typedef  int64_t L_suseconds_t;
  typedef uint32_t L_tcflag_t;
  typedef  int64_t L_time_t;
  typedef uint64_t L_ulong;
  typedef uint64_t L_uptr_t;

};

const char *OSBDefs::oDYNLOADER  = "ld.so";
const char *OSBDefs::oSYSNAME    = "Linux";
const char *OSBDefs::oNODENAME   = "LibertyIA64";
const char *OSBDefs::oRELEASE    = "2.6.28.8";
const char *OSBDefs::oVERSION    = "Emulated #1 Fri Jun 8 13:06:07 EDT 2001";
const char *OSBDefs::oMACHINE    = "ia64";
const char *OSBDefs::oDOMAINNAME = "unknown";

  const int OSBDefs::oNCCS;
  const int OSBDefs::oNCC;
  const int OSBDefs::oNUM_DLINFO_ENTRIES;
  const int OSBDefs::oHWCAP;
  const int OSBDefs::oCLKTCK;
  const unsigned OSBDefs::oPAGE_SIZE;
  const unsigned OSBDefs::oPAGE_BITS;
  const uint64_t OSBDefs::oRLIMIT_INFINITY;
  const int OSBDefs::oCLONE_STACK_SIZE;
  const LSE_emu_addr_t OSBDefs::oGATE_AREA;

struct OSDefs : public OSDerivedDefs<OSBDefs> {

  struct L___sysctl_args {
    LSE_emu_addr_t LSE_name;
    uint32_t       LSE_nlen;
    uint32_t       LSE___pad1;
    LSE_emu_addr_t LSE_oldval;
    LSE_emu_addr_t LSE_oldlenp;
    LSE_emu_addr_t LSE_newval;
    L_size_t       LSE_newlen;
  };

  struct L_newstat {
    uint64_t             LSE_st_dev; /* 0 */
    uint64_t             LSE_st_ino; /* 8 */
    uint64_t             LSE_st_nlink; /* 16 */
    uint32_t             LSE_st_mode; /* 24 */
    uint32_t             LSE_st_uid;  /* 28 */
    uint32_t             LSE_st_gid;  /* 32 */
    uint32_t             LSE_pad0; /* 36 */
    uint64_t             LSE_st_rdev;  /* 40 */
    uint64_t             LSE_st_size;  /* 48 */
    uint64_t             LSE_st_atime; /* 56 */
    uint64_t             LSE_reserved0; /* 64 */ 
    uint64_t             LSE_st_mtime;         /* 72 */
    uint64_t             LSE_reserved1; /* 80 */
    uint64_t             LSE_st_ctime;         /* 88 */
    uint64_t             LSE_reserved2; /* 96 */
    uint64_t             LSE_st_blksize;       /* 104 */
    int64_t              LSE_st_blocks;        /* 112 */
    uint64_t             LSE_unused[3];  /* 120 */
    L_newstat & operator =(const struct stat &buf) {
      LSE_st_dev     = LSE_h2l((uint64_t)buf.st_dev);
      LSE_st_ino     = LSE_h2l((uint64_t)buf.st_ino);
      LSE_st_nlink   = 0;
      LSE_st_mode    = LSE_h2l((uint32_t)buf.st_mode);
      LSE_st_uid     = LSE_h2l((uint32_t)buf.st_uid);
      LSE_st_gid     = LSE_h2l((uint32_t)buf.st_gid);
      LSE_pad0       = 0;
      LSE_st_rdev    = LSE_h2l((uint64_t)buf.st_rdev);
      LSE_st_size    = LSE_h2l((uint64_t)buf.st_size);
      LSE_st_atime   = LSE_h2l((uint64_t)buf.st_atime);
      LSE_reserved0  = 0;
      LSE_st_mtime   = LSE_h2l((uint64_t)buf.st_mtime);
      LSE_reserved1  = 0;
      LSE_st_ctime   = LSE_h2l((uint64_t)buf.st_ctime);
      LSE_reserved2  = 0;
      LSE_st_blksize = LSE_h2l((uint64_t)buf.st_blksize);
      LSE_st_blocks  = LSE_h2l((int64_t)buf.st_blocks);
      LSE_unused[0]  = LSE_unused[1] = LSE_unused[2] = LSE_unused[3] = 0;
    }
  };

  struct L_stat64 {
    uint64_t LSE_st_dev;
    uint32_t LSE_pad0;
    uint32_t LSE_st_ino;
    uint32_t LSE_st_mode;
    uint32_t LSE_st_nlink;
    uint32_t LSE_st_uid;
    uint32_t LSE_st_gid;
    uint64_t LSE_st_rdev;	
    uint32_t LSE_pad1;
    int64_t  LSE_st_size;
    int32_t  LSE_st_blksize;
    int32_t  LSE_st_blocks;
    int32_t  LSE_pad2;
    uint32_t LSE_st_atime;
    uint32_t LSE_st_atimensec;
    uint32_t LSE_st_mtime;
    uint32_t LSE_st_mtimensec;
    uint32_t LSE_st_ctime;
    uint32_t LSE_st_ctimensec;
    uint32_t LSE_st_ino_lo;
    uint32_t LSE_st_ino_hi;
    L_stat64& operator =(const LSE_stat64 &buf) {
      LSE_st_dev     = LSE_h2l((uint64_t)buf.st_dev);
      LSE_pad0 = 0;
      LSE_st_ino     = LSE_h2l((uint32_t)buf.st_ino);
      LSE_st_mode    = LSE_h2l((uint32_t)buf.st_mode);
      LSE_st_nlink   = LSE_h2l((uint32_t)buf.st_nlink);
      LSE_st_uid     = LSE_h2l((uint32_t)buf.st_uid);
      LSE_st_gid     = LSE_h2l((uint32_t)buf.st_gid);
      LSE_st_rdev    = LSE_h2l((uint64_t)buf.st_rdev);
      LSE_pad1 = 0;
      LSE_st_size    = LSE_h2l((uint64_t)buf.st_size);
      LSE_st_blksize = LSE_h2l((int32_t)buf.st_blksize);
      LSE_st_blocks  = LSE_h2l((int64_t)buf.st_blocks);
      LSE_pad2 = 0;
      LSE_st_atime   = LSE_h2l((uint32_t)buf.st_atime);
      LSE_st_atimensec = 0;
      LSE_st_mtime   = LSE_h2l((uint32_t)buf.st_mtime);
      LSE_st_mtimensec = 0;
      LSE_st_ctime   = LSE_h2l((uint32_t)buf.st_ctime);
      LSE_st_ctimensec = 0;
      LSE_st_ino_lo = LSE_st_ino_hi = 0;
    }
  };

  struct L_termios {
    L_tcflag_t LSE_c_iflag;  /* 0-3 */
    L_tcflag_t LSE_c_oflag;  /* 4-7 */
    L_tcflag_t LSE_c_cflag;  /* 8-11 */
    L_tcflag_t LSE_c_lflag;  /* 12-15 */
    L_cc_t     LSE_c_line;       /* 16 */
    L_cc_t     LSE_c_cc[oNCCS]; /* 17-48 */
    uint8_t       LSE_pad0[3]; /* so it becomes aligned anywhere */ /* 49-51 */
    L_speed_t  LSE_c_ispeed;  /* 52-55 */
    L_speed_t  LSE_c_ospeed;  /* 56-59 */
#ifdef TCGETS
    L_termios& operator=(const struct termios &buf) {
      LSE_c_iflag = LSE_h2l((L_tcflag_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2l((L_tcflag_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2l((L_tcflag_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2l((L_tcflag_t)buf.c_lflag);
      if (oNCCS <= NCCS) {
	for (int i=0;i<oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<oNCCS;i++) LSE_c_cc[i] = 0;
      }
      for (int i = 0; i < 4; ++i) LSE_pad0[i] = 0;
#ifdef __linux
      LSE_c_line = buf.c_line;
      LSE_c_ispeed = LSE_h2l((L_speed_t)buf.c_ispeed);
      LSE_c_ospeed = LSE_h2l((L_speed_t)buf.c_ospeed);
#else
      LSE_c_line = 0; // N_TTY
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2l(x);
      LSE_c_ospeed = LSE_h2l(x);
#endif
    }
#elif defined(TCGETA)
    L_termios& operator=(const struct termio &buf) {
      LSE_c_iflag = LSE_h2l((L_tcflag_t)buf.c_iflag);
      LSE_c_oflag = LSE_h2l((L_tcflag_t)buf.c_oflag);
      LSE_c_cflag = LSE_h2l((L_tcflag_t)buf.c_cflag);
      LSE_c_lflag = LSE_h2l((L_tcflag_t)buf.c_lflag);
      if (oNCCS <= NCCS) {
	for (int i=0;i<oNCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
      } else {
	int i;
	for (i=0;i<NCCS;i++) LSE_c_cc[i] = buf.c_cc[i];
	for (;i<oNCCS;i++) LSE_c_cc[i] = 0;
      }
      for (int i = 0; i < 4; ++i) LSE_pad0[i] = 0;
#ifdef __linux
      LSE_c_line = buf.c_line;
#else
      LSE_c_line = 0; // N_TTY
#endif
      L_speed_t x = 9600;
      LSE_c_ispeed = LSE_h2b(x);
      LSE_c_ospeed = LSE_h2b(x);
    }
#endif
  };

  struct L_siginfo {
    int si_signo;
    int si_errno;
    int si_code;
    uint32_t __pad;
    union {
      int pad[28];
      struct {
	int pid;
	int uid;
	L_sigval_t sigval;
      } normal;
      struct {
	int pid;
	int uid;
	int status;
	L_clock_t utime;
	L_clock_t stime;
      } child;
      struct {
	int tid;
	int overrun;
	L_sigval_t sigval;
	int _private;
      } timer;
      struct {
	uint64_t band;
	int fd;
      } poll;
      struct {
	LSE_emu_addr_t addr;
	int imm;
      } fault;
    } d;
    
    inline struct Linux_siginfo_t tosiginfo();
    inline void fromsiginfo(struct Linux_siginfo_t &v);

    L_siginfo(struct Linux_siginfo_t &v) { fromsiginfo(v); }
    L_siginfo() {}
  };

};


struct IA64_context_saver { 
  uint32_t nat;
  LSE_emu_addr_t ip;
  LSE_emu_addr_t bsp, bspstore;
  IA64_SR_t cfm;
  uint64_t rnat, ccv, unat, fpsr, pfs, lc, pr, ar25, ar26, rsc, um;
  uint64_t gr[32], br[8];
  IA64_FR_t fr[128];
  uint64_t sigmask;
};

  // NOTE: I cannot move this up right now because sigreturn_body has a
  // hook which uses it

#define LSE_LINUX_CODE
#define LSE_LINUX_USE_DEVMEM
#define CC_mem_redirect(x,y)  ((x)->mem = (y))
#define CC_ict_osinfo(x)      (x)->osinfo
#define CC_ict_hwcno(x)       (x)->hwcno
#define CC_ict_startaddr(x)   (x)->startaddr
#define CC_ict_mem(x)         (x)->mem
#define CC_elftype            64
#define CC_emu_interface(x)   (*((x)->di->einterface))
#define OS_emuvar(x,y)        (((IA64_dinst_t *)((x)->etoken))->y)
#define OS_ict_emuvar(x,y)    ((x)->di->y)
#define OS_interface(x)       ((Linux_dinst_t *)OS_emuvar(x,ostoken))
#define OS_interface_set(x,y) OS_emuvar(x,ostoken) = (void *)(y)
#define CC_addrsize           8
#define CC_get_stackptr(x)    ((x)->r[12].val)

static inline void CC_sys_set_return(CC_isacontext_t *realct, int which,
				     uint64_t val) {
  if (which == 0) {
    realct->r[10].val = val;
    realct->r[10].nat = 0;
  } else {
    realct->r[8].val = val;
    realct->r[8].nat = 0;
  }
}

static inline uint64_t CC_sys_get_return(CC_isacontext_t *realct, int which) {
  if (which == 0) return realct->r[10].val;
  else return realct->r[8].val;
}

#define CC_dlinfo()  						 \
  OS_auxv_entry(32, 0/*syscall through epc */); /* AT_SYSINFO */ \
  OS_auxv_entry(33, OSDefs::oGATE_AREA);  /* AT_SYSINFO_EHDR */ 

#define CC_hook_pipe()				     \
  realct->r[8].val = fdes[0];  realct->r[8].nat = 0; \
  realct->r[9].val = fdes[1];  realct->r[9].nat = 0;

#define CC_open_filter(fname) (!strncmp(fname,"/proc/",6))

static int CC_elfarchchecker(int foundflag, void *ct) {
  return foundflag == 50;
}

#define CC_execveguts() IA64_execveguts(realct, start_stack, osct)

static inline void IA64_execveguts(CC_isacontext_t *realct,
				   LSE_emu_addr_t start_stack,
				   struct Linux_context_t *osct);

#define CC_context_copy(i,ppc,pc,cm) EMU_context_copy(i,ppc,pc)
#define CC_context_destroy(i,cp)  EMU_context_destroy(cp)

#define CC_cloneguts() IA64_cloneguts(stackstart,stacksize,newisact)

static inline void IA64_cloneguts(LSE_emu_addr_t stackstart,
				  LSE_emu_addr_t stacksize,
				  CC_isacontext_t *newisact) {
  /* Linux actually sets up the forked off thread with the same register
   * values, but a different stack.  Kind of scary, when you think about
   * it.  But it really is the way it is supposed to work.  The stacked GRs
   * are handled by cleverness in OS with calls and flushrs, etc., but
   * we actually do not have to do that, since we just get a copy of the
   * physical registers!  Note that we are implicitly assuming that all 
   * hardware contexts have the same number of physical registers.
   */
  if (stackstart + stacksize != 0) {
    newisact->r[12].val = stackstart + stacksize;
    newisact->r[12].nat = 0;
    newisact->ar[IA64_ADDR_BSPSTORE] = stackstart;
    newisact->ar[IA64_ADDR_BSP] = stackstart;
    newisact->RSE_other.NDirty = 0;
    newisact->RSE_other.NClean = 0;
  }
  newisact->ar[IA64_ADDR_RNAT] = 0;
  CC_sys_set_return(newisact, 0, 0);
  CC_sys_set_return(newisact, 1, 0);  
}

#define CC_sigreturn(n) IA64_sigreturn(realct, osct, sigmask)

static inline void IA64_sigreturn(CC_isacontext_t *realct,
				  struct Linux_context_t *osct,
				  uint64_t &sigmask);

#define CC_init_hook(x) (ldi->ctl.recordOS=false)

#define CC_invalidate_tlb(ctx)
#define CC_drop_mapping(os, va, pa)
#define CC_obtain_write_permission(ct,va,ln) (false)

#define CC_chkpt_cntl_clear(c) ((c).recordOS = (c).incrementalMem = false)
#define CC_ict_write_chkpt(c,ci) EMU_write_context(cptFile,nmark,c,ci)
#define CC_ict_parse_chkpt(c)                            \
  if ((cerr = EMU_parse_context(intr, cptFile, c, ctl))) \
    return cerr;                                         \
  CC_mem_redirect(c,&osct->mem->vmdevice);


#define CC_arg_ptr(n,s) IA64_arg_ptr(realct, n, s)
#define CC_arg_int(n,s) IA64_arg_int(realct, n, s)

LSE_emu_addr_t IA64_arg_ptr(CC_isacontext_t *realct, int n, const char *sig) {
  return (get_r_ptr_ctx(realct, n + 32 + SOL_ct(realct))->val);
}

uint64_t IA64_arg_int(CC_isacontext_t *realct, int n, const char *sig) {
  return (get_r_ptr_ctx(realct, n + 32 + SOL_ct(realct))->val);
}

#include "OS/LSE_Linux.h"

  inline struct Linux_siginfo_t OSDefs::L_siginfo::tosiginfo() { 
    struct Linux_siginfo_t nv; // TODO: fill in
    return nv;
  }

  inline void OSDefs::L_siginfo::fromsiginfo(struct Linux_siginfo_t &v) {
    si_signo = LSE_h2b((int)v.LSEsi_signo);
    si_errno = LSE_h2b((int)v.LSEsi_errno);
    si_code = LSE_h2b((int)v.LSEsi_code);
    switch (v.LSEsi_code) {
    case OSDefs::oSI_USER:
    case OSDefs::oSI_TKILL:
      d.normal.pid = LSE_h2b((int)v.LSEsi_pid);
      d.normal.uid = LSE_h2b((int)v.LSEsi_uid);
      break;
    case OSDefs::oCLD_EXITED:
      d.child.pid = LSE_h2b((int)v.LSEsi_pid);
      d.child.uid = LSE_h2b((int)v.LSEsi_uid);
      d.child.status = LSE_h2b((int)v.LSEsi_status);
      d.child.utime = LSE_h2b((typeof d.child.utime)v.LSEsi_utime);
      d.child.stime = LSE_h2b((typeof d.child.stime)v.LSEsi_stime);
      break;
    default: break;
    }
  }

static inline void IA64_execveguts(CC_isacontext_t *realct,
				   LSE_emu_addr_t start_stack,
				   struct Linux_context_t *osct) {

  // The gate page
  do_mmap(osct, OSDefs::oGATE_AREA, IA64_PAGE_SIZE * 2, 
          0, OSDefs::oPROT_READ|OSDefs::oPROT_EXEC, 
          OSDefs::oMAP_FIXED | OSDefs::oMAP_ANONYMOUS | OSDefs::oMAP_PRIVATE,
	  0);

  // Now, I need to put the right stuff in the gate area!
  // trampoline must just do the sigreturn syscall, because we set up
  // all of the magic for the registers and stack, including the return 
  // pointer which will put us in the trampoline.  So really, the trampoline
  // is just to catch us if we return funny.
  // mov r15=1181;; break.m 0x100000 nop.i 0x0;;
  uint8_t trampcode[]={ 0x0b, 0x78, 0x74, 0x00, 0x09, 0x21, 0x00, 0x00, 0x00,
  	   		0x00, 0x04, 0x00, 0x00, 0x00, 0x04, 0x00 };
  OS_mem_write(realct, OSDefs::oGATE_AREA, 16, &trampcode);

  // And set up a bunch of registers

  realct->ar[IA64_ADDR_BSPSTORE] = IA64_USTACK_BASE;
  realct->ar[IA64_ADDR_BSP] = IA64_USTACK_BASE;
  realct->RSE_other.NDirty = 0;
  realct->RSE_other.NClean = 0;
  realct->ar[IA64_ADDR_RNAT] = 0;
  realct->ar[IA64_ADDR_RSC] = 0xf;

  uint64_t psr = 0;
  rf_set(psr,PSR_dfh,0); /* Linux: makes this 1 and then enables at fault
			  *        used to make context switch faster if the
			  *        high registers are not used.
			  */
  rf_set(psr,PSR_mfh,0);
  rf_set(psr,PSR_cpl,3);
  rf_set(psr,PSR_ri,0);
  rf_set(psr,PSR_is,0);
  rf_set(psr,PSR_ic,0);  /* disable interrupts */
  realct->sr[IA64_ADDR_PSR].PSR = psr;

  realct->r[8].nat = 0;
  realct->r[8].val = 0;
  realct->r[12].nat = 0;
  realct->r[12].val = start_stack;
}

struct IA64_siginfo {
  int si_signo;
  int si_errno;
  int si_code;
  int pad;
  union {
    int pad[28];
    struct {
      int pid;
      int uid;
      OSDefs::L_sigval_t sigval;
    } normal;
    struct {
      int pid;
      int uid;
      int status;
      OSDefs::L_clock_t utime;
      OSDefs::L_clock_t stime;
    } child;
    struct {
      int tid;
      int overrun;
      OSDefs::L_sigval_t sigval;
      int _private;
    } timer;
    struct {
      uint64_t band;
      int fd;
    } poll;
    struct {
      LSE_emu_addr_t addr;
      int imm;
      unsigned int flags;
      uint64_t isr;
    } fault;
  } d;
  inline struct Linux_siginfo_t tosiginfo();
  inline void fromsiginfo(struct Linux_siginfo_t &v);
  IA64_siginfo(struct Linux_siginfo_t &v) { fromsiginfo(v); }
  IA64_siginfo() {}
};  

boolean Linux_is_os(IA64_context_t *realct,
                LSE_emu_instr_info_t *ii,
                int unit, uint64_t breakno) {
  return (breakno == 0x100000);
}

  inline Linux_siginfo_t IA64_siginfo::tosiginfo() {
    Linux_siginfo_t nv;
    return nv;
  }

  inline void IA64_siginfo::fromsiginfo(Linux_siginfo_t &v) {
      si_signo = LSE_h2l((int)v.LSEsi_signo);
      si_errno = LSE_h2l((int)v.LSEsi_errno);
      si_code = LSE_h2l((int)v.LSEsi_code);
      switch (v.LSEsi_code) {
      case OSDefs::oSI_USER:
        d.normal.pid = LSE_h2l((int)v.LSEsi_pid);
	d.normal.uid = LSE_h2l((int)v.LSEsi_uid);
        break;
      case OSDefs::oCLD_EXITED:
        d.child.pid    = LSE_h2l((int)v.LSEsi_pid);
        d.child.uid    = LSE_h2l((int)v.LSEsi_uid);
	d.child.status = LSE_h2l((int)v.LSEsi_status);
        d.child.utime  = LSE_h2l((typeof d.child.utime)v.LSEsi_utime);
        d.child.stime  = LSE_h2l((typeof d.child.stime)v.LSEsi_stime);
	break;
      default: break;
      }
  }

static inline void IA64_sigreturn(CC_isacontext_t *realct,
				  struct Linux_context_t *osct,
				  uint64_t &sigmask) {

  realct->ALAT_numentries = 0; // flush the ALAT again

  LSE_emu_addr_t sp, slen;
  sp = realct->r[12].val + 16;
  struct IA64_context_saver savecont;

  slen = sizeof(struct IA64_context_saver);
  CC_mem_read(realct->mem, sp, slen, &savecont);
  
  sigmask = savecont.sigmask;
  realct->startaddr = savecont.ip;
  realct->sr[0] = savecont.cfm;
  realct->ar[IA64_ADDR_RSC] =      savecont.rsc; 
  realct->ar[IA64_ADDR_BSP] =      savecont.bsp;
  realct->ar[IA64_ADDR_BSPSTORE] =      savecont.bspstore;
  realct->ar[IA64_ADDR_RNAT] =      savecont.rnat;
  realct->ar[IA64_ADDR_CCV] =      savecont.ccv;
  realct->ar[IA64_ADDR_UNAT] =      savecont.unat;
  realct->ar[IA64_ADDR_FPSR] =      savecont.fpsr;
  realct->ar[IA64_ADDR_PFS] =      savecont.pfs;
  realct->ar[IA64_ADDR_LC] =      savecont.lc;
  realct->ar[25] =      savecont.ar25;
  realct->ar[26] =      savecont.ar26;
  realct->sr[1].PSR &= ~rf_mask(PSR_umask);
  realct->sr[1].PSR |= savecont.um;
  for (int i = 0; i < 32; i++) {
     realct->r[i].val = savecont.gr[i];
     realct->r[i].nat = (savecont.nat >> i) & 1;
  }
  for (int i = 0; i < 64; i++) realct->p[i] = (savecont.pr >> i) & 1;
  memcpy(&realct->f[0], &savecont.fr[0], sizeof(realct->f));
  memcpy(&realct->b[0], &savecont.br[0], sizeof(realct->b));

  // now fill the registers for the interrupted frame.  The CFM has the
  // size of the frame and the BSPSTORE/BSP are pointing to the right place
  // (and should be pointing at the same place).  So all that is needed is
  // the appropriate "return" behavior.

  IA64_SR_t &cfm = realct->sr[0];
  uint64_t bsp = realct->ar[IA64_ADDR_BSP];
  uint64_t &rnat = realct->ar[IA64_ADDR_RNAT];
  uint64_t loadp = realct->ar[IA64_ADDR_BSPSTORE];
  int bitnum = (loadp>>3) & 0x3f;
  uint64_t mask = uint64_t(1) << bitnum;
  int rno = IA64_GR_FAST_ROT(cfm.CFM.BOF + rf_get(cfm.CFM.val,CFM_sof),
      	    		     realct->num_stacked_phys);

  while (loadp != bsp) {
    loadp -= 8;
    bitnum--;
    mask >>= 1;
    if (bitnum == -1) { // get RNAT
      uint64_t urnat;
      CC_mem_read(realct->mem, loadp, 8, &urnat);
      rnat = LSE_l2h(urnat);
      bitnum = 63;
      mask = uint64_t(1) << 63;
    } else {
      rno = IA64_GR_FAST_ROT(rno - 1 + realct->num_stacked_phys,
			     realct->num_stacked_phys);
      uint64_t uval;
      CC_mem_read(realct->mem, loadp, 8, &uval);
      realct->physr[rno].val = LSE_l2h(uval);
      realct->physr[rno].nat = (mask & rnat) ? 1 : 0;
    }
  }
  realct->ar[IA64_ADDR_BSPSTORE] = bsp;

  handle_sigaltstack(realct, osct, 1,0,CC_get_stackptr(realct));  
}

static bool CC_do_a_signal(IA64_context_t *realct, bool must_restart,
       	    		   LSE_emu_iaddr_t pc, LSE_emu_iaddr_t &npc) {

  // This is, without a doubt, the most evil code in the whole OS emulation and
  // is the most evil ISA to do it for because of the nastiness of 
  // the IA64 register stack.

  Linux_context_t *osct = OS_ict_osinfo(realct);

  if (osct->sigPendingFlag) {
    int actualerrno = realct->r[8].val;

    // pick a signal and find out all the information about it.
    Linux_siginfo_t sinfo;
    Linux_sighandinf_t shand;

    uint64_t maskafter = (osct->oldSigBlockedValid ? osct->oldsigblocked :
			  osct->sigblocked);

    pick_signal(osct, sinfo, shand);

    if (realct->r[10].val != -1) must_restart = false;
;
    if (sinfo.LSEsi_signo > 0) { // do signal

      if (must_restart &&
          (actualerrno == OSDefs::oERESTART_RESTARTBLOCK ||
           actualerrno == OSDefs::oERESTARTNOHAND ||
	   (actualerrno == OSDefs::oERESTARTSYS && 
	    !(OSDefs::oSA_RESTART & shand.flags)))) {

        realct->r[8].val = OSDefs::oEINTR;

      } else if (must_restart && actualerrno == OSDefs::oERESTARTNOINTR) {
	must_restart = false;
        npc = realct->startaddr = pc;
      }

      // NOTE: the type of sig context should be ucontext_t, but I am *not*
      // going there unless I absolutely have to...
      //
      // The RSE stuff is annoying.  I do not actually have to flush
      // the register stack because each context has independent copies of
      // the physical registers and pointers, unlike real hardware.  However,
      // I do need to allocate a new frame, somewhat in-line with the
      // old frame.  The way to do that by emulating a cover followed by
      // an alloc *after* the CFM state has been saved.  There may be a small
      // amount of flushing of the RSE as a result.
      // ================================================================
      //
      //  old frame
      //  ==================== 
      //  signal handler caller frame:
      //	sig context (caller-save registers)
      //	sig info
      //  =========================
      //
      // RSE:  old frames | argument registers
      //
      // ALAT: flushed

      realct->ALAT_numentries = 0;
      
      LSE_emu_addr_t sp, slen;

      // save context information (sigcontext)

      IA64_context_saver savecont;

      savecont.ip = npc;
      savecont.cfm = realct->sr[0];
      savecont.bsp = realct->ar[IA64_ADDR_BSP];
      savecont.rsc = realct->ar[IA64_ADDR_RSC];
      savecont.ccv = realct->ar[IA64_ADDR_CCV];
      savecont.unat = realct->ar[IA64_ADDR_UNAT];
      savecont.fpsr = realct->ar[IA64_ADDR_FPSR];
      savecont.pfs = realct->ar[IA64_ADDR_PFS];
      savecont.lc = realct->ar[IA64_ADDR_LC];
      savecont.ar25 = realct->ar[25];
      savecont.ar26 = realct->ar[26];
      savecont.um = realct->sr[1].PSR & rf_mask(PSR_umask);
      savecont.nat = 0;
      savecont.sigmask = maskafter;
      savecont.pr = 0;

      for (int i = 0; i < 32; i++) {
        savecont.gr[i] = realct->r[i].val;
	savecont.nat |= (((uint32_t)realct->r[i].nat) << i);
      }

      for (int i = 0; i < 64; i++) 
        savecont.pr |= realct->p[i] ? (uint64_t(1)<<i) : 0;

      memcpy(&savecont.fr[0], &realct->f[0], sizeof(realct->f));
      memcpy(&savecont.br[0], &realct->b[0], sizeof(realct->b));

      // emulate a cover
      IA64_SR_t &cfm = realct->sr[0];
      LSE_emu_addr_t &bsp = realct->ar[IA64_ADDR_BSP];

      unsigned int bof = IA64_GR_FAST_ROT(cfm.CFM.BOF + 
      	       	       	 		  rf_get(cfm.CFM.val,CFM_sof),
      	    		                  realct->num_stacked_phys);

      realct->RSE_other.NDirty += rf_get(cfm.CFM.val,CFM_sof);
      bsp = (bsp&~INT64_C(7)) + 8*(rf_get(cfm.CFM.val,CFM_sof) + 
				(((bsp>>3)&0x3f)+rf_get(cfm.CFM.val,CFM_sof))
				/63);

      rf_set(cfm.CFM.val,CFM_sof,0);
      rf_set(cfm.CFM.val,CFM_sol,0);
      rf_set(cfm.CFM.val,CFM_sor,0);
      rf_set(cfm.CFM.val,CFM_rrb,0);
      cfm.CFM.BOF = bof;

      // now flushrs.  It is not strictly necessary, but makes the stack
      // a lot easier to deal with, as all we will need to do at return is 
      // recover the registers in the interrupted frame.

      LSE_emu_addr_t &bspstore = realct->ar[IA64_ADDR_BSPSTORE];
      uint64_t &rnat = realct->ar[IA64_ADDR_RNAT];

      int numtodo = realct->RSE_other.NDirty + 
      	      (((bspstore>>3) & 0x3f) + realct->RSE_other.NDirty)/63;
      
      int bitnum = (bspstore >> 3) & 0x3f;
      int rno = IA64_GR_FAST_ROT(bof - realct->RSE_other.NDirty +
      	      			 realct->num_stacked_phys, 
                                 realct->num_stacked_phys);
      uint64_t mask = uint64_t(1) << bitnum;
     
      while (numtodo--) {
        if (bitnum == 63) {
	  uint64_t urnat = LSE_h2l(rnat);
	  realct->mem->write(bspstore, (unsigned char *)&rnat, 8);
          bitnum = 0;
	  bspstore += 8;
	  mask = 1;
        } else {
	  rnat = (rnat & ~mask) | 
	         (((uint64_t)realct->physr[rno].nat) << bitnum);
	  uint64_t rval = LSE_h2l(realct->physr[rno].val);
	  CC_mem_write(realct->mem,bspstore, 8, &rval);
	  bspstore += 8;
	  bitnum++;
	  rno = IA64_GR_FAST_ROT(rno + 1,realct->num_stacked_phys);
	  mask <<= 1;
        }
      }

      realct->RSE_other.NClean += realct->RSE_other.NDirty;
      realct->RSE_other.NDirty = 0;
      realct->RSE_other.NClean = 0; // so we will always reload later.
      rf_set(cfm.CFM.val,CFM_sof,3); // frame is 3, but all output!

      savecont.bspstore = realct->ar[IA64_ADDR_BSPSTORE];
      savecont.rnat = realct->ar[IA64_ADDR_RNAT];

      IA64_siginfo infostruct(sinfo);

      sp = realct->r[12].val;
      slen = sizeof(IA64_siginfo);
      sp -= slen;
      realct->mem->write(sp, (unsigned char *)&infostruct, slen);
      rno = IA64_GR_FAST_ROT(cfm.CFM.BOF+1, realct->num_stacked_phys);
      realct->physr[rno].val = sp;
      realct->physr[rno].nat = 0;

      slen = sizeof(struct IA64_context_saver);
      sp -= slen;
      CC_mem_write(realct->mem, sp, slen, &savecont);

      rno = IA64_GR_FAST_ROT(cfm.CFM.BOF+2,realct->num_stacked_phys);
      realct->physr[rno].val = sp;
      realct->physr[rno].nat = 0;

      rno = IA64_GR_FAST_ROT(cfm.CFM.BOF,realct->num_stacked_phys);
      realct->physr[rno].val = sinfo.LSEsi_signo;
      realct->physr[rno].nat = 0;

      // Set up return address to go to trampoline and correct stack
      realct->b[0] = OSDefs::oGATE_AREA;
      realct->r[12].val = sp - 16;  // scratch area is required and used

      // and emulate the call by setting up next_pc
      LSE_emu_addr_t raddr;
      // now go to the handler.  We assume the syscall did not block!
      CC_mem_read(realct->mem, shand.handler, 8, &raddr);
      npc = realct->startaddr = LSE_l2h(raddr);

      // update pending state

      if (!(shand.flags & OSDefs::oSA_NODEFER)) {
        osct->sigblocked |= shand.sigmask;
        osct->sigblocked |= uint64_t(1) << (sinfo.LSEsi_signo-1);

        osct->sigPendingFlag = (osct->sigpending & ~osct->sigblocked) || 
  		               (osct->tgroup->sigpending & ~osct->sigblocked);
      }

#ifdef DEBUG_SIGNAL
      std::cerr << "Found signal " << sinfo.LSEsi_signo 
      << " handler " << std::hex << npc << std::dec
      << "\n";
#endif

      return true;
    } 

    // no signal handled... redo the syscall if it reported a restart

    if (must_restart &&
        (actualerrno == OSDefs::oERESTART_RESTARTBLOCK ||
         actualerrno == OSDefs::oERESTARTNOHAND ||
	 actualerrno == OSDefs::oERESTARTSYS ||
	 actualerrno == OSDefs::oERESTARTNOINTR)) {

      npc = realct->startaddr = pc;
      if (actualerrno == OSDefs::oERESTARTNOINTR) {
        realct->r[15].val = 1246; // restart syscall
        realct->r[15].nat = 0;
      }

    }

  } // if sigpending
  return false;
}

int Linux_call_os(IA64_context_t *realct,
    		LSE_emu_instr_info_t *ii,
                LSE_emu_iaddr_t curr_pc,
                LSE_emu_iaddr_t &following_pc,
		int unit,
		uint64_t breakno) {

  uint64_t r15val;
  int osres=0;


  if (breakno != 0x100000) {
    ii->privatef.actual_intr = ii->privatef.potential_intr 
	= (int)IA64_intr_breakinstruction;
    return 1;
  }
  if (get_r_nat(realct,ii,15)) {
    ii->privatef.actual_intr = ii->privatef.potential_intr 
	= (int)IA64_intr_registernatconsumption;
    return 1;
  }
  realct->startaddr = following_pc;
  r15val = get_r_val(realct,ii,15);

  int callno = r15val;

  switch (callno) {

    case 1024 : 
       osres = Linux_s_ni_syscall(callno,realct,curr_pc,0);
       break;

    case 1025 : 
       osres = Linux_s_exit(callno,realct,curr_pc,0);
       break;

    case 1026 : 
       osres = Linux_s_read(callno,realct,curr_pc,0);
       break;

    case 1027 : 
       osres = Linux_s_write(callno,realct,curr_pc,0);
       break;

    case 1028 : 
       osres = Linux_s_open(callno,realct,curr_pc,0);
       break;

    case 1029 : 
       osres = Linux_s_close(callno,realct,curr_pc,0);
       break;

    case 1030 : 
       osres = Linux_s_creat(callno,realct,curr_pc,0);
       break;

    case 1031 : 
       osres = Linux_s_link(callno,realct,curr_pc,0);
       break;

    case 1032 : 
       osres = Linux_s_unlink(callno,realct,curr_pc,0);
       break;

    case 1033 : 
       osres = Linux_s_execve(callno,realct,curr_pc,0);
       break;

    case 1034 : 
       osres = Linux_s_chdir(callno,realct,curr_pc,0);
       break;

    case 1035 : 
       osres = Linux_s_fchdir(callno,realct,curr_pc,0);
       break;

    case 1036 : 
       osres = Linux_s_utimes(callno,realct,curr_pc,0);
       break;

    case 1037 : 
       osres = Linux_s_mknod(callno,realct,curr_pc,0);
       break;

    case 1038 : 
       osres = Linux_s_chmod(callno,realct,curr_pc,0);
       break;

    case 1039 : 
       osres = Linux_s_chown(callno,realct,curr_pc,0);
       break;

    case 1040 : 
       osres = Linux_s_lseek(callno,realct,curr_pc,0);
       break;

    case 1041 : 
       osres = Linux_s_getpid(callno,realct,curr_pc,0);
       break;

    case 1042 : 
       osres = Linux_s_getppid(callno,realct,curr_pc,0);
       break;

    case 1043 : 
       osres = Linux_s_mount(callno,realct,curr_pc,0);
       break;

    case 1044 : 
       osres = Linux_s_umount(callno,realct,curr_pc,0);
       break;

    case 1045 : 
       osres = Linux_s_setuid(callno,realct,curr_pc,0);
       break;

    case 1046 : 
       osres = Linux_s_getuid(callno,realct,curr_pc,0);
       break;

    case 1047 : 
       osres = Linux_s_geteuid(callno,realct,curr_pc,0);
       break;

    case 1048 : 
       osres = Linux_s_ptrace(callno,realct,curr_pc,0);
       break;

    case 1049 : 
       osres = Linux_s_access(callno,realct,curr_pc,0);
       break;

    case 1050 : 
       osres = Linux_s_sync(callno,realct,curr_pc,0);
       break;

    case 1051 : 
       osres = Linux_s_fsync(callno,realct,curr_pc,0);
       break;

    case 1052 : 
       osres = Linux_s_fdatasync(callno,realct,curr_pc,0);
       break;

    case 1053 : 
       osres = Linux_s_kill(callno,realct,curr_pc,0);
       break;

    case 1054 : 
       osres = Linux_s_rename(callno,realct,curr_pc,0);
       break;

    case 1055 : 
       osres = Linux_s_mkdir(callno,realct,curr_pc,0);
       break;

    case 1056 : 
       osres = Linux_s_rmdir(callno,realct,curr_pc,0);
       break;

    case 1057 : 
       osres = Linux_s_dup(callno,realct,curr_pc,0);
       break;

    case 1058 : 
       osres = Linux_s_pipe(callno,realct,curr_pc,0);
       break;

    case 1059 : 
       osres = Linux_s_times(callno,realct,curr_pc,0);
       break;

    case 1060 : 
       osres = Linux_s_brk(callno,realct,curr_pc,0);
       break;

    case 1061 : 
       osres = Linux_s_setgid(callno,realct,curr_pc,0);
       break;

    case 1062 : 
       osres = Linux_s_getgid(callno,realct,curr_pc,0);
       break;

    case 1063 : 
       osres = Linux_s_getegid(callno,realct,curr_pc,0);
       break;

    case 1064 : 
       osres = Linux_s_acct(callno,realct,curr_pc,0);
       break;

    case 1065 : 
       osres = Linux_s_ioctl(callno,realct,curr_pc,0);
       break;

    case 1066 : 
       osres = Linux_s_fcntl(callno,realct,curr_pc,0);
       break;

    case 1067 : 
       osres = Linux_s_umask(callno,realct,curr_pc,0);
       break;

    case 1068 : 
       osres = Linux_s_chroot(callno,realct,curr_pc,0);
       break;

    case 1069 : 
       osres = Linux_s_ustat(callno,realct,curr_pc,0);
       break;

    case 1070 : 
       osres = Linux_s_dup2(callno,realct,curr_pc,0);
       break;

    case 1071 : 
       osres = Linux_s_setreuid(callno,realct,curr_pc,0);
       break;

    case 1072 : 
       osres = Linux_s_setregid(callno,realct,curr_pc,0);
       break;

    case 1073 : 
       osres = Linux_s_getresuid(callno,realct,curr_pc,0);
       break;

    case 1074 : 
       osres = Linux_s_setresuid(callno,realct,curr_pc,0);
       break;

    case 1075 : 
       osres = Linux_s_getresgid(callno,realct,curr_pc,0);
       break;

    case 1076 : 
       osres = Linux_s_setresgid(callno,realct,curr_pc,0);
       break;

    case 1077 : 
       osres = Linux_s_getgroups(callno,realct,curr_pc,0);
       break;

    case 1078 : 
       osres = Linux_s_setgroups(callno,realct,curr_pc,0);
       break;

    case 1079 : 
       osres = Linux_s_getpgid(callno,realct,curr_pc,0);
       break;

    case 1080 : 
       osres = Linux_s_setpgid(callno,realct,curr_pc,0);
       break;

    case 1081 : 
       osres = Linux_s_setsid(callno,realct,curr_pc,0);
       break;

    case 1082 : 
       osres = Linux_s_getsid(callno,realct,curr_pc,0);
       break;

    case 1083 : 
       osres = Linux_s_sethostname(callno,realct,curr_pc,0);
       break;

    case 1084 : 
       osres = Linux_s_setrlimit(callno,realct,curr_pc,0);
       break;

    case 1085 : 
       osres = Linux_s_getrlimit(callno,realct,curr_pc,0);
       break;

    case 1086 : 
       osres = Linux_s_getrusage(callno,realct,curr_pc,0);
       break;

    case 1087 : 
       osres = Linux_s_gettimeofday(callno,realct,curr_pc,0);
       break;

    case 1088 : 
       osres = Linux_s_settimeofday(callno,realct,curr_pc,0);
       break;

    case 1089 : 
       osres = Linux_s_select(callno,realct,curr_pc,0);
       break;

    case 1090 : 
       osres = Linux_s_poll(callno,realct,curr_pc,0);
       break;

    case 1091 : 
       osres = Linux_s_symlink(callno,realct,curr_pc,0);
       break;

    case 1092 : 
       osres = Linux_s_readlink(callno,realct,curr_pc,0);
       break;

    case 1093 : 
       osres = Linux_s_uselib(callno,realct,curr_pc,0);
       break;

    case 1094 : 
       osres = Linux_s_swapon(callno,realct,curr_pc,0);
       break;

    case 1095 : 
       osres = Linux_s_swapoff(callno,realct,curr_pc,0);
       break;

    case 1096 : 
       osres = Linux_s_reboot(callno,realct,curr_pc,0);
       break;

    case 1097 : 
       osres = Linux_s_truncate(callno,realct,curr_pc,0);
       break;

    case 1098 : 
       osres = Linux_s_ftruncate(callno,realct,curr_pc,0);
       break;

    case 1099 : 
       osres = Linux_s_fchmod(callno,realct,curr_pc,0);
       break;

    case 1100 : 
       osres = Linux_s_fchown(callno,realct,curr_pc,0);
       break;

    case 1101 : 
       osres = Linux_s_getpriority(callno,realct,curr_pc,0);
       break;

    case 1102 : 
       osres = Linux_s_setpriority(callno,realct,curr_pc,0);
       break;

    case 1103 : 
       osres = Linux_s_statfs(callno,realct,curr_pc,0);
       break;

    case 1104 : 
       osres = Linux_s_fstatfs(callno,realct,curr_pc,0);
       break;

    case 1106 : 
       osres = Linux_s_semget(callno,realct,curr_pc,0);
       break;

    case 1107 : 
       osres = Linux_s_semop(callno,realct,curr_pc,0);
       break;

    case 1108 : 
       osres = Linux_s_semctl(callno,realct,curr_pc,0);
       break;

    case 1109 : 
       osres = Linux_s_msgget(callno,realct,curr_pc,0);
       break;

    case 1110 : 
       osres = Linux_s_msgsnd(callno,realct,curr_pc,0);
       break;

    case 1111 : 
       osres = Linux_s_msgrcv(callno,realct,curr_pc,0);
       break;

    case 1112 : 
       osres = Linux_s_msgctl(callno,realct,curr_pc,0);
       break;

    case 1113 : 
       osres = Linux_s_shmget(callno,realct,curr_pc,0);
       break;

    case 1114 : 
       osres = Linux_s_shmat(callno,realct,curr_pc,0);
       break;

    case 1115 : 
       osres = Linux_s_shmdt(callno,realct,curr_pc,0);
       break;

    case 1116 : 
       osres = Linux_s_shmctl(callno,realct,curr_pc,0);
       break;

    case 1117 : 
       osres = Linux_s_syslog(callno,realct,curr_pc,0);
       break;

    case 1118 : 
       osres = Linux_s_setitimer(callno,realct,curr_pc,0);
       break;

    case 1119 : 
       osres = Linux_s_getitimer(callno,realct,curr_pc,0);
       break;

    case 1123 : 
       osres = Linux_s_vhangup(callno,realct,curr_pc,0);
       break;

    case 1124 : 
       osres = Linux_s_lchown(callno,realct,curr_pc,0);
       break;

    case 1125 : 
       osres = Linux_s_remap_file_pages(callno,realct,curr_pc,0);
       break;
    case 1126 : 
       osres = Linux_s_wait4(callno,realct,curr_pc,0);
       break;

    case 1127 : 
       osres = Linux_s_sysinfo(callno,realct,curr_pc,0);
       break;

    case 1128 : 
       osres = Linux_s_clone(callno,realct,curr_pc,0);
       break;

    case 1129 : 
       osres = Linux_s_setdomainname(callno,realct,curr_pc,0);
       break;

    case 1130 : 
       osres = Linux_s_newuname(callno,realct,curr_pc,0);
       break;

    case 1131 : 
       osres = Linux_s_adjtimex(callno,realct,curr_pc,0);
       break;

    case 1133 : 
       osres = Linux_s_init_module(callno,realct,curr_pc,0);
       break;

    case 1134 : 
       osres = Linux_s_delete_module(callno,realct,curr_pc,0);
       break;

    case 1137 : 
       osres = Linux_s_quotactl(callno,realct,curr_pc,0);
       break;

    case 1138 : 
       osres = Linux_s_bdflush(callno,realct,curr_pc,0);
       break;

    case 1139 : 
       osres = Linux_s_sysfs(callno,realct,curr_pc,0);
       break;

    case 1140 : 
       osres = Linux_s_personality(callno,realct,curr_pc,0);
       break;

    case 1142 : 
       osres = Linux_s_setfsuid(callno,realct,curr_pc,0);
       break;

    case 1143 : 
       osres = Linux_s_setfsgid(callno,realct,curr_pc,0);
       break;

    case 1144 : 
       osres = Linux_s_getdents(callno,realct,curr_pc,0);
       break;

    case 1145 : 
       osres = Linux_s_flock(callno,realct,curr_pc,0);
       break;

    case 1146 : 
       osres = Linux_s_readv(callno,realct,curr_pc,0);
       break;

    case 1147 : 
       osres = Linux_s_writev(callno,realct,curr_pc,0);
       break;

    case 1148 : 
       osres = Linux_s_pread(callno,realct,curr_pc,0);
       break;

    case 1149 : 
       osres = Linux_s_pwrite(callno,realct,curr_pc,0);
       break;

    case 1150 : 
       osres = Linux_s__sysctl(callno,realct,curr_pc,0);
       break;

    case 1151 : 
       osres = Linux_s_mmap(callno,realct,curr_pc,0);
       break;

    case 1152 : 
       osres = Linux_s_munmap(callno,realct,curr_pc,0);
       break;

    case 1153 : 
       osres = Linux_s_mlock(callno,realct,curr_pc,0);
       break;

    case 1154 : 
       osres = Linux_s_mlockall(callno,realct,curr_pc,0);
       break;

    case 1155 : 
       osres = Linux_s_mprotect(callno,realct,curr_pc,0);
       break;

    case 1156 : 
       osres = Linux_s_mremap(callno,realct,curr_pc,0);
       break;

    case 1157 : 
       osres = Linux_s_msync(callno,realct,curr_pc,0);
       break;

    case 1158 : 
       osres = Linux_s_munlock(callno,realct,curr_pc,0);
       break;

    case 1159 : 
       osres = Linux_s_munlockall(callno,realct,curr_pc,0);
       break;

    case 1160 : 
       osres = Linux_s_sched_getparam(callno,realct,curr_pc,0);
       break;

    case 1161 : 
       osres = Linux_s_sched_setparam(callno,realct,curr_pc,0);
       break;

    case 1162 : 
       osres = Linux_s_sched_getscheduler(callno,realct,curr_pc,0);
       break;

    case 1163 : 
       osres = Linux_s_sched_setscheduler(callno,realct,curr_pc,0);
       break;

    case 1164 : 
       osres = Linux_s_sched_yield(callno,realct,curr_pc,0);
       break;

    case 1165 : 
       osres = Linux_s_sched_get_priority_max(callno,realct,curr_pc,0);
       break;

    case 1166 : 
       osres = Linux_s_sched_get_priority_min(callno,realct,curr_pc,0);
       break;

    case 1167 : 
       osres = Linux_s_sched_rr_get_interval(callno,realct,curr_pc,0);
       break;

    case 1168 : 
       osres = Linux_s_nanosleep(callno,realct,curr_pc,0);
       break;

    case 1169 : 
       osres = Linux_s_nfsservctl(callno,realct,curr_pc,0);
       break;

    case 1170 : 
       osres = Linux_s_prctl(callno,realct,curr_pc,0);
       break;

    case 1171 : 
       osres = Linux_s_getpagesize(callno,realct,curr_pc,0);
       break;

    case 1172 : 
       osres = Linux_s_mmap2(callno,realct,curr_pc,0);
       break;

    case 1173 : 
       osres = Linux_s_pciconfig_read(callno,realct,curr_pc,0);
       break;

    case 1174 : 
       osres = Linux_s_pciconfig_write(callno,realct,curr_pc,0);
       break;

    case 1175 : 
       osres = Linux_s_perfmonctl(callno,realct,curr_pc,0);
       break;

    case 1176 : 
       osres = Linux_s_sigaltstack(callno,realct,curr_pc,0);
       break;

    case 1177 : 
       osres = Linux_s_rt_sigaction(callno,realct,curr_pc,0);
       break;

    case 1178 : 
       osres = Linux_s_rt_sigpending(callno,realct,curr_pc,0);
       break;

    case 1179 : 
       osres = Linux_s_rt_sigprocmask(callno,realct,curr_pc,0);
       break;

    case 1180 : 
       osres = Linux_s_rt_sigqueueinfo(callno,realct,curr_pc,0);
       break;

    case 1181 : 
       osres = Linux_s_rt_sigreturn(callno,realct,curr_pc,0);
       realct->startaddr = following_pc;
       break;

    case 1182 : 
       osres = Linux_s_rt_sigsuspend(callno,realct,curr_pc,0);
       break;

    case 1183 : 
       osres = Linux_s_rt_sigtimedwait(callno,realct,curr_pc,0);
       break;

    case 1184 : 
       osres = Linux_s_getcwd(callno,realct,curr_pc,0);
       break;

    case 1185 : 
       osres = Linux_s_capget(callno,realct,curr_pc,0);
       break;

    case 1186 : 
       osres = Linux_s_capset(callno,realct,curr_pc,0);
       break;

    case 1187 : 
       osres = Linux_s_sendfile(callno,realct,curr_pc,0);
       break;

    case 1190 : 
       osres = Linux_s_socket(callno,realct,curr_pc,0);
       break;

    case 1191 : 
       osres = Linux_s_bind(callno,realct,curr_pc,0);
       break;

    case 1192 : 
       osres = Linux_s_connect(callno,realct,curr_pc,0);
       break;

    case 1193 : 
       osres = Linux_s_listen(callno,realct,curr_pc,0);
       break;

    case 1194 : 
       osres = Linux_s_accept(callno,realct,curr_pc,0);
       break;

    case 1195 : 
       osres = Linux_s_getsockname(callno,realct,curr_pc,0);
       break;

    case 1196 : 
       osres = Linux_s_getpeername(callno,realct,curr_pc,0);
       break;

    case 1197 : 
       osres = Linux_s_socketpair(callno,realct,curr_pc,0);
       break;

    case 1198 : 
       osres = Linux_s_send(callno,realct,curr_pc,0);
       break;

    case 1199 : 
       osres = Linux_s_sendto(callno,realct,curr_pc,0);
       break;

    case 1200 : 
       osres = Linux_s_recv(callno,realct,curr_pc,0);
       break;

    case 1201 : 
       osres = Linux_s_recvfrom(callno,realct,curr_pc,0);
       break;

    case 1202 : 
       osres = Linux_s_shutdown(callno,realct,curr_pc,0);
       break;

    case 1203 : 
       osres = Linux_s_setsockopt(callno,realct,curr_pc,0);
       break;

    case 1204 : 
       osres = Linux_s_getsockopt(callno,realct,curr_pc,0);
       break;

    case 1205 : 
       osres = Linux_s_sendmsg(callno,realct,curr_pc,0);
       break;

    case 1206 : 
       osres = Linux_s_recvmsg(callno,realct,curr_pc,0);
       break;

    case 1207 : 
       osres = Linux_s_pivot_root(callno,realct,curr_pc,0);
       break;

    case 1208 : 
       osres = Linux_s_mincore(callno,realct,curr_pc,0);
       break;

    case 1209 : 
       osres = Linux_s_madvise(callno,realct,curr_pc,0);
       break;

    case 1210 : 
       osres = Linux_s_newstat(callno,realct,curr_pc,0);
       break;

    case 1211 : 
       osres = Linux_s_newlstat(callno,realct,curr_pc,0);
       break;

    case 1212 : 
       osres = Linux_s_newfstat(callno,realct,curr_pc,0);
       break;

    case 1213 : 
       osres = Linux_s_clone2(callno,realct,curr_pc,0);
       break;

    case 1214 : 
       osres = Linux_s_getdents64(callno,realct,curr_pc,0);
       break;

    case 1215 : 
       osres = Linux_s_getunwind(callno,realct,curr_pc,0);
       break;

    case 1216 : 
       osres = Linux_s_readahead(callno,realct,curr_pc,0);
       break;

    case 1217 : 
       osres = Linux_s_setxattr(callno,realct,curr_pc,0);
       break;

    case 1218 : 
       osres = Linux_s_lsetxattr(callno,realct,curr_pc,0);
       break;

    case 1219 : 
       osres = Linux_s_fsetxattr(callno,realct,curr_pc,0);
       break;

    case 1220 : 
       osres = Linux_s_getxattr(callno,realct,curr_pc,0);
       break;

    case 1221 : 
       osres = Linux_s_lgetxattr(callno,realct,curr_pc,0);
       break;

    case 1222 : 
       osres = Linux_s_fgetxattr(callno,realct,curr_pc,0);
       break;

    case 1223 : 
       osres = Linux_s_listxattr(callno,realct,curr_pc,0);
       break;

    case 1224 : 
       osres = Linux_s_llistxattr(callno,realct,curr_pc,0);
       break;

    case 1225 : 
       osres = Linux_s_flistxattr(callno,realct,curr_pc,0);
       break;

    case 1226 : 
       osres = Linux_s_removexattr(callno,realct,curr_pc,0);
       break;

    case 1227 : 
       osres = Linux_s_lremovexattr(callno,realct,curr_pc,0);
       break;

    case 1228 : 
       osres = Linux_s_fremovexattr(callno,realct,curr_pc,0);
       break;

    case 1229 : 
       osres = Linux_s_tkill(callno,realct,curr_pc,0);
       break;

    case 1230 : 
       osres = Linux_s_futex(callno,realct,curr_pc,0);
       break;

    case 1231 : 
       osres = Linux_s_sched_setaffinity(callno,realct,curr_pc,0);
       break;

    case 1232 : 
       osres = Linux_s_sched_getaffinity(callno,realct,curr_pc,0);
       break;

    case 1233 : 
       osres = Linux_s_set_tid_address(callno,realct,curr_pc,0);
       break;

    case 1234 : 
       osres = Linux_s_fadvise64_64(callno,realct,curr_pc,0);
       break;

    case 1235 : 
       osres = Linux_s_tgkill(callno,realct,curr_pc,0);
       break;

    case 1236 : 
       osres = Linux_s_exit_group(callno,realct,curr_pc,0);
       break;

    case 1237 : 
       osres = Linux_s_lookup_dcookie(callno,realct,curr_pc,0);
       break;

    case 1238 : 
       osres = Linux_s_io_setup(callno,realct,curr_pc,0);
       break;

    case 1239 : 
       osres = Linux_s_io_destroy(callno,realct,curr_pc,0);
       break;

    case 1240 : 
       osres = Linux_s_io_getevents(callno,realct,curr_pc,0);
       break;

    case 1241 : 
       osres = Linux_s_io_submit(callno,realct,curr_pc,0);
       break;

    case 1242 : 
       osres = Linux_s_io_cancel(callno,realct,curr_pc,0);
       break;

    case 1243 : 
       osres = Linux_s_epoll_create(callno,realct,curr_pc,0);
       break;

    case 1244 : 
       osres = Linux_s_epoll_ctl(callno,realct,curr_pc,0);
       break;

    case 1245 : 
       osres = Linux_s_epoll_wait(callno,realct,curr_pc,0);
       break;

    case 1246 : 
       osres = Linux_s_restart_syscall(callno,realct,curr_pc,0);
       break;

    case 1247 : 
       osres = Linux_s_semtimedop(callno,realct,curr_pc,0);
       break;

    case 1248 : 
       osres = Linux_s_timer_create(callno,realct,curr_pc,0);
       break;

    case 1249 : 
       osres = Linux_s_timer_settime(callno,realct,curr_pc,0);
       break;

    case 1250 : 
       osres = Linux_s_timer_gettime(callno,realct,curr_pc,0);
       break;

    case 1251 : 
       osres = Linux_s_timer_getoverrun(callno,realct,curr_pc,0);
       break;

    case 1252 : 
       osres = Linux_s_timer_delete(callno,realct,curr_pc,0);
       break;

    case 1253 : 
       osres = Linux_s_clock_settime(callno,realct,curr_pc,0);
       break;

    case 1254 : 
       osres = Linux_s_clock_gettime(callno,realct,curr_pc,0);
       break;

    case 1255 : 
       osres = Linux_s_clock_getres(callno,realct,curr_pc,0);
       break;

    case 1256 : 
       osres = Linux_s_clock_nanosleep(callno,realct,curr_pc,0);
       break;

    case 1257 : 
       osres = Linux_s_fstatfs64(callno,realct,curr_pc,0);
       break;

    case 1262 : 
       osres = Linux_s_mq_open(callno,realct,curr_pc,0);
       break;

    case 1263 : 
       osres = Linux_s_mq_unlink(callno,realct,curr_pc,0);
       break;

    case 1264 : 
       osres = Linux_s_mq_timedsend(callno,realct,curr_pc,0);
       break;

    case 1265 : 
       osres = Linux_s_mq_timedreceive(callno,realct,curr_pc,0);
       break;

    case 1266 : 
       osres = Linux_s_mq_notify(callno,realct,curr_pc,0);
       break;

    case 1267 : 
       osres = Linux_s_mq_getsetattr(callno,realct,curr_pc,0);
       break;

    case 1268 : 
       osres = Linux_s_kexec_load(callno,realct,curr_pc,0);
       break;

    default:
     realct->r[10].val = -1;
     realct->r[10].nat = 0;
     realct->r[8].val = (uint64_t)OSDefs::oENOSYS;	
     realct->r[8].nat = 0;
     break;
  }

  if (OS_ict_osinfo(realct)->state == ProcBlocked &&
      OS_ict_osinfo(realct)->sigPendingFlag) { 
    // we blocked with a signal pending!
    OS_ict_osinfo(realct)->state = ProcRunnable;
    OS_ict_osinfo(realct)->os->ready_list.push_back(OS_ict_osinfo(realct));
    OS_ict_osinfo(realct)->os->wantScheduling = true; 
  } else CC_do_a_signal(realct, r15val != 1181, curr_pc, following_pc);

  //if (OS_ict_osinfo(realct)->state != ProcBlocked)
  // OS_ict_osinfo(realct)->trapno = 0;

  if (OS_ict_osinfo(realct)->os->wantScheduling)
    Linux_schedule(&CC_emu_interface(realct));    

  following_pc = realct->startaddr;

  return 0; 
}

} // namespace LSE_IA64
