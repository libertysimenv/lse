/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Disassembler and decode information dumper for IA64
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 */
#include "LSE_IA64.h"
using namespace LSE_IA64;
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

static char *pname;
#include "LSE_emu_alonesupp.c" /* yes, the C file */

static void usage() {
  fprintf(stderr,
          "\n%s <options> <binary> [<startaddr>]\n",
          pname);
  fprintf(stderr,"\t--decode\t\tDump decode information #\n");
  exit(1);
}

static char *cleanenvp[]={0};

int dodecode = 0;
int running;

int main(int argc, char *argv[], char **envp)
{
  int rval;
  int i;
  char *rargs[2];
  int rcnt = 0;

  pname = argv[0];
  LSE_sim_exit_status = 0;
  EMU_init(&LSE_emu_interface);

  for (i=1;i<argc;i++) {
    if (!strcmp(argv[i],"--help")) usage();
    else if (!strcmp(argv[i],"-help")) usage();
    else if (!strncmp(argv[i],"--decode",8)) dodecode = 1;
    else if (rcnt < 2) rargs[rcnt++] = argv[i];
    else {
      fprintf(stderr,"Error: unrecognized argument\n");
      usage();
    }
  }
  if (rcnt < 1) {
    fprintf(stderr,"Error: Missing argument\n");
    usage();
  }

  LSE_emu_standalone_init();

  LSE_emu_create_context(1);

  rval = EMU_context_load(&LSE_emu_interface, 1, 1, rargs, cleanenvp);

  if (rval) {
    fprintf(stderr,"Error while loading program\n");
    exit(1);
  }

  {
    LSE_emu_addr_t addr;
    LSE_emu_instr_info_t ii;
    LSE_emu_ctoken_t ctx = LSE_emu_hwcontexts_table[1].ctok;

    addr = EMU_get_start_addr(ctx);
    ii.hwcontextno = 1;
    ii.swcontexttok= ctx;

    while (1) {
      EMU_init_instr(&ii,1,ctx,addr);      
      EMU_do_step(&ii,(LSE_emu_instrstep_name_t)0, FALSE);  /* ifetch */
      /* presumably fell off end.... */
      if (ii.privatef.actual_intr || ii.privatef.potential_intr) break;
      
      EMU_disassemble_addr(ctx,addr,stdout);
	
      if (dodecode) {
	static const char *spacenames[]={"LSE","GR","FR","PR","BR","AR",
					 "SR","RSE","mem"};
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)1, FALSE);  /* decode */

	for (i=0;i<LSE_emu_max_operand_src;i++) {
	  if (ii.operand_src[i].spaceid || ii.operand_src[i].spaceaddr.LSE) {
	    fprintf(stdout,"  s %d  %s  %d %016"PRIx64" %016"PRIx64"\n",i,
		    spacenames[ii.operand_src[i].spaceid],
		    ii.operand_src[i].spaceaddr.LSE,
		    ii.operand_src[i].uses.reg.bits[0],
		    ii.operand_src[i].uses.reg.bits[1]);
	  }
	}
	for (i=0;i<LSE_emu_max_operand_dest;i++) {
	  if (ii.operand_dest[i].spaceid || ii.operand_dest[i].spaceaddr.LSE) {
	    fprintf(stdout,"  d %d  %s  %d %016"PRIx64" %016"PRIx64"\n",i,
		    spacenames[ii.operand_dest[i].spaceid],
		    ii.operand_dest[i].spaceaddr.LSE,
		    ii.operand_dest[i].uses.reg.bits[0],
		    ii.operand_dest[i].uses.reg.bits[1]);
	  }
	}
      }

      addr = addr + 1;
      if ((addr & 3) == 3) addr += 13;

    } /* while 1 */
  }

  EMU_finish(&LSE_emu_interface);
  LSE_emu_standalone_finalize();
  return LSE_sim_exit_status;
}

