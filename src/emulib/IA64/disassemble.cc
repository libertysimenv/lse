/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Disassembly function for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file contains functions for printing out good stuff.  These include
 * disassembly, interruption names, and state dumping.
 *
 */
#include <stdio.h>
#include "LSE_IA64.h"
#include "IA64_fp.h"

namespace LSE_IA64 {
static const char *arnames[]={
  ".kr0", ".kr1", ".kr2", ".kr3", ".kr4", ".kr5", ".kr6", ".kr7",
  "8", "9", "10", "11", "12", "13", "14", "15",
  ".rsc", ".bsp", ".bspstore", ".rnat", "20", ".fcr", "22", "23",
  ".eflag", ".csd", ".ssd", ".cflg",".fsr",".fir","30","31",
  ".ccv","33","34","35",".unat","37","38","39",
  ".fpsr","41","42","43",".itc","45","46","47",
  "48","49","50","51","52","53","54","55",
  "56","57","58","59","60","61","62","63",
  ".pfs",".lc",".ec"
};

static const char *Table_4_26[] = { ".sptk", "", ".dptk", "" };
static const char *Table_4_38[] = { "", ".nt1", "", ".nta" };
static const char *Table_4_39[] = { "", "", "", ".nta" };
static const char *Table_4_40[] = { "", ".nt1", ".nt2", ".nta" };
static const char *Table_4_49[] = { ".few", ".many" };
static const char *Table_4_50[] = { ".sptk", ".spnt", ".dptk", ".dpnt" };
static const char *Table_4_51[] = { "", ".sptk", "", ".spnt", 
			      "", ".dptk", "", ".dpnt" };
static const char *Table_4_52[] = { "", ".clr" };
static const char *Table_4_54[] = { "", ".imp" };
static const char *Table_4_55[] = { ".sptk",".loop",".dptk",".exit" };
static const char *Table_4_56[] = { ".sptk","",".dptk","" };
static const char *Table_4_61[] = { ".s0", ".s1", ".s2", ".s3" };

static const char *btype_table[] = {
  ".cond", ".ia", ".wexit", ".wtop", 
  ".ret", ".cloop", ".cexit", ".ctop"
};
static inline void print_pred(FILE *outfile, int pr) {
  if (pr) fprintf(outfile,"(p%02d) ",pr);
  else fprintf(outfile,"      ");
}

static void do_disassem(unsigned char *, LSE_emu_addr_t, FILE *);

void EMU_disassemble_addr(LSE_emu_ctoken_t ctx, LSE_emu_addr_t addr, 
			  FILE *outfile) {
  IA64_context_t *realct = (IA64_context_t *)ctx;
  unsigned char instrbuf[16]; /* instruction buffer */
  uint64_t addr0=addr & ~0xf;

  try {
    realct->mem->read(addr0, (unsigned char *)&instrbuf[0], 16);
  } catch (LSE_device::deverror_t e) {
    fprintf(outfile,"0x%016" PRIx64 "/%d -> %s\n",addr0,(int)(addr&0xf),
	    e.errstring());
    return;
  }
  do_disassem(instrbuf, addr, outfile);
}

static const char *btype_name="IMFBXLRES";

void EMU_disassemble_instr(LSE_emu_instr_info_t *ii, FILE *outfile) {
  if (ii->extra.formatno > 600) {
    int btemplate;
    btemplate = ii->extra.bundle.bytes[0] & 0x1f;
    
    fprintf(outfile, "%016" PRIx64 "/%d: %02x [%c%c%c] %011" PRIx64 " ",
	    (ii->addr & ~UINT64_C(0xf)),(int)(ii->addr&0x3),
	    btemplate,
	    btype_name[bundle_types[btemplate][0]],
	    btype_name[bundle_types[btemplate][1]],
	    btype_name[bundle_types[btemplate][2]],
	    ii->extra.instr);
    (*(decode_details[ii->privatef.formatint].funcs.
       disassemble_func))(ii,outfile);
  } else {
    do_disassem(ii->extra.bundle.bytes, ii->addr, outfile);
  }
}

void do_disassem(unsigned char *instrbuf, LSE_emu_addr_t addr, FILE *outfile) {
  
  uint64_t addr0=addr & ~0xf;
  uint64_t instr,instrcont=0;
  int imm22, imm14, imm8;
  uint64_t imm64;
  int btemplate, unit, majorop;
  IA64_decode_t *dt;
  int format, stopafter;

  btemplate = instrbuf[0] & 0x1f;
  fprintf(outfile,"%016" PRIx64 "/%d: %02x [%c%c%c] ",
	  addr0,(int)(addr&0x3),btemplate,
	  btype_name[bundle_types[btemplate][0]],
	  btype_name[bundle_types[btemplate][1]],
	  btype_name[bundle_types[btemplate][2]]);
  unit = bundle_types[btemplate][addr&0x3];
  stopafter = bundle_stops[btemplate][addr&0x3];

  switch (addr & 0x3) {
  case 0:
    instr = (instrbuf[0]>>5) | 
      ((uint64_t)instrbuf[1]<<3) | 
      ((uint64_t)instrbuf[2]<<11) |
      ((uint64_t)instrbuf[3] << 19) | 
      ((uint64_t)instrbuf[4]<<27) | 
      ((uint64_t)(instrbuf[5]&0x3f)<<35);
    break;
  case 1:
    instr = (instrbuf[5]>>6) | 
      ((uint64_t)instrbuf[6]<<2) | 
      ((uint64_t)instrbuf[7]<<10) |
      ((uint64_t)instrbuf[8] << 18) | 
      ((uint64_t)instrbuf[9]<<26) | 
      ((uint64_t)(instrbuf[10]&0x7f)<<34);
    break;
  case 2 :
    instr = (instrbuf[10]>>7) | 
      ((uint64_t)instrbuf[11]<<1) | 
      ((uint64_t)instrbuf[12]<<9) |
      ((uint64_t)instrbuf[13]<<17) | 
      ((uint64_t)instrbuf[14]<<25) | 
      ((uint64_t)instrbuf[15]<<33);
    if (unit==4) {
      instrcont = (instrbuf[5]>>6) | 
      ((uint64_t)instrbuf[6]<<2) | 
      ((uint64_t)instrbuf[7]<<10) |
      ((uint64_t)instrbuf[8] << 18) | 
      ((uint64_t)instrbuf[9]<<26) | 
      ((uint64_t)(instrbuf[10]&0x7f)<<34);
    }
    break;
  default:
    instr = 0;
  }
  fprintf(outfile,"%011" PRIx64 " ",instr);
  if (unit==5) {
    print_pred(outfile,0);
    fprintf(outfile,"<L-format instruction>\n");
    return;
  }
  majorop = (instr>>37) & 0xf;

  if (unit > 4) {
    fprintf(outfile,"<bad bundle header>\n");
    return;
  }

  /* Now, find the instruction format */
  format = 0;
  dt = decode_tables[unit];
  while (format==0 && dt != NULL) {
    int ino=(instr>>dt->shift)&dt->mask;
    format = dt->info[ino].formatno;
    dt = dt->info[ino].nextlevelp;
  }

  switch (format) {

  case -1: 
    print_pred(outfile,0);
    goto reserved;
  case -2: 
    print_pred(outfile,all_qp(instr));
    goto reservedpred;
  case -3: 
    print_pred(outfile,all_qp(instr));
    goto reservedpredb;

  case 1: /* I1 multimedia multiply and shift */
    {
      static int counts[]={0,7,15,16};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"pmpyshr2%s r%d=r%d,r%d,%d",
	      (I1_x2b(instr)&2) ? "" : ".u",
	      all_r1(instr), all_r2(instr), all_r3(instr),
	      counts[I1_ct2d(instr)]);
    }
    break;

  case 2: /* I2 multimedia multiply/mix/pack/unpack */
    {
      static const char *i2op[] = {
	"pack","unpack","mix","",
	"pmin","pmax","","pmpy",
	"pack","unpack","mix","",
	"pmin","pmax","psad","pmpy"
      };
      static const char *i2ext[] = {
	".uss",".h",".r","",
	".u",".u","",".r",
	".sss",".l",".l","",
	"","","",".l"
      };
      int opno = (I2_x2b(instr)<<2)|I2_x2c(instr);

      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s%d%s r%d=r%d,r%d",
	      i2op[opno], 1<<(int)((I2_za(instr)<<1)|I2_zb(instr)),
	      i2ext[opno],
	      all_r1(instr), all_r2(instr), all_r3(instr));
    }
    break;

  case 3: /* I3 multimedia mux1 */
    {
      static const char *i3mbtypes[] = { 
	"@brcst","illegal1","illegal2","illegal3",
	"illegal4","illegal5","illegal6","illegal7",
	"@mix","@shuf","@alt", "@rev",
	"illegalC","illegalD","illegalE","illegalF"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"mux1 r%d=r%d,%s",
	      all_r1(instr), all_r2(instr),
	      i3mbtypes[I3_mbt4c(instr)]);
    }
    break;

  case 4: /* I4 multimedia mux2 */
    {
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"mux2 r%d=r%d,0x%02x",
	      all_r1(instr), all_r2(instr), (int)I4_mht8c(instr));
    }
    break;

  case 5: /* I5 shift right-variable */
    {
      static const char *i5ops[] = { "","pshr2","pshr4","shr"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s%s r%d=r%d,r%d",
	      i5ops[(I5_za(instr)<<1) +I5_zb(instr)],
	      I5_x2b(instr) ? "" : ".u",
	      all_r1(instr), all_r3(instr),
	      all_r2(instr));
    }
    break;
    
  case 6: /* I6 multimedia shift right-fixed */
    {
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"pshr%d%s r%d=r%d,%d",
	      I6_zb(instr) ? 2 : 4,
	      (I6_x2b(instr)&2) ? "" : ".u",
	      all_r1(instr), all_r3(instr),
	      (int)I6_count5b(instr));
    }
    break;
    
  case 7: /* I7 shift left-variable */
    {
      static const char *i5ops[] = { "","pshl2","pshl4","shl"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s r%d=r%d,r%d",
	      i5ops[(I7_za(instr)<<1) +I7_zb(instr)],
	      all_r1(instr), all_r2(instr),
	      all_r3(instr));
    }
    break;

  case 8: /* I8 multimedia shift left-fixed */
    {
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"pshl%d r%d=r%d,%d",
	      I8_zb(instr) ? 2 : 4,
	      all_r1(instr), all_r2(instr),
	      31-(int)I8_ccount5c(instr));
    }
    break;

  case 9: /* I9 population count */
    {
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"popcnt r%d=r%d",
	      all_r1(instr), all_r3(instr));
    }
    break;

  case 10: /* I10 shift right pair */
    {
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"shrp r%d=r%d,r%d,%d",
	      all_r1(instr), all_r2(instr),
	      all_r3(instr), (int)I10_count6d(instr));
    }
    break;

  case 11: /* I11 extract (also shr by immediate) */
    {
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"extr%s r%d=r%d,%d,%d",
	      I11_y(instr) ? "":".u",
	      all_r1(instr), all_r3(instr),
	      (int)(I11_pos6b(instr)), (int)(I11_len6d(instr)+1));
    }
    break;
    
  case 12: /* I12 dep.z */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"dep.z r%d=r%d,%d,%d",
	    all_r1(instr), all_r2(instr),
	    (int)(63-I12_cpos6c(instr)),
	    (int)(I12_len6d(instr)+1));
    break;
    
  case 13: /* I13 dep.z immediate */
    {
      imm8 = I27_imm7b(instr);
      if (I27_s(instr)) imm8 |= (~0x7f);
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"dep.z r%d=%d,%d,%d",
	      all_r1(instr), imm8,
	      (int)(63-I13_cpos6c(instr)),
	      (int)(I13_len6d(instr)+1));
    }
    break;

  case 14: /* I14 - deposit immediate */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"dep r%d=%d,r%d,%d,%d",
	    all_r1(instr), 
	    I14_s(instr) ? -1 : 0,
	    all_r3(instr),
	    (int)(63-I14_cpos6b(instr)),
	    (int)(I14_len6d(instr)+1));
    break;
    
  case 15: /* I15 - deposit */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"dep r%d=r%d,r%d,%d,%d",
	    all_r1(instr), 
	    all_r2(instr),
	    all_r3(instr),
	    (int)(63-I15_cpos6d(instr)),
	    (int)(I15_len4d(instr)+1));
    break;
    
  case 16: /* I16 test bit */
    {
      static const char *i16ops[]={
	".z",".z.unc",".z.and",".nz.and",".z.or",".nz.or",".z.or.andcm",
	".nz.or.andcm"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"tbit%s p%d,p%d=r%d,%d",
	      i16ops[I16_ta(instr)*4 + I16_tb(instr)*2+I16_c(instr)],
	      I16_p1(instr), I16_p2(instr), all_r3(instr),
	      (int)(I16_pos6b(instr)));
    }
    break;
    
  case 17: /* I17 test NaT */
    {
      static const char *i17ops[]={
	".z",".z.unc",".z.and",".nz.and",".z.or",".nz.or",".z.or.andcm",
	".nz.or.andcm"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"tnat%s p%d,p%d=r%d",
	      i17ops[I17_ta(instr)*4 + I17_tb(instr)*2+I17_c(instr)],
	      I17_p1(instr), I17_p2(instr), all_r3(instr));
    }
    break;

  case 18: /* I18 nop/hint */
    print_pred(outfile,all_qp(instr));
    imm22 = I18_imm20a(instr) | (I18_i(instr) << 20); /* really imm21 */
    fprintf(outfile,"%s.i 0x%x",
	    I18_y(instr) ? "hint":"nop", imm22);
    break;

  case 19: /* I19 break */
    print_pred(outfile,all_qp(instr));
    imm22 = I19_imm20a(instr) | (I19_i(instr) << 20); /* really imm21 */
    fprintf(outfile,"break.i 0x%x",imm22);
    break;

  case 20: /* I20 integer speculation check (i-unit) */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)I20_imm7a(instr);
    imm64 |= ((uint64_t)I20_imm13c(instr))<<7;
    if (I20_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"chk.s.i r%d,%" PRIx64,
	    all_r2(instr),
	    imm64+(addr & ~INT64_C(3)));
    break;

  case 21: /* I21 - mov to br */
    imm64 = I21_timm9c(instr) & 0xFF;
    if (imm64 & 0x100) imm64 |= ~INT64_C(0xFF);
    imm64 <<= 4;
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"mov%s%s%s b%d=r%d,0x%" PRIx64,
	    (I21_x(instr) ? ".ret" : ""),
	    Table_4_26[I21_wh(instr)],
	    Table_4_54[I21_ih(instr)],
	    I21_b1(instr), all_r2(instr),
	    (imm64+(addr & ~0x3))&INT64_C(0x1FFF));
    break;

  case 22: /* I22 - mov from br */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"mov r%d=b%d",all_r1(instr), I22_b2(instr));
    break;

  case 23: /* I23 - mov to predicates - register */
    print_pred(outfile,all_qp(instr));
    imm64 = I23_mask7a(instr);
    imm64 |= (I23_mask8c(instr)<<7);
    if (I23_s(instr)) imm64 |= ~INT64_C(0x7FFF);
    imm64 <<= 1;
    fprintf(outfile,"mov pr=r%d,0x%" PRIx64,all_r2(instr),imm64);
    break;

  case 24: /* I24 - mov to predicates - immediate */
    print_pred(outfile,all_qp(instr));
    imm64 = I24_imm27a(instr);
    if (I24_s(instr)) imm64 |= ~INT64_C(0x7FFFFFF);
    imm64 <<= 16;
    fprintf(outfile,"mov pr.rot=0x%" PRIx64,imm64);
    break;

  case 25: /* I25 - mov from pr/ip */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"mov r%d=%s",
	    all_r1(instr), I25_x6(instr)==0x30 ? "ip" : "pr");
    break;

  case 26: /* I26 - mov to AR */
    print_pred(outfile,all_qp(instr));
    if (I26_ar3(instr)>66) 
      fprintf(outfile,"mov.m ar%d=r%d",I26_ar3(instr),
	      all_r2(instr));
    else
      fprintf(outfile,"mov.i ar%s=r%d",
	      arnames[I26_ar3(instr)],
	      all_r2(instr));
    break;

  case 27: /* I27 - mov to AR - immediate */
    print_pred(outfile,all_qp(instr));
    imm8 = I27_imm7b(instr);
    if (I27_s(instr)) imm8 |= (~0x7f);
    if (I27_ar3(instr)>66) 
      fprintf(outfile,"mov.m ar%d=%d",I27_ar3(instr),imm8);
    else
      fprintf(outfile,"mov.i ar%s=%d",
	      arnames[I27_ar3(instr)],imm8);
    break;

  case 28: /* I28 - mov from AR (I-unit) */
    print_pred(outfile,all_qp(instr));
    if (I28_ar3(instr)>66)
      fprintf(outfile,"mov.i r%d=ar%d",
	      all_r1(instr),I28_ar3(instr));
    else fprintf(outfile,"mov.i r%d=ar%s",
		 all_r1(instr),arnames[I28_ar3(instr)]);
    break;

  case 29: /* I29 - sign/zero extensions */
    {
      static const char *i29_ops[]={
	"zxt1","zxt2","zxt4","",
	"sxt1","sxt2","sxt4","",
	"czx1.l","czx2.l","","",
	"czx1.r","czx2.r","",""
      };
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s r%d=r%d",
	      i29_ops[I29_x6(instr) & 0xf],
	      all_r1(instr), all_r3(instr));
    }
    break;


  case 101: /* M1 - integer load */
  case 102: /* M2 - integer load/increment by register */
  case 103: /* M3 - integer load/increment by immediate */
    print_pred(outfile,all_qp(instr));
    {
      int comple = M3_x6(instr)>>2;
      static const char *completers[] = {
	"",".s",".a",".sa",".bias",".acq",".fill","",
	".c.clr",".c.nc",".c.clr.acq"
      };
      static const char *loadhint[] = {
	"",".nt1","",".nta"
      };
      fprintf(outfile,"ld%d%s%s r%d=[r%d]",(1<<(M3_x6(instr)&3)),
	      completers[comple],loadhint[M3_hint(instr)],
	      all_r1(instr), all_r3(instr));
      if (format==102) fprintf(outfile,",r%d",all_r2(instr));
      else if (format != 101) {
	int imm9;
	imm9 = M3_imm7b(instr);
	imm9 |= (M3_i(instr) << 7);
	if (M3_s(instr)) imm9 |= ~(0xff);
	fprintf(outfile,",%d",imm9);
      }
    }
    break;

  case 104: /* M4 - integer store */
  case 105: /* M5 - integer store - increment by immediate */
    print_pred(outfile,all_qp(instr));
    {
      int comple = (M4_x6(instr)>>2) & 3;
      static const char *completers[] = {
	"",".rel",".spill",""
      };
      static const char *storehint[] = {
	"","","",".nta"
      };
      fprintf(outfile,"st%d%s%s [r%d]=r%d",(1<<(M4_x6(instr)&3)),
	      completers[comple],storehint[M4_hint(instr)],
	      all_r3(instr), all_r2(instr));
      if (format==105) {
	int imm9;
	imm9 = M5_imm7a(instr);
	imm9 |= (M5_i(instr) << 7);
	if (M5_s(instr)) imm9 |= ~(0xff);
	fprintf(outfile,",%d",imm9);
      }
    }
    break;

  case 106: /* M6 - FP load */
  case 107: /* M7 - FP load/increment by register */
  case 108: /* M8 - FP load/increment by immediate */
    print_pred(outfile,all_qp(instr));
    {
      int comple = M6_x6(instr)>>2;
      static const char *completers[] = {
	"",".s",".a",".sa","","","","",
	".c.clr",".c.nc",""
      };
      static const char *loadhint[] = {
	"",".nt1","",".nta"
      };
      static const char *sizes[] = { 
	"e", "8", "s", "d"
      };
      fprintf(outfile,"ldf%s%s%s f%d=[r%d]",
	      (M6_x6(instr) != 0x1b ? sizes[M6_x6(instr)&3] : ".fill"),
	      completers[comple],loadhint[M6_hint(instr)],
	      all_f1(instr), all_r3(instr));
      if (format==107) fprintf(outfile,",r%d",all_r2(instr));
      else if (format == 108) {
	int imm9;
	imm9 = M8_imm7b(instr);
	imm9 |= (M8_i(instr) << 7);
	if (M8_s(instr)) imm9 |= ~(0xff);
	fprintf(outfile,",%d",imm9);
      }
    }
    break;

  case 109: /* M9 - FP Store */
  case 110: /* M10 - FP Store - increment by immediate */
    print_pred(outfile,all_qp(instr));
    {
      static const char *completers[] = {
	"e","8","s","d"
      };
      fprintf(outfile,"stf%s%s [r%d]=f%d",
	      ((M4_x6(instr) < 0x34) ? 
	       completers[M4_x6(instr)&3] : ".spill"),
	      Table_4_39[M9_hint(instr)],
	      all_r3(instr), all_f2(instr));
      if (format==110) {
	int imm9;
	imm9 = M10_imm7a(instr);
	imm9 |= (M10_i(instr) << 7);
	if (M10_s(instr)) imm9 |= ~(0xff);
	fprintf(outfile,",%d",imm9);
      }
    }
    break;

  case 111: /* M11 - FP load pair */
    {
      static const char *sizes[]={"","8","s","d"};
      static const char *completers[] = {
	"",".s",".a",".sa","","","","",
	".c.clr",".c.nc","" };
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"ldfp%s%s%s f%d,f%d=[r%d]",
	      sizes[M12_x6(instr) & 3],
	      completers[M6_x6(instr)>>2],
	      Table_4_38[M12_hint(instr)],
	      all_f1(instr),all_f2(instr),
	      all_r3(instr));
    }
    break;

  case 112: /* M12 - FP load pair - increment by immediate */
    {
      static const char *sizes[]={"","8","s","d"};
      static const char *completers[] = {
	"",".s",".a",".sa","","","","",
	".c.clr",".c.nc","" };
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"ldfp%s%s%s f%d,f%d=[r%d],%d",
	      sizes[M12_x6(instr) & 3],
	      completers[M6_x6(instr)>>2],
	      Table_4_38[M12_hint(instr)],
	      all_f1(instr),all_f2(instr),
	      all_r3(instr), (M12_x6(instr) & 1) ? 16 : 8);
    }
    break;

  case 113: /* M13 - line prefetch */
  case 114: /* M14 - line prefetch/increment by register */
  case 115: /* M15 - line prefetch/increment by immediate */
    print_pred(outfile,all_qp(instr));
    {
      int comple = M13_x6(instr)&3;
      static const char *completers[] = {
	"",".excl",".fault",".fault.excl" 
      };
      fprintf(outfile,"lfetch%s%s [r%d]",
	      completers[comple],Table_4_40[M13_hint(instr)],
	      all_r3(instr));
      if (format==114) fprintf(outfile,",r%d",all_r2(instr));
      else if (format == 115) {
	int imm9;
	imm9 = M15_imm7b(instr);
	imm9 |= (M15_i(instr) << 7);
	if (M15_s(instr)) imm9 |= ~(0xff);
	fprintf(outfile,",%d",imm9);
      }
    }
    break;

  case 116: /* M16 - Exchange/Compare and Exchange */
    print_pred(outfile,all_qp(instr));
    {
      int comple = M16_x6(instr);
      if (comple>7) {
	fprintf(outfile,"xchg%d%s r%d=[r%d],r%d",
		1<<(comple&3),Table_4_38[M16_hint(instr)],
		all_r1(instr), all_r3(instr), all_r2(instr));
      } else {
	fprintf(outfile,"cmpxchg%d%s%s r%d=[r%d],r%d,ar.ccv",
		1<<(comple&3), (comple&4)? ".rel" : ".acq",
		Table_4_38[M16_hint(instr)],
		all_r1(instr), all_r3(instr), all_r2(instr));
      }
    }
    break;

  case 117: /* M17 - Exchange/Compare and Exchange */
    print_pred(outfile,all_qp(instr));
    {
      int comple = M17_x6(instr);
      int inc3;
      static int incs[]={16,8,4,1};
      inc3 = (M17_s(instr)? -1 : 1) * incs[M17_i2b(instr)];
      fprintf(outfile,"fetchadd%d%s%s r%d=[r%d],%d",
	      1<<(comple&3), (comple&4)? ".rel" : ".acq",
	      Table_4_38[M16_hint(instr)],
	      all_r1(instr), all_r3(instr), inc3);
    }
    break;

  case 118: /* M18 - Set FR */
    {
      static const char *m18_ops[]={"setf.sig","setf.exp","setf.s","setf.d"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s f%d=r%d",
	      m18_ops[M18_x6(instr) & 3],
	      all_f1(instr),all_r2(instr));
    }
    break;

  case 119: /* M19 - Get FR */
    {
      static const char *m19_ops[]={"getf.sig","getf.exp","getf.s","getf.d"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s r%d=f%d",
	      m19_ops[M19_x6(instr) & 3],
	      all_r1(instr),all_f2(instr));
    }
    break;

  case 120: /* M20 - Integer speculation check (M-unit) */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)M20_imm7a(instr);
    imm64 |= ((uint64_t)M20_imm13c(instr))<<7;
    if (M20_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"chk.s.m r%d,%" PRIx64,
	    all_r2(instr),
	    imm64+(addr & ~INT64_C(3)));
    break;

  case 121: /* M21 - FP speculation check */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)M21_imm7a(instr);
    imm64 |= ((uint64_t)M21_imm13c(instr))<<7;
    if (M21_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"chk.s f%d,%" PRIx64,
	    all_f2(instr),
	    imm64+(addr & ~INT64_C(3)));
    break;

  case 122: /* M22 - Integer advanced load check */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)M22_imm20b(instr);
    if (M22_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"chk.a.%s r%d,%" PRIx64,
	    ((M22_x3(instr)&1)?"clr":"nc"),
	    all_r1(instr),
	    imm64+(addr & ~INT64_C(3)));
    break;

  case 123: /* M23 - FP advanced load check */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)M23_imm20b(instr);
    if (M23_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"chk.a.%s f%d,%" PRIx64,
	    ((M23_x3(instr)&1)?"clr":"nc"),
	    all_f1(instr),
	    imm64+(addr & ~INT64_C(3)));
    break;

  case 124: /* M24 - Sync/Fence/Serialize/ALAT control */
    {
      static const char *ops_m24[]={"<bad>","invala","fwb","srlz.d",
		       "<bad>","<bad>","<bad>","srlz.i",
		       "<bad>","<bad>","mf","<bad>",
		       "<bad>","<bad>","mf.a","sync.i"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s",ops_m24[M24_x4(instr)<<2 | M24_x2(instr)]);
    }
    break;

  case 125: /* M25 - RSE Control */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"%s",(M25_x4(instr)&0x4) ? "flushrs" : "loadrs");
    break;

  case 126: /* M26 - Integer ALAT Entry Invalidate */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"invala.e r%d",all_r1(instr));
    break;

  case 127: /* M27 - Floating-point ALAT Entry Invalidate */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"invala.e f%d",all_f1(instr));
    break;

  case 128: /* M28 - Flush Cache/Purge Translation Cache Entry */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"%s r%d",
	    (M28_x6(instr)&0x4) ? "ptc.e" : "fc", all_r3(instr));
    break;

  case 129: /* M29 - mov.m to ar */
    print_pred(outfile,all_qp(instr));
    if (M29_ar3(instr)>66) 
      fprintf(outfile,"mov.m ar%d=r%d",M29_ar3(instr),
	      all_r2(instr));
    else
      fprintf(outfile,"mov.m ar%s=r%d",
	      arnames[M29_ar3(instr)],
	      all_r2(instr));
    break;

  case 130: /* M30 - mov.m to ar - immediate */
    print_pred(outfile,all_qp(instr));
    imm8 = M30_imm7b(instr);
    if (M30_s(instr)) imm8 |= (~0x7f);
    if (M30_ar3(instr)>66) 
      fprintf(outfile,"mov.m ar%d=%d",M30_ar3(instr),imm8);
    else
      fprintf(outfile,"mov.m ar%s=%d",arnames[M30_ar3(instr)],imm8);
    break;

  case 131: /* M31 - mov.m from ar */
    print_pred(outfile,all_qp(instr));
    if (M31_ar3(instr)>66)
      fprintf(outfile,"mov.m r%d=ar%d",all_r1(instr),
	      M31_ar3(instr));
    else
      fprintf(outfile,"mov.m r%d=ar%s",all_r1(instr),
	      arnames[M31_ar3(instr)]);
    break;

  case 132: /* M32 - mov to CR */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"mov cr%d=r%d",M32_cr3(instr),all_r2(instr));
    break;

  case 133: /* M33 - mov from CR */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"mov r%d=cr%d",all_r1(instr),M33_cr3(instr));
    break;

  case 134: /* M34 - alloc */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"alloc r%d=ar.pfs,%d,%d,%d,%d",
	    all_r1(instr),
	    (int)M34_sol(instr),0,
	    (int)(M34_sof(instr) - M34_sol(instr)),
	    (int)M34_sor(instr) << 3
	    );
    if (all_qp(instr) != 0) 
      fprintf(outfile," <improper predicate>");
    break;

  case 135: /* M35 - Move to PSR */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"mov psr%s=r%d",
	    (M35_x6(instr)&4) ? ".l" : ".um", all_r2(instr));
    break;
    
  case 136: /* M36 - Move from PSR */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"mov r%d=psr%s",all_r1(instr),
	    (M36_x6(instr)&4) ? "" : ".um");
    break;
    
  case 137: /* M37 - break */
    print_pred(outfile,all_qp(instr));
    imm22 = M37_imm20a(instr) | (M37_i(instr) << 20); /* really imm21 */
    fprintf(outfile,"break.m 0x%x",imm22);
    break;

  case 148: /* M48 - nop/hint */
    print_pred(outfile,all_qp(instr));
    imm22 = M48_imm20a(instr) | (M48_i(instr) << 20); /* really imm21 */
    fprintf(outfile,"%s.m 0x%x",
	    M48_y(instr) ? "hint":"nop", imm22);
    break;

  case 138: /* M38 - Probe Register */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"probe%s r%d=r%d,r%d",
	    (M38_x6(instr)&1) ? ".w" : ".r",
	    all_r1(instr),all_r3(instr),all_r2(instr));
    break;
    
  case 139: /* M39 - Probe Immediate */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"probe%s r%d=r%d,%d",
	    (M39_x6(instr)&1) ? ".w" : ".r",
	    all_r1(instr),all_r3(instr),(int)M39_i2b(instr));
    break;
    
  case 140: /* M40 - Probe Fault Immediate */
    {
      static const char *comple[] = { "", ".rw.fault",".r.fault",".w.fault" };
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"probe%s r%d,%d",comple[M40_x6(instr)&3],
	      all_r3(instr),(int)M40_i2b(instr));
    }
    break;
    
  case 141: /* M41 - Translation Cache Insert */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"itc%s r%d",
	    (M41_x6(instr)&1) ? ".i" : ".d",
	    all_r2(instr));
    break;

  case 142: /* M42 - Move to Indirect Register/Translation Register Insert */
    print_pred(outfile,all_qp(instr));
    if (M42_x6(instr)>5) {
      fprintf(outfile,"itr%s[r%d]=r%d",
	      (M41_x6(instr)&1) ? ".i itr" : ".d dtr",
	      all_r3(instr),all_r2(instr));
    } else {
      static const char *regs[] = { "rr","dbr","ibr","pkr","pmc","pmd" };
      fprintf(outfile,"mov %s[r%d]=r%d",
	      regs[M42_x6(instr)&7],all_r3(instr),all_r2(instr));
    }
    break;

  case 143: /* M43 - Move from Indirect Register */
    print_pred(outfile,all_qp(instr));
    {
      static const char *regs[] = { "rr","dbr","ibr","pkr","pmc","pmd","","cpuid" };
      fprintf(outfile,"mov r%d=%s[r%d]",
	      all_r1(instr),regs[M43_x6(instr)&7],all_r3(instr));
    }
    break;

  case 144: /* M44 - Set/Reset User/System Mask */
    print_pred(outfile,all_qp(instr));
    {
      static const char *name[] = { "sum", "rum", "ssm", "rsm" };
      int imm24 = ( (M44_i(instr)<<23) | (M44_i2d(instr)<<21) | 
		    (M44_imm21a(instr)));
      fprintf(outfile,"%s 0x%x",
	      name[M44_x4(instr)&3],imm24);
    }
    break;

  case 145: /* M45 - Translation Purge */
    print_pred(outfile,all_qp(instr));
    {
      static const char *comple[] = { "","c.l","c.g","c.ga","r.d","r.i"};
      fprintf(outfile,"pt%s r%d,r%d",
	      comple[M45_x6(instr)&7],all_r3(instr),all_r2(instr));
    }
    break;

  case 146: /* M46 - Translation Access */
    print_pred(outfile,all_qp(instr));
    {
      static const char *name[] = { "","","thash","ttag","","","tpa","tak"};
      fprintf(outfile,"%s r%d=r%d",
	      name[M46_x6(instr)&7],all_r1(instr),all_r3(instr));
    }
    break;


  case 201: /* F1 - floating-point multiply-add */
    {
      static const char *f2ops[] = { 
	"fma", "fma.s", "fma.d", "fpma",
	"fms", "fms.s", "fms.d", "fpms",
	"fnma", "fnma.s", "fnma.d", "fpnma"
      };
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s%s f%d=f%d,f%d,f%d",
	      f2ops[((all_majorop(instr)<<1)|F1_x(instr))&0xf],
	      Table_4_61[F1_sf(instr)],
	      all_f1(instr), all_f3(instr),
	      all_f4(instr), all_f2(instr));
    }
    break;

  case 202: /* F2 - fixed-point multiply-add */
    {
      static const char *f2opers[] = { ".l", "", ".hu", ".h" };
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"xma%s f%d=f%d,f%d,f%d",
	      f2opers[F2_x2(instr)],
	      all_f1(instr), all_f3(instr),
	      all_f4(instr), all_f2(instr));
    }
    break;

  case 203: /* F3 - floating-point select */
    {
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"fselect f%d=f%d,f%d,f%d",
	      all_f1(instr), all_f3(instr),
	      all_f4(instr), all_f2(instr));
    }
    break;

  case 204: /* F4 - floating-point compare */
    {
      static const char *f4opers[] = { ".eq", ".lt", ".le", ".unord"};
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"fcmp%s%s%s p%d,p%d=f%d,f%d",
	      f4opers[(F4_ra(instr) << 1) | F4_rb(instr)],
	      F4_ta(instr) ? ".unc" : "",
	      Table_4_61[F4_sf(instr)],
	      F4_p1(instr), F4_p2(instr),
	      all_f2(instr), all_f3(instr));
    }
    break;

  case 205: /* F5 - floating-point class */
    {
      int class9;
      print_pred(outfile,all_qp(instr));
      class9 = (F5_fclass7c(instr) << 2) | F5_fc2(instr);
      fprintf(outfile,"fclass.m%s p%d,p%d=f%d,0x%03x",
	      F5_ta(instr) ? ".unc" : "",
	      F5_p1(instr), F5_p2(instr),
	      all_f2(instr), class9);
    }
    break;

  case 206: /* F6 - floating-point reciprocal approximation */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"%s%s f%d,p%d=f%d,f%d",
	    all_majorop(instr) ? "fprcpa" : "frcpa",
	    Table_4_61[F6_sf(instr)],
	    all_f1(instr), F6_p2(instr),
	    all_f2(instr), all_f3(instr));
    break;

  case 207: /* F7 - floating-point reciprocal sqrt approximation */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"%s%s f%d,p%d=f%d",
	    all_majorop(instr) ? "fprsqrta" : "frsqrta",
	    Table_4_61[F7_sf(instr)],
	    all_f1(instr), F7_p2(instr),all_f3(instr));
    break;

  case 208: /* F8 - minimum/maximum and parallel compare */
    {
      static const char *lowx[] = {"min","max","amin","amax"};
      static const char *highx[] = { "cmp.eq", "cmp.lt", "cmp.le", "cmp.unord",
			       "cmp.neq", "cmp.nlt", "cmp.nle", "cmp.ord" };
      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"%s%s%s f%d=f%d,f%d",
	      all_majorop(instr) ? "fp" : "f",
	      (F8_x6(instr) >= 0x30 ? highx[F8_x6(instr)&7] :
	       lowx[F8_x6(instr)&3]),
	      Table_4_61[F8_sf(instr)],
	      all_f1(instr),all_f2(instr),all_f3(instr));
    }
    break;

  case 209: /* F9 - merge and logical */
    {
      static const char *mergecomp[] = { ".s", ".ns", ".se", "" };
      static const char *fswapcomp[] = { "", ".nl", ".nr", "" };
      static const char *logic[] = { "fand","fandcm","for","fxor" };
      static const char *frest[] = { "", "fmix.lr", "fmix.r", "fmix.l",
			       "fsxt.r", "fsxt.l", "", "" };
      print_pred(outfile,all_qp(instr));
      if (F9_x6(instr) < 0x13) {
	fprintf(outfile,"f%smerge%s",
		all_majorop(instr) ? "p" : "",
		mergecomp[F9_x6(instr) & 3]);
      } else if (F9_x6(instr) == 0x28) {
	fprintf(outfile,"fpack");
      } else if (F9_x6(instr) < 0x30) {
	fprintf(outfile,"%s",logic[F9_x6(instr) & 3]);
      } else if (F9_x6(instr) < 0x38) {
	fprintf(outfile,"fswap%s", fswapcomp[F9_x6(instr) & 0x3]);
      } else {
	fprintf(outfile,"%s", frest[F9_x6(instr) & 7]);
      }
      fprintf(outfile," f%d=f%d,f%d",
	      all_f1(instr), all_f2(instr), all_f3(instr));
    }
    break;

  case 210: /* F10 - convert floating-point to fixed-point */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"f%scvt.fx%s%s%s f%d=f%d",
	    all_majorop(instr)?"p":"",
	    (F10_x6(instr) & 1)?"u":"",
	    (F10_x6(instr) & 2)?".trunc":"",
	    Table_4_61[F10_sf(instr)],
	    all_f1(instr), all_f2(instr));
    break;

  case 211: /* F11 - convert fixed-point to floating-point */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"fcvt.xf f%d=f%d",
	    all_f1(instr), all_f2(instr));
    break;

  case 212: /* F12 - floating-point set controls */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"fsetc%s 0x%02x,0x%02x",
	    Table_4_61[F12_sf(instr)],
	    (int)F12_amask7b(instr),(int)F12_omask7c(instr));
    break;

  case 213: /* F13 - floating-point clear flags */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"fclrf%s",
	    Table_4_61[F13_sf(instr)]);
    break;

  case 214: /* F14 - floating-point check flags */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)F14_imm20a(instr);
    if (F14_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"fchkf%s 0x%" PRIx64,
	    Table_4_61[F13_sf(instr)],
	    imm64 + (addr & ~INT64_C(0x3)));
    break;

  case 215: /* F15 - break */
    print_pred(outfile,all_qp(instr));
    imm22 = F15_imm20a(instr) | (F15_i(instr) << 20); /* really imm21 */
    fprintf(outfile,"break.f 0x%x",imm22);
    break;

  case 216: /* F16 - nop/hint */
    print_pred(outfile,all_qp(instr));
    imm22 = F16_imm20a(instr) | (F16_i(instr) << 20); /* really imm21 */
    fprintf(outfile,"%s.f 0x%x",
	    F16_y(instr) ? "hint":"nop", imm22);
    break;

  case 301: /* B1 - IP-relative branch */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)B1_imm20b(instr);
    if (B1_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"br%s%s%s%s 0x%" PRIx64,
	    btype_table[B1_btype(instr)],
	    Table_4_50[B1_wh(instr)],
	    Table_4_49[B1_p(instr)],
	    Table_4_52[B1_d(instr)],
	    imm64 + (addr & ~INT64_C(0x3)));
    break;

  case 302: /* B2 - IP-relative counted branch */
    print_pred(outfile,0);
    imm64 = (uint64_t)B1_imm20b(instr);
    if (B1_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"br%s%s%s%s 0x%" PRIx64,
	    btype_table[B1_btype(instr)],
	    Table_4_50[B1_wh(instr)],
	    Table_4_49[B1_p(instr)],
	    Table_4_52[B1_d(instr)],
	    imm64 + (addr & ~INT64_C(0x3)));
    break;

  case 303: /* B3 - IP-relative call */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)B3_imm20b(instr);
    if (B3_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
    imm64 <<= 4;
    fprintf(outfile,"br.call%s%s%s b%d=0x%" PRIx64,
	    Table_4_50[B3_wh(instr)],
	    Table_4_49[B3_p(instr)],
	    Table_4_52[B3_d(instr)],
	    B3_b1(instr),imm64 + (addr & ~INT64_C(0x3)));
    break;

  case 304: /* B4 - indirect branch */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"br%s%s%s%s b%d",
	    btype_table[B4_btype(instr)],
	    Table_4_50[B4_wh(instr)],
	    Table_4_49[B4_p(instr)],
	    Table_4_52[B4_d(instr)],
	    B4_b2(instr));
    break;

  case 305: /* B5 - indirect call */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"br.call%s%s%s b%d=b%d",
	    Table_4_51[B4_wh(instr)],
	    Table_4_49[B4_p(instr)],
	    Table_4_52[B4_d(instr)],
	    B5_b1(instr),B5_b2(instr));
    break;

  case 306: /* B6 - IP-relative predict */
    {
      int imm13;
      print_pred(outfile,0);
      imm64 = (uint64_t)B6_imm20b(instr);
      if (B6_s(instr)) imm64 |= ~INT64_C(0xFFFFF);
      imm64 <<= 4;
      imm13 = B6_timm7a(instr) | (B6_t2e(instr)<<7);
      if (B6_t2e(instr)&2) imm13 |= ~0xFF;
      imm13 <<= 4;
      fprintf(outfile,"brp%s%s 0x%" PRIx64 ",0x%x",
	      Table_4_55[B6_wh(instr)],
	      Table_4_54[B6_ih(instr)],
	      imm64+(addr & ~UINT64_C(3)),imm13);
    }
    break;

  case 307: /* B7 - indirect predict */
    {
      int imm13;
      print_pred(outfile,0);
      imm13 = B7_timm7a(instr) | (B7_t2e(instr)<<7);
      if (B7_t2e(instr)&2) imm13 |= ~0xFF;
      imm13 <<= 4;
      fprintf(outfile,"brp%s%s%s b%d,0x%x",
	      (B7_x6(instr)==0x11 ? ".ret" : ""),
	      Table_4_56[B7_wh(instr)],
	      Table_4_54[B7_ih(instr)],
	      B7_b2(instr),
	      imm13);
    }
    break;

  case 308: /* B8 - miscellaneous B-unit */
    {
      static const char *op308[]={
	"epc","","cover","",
	"clrrrb", "clrrrb.pr", "", "",
	"rfi", "", "", "", 
	"bsw.0", "bsw.1", "",""};
      print_pred(outfile,0);
      fprintf(outfile,"%s",
	      op308[B8_x6(instr) & 0xf]);
    }
    break;

  case 309: /* B9 - nop/break */
    print_pred(outfile,all_qp(instr));
    imm22 = B9_imm20a(instr) | (B9_i(instr) << 20); /* really imm21 */
    fprintf(outfile,"%s.b 0x%x",
	    all_majorop(instr) ? "nop":"break", imm22);
    break;

  case 401: /* X1 */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)X1_imm20a(instr);
    imm64 |= (uint64_t)X1_i(instr) << 20;
    imm64 |= (instrcont << 21);
    fprintf(outfile,"break.x 0x%" PRIx64, imm64);
    break;

  case 402: /* X2 */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)X2_imm7b(instr);
    imm64 |= ((uint64_t)X2_imm9d(instr) << 7);
    imm64 |= ((uint64_t)X2_imm5c(instr) << 16);
    imm64 |= ((uint64_t)X2_ic(instr) << 21);
    imm64 |= (instrcont << 22);
    imm64 |= ((uint64_t)X2_i(instr) << 63);
    fprintf(outfile,"movl r%d=0x%" PRIx64,
	    all_r1(instr),imm64);
    break;

  case 403: /* X3 */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)X3_imm20b(instr) << 4;
    imm64 |= ((instrcont & ~UINT64_C(3))<< 22);
    imm64 |= (uint64_t)X3_i(instr) << 63;
    fprintf(outfile,"brl%s%s%s%s 0x%" PRIx64,
	    btype_table[X3_btype(instr)],
	    Table_4_50[X3_wh(instr)],
	    Table_4_49[X3_p(instr)],
	    Table_4_52[X3_d(instr)],
	    imm64 + (addr & ~UINT64_C(0x3)));
    break;

  case 404: /* X4 */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)X4_imm20b(instr) << 4;
    imm64 |= ((instrcont & ~UINT64_C(3))<< 24);
    imm64 |= (uint64_t)X4_i(instr) << 63;
    fprintf(outfile,"brl.call%s%s%s b%d=0x%" PRIx64,
	    Table_4_50[X4_wh(instr)],
	    Table_4_49[X4_p(instr)],
	    Table_4_52[X4_d(instr)],
	    X4_b1(instr),
	    imm64 + (addr & ~UINT64_C(0x3)));
    break;

  case 405: /* X5 - nop/hint */
    print_pred(outfile,all_qp(instr));
    imm64 = (uint64_t)X1_imm20a(instr);
    imm64 |= (uint64_t)X1_i(instr) << 20;
    imm64 |= (instrcont << 21);
    fprintf(outfile,"%s.x 0x%" PRIx64,
	    X5_y(instr) ? "hint":"nop", imm64);
    break;

  case 501: /* A1 */
    print_pred(outfile,all_qp(instr));
    switch (A1_x4(instr)) {
    case 0:
      fprintf(outfile,"add r%d=r%d,r%d%s",
	      all_r1(instr), all_r2(instr), all_r3(instr),
	      A1_x2b(instr) ? ",1" : "");
      break;
    case 1:
      fprintf(outfile,"sub r%d=r%d,r%d%s",
	      all_r1(instr), all_r2(instr), all_r3(instr),
	      !A1_x2b(instr) ? ",1" : "");
      break;
    case 2:
      fprintf(outfile,"addp4 r%d=r%d,r%d",
	      all_r1(instr), all_r2(instr), all_r3(instr));
      break;
    case 3:
      switch (A1_x2b(instr)) {
      case 0:
	fprintf(outfile,"and r%d=r%d,r%d",
		all_r1(instr), all_r2(instr), all_r3(instr));
	break;
      case 1:
	fprintf(outfile,"andcm r%d=r%d,r%d",
		all_r1(instr), all_r2(instr), all_r3(instr));
	break;
      case 2:
	fprintf(outfile,"or r%d=r%d,r%d",
		all_r1(instr), all_r2(instr), all_r3(instr));
	break;
      case 3:
	fprintf(outfile,"xor r%d=r%d,r%d",
		all_r1(instr), all_r2(instr), all_r3(instr));
	break;
      }
      break;
    default:
      goto mistake;
    }
    break;

  case 502: /* A2 shladd/shladdp4 */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile,"%s r%d=r%d,%d,r%d",
	    (A2_x4(instr) & 2) ? "shladdp4" : "shladd",
	    all_r1(instr), all_r2(instr), (int)(A2_ct2d(instr)+1),
	    all_r3(instr));
    break;

  case 503: /* A3 Integer ALU - Immediate8-Register */
    {
      static const char *a3ops[]={
	"","sub","","",
	"and","andcm","or","xor"};
      print_pred(outfile,all_qp(instr));
      imm8 = A3_imm7b(instr);
      if (A3_s(instr)) imm8 |= ~(0x7f);
      fprintf(outfile,"%s r%d=%d,r%d",
	      a3ops[A3_x2b(instr)+((A3_x4(instr)&2)<<1)],
	      all_r1(instr), imm8, all_r3(instr));
    }
    break;

  case 504: /* A4 adds/addp4 */
    print_pred(outfile,all_qp(instr));
    imm14 = A4_imm7b(instr) | (A4_imm6d(instr)<<7);
    if (A4_s(instr)) imm14 |= ~(0x1FFF);
    fprintf(outfile,"%s r%d=%d,r%d",
	    A4_x2a(instr)==2?"adds" : "addp4",
	    all_r1(instr),imm14,all_r3(instr));
    break;

  case 505: /* A5 addl */
    print_pred(outfile,all_qp(instr));
    imm22 = A5_imm7b(instr);
    imm22 |= (A5_imm9d(instr) << 7);
    imm22 |= (A5_imm5c(instr) << 16);
    if (A5_s(instr)) imm22 |= ~(0x1FFFFF);
    fprintf(outfile,"addl r%d=%d,r%d",
	    all_r1(instr),imm22,A5_r3(instr));
    break;

  case 506: /* A6 integer compare - register-register */
  case 507: /* A7 integer compare to zero */
  case 508: /* A8 integer compare immediate-register */
    {
      static const char *comptypename[]= {
	".lt", ".ltu", ".eq", "", 
	".lt.unc", ".ltu.unc", ".eq.unc", "", 
	".eq.and", ".eq.or", ".eq.or.andcm", "",
	".ne.and", ".ne.or", ".ne.or.andcm", "",

	".gt.and", ".gt.or", ".gt.or.andcm", "", 
	".le.and", ".le.or", ".le.or.andcm", "", 
	".ge.and", ".ge.or", ".ge.or.andcm", "", 
	".lt.and", ".lt.or", ".lt.or.andcm", "", 
      };
      int comptype = ((all_majorop(instr) &3)) |
	(A6_c(instr)<<2) |
	(A6_ta(instr)<<3) |
	(((A6_x2(instr) & 2) || !A6_tb(instr)) ? 0 : 16);

      print_pred(outfile,all_qp(instr));
      fprintf(outfile,"cmp%s%s p%d,p%d=",
	      (A6_x2(instr) & 1) ? "4" : "",
	      comptypename[comptype],
	      A6_p1(instr),
	      A6_p2(instr));
      switch (format) {
      case 506 : 
	fprintf(outfile,"r%d,r%d",
		all_r2(instr), all_r3(instr));
	break;
      case 507:
	fprintf(outfile,"r0,r%d",all_r3(instr));
	break;
      case 508:
	imm8 = A8_imm7b(instr);
	if (A8_s(instr)) imm8 |= ~(0x7f);
	fprintf(outfile,"%d,r%d",imm8,all_r3(instr));
      }
    }
    break;

  case 509: /* A9 - multimedia ALU */
    {
      static const char *a9ops[] = { "padd","psub","pavg","pavgsub"};
      static const char *a9compl[] = { "",".sss",".uuu",".uus",
				 "",".sss",".uuu",".uus",
				 "","","",".raz",
				 "","","","" };
      int size = (1<<(A9_za(instr)*2+A9_zb(instr)));
      print_pred(outfile,all_qp(instr));
      if (A9_x4(instr) >= 4) { 
	fprintf(outfile,"pcmp%d%s",size, A9_x2b(instr) ? ".gt" : ".eq");
      } else {
	fprintf(outfile,"%s%d%s", a9ops[A9_x4(instr) & 3],
		size,a9compl[(A9_x4(instr)<<2)|A9_x2b(instr)]);
      }
      fprintf(outfile," r%d=r%d,r%d",
	      all_r1(instr),all_r2(instr),all_r3(instr));
    }
    break;

  case 510: /* A10 - multimedia shift and add */
    print_pred(outfile,all_qp(instr));
    fprintf(outfile, "%s r%d=r%d,%d,r%d",
	    A10_x4(instr)==6 ? "pshradd2" : "pshladd2",
	    all_r1(instr), all_r2(instr),
	    (int)(A10_ct2d(instr) + 1),
	    all_r3(instr));
    break;

  default:
    print_pred(outfile,0);
    goto notyet;
  } /* case instruction formats */

  fprintf(outfile," %s\n",stopafter? ";;" : "");

 handle_long:
  return;

 notyet:
  fprintf(outfile,"<disassembly %d not yet implemented>\n",format);
  goto handle_long;

 mistake:
  fprintf(outfile,"<bug in the disassembly routine>\n");
  goto handle_long;
 reserved:
  fprintf(outfile,"<reserved>\n");
  goto handle_long;
 reservedpred:
  fprintf(outfile,"<reserved if predicate equals 1>\n");
  goto handle_long;
 reservedpredb:
  fprintf(outfile,"<reserved if predicate equals 1 (B-unit)>\n");
  goto handle_long;

}

const char *EMU_intr_string(int fault) {
  switch ((IA64_intr_t)fault) {
  case IA64_intr_none : return "";

  case IA64_intr_instructionpagenotpresent :
    return "Instruction Page Not Present fault";

  case IA64_intr_illegaloperation : 
    return "Illegal Operation fault";
  case IA64_intr_breakinstruction : 
    return "Break Instruction fault";
  case IA64_intr_privilegedoperation : 
    return "Privileged Operation fault";
  case IA64_intr_disabledfpregister : 
    return "Disabled Floating-point Register fault";

  case IA64_intr_registernatconsumption : 
    return "Register NaT Consumption fault";
  case IA64_intr_reservedregisterfield : 
    return "Reserved Register/Field fault";
  case IA64_intr_privilegedregister : 
    return "Privileged Register fault";
  case IA64_intr_speculativeoperation : 
    return "Speculative Operation fault";

  case IA64_intr_unaligneddatareference : 
    return "Unaligned Data Reference fault";

  case IA64_intr_floatingpointfault : 
    return "Floating Point fault";

  case IA64_intr_unimplementedinstructionaddress : 
    return "Unimplemented Instruction Address trap";
  case IA64_intr_floatingpointtrap : 
    return "Floating Point trap";
  case IA64_intr_lowerprivilegetransfer : 
    return "Lower Privilege Transfer trap";
  case IA64_intr_takenbranch : 
    return "Taken Branch trap";
  case IA64_intr_singlestep :
    return "Single Step trap";

  case IA64_intr_memory : 
    return "Memory error fault";
  case IA64_intr_notimplemented: 
    return "Not yet implemented abort";
  case IA64_intr_rsenotimplemented: 
    return "RSE behavior not yet implemented abort";
  default : return "Unknown fault";
  }
}

static inline char IA64_RT_GR_INFRAME_int(IA64_context_t *realct,
					      int N) {
  if (N<32) return ' ';
  if (N-32 >= rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sof)) return ' ';
  if (N-32 < (rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sor) << 3)) 
    if (N-32 < rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sol)) return 'R';
    else return 'r';
  else
    if (N-32 < rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sol)) return '=';
    else return '.';
}

#define IA64_RT_GR_INFRAME(N) IA64_RT_GR_INFRAME_int(realct,N)

void EMU_dump_state(LSE_emu_ctoken_t ctx, FILE *outfile, int dofp) {
  IA64_context_t *realct = (IA64_context_t *)ctx;
  int i;
  int rrbpr, rrbfr, rrbgr, sor, bn, bof;
  static const char cval[]="-N";
  static const char cval2[]="+-";

  rrbpr = RRB_PR_ct(realct);
  rrbfr = RRB_FR_ct(realct);
  rrbgr = RRB_GR_ct(realct);
  sor = SOR_ct(realct)<<3;
  bn = rf_get(realct->sr[IA64_ADDR_PSR].PSR,PSR_bn);
  bof = realct->sr[IA64_ADDR_CFM].CFM.BOF;

  fprintf(outfile,
	  "cfm: (f%d.f%d.g%d).R%d.L%d.F%d/%d\t"
	  "ec: %" PRId64 "\tlc: %" PRId64 "\tpfs: %" PRIx64 "\tUNAT:%" PRIx64,
	  (int)rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_rrb_pr), 
	  (int)rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_rrb_fr),
	  (int)rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_rrb_gr),
	  (int)rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sor),
	  (int)rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sol), 
	  (int)rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sof),
	  realct->sr[IA64_ADDR_CFM].CFM.BOF,
	  realct->ar[IA64_ADDR_EC],
	  realct->ar[IA64_ADDR_LC],
	  realct->ar[IA64_ADDR_PFS],
	  realct->ar[IA64_ADDR_UNAT]);
  fprintf(outfile,
	  "\tfpsr:%04x.%04x.%04x.%04x.%02x\n",
	  (int)rf_get(realct->ar[IA64_ADDR_FPSR],FPSR_sf3),
	  (int)rf_get(realct->ar[IA64_ADDR_FPSR],FPSR_sf2),
	  (int)rf_get(realct->ar[IA64_ADDR_FPSR],FPSR_sf1),
	  (int)rf_get(realct->ar[IA64_ADDR_FPSR],FPSR_sf0),
	  (int)rf_get(realct->ar[IA64_ADDR_FPSR],FPSR_traps));
  fprintf(outfile,"bsp: %" PRIx64 "\tbspstore: %" PRIx64 "\t"
	  "NDirty:%-4ld NClean:%-4ld rnat:%016" PRIx64 "\n",
	  realct->ar[IA64_ADDR_BSP],
	  realct->ar[IA64_ADDR_BSPSTORE],
	  realct->RSE_other.NDirty,
	  realct->RSE_other.NClean,
	  realct->ar[IA64_ADDR_RNAT]);

  for(i=0;i<GR_FILE_SIZE/4;i++) {
    fprintf(outfile,"r%-3d%c%-3d %c 0x%16" PRIx64 " ! ",
	    i,
	    IA64_RT_GR_INFRAME(i),
	    get_r_pno(i,rrbgr,sor,bof,bn,realct->num_stacked_phys),
	    cval[(get_r_point(realct,i,rrbgr,sor,bn,bof))->nat >0],
	    get_r_point(realct,i,rrbgr,sor,bn,bof)->val);
    fprintf(outfile,"r%-3d%c%-3d %c 0x%16" PRIx64 " ! ",
	    i+GR_FILE_SIZE/4,
	    IA64_RT_GR_INFRAME(i+GR_FILE_SIZE/4),
	    get_r_pno(i+GR_FILE_SIZE/4,rrbgr,sor,bof,bn,realct->num_stacked_phys),
	    cval[get_r_point(realct,i+GR_FILE_SIZE/4,rrbgr,sor,bn,bof)->nat>0],
	    get_r_point(realct,i+GR_FILE_SIZE/4,rrbgr,sor,bn,bof)->val);
    fprintf(outfile,"r%-3d%c%-3d %c 0x%16" PRIx64 " ! ",
	    i+GR_FILE_SIZE/2,
	    IA64_RT_GR_INFRAME(i+GR_FILE_SIZE/2),
	    get_r_pno(i+GR_FILE_SIZE/2,rrbgr,sor,bof,bn,realct->num_stacked_phys),
	    cval[get_r_point(realct,i+GR_FILE_SIZE/2,rrbgr,sor,bn,bof)->nat>0],
	    get_r_point(realct,i+GR_FILE_SIZE/2,rrbgr,sor,bn,bof)->val);
    fprintf(outfile,"r%-3d%c%-3d %c 0x%16" PRIx64 "\n",
	    i+3*GR_FILE_SIZE/4,
	    IA64_RT_GR_INFRAME(i+3*GR_FILE_SIZE/4),
	    get_r_pno(i+3*GR_FILE_SIZE/4,rrbgr,sor,bof,bn,realct->num_stacked_phys),
	    cval[get_r_point(realct,i+3*GR_FILE_SIZE/4,rrbgr,sor,bn,bof)
		->nat>0],
	    get_r_point(realct,i+3*GR_FILE_SIZE/4,rrbgr,sor,bn,bof)->val);
  }

  fprintf(outfile,"--------------------------------"
	  "--------------------------------\n");
  for(i=0;i<PR_FILE_SIZE/4;i++) {
    fprintf(outfile,"p%-2d %2d      %d !",
	    i,
	    (int)IA64_RT_PR_NO(i,rrbpr),
	    realct->p[(IA64_RT_PR_NO(i,rrbpr))]);
    fprintf(outfile," p%-2d %2d      %d !",
	    i+PR_FILE_SIZE/4,
	    (int)IA64_RT_PR_NO(i+PR_FILE_SIZE/4,rrbpr),
	    realct->p[(IA64_RT_PR_NO(i+PR_FILE_SIZE/4,rrbpr))]);
    fprintf(outfile," p%-2d %2d      %d !",
	    i+PR_FILE_SIZE/2,
	    (int)IA64_RT_PR_NO(i+PR_FILE_SIZE/2,rrbpr),
	    realct->p[(IA64_RT_PR_NO(i+PR_FILE_SIZE/2,rrbpr))]);
    fprintf(outfile," p%-2d %2d      %d\n",
	    i+3*PR_FILE_SIZE/4,
	    (int)IA64_RT_PR_NO(i+3*PR_FILE_SIZE/4,rrbpr),
	    realct->p[(IA64_RT_PR_NO(i+3*PR_FILE_SIZE/4,rrbpr))]);
  }

  if (dofp) {
    fprintf(outfile,"--------------------------------"
	    "--------------------------------\n");
    for(i=0;i<FR_FILE_SIZE/2;i++) {
      fprintf(outfile,"f%-3d %-3d %c0x%05x %016" PRIx64 " %16.8g ! ",
	      i,
	      (int)IA64_RT_FR_NO(i,rrbfr),
	      cval2[realct->f[IA64_RT_FR_NO(i,rrbfr)].sign],
	      realct->f[IA64_RT_FR_NO(i,rrbfr)].exponent,
	      realct->f[IA64_RT_FR_NO(i,rrbfr)].significand,
	      fp_reg2host(&realct->f[IA64_RT_FR_NO(i,rrbfr)]));
      fprintf(outfile,"f%-3d %-3d %c0x%05x %016" PRIx64 " %16.8g\n",
	      i+FR_FILE_SIZE/2,
	      (int)IA64_RT_FR_NO(i+FR_FILE_SIZE/2,rrbfr),
	      cval2[realct->f[IA64_RT_FR_NO(i+FR_FILE_SIZE/2,rrbfr)].sign],
	      realct->f[IA64_RT_FR_NO(i+FR_FILE_SIZE/2,rrbfr)].exponent,
	      realct->f[IA64_RT_FR_NO(i+FR_FILE_SIZE/2,rrbfr)].significand,
	      fp_reg2host(&realct->f[IA64_RT_FR_NO(i+FR_FILE_SIZE/2,rrbfr)]));
    }
  }

  fprintf(outfile,"--------------------------------"
	  "--------------------------------\n");
  for(i=0;i<BR_FILE_SIZE/2;i++) {
    fprintf(outfile,"b%-2d        0x%16" PRIx64 " ! ",
	    i,
	    realct->b[i]);
    fprintf(outfile,"b%-2d        0x%16" PRIx64 "\n",
	    i+BR_FILE_SIZE/2,
	    realct->b[i+BR_FILE_SIZE/2]);
  }
  fprintf(outfile,"================================"
	  "================================\n");
}

/* Temporary hack.... not thread-safe */
void IA64_disassemble_to_string(LSE_emu_instr_info_t *ii, char *s, size_t n) {
  FILE *outfile;
  outfile = fopen("tempia64dis","w+");
  EMU_disassemble_instr(ii,outfile);
  rewind(outfile);
  fgets(s,n,outfile);
  fclose(outfile);
}

} // namespace LSE_IA64
