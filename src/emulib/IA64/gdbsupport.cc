/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * GDB support for the IA64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This is the GDB stub for the IA64 emulator.  It's a blast!
 *
 * TODO: review all endianness in this file
 *       - figure out how to affect NAT's in r1-r31 (through magic UNAT?)
 */
#include "LSE_IA64.h"
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
/* extern int inet_aton(const char *cp, struct in_addr * addr); */
#include <unistd.h>
#include "IA64_fp.h"
using namespace LSE_IA64;
#include "LSE_emu_alonesupp.h"

/* #define DEBUG_PACKETS */
/* #define DEBUG_GDBSET */
#define PACKET_SIZE 12000

static FILE *debugfile;
static char packetbuf[PACKET_SIZE+5]="$";
static char sbuf[4] = "#00";

static int fdgetchar(int fd) {
  char c;
  int rval;
  rval = read(fd,&c,1);
  if (rval < 0) {
    fprintf(stderr,"Error %d while reading from GDB socket\n",errno);
    exit(1);
  }
  return rval ? c : EOF;
}

static void sendpacket(int fd, const char *s) {
  int a = 0;
  char c;
  const char *p = s;

  while ((c=*(p++))) a = (a + c) % 256;

  sprintf(sbuf+1,"%02x",a);
  strcpy(packetbuf+1,s);
  strcat(packetbuf,sbuf);
  if (debugfile)
    fprintf(debugfile,"-> %-60.60s\n",packetbuf);
  write(fd,packetbuf,strlen(packetbuf));
}

static char *printhex8(char *p, uint64_t val) {
  int j;
  for (j=0;j<8;j++) {
    sprintf(p,"%02x",(int)val&0xff);
    p+=2;
    val >>= 8;
  }
  return p;
}

static int gethexint(char *p, int *valp) {
  int count = 0;
  int val = 0;
  while (*p && count<8) {
    if (*p >= '0' && *p <= '9') val = (val * 16) + *p - '0';
    else if (*p >= 'a' && *p <= 'f') val = (val * 16) + *p - 'a' + 10;
    else if (*p >= 'A' && *p <= 'F') val = (val * 16) + *p - 'A' + 10;
    else break;
    count ++;
    p++;
  }
  *valp = val;
  return count;
}

static int gethexbyte(char *p, unsigned char *valp) {
  int count = 0;
  unsigned char val = 0;
  while (*p && count<2) {
    if (*p >= '0' && *p <= '9') val = (val * 16) + *p - '0';
    else if (*p >= 'a' && *p <= 'f') val = (val * 16) + *p - 'a' + 10;
    else if (*p >= 'A' && *p <= 'F') val = (val * 16) + *p - 'A' + 10;
    else break;
    count ++;
    p++;
  }
  *valp = val;
  return count & 1 ? 0 : count; /* complain about odd bytes */
}

static int gethex64(char *p, uint64_t *valp) {
  int count = 0;
  uint64_t val = 0;
  while (*p && count<16) {
    if (*p >= '0' && *p <= '9') val = (val * 16) + *p - '0';
    else if (*p >= 'a' && *p <= 'f') val = (val * 16) + *p - 'a' + 10;
    else if (*p >= 'A' && *p <= 'F') val = (val * 16) + *p - 'A' + 10;
    else break;
    count ++;
    p++;
  }
  *valp = val;
  return count;
}

/************************** Really IA64-dependent code! ***************/

static void
do_flush(IA64_context_t *realct) 
{
  LSE_emu_addr_t bsp = realct->ar[IA64_ADDR_BSP];
  LSE_emu_addr_t bspstore = realct->ar[IA64_ADDR_BSPSTORE];
  int bitnum = (bspstore >> 3) & 0x3f;
  uint64_t mask = INT64_C(1) << bitnum;
  uint64_t rnat = realct->ar[IA64_ADDR_RNAT];
  uint64_t len = 8;
  int sof = rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sof);
  int rno = realct->sr[IA64_ADDR_CFM].CFM.BOF - realct->RSE_other.NDirty;
  rno = IA64_GR_FAST_ROT(rno,realct->num_stacked_phys);

#ifdef DEBUG_RSE
  fprintf(stderr,"RSE: GDB bsp=%016"PRIx64" bspstore=%016"PRIx64"\n",bsp,bspstore);
#endif

  /* need to do a cover as well, but not update CFM, while updating BSP */
  bsp += 8*(sof + (((bsp>>3)&0x3f)+sof)/63);
#ifdef DEBUG_RSE
  fprintf(stderr,"RSE: GDB doing cover with size %d to %016"PRIx64"\n",sof,bsp);
#endif

  realct->ar[IA64_ADDR_BSP] = bsp;
  /* Do not know about these... */
  realct->ar[IA64_ADDR_BSPSTORE] = bsp;
  realct->RSE_other.NDirty = 0;
  realct->RSE_other.NClean = 0;

  while (bspstore < bsp) {
    rnat = (rnat & ~mask) | 
      (((uint64_t)realct->physr[rno].nat) << bitnum);

#ifdef DEBUG_RSE
    fprintf(stderr,"RSE: GDB flushing %d %c %016"PRIx64" to %016"PRIx64"\n", 
  	    rno,
	    realct->physr[rno].nat ? 'N' : '-',
	    realct->physr[rno].val,
	    bspstore);
#endif
    /* TODO: endianness */
    try {
      realct->mem->write(bspstore,(unsigned char *)&realct->physr[rno].val,len);
    } catch (LSE_device::deverror_t) {
      /* hmm.... */
      break;
    }

    bspstore += 8;
    bitnum++;
    rno = IA64_GR_FAST_ROT(rno + 1,realct->num_stacked_phys);
    mask <<= 1;
    if (bitnum==63) {
#ifdef DEBUG_RSE
      /* store here */
      fprintf(stderr,"RSE: GDB flushing RNAT %016"PRIx64" to %016"PRIx64"\n", 
	      rnat,bspstore);
#endif
      /* TODO: endianness */
      try {
	realct->mem->write(bspstore,(unsigned char *)&rnat,len);
      } catch (LSE_device::deverror_t) {
	/* hmm */
	break; /* do not fall-through on error */
      }
      bitnum = 0;
      bspstore += 8;
      mask = INT64_C(1);
    }
  }
  realct->ar[IA64_ADDR_RNAT] = rnat;
}

static int
undo_flush(IA64_context_t *realct) 
{
  LSE_emu_addr_t bsp = realct->ar[IA64_ADDR_BSP], loadp;
#ifdef DEBUG_RSE
  LSE_emu_addr_t bspstore = realct->ar[IA64_ADDR_BSPSTORE];
#endif
  int bitnum;
  int numtodo;
  uint64_t mask;
  uint64_t rnat = realct->ar[IA64_ADDR_RNAT];
  uint64_t len = 8;
  int sof = rf_get(realct->sr[IA64_ADDR_CFM].CFM.val,CFM_sof);
  unsigned int rno = realct->sr[IA64_ADDR_CFM].CFM.BOF + sof;
  rno = IA64_GR_FAST_ROT(rno,realct->num_stacked_phys);

#ifdef DEBUG_RSE
  fprintf(stderr,"RSE: GDB bsp=%016"PRIx64" bspstore=%016"PRIx64"\n",bsp,bspstore);
#endif

  /* need to do a loadrs to get the frame back in.... */
  bsp -= 8*(sof + (62-((bsp>>3)&0x3f)+sof)/63);

#ifdef DEBUG_RSE
  fprintf(stderr,
	  "RSE: GDB doing loadrs with size %d to %016"PRIx64"\n",sof,bsp);
#endif
  realct->ar[IA64_ADDR_BSP] = bsp;
  
  numtodo = sof;
  loadp = bsp + 8*(numtodo+(((bsp>>3)&0x3f)+numtodo)/63);
  bitnum = (loadp>>3) & 0x3f;
  mask = INT64_C(1) << bitnum;

  while (numtodo) {

    loadp -= 8; /* pre-decrement */
    bitnum--;
    numtodo --;
    mask >>= 1;
    rno = IA64_GR_FAST_ROT(rno - 1 + realct->num_stacked_phys,
			   realct->num_stacked_phys);
    
    if (bitnum == -1) { /* Get RNAT */
      
      /* TODO: endianness */
      try {
	realct->mem->read(loadp, (unsigned char *)&rnat, len);
      } catch (LSE_device::deverror_t) {
	/* TODO: set up fault state correctly */
	return 1;
	break; /* do not fall-through on error */
      }
#ifdef DEBUG_RSE
      /* load here */
      fprintf(stderr,"RSE: GDB filling RNAT %016"PRIx64" from %016"PRIx64"\n", 
		    rnat,loadp);
#endif
      bitnum = 62;
      loadp -= 8;
      mask = INT64_C(1) << 62;
    }
    
    /* TODO: endianness */
    try {
      realct->mem->read(loadp, (unsigned char *)&realct->physr[rno].val, len);
    } catch (LSE_device::deverror_t) {
      /* TODO: set up fault state correctly */
      return 1;
      break; /* do not fall-through on error */
    }
    realct->physr[rno].nat = (mask & rnat) ? 1:0;
    
#ifdef DEBUG_RSE
    fprintf(stderr,"RSE: GDB filling %d %016"PRIx64" %c from %016"PRIx64"\n",rno,
	    realct->physr[rno].val,realct->physr[rno].nat?'N':'-',loadp);
#endif

  } /* while(numtodo) */

  /* ugly side effect that cannot be rolled back! */
  realct->ar[IA64_ADDR_RNAT] = rnat;
  realct->ar[IA64_ADDR_BSP] = bsp;
  realct->ar[IA64_ADDR_BSPSTORE] = bsp;

  /* NAV 2004/08/22: Setting clean to sof seems to cause the RSE to go
     out of sync, if we don't change it, things seem to work better */
  /*realct->RSE_other.NClean = sof;*/
  return 0;
}

/************ END of really IA64-dependent code ********************/

static int EMU_gdb_socket, EMU_gdb_conn;

void EMU_gdb_init(int port) {
  struct sockaddr_in saddr;
  struct sockaddr caddr;
  socklen_t caddrs=sizeof(caddr);

  /* Get ourselves a connection */
  EMU_gdb_socket = socket(PF_INET, SOCK_STREAM,0);
  if (EMU_gdb_socket<0) {
    fprintf(stderr,"Unable to create GDB socket\n");
    exit(1);
  }
  saddr.sin_family = AF_INET;
  saddr.sin_port = htons(port);
  inet_aton("127.0.0.1",&saddr.sin_addr); /* loopback */
  if (bind(EMU_gdb_socket,(struct sockaddr *)&saddr,sizeof(saddr))) {
    fprintf(stderr,"bind() call for GDB socket failed\n");
    perror("");
    close(EMU_gdb_socket);
    exit(1);
  }
  if (listen(EMU_gdb_socket,1)) {
    fprintf(stderr,"listen() call for GDB socket failed\n");
    close(EMU_gdb_socket);
    exit(1);
  }
  EMU_gdb_conn = accept(EMU_gdb_socket,
			(struct sockaddr *)&caddr, &caddrs);
  if (EMU_gdb_conn < 0) {
     fprintf(stderr,"accept() call for GDB socket failed with error %d\n",
	     errno);
     close(EMU_gdb_socket);
     exit(1);
  }

#ifdef DEBUG_PACKETS
  debugfile = stderr;
#else
  debugfile = NULL;
#endif

}

void EMU_gdb_start_context(LSE_emu_contextno_t hwcno) {
  IA64_context_t *ctx;
  ctx = (IA64_context_t *)LSE_emu_hwcontexts_table[hwcno].ctok;
  rf_set(ctx->sr[IA64_ADDR_PSR].PSR,PSR_ss,1);
}

void EMU_gdb_report_end(IA64_dinst_t *di) {
  char newpacket[7];
  sprintf(newpacket,"W%02x",LSE_sim_exit_status);
  sendpacket(EMU_gdb_conn,newpacket);
}

/* return value indicates whether GDB has detached.... */
int 
EMU_gdb_enter(int signo, int cno) {
  IA64_context_t *ctx;
  int c;
  int rval, stepping;
  char packet[PACKET_SIZE+1];
  char newpacket[PACKET_SIZE+1];
  int pcount;
  char *npp;
  int i;
  uint64_t val, addr, len64;
  int len, wlen, lenmax;
  unsigned char *bufp;
  int *flagsp;

  /* We are essentially in handle_exception now */

  ctx = (IA64_context_t *)LSE_emu_hwcontexts_table[cno].ctok;
  /* entering GDB; need to flush general registers to register stack */
  do_flush(ctx);

  /* inform GDB of why we stopped */
  sprintf(newpacket,"S%02x",signo);
  sendpacket(EMU_gdb_conn,newpacket);
#ifdef DEBUG_GDBSET
  fprintf(stderr,"IP=%016"PRIx64"\n",LSE_emu_addr_table[cno]);
#endif

  c = fdgetchar(EMU_gdb_conn);
  while (c>=0) { /* ends while loop at end of file */
      
    /* find packet start */
    
    if (debugfile) fputs("<- ",debugfile);
    
    while (c>=0 && c != '$' && c != '-') {
      if (debugfile) putc(c,debugfile);
      c = fdgetchar(EMU_gdb_conn);
    }
    if (c < 0) break;
    
    /* Check for re-runs */
    if (c == '-') {
      if (debugfile) fputs("<- -\n",debugfile);
      write(EMU_gdb_conn,packetbuf,strlen(packetbuf));
      c = fdgetchar(EMU_gdb_conn);
      continue;
    }
    
    /* get the packet.... watch out for overruns */
    pcount = 0;
    while (c != '#' && pcount < PACKET_SIZE) {
      packet[pcount++] = c;
      c = fdgetchar(EMU_gdb_conn);
    }
    packet[pcount] = 0;

    /* Throw away the checksum */
    fdgetchar(EMU_gdb_conn);
    fdgetchar(EMU_gdb_conn);
    
    /* Process the packet */
    
    if (debugfile) {
      fprintf(debugfile,"%-60.60s\n",packet);
      fflush(debugfile);
    }

    write(EMU_gdb_conn,"+",1);  /* we got the packet */ 
    
    stepping = 0;
    
    switch (packet[1]) {
#ifdef LATER
    case 'H' :
      sendpacket(EMU_gdb_conn,"OK");
      break;
#endif

    case '?':
      /* TODO: should be reason of last signal */
      sendpacket(EMU_gdb_conn,"S05");  /* sigtrap for debug? */
      break;

    case 'G':
      len = 0;
      lenmax = 128+128+64+8+2+4+128-1;
      npp = packet+2;
      goto doregs;
    case 'P':
      npp = packet+2;
      rval = gethexint(npp,&len);
      if (!rval || !(npp = npp + rval) || *npp++ != '=') {
	sendpacket(EMU_gdb_conn,"E01");
	break;
      }
      lenmax = len;
    doregs:
      {
	int printall = len != lenmax;
	int hadfloat = 0;
	
	for (;len<=lenmax;len++) {
	  npp+=gethex64(npp,&val);
	  val = LSE_end_be2h_ll(val);
	  if (len==0) { /* do not modify r0 */
	    if (!printall) {
	      fprintf(stderr,"IP=%016"PRIx64"\n",
		      LSE_emu_addr_table[cno]);
	      EMU_dump_state(ctx,stderr,0);
	    }
	  } else if (len < 32) { /* gr */
	    ctx->r[len].val = val;
#ifdef DEBUG_GDBSET
	    fprintf(stderr,"r%d = %016"PRIx64"\n",len,val);
#endif
	  } else if (len < 128) { /* stacked r -- not real */
	  } else if (len < 256) { /* fp */
	    hadfloat = 1;
	    if (len > 129) { /* prevent modifications to f0/f1 */
	      ctx->f[len-128].significand = val;
	      npp+=gethex64(npp,&val);
	      val = LSE_end_be2h_ll(val);
	      ctx->f[len-128].sign = (val >> 17) & 1;
	      ctx->f[len-128].exponent = val & 0x1ffff;
#ifdef DEBUG_GDBSET
	      fprintf(stderr,"f%d = %f\n",len-128,
		      fp_reg2host(&ctx->f[len-128]));
#endif
	    } else {
	      npp+=gethex64(npp,&val);
	      if (len == 128 && !printall) {
		fprintf(stderr,"IP=%016"PRIx64"\n",
			LSE_emu_addr_table[cno]);		
		EMU_dump_state(ctx,stderr,1);
	      }
	    }
	  } else if (len < 0x140) { /* preds done through pr.... */
	  } else if (len < 0x148) { /* branch regs */
	    ctx->b[len-0x140] = val;
#ifdef DEBUG_GDBSET
	    fprintf(stderr,"b%d = %016"PRIx64"\n",len-0x140,val);
#endif
	  } else if (len < 0x14a) { /* vfp, vrap */
	  } else if (len == 0x14a) { /* pr */
	    val |= 1;
	    for (i=0;i<=63;i++) {
	      ctx->p[i] = (val & 1);
		val >>=1;
	    }
#ifdef DEBUG_GDBSET
	    fprintf(stderr,"pr = %016"PRIx64"\n",val);
#endif
	  } else if (len == 0x14b) { /* ip */
	    LSE_emu_addr_table[cno] = val;
#ifdef DEBUG_GDBSET
	    fprintf(stderr,"ip = %016"PRIx64"\n",val);
#endif
	  } else if (len == 0x14c) { /* psr */
	    ctx->sr[IA64_ADDR_PSR].PSR = val;
#ifdef DEBUG_GDBSET
	    fprintf(stderr,"psr = %016"PRIx64"\n",val);
#endif
	  } else if (len == 0x14d) { /* cfm */
	    ctx->sr[IA64_ADDR_CFM].CFM.val = val;
#ifdef DEBUG_GDBSET
	    fprintf(stderr,"cfm = %016"PRIx64"\n",val);
#endif
	  } else if (len < 0x1ce) {  /* ar's */
	    ctx->ar[len - 0x14e] = val;
#ifdef DEBUG_GDBSET
	    fprintf(stderr,"ar%d = %016"PRIx64"\n",len-0x14e,val);
#endif
	  } else { /* nats -- not possible */
	  }
	}
      }
      sendpacket(EMU_gdb_conn,"OK");
      break;

    case 'g':
      npp = newpacket;
      for (i=0;i<32;i++) {
	npp=printhex8(npp,ctx->r[i].val);
      }
      for (i=32;i<128;i++) { /* ignored */
	sprintf(npp,"0000000000000000"); npp+=16;
      }
      for (i=0;i<128;i++) {
	npp=printhex8(npp,ctx->f[i].significand);
	npp=printhex8(npp,ctx->f[i].exponent | (ctx->f[i].sign << 17));
      }
      for (i=0;i<64;i++) {  
	npp=printhex8(npp,ctx->p[i]?-1:0);
      }
      for (i=0;i<8;i++) {
	npp=printhex8(npp,ctx->b[i]);
      }
      
      /* vfp, vrap */
      sprintf(npp,"0000000000000000"); npp+=16;
      sprintf(npp,"0000000000000000"); npp+=16;

	/* pr */
      for (val=0,i=63;i>=0;i--) {
	val = (val << 1) | ctx->p[i];
      }      
      npp=printhex8(npp,val);
      /* ip, psr, cfm */
      npp=printhex8(npp,LSE_emu_addr_table[cno]);
      npp=printhex8(npp,ctx->sr[IA64_ADDR_PSR].PSR);
      npp=printhex8(npp,ctx->sr[IA64_ADDR_CFM].CFM.val);

	/* ar's (not unat!)*/
      for (i=0;i<128;i++) {
	npp = printhex8(npp,ctx->ar[i]);
      }

      /* fprintf(stderr,"BSP is %016"PRIx64"\n",ctx->ar[IA64_ADDR_BSP]); */
      /* NAT's do not seem to work... gdb takes them at UNAT/RNAT/reg stack */
      for (i=0;i<128;i++) {
	npp=printhex8(npp,ctx->r[i].nat?-1:0);
      }

      sendpacket(EMU_gdb_conn,newpacket);
      break;

    case 'm':
      npp = packet+2;
      rval=gethex64(npp,&addr);
      if (rval && (npp=npp+rval) && (*npp++ == ',')
	  && (rval=gethexint(npp,&len))) {
	if (len > 1024) len = 1024;
	wlen = len;
	try {
	  len = ctx->mem->translate(addr, &bufp);
	  npp = newpacket;
	  if (len > wlen) len=wlen;
	  for (i=0;i<len;i++) {
	    sprintf(npp,"%02x",(unsigned int)bufp[i]); npp+=2;
	  }
	  *npp = 0;
	  sendpacket(EMU_gdb_conn,newpacket);
	} catch (LSE_device::deverror_t) {
	  sendpacket(EMU_gdb_conn,"E03");
	}
      }
      else
	sendpacket(EMU_gdb_conn,"E01");
      break;

    case 'M':
      npp = packet+2;
      rval=gethex64(npp,&addr);
      if (rval && (npp=npp+rval) && (*npp++ == ',')
	  && (rval=gethexint(npp,&len))) {

	npp+=rval;
	if (*npp++ != ':') {
	  sendpacket(EMU_gdb_conn,"E01");
	  break;
	}

	bufp = (unsigned char *)malloc(len);

	for (i=0;i<len;i++) {
	  if (!*npp || !(rval=gethexbyte(npp,&bufp[i]))) {
	    sendpacket(EMU_gdb_conn,"E01");
	    free(bufp);
	    goto finishpacket;
	  }
	  npp+=rval;
	}

	len64 = len;
	try {
	  ctx->mem->write(addr,(unsigned char *)bufp, len64);
	  sendpacket(EMU_gdb_conn,"OK");
	} catch (LSE_device::deverror_t) {
	  sendpacket(EMU_gdb_conn,"E03");
	} 
	free(bufp);
      }
      else
	sendpacket(EMU_gdb_conn,"E01");
      break;

    case 's':
      stepping = 1;
    case 'c':
      rval=gethex64(packet+2,&addr);
      if (rval) {
	LSE_emu_addr_table[cno] = addr;
      }
      undo_flush(ctx);
      rf_set(ctx->sr[IA64_ADDR_PSR].PSR,PSR_ss,stepping);
      return 0;
      break;

    default:
      sendpacket(EMU_gdb_conn,"");
      break;
    }

  finishpacket:
    c = fdgetchar(EMU_gdb_conn);
    
  } /* while c >= 0 */

  rf_set(ctx->sr[IA64_ADDR_PSR].PSR,PSR_ss,0);
  return 1; /* indicate that GDB has detached and program can run free... */

} /* EMU_gdb_enter */

void EMU_gdb_finish() {
  close(EMU_gdb_conn);
  close(EMU_gdb_socket);
  exit(0);
}

