/* 
 * Copyright (c) 2000-2012 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Main program for testing the ia64 emulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * A stub for calling the IA64 emulator so we can test it.  Also provides
 * some useful debugging features.
 *
 */
#include "LSE_IA64.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

using namespace LSE_IA64;

static char *pname;

/*************** interface handling ************/

#include "LSE_emu_alonesupp.c" /* yes, that's the .c file */

static void usage() {
  fprintf(stderr,
          "\n%s <options> [ [(--|binary)] <program options>]\n",
          pname);
  fprintf(stderr,"\t-c|--cleanenv\t\tClean all environments\n");
  fprintf(stderr,"\t--env:VAR=VALUE\t\tAdd VAR to environment\n");
  fprintf(stderr,"\t--hw:#\t\t\tNumber of hardware contexts\n");
  fprintf(stderr,"\t--gdb:#\t\t\tStart GDB stub using port #\n");
  fprintf(stderr,"\t--ignorefault\t\tDo not end on faults\n");
  fprintf(stderr,"\t--readchecks\t\tExecute from a checkpoint file\n");
  fprintf(stderr,"\t--checkfile=<fname>\tCheckpoint file name\n");
  fprintf(stderr,"\t--checkname=<name>\tCheckpoint name\n");
  fprintf(stderr,"\t--checkcompress\t\tCompress when writing checkpoints\n");

  fprintf(stderr,"\n Sampling options\n");
  fprintf(stderr,"\t--sample:first=#\tFirst instruction to sample\n");
  fprintf(stderr,"\t--sample:period=#\tSampling period\n");
  fprintf(stderr,"\t--sample:length=#\tLength of sampling state\n");
  fprintf(stderr,"\t--sample:warmup=#\tLength of warmup state\n");
  fprintf(stderr,"\t--sample:max=#\t\tMaximum number of samples\n");
  fprintf(stderr,"\t--sample:report\t\tReport sampling state changes\n");
  fprintf(stderr,"\t    setting period=0 prevents state transitions;\n");
  fprintf(stderr,"\t    the initial state is sampling if any of the\n");
  fprintf(stderr,"\t    actions but syscall are chosen\n");
  fprintf(stderr,"  Actions taking place during sampling/warmup\n");
  fprintf(stderr,"\t--trace\t\t\tTrace instructions\n");
  fprintf(stderr,"\t--optrace\t\tReport operands when tracing\n");
  fprintf(stderr,"\t--dump\t\t\tDump register file state\n");
  fprintf(stderr,"\t--syscall\t\tReport syscalls\n");
  fprintf(stderr,"\t--forcesample\t\tDummy to force sampling initial state\n");
  fprintf(stderr,"\t--rollback\t\tTurn on rollback\n");
  fprintf(stderr,"\t--steps\t\t\tTurn on steps\n");
  fprintf(stderr,"\t--sub\t\t\tTurn on sub-steps for opfetch/writeback\n");

  fprintf(stderr,"\t--writechecks=[full|os|incr]\tWrite checkpoints\n");

  fprintf(stderr,"\n Emulator options (prefix with `-emu:')\n");
  EMU_print_usage(&LSE_emu_interface);
  exit(1);
}

static char **cleanenvp = NULL;


uint64_t icount=0;
int substep = 0;
int64_t samplePeriod=0, sampleLength=0, sampleWarmup=0, sampleFirst= 0,
  sampleToGo, sampleCount, sampleMax= -1;
int dotrace=0;
int dooptrace=0;
int dodump=0;
int dosyscall=0;
int doseparate=0;
int ignorefault=0;
int sampleReport=0;
int forcesample = 0;
int dorollback=0;
static int numHWcontexts=1;
static int readCheckpoints=0;
static int writeCheckpoints = 0;
static char defaultCFN[]="emu.cpf";
static char *cptFileName=defaultCFN;
static LSE_chkpt::file_t *cptFile=NULL;
static char *cptName=NULL;
static boolean cptCompress=FALSE;
LSE_emu_chkpt_cntl_t cptControl;

typedef enum {
  ffwd, warm, sample
} sampleState_t;
static const char *sampleStateNames[] = { "Ffwd", "Warmup", "Sample" };

static sampleState_t sampleState;

static void run_stuff(int,boolean);
extern void EMU_gdb_init(int);
extern void EMU_gdb_finish(void);
extern void EMU_gdb_start_context(LSE_emu_contextno_t);
extern int EMU_gdb_enter(int, LSE_emu_contextno_t);
extern void EMU_gdb_report_end(IA64_dinst_t *);
//extern void IA64_print_instr_oper_vals(FILE *,char *,LSE_emu_instr_info_t *);

void build_cleanenv() 
{
  cleanenvp = (char**)malloc(sizeof(char*));
  *cleanenvp = NULL;
}

void add_to_cleanenv(char *str)
{
  int i = 0;
  while(cleanenvp[i]) i++;
  cleanenvp = (char **)realloc(cleanenvp, sizeof(char*) * (i+2));
  cleanenvp[i] = str;
  cleanenvp[i+1] = NULL;
}

void destroy_cleanenv()
{
  if(cleanenvp)
    free(cleanenvp);
}

static void start_checkpoint();
static void end_checkpoint();

int main(int argc, char *argv[], char **envp)
{
  int cleanenv=0, numused, ret;
  int rval;
  int gdbport=0;
  int i, cno;

  pname = argv[0];
  LSE_sim_exit_status = 0;
  EMU_init(&LSE_emu_interface);

  for (i=1;i<argc;) {
    if (!strcmp(argv[i],"--")) {
      i++;
      break;
    }
    if (!strncmp(argv[i],"-emu:",5)) {
      numused = EMU_parse_arg(&LSE_emu_interface, argc-i,argv[i]+5,argv+i+1);
      if (numused <= 0) {
        usage();
      }
      i+= numused;
    }
    else {
      if (!strcmp(argv[i],"--help")) usage();
      else if (!strcmp(argv[i],"-help")) usage();
      else if (!strcmp(argv[i],"-c") ||
	       !strcmp(argv[i],"--cleanenv")) {
	cleanenv = 1;
	build_cleanenv();
      }
      else if (!strncmp(argv[i],"--env:",6)) {
	if(!cleanenv) {
	  fprintf(stderr, "Only use --env with -c or --cleanenv\n");
	  usage();
	} else {
	  add_to_cleanenv(argv[i]+6);
	}
      }
      else if (!strncmp(argv[i],"--hw:",5)) {
	ret = sscanf(argv[i]+5,"%d",&numHWcontexts);
	if (ret<=0) numHWcontexts=1;
      }
      else if (!strncmp(argv[i],"--gdb:",6)) {
	ret = sscanf(argv[i]+6,"%d",&gdbport);
	if (ret<=0) gdbport=0;
      }
      else if (!strcmp(argv[i],"--steps")) doseparate = 1;
      else if (!strcmp(argv[i],"--sub")) substep = 1;
      else if (!strcmp(argv[i],"--rollback")) dorollback = 1;
      else if (!strcmp(argv[i],"--ignorefault")) ignorefault = 1;
      else if (!strcmp(argv[i],"--forcesample")) forcesample = 1;

      else if (!strncmp(argv[i],"--writechecks=",14)) {
	if (!strcmp(argv[i]+14,"full")) {
	  writeCheckpoints=1;
	} else if (!strcmp(argv[i]+14,"os")) {
	  writeCheckpoints=3;
	} else {
	  writeCheckpoints=2;
	}
      }
      else if (!strcmp(argv[i],"--readchecks")) readCheckpoints=1;
      else if (!strncmp(argv[i],"--checkfile=",12)) {
	cptFileName = argv[i]+12;
      }
      else if (!strncmp(argv[i],"--checkname=",12)) {
	cptName = argv[i]+12;
      }
      else if (!strcmp(argv[i],"--checkcompress")) cptCompress=TRUE;

      else if (!strcmp(argv[i],"--sample:report")) sampleReport = 1;
      else if (!strncmp(argv[i],"--sample:period=",16)) {
	ret = sscanf(argv[i]+16,"%"SCNd64,&samplePeriod);
	if (ret<=0) samplePeriod=0;
      }
      else if (!strncmp(argv[i],"--sample:length=",16)) {
	ret = sscanf(argv[i]+16,"%"SCNd64,&sampleLength);
	if (ret<=0) sampleLength=0;
      }
      else if (!strncmp(argv[i],"--sample:warmup=",16)) {
	ret = sscanf(argv[i]+16,"%"SCNd64,&sampleWarmup);
	if (ret<=0) sampleWarmup=0;
      }
      else if (!strncmp(argv[i],"--sample:first=",15)) {
	ret = sscanf(argv[i]+15,"%"SCNd64,&sampleFirst);
	if (ret<=0) sampleFirst=0;
      } 
      else if (!strncmp(argv[i],"--sample:max=",13)) {
	ret = sscanf(argv[i]+13,"%"SCNd64,&sampleMax);
	if (ret<=0) sampleMax=0;
      } 
      else if (!strcmp(argv[i],"--trace")) dotrace = 1;
      else if (!strcmp(argv[i],"--optrace")) dooptrace = 1;
      else if (!strcmp(argv[i],"--syscall")) dosyscall = 1;
      else if (!strcmp(argv[i],"--dump")) dodump = 1;

      else if (argv[i][0]!='-') break;
      else {
	fprintf(stderr,"Error: unrecognized argument\n");
	usage();
      }
      i++;
    }
  }
  if (i==argc && !readCheckpoints) { /* missing binary */
    fprintf(stderr,"Must have a binary name\n");
    usage();
  }

  if (readCheckpoints && writeCheckpoints) {
    fprintf(stderr,
	    "Cannot both read and write checkpoints simultaneously\n");
    usage();
  }

  /* make things nice for just doing --sample:first */
  if (sampleFirst && !samplePeriod && !sampleLength)
     samplePeriod = sampleLength = INT64_MAX;

  /* fprintf(stderr,"%lld %lld %lld\n",samplePeriod,sampleLength,tracecount); */

  LSE_emu_standalone_init();

  for (cno=1;cno<=numHWcontexts;cno++) {
    LSE_emu_create_context(cno); 
  }

  if (!readCheckpoints)  {
    
    rval = EMU_context_load(&LSE_emu_interface, 1, argc-i,
			    argv+i,cleanenv? cleanenvp : envp);
    if (rval) {
      fprintf(stderr,"Error while loading program\n");
      exit(1);
    }

    /* because it wasn't loaded when we started mapping it... */
    LSE_emu_addr_table[1]
      = EMU_get_start_addr(LSE_emu_hwcontexts_table[1].ctok);
  }

  if (gdbport) {
    EMU_gdb_init(gdbport);
  }

  /* deal with starting up checkpoint here... */
  if (writeCheckpoints) {
    char pbuf[50];

    cptFile = new LSE_chkpt::file_t(cptFileName,"w");
    if (!cptName) cptName = (char *)"unknown";

    cptFile->begin_header_write(cptName);

    switch (writeCheckpoints) {
    case 1 : /* full */
      cptControl.recordOS = FALSE;
      cptControl.incrementalMem = FALSE;
      break;
    case 2 : /* incremental */
      cptControl.recordOS = TRUE;
      cptControl.incrementalMem = TRUE; 
      break;
    case 3 : /* OS */
      cptControl.recordOS = TRUE;
      cptControl.incrementalMem = FALSE; 
      break;
    default  :
      cptControl.recordOS = FALSE;
      cptControl.incrementalMem = FALSE;
      break;
    }

    /* global parameters -- sort of standard here... */
    sprintf(pbuf, "SPERIOD=%" PRId64,samplePeriod);
    cptFile->add_globalparm(pbuf);
    sprintf(pbuf, "SLENGTH=%" PRId64,sampleLength);
    cptFile->add_globalparm(pbuf);
    sprintf(pbuf, "SWARMUP=%" PRId64,sampleWarmup);
    cptFile->add_globalparm(pbuf);
    sprintf(pbuf, "SFIRST=%" PRId64,sampleFirst);
    cptFile->add_globalparm(pbuf);

    EMU_chkpt_add_toc(&LSE_emu_interface, cptFile, "emu.0", 0, &cptControl);
    EMU_chkpt_add_toc(&LSE_emu_interface, cptFile, "emu.1", 1, &cptControl);
    cptFile->end_header_write();

    /* and actually run things... */

    icount = 0;
    sampleCount = 0;

    run_stuff(gdbport,FALSE);

    // and put out one more checkpoint for the end that does nothing so that
    // playback knows all threads have disappeared.
    start_checkpoint();
    end_checkpoint();

    cptFile->close();

    delete cptFile;
  } else if (readCheckpoints) {
    LSE_chkpt::error_t cerr;
    char *fileID, *str;
    
    cptFile = new LSE_chkpt::file_t(cptFileName,"r");
    cerr = cptFile->begin_header_read(&fileID);
    if (cerr) {
      fprintf(stderr,"Checkpoint error #%d\n",cerr);
      exit(cerr);
    }

    cptFile->get_globalparm(&str,FALSE); /* SPERIOD */
    sscanf(str+8,"%" SCNd64,&samplePeriod);
    cptFile->get_globalparm(&str,FALSE); /* SLENGTH */
    sscanf(str+8,"%" SCNd64,&sampleLength);
    cptFile->get_globalparm(&str,FALSE); /* SWARMUP */
    sscanf(str+8,"%" SCNd64,&sampleWarmup);
    cptFile->get_globalparm(&str,FALSE); /* SFIRST */
    sscanf(str+7,"%" SCNd64,&sampleFirst);

    /* change variables to run off of checkpoints */
    sampleFirst = 0;

    /* verify the table of contents */
    EMU_chkpt_check_toc(&LSE_emu_interface, cptFile,NULL,0,NULL,&cptControl);
    EMU_chkpt_check_toc(&LSE_emu_interface, cptFile,NULL,1,NULL,&cptControl);

    icount = 0;
    sampleCount = 0;

    cptFile->seek(1,SEEK_SET); /* first checkpoint */
    while (cptFile->more_checkpoints()) {

      cptFile->begin_checkpoint_read(&icount,NULL);

      cerr = EMU_chkpt_read_segment(&LSE_emu_interface, cptFile, NULL, 0,
				    &cptControl);
      if (cerr) {
	fprintf(stderr,"Checkpoint error #%d\n",cerr);
	exit(cerr);
      }

      cerr = EMU_chkpt_read_segment(&LSE_emu_interface, cptFile,NULL,1,
				    &cptControl);
      if (cerr) {
	fprintf(stderr,"Checkpoint error #%d\n",cerr);
	exit(cerr);
      }

      /* have to update address table... ugh! */
      for (i=1;i<=LSE_emu_hwcontexts_total;i++) {
	if (LSE_emu_hwcontexts_table[i].valid && 
	    LSE_emu_hwcontexts_table[i].ctok) 
	  LSE_emu_addr_table[i] 
	    = ((IA64_context_t *)LSE_emu_hwcontexts_table[i].ctok)->startaddr;
      }

      run_stuff(gdbport,TRUE);

      if (sampleMax > 0 && sampleCount >= sampleMax) {
	/* hit max -- will end */
	fprintf(stderr, "Hit max samples -- terminating\n");
	break;
      }

    }

    cptFile->close();

    delete cptFile;
  } else {

    icount = 0;
    sampleCount = 0;

    run_stuff(gdbport,FALSE);
    
  }

  /* and finish dealing with checkpoint here...*/

  if (gdbport) {
    EMU_gdb_finish();
  }

  fprintf(stderr,"Attempted %lld instructions\n", icount);
  EMU_finish(&LSE_emu_interface); 
  LSE_emu_standalone_finalize();

  destroy_cleanenv();

  return LSE_sim_exit_status;
}


static struct {
  int cno;
  boolean needDump;
} continuation;  /* continuation of where we were and all... */

/* way to run when in a fast-forward period.  Nothing can be printed and
 * the fastest modes are off
 * are off.  This is supposed to be the "blazing fast" mode.
 */
static void do_fast() {
  int cno; /* sometimes locals are faxster */
  LSE_emu_ctoken_t ctx;
  LSE_emu_addr_t addr;
  LSE_emu_instr_info_t ii;

  cno = continuation.cno;

  while (LSE_sim_terminate_count) {

    for (;cno<=LSE_emu_hwcontexts_total;cno++) {
      if (!(ctx=LSE_emu_hwcontexts_table[cno].ctok)) continue;

      addr = LSE_emu_addr_table[cno];
	
      icount++;
	
    tryagain:	
      EMU_init_instr(&ii,cno,ctx,addr);
	
      EMU_do_all_steps(&ii);

      /* unit 5 are continuations and thus not real instructions; we should
       * ignore them -- should never happen... */
      if (ii.extra.unit == 5) {
	LSE_emu_addr_table[cno] = addr = ii.addr = ii.next_pc;
	goto tryagain;
      }

      if (ii.privatef.actual_intr) {
	if (!ignorefault) EMU_dump_state(ctx,stderr,1);
	fprintf(stderr,
		"Fault encountered: %s at %"PRIx64"/%d "
		"(count=%d/%lld)\n",
		EMU_intr_string(ii.privatef.actual_intr),
		addr&~0x3,(int)addr&0x3,cno,
		icount);
	EMU_disassemble_addr(ctx,addr,stderr);
	if (!ignorefault) exit(1);
      } /* ii.privatef.actual_intr */

      if (LSE_emu_hwcontexts_table[cno].ctok == ctx)
	LSE_emu_addr_table[cno] = ii.next_pc;

      sampleToGo--;
      if (!sampleToGo) goto finish;

    } /* for cno */

    cno = 1;
  }
 finish:
  continuation.cno = ++cno;
}

/* way to run when in a non-fast-forward period.  Things can be printed out
 * and slower modes of operation, including gdb, can be used.
 */
static int do_slow(int gdbport) {
  int cno; /* sometimes locals are faxster */
  LSE_emu_ctoken_t ctx;
  LSE_emu_addr_t addr;
  LSE_emu_instr_info_t ii;
  int j;

  cno = continuation.cno;

  if (continuation.needDump) {
    int tcno;
    boolean done = FALSE;
    /* find the next mapped context so we can dump it ... */
    for (tcno = cno;tcno<=LSE_emu_hwcontexts_total && !done;tcno++) {
      if (!(LSE_emu_hwcontexts_table[tcno].ctok)) continue;
      done = TRUE;
      break;
    }
    if (!done) {
      for (tcno=1;tcno<cno && !done;tcno++) {
	if (!(LSE_emu_hwcontexts_table[tcno].ctok)) continue;
	done = TRUE;
	break;
      }
    }
    if (done) {
      EMU_dump_state(LSE_emu_hwcontexts_table[tcno].ctok,stderr,1);
      continuation.needDump = FALSE;
    }
  }

  while (LSE_sim_terminate_count) {

    for (;cno<=LSE_emu_hwcontexts_total;cno++) {
      if (!(ctx=LSE_emu_hwcontexts_table[cno].ctok)) continue;

      addr = LSE_emu_addr_table[cno];
      
      icount++;
      
    tryagain:	
      EMU_init_instr(&ii,cno,ctx,addr);
      
      if (!doseparate) {
	EMU_do_all_steps(&ii);
      } else {
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)0, FALSE);  /* ifetch */
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)1, FALSE);  /* decode */
	if (substep) { 
	  for (j=0;j<LSE_emu_max_operand_src;j++) 
	    EMU_fetch_operand(&ii,(LSE_emu_operand_name_t)j,FALSE);
	} else EMU_do_step(&ii,(LSE_emu_instrstep_name_t)2,FALSE);  /* opfetch */

        if (dosyscall && ii.iclasses.is_sideeffect)
          fprintf(stderr,"%d/%-10" PRIu64 " ", cno,icount-1);

	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)3, FALSE);   /* evaluate */
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)4, FALSE);   /* ldmemory */
	EMU_do_step(&ii,(LSE_emu_instrstep_name_t)5, FALSE);   /* format */
	if (substep) { 
	  for (j=0;j<LSE_emu_max_operand_dest;j++) 
	    EMU_writeback_operand(&ii,(LSE_emu_operand_name_t)j, FALSE);
	} else EMU_do_step(&ii,(LSE_emu_instrstep_name_t)6, FALSE);   /* writeback */
      }

      /* unit 5 are continuations and thus not real instructions; we should
       * ignore them */
      if (ii.extra.unit == 5) {
	LSE_emu_addr_table[cno] = addr = ii.addr = ii.next_pc;
	goto tryagain;
      }

      if (ii.privatef.actual_intr) {
	if (gdbport) {
	  switch (ii.privatef.actual_intr) {
	  case IA64_intr_singlestep:
	    /* update the PC to the next instruction */
	    if (LSE_emu_hwcontexts_table[cno].ctok == ctx)
	      LSE_emu_addr_table[cno] = ii.next_pc;
	    if (EMU_gdb_enter(5,cno)) {
	      gdbport = 0;
	      goto finish;
	    }
	    goto gdbchanged;
	    break;
	  case IA64_intr_breakinstruction:
	    if (EMU_gdb_enter(5,cno)) {
	      gdbport = 0;
	      goto finish;
	    }
	    goto gdbchanged;
	    break;
	  default:
	    fprintf(stderr,
		    "Fault encountered: %s at %"PRIx64"/%d "
		    "(count=%d/%lld)\n",
		    EMU_intr_string(ii.privatef.actual_intr),
		    addr&~0x3,(int)addr&0x3,cno,
		    icount);
	    EMU_disassemble_addr(ctx,addr,stderr);
	    if (EMU_gdb_enter(5,cno)) {
	      gdbport = 0;
	      goto finish;
	    }
	    goto gdbchanged;
	  } /* switch interrupt */
	} else { /* gdbport */
	  if (!ignorefault) EMU_dump_state(ctx,stderr,1);
	  fprintf(stderr,
		  "Fault encountered: %s at %"PRIx64"/%d "
		  "(count=%d/%lld)\n",
		  EMU_intr_string(ii.privatef.actual_intr),
		  addr&~0x3,(int)addr&0x3,cno,
		  icount);
	  EMU_disassemble_addr(ctx,addr,stderr);
	  if (!ignorefault) exit(1);
	} /* else gdbport */
      } /* ii.privatef.actual_intr */
      
      /* do the tracing */

      if (dotrace) {
	fprintf(stderr,"%d/%-10lld ", cno,icount-1);
	EMU_disassemble_addr(ctx,addr,stderr);
	if (dooptrace) {
	  IA64_print_instr_oper_vals(stderr,"+\t\t",&ii);
	} /* dooptrace */
      }

      if (LSE_emu_hwcontexts_table[cno].ctok == ctx)
	LSE_emu_addr_table[cno] = ii.next_pc;

      /* if (addrs[cno]==dumpaddr) dodump=1; */
      if (dodump) 
	EMU_dump_state(ctx,stderr,
		       (ii.extra.unit==2 && ii.extra.formatno!=215) ||
		       (ii.extra.formatno>=106 && ii.extra.formatno<=112) ||
		       ii.extra.formatno==118 ||
		       ii.extra.formatno==119 ||
		       ii.extra.formatno==121 ||
		       ii.extra.formatno==123);

      sampleToGo--;
      if (!sampleToGo) goto finish;

    gdbchanged:;

    } /* for cno */

    cno = 1;

  } /* while 1 */
 finish:
  continuation.cno = ++cno;
  return gdbport;
} /* do_slow */

static void start_checkpoint(void) {
  int i;

  /* have to update address table... ugh! */
  for (i=1;i<=LSE_emu_hwcontexts_total;i++) {
    if (LSE_emu_hwcontexts_table[i].valid && LSE_emu_hwcontexts_table[i].ctok) 
      ((IA64_context_t *)LSE_emu_hwcontexts_table[i].ctok)->startaddr =
	LSE_emu_addr_table[i];
  }

  /* at beginning of a "detailed" period; need to either dump state
   * or start recording
   */
  cptFile->begin_checkpoint_write(icount, cptCompress);
  EMU_chkpt_write_segment(&LSE_emu_interface, cptFile,"emu.0",0,&cptControl);
}

static void end_checkpoint(void) {
  EMU_chkpt_write_segment(&LSE_emu_interface, cptFile,"emu.1",1,&cptControl);
  cptFile->end_checkpoint_write();
}

static void run_stuff(int gdbport, boolean stopAfterOne) {
  boolean mustbeslow;
  boolean hitstart;
  int tc = 0;
  bool hms = false;

  /* set up sampling */
  if (samplePeriod) {
    if (sampleFirst) { /* skip first */
      if (sampleFirst > sampleWarmup) {
	sampleState = ffwd;
	sampleToGo = sampleFirst - sampleWarmup;
      } else {
	sampleState = warm;
	sampleToGo = sampleFirst;
      }
    } else { /* start right off */
      if (sampleLength) {
	sampleState = sample;
	sampleToGo = sampleLength;
      } else {
	sampleState = ffwd;
	sampleToGo = (samplePeriod - sampleLength - sampleWarmup);
	if (sampleToGo <= 0) sampleState = sample;
      }
    }
  } else {
    sampleState = (dotrace || dodump || doseparate || substep || forcesample
		   || dosyscall)
      ? sample : ffwd;
    sampleToGo = INT64_MAX;
  }
  hitstart = sampleState != ffwd;
  ((IA64_dinst_t *)LSE_emu_interface.etoken)->EMU_trace_syscalls 
    = (hitstart && dosyscall);
  ((IA64_dinst_t *)LSE_emu_interface.etoken)->EMU_do_rollback 
    = (hitstart && dorollback);
  continuation.needDump = (hitstart && dodump);

  if (hitstart && writeCheckpoints) {
    start_checkpoint();
  }

  /* Rather than set the single step flag, just call
     the gdb support routines directly.  This seems to fix an
     off-by-one error when dealing with shared objects and symbols. */
  /*if (gdbport) EMU_gdb_start_context(1);*/
  if (gdbport)
    if (EMU_gdb_enter(5,1)) 
      gdbport = 0;

  continuation.cno = 1;

  if (sampleReport) {
    LSE_emu_contextno_t cno = continuation.cno;
    if (!cno) cno = LSE_emu_hwcontexts_total - 1;
    /* handle state change stuff */
    if (LSE_emu_hwcontexts_table[cno].ctok) {
      fprintf(stderr,"Start in context %d in state %s; "
	      "total inst=%lld toGo=%lld "
	      "IP now %"PRIx64"\n",cno,
	      sampleStateNames[sampleState],icount,sampleToGo,
	      LSE_emu_addr_table[cno]);
    } else {
      fprintf(stderr,"Start in context %d in state %s; "
	      "total inst=%lld toGo=%lld context unmapped\n",
	      cno, sampleStateNames[sampleState],icount,sampleToGo);
    }
  }      

  mustbeslow = gdbport != 0 || sampleState != ffwd;

  while (LSE_sim_terminate_count) {

    if (mustbeslow) {
      gdbport = do_slow(gdbport);
      mustbeslow = gdbport != 0 || sampleState != ffwd;
    }
    else do_fast();

    /* check for sampler state changes... */
    hitstart = FALSE;

    while (sampleToGo <= 0) {
      switch (sampleState) {
      case ffwd:
	sampleState = warm;
	if (writeCheckpoints) start_checkpoint();
	if (dodump) continuation.needDump = TRUE;
	sampleToGo += sampleWarmup;
	break;
      case warm:
	if (sampleLength) { /* actually a special case */
	  sampleState = sample;
	  sampleToGo += sampleLength;
	} else {
	  sampleState = ffwd;
	  sampleToGo += (samplePeriod - sampleWarmup);
	}
	break;
      case sample:
      default:
	sampleToGo += (samplePeriod - sampleLength - sampleWarmup);
	/* end sampling */
	sampleState = ffwd;
	sampleCount++;

	if (writeCheckpoints) end_checkpoint();

	if (stopAfterOne) {
	  tc = LSE_sim_terminate_count;
	  LSE_sim_terminate_count=0;
	}
	if (sampleMax > 0 && sampleCount >= sampleMax) {
	  /* hit max -- will end */
	  fprintf(stderr, "Hit max samples -- terminating\n");
	  LSE_sim_terminate_count = 0;
	  hms = true;
	  goto finished;
	}
	break;
      } /* switch */

      ((IA64_dinst_t *)LSE_emu_interface.etoken)->EMU_trace_syscalls 
	= (sampleState != ffwd && dosyscall);
#ifdef DEBUG_RSE
      ((IA64_dinst_t *)LSE_emu_interface.etoken)->IA64_rsedebugprint 
	= (sampleState != ffwd);
#endif

      if (sampleReport) {
	LSE_emu_contextno_t cno = continuation.cno-1;
	if (!cno) cno = LSE_emu_hwcontexts_total - 1;

	/* handle state change stuff */
	if (LSE_emu_hwcontexts_table[cno].ctok) {
	  fprintf(stderr,"Switch context %d to state %s; "
		  "total inst=%lld toGo=%lld "
		  "IP now %"PRIx64"\n",cno,
		  sampleStateNames[sampleState],icount,sampleToGo,
		  LSE_emu_addr_table[cno]);
	} else {
	  fprintf(stderr,"Switch context %d to state %s; "
		  "total inst=%lld toGo=%lld context unmapped\n",
		  cno, sampleStateNames[sampleState],icount,sampleToGo);
	}
      }
      
      mustbeslow = gdbport != 0 || sampleState != ffwd;

    } /* while sampleTogo <= 0 */

  } /* while LSE_sim_terminate_count */

 finished:

  if (stopAfterOne) LSE_sim_terminate_count= tc;
  /* deal with final sample to finish... */
  if (sampleState != ffwd && writeCheckpoints) end_checkpoint();

  if (gdbport) 
    EMU_gdb_report_end((IA64_dinst_t *)LSE_emu_interface.etoken);

  return;

}
