typedef datatype:struct {
  xsrc:int;
  ysrc:int;
  xdest:int;
  ydest:int;
};

module node {
  using corelib;

  constrain('data_t, datatype);
  
  inport north_in : 'data_t;
  inport south_in : 'data_t;
  inport east_in : 'data_t;
  inport west_in : 'data_t;
  
  outport north_out : 'data_t;
  outport south_out : 'data_t;
  outport east_out : 'data_t;
  outport west_out : 'data_t;
  
  parameter node_generation_bandwidth = 1 : int;
  parameter node_accept_bandwidth = 1 : int;
  parameter arbiter_output_width = 1 : int;
  parameter width : int;
  parameter height : int;
  parameter x : int;
  parameter y : int;
  
  instance node_source : source; 
  instance node_sink : sink;

  instance input_arbiter : arbiter;
  instance sink_output_demux: demux;
  instance output_buffer:delay;

  instance north_aligner : aligner;
  instance south_aligner : aligner;
  instance east_aligner : aligner;
  instance west_aligner : aligner;
  instance switchbox:demux;

  LSS_connect_bus_EI(node_source.out, input_arbiter.in, 
		     node_generation_bandwidth);

  LSS_connect_bus_EI(north_in, input_arbiter.in, north_in.width);
  LSS_connect_bus_EI(south_in, input_arbiter.in, south_in.width);
  LSS_connect_bus_EI(east_in,  input_arbiter.in, east_in.width);
  LSS_connect_bus_EI(west_in,  input_arbiter.in, west_in.width);

  LSS_connect_bus(input_arbiter.out, sink_output_demux.in, 
		  arbiter_output_width);

  tee::connect_bus_from_tee_output(sink_output_demux.out, node_sink.in,
                                   arbiter_output_width, 2, 0);
  tee::connect_bus_from_tee_output(sink_output_demux.out, output_buffer.in, 
                                   arbiter_output_width, 2, 1);

  LSS_connect_bus(output_buffer.out, switchbox.in, arbiter_output_width);

  tee::connect_bus_from_tee_output(switchbox.out, north_aligner.in, 
                                   arbiter_output_width, 4, 0);
  tee::connect_bus_from_tee_output(switchbox.out, south_aligner.in, 
                                   arbiter_output_width, 4, 1);
  tee::connect_bus_from_tee_output(switchbox.out, east_aligner.in,
                                   arbiter_output_width, 4, 2);
  tee::connect_bus_from_tee_output(switchbox.out, west_aligner.in,
                                   arbiter_output_width, 4, 3);

  LSS_connect_bus(north_aligner.out, north_out, north_out.width);
  LSS_connect_bus(south_aligner.out, south_out, south_out.width);
  LSS_connect_bus(east_aligner.out, east_out, east_out.width);
  LSS_connect_bus(west_aligner.out, west_out, west_out.width);

  input_arbiter.comparison_func = 
    arbiter::round_robin_comparison_func("pri");
  input_arbiter.extension = 
    arbiter::round_robin_extension("pri");
  input_arbiter.end_of_timestep =
    arbiter::round_robin_end_of_timestep("pri");

  node_source.funcheader = <<<
    #include <stdlib.h>
  >>>;

  node_source.create_data = <<<{
    int xcoord,ycoord;
    if(((random() % 2) == 0)) {
      xcoord = random()%${width};
      ycoord = random()%${height};
      data->xdest = xcoord;
      data->ydest = ycoord;
      data->xsrc = ${x};
      data->ysrc = ${y};
      return LSE_signal_something | LSE_signal_enabled;
    } else {
      return LSE_signal_nothing | LSE_signal_disabled;
    }
  }>>>;

  sink_output_demux.choose_logic = <<<{
    if((data->xdest == ${x}) &&
       (data->ydest == ${y})) {
	 return 0;
       }  
    return 1;
  }>>>;

  node_source.funcheader = <<<
    #include <stdlib.h>
  >>>;
  switchbox.choose_logic = <<<{
    int e_horiz = (${width} + data->xdest - ${x}) % ${width};
    int w_horiz = (${width} + ${x} - data->xdest) % ${width};
    int s_vert = (${height} + data->ydest - ${y}) % ${height};
    int n_vert = (${height} + ${y} - data->ydest) % ${height};
    
    fprintf(stderr, "b%d%d (from b%d%d) at b${y}${x}: %d %d %d %d\n",
	    data->ydest, data->xdest, data->ysrc, data->xsrc,
	    e_horiz, w_horiz, n_vert, s_vert);
    if(e_horiz && n_vert) {
      if(random()% 2) {
	return (e_horiz > w_horiz) ? 3 : 2;
      } else {
	return (n_vert > s_vert) ? 1 : 0;
      }
    } else if(e_horiz) {
      return (e_horiz > w_horiz) ? 3 : 2;
    } else if(n_vert) {
      return (n_vert > s_vert) ? 1 : 0;
    } else {
      /* Huh, just go north */
      return 0;
    }
  }>>>;

  output_buffer.pass_acks_when_full=FALSE;

  fun rgb(r : int, g : int, b : int) => int {
    return (r << 16) + (g << 8) + b;
  };

  var colors = {rgb(255,0,0), rgb(255,127,0), rgb(127,127,127),
		rgb(255,255,0), rgb(0,255,0), rgb(0,127,127),
		rgb(255,0,255), rgb(0,255,255), rgb(0,0,255)} : int[];

  lvlString = <<<
    ${this}.Width=int 120
    ${this}.Height=int 130
    ${this}.Label Placement=string "top"
    ${this}.Show Table=bool true
    ${this}.Show Table Headers=bool false
    ${this}.Table Font Size=int 22
    ${this}.Table Rows=int 1
    ${this}.Row Height=int 18
    ${this}.Table Columns=int 1
    ${this}.Column Width=int 95
    ${this}.Label Font Size=int 24

    ${this}.Fill Color=color ${colors[3 * y + x]}

    ${this}.north_in.X Coordinate=int 30
    ${this}.north_in.Y Coordinate=int -5
    ${this}.north_in.Connect Site=string "North"
    ${this}.north_in.Label=string ""

    ${this}.north_out.X Coordinate=int 88
    ${this}.north_out.Y Coordinate=int -5
    ${this}.north_out.Connect Site=string "North"
    ${this}.north_out.Label=string ""

    ${this}.south_in.X Coordinate=int 88
    ${this}.south_in.Y Coordinate=int 131
    ${this}.south_in.Connect Site=string "South"
    ${this}.south_in.Label=string ""

    ${this}.south_out.X Coordinate=int 30
    ${this}.south_out.Y Coordinate=int 131
    ${this}.south_out.Connect Site=string "South"
    ${this}.south_out.Label=string ""

    ${this}.east_in.X Coordinate=int 121
    ${this}.east_in.Y Coordinate=int 81
    ${this}.east_in.Connect Site=string "East"
    ${this}.east_in.Label=string ""

    ${this}.east_out.X Coordinate=int 121
    ${this}.east_out.Y Coordinate=int 35
    ${this}.east_out.Connect Site=string "East"
    ${this}.east_out.Label=string ""

    ${this}.west_in.X Coordinate=int -5
    ${this}.west_in.Y Coordinate=int 35
    ${this}.west_in.Connect Site=string "West"
    ${this}.west_in.Label=string ""

    ${this}.west_out.X Coordinate=int -5
    ${this}.west_out.Y Coordinate=int 81
    ${this}.west_out.Connect Site=string "West"
    ${this}.west_out.Label=string ""
  >>>;
};
