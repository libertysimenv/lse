#!/bin/bash

LFSR_FILES='lfsr/lfsr.lss lfsr/xor.lss'
TORUS_FILES='torus/torus.lss torus/block.lss'
IA64_FILES='ia64/ia64singlecycle.lss ia64/ia64simplemulti.lss ia64/ia64multicycle.lss ia64/ia64_newid.lss ia64/delayn.lss'

A2PS_OPTIONS='--header=" " --left-footer="" --footer=" "  --right-footer="" --left-title="" '

FILES=$(find -name "*.lss" -and -not -path "*demo*" -and -not -path "*pipeline_stall*")

/bin/sh -c "a2ps $A2PS_OPTIONS $LFSR_FILES -o lfsr.ps"
/bin/sh -c "a2ps $A2PS_OPTIONS $TORUS_FILES -o torus.ps"
/bin/sh -c "a2ps $A2PS_OPTIONS $IA64_FILES -o ia64.ps"


