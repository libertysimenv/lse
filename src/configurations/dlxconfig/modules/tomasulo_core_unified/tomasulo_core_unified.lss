module tomasulo_core_unified {
  using corelib;
  
  include "util.lss";
  
  LSE_debug_dynid_limit = 1000;

  /********* DLX specific modules **************/
  // Include DLX support modules.  Some of these should be moved to archlib
  using dlxlib;
  //Configuration specific modules
  include "spec_latches.lss"; // wrapped up latches for pipe stages and
  // the resstations

  include "fus.lss"; /* Functional units, integer alu, shifter, etc. */

  /*********************************************/

  /* Ports */
  outport dmem_request : mem_request_t;
  inport  dmem_result  : mem_response_t;

  /* Parameters */
  runtimeable parameter imem_input_file : string;
              parameter imem_size = 4096        /* 4 KB */ : int;
  runtimeable parameter initial_pc          : int;
              parameter num_phys_regs = 128 : int;

  /* This should be a parameter once the decoder can handle this correctly */
              var num_isa_regs = 32:const int;

  instance imem:imem; // Split i and dmem, no caches yet.
  imem.input_file = imem_input_file;
  imem.size=imem_size;

  instance pc:pc;
  pc.initial_pc = initial_pc;

  instance reg_write_control:reg_write_control;
  instance rfile:regfile;
  instance rntable:spec_rename_table; 

  instance fetch_stage:fetch_stage;

  instance fetch_decode_latch:fetch_decode_latch;

  instance decstage:decode_stage;

  instance ialu_resstation:operand_reservation_station;
  instance shifter_resstation:operand_reservation_station;
  instance branch_resstation:spec_reservation_station;

  instance lsu:lsu; 
  instance ialu:integer_alu;
  instance shifter:shifter;
  instance branchunit:branchunit;

  instance wb_arbiter:arbiter;
  instance ex_wb_latch:dropblockflop;

  instance commit_stage:commit_stage;

  // structure sizes
  commit_stage.num_rob_entries=num_phys_regs - num_isa_regs; 
  rfile.num_regs=num_phys_regs;        // Change num_phys_regs above, not this!
  rntable.num_isa_regs=num_isa_regs;   // Do not change this, will break things
  rntable.num_phys_regs=num_phys_regs; // Change num_phys_regs above, not this!


  /********************** Main Datapath connections **********************/
  pc.out -> fetch_stage.pc_in;
  fetch_stage.out -> fetch_decode_latch.in;
  fetch_stage.npc_out -> pc.in;

  fetch_decode_latch.out -> decstage.instr_in;

  instance instr_fo:tee;
  instance hole:sink;
  instance idist:instr_dispersal;

  decstage.all_instr_out -> instr_fo.in;
  instr_fo.out -> commit_stage.rob_instr_in;
  instr_fo.out -> idist.in;
  idist.out[0] -> ialu_resstation.instr_in;
  idist.out[1] -> shifter_resstation.instr_in;
  idist.out[2] -> branch_resstation.instr_in;
  idist.out[3] -> hole.in;

  decstage.instr_out[0] -> hole.in;
  decstage.instr_out[1] -> hole.in;
  decstage.instr_out[2] -> hole.in;
  decstage.instr_out[3] -> lsu.instr_in;

  instance shifter_pipeback_writer:operand_pipeback_writer;
  instance ialu_pipeback_writer:operand_pipeback_writer;

  branch_resstation.instr_out -> branchunit.instr_in;

  shifter_resstation.op1 -> shifter.op1;
  shifter_resstation.op2 -> shifter.op2;
  shifter_resstation.op -> shifter.op;
  shifter.result -> shifter_pipeback_writer.result_in;
  shifter_resstation.pf_out -> shifter_pipeback_writer.pf_in;

  ialu_resstation.op1 -> ialu.op1;
  ialu_resstation.op2 -> ialu.op2;
  ialu_resstation.op -> ialu.op;
  ialu.result -> ialu_pipeback_writer.result_in;
  ialu_resstation.pf_out -> ialu_pipeback_writer.pf_in;

  ialu_pipeback_writer.out[0]    -> wb_arbiter.in[0];
  shifter_pipeback_writer.out[0] -> wb_arbiter.in[1];
  branchunit.result              -> wb_arbiter.in[2];
  lsu.instr_out                  -> wb_arbiter.in[3];

  wb_arbiter.out -> ex_wb_latch.in[0];

  instance cdb_fanout:tee;
  ex_wb_latch.out[0] -> cdb_fanout.in[0];

  cdb_fanout.out -> reg_write_control.cdb_in;
  reg_write_control.write_addr_out -> rfile.write_addr;
  reg_write_control.write_data_out -> rfile.write_data;
  cdb_fanout.out -> ialu_resstation.cdb_in;
  cdb_fanout.out -> shifter_resstation.cdb_in;
  cdb_fanout.out -> lsu.cdb_in;
  cdb_fanout.out -> rntable.cdb_in;
  cdb_fanout.out -> branch_resstation.cdb_in;
  cdb_fanout.out -> commit_stage.cdb_in;


  /* Connections used if committing, not when squashing or resolving **/
  commit_stage.write_entry_addr -> rntable.write_entry_addr;
  commit_stage.write_entry_reg -> rntable.write_entry_reg;
  commit_stage.commit_store -> lsu.commit_store; 

  /*************** Other connections by stage ****************/
  /********** Fetch Stage ***********/
  fetch_stage.imem_request -> imem.request;
  imem.result -> fetch_stage.imem_data_in;

  /*********************************/

  /***** Decode stage connections */
  instance entry_reg_out_fo:tee;
  instance extract_physreg:converter;

  extract_physreg.convert_func=<<<
    return data >> 1; /* Low order bit is register status 
			 (1 meaning writer has finished) */
  >>>;

  decstage.rename_table_write -> rntable.spec_alloc_entry_addr;

  /* Get a physical register for this instruction */
  LSS_connect_bus(rntable.alloc_physreg_out, decstage.phys_dest_register,1);

  LSS_connect_bus(decstage.register_read_requests,rntable.read_entry,2);
  LSS_connect_bus(rntable.entry_out,entry_reg_out_fo.in,2);

  /* Register file read and returned data connections */
  tee::connect_bus_from_tee_output(entry_reg_out_fo.out, extract_physreg.in,
				   2,2,0);
  LSS_connect_bus(extract_physreg.out, rfile.read_addr,2);
  LSS_connect_bus(rfile.read_out, decstage.register_read_results, 2);

  /* Rename information into decstage */
  tee::connect_bus_from_tee_output(entry_reg_out_fo.out, 
				   decstage.rename_read_results,
				   2,2,1);
  /*******************************/


  /**** Ex stage connections *****/
  ialu_resstation.num_entries = 6;
  shifter_resstation.num_entries = 4;
  branch_resstation.num_entries = 3;

  lsu.num_lsq_entries=5;
  lsu.num_pending_loads=2;
  lsu.mem_request_out -> dmem_request;
  dmem_result -> lsu.mem_data_in;

  wb_arbiter.comparison_func = <<<
    static LSE_time_t last_time = LSE_time_construct(0,0);
  static int last_choice=0;
  int dist1,dist2;
  int choice;

  if(LSE_time_ne(last_time, LSE_time_now)) {
    last_choice++;
    last_time=LSE_time_now;
  }
  choice=last_choice%LSE_port_width(in);
  dist1 = ((port1 - choice) > 0)?(port1-choice):
    (port1-choice+LSE_port_width(in));
  dist2 = ((port2 - choice) > 0)?(port2-choice):
    (port2-choice+LSE_port_width(in));

  if(LSE_signal_extract_data(status1) == LSE_signal_nothing) {
    return 1;
  } else if(LSE_signal_extract_data(status1) == LSE_signal_unknown) {
    return -1;
  } else if(LSE_signal_extract_data(status2) == LSE_signal_unknown) {
    return -1;
  } else if(LSE_signal_extract_data(status2) == LSE_signal_nothing) {
    return 0;
  } else {
    if(dist2 < dist1) return 1;
    else return 0;
  }
  >>>;
  /***********************/

  /**** Write back stage connections */

  // This space left intentionally blank, except for this comment ;)

  /************************************/

  /**** Commit stage connections ***/
  instance clear_pipe_fo:tee;

  commit_stage.redirect_fetch -> fetch_stage.use_npc_in;
  commit_stage.correct_npc -> fetch_stage.npc_in;
  commit_stage.clear_pc -> pc.drop_control;
  commit_stage.stall_fetch -> pc.block_control;
  commit_stage.stall_decode -> fetch_decode_latch.block_control;
  commit_stage.clear_fd_latch -> fetch_decode_latch.drop_control;
  commit_stage.clear_sbuf -> lsu.sbuf_clear;

  commit_stage.clear_pipeline -> clear_pipe_fo.in;
  clear_pipe_fo.out -> lsu.exmem_drop_control;
  clear_pipe_fo.out -> ialu_resstation.clear;
  clear_pipe_fo.out -> shifter_resstation.clear;
  clear_pipe_fo.out -> branch_resstation.clear;
  clear_pipe_fo.out -> lsu.lsq_clear;
  clear_pipe_fo.out -> ex_wb_latch.drop_control;

  commit_stage.reg_to_free -> rntable.free_reg;

  lsu.write_req_q_is_empty -> commit_stage.write_req_q_is_empty;

};
