subpackage decoder {
  typedef decoded_instr_t:struct {
    op:uint32; /* Hmm, now the enum
		  in instructions.h
		  needs to be an LSS
		  type too.  What is
		  the best way to
		  handle this? */
    rop1:int;
    rop2:int;
    rd:int;
    sa:int;
    imm:int;
    joff:int;
  };
}

module decoder {
  using corelib;

  inport in:DLX_data_t;
  outport out:decoder::decoded_instr_t;

  instance m:converter;

  if(in.width != out.width) {
    punt("in and out must have the same width");
  }

  LSS_connect_bus(in,m.in,in.width);
  LSS_connect_bus(m.out,out,out.width);

  m.funcheader = <<<
#include <instructions.h>
  >>>;

  m.convert_func = <<<
    DLX_ops op;
    DLX_iformat format;
    ${decoder::decoded_instr_t} ret;

    uint32_t instr=data;

    int rop1= -1,
        rop2= -1, 
        rd= -1, 
        sa= -1,
        imm= -1,
        joff= -1;

    op = DLX_decode_op(instr);
    if(op == -1) {
      LSE_report_err("Could not decode op.  id=0x%x",id);
    }
    rop1=DLX_get_register_operand1(op,instr);
    rop2=DLX_get_register_operand2(op,instr);
    rd=DLX_get_register_dest(op,instr);
    sa=DLX_get_sa(op,instr);
    imm=DLX_get_imm(op,instr);
    joff=DLX_get_offset(op,instr);

    ret.op=op;
    ret.rop1=rop1;

    switch(op) {
    case SB:
    case SH:
    case SW:
      ret.rop2=rd;
      ret.rd= -1;
      break;
    case J:
    case JR:
    case BEQZ:
    case BNEZ:
      ret.rd= -1;
      break;
    default:
      ret.rop2=rop2;
      ret.rd=rd;
      break;
    }
    ret.sa=sa;
    ret.imm=imm;
    ret.joff=joff;
    return ret;
  >>>;
};
