/*%
 * 
 * This is a one line description of the file.  
 *
 * Authors: Author Name <email-addr@yourdomain.edu>
 *
 * This is a long description of the file that is usually one or two
 * paragraphs in length.  After reading it, one should be able to
 * understand the purpose of the code in this file without looking at
 * anything else.
 *
 */

subpackage simplemem {
  typedef bulk_operation_func_t:userpoint(<<<int instno, 
                                             LSE_signal_t status,
                                             LSE_dynid_t id,
                                             LSE_port_type(bulk_oper) *data,
                                             unsigned char *memory,
                                             LSE_dynid_t *id_array>>>
                                          =><<<void>>>);
  var bulk_clear_func=<<<
    int i;
    if(LSE_signal_extract_data(status) == LSE_signal_something &&
       LSE_signal_extract_enable(status) == LSE_signal_enabled) {
      for(i=0;i<PARM(size);i++) {
        memory[i]=0;
      }
    }
  >>>:const bulk_operation_func_t;

  var bulk_clear_control=<<<
      return LSE_signal_extract_data(istatus) |
             LSE_signal_extract_enable(istatus) | 
             LSE_signal_ack;
  >>>:string;

  typedef request_type_t:enum {read_request, write_request};

  typedef request_t:struct {
    addr:int; /* This should be LSE_emu_addr_t but we still
                 have no-emulator issues here */
    request_type:request_type_t;
    write_data:int; /* Broken type here */     
    write_mask:int; /* Broken type here */
  };
};

module simplemem {
  using dlxlib.simplemem;

  tar_file = "simplemem";

  method get_memory_contents:(<<<int addr>>> => <<<unsigned char>>>);

  /* Update the next few parameter values to indicate whether or not your
     module uses a phase_start,phase, or phase_end function */
  phase_start = FALSE;
  phase = FALSE;
  phase_end = TRUE;

  export simplemem::request_type_t as request_type_t;

  /* Update the next parameter to indicate whether or not your module
     reacts only to its inputs */
  reactive = TRUE;

  /* Define parameters (basic and algorithmic) here.
   *
   * The syntax is:
   *   parameter PARMNAME = DEFAULTVALUE : TYPE;
   * Note the default value is optional.
   *
   * Examples:
   *   parameter unblock_at_receive = TRUE : boolean;
   *   parameter allow_to_pass_func : userpoint(
   *                "int instno, LSE_dynid_t id, LSE_port_type(src) *data" => 
   *                "int");
   */

   parameter store_and_report_ids=FALSE:boolean;
   parameter size:int; /* Size in bytes.  We need a bigger type here! */
   parameter addr_unit=1:int; /* word accessed is at addr_unit*addr */

   if((size % addr_unit) != 0) {
     punt("Sorry, size must be a multiple of the addr_unit");
   }

   /* function to initialize memory */	
   parameter init_func=<<< return; >>>:userpoint(<<<unsigned char *memory, int size>>>=><<<void>>>); 
   
   parameter init_id_func=<<< 
     int i;
     for(i=0;i<len;i++) {
       id_array[i]=NULL;
     }
   >>>:userpoint(<<<LSE_dynid_t *id_array, int len>>>=><<<void>>>); 

  /* Doesn't work yet */
  // parameter rwsize:int; /* How much data is read and written in one cycle */

   parameter use_write_mask=FALSE:boolean; /* Write the whole data if
                                              FALSE, use the mask to decide 
                                              writes if TRUE */
   parameter bulk_operation_func=<<< return; >>>:simplemem::bulk_operation_func_t;

   export request_t as request_t;

   inport request:request_t;
   inport bulk_oper:*; /* For things like flash clear, etc. */
   outport result:int; /* This type is really broken too */


   if(request.width != result.width) {
     punt("Error: request and result must have the same width\n");
   }

   bulk_oper.handler=FALSE;
   bulk_oper.independent=TRUE;

   request.handler=TRUE;
   result.handler=TRUE;
};
