module reg_accum {
  using corelib;

  inport in:pipefront_reg_t;
  outport out:pipefront_reg_t;

  inport rop1:int;
  inport rop2:int;

  instance body:combiner;

  body.inputs={"in","rop1","rop2"};
  body.outputs={"out"};
  body.combine_calculates_enable=TRUE;
  body.wait_for_data=FALSE;
  body.require_all_data_present=FALSE;
  body.propagate_nothing=FALSE;
  body.NACK_nothing=FALSE;  

  if(in.width != out.width) {
    punt("in and out must have the same width");
  }

  if(in.width != rop1.width) {
    punt("in and rop1 must have the same width");
  }

  if(rop2.width != rop1.width) {
    punt("rop2 and rop1 must have the same width");
  }

  LSS_connect_bus(in,body.in,in.width);
  LSS_connect_bus(rop1,body.rop1,in.width);
  LSS_connect_bus(rop2,body.rop2,in.width);

  LSS_connect_bus(body.out,out,in.width);

  body.funcheader = <<<
#include <instructions.h>
  >>>;

  body.combine = <<<
    int32_t *tmp_rop1,*tmp_rop2;
    uint32_t *rop1,*rop2;
    uint32_t imm;
    boolean need_op1, need_op2, rop1_ok, rop2_ok;
    DLX_iformat format;
    
    tmp_rop1=rop1_data;
    tmp_rop2=rop2_data;

    // Hopefully this prevents triggering of a gcc "bug".  Damn ANSI
    // standards committee.
    rop1=(uint32_t *)tmp_rop1; rop2=(uint32_t *)tmp_rop2; 

    if(LSE_signal_extract_data(in_status) == LSE_signal_unknown) {
      *out_status = LSE_signal_unknown;
      return;
    }

    if(LSE_signal_extract_data(in_status) == LSE_signal_nothing) {
      *out_status = LSE_signal_nothing | LSE_signal_enabled;
      if(LSE_signal_extract_data(rop1_status) == LSE_signal_something) {
         LSE_report_err("Something wrong, in missing, rop1 present\n");
      }
      if(LSE_signal_extract_data(rop2_status) == LSE_signal_something) {
         LSE_report_err("Something wrong, in missing, rop2 present\n");
      }
      return;
    }

    LSE_data_copy(${pipefront_reg_t},out_data,in_data);

    format = DLX_get_format_by_op(static_cast<DLX_ops>(in_data->iinfo.op));
    switch(format) {
      case RType:
        need_op1 = TRUE;
        rop1_ok=FALSE;
        switch(in_data->iinfo.op) {
        case SLLI:
        case SRLI:
        case SRAI:
          need_op2 = FALSE;
          rop2_ok = TRUE;
          break;
        default:
          need_op2 = TRUE;
          rop2_ok=FALSE;
          break;
        }
        break;
      case FRType:
        need_op1 = TRUE;
        rop1_ok=FALSE;
        need_op2 = TRUE; /* Incorrect for shift ops, please fix */
        rop2_ok=FALSE;
        break;
      case IType:
	switch(in_data->iinfo.op) {
        case LHI:
          need_op1 = FALSE;
          rop1_ok = TRUE;
          need_op2 = FALSE;
          rop2_ok = TRUE;
          break;
        case SW:
        case SH:
        case SB:
          need_op1 = TRUE;
          rop1_ok=FALSE;
          need_op2 = TRUE;
          rop2_ok=FALSE;
          break;
        default:
          need_op1 = TRUE;
          rop1_ok=FALSE;
          need_op2 = FALSE;
          rop2_ok=TRUE;
          break;
        }
        break;
      case FIType:
        need_op1 = TRUE;
        rop1_ok=FALSE;
        need_op2 = FALSE;
        rop2_ok=TRUE;
        break;
      case JType:
        need_op1 = FALSE;
        rop1_ok=TRUE;
        need_op2 = FALSE;
        rop2_ok=TRUE;
        break;
    }

    if(need_op1) {
      if(LSE_signal_extract_data(rop1_status) == LSE_signal_nothing) {
        LSE_report_err("Need rop1 but is absent\n");
      }
      if(LSE_signal_extract_data(rop1_status) == LSE_signal_unknown) {
        *out_status = LSE_signal_unknown;
        return;
      }
      rop1_ok=TRUE;
    }

    if(need_op2) {
      if(LSE_signal_extract_data(rop2_status) == LSE_signal_nothing) {
        LSE_report_err("Need rop2 but is absent");
      }
      if(LSE_signal_extract_data(rop2_status) == LSE_signal_unknown) {
        *out_status = LSE_signal_unknown;
        return;
      }
      rop2_ok=TRUE;
    }
    
    if(rop1_ok && rop2_ok) { /* We got all our inputs */
      switch(format) {
      case RType:
        switch(in_data->iinfo.op) {
        case SRLI:
        case SRAI:
	  out_data->op1_value= *rop1;
	  out_data->op2_value=in_data->iinfo.sa;
          break;
        default:
	  out_data->op1_value= *rop1;
	  out_data->op2_value= *rop2;
          break;
        }
        break;
      case FRType:
	out_data->op1_value= *rop1;
	out_data->op2_value= *rop2;
        break;
      case IType:
        switch(in_data->iinfo.op) {
        case LHI:
	  out_data->op1_value=in_data->iinfo.imm;
          break;
        case SW:
        case SH:
        case SB:
	  out_data->op1_value= *rop1;
	  out_data->op2_value= *rop2;
	  out_data->op3_value=in_data->iinfo.imm;
          break;

          break;
        default:
	  out_data->op1_value= *rop1;
	  out_data->op2_value=in_data->iinfo.imm;
          break;
        }
        break;
      case FIType:
	out_data->op1_value= *rop1;
	out_data->op2_value=in_data->iinfo.imm;
        break;
      case JType:
	out_data->op1_value=in_data->iinfo.joff;
        break;
      } 
      *out_id=in_id;
      *out_status = LSE_signal_something | 
                    LSE_signal_extract_enable(in_status);
    }
  >>>;

};
