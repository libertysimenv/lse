#include "instructions.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <netinet/in.h>

int main(int argc, char *argv[])
{
  uint32_t instr;
  DLX_ops op;

  if(argc == 2) {
    instr=strtoul(argv[1],NULL,0);
    printf("Read instr=%x ",instr);
    op=DLX_decode_op(instr);
    printf("op=%x, opstr=%s\n",op,DLX_op2str[op]);
    DLX_print_instruction(stdout, instr);
  } else {
    while(!feof(stdin)) {
      fread(&instr, 4, 1, stdin);
      instr = htonl(instr);
      DLX_print_instruction(stdout, instr);
    }
  }

  return 0;
}
