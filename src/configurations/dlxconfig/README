This directory contains models of several DLX processors.  The models
current include:

1) tomasulodlx_spec - a speculative Tomasulo style out-of-order
                      processor with distributed reservation stations

2) tomasulodlx_spec_unified - a speculative Tomasulo style out-of-order
                              processor with a single unified
                              reservation station

More detailed information about the models is in the doc/ directory.

-----------------------------------------------------------------------------

To build the models, use make.  The Makefile defines the following targets:

1) tomasulodlx_spec - builds the model in tomasulodlx_spec.lss

2) tomasulodlx_spec_clean - performs a clean (non-incremental) build
                            of the model in tomasulodlx_spec.lss

3) tomasulodlx_spec_nomod - builds the model in tomasulodlx_spec.lss
                            without reinstalling the modules in the
                            modules directory

4) tomasulodlx_spec_unified - builds the model in
                              tomasulodlx_spec_unified.lss

5) tomasulodlx_spec_unified_clean - performs a clean (non-incremental)
                                    build of the model in
                                    tomasulodlx_spec_unified.lss

6) tomasulodlx_spec_unified_nomod - builds the model in
                                    tomasulodlx_spec_unified.lss
                                    without reinstalling the modules
                                    in the modules directory

-----------------------------------------------------------------------------

To visualize the configs, first make sure the modules are built.  This
can be done by going into the modules directory and typing "make
install".  Then run:

visualizer --mpathbeg modules/modulelib configname

For example:

visualizer --mpathbeg modules/modulelib tomasulodlx_spec.lss


Enjoy!

NOTE: files in the dlx_no_pipeline and old-configs are not up-to-date
      with versions of LSE after changing to C++

