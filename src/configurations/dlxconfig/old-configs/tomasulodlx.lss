include "flop.lss";
include "sink.lss";
include "source.lss";
include "demux.lss";
include "datasource.lss";
include "tee.lss";
include "typecast.lss";
include "arbiter.lss";
include "filter.lss";
include "router.lss";

using dlxlib;

include "util.lss";

include "fus.lss"; /* Functional units, integer alu, shifter, etc. */

SIM_show_port_statuses=TRUE;
SIM_require_live_contexts=FALSE;

/********* DLX specific modules **************/
module drop_block_gates {
  include "gate.lss";

  inport in:int;
  outport out:int;

  inport drop_control:int;
  inport block_control:int;

  instance block_gate:gate;
  instance drop_gate:gate; /* Drops instruction if branch marked as taken */

  block_gate.gate_style = blocker;
  drop_gate.gate_style = filter;

  drop_gate.gate_control = <<< 
    LSE_signal_t cstatus;
    LSE_dynid_t cid;
    LSE_port_type(control) *cdata;

    cstatus = LSE_port_get(control[0],&cid,&cdata);
    if(!LSE_signal_data_known(cstatus)) return -1;

    if(LSE_signal_data_present(cstatus)) {
      if(*cdata) return 0;
      else return 1;
    } else {
      return 1;
    }
  >>>;

  block_gate.gate_control = <<<
    LSE_signal_t cstatus;
    LSE_dynid_t cid;
    LSE_port_type(dest) *cdata;
    cstatus = LSE_port_get(control[0],&cid,&cdata);

    if(!LSE_signal_data_known(cstatus)) {
      return -1;
    }

    if(LSE_signal_data_present(cstatus)) {
      return *cdata;
    } 
  >>>;

  in -> drop_gate.src;
  drop_gate.dest -> block_gate.src;
  block_gate.dest -> out;

  block_control -> block_gate.control[0];
  drop_control -> drop_gate.control[0];
};

module decode_stage {
  var i:int;

  inport instr_in:int;

  outport instr_out:none;
  outport rop1_request:int;
  outport rop2_request:int;
  inport rop1_in:int;
  inport rop2_in:int;

  inport cdb_in:int; /* Destination register being written on CDB */

  instance id2addr:typecast;
  id2addr.typecast_func = <<< 
    return LSE_emu_dynid_get(id,extra.rd);
  >>>;

  instance decoder:decoder;
  instance rreq:reg_requester;
  instance raccum:reg_accum;
  instance rnaccum:ren_accum;  

  instance block_rename_write:filter;
  instance rntable:rename_table; 

  instance rop1_tee:tee;
  instance rop2_tee:tee;
  instance decoder_fanout:tee;
  instance idist:demux;

  if(rop1_request.width != rop2_request.width) {
    punt("rop1 and rop2 request ports must have the same width\n");
  }

  if(instr_in.width != rop2_request.width) {
    punt("rop1 and rop2 request ports must have the same width as instr_in\n");
  }

  if(instr_in.width != 1) {
    punt("Sorry, decode_stage only supports one decode per cycle right now due to demux userpoint implemenation.  Please Fix");
  }

  rntable.num_regs=32;

  instance get_rd:typecast;
  get_rd.typecast_func = <<< return LSE_emu_dynid_get(id,extra.rd); >>>;

  LSS_connect_bus(instr_in, decoder.in, instr_in.width);

  decoder.out -> decoder_fanout.src;

  block_rename_write.ack_uses_filter=FALSE;
  block_rename_write.allow_to_pass_func=<<<
    if(LSE_emu_dynid_get(id,extra.rd) <= 0) return 0;
    return 1;
  >>>;

  decoder_fanout.dest -> rreq.in;
  decoder_fanout.dest -> get_rd.src;
  get_rd.dest -> block_rename_write.src;
  block_rename_write.dest -> rntable.write_entry_addr;

  rreq.out -> raccum.in;

  LSS_connect_bus(cdb_in,id2addr.src,cdb_in.width);

  LSS_connect_bus(rop1_in,raccum.rop1, rop1_in.width);
  LSS_connect_bus(rop2_in,raccum.rop2, rop2_in.width);

  LSS_connect_bus(raccum.out,rnaccum.in,instr_in.width);

  LSS_connect_bus(rreq.rop1,rop1_tee.src,rop1_request.width);
  LSS_connect_bus(rreq.rop2,rop2_tee.src,rop2_request.width);

  TEE_connect_bus_from_tee_output(rop1_tee.dest, 
                                  rop1_request, 
                                  rop1_request.width,
                                  2,1); 
  TEE_connect_bus_from_tee_output(rop2_tee.dest, 
                                  rop2_request, 
                                  rop2_request.width,
                                  2,1); 

  TEE_connect_bus_from_tee_output(rop1_tee.dest, 
                                  rntable.read_entry, 
                                  rop1_request.width,
                                  0);
  for(i=0; i < rop2_request.width; i++) {
    rop2_tee.dest[2 * i] -> 
              rntable.read_entry[rop1_request.width + i];
  }

  LSS_connect_bus(rntable.entry_out, rnaccum.rid1, rop1_request.width);
  for(i=0; i < rop2_request.width; i++) {
    rntable.entry_out[i + rop1_request.width] -> rnaccum.rid2[i];
  }

  LSS_connect_bus(rnaccum.out,idist.src,instr_in.width);

  LSS_connect_bus(idist.dest,instr_out,instr_out.width);

  idist.funcheader = <<<
#include <instructions.h>
  >>>;

  idist.choose_logic = <<< 
    switch(LSE_emu_dynid_get(id,extra.op)) {
      case J:
      case JALR:
      case JAL:
      case BEQZ:
      case BNEZ:
        return 2;
        break;
      case SLL:
      case SRL:
      case SRA:
      case SLLI:
      case SRLI:
      case SRAI:
        return 1;
        break;
      default:
        return 0;
        break;
    }
  >>>;

  LSS_connect_bus(id2addr.dest,rntable.invalidate_entry,cdb_in.width);
};

/******
 * This module will stall the stage before it and will drop the first
 * message out of the stage after the stall is resolved if the branch is
 * taken.  There is an internal delay of one cycle for the stall for
 * correctness in the decode stage use and to model realistic timing for
 * the fetch stage.
 ******/
module branch_handler {
  include "gate.lss";
  include "flop.lss";
  include "skeleton.lss";

  inport pc_select:int;  
  inport saw_branch:none;  
  inport branch_complete:none;  
 
  outport drop_control:int;
  outport block_control:int;

  instance delay_saw:flop; /* We want to block instructions the next
                              cycle if this cycle had a branch. */
  instance block_gate_control:skeleton;
  
  saw_branch -> delay_saw.src;
  delay_saw.dest -> block_gate_control.in1[0];
  branch_complete -> block_gate_control.in1[1];
  block_gate_control.out1[0] ->:int block_control;
  pc_select -> drop_control;

  block_gate_control.funcheader = <<<
    GLOBDEF(LSE_signal_t,saw_status);
    GLOBDEF(LSE_signal_t,complete_status);
    GLOBDEF(int,state=1);
    GLOBDEF(LSE_dynid_t,state_id=NULL);
  
    static __inline__ void update_state_id(LSE_dynid_t id) {
      if(GLOB(state_id)) 
        LSE_dynid_cancel(GLOB(state_id));
      GLOB(state_id)=id;
      LSE_dynid_register(GLOB(state_id));
    }
  >>>;

  block_gate_control.calc_out1 = <<<
    LSE_dynid_t complete_id, saw_id;
    GLOB(saw_status) = LSE_port_get(in1[0],&saw_id,NULL);
    GLOB(complete_status) = LSE_port_get(in1[1],&complete_id,NULL);

    if(!LSE_signal_data_known(GLOB(complete_status))) {
      *status=LSE_signal_unknown;
      return;
    }

    if(LSE_signal_data_present(GLOB(complete_status))) {
      *data=1;
      *ids=GLOB(state_id)=complete_id;
      update_state_id(complete_id);
      *status=LSE_signal_extract_enable(GLOB(complete_status));
    } else if(LSE_signal_data_known(GLOB(saw_status))) {
      if(LSE_signal_data_present(GLOB(saw_status))) {
        *data=0;
        update_state_id(saw_id);
        *ids=saw_id;
        *status=LSE_signal_extract_enable(GLOB(saw_status));
      } else {
        *data=GLOB(state);
        if(GLOB(state_id)==NULL) GLOB(state_id)=LSE_dynid_create();
        *ids=GLOB(state_id);
        *status=LSE_signal_enabled;
      }
    } else {
      *status=LSE_signal_unknown;
      return;
    }
    *status |= LSE_signal_something;
    return;
  >>>;

  block_gate_control.end_of_timestep = <<<
    int skip_assign=0;
    if(LSE_signal_data_present(GLOB(complete_status))) {
      if(LSE_signal_extract_enable(GLOB(complete_status)) == LSE_signal_enabled) {
        GLOB(state)=1;
        skip_assign=1;
      }
    } 
    if(LSE_signal_data_present(GLOB(saw_status))) {
      if(LSE_signal_extract_enable(GLOB(saw_status)) == LSE_signal_enabled) {
        if(!skip_assign) {
          GLOB(state)=0; 
        }
      }
    }
    GLOB(saw_status)=LSE_signal_unknown;
    GLOB(complete_status)=LSE_signal_unknown;
  >>>;

  block_gate_control.ack_inputs = <<< 
    LSE_signal_t ack_status;
    ack_status = LSE_port_get(out1[instno],NULL,NULL);

    LSE_port_set(in1[2*instno].ack,ack_status);
    LSE_port_set(in1[2*instno+1].ack,ack_status);
  >>>;

};

module fetch_stage {
  outport out:int;
  outport imem_request:int;
  outport imem_request_type:boolean;
  outport npc_out:int;

  inport imem_data_in:int;
  inport cdb_in:int;
  inport pc_in:int;
  inport istaken:int;

  instance cdb_in_fanout:tee;

  /**** Fetch stage connections */

  instance pc_fanout:tee;
  instance inc4:typecast;
  instance npc_select:router;
  instance getnpc:typecast;
  instance readreqsource:boolsource;

  readreqsource.torf=FALSE;

  getnpc.typecast_func = <<< 
    return LSE_emu_dynid_get(id,extra.nextpc); 
  >>>;

  instance istaken_fanout:tee;

  pc_in -> pc_fanout.src;
  pc_fanout.dest ->:int imem_request; 
  readreqsource.dest -> imem_request_type;
  imem_data_in -> out;

  pc_fanout.dest -> inc4.src;
  inc4.typecast_func = <<< return data + 4; >>>;
  inc4.dest -> npc_select.src[0];

  cdb_in -> cdb_in_fanout.src;
  cdb_in_fanout.dest -> getnpc.src;
  getnpc.dest -> npc_select.src[1];
  istaken-> istaken_fanout.src;
  istaken_fanout.dest -> npc_select.route_info[0];

  npc_select.default_routing_function = <<< route_table[0] = 0; >>>; 
  npc_select.src.control = <<< 
        LSE_signal_t tstatus;

        tstatus=LSE_port_query(${this.instance_name}.npc_select:dest[0].ack);
        return LSE_signal_extract_data(istatus) | 
	       LSE_signal_extract_enable(istatus) | 
               tstatus;
  >>>;

  // never stall write back because we didn't select the nextpc from the cdb
  getnpc.src.control = <<< return LSE_signal_extract_enable(istatus) |
                                  LSE_signal_extract_data(istatus) | 
                                  LSE_signal_ack; >>>;

  npc_select.dest[0] -> npc_out;

  /*****************************/

};

module fetch_stage_latch {
  inport in:int;
  outport out:int;

  inport drop_control:int;
  inport block_control:int;

  instance latch_flop:flop;
  instance gates:drop_block_gates;

  in -> latch_flop.src;
  latch_flop.dest -> gates.in;
  gates.out -> out;
  
  drop_control -> gates.drop_control;
  block_control -> gates.block_control;
};

module operand_reservation_station {

  module operand_splitter {
    include "typecast.lss";
    include "tee.lss";

    inport in:none;

    outport op1:int;
    outport op2:int;

    instance fanout:tee;
    instance op1_getter:typecast;
    instance op2_getter:typecast;
    var i:int;

    if(op1.width != in.width) { punt("op1 and in ports must have same width"); }
    if(op2.width != in.width) { punt("op2 and in ports must have same width"); }

    op1_getter.typecast_func = <<<
      return LSE_emu_dynid_get(id,extra.op1_value);
    >>>;

    op2_getter.typecast_func = <<<
      return LSE_emu_dynid_get(id,extra.op2_value);
    >>>;

    LSS_connect_bus(in,fanout.src,in.width);
    for(i=0;i<in.width;i++) {
      fanout.dest -> op1_getter.src;
    }

    for(i=0;i<in.width;i++) {
      fanout.dest -> op2_getter.src;
    }

    LSS_connect_bus(op1_getter.dest, op1, op1.width);
    LSS_connect_bus(op2_getter.dest, op2, op2.width);
  };


  inport instr_in:none;
  inport cdb_in:int;

  outport op1:int;
  outport op2:int;

  parameter num_entries:int;  

  instance rs:reservation_station;
  rs.num_entries=num_entries;

  instance os:operand_splitter;

  LSS_connect_bus(instr_in,rs.instr_in,instr_in.width);
  LSS_connect_bus(cdb_in,rs.cdb_in,cdb_in.width);
  LSS_connect_bus(rs.instr_out,os.in,instr_in.width);
  os.op1 -> op1;
  os.op2 -> op2;

  if(op1.width > 1 ||
     op2.width > 1) {
    punt("Sorry, operand_reservation station only support output op1,op2 width of 1 right now");
  }
};

module branch_control {
  include "tee.lss";
  include "typecast.lss";

  inport cdb_in:int;
  inport branch_decode_in:none;

  outport istaken:int;
  outport drop_control:int;
  outport block_control:int;

  instance cdb_fanout:tee;
  instance drop_control_fo:tee;
  instance block_control_fo:tee;

  instance int2none:typecast;
  int2none.typecast_func = <<< return NULL; >>>;

  instance branch_filter:filter;
  branch_filter.funcheader = <<<
#include "instructions.h"
  >>>;
  branch_filter.ack_uses_filter = FALSE;
  branch_filter.allow_to_pass_func = <<< 
    switch(LSE_emu_dynid_get(id,extra.op)) {
    case J:
    case JAL:
    case JALR:
    case BNEZ:
    case BEQZ:
    case JR:
      return 1;
      break;
    default:
      return 0;
    }
  >>>;

  instance istaken_tc:typecast;
  instance istaken_fanout:tee;
  istaken_tc.funcheader = <<<
#include "instructions.h"
  >>>;

  istaken_tc.typecast_func = <<< 
    switch(LSE_emu_dynid_get(id,extra.op)) {
    case J:
    case JAL:
    case JALR:
    case BNEZ:
    case BEQZ:
    case JR:
      if(LSE_emu_dynid_get(id,extra.branch_taken)) {
        return 1;
      } else {
        return 0;
      }
      break;
    default:
      return 0;
    }
  >>>;


  instance branch_handler:branch_handler;

  var i:int;
  cdb_in -> cdb_fanout.src;

  cdb_fanout.dest -> istaken_tc.src;
  istaken_tc.dest -> istaken_fanout.src;
  for(i=0;i<istaken.width;i++) {
    istaken_fanout.dest -> istaken;
  }

  cdb_fanout.dest -> branch_filter.src;
  branch_filter.dest -> int2none.src;
  int2none.dest -> branch_handler.branch_complete;

  istaken_fanout.dest -> branch_handler.pc_select;

  branch_decode_in -> branch_handler.saw_branch;

  branch_handler.drop_control -> drop_control_fo.src;
  branch_handler.block_control -> block_control_fo.src;
  
  LSS_connect_bus(drop_control_fo.dest,drop_control,drop_control.width);
  LSS_connect_bus(block_control_fo.dest,block_control,block_control.width);
};

module pc {
  inport src:int;
  outport dest:int;

  inport drop_control:int;
  inport block_control:int;

  instance pc:flop;
  instance dynid_gen:idgen;
  instance gates:drop_block_gates;

  pc.initial_state = <<< 
    init_id[porti]=LSE_dynid_create();
    init_value[porti]=0; /* Start at PC 0 */
    return TRUE; 
  >>>;

  pc.src.control = <<< return LSE_signal_extract_enable(istatus) |
                            LSE_signal_extract_data(istatus) | 
                            LSE_signal_ack; >>>;
  src -> pc.src;
  pc.dest -> dynid_gen.in;
  dynid_gen.out -> gates.in;
  gates.out -> dest;

  drop_control -> gates.drop_control;
  block_control -> gates.block_control;
};

module reg_write_control {
  include "filter.lss";
  include "tee.lss";
  include "typecast.lss";

  instance reg_write_filter:filter;

  inport cdb_in:int;
  outport write_data_out:int;
  outport write_addr_out:int;
  
  instance cdb_fo:tee;

  instance id2addr:typecast; 
  id2addr.typecast_func = <<< 
    return LSE_emu_dynid_get(id,extra.rd);
  >>>;


  reg_write_filter.ack_uses_filter=FALSE;
  reg_write_filter.allow_to_pass_func=<<<
    if(LSE_emu_dynid_get(id,extra.rd) <= 0) return 0;
    return 1;
  >>>;

  cdb_in -> cdb_fo.src;

  cdb_fo.dest -> reg_write_filter.src;
  reg_write_filter.dest -> write_data_out;

  cdb_fo.dest -> id2addr.src;
  id2addr.dest -> reg_write_filter.src;

  reg_write_filter.dest -> write_addr_out;
};
/*********************************************/

instance imem:imem;
instance fetch_stage_latch:fetch_stage_latch;


instance branch_control:branch_control;

instance decstage:decode_stage;
instance rfile:regfile;


instance ialu_resstation:operand_reservation_station;

instance shifter_resstation:operand_reservation_station;

instance branch_resstation:reservation_station;


instance ialu:integer_alu;
instance shifter:shifter;
instance branchunit:branchunit;

instance wb_arbiter:arbiter;
instance ex_stage_latch:flop;

instance fetch_stage:fetch_stage;

instance pc:pc;
instance cdb_fanout:tee;
instance reg_write_control:reg_write_control;


rfile.num_regs=32;

imem.size=4096; /* 4k memory */

fetch_stage.imem_request -> imem.request;
fetch_stage.imem_request_type -> imem.request_type;
imem.result -> fetch_stage.imem_data_in;

branch_control.drop_control -> pc.drop_control;
branch_control.block_control -> pc.block_control;
pc.dest -> fetch_stage.pc_in;

fetch_stage.out -> fetch_stage_latch.in;
fetch_stage.npc_out -> pc.src;
branch_control.istaken -> fetch_stage.istaken;

fetch_stage_latch.out -> decstage.instr_in;
branch_control.drop_control -> fetch_stage_latch.drop_control;
branch_control.block_control -> fetch_stage_latch.block_control;

cdb_fanout.dest -> branch_control.cdb_in;

/***** Decode stage connections */
decstage.rop1_request[0] -> rfile.read_addr[0];
decstage.rop2_request[0] -> rfile.read_addr[1];
rfile.read_out[0] -> decstage.rop1_in;
rfile.read_out[1] -> decstage.rop2_in;
reg_write_control.write_addr_out -> rfile.write_addr;
reg_write_control.write_data_out -> rfile.write_data;
/*******************************/

instance branch_fanout:tee;
decstage.instr_out[0] -> ialu_resstation.instr_in;
decstage.instr_out[1] -> shifter_resstation.instr_in;
decstage.instr_out[2] -> branch_fanout.src;

branch_fanout.dest -> branch_resstation.instr_in;
branch_fanout.dest -> branch_control.branch_decode_in;

/**** Ex stage connections */
ialu_resstation.num_entries = 3;
ialu_resstation.op1 -> ialu.op1;
ialu_resstation.op2 -> ialu.op2;

shifter_resstation.num_entries = 5;
shifter_resstation.op1 -> shifter.op1;
shifter_resstation.op2 -> shifter.op2;

branch_resstation.num_entries = 1;
branch_resstation.instr_out -> branchunit.instr_in;

ialu.result -> wb_arbiter.src[0];
shifter.result -> wb_arbiter.src[1];
branchunit.result -> wb_arbiter.src[2];
wb_arbiter.dest -> ex_stage_latch.src[0];

wb_arbiter.comparison_func = <<<
  static LSE_time_t last_time = LSE_time_construct(0,0);
  static int last_choice=0;
  int dist1,dist2;
  int choice;

  if(LSE_time_ne(last_time, LSE_time_now)) {
    last_choice++;
    last_time=LSE_time_now;
  }
  choice=last_choice%LSE_port_width(src);
  dist1 = ((port1 - choice) > 0)?(port1-choice):
                                 (port1-choice+LSE_port_width(src));
  dist2 = ((port2 - choice) > 0)?(port2-choice):
                                 (port2-choice+LSE_port_width(src));

  if(LSE_signal_extract_data(status1) == LSE_signal_nothing) {
    return 1;
  } else if(LSE_signal_extract_data(status1) == LSE_signal_unknown) {
    return -1;
  } else if(LSE_signal_extract_data(status2) == LSE_signal_unknown) {
    return -1;
  } else if(LSE_signal_extract_data(status2) == LSE_signal_nothing) {
    return 0;
  } else {
    if(dist2 < dist1) return 1;
    else return 0;
  }
>>>;
/***********************/

ex_stage_latch.dest[0] -> cdb_fanout.src[0];

/**** Write back stage connections */
cdb_fanout.dest -> reg_write_control.cdb_in;
cdb_fanout.dest -> ialu_resstation.cdb_in;
cdb_fanout.dest -> shifter_resstation.cdb_in;
cdb_fanout.dest -> fetch_stage.cdb_in;
cdb_fanout.dest -> decstage.cdb_in;
cdb_fanout.dest -> branch_resstation.cdb_in;
/************************************/

include "pipeline_stats.lss";
include "tomasulodlx_lsdl.lss";
