#!/bin/sh
FILES="modules/simplemem/test/results/bulktest.stderr			\
modules/simplemem/test/results/bulktest.stdout			\
modules/simplemem/test/results/idtest.stderr				\
modules/simplemem/test/results/idtest.stdout				\
modules/simplemem/test/results/readtest.stderr			\
modules/simplemem/test/results/readtest.stdout			\
modules/simplemem/test/results/writemasktest.stderr			\
modules/simplemem/test/results/writemasktest.stdout			\
modules/simplemem/test/results/writetest.stderr			\
modules/simplemem/test/results/writetest.stdout			\
modules/simplemem/test/bulktest.lss					\
modules/simplemem/test/idtest.lss					\
modules/simplemem/test/readtest.lss					\
modules/simplemem/test/writemasktest.lss				\
modules/simplemem/test/writetest.lss					\
modules/simplemem/Makefile						\
modules/simplemem/simplemem.clm					\
modules/simplemem/simplemem.lss					\
modules/alu/Makefile							\
modules/alu/alu.am							\
modules/alu/alu.clm							\
modules/alu/alu.lss							\
modules/alu/test/alutest.lss						\
modules/regfile/test/rftest.lss					\
modules/regfile/Makefile						\
modules/regfile/regfile.lss						\
modules/decoder/Makefile						\
modules/decoder/decoder.lss						\
modules/imem/Makefile							\
modules/imem/imem.lss							\
modules/reg_accum/Makefile						\
modules/reg_accum/reg_accum.lss					\
modules/reg_requester/Makefile					\
modules/reg_requester/reg_requester.lss				\
modules/ren_accum/Makefile						\
modules/ren_accum/ren_accum.lss					\
modules/Makefile							\
modules/rename_table/test/test.lss					\
modules/rename_table/Makefile						\
modules/rename_table/rename_table.lss					\
modules/rename_table/cancel_reads.clm					\
modules/rename_table/cancel_reads.lss					\
modules/reservation_station/Makefile					\
modules/reservation_station/reservation_station.lss			\
modules/store_buffer/Makefile						\
modules/store_buffer/store_buffer.lss					\
modules/store_buffer/lookup_handler.clm				\
modules/store_buffer/lookup_handler.lss				\
modules/dlxlib.lss							\
modules/writebypasscomparator/test/writebypasscomparatortest.lss	\
modules/writebypasscomparator/Makefile				\
modules/writebypasscomparator/writebypasscomparator.clm		\
modules/writebypasscomparator/writebypasscomparator.lss		\
modules/data_memory/data_memory.lss					\
modules/data_memory/Makefile						\
modules/regalloc_manager/Makefile					\
modules/regalloc_manager/regalloc_manager.clm				\
modules/regalloc_manager/regalloc_manager.lss				\
modules/tomasulo_core/Makefile					\
modules/tomasulo_core/tomasulo_core.lss				\
modules/tomasulo_core_unified/Makefile				\
modules/tomasulo_core_unified/tomasulo_core_unified.lss		\
modules/others/fus.lss						\
modules/others/Makefile						\
modules/others/spec_latches.lss					\
modules/others/spec_lsu.lss						\
modules/others/spec_pipestages.lss					\
modules/others/spec_rename.lss					\
modules/others/util.lss						\
Makefile								\
doc/tomasulodlx_specs.pdf						\
instructions.h							\
AUTHORS								\
README								\
tests/README                                                    \
tests/test.bin							\
tests/test.s								\
tests/test2.bin							\
tests/test2.s								\
tests/test3.bin							\
tests/test3.s								\
tests/test_waw.bin							\
tests/test_waw.s							\
tests/test_mem.bin							\
tests/test_mem.s							\
tests/fib.bin								\
tests/fib.o								\
tests/fib.s								\
tomasulodlx_spec_unified.lss						\
tomasulodlx_spec.lss							\
tomasulodlx_spec.lss.properties					\
tomasulodlx_spec_unified.lss.properties				\
core_stats.lss							\
vis_stats.lss							\
vis_pipeline_table_stats.lss"

VERSION=2.0
mkdir tomasulodlx-${VERSION}
for fil in $FILES; do
    mkdirhier tomasulodlx-${VERSION}/$(dirname $fil)
    cp $fil tomasulodlx-${VERSION}/$fil
done
tar -cvzf tomasulodlx-${VERSION}.tar.gz tomasulodlx-${VERSION}
rm -rf tomasulodlx-${VERSION}

