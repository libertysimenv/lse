using dlxlib;

include "dlx_no_pipeline_modules.lss";
include "combiner.lss";

/************************************************************************/
/* toplevel instances */

/* the machine's PC */
instance PC:pc;
/* adder for the PC, calculates PC += 4 */
instance pc_adder:adder;
/* adder to calculate branch targets */
instance offset_adder:adder;
/* constant 4 generator for the PC adder */
instance four:datasource;
/* instruction memory */
instance instr_mem:instr_mem;
/* control unit */
instance control:control;
/* the register file */
instance regfile:new_regfile;
/* used to get the immediate field from the instruction */
instance imm_getter:immediate_getter;
/* branch control unit */
instance branch_control:branch_control;
/* the alu */
instance alu:integer_alu;
/* mux the second op from the regfile with the immediate */
instance alu_op_2_mux:router;
/* data memory */
instance data_mem:simplemem;
/* mux the data being written back */
instance wb:wb;

/* instance parameters */
data_mem.size=1024*1024; /* 1 MB */
regfile.num_regs=32;
instr_mem.size=4096;             /* 4k memory */
four.create_data = <<<
  *data = 4;
  return LSE_signal_all_yes;
                   >>>;

PC.NPC_in.control = <<<
    return istatus | LSE_signal_ack | LSE_signal_enabled;
		    >>>;

/************************************************************************/
/* PC signals */
PC.PC_out -> pc_adder.op1;
PC.PC_out -> instr_mem.request;
four.dest -> pc_adder.op2;


/************************************************************************/
/* pc_adder signals */
pc_adder.result -> branch_control.npc_in;//branch_mux.src[0];
pc_adder.result -> offset_adder.op1;


/************************************************************************/
/* instruction memory */
instr_mem.result -> branch_control.instr_in;
instr_mem.result -> control.instr_in;
instr_mem.result -> imm_getter.instr_in;


/************************************************************************/
/* register file signals */
regfile.read_data_1 -> alu.op1;
regfile.read_data_1 -> branch_control.branch_taken;
regfile.read_data_2 -> alu_op_2_mux.src[0];//r2fanout.src;
regfile.read_data_2 -> data_mem.write_data;


/************************************************************************/
/* control signals */
control.rop1 -> regfile.read_reg_1;
control.rop2 -> regfile.read_reg_2;
control.wop -> regfile.write_addr;
control.mem_to_reg -> wb.route_info;
control.mem_write -> data_mem.request_type;
control.imm -> alu_op_2_mux.route_info;
control.alu_op -> alu.alu_op;

/************************************************************************/
/* alu signals */
alu.result -> data_mem.request;
alu.result -> wb.src[0];


/************************************************************************/
/* alu_op_2_mux signals */
alu_op_2_mux.dest -> alu.op2;
alu_op_2_mux.default_routing_function = <<< route_table[0] = 0; >>>; 
alu_op_2_mux.src.control = <<< 
  LSE_signal_t tstatus;

  tstatus=LSE_port_query(alu_op_2_mux:dest[0].ack);
  return LSE_signal_extract_data(istatus) | LSE_signal_extract_enable(istatus) | 
         tstatus;
  >>>;


/************************************************************************/
/* data memory signals */
data_mem.result -> wb.src[1];


/************************************************************************/
/* imm_getter signals */
imm_getter.imm_out -> offset_adder.op2;//branch_shifter.in;
imm_getter.imm_out -> alu_op_2_mux.src[1];

/************************************************************************/
/* offset_adder signals */
offset_adder.result -> branch_control.branch_target;//branch_mux.src[1];


/************************************************************************/
/* branch control signals */
branch_control.npc_out -> PC.NPC_in;


/************************************************************************/
/* wb signals */

wb.dest -> regfile.write_data;
wb.default_routing_function = <<< route_table[0] = 0; >>>; 
wb.src.control = <<< 
  LSE_signal_t tstatus;

  tstatus=LSE_port_query(wb.flop:src[0].ack);
  return LSE_signal_extract_data(istatus) | LSE_signal_extract_enable(istatus) | 
         tstatus;
>>>;

include "dlx_no_pipeline_collectors.lss";
include "dlx_no_pipeline.lss.lvl";
