# When this program enters the terminating infinite loop, r1 has the 
# fibonacci number specified by r3 (i.e. if r0 is 0 or 1, r1
# should have 1, with r3=8 r1 should be 34
.text
.extern _start
.proc _start
_start:	addi r1,r0,#1 		; x=1
	addi r2,r0,#1		; y=1
	addi r3,r0,#5		; n=5
	beqz r3,end		; if(n==0) goto end:
loop:   add r4,r2,r0		; tmp=y
	add r2,r2,r1		; y=y+x
	add r1,r4,r0		; x=y
	addi r3,r3,-1		; n--
	bnez r3,loop		; if(n != 0) goto loop
        or r0,r1,r0      	; Force value of r1 into the trace
end:    j end
        or r0,r1,r0      	; Force value of r1 into the trace
	or r0,r1,r0      	; Force value of r1 into the trace
        or r0,r1,r0      	; Force value of r1 into the trace
        or r0,r1,r0      	; Force value of r1 into the trace
        or r0,r1,r0      	; Force value of r1 into the trace
