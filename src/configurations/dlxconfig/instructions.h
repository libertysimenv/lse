#ifndef _INSTRUCTIONS_H_
#define _INSTRUCTIONS_H_

#include <stdint.h>
#include <stdio.h>

static char *DLX_op2str[] = {
  "lb",
  "lbu",
  "sb",
  "lh",
  "lhu",
  "sh",
  "lw",
  "sw",
  "lf",
  "ld",
  "sf",
  "sd",
  "movi2s",
  "movs2i",
  "movf",
  "movd",
  "movfp2i",
  "movi2fp",
  "add",
  "addi",
  "addu",
  "addui",
  "sub",
  "subi",
  "subu",
  "subui",
  "mult",
  "multu",
  "div",
  "divu",
  "and",
  "andi", 
  "or",
  "ori",
  "xop",
  "xopi",
  "lhi",
  "sll",
  "srl",
  "sra",
  "slli",
  "srli",
  "srai",
  "slt",
  "slti",
  "sgt",
  "sgti",
  "sle",
  "slei",
  "sge",
  "sgei",
  "seq",
  "seqi",
  "sne",
  "snei",
  "beqz",
  "bnez",
  "bfpt",
  "bfpf",
  "j",
  "jr",
  "jal",
  "jalr",
  "trap",
  "rfe",
  "addd",
  "addf",
  "subd",
  "subf",
  "multd",
  "multf",
  "divd",
  "divf",
  "cvtf2d",
  "cvtf2i",
  "cvtd2f",
  "cvtd2i",
  "cvti2f",
  "cvti2d",
  "ltd",
  "led",
  "gtd",
  "ged",
  "eqd",
  "ned",
  "ltf",
  "lef",
  "gtf",
  "gef",
  "eqf",
  "nef"
};

typedef enum {
  /* Data movement */
  LB=0, LBU, SB,
  LH, LHU, SH,
  LW, SW,
  LF, LD, SF, SD,
  MOVI2S, MOVS2I,
  MOVF, MOVD, /* Not supported */
  MOVFP2I, MOVI2FP, /* Not supported */
  /* Arithmetic */
  ADD, ADDI, ADDU, ADDUI,
  SUB, SUBI, SUBU, SUBUI,
  MULT, MULTU, DIV, DIVU,
  AND, ANDI,
  OR, ORI, XOP, XOPI, /* Is XOP right or is it a typo on the web page? */
  LHI,
  SLL, SRL, SRA, SLLI, SRLI, SRAI,
  SLT, SLTI, SGT, SGTI, SLE, SLEI, SGE, SGEI, SEQ, SEQI, SNE, SNEI,
  /* Control instructions */
  BEQZ, BNEZ,
  BFPT, BFPF, /* Not supported */
  J,  JR,
  JAL,  JALR,
  TRAP, /* Not supported */
  RFE, /* Not supported */
  /* Floating point ops, all not supported */
  ADDD, ADDF,
  SUBD, SUBF,
  MULTD, MULTF,
  DIVD, DIVF,
  CVTF2D, CVTF2I, CVTD2F, 
  CVTD2I, CVTI2F, CVTI2D,
  LTD, LED, GTD, GED, EQD, NED, LTF, LEF, GTF, GEF, EQF, NEF
} DLX_ops;

/* Instruction formats */
typedef enum {
  IType, RType, JType, FIType, FRType
} DLX_iformat;

static DLX_iformat op2format_array[]={
  /* Data movement */
  IType /* LB */, IType /* LBU */, IType /*SB*/,
  IType /* LH */, IType /* LHU */, IType /* SH */,
  IType /* LW */, IType /* SW */,
  FIType /* LF */, FIType /* LD */, FIType /* SF */, FIType /* SD */,
  RType /* MOVI2S */, RType /* MOVS2I */,
  FRType /* MOVF */, FRType /* MOVD */, 
  FRType /* MOVFP2I */, FRType /* MOVI2FP */,
  /* Arithmetic  */
  RType /* ADD */, IType /* ADDI */, RType /* ADDU */, IType /* ADDUI */,
  RType /* SUB */, IType /* SUBI */, RType /* SUBU */, IType /* SUBUI */,
  RType /* MULT */, RType /* MULTU */, RType /* DIV */, RType /* DIVU */,
  RType /* AND */, IType /* ANDI */,
  RType /* OR */, IType /* ORI */, RType /* XOP */, IType /* XOPI */, 
  IType /* LHI */,
  RType /* SLL */, RType /* SRL */, RType /* SRA */, RType /* SLLI */, RType /* SRLI */, RType /* SRAI */,
  RType /* SLT */, IType /* SLTI */, RType /* SGT */, IType /* SGTI */, RType /* SLE */, IType /* SLEI */, RType /* SGE */, IType /* SGEI */, RType /* SEQ */, IType /* SEQI */, RType /* SNE */, IType /* SNEI */,
  /*  Control instructions  */
  IType /* BEQZ */, IType /* BNEZ */,
  FIType /* BFPT */, FIType /* BFPF */,
  JType /* J */, IType /*  JR */,
  JType /* JAL */, IType /*  JALR */,
  JType /* TRAP */,
  JType /* RFE */,
  /*  Floating point ops, all not supported  */
  FRType /* ADDD */, FRType /* ADDF */,
  FRType /* SUBD */, FRType /* SUBF */,
  FRType /* MULTD */, FRType /* MULTF */,
  FRType /* DIVD */, FRType /* DIVF */,
  FRType /* CVTF2D */, FRType /* CVTF2I */, FRType /* CVTD2F */,
  FRType /* CVTD2I */, FRType /* CVTI2F */, FRType /* CVTI2D */,
  FRType /* LTD */, FRType /* LED */, FRType /* GTD */, FRType /* GED */, FRType/* EQD */, FRType /* NED */, FRType /* LTF */, FRType /* LEF */, FRType /* GTF */, FRType /* GEF */, FRType /* EQF */, FRType /* NEF */
};

/* Opcodes */
/* Data movement */
#define OPC_LB (0x20)
#define OPC_LBU (0x24) 
#define OPC_SB (0x28)
#define OPC_LH (0x21)
#define OPC_LHU (0x25)
#define OPC_SH (0x29)
#define OPC_LW (0x23)
#define OPC_SW (0x2b)
#define OPC_LF (0x31)
#define OPC_LD (0x35)
#define OPC_SF (0x39)
#define OPC_SD (0x3d)
#define OPC_MOVI2S (0x0)
#define OPC_MOVS2I (0x0)
#define OPC_MOVF (0x11)
#define OPC_MOVD (0x11)
#define OPC_MOVFP2I (0x11)
#define OPC_MOVI2FP (0x11)
/* Arithmetic */
#define OPC_ADD (0x0)
#define OPC_ADDI (0x08)
#define OPC_ADDU (0x0)
#define OPC_ADDUI (0x09)
#define OPC_SUB (0x0)
#define OPC_SUBI (0xa)
#define OPC_SUBU (0x0)
#define OPC_SUBUI (0xb)
#define OPC_MULT (0x0)
#define OPC_MULTU (0x0)
#define OPC_DIV (0x0)
#define OPC_DIVU (0x0)
#define OPC_AND (0x0)
#define OPC_ANDI (0xc)
#define OPC_OR (0x0)
#define OPC_ORI (0xd)
#define OPC_XOP (0x0)
#define OPC_XOPI (0xe)
#define OPC_LHI (0xf)
#define OPC_SLL (0x0)
#define OPC_SRL (0x0)
#define OPC_SRA (0x0)
#define OPC_SLLI (0x0)
#define OPC_SRLI (0x0)
#define OPC_SRAI (0x0)
#define OPC_SLT (0x0)
#define OPC_SLTI (0x1c)
#define OPC_SGT (0x0)
#define OPC_SGTI (0x19)
#define OPC_SLE (0x0)
#define OPC_SLEI (0x1e)
#define OPC_SGE (0x0)
#define OPC_SGEI (0x1b)
#define OPC_SEQ (0x0)
#define OPC_SEQI (0x1a)
#define OPC_SNE (0x0)
#define OPC_SNEI (0x1d)
/* Control instructions */
#define OPC_BEQZ (0x04)
#define OPC_BNEZ (0x05)
#define OPC_BFPT (0x06) /* Hmm which of these two is which? */
#define OPC_BFPF (0x07) /* Hmm which of these two is which? */
#define OPC_J  (0x2)
#define OPC_JR (0x16)
#define OPC_JAL (0x3)
#define OPC_JALR (0x17)
#define OPC_TRAP (0x3e)
#define OPC_RFE (0x3f)
/* Floating point ops, all not supported */
#define OPC_ADDD (0x11)
#define OPC_ADDF (0x11)
#define OPC_SUBD (0x11)
#define OPC_SUBF (0x11)
#define OPC_MULTD (0x11)
#define OPC_MULTF (0x11)
#define OPC_DIVD (0x11)
#define OPC_DIVF (0x11)
#define OPC_CVTF2D (0x11)
#define OPC_CVTF2I (0x11)
#define OPC_CVTD2F (0x11)
#define OPC_CVTD2I (0x11)
#define OPC_CVTI2F (0x11)
#define OPC_CVTI2D (0x11)
#define OPC_LTD (0x11)
#define OPC_LED (0x11)
#define OPC_GTD (0x11)
#define OPC_GED (0x11)
#define OPC_EQD (0x11)
#define OPC_NED (0x11)
#define OPC_LTF (0x11)
#define OPC_LEF (0x11)
#define OPC_GTF (0x11)
#define OPC_GEF (0x11)
#define OPC_EQF (0x11)
#define OPC_NEF (0x11)

/* Extended OP codes */

/* Data movement */
#define EOPC_MOVI2S (0x10)
#define EOPC_MOVS2I (0x11)
#define EOPC_MOVF (0x8)
#define EOPC_MOVD (0x8)
#define EOPC_MOVFP2I (0x09)
#define EOPC_MOVI2FP (0x0a)

/* Arithmetic */
#define EOPC_ADD (0x20)
#define EOPC_ADDU (0x21)
#define EOPC_SUB (0x22)
#define EOPC_SUBU (0x23)
#define EOPC_MULT (0x0)
#define EOPC_MULTU (0xff)
#define EOPC_DIV (0xff)
#define EOPC_DIVU (0xff)
#define EOPC_AND (0x24)
#define EOPC_OR (0x25)
#define EOPC_XOP (0x26)
#define EOPC_SLL (0x04)
#define EOPC_SRL (0x06)
#define EOPC_SRA (0x07)
#define EOPC_SLLI (0x00)
#define EOPC_SRLI (0x02)
#define EOPC_SRAI (0x03)
#define EOPC_SLT (0x2c)
#define EOPC_SGT (0x29)
#define EOPC_SLE (0x2e)
#define EOPC_SGE (0x2b)
#define EOPC_SEQ (0x2a)
#define EOPC_SNE (0x2d)
/* Floating point ops, all not supported.  
   Precision is in mode register.
   Conversion types in separate field */
#define EOPC_ADDD (0x00)
#define EOPC_ADDF (0x00)
#define EOPC_SUBD (0x01)
#define EOPC_SUBF (0x01)
#define EOPC_MULTD (0x02)
#define EOPC_MULTF (0x02)
#define EOPC_DIVD (0x03)
#define EOPC_DIVF (0x03)
#define EOPC_CVTF2D (0x20)
#define EOPC_CVTF2I (0x20)
#define EOPC_CVTD2F (0x21)
#define EOPC_CVTD2I (0x21)
#define EOPC_CVTI2F (0x24)
#define EOPC_CVTI2D (0x24)
#define EOPC_LTD (0x3c)
#define EOPC_LED (0x3e)
#define EOPC_GTD (0x39)
#define EOPC_GED (0x2b)
#define EOPC_EQD (0x3a)
#define EOPC_NED (0x3d)
#define EOPC_LTF (0x3c)
#define EOPC_LEF (0x3e)
#define EOPC_GTF (0x39)
#define EOPC_GEF (0x2b)
#define EOPC_EQF (0x3a)
#define EOPC_NEF (0x3d)

/* MISC */
#define INSTR_BITS (sizeof(uint32_t)*8)
#define OPC_BITS (6)
#define EOPC_BITS (6)
#define OPC_MASK (0xfc000000)
#define EOPC_MASK (0x0000002f)

static DLX_iformat DLX_get_format_by_opc(uint32_t opc)
{
  switch(opc) {
  case 0:
    return RType;
    break;
  case 0x11:
    return FRType;
    break;
  case 0x02:
  case 0x03:
  case 0x3e:
  case 0x3f:
    return JType;
    break;
  case 0x31:
  case 0x35:
  case 0x39:
  case 0x3d:
  case 0x06:
  case 0x07:
    return FIType;
    break;
  default:
    return IType;
    break;
  }
}

static DLX_ops DLX_get_iop_by_eopc(uint32_t eopc) 
{
  switch(eopc) {
  case EOPC_MOVI2S:
    return MOVI2S;
    break;
  case EOPC_MOVS2I:
    return MOVS2I;
    break;
  case EOPC_MOVF: /* Need to look at status register to really tell apart */
    /*  case EOPC_MOVD: */
    return MOVD;
    break;
  case EOPC_MOVFP2I:
    return MOVFP2I;
    break;
  case EOPC_MOVI2FP:
    return MOVI2FP;
    break;
/* Arithmetic */
  case EOPC_ADD:
    return ADD;
    break;
  case EOPC_ADDU:
    return ADDU;
    break;
  case EOPC_SUB:
    return SUB;
    break;
  case EOPC_SUBU:
    return SUBU;
    break;
    /* Not supported yet */
#if 0
  case EOPC_MULT:
    return MULT;
    break;
  case EOPC_MULTU:
    return MULTU;
    break;
  case EOPC_DIV:
    return DIV;
    break;
  case EOPC_DIVU:
    return DIVU;
    break;
#endif
  case EOPC_AND:
    return AND;
    break;
  case EOPC_OR:
    return OR;
    break;
  case EOPC_XOP:
    return XOP;
    break;
  case EOPC_SLL:
    return SLL;
    break;
  case EOPC_SRL:
    return SRL;
    break;
  case EOPC_SRA:
    return SRA;
    break;
  case EOPC_SLLI:
    return SLLI;
    break;
  case EOPC_SRLI:
    return SRLI;
    break;
  case EOPC_SRAI:
    return SRAI;
    break;
  case EOPC_SLT:
    return SLT;
    break;
  case EOPC_SGT:
    return SGT;
    break;
  case EOPC_SLE:
    return SLE;
    break;
  case EOPC_SGE:
    return SGE;
    break;
  case EOPC_SEQ:
    return SEQ;
    break;
  case EOPC_SNE:
    return SNE;
    break;
  default:
    return static_cast<DLX_ops>(-1);
    break;
  }
}

static DLX_ops DLX_decode_op(uint32_t instr) 
{
  DLX_iformat format;
  uint32_t opc;
  uint32_t eopc;
  
  opc = instr >> (INSTR_BITS - OPC_BITS);
  eopc = instr & EOPC_MASK;
  
  format=DLX_get_format_by_opc(opc);
  switch(format) {
  case RType:
    return DLX_get_iop_by_eopc(eopc);
    break;
  case FIType:
  case FRType:
    // Not implemented
    fprintf(stderr, "Attempt to decode unsupported floating point op\n");
    return static_cast<DLX_ops>(-1);
    break;
  case JType:
    switch(opc) {
    case OPC_J:
      return J;
      break;
    case OPC_JAL:
      return JAL;
      break;
    case OPC_TRAP:
      return TRAP;
      break;
    case OPC_RFE:
      return RFE;
      break;
    default:
      fprintf(stderr, "Unsupported JType Instruction\n");
      return static_cast<DLX_ops>(-1);
      break;
    }
    break;
  case IType:
    switch(opc) {
    case OPC_LB:
      return LB;
      break;
    case OPC_LBU:
      return LBU;
      break;
    case OPC_SB:
      return SB;
      break;
    case OPC_LH:
      return LH;
      break;
    case OPC_LHU:
      return LHU;
      break;
    case OPC_SH:
      return SH;
      break;
    case OPC_LW:
      return LW;
      break;
    case OPC_SW:
      return SW;
      break;
    case OPC_LF:
      return LF;
      break;
    case OPC_LD:
      return LD;
      break;
    case OPC_SF:
      return SF;
      break;
    case OPC_SD:
      return SD;
      break;
/* Arithmetic */
    case OPC_ADDI:
      return ADDI;
      break;
    case OPC_ADDUI:
      return ADDUI;
      break;
    case OPC_SUBI:
      return SUBI;
      break;
    case OPC_SUBUI:
      return SUBUI;
      break;
    case OPC_ANDI:
      return ANDI;
      break;
    case OPC_ORI:
      return ORI;
      break;
    case OPC_XOPI:
      return XOPI;
      break;
    case OPC_LHI:
      return LHI;
      break;
    case OPC_SLTI:
      return SLTI;
      break;
    case OPC_SGTI:
      return SGTI;
      break;
    case OPC_SLEI:
      return SLEI;
      break;
    case OPC_SGEI:
      return SGEI;
      break;
    case OPC_SEQI:
      return SEQI;
      break;
    case OPC_SNEI:
      return SNEI;
/* Control instructions */
    case OPC_BEQZ:
      return BEQZ;
      break;
    case OPC_BNEZ:
      return BNEZ;
      break;
    case OPC_JR:
      return JR;
      break;
    case OPC_JALR:
      return JALR;
      break;
    default:
      fprintf(stderr, "Unsupported IType Instruction\n");
      return static_cast<DLX_ops>(-1);
      break;
    }
    break;
  default:
    fprintf(stderr, "Unknown Instruction\n");
    return static_cast<DLX_ops>(-1);
    break;
  }
}

static DLX_iformat DLX_get_format_by_op(DLX_ops op) 
{
  return op2format_array[op];
}

#define RTYPE_RD_MASK (0x0000f800)
#define RTYPE_RD_SHIFT (11)

#define RTYPE_ROP1_MASK (0x03e00000)
#define RTYPE_ROP1_SHIFT (21)

#define RTYPE_ROP2_MASK (0x001f0000)
#define RTYPE_ROP2_SHIFT (16)

#define RTYPE_SA_MASK (0x000007c00)
#define RTYPE_SA_SHIFT (6)

#define ITYPE_IOP_MASK (0x0000ffff)
#define ITYPE_IOP_SHIFT (0)

#define ITYPE_RD_MASK (0x001f0000)
#define ITYPE_RD_SHIFT (16)

#define ITYPE_ROP1_MASK (0x03e00000)
#define ITYPE_ROP1_SHIFT (21)

#define JTYPE_OFFSET_MASK (0x03ffffff)
#define JTYPE_OFFSET_SHIFT (0)

static uint32_t DLX_get_register_operand1(DLX_ops op, uint32_t instr)
{
  DLX_iformat format;
  
  format=DLX_get_format_by_op(op);
  switch(format) {
  case JType:
    return static_cast<uint32_t>(-1); /* No source register operands */
  default: /* All other formats are the same */
    return (RTYPE_ROP1_MASK & instr) >> RTYPE_ROP1_SHIFT; 
    break;
  }
}

static uint32_t DLX_get_register_operand2(DLX_ops op, uint32_t instr)
{
  DLX_iformat format;
  
  format=DLX_get_format_by_op(op);
  switch(format) {
  case JType:
    return static_cast<uint32_t>(-1); /* No source register operands */
  case IType:
  case FIType:
    return static_cast<uint32_t>(-1); /* No source register operand 2 */
    break;
  default: /* All other formats are the same */
    return (RTYPE_ROP2_MASK & instr) >> RTYPE_ROP2_SHIFT; 
    break;
  }
}

static uint32_t DLX_get_register_dest(DLX_ops op, uint32_t instr)
{
  DLX_iformat format;
  
  format=DLX_get_format_by_op(op);
  switch(format) {
  case JType:
    switch(op) {
    case JAL:
      return 31; /* Link register is r31 */
      break;
    default:
      return static_cast<uint32_t>(-1); /* No dest register operands */
      break;
    }
  case IType:
    switch(op) {
    case JALR:
      return 31; /* Link register is r31 */
      break;
    default:
      break;
    }
  case FIType:
    return (ITYPE_RD_MASK & instr) >> ITYPE_RD_SHIFT;
    break;
  case FRType:
  case RType:
    return (RTYPE_RD_MASK & instr) >> RTYPE_RD_SHIFT;
    break;
  default: /* All other formats are the same */
    return static_cast<uint32_t>(-1);
    break;
  }
}

static uint32_t DLX_get_sa(DLX_ops op, uint32_t instr)
{
  DLX_iformat format;
  
  format=DLX_get_format_by_op(op);
  switch(format) {
  case RType:
    return (RTYPE_SA_MASK & instr) >> RTYPE_SA_SHIFT;
    break;
  default: /* All other formats are the same */
    return static_cast<uint32_t>(-1);
    break;
  }
}

static uint32_t DLX_get_imm(DLX_ops op, uint32_t instr)
{
  DLX_iformat format;
  
  format=DLX_get_format_by_op(op);
  switch(format) {
  case IType:
  case FIType:
    return (ITYPE_IOP_MASK & instr) >> ITYPE_IOP_SHIFT;
    break;
  default: /* All other formats are the same */
    return static_cast<uint32_t>(-1);
    break;
  }
}

static uint32_t DLX_get_offset(DLX_ops op, uint32_t instr)
{
  DLX_iformat format;
  
  format=DLX_get_format_by_op(op);
  switch(format) {
  case JType:
    return (JTYPE_OFFSET_MASK & instr) >> JTYPE_OFFSET_SHIFT;
  default: /* All other formats are the same */
    return static_cast<uint32_t>(-1);
    break;
  }
}

static void DLX_tostring_itype(char *str, int n, uint32_t instr)
{
  uint32_t rs1;
  uint32_t rd;
  uint32_t imm;
  int32_t simm;
  int16_t small;
  DLX_ops op;
  
  op  = DLX_decode_op(instr);
  rs1 = DLX_get_register_operand1(op, instr);
  rd  = DLX_get_register_dest(op, instr);
  imm = DLX_get_imm(op,instr);
  small = imm;
  simm = small;

  switch(op) {
  case LB:
  case LH:
  case LW:
  case LBU:
  case LHU:
    snprintf(str, n, "%s r%d,#%d(r%d)", DLX_op2str[op], rd, simm, rs1);
    break;
  case SB:
  case SH:
  case SW:
    snprintf(str, n, "%s #%d(r%d),r%d", DLX_op2str[op], simm, rs1, rd);
    break;
  case ADDI:
  case SUBI:
    snprintf(str, n, "%s r%d,r%d,#%d", DLX_op2str[op], rd, rs1, simm);
    break;
  case BEQZ:
  case BNEZ:
    snprintf(str, n, "%s r%d,r%d,#%d", DLX_op2str[op], rd, rs1, simm);
    break;
  default:
    snprintf(str, n, "%s r%d,r%d,#%d", DLX_op2str[op], rd, rs1, imm);
    break;
  }
}

static void DLX_tostring_rtype(char *str, int n, uint32_t instr)
{
  uint32_t rs1;
  uint32_t rs2;
  uint32_t rd;
  uint32_t sa;
  DLX_ops op;
  
  op  = DLX_decode_op(instr);
  rs1 = DLX_get_register_operand1(op, instr);
  rs2 = DLX_get_register_operand2(op, instr);
  rd  = DLX_get_register_dest(op, instr);
  sa  = DLX_get_sa(op, instr);

  switch(op) {
  case SLLI:
  case SRLI:
  case SRAI:
    snprintf(str, n, "%s r%d,r%d,#%d", DLX_op2str[op], rd, rs1, sa);
    break;
  case MOVS2I:
  case MOVI2S:
    snprintf(str, n, "Sorry cannot decode this");
    break;
  case JR:
  case JALR:
    snprintf(str, n, "%s r%d", DLX_op2str[op], rs1);
    break;
  default:
    snprintf(str, n, "%s r%d,r%d,r%d", DLX_op2str[op], rd, rs1, rs2);
    break;
  }
}

static void DLX_tostring_jtype(char *str, int n, uint32_t instr)
{
  DLX_ops op;
  int32_t offset;
  uint32_t negative;
  
  op  = DLX_decode_op(instr);
  offset = DLX_get_offset(op, instr);
  negative = offset & 0x02000000;
  if(negative) offset |= 0xfc000000;

  switch(op) {
  case TRAP:
  case RFE: 
    snprintf(str, n, "Sorry cannot decode this");
    break;
  default:
    snprintf(str, n, "%s #%d", DLX_op2str[op], offset);
    break;
  }
}

static void DLX_tostring_instruction(char *str, int n, uint32_t instr)
{
  DLX_ops op;
  DLX_iformat fmt;

  op = DLX_decode_op(instr);
  fmt = DLX_get_format_by_op(op);

  switch(fmt) {
  case IType:
    DLX_tostring_itype(str, n, instr);
    break;
  case RType:
    DLX_tostring_rtype(str, n, instr);
    break;
  case JType:
    DLX_tostring_jtype(str, n, instr);
    break;
  case FIType:
  case FRType:
    snprintf(str, n, "Sorry, can't print floating point ops");
    break;
  }
}

static void DLX_print_instruction(FILE *out, uint32_t instr)
{
  char str[100];

  DLX_tostring_instruction(str, 100, instr);
  fprintf(out, "%s\n", str);
}

#endif /* _INSTRUCTIONS_H_ */
