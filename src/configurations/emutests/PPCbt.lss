using corelib;

using LSE_emu;

var emu = LSE_emu::create("inst0",<<<LSE_PowerPC --
include PowerPC64.lis
include PPCLinux.lis
include PPCbuild.lis
include PowerPC_compat.lis
include codecache.lis
include PPCllvmIBT.lis
show maximal queue;
implement fast = llvmIBT;
>>>,"") : domain ref;
add_to_domain_searchpath(emu);

module emulate {
  instance emulate:converter;

  inport in:'a;
  outport out:'a;

  LSS_connect_bus(in,emulate.in,in.width);
  LSS_connect_bus(emulate.out,out,out.width);

  if(in.width != out.width) {
    punt("in.width and out.width must match");
  }

  emulate.convert_func = <<<
    LSE_emu::EMU_dofast(*LSE_dynid_element_ptr(id,attr:LSE_emu:instr_info));
    if (LSE_emu_dynid_get(id, fault))	{
      if (LSE_emu::EMU_handle_fault(LSE_emu_dynid_get(id, swcontexttok),
				    LSE_emu_dynid_get(id, fault),
				    LSE_emu_dynid_get(id, addr),
				    LSE_emu_dynid_get(id, next_pc))) {
	std::cerr << "Fault encountered at 0x" << std::hex 
		  << LSE_emu_dynid_get(id, addr) << std::dec << "\n";
	LSE_emu_disassemble(id, stderr);
	LSE_emu::PPC_dump_state(LSE_emu_dynid_get(id, swcontexttok),
				stderr, 1);
	LSE_sim_terminate_now = true;
	LSE_sim_exit_status = 42;
      }
    }
    return data;
  >>>;
};

module npc {
  instance npc:converter;

  inport in:'a;
  outport out:'a;

  LSS_connect_bus(in,npc.in,in.width);
  LSS_connect_bus(npc.out,out,out.width);

  if(in.width != out.width) {
    punt("in.width and out.width must match");
  }

  npc.convert_func = <<<
    static void *lastone = 0;
    void *newone = LSE_emu_get_context_mapping(1);
    if (!newone) return 0;
    if (!lastone) lastone = newone;
    if (lastone == newone) return LSE_emu_dynid_get(id,next_pc);
    else {
        lastone = newone;
        return LSE_emu_get_start_addr(1);
    }
  >>>;
};

module newid {
  instance idgen:source;

  instance makenewid:combiner;

  inport in:LSE_emu_iaddr_t;
  outport out:'a;

  if(in.width != out.width) {
    punt("in.width and out.width must match");
  }

  LSS_connect_bus(in, makenewid.in, in.width);
  LSS_connect_bus(makenewid.out, out, out.width);

  LSS_connect_bus(idgen.out, makenewid.newid, in.width, none);

  idgen.create_data = <<< *data = NULL; 
                          return LSE_signal_something |
                                 LSE_signal_enabled; >>>;

  makenewid.inputs = {"in","newid"};
  makenewid.outputs = {"out"};
  
  makenewid.combine = <<<
    *out_id = newid_id;
    *out_data = *in_data;
    *out_status = in_status;
    LSE_emu_init_instr(*out_id, 1, *out_data);
  >>>;
};

instance pc:delay;
instance emulateinstr:emulate;
instance getnextpc:npc;
instance makenewid:newid;

pc.initial_state = <<<
  LSE_dynid_t myid;
  LSE_emu_iaddr_t addr;

  *init_id=LSE_dynid_create();
  addr=LSE_emu_get_start_addr(1);
  LSE_emu_init_instr(*init_id,1,addr);
  *init_value = addr;

  return TRUE;
>>>;

pc.out ->[LSE_emu_iaddr_t] emulateinstr.in;
emulateinstr.out -> getnextpc.in;
getnextpc.out -> makenewid.in;
makenewid.out -> pc.in;

pc.in.control = <<< return LSE_signal_extract_data(istatus) | 
                           LSE_signal_extract_enable(istatus) | 
                           LSE_signal_ack; >>>;
