/* -*-c-*- 
 * Copyright (c) 2000-2004 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Itanium 2 emulator extensions
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This package provides additional internal instructions in the 
 * Itanium 2 processor.
 *
 * The function attach_extensions should be called only once per
 * lss invocation...
 *
 */
subpackage I2_emu_extensions {

  import corelib;
  import LSE_emu;

  typedef formats_t : struct {
    rsefill : runtime_var ref;
    rsespill : runtime_var ref;
    arupdate : runtime_var ref;
    rnatspill : runtime_var ref;
    rnatfill : runtime_var ref;
  };

  var already_attached = FALSE : boolean;
  var saveFormats : formats_t;

  // Define the instructions...
  fun attach_extensions() => formats_t {
    if (!already_attached) {
      already_attached = TRUE;
      saveFormats.rsefill = new runtime_var("rsefill",int);
      saveFormats.rsespill = new runtime_var("rsespill",int);
      saveFormats.arupdate = new runtime_var("arupdate",int);
      saveFormats.rnatfill = new runtime_var("rnatfill",int);
      saveFormats.rnatspill = new runtime_var("rnatspill",int);

      var emu : instance ref;
      emu = new instance(<<<LSE_IA64_I2_extensions>>>,corelib::sink);

      emu.extension = <<<
#define src_base       LSE_emu_operand_name_src_base
#define src_store_data LSE_emu_operand_name_src_store_data
#define src_psr        LSE_emu_operand_name_src_psr
#define src_rsc        LSE_emu_operand_name_src_rsc
#define src_rnat       LSE_emu_operand_name_src_rnat
#define src_load       LSE_emu_operand_name_src_load
#define src_op1        LSE_emu_operand_name_src_op1
#define dest_rnat      LSE_emu_operand_name_dest_rnat
#define dest_store     LSE_emu_operand_name_dest_store
#define dest_result    LSE_emu_operand_name_dest_result
#define get_src_op(di,op,f) (di->operand_val_src[op].data.f)
#define get_dest_op(di,op,f) (di->operand_val_dest[op].data.f)

      /************ RSE fill instructions  ****************/

      static void decode_rsefill(LSE_emu_instr_info_t *ii) 
      {
	/* CFM not needed because pre-renamed */
	/* no qp */

	ii->operand_src[src_base].uses.reg.bits[0] = ~0;
	ii->operand_src[src_base].spaceid = (LSE_emu_spaceid_t)0;
	ii->operand_src[src_base].spaceaddr.LSE = 1; /* call it immediate */

	ii->operand_src[src_rnat].uses.reg.bits[0] = ~0;
	ii->operand_src[src_rnat].spaceid = LSE_emu_spaceid_AR;
	ii->operand_src[src_rnat].spaceaddr.AR = 19;

	ii->operand_src[src_psr].uses.reg.bits[0] |= (
						      (1ULL<<38) /* da */ |
						      (1ULL<<39) /* dd */ |
						      (1ULL<<17) /* dt */ |
						      (1ULL<<15) /* pk */ |
						      (1ULL<<24) /* db */);
	ii->operand_src[src_rsc].uses.reg.bits[0] = ~0;
	ii->operand_src[src_rsc].spaceid = LSE_emu_spaceid_AR;
	ii->operand_src[src_rsc].spaceaddr.AR = 16;

	ii->operand_src[src_load].uses.mem.flags = LSE_emu_memaccess_read;
	ii->operand_src[src_load].uses.mem.size = 8;
	ii->operand_src[src_load].spaceid = LSE_emu_spaceid_mem;

	ii->operand_dest[dest_result].uses.reg.bits[0] = ~0;
	ii->operand_dest[dest_result].uses.reg.bits[1] = 1;
	ii->operand_dest[dest_result].spaceid = LSE_emu_spaceid_GR;
	ii->operand_dest[dest_result].spaceaddr.GR = 0; /* to fill in */

	ii->iclasses.is_load = TRUE;
	ii->extra.btemplate = 0;
	ii->extra.unit = 0;
	ii->extra.serialization = 0;
	ii->extra.instr = 0;
      }
      static void opfetch_rsefill(LSE_emu_instr_info_t *ii,
				  LSE_emu_operand_name_t opname,
				  int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	switch (opname) {
	  case 0:
	    if (!fallthrough) break;
	  case 1: /* PSR */
	  {
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_SR,
				    &ii->operand_src[1].spaceaddr,
				    0);
	    ii->operand_val_src[1].valid = 1;
	    ii->operand_val_src[1].data.SR = sd.SR;
	    if (!fallthrough) break;
	  }
	  case src_rsc :
	  {
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_src[src_rsc].spaceaddr,
				    0);
	    ii->operand_val_src[src_rsc].valid = 1;
	    ii->operand_val_src[src_rsc].data.AR = sd.AR;
	    if (!fallthrough) break;
	  }
	  case src_rnat :
	  {
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_src[src_rnat].spaceaddr,
				    0);
	    ii->operand_val_src[src_rnat].valid = 1;
	    ii->operand_val_src[src_rnat].data.AR = sd.AR;
	    if (!fallthrough) break;
	  }
	  default : break;
	} /* switch */
      }
      static void evaluate_rsefill(LSE_emu_instr_info_t *ii) 
      {
	LSE_emu_addr_t addr = get_src_op(ii,src_base,AR);
	ii->operand_src[src_load].spaceaddr.mem = addr;
      }
      static void memory_rsefill(LSE_emu_instr_info_t *ii) 
      {
	LSE_emu_spacedata_t sd;
	LSE_emu_call_extra_func(EMU_space_read,
				&sd,ii->swcontexttok,
				LSE_emu_spaceid_mem,
				&ii->operand_src[src_load].spaceaddr,
				8);
	memcpy(get_src_op(ii,src_load,memdata),sd.mem,8);
      }
      static void format_rsefill(LSE_emu_instr_info_t *ii) 
      {
	uint64_t newval;
	int newnat;
	uint64_t rsc = get_src_op(ii,src_rsc,AR);
	LSE_emu_addr_t addr = get_src_op(ii,src_base,GR.val);
	newval = 0;
	if ((rsc>>4)&1) { /* RSC.be */
	  /* newval is now big-endian with 0 in lower (in address) bytes */
	  memcpy(((char *)&newval),get_src_op(ii,src_load,memdata),8);
#ifdef LATER
	  newval = LSE_end_be2h_ll(newval);
#endif
	} else {
	  memcpy((void *)&newval,get_src_op(ii,src_load,memdata),8);
	  /* newval is now little-endian with 0 in upper (in address) bytes */
#ifdef LATER
	  newval = LSE_end_le2h_ll(newval);
#endif
	}
	ii->operand_val_dest[dest_result].data.GR.val = newval;
	newnat = (get_src_op(ii,src_rnat,AR) >> ((addr>>3)&0x3f) )&1;
	ii->operand_val_dest[dest_result].data.GR.nat = newnat;
      }
      static void writeback_rsefill(LSE_emu_instr_info_t *ii,
				    LSE_emu_operand_name_t opname,
				    int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	switch (opname) {
	case dest_result: 
	  {
	    sd.GR.val = get_dest_op(ii,dest_result,GR.val);
	    sd.GR.nat = get_dest_op(ii,dest_result,GR.nat);
	    LSE_emu_call_extra_func(EMU_space_write,
				    ii->swcontexttok,
				    LSE_emu_spaceid_GR,
				    &ii->operand_dest[dest_result].spaceaddr,
				    &sd, 0);
	  }
	  if (!fallthrough) break;
	default:
	  break;
	} /* switch */
      }
      static void disassemble_rsefill(LSE_emu_instr_info_t *ii,
				      FILE *fp) 
      {
	fprintf(fp,"     " "RSE fill reg %d\n",
		ii->operand_dest[dest_result].spaceaddr.GR);
      }


      static void decode_rnatfill(LSE_emu_instr_info_t *ii) 
      {
	/* CFM not needed because pre-renamed */
	/* no qp */

	ii->operand_src[src_base].uses.reg.bits[0] = ~0;
	ii->operand_src[src_base].spaceid = (LSE_emu_spaceid_t)0;
	ii->operand_src[src_base].spaceaddr.LSE = 1; /* call it immediate */

	ii->operand_src[src_psr].uses.reg.bits[0] |= (
						      (1ULL<<38) /* da */ |
						      (1ULL<<39) /* dd */ |
						      (1ULL<<17) /* dt */ |
						      (1ULL<<15) /* pk */ |
						      (1ULL<<24) /* db */);
	ii->operand_src[src_rsc].uses.reg.bits[0] = ~0;
	ii->operand_src[src_rsc].spaceid = LSE_emu_spaceid_AR;
	ii->operand_src[src_rsc].spaceaddr.AR = 16;

	ii->operand_src[src_load].uses.mem.flags = LSE_emu_memaccess_read;
	ii->operand_src[src_load].uses.mem.size = 8;
	ii->operand_src[src_load].spaceid = LSE_emu_spaceid_mem;

	ii->operand_dest[dest_result].uses.reg.bits[0] = ~0;
	ii->operand_dest[dest_result].spaceid = LSE_emu_spaceid_AR;
	ii->operand_dest[dest_result].spaceaddr.AR = 19;

	ii->iclasses.is_load = TRUE; 
	ii->extra.btemplate = 0;
	ii->extra.unit = 0;
	ii->extra.serialization = 0;
	ii->extra.instr = 0;
     }
      static void opfetch_rnatfill(LSE_emu_instr_info_t *ii,
				  LSE_emu_operand_name_t opname,
				  int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	switch (opname) {
	  case 0:
	    if (!fallthrough) break;
	  case 1: /* PSR */
	  {
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_SR,
				    &ii->operand_src[1].spaceaddr,
				    0);
	    ii->operand_val_src[1].valid = 1;
	    ii->operand_val_src[1].data.SR = sd.SR;
	    if (!fallthrough) break;
	  }
	  case src_rsc :
	  {
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_src[src_rsc].spaceaddr,
				    0);
	    ii->operand_val_src[src_rsc].valid = 1;
	    ii->operand_val_src[src_rsc].data.AR = sd.AR;
	    if (!fallthrough) break;
	  }
	  default : break;
	} /* switch */
      }
      static void evaluate_rnatfill(LSE_emu_instr_info_t *ii) 
      {
	LSE_emu_addr_t addr = get_src_op(ii,src_base,AR);
	ii->operand_src[src_load].spaceaddr.mem = addr;
      }
      static void memory_rnatfill(LSE_emu_instr_info_t *ii) 
      {
	LSE_emu_spacedata_t sd;
	LSE_emu_call_extra_func(EMU_space_read,
				&sd,ii->swcontexttok,
				LSE_emu_spaceid_mem,
				&ii->operand_src[src_load].spaceaddr,
				8);
	memcpy(get_src_op(ii,src_load,memdata),sd.mem,8);
      }
      static void format_rnatfill(LSE_emu_instr_info_t *ii) 
      {
	uint64_t newval;
	uint64_t rsc = get_src_op(ii,src_rsc,AR);
	LSE_emu_addr_t addr = get_src_op(ii,src_base,AR);
	newval = 0;
	if ((rsc>>4)&1) { /* RSC.be */
	  /* newval is now big-endian with 0 in lower (in address) bytes */
	  memcpy(((char *)&newval),get_src_op(ii,src_load,memdata),8);
#ifdef LATER
	  newval = LSE_end_be2h_ll(newval);
#endif
	} else {
	  memcpy((void *)&newval,get_src_op(ii,src_load,memdata),8);
	  /* newval is now little-endian with 0 in upper (in address) bytes */
#ifdef LATER
	  newval = LSE_end_le2h_ll(newval);
#endif
	}
	ii->operand_val_dest[dest_result].data.AR = newval;
      }
      static void writeback_rnatfill(LSE_emu_instr_info_t *ii,
				    LSE_emu_operand_name_t opname,
				    int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	switch (opname) {
	case dest_result: 
	  {
	    sd.AR = get_dest_op(ii,dest_result,AR);
	    LSE_emu_call_extra_func(EMU_space_write,
				    ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_dest[dest_result].spaceaddr,
				    &sd, 0);
	  }
	  if (!fallthrough) break;
	default:
	  break;
	} /* switch */
      }
      static void disassemble_rnatfill(LSE_emu_instr_info_t *ii,
				      FILE *fp) 
      {
	fprintf(fp,"     " "RNAT fill\n");
      }


      /************ RSE spill instructions  ****************/

      static void decode_rsespill(LSE_emu_instr_info_t *ii) 
      {
	/* CFM not needed because pre-renamed */
	/* no qp */
	ii->operand_src[src_store_data].uses.reg.bits[0] = ~0;
	ii->operand_src[src_store_data].uses.reg.bits[1] = 1;
	ii->operand_src[src_store_data].spaceid = LSE_emu_spaceid_GR;
	ii->operand_src[src_store_data].spaceaddr.GR = 0; /* to fill in */

	ii->operand_src[src_base].uses.reg.bits[0] = ~0;
	ii->operand_src[src_base].spaceid = (LSE_emu_spaceid_t)0;
	ii->operand_src[src_base].spaceaddr.LSE = 1; /* call it immediate */
	ii->operand_src[src_psr].uses.reg.bits[0] |= (
						      (1ULL<<38) /* da */ |
						      (1ULL<<39) /* dd */ |
						      (1ULL<<17) /* dt */ |
						      (1ULL<<15) /* pk */ |
						      (1ULL<<24) /* db */);
	ii->operand_src[src_rsc].uses.reg.bits[0] = ~0;
	ii->operand_src[src_rsc].spaceid = LSE_emu_spaceid_AR;
	ii->operand_src[src_rsc].spaceaddr.AR = 16;

	ii->operand_dest[dest_rnat].uses.reg.bits[0] = ~0;
	ii->operand_dest[dest_rnat].spaceid = LSE_emu_spaceid_AR;
	ii->operand_dest[dest_rnat].spaceaddr.AR = 19;

	ii->operand_dest[dest_store].uses.mem.flags = LSE_emu_memaccess_write;
	ii->operand_dest[dest_store].uses.mem.size = 8;
	ii->operand_dest[dest_store].spaceid = LSE_emu_spaceid_mem;

	ii->iclasses.is_store = TRUE;
	ii->extra.btemplate = 0;
	ii->extra.unit = 0;
	ii->extra.serialization = 0;
	ii->extra.instr = 0;
      } /* decode_rsespill */
      static void opfetch_rsespill(LSE_emu_instr_info_t *ii,
				   LSE_emu_operand_name_t opname,
				   int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	switch (opname) {
	  case 0:
	    if (!fallthrough) break;
	  case 1: /* PSR */
	  {
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_SR,
				    &ii->operand_src[1].spaceaddr,
				    0);
	    ii->operand_val_src[1].valid = 1;
	    ii->operand_val_src[1].data.SR = sd.SR;
	    if (!fallthrough) break;
	  }
	  case src_store_data : /* will be in GR */
	  {
	    /* already rotated because that's how we defined the
	     * instruction....
	     */
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_GR,
				    &ii->operand_src[src_store_data].
				    spaceaddr,
				    0);
	    ii->operand_val_src[src_store_data].valid = 1;
	    ii->operand_val_src[src_store_data].data.GR.val = sd.GR.val;
	    ii->operand_val_src[src_store_data].data.GR.nat = sd.GR.nat;
	    if (!fallthrough) break;
	  }
	  case src_rsc :
	  {
	    /* already rotated because that's how we defined the
	     * instruction....
	     */
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_src[src_rsc].spaceaddr,
				    0);
	    ii->operand_val_src[src_rsc].valid = 1;
	    ii->operand_val_src[src_rsc].data.AR = sd.AR;
	    if (!fallthrough) break;
	  }
	  default : break;
	} /* switch */
      } /* opfetch_rsespill */
      static void evaluate_rsespill(LSE_emu_instr_info_t *ii) 
      {
	LSE_emu_addr_t addr = get_src_op(ii,src_base,GR.val);

	ii->operand_dest[dest_store].spaceaddr.mem = addr;
    
	ii->operand_val_dest[dest_rnat].data.AR 
	= ((ii->operand_val_src[src_store_data].data.GR.nat) ?
	   (1LL<<((addr>>3) & 0x3f)) : 0);
      }
      static void memory_rsespill(LSE_emu_instr_info_t *ii) 
      { /* do nothing */
      }
      static void format_rsespill(LSE_emu_instr_info_t *ii) 
      {
	uint64_t len = 8;
	uint64_t newval;
	uint64_t rsc = get_src_op(ii,src_rsc,AR);
	
	newval = get_src_op(ii,src_store_data,GR.val);
	if ((rsc>>4)&1) { /* RSC.be */
#ifdef LATER
	  newval = LSE_end_h2be_ll(newval);
#endif
	  memcpy(get_dest_op(ii,dest_store,memdata),
		 ((char *)&newval)+8-len,len);
	} else {
#ifdef LATER
	  newval = LSE_end_h2le_ll(newval);
#endif
	  memcpy(get_dest_op(ii,dest_store,memdata),&newval,len);
	}
      }
      static void writeback_rsespill(LSE_emu_instr_info_t *ii,
				    LSE_emu_operand_name_t opname,
				    int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	int memerr;
	switch (opname) {
	case 0 :
	  if (!fallthrough) break;
	case dest_store: /* so we can fall through */
	  memcpy(sd.mem,get_dest_op(ii,dest_store,memdata),8);
	  LSE_emu_call_extra_func(EMU_space_write,
				  ii->swcontexttok,
				  LSE_emu_spaceid_mem,
				  &ii->operand_dest[dest_store].spaceaddr,
				  &sd, 8);
	  if (!fallthrough) break;
	case dest_rnat: 
	  {
	    uint64_t mask;
	    mask = (1LL<<((get_src_op(ii,src_base,GR.val)>>3) & 0x3f));
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_dest[dest_rnat].spaceaddr,
				    0);
	    sd.AR = ((~mask & sd.AR) | 
		     (mask & get_dest_op(ii,dest_rnat,AR)));
	    LSE_emu_call_extra_func(EMU_space_write,
				    ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_dest[dest_rnat].spaceaddr,
				    &sd, 0);
	  }
	  if (!fallthrough) break;
	default:
	  break;
	} /* switch */
      }
      static void disassemble_rsespill(LSE_emu_instr_info_t *ii,
				      FILE *fp) 
      {
	fprintf(fp,"     " "RSE spill reg %d\n",
		ii->operand_src[src_store_data].spaceaddr.GR);
      }


      /**** RNAT spilling *****/

      static void decode_rnatspill(LSE_emu_instr_info_t *ii) 
      {
	/* CFM not needed */
	/* no qp */
	ii->operand_src[src_store_data].uses.reg.bits[0] = ~0;
	ii->operand_src[src_store_data].uses.reg.bits[1] = 1;
	ii->operand_src[src_store_data].spaceid = LSE_emu_spaceid_AR;
	ii->operand_src[src_store_data].spaceaddr.GR = 19;

	ii->operand_src[src_base].uses.reg.bits[0] = ~0;
	ii->operand_src[src_base].spaceid = (LSE_emu_spaceid_t)0;
	ii->operand_src[src_base].spaceaddr.LSE = 1; /* call it immediate */
	ii->operand_src[src_psr].uses.reg.bits[0] |= (
						      (1ULL<<38) /* da */ |
						      (1ULL<<39) /* dd */ |
						      (1ULL<<17) /* dt */ |
						      (1ULL<<15) /* pk */ |
						      (1ULL<<24) /* db */);
	ii->operand_src[src_rsc].uses.reg.bits[0] = ~0;
	ii->operand_src[src_rsc].spaceid = LSE_emu_spaceid_AR;
	ii->operand_src[src_rsc].spaceaddr.AR = 16;

	ii->operand_dest[dest_store].uses.mem.flags = LSE_emu_memaccess_write;
	ii->operand_dest[dest_store].uses.mem.size = 8;
	ii->operand_dest[dest_store].spaceid = LSE_emu_spaceid_mem;

	ii->iclasses.is_store = TRUE;
	ii->extra.btemplate = 0;
	ii->extra.unit = 0;
	ii->extra.serialization = 0;
	ii->extra.instr = 0;
      } /* decode_rnatspill */
      static void opfetch_rnatspill(LSE_emu_instr_info_t *ii,
				   LSE_emu_operand_name_t opname,
				   int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	switch (opname) {
	  case 0:
	    if (!fallthrough) break;
	  case 1: /* PSR */
	  {
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_SR,
				    &ii->operand_src[1].spaceaddr,
				    0);
	    ii->operand_val_src[1].valid = 1;
	    ii->operand_val_src[1].data.SR = sd.SR;
	    if (!fallthrough) break;
	  }
	  case src_store_data : /* will be in GR */
	  {
	    /* already rotated because that's how we defined the
	     * instruction....
	     */
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_src[src_store_data].
				    spaceaddr,
				    0);
	    ii->operand_val_src[src_store_data].valid = 1;
	    ii->operand_val_src[src_store_data].data.AR = sd.AR;
	    if (!fallthrough) break;
	  }
	  case src_rsc :
	  {
	    /* already rotated because that's how we defined the
	     * instruction....
	     */
	    LSE_emu_call_extra_func(EMU_space_read,
				    &sd,ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_src[src_rsc].spaceaddr,
				    0);
	    ii->operand_val_src[src_rsc].valid = 1;
	    ii->operand_val_src[src_rsc].data.AR = sd.AR;
	    if (!fallthrough) break;
	  }
	  default : break;
	} /* switch */
      } /* opfetch_rnatspill */
      static void evaluate_rnatspill(LSE_emu_instr_info_t *ii) 
      {
	LSE_emu_addr_t addr = get_src_op(ii,src_base,GR.val);

	ii->operand_dest[dest_store].spaceaddr.mem = addr;
    
      }
      static void memory_rnatspill(LSE_emu_instr_info_t *ii) 
      { /* do nothing */
      }
      static void format_rnatspill(LSE_emu_instr_info_t *ii) 
      {
	uint64_t len = 8;
	uint64_t newval;
	uint64_t rsc = get_src_op(ii,src_rsc,AR);
	
	newval = get_src_op(ii,src_store_data,AR);
	if ((rsc>>4)&1) { /* RSC.be */
#ifdef LATER
	  newval = LSE_end_h2be_ll(newval);
#endif
	  memcpy(get_dest_op(ii,dest_store,memdata),
		 ((char *)&newval)+8-len,len);
	} else {
#ifdef LATER
	  newval = LSE_end_h2le_ll(newval);
#endif
	  memcpy(get_dest_op(ii,dest_store,memdata),&newval,len);
	}
      }
      static void writeback_rnatspill(LSE_emu_instr_info_t *ii,
				    LSE_emu_operand_name_t opname,
				    int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	switch (opname) {
	case 0 :
	  if (!fallthrough) break;
	case dest_store: /* so we can fall through */
	  memcpy(sd.mem,get_dest_op(ii,dest_store,memdata),8);
	  LSE_emu_call_extra_func(EMU_space_write,
				  ii->swcontexttok,
				  LSE_emu_spaceid_mem,
				  &ii->operand_dest[dest_store].spaceaddr,
				  &sd, 8);
	  if (!fallthrough) break;
	default:
	  break;
	} /* switch */
      }
      static void disassemble_rnatspill(LSE_emu_instr_info_t *ii,
				      FILE *fp) 
      {
	fprintf(fp,"     " "RNAT spill\n");
      }

      /******** writeback of ARs after a br.ret fill ********/

      static void decode_arupdate(LSE_emu_instr_info_t *ii) 
      {
	/* CFM not needed */
	/* no qp */
	ii->operand_src[src_op1].uses.reg.bits[0] = ~0;
	ii->operand_src[src_op1].spaceid = (LSE_emu_spaceid_t)0;
	ii->operand_src[src_op1].spaceaddr.LSE = 1; /* call it immediate */

	ii->operand_dest[dest_result].uses.reg.bits[0] = ~0;
	ii->operand_dest[dest_result].spaceid = LSE_emu_spaceid_AR;
	ii->operand_dest[dest_result].spaceaddr.AR = 18;
	ii->extra.btemplate = 0;
	ii->extra.unit = 0;
	ii->extra.serialization = 0;
	ii->extra.instr = 0;
      }
      static void opfetch_arupdate(LSE_emu_instr_info_t *ii,
				  LSE_emu_operand_name_t opname,
				  int fallthrough) 
      { /* actually nothing to do! the user must supply the immediate */
      }
      static void evaluate_arupdate(LSE_emu_instr_info_t *ii) 
      {
	ii->operand_val_dest[dest_result].data.AR = get_src_op(ii,src_op1,AR);
      }
      static void memory_arupdate(LSE_emu_instr_info_t *ii) 
      {
      }
      static void format_arupdate(LSE_emu_instr_info_t *ii) 
      {
      }
      static void writeback_arupdate(LSE_emu_instr_info_t *ii,
				    LSE_emu_operand_name_t opname,
				    int fallthrough) 
      {
	LSE_emu_spacedata_t sd;
	switch (opname) {
	case dest_result: 
	  {
	    sd.AR = get_dest_op(ii,dest_result,AR);
	    LSE_emu_call_extra_func(EMU_space_write,
				    ii->swcontexttok,
				    LSE_emu_spaceid_AR,
				    &ii->operand_dest[dest_result].spaceaddr,
				    &sd, 0);
	  }
	  if (!fallthrough) break;
	default:
	  break;
	} /* switch */
      }
      static void disassemble_arupdate(LSE_emu_instr_info_t *ii,
				      FILE *fp) 
      {
	fprintf(fp,"     " "RSE AR (BSPSTORE) update\n");
      }

#undef get_src_op
#undef get_dest_op
#undef src_base
#undef src_store_data
#undef src_psr
#undef src_rsc
#undef src_rnat
#undef src_load
#undef src_op1
#undef dest_rnat
#undef dest_store
#undef dest_result
      >>>;

      emu.init = <<<{
	IA64_instr_hooks_t hooks;
	hooks.decode = decode_rsefill;
	hooks.opfetch = opfetch_rsefill;
	hooks.evaluate = evaluate_rsefill;
	hooks.memory = memory_rsefill;
	hooks.format = format_rsefill;
	hooks.writeback = writeback_rsefill;
	hooks.disassemble = disassemble_rsefill;
	${saveFormats.rsefill} = LSE_emu_call_extra_func(IA64_instr_define,
							 &hooks);
	hooks.decode = decode_rnatfill;
	hooks.opfetch = opfetch_rnatfill;
	hooks.evaluate = evaluate_rnatfill;
	hooks.memory = memory_rnatfill;
	hooks.format = format_rnatfill;
	hooks.writeback = writeback_rnatfill;
	hooks.disassemble = disassemble_rnatfill;
	${saveFormats.rnatfill} = LSE_emu_call_extra_func(IA64_instr_define,
							  &hooks);
	hooks.decode = decode_rsespill;
	hooks.opfetch = opfetch_rsespill;
	hooks.evaluate = evaluate_rsespill;
	hooks.memory = memory_rsespill;
	hooks.format = format_rsespill;
	hooks.writeback = writeback_rsespill;
	hooks.disassemble = disassemble_rsespill;
	${saveFormats.rsespill} = LSE_emu_call_extra_func(IA64_instr_define,
							  &hooks);
	hooks.decode = decode_rnatspill;
	hooks.opfetch = opfetch_rnatspill;
	hooks.evaluate = evaluate_rnatspill;
	hooks.memory = memory_rnatspill;
	hooks.format = format_rnatspill;
	hooks.writeback = writeback_rnatspill;
	hooks.disassemble = disassemble_rnatspill;
	${saveFormats.rnatspill} = LSE_emu_call_extra_func(IA64_instr_define,
							   &hooks);
	hooks.decode = decode_arupdate;
	hooks.opfetch = opfetch_arupdate;
	hooks.evaluate = evaluate_arupdate;
	hooks.memory = memory_arupdate;
	hooks.format = format_arupdate;
	hooks.writeback = writeback_arupdate;
	hooks.disassemble = disassemble_arupdate;
	${saveFormats.arupdate} = LSE_emu_call_extra_func(IA64_instr_define,
							  &hooks);
      }>>>;

    }
    return saveFormats;
  };

};  /* subpackage I2_pfMon */
