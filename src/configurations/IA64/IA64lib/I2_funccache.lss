/* -*-c-*- 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * This module is a functional model of a cache
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This module is a functional model of a cache with minimal functionality
 *
 */

module I2_funccache
{
  using IA64lib.I2_funccache;
  using LSE_emu;
  import LSE_chkpt;
  add_to_domain_searchpath(LSE_chkpt::checkpointer);
  add_to_domain_searchpath(LSE_domain.LSE_emu);

/*  Specifies the tar file to use to get the module code  */

  tar_file = "IA64lib/I2_funccache.clm";

  phase_start = FALSE;
  phase_end = FALSE;
  phase = FALSE;
  reactive = FALSE;
  port_dataflow = <<<[]>>>;

/*  Define parameters of the module  */

  runtimeable parameter nsets = 1:int;
  runtimeable parameter bsize = 1:int;
  runtimeable parameter assoc = 1:int;
  runtimeable parameter hit_latency = 1:int;

/*  Define ports of the module  */

/*  Define control functions and queries in the module  */

  method cache_access:(<<<LSE_dynid_t id, int iswrite,
		       LSE_emu_addr_t addr, int size,
		       LSE_time_t start_at, LSE_emu_addr_t *replace_addr, 
		       boolean timed, int flags>>> 
		       => <<< int >>>);
  
  method cache_probe_victim:(<<<LSE_dynid_t id,
		      LSE_emu_addr_t addr, LSE_emu_addr_t *victimp >>> 
   => <<< int >>>);

  parameter handle_miss = <<<
    return 1;
  >>>:
  userpoint(<<<LSE_dynid_t id, int iswrite, LSE_emu_addr_t addr, int size,
	    LSE_time_t start_at, boolean timed, int flags>>> => 
	    <<< int >>>);

  parameter chkpt_replay_access = <<<>>> :
    userpoint(<<<LSE_emu_addr_t addr, boolean isWrite>>> => <<< void >>>);

  parameter chkpt_read_data = <<<>>> :
    userpoint(<<<void >>> => <<< void >>>);

  /*  Define events in the module  */

  emits event ACCESS {
    id:<<<LSE_dynid_t >>>;
    iswrite:<<<int >>>;
    addr:<<<LSE_emu_addr_t >>>;
    size:<<<int >>>;
    hit:<<<boolean >>>;
    flags: <<<int>>>;
  };

  emits event WRITEBACK
  {
    id:<<<LSE_dynid_t >>>;

  };

  emits event INVALIDATE
  {
    id:<<<LSE_dynid_t >>>;

  };

  emits event REPLACE
  {
    id:<<<LSE_dynid_t >>>;

  };

  /************************************************************/
  /************** Methods/userpoints for checkpointing ********/
  /************************************************************/
  /* 
   * polymorphism reigns, but makes life difficult for us, principally
   * in the encoding of data....  
   */

  method chkpt_add_toc:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  method chkpt_check_toc:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  method chkpt_read_data:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  method chkpt_skip_data:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  method chkpt_write_data:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

};
