/* 
 * Copyright (c) 2000-2004 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Itanium 2 DET stage
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This module is the DET stage of an Itanium 2 processor.  This stage
 * performs within-group bypassing of PR, BR, and PFS to branches and
 * then detects branch mispredictions and exception.s
 *
 *
 * What can be parameterized:
 *
 * - hardware context number
 *
 * Requires a parameters: 
 * - what module stored the branch information?
 *
 * Note that the number of issue ports of different kinds are set
 * by the port widths of the corresponding output ports.  Likewise,
 * the number of bundles to consider is set by the input port
 * width.
 *
 * TODO:
 *   - deal with microtraps that replay the trapping instruction
 *   - interruptions/microtraps should end up inserting 8 bubbles; two
 *     more than the normal misprediction recovery.
 *
 */


module I2_DET
{
  import corelib;
  import LSE_emu;
  add_to_domain_searchpath(LSE_domain.LSE_emu);

  /***************************************************************/
  /************************* Parameters ***************************/
  /***************************************************************/

  /* hardware context number for emulation */
  parameter hwContextNum = 1 : int;

  /* number of bytes in a bundle (used for finding slot number) */
  parameter bundleBytes = 16 : int;

  /* who stored the branch information? */
  internal parameter branchSinkName : literal;

  /* front-end ID number field; needed to handle interrupts properly */
  internal parameter feIDno : literal;

  /* back-end ID number field; needed to handle redirects properly */
  internal parameter beIDno : literal;

  runtimeable parameter reportBypass = FALSE : boolean;
  runtimeable parameter dumbBBB = TRUE : boolean;

  /***************************************************************/
  /************************* Ports *******************************/
  /***************************************************************/

  /* instructions which have been executed */
  inport ALU_in : IA64lib::TypeOf_renamed_instr;
  inport BR_in : IA64lib::TypeOf_renamed_instr;
  inport OTHER_in : IA64lib::TypeOf_renamed_instr; /* any other port */

  /* other exception sources; dynid should be excepting instruction */
  inport exceptions : LSE_emu::LSE_emu_addr_t;

  /* redirection because of mis-prediction or traps */
  outport redirect_fetch : LSE_emu::LSE_emu_addr_t;

  /* oldest taken branch (without taking into account redirection) */
  outport taken_branch : IA64lib::TypeOf_renamed_instr;

  /* between-instructions interruption - will cause a redirect_fetch 
   * to be sent to everybody.
   */
  inport interrupt : LSE_emu::LSE_emu_addr_t;

  /*
   * micro-trap to be taken on or after an instruction
   */
  inport microtrap : LSE_emu::LSE_emu_addr_t;

  /* stalls computed elsewhere that should prevent detection */
  inport stallDET : *;

  /***************************************************************/
  /************************* sanity checks ***********************/
  /***************************************************************/

  /************************************************************************/
  /********************** Useful measurements *****************************/
  /************************************************************************/

  emits event REDIRECTED {
    id : <<<LSE_dynid_t>>>;
    addr : <<<LSE_emu_addr_t>>>;
    reason : <<<int>>>; /* 1 = sideeffect, 2 = microtrap, 3 = bpred,
			 * 4 = PFS, 5 = rotation, 6 = PSR, 7 = interrupt,
			 * 8 = rotation, 9 = stage(don't know what this is)
			 */
  };
  
  /************************************************************************/
  /********************** And the work....    *****************************/
  /************************************************************************/

   /* This stage has three functions:
    *
    * I. Bypass predicates, BRs, and PFS into branch instructions
    * 
    * II. Detect pipeline flush conditions.  There are 
    *    five of them:
    *
    *    1) exception (including break instructions and microtraps)
    *    2) branch misprediction
    *    3) stage misprediction (don't know exactly what this is, so
    *                            we don't have it at present)
    *    4) rotation misprediction (wrong CFM due to wrong rotation)
    *    5) pfs misprediction (wrong CFM due to wrong PFS prediction)
    *
    * III. Determine oldest taken branches so that younger instructions 
    *      in the same bundle may be removed on their way to the next stage.
    *
    * Note that bypassing must happen before flush detection and can only 
    * happen for ALUs to BR units because it is illegal to have BR->BR WAR
    * dependencies in the same group.  But we just make it happen for all
    * because that's easiest...
    *
    */

  instance detectSort : corelib::arbiter;
  instance bypassCalc : corelib::reducer;
  instance bypassCalcTee : corelib::tee;
  instance detectMispred : corelib::reducer;
  instance takenBranch : corelib::aligner;

  var totalPorts = ALU_in.width + BR_in.width + OTHER_in.width : int;

   { var i : int;
   for (i=0;i<ALU_in.width;i++) { ALU_in -> detectSort.in; }
   for (i=0;i<OTHER_in.width;i++) { OTHER_in -> detectSort.in; }
   for (i=0;i<BR_in.width;i++) { BR_in -> detectSort.in; }
   }

  detectSort.comparison_func = <<<
    /* want to force nothings to end... */

    if (!LSE_signal_data_known(status1) || !LSE_signal_data_known(status2))
      return -1;
    else if (!LSE_signal_data_present(status1) ||
	     (LSE_signal_data_present(status2) &&
	      LSE_dynid_get(id1,${beIDno})>LSE_dynid_get(id2,${beIDno})))
      return 1; /* out of order */
      /* force nothing to end */
    else return 0;
  >>>;

  LSS_connect_bus(detectSort.out,bypassCalcTee.in,totalPorts);

  { var i : int;
  for (i=0;i<totalPorts;i++) {
    bypassCalcTee.out[i*3] -> bypassCalc.in;
    bypassCalcTee.out[i*3+1] -> detectMispred.in;
    bypassCalcTee.out[i*3+2] -> takenBranch.in;
  }
  }

  /* finding the oldest taken branch is as simple as passing through
   * all taken branches to an aligner, but it does require that the 
   * branches be in program order and that bypasses be calculated first.
   *
   * Note that "check"-style and side-effecting branches are not a concern
   * since they *always* lead to a redirect; we're just worried about
   * taken branches in the middle of bundles that aren't redirects.
   *
   */
  takenBranch.in.control = <<<
    LSE_signal_t sig = LSE_port_query(${bypassCalc}:out[0].data,NULL,NULL);

  if (!LSE_signal_data_known(sig)||
      !LSE_signal_data_known(istatus)) 
    return LSE_signal_enabled | LSE_signal_ack;

  if (LSE_signal_data_present(istatus) && 
      LSE_emu_dynid_is(id,cti) && LSE_emu_dynid_get(id,branch_dir))
    return LSE_signal_all_yes;
  else return (LSE_signal_nothing | LSE_signal_enabled | LSE_signal_ack);
  >>>;

  takenBranch.out -> taken_branch;

  /***************** Calculate the bypasses *************/

  bypassCalc.reduce_calculates_enable = FALSE;
  bypassCalc.wait_for_data = TRUE;
  bypassCalc.propagate_nothing = TRUE;
  bypassCalc.out.width = 1; /* force the port to open */

  bypassCalc.reduce = <<<
#define src_qp LSE_emu_operand_name_src_qp
#define src_pfs LSE_emu_operand_name_src_pfs
#define src_branchoff LSE_emu_operand_name_src_branchoff
  int i,j;
  LSE_dynid_t id;
  boolean modsCFM;
  LSE_emu_operand_val_t oldCFM;
  LSE_emu_operand_info_t opinf;
  int paddr, offset, formatno;
  uint64_t instr;

  struct { /* do we have bypass values? */
    boolean PR[64];
    boolean PFS;
    boolean BR[8];
  } bypassControl;
  struct {
    boolean PR[64];
    uint64_t PFS;
    uint64_t BR[8];
  } bypassVal;

  memset(&bypassControl,0,sizeof(bypassControl));

  for (i=0;i<LSE_port_width(in);i++) {

    boolean writer,pwriter;

    if (!LSE_signal_data_present(in_statusp[i]) || !in_datap[i].valid) 
      continue;

    id = in_idp[i];

    /* Set so-far bypassed values into control instructions... */

    if (LSE_emu_dynid_is(id,cti) && LSE_emu_dynid_get(id,extra.unit)==3
	&&!LSE_emu_dynid_is(id,sideeffect)) {
      
      /* check qp */
      opinf = LSE_emu_dynid_get(id,operand_src[src_qp]);
      opinf.spaceaddr.PR = in_datap[i].srcs[src_qp].PR & 0xffff;
      if (opinf.spaceid==LSE_emu_spaceid_PR && 
	  bypassControl.PR[opinf.spaceaddr.PR]) {
	
	if (${reportBypass}) {
	  fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
	  fprintf(LSE_stderr, " BY: DET bypassed predicate %d, value %d\n",
		  opinf.spaceaddr.PR,
		  bypassVal.PR[opinf.spaceaddr.PR]);
	}
	LSE_emu_dynid_set(id,operand_val_src[src_qp].data.PR,
			  bypassVal.PR[opinf.spaceaddr.PR]);
      }
      
      /* check pfs */
      opinf = LSE_emu_dynid_get(id,operand_src[src_pfs]);
      if (bypassControl.PFS && opinf.spaceid==LSE_emu_spaceid_AR &&
	  opinf.spaceaddr.AR==64) {
	if (${reportBypass}) {	
	  fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
	  fprintf(LSE_stderr,
		  " BY: DET bypassed PFS value %llx\n", bypassVal.PFS);
	}
	LSE_emu_dynid_set(id,operand_val_src[src_pfs].data.AR,
			  bypassVal.PFS);
      }
      
      /* check offset */
      opinf = LSE_emu_dynid_get(id,operand_src[src_branchoff]);
      if (opinf.spaceid==LSE_emu_spaceid_BR &&
	  bypassControl.BR[opinf.spaceaddr.BR]) {
	if (${reportBypass}) {
	  fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
	  fprintf(LSE_stderr," BY: DET bypassed BR[%d] value %llx\n", 
		  opinf.spaceaddr.BR,bypassVal.BR[opinf.spaceaddr.BR]);
	}
	LSE_emu_dynid_set(id,operand_val_src[src_branchoff].data.BR,
			  bypassVal.BR[opinf.spaceaddr.BR]);
      }
      
      /* and evaluate after bypassing */
      
      if (!LSE_emu_dynid_is(id,sideeffect)) {
	LSE_emu_do_instrstep(id,LSE_emu_instrstep_name_evaluate);
      }

    } /* if is a CTI */

    formatno = LSE_emu_dynid_get(id,extra.formatno);
    instr = LSE_emu_dynid_get(id,extra.instr);

    /****************** bypass from outputs of instr *********************
     * we don't have to worry about bypassing from branch to branch,
     * as none of those dependencies are allowed.  (The apparent one
     * between a ctop-like branch writing PR63 and a subsequent using
     * PR63 is removed by the fact that the PR63-writing branch must
     * end a bundle, and thus must be followed by a break since it is
     * a branch at the end of a bundle.)
     *
     * we do have to worry about the fact that predicate writes depend
     * upon more than just the qp....
     *
     */

    writer = LSE_emu_dynid_get(id,operand_val_src[src_qp].data.PR);
    pwriter =
      (formatno == 506 || formatno == 507 || formatno == 508 || /* int cmp */
       formatno == 16 || formatno == 17) &&   /* test bit/NaT */
      LSE_emu_dynid_get(id,operand_val_int[LSE_emu_operand_name_int_updatepred]
			.data.PR);
    
    if (writer || pwriter) {
      
      for (j=0;j<LSE_emu_max_operand_dest;j++) {
	int offset;
	
	opinf = LSE_emu_dynid_get(id,operand_dest[j]);

	if (opinf.spaceid == LSE_emu_spaceid_PR) {
	  opinf.spaceaddr.PR = in_datap[i].dests[j].PR & 0xffff;
	  if (opinf.spaceaddr.PR != 64) {
	    if (pwriter) {
	      bypassControl.PR[opinf.spaceaddr.PR] = TRUE;
	      bypassVal.PR[opinf.spaceaddr.PR] 
		= LSE_emu_dynid_get(id,operand_val_dest[j].data.PR);
	      
	      if (${reportBypass}) {
		fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
		fprintf(LSE_stderr,
			" BY: DET saw bypassable %d to predicate %d\n",
			bypassVal.PR[opinf.spaceaddr.PR],
			opinf.spaceaddr.PR);
	      }
	    }
	  } else if (writer) { /* bulk preds */
	    int k;
	    uint64_t PRdata 
	      = LSE_emu_dynid_get(id,operand_val_dest[j].data.imm64);
	    for (k=1;k<64;k++) {
	      if (opinf.uses.reg.bits[0] & (1ULL<<k)) { 
		bypassControl.PR[k] = TRUE;
		bypassVal.PR[k] = (PRdata >> k) & 1; 
		if (${reportBypass}) {
		  fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
		  fprintf(LSE_stderr,
			  " BY: DET saw bypassable %d to predicate %d\n",
			  bypassVal.PR[k],k);
		}
	      }
	    }
	  } /* writer */
	  
	} else if (opinf.spaceid == LSE_emu_spaceid_BR && writer) {
	  
	  bypassControl.BR[opinf.spaceaddr.BR] = TRUE;
	  bypassVal.BR[opinf.spaceaddr.BR] 
	    = LSE_emu_dynid_get(id,operand_val_dest[j].data.BR);
	  if (${reportBypass}) {
	    fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
	    fprintf(LSE_stderr, " BY: DET saw bypassable BR%d\n",
		    opinf.spaceaddr.BR);
	  }
	} else if (opinf.spaceid == LSE_emu_spaceid_AR &&
		   opinf.spaceaddr.AR == 64 && writer) {
	  
	  bypassControl.PFS = TRUE;
	  bypassVal.PFS = LSE_emu_dynid_get(id,operand_val_dest[j].data.AR);
	  if (${reportBypass}) {
	    fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
	    fprintf(LSE_stderr, " BY: DET saw bypassable PFS\n");
	  }
	}
	
      } /* for destination operands */
    } /* is a writer of something */

  } /* for ports */
  *out_statusp = LSE_signal_nothing;
  return;
#undef src_qp
#undef src_pfs
#undef src_branchoff
  >>>;

  structadd(detectMispred, <<<LSE_dynid_t>>>,<<<int>>>,<<<mispred>>>);

  /* make detection/bypassing/routing wait until we've bypassed */
  detectMispred.in.control = <<<
    LSE_signal_t sig;
    sig = LSE_port_query(${bypassCalc}:out[0].data,NULL,NULL);
    if (LSE_signal_data_known(sig)) 
      return (LSE_signal_something | LSE_signal_extract_enable(istatus) |
	      LSE_signal_extract_ack(ostatus));
    else
      return (LSE_signal_extract_enable(istatus) |
	      LSE_signal_extract_ack(ostatus));
    >>>;

  detectMispred.reduce_calculates_enable = FALSE;
  detectMispred.wait_for_data = TRUE;
  /* important since we can create output due to external interrupts
   * even when there is no internal thing to resolve
   */
  detectMispred.propagate_nothing = FALSE;

  /* 
   * execute branches; look for the first mispredicted branch and the first
   * taken branch.  Must also deal with intra-group bypassing of predicates,
   * pfs, and branch registers.  Note that we need these to go in program
   * order; the detectSort forces that to occur...
   *
   * Multi-way branches are kind of fun.  The removal of "extra" instructions
   * after the multi-way branch is taken care of in WB where instructions
   * after a taken branch are dropped (after updating the scoreboard).  The
   * assumption is that branches from the middle (except for checks, which
   * are more like exceptions) can only be followed by more branches or
   * at least by things which actually hit WB at the same time as branches.
   *
   * But when we want to use extra instructions in a bundle after a branch 
   * that we incorrectly predicted taken, we need to make certain 
   * that we move the redirection message on to the last useful instruction
   * and set the target address to the instruction following the last
   * useful one (which may be the useful one's target...)
   *
   * And even worse: for now, we have a "dumbBBB" parameter to turn off
   * this beautiful behavior, because we don't bypass properly when we
   * have a BBB....
   *
   * A useful invariant: a non-null redirectID always points to the earliest
   * branch mispredicted to be taken, until we are setting up to actually
   * report the misprediction.  In other words, redirectID is a "redirect
   * to inline" instruction.
   *
   * TODO: proper interrupt/exception prioritization
   *
   */

  // ugh... This bit of state tracks the last front-end IDno so that we
  // can insert interrupt redirects with appropriate idnos....
  var lastnum = new runtime_var("lastnum",LSE_dynid_num_t) : runtime_var ref;

  detectMispred.init = <<<
    ${lastnum} = 0;
  >>>;

  detectMispred.reduce = <<<
#define src_qp LSE_emu_operand_name_src_qp
#define src_psr LSE_emu_operand_name_src_psr
#define src_cfm LSE_emu_operand_name_src_cfm
#define dest_cfm LSE_emu_operand_name_dest_cfm
#define dest_rse LSE_emu_operand_name_dest_rse
#define dest_rsemem LSE_emu_operand_name_dest_rsemem
#define dest_psr LSE_emu_operand_name_dest_psr
  int i,j;
  LSE_dynid_t id;
  boolean modsCFM, modsRSE;
  LSE_dynid_t redirectID = NULL, microID=NULL;
  LSE_emu_addr_t redirectAddr, interruptAddr;
  boolean had_interrupt = FALSE;
  int reason = 0;
  int formatno;

  /* this perhaps would look better structurally, but in fact, it's far
   * easier to deal with here...  Perhaps if we were to use a combiner,
   * but we need the "reducing" behavior...
   */

  for (i=0;i<LSE_port_width(${this}:stallDET);i++) {
    LSE_signal_t sig;
    sig = LSE_port_query(${this}:stallDET[i].data,NULL,NULL);
    if (!LSE_signal_data_known(sig)) return;
    if (LSE_signal_data_present(sig)) { /* don't do anything! */
      *out_statusp = LSE_signal_nothing;
      return;
    }
  }

  for (i=0;i<LSE_port_width(${this}:interrupt);i++) {
    LSE_signal_t sig;
    LSE_emu_addr_t *naddr;
    sig = LSE_port_query(${this}:interrupt[i].data,NULL,&naddr);
    if (!LSE_signal_data_known(sig)) return;
    if (LSE_signal_data_present(sig)) {
      interruptAddr = *naddr;
      had_interrupt = TRUE;
      break;
    }
  }

  for (i=0;i<LSE_port_width(${this}:microtrap);i++) {
    LSE_signal_t sig;
    LSE_emu_addr_t *naddr;
    sig = LSE_port_query(${this}:microtrap[i].data,&id,NULL);
    if (!LSE_signal_data_known(sig)) return;
    if (LSE_signal_data_present(sig)) {
      if (microID == NULL || 
	  LSE_dynid_get(microID,${beIDno})> LSE_dynid_get(id,${beIDno})) { 
	/* oldest */
	microID = id;
      }
    }
  }

  /* Now look for the instructions */

  for (i=0;i<LSE_port_width(in);i++) {
    if (LSE_signal_data_present(in_statusp[i])) {
      id = in_idp[i];
      ${lastnum} = LSE_dynid_get(id,${feIDno}); /* we're in-order, so OK */
      if (!in_datap[i].valid) continue;

	/**************** check for bad address **********************/
      if (((LSE_emu_dynid_get(id,addr)>>60)&0xf) != 4) {
	  LSE_report_err("Executing in never-never land %llx(%lld)",
			 LSE_emu_dynid_get(id,addr),
			 LSE_dynid_get(id,idno));
	}

      /******************* check for side effecting *****************/

      if (LSE_emu_dynid_is(id,sideeffect)) {

	/* we can execute side-effecting instructions here because we know
	 * that this is the last one that will be executed, it needed
	 * to have executed, and everything before it has written
	 * back (ensured in scoreboard).  We need to do them here so
	 * that we calculate the proper next_pc.
	 */
	LSE_emu_do_instrstep(id,LSE_emu_instrstep_name_evaluate);

	/* worry about remapping; if it happens, just direct us
	 * to the first address of the new context....
	 */
	if (LSE_emu_get_context_mapping(${hwContextNum}) !=
	    LSE_emu_dynid_get(id,swcontexttok)) {
	  if (LSE_emu_get_context_mapping(${hwContextNum})) {
	    LSE_emu_dynid_set(id,next_pc,
			      LSE_emu_get_start_addr(${hwContextNum}));
	  }
	}

	LSE_dynid_set(id,field:mispred,0x8);
	reason = 1;
	/* if we did a break instruction that we don't know about, 
	   then simulation has gone wild, and it's a good idea
	   to stop.
	*/
	if (LSE_emu_dynid_get(id,privatef.actual_intr)) {
	  LSE_report_err("Executed break instruction %llx(%lld) that "
			 "wasn't an OS call\n", LSE_emu_dynid_get(id,addr),
			 LSE_dynid_get(id,idno));
 	}
	goto has_redirect;
      } /* if sideeffect */

      /******************* do microtrap check ************************/

      if (id == microID) {
	LSE_dynid_set(id,field:mispred,0x800);
	reason = 2;
	goto has_redirect;
      }

      /******************* do interruption check *********************/

      /******************* do speculation resolution *****************/

      if (LSE_emu_dynid_is(id,cti) && 
	  (LSE_emu_dynid_get(id,branch_dir) != 
	   LSE_dynid_get(id,field:${branchSinkName}:bpred.dir)

	   || (LSE_dynid_get(id,field:${branchSinkName}:bpred.dir) &&
		     LSE_emu_dynid_get(id,next_pc) != 
		     LSE_dynid_get(id,field:${branchSinkName}:bpred.target))
		 )) {

	LSE_dynid_set(id,field:mispred,0x8);
	reason = 3;
	/* we cannot attempt to complete the bundle if we're supposed to
	 * take it..
	 *
	 * We also cannot attempt to complete if dumbBBB is on.
	 */
	if (${dumbBBB} || LSE_emu_dynid_get(id,branch_dir)) goto has_redirect;
	redirectID = id;
      }

      /* modifies the CFM if it has the CFM as a destination and the 
       * instruction is predicated on, or it's a taken branch, 
       * or it's a branch type that always modifies rrb
       */
      modsCFM = 
	( (LSE_emu_dynid_get(id,operand_dest[dest_cfm].spaceid)
	   == LSE_emu_spaceid_SR)  &&
	  LSE_emu_dynid_get(id,operand_dest[dest_cfm].spaceaddr.SR) == 0 &&
	  
	  (LSE_emu_dynid_get(id,operand_val_src[src_qp].data.PR) ||
	   (LSE_emu_dynid_is(id,cti) &&
	    (LSE_emu_dynid_get(id,branch_dir) ||
	     LSE_emu_dynid_get(id,extra.decode.cti.btype)== 2 || 
	     LSE_emu_dynid_get(id,extra.decode.cti.btype)== 3 || 
	     LSE_emu_dynid_get(id,extra.decode.cti.btype)== 5 || 
	     LSE_emu_dynid_get(id,extra.decode.cti.btype)== 6 || 
	     LSE_emu_dynid_get(id,extra.decode.cti.btype)== 7))));

      /* check for bad CFM predictions... we need to know whether we
       * actually want to update it ourselves!
       */
      
      if (modsCFM) {
	LSE_emu_operand_val_t newCFM,oldCFM;

	oldCFM = in_datap[i].predNewCFM;
	newCFM = LSE_emu_dynid_get(id,operand_val_dest[dest_cfm]);

	if (oldCFM.data.SR.CFM.val != newCFM.data.SR.CFM.val ||
	    oldCFM.data.SR.CFM.BOF != newCFM.data.SR.CFM.BOF) {
	  
	  formatno = LSE_emu_dynid_get(id,extra.formatno);
	  
	  if (formatno == 304) { /* return */
	    reason = 4;
	    if (${reportBypass}) {
	      fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
	      fprintf(LSE_stderr,
		      ": PFS misprediction for %llx(%lld); "
		      "pred %llx != real %llx\n",
		      LSE_emu_dynid_get(id,addr),LSE_dynid_get(id,idno),
		      oldCFM.data.SR.CFM.val, newCFM.data.SR.CFM.val);
	    }
	  } else {
	    reason = 5;
	    if (${reportBypass}) {
	      fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
	      fprintf(LSE_stderr,
		      ": rotation misprediction for %llx(%lld); "
		      "pred %llx != real %llx\n",
		      LSE_emu_dynid_get(id,addr),LSE_dynid_get(id,idno),
		      oldCFM.data.SR.CFM.val, newCFM.data.SR.CFM.val);
	    }
	  }
	  LSE_dynid_set(id,field:mispred,
			LSE_dynid_get(id,field:mispred)|0x40);
	  /* everything after this is bogus, so we can't wait to redirect */
	  goto has_redirect;
	}
      } /* modifies CFM */

      /* check whether branch returns are changing PSR.cpl; if so, treat
       * as a mis-prediction (since we predicted that they wouldn't change
       * earlier on)
       */
      if (LSE_emu_dynid_get(id,extra.formatno)==304 &&
	  LSE_emu_dynid_get(id,branch_dir) &&
	  LSE_emu_dynid_get(id,extra.decode.cti.btype)== 4 &&
	  (LSE_emu_dynid_get(id,operand_val_src[src_psr].data.SR.PSR) !=
	   LSE_emu_dynid_get(id,operand_val_dest[dest_psr].data.SR.PSR))
	  ) {
	
	reason = 6;
	  LSE_dynid_set(id,field:mispred,
			LSE_dynid_get(id,field:mispred)|0x200);
	  /* everything after this is bogus, so we can't wait to redirect */
	  goto has_redirect;
      }

      if (LSE_emu_dynid_is(id,cti) && LSE_emu_dynid_get(id,branch_dir)) {
	/* nothing wrong here, but don't want to check any more.  We need
	 * to exit out so that any left-over redirection can occur on us..
	 */
	break;
      }

    } /* valid data */
  } /* ports */

  if (redirectID) goto has_redirect;

  if (had_interrupt) {
    id = LSE_dynid_create();
	/* so we'll know it */
    LSE_emu_dynid_set(id,addr,UINT64_C(0xfeedfacedeadbeef)); 
    LSE_dynid_cancel(id);
    LSE_dynid_set(id,${feIDno},${lastnum});
    /* clear out the instruction... since new dynids only clear fields */
    LSE_emu_init_instr(id,${hwContextNum},UINT64_C(0xfeedfacedeadbeef));
    *out_idp = id;
    *out_datap = interruptAddr;
    *out_statusp = LSE_signal_something;
    LSE_event_record(${this}:REDIRECTED,id,*out_datap,7);
  } else *out_statusp = LSE_signal_nothing;
  return;
 has_redirect:
  /* mark that we saw it... */
  LSE_dynid_set(id,field:mispred,LSE_dynid_get(id,field:mispred)|0x4);

  if (had_interrupt) {
    *out_datap = interruptAddr;
  } else {
    /* true follower of last good instruction */
    *out_datap = LSE_emu_dynid_get(id,next_pc); 
  }
  *out_idp = id;
  *out_statusp = LSE_signal_something;
  LSE_event_record(${this}:REDIRECTED,id,*out_datap,reason);
  return;
#undef src_qp
#undef src_psr
#undef src_cfm
#undef dest_cfm
#undef dest_rse
#undef dest_rsemem
#undef dest_psr
  >>>;

  detectMispred.out ->[LSE_emu::LSE_emu_addr_t] redirect_fetch;

  if (interrupt.width > 0) { /* dummy so LSS doesn't complain */
    instance interruptSink : corelib::sink;
    LSS_connect_bus(interrupt,interruptSink.in,interrupt.width);
  }

  if (microtrap.width > 0) { /* dummy so LSS doesn't complain */
    instance microtrapSink : corelib::sink;
    LSS_connect_bus(microtrap,microtrapSink.in,microtrap.width);
  }

  if (stallDET.width > 0) { /* dummy so LSS doesn't complain */
    instance stallDETSink : corelib::sink;
    LSS_connect_bus(stallDET,stallDETSink.in,stallDET.width);
  }

};
