/* 
 * Copyright (c) 2000-2004 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Itanium 2 front-end model, IPG stage
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This module is the control and main logic of the IPG stage of the
 * Itanium 2.  It does not include the cache, branch predictors, or
 * prefetch engines; these are supplied externally to allow us to 
 * be more modular.  It also does *not* include a stage flop between
 * IPG and ROT.
 *
 * The pipeline starts at the IPG stage.  This stage contains 
 * IP (instruction pointer) selection logic to determine the
 * first instruction to fetch in this cycle.  That address is sent
 * to the first-level I-cache.  The cache looks up the instruction
 * and branch information, which includes prediction information.
 * As they come out of the cache, instructions are registered in
 * the IPG->ROT stage flop.
 *
 * What can be parameterized:
 *
 *  - cache access size in bundles
 *  - bundle size in bytes
 *  - hardware context number
 *  - whether to register redirect signals
 *
 * Support for fast-forward/sampling should be handled externally to
 * this module; simply stall the module during cycles when new 
 * instructions should not be begun and redirect it to begin again.
 *
 * For MP support, if the module is to be quiescent as simulation
 * begins, either do not map its initial context or stall it.
 *
 */

module I2_IPG
{
  import corelib;
  import LSE_emu;
  add_to_domain_searchpath(LSE_domain.LSE_emu);

  /***************************************************************/
  /************************* Ports *******************************/
  /***************************************************************/

  /* address to fetch
   *  enable = YES
   */
  outport fetch_addr : LSE_emu::LSE_emu_addr_t;

  /* redirect the fetch behavior... 
   *   if this is multi-ported, port instances attach in decreasing
   *   order of priority
   *  enable ignored / ack = YES
   */
  inport redirect_fetch : LSE_emu::LSE_emu_addr_t;

  /* stall the fetch; no icache access
   *  enable ignored / ack = YES 
   */
  inport stall : 'stall_datatype;

  /***************************************************************/
  /************************* Parameters ***************************/
  /***************************************************************/

  /* cache fetch size in bundles */
  parameter fetchBundles = 2 : int;

  /* size of a bundle in bytes */
  parameter bundleByteSize = 16 : int;

  /* hardware context number for emulation */
  parameter hwContextNum = 1 : int;

  /* register redirections */
  parameter registerRedirect = TRUE : boolean;

  var fetchSize = fetchBundles * bundleByteSize : int;

  /***************************************************************/
  /*********************** Sanity checks *************************/
  /***************************************************************/

  if (fetch_addr.width != 1) {
     punt("IPG generates one and only one address per cycle," +
	  " so fetch_addr must have width 1");
  }

  /***************************************************************/
  /************************* IPG STAGE ***************************/
  /***************************************************************/

  /*************** Instruction Pointer logic *********************
   *
   * What it is:
   *
   *   The IP is selected from among the last IP + fetchSize, the 
   *   last IP, and redirections from either branch prediction or
   *   the backend of the pipe.
   *
   *   Redirection inputs may be registered; this is controlled by
   *   the registerRedirect parameter.   The registering of the 
   *   redirect is essentially a pipe-stage register for whatever stage 
   *   might be redirecting.  
   *
   *   Stalling occurs through both back-pressure (nacks) on the
   *   fetch_addr port and through explicit stall signals; a "something"
   *   on any stall signal yields a stall.  Stalls cause the IP to
   *   update to (this IP) instead of (this IP + fetchSize).  
   *
   *   The address update is not purely IP + fetchSize; the new address
   *   is naturally aligned to fetchSize.  This allows fetches to begin
   *   with  a partial access but then become full accesses later.
   *   This does imply that the cache needs to be smart about unaligned
   *   addresses!
   *
   * Actual implementation:
   *
   *   - delays for each redirect (optional)
   *   - aligner for the mux + 
   *   - converter for adder/mux combination (using an input control 
   *     function to ensure that all stalls are resolved before 
   *     conversion)
   *   - gate to filter out cache accesses
   *
   *   The IP mux is done using an aligner as there is definitely a 
   *   fixed priority to these things:
   *      - highest - redirection
   *      - lowest  - IP + fetchSize / 0        (from last fetch)
   *
   ******************************************************************/

  instance IPflop:        corelib::delay;
  instance IPmux:         corelib::aligner;
  instance IPadder:       corelib::converter;
  instance IPtee:         corelib::tee;

  instance stallSink:     corelib::sink;

  /* feed into IP mux */

  LSS_connect_bus(redirect_fetch, IPmux.in, redirect_fetch.width);
  IPadder.out -> IPmux.in[redirect_fetch.width];

  IPmux.in.control = <<< return LSE_signal_all_yes; >>>;
  IPmux.out.control = <<< return LSE_signal_all_yes; >>>;

  /* IP flop */

  IPmux.out -> IPflop.in;
  IPflop.out -> IPtee.in;
  IPflop.pass_acks_when_full = FALSE;
  IPflop.pass_acks_to_enable = FALSE;

  IPflop.initial_state = <<<
    if (LSE_emu_get_context_mapping(${hwContextNum})) {
      *init_id = LSE_dynid_create();
      *init_value = LSE_emu_get_start_addr(${hwContextNum});
      return TRUE;
    }
    else return FALSE;
  >>>;

  IPflop.out.control = <<<
    LSE_signal_t stallsig = LSE_signal_something, newack, redirected,sig;
    LSE_port_type(out) *newval, *addrincr;
    int i;

    for (i=0;i<${stall.width};i++) {
      sig = LSE_port_query(${this}:stall[i].data,NULL,NULL);
      stallsig &= LSE_signal_not(sig);
    }

    /* determine redirection by noting whether the IPmux nacked the port
     * query...
     */
    redirected = LSE_port_query(${IPmux}:in[${redirect_fetch.width}].localack);
    redirected = LSE_signal_not(redirected);

    newack = LSE_signal_data2ack(stallsig) & ostatus;
    newack = LSE_signal_or(newack, redirected);

    return (LSE_signal_something | LSE_signal_enabled |
	    LSE_signal_extract_ack(newack));
  >>>;

  /* deal with the context ending on us; if it does, we want to lose
   * our recirculating instruction.  If that happens, we can only get
   * restarted by someone injecting a new address in through the
   * redirect_fetch port.
   */
  IPflop.drop_func = <<<
    return (!LSE_emu_get_context_mapping(${hwContextNum}));
  >>>;

  /* Output tee stuff... */

  IPtee.out -> fetch_addr;
  IPtee.out -> IPadder.in;

  /* Adder logic */

  IPadder.convert_func = <<<
    return ((data + ${fetchSize}) / ${fetchSize}) * ${fetchSize}; 
  >>>;

  LSS_connect_bus(stall,stallSink.in,stall.width);

};
