#!/usr/bin/perl

#
# pipeview - pipeline trace pretty printer
#
# This file is a part of the SimpleScalar tool suite written by
# Todd M. Austin as a part of the Multiscalar Research Project.
#  
# The tool suite is currently maintained by Doug Burger and Todd M. Austin.
# 
# Copyright (C) 1994, 1995, 1996, 1997 by Todd M. Austin
#
# This source file is distributed "as is" in the hope that it will be
# useful.  It is distributed with no warranty, and no author or
# distributor accepts any responsibility for the consequences of its
# use. 
#
# Everyone is granted permission to copy, modify and redistribute
# this source file under the following conditions:
#
#    This tool set is distributed for non-commercial use only. 
#    Please contact the maintainer for restrictions applying to 
#    commercial use of these tools.
#
#    Permission is granted to anyone to make or distribute copies
#    of this source code, either as received or modified, in any
#    medium, provided that all copyright notices, permission and
#    nonwarranty notices are preserved, and that the distributor
#    grants the recipient permission for further redistribution as
#    permitted by this document.
#
#    Permission is granted to distribute this file in compiled
#    or executable form under the same conditions that apply for
#    source code, provided that either:
#
#    A. it is accompanied by the corresponding machine-readable
#       source code,
#    B. it is accompanied by a written offer, with no time limit,
#       to give anyone a machine-readable copy of the corresponding
#       source code in return for reimbursement of the cost of
#       distribution.  This written offer must permit verbatim
#       duplication by anyone, or
#    C. it is distributed by someone who received only the
#       executable form, and is accompanied by a copy of the
#       written offer of source code that they received concurrently.
#
# In other words, you are welcome to use, share and improve this
# source file.  You are forbidden to forbid anyone else to use, share
# and improve what you give them.
#
# INTERNET: dburger@cs.wisc.edu
# US Mail:  1210 W. Dayton Street, Madison, WI 53706
#
# $Id: ia64-pipeview.pl 14783 2004-08-13 15:43:23Z dpenry $
#
# $Log$
# Revision 1.1  2004/08/13 15:43:22  dpenry
# Put the Itanium model into src/configurations
#
# Revision 1.1  2004/06/08 18:24:58  dpenry
# Make a copy of the base I2 which we're using for experiments so that the
# I2 model can be improved independently
#
# Revision 1.13  2004/02/04 18:19:37  dpenry
# - Data TLBs
# - control speculation support
# - data speculation support (using emulator's "perfect" ALAT)
#
# Revision 1.12  2004/01/30 19:09:26  dpenry
# - Lots of DCU structuring
# - Proper location for using statements
# - Set up default domain search path for hierarchical modules properly
# - L1D working
#
# Revision 1.11  2004/01/19 22:44:30  dpenry
# - Fix up PSR scoreboarding
# - Release WAW stall during cycle when final bypass occurs
# - Fix up FPSR scoreboarding
#
# Revision 1.10  2004/01/09 02:17:13  dpenry
# - If you speculate CFM, you also need to speculate internal RSE
#   state, or there's no point in the speculation (you would end up stalling
#   in common code situations)
# - Handle multi-way branch bundles with a single resolution per bundle
#   if an early branch was mispredicted taken
#
# Revision 1.9  2004/01/08 22:32:10  dpenry
# Fix CFM speculation
#
# Revision 1.8  2004/01/08 17:56:22  dpenry
# - Stall RSE when it needs to spill -- a temporary hack until proper
#   RSE memory ops are inserted into the pipe.  fill is not taken care of,
#   but is not likely to be an issue, unless a full frame was spilled,
#   and loadrs isn't even implemented in the emulator yet.
# - Compute "busy" signal/s for pipe and use to stall side-effecting
#   instructions
#
# Revision 1.7  2004/01/07 00:44:33  dpenry
# Checkpoint ... not working at the present moment because the out-of-order
# writeback (which is really there in I2) is messing up the break
# instructions.
#
# Revision 1.6  2004/01/03 23:50:14  dpenry
# - Prevent multi-branch bundles from redirecting fetch more than once
#   in the front end
# - Avoid control idioms which lead to dynamic sections
#
# Revision 1.5  2004/01/02 05:24:49  dpenry
# - Start working on the scoreboard
# - Fix use of uninitialized variable ... very, very subtle to find...
#
# Revision 1.4  2003/12/31 21:03:08  dpenry
# Work on rename stage; this stage is more interesting that it would seem
# (and justifies being a whole stage) because a form of data speculation
# is used for CFM values (thus yielding the "rotation mispredict" flush
# in the I2 performance counters).  Our speculation isn't quite as good
# as I2's yet, but it's there...
#
# Also still needs to insert mandatory RSE actions; for now, generates
# an "RSE mispredict" flush when a mandatory fill should have occurred.
#
# Revision 1.3  2003/12/31 01:58:00  dpenry
# Implement port restrictions on dispersal.  Not as hard as I'd feared...
# (of course, I'm just guessing at some of the rules, since for some of
#  them Intel just says "they're hard")
#
# Revision 1.2  2003/12/30 00:23:17  dpenry
# - Start structuring backend
# - IBuf works on bundles, not instructions
# - Split issue after stop bits
#
# Revision 1.1  2003/12/29 19:20:37  dpenry
# - Pipeline tracing for IA64
#
# Revision 1.1.1.1  2000/11/30 18:34:56  rjoseph
# Re-import of simplesim
#
# Revision 1.1.1.1  2000/05/26 15:18:57  taustin
# SimpleScalar Tool Set
#
#
# Revision 1.1  1998/08/27 15:51:28  taustin
# Initial revision
#
# 

#
# TODO: check live lengths...
#

if (@ARGV != 1)
  {
    print STDERR "Usage: pview <pipe_trace>\n";
    exit -1;
  }

open(TRC_FILE, $ARGV[0])
	|| die "Cannot open pipeline trace file `$ARGV[0]'";

$cycle = 0;

print "Note: this shows when instructions *complete* stages\n";
print "\n";
print "Instruction event legend:\n";
print "\n";
print "    * - cache miss\n";
print "    ! - TLB miss\n";
print "    / - DET: branch misprediction used\n";
print "    \\ - DET: branch misprediction detected\n";
print "    : - DET: microtrap (e.g. data speculation failure)\n"; 
print "    @ - DET: CFM misprediction (PFS or rotation mispred)\n";
print "    ^ - DET: RSE internal state misprediction\n";
print "    % - DET: Privilege level misprediction (kind of a PFS mispred)\n";
print "    + - DET: branch taken\n";
print "    ^ - REG: stall in EXE due to st/ld conflict\n";
print "    ; - EXP: split issue due to stop bit\n";
print "    . - ROT: split issue at EXE due to port conflict\n";
print "\n";

while (<TRC_FILE>)
  {
    # remove carriage return
    chop;

    # new instruction
    if (/^\+\s+(\d+)\s+(\d+)\s+([0-9a-fA-F]+.[0-9]+).\s+(.*)\n?$/)
      {
	# register this instruction in the live instruction hash table
	$insts{$1} = 1;
	$insts_pc{$1} = $3;
	$insts_asm{$1} = $4;
	$insts_iid{$1} = 
	    chr ((ord("a") + (($1 / 26) % 26))) .
	    chr (ord("a") + ($1 % 26));

	# print instruction id and asm info
	print "$insts_iid{$1} = `$3($2): $4'\n";
      }
    elsif (/^\s+[0-9a-fA-F]+/) {  
	# simply a continuation of assembly
    }
    # deleted instruction
    elsif (/^\-\s+(\d+)\n?$/)
      {
	# record deletion, these are processed after pipe info is printed
	@dead_pseqs = ($1, @dead_pseqs);
      }
    # stalls
    elsif (/^\$\s+(\w+)\s+(.*)\n?$/) {
	$stalls{$1} = $2;
    }
    # new cycle
    elsif (/^@\s+(\d+)\n?$/)
      {
	# clear all instruction stage lists
	@ipg_iids = ();
	@rot_iids = ();
	@exp_iids = ();
	@ren_iids = ();
	@reg_iids = ();
	@exe_iids = ();
	@det_iids = ();
	@dcu_iids = ();
	@fp_iids = ();
	@wrb_iids = ();

	# print last cycle pipeline state
	foreach $pseq (keys %insts)
	  {
	    # partition by instruction stage
	    $stage = $insts_stage{$pseq};

	    # decode the instruction events
	    $events = hex($insts_events{$pseq});
	    $evstr = "";
	    if ($events & 0x00000001)		# cache miss
	      {
		$evstr = $evstr . "*";
	      }
	    if (($events & 0x00000002) != 0)	# TLB miss
	      {
		$evstr = $evstr . "!";
	      }
	    if ($events & 0x00000004)		# mis-predict used
	      {
		$evstr = $evstr . "/";
	      }
	    if ($events & 0x00000008)		# mis-predict detected
	      {
		$evstr = $evstr . "\\";
	      }
	    if ($events & 0x00000010)		# split due to stop bit
	      {
		$evstr = $evstr . ";";
	      }
	    if ($events & 0x00000020)		# split due to port conflict
	      {
		$evstr = $evstr . ".";
	      }
	    if ($events & 0x00000040)		# CFM mis-predict detected
	      {
		$evstr = $evstr . "\@";
	      }
	    if ($events & 0x00000080)		# RSE stall/mis-predict
	      {
		$evstr = $evstr . "^";
	      }
	    if ($events & 0x00000100)		# Branch taken
	      {
		$evstr = $evstr . "+";
	      }
	    if ($events & 0x00000200)		# privilege level mis-predict
	      {
		$evstr = $evstr . "%";
	      }
	    if ($events & 0x00000400)		# st/ld conflict
	      {
		$evstr = $evstr . "^";
	      }
	    if ($events & 0x00000800)		# microtrap
	      {
		$evstr = $evstr . ":";
	      }
	    if ($events & 0x00800000)		# stage counter
	      {
		  $nstr = sprintf("%d",(($events >> 16) & 127));
		  $evstr = $evstr . $nstr;
	      }

	    if ($stage eq "IPG")
	      {
		@ipg_iids = (@ipg_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "ROT")
	      {
		@rot_iids = (@rot_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "EXP")
	      {
		@exp_iids = (@exp_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "REN")
	      {
		@ren_iids = (@ren_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "REG")
	      {
		@reg_iids = (@reg_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "EXE")
	      {
		@exe_iids = (@exe_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "DET")
	      {
		@det_iids = (@det_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "WRB")
	      {
		@wrb_iids = (@wrb_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "DCU")
	      {
		@dcu_iids = (@dcu_iids, $insts_iid{$pseq} . $evstr);
	      }
	    elsif ($stage eq "FP")
	      {
		@fp_iids = (@fp_iids, $insts_iid{$pseq} . $evstr);
	      }
	    else
	      {
		print "warning: unknown stage\n";
	      }
	  }


#	print "** ipg_iids: ";
#	foreach $iid (@ipg_iids)
#	  {
#	    print "$iid ";
#         }
#	print "\n";

	# sort stage lists
	@ipg_iids = sort @ipg_iids;
	@rot_iids = sort @rot_iids;
	@exp_iids = sort @exp_iids;
	@ren_iids = sort @ren_iids;
	@reg_iids = sort @reg_iids;
	@exe_iids = sort @exe_iids;
	@det_iids = sort @det_iids;
	@dcu_iids = sort @dcu_iids;
	@fp_iids = sort @fp_iids;
	@wrb_iids = sort @wrb_iids;

	# print pipeline header
	if ((@ipg_iids + @rot_iids + @exp_iids + @ren_iids + @reg_iids +
	     @exe_iids + @det_iids + @dcu_iids + @wrb_iids + @fp_iids) > 0)
	  {
	    print "\n";
	    print " [IPG]    ", " [ROT]    ", " [EXP]    ", " [REN]    ", 
		  " [REG]    ", " [EXE]    ", " [DET]    ", " [WRB]    ",
	          " (DCU)    ", " (FP)     ","\n";
	  }
	# print stalls
	if (%stalls) {
	    if ($stalls{"IPG"}) {
		printf " %-8s ", $stalls{"IPG"};
	    }
	    else { printf "          "; }

	    if ($stalls{"ROT"}) {
		printf " %-8s ", $stalls{"ROT"};
	    }
	    else { printf "          "; }

	    if ($stalls{"EXP"}) {
		printf " %-8s ", $stalls{"EXP"};
	    }
	    else { printf "          "; }

	    if ($stalls{"REN"}) {
		printf " %-8s ", $stalls{"REN"};
	    }
	    else { printf "          "; }

	    if ($stalls{"REG"}) {
		printf " %-8s ", $stalls{"REG"};
	    }
	    else { printf "          "; }

	    if ($stalls{"EXE"}) {
		printf " %-8s ", $stalls{"EXE"};
	    }
	    else { printf "          "; }

	    if ($stalls{"DET"}) {
		printf " %-8s ", $stalls{"DET"};
	    }
	    else { printf "          "; }

	    if ($stalls{"WRB"}) {
		printf " %-8s ", $stalls{"WRB"};
	    }
	    else { printf "          "; }

	    if ($stalls{"DCU"}) {
		printf " %-8s ", $stalls{"DCU"};
	    }
	    else { printf "          "; }

	    if ($stalls{"FP"}) {
		printf " %-8s ", $stalls{"FP"};
	    }
	    else { printf "          "; }

            print "\n";
	}

	# print stage data
	while ((@ipg_iids + @rot_iids + @exp_iids + @ren_iids + @reg_iids +
		@exe_iids + @det_iids + @wrb_iids + @dcu_iids + @fp_iids) > 0)
	  {
	    if (@ipg_iids > 0)
	      {
		($iid, @ipg_iids) = @ipg_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@rot_iids > 0)
	      {
		($iid, @rot_iids) = @rot_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@exp_iids > 0)
	      {
		($iid, @exp_iids) = @exp_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@ren_iids > 0)
	      {
		($iid, @ren_iids) = @ren_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@reg_iids > 0)
	      {
		($iid, @reg_iids) = @reg_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@exe_iids > 0)
	      {
		($iid, @exe_iids) = @exe_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@det_iids > 0)
	      {
		($iid, @det_iids) = @det_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@wrb_iids > 0)
	      {
		($iid, @wrb_iids) = @wrb_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@dcu_iids > 0)
	      {
		($iid, @dcu_iids) = @dcu_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    if (@fp_iids > 0)
	      {
		($iid, @fp_iids) = @fp_iids;
		printf "  %-6s  ", $iid;
	      }
	    else
	      {
		print "          ";
	      }

	    print "\n";
	  }
	print "\n";

	# delete instruction that finished in this cycle
	foreach $pseq (@dead_pseqs)
	  {
	    delete $insts{$pseq};
	    delete $insts_pc{$pseq};
	    delete $insts_asm{$pseq};
	    delete $insts_iid{$pseq};
	    delete $insts_stage{$pseq};
	    delete $insts_events{$pseq};
	  }
	@dead_pseqs = ();
	%stalls = {};

        # go on to the next cycle
	$cycle = $1;
	print "@ $cycle\n";
      }
    # instruction transition to a new stage
    elsif (/^\*\s+(\d+)\s+(\w+)\s+(0x[0-9a-fA-F]+)\n?$/)
      {
	if ($insts{$1})
	  {
	    # this instruction is live...
	    $insts_stage{$1} = $2;
	    $insts_events{$1} = $3;
	  }
	else
	  {
	    # this instruction is dead...
	    # print STDERR "inst `$1' is dead...\n";
	  }
      }
    else
      {
	print "warning: could not parse line: `$_'\n";
      }
  }

close(TRC_FILE);

