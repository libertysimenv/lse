/* -*-c-*- 
 * Copyright (c) 2000-2004 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Pipe tracing support for Itanium 2
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 */

import corelib;

subpackage pipetrace {

  fun add_pipetracing(pipetrace: parameter ref, /* do I pipetrace? */
		      CPUname : literal, /* instance name of the CPU */
		      TFilename : literal /* trace filename */
	) => void {

  /**************************************************************************/
  /*************************** Pipetrace support ****************************/
  /**************************************************************************/

    var backEnd = "backEnd" : literal;
    var frontEnd = "frontEnd" : literal;
    var DCU = "DCU" : literal;

    if (CPUname != "") {
      backEnd = CPUname + "." + backEnd;
      frontEnd = CPUname + "." + frontEnd;
      DCU = CPUname + "." + DCU;
    }

    /* a dummy instance */
    instance traceCollector : corelib::sink;

    /* variables and fields */

    var trace_file = new runtime_var("trace_file",external("FILE *")) 
      : runtime_var ref;
    var trace_seq = new runtime_var("trace_seq",external("unsigned int")) 
      : runtime_var ref;
    
    structadd(traceCollector, <<<LSE_dynid_t>>>,<<<unsigned int>>>,
	      <<<ptrace_seq>>>);
    structadd(traceCollector, <<<LSE_dynid_t>>>,<<<boolean>>>,
	      <<<valid>>>);

    /* setup, finish, and per-time-step stuff... */
    traceCollector.init = <<<
    if (${pipetrace}) {
      ${trace_file} = fopen("${TFilename}","w");
      ${trace_seq} = 0;
    }
    >>>;

    traceCollector.start_of_timestep = <<<
    if (${pipetrace}) {
      fprintf(${trace_file},"@ %lld\n",LSE_time_get_cycle(LSE_time_now));
    }
    >>>;

    traceCollector.finish = <<<
    if (${pipetrace}) { 
      fprintf(${trace_file},"@ %lld\n",LSE_time_get_cycle(LSE_time_now));
      fclose(${trace_file});
    }
    >>>;

    /* killed instructions */

    collector INSTRUCTION_CANCELLED on <<<${backEnd}>>> {
      record = <<<
	if (${pipetrace} && LSE_dynid_get(id,field:${traceCollector}:valid)) {
	  fprintf(${trace_file},"- %d\n",
	          LSE_dynid_get(id,field:${traceCollector}:ptrace_seq));
	}
      >>>;
    };

    collector INSTRUCTION_CANCELLED on <<<${frontEnd}>>> {
      record = <<<
	if (${pipetrace} && LSE_dynid_get(id,field:${traceCollector}:valid)) {
	  fprintf(${trace_file},"- %d\n",
		  LSE_dynid_get(id,field:${traceCollector}:ptrace_seq));
	}	
      >>>;
    };

    collector INSTRUCTION_FINISHED on <<<${backEnd}>>> {
      record = <<<{
	if (${pipetrace} && LSE_dynid_get(id,field:${traceCollector}:valid)) {
	  fprintf(${trace_file}, "* %d WRB 0x%08x\n", 
	          LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),0);
	  fprintf(${trace_file}, "- %d\n",
	          LSE_dynid_get(id,field:${traceCollector}:ptrace_seq));
	}
      }>>>;
    };

    /* moving through stages */
    
    collector FINISHED_STAGE on <<<${frontEnd}>>> {
      record = <<<
	if (${pipetrace} && valid) {
	  int extra = 0;
	  const char *stage;
	  LSE_dynid_t id2;
	  LSE_signal_t sig;
	  
	  switch (stageNum) {
	  case 0: /* IPG */
	    stage = "IPG";
	    if (LSE_emu_dynid_get(id,extra.formatno)) {
	      LSE_dynid_set(id,field:${traceCollector}:valid,TRUE);
	      LSE_dynid_set(id,field:${traceCollector}:ptrace_seq,
	                    ${trace_seq});
	      fprintf(${trace_file}, "+ %d %lld ",
	      LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),
	      LSE_dynid_get(id,idno));
	      ${trace_seq}++;
	      LSE_emu_disassemble(id,${trace_file});
	    }
	    break;
	  case 1: /* ROT */
	    stage = "ROT";
	    sig = LSE_port_query(${frontEnd}.BRpipe:cycle1_redirect[0],&id2,NULL);
	    if (LSE_signal_data_present(sig) && LSE_dynid_eq(id,id2))
	      extra = 8;
	    break;
	  default:
	    stage = "unknown";
	    break;
	  } /* switch (stageNum) */
	  if (LSE_dynid_get(id,field:${traceCollector}:valid)) {
	    fprintf(${trace_file}, "* %d %s 0x%08x\n", 
	            LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),stage,
	                          extra);
	  }
	} /* pipetrace on */
      >>>;
    };

    collector FINISHED_STAGE on <<<${backEnd}>>> {
      record = <<<
	if (${pipetrace}) {
	  if (LSE_dynid_get(id,field:${traceCollector}:valid) ||
	      LSE_emu_dynid_get(id,extra.formatno)>600) {
	    int extra = 0;
	    const char *stage;
	    switch (stageNum) {
	    case 0: /* EXP */
	      extra = (LSE_emu_dynid_get(id,extra.serialization)&1)?0x10 : 0;
	      stage = "EXP";
	      break;
	    case 1: /* REN */
	      stage = "REN";
	      /* need to add RSE ops...*/
	      if (LSE_emu_dynid_get(id,extra.formatno)>600) {
		LSE_dynid_set(id,field:${traceCollector}:valid,TRUE);
		LSE_dynid_set(id,field:${traceCollector}:ptrace_seq,
		              ${trace_seq});
		fprintf(${trace_file}, "+ %d %lld ",
		        LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),
		        LSE_dynid_get(id,idno));
		${trace_seq}++;
		LSE_emu_disassemble(id,${trace_file});
	      }
	      break;
	    case 2: /* REG */
	      stage = "REG";
	      break;
	    case 3: /* EXE */
	      stage = "EXE";
	      break;
	    case 4: /* DET */
	      stage = "DET";
	      extra = (LSE_dynid_get(id,
                                     field:${backEnd}.DET.detectMispred:mispred)
		       | ( (LSE_emu_dynid_is(id,cti) &&
			    LSE_emu_dynid_get(id,branch_dir)) ? 0x100 : 0));
	      break;
	    case 5: /* WRB */
	      goto notrace;
#ifdef NOMORE
	      { int formatno;
	        formatno = LSE_emu_dynid_get(id,extra.formatno);
		if (formatno > 100 && formatno < 116 &&
		    ((${IA64lib::TypeOf_renamed_instr} *)datap)->unit ==
		    ${IA64lib::ALU})
		  goto notrace;
	      }
	      stage = "WRB";
	      fprintf(${trace_file}, "- %d\n",
	              LSE_dynid_get(id,field:${traceCollector}:ptrace_seq));
#endif
	      break;
	    default:
	      if (stageNum > 200) { /* DCU */
		stage = "DCU";
		extra = 0x00800000 + ((stageNum-200)<<16);
	      } else if (stageNum > 100) { /* FPU */
		stage = "FP";
		extra = 0x00800000 + ((stageNum-100)<<16);
	      } else 
		stage = "unknown";
	      break;
	    } /* switch (stageNum) */
	    fprintf(${trace_file}, "* %d %s 0x%08x\n", 
	            LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),stage,
	            extra);
	    notrace: ; /* if we decide not to trace (for duplicate instrs) */
	  } /* if valid */
	} /* if pipetrace on */
      >>>;
    };
    
    collector FINISHED_STAGE on <<<${DCU}>>> {
      record = <<<
	if (${pipetrace}) {
	  if (LSE_dynid_get(id,field:${traceCollector}:valid)) {
	    int extra = 0;
	    const char *stage;
	    switch (stageNum) {
	    case 0: /* EXP */
	      extra = (LSE_emu_dynid_get(id,extra.serialization)&1)?0x10 : 0;
	      stage = "EXP";
	      break;
	    case 1: /* REN */
	      stage = "REN";
	      break;
	    case 2: /* REG */
	      stage = "REG";
	      break;
	    case 3: /* EXE */
	      stage = "EXE";
	      break;
	    case 4: /* DET */
	      stage = "DET";
	      extra = (LSE_dynid_get(id,
                                     field:${backEnd}.DET.detectMispred:mispred)
		       | ( (LSE_emu_dynid_is(id,cti) &&
			    LSE_emu_dynid_get(id,branch_dir)) ? 0x100 : 0));
	      break;
	    default:
	      if (stageNum > 200) { /* DCU */
		stage = "DCU";
		extra = 0x00800000 + ((stageNum-200)<<16);
	      } else if (stageNum > 100) { /* FPU */
		stage = "FP";
		extra = 0x00800000 + ((stageNum-100)<<16);
	      } else 
		stage = "unknown";
	      break;
	    } /* switch (stageNum) */
	    fprintf(${trace_file}, "* %d %s 0x%08x\n", 
	            LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),stage,
	            extra);
	    notrace: ; /* don't want to do anything...*/
	  } /* if valid */
	} /* if pipetrace on */
      >>>;
    };

    collector PORT_CONFLICT on <<<${backEnd}.EXP>>> {
      record = <<<
	  /* stuck in ROT */
	if (${pipetrace}) {
	  fprintf(${trace_file}, "* %d ROT 0x%08x\n", 
	          LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),32);
	}
      >>>;
    };

    if (FALSE) {
    collector STLD_CONFLICT on <<<${DCU}>>> {
      record = <<<
	  /* stuck in REG before EXE */
	if (${pipetrace}) {
	  fprintf(${trace_file}, "* %d REG 0x%08x\n", 
	          LSE_dynid_get(ldID,field:${traceCollector}:ptrace_seq),
	          0x400);
	}
      >>>;
    };
    }

    collector STALL on <<<${DCU}>>> {
      record = <<<
	  /* stuck in REG before EXE */
      if (${pipetrace}) {
	if (stageNum == 3) {
	  switch (reason) {
	    case 0:
	      fprintf(${trace_file}, "* %d REG 0x%08x\n", 
	              LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),
	              0x400);
	      break;
	    case 1:
	    case 2:
	    case 3:
	      fprintf(${trace_file}, "* %d REG 0x%08x\n", 
	              LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),
	              0x2);
	      break;
	  } 
	} else if (stageNum == 4) {
	  switch (reason) {
	    case 0:
	      fprintf(${trace_file}, "* %d EXE 0x%08x\n", 
	              LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),
	              0x400);
	      break;
	    case 1:
	    case 2:
	    case 3:
	      fprintf(${trace_file}, "* %d EXE 0x%08x\n", 
	              LSE_dynid_get(id,field:${traceCollector}:ptrace_seq),
	              0x2);
	      break;
	  } 
	}
      }
      >>>;
    };

    var rememberStall = new runtime_var("rememberStall",int) : runtime_var ref;

    collector STALL on <<<${frontEnd}>>> {
      init = <<< ${rememberStall} = 0; >>>;
      record = <<<
	${rememberStall} = 14 - reason;
      >>>;
    };

    /* and other stuff needing taken care of... */
    traceCollector.end_of_timestep = <<<
    int i,j;
    LSE_signal_t sig;
    LSE_dynid_t id;
    static int ipgbubblecause=0, wrbbubblecause=0,
               rotbubblecause=0, expbubblecause=0,
               renbubblecause=0, regbubblecause=0,
               exebubblecause=0, detbubblecause=0;
    static const char *stall_reasons[] = {
      "", "unrch", "ibfull","ibub","ic_rec",
      "binit","brq","ilock","plp","imiss",
      "tlbmiss","flush", "estl", "flush", "misf", 
      "dhaz", "dcu", "rse"};

    if (${pipetrace}) {
      LSE_dynid_t regstallID=NULL;
      boolean foundstall = FALSE;

      /* finished WRB */

      wrbbubblecause = detbubblecause;
      if (wrbbubblecause) 
	fprintf(${trace_file},"$ WRB %s\n",stall_reasons[wrbbubblecause]);

      /* out of DET */
      
      detbubblecause = exebubblecause;
      if (detbubblecause) 
	fprintf(${trace_file},"$ DET %s\n",stall_reasons[detbubblecause]);

      /* out of EXE */
      if (!foundstall) { /* stall in DET doesn't squash EXE bubble */
	exebubblecause = regbubblecause;
	sig = LSE_port_query(${DCU}:stallEXEout[0],NULL,NULL);
	if (LSE_signal_data_present(sig)) {
	  exebubblecause = 16;
	  foundstall = TRUE;
	}
      }
      if (exebubblecause) 
	fprintf(${trace_file},"$ EXE %s\n",stall_reasons[exebubblecause]);

      /* out of REG */
      if (foundstall && regbubblecause && regbubblecause != 17) { 
	/* stall in EXE squashes REG bubble */
	foundstall = FALSE;
	regbubblecause = 0;
      }
      if (!foundstall) { 
	regbubblecause = renbubblecause;
	sig = LSE_port_query(${backEnd}.REG.calcStall:out[0],NULL,NULL);
	if (LSE_signal_data_present(sig)) { /* generated a stall here */
	  regbubblecause = 15;
	  foundstall = TRUE;
	}
      }
      if (regbubblecause) 
	fprintf(${trace_file},"$ REG %s\n",stall_reasons[regbubblecause]);

      /* out of REN */
      
      /* stall in REG does not squash REN bubbles because these bubbles 
       * hold the REN state machine; they contain instructions!  This
       * makes it kind of hard to compute what's going on....
       */
      if (foundstall && renbubblecause != 17 && renbubblecause) { 
	foundstall = FALSE;
	renbubblecause = 0;
      }
      if (!foundstall) {
	LSE_signal_t sig1;
	sig = LSE_port_query(${backEnd}.REN.calcStall:out[0],NULL,NULL);
	if (LSE_signal_data_present(sig)) { /* generated a stall here */
	  renbubblecause = 17;
	  foundstall = TRUE;
	} else {
	  /* don't take in the expbubble if we were being held off and
	   * had a bubble before...
	   */
	  if (renbubblecause == 17) {
	    sig = LSE_port_query(${backEnd}.REN.instrMux:out[0].ack);
	    if (LSE_signal_ack_present(sig))
	      renbubblecause = expbubblecause;
	    else foundstall = TRUE; /* still inserting a stall */
	  } else renbubblecause = expbubblecause;
	}
      }
      if (renbubblecause) 
	fprintf(${trace_file},"$ REN %s\n",stall_reasons[renbubblecause]);

      /* because of the buffer, must not just pass bubbles */
      if (foundstall && expbubblecause) { 
	foundstall = FALSE;
	expbubblecause = 0;
      }
      if (!foundstall) {
	sig = LSE_port_query(${frontEnd}:fetched_bundle[0],NULL,NULL);
	if (LSE_signal_data_present(sig)) expbubblecause = 0;
	else expbubblecause = rotbubblecause;
      }
      if (expbubblecause) 
	fprintf(${trace_file},"$ EXP %s\n",stall_reasons[expbubblecause]);

      /* into ROT */

      rotbubblecause = ipgbubblecause;
      if (rotbubblecause) 
	fprintf(${trace_file},"$ ROT %s\n",stall_reasons[rotbubblecause]);

      /* finished IPG */

      ipgbubblecause = 0; /* flush doesn't cause a bubble here! */
      if (${rememberStall}) ipgbubblecause = ${rememberStall};
      ${rememberStall} = 0;

      if (ipgbubblecause) 
	fprintf(${trace_file},"$ IPG %s\n",stall_reasons[ipgbubblecause]);

      sig = LSE_port_query(${frontEnd}:redirect_fetch[0], NULL, NULL);
      if (LSE_signal_data_present(sig)) {
	  ipgbubblecause = rotbubblecause = expbubblecause = renbubblecause =
	  regbubblecause = exebubblecause = 13;
      }

    } /* pipetrace */

    >>>;

  }; /* fun add_pipetracing */

}; /* subpackage pipetrace */
