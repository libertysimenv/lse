#!/bin/sh
FILES="./ia64/ia64multicycle.lss		\
./ia64/ia64_newid.lss				\
./ia64/ia64_npc.lss				\
./ia64/delayn.lss				\
./ia64/ia64singlecycle.lss.properties		\
./ia64/ia64singlecycle.lss			\
./ia64/ia64multicycle.lss.properties		\
./ia64/ia64simplemulti.lss			\
./ia64/ia64simplemulti.lss.properties		\
./ia64/wc					\
./lfsr/lfsr.lss					\
./lfsr/lfsr.lss.properties			\
./lfsr/xor.lss"

VERSION=2.0
mkdir tutorial-demos-${VERSION}
for fil in $FILES; do
    mkdirhier tutorial-demos-${VERSION}/$(dirname $fil)
    cp $fil tutorial-demos-${VERSION}/$fil
done
tar -cvzf tutorial-demos-${VERSION}.tar.gz tutorial-demos-${VERSION}
rm -rf tutorial-demos-${VERSION}

