Title: Short Tutorial
Author: Manish Vachharajani <manishv@ee.princeton.edu>
        David A. Penry <dpenry@cs.princeton.edu>
        Neil Vachharajani <nvachhar@cs.princeton.edu>

This is a very rough and short tutorial describing
how to use the simulator.  It basically walks one
through getting the wc benchmark to run on the 
simulator.

NOTE: This short tutorial only applies to maintainers of Liberty
and those who have CVS access to Liberty.  If instead you have
a distribution of Liberty, follow the top-level INSTALL and tutorial
instructions in that distribution.

Preparations:
     Set the environment variable CVSROOT to 
       ":ext:forest.cs.princeton.edu:/liberty/repository"
     Set the CVS_RSH environment variable to "ssh". 
     Your password is the same password that you have on the
     mescal[1-5].cs.princeton.edu machines.
     Make sure that the jave environment variables are set correctly.  
         On mescal[1-5] this is
         JAVA_HOME=/usr/java/j2sdk1.4.1_02
         PATH=$JAVA_HOME/bin:$PATH

     Make a working directory called ~/liberty where you check out all the 
     modules to install. "cd" into that directory. 

     Make certain python is in your PATH.

I. Checkout the sources and setup up up Liberty environment variables

  A. Check out all the sources
     Run:

        cvs co src

     From within the ~/liberty directory
  
  B. Run:

        src/scripts/scripts/l-env --LIBERTY=${HOME}/liberty  > ${HOME}/.liberty

      The file ~/.liberty now contains definitions of Liberty environment 
      variables.

      Source .liberty now by doing:
		in csh/tcsh:	source ~/.liberty
				rehash
		in sh/bash: 	. ~/.liberty

      Keep .liberty so that you may source it when starting
      other sessions; it would be a good idea to source it in
      your shell startup scripts.

      Note that .liberty will set your PATH, LD_LIBRARY_PATH, 
      and CLASSPATH to point to the necessary scripts, binaries,
      libraries, and Java classes.

   C. If you intend to develop private modules (those outside the
      CVS tree), set:

	LIBERTY_SIM_USER_PATH

      to the directory in which you wish to develop these modules.

      You should set this environment variable when starting other sessions.

II. Install the necessary packages

  A. Change into the ${HOME}/liberty/src directory
  
  B. Run:
  
        ./goLiberty

     This will compile and install all the packages necessary to 
     use Liberty.

  C. Install IMPACT
     
     If you are on the mescal[4-5].cs.princeton.edu machines, you will
     only need to set a few environment variables.  I recommend
     putting these definitions into a file ${HOME}/.impact, and then
     source that file from your shell startup scripts.  

     You can cut and paste the following snippet into a .impact file if
     you use sh/bash.  If you use csh/tcsh, you will need to adapt it
     to your shell's syntax.

       export IMPACT_VER=02-07-19
       export IMPACT_REL_PATH=/liberty/impact.${IMPACT_VER}
       export IMPACT_ROOT=/liberty/impact.${IMPACT_VER}
       export IMPACT_HOST_PLATFORM=x86lin
       export IMPACT_HOST_COMPILER=gcc
       export IMPACT_BUILD_TYPE=${IMPACT_HOST_PLATFORM}
       export STD_PARMS_FILE=${IMPACT_ROOT}/parms/STD_PARMS.IMPACT
       export DEFAULT_PROJECT=EPIC_8G_1BL
       export PATH=$PATH:${IMPACT_REL_PATH}/bin/${IMPACT_BUILD_TYPE}_c:${IMPACT_REL_PATH}/scripts

     Otherwise, checkout IMPACT from the repository, or download it
     from UIUC(I don't know if these versions work, impact.02-07-19 above
     works for me).  Follow all the README.install directions

  D. Set up the benchmarks directory

     If you are on the mescal[1-5].cs.princeton.edu machines, you
     can set USER_BENCH_PATH1 to /liberty/benchmarks and you're done.
     Like for all other environment variables, its probably a good idea
     to put this in your shell startup scripts.

     Otherwise, change into a directory where you wish to put the
     benchmarks and do a cvs co benchmarks from the repository (This
     will take a while and about 200MB of disk space).  This is
     optional; you can use the benchmarks that come with IMPACT
     instead.

     If you do check this stuff out set the USER_BENCH_PATH1
     environment variable to point to this directory.  If you are
     using IMPACT benchmarks set USR_BENCH_PATH1 to point to IMPACT's
     benchmark directory.  If you need more than one benchmark
     directory, you can also set USER_BENCH_PATH2.

III. Building the simulator

  A. cd into a new working directory (outside of the CVS source tree)

  B. Copy ~/liberty/src/simulator/test/features/demo.xml from the
     simulator source tree to your working directory.

  C. Run echo wc | ls-prep-bench O

       This will use IMPACT and liberty to compile the wc benchmark
       for simulation.  The O parameter tells IMPACT and Liberty to
       use optimized code.  (O_im_p should use optimized register
       allocated scheduled code, HS is Hyperblock/superblock formed
       code, and S is super block formed code only, etc.)

       The benchmark object library is created in 
         ./benches/wc/wc.EM4/Xemu.pieces.

  D. Run ls-build demo

       This builds the simulator framework for the machine described
       in the demo.xml file.  It creates the framework in
       ./machines/demo.  The actual module code is expanded in
       a directory tree under machines/demo/<root node name>.
       At the time of this writing the root node is mainpe.

  E. Run echo wc | ls-run-bench demo

       This links in the wc program with the simulator framework
       built above, runs the benchmark with the first input set,
       and then shows the diff of the results.

From here on out, you're on your own.  Reverse engineering the scripts
should be much easier with the info above though.

IV. Installing BLiSS

  BLiSS is a package that integrates the Liberty simulator with the
  functional simulator of SimpleScalar.  It also wraps SimpleScalar's 
  cache and branch predictor for use as Liberty modules.

  A. Get and install SimpleScalar version 3.0.  Follow their
     directions for compilation.  Make sure you configure it to use
     the Alpha instruction set.

  B. Set the SIMPLESCALAR environment variable to point to the
     place where you installed SimpleScalar.

  C. Change into the ~/liberty/src/bliss directory

  D. Run:

        scripts/bliss-env > ${HOME}/.bliss

     Then source the ${HOME}/.bliss file.  You will need to source this
     file any time you use BLiSS.  You may want to put it into your
     shell startup scripts.

  E. Run:

        ./doInstall

     In general, all Liberty packages in the CVS repository have a script
     called doInstall which will configure, build, and install the package
     provided all its environment variables are set.  The l-env and bliss-env
     scripts, presently, set all the necessary environment variables.

  BLiSS is now completely installed.  The next section will walk you
  through building a simulator with BLiSS.

V. Building the simulator with BLiSS

  A. cd into a new working directory (outside of the CVS source tree)

  B. Copy ~/liberty/src/simulator/test/features/demo.xml from the
     simulator source tree to your working directory.

  C. Run bliss-build demo

       This builds the simulator framework for the machine described
       in the demo.xml file.  It creates the framework in
       ./machines/demo.  The actual module code is expanded in
       a directory tree under machines/demo/<root node name>.
       At the time of this writing the root node is mainpe.

  D. Set the LIBERTY_BENCH_BIN_PATH environment variable

       You should make this point to your library of precompiled
       Alpha/OSF binaries.  If you are on the
       mescal[1-5].cs.princeton.edu machines, you can make this
       variable point to /liberty/benchmarks.alpha.OSF/bin-opt.

  E. Run echo wc | bliss-run-bench demo

       This links in the wc program with the simulator framework
       built above, runs the benchmark with the first input set,
       and then shows the diff of the results.

  Note that when you use BLiSS, there is no need to prepare the
  benchmark.  Any binary that SimpleScalar can run should work with
  BLiSS.  If you are interested in integrating a new functional
  simulator (in Liberty we call these emulators) with Liberty,
  understanding how BLiSS works is a good place to start.









