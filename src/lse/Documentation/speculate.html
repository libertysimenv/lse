<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=iso-8859-1">
	<TITLE></TITLE>
	<META NAME="GENERATOR" CONTENT="StarOffice/5.2 (Linux)">
	<META NAME="AUTHOR" CONTENT=" ">
	<META NAME="CREATED" CONTENT="20010831;13592600">
	<META NAME="CHANGEDBY" CONTENT="David A. Penry">
	<META NAME="CHANGED" CONTENT="20010929;13413000">
</HEAD>
<BODY>
<H1 ALIGN=CENTER>Speculation in Liberty</H1>
<H3 ALIGN=CENTER>29 September 2001</H3>
<P ALIGN=CENTER><B>David A. Penry</B></P>
<P><BR><BR>
</P>
<H2>Introduction</H2>
<P>A simulator for modern microprocessor architectures must be aware
of and support speculation. Speculation can arise from many sources:
control speculation and data speculation are the most obvious, but
exceptions are also a form of speculation in the sense that if an
exception can be raised for an instruction, all later (in program
order) instructions are still speculative (in a precise machine, at
least). Pipeline flush operations can also be seen as exceptions. In
general, Liberty defines speculative as &quot;not guaranteed to
permanently update architectural state&quot;. The Liberty toolkit
provides support for many different kinds of speculation and
facilities for the researcher to extend the supported speculations.</P>
<P>The problem of speculation support encompasses two main themes:</P>
<OL>
	<LI><P>Determining the speculative state which is present in the
	processor and distinguishing this from &quot;true&quot; state.</P>
	<LI><P>Determining when different parts of the processor learn about
	mis-speculation or confirmation of speculation; i.e. determining
	when speculative state is forgotten or transformed to &quot;true&quot;
	state.</P>
</OL>
<P>These two themes are discussed in this document.</P>
<H2>Speculative state</H2>
<P>There are three kinds of state (whether speculative or
non-speculative): architectural state, memory state, and internal
state. Architectural state is that state which is directly visible to
the programmer, e.g. architectural registers. Memory state is the
state of all addressable memories. Internal state is all other state.
The three types of state are handled differently in Liberty, as
described below:</P>
<H3>Architectural state</H3>
<P>Changes to architectural state are computed through the emulation
interface. Emulation and interpretation use the same interface, so
when emulation is referred to, interpretation can also be assumed
unless otherwise noted. Emulation is the process whereby dynamic
instruction information is computed and speculative architectural
state is updated. Emulation can be performed in several different
places:</P>
<OL>
	<LI><P>At the point of deciding the next instruction. This point
	does not support self-modifying code. Data speculation is only
	possible if the speculated results cannot affect control transfer
	behavior.</P>
	<LI><P>At the point where an instruction returns from memory. This
	point makes self-modifying code possible. Data speculation has the
	same restrictions as in the previous case.</P>
	<LI><P>At the point where an instruction's operands are available.
	This point makes data speculation possible. Note that it may be
	necessary in some cases to break up an instruction into
	sub-instructions because operands become available at different
	times. This is an open issue.</P>
</OL>
<P>In general, the earlier that emulation is done, the more
assumptions of &quot;perfect&quot; behavior that can be made by the
simulation. For example, if the emulation is done when the next
instruction is decided, then a simulator configuration that always
predicts the next instruction perfectly (thus never having to truly
control-speculate) can be simulated properly, subject to the
limitations of this emulation point. 
</P>
<P>The format of the architectural state is defined in &quot;isa.h&quot;
for the benchmark/interpreter and space for one &quot;main&quot; copy
of the architectural state is pre-allocated. API calls exist to
allocate additional copies of state and to save/restore the state
into/from such copies.</P>
<P>Any module which performs speculation (e.g. instruction
sequencers, O-O-O issue units with data speculation) <B>must</B>
implement mechanisms to separate the speculative and non-speculative
architectural state. The exact mechanisms are left up to the module,
however, there are &quot;standard&quot; ways of dealing with
speculation which the standard modules use. In all cases,
emulation/interpretation can only take place using operands in the
&quot;main&quot; architectural state. The standard mechanisms are:</P>
<UL>
	<LI><P>As instructions execute, the original contents of destination
	registers are buffered with the dynamic instruction information.</P>
	<LI><P>Upon notification of mis-speculation (see the section on
	timing for how this is accomplished), the speculating module copies
	buffered original contents to the &quot;main&quot; state in reverse
	order back to the point of mis-speculation. Mis-speculation only
	undoes the instructions which are reported as dependent upon the
	mis-speculation by the speculating module. This allows squashing of
	selective groups of instructions.</P>
	<LI><P>Exceptions use the same mechanism, but are sent to the
	instruction sequencer. If an instruction which causes an exception
	is not the oldest one, then execution of any exception handler will
	be speculative. Internally generated pipeline flushes for any reason
	can be implemented using exceptions.</P>
</UL>
<P>There are API calls to help with the standard mechanisms.</P>
<P>The standard mechanisms work for control speculation without any
additional thought. Data speculation requires a bit more analysis.
One way it could work is by placing the speculative operands into the
&quot;main&quot; state before emulation and placing results into the
&quot;main&quot; state at instruction completion (the result update
is in case the instruction result had been speculated at some point).
Then if the mechanism for dealing with data mis-speculation is to
re-inject the squashed data-dependent instructions out of some buffer
without going through the instruction fetch process, then the
standard mechanism for dealing with mis-speculation will work. (Of
course, the re-injection is up to the speculating module). If the way
of dealing with mis-speculation is to flush all later instructions
(later in program order) and re-fetch, then an exception (encoded
appropriately as a &quot;flush&quot; exception) should be sent to the
instruction sequencer, which will recover the state. The
data-speculating unit can then forget any state it has saved.</P>
<P>Note again that the mechanism used depends upon the module. It is
entirely possible to use different mechanisms; one reasonable
decision might be to let the modules keep track of this state
themselves just as in the processor being modelled (though that is
likely to be less efficient).</P>
<P>Speculation which cannot result in incorrect architectural state
(i.e. speculation used for scheduling non-memory operations) does <B>not</B>
require any roll-back mechanisms.</P>
<P>Microarchitectures which have more complicated fetch behavior,
such as multi-threaded or &quot;execute down multiple paths&quot;
microarchitectures, will require special instruction sequencers and
issue modules. Such modules could use multiple current states and
switch between them as needed before emulating. The API calls should
still be helpful.</P>
<P>Imprecise exceptions can be supported by allowing instructions to
commit out of order.</P>
<H3>Memory state</H3>
<P>The handling of memory state depends upon whether a uni-processor
or multi-processor/multi-threaded simulation is taking place. Each
case is covered below:</P>
<P>Note that self-modifying code can only be run if an interpreter
which does not do pre-decoding is used.</P>
<H4>Uniprocessor</H4>
<P>The memory state changes are handled just as architectural state
changes are. Changes to memory state are computed by the emulation
interface and are immediately available. Each instruction that
modifies memory buffers the previous memory values in its dynamic
information structure. Mis-speculation and exceptions roll back
memory state changes.</P>
<P>This suffices for most situations, but fails when:</P>
<UL>
	<LI><P>Self-modifying code changes an instruction which would be
	fetched before the store completes or which would be in the
	instruction cache. (If the instruction cache is allowed to become
	stale by the architecture, the instruction cache may need to model
	not just values but also data).</P>
	<LI><P>The program (even though it is uniprocessor) uses a weaker
	memory model than sequential consistency. (An example: *a = 1; while
	(*a != 1); might iterate if a weak memory model is used. 
	</P>
	<LI><P>Execution of stores down both paths of a branch
	simultaneously is part of the microarchitecture. It is just too
	complicated to try to keep track of multiple speculative memory
	states!</P>
</UL>
<P>In either of these cases, the simulator should be put into a
multi-processor mode. 
</P>
<H4>Multi-processor/multi-threaded/self-modifying code</H4>
<P>When multiple processors are being simulated, the exact timing of
memory operations matters, as ordering may affect the values of loads
or even exceptions generated. Thus, loads and stores are not
buffered, but are fully simulated. This leads to the following
restrictions:</P>
<UL>
	<LI><P>In a multi-processor situation, no instruction using memory
	operands may be emulated until operands have been fetched from
	memory. These operands include data returned from loads. In such a
	situation, these instructions are issued before being emulated. It
	may be necessary to break apart the instruction into
	sub-instructions. How this is to be done flexibly is an open issue.</P>
	<LI><P>Stores are not actually put into memory until they are put
	into memory in the simulated machine.</P>
	<LI><P>Memory hierarchy component modules should perform all
	operations using &quot;real&quot; timing and obeying &quot;real&quot;
	coherency rules.</P>
</UL>
<P>Of course, it is anticipated that there will be a significant
performance impact from these restrictions. It is possible to
tradeoff accuracy with performance by carefully relaxing various
restrictions. For example, code using atomic T&amp;S or swap
operations to implement locks would operate &quot;correctly&quot; (if
perhaps with incorrect ordering of the processors contending for the
lock) regardless of when emulation takes place, as long as the
emulation code implements the T&amp;S or swap as an atomic operation.</P>
<H3>Internal state</H3>
<P>Changes to internal state are computed by the various modules and
channels. All this state is located in the modules or channels.
Whether a module separates speculative and non-speculative state is
entirely up to the module. The module may receive notification that
previous speculations are resolved as correct or wrong and may then
update internal state as desired.  Channels never separate
speculative and non-speculative states and cannot receive
notification about previous speculations.</P>
<P>Note that in-memory structures (e.g. page tables) that are
accessed by the hardware but not by emulated or interpreted code may
be considered part of internal state. For example, if page fault
handlers are executed as part of the emulated code, the page tables
would not be internal state (as they are going to be accessed by
executing instructions), but if they are a part of the &quot;simulation
environment&quot; they are considered internal state.</P>
<H2>Speculation timing</H2>
<P>To resolve a speculation, the speculating unit must have inputs
which allow it to know the real results so that comparisons can be
made. If such inputs are missing, speculation is impossible and only
non-speculative execution should proceed. Some units may treat a
missing real result port as a &quot;can't occur&quot; action: one
example is the instruction sequencer. If the exception_occurred port
is not connected, it assumes that exceptions are not being modelled
and ignores exceptions as a possible reason for fetched instructions
to be speculative.</P>
<P>Once a speculation has been resolved, other modules need to be
informed about what has happened. There is a simulator type
(LSE_resolution_t) provided for messages about resolutions. An API is
provided to build messages of this type. The speculating module
builds a message with a list of instructions which are now resolved,
<B>in the order in which they were speculated</B>.<SPAN STYLE="font-weight: medium">
This message is received by modules on a normal port. Modules may
then use this message to deal with internal state.</SPAN></P>
<P><SPAN STYLE="font-weight: medium">It is possible that a
speculating module may experience some latency after the resolution
before it stops speculating. If this happens, it may continue to add
instructions to the message (even after it has been sent). However,
modules which have received the message already will <B>not</B> see
the additional instructions (after all, you can't turn back the
clock!) Presumably a module which has already received the message
used it only to stop doing something and will not use the message
contents until after enough delay has occurred. Alternatively, a
module which &quot;duplicates&quot; the message after the speculation
has stopped may be added to resend the message.</SPAN></P>
<H2>Multiple speculations</H2>
<P>A machine may have multiple sources of speculation.  For example,
when exceptions and control transfer speculations are possible, they
really form two different sources of speculation.  Some modules may
treat different speculation sources differently.   For example, a
branch predictor might wish to delay updating its state while an
instruction is still control-speculative, but not care whether the
instruction (or a previous instruction) might experience an
exception.  Furthermore, if a speculation is not made, there does not
need to be a resolution message for it.  Therefore, the following
services are provided by the simulator framework:</P>
<OL>
	<LI><P>Modules which create speculation declare that they are doing
	so.  The control transfer and exception speculations are built-in
	and do not need to be declared.</P>
	<LI><P>Modules which care about speculation declare which sources of
	speculation they are interested in.  By default they are interested
	in all, unless their resolution ports are not connected.</P>
	<LI><P>An API call is provided which allows speculating modules to
	mark a dynamic ID with their speculation.  Another API call allows a
	module to record which resolutions it has seen and to determine
	whether an instruction is still or was ever speculated at the
	sources of speculation which the module cares about.</P>
</OL>
<P>These services allow the user to configure which speculations
matter to which modules.</P>
<H2>Open issues</H2>
<UL>
	<LI><P>Sub-instruction timing: what happens if operands are read at
	different times in the real machine and the timing matters? An
	example would be a memory-memory architecture with post increment
	doing things like: add (r1),(r1)+,(r1)+ where it's an MP system and
	there could be someone else writing to these areas of memory.... The
	way to handle it is clear: actually perform the sub-instructions at
	different times, but the way to specify this in an ISA-independent
	fashion when writing the modules is not.</P>
</UL>
</BODY>
</HTML>
