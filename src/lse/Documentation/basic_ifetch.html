<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=iso-8859-1">
	<TITLE>Liberty Simulator Base Instruction Fetch Unit</TITLE>
	<META NAME="GENERATOR" CONTENT="StarOffice/5.2 (Win32)">
	<META NAME="CREATED" CONTENT="20010124;15202010">
	<META NAME="CHANGEDBY" CONTENT="David A. Penry">
	<META NAME="CHANGED" CONTENT="20010125;19185860">
</HEAD>
<BODY>
<H1 ALIGN=CENTER>Liberty Simulator Base Instruction Fetch Unit</H1>
<H4 ALIGN=CENTER>David A. Penry</H4>
<P ALIGN=CENTER>Last modified: 25 Jan 2001</P>
<H2>Introduction</H2>
<P>This document describes the &quot;base&quot; instruction fetch
unit operation and interfaces. Other instruction fetch units should
maintain the same interfaces in order to interact well with other
units of the simulator, but may define additional interfaces or data
structures.<BR><BR><B>NOTE: this document describes how the
instruction fetch unit should work; at present not all of these
features are available. All ports are used for communication, but
some of the results are ignored. For example, no speculation is done;
in fact, the Ifetch unit flushes the pipe after every CTI, whether
taken or non-taken.</B></P>
<H2>Theory of operation</H2>
<P>The instruction fetch unit is responsible for fetching
instructions, decoding the instructions, and placing them in the
issue window. Decoding is not a traditional operation of an
instruction fetch unit; for an explanation of why it is included in
the instruction fetch unit here, see the simulator theory of
operation. 
</P>
<H3>Instruction fetch data structure</H3>
<P>The base instruction fetch unit data structure is a buffer for
holding fetched instructions. It is made up of a number of lines,
each able to hold a given number of either bytes or instructions.
Lines are &quot;atomic&quot; units; while it may take a while to fill
a line, no instruction may be used until the line is full. A line is
not marked empty until it has been completely used or thrown away due
to a control transfer instruction. Thus, a buffer which fills an
instruction at a time, &quot;shifts&quot; single instructions into
decode, and then no longer remembers them should be modelled as a
buffer with one instruction or perhaps one byte wide lines. The width
of each line is set by the <I>fetch_line_width</I> parameter and the
depth of the buffer is set by the <I>fetch_num_lines</I> parameter 
</P>
<P>The selection of instruction vs. byte buffer sizing is controlled
with the <I>fetch_buf_is_bytes</I> parameter. For RISC architectures,
we would expect to set this parameter false. The ifetch unit must
handle the instruction to bytes translations; the memory hierarchy
deals exclusively with bytes. 
</P>
<P>Before requesting bytes from the memory hierarchy the instruction
fetch unit <B>must</B> know how many (and which) bytes will be
fetched by the request so that it does not issue multiple requests
for the same line of data. This can be handled through a query to the
memory hierarchy. 
</P>
<H3>Instruction fetch steps</H3>
<P>Each instruction goes through the following steps. Steps for a
particular instruction occur one at a time unless otherwise noted. 
</P>
<OL>
	<LI><P>Ifetch unit determines that the instruction is interesting to
	fetch. <BR><BR>A dynamic instruction ID is allocated at this point.
	<BR><BR>See later description of how the decision about what is
	interesting is made. <BR><BR><BR>
	</P>
	<LI><P>The instruction is emulated through exe.c. <BR><BR>Because
	emulation happens at a basic-block granularity, what instructions
	have been emulated must be kept track of so that it is not
	repeated.<BR><BR>The results of the instruction emulation are
	available to all units as part of the dynamic ID structure.
	Therefore, it is unnecessary for units to have ports for the &quot;real&quot;
	results of the instruction as inputs for validating predictions.</P>
	<LI><P>The Ifetch unit &quot;fetches&quot; the instruction.&nbsp;
	This consists of two parallel steps:</P>
</OL>
<OL START=4>
	<OL>
		<LI><P STYLE="margin-bottom: 0in">If the instruction is not already
		in a fetch buffer, Ifetch unit requests it from the memory
		hierarchy.</P>
		<P STYLE="margin-bottom: 0in"><BR>An instruction may be already in
		a fetch buffer because: 
		</P>
		<UL>
			<LI><P STYLE="margin-bottom: 0in">A previous fetch fetched this
			instruction as a side effect and it was kept. This case describes
			the operation of line buffers; the cache returns an entire cache
			line and all these instructions are buffered. 
			</P>
			<LI><P STYLE="margin-bottom: 0in">The instruction was previously
			fetched for some reason and is still in a buffer where it can be
			used. An example of such a case is a design where fetched cache
			lines are buffered in the ifetch unit and branches within the line
			are allowed to fetch out of these bufffered cache lines,
			effectively allowing small loops or if/then/else's to branch
			without penalties due to fetches. Several parameters control this
			behavior: 
			</P>
			<UL>
				<LI><P STYLE="margin-bottom: 0in">When <I>cti_bw_can_hit_buffer</I>
				is true, control transfer targets can hit targets earlier in the
				same fetch buffer line 
				</P>
				<LI><P STYLE="margin-bottom: 0in">When <I>cti_fw_can_hit_buffer</I>
				is true, control transfer targets can hit targets later in the
				same fetch buffer line 
				</P>
				<LI><P STYLE="margin-bottom: 0in">When <I>cti_df_can_hit_buffer</I>
				is true, control transfer targets can hit targets in different
				fetch buffer lines which have not been fetched specifically for
				that target. 
				</P>
			</UL>
		</UL>
		<P STYLE="margin-bottom: 0in">The request is sent on
		<B>instruct_mem_trigger</B> and <B>instruct_mem_addr</B> with zero
		delay. It is necessary to query <B>instruct_mem_trigger</B> about
		what bytes will be returned by the request.&nbsp; The request
		returns on <B>instruct_mem_valid</B>. 
		</P>
		<LI><P STYLE="margin-bottom: 0in">Ifetch unit notifies other units
		of the fetch <B>without having decoded the instruction</B> (since
		it has not yet &quot;officially&quot; arrived). 
		</P>
		<P>&nbsp; <BR>A branch target buffer is an example of what could be
		attached to this port. In general, any structure which is triggered
		by fetch and operates without knowing anything but the instruction
		address can be consulted here. Such a structure can make a
		prediction about the next address to fetch. If the structure does
		not make a prediction, the result port must not be connected.
		<BR><BR>This notification is sent on <B>fetch_notify_trigger</B>
		and <B>fetch_notify_addr</B>.&nbsp; The result (if any) returns on
		<B>fetch_notify_valid</B> and <B>fetch_notify_target</B>. 
		</P>
	</OL>
	<P STYLE="margin-bottom: 0in">If either of these steps is blocked
	from starting because needed output ports are blocked, both are
	blocked.&nbsp; They are also both blocked from starting if decode
	(the next step) is blocked.<BR><BR>
	</P>
	<LI><P STYLE="margin-bottom: 0in">The Ifetch unit decodes the
	instruction (after instruction has been fetched from memory).<BR><BR>The
	pipeline behavior of decodes is modelled with an external channel.
	The port to send to is <B>decode_trigger</B> and the port to receive
	from is <B>decode_valid</B>. The timing/bandwidth of decode are
	channel parameters, however, as there is no such channel
	implemented, the bandwidth limiting and latency logic are inside the
	decode unit for now.<BR><BR>This takes time <I>decode_latency</I> to
	do. <I>decode_bandwidth_phase</I> limits the number of instructions
	that can be decoded per clock phase and <I>decode_bandwidth_cycle</I>
	limits the number of instructions that can be decoded per cycle.
	<I>decode_bandwidth_inverted</I> inverts the bandwidth
	calculation.The sending of decodes to <B>decode_trigger</B> is
	blocked from starting when branch prediction is blocked and
	<I>propagate_bp_blocked</I> is true or when <B>decode_trigger</B> is
	blocked.<BR><BR>
	</P>
	<LI><P>Instruction is handled after decoding. This includes two (or
	three) parallel steps:</P>
	<OL>
		<LI><P>If instruction is an appropriate CTI, Ifetch unit asks
		branch predictor where the CTI will go. <BR><BR>A branch predictor
		is an example of what could be attached to this port. In general,
		any structure which can predict the next fetch address given decode
		information can be consulted here. There is an inherent conflict
		between the potential results of a prediction here and a prediction
		from the fetch notification. The conflict is resolved by taking the
		first predicted value to arrive at the fetch unit; if they arrive
		in the same clock, the behavior is non-deterministic.<BR><BR>If no
		such structure exists, nothing happens. If multiple structures
		(e.g. a branch predictor and a return address stack) are required,
		another module arbitrating between them must be connected. Such an
		arbiter would accept requests and determine which goes to which
		structure. The arbiter must support the same queries as these
		structures. <BR><BR>This request is sent on <B>branch_pred_trigger</B>
		and <B>branch_pred_addr</B> with zero delay. It is made in parallel
		with both issue and decode notification. The request returns on
		<B>branch_pred_valid</B> and <B>branch_pred_target</B>. <BR><BR><BR>
		</P>
		<LI><P>Ifetch unit notifies other units that decode is completed
		<BR><BR>A branch predictor is an example of what could be attached
		to this port. In general, any structure which needs to know that an
		instruction has been decoded but doesn't wait for it to be issued
		can be attached here. If no such structure exists, nothing happens.
		<BR><BR>This notification is sent on <B>decode_notify_trigger</B><SPAN STYLE="font-weight: medium">.</SPAN></P>
	</OL>
	<P>If either of these steps is blocked from starting because needed
	output ports are blocked, both are blocked.&nbsp; They are also
	blocked from starting if issue (the next step) is blocked and
	<I>propagate_issue_blocked</I> is true.<BR><BR>Also, if
	<I>issue_waits_for_bp</I> is false, the next step (send to issue
	unit) is performed in parallel with these two steps; in this case
	all three parallel steps block from starting if any of them block
	from starting.<BR><BR><B>NOTE: This will be changing; why should
	issue of the branch wait for the branch predictor to be finished
	doing its prediction?</B></P>
	<LI><P>Ifetch unit sends instruction to issue unit.<BR><BR>The
	notification is sent on <B>issue_trigger</B>. <BR><BR><BR>
	</P>
	<LI><P>If the instruction was a conditional branch, the instruction
	fetch unit is eventually notified of its resolution. &nbsp; 
	</P>
	<P STYLE="margin-bottom: 0in">Resolution activities take place as
	described later. The port used is <B>result_known_trigger</B>. If
	nothing is attached to this port, the <B>completed_trigger</B> port
	is used. If nothing is attached to that port, resolution is assumed
	to be immediate after the later of issue, decode notification, and
	branch prediction.<BR><BR><B>NOTE: This will be changing; both the
	direction and target address are known at different times and must
	be treated differently.<BR></B><BR>
	</P>
	<LI><P>When the instruction is completed, the instruction fetch unit
	is notified.<BR><BR>This is used to determine when all instructions
	have completed so that simulation may terminate. The
	<B>completed_trigger</B> port receives the notification. If this
	port is not connected, <B>result_known_trigger</B> is used. If that
	port is not connected as well, completion is assumed to be immediate
	after the later of issue, decode notification, and branch prediction
	requests. <BR><BR><BR>
	</P>
</OL>
<P>Multiple instructions will be in flight at any given time; the
policy for the first step determines how instruction fetches overlap.
Multiple steps can overlap for a single instruction where this makes
sense. 
</P>
<H3>Control of instruction overlapping</H3>
<P>The instruction fetch unit manages the multiple instructions in
flight by maintaining a state machine for each instruction. It
attempts to move each instruction through its states as quickly as it
can, subject to the back-pressure from the issue unit and queries
that it makes and the bandwidth limitations in decode. Instructions
are operated on in order of their states; those closest to the end of
their state machines are operated on first. A partial ordering of
instructions is maintained; no instruction is allowed to pass another
one which is ahead of it in a trace, thus preserving &quot;in-order&quot;
fetching. If there is free instruction buffer space, an additional
fetch is launched. 
</P>
<H3>Determining what fetch to launch</H3>
<P>The most basic parameter is <I>speculate_fetch</I>. When this is
false, no speculative instructions are ever fetched, so fetches
beyond the branch are not launched until the branch resolves. Thus
the fetch unit simply fetches in-line until a branch is recognized,
either through a BTB or decode and then waits for it to resolve. 
</P>
<P>When it is true, then <I>speculate_issue</I> comes into play. When
this is false, fetches beyond branches may be launched, but the
results may not be issued. Now a choice of what to fetch must be
made. Fetch continues in-line unless a CTI is reached. Once a CTI is
reached, if there is a way to predict what to do (in-line vs. some
target), fetch will do that (not cancelling previous fetch lines
yet). If not, then fetch continues in-line unless this is obviously
stupid (indirect jumps). When a CTI resolves, either the new fetch
lines (which may not have returned from the memory subsystem yet!) or
the previous fetch lines are cancelled. Fetches in flight (i.e. in
the state machines) that need to be cancelled are squashed
immediately. If <I>check_cti_resolution</I> is true, the simulation's
resolution of the CTI is checked against the actual one. Value
predictors, BTBs, and branch predictors must be notified that all
instructions which depended upon the resolved CTI are committed or
squashed. This is done using the <B>commit_out_trigger</B> and
<B>squash_out_trigger</B> ports. The number of outstanding CTIs is
limited by <I>speculate_cti_limit</I>. 
</P>
<P>When <I>speculate_issue</I> is true, it may make sense to fetch
and issue along multiple paths. When <I>speculate_multi_path</I> is
false, fetch occurs as in the previous paragraph (but all units must
be notified of commits and squashes). When it is true, fetch will
alternate among possible paths, forming a tree of such paths.
Round-robin scheduling of these fetches is presumed. A new branch of
the tree is only made when a conditional branch is encountered.
Resolutions are handled in a similar fashion. Obviously, there is
more to think about here. 
</P>
<P>When a decision to speculatively fetch happens, the architectural
state of the processor must be saved and attached to the information
about dependent instructions. CTI resolutions which cancel a decision
must restore the architectural state before restarting the fetch on
the correct path. 
</P>
<H2>Ports</H2>
<H3>Ports to instruction memory</H3>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><B>instruct_mem_trigger</B> -
	output - void - trigger instruction fetch from memory hierarchy. 
	</P>
	<UL>
		<LI><P STYLE="margin-bottom: 0in">fetch_return_size(addr) : &lt;
		start, length&gt; : - data range that will be returned with a fetch
		of addr. 
		</P>
	</UL>
	<LI><P STYLE="margin-bottom: 0in"><B>instruct_mem_addr</B> - output
	- address - target machine address to fetch 
	</P>
	<LI><P><B>instruct_mem_valid</B>- input - void - instruction fetch
	from memory finished 
	</P>
</UL>
<H3>Ports to branch predictors</H3>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><B>fetch_notify_trigger</B> -
	output - void - predict branch/get BTB before decode 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><B>fetch_notify_addr</B> - output
	- address - target machine address 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><B>fetch_notify_valid</B> - input
	- boolean - is address predicted? 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><B>fetch_notify_target</B> - input
	- addr - address (only if predicted) 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><B>branch_pred_trigger</B> -
	output - ID - predict branch/get BTB after decode 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><B>branch_pred_addr</B> - output -
	address - target machine address 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><B>branch_pred_valid</B> - input -
	boolean - is branch predicted? 
	</P>
	<LI><P><B>branch_pred_target</B> - input - addr - address (only if
	predicted) - note if it's predicted not taken, the not-taken address
	must be sent; this is available in the dynamic ID even though a real
	branch predictor wouldn't have stored it. 
	</P>
</UL>
<H3>Decode ports</H3>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><B>decode_trigger</B> - output -
	void - do decode 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><B>decode_valid</B> - input - void
	- decode is finished 
	</P>
	<LI><P><B>decode_notify_trigger</B> - output - void - decode has
	completed (<I>temporary port</I>) 
	</P>
</UL>
<H3>Ports to issue unit</H3>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><B>issue_trigger</B> - output -
	void - instruction ready for issue 
	</P>
</UL>
<H3>Resolution ports</H3>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><B>result_known_trigger</B> -
	output - void - instruction result known 
	</P>
	<LI><P><B>completion_trigger</B> - input - void - instruction
	completed 
	</P>
</UL>
<H3>Broadcast ports</H3>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><B>commit_out_trigger</B> - output
	- void - instruction is no longer speculative 
	</P>
	<LI><P><B>squash_out_trigger</B> - output - void - instruction was
	speculative and is now squashed. 
	</P>
</UL>
<H2>Parameters</H2>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><I>cti_bw_can_hit_buffer</I> -
	CTIs can hit backwards within line buffer 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>cti_fw_can_hit_buffer</I> -
	CTIs can hit forwards within line buffer 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>cti_df_can_hit_buffer</I> -
	CTIs can hit other lines in line buffer (assumed that can always hit
	lines specifically prefetched for them) 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>decode_bandwidth_phase</I> -
	maximum decodes per clock phase (<I>temporary</I>) 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>decode_bandwidth_cycle</I> -
	maximum decodes per clock cycle (<I>temporary</I>) 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>decode_bandwidth_inverted</I> -
	when true, causes all bandwidth calculations for decode to read as
	clock cycles(phases) per decode. Used for non-pipelined decode
	units. (<I>temporary</I>) 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>decode_latency</I> - time to
	decode instruction before can be considered for issuing (<I>temporary</I>)
		</P>
	<LI><P STYLE="margin-bottom: 0in"><I>fetch_buf_is_bytes</I> - fetch
	buffer line width measured in bytes (true) or instructions (false) 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>fetch_line_width</I> - fetch
	buffer line width 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>fetch_num_lines</I> - number of
	lines in fetch buffer 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>issue_waits_for_bp</I> - don't
	send to issue unit if branch predictor hasn't responded 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>propagate_bp_blocked - stop
	pipe immediately if branch predictor blocked</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>propagate_decode_blocked - stop
	pipe immediately if decode blocked</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>propagate_dnotify_blocked -
	stop pipe immediately if decode notify blocked</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>propagate_fnotify_blocked -
	stop pipe immediately if fetch notify blocked</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>propagate_issue_blocked - stop
	pipe immediately if issue blocked</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>speculate_cti_limit - Maximum
	CTIs to speculate past</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>speculate_fetch - can fetch
	speculative instructions</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>speculate_issue - can put
	speculative instructions in the issue window</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>speculate_multi_path -
	speculate fetch along multiple paths</I> 
	</P>
</UL>
<H2><I>Events</I></H2>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><I>FETCH(id) - instruction is
	fetched</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>EMULATE(id) - instruction is
	head of a call to emulation</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>MEM_FETCH(id) - instruction is
	requested from memory</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>MEM_RETURNED(id) - instruction
	is returned from memory</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>FETCH_PRED_RETURNED(id, bool
	predicted, LSE_emu_addr_t addr) - prediction after fetch returns a
	prediction</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>BRANCH_PRED_REQ(id) - asking
	for branch prediction</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>BRANCH_PRED_RETURNED(id, bool
	predicted, LSE_emu_addr_t addr) - branch prediction returns</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>DECODE(id) - decoding</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>ISSUE_TO_WINDOW(id) - send
	instruction to issue window.</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>CTI_SPECULATED(id) -
	speculating fetch beyond CTI with id</I> 
	</P>
	<LI><P><I>CTI_RESOLVED(id) - speculated CTI is resolved</I> 
	</P>
</UL>
<H2><I>Outstanding issues</I></H2>
<UL>
	<LI><P STYLE="margin-bottom: 0in"><I>Modelling trace caches</I> 
	</P>
	<LI><P STYLE="margin-bottom: 0in"><I>Modelling stored decode
	knowledge in the fetch unit</I> 
	</P>
	<LI><P><I>Limiting bandwidth of requests to other units when there
	are delays in the communication paths.</I> <BR><I>We want to have
	units do their own bandwidth limiting; i.e. if the branch predictor
	can make one prediction per clock we don't want anyone else to have
	to have that as a parameter, but with delays we have to &quot;see
	into the future&quot; to send a request without exceeding bandwidth.
	If bandwidth is fixed and guaranteed, one query at init can resolve;
	if bandwidth can fluctuate or block, then some protocol must be
	arranged. Sending tokens representing bandwidth back and forth as
	between the issue window and the instruction fetch unit is a good
	idea, but do we want to generalize it to everyone? Another
	possibility for fixed bandwidths it to just ignore the possibility
	of overflow and make certain through upstream bandwidth constraints
	of the sender that nothing will overflow (as happens in many real
	designs: we can't decode more than one instruction at a time, so we
	can't branch predict more than one)</I> 
	</P>
	<LI><P><I>Exception handling</I> 
	</P>
</UL>
</BODY>
</HTML>
