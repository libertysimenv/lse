		To do list for the simulator

Methodology
-----------
- Multiple clock domains
- Think about making scheduled queries look more like ports
- Think about "get" APIs and data buffer handling
5/18/05 - Think about demand-driven calculation

Command-line interface
----------------------
9/8/04 - command-line option to redirect LSE_stderr to a file

Infrastructure (framework/scripts)
-------------------------------------------
2/9/11 - The LSS parser does not write types in a deterministic order; this
         can cause multiple types with the same definitions to change names
         from build to build, leading to rebuilds when nothing changed.  
	 Sorting the type names might help, since the names at least seem
	 to be consistent (though I have not verified this)
7/16/09 - When domain instance or class headers change, we don't know
          to do a rebuild.  Could do this via checksums on the headers,
          but what about additional headers they include?  It might be
          easier to just say "no changes in domains trigger rebuilds" and
          be sure the user knows this.
4/1/08 - Change instno to porti in control points to match all manuals
         and corelib modules, but use a reference so that we don't
 	 break old code.
9/8/07 - Should I get rid of the reactive attribute?  It isn't used for
  	 scheduling; even if it were, the configurer can mess things up
	 by using state in a userpoint.
6/8/06 - Think seriously about changing the phase_start rules or allowing
	 delays in value assignments so that I can treat phase_start as 
	 a simple W-codeblock and make it legal to perform queries in it.
	 Might want to have 'produces' rules in annotations as well...
6/8/06 - Better port query implementation and analysis: do not dynamically
         put together who is to be woken up, but form complete master list
	 of codeblocks who might call.  As they query, mark them as "needed"
         if value unknown.  Be careful to make data structures efficient.
	 On setting of value, only wakeup those requiring run; may also
	 skip the marking and wakeup steps when doing static scheduling.  
	 This is also good because it moves towards supporting fanout and
	 drivers instead of handlers.  Means that textual port query 
	 analysis absolutely required for correct execution, even in 
	 dynamic scheduling.
6/8/06 - Allow instance port_dataflow annotation to affect queried signals
   	 at finer granularity; this will allow user to really fine-tune stuff
5/31/06 - Interesting problem with selective-trace scheduling.  It does not
	  maintain the "must execute at least once" invariant because the
	  fact that something is scheduled doesn't guarantee that it will
	  be executed.  Issue shows up when there are mixed phase functions
	  with userpoints which might be called at phase_start and there
	  are handlers.  The pipe module has the problem.
	  May want to remove the guarantee entirely, which
	  will cause all sorts of interesting things to occur...
5/24/06 - Remove unneeded variables sccsG and reachG in find_static_sched_int
	  in scheduler.c
4/30/06 - Remove m4_ everything and make tokenizer work as
	  simple transformations to a parse tree: what we have
	  now is overkill for speed and complexity
8/28/05 - Use tokens when analyzing cf dependencies
5/18/05 - Error checking in scheduler when malloc returns NULL
9/8/04 - Finer-grained rebuild on event changes
2/5/04 - consider making SIM_time_numticks_t a signed number so 
	 that subtraction makes sense for it
9/9/03 - Make it possible to do LSS enumerated types on the command line
	 (easier said than done, as we do not maintain a list of values in
	  the back end) 
09/03/03 - Ensure that simulator finalizes correctly (i.e. releases
	   all memory, etc.) so that main loops which repeat simulation
	   will work properly.
7/23/03 - Global user functions for start/end of cycle & init/finish
7/4/03   - Check for query index out of range at analysis when 
	   possible (handle_queries in LSE_schedule.py)
2/13/03 - New stats abilities
	  - Add stat delay semantics
  	  - Coordinated statistics
1/24/03 - Look at -Wall and see what can be done
08/20/03 - Complain about scheduled queries in phase_start?
10/23/03 - Improve error reporting when parsing domain m4 files
	   (e.g. checking that an invoked backend interface exists)
??? - Figure out LSE_lobotomize_schedule_code: why is TRUE not much slower?
??? - Examine keeping one copy of port status vs. two copies.
??? - Port queries in end_of_timestep show up in px signal dependencies.  Is
      this necessary?

- Scheduling:
	6/21/06 - Think about threading list scheduling implementation
		to see if there are lower-complexity ways to do some
		things, particularly the ETF-like stuff.
	6/8/06 - Change forcing at beginning of dynamic sections to
		 insert directly into the static schedule; this will
	 	 cause places which would have scheduled this code
		 earlier to see it as a static schedule.  Must think
		 about ordering of these and the effect upon things already
		 in the schedule.  Might result in more invocations,
		 now that I think about it.
	7/16/03 - Look at the SCHED_INFINITE values being returned in
		  scheduler.c.  Some of them could profitably be
		  S->num_on instead of F->num_nodes; right now a single
		  give-up can cause the whole schedule to be given
		  up?  Be careful, though: when returning a lower
		  infinite, need to create a dynamic schedule to go
		  with it and need to prevent runtime explosion
	8/19/03 - Make LSE_controlpoint_call_default() understood as nesting
		  its return values in analysis
	- Specialize schedulable query handling code
	- Make LSE_signal_or and LSE_signal_and analyzable.
	- Prevent queries in end_of_timestep from actually trying to schedule!
	- distinguish between scheduled and non-scheduled queries in
	  code points for scheduling purposes 
	  (LSE_database.py:inst_has_a_querying_userpoint,queryReachable)
	9/8/04 - Ability to split parallel sections in schedule while 
		 coalescing could improve the ability of things to be moved 
		 (right now a bad coalesce will mess things up badly)


Other
-----

