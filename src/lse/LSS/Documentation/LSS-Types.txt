Base Types [backend type]
----------

BoolType            [bool]
CharType            [N/A]
CodeType            [N/A]
DecodePointType     [N/A]
FloatType           [N/A]
InstanceRefType     [N/A]
IntType             [int]
LiteralType         [N/A]
NoneType            [none]
ParameterRefType    [N/A]
PortInstanceRefType [N/A]
PortRefType         [N/A]
StringType          [N/A]
UnknownType         
VoidType            [void]

UserPointType (not really a type, but a class for all userpoint types)
AggregateType (not really a type, but a class for all aggregate types)
ArrayType (not really a type, but makes arrays of other types)
EnumType (not really a type, but a class for all enumerated types)
FunctionType (not really a type, but a class for all function types)

ExternalType (not really a type, but a wrapper for external types)

ModifiedType (not really a type, an abstract class for type modifiers)
ConstType


Sub Type Relation
------------------
CodeType <: DecodePointType
CodeType <: UserPointType (all user points)
IntType <: FloatType
StringType <: LiteralType
AggregateType (with labels l0,l1,...,ln) <: 
       AggregateType (with labels l0,l1,...lm) where n >= m
ArrayType (with element type t and width w) <: 
       ArrayType (with elementType t and width -1)
UnknownType <: PortRefType
UnknownType <: ParameterRefType
UnknownType <: InstanceRefType

Compatability Relation
-----------------------
ArrayType (with element type t and width w) COMPAT
       ArrayType (with elementType t and width -1)
UnknownType COMPAT PortRefType
UnknownType COMPAT ParameterRefType
UnknownType COMPAT InstanceRefType


