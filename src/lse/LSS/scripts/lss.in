#!/bin/sh
# The next line is a null operation in the shell.  We do this to find
# python in the user's path
"""cd" .
exec python $0 ${1+"$@"}
"
"""

import getopt,sys,string,os

def usage(msg=""):
    progname = os.path.basename(sys.argv[0])
    sys.stderr.write("Error: " + msg + "\n")
    sys.stderr.write("""
Usage: %s [options] [LSS filenames]

 Builds the source tree for a module

 Arguments:
    filename            - the name of an LSS file to process

 Options:
    -h|--help           - show this message
    -m                  - specify the module search path 
                            (defaults to $LSS_MOD_PATH)
    -o                  - specify the output directory (defaults to '.')
    --fraternite        - handle Fraternite specific extensions
    --jflags            - specify a Java option
"""%progname)

def dosysfail(cmd):
    prog = os.path.basename(sys.argv[0])
    if os.system(cmd) != 0:
        sys.stderr.write("\nFailed in " + cmd + "\n")
        sys.exit(1)

def main():
    LSE=os.path.abspath(os.path.dirname(os.path.dirname(sys.argv[0])))
    LSE_BIN=LSE + "/bin"
    LSE_DATA=LSE + "/share"
    LSE_INCLUDE=LSE + "/include"
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], "dhm:o:",
                                   ["fraternite", "help","jflags="])
    except getopt.GetoptError, (msg,opt):
        # Print usage message and exit
        usage(msg)
        sys.exit(2)

    output_dir = "."
    
    if os.environ.has_key('LSS_MOD_PATH'):
	module_search_path = os.environ['LSS_MOD_PATH']
    else:
	module_search_path = ''
	
    debug_mode = 'no';
    fraternite = 'no';

    javaopts = ""

    for option,argument in opts:
        if option in ["-h", "--help"]:
            usage()
            sys.exit(2)
        if option in ["-m"]:
            module_search_path = argument
        if option in ["-o"]:
            output_dir = argument
        if option in ["-d"]:
            debug_mode = 'yes';
        if option in ["--fraternite"]:
            fraternite = 'yes';
        if option in ["--jflags"]:
            javaopts += " " + argument

    if len(args) < 1:
        usage("No input file specified")
        sys.exit(2)

    if len(module_search_path) != 0:
      module_search_path = LSE_DATA + "/lse:" + module_search_path
    else:
      module_search_path = LSE_DATA + "/lse"

    @ISCYGWIN_TRUE@iscygwin = 1
    @ISCYGWIN_FALSE@iscygwin = 0

    try:
      classpath = os.environ['CLASSPATH']
    except:
      classpath = None
    jarfile = LSE_DATA+"/ext/java/org.jutil-coal.jar:" + \
	LSE_DATA+"/ext/java/java_cup-runtime.jar:" + LSE_DATA + "/lse/LSS.jar"
    if classpath: classpath = classpath + ":" + jarfile
    else: classpath = jarfile

    if iscygwin:
	jarfile = "`cygpath -w " + jarfile + "`"
	inputfile = "`cygpath -w " + inputfile + "`"
	output_dir = "`cygpath -w " + output_dir + "`"
	module_search_path="\"`cygpath -w -p " + module_search_path + "`\""
	classpath = "`cygpath -w -p " + classpath + "`"
   
    dosysfail("java" + javaopts + 
              " -DLiberty.LSS.output_dir=" + output_dir +
    	      " -DLiberty.LSS.mod_path=" + module_search_path + 
              " -Ddebug_mode=" + debug_mode +
              " -DLiberty.LSS.fraternite=" + fraternite +
	      " -cp " + classpath +
	      #" -jar + jarfile +
	      " Liberty.LSS.Main " + string.join(args," "))

if __name__ == "__main__":
    main()
