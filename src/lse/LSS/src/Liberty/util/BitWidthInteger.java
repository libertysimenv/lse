package Liberty.util;

import java.math.BigInteger;

public class BitWidthInteger extends Number implements Comparable
{
    private BigInteger value;
    private boolean signed;
    private int width;
    private final BigInteger signSwitchOver;
    private final BigInteger modulus;

    public BitWidthInteger(boolean signed, int width, BigInteger value) {
	this.signed = signed;
	this.width = width;

	BigInteger two = new BigInteger("2");
	modulus = two.pow(width);

	if(signed) {
	    signSwitchOver = modulus.divide(two);
	} else {
	    signSwitchOver = null;
	}

	this.value = truncate(value);
    }

    public BitWidthInteger(int width, BigInteger value) {
	this(true, width, value);
    }

    private BigInteger truncate(BigInteger v) {
	/* Make v have the correct number of bits */
	v = v.mod(modulus);
	
	/* Correct for signed values */
	if(signed) { 
	    if(v.compareTo(signSwitchOver) >= 0) {
		v = v.subtract(modulus);
	    }
	}
	
	return v;
    }

    public String toString() {
	return value.toString();
    }

    /* Functions from number */
    
    public byte byteValue() {
	return value.byteValue();
    }

    public double doubleValue() {
	return value.doubleValue();
    }

    public float floatValue() {
	return value.floatValue();
    }

    public int intValue() {
	return value.intValue();
    }

    public long longValue() {
	return value.longValue();
    }

    public short shortValue() {
	return value.shortValue();
    }

    /* Compare function */
    public int compareTo(Object o) {
	return value.compareTo(((BitWidthInteger)o).value);
    }

    /* Useful functions */
    public int getWidth() {
	return width;
    }

    public boolean getSigned() {
	return signed;
    }

    /* Operation functions */
    public BitWidthInteger cast(boolean signed, int width) {
	return new BitWidthInteger(signed, width, value);
    }

    private static BitWidthInteger buildBitWidthInteger(BitWidthInteger src1,
							BitWidthInteger src2,
							BigInteger v) {
	boolean signed = src1.getSigned() || src2.getSigned();
	int width = src1.getWidth() < src2.getWidth() ? src2.getWidth() : 
	    src1.getWidth();
	return new BitWidthInteger(signed, width, v);

    }

    public BitWidthInteger add(BitWidthInteger v) {
	BigInteger nv = value.add(v.value);
	return buildBitWidthInteger(this, v, nv);
    }

    public BitWidthInteger subtract(BitWidthInteger v) {
	BigInteger nv = value.subtract(v.value);
	return buildBitWidthInteger(this, v, nv);
    }

    public BitWidthInteger mulitply(BitWidthInteger v) {
	BigInteger nv = value.multiply(v.value);
	return buildBitWidthInteger(this, v, nv);
    }

    public BitWidthInteger divide(BitWidthInteger v) {
	BigInteger nv = value.divide(v.value);
	return buildBitWidthInteger(this, v, nv);
    }

    public BitWidthInteger shiftLeft(int n) {
	return new BitWidthInteger(signed, width, value.shiftLeft(n));
    }

    public BitWidthInteger shiftRight(int n) {
	return new BitWidthInteger(signed, width, value.shiftRight(n));
    }

    public BitWidthInteger negate() {
	return new BitWidthInteger(signed, width, value.negate());
    }

    public BitWidthInteger increment() {
	return new BitWidthInteger(signed, width,
				   value.add(BigInteger.ONE));
    }

    public BitWidthInteger decrement() {
	return new BitWidthInteger(signed, width, 
				   value.subtract(BigInteger.ONE));
    }
}
