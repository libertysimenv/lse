package Liberty.util;

import java.util.*;

public class CheckpointHashMap implements CheckpointMap, BasicMap
{
    private CheckpointHashMap checkpoint;
    private HashMap mappings;
    private boolean locked;

    public CheckpointHashMap() {
	mappings = new HashMap();
	checkpoint = null;
	locked = false;
    }

    private CheckpointHashMap(CheckpointHashMap cp) {
	this();
	checkpoint = cp;
    }

    public CheckpointMap checkpoint() {
	locked = true;
	return new CheckpointHashMap(this);
    }

    public int size() {
	if(checkpoint != null)
	    return (checkpoint.size() + mappings.size());
	else
	    return mappings.size();
    }

    public boolean isEmpty() {
	if(checkpoint != null) 
	    return (checkpoint.isEmpty() && mappings.isEmpty());
	else
	    return mappings.isEmpty();
    }

   public boolean containsKey(Object key) {
	if(checkpoint != null)
	    return (checkpoint.containsKey(key) || mappings.containsKey(key));
	else
	    return mappings.containsKey(key);
    }

    public boolean containsValue(Object value) {
	if(checkpoint != null)
	    return (checkpoint.containsValue(value) || 
		    mappings.containsValue(value));
	else
	    return mappings.containsValue(value);
    }

    public Object get(Object key) {
	if(checkpoint != null) {
	    if(checkpoint.containsKey(key))
		return checkpoint.get(key);
	    else
		return mappings.get(key);
	} else
	    return mappings.get(key);
    }

    public Object put(Object key, Object value) {
	if(locked && !containsKey(key)) {
	    throw new UnsupportedOperationException("Cannot put new keys " +
						    "into a locked " +
						    "CheckpointHashMap");
	} else {
	    if(checkpoint != null &&
	       checkpoint.containsKey(key))
		return checkpoint.put(key,value);
	    else
		return mappings.put(key,value);
	}
    }

    public Object remove(Object key) throws UnsupportedOperationException {
	throw new 
	    UnsupportedOperationException("Remove operation not supported " + 
					  "on CheckpointHashMap");
    }

    public void putAll(Map t) {
	throw new 
	    UnsupportedOperationException("PutAll operation not supported " + 
					  "on CheckpointHashMap");
    }

    public void clear() throws UnsupportedOperationException {
	throw new 
	    UnsupportedOperationException("Clear operation not supported " + 
					  "on CheckpointHashMap");
    }
 
    public Set keySet() {
	if(checkpoint != null) {
	    return new PairSet(checkpoint.keySet(), mappings.keySet());
	}
	else
	    return mappings.keySet();
    }

    public Collection values() {
	if(checkpoint != null)
	    return new PairCollection(checkpoint.values(), mappings.values());
	else
	    return mappings.values();
    }

    public Set entrySet() {
	if(checkpoint != null)
	    return new PairSet(checkpoint.entrySet(), mappings.entrySet());
	else
	    return mappings.entrySet();
    }

    public boolean equals(Object o) {
	try {
	    Map m = (Map)o;
	    return m.entrySet().equals(this.entrySet());
	} catch (ClassCastException e) {
	    return false;
	}
    }

    public int hashCode() {
	Set es = this.entrySet();
	Iterator i = es.iterator();
	int hc = 0;

	while(i.hasNext()) {
	    Object o = i.next();
	    hc += o.hashCode();
	}
	return hc;
    }
}

class PairCollection implements Collection {
    ArrayList members;

    PairCollection(Collection c1, Collection c2) {
	members = new ArrayList();
	members.addAll(c1);
	members.addAll(c2);
    }

    public int size() {
	return members.size();
    }

    public boolean isEmpty() {
	return members.isEmpty();
    }

    public boolean contains(Object o) {
	return members.contains(o);
    }

    public Iterator iterator() {
	return members.iterator();
    }

    public Object[] toArray() {
	return members.toArray();
    }

    public Object[] toArray(Object[] inArray) {
	return members.toArray(inArray);
    }

    public boolean add(Object o) {
	throw new 
	    UnsupportedOperationException("Add operation not supported " + 
					  "on PairCollection");
    }

    public boolean remove(Object o) {
	throw new 
	    UnsupportedOperationException("Remove operation not supported " + 
					  "on PairCollection");
    }

    public boolean containsAll(Collection c) {
	return members.containsAll(c);
    }

    public boolean addAll(Collection c) {
	throw new 
	    UnsupportedOperationException("AddAll operation not supported " + 
					  "on PairCollection");
    }

    public boolean removeAll(Collection c) {
	throw new 
	    UnsupportedOperationException("RemoveAll operation not " + 
					  "supported PairCollection");
    }

    public boolean retainAll(Collection c) {
	throw new 
	    UnsupportedOperationException("RetainAll operation not " + 
					  "supported PairCollection");
    }

    public void clear() {
	throw new 
	    UnsupportedOperationException("Clear operation not " + 
					  "supported PairCollection");
    }

    public String toString() {
	return members.toString();
    }
}

class PairSet extends PairCollection implements Set
{
    public PairSet(Set a, Set b) {
	super(a,b);
    }
}
