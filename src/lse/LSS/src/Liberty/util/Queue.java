package Liberty.util;

import java.util.*;

public class Queue extends Vector
{
    public Queue() {
	super();
    }

    public Queue(Queue q) {
	super(q);
    }
    
    public boolean empty() {
	return isEmpty();
    }

    public Object peek() throws EmptyQueueException {
	try {
	    return firstElement();
	} catch(NoSuchElementException e) {
	    throw new EmptyQueueException();
	}
    }

    public Object dequeue() throws EmptyQueueException {
	try {
	    return remove(0);
	} catch(ArrayIndexOutOfBoundsException e) {
	    throw new EmptyQueueException();
	}
    }

    public Object enqueue(Object o) {
	addElement(o);
	return o;
    }

    int search(Object o) {
	int i = indexOf(o,0);
	if(i == -1) return -1;
	else return i+1;
    }
}
