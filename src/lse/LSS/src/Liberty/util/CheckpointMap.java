package Liberty.util;

import java.util.*;

public interface CheckpointMap extends Map {
    public CheckpointMap checkpoint();
}
