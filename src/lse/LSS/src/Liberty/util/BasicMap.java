package Liberty.util;

import java.util.*;

public interface BasicMap
{
    public boolean containsKey(Object key);
    public Object get(Object key);
    public Object put(Object key, Object value);
    public Set keySet();
}
