package Liberty.LSS;

import java.util.*;

public abstract class LSSException extends Exception
{
    CodeObtainable srcLine;
    List context; // Calling context in which the Exception occurred

    public LSSException() {
	super();
	context=null;
    }
    
    public LSSException(String message) {
	super(message);
	srcLine = null;
    }

    public LSSException(CodeObtainable o, String message) {
	super(message);
	srcLine = o;
    }

    public void addToCallingContext(CodeObtainable callsite) {
	if(context == null) context=new LinkedList();
	context.add(callsite);
    }

    public void clearCallingContext() {
	context=null;
    }

    public String getMessage() {
	String message = "";

	if(srcLine != null) {
	    message = "Error on line " + srcLine.getLine() + 
		" in " + srcLine.getFileName() + ": ";
	}

	message += super.getMessage();

	if(context != null) {
	    Iterator i=context.iterator();
	    CodeObtainable callsite;

	    callsite = (CodeObtainable) i.next();

            message += "\n" + "In function called on line " + callsite.getLine() + " in " + callsite.getFileName() + "\n";
	    while(i.hasNext()) {
		callsite = (CodeObtainable)i.next();
		message += "called from function on line " + callsite.getLine() + " in " + callsite.getFileName() + "\n";
	    }
        }

	return message;
    }

    public boolean hasLineNumber() {
	return (srcLine != null);
    }
    
    public CodeObtainable getSourceLine() {
	return srcLine;
    }

    public String getRawMessage() {
	return super.getMessage();
    }
}
