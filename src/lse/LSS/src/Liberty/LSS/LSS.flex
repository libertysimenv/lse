package Liberty.LSS;
import Liberty.LSS.AST.*;
import java_cup.runtime.*;
import java.util.*;

%%

%{
  private Stack curlyBraceStack = new Stack();
  private StringBuffer pws = new StringBuffer();
  private String filename = null;
  private int stringReturnState;

  /* Add some constructors so we can pass in a file name */
  public Scanner(java.io.Reader in, String fn)
  {
      this(in);
      setFileName(fn);
  }

  public Scanner(java.io.InputStream in, String fn)
  {
      this(in);
      setFileName(fn);
  }

  public void setFileName(String fn)
  {
      filename = fn;
  }

  private TokenInfo buildTokenInfo() 
  {
      TokenInfo ti = 
	  new TokenInfo(filename, pws.toString(), yytext(), 
			yyline+1, yychar+1, 
			yychar + yytext().length());
      //pws.setLength(0); 
      pws = new StringBuffer();
      return ti; 
  }
%}

%line
%column
%char

%public
%class Scanner

%cupsym Tokens
%cup

CDECINT    =    0 | [1-9][0-9]*
CBININT    =    0[bB][01]+
COCTINT    =    0[0-7]+
CHEXINT    =    0[xX][0-9a-fA-F]+
CINT       =    {CDECINT}|{COCTINT}|{CHEXINT}
CFLOAT     =    (([0-9]+("."[0-9]*)?)|("."[0-9]+))([eE][+-]?[0-9]+)?
CCHAR      =    '[ -~]' | '\\' | '\n' | '\t' | '\r'
IDENTIFIER =    [A-Za-z][_A-Za-z0-9]*

%state EMBED
%state SUBST_STRING
%state SUBST_STRING_END
%state MULTI_STRING
%state SINGLE_STRING

%%
<YYINITIAL,EMBED> {
  /* Constants */
  {CBININT}    { long x = Long.parseLong(yytext().substring(2),2);
	         IntLiteral v = new IntLiteral(x, buildTokenInfo());
                 return new Symbol(Tokens.INT_LITERAL, v); }
  {CINT}       { long x = Long.decode(yytext()).intValue();
	         IntLiteral v = new IntLiteral(x, buildTokenInfo());
		 return new Symbol(Tokens.INT_LITERAL, v); }
  {CFLOAT}     { double x = Double.parseDouble(yytext());
	         FloatLiteral v = new FloatLiteral(x, buildTokenInfo());
                 return new Symbol(Tokens.FLOAT_LITERAL, v); }
  {CCHAR}      { String x = yytext();
                 x = x.substring(1,x.length()-1);
                 CharLiteral v = new CharLiteral(x, buildTokenInfo());
                 return new Symbol(Tokens.CHAR_LITERAL, v); }
  \"\"\"       { stringReturnState = yystate();
                 yybegin(MULTI_STRING);
                 return new Symbol(Tokens.TRIPLE_QUOTE, buildTokenInfo()); }
  \"           { stringReturnState = yystate();
                 yybegin(SINGLE_STRING);
                 return new Symbol(Tokens.QUOTE, buildTokenInfo()); }
  "<<<"        { yybegin(SUBST_STRING); 
                 return new Symbol(Tokens.STRING_BEGIN, buildTokenInfo()); } 

  /* Package stuff */
  "package"    { return new Symbol(Tokens.PACKAGE, buildTokenInfo()); }
  "subpackage" { return new Symbol(Tokens.SUBPACKAGE, buildTokenInfo()); }
  "using"      { return new Symbol(Tokens.USING, buildTokenInfo()); }

  /* Flow Control */
  "if"         { return new Symbol(Tokens.IF, buildTokenInfo()); }
  "else"       { return new Symbol(Tokens.ELSE, buildTokenInfo()); }
  "elsif"      { return new Symbol(Tokens.ELSIF, buildTokenInfo()); }
  "break"      { return new Symbol(Tokens.BREAK, buildTokenInfo()); }
  "for"	       { return new Symbol(Tokens.FOR, buildTokenInfo()); }
  "return"     { return new Symbol(Tokens.RETURN, buildTokenInfo()); }
		       	       	       	       	  
  /* Data Types */
  "type"        { return new Symbol(Tokens.TYPE, buildTokenInfo()); }
  "int"	        { return new Symbol(Tokens.INT, buildTokenInfo()); }
  "char"        { return new Symbol(Tokens.CHAR, buildTokenInfo()); }
  "float"       { return new Symbol(Tokens.FLOAT, buildTokenInfo()); }
  "double"      { return new Symbol(Tokens.DOUBLE, buildTokenInfo()); }
  "literal"     { return new Symbol(Tokens.LITERAL, buildTokenInfo()); }
  "string"      { return new Symbol(Tokens.STRING, buildTokenInfo()); }
  "boolean"     { return new Symbol(Tokens.BOOL, buildTokenInfo()); }
  "enum"        { return new Symbol(Tokens.ENUM, buildTokenInfo()); }
  "void"        { return new Symbol(Tokens.VOID, buildTokenInfo()); }
  "none"        { return new Symbol(Tokens.NONE, buildTokenInfo()); }
  "struct"      { return new Symbol(Tokens.STRUCT, buildTokenInfo()); }
  "userpoint"   { return new Symbol(Tokens.USERPOINT, buildTokenInfo()); }
  "runtime_var" { return new Symbol(Tokens.RUNTIMEVAR, buildTokenInfo()); }
  "ref"         { return new Symbol(Tokens.REF, buildTokenInfo()); }
  "domain"      { return new Symbol(Tokens.DOMAIN, buildTokenInfo()); }
  
  /* Modifiers */
  "const"       { return new Symbol(Tokens.CONST, buildTokenInfo()); }

  /* Other */
  "import"       { return new Symbol(Tokens.IMPORT, buildTokenInfo()); }
  "include"      { return new Symbol(Tokens.INCLUDE, buildTokenInfo()); }
  "var"          { return new Symbol(Tokens.VAR, buildTokenInfo()); }
  "typedef"      { return new Symbol(Tokens.TYPEDEF, buildTokenInfo()); }
  "fun"          { return new Symbol(Tokens.FUN, buildTokenInfo()); }
  "module"       { return new Symbol(Tokens.MODULE, buildTokenInfo()); }
  "instance"     { return new Symbol(Tokens.INSTANCE, buildTokenInfo()); }
  "parameter"    { return new Symbol(Tokens.PARAMETER, buildTokenInfo()); }
  "runtimeable"  { return new Symbol(Tokens.RUNTIMEABLE, buildTokenInfo()); }
  "runtime_parm" { return new Symbol(Tokens.RUNTIMEPARM, buildTokenInfo()); }
  "new"          { return new Symbol(Tokens.NEW, buildTokenInfo()); }
  "local"        { return new Symbol(Tokens.LOCAL, buildTokenInfo()); }
  "internal"     { return new Symbol(Tokens.INTERNAL, buildTokenInfo()); }
  "export"       { return new Symbol(Tokens.EXPORT, buildTokenInfo()); }
  "as"           { return new Symbol(Tokens.AS, buildTokenInfo()); }  
  "on"           { return new Symbol(Tokens.ON, buildTokenInfo()); }
  "initialized"  { return new Symbol(Tokens.INITIALIZED, buildTokenInfo()); }

  /* Ports */
  "inport"      { return new Symbol(Tokens.INPORT, buildTokenInfo()); }
  "outport"     { return new Symbol(Tokens.OUTPORT, buildTokenInfo()); }
  "port"        { return new Symbol(Tokens.PORT, buildTokenInfo()); }

  /* Events and statistics */
  "emits"       { return new Symbol(Tokens.EMITS, buildTokenInfo()); }
  "event"       { return new Symbol(Tokens.EVENT, buildTokenInfo()); }
  "collector"   { return new Symbol(Tokens.COLLECTOR, buildTokenInfo()); }

  /* Queries */
  "query"       { return new Symbol(Tokens.QUERY, buildTokenInfo()); }
  "method"      { return new Symbol(Tokens.METHOD, buildTokenInfo()); }
  "locked"      { return new Symbol(Tokens.LOCKED, buildTokenInfo()); }

  /* Operators */
  "|||"           { return new Symbol(Tokens.BITWISE_OR, buildTokenInfo()); }
  "=>"          { return new Symbol(Tokens.RETURNS, buildTokenInfo()); }
  "->"          { return new Symbol(Tokens.CONNECT, buildTokenInfo()); }
  "<<"          { return new Symbol(Tokens.LEFTSHIFT, buildTokenInfo()); }
  ">>"          { return new Symbol(Tokens.RIGHTSHIFT, buildTokenInfo()); }
  "+"           { return new Symbol(Tokens.PLUS, buildTokenInfo()); }
  "-"           { return new Symbol(Tokens.MINUS, buildTokenInfo()); }
  "*"           { return new Symbol(Tokens.TIMES, buildTokenInfo()); }
  "/"           { return new Symbol(Tokens.DIVIDE, buildTokenInfo()); }
  "%"           { return new Symbol(Tokens.MOD, buildTokenInfo()); }
  "="           { return new Symbol(Tokens.ASSIGN, buildTokenInfo()); }
  "?="          { return new Symbol(Tokens.COND_ASSIGN, buildTokenInfo()); }
  "+="          { return new Symbol(Tokens.PLUS_ASSIGN, buildTokenInfo()); }
  "-="          { return new Symbol(Tokens.MINUS_ASSIGN, buildTokenInfo()); }
  "*="          { return new Symbol(Tokens.TIMES_ASSIGN, buildTokenInfo()); }
  "/="          { return new Symbol(Tokens.DIVIDE_ASSIGN, buildTokenInfo()); }
  "%="          { return new Symbol(Tokens.MOD_ASSIGN, buildTokenInfo()); }
  "++"          { return new Symbol(Tokens.INCREMENT, buildTokenInfo()); }
  "--"          { return new Symbol(Tokens.DECREMENT, buildTokenInfo()); }
  "!"           { return new Symbol(Tokens.EXCLAMATION, buildTokenInfo()); }
  "&&"          { return new Symbol(Tokens.LOGICAL_AND, buildTokenInfo()); }
  "||"          { return new Symbol(Tokens.LOGICAL_OR, buildTokenInfo()); }
  "<"           { return new Symbol(Tokens.LESS_THAN, buildTokenInfo()); }
  ">"           { return new Symbol(Tokens.GREATER_THAN, buildTokenInfo()); }
  "<="          { return new Symbol(Tokens.LESS_THAN_EQUAL, buildTokenInfo()); }
  ">="          { return new Symbol(Tokens.GREATER_THAN_EQUAL, buildTokenInfo()); }
  "=="          { return new Symbol(Tokens.EQUAL, buildTokenInfo()); }
  "!="          { return new Symbol(Tokens.NOT_EQUAL, buildTokenInfo()); }
  ";"           { return new Symbol(Tokens.SEMICOLON, buildTokenInfo()); }
  ","           { return new Symbol(Tokens.COMMA, buildTokenInfo()); }
  "?"           { return new Symbol(Tokens.QUESTION, buildTokenInfo()); }
  ":"           { return new Symbol(Tokens.COLON, buildTokenInfo()); }
  "."           { return new Symbol(Tokens.DOT, buildTokenInfo()); }
  "|"           { return new Symbol(Tokens.PIPE, buildTokenInfo()); }
  "("           { return new Symbol(Tokens.LPAREN, buildTokenInfo()); }
  ")"           { return new Symbol(Tokens.RPAREN, buildTokenInfo()); }
  "["           { return new Symbol(Tokens.LBRACK, buildTokenInfo()); }
  "]"           { return new Symbol(Tokens.RBRACK, buildTokenInfo()); }
  "&"           { return new Symbol(Tokens.BITWISE_AND, buildTokenInfo()); }
  "~"           { return new Symbol(Tokens.BITWISE_NOT, buildTokenInfo()); }

  "::"          { return new Symbol(Tokens.PACKAGE_SEPERATOR,  
                                    buildTokenInfo()); }
  "''"          { return new Symbol(Tokens.TYPE_OF, buildTokenInfo()); }

  {IDENTIFIER}    { Identifier v = new Identifier(yytext(), buildTokenInfo());
                    return new Symbol(Tokens.IDENT, v); }
  '{IDENTIFIER}   { TypeVar v = new TypeVar(yytext().substring(1), TypeVar.TYPE, buildTokenInfo());
                    return new Symbol(Tokens.TYPEVAR, v); }
  #{IDENTIFIER}   { TypeVar v = new TypeVar(yytext().substring(1), TypeVar.NUMERIC, buildTokenInfo());
                    return new Symbol(Tokens.SIZEVAR, v); }

  /* Whitespace & comments */  
  [ \t\r\n]     { pws.append(yytext()); }
  "/*" ~"*/"    { pws.append(yytext()); }
  "//" ~"\n"    { pws.append(yytext()); }

  <<EOF>>       { return new Symbol(Tokens.EOF, buildTokenInfo()); }

}

<YYINITIAL> {
  "{"           { return new Symbol(Tokens.LBRACE, buildTokenInfo()); }
  "}"           { return new Symbol(Tokens.RBRACE, buildTokenInfo()); }
}

<MULTI_STRING> {
  \"\"\"       { yybegin(stringReturnState);
                 return new Symbol(Tokens.TRIPLE_QUOTE, buildTokenInfo()); }
  ~(\"\"\")    { yypushback(3);
                 StringFragment v = new StringFragment(yytext(), buildTokenInfo());
                 return new Symbol(Tokens.STRING_FRAGMENT, v); }
}

<SINGLE_STRING> {
  \"           { yybegin(stringReturnState);
                 return new Symbol(Tokens.QUOTE, buildTokenInfo()); }
  [^\n\r\"]+   { StringFragment v = new StringFragment(yytext(), buildTokenInfo());
                 return new Symbol(Tokens.STRING_FRAGMENT, v); }
}

<EMBED> {
  "{"           { Integer i = (Integer)curlyBraceStack.pop();
                  curlyBraceStack.push(new Integer(i.intValue() + 1));
		  return new Symbol(Tokens.LBRACE, buildTokenInfo()); }
  "}"           { Integer i = (Integer)curlyBraceStack.pop();
                  if(i.intValue() == 0) {
                    yybegin(SUBST_STRING);
	            return new Symbol(Tokens.EMBED_END, buildTokenInfo());
                  } else {
                    curlyBraceStack.push(new Integer(i.intValue() - 1));
  		    return new Symbol(Tokens.RBRACE, buildTokenInfo());
                  }
                }
}

<SUBST_STRING> {
  !([^]* (">>>"|"${") [^]*) ">>>"  { yybegin(SUBST_STRING_END);
                                     yypushback(3);
                                     StringFragment v = new StringFragment(yytext(), buildTokenInfo());
                                     return new Symbol(Tokens.STRING_FRAGMENT, v); }

  !([^]* (">>>"|"${") [^]*) "${"  { yybegin(SUBST_STRING_END);
                                    yypushback(2);
                                    StringFragment v = new StringFragment(yytext(), buildTokenInfo());
                                    return new Symbol(Tokens.STRING_FRAGMENT, v); }
}

<SUBST_STRING_END> {
  ">>>" { if(curlyBraceStack.empty()) yybegin(YYINITIAL);
          else yybegin(EMBED);
          return new Symbol(Tokens.STRING_END, buildTokenInfo()); }
  "${"  { curlyBraceStack.push(new Integer(0)); yybegin(EMBED);
          return new Symbol(Tokens.EMBED_BEGIN, buildTokenInfo()); }
}

.|\n    { return new Symbol(Tokens.ILLEGAL_TOKEN, buildTokenInfo()); }
