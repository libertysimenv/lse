package Liberty.LSS.types;

/* This class should be changed into an interface if there are
 * subtypes of char */

public class CharValue extends ValueBase {
    private char value;

    public CharValue(char v) {
	super(Types.charType);
	value = v;
    }

    public char getValue() {
	return value;
    }

    public String toString() {
	return "'" + value + "'";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.charType)) {
	    char charv = ((CharValue)v).getValue();
	    return (charv == value);
	} else 
	    return false;
    }

    public int hashCode() {
	return (new Character(value)).hashCode();
    }
}
