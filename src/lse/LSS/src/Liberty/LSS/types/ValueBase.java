package Liberty.LSS.types;

public abstract class ValueBase implements Value {
    private Type type;

    public ValueBase(Type t) {
	type = t;
    }

    public Type getType() {
	return type;
    }
}
