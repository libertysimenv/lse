package Liberty.LSS.types;

import Liberty.LSS.*;
import Liberty.LSS.AST.UnimplementedException;
import Liberty.LSS.AST.UndefinedSymbolException;
import Liberty.LSS.AST.SymbolAlreadyDefinedException;
import Liberty.LSS.AST.ReturnException;
import Liberty.LSS.AST.BreakException;
import Liberty.LSS.AST.FunctionException;
import Liberty.LSS.AST.ParseException;
import Liberty.LSS.AST.ExecutionContext;
import Liberty.LSS.AST.CompoundStatement;
import Liberty.LSS.AST.IdentifierLValue;
import java.util.*;

public class OverloadedFunctionValue extends ValueBase implements Callable {
    private Map functions;

    public OverloadedFunctionValue(OverloadedFunctionType t,
				   Collection funcs) 
	throws TypeException {
	super(t);

	Vector types = new Vector();
	Iterator i = funcs.iterator();
	functions = new HashMap();
	while(i.hasNext()) {
	    Value v = (Value)i.next();
	    FunctionType ft = (FunctionType)v.getType();
	    types.add(ft);
	    functions.put(new Integer(ft.getNumArguments()), v);
	}
	Type verifyType = new OverloadedFunctionType(types);

	if(!verifyType.equals(t)) {
	    throw new TypeException("Overloaded function with type " + 
				    verifyType + " masquerading as type " + t);
	}
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite) 
	throws UnimplementedException, 
	       TypeException,
	       UndefinedSymbolException,
	       FunctionException,
	       UninitializedVariableException,
	       ParseException {
	Callable c = (Callable)functions.get(new Integer(argValues.size()));

	if(c == null)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");

	return c.call(argValues, callSite);
    }

    public Collection getFunctions() {
	return functions.values();
    }
}
