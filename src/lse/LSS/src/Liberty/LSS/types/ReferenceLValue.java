package Liberty.LSS.types;

/* If an Lvalue implements this interface, then
 * lvalue.getType().isReference() should be true */

public interface ReferenceLValue extends LValue {
    public LValue deref() 
	throws TypeException,
	       UninitializedVariableException;
}
