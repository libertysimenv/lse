package Liberty.LSS.types;

public class DeferredIntType extends IntType
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new DeferredIntType());

    public String toString() {
	return "deferred int";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof DeferredIntType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}

