package Liberty.LSS.types;

import java.util.*;

public class EnumType extends Type
{
    private EnumValue[] values;

    public EnumType(Vector ids) {
	Iterator i = ids.iterator();
	Vector valueVector = new Vector();

	while(i.hasNext()) {
	    String id = (String)i.next();
	    EnumValue v = new EnumValue(id, this);
	    valueVector.add(v);
	}

	/* Make values have the correct type dynamically */
	values = new EnumValue[0]; 
	values = (EnumValue[])valueVector.toArray(values);
    }

    public String toString() {
	String s = "enum ?? {" + values[0];
	for(int i = 1; i < values.length; i++)
	    s += ", " + values[i];
	s += "} ??";
	return s;
    }

    public EnumValue[] getValues() {
	return values;
    }

    public String getBackendType() {
	String s = "enum ?? { \nLSEut_enumdef(" + values[0] + ")";
	for(int i = 1; i < values.length; i++)
	    s += ",\n LSEut_enumdef(" + values[i] + ")";
	s += "\n} ??";
	return s;
    }

}

