package Liberty.LSS.types;

import java.util.*;

public class ArrayValue extends ValueBase implements Indexable,Aggregate {
    int size;
    Value[] values;
    Type elementType;
    ArrayType arrayType;

    public ArrayValue(ArrayType t, Vector v) throws TypeException {
	super(t);
	arrayType = t;

	size = v.size();

	if(size != arrayType.getSize()) {
	    throw new TypeException("Attempting to put " + 
				    Integer.toString(size) + 
				    " elements into value of type " + t);
	}

	elementType = arrayType.getElementType();
	while(!elementType.isBaseType()) {
	    elementType = elementType.getBaseType();
	}
	values = new Value[size];

	for(int i = 0; i < size; i++) {
	    values[i] = (Value)v.elementAt(i);
	    if(values[i] != null && 
	       !elementType.equals(values[i].getType())) {
		throw new TypeException(values[i].toString() + ":" + 
					values[i].getType().toString() + 
					" is not compatible with " + 
					elementType.toString());
	    }
	}
    }

    public Vector getValue() {
	Vector v = new Vector(size);
	for(int i = 0; i < size; i++)
	    v.add(values[i]);
	return v;
    }

    public Value getSubValue(int i) 
	throws TypeException,
	       UninitializedVariableException {
	if(i >= size) {
	    throw new TypeException("Index " + i + " out-of-bounds on type " +
				    arrayType);
	} else if(values[i] == null) {
	    throw new UninitializedVariableException("Element " + i + 
						     " has not been " + 
						     "initialized");
	} else {
	    return values[i];
	}
    }

    public Value getSubValue(String field)
	throws TypeException {
	if(field.equals("size")) {
	    return new NumericIntValue(size);
	} else {
	    throw new TypeException("Type " + arrayType + 
				    " has no field named " + field);
	}
    }

    public String toString() {
	/* Don't explicitly call toString since values[i] could be
	   null. Java does the Right Thing(tm) if it divines that
	   toString needs to be called */
	String s = "";
	if(size > 0)
	    s += values[0];
	for(int i = 1; i < size; i++)
	    s += ", " + values[i];
 	return "{" + s + "}";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, arrayType)) {
	    /* Type relation guarantees that: 
                1) sizes match 
                2) v is Indexable */
	    Indexable iv = (Indexable)v;
	    Value v1, v2;
	    try {
		for(int i = 0; i < size; i++) {
		    try { v1 = this.getSubValue(i); } 
		    catch(UninitializedVariableException e) { v1 = null; }

		    try { v2 = iv.getSubValue(i); }
		    catch(UninitializedVariableException e) { v2 = null; }

		    if((v1 == null && v2 != null) ||
		       (v1 != null && v2 == null) ||
		       (v1 != null && v2 != null && 
			!v1.equals(v2))) return false;
		}
		return true;
	    } catch(TypeException e) {
		/* Something very bad happened */
		throw new RuntimeTypeException(e.getMessage());
	    }
	} else {
	    return false;
	}
    }

    public int hashCode() {
	return values.hashCode();
    }
}

