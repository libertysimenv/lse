package Liberty.LSS.types;

import Liberty.LSS.AST.InstanceEnvironment;
import Liberty.LSS.CodeObtainable;

public class InstanceRefType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new InstanceRefType());

    public String toString() {
	return "instance ref";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof InstanceRefType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public boolean isAggregate() {
	return true;
    }

    public Type getSubValueType(String field) 
	throws TypeException {
	/* We don't know the type, so we will just throw an
	 * exception. */
	throw new TypeException("Field types on instances are not known");
    }

    public LValue getLValue(LValue base) {
	return new InstanceLValue(base);
    }

    public LValue getLValue(Value base) {
	return new InstanceLValue(base);
    }

    public String getBackendType() {
	return "LSE_instanceref_t";
    }

}

class InstanceLValue implements AggregateLValue
{
    Value instance; 
    LValue base;    

    /* Invariants:

       1) instance <: InstanceRef 
       2) InstanceRef <: base.type (since base will be asked to store
                                    InstanceRef's)
       3) base.type <: InstanceRef (since instance = base.value)
       2 + 3 => base.type == InstanceRef
    */

    public InstanceLValue(Value e) {
	/* LValues produce with this constructor have no backing
	 * store.  Therefore, they are effectively const variables.
	 * Any assignment directly to this LValue will throw an
	 * exception. */
	
	if(TypeRelations.isSubtype(e.getType(), Types.instanceRefType)) {
	    instance = e;
	    base = null;
	} else {
	    throw new RuntimeTypeException("Cannot create an instance " +
					   "lvalue with data of type " + 
					   e.getType());
	}
    }
    
    public InstanceLValue(LValue i) {
	Type t = i.getType();
	while(!t.isBaseType())
	    t = t.getBaseType();

	if(t.equals(Types.instanceRefType)) {
	    try {
		instance = i.getValue();
	    } catch(UninitializedVariableException e) {
		instance = null;
	    }
	    base = i;
	} else {
	    throw new RuntimeTypeException("Cannot create an instance " + 
					   "lvalue with data of type " + t);
	}
    }

    public void assign(Value v)
	throws TypeException {
	assign(v,null);
    }

    public void assign(Value v, CodeObtainable s) 
	throws TypeException {
	if(base != null) {
	    base.assign(v,s);
	    instance = v;
	}
	else
	    throw new TypeException("Cannot assign to instance " + instance);
    }	

    public Type getType() {
	if(base != null)
	    return base.getType();
	else
	    return new ConstType(Types.instanceRefType);
    }

    public Value getValue()
	throws UninitializedVariableException {
	if(instance == null) {
	    throw new UninitializedVariableException("Instance ref " + 
						     "uninitialized");
	}
	return instance;
    }

    public LValue getSubLValue(String field) 
	throws TypeException {
	if(instance == null) {
	    throw new 
		TypeException("Instance ref uninitialized");
	}

	if(instance.getType().equals(Types.instanceRefType)) {
	    return ((InstanceEnvironment)instance).getSubLValue(field);
	} else {
	    LValue wrap = instance.getType().getLValue(instance);
	    if(wrap.getType().isAggregate())
		return ((AggregateLValue)wrap).getSubLValue(field);
	    else
		throw new TypeException("There is no subfield named " +
					field + " on instance " +
					instance);
	}
    }

    public String toString() {
	if(base != null) return base.toString();
	else return instance.toString();
    }

}
