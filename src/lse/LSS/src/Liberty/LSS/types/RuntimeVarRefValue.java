package Liberty.LSS.types;

import Liberty.LSS.types.*;
import java.util.regex.*;

/* This class should be changed into an interface if there are
 * subtypes of RuntimeVarRefType */

public class RuntimeVarRefValue extends ValueBase {
    String uniqueName;
    String userName;
    Type type;

    public RuntimeVarRefValue(String userName, String uniqueName,
			      Type type) {
	super(Types.runtimeVarRefType);
	this.userName = userName;
	this.uniqueName = uniqueName;
	this.type = type;
    }

    public String toString() {
	return "Runtime Variable(" + userName + 
	    "<" + uniqueName + ">" + 
	    ":" + type + ")";
    }

    public String toCurlyString() {
	return "LSEuv_ref(" + uniqueName + ")";
    }

    public Type getType() {
	return Types.runtimeVarRefType;
    }

    public String getUserName() {
	return userName;
    }
    
    public String getUniqueName() {
	return uniqueName;
    }

    public Type getVarType() {
	return type;
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.runtimeVarRefType)) {
	    RuntimeVarRefValue rv = (RuntimeVarRefValue)v;
	    return rv.uniqueName.equals(uniqueName) && rv.type.equals(type);
	} else 
	    return false;
    }

    public int hashCode() {
	return uniqueName.hashCode() + type.hashCode();
    }
}
