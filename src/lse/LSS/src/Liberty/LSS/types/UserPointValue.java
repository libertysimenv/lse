package Liberty.LSS.types;

/* This class should be changed into an interface if there are
 * subtypes of userpoint */

/* This class is not an interface because it inherits from StringValue
 * and so does its subtype (actually its subtype is StringValue) */

import java.util.*;

public class UserPointValue extends StringValue {
    private UserPointType userPointType;

    public UserPointValue(UserPointType t, Vector v) {
	super(v,t);
	userPointType = t;
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, userPointType)) {
	    Vector codev = ((UserPointValue)v).getValue();
	    return (codev.equals(getValue()));
	} else 
	    return false;
    }

    public String toString() {
	return getFlattenedValue();
    }
}
