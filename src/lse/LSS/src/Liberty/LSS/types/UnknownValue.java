package Liberty.LSS.types;

import Liberty.LSS.AST.InstanceEnvironment;
import Liberty.LSS.AST.AssignmentLogEntry;
import java.util.*;

public class UnknownValue extends ValueBase implements Indexable, Aggregate {
    private Vector names;
    private InstanceEnvironment inst;

    public UnknownValue(InstanceEnvironment i, Vector v) {
	super(new UnknownType());
	inst = i;
	names = v;
    }

    public InstanceEnvironment getInstance() {
	return inst;
    }

    public Vector getNames() {
	return names;
    }

    public Value getSubValue(String field) {
	Vector v = new Vector(names);
	v.add(field);
	return new UnknownValue(inst, v);
    }

    public Value getSubValue(int i) {
	Vector v = new Vector(names);
	v.add(new Integer(i));
	return new UnknownValue(inst, v);
    }

    public UnknownValue chop() {
	Vector v = new Vector(names);
	v.remove(0);
	return new UnknownValue(inst, v);
    }

    public String toString() {
	Iterator i = names.iterator();
	String s = i.next().toString();
	while(i.hasNext()) {
	    Object o = i.next();
	    if(o instanceof String)
		s += "." + o;
	    if(o instanceof Integer)
		s += "[" + o + "]";
	}
	return s;
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(o instanceof UnknownValue) {
	    UnknownValue u = (UnknownValue)o;
	    if(!inst.equals(u.inst)) return false;
	    
	    Iterator i = names.iterator();
	    Iterator j = u.names.iterator();
	    while(i.hasNext() && j.hasNext()) {
		Object o1 = i.next();
		Object o2 = j.next();
		if(!o1.equals(o2)) return false;
	    }
	    if(i.hasNext() || j.hasNext()) return false;
	    else return true;
	} else
	    return false;
    }

    public int hashCode() {
	int hc = inst.hashCode();
	Iterator i = names.iterator();
	while(i.hasNext()) {
	    Object o = i.next();
	    hc += o.hashCode();
	}
	return hc;
    }
}
