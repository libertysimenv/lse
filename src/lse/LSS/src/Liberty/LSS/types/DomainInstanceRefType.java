package Liberty.LSS.types;

import java.util.*;
import Liberty.LSS.*;

public class DomainInstanceRefType extends Type
    implements AggregateType
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new DomainInstanceRefType());

    private LinkedHashMap elementTypes;

    public DomainInstanceRefType() {
	elementTypes = new LinkedHashMap();

	elementTypes.put("class", Types.stringType);
	elementTypes.put("buildArgs", Types.stringType);
	elementTypes.put("inst", Types.stringType);
	elementTypes.put("name", Types.stringType);
	elementTypes.put("runArgs", Types.stringType);
    }
    
    public boolean isAggregate() {
	return true;
    }

    public Type getSubValueType(String field) 
	throws TypeException {
	if(elementTypes.containsKey(field)) {
	    return (Type)elementTypes.get(field);
	} else {
	    throw new TypeException("Element " + field + 
				    " is not a member of " +
				    "struct of type " + this);
	}
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
	return new DomainInstanceRefTypeLValue(base);
    }

    public String toString() {
	return "domain ref";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof DomainInstanceRefType) {
	    return true;
	} else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public String getBackendType() {
	return "LSE_domain_t";
    }
}

class DomainInstanceRefTypeLValue implements AggregateLValue
{
    private LValue base;
    
    public DomainInstanceRefTypeLValue(LValue base) {
	Type baseType = base.getType();
	while(!baseType.isBaseType())
	    baseType = baseType.getBaseType();

	if(!baseType.equals(Types.domainInstanceRefType)) {
	    throw new RuntimeTypeException("Cannot create an LValue for " +
					   "data of type " + 
					   Types.domainInstanceRefType + 
					   " using a backing store of type " + 
					   base.getType());
	}
	this.base = base;
    }

    public Type getType() {
	return Types.domainInstanceRefType;
    }

    public Value getValue() 
	throws UninitializedVariableException {
	return base.getValue();
    }

    public void assign(Value v) 
	throws TypeException {
	base.assign(v);
    }
    
    public void assign(Value v, CodeObtainable s)
	throws TypeException {
	base.assign(v,s);
    }

    public LValue getSubLValue(String name) 
	throws TypeException {
	throw new TypeException("Cannot assign to field " +
				name + " on a " + getType());

    }
}
