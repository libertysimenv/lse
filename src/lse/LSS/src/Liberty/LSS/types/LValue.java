package Liberty.LSS.types;

import Liberty.LSS.CodeObtainable;

public interface LValue
{
    public void assign(Value v) throws TypeException;
    /* For deferred error reporting */
    public void assign(Value v, CodeObtainable s) throws TypeException; 

    public Type getType();
    public Value getValue() throws UninitializedVariableException;
}
