package Liberty.LSS.types;

/* If a type implements this interface, then
 * isAggregate() should be true */

public interface AggregateType {
    public Type getSubValueType(String name) 
	throws TypeException,
	       UninitializedVariableException;
}
