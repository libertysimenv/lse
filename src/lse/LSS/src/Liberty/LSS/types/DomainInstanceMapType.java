package Liberty.LSS.types;

import Liberty.LSS.*;
import java.util.*;

/**
 * This class is used as the type for a default parameter that exists
 * on all LSS module instances.  The parameter is used to track the
 * default domain instance for a particular domain class.  It is
 * presented to the user as an aggregate type.  However, the fields of
 * the aggregate type may change depending on what domain classes are
 * registered.  Thus, the {@link Set} that is passed into the
 * constructor may be changed to introduce and remove fields in values
 * of this type.
 */

public class DomainInstanceMapType extends Type 
    implements AggregateType
{
    protected Set domainClasses;
    
    /**
     * Creates an instance of this type.  This type will store the
     * {@link Set} passed as an argument.  Therefore changes to that
     * set will be reflected in this type.  It is generally a bad idea
     * to remove items from the set, but adding items is okay.
     *
     * @param domainClasses a set of {@link String}s which define the
     *                      valid fields of this aggregate.
     */
    public DomainInstanceMapType(Set domainClasses) {
	this.domainClasses = domainClasses;
    }

    /**
     * Identifies this type as an aggregate.
     *
     * @return true
     */
    public boolean isAggregate() {
	return true;
    }

    /**
     * Identifies the type of subfields.  If the subfield exists, its
     * type is {@link Types.domainInstanceRefType}.
     *
     * @param  name  the name of the field
     * @return {@link Types.domainInstanceRefType} if the name exists
     */
    public Type getSubValueType(String name) 
	throws TypeException,
	       UninitializedVariableException {
	if(domainClasses.contains(name)) {
	    return Types.domainInstanceRefType;
	} else {
	    throw new TypeException("Element " + name + 
				    " is not a member of " +
				    "type " + this);
	}
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
	return new DomainInstanceMapLValue(this, base);
    }

    public String toString() {
	return "DomainInstanceMap" + domainClasses;
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof DomainInstanceMapType) {
	    Set s2 = ((DomainInstanceMapType)t2).getDomainClasses();
	    return domainClasses.equals(s2);
	} else {
	    return false;
	}
    }

    public int hashCode() {
	return domainClasses.hashCode();
    }

    public Set getDomainClasses() {
	return domainClasses;
    }
}

class DomainInstanceMapLValue implements AggregateLValue
{
    private DomainInstanceMapType type;
    private LValue base;
    
    public DomainInstanceMapLValue(DomainInstanceMapType ty, LValue base) {
	Type baseType = base.getType();
	while(!baseType.isBaseType())
	    baseType = baseType.getBaseType();

	if(baseType.equals(ty)) {
	    this.type = ty;
	    this.base = base;
	} else {
	    throw new RuntimeTypeException("Cannot create an LValue for " +
					   "data of type " + ty + 
					   " using a backing store of type " + 
					   base.getType());
	}
    }

    public Type getType() {
	return type;
    }

    public Value getValue() 
	throws UninitializedVariableException {
	return base.getValue();
    }

    public void assign(Value v) 
	throws TypeException {
	base.assign(v);
    }
    
    public void assign(Value v, CodeObtainable s)
	throws TypeException {
	base.assign(v,s);
    }

    public LValue getSubLValue(String name) 
	throws TypeException {
	LValue lv = new DomainInstanceMapFieldLValue(name, this);
	Type fieldType = lv.getType();
	return fieldType.getLValue(lv);
    }
}

class DomainInstanceMapFieldLValue implements LValue
{
    private LValue base;
    private String field;
    private Type type;

    public DomainInstanceMapFieldLValue(String field, LValue base) 
	throws TypeException {
	this.base = base;
	this.field = field;
	try {
	    this.type = ((AggregateType)base.getType()).getSubValueType(field);
	} catch(UninitializedVariableException e) {
	    /* What the #!?... */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public Type getType() {
	return type;
    }

    public Value getValue() 
	throws UninitializedVariableException {

	Aggregate baseValue = (Aggregate)base.getValue();
	
	try {
	    return baseValue.getSubValue(field);
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(base + "." + field + 
						     " is not initialized");
	} catch(TypeException e) {
	    /* We've already ensured the field exists, so... */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    protected Value constructValue(Value fieldValue, CodeObtainable src) 
	throws TypeException {
	DomainInstanceMapValue newValue;
	HashMap values;

	/* Get the current value if it exists */
	try { 
	    DomainInstanceMapValue old = 
		(DomainInstanceMapValue)base.getValue();
	    newValue = new DomainInstanceMapValue(old);
	} catch(UninitializedVariableException e) {
	    DomainInstanceMapType baseType = 
		(DomainInstanceMapType)base.getType();
	    newValue = new DomainInstanceMapValue(baseType, new HashMap());
	}

	fieldValue = Coerce.assignCoerce(fieldValue, type, src);
	try {
	    newValue.setSubValue(field, (DomainInstanceRefValue)fieldValue);
	    return newValue;
	} catch(TypeException e) {
	    /* Can't happen */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public void assign(Value v) 
	throws TypeException {
	Value nv = constructValue(v, null);
	base.assign(nv);
    }

    public void assign(Value v, CodeObtainable s)
	throws TypeException {
	Value nv = constructValue(v, s);
	base.assign(nv, s);
    }
}
