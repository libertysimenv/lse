package Liberty.LSS.types;

import java.util.*;

public class FunctionType extends Type 
    implements CallableType
{
    private Type[] arguments;
    private Type  returnType;
    private Set dependencySet = null;

    public FunctionType(Vector a, Type rt) {
	if(a == null || a.size() == 0)
	    arguments = null;
	else {
	    arguments = new Type[a.size()];
	    arguments = (Type[])a.toArray(arguments);
	}
	returnType = rt;
    }

    public String toString() {
	String args;
	if(arguments != null) {
	    args = arguments[0].toString();
	    for(int i = 1; i < arguments.length; i++) {
		args += ", " + arguments[i].toString();
	    }
	} else 
	    args = "";

	if(arguments != null && arguments.length == 1)
	    return args + " => " + 
		returnType.toString();
	else
	    return "(" + args + ")" + " => " + 
		returnType.toString();

    }

    public int getNumArguments() {
	if(arguments == null) return 0;
	return arguments.length;
    }

    protected Type[] getArguments() {
	return arguments;
    }

    public Type getArgumentType(int index) 
	throws TypeException {
	if(arguments != null && index < arguments.length)
	    return arguments[index];
	else
	    throw new TypeException(this + " has no argument number " + index);
    }

    public Type getReturnType() {
	return returnType;
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof FunctionType) {
	    FunctionType ft2 = (FunctionType)t2;
	    Type[] arguments2 = ft2.getArguments();
	    if(!returnType.equals(ft2.getReturnType()))
		return false;
	    if(arguments == null && arguments2 == null)
		return true;
	    if((arguments == null && arguments2 != null ||
		arguments != null && arguments2 == null))
		return false;
	    if(arguments.length != arguments2.length)
		return false;
	    for(int i = 0; i < arguments.length; i++)
		if(!arguments[i].equals(arguments2[i]))
		    return false;
	    return true;
	} else
	    return false;
    }

    public int hashCode() {
	int hc = returnType.hashCode();
	if(arguments != null)
	    for(int i = 0; i < arguments.length; i++) {
		hc += arguments[i].hashCode();
	    }
	return hc;
    }

    public boolean isCallable() {
	return true;
    }

    public Set dependsOn() {
	if(dependencySet == null) {
	    if(arguments != null) {
		dependencySet = new HashSet(arguments.length+1,1);
		for(int i = 0; i < arguments.length; i++)
		    dependencySet.add(arguments[i]);
	    } else {
		dependencySet = new HashSet(1);
	    }
	    dependencySet.add(returnType);
	}
	return dependencySet;
    }
}

