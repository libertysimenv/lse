package Liberty.LSS.types;

public class RuntimeVarRefType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new RuntimeVarRefType());

    public String toString() {
	return "runtime_var ref";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof RuntimeVarRefType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}

