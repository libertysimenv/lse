package Liberty.LSS.types;

import Liberty.LSS.AST.Parameter;
import Liberty.LSS.*;

public class ParameterRefType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new ParameterRefType());

    public String toString() {
	return "parameter ref";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof ParameterRefType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public boolean isReference() {
	return true;
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
	return new ParameterLValue(base);
    }

    public LValue getLValue(Value base) 
	throws TypeException {
	return new ParameterLValue(base);
    }
}

class ParameterLValue implements ReferenceLValue
{
    private LValue base;
    private Value parm;

   /* Invariants:

       1) parm <: ParameterRef 
       2) ParameterRef <: base.type (since base will be asked to store
                                     ParameterRef's)
       3) base.type <: ParameterRef (since parm = base.value)
       2 + 3 => base.type == ParameterRef
    */

    public ParameterLValue(LValue b) {
	Type t = b.getType();
	while(!t.isBaseType())
	    t = t.getBaseType();

	if(t.equals(Types.parameterRefType)) {
	    base = b;
	    try {
		parm = b.getValue();
	    } catch(UninitializedVariableException e) {
		parm = null;
	    }
	} else {
	    throw new RuntimeTypeException("Cannot create a parameter " + 
					   "lvalue with data of type " + t);
	}
    }

    public ParameterLValue(Value p) {
	/* LValues produced with this constructor have no backing
	 * store.  Therefore, they are effectively const variables.
	 * Any assignment directly to this LValue will throw an
	 * exception, unless a deref is implied*/

	if(TypeRelations.isSubtype(p.getType(),Types.parameterRefType)) {
	    parm = p;
	    base = null;
	} else {
	    throw new RuntimeTypeException("Cannot create a parameter " +
					   "lvalue with data of type " + 
					   p.getType());
	}
    }

    public Type getType() {
	if(base != null) return base.getType();
	else return new ConstType(Types.parameterRefType);
    }

    public Value getValue() 
	throws UninitializedVariableException {
	if(parm == null) {
	    throw new UninitializedVariableException("Parameter ref " +
						     "uninitialized");
	}
	return parm;
    }

    public void assign(Value v) 
	throws TypeException {
	assign(v,null);
    }

    public void assign(Value v, CodeObtainable s) 
	throws TypeException {
	try { 
	    if(base != null && 
	       TypeRelations.isCompatible(v.getType(), 
					  Types.parameterRefType)) {
		base.assign(v,s);
		parm = v;
		return;
	    }
	} catch(TypeException ignore) {
	    /* Do nothing... we'll try something else. */
	}

	/* Try dereferencing the parm and assigning... */
	try {
	    deref().assign(v,s);
	} catch(UninitializedVariableException e) {
	    throw new TypeException(e.getMessage());
	}
    }

    public LValue deref() 
	throws TypeException, 
	       UninitializedVariableException {
	if(parm == null) {
	    throw new UninitializedVariableException("Parameter ref " + 
						     "uninitialized");
	} else {
	    if(parm.getType().equals(Types.parameterRefType)) {
		return ((Parameter)parm).getLValue();
	    } else if(parm.getType().isReference()) {
		ReferenceLValue lv = 
		    (ReferenceLValue)parm.getType().getLValue(parm);
		return lv.deref();
	    } else {
		throw new TypeException("Cannot dereference lvalue " + parm);
	    }
	}
    }
}

