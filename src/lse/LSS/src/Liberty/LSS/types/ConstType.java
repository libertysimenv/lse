package Liberty.LSS.types;

public class ConstType extends ModifiedType
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new ConstType(null));

    public ConstType(Type bt) {
	super(bt);
    }

    public String toString() {
	return "const " + baseType.toString();
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof ConstType)
	    return super.equals(t2);
	else
	    return false;
    }

    public int hashCode() {
	return getBaseType().hashCode() + memoizedHashCode;
    }

    public String getBackendType() {
	return "const " + getBaseType().getBackendType();
    }
}

