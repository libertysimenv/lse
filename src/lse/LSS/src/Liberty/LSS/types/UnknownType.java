package Liberty.LSS.types;

import Liberty.LSS.*;
import Liberty.LSS.AST.AssignmentLogEntry;
import java.util.*;

/****************************** NOTE ******************************/
/* Nothing should be a subtype of UnknownType.  If something is a */
/* subtype, bad things will happen                                */
/******************************************************************/

public class UnknownType extends Type
    implements AggregateType, IndexableType
{
    /* Used to calculate the hashCode for this type */
    private static final int memoizedHashCode = 
	System.identityHashCode(new UnknownType());


    public String toString() {
	return "(unknown)";
    }

    public boolean isIndexable() {
	return true;
    }

    public boolean isAggregate() {
	return true;
    }

    public Type getSubValueType(int index) {
	return Types.unknownType;
    }

    public Value createIndexableValue(Vector v)
	throws TypeException {
	throw new TypeException("Cannot create unknown from vector");    
    }

    public Type getSubValueType(String field) {
	return Types.unknownType;
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
	throw new RuntimeTypeException("Unknowns cannot have a backing store");
    }

    public LValue getLValue(Value base) 
	throws TypeException {
	return new UnknownLValue(base);
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof UnknownType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}

class UnknownLValue implements AggregateLValue, IndexableLValue
{
    private UnknownValue var;

    public UnknownLValue(Value v) {
	if(Types.unknownType.equals(v.getType())) {
	    var = (UnknownValue)v;
	} else {
	    throw new RuntimeTypeException("Cannot create an unknown lvalue " +
					   "with data of type " + v.getType());
	}
    }

    private void assign(AssignmentLogEntry e) 
	throws TypeException {
	var.getInstance().addAssignment(e);
    }
    
    public void assign(Value v) 
	throws TypeException {
	assign(new AssignmentLogEntry(var, v));
    }

    public void assign(Value v, CodeObtainable s) 
	throws TypeException {
	assign(new AssignmentLogEntry(var, v, s));
    }
    
    public Type getType() {
	return Types.unknownType;
    }

    public Value getValue() {
	return var;
    }

    public LValue getSubLValue(String field) {
	return new UnknownLValue(var.getSubValue(field));
    }

    public LValue getSubLValue(int index) {
	return new UnknownLValue(var.getSubValue(index));
    }

    public String toString() {
	return var.toString();
    }
}

