package Liberty.LSS.types;

public class ExternalValue extends ValueBase {
    String value;

    public ExternalValue(Type type, String v) {
	super(type);
	value = v;
    }

    public String getValue() {
	return value;
    }

    public String toString() {
	return value;
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	if(v instanceof ExternalValue) {
	    String strv = ((ExternalValue)v).getValue();
	    return (strv == value);
	} else 
	    return false;
    }

    public int hashCode() {
	return value.hashCode() + this.getType().hashCode();
    }
}
