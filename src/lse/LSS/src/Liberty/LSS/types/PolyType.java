package Liberty.LSS.types;

public abstract class PolyType extends Type
{
    public LValue getLValue(LValue base) 
	throws TypeException {
	throw new TypeException("Type " + this + " is not an lvalue");
    }

    public boolean isPolymorphic() {
	return true;
    }
}
