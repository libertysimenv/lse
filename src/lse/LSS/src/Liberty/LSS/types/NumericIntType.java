package Liberty.LSS.types;

public class NumericIntType extends IntType
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new NumericIntType());

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof NumericIntType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}

