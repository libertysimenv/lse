package Liberty.LSS.types.inference;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import java.util.*;

public class TypeInferenceEngine
{
    public static void inferTypes(Set constraints, Map typeVariableContext)
	throws TypeInferenceException {
	realInferTypes(constraints, typeVariableContext);

	/* Propagate the type variable definitions */
	propagateVariables(typeVariableContext);

	/* Ensure the system is not underconstrained */
	Set underConstrainedVars = checkUnderconstraints(typeVariableContext);
	if(underConstrainedVars.size() != 0) {
	    throw 
		UnderConstrainedException.create(underConstrainedVars);
	}
    }

    public static void realInferTypes(Set constraints, Map typeVariableContext)
	throws TypeInferenceException {
	Set oldConstraints;

	do {
	    phase1(constraints, typeVariableContext);
	    oldConstraints = new HashSet(constraints);
	    phase2(constraints, typeVariableContext);
	} while(!oldConstraints.equals(constraints));
	phase3(constraints, typeVariableContext);
    }


    public static void propagateVariables(Map typeVariableContext) {
	/* Propagate all the values from some type variable
	 * assignments to other type variable assignments */
	boolean changes;
	do {
	    changes = false;
	    Map newTypeVariableContext = new HashMap();
	    Set entries = typeVariableContext.entrySet();
	    Iterator i = entries.iterator();
	    while(i.hasNext()) {
		Map.Entry entry = (Map.Entry)i.next();
		Type t = (Type)entry.getValue();
		Type tprime;
		tprime = t.substitute(typeVariableContext);
		if(!tprime.equals(t)) changes = true;
		newTypeVariableContext.put(entry.getKey(), tprime);
	    }
	    typeVariableContext.putAll(newTypeVariableContext);
	} while(changes);
    }

    public static void substituteAll(Set constraints, 
				     Map typeVariableContext) {
	Collection newConstraints = new LinkedList();
	Iterator i = constraints.iterator();
	while(i.hasNext()) {
	    Constraint c = (Constraint)i.next();
	    Constraint cprime;
	    cprime = c.substitute(typeVariableContext);
	    
	    if(!cprime.equals(c)) {
		i.remove();
		newConstraints.add(cprime);
	    }
	}

	// We must side-effect constraints
	constraints.addAll(newConstraints);
    }

    public static void phase1(Set constraints, Map typeVariableContext)
	throws TypeInferenceException {
	Iterator i = constraints.iterator();

	while(i.hasNext()) {
	    Constraint c = (Constraint)i.next();
	    if(c.isSimple()) {
		ConstraintSatisfaction s = 
		    c.isSatisfiable();
		if(!s.isSatisfiable()) {
		    ConstraintSource o = c.getOrigin();
		    throw new 
			OverConstrainedException(c, "The constraint from " + 
						 o.getIdentifierString() + 
						 " causes the system to be " + 
						 "overconstrained.  " + 
						 "Constraint " + c + 
						 " cannot be satisfied");
		}
		
		i.remove();
		/* Case 1 - satisfaction predicates are empty */
		if(s.getConstraints().isEmpty())
		    continue;

		/* Case 2 - satisfaction predicates is just this constraint */
		if(s.getConstraints().size() == 1) {
		    Iterator j = s.getConstraints().iterator();
		    Constraint c2 = (Constraint)j.next();
		    if(c.equals(c2)) {
			Type[] pair = c.getPair();
			TypeVariable var;
			Type value;

			if(pair[0] instanceof TypeVariable) {
			    var = (TypeVariable)pair[0];
			    value = pair[1];
			} else {
			    var = (TypeVariable)pair[1];
			    value = pair[0];
			}
			typeVariableContext.put(var,value);
			substituteAll(constraints, typeVariableContext);
			// Start the loop over with the new constraints
			i = constraints.iterator();
			continue;
		    }
		}

		/* Case 3 - complex satisfaction predicates */
		constraints.addAll(s.getConstraints());
		// Start the loop over with the new constraints
		i = constraints.iterator();
	    }
	}
    }

    public static void phase2(Set constraints, Map typeVariableContext) 
	throws TypeInferenceException {
	Iterator i;
	Set newConstraints;

	do {
	    i = constraints.iterator();
	    newConstraints = new HashSet();

	    while(i.hasNext()) {
		Constraint c = (Constraint)i.next();
		if(!c.isSimple()) {
		    ConstraintSatisfaction s = 
			c.isSatisfiable();
		    if(!s.isSatisfiable()) {
			ConstraintSource o = c.getOrigin();
			throw new 
			    OverConstrainedException(c, "The constraint from " + 
						     o.getIdentifierString() +
						     " causes the system to be " + 
						     "overconstrained.  " + 
						     "Constraint " + c + 
						     " cannot be satisfied");
		    } else {
			Set ncs = s.getConstraints();
			if(ncs.size() == 1) {
			    Constraint nc = (Constraint)ncs.iterator().next();
			    if(!nc.equals(c)) {
				i.remove();
				newConstraints.addAll(ncs);
			    }
			} else {
			    i.remove();
			    newConstraints.addAll(s.getConstraints());
			}
		    }
		}
	    }
	    constraints.addAll(newConstraints);
	} while(newConstraints.size() != 0);
    }

    public static void phase3(Set constraints, Map typeVariableContext) 
	throws TypeInferenceException {
	/* Partition the problem into independent pieces */
 	Set constraintSets = partitionConstraints(constraints);

 	/* Run the remainder of the inference problem on each piece */
 	Iterator i = constraintSets.iterator();
 	while(i.hasNext()) {
 	    Set s = (Set)i.next();
 	    phase3set(s, typeVariableContext);
 	}
    }

    public static void phase3set(Set constraints, Map typeVariableContext) 
	throws TypeInferenceException {
	
	Iterator i = constraints.iterator();
	Set newConstraints = new HashSet();

	Map completeTypeVariableContext = null;
	Vector overConstrainedExceptions = new Vector();

	while(i.hasNext()) {
	    Constraint c = (Constraint)i.next();
	    if(c.isDecision()) {
		Type[] pair = c.getPair();
		UnionType ut;
		Type other;
		if(pair[0] instanceof UnionType) {
		    ut = (UnionType)pair[0];
		    other = pair[1];
		} else {
		    ut = (UnionType)pair[1];
		    other = pair[0];
		}

		List alternatives = ut.getOptions();
		Iterator a = alternatives.iterator();
		int count = 0;
		while(a.hasNext()) {
		    Type choice = (Type)a.next();
		    Map subTypeVariableContext = 
			new HashMap(typeVariableContext);

		    Set subConstraints = new HashSet(constraints);
		    subConstraints.remove(c);
		    subConstraints.add(new Constraint(choice, other, 
						      c.getOrigin()));
		    
		    try {
			realInferTypes(subConstraints, subTypeVariableContext);

			count ++;
			if(count == 1) {
			    completeTypeVariableContext = 
				subTypeVariableContext;
			}
			if(count > 1) {
			    Set diffs = 
				checkEquivalence(completeTypeVariableContext,
						 subTypeVariableContext);
			    if(diffs.size() > 0) {
				throw 
				    UnderConstrainedException.create(diffs);
			    }
			}
		    } catch(OverConstrainedException e) {
			/* okay, this was a bad choice, throw it out */
			overConstrainedExceptions.add(e);
		    }
		}
		if(count > 0) {
		    typeVariableContext.putAll(completeTypeVariableContext);
		} else {
		    Set overConstraints = new HashSet();
		    String message;
		    message = "The system is overconstrained, One or more " + 
			"of the following constraints must be " + 
			"satisfiable:\n";

		    Iterator o = overConstrainedExceptions.iterator();
		    int condNum = 1;
		    while(o.hasNext()) {
			OverConstrainedException e = 
			    (OverConstrainedException)o.next();
			Set s = e.getConstraintSet();
			Iterator j = s.iterator();

			while(j.hasNext()) {
			    Constraint errorc = (Constraint)j.next();
			    overConstraints.add(errorc);
			    message += condNum + ") constraint " + errorc + 
				" from " + 
				errorc.getOrigin().getIdentifierString();
			    if(j.hasNext()) message += "\n";
			    condNum++;
			}
		    }
		    throw new OverConstrainedException(overConstraints,
						       message);
		}
		return;
	    }
	}
    }

    protected static Set partitionConstraints(Set constraints) {
	Iterator ci = constraints.iterator();
		
	/* Calculate an adjacency matrix */
	int h = 0, k = 0;
	boolean[][] adjacencyMatrix = 
	    new boolean[constraints.size()][constraints.size()];
	Constraint[] constraintArray = new Constraint[constraints.size()];
	Set[] typeVarSets = new HashSet[constraints.size()];
	
	while(ci.hasNext()) {
	    Constraint c = (Constraint)ci.next();
	    constraintArray[k] = c;
	    typeVarSets[k] = c.getTypeVarSet();

	    for(h = 0; h < k; h++) {
		Set intersection = new HashSet(typeVarSets[k]);
		intersection.retainAll(typeVarSets[h]);
		adjacencyMatrix[h][k] = (intersection.size() != 0);
		adjacencyMatrix[k][h] = (intersection.size() != 0);
	    }
	    k++;
	}
	
	/* Do a depth first traversal of the adjacency matrix */
	boolean[] visited = new boolean[constraints.size()];
	Set constraintSets = new HashSet();
	for(k = 0; k < visited.length; k++)
	    visited[k] = false;
	
	for(k = 0; k < visited.length; k++) {
	    if(!visited[k]) {
		Set s = depthFirstSearch(adjacencyMatrix, visited, 
					 constraintArray, k);
		constraintSets.add(s);
	    }
	}
	
	return constraintSets;
    }

    protected static Set depthFirstSearch(boolean[][] adjacencyMatrix,
				       boolean[] visited,
				       Constraint[] constraints,
				       int index) {
	if(visited[index]) return null;

	int numNodes = visited.length;
	Set s = new HashSet();
	s.add(constraints[index]);
	visited[index] = true;

	for(int i = 0; i < numNodes; i++) {
	    if(i == index) continue;
	    if(visited[i]) continue;
	    if(!adjacencyMatrix[index][i]) continue;
	    s.addAll(depthFirstSearch(adjacencyMatrix, 
				      visited, 
				      constraints, i));
	}
	return s;
    }

    protected static Set checkEquivalence(Map typeVariableContext1,
					  Map typeVariableContext2) {

	/* Type variables that start with ''' are considered
	 * insignificant.  Thus, this checks for equivalence
	 * disregarding those type variables. */
	
	/* We assume that any keys that are in typeVariableContext2
	 * that are not in typeVariableContext1 are insignificant */

	Type t1, t2;
	Iterator i = typeVariableContext1.entrySet().iterator();
	Set differences = new HashSet();

	while(i.hasNext()) {
	    Map.Entry entry = (Map.Entry)i.next();
	    TypeVariable var = (TypeVariable)entry.getKey();
	    if(!var.toString().startsWith("'''")) {
		t1 = (Type)entry.getValue();
		t2 = (Type)typeVariableContext2.get(entry.getKey());

		if(!t1.equals(t2)) {
		    if(var.mustResolve())
			differences.add(var);
		}
	    }
	}
	return differences;
    }

    public static void constrainType(Map typeVariableContext,
				     Map newMappings,
				     Type t) {
	if(!t.isPolymorphic()) return;

	if(t instanceof TypeVariable) {
	    if(newMappings.containsKey(t) ||
	       typeVariableContext.containsKey(t))
		return;
	    newMappings.put(t, Types.intType);
	} else {
	    Set s = t.dependsOn();
	    Iterator i = s.iterator();
	    while(i.hasNext()) {
		t = (Type)i.next();
		constrainType(typeVariableContext, newMappings, t);
	    }
	}
    }

    public static Set checkUnderconstraints(Map typeVariableContext) {
	/* Type variables that start with ''' are considered
	 * insignificant.  Thus, this checks for underconstraint
	 * in all other type variables. */
	
	Type t;
	Iterator i = typeVariableContext.entrySet().iterator();
	Set underConstraints = new HashSet();

	Map newMappings = new HashMap();

	while(i.hasNext()) {
	    Map.Entry entry = (Map.Entry)i.next();
	    TypeVariable var = (TypeVariable)entry.getKey();
	    if(!var.toString().startsWith("'''")) {
		t = (Type)entry.getValue();
		if(t.isPolymorphic()) {
		    if(var.mustResolve()) {
			underConstraints.add(var);
		    } else {
			constrainType(typeVariableContext, newMappings, t);
		    }
		}
	    }
	}

	typeVariableContext.putAll(newMappings);
	propagateVariables(typeVariableContext);
	return underConstraints;
    }

}
