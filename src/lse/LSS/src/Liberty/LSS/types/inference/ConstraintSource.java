package Liberty.LSS.types.inference;

public interface ConstraintSource
{
    public String getIdentifierString();
}
