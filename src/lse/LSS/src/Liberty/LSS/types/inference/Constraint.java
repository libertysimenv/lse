package Liberty.LSS.types.inference;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import java.util.*;

public class Constraint {
    private Type[] pair;
    private ConstraintSource origin;

    public Constraint(Type a, Type b, ConstraintSource s) {
	pair = new Type[2];
	pair[0] = a;
	pair[1] = b;
	origin = s;
    }

    public Type[] getPair() {
	return pair;
    }

    public ConstraintSource getOrigin() {
	return origin;
    }

    public boolean isTrivial() {
	return (!pair[0].isPolymorphic() && !pair[1].isPolymorphic());
    }

    public boolean isSimple() {
	if((pair[0] instanceof TypeVariable && 
	    !(pair[1] instanceof UnionType)) ||
	   (pair[1] instanceof TypeVariable && 
	    !(pair[0] instanceof UnionType)))
	    return true;
	return false;
    }

    public boolean isDecision() {
	if(pair[0] instanceof UnionType || pair[1] instanceof UnionType)
	    return true;
	return false;
    }

    public ConstraintSatisfaction isSatisfiable() {
	ConstraintSatisfaction s1 = pair[0].canBeEqual(pair[1], origin);
	ConstraintSatisfaction s2 = pair[1].canBeEqual(pair[0], origin);
	/* Do they agree? */
	if(s1.isSatisfiable() && s2.isSatisfiable()) {
	    if(!(s1.getConstraints().equals(s2.getConstraints())))
		throw new RuntimeTypeException(pair[0] + " and " + pair[1] +
					       " disagree on whether they " + 
					       "can be equal");
	    return s1;
	}

	if(s2.isSatisfiable()) return s2;
	return s1;
    }

    public String toString() {
	return pair[0].toString() + " = " + pair[1].toString();
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(o instanceof Constraint) {
	    Type[] pair2 = ((Constraint)o).getPair();
	    boolean co = (pair[0].equals(pair2[0]) &&
			  pair[1].equals(pair2[1]));
	    boolean contra = (pair[0].equals(pair2[1]) &&
			      pair[1].equals(pair2[0]));
	    return (co || contra);
	} else {
	    return false;
	}
    }

    public int hashCode() {
	return pair[0].hashCode() + 
	    pair[1].hashCode();
    }

    public Constraint substitute(Map typeVarValues) {
	Type p0, p1;
	p0 = pair[0].substitute(typeVarValues);
	p1 = pair[1].substitute(typeVarValues);
	return new Constraint(p0,p1,origin);
    }

    public static Set getTypeVarSet(Type t) {
	Set s = new HashSet();
	Set depends = t.dependsOn();
	Iterator i = depends.iterator();
	while(i.hasNext()) {
	    Type dt = (Type)i.next();
	    if(dt instanceof TypeVariable)
		s.add(dt);
	    else if(dt.isPolymorphic())
		s.addAll(getTypeVarSet(dt));
	}
	return s;
    }

    public Set getTypeVarSet() {
	Set s = getTypeVarSet(pair[0]);
	s.addAll(getTypeVarSet(pair[1]));
	return s;
    }
}
