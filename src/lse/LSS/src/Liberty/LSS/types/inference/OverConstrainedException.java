package Liberty.LSS.types.inference;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

import Liberty.LSS.LSSException;

public class OverConstrainedException extends TypeInferenceException {
    /* Set of constraints of which at least one can't be satisfied */
    private Set constraints;

    public OverConstrainedException(Constraint c) {
	super();
	constraints = new HashSet();
	constraints.add(c);
    }

    public OverConstrainedException(Constraint c, String msg) {
	super(msg);
	constraints = new HashSet();
	constraints.add(c);
    }

    public OverConstrainedException(Set c, String msg) {
	super(msg);
	constraints = c;
    }

    public Set getConstraintSet() {
	return constraints;
    }
}
