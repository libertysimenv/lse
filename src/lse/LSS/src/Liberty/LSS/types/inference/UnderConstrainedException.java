package Liberty.LSS.types.inference;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import java.util.*;

import Liberty.LSS.LSSException;

public class UnderConstrainedException extends TypeInferenceException {
    public Set underConstrainedVars;

    public UnderConstrainedException(Set s) {
	super();
	underConstrainedVars = s;
    }

    public UnderConstrainedException(Set s, String msg) {
	super(msg);
	underConstrainedVars = s;
    }

    public static UnderConstrainedException create(Set s) {
	String message;
	message = "The following type variables are " + 
	    "underconstrained: ";
	Iterator i = s.iterator();
	while(i.hasNext()) {
	    message += i.next();
	    if(i.hasNext()) 
		message += ", ";
	}
	return new UnderConstrainedException(s, message);
    }
}
