package Liberty.LSS.types.inference;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import java.util.*;

public class ConstraintSatisfaction {
    private boolean satisfiable;
    private Set predicates;

    public ConstraintSatisfaction(boolean s, Set p) {
	satisfiable = s;
	predicates = p;
    }

    public boolean isSatisfiable() {
	return satisfiable;
    }

    public Set getConstraints() {
	return predicates;
    }
} 
