package Liberty.LSS.types.inference;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.LSSException;

public class TypeInferenceException extends LSSException {
    public TypeInferenceException() {
	super();
    }

    public TypeInferenceException(String msg) {
	super("Error: " + msg);
    }
}
