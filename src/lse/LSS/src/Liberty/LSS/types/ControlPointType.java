package Liberty.LSS.types;

public class ControlPointType extends Type
{
    /* Used to calculate the hashCode for this type */
    private static final int memoizedHashCode = 
	System.identityHashCode(new ControlPointType());

    public String toString() {
	return "controlpoint";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof ControlPointType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}
