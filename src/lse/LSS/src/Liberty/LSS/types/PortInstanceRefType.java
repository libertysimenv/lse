package Liberty.LSS.types;

public class PortInstanceRefType extends Type 
    implements AggregateType
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new PortInstanceRefType());

    public String toString() {
	return "(port instance ref)";
    }

    public boolean isAggregate() {
	return true;
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
	throw new TypeException("Port instances are not lvalues");
    }

    public Type getSubValueType(String field) 
	throws TypeException,
	       UninitializedVariableException {
	if(field.equals("connected"))
	   return Types.boolType;
	else
	    throw new TypeException(field + 
				    " is not a valid attribute on type " + 
				    this);
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof PortInstanceRefType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}

