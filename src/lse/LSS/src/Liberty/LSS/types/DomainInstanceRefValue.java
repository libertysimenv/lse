package Liberty.LSS.types;

import java.util.*;

public class DomainInstanceRefValue extends ValueBase implements Aggregate {
    private Map values;

    public DomainInstanceRefValue(String dclass, 
				  StringValue buildArgs, 
				  String instance, 
				  StringValue name, 
				  StringValue runArgs) {
	super(Types.domainInstanceRefType);

	values = new LinkedHashMap();
	values.put("class", new StringValue(dclass));
	values.put("buildArgs", buildArgs);
	values.put("inst", new StringValue(instance));
	values.put("name", name);
	values.put("runArgs", runArgs);
    }

    public Value getSubValue(String s) 
	throws TypeException {
	if(values.containsKey(s)) {
	    Value v = (Value)values.get(s);
	    return v;
	} else {
	    throw new TypeException("Field " + s + " is not a member of " +
				    "type " + Types.domainInstanceRefType);
	}
    }

    public String getClassName() {
	return ((StringValue)values.get("class")).getFlattenedValue();
    }

    public String getBuildArgs() {
	return ((StringValue)values.get("buildArgs")).getFlattenedValue();
    }

    public String getInstName() {
	return ((StringValue)values.get("inst")).getFlattenedValue();
    }

    public String getName() {
	return ((StringValue)values.get("name")).getFlattenedValue();
    }

    public String getRunArgs() {
	return ((StringValue)values.get("runArgs")).getFlattenedValue();
    }

    public String getQualifiedName() {
	String instName = getInstName();
	String name = getName();
	String qualName = 
	    ((instName.equals("")) ? "" : instName + ".") +  name;
	return qualName;
    }

    public String toCurlyString() {
	String ref = "LSEdi_ref(" + getQualifiedName() + ")";
	return ref;
    }

    public String toString() {
	Iterator i = values.entrySet().iterator();
	String elements = "";
	String name;
	boolean first = true;

	while(i.hasNext()) {
	    if(!first)
		elements += ", ";
	    first = false;
	    Map.Entry item = (Map.Entry)i.next();
	    name = (String)item.getKey();
	    elements += name + " = " + 
		(String)((StringValue)item.getValue()).getFlattenedValue();
	}
	return "{" + elements + "}";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(t.equals(Types.domainInstanceRefType)) {
	    DomainInstanceRefValue dirv = (DomainInstanceRefValue)v;
	    return values.equals(dirv.values);
	} else 
	    return false;
    }

    public int hashCode() {
	return values.hashCode();
    }
}
