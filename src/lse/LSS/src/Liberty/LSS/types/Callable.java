package Liberty.LSS.types;

import java.util.*;
import Liberty.LSS.*;
import Liberty.LSS.AST.*;

/* If a value implements this interface, then
 * value.getType().isCallable() should be true */

public interface Callable {
    public Value call(Vector argValues, CodeObtainable callSite) 
	throws UnimplementedException, 
	       TypeException,
	       UndefinedSymbolException,
	       FunctionException,
	       UninitializedVariableException,
	       ParseException;
}
