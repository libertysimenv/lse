package Liberty.LSS.types;

/* This class should be changed into an interface if there are
 * subtypes of controlpoint */

/* This class is not an interface because it inherits from String
 * and so does its subtype (actually its subtype is String) */

import java.util.*;

public class ControlPointValue extends StringValue {
    public ControlPointValue(String s) {
	super(s, Types.controlPointType);
    }

    public ControlPointValue(Vector v) {
	super(v, Types.controlPointType);
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.controlPointType)) {
	    Vector codev = ((ControlPointValue)v).getValue();
	    return (codev.equals(getValue()));
	} else 
	    return false;
    }

    public String toString() {
	return getFlattenedValue();
    }
}
