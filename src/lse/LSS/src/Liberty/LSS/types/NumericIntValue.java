package Liberty.LSS.types;

/* This class should be changed into an interface if there are
 * subtypes of int */

public class NumericIntValue extends ValueBase {
    long value;

    public NumericIntValue(long v) {
	super(Types.numericIntType);
	value = v;
    }

    public NumericIntValue(Long v) {
	this(v.longValue());
    }

    public NumericIntValue(Integer v) {
	this(v.intValue());
    }

    public long getValue() {
	return value;
    }

    public String toString() {
	return new Long(value).toString();
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.numericIntType)) {
	    long intv = ((NumericIntValue)v).getValue();
	    return (intv == value);
	} else 
	    return false;
    }

    public int hashCode() {
	return (new Long(value)).hashCode();
    }
}
