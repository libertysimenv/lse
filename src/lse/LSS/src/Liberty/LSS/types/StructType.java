package Liberty.LSS.types;

import java.util.*;
import Liberty.LSS.*;
import Liberty.LSS.types.inference.*;

public class StructType extends Type
    implements AggregateType
{
    private HashMap elementTypes;
    private HashSet elementNames;
    private LinkedList elementNamesOrder;
    private boolean poly;
    private Set dependencySet = null;

    public StructType(Vector t, Vector n) 
	throws TypeException {
	poly = false;
	if(t.size() != n.size()) {
	    throw new RuntimeTypeException("Cannot create struct type" +
					   " with different numbers of" +
					   " element types and names");
	}

	int size = t.size();
	elementTypes = new HashMap();
	elementNames = new HashSet();
	elementNamesOrder = new LinkedList();

	for(int i = 0; i < size; i++) {
	    String name = (String)n.get(i);
	    if(elementNames.contains(name)) {
		throw new TypeException("Cannot create struct type" +
					" with entry name " + 
					name + " duplicated");
	    } else {
		Type type = (Type)t.get(i);
		elementNames.add(name);
		elementNamesOrder.add(name);
		elementTypes.put(name, type);
		if(type.isPolymorphic()) poly = true;
	    }
	}
    }
    
    public boolean isAggregate() {
	return true;
    }

    public boolean isPolymorphic() {
	return poly;
    }

    public Type getSubValueType(String field) 
	throws TypeException {
	if(elementNames.contains(field)) {
	    return (Type)elementTypes.get(field);
	} else {
	    throw new TypeException("Element " + field + 
				    " is not a member of " +
				    "struct of type " + this);
	}
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
	if(isPolymorphic()) {
	    throw new TypeException("Type " + this + " is not an lvalue");
	}
	return new StructTypeLValue(this, base);
    }

    public String toString() {
	String elements = "";
	String name;
	Iterator i = elementNamesOrder.iterator();
	boolean first = true;
	while(i.hasNext()) {
	    if(!first)
		elements += " ";
	    first = false;
	    name = (String)i.next();
	    elements += name + ":" + elementTypes.get(name) + ";";
	}
    	return "struct ?? {" + elements + "} ??";
    }

    public Set getElementNames() {
	return elementNames;
    }

    public Type getElementType(String name) 
	throws TypeException {
	if(elementTypes.containsKey(name)) {
	    return (Type)elementTypes.get(name);
	} else {
	    throw new TypeException("Type " + this + " has no field named " +
				    name);
	}
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof StructType) {
	    StructType at2 = (StructType)t2;

	    /* Compare types including names but ignoring order */
	    return elementTypes.equals(at2.elementTypes);
	} else
	    return false;
    }

    public int hashCode() {
	return elementTypes.hashCode();
    }

    public String getBackendType(Map typesToNames) {
	String body = "";
	Type type;
	String backendType;
	String name;
	
	if(isPolymorphic())
	    return super.getBackendType();

	ArrayList elements = new ArrayList(elementNames);
	Collections.sort(elements);
	Iterator i = elements.iterator();
	while(i.hasNext()) {
	    name = (String)i.next();
	    type = (Type)elementTypes.get(name);
	    if(typesToNames.containsKey(type))
		backendType = ("LSEut_ref(" + 
			       (String)typesToNames.get(type) +
			       ")");
	    else
		backendType = type.getBackendType(typesToNames);
	    body += "(" + backendType + ") " + name + ",";
	}
	
	return "LSEut_structdef(" +
	    body + ")";
    }
    
    public String getBackendType() {
	return getBackendType(new HashMap());
    }

    public ConstraintSatisfaction canBeEqual(Type t, 
					     ConstraintSource origin) {
	if(!isPolymorphic()) {
	    return super.canBeEqual(t, origin);
	} else {
	    Set s = new HashSet();
	    if(t instanceof StructType) {
		StructType st = (StructType)t;
		
		/* Check to see if field names are the same */
		if(!elementNames.equals(st.elementNames)) {
		    return new ConstraintSatisfaction(false, s);
		}

		Iterator i = elementNamesOrder.iterator();
		while(i.hasNext()) {
		    String field = (String)i.next();
		    s.add(new Constraint((Type)elementTypes.get(field),
					 (Type)st.elementTypes.get(field), 
					 origin));
		}
		return new ConstraintSatisfaction(true, s);
	    } else {
		return new ConstraintSatisfaction(false, s);
	    }
	}
    }

    public Type substitute(Map typeVarValues) {
	if(isPolymorphic()) {
	    Iterator i = elementNamesOrder.iterator();
	    Vector names = new Vector();
	    Vector types = new Vector();
	    while(i.hasNext()) {
		String field = (String)i.next();
		Type t = (Type)elementTypes.get(field);
		t = t.substitute(typeVarValues);
		names.add(field);
		types.add(t);
	    }
	    try {
		return new StructType(types, names);
	    } catch(TypeException e) {
		throw new RuntimeTypeException(e.getMessage());
	    }
	} else {
	    return this;
	}
    }

    public Set dependsOn() {
	if(dependencySet == null) {
	    dependencySet = new HashSet(elementNames.size(),1);
	    Iterator i = elementTypes.values().iterator();
	    while(i.hasNext()) {
		dependencySet.add(i.next());
	    }
	}
	return dependencySet;
    }
}

class StructTypeLValue implements AggregateLValue
{
    private StructType type;
    private LValue base;
    
    public StructTypeLValue(StructType ty, LValue base) {
	Type baseType = base.getType();
	while(!baseType.isBaseType())
	    baseType = baseType.getBaseType();

	if(baseType.equals(ty)) {
	    this.type = ty;
	    this.base = base;
	} else {
	    throw new RuntimeTypeException("Cannot create an LValue for " +
					   "data of type " + ty + 
					   " using a backing store of type " + 
					   base.getType());
	}
    }

    public Type getType() {
	return type;
    }

    public Value getValue() 
	throws UninitializedVariableException {
	return base.getValue();
    }

    public void assign(Value v) 
	throws TypeException {
	base.assign(v);
    }
    
    public void assign(Value v, CodeObtainable s)
	throws TypeException {
	base.assign(v,s);
    }

    public LValue getSubLValue(String name) 
	throws TypeException {
	LValue lv = new StructFieldLValue(name, this);
	Type fieldType = lv.getType();
	return fieldType.getLValue(lv);
    }
}

class StructFieldLValue implements LValue
{
    private LValue base;
    private String field;
    private Type type;

    public StructFieldLValue(String field, LValue base) 
	throws TypeException {
	this.base = base;
	this.field = field;
	this.type = ((StructType)base.getType()).getElementType(field);
    }

    public Type getType() {
	return type;
    }

    public Value getValue() 
	throws UninitializedVariableException {

	Aggregate baseValue = (Aggregate)base.getValue();
	
	try {
	    return baseValue.getSubValue(field);
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(base + "." + field + 
						     " is not initialized");
	} catch(TypeException e) {
	    /* We've already ensured the field exists, so... */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    protected Value constructValue(Value fieldValue, CodeObtainable src) 
	throws TypeException {
	Aggregate currentValue;
	HashMap values;

	/* Get the current value if it exists */
	try { 
	    currentValue = (Aggregate)base.getValue();
	} catch(UninitializedVariableException e) {
	    currentValue = null;
	}

	/* Build a shell aggregate */
	values = new HashMap();
	StructType baseType = (StructType)base.getType();
	Iterator i = baseType.getElementNames().iterator();
	Value v;

	/* Copy the values from the existing value or initialize to
	 * null */
	while(i.hasNext()) {
	    String field = (String)i.next();
	    if(currentValue != null) {
		try {
		    v = currentValue.getSubValue(field);
		}  catch(TypeException e) {
		    /* We're iterating over names given by the type so... */
		    throw new RuntimeTypeException(e.getMessage());
		} catch(UninitializedVariableException e) {
		    v = null;
		}
	    } else 
		v = null;
	    values.put(field, v);
	}

	fieldValue = Coerce.assignCoerce(fieldValue, type, src);
	values.put(field, fieldValue);
	try {
	    return new StructValue((StructType)base.getType(), values);
	} catch(TypeException e) {
	    /* Can't happen */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }
    
    public void assign(Value v) 
	throws TypeException {
	Value nv = constructValue(v, null);
	base.assign(nv);
    }

    public void assign(Value v, CodeObtainable s)
	throws TypeException {
	Value nv = constructValue(v, s);
	base.assign(nv, s);
    }
}
