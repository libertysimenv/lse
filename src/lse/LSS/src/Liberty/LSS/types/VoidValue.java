package Liberty.LSS.types;

/* This class should be changed into an interface if there are
 * subtypes of void */

public class VoidValue extends ValueBase 
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new VoidValue());

    public VoidValue() {
	super(Types.voidType);
    }

    public String toString() {
	return "void";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(t.equals(Types.voidType)) {
	    return true;
	} else {
	    return false;
	}
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}
