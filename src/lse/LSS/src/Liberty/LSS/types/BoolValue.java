package Liberty.LSS.types;

/* This class should be changed into an interface if there are
 * subtypes of boolean */

public class BoolValue extends ValueBase {
    boolean value;

    public BoolValue(boolean v) {
	super(Types.boolType);
	value = v;
    }

    public BoolValue(Boolean v) {
	this(v.booleanValue());
    }

    public boolean getValue() {
	return value;
    }

    public String toString() {
	return new Boolean(value).toString().toUpperCase();
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.boolType)) {
	    boolean boolv = ((BoolValue)v).getValue();
	    return (boolv == value);
	} else 
	    return false;
    }

    public int hashCode() {
	return (new Boolean(value)).hashCode();
    }
}
