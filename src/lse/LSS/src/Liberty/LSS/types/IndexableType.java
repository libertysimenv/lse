package Liberty.LSS.types;

import java.util.*;

/* If a type implements this interface, then
 * isIndexable() should be true */

public interface IndexableType {
    public Type getSubValueType(int index) 
	throws TypeException,
	       UninitializedVariableException;
    public Value createIndexableValue(Vector v)
	throws TypeException;
}
