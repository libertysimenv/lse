package Liberty.LSS.types;

/* If an LValue implements this interface, then
 * lvalue.getType().isIndexable() should be true */

public interface IndexableLValue extends LValue {
    public LValue getSubLValue(int index) 
	throws TypeException;
}
