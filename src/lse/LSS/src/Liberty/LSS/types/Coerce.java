package Liberty.LSS.types;

import Liberty.LSS.*;
import java.util.*;
import Liberty.LSS.AST.*;

public class Coerce
{
    /* We want a coercion between 'a and 'b if 
     *  1) 'a <: 'b 
     *  2) not ('a :<: 'b) 
     *
     * This function will perform that coercion
     */
    public static Value subtypeCoerce(Value v, Type t) 
	throws TypeException {
	if(TypeRelations.isCompatible(v.getType(), t)) {
	    return v;
	}
	
	/* String -> UserPoint */
	if(v.getType().equals(Types.stringType) &&
	   (t instanceof UserPointType)) {
	    return new UserPointValue((UserPointType)t, 
				      ((StringValue)v).getValue());
	}
	
	/* String -> ControlPoint */
	if(v.getType().equals(Types.stringType) &&
	   (t.equals(Types.controlPointType))) {
	    return new ControlPointValue(((StringValue)v).getValue());
	}
	
	/* Int -> Float */
	if(v.getType().equals(Types.numericIntType) &&
	   t.equals(Types.floatType)) {
	    return new FloatValue((double)((NumericIntValue)v).getValue());
	}

	/* {'a x; 'b y; 'c z; ... } -> {'a x; 'b y; 'c z'} */
	if(v.getType() instanceof StructType &&
	   t instanceof StructType &&
	   TypeRelations.isSubtype(v.getType(), t)) {
	    HashMap values = new HashMap();
	    Iterator i = ((StructType)t).getElementNames().iterator();
	    
	    try {
		while(i.hasNext()) {
		    String fieldName = (String)i.next();
		    Value fieldValue;
		    try {
			fieldValue = ((Aggregate)v).getSubValue(fieldName);
		    } catch(UninitializedVariableException e) {
			fieldValue = null;
		    }
		    values.put(fieldName, fieldValue);
		}
	    } catch(TypeException e) {
		/* Shouldn't happen */
		throw new RuntimeTypeException(e.getMessage());
	    }
	    return new StructValue((StructType)t, values);
	}

	/* 'a[n] -> 'b[n] */
	if(v.getType() instanceof ArrayType &&
	   t instanceof ArrayType &&
	   (((ArrayType)v.getType()).getSize() == ((ArrayType)t).getSize() ||
	    ((ArrayType)t).getSize() == -1)) {
	    Type torig = ((ArrayType)v.getType()).getElementType();
	    Type tnew = ((ArrayType)t).getElementType();
	    Type tnewval = new ArrayType(tnew, 
					 ((ArrayType)v.getType()).getSize());

	    Vector originalVals = ((ArrayValue)v).getValue();
	    Vector newVals = new Vector();
	    Iterator i = originalVals.iterator();
	    while(i.hasNext()) {
		Value newVal = subtypeCoerce((Value)i.next(), tnew);
		newVals.add(newVal);
	    }
	    return subtypeCoerce(new ArrayValue((ArrayType)tnewval, newVals),
				 t);
	}

        /* nil <: 'b[] and
           nil <: 'b[0] */
        if(v.getType() instanceof NilType &&
           t instanceof ArrayType) {
            Type et = ((ArrayType)t).getElementType();
            ArrayType zat = new ArrayType(et, 0);
            Value nv = new ArrayValue(zat, new Vector());
            return subtypeCoerce(nv, t);
        }

	/* String -> Literal */
	if(v.getType().equals(Types.stringType) &&
	   t.equals(Types.literalType)) {
	    return new LiteralValue(((StringValue)v).getValue());
	}

	/* Error messages */
	if(TypeRelations.isSubtype(v.getType(), t)) {
	    throw new TypeException("Coercion between " + v.getType() + 
				    " and " + t + " not yet implemented");
	} else {
	    throw new TypeException("Cannot coerce " + v.getType() + 
				    " into " + t);
	}
    }

    public static Value noaryInvoke(Value v, CodeObtainable origin) 
	throws TypeException {
	if(v.getType().isCallable()) {
	    try {
		Value retVal = ((Callable)v).call(new Vector(), origin);
		return retVal;
	    } catch(UnimplementedException e) {
		throw new TypeException(e.getMessage());
	    } catch(UndefinedSymbolException e) {
		throw new TypeException(e.getMessage());
	    } catch(FunctionException e) {
		throw new TypeException(e.getMessage());
	    } catch(UninitializedVariableException e) {
		throw new TypeException(e.getMessage());
	    } catch(ParseException e) {
		throw new TypeException(e.getMessage());
	    }
	} else {
	    return v;
	}
    }

    public static Value assignCoerce(Value v, Type varType, 
				     CodeObtainable origin) 
	throws TypeException {
	/* Try dereferencing RHS if types don't match */
	try {
	    if(!TypeRelations.isCompatible(v.getType(), varType))
		v = TypeRelations.attemptDeref(v);
	} catch(UninitializedVariableException e) {
	    throw new TypeException(e.getMessage());
	}

	if(!TypeRelations.isCompatible(v.getType(), varType)) {
	    v = noaryInvoke(v, origin);
	}

	/* See if coercion helps */
	v = Coerce.subtypeCoerce(v, varType);
	
	return v;
    }
}
