package Liberty.LSS.types;

import Liberty.LSS.*;

public class UninitializedVariableException extends LSSException
{
    public UninitializedVariableException(String m) {
	super(m);
    }

    public UninitializedVariableException(CodeObtainable o, String m) {
	super(o,m);
    }
}
