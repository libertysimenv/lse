package Liberty.LSS.types;

import Liberty.LSS.IMR.*;
import Liberty.LSS.types.inference.*;
import java.util.*;

public class ExternalType extends Type
{
    private StringValue externalTypeName;

    public ExternalType(StringValue name) {
	externalTypeName = name;
    }

    public String toString() {
	return "external(" + externalTypeName.getFlattenedValue() + ")";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof ExternalType &&
	   externalTypeName.CArgListEquals(((ExternalType)t2).externalTypeName))
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return externalTypeName.hashCode();
    }   

    public String getBackendType() {
	return getBackendType(new HashMap());
    }

    public String getBackendType(Map typesToNames) {
	try {
	    StringValue strv = 
		ConnectionManagement.substituteString(externalTypeName,
						      new HashMap(),
						      typesToNames);
	    return strv.getFlattenedValue();
	} catch(TypeInferenceException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public boolean isPolymorphic() {
	class Manipulator implements StringValue.PieceManipulator {
	    boolean poly = false;
	    
	    public void handleString(String s) {}
	    public void handleType(Type t) {
		if(t.isPolymorphic())
		    poly = true;
	    }
	    
	    public boolean isPolymorphic() {
		return poly;
	    }
	}
	
	Manipulator manip = new Manipulator();
	Iterator i = externalTypeName.getValue().iterator();
	while(i.hasNext()) {
	    StringValue.Piece p = (StringValue.Piece)i.next();
	    p.manipulate(manip);
	}

	return manip.isPolymorphic();
    }

    /* For Type Inference */

    public Type substitute(final Map typeVarValues) {
	class Manipulator implements StringValue.PieceManipulator {
	    public Vector pieces = new Vector();
	    
	    public void handleString(String s) {
		pieces.add(StringValue.Piece.create(s));
	    }
	    public void handleType(Type t) {
		t = t.substitute(typeVarValues);
		if(t instanceof ArraySizeType) {
		    int val = ((ArraySizeType)t).getSize();
		    String str = (new Integer(val)).toString();
		    pieces.add(StringValue.Piece.create(str));
		} else {
		    pieces.add(StringValue.Piece.create(t));
		}
	    }
	}
	
	Manipulator manip = new Manipulator();
	Iterator i = externalTypeName.getValue().iterator();
	while(i.hasNext()) {
	    StringValue.Piece p = (StringValue.Piece)i.next();
	    p.manipulate(manip);
	}
	return new ExternalType(new StringValue(manip.pieces));
    }

    public Set dependsOn() {
	class Manipulator implements StringValue.PieceManipulator {
	    Set dependSet = new HashSet();
	    
	    public void handleString(String s) {}
	    public void handleType(Type t) {
		dependSet.add(t);
	    }
	    
	    public Set dependsOn() {
		return dependSet;
	    }
	}
	
	Manipulator manip = new Manipulator();
	Iterator i = externalTypeName.getValue().iterator();
	while(i.hasNext()) {
	    StringValue.Piece p = (StringValue.Piece)i.next();
	    p.manipulate(manip);
	}

	return manip.dependsOn();
    }
}
