package Liberty.LSS.types;

public class FloatType extends Type
{
    /* Used to calculate the hashCode for this type */
    private static final int memoizedHashCode = 
	System.identityHashCode(new FloatType());

    public String toString() {
	return "float";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof FloatType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public String getBackendType() {
	return "double";
    }
}
