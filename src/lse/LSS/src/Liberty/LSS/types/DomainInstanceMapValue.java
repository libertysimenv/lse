package Liberty.LSS.types;

import Liberty.LSS.*;
import java.util.*;

public class DomainInstanceMapValue extends ValueBase implements Aggregate {
    private DomainInstanceMapType type;
    private HashMap values;

    public DomainInstanceMapValue(DomainInstanceMapValue from) {
	super(from.type);
	type = from.type;
	values = from.values;
    }

    public DomainInstanceMapValue(DomainInstanceMapType t, Map v) 
	throws TypeException {
	super(t);
	type = t;

	/* Check to see that the values have the correct names */
	if(!type.getDomainClasses().containsAll(v.keySet())) {
	    Set errorSet = new HashSet(v.keySet());
	    errorSet.removeAll(type.getDomainClasses());
	    throw new TypeException("Cannot create a value of type " + 
				    type + " with labels " + errorSet);
	}

	/* Check the types of the values */
	values = new HashMap();
	Iterator names = type.getDomainClasses().iterator();
	while(names.hasNext()) {
	    String elementName = (String)names.next();
	    Value elementValue = (Value)v.get(elementName);
	    if(elementValue != null) {
		Type elementType = elementValue.getType();
		Type subValueType;
		try {
		    subValueType = type.getSubValueType(elementName);
		} catch(UninitializedVariableException e) {
		    /* Huh? */
		    throw new RuntimeTypeException(e.getMessage());
		}

		if(elementType.equals(subValueType)) {
		    values.put(elementName, elementValue);
		} else {
		    throw new TypeException("Cannot create a value of type " +
					    type + " with label " + 
					    elementName + 
					    " = " + elementValue + ":" + 
					    elementType);
		}
	    } else {
		values.put(elementName, null);
	    }
	}
    }

    public Value getSubValue(String s) 
	throws TypeException,
	       UninitializedVariableException {
	if(type.getDomainClasses().contains(s)) {
	    Value v = (Value)values.get(s);
	    if(v == null)
		throw new UninitializedVariableException("No default " + 
							 "domain instance " + 
							 "for domain class " + 
							 s + " has been " + 
							 "assigned");
	    else 
		return v;
	} else {
	    throw new TypeException("Element " + s + " is not a member of " +
				    "type " + type);
	}
    }

    /* Only use this after the copy constructor is invoked */
    protected void setSubValue(String s, DomainInstanceRefValue v)
	throws TypeException {
	if(!type.getDomainClasses().contains(s)) {
	    throw new TypeException("Element " + s + " is not a member of " +
				    "type " + type);
	}

	values.put(s,v);
    }

    public String toString() {
	/* Don't explicitly call toString since
	   values[i] could be null. JAVA does
	   the right thing if it divines that
	   toString needs to be called */
	Iterator i = type.getDomainClasses().iterator();
	String elements = "";
	String name;
	boolean first = true;

	while(i.hasNext()) {
	    if(!first)
		elements += ", ";
	    first = false;
	    name = (String)i.next();
	    elements += name + " = " + values.get(name);
	}
	return "{" + elements + "}";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(t.equals(type)) {
	    /* Type Relation guarantees:
 	        1) Field names match (strict equality)
		2) Field types match
	    */
	    Aggregate aggv = (Aggregate)v;
	    Value v1, v2;
	    Iterator i = type.getDomainClasses().iterator();
	    String field;
	    try {
		while(i.hasNext()) {
		    field = (String)i.next();
		    try { v1 = this.getSubValue(field); } 
		    catch(UninitializedVariableException e) { v1 = null; }
		    
		    try { v2 = aggv.getSubValue(field); }
		    catch(UninitializedVariableException e) { v2 = null; }
		    
		    if((v1 == null && v2 != null) ||
		       (v1 != null && v2 == null) ||
		       (v1 != null && v2 != null && 
			!v1.equals(v2))) return false;
		}
	    } catch(TypeException e) {
		/* Can't happen */
		throw new RuntimeTypeException(e.getMessage());
	    }
	    return true;
	} else 
	    return false;
    }

    public int hashCode() {
	return type.hashCode() + values.hashCode();
    }
}
