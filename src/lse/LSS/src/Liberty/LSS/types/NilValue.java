package Liberty.LSS.types;

import java.util.*;

public class NilValue extends ValueBase implements Indexable,Aggregate {
    private static final int memoizedHashCode = 
	System.identityHashCode(new NilValue());

    public NilValue() {
	super(Types.nilType);
    }

    public Value getSubValue(int i) 
	throws TypeException,
	       UninitializedVariableException {
        throw new TypeException("Index " + i + " out-of-bounds on type " +
                                Types.nilType);
    }

    public Value getSubValue(String field)
	throws TypeException {
	if(field.equals("size")) {
	    return new NumericIntValue(0);
	} else {
	    throw new TypeException("Type " + Types.nilType + 
				    " has no field named " + field);
	}
    }

    public String toString() {
        return "nil";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof NilValue))
	    return false;
        return true;
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}

