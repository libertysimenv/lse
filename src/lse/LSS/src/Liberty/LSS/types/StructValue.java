package Liberty.LSS.types;

import java.util.*;

public class StructValue extends ValueBase implements Aggregate {
    private StructType type;
    private HashMap values;

    public StructValue(StructType t, HashMap v) 
	throws TypeException {
	super(t);
	type = t;

	/* Check to see that the values have the correct names */
	if(!type.getElementNames().equals(v.keySet())) {
	    throw new TypeException("Cannot create a value of type " + 
				    type + " with labels " + v.keySet());
	}

	/* Check the types of the values */
	values = new HashMap();
	Iterator names = v.keySet().iterator();
	while(names.hasNext()) {
	    String elementName = (String)names.next();
	    Value elementValue = (Value)v.get(elementName);
	    if(elementValue != null) {
		Type elementType = elementValue.getType();
		if(TypeRelations.
		   isCompatible(elementType, 
				type.getElementType(elementName))) {
		    values.put(elementName, elementValue);
		} else {
		    throw new TypeException("Cannot create a value of type " +
					    type + " with label " + 
					    elementName + 
					    " = " + elementValue + ":" + 
					    elementType);
		}
	    } else {
		values.put(elementName, null);
	    }
	}
    }

    public Value getSubValue(String s) 
	throws TypeException,
	       UninitializedVariableException {
	if(values.containsKey(s)) {
	    Value v = (Value)values.get(s);
	    if(v == null)
		throw new UninitializedVariableException("Element " + s +
							 " on struct has " +
							 "not been " +
							 "initialized");
	    else 
		return v;
	} else {
	    throw new TypeException("Element " + s + " is not a member of " +
				    "struct of type " + type);
	}
    }

    public String toString() {
	/* Don't explicitly call toString since
	   values[i] could be null. JAVA does
	   the right thing if it divines that
	   toString needs to be called */
	Iterator i = type.getElementNames().iterator();
	String elements = "";
	String name;
	boolean first = true;

	while(i.hasNext()) {
	    if(!first)
		elements += ", ";
	    first = false;
	    name = (String)i.next();
	    elements += name + " = " + values.get(name);
	}
	return "{" + elements + "}";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(t.equals(type)) {
	    /* Type Relation guarantees:
 	        1) Field names match (strict equality)
		2) Field types match
	    */
	    Aggregate aggv = (Aggregate)v;
	    Value v1, v2;
	    Iterator i = type.getElementNames().iterator();
	    String field;
	    try {
		while(i.hasNext()) {
		    field = (String)i.next();
		    try { v1 = this.getSubValue(field); } 
		    catch(UninitializedVariableException e) { v1 = null; }
		    
		    try { v2 = aggv.getSubValue(field); }
		    catch(UninitializedVariableException e) { v2 = null; }
		    
		    if((v1 == null && v2 != null) ||
		       (v1 != null && v2 == null) ||
		       (v1 != null && v2 != null && 
			!v1.equals(v2))) return false;
		}
	    } catch(TypeException e) {
		/* Can't happen */
		throw new RuntimeTypeException(e.getMessage());
	    }
	    return true;
	} else 
	    return false;
    }

    public int hashCode() {
	return values.hashCode();
    }
}
