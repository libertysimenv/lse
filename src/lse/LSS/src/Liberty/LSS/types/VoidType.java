package Liberty.LSS.types;

public class VoidType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new VoidType());

    public String toString() {
	return "void";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof VoidType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public String getBackendType() {
	return "void";
    }
}
