package Liberty.LSS.types;

public class ArraySizeType extends Type
{
    private int size;

    public ArraySizeType(int s) {
	size = s;
    }

    public int getSize() {
	return size;
    }

    public String toString() {
	return "ArraySize(" + size + ")";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof ArraySizeType)
	    return (size == ((ArraySizeType)t2).size);
	else
	    return false;
    }

    public int hashCode() {
	return (new Integer(size)).hashCode();
    }
}

