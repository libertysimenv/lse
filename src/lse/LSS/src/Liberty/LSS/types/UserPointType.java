package Liberty.LSS.types;

import java.util.*;

public class UserPointType extends Type
{
    private StringValue arguments;
    private StringValue returnType;

    public UserPointType(StringValue a, StringValue rt) {
	arguments = a;
	returnType = rt;
    }

    public String toString() {
	return "userpoint(" + 
	    arguments.toString() + 
	    " => " + 
	    returnType.toString() +
	    ")";
    }

    public StringValue getArguments() {
	return arguments;
    }

    public StringValue getReturnType() {
	return returnType;
    }    

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof UserPointType) {
	    UserPointType ut2 = (UserPointType)t2;
	    return (arguments.CArgListEquals(ut2.arguments) &&
		    returnType.CArgListEquals(ut2.returnType));
	} else
	    return false;
    }

    public int hashCode() {
	return arguments.hashCode() + returnType.hashCode();
    }

    public boolean isPolymorphic() {
	class Manipulator implements StringValue.PieceManipulator {
	    public boolean polymorphic = false;

	    public void handleString(String s) {
		/* Do nothing */
	    }
	    public void handleType(Type t) {
		if(t.isPolymorphic())
		    polymorphic = true;
	    }
	}

	Manipulator m = new Manipulator();
	Iterator i;
	
	i = arguments.getValue().iterator();
	while(i.hasNext()) {
	    StringValue.Piece p = (StringValue.Piece)i.next();
	    p.manipulate(m);
	}

	i = returnType.getValue().iterator();
	while(i.hasNext()) {
	    StringValue.Piece p = (StringValue.Piece)i.next();
	    p.manipulate(m);
	}

	return m.polymorphic;
    }

    public StringValue substituteString(StringValue str,
					final Map typeVariableContext,
					final Map typesToNames) {
	if(str == null) return null;
	class Manipulator implements StringValue.PieceManipulator {
	    public Vector pieces = new Vector();

	    public void handleString(String s) {
		pieces.add(StringValue.Piece.create(s));
	    }
	    public void handleType(Type t) {
		t = t.substitute(typeVariableContext, typesToNames);

		String piece;
		if(t instanceof ArraySizeType) {
		    int val = ((ArraySizeType)t).getSize();
		    piece = (new Integer(val)).toString();
		} else {
		    if(t.isPolymorphic()) {
			pieces.add(StringValue.Piece.create(t));
			return;
		    }
		    
		    if(typesToNames.containsKey(t)) {
			piece = "LSEut_ref(" + 
			    (String)typesToNames.get(t) + ")";
		    } else {
			/* If it doesn't exist, emit the backend type */
			piece = t.getBackendType(typesToNames);
		    }
		}
		pieces.add(StringValue.Piece.create(piece));
	    }
	}
	
	Manipulator manip = new Manipulator();
	Iterator i = str.getValue().iterator();
	while(i.hasNext()) {
	    StringValue.Piece p = (StringValue.Piece)i.next();
	    p.manipulate(manip);
	}

	StringValue value = new StringValue(manip.pieces);
	return value;
    }


    public Type substitute(Map typeVarValues, Map globalTypes) {
	StringValue newargs = 
	    substituteString(arguments, typeVarValues, globalTypes);
	StringValue newret = 
	    substituteString(returnType, typeVarValues, globalTypes);
	return new UserPointType(newargs, newret);
    }
}

