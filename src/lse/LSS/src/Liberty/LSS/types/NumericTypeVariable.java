package Liberty.LSS.types;

public class NumericTypeVariable extends TypeVariable 
{
    public NumericTypeVariable(String n) {
	name = n;
    }

    /* Numeric type variables behave like types in some senses, and
     * values in another.  This is where we want it to behave like a
     * value */
    public Type getType() {
	return Types.deferredIntType;
    }
}
