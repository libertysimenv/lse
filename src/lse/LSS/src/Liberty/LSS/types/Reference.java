package Liberty.LSS.types;

/* If a value implements this interface, then
 * value.getType().isReference() should be true */

public interface Reference {
    public Value deref() throws TypeException,
				UninitializedVariableException;
}
