package Liberty.LSS.types;

/* If a value implements this interface, then
 * value.getType().isAggregate() should be true */

public interface Aggregate {
    public Value getSubValue(String name) 
	throws TypeException,
	       UninitializedVariableException;
}
