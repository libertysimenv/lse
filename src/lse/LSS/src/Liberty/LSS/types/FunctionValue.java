package Liberty.LSS.types;

import Liberty.LSS.*;
import Liberty.LSS.AST.UnimplementedException;
import Liberty.LSS.AST.UndefinedSymbolException;
import Liberty.LSS.AST.SymbolAlreadyDefinedException;
import Liberty.LSS.AST.ReturnException;
import Liberty.LSS.AST.BreakException;
import Liberty.LSS.AST.FunctionException;
import Liberty.LSS.AST.ParseException;
import Liberty.LSS.AST.ExecutionContext;
import Liberty.LSS.AST.CompoundStatement;
import Liberty.LSS.AST.IdentifierLValue;
import java.util.*;

public class FunctionValue extends ValueBase implements Callable {
    private ExecutionContext context;
    private String[] arguments;
    private CompoundStatement body;

    public FunctionValue(FunctionType t, Vector a, 
			 CompoundStatement b, ExecutionContext c) {
	super(t);
	arguments = new String[a.size()];
	arguments = (String[])a.toArray(arguments);
	body =  b;
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite) 
	throws UnimplementedException, 
	       TypeException,
	       UndefinedSymbolException,
	       FunctionException,
	       UninitializedVariableException,
	       ParseException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	Type[] argTypes = ((FunctionType)getType()).getArguments();
	if(argTypes == null) argTypes = new Type[0];
	
	if(argTypes.length != argValues.size())
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");
	
	for(int i = 0; i < argTypes.length; i++) {
	    try {
		c.addVariable(arguments[i], argTypes[i], null);
		
		LValue lv = new IdentifierLValue(c,arguments[i]);
		lv = lv.getType().getLValue(lv);
		lv.assign((Value)argValues.elementAt(i));
	    } catch(TypeException e) {
		throw new FunctionException("Argument " + 
					    Integer.toString(i+1) + 
					    " in call to " +
					    "function with type " + 
					    getType() +
					    " must have type " +
					    argTypes[i]);
	    } catch(SymbolAlreadyDefinedException e) {
		/* This shouldn't ever happen */
		throw new RuntimeTypeException(e.getMessage());
	    }
	}

	try {
	    body.execute(c);
	} catch(ReturnException e) {
	    Value rv = e.getValue();
	    Type returnType = ((CallableType)getType()).getReturnType();

	    if(TypeRelations.isSubtype(rv.getType(), returnType))
		return Coerce.subtypeCoerce(rv, returnType);
	    else {
		try {
		    rv = TypeRelations.attemptDeref(rv);
		} catch(UninitializedVariableException e2) {
		    /* Oh well... the following code will just throw a
		     * FunctionException */
		}

		if(TypeRelations.isSubtype(rv.getType(), returnType))
		    return Coerce.subtypeCoerce(rv, returnType);
		else
		    throw new FunctionException("Function with type " + 
						getType() + 
						" returned type " + 
						rv.getType());
	    }
	} catch(BreakException e) {
	    throw new TypeException(e.getMessage());
	} catch(SymbolAlreadyDefinedException e) {
	    throw new TypeException(e.getMessage());
	}

	if(((FunctionType)getType()).getReturnType().equals(Types.voidType))
	    return new VoidValue();

	throw new FunctionException("Function with type " + getType() + " did not" +
				    " return a value");
    }
}
