package Liberty.LSS.types;

import Liberty.LSS.*;
import java.util.*;

public class RuntimeParmType extends Type
{
    protected Type parmType;
    protected Set dependencySet = null;

    public RuntimeParmType(Type bt) {
	parmType = bt;
    }

    public Type getParmType() {
	return parmType;
    }

    public LValue getLValue(LValue base)
	throws TypeException {
	return new RuntimeParmLValue(base, this);
    }
    
    public boolean equals(Object t2) {
	if(t2 == null) return false;

	if(t2 instanceof RuntimeParmType) {
	    RuntimeParmType tt2;
	    tt2 = (RuntimeParmType)t2;
	    return parmType.equals(tt2.parmType);
	} else { 
	    return false;
	}
    }

    public Set dependsOn() {
	if(dependencySet == null) {
	    dependencySet = new HashSet(1,1);
	    dependencySet.add(parmType);
	}
	return dependencySet;
    }

    public int hashCode() {
	return parmType.hashCode();
    }

    public String toString() {
	return "RuntimeParmType(" + parmType.toString() + ")";
    }

    public String getBackendType() {
	return parmType.getBackendType();
    }

}


class RuntimeParmLValue implements LValue
{
    private LValue base;
    private RuntimeParmType type;

    public RuntimeParmLValue(LValue b, RuntimeParmType t) {
	base = b;
	type = t;
    }

    public void assign(Value v) throws TypeException {
	assign(v, null);
    }

    public boolean tryAssign(Value v, CodeObtainable s) throws TypeException {
	if(TypeRelations.isSubtype(v.getType(), type)) {
	    base.assign(v, s);
	    return true; 
	} else if(TypeRelations.isSubtype(v.getType(), type.getParmType())) {
	    try {
		v = new RuntimeParmValue(false, 
					 new RuntimeParmType(v.getType()), 
					 v, null, null);
	    } catch(TypeException e) {
		throw new RuntimeTypeException(e.getMessage());
	    }
	    base.assign(v, s);
	    return true;
	} else {
	    return false;
	}
    }	

    public void assign(Value v, CodeObtainable s) throws TypeException {
	if(!tryAssign(v,s)) {
	    try {
		v = TypeRelations.attemptDeref(v);
		if(!tryAssign(v,s)) {
		    /* Just go ahead and assign anyway and let the
		     * lower lvalue throw an exception */
		    base.assign(v,s);
		}
	    }  catch(UninitializedVariableException e) {
		if(s != null)
		    throw new TypeException(s, e.getMessage());
		else
		    throw new TypeException(e.getMessage());
	    }
	}
    }

    public Type getType() {
	return type;
    }

    public Value getValue() throws UninitializedVariableException {
	return base.getValue();
    }
}


