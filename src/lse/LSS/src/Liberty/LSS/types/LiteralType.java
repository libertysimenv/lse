package Liberty.LSS.types;

public class LiteralType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new LiteralType());

    public String toString() {
	return "literal";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof LiteralType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    // A backend type is added for literal so that literal parameters
    // can output a type.... the type itself does is a "special" type
    public String getBackendType() {
	return "LSE_literal_t";
    }

}
