package Liberty.LSS.types;

import java.util.*;

public abstract class ModifiedType extends Type
    implements AggregateType, CallableType, IndexableType
{
    protected Type baseType;
    protected Set dependencySet = null;

    public ModifiedType(Type bt) {
	baseType = bt;
    }

    public boolean isBaseType() {
	return false;
    }

    public Type getBaseType() {
	return baseType;
    }

    public LValue getLValue(Value base) 
	throws TypeException {
	return baseType.getLValue(base);	
    }

    public LValue getLValue(LValue base)
	throws TypeException {
	return baseType.getLValue(base);
    }
    
    public boolean isIndexable() {
	return baseType.isIndexable();
    }

    public boolean isAggregate() {
	return baseType.isAggregate();
    }

    public boolean isCallable() {
	return baseType.isCallable();
    }

    public boolean isReference() {
	return baseType.isReference();
    }

    public boolean isPolymorphic() {
	return baseType.isPolymorphic();
    }
    
    public boolean equals(Object t2) {
	if(t2 == null) return false;

	Type tt2;
	if(t2 instanceof Type)
	    tt2 = (Type)t2;
	else
	    return false;
	if(tt2.isBaseType()) return false;
	return tt2.getBaseType().equals(baseType);
    }

    public Type getSubValueType(String field) 
	throws TypeException,
	       UninitializedVariableException {
	if(isAggregate())
	    return ((AggregateType)baseType).getSubValueType(field);
	else
	    throw new TypeException(this + " is not an aggregate type");
    }

    public Type getSubValueType(int index) 
	throws TypeException,
	       UninitializedVariableException {
	if(isIndexable())
	    return ((IndexableType)baseType).getSubValueType(index);
	else
	    throw new TypeException(this + " is not an indexable type");
    }

    public Value createIndexableValue(Vector v)
	throws TypeException {
	if(isIndexable())
	    return ((IndexableType)baseType).createIndexableValue(v);
	else
	    throw new TypeException(this + " is not an indexable type");
    }

    public Type getReturnType() 
	throws TypeException {
	if(isCallable())
	    return ((CallableType)baseType).getReturnType();
	else
	    throw new TypeException(this + " is not a callable type");
    }

    public Set dependsOn() {
	if(dependencySet == null) {
	    dependencySet = new HashSet(1,1);
	    dependencySet.add(baseType);
	}
	return dependencySet;
    }
}

