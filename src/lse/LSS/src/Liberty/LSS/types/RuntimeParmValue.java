package Liberty.LSS.types;

public class RuntimeParmValue extends ValueBase {
    private RuntimeParmType type;
    private Value defaultValue;
    private boolean runtimed;
    private String clname;
    private String cldesc;

    public RuntimeParmValue(boolean r, RuntimeParmType t, Value def, 
			    String name, String desc) 
	throws TypeException {
	super(t);
	type = t;

	if(!TypeRelations.isCompatible(def.getType(), type.getParmType())) {
	    throw new TypeException("Default value does not match type");
	}
	defaultValue = def;

	runtimed = r;
	clname = name;
	cldesc = desc;
    }

    public Value getDefaultValue() {
	return defaultValue;
    }

    public boolean getRuntimed() {
	return runtimed;
    }

    public String getCommandLineName() {
	return clname;
    }

    public String getCommandLineDesc() {
	return cldesc;
    }

    public String toString() {
	if(!runtimed) return defaultValue.toString();
	else {
	    return "LSEuv_rp_ref(" + clname + ")";
	}
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof RuntimeParmValue))
	    return false;
	
	RuntimeParmValue v = (RuntimeParmValue)o;
	if(!defaultValue.equals(v.defaultValue)) return false;
	if(!clname.equals(v.clname)) return false;
	if(!cldesc.equals(v.cldesc)) return false;
	if(runtimed != v.runtimed) return false;
	return true;
    }

    public int hashCode() {
	return defaultValue.hashCode() +
	    clname.hashCode() +
	    cldesc.hashCode() + 
	    (new Boolean(runtimed)).hashCode();
    }
}
