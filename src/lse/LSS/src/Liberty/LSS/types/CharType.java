package Liberty.LSS.types;

public class CharType extends Type
{
    /* Used to calculate the hashCode for this type */
    private static final int memoizedHashCode = 
	System.identityHashCode(new CharType());

    public String toString() {
	return "char";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof CharType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public String getBackendType() {
	return "char";
    }
}
