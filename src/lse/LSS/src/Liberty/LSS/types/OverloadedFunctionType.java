package Liberty.LSS.types;

import java.util.*;

public class OverloadedFunctionType extends Type 
    implements CallableType
{
    private Type returnType;
    private Map functions;
    private Set dependencySet = null;

    public OverloadedFunctionType(Collection funcs) 
	throws TypeException {
	if(funcs.size() == 0) {
	    throw new TypeException("An overloaded function must have at " + 
				    "least one function");
	}

	Iterator i = funcs.iterator();
	returnType = null;
	functions = new HashMap();

	while(i.hasNext()) {
	    FunctionType f = (FunctionType)i.next();
	    if(returnType == null)
		returnType = f.getReturnType();
	    if(!returnType.equals(f.getReturnType())) {
		throw new TypeException("Return types must agree for all " + 
					"functions in an overloaded function");
	    }
	    Object test = functions.get(new Integer(f.getNumArguments()));
	    if(test != null) {
		throw new TypeException("Only 1 function may take " +
					f.getNumArguments() + " argument in " +
					" an overloaded function");
	    }
	    functions.put(new Integer(f.getNumArguments()), f);
	}
    }

    public String toString() {
	Iterator i = functions.values().iterator();
	boolean first = true;
	String s = "";
	while(i.hasNext()) {
	    if(!first) s += " + ";
	    if(first) first=false;
	    s += i.next();
	}
	return s;
    }

    public Type getReturnType() {
	return returnType;
    }

    public Collection getFunctionTypes() {
	return functions.values();
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof OverloadedFunctionType) {
	    OverloadedFunctionType ft2 = (OverloadedFunctionType)t2;
	    return functions.equals(ft2.functions);
	} else
	    return false;
    }

    public int hashCode() {
	return functions.hashCode();
    }

    public boolean isCallable() {
	return true;
    }

    public Set dependsOn() {
	if(dependencySet == null) {
	    Iterator i = functions.values().iterator();
	    dependencySet = new HashSet(functions.values().size(),1);
	    while(i.hasNext()) {
		dependencySet.add(i.next());
	    }
	    dependencySet.add(returnType);
	}
	return dependencySet;
    }
}

