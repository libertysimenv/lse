package Liberty.LSS.types;

import java.util.*;
import Liberty.LSS.*;
import Liberty.LSS.types.inference.*;

public class NilType extends Type
    implements AggregateType, IndexableType
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new NilType());

    public NilType() {
    }

    public String toString() {
        return "nil";
    }

    public boolean isPolymorphic() {
	return false;
    }

    public Type getSubValueType(int index) 
      throws TypeException {
        throw new TypeException("Index " + index + " out of bounds on type " + 
                                Types.nilType);
    }

    public Type getSubValueType(String field) 
	throws TypeException {
	if(field.equals("size")) {
	    return Types.numericIntType;
	} else {
	    throw new TypeException("Type " + this + 
				    " has no field named " + field);
	}
    }

    public Value createIndexableValue(Vector v)
	throws TypeException {
	
	if(v.size() != 0)
	    throw new TypeException("Cannot create a value of type " + this + 
				    " from a vector of length " + v.size());
	    
	return new NilValue();
    }

    public int getSize() {
        return 0;
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof NilType)
            return true;
	else
	    return false;
    }

    public int hashCode() {
        return memoizedHashCode;
    }

    public boolean isIndexable() {
	return true;
    }

    public boolean isAggregate() {
	return true;
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
        throw new TypeException("Type " + this + " is not an lvalue");
    }      
}
 

