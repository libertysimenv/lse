package Liberty.LSS.types;

import Liberty.LSS.*;

public class TypeException extends LSSException
{
    public TypeException(String m) {
	super(m);
    }

    public TypeException(CodeObtainable o, String m) {
	super(o,m);
    }
}
