package Liberty.LSS.types;

public class GeneralExternalType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new GeneralExternalType());

    public String toString() {
	return "general_external";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof GeneralExternalType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }
}

