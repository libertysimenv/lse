package Liberty.LSS.types;

public class CArgListCanonicalizer {
    private static boolean formsDigram(char first, char second) {
	if(first == '>' && second == '>') return true; /* Stupid C++ */
	else return false;
    }

    public static String canonicalize(String value) {
	StringBuffer canon = new StringBuffer();
	
	boolean punctuation = true;   /* Swallow leading space */
	boolean sawSpace = false;
	boolean realSawSpace = false; /* Even between operators */
	char previousCharacter = '_'; /* this should be a safe initial value */
	
	for(int i = 0; i < value.length(); i++) {
	    if(Character.isWhitespace(value.charAt(i))) {
		realSawSpace = true;
		if(punctuation) {
		    // Just swallow this space
		} else {
		    sawSpace = true;
		}
	    } else {
		/* Java Identifier's and C identifiers are identical,
		 * that is why using isJavaIdentifier is safe here */
		punctuation = !Character.isJavaIdentifierPart(value.charAt(i));
		if(realSawSpace &&
		   formsDigram(previousCharacter, value.charAt(i))) {
		    //		    System.out.printf("Found digram %c%c\n", previousCharacter,
		    //				      value.charAt(i));
		    sawSpace = false;
		    canon.append(' ');
		}

		if(sawSpace) {
		    sawSpace = false;
		    if(!punctuation) {
			canon.append(' ');
		    }
		}

		canon.append(value.charAt(i));
		previousCharacter = value.charAt(i);
		realSawSpace = false;
	    }
	}
	return canon.toString();
    }

    public static void main(String args[]) {
	System.out.println("--" + CArgListCanonicalizer.canonicalize(args[0]) + "--");
    }
}

