package Liberty.LSS.types;

public class StringType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new StringType());

    public String toString() {
	return "string";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof StringType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public String getBackendType() {
	return "char *";
    }
}
