package Liberty.LSS.types;

import java.util.*;
import Liberty.LSS.*;
import Liberty.LSS.types.inference.*;

public class ArrayType extends Type
    implements AggregateType, IndexableType
{
    private Type elementType;
    private int size;
    private NumericTypeVariable sizeVar;
    private Set dependencySet = null;

    public ArrayType(Type t) {
	elementType = t;
	size = -1;
	sizeVar = null;
    }

    public ArrayType(Type t, int s) {
	this(t);
	size = s;
    }

    public ArrayType(Type t, NumericTypeVariable v) {
	this(t);
	sizeVar = v;
    }

    public String toString() {
	if(size != -1) {
	    return elementType.toString() + "[" +
		Integer.toString(size) + "]";
	} else if(sizeVar == null) {
	    return elementType.toString() + "[]";
	} else {
	    return elementType.toString() + "[" + 
		sizeVar.toString() + "]";
	}
    }

    public boolean isPolymorphic() {
	if(sizeVar != null || elementType.isPolymorphic())
	    return true;
	return false;
    }

    public Type getElementType() {
	return elementType;
    }

    public Type getSubValueType(int index) {
	return getElementType();
    }

    public Value createIndexableValue(Vector v)
	throws TypeException {
	if(isPolymorphic()) {
	    throw new TypeException("Cannot create a value of " + 
				    "polymorphic type " + this);
	}
	
	ArrayType t = this;
	if(size == -1)
	    t = new ArrayType(elementType, v.size());
	else if(v.size() != size)
	    throw new TypeException("Cannot create a value of type " + this + 
				    " from a vector of length " + v.size());
	    
	return new ArrayValue(t, v);
    }

    public Type getSubValueType(String field) 
	throws TypeException {
	if(field.equals("size")) {
	    return Types.numericIntType;
	} else {
	    throw new TypeException("Type " + this + 
				    " has no field named " + field);
	}
    }

    public int getSize() {
	return size;
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof ArrayType) {
	    ArrayType at2 = (ArrayType)t2;
	    return (getElementType().equals(at2.getElementType()) &&
		    getSize() == at2.getSize() &&
		    sizeVar == at2.sizeVar);
	} else
	    return false;
    }

    public int hashCode() {
	return elementType.hashCode() + size;
    }

    public boolean isIndexable() {
	return true;
    }

    public boolean isAggregate() {
	return true;
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
	if(isPolymorphic()) {
	    throw new TypeException("Type " + this + " is not an lvalue");
	}
	return new ArrayTypeLValue(this, base);
    }
    
    public String getBackendType(Map typesToNames) {
	if(size != -1) {
	    String backendType;
	    if(typesToNames.containsKey(elementType))
		backendType = ("LSEut_ref(" + 
			       (String)typesToNames.get(elementType) +
			       ")");
	    else
		backendType = elementType.getBackendType(typesToNames);
	    return "LSEut_arraydef(" + size + "," + backendType + ")";
	} else {
	    return super.getBackendType();
	}
    }

    public String getBackendType() {
	return getBackendType(new HashMap());
    }

    public ConstraintSatisfaction canBeEqual(Type t, ConstraintSource origin) {
	if(!isPolymorphic()) {
	    return super.canBeEqual(t, origin);
	} else {
	    Set s = new HashSet();
	    if(t instanceof ArrayType) {
		/* Verify element types are equal */
		ArrayType at = (ArrayType)t;
		s.add(new Constraint(getElementType(), 
				     at.getElementType(), origin));
		
		/* Verify sizes are equal */
		Type other;
		Type me;

		if(sizeVar != null) {
		    me = sizeVar;
		} else {
		    me = new ArraySizeType(size);
		}

		if(at.sizeVar != null)
		    other = at.sizeVar;
		else
		    other = new ArraySizeType(at.size);

		s.add(new Constraint(me, other, origin));
		return new ConstraintSatisfaction(true, s);
	    } else {
		return new ConstraintSatisfaction(false, s);
	    }
	}
    }
    
    public Type substitute(Map typeVarValues) {
	Type et = getElementType();
	et = et.substitute(typeVarValues);
	
	if(sizeVar != null) {
	    Type t = (Type)typeVarValues.get(sizeVar);
	    if(t != null) {
		if(t instanceof ArraySizeType) {
		    return new ArrayType(et, ((ArraySizeType)t).getSize());
		} else if(t instanceof NumericTypeVariable) {
		    return new ArrayType(et, (NumericTypeVariable)t);
		} else {
		    throw new RuntimeTypeException("Bogus type " + t + 
						   " leaked into " + 
						   "array size");
		}
	    } else {
		return new ArrayType(et, sizeVar);
	    }
	}
	
	return new ArrayType(et, size);
    }

    public Set dependsOn() {
	if(dependencySet == null) {
	    dependencySet = new HashSet(1,1);
	    dependencySet.add(elementType);
	}
	return dependencySet;
    }
}
    

class ArrayTypeLValue implements IndexableLValue, AggregateLValue
{
    private ArrayType type;
    private LValue base;
    
    public ArrayTypeLValue(ArrayType ty, LValue base) {
	Type baseType = base.getType();
	while(!baseType.isBaseType())
	    baseType = baseType.getBaseType();

	if(ty.equals(baseType)) {
	    this.type = ty;
	    this.base = base;
	} else {
	    throw new RuntimeTypeException("Cannot create an LValue for " +
					   "data of type " + ty + 
					   " using a backing store of type " + 
					   base.getType());
	}
    }

    public Type getType() {
	return type;
    }

    public Value getValue() 
	throws UninitializedVariableException {
	return base.getValue();
    }

    public void assign(Value v) 
	throws TypeException {
	base.assign(v);
    }
    
    public void assign(Value v, CodeObtainable s)
	throws TypeException {
	base.assign(v,s);
    }

    public LValue getSubLValue(int index) 
	throws TypeException {
	LValue lv = new ArrayIndexLValue(this, index);
	Type fieldType = lv.getType();
	return fieldType.getLValue(lv);
    }

    public LValue getSubLValue(String name)
	throws TypeException {
	if(name.equals("size"))
	    throw new TypeException("Cannot write to field size on type " +
				    type);
	else
	    throw new TypeException("Type " + type + " has no field named " +
				    name);
    }
}


class ArrayIndexLValue implements LValue
{
    LValue base;
    int index;
    Type type;
    int size;

    public ArrayIndexLValue(LValue base, int i) 
	throws TypeException {
	this.base = base;
	this.index = i;

	ArrayType arrayType = (ArrayType)base.getType();

	/* Grab the element type and size */
	type = arrayType.getElementType();
	size = arrayType.getSize();
	
	/* Verify array bounds */
	if(size != -1 && index >= size) {
	    throw new TypeException("Index " + i + " out-of-bounds for type " +
				    arrayType);
	}
    }

    public Type getType() {
	return type;
    }

    public Value getValue() throws UninitializedVariableException {
	try {
	    Indexable v = (Indexable)base.getValue();
	    return v.getSubValue(index);
	} catch(TypeException e) {
	    /* The current value may not have this index */
	    throw new UninitializedVariableException(base + "[" + index + "]" +
						     " has not been " +
						     "initialized");
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(base + "[" + index + "]" +
						     " has not been " + 
						     "initialized");
	}
    }

    public String toString() {
	return base + "[" + index + "]";
    }
    
    protected Value constructValue(Value elementValue, CodeObtainable src)
	throws TypeException {
	Vector values;

	try {
	    ArrayValue baseValue = (ArrayValue)base.getValue();

	    /* Assume that the value type is ArrayValue.  If something
	       is compatible with arrays but the value is not represented
	       in an ArrayValue, this will break. */
	    values = baseValue.getValue(); 
	} catch(UninitializedVariableException e) {
	    values = new Vector();
	    if(size != -1) 
		values.setSize(size); 
	}

	if(index+1 > values.size()) {
	    /* Grow the vector if necessary.  If index is the "new"
	     * last element of the vector, than the vector's size is
	     * index+1.  Thus we compare index+1 to the current size
	     * and modify appropriately */
	    values.setSize(index+1);
	}

	elementValue = Coerce.assignCoerce(elementValue, type, src);
	values.set(index,elementValue);

	ArrayType at = new ArrayType(type,values.size());
	return new ArrayValue(at, values);
    }

    public void assign(Value v) 
	throws TypeException {
	Value newValue = constructValue(v, null);
	base.assign(newValue);
    }

    public void assign(Value v, CodeObtainable s) 
	throws TypeException {
	Value newValue = constructValue(v, s);
	base.assign(newValue, s);
    }
}
