package Liberty.LSS.types;

public class Types
{
    public static Type boolType = new BoolType();
    public static Type intType = new IntType();
    public static Type numericIntType = new NumericIntType();
    public static Type deferredIntType = new DeferredIntType();
    public static Type floatType = new FloatType();
    public static Type charType = new CharType();
    public static Type stringType = new StringType();
    public static Type controlPointType = new ControlPointType();
    public static Type voidType = new VoidType();
    public static Type noneType = new NoneType();
    public static Type literalType = new LiteralType();
    public static Type instanceRefType = new InstanceRefType();
    public static Type portRefType = new PortRefType();
    public static Type portInstanceRefType = new PortInstanceRefType();
    public static Type parameterRefType = new ParameterRefType();
    public static Type unknownType = new UnknownType();
    public static Type typeType = new TypeType();
    public static Type runtimeVarRefType = new RuntimeVarRefType();
    public static Type domainInstanceRefType = new DomainInstanceRefType();
    public static Type generalExternalType = new GeneralExternalType();
    public static Type nilType = new NilType();
}
