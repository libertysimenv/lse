package Liberty.LSS.types;

/**
 * This class represents the type of types in the LSS programming
 * language.  Recall that <em>all</em> types are values, so this class
 * must also implement the <code>getType</code> method.  For this
 * type, the method returns an instance of this class since the type
 * of all types is represented by this class.
 */

public class TypeType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(Types.typeType);

    /**
     * Constructs an instance of <code>TypeType</code>.
     */
    public TypeType() {}

    /**
     * Compares the specified object with this type.  If the specified
     * object is a <code>TypeType</code> this method returns
     * <code>true</code>.
     *
     * @param   o   object to be compared for equality
     * @return <code>true</code> if the specified object is a
     *         <code>TypeType</code>
     */
    public boolean equals(Object o) {
	if(o instanceof TypeType)
	    return true;
	return false;
    }

    /**
     * Returns the hash code for this type.  The hash code of any
     * TypeType instance is defined to be the
     * <code>System.identityHashCode(Types.typeType)</code>.
     *
     * @return  the hash code of this type
     */
    public int hashCode() {
	return memoizedHashCode;
    }

    /**
     * Returns the string <code>"type"</code>.
     *
     * @return the string <code>"type"</code>
     */
    public String toString() {
	return "type";
    }
}
