package Liberty.LSS.types;

import java.util.*;
import Liberty.LSS.types.inference.*;

/**
 * This class represents a <em>type</em> in the LSS language.  Types
 * are also values in LSS.  The type of a type is always an instance
 * of the class {@link TypeType}.  This class is <code>abstract</code>
 * and is provided as the root of the type class hierachy.
 * Monomorphic types should inherit from {@link Type}.
 */

public abstract class Type implements Value
{
    public static final Set EMPTY_SET = new HashSet(0,1);

    /**
     * Returns the type of this <code>Value</code> instance.  The
     * returned type is always an instance of <code>TypeType</code>.
     *
     * @return The type of this value which is an instance of
     *         <code>TypeType</code>
     */
    public Type getType() {
	return Types.typeType;
    }

    /**
     * Returns the backend type of this <code>Value</code> instance.
     * The implementation here always throws a {@link
     * RuntimeTypeException} since the backend type may frequently
     * <em>not</em> be defined by implementing classes.
     *
     * @throws RuntimeTypeException  If this class has no backend type
     * @return The type of this value which is an instance of
     *         <code>TypeType</code>
     */
    public String getBackendType() {
	throw new RuntimeTypeException("Type " + this + 
				       " has no backend type");
    }	

    /**
     * Returns the backend type of this <code>Value</code> instance.
     * This implementation discards the parameter
     * <code>typesToNames</code> and calls {@link #getBackendType()}
     * since many implementing classes will not have any embedded
     * types.
     *
     * <p>An overriden implementation of this method <em>should</em>
     * emit type names rather than type definitions for embedded types
     * if there is a mapping between the type and its name in the
     * <code>typesToNames</code> map.
     *
     * @param typesToNames a map which maps <code>Type</code>s to
     *                     globally unique names
     * @throws RuntimeTypeException  If this class has no backend type
     * @return The type of this value which is an instance of
     *         <code>TypeType</code>
     */
    public String getBackendType(Map typesToNames) {
	return getBackendType();
    }

    public boolean isPolymorphic() {
	return false;
    }

    public boolean isBaseType() {
	return true;
    }

    public Type getBaseType() {
	return this;
    }

    /* This function returns an LValue that handles type specific
     * functionality.  The argument base is essentially an LValue
     * representing the "backing-store" for a variable. */
    public LValue getLValue(LValue base) 
	throws TypeException {
	return base;
    }

    /* This function returns an LValue that allows non-lvalues to have
     * subfields which are lvalues.  The returned lvalue should throw
     * an exception when the assign method is called, but return full
     * lvalues when subfield methods are invoked. */
    public LValue getLValue(Value base) 
	throws TypeException {
	throw new TypeException("Type " + this + " is not an lvalue");
    }

    /* The following three functions declare properties of this type.
     * A true return value from each function forces values of this
     * type to implement a certain interface and the LValue returned
     * by getLValue to implement a certain interface.  The following
     * table summarizes these requirements:
     *
     *  Function   | Type Interface | Value Interface | LValue Interface
     *-------------+----------------+-----------------+------------------
     * isIndexable |  IndexableType |    Indexable    |  IndexableLValue
     * isAggregate |  AggregateType |    Aggregate    |  AggregateLValue
     * isCallable  |  CallableType  |    Callable     |       N/A
     * isReference |     N/A        |    Reference    |  ReferenceLValue
     *
     * The LValue interface names seem like subclasses of LValue, but
     * they are in fact INTERFACES!
     */

    public boolean isIndexable() {
	return false;
    }

    public boolean isAggregate() {
	return false;
    }
    
    public boolean isCallable() {
	return false;
    }

    public boolean isReference() {
	return false;
    }

    /* For Type Inference */

    public ConstraintSatisfaction canBeEqual(Type t, ConstraintSource origin) {
	Set s = new HashSet();
	boolean b = this.equals(t);
	return new ConstraintSatisfaction(b,s);
    }

    public Type substitute(Map typeVarValues) {
	return this;
    }

    public Type substitute(Map typeVarValues, Map globalTypes) {
	return substitute(typeVarValues);
    }

    /* For outputting types */
    public Set dependsOn() {
	return EMPTY_SET;
    }
}
