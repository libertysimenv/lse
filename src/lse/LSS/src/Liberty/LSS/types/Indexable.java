package Liberty.LSS.types;

/* If a value implements this interface, then
 * value.getType().isIndexable() should be true */

public interface Indexable {
    public Value getSubValue(int index) 
	throws TypeException,
	       UninitializedVariableException;
}
