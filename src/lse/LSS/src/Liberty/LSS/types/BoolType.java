package Liberty.LSS.types;

public class BoolType extends Type
{
    /* Used to calculate the hashCode for this type */
    private static final int memoizedHashCode = 
	System.identityHashCode(new BoolType());

    public String toString() {
	return "boolean";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof BoolType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public String getBackendType() {
	return "boolean";
    }
}
