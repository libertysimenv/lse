package Liberty.LSS.types;

public class IntType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new IntType());

    public String toString() {
	return "int";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof IntType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public String getBackendType() {
	return "int";
    }
}

