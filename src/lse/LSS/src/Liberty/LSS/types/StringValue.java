package Liberty.LSS.types;

import java.text.*;
import java.util.*;

/* This class should be changed into an interface if there are
 * subtypes of string */

public class StringValue extends ValueBase {
    public static interface PieceManipulator {
	public void handleString(String s);
	public void handleType(Type t);
    }

    public static abstract class Piece {
	public abstract void manipulate(PieceManipulator m);
	public static Piece create(String s) {
	    return new StringPiece(s);
	}
	public static Piece create(Type t) {
	    return new TypePiece(t);
	}
	public abstract boolean CArgListEquals(Piece p);
    }

    private static class StringPiece extends Piece {
	private String str;
	public StringPiece(String s) {
	    str = s;
	}
	public void manipulate(PieceManipulator m) {
	    m.handleString(str);
	}
	public String toString() {
	    return str;
	}
	public boolean equals(Object o) {
	    if(o == null) return false;
	    if(o instanceof StringPiece) {
		return str.equals(((StringPiece)o).str);
	    } else {
		return false;
	    }
	}
	public boolean CArgListEquals(Piece p) {
	    if(p == null) return false;
	    if(p instanceof StringPiece) {
		String str1 = CArgListCanonicalizer.canonicalize(str);
		String str2 = 
		    CArgListCanonicalizer.canonicalize(((StringPiece)p).str);
		return str1.equals(str2);
	    } else {
		return false;
	    }
	}

	public int hashCode() {
	    return str.hashCode();
	}
    }

    private static class TypePiece extends Piece {
	private Type type;
	public TypePiece(Type t) {
	    type = t;
	}
	public void manipulate(PieceManipulator m) {
	    m.handleType(type);
	}
	public String toString() {
	    return type.toString();
	}
	public boolean equals(Object o) {
	    if(o == null) return false;
	    if(o instanceof TypePiece) {
		return type.equals(((TypePiece)o).type);
	    } else {
		return false;
	    }
	}
	public boolean CArgListEquals(Piece p) {
	    return equals(p);
	}
	public int hashCode() {
	    return type.hashCode();
	}
    }

    private Vector pieces;

    public StringValue(Vector v) {
	this(v, Types.stringType);
    }

    public StringValue(String s) {
	this(s, Types.stringType);
    }

    protected StringValue(String s, Type t) {
	this(buildVector(s), t);
    }

    protected StringValue(Vector v, Type t) {
	super(t);
	PieceManipulator m = new PieceManipulator() {
		public StringBuffer runningStr = null;
		public void handleType(Type t) {
		    /* Purge the running string */
		    runningStr = null;

		    /* Add this type */
		    pieces.add(Piece.create(t));
		}
		public void handleString(String s) {
		    if(runningStr == null) {
			runningStr = new StringBuffer(s);
			pieces.add(Piece.create(runningStr.toString()));
		    } else {
			runningStr.append(s);
			pieces.setElementAt(Piece.create(runningStr.toString()),
					    pieces.size() - 1);
		    }
		}
	    };

	if(v.size() == 0) {
	    v.add(Piece.create(""));
	}

	pieces = new Vector();
	Iterator i = v.iterator();
	while(i.hasNext()) {
	    Piece p = (Piece)i.next();
	    p.manipulate(m);
	}
    }

    public static Vector buildVector(String s) {
	Vector v = new Vector();
	v.add(StringValue.Piece.create(s));
	return v;
    }

    public static Vector buildVector(Type t) {
	Vector v = new Vector();
	v.add(StringValue.Piece.create(t));
	return v;
    }

    public Vector getValue() {
	return pieces;
    }

    public String getFlattenedValue() {
	Iterator i = pieces.iterator();
	StringBuffer b = new StringBuffer();
	while(i.hasNext()) {
	    Piece p = (Piece)i.next();
	    b.append(p.toString());
	}
	return b.toString();
    }

    public String getUnescapedValue() {
	return unescape(getFlattenedValue());
    }

    public static String unescape(String s) {
	StringCharacterIterator i = new StringCharacterIterator(s);
	String ret = "";
	char c;
	
	c = i.first();
	while(c != CharacterIterator.DONE) {
	    if(c == '\\') {
		c = i.next();
		switch(c) {
		case 'n':
		    ret += '\n';
		    break;
		case 'r':
		    ret += '\r';
		    break;
		case 't':
		    ret += '\t';
		    break;
		case '"':
		    ret += '"';
		    break;
		case CharacterIterator.DONE:
		    ret += '\\';
		    return ret;
		default:
		    ret += c;
		    break;
		}
	    } else {
		ret += c;
	    }
	    c = i.next();
	}
	return ret;
    }

    public String toString() {
	return "\"" + getFlattenedValue() + "\"";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.stringType)) {
	    Vector stringv = ((StringValue)v).getValue();
	    return (stringv.equals(getValue()));
	} else 
	    return false;
    }

    public boolean CArgListEquals(Value v) {
	if(v == null) return false;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.stringType)) {
	    Vector stringv1 = getValue();
	    Vector stringv2 = ((StringValue)v).getValue();
	    if(stringv1.size() != stringv2.size())
		return false;

	    int size = stringv1.size();
	    for(int i = 0; i < size; i++) {
		Piece p1 = (Piece)stringv1.elementAt(i);
		Piece p2 = (Piece)stringv2.elementAt(i);
		if(!p1.CArgListEquals(p2))
		    return false;
	    }
	    return true;
	} else 
	    return false;
    }

    public int hashCode() {
	return pieces.hashCode();
    }
}
