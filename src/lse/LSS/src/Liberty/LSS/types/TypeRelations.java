package Liberty.LSS.types;

import java.util.*;
import Liberty.LSS.*;

public class TypeRelations
{
    /* We must maintain the invariant that if isCompatible(t1,t2) is
     * true then isSubtype(t1,t2) is also true.  Therefore,
     * compatibility is a sub-relation of subtype*/
    public static boolean isCompatible(Type t1, Type t2) {
	/* 'a :<: 'a */
	if(t1.equals(t2)) return true;

	/* 'a[n] :<: 'b[] and
           'a[n] :<: 'b[n] if 'a :<: 'b */
	if(t1 instanceof ArrayType &&
	   t2 instanceof ArrayType) {
	    int s1 = ((ArrayType)t1).getSize();
	    int s2 = ((ArrayType)t2).getSize();
	    Type et1 = ((ArrayType)t1).getElementType();
	    Type et2 = ((ArrayType)t2).getElementType();
	    if(isCompatible(et1,et2) && (s1 == s2 || s2 == -1)) return true;
	}

	/* UnknownType :<: PortRefType
	   UnknownType :<: PortInstanceRefType
	   UnknownType :<: ParameterRefType
	   UnknownType :<: InstanceRefType */
	if(t1.equals(Types.unknownType)) {
	    if(t2.equals(Types.portRefType) ||
	       t2.equals(Types.portInstanceRefType) ||
	       t2.equals(Types.parameterRefType) ||
	       t2.equals(Types.instanceRefType)) return true;
	}

	/* NumericIntType :<: IntType
	   DeferredIntType :<: IntType */
	if(t2.equals(Types.intType)) {
	    if(t1.equals(Types.numericIntType) ||
	       t1.equals(Types.deferredIntType)) return true;
	}

	/* RuntimeParmType('a) :<: RuntimeParmType('b) if
           'a :<: 'b */
	if(t1 instanceof RuntimeParmType &&
	   t2 instanceof RuntimeParmType) {
	    Type pt1 = ((RuntimeParmType)t1).getParmType();
	    Type pt2 = ((RuntimeParmType)t2).getParmType();
	    if(isCompatible(pt1, pt2)) return true;
	}
	 
	return false;
    }

    public static boolean isSubtype(Type t1, Type t2) {
	/* 'a <: 'a */
	if(t1.equals(t2)) return true;

	/* StringType <: UserPointType(*) 
           StringType <: ControlPointType */
	if(t1.equals(Types.stringType)) {
	    if(t2.equals(Types.controlPointType) ||
	       t2 instanceof UserPointType) return true;
	}

	/* NumericIntType <: IntType
	   DeferredIntType <: IntType */
	if(t2.equals(Types.intType)) {
	    if(t1.equals(Types.numericIntType) ||
	       t1.equals(Types.deferredIntType)) return true;
	}

	/* NumericIntType <: FloatType */
	if(t1.equals(Types.numericIntType) &&
	   t2.equals(Types.floatType)) return true;

	/* StringType <: LiteralType */
	if(t1.equals(Types.stringType) &&
	   t2.equals(Types.literalType)) return true;

	/* {'a x; 'b y; 'c z; ... } <: {'a x; 'b y; 'c z'} */
	if(t1 instanceof StructType &&
	   t2 instanceof StructType) {
	    Set labels1 = ((StructType)t1).getElementNames();
	    Set labels2 = ((StructType)t2).getElementNames();
	    if(labels1.containsAll(labels2)) {
		Iterator i = labels2.iterator();
		boolean match = true;
		while(i.hasNext()) {
		    String field = (String)i.next();
		    Type ft1,ft2;
		    try {
			ft1 = ((StructType)t1).getElementType(field);
			ft2 = ((StructType)t2).getElementType(field);
		    } catch(TypeException e) {
			/* Can't happen */
			throw new RuntimeTypeException(e.getMessage());
		    }
		    if(!ft1.equals(ft2)) match = false;
		}
		if(match) return true;
	    }
	}

	/* 'a[n] <: 'b[] and
           'a[n] <: 'b[n] if 'a <: 'b */
	if(t1 instanceof ArrayType &&
	   t2 instanceof ArrayType) {
	    int s1 = ((ArrayType)t1).getSize();
	    int s2 = ((ArrayType)t2).getSize();
	    Type et1 = ((ArrayType)t1).getElementType();
	    Type et2 = ((ArrayType)t2).getElementType();
	    if(isSubtype(et1,et2) && (s1 == s2 || s2 == -1)) return true;
	}

        /* nil <: 'b[] and
           nil <: 'b[0] */
        if(t1 instanceof NilType &&
           t2 instanceof ArrayType) {
            if(((ArrayType)t2).getSize() == -1 ||
               ((ArrayType)t2).getSize() == 0)
                return true;
            return false;
        }
	
	/* UnknownType <: PortRefType
	   UnknownType <: PortInstanceRefType
	   UnknownType <: ParameterRefType
	   UnknownType <: InstanceRefType */
	if(t1.equals(Types.unknownType)) {
	    if(t2.equals(Types.portRefType) ||
	       t2.equals(Types.portInstanceRefType) ||
	       t2.equals(Types.parameterRefType) ||
	       t2.equals(Types.instanceRefType)) return true;
	}

	/* RuntimeParmType('a) <: RuntimeParmType('b) if
           'a :<: 'b */
	if(t1 instanceof RuntimeParmType &&
	   t2 instanceof RuntimeParmType) {
	    Type pt1 = ((RuntimeParmType)t1).getParmType();
	    Type pt2 = ((RuntimeParmType)t2).getParmType();
	    if(isCompatible(pt1, pt2)) return true;
	}

	/* GeneralExternalType <: ExternalType(*) */
	if(t1 instanceof GeneralExternalType &&
	   t2 instanceof ExternalType)
	    return true;

	return false;
    }

    /* Common idioms that are useful to define once */

    public static Value attemptDeref(Value v, CodeObtainable src)
	throws UninitializedVariableException,
	       TypeException {
	try {
	    return attemptDeref(v);
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(src, e.getMessage());
	}
    }

    public static Value attemptDeref(Value v) 
	throws UninitializedVariableException,
	       TypeException {
	if(v.getType().isReference())
	    return ((Reference)v).deref();
	else
	    return v;
    }
}
