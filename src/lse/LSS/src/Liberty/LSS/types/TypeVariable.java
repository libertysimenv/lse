package Liberty.LSS.types;

import Liberty.LSS.types.inference.*;
import java.util.*;

public class TypeVariable extends PolyType
{
    protected String name;
    protected boolean mustResolveFlag;

    private int memoizedHashCode;

    protected TypeVariable() { 
	mustResolveFlag = false;
	memoizedHashCode = 0;
    }

    public TypeVariable(String n) {
	this();
	name = "'" + n;
	memoizedHashCode = name.hashCode();
    }

    public TypeVariable(String n, boolean b) {
	this(n);
	mustResolveFlag = b;
    }

    public String toString() {
	return name;
    }

    public boolean mustResolve() {
	return mustResolveFlag;
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof TypeVariable)
	    return name.equals(((TypeVariable)t2).name);
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public ConstraintSatisfaction canBeEqual(Type t, ConstraintSource origin) {
	Set s = new HashSet();
	if(this.equals(t)) {
	    return new ConstraintSatisfaction(true,s);
	} else {
	    Map m = new HashMap();
	    m.put(this, Types.numericIntType);
	    Type substt = t.substitute(m);
	    if(!t.equals(substt)) {
		/* We have a circular definition here.  this != t, but
		 * t references this.  */
		return new ConstraintSatisfaction(false,s);
	    } else {
		s.add(new Constraint(this, t, origin));
		return new ConstraintSatisfaction(true,s);
	    }
	}
    }

    public Type substitute(Map typeVarValues) {
	/* This should ideally use containsKey, but does not to
	 * improve performance. */
	Type t = (Type)typeVarValues.get(this);
	if(t != null)
	    return t;
	else
	    return this;
    }
}

