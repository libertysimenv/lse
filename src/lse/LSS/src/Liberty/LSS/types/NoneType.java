package Liberty.LSS.types;

public class NoneType extends Type
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new NoneType());

    public String toString() {
	return "none";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof NoneType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }   

    public String getBackendType() {
	return "LSE_type_none";
    }
}
