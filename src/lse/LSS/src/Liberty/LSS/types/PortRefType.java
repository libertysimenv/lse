package Liberty.LSS.types;

import Liberty.LSS.CodeObtainable;
import Liberty.LSS.AST.Port;
import java.util.*;

public class PortRefType extends Type
    implements AggregateType, IndexableType
{
    private static final int memoizedHashCode = 
	System.identityHashCode(new PortRefType());

    public String toString() {
	return "port ref";
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof PortRefType)
	    return true;
	else
	    return false;
    }

    public int hashCode() {
	return memoizedHashCode;
    }

    public boolean isIndexable() {
	return true;
    }

    public boolean isAggregate() {
	return true;
    }

    public Type getSubValueType(int index) {
	return Types.portInstanceRefType;
    }

    public Value createIndexableValue(Vector v)
	throws TypeException {
	throw new TypeException("Cannot create port from port " + 
				"instance vector");
    }

    public Type getSubValueType(String field) 
	throws TypeException {
	if(field.equals("independent"))
	   return Types.boolType;
	else if(field.equals("handler"))
	    return Types.boolType;
	else if(field.equals("control")) 
	    return Types.controlPointType;
	else if(field.equals("width"))
	    return Types.numericIntType;
	else if(field.equals("connected"))
	    return Types.boolType;
	else
	    throw new TypeException(field + 
				    " is not a valid attribute on type " + 
				    this);
    }

    public LValue getLValue(LValue base) 
	throws TypeException {
	return new PortLValue(base);
    }

    public LValue getLValue(Value base) 
	throws TypeException {
	return new PortLValue(base);
    }
}

class PortLValue implements IndexableLValue, AggregateLValue
{
    Value port;
    LValue base;

   /* Invariants:

       1) port <: PortRef 
       2) PortRef <: base.type (since base will be asked to store
                                ParameterRef's)
       3) base.type <: PortRef (since port = base.value)
       2 + 3 => base.type == PortRef
    */

    public PortLValue(Value p) {
	/* LValues produced with this constructor have no backing
	 * store.  Therefore, they are effectively const variables.
	 * Any assignment directly to this LValue will throw an
	 * exception. */

	if(TypeRelations.isSubtype(p.getType(),Types.portRefType)) {
	    port = p;
	    base = null;
	} else {
	    throw new RuntimeTypeException("Cannot create a port lvalue " + 
					   "with data of type " + p.getType());
	}
    }
    
    public PortLValue(LValue b) {
	Type t = b.getType();
	while(!t.isBaseType())
	    t = t.getBaseType();

	if(t.equals(Types.portRefType)) {
	    base = b;
	    try {
		port = b.getValue();
	    } catch(UninitializedVariableException e) {
		port = null;
	    }
	} else {
	    throw new RuntimeTypeException("Cannot create a port lvalue " + 
					   "with data of type " + t);
	}
    }

    public void assign(Value v)
	throws TypeException {
	assign(v,null);
    }

    public void assign(Value v, CodeObtainable s) 
	throws TypeException {
	if(base != null)
	    base.assign(v,s);
	else
	    throw new TypeException("Cannot assign to port " + port);
    }

    public Type getType() {
	if(base != null)
	    return base.getType();
	else
	    return new ConstType(Types.portRefType);
    }

    public Value getValue() 
	throws UninitializedVariableException {
	if(port == null) {
	    throw new UninitializedVariableException("Port ref uninitialized");
	}
	return port;
    }

    public LValue getSubLValue(String field)
	throws TypeException {
	if(port == null) {
	    throw new TypeException("Port ref uninitialized");
	}

	if(port.getType().equals(Types.portRefType)) {
	    return ((Port)port).getSubLValue(field);
	} else {
	    LValue wrap = port.getType().getLValue(port);
	    if(wrap.getType().isAggregate())
		return ((AggregateLValue)wrap).getSubLValue(field);
	    else
		throw new TypeException("There is no subfield named " +
					field + " on port " +
					port);
	}
    }

    public LValue getSubLValue(int index) throws TypeException {
	throw new TypeException("Port instances are not lvalues");
    }

    public String toString() {
	if(base != null) return base.toString();
	else return port.toString();
    }
}
