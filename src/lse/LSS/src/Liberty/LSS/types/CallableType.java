package Liberty.LSS.types;

import java.util.*;
import Liberty.LSS.AST.*;

/* If a type implements this interface, then
 * isCallable() should be true */

public interface CallableType {
    public Type getReturnType()
	throws TypeException;
}
