package Liberty.LSS.types;

/* This class should be changed into an interface if there are
 * subtypes of float */

public class FloatValue extends ValueBase {
    private double value;

    public FloatValue(double v) {
	super(Types.floatType);
	value = v;
    }

    public FloatValue(Double v) {
	this(v.doubleValue());
    }

    public FloatValue(Float v) {
	this(v.floatValue());
    }

    public double getValue() {
	return value;
    }

    public String toString() {
	return new Double(value).toString();
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.floatType)) {
	    double floatv = ((FloatValue)v).getValue();
	    return (floatv == value);
	} else 
	    return false;
    }

    public int hashCode() {
	return (new Double(value)).hashCode();
    }
}
