package Liberty.LSS.types;

import java.text.*;
import java.util.*;

/* This class should be changed into an interface if there are
 * subtypes of literal */

/* This class is not an interface because it inherits from StringValue
 * and so does its subtype (actually its subtype is StringValue) */

public class LiteralValue extends StringValue {
    public LiteralValue(Vector v) {
	super(v, Types.literalType);
    }

    public LiteralValue(String s) {
	super(s, Types.literalType);
    }

    public String toString() {
	return getFlattenedValue();
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, Types.literalType)) {
	    Vector stringv = ((LiteralValue)v).getValue();
	    return (stringv.equals(getValue()));
	} else 
	    return false;
    }

    public int hashCode() {
	return getValue().hashCode();
    }
}
