package Liberty.LSS.types;

import Liberty.LSS.*;

public class RuntimeTypeException extends LSSRuntimeException
{
    public RuntimeTypeException(String m) {
	super(m);
    }

    public RuntimeTypeException(CodeObtainable o, String m) {
	super(o,m);
    }
}
