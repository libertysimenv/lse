package Liberty.LSS.types;

/* If an Lvalue implements this interface, then
 * lvalue.getType().isAggregate() should be true */

public interface AggregateLValue extends LValue {
    public LValue getSubLValue(String name) 
	throws TypeException;
}
