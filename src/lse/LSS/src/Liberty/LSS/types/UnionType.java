package Liberty.LSS.types;

import Liberty.LSS.types.inference.*;
import Liberty.LSS.*;
import java.util.*;

public class UnionType extends PolyType
{
    private Vector options;
    private Set dependencySet = null;

    public UnionType(List l) {
	options = new Vector(l);
    }

    public String toString() {
	Iterator i = options.iterator();
	String s = "";
	while(i.hasNext()) {
	    Type t = (Type)i.next();
	    s += t.toString();
	    if(i.hasNext()) 
		s += " | ";
	}
	return s;
    }

    public boolean equals(Object t2) {
	if(t2 == null) return false;
	if(t2 instanceof UnionType) {
	    UnionType ut2 = (UnionType)t2;
	    return options.equals(ut2.options);
	} else
	    return false;
    }

    public int hashCode() {
	return options.hashCode();
    }

    public List getOptions() {
	return options;
    }

    public ConstraintSatisfaction canBeEqual(Type t, ConstraintSource origin) {
	Iterator i = options.iterator();
	Vector v = new Vector();
	while(i.hasNext()) {
	    Type option = (Type)i.next();
	    Constraint c = new Constraint(option, t, origin);
	    ConstraintSatisfaction s = c.isSatisfiable();
	    if(s.isSatisfiable()) v.add(option);
	}

	Set s = new HashSet();
	if(v.isEmpty()) 
	    return new ConstraintSatisfaction(false, s);
	else {
	    Type t2;
	    if(v.size() > 1) 
		t2 = new UnionType(v);
	    else
		t2 = (Type)v.elementAt(0);
	    Constraint c = new Constraint(t2, t, origin);
	    s.add(c);
	    return new ConstraintSatisfaction(true, s);
	}

    }

    public Type substitute(Map typeVarValues) {
	Iterator i = options.iterator();
	Vector v = new Vector();
	while(i.hasNext()) {
	    Type t = (Type)i.next();
	    t = t.substitute(typeVarValues);
	    v.add(t);
	}
	return new UnionType(v);
    }

    public Set dependsOn() {
	if(dependencySet == null) {
	    dependencySet = new HashSet(options.size(), 1);
	    Iterator i = options.iterator();
	    while(i.hasNext()) {
		dependencySet.add(i.next());
	    }
	}
	return dependencySet;
    }
}
