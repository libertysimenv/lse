package Liberty.LSS.types;
import java.util.*;

public class EnumValue extends ValueBase {
    String value;

    public EnumValue(String v, EnumType t) {
	super(t);
	value = v;
    }

    public String toString() {
	return value.toString();
    }

    public String toCurlyString(Map typesToNames) {
	String typeName = (String)typesToNames.get(getType());
	return "LSEut_enumref(" + typeName+","+value.toString() + ")";
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Value))
	    return false;
	
	Value v = (Value)o;
	Type t = v.getType();
	if(TypeRelations.isSubtype(t, getType())) {
	    String enumv = ((EnumValue)v).value;
	    return (enumv.equals(value));
	} else 
	    return false;
    }

    public int hashCode() {
	return value.hashCode();
    }
}
