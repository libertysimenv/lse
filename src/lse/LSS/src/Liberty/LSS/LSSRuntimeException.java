package Liberty.LSS;

public abstract class LSSRuntimeException extends RuntimeException
{
    public LSSRuntimeException() {
	super();
    }
    
    public LSSRuntimeException(String message) {
	super(message);
    }

    public LSSRuntimeException(CodeObtainable o, String message) {
	super("Error on line " + o.getLine() +
	      " in " + o.getFileName() + ": " + message);
    }
}
