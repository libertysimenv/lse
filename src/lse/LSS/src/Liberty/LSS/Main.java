package Liberty.LSS;

import Liberty.util.*;
import Liberty.LSS.types.*;
import Liberty.LSS.AST.*;
import Liberty.LSS.IMR.*;

import java.util.*;
import java.io.*;

public class Main {
    public static void main(String[] args) {
    long lssStartTime = System.currentTimeMillis();
    String filename = null;//System.getProperty("Liberty.LSS.input_file");
    String outputDir = System.getProperty("Liberty.LSS.output_dir");
    String modulePath = System.getProperty("Liberty.LSS.mod_path");
    String debugMode = System.getProperty("debug_mode");

    if(outputDir == null) outputDir = ".";

    StatementList sl = null;
    Vector sv = new Vector();
    // Note: startup isn't part of parmdecl because it is technically
    // not parameters and it has to execute at the toplevel context instead
    // of the LSS package.
    StatementList parmdeclSL = null;
    StatementList builtinsSL = null;
    StatementList startupSL = null; 
    Collection topLevelParms;

    ExecutionContext c = createContext(modulePath);
    InstanceEnvironment env = c.instanceSpace.getCurrentEnvironment();

    try {
        /* Read and parse input */
        Reader in;
        String parmdeclFilename;
        String builtinsFilename;
        String startupFilename;

        builtinsFilename = c.getFileName("LSS_builtins.lss");
        in = new FileReader(builtinsFilename);
        builtinsSL = IncludeStatement.parseFile(in, builtinsFilename);

        parmdeclFilename = c.getFileName("framework" + File.separator +
					 "LSE_parmdecl.lss");
        in = new FileReader(parmdeclFilename);
        parmdeclSL = IncludeStatement.parseFile(in, parmdeclFilename);

        startupFilename = c.getFileName("framework" + File.separator +
					"LSE_startup.lss");
        in = new FileReader(startupFilename);
        startupSL = IncludeStatement.parseFile(in, startupFilename);

	if(args.length == 0) {
	    in = new InputStreamReader(System.in);
	    filename = "";

	    sl = IncludeStatement.parseFile(in, filename);
	} else {

	    for(int i = 0; i < args.length; i++) {
		filename = args[i];

		if(filename == "-") {
		    in = new InputStreamReader(System.in);
		    filename = "";
		} else {
		    in = new FileReader(filename);
		}
		
		sl = IncludeStatement.parseFile(in, filename);
		if (args.length > 1) {
		    sv.addAll(sl.getStatementVector());
		}
	    }
	}

    } catch(IOException e) {
        System.err.println(e.getMessage());
        System.exit(1);
    } catch(Exception e) { /* CUP produces this */
        System.err.println(e.getMessage());
        System.exit(1);
    }

    HashMap instances;
    LinkedHashMap globalTypes;
    Collection runtimeVars;
    try {
        bootstrapExecutionContext(c, parmdeclSL, builtinsSL, startupSL);
	instances = null;
	if (args.length > 1) {
	    sl = new StatementList(sv);
	}
	instances = processStatementList(sl, c);
	/*
	for(int i = 0; i < sv.size(); i++) {
	    instances = processStatementList((StatementList)sv.elementAt(i), 
					     c);
	}
	*/
        topLevelParms = processTopLevelParms(c);
        Module.copyCollectors(instances, c);

        /* Check to see if there is at least 1 instance */
        if(instances.size() == 0) {
        throw new TypeException("Error: The configuration" +
                    " has no instances");
        }

        /* CountPort.go(instances.values()); */

        /* Perform type inference */
        runtimeVars = c.instanceSpace.getRuntimeVars();
        ConnectionManagement.connectionsOk(instances.values());
        ConnectionManagement.inferTypes(instances,
                        c.instanceSpace,
                        topLevelParms,
                        runtimeVars,
                        env.getConstraints());
        globalTypes = c.instanceSpace.getGlobalTypes();
        globalTypes = InstanceSpace.filterGlobalTypes(globalTypes);


        /* Build adapters */
        LinkedHashSet adapterInstances = new LinkedHashSet();
        LinkedHashSet adapters = new LinkedHashSet();
        buildAdapters(new File(outputDir + File.separator + "adapters"),
              instances.values(), adapters, adapterInstances);

        /* Dump info files */
        Info info = new Info(outputDir + File.separator + "database");
        info.printTypes(globalTypes);
        info.printParameters(topLevelParms, globalTypes);
        info.printInstances(instances.values(), globalTypes);
        info.printInstances(adapterInstances, globalTypes);
        /* Top-level events/collectors */
        info.printEvents(c.instanceSpace.getEvents(), globalTypes);
        info.printCollectors(c.instanceSpace.getCollectors());
        /* Top-level runtime vars */
        info.printRuntimeVars(runtimeVars, globalTypes);
        Adapter.output(adapters);
	/* Top-level search path */
	info.printDomainSearchPath(c.instanceSpace.getCurrentEnvironment().
				   getLocalDomainSearchPath(),
				   c.instanceSpace.getCurrentEnvironment().
				   getHierDomainSearchPath());

        /* Domain Instances */
        Collection domainInstances = c.instanceSpace.getDomainInstances();
        domainInstances = DomainInstance.wrapCollection(domainInstances);
        info.printDomainInstances(domainInstances);

        info.close();
    } catch(LSSException e) {
        if(debugMode != null && debugMode.equals("yes"))
        e.printStackTrace();
        else
        System.err.println(e.getMessage());
        System.exit(1);
    } catch(IOException e) {
        System.err.println(e.getMessage());
    } catch(Exception e) { /* This is bad if it happens */
        e.printStackTrace();
        System.exit(1);
    }

    long lssEndTime = System.currentTimeMillis();
    System.out.println("Total LSS time: " +
                       (lssEndTime-lssStartTime)/1000.0 + "s");
    }

    public static ExecutionContext createContext(String modulePath) {
    ExecutionContext c = new ExecutionContext();

    Vector modulePathVector = new Vector();
    if(modulePath != null) {
        StringTokenizer pathMembers
        = new StringTokenizer(modulePath, File.pathSeparator);
        while(pathMembers.hasMoreTokens()) {
        modulePathVector.add(new File(pathMembers.nextToken()));
        }
    }
    c.setModulePath(modulePathVector);

    return c;
    }

    public static void bootstrapExecutionContext(ExecutionContext c,
                         StatementList parmdeclSL,
                         StatementList builtinsSL,
                         StatementList startupSL)
    throws LSSException {
    /* Create standard packages */
    Liberty.LSS.AST.Package defaultPackage =
        new Liberty.LSS.AST.Package(null);
    Liberty.LSS.AST.Package lssPackage =
        new Liberty.LSS.AST.Package("LSS");

    c.instanceSpace.addPackage(defaultPackage);
    c.instanceSpace.addPackage(lssPackage);
    c.resetPackageSearchList();

    c.setCurrentPackage(lssPackage);

    /* Built in values */
    c.addVariable("true", Types.boolType, new BoolValue(true));
    c.addVariable("false", Types.boolType, new BoolValue(false));
    c.addVariable("TRUE", Types.boolType, new BoolValue(true));
    c.addVariable("FALSE", Types.boolType, new BoolValue(false));

    /* Built in functions */
    Value structAdd = new StructAddFunction(c);
    Value structCreate = new StructCreateFunction(c);
    Value enumCreate = new EnumCreateFunction(c);
    Value enumValue = new EnumValueFunction(c);
    Value print = new PrintFunction(c);
    Value toLiteral = new ToLiteralFunction(c);
    Value toString = new ToStringFunction(c);
    Value punt = new PuntFunction(c);
    Value pointer = new PointerFunction(c);
    Value external = new ExternalFunction(c);
    Value externalValue = new ExternalValueFunction(c);
    Value warn = new WarnFunction(c);
    Value defaultDomainInstance = new DefaultDomainInstanceFunction(c);
    Value addToDomainSearchPath = new AddToDomainSearchPathFunction(c);
    Value constrain = new ConstrainFunction(c);
    Value nil = new NilValue();
    Value getPort = new GetPortFunction(c);
    Value getParameter = new GetParameterFunction(c);
    Value isRuntimed = new IsRuntimedFunction(c);

    c.addVariable("nil", nil.getType(), nil);
    c.addVariable("constrain", constrain.getType(), constrain);
    c.addVariable("structadd", structAdd.getType(), structAdd);
    c.addVariable("struct_create", structCreate.getType(), structCreate);
    c.addVariable("enum_create", enumCreate.getType(), enumCreate);
    c.addVariable("enum_value", enumValue.getType(), enumValue);
    c.addVariable("print", print.getType(), print);
    c.addVariable("punt", punt.getType(), punt);
    c.addVariable("warn", warn.getType(), warn);
    c.addVariable("to_literal", toLiteral.getType(), toLiteral);
    c.addVariable("to_string", toString.getType(), toString);
    c.addVariable("pointer", pointer.getType(), pointer);
    c.addVariable("external", external.getType(), external);
    c.addVariable("externalValue", externalValue.getType(), externalValue);
    c.addVariable("default_domain_instance",
              defaultDomainInstance.getType(),
              defaultDomainInstance);
    c.addVariable("add_to_domain_searchpath",
              addToDomainSearchPath.getType(),
              addToDomainSearchPath);
    c.addVariable("get_port", getPort.getType(), getPort);
    c.addVariable("get_parameter", getParameter.getType(), getParameter);
    c.addVariable("is_runtimed", isRuntimed.getType(), isRuntimed);

    /* Top-level events */
    Event start_of_timestep = new Event("start_of_timestep");
    Event end_of_timestep = new Event("end_of_timestep");
    c.instanceSpace.addEvent(start_of_timestep);
    c.instanceSpace.addEvent(end_of_timestep);

    processStatementList(parmdeclSL, c);
    processStatementList(builtinsSL, c);

    c.setCurrentPackage(defaultPackage);

    /* Make sure the top-level has the LSE_domain parameter */
    Set dc = c.instanceSpace.getDomainClasses();
    DomainInstanceMapType dimt = new DomainInstanceMapType(dc);

    try {
        InstanceEnvironment env = c.instanceSpace.getCurrentEnvironment();
        Liberty.LSS.AST.Parameter p =
        env.addParameter("LSE_domain", dimt, true, false);
        c.addVariable("LSE_domain",
            new ConstType(Types.parameterRefType), p);
        Value def = new DomainInstanceMapValue(dimt, new HashMap());
        p.getLValue().assign(def);
    } catch(SymbolAlreadyDefinedException e) {
        /* Bad things are afoot */
        throw new RuntimeTypeException(e.getMessage());
    } catch(UndefinedSymbolException e) {
        /* Bad things are afoot */
        throw new RuntimeTypeException(e.getMessage());
    } catch(TypeException e) {
        /* Bad things are afoot */
        throw new RuntimeTypeException(e.getMessage());
    } catch(UninitializedVariableException e) {
        /* Bad things are afoot */
        throw new RuntimeTypeException(e.getMessage());
    }

    processStatementList(startupSL, c);

    }

    public static HashMap processStatementList(StatementList sl,
                           ExecutionContext c)
    throws LSSException {
    if(sl == null) {
        /* No statements => no instances */
        return new HashMap();
    }

    InstanceEnvironment env;
    env = c.instanceSpace.getCurrentEnvironment();
    sl.execute(c);

    /* Get Instance Environment */
    InstanceEnvironment myEnv = c.instanceSpace.getCurrentEnvironment();

    /* Calculate child port widths */
    Module.calculateImplicitPortValues(myEnv);

    /* Process all top-level instances */
    LinkedHashMap instances = new LinkedHashMap();
    Liberty.util.Queue pendingInstances = myEnv.getPendingInstances();
    while(!pendingInstances.empty()) {
        InstanceEnvironment e =
        (InstanceEnvironment)pendingInstances.dequeue();
        e.setProcessedFlag();
        c.instanceSpace.setCurrentEnvironment(e);
        Module.propagateDomainParameter(c,myEnv);
        e.getModule().execute(c);

        Instance inst = e.getInstance();
        instances.put(inst.getName(), inst);
    }

    /* Run the connection log */
    HashMap typeVars = new HashMap();
    Module.copyConnections(null, env, typeVars);
    c.instanceSpace.setCurrentEnvironment(env);

    return instances;
    }

    public static Collection processTopLevelParms(ExecutionContext context)
    throws UninitializedVariableException {
    InstanceEnvironment e = context.instanceSpace.getCurrentEnvironment();
    LockableAttributeCollection c = e.getParameterValues();
    Iterator i = c.getVariableSet().iterator();
    Vector tlp = new Vector();

    while(i.hasNext()) {
        String var = (String)i.next();
        Value val, defVal;

        Type t;
        boolean exportFlag;

        try {
        val = c.getVariableValue(var);
        defVal = c.getVariableDefaultValue(var);
        t = c.getVariableType(var);
        exportFlag = e.getParameter(var).getExportFlag();
        } catch(UndefinedSymbolException ex) {
        /* This can't happen */
        throw new RuntimeTypeException(ex.getMessage());
        }

        while(!t.isBaseType())
        t = t.getBaseType();

        /* Skip code typed parameters */
        if(t instanceof UserPointType) {
        String msg = "Mysterious code-typed parameter " + var +
            " at the toplevel";
        throw new RuntimeTypeException(msg);
        } else {
        if(val == null) {
            String msg = "Error: " +
            "Toplevel parameter " + var + " must be set";
            throw new UninitializedVariableException(msg);
        }

        Liberty.LSS.IMR.Parameter p =
            new Liberty.LSS.IMR.Parameter(var, t, val, exportFlag);
        tlp.add(p);
        }
    }
    return tlp;
    }

    public static void buildAdapters(File dir,
                     Collection instances,
                     Set adapters,
                     Set adapterInstances) {

    Iterator i = instances.iterator();
    while(i.hasNext()) {
        Instance inst = (Instance)i.next();
        if(!inst.isLeaf()) {
        Adapter a = Adapter.build(dir, inst);
        if(a != null) {
            adapters.add(a);
            adapterInstances.add(a.getInstance());
        }

        buildAdapters(dir, inst.getSubInstances(), adapters,
                  adapterInstances);
        }
    }
    }
}
