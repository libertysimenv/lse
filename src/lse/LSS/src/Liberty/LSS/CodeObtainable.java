package Liberty.LSS;

public interface CodeObtainable {
    public String getOriginalCode();
    public int getLine();
    public String getFileName();
}
