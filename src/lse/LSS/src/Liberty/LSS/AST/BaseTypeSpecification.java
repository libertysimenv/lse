package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class BaseTypeSpecification extends TypeSpecification
{
    private TokenInfo typeToken;

    public BaseTypeSpecification(TokenInfo t) {
	typeToken = t;
    }

    public String getOriginalCode() {
	return typeToken.getOriginalCode();
    }
    
    public String toString() {
	return typeToken.toString();
    }

    public int getLine() {
	return typeToken.getLine();
    }

    public String getFileName() {
	return typeToken.getFileName();
    }

    public Type getType(ExecutionContext c) {
	if(typeToken.toString().equals("char"))
	    return Types.charType;
	if(typeToken.toString().equals("int"))
	    return Types.intType;
	if(typeToken.toString().equals("float") ||
	   typeToken.toString().equals("double"))
	    return Types.floatType;
	if(typeToken.toString().equals("void"))
	    return Types.voidType;
	if(typeToken.toString().equals("none"))
	    return Types.noneType;
	if(typeToken.toString().equals("boolean"))
	    return Types.boolType;
	if(typeToken.toString().equals("literal"))
	    return Types.literalType;
	if(typeToken.toString().equals("string"))
	    return Types.stringType;
	if(typeToken.toString().equals("type"))
	    return Types.typeType;
	return null;
    }
}
