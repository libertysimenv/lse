package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public interface VariableCollection
{
    public boolean variableDefined(String var);
    public Type getVariableType(String var)
	throws UndefinedSymbolException;
    public Value getVariableValue(String var) 
	throws UndefinedSymbolException;
    public void setVariableValue(String var, Value v)
	throws UndefinedSymbolException,
	       TypeException;
    public void initVariable(String var, Value v) 
	throws TypeException,
	       UndefinedSymbolException;
    public void addVariable(String var, Type t, Value v)
	throws SymbolAlreadyDefinedException,
	       EmptyContextException,
	       TypeException;
    public void overloadFunctionType(String var, Type t)
	throws SymbolAlreadyDefinedException,
	       EmptyContextException,
	       TypeException;
    public void overloadFunctionValue(String var, Value v)
	throws UndefinedSymbolException,
	       EmptyContextException,
	       TypeException;
    public Set getVariableSet();
}
