package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ExpressionStatement extends Statement {
    private TokenInfo semi;
    private Expression expr;

    public ExpressionStatement(Expression e, TokenInfo s) {
	expr = e;
	semi = s;
    }

    public String getOriginalCode() {
	String s = "";
	if(expr != null)
	    s += expr.getOriginalCode();
	s += semi.getOriginalCode();
	return s;
    }

    public String toString() {
	if(expr != null)
	    return expr.toString() + semi.toString();
	else
	    return semi.toString();
    }

    public Expression getExpression() {
	return expr;
    }

    public int getLine() {
	return expr.getLine();
    }

    public String getFileName() {
	return expr.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	if(expr != null)
	    expr.evaluate(c);
    }
}
