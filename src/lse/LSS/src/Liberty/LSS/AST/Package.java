package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.util.*;
import Liberty.LSS.IMR.Event;

import java.util.*;
import java.io.*;


public class Package extends StateCollection
{
    private String name;

    public Package(String name)
    {
	super();
	this.name = name;
    }

    /* Copy Constructor */
    private Package(Package p) {
	super(p);
	name = p.name;
    }

    protected StateCollection cloneState() {
	return new Package(this);
    }

    /* Returned package cannot have new symnols added to it */
    public StateCollection checkpointState() {
	Package p = (Package)super.checkpointState();
	return p;
    }

    public Package checkpoint() {
	return (Package)checkpointState();
    }

    public String getName() {
	return name;
    }

    public String qualifySymbol(String var) {
	if(getName() == null) return var;
	return getName() + "::" + var;
    }

    /* There is no notion of scope in packages so just make these
     * functions except */
    public StateCollection enterScope() {
	throw new RuntimeTypeException("Cannot change scope of package");
    }

    public StateCollection leaveScope() {
	throw new RuntimeTypeException("Cannot change scope of package");
    }
}
