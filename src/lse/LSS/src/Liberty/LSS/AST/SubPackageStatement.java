package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import java.util.*;

public class SubPackageStatement extends Statement
{
    protected TokenInfo packageToken;
    protected DottedIdentList packageName;
    protected CompoundStatement body;

    public SubPackageStatement(TokenInfo packageToken, DottedIdentList name,
			       CompoundStatement body) {
	this.packageToken = packageToken;
	this.packageName = name;
	this.body = body;
    }

    public String getOriginalCode() {
	return packageToken.getOriginalCode() +
	    packageName.getOriginalCode() + 
	    body.getOriginalCode();
    }

    public String toString() {
	return packageToken.toString() + 
	    " " + packageName.toString() + body.toString();
    }

    public int getLine() {
	return packageToken.getLine();
    }
    
    public String getFileName() {
	return packageToken.getFileName();
    }

    public void execute(ExecutionContext c)
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	String currentPkgName = c.getCurrentPackage().getName();
	String pkgName = (currentPkgName == null ? "" : currentPkgName + ".") +
	    packageName.toString();
	
	/* Make sure this package isn't already defined */
	try {
	    c.instanceSpace.getPackage(pkgName);
	    throw new SymbolAlreadyDefinedException(this, "Package " +
						pkgName + " already exists");
	} catch(UndefinedSymbolException e) {
	    /* Package doesn't exist, so lets continue...*/
	}

	/* Check to see if this package is already being imported */
	Stack packageProcessStack = c.instanceSpace.getPackageProcessStack();
	if(packageProcessStack.search(pkgName) != -1) {
	    packageProcessStack.push(pkgName);
	    throw new TypeException(this, "Recursive package inclusion.  " + 
				    "Import stack is: " + packageProcessStack);
	}
	
	/* Seems like we're okay */
	Package oldPackage = c.getCurrentPackage();
	List oldSearchList = c.getPackageSearchList();

	Package p = new Package(pkgName);
	c.setCurrentPackage(p);
	List newSearchList = new Vector(oldSearchList);
        newSearchList.add(0, p);
	c.setPackageSearchList(newSearchList);

	packageProcessStack.push(pkgName);
	StatementList statements = body.getStatementList();
	if(statements != null) statements.execute(c);
	packageProcessStack.pop();

	c.setPackageSearchList(oldSearchList);
	c.setCurrentPackage(oldPackage);
	c.instanceSpace.addPackage(p);
    }
}
