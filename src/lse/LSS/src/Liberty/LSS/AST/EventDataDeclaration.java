package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class EventDataDeclaration implements CodeObtainable
{
    private Identifier name;
    private TokenInfo colon, semi;
    private StringLiteral type;

    public EventDataDeclaration(Identifier n, TokenInfo c,
				StringLiteral t, TokenInfo s) {
	name = n;
	colon = c;
	type = t;
	semi = s;
    }

    public String getOriginalCode() {
	return name.getOriginalCode() + 
	    colon.getOriginalCode() +
	    type.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return name.toString() + " " +
	    colon.toString() + " " +
	    type.toString() + 
	    semi.toString();
    }

    public int getLine() {
	return name.getLine();
    }

    public String getFileName() {
	return name.getFileName();
    }

    public String getName() {
	return name.toString();
    }

    public StringValue getType(ExecutionContext c)
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	return (StringValue)type.evaluate(c);
    }
}
