package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class IdentifierLValue implements LValue
{
    private VariableCollection context;
    private final String id;
    private Value value;
    private final Type type;

    public IdentifierLValue(VariableCollection c, String i) 
	throws UndefinedSymbolException {
	context = c;
	id = i;
	value = context.getVariableValue(id);
	type = context.getVariableType(id);
    }

    public void assign(Value v)
	throws TypeException {
	assign(v, null);
    }
    
    public void assign(Value v, CodeObtainable o) throws TypeException {
	try {
	    Type varType = type;
	    while(!varType.isBaseType())
		varType = varType.getBaseType();

	    v = Coerce.assignCoerce(v, varType, o);
	    
	    /* Do the actual assignment */
	    context.setVariableValue(id, v);
	    value = v;
	} catch(UndefinedSymbolException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public Type getType() {
	return type;
    }

    public Value getValue() throws UninitializedVariableException {
	if(value == null) {
	    throw new UninitializedVariableException("Symbol " + id + 
						     " has not " +
						     "been initialized");
	}

	return value;
    }

    public String toString() {
	return id;
    }
}
