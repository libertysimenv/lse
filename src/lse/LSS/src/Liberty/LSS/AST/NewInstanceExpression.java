package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class NewInstanceExpression extends ExpressionBase {
    private TokenInfo newToken, instanceToken;
    private TokenInfo lparen, rparen, comma;
    private Expression instanceNameExpr;
    private Identifier moduleName;

    public NewInstanceExpression(TokenInfo n, TokenInfo i,
				 TokenInfo lp, Expression e,
				 TokenInfo c, Identifier m,
				 TokenInfo rp) {
	newToken = n;
	instanceToken = i;
	lparen = lp;
	instanceNameExpr = e;
	comma = c;
	moduleName = m;
	rparen = rp;
    }

    public String getOriginalCode() {
	return newToken.getOriginalCode() +
	    instanceToken.getOriginalCode() +
	    lparen.getOriginalCode() +
	    instanceNameExpr.getOriginalCode() +
	    comma.getOriginalCode() +
	    moduleName.getOriginalCode() +
	    rparen.getOriginalCode();
    }

    public String toString() {
	return newToken.toString() + " " +
	    instanceToken.toString() +
	    lparen.toString() +
	    instanceNameExpr.toString() +
	    comma.toString() + " " +
	    moduleName.toString() +
	    rparen.toString();
    }

    public int getLine() {
	return newToken.getLine();
    }

    public String getFileName() {
	return newToken.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Value v = instanceNameExpr.evaluate(c);
	if(!TypeRelations.isSubtype(v.getType(), Types.stringType))
	    v = TypeRelations.attemptDeref(v,this);

	if(!TypeRelations.isSubtype(v.getType(), Types.stringType))
	    throw new TypeException(this, "instance name in new" + 
				    " expression " + 
				    "must have type string not " + 
				    v.getType());
	String name = ((StringValue)v).getFlattenedValue();
	return InstanceDeclaration.newInstance(c, this, 
					       moduleName.toString(),
					       name);
    }
}
