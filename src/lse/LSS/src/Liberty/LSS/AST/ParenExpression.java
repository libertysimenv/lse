package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ParenExpression extends ExpressionBase
{
    private TokenInfo lparen, rparen;
    private Expression expr;

    public ParenExpression(TokenInfo lp, Expression e, TokenInfo rp) {
	lparen = lp;
	rparen = rp;
	expr = e;
    }

    public String getOriginalCode() {
	return lparen.getOriginalCode() + expr.getOriginalCode() + 
	    rparen.getOriginalCode();
    }

    public String toString() {
	return lparen.toString() + expr.toString() + rparen.toString();
    }

    public int getLine() {
	return lparen.getLine();
    }

    public String getFileName() {
	return lparen.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException, 
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	return expr.evaluate(c);
    }

    public LValue getLValue(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	return expr.getLValue(c);
    }
}
