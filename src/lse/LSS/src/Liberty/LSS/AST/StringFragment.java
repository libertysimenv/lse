package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import java.util.*;

public class StringFragment implements CodeObtainable
{
    TokenInfo token;
    String fragment;

    TokenInfo embedBegin, embedEnd;
    Expression expr;

    public StringFragment(String s, TokenInfo token) {
	fragment = s;
	this.token = token;
	expr = null;
    }

    public StringFragment(TokenInfo eb, Expression ex, TokenInfo ee) {
	embedBegin = eb;
	expr = ex;
	embedEnd = ee;
	fragment = null;
    }

    public String getOriginalCode() {
	if(fragment != null) {
	    return token.getOriginalCode();
	} else {
	    return embedBegin.getOriginalCode() +
		expr.getOriginalCode() +
		embedEnd.getOriginalCode();
	}
    }

    public int getLine() {
	if(fragment != null)
	    return token.getLine();
	else
	    return embedBegin.getLine();
    }

    public String getFileName() {
	if(fragment != null) 
	    return token.getFileName();
	else
	    return embedBegin.getFileName();
    }

    public String toString() {
	if(fragment != null) {
	    return fragment;
	} else {
	    return embedBegin.toString() +
		expr.toString() +
		embedEnd.toString();
	}
    }

    public Vector evaluateFragment(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	if(fragment != null) 
	    return StringValue.buildVector(fragment);
	
	Value v = expr.evaluate(c);
	v = TypeRelations.attemptDeref(v, this);
	v = Coerce.noaryInvoke(v, this);

	if(v.getType().equals(Types.stringType)) {
	    Vector vec = new Vector();
	    vec.add(StringValue.Piece.create("\""));
	    vec.addAll(((StringValue)v).getValue());
	    vec.add(StringValue.Piece.create("\""));
	    return vec;
	} else if(v.getType().equals(Types.literalType)) {
	    return new Vector(((LiteralValue)v).getValue());
	} else if(v.getType().equals(Types.typeType)) {
	    Type t = (Type)v;
	    return StringValue.buildVector(t);
	} else if (v.getType() instanceof EnumType) {
	    HashMap typesToNames = c.instanceSpace.getGlobalTypes();
	    return StringValue.buildVector(((EnumValue)v).toCurlyString(typesToNames));
	} else if (v.getType().equals(Types.runtimeVarRefType)) {
	    return StringValue.buildVector(((RuntimeVarRefValue)v).toCurlyString());
	} else if (v.getType().equals(Types.deferredIntType)) {
	    return StringValue.buildVector((Type)v);
	} else if (v.getType().equals(Types.domainInstanceRefType)) {
	    return StringValue.buildVector(((DomainInstanceRefValue)v).toCurlyString());
	} else
	    return StringValue.buildVector(v.toString());
    }
}
