package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class WarnFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType warnType;

    static {
	Vector v = new Vector();
	v.add(Types.stringType);
	warnType = new FunctionType(v, Types.voidType);
    }

    public WarnFunction(ExecutionContext c) {
	super(warnType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite) 
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	if(argValues.size() != 1)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + argValues.size() +
					" arguments");

	String str;
	Value v = (Value)argValues.elementAt(0);
	try {
	    v = TypeRelations.attemptDeref(v);
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}

	if(TypeRelations.isSubtype(v.getType(), Types.stringType)) {
	    StringValue s = (StringValue)v;
	    str = s.getUnescapedValue();
	    System.err.println("Warning: " + str);
	} else {
	    throw new FunctionException("Argument 1 in call to " +
					"function with type " + getType() + 
					" must have type " +
					Types.stringType);
	}

	return new VoidValue();
    }
}
