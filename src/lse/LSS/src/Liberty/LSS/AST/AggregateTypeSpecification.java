package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class AggregateTypeSpecification extends TypeSpecification
{
    private TokenInfo aggregateToken;
    private TokenInfo lbrace,rbrace;
    private AggregateItemList items;

    public AggregateTypeSpecification(TokenInfo a, TokenInfo lb, 
				      AggregateItemList l, TokenInfo rb) {
	aggregateToken = a;
	lbrace = lb;
	rbrace = rb;
	items = l;
    }

    public String getOriginalCode() {
	return aggregateToken.getOriginalCode() + 
	    lbrace.getOriginalCode() +
	    items.getOriginalCode() +
	    rbrace.getOriginalCode();
    }
    
    public String toString() {
	return aggregateToken.toString() + " " +
	    lbrace.toString() + 
	    items.toString() +
	    rbrace.toString();
    }

    public int getLine() {
	return aggregateToken.getLine();
    }

    public String getFileName() {
	return aggregateToken.getFileName();
    }

    public Type getType(ExecutionContext c) 
	throws TypeException,
	       UninitializedVariableException {
	Iterator i = items.iterator();
	Vector types = new Vector();
	Vector names = new Vector();

	while(i.hasNext()) {
	    ArgumentDeclaration d = (ArgumentDeclaration)i.next();
	    names.add(d.getName().toString());
	    types.add(TypeSpecification.getType(c, d.getTypeSpecification()));
	}

	try {
	    Type newType = new StructType(types, names);
	    c.instanceSpace.addGlobalType(newType);
	    return newType;
	} catch(TypeException e) {
	    throw new TypeException("Error on line " + getLine() + 
				    " of " + getFileName() + 
				    ": " + e.getMessage());
	}
    }
}
