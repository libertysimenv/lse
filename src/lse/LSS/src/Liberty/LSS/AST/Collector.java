package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class Collector
{
    HashMap attributes;
    String eventName;
    String[] instanceName;
    CollectorDeclaration decl;

    public Collector(CollectorDeclaration d,
		     String e, String[] i) {
	decl = d;
	instanceName = (String[])i.clone();
	eventName = e;

	attributes = new HashMap();
	attributes.put("header", null);
	attributes.put("decl", null);
	attributes.put("init", null);
	attributes.put("record", null);
	attributes.put("report", null);
    }

    public CollectorDeclaration getDeclaration() {
	return decl;
    }

    public void set(String attr, StringValue v) 
	throws UndefinedSymbolException,
	       TypeException {
	if(attributes.containsKey(attr)) {
	    StringValue oldv = (StringValue)attributes.get(attr);
	    if(oldv == null)
		attributes.put(attr, v);
	    else throw new TypeException("You can only set " + 
					 attr + " once.");
	} else
	    throw new UndefinedSymbolException("Field " + attr + "does not" + 
					       " exist on collectors.");
    }

    public StringValue get(String attr) {
	return (StringValue)attributes.get(attr);
    }

    public String getEventName() {
	return eventName;
    }

    public String[] getInstanceName() {
	return instanceName;
    }
}
