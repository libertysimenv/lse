package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.AST.*;
import Liberty.util.*;
import java.util.*;
import java.io.*;
import Liberty.LSS.IMR.Event;

public abstract class AbstractVariableCollection 
    implements VariableCollection
{
    private BasicMap typingContext;

    protected void init(BasicMap t) {
	typingContext = t;
    }

    public String qualifySymbol(String var) {
	return var;
    }

    public boolean variableDefined(String var) {
	return localVariableDefined(var);
    }

    private boolean localVariableDefined(String var) {
	return typingContext.containsKey(var);
    }

    public boolean canDefineVar(String var) {
	return !localVariableDefined(var);
    }

    public Type getVariableType(String var) 
	throws UndefinedSymbolException {
	if(!localVariableDefined(var))
	    throw new UndefinedSymbolException("Symbol " + 
					       qualifySymbol(var) + 
					       " is not defined");
	return (Type)typingContext.get(var);
    }


    public void addVariable(String var, Type t, Value v)
	throws SymbolAlreadyDefinedException,
	       TypeException {
	if(!canDefineVar(var))
	    throw new SymbolAlreadyDefinedException("Symbol " +
						    qualifySymbol(var) + 
						    " already defined");

	/* This is a giant hack and should be remedied by a better
	 * class hierarchy */
	if(typingContext instanceof ScopedSymbolTable) {
	    ((ScopedSymbolTable)typingContext).add(var,t);
	} else {
	    typingContext.put(var,t);
	}
    }

    public void overloadFunctionType(String var, Type t) 
	throws TypeException,
	       SymbolAlreadyDefinedException {
	if(canDefineVar(var))
	    addVariable(var, new ConstType(t), null);
	else {
	    Type oldType = (Type)typingContext.get(var);
	    Vector types = new Vector();

	    if(!(oldType instanceof ConstType))
		throw new SymbolAlreadyDefinedException("Symbol " +
							qualifySymbol(var) + 
							" already defined");
	    oldType = oldType.getBaseType();

	    if(oldType instanceof FunctionType) {
		types.add(oldType);
	    } else if(oldType instanceof OverloadedFunctionType) {
		OverloadedFunctionType oft = (OverloadedFunctionType)oldType;
		types.addAll(oft.getFunctionTypes());
	    } else {
		throw new SymbolAlreadyDefinedException("Symbol " +
							qualifySymbol(var) + 
							" already defined");
	    }
	    types.add(t);
	    Type newType = new OverloadedFunctionType(types);
	    typingContext.put(var, new ConstType(newType));
	}
    }

    public Set getVariableSet() {
	return typingContext.keySet();
    }
   
    protected void typeCheck(String var, Value v) 
    throws UndefinedSymbolException,
	   TypeException {
	Type t2 = v.getType();
	Type t1 = getVariableType(var);
	while(!t1.isBaseType())
	    t1 = t1.getBaseType();
	
	/* This function does no coercion.  Value must already have the
	   correct type */
	if(!TypeRelations.isCompatible(t2,t1)) {
	    throw new TypeException("Cannot assign value of type " + t2 + 
				    " to symbol " + var + ":"+t1);
	}
    }
    
    protected boolean isConst(Type t) {
	while(!t.isBaseType()) {
	    if(t instanceof ConstType)
		return true;
	    t = t.getBaseType();
	}
	return false;
    }
}
