package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import java.io.*;

public class StringLiteral extends ExpressionBase {
    TokenInfo openLiteral;
    TokenInfo closeLiteral;
    Vector fragments;

    public StringLiteral(TokenInfo open, Vector v, TokenInfo close) {
	openLiteral = open;
	fragments = v;
	closeLiteral = close;
    }

    public String getOriginalCode() {
	String s = "";
	Iterator i = fragments.iterator();
	while(i.hasNext()) {
	    CodeObtainable o = (CodeObtainable)i.next();
	    s += o.getOriginalCode();
	}

	return openLiteral.getOriginalCode() + 
	    s + closeLiteral.getOriginalCode();
    }

    public int getLine() {
	return openLiteral.getLine();
    }

    public String getFileName() {
	return openLiteral.getFileName();
    }

    public String toString() {
	String s = "";
	Iterator i = fragments.iterator();
	while(i.hasNext()) {
	    Object o = i.next();
	    s += o.toString();
	}
	return openLiteral.toString() + 
	    s + closeLiteral.toString();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Iterator i = fragments.iterator();
	Vector v = new Vector();
	while(i.hasNext()) {
	    StringFragment f = (StringFragment)i.next();
	    v.addAll(f.evaluateFragment(c));
	}

	return new StringValue(v);
    }
}
