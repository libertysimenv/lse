package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class SubfieldExpression extends ExpressionBase
{
    private TokenInfo dot;
    private Expression expr;
    private Identifier id;

    public SubfieldExpression(Expression e, TokenInfo d,
			      Identifier i) {
	dot = d;
	expr = e;
	id = i;
    }

    public static Expression create(DottedIdentList list) {
	Vector v = list.getIdentVector();
	int pos = 1;
	Expression e = (Expression)v.elementAt(0);
	while(v.size() - pos + 1 >= 2) {
	    e = new 
		SubfieldExpression(e, (TokenInfo)v.elementAt(pos), 
				   (Identifier)v.elementAt(pos+1));
	    pos += 2;
	}
	return e;
    }

    public String getOriginalCode() {
	return expr.getOriginalCode() + 
	    dot.getOriginalCode() + 
	    id.getOriginalCode();
    }

    public String toString() {
	return expr.toString() +
	    dot.toString() + 
	    id.toString();
    }

    public int getLine() {
	return expr.getLine();
    }
    
    public String getFileName() {
	return expr.getFileName();
    }

    public static Value getSubValue(Value v, String field,
				    CodeObtainable src) 
	throws TypeException,
	       UninitializedVariableException {
	Aggregate aggregate;

	try {
	    /* Try to deref if we are a reference and not an aggregate */
	    if(!v.getType().isAggregate())
		v = TypeRelations.attemptDeref(v);
	    
	    /* Are we really an aggregate? */
	    if(!v.getType().isAggregate()) 
		throw new TypeException("Value has no subfield named " + 
					field);
	    
	    aggregate = (Aggregate)v;
	    return aggregate.getSubValue(field);
	} catch(TypeException e) {
	    throw new TypeException(src, e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(src, e.getMessage());
	}
    }

    public static LValue getSubLValue(LValue lv, String field,
				      CodeObtainable src) 
	throws TypeException,
	       UninitializedVariableException {
	AggregateLValue aggregate;

	try {
	    /* Try to deref if we are a reference and not an aggregate */
	    if(!lv.getType().isAggregate() &&
	       lv.getType().isReference()) {
		lv = ((ReferenceLValue)lv).deref();
	    }
	    
	    /* Are we really an aggregate? */
	    if(!lv.getType().isAggregate()) 
		throw new TypeException("Value has no subfield named " + 
					field);
	    
	    aggregate = (AggregateLValue)lv;
	    return aggregate.getSubLValue(field);
	} catch(TypeException e) {
	    throw new TypeException(src, e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(src, e.getMessage());
	}
    }


    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value aggregate = expr.evaluate(c);
	return getSubValue(aggregate, id.toString(),this);
    }

    public LValue getLValue(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	LValue lv = expr.getLValue(c);
	return getSubLValue(lv, id.toString(), this);
    }
}

