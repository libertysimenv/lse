package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class IdBase implements CodeObtainable {
    private String id;
    private TokenInfo token;

    public IdBase(String v, TokenInfo ti) {
	token = ti;
	id = v;
    }

    public String toString() {
	return id;
    }
    
    public TokenInfo getToken() {
	return token;
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(o instanceof IdBase)
	    return this.toString().equals(o.toString());
	else return false;
    }

    public int hashCode() {
	return this.toString().hashCode();
    }

    public String getOriginalCode() {
	return token.getOriginalCode();
    }
    
    public int getLine() {
	return token.getLine();
    }

    public String getFileName() {
	return token.getFileName();
    }
}
