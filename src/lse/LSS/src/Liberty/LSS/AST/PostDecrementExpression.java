package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class PostDecrementExpression extends UnaryOpExpression
{
    public PostDecrementExpression(Expression e, TokenInfo o) {
	super(o,e);
    }

    public String getOriginalCode() {
	return expr.getOriginalCode() + operator.getOriginalCode();
    }

    public String toString() {
	return expr.toString() + operator.toString();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	try {
	    LValue lv = getExpression().getLValue(c);
	    Value v = lv.getValue();
	    lv.assign(Operate.decrement(v));
	    return v;
	} catch(TypeException e) {
	    throw new TypeException("Error on line " + this.getLine() + 
				    " of " + this.getFileName() + 
				    ": " + e.getMessage());
	}
    }
}
