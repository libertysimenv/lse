package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ReturnException extends ControlTransferException {
    private ReturnStatement returnStatement;
    private Value value;

    public ReturnException(Value v, ReturnStatement s) {
	value = v;
	returnStatement = s;
    }

    public ReturnException(ReturnStatement s) {
	value = new VoidValue();
	returnStatement = s;
    }

    public ReturnStatement getStatement() {
	return returnStatement;
    }

    public Value getValue() {
	return value;
    }

    public String getMessage() {
	return "Error: return statement on line " + returnStatement.getLine() +
	    " of " + returnStatement.getFileName() + " is not inside a function";
    }
}
