package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class UnaryTypeOfExpression extends UnaryOpExpression
{
    public UnaryTypeOfExpression(TokenInfo o, Expression e) {
	super(o,e);
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value v = this.getExpression().evaluate(c);

	InstanceEnvironment e = c.instanceSpace.getCurrentEnvironment();
	String fqn = e.getFullyQualifiedName();
	if(fqn == null)
	    fqn = "";
	else
	    fqn = fqn + ".";

	if(v.getType().equals(Types.unknownType)) {
	    return new TypeVariable("'" + fqn + ((UnknownValue)v).toString(), true);
	} if(TypeRelations.isSubtype(v.getType(), Types.portRefType)) {
	    return new TypeVariable("'" + fqn + ((Port)v).getName(), true);
	} else {
	    throw new TypeException(this.getExpression(), 
				    "Expression must have type " + Types.portRefType);
	}
    }
}
