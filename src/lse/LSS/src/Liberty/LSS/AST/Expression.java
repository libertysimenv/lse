package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public interface Expression extends CodeObtainable, Evaluatable, Assignable {
}
