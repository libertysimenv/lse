package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ArrayIndexExpression extends ExpressionBase
{
    private TokenInfo lbrack, rbrack;
    private Expression expr,index;
    
    public ArrayIndexExpression(Expression e, TokenInfo lb,
				Expression i, TokenInfo rb) {
	lbrack = lb;
	rbrack = rb;
	expr = e;
	index = i;
    }
    
    public String getOriginalCode() {
	String indexStr;
	if(index == null)
	    indexStr = "";
	else
	    indexStr = index.getOriginalCode();
	
	return expr.getOriginalCode() + 
	    lbrack.getOriginalCode() + 
	    indexStr +
	    rbrack.getOriginalCode();
    }
    
    public String toString() {
	String indexStr;
	if(index == null)
	    indexStr = "";
	else
	    indexStr = index.toString();
	
	return expr.toString() +
	    lbrack.toString() + 
	    indexStr +
	    rbrack.toString();
    }
    
    public Expression getArrayExpression() {
	return expr;
    }
    
    public Expression getIndexExpression() {
	return index;
    }
    
    public int getLine() {
	return expr.getLine();
    }
    
    public String getFileName() {
	return expr.getFileName();
    }
    
    protected int getIndex(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value iValue = index.evaluate(c);
	if(!TypeRelations.isSubtype(iValue.getType(), Types.numericIntType))
	    iValue = TypeRelations.attemptDeref(iValue, index);
	
	if(TypeRelations.isSubtype(iValue.getType(), Types.numericIntType)) {
	    int i = (int)((NumericIntValue)iValue).getValue();
	    return i;
	} else {
	    throw new TypeException(index, "Array indexes must have type " + 
				    Types.numericIntType + 
				    " not " + iValue.getType());
	}
    }

    protected Value getIndexValue(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value iValue = index.evaluate(c);
	if(!TypeRelations.isSubtype(iValue.getType(), Types.numericIntType) &&
	   !(iValue instanceof NumericTypeVariable))
	    iValue = TypeRelations.attemptDeref(iValue, index);
	
	return iValue;
    }
    
    public static Type getSubValue(ExecutionContext c,
				   Value v, CodeObtainable src) 
	throws TypeException,
	       UninitializedVariableException {
	if(!v.getType().equals(Types.typeType)) {
	    throw new TypeException(src, "You must specify an index");
	}
	
	Type newType = new ArrayType((Type)v);
	c.instanceSpace.addGlobalType(newType);
	return newType;
    }
    
    public static Type getSubValue(ExecutionContext c,
				   Value v, NumericTypeVariable s, 
				   CodeObtainable src)
	throws TypeException,
	       UninitializedVariableException {
	if(!v.getType().equals(Types.typeType)) {
	    throw new TypeException(src, "You must specify an index");
	}
	
	InstanceEnvironment e = c.instanceSpace.getCurrentEnvironment();
	Type newType = new ArrayType((Type)v, s);
	c.instanceSpace.addGlobalType(newType);
	return newType;
    }
	
    public static Type getSubValue(ExecutionContext c,
				   Type t, int size, 
				   CodeObtainable src) {
	Type newType = new ArrayType(t, size);
	c.instanceSpace.addGlobalType(newType);
	return newType;
    }
    
    public static Value getSubValue(Value v, int index, CodeObtainable src)
	throws TypeException,
	       UninitializedVariableException {
	Indexable indexable;
	
	try {
	    /* Try to deref if we are a reference and not indexable */
	    if(!v.getType().isIndexable() &&
	       v.getType().isReference()) {
		v = ((Reference)v).deref();
	    }
	    
	    /* Are we really indexable? */
	    if(!v.getType().isIndexable()) 
		throw new TypeException("Value is not indexable");
	    
	    indexable = (Indexable)v;
	    return indexable.getSubValue(index);
	} catch(TypeException e) {
	    throw new TypeException(src, e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(src, e.getMessage());
	}
    }
    
    public static LValue getSubLValue(LValue lv, int index, CodeObtainable src)
	throws TypeException,
	       UninitializedVariableException {
	IndexableLValue indexable;
	
	try {
	    /* Try to deref if we are a reference and not indexable */
	    if(!lv.getType().isIndexable() &&
	       lv.getType().isReference()) {
		lv = ((ReferenceLValue)lv).deref();
	    }
	    
	    /* Are we really indexable? */
	    if(!lv.getType().isIndexable()) 
		throw new TypeException("Value is not indexable");
	    
	    indexable = (IndexableLValue)lv;
	    return indexable.getSubLValue(index);
	} catch(TypeException e) {
	    throw new TypeException(src, e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(src, e.getMessage());
	}
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value array = expr.evaluate(c);

	if(index == null) {
	    return getSubValue(c,array,this);
	} else {
	    if(!array.getType().equals(Types.typeType) &&
	       !array.getType().isIndexable() &&
	       array.getType().isReference()) {
		try {
		    array = ((Reference)array).deref();
		} catch(LSSException e) {
		    /* Oh well, no big loss...*/
		}
	    }

	    if(array.getType().equals(Types.typeType)) {
		Value ind = getIndexValue(c);
		if(TypeRelations.isSubtype(ind.getType(), 
					   Types.numericIntType)) {
		    int i = (int)((NumericIntValue)ind).getValue();
		    return getSubValue(c, (Type)array, i, this);
		} else if(ind instanceof NumericTypeVariable) {
		    return getSubValue(c, (Type)array, 
				       (NumericTypeVariable)ind, this);
		} else {
		    throw new TypeException(index, "Array type lengths " + 
					    "must be numeric type variables " +
					    "or have type " + 
					    Types.numericIntType + 
					    " not " + ind.getType());
		}
	    } else {
		int i = getIndex(c);
		return getSubValue(array, i, this);
	    }
	}
    }

    public LValue getLValue(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	if(index == null) {
	    throw new TypeException(this + " is not an LValue");
	}

	int i = getIndex(c);
	LValue lv = expr.getLValue(c);
	return getSubLValue(lv, i, this);
    }
}

