package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class CallExpression extends ExpressionBase
{
    private TokenInfo lparen, rparen;
    private Expression func;
    private ExpressionList args;

    public CallExpression(Expression f, TokenInfo lp,
				ExpressionList a, TokenInfo rp) {
	this(f,lp,rp);
	args = a;
    }

    public CallExpression(Expression f, TokenInfo lp,
				TokenInfo rp) {
	lparen = lp;
	rparen = rp;
	func = f;
	args = null;
    }

    public String getOriginalCode() {
	String argStr = args == null ? "" :
	    args.getOriginalCode();

	return func.getOriginalCode() + 
	    lparen.getOriginalCode() + 
	    argStr +
	    rparen.getOriginalCode();
    }

    public String toString() {
	String argStr = args == null ? "" :
	    args.toString();

	return func.toString() +
	    lparen.toString() + 
	    argStr +
	    rparen.toString();
    }

    public int getLine() {
	return func.getLine();
    }
    
    public String getFileName() {
	return func.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value function = func.evaluate(c);
	Vector arguments = new Vector();

	if(args != null) {
	    Iterator i = args.iterator();
	    while(i.hasNext()) {
		Expression e = (Expression)i.next();
		arguments.add(e.evaluate(c));
	    }
	}

	try {
	    /* Try dereferencing the function if it is not callable */
	    if(!function.getType().isCallable() &&
	       function.getType().isReference())
		function = ((Reference)function).deref();

	} catch(UninitializedVariableException e) { 
	    // FIXME: No call trace for this exception.  Wrong line number if exception occurs in body of callee
	    throw new UninitializedVariableException(func, e.getMessage());
	}


	if(!function.getType().isCallable()) {
	    throw new TypeException(func, "Cannot call value of type " + 
				    function.getType());
	}
	
	try {
	    return ((Callable)function).call(arguments, this);
	} catch(FunctionException e) {
	    throw new TypeException(func, e.getMessage());
	} catch(UninitializedVariableException e) { 
	    // FIXME: No call trace for this exception.  Wrong line number if exception occurs in body of callee
	    throw new UninitializedVariableException(func, e.getMessage());
	} catch (UnimplementedException e) {
            e.addToCallingContext(func);
	    throw e;
        } catch (TypeException e) {
            e.addToCallingContext(func);
	    throw e;
        } catch (UndefinedSymbolException e) {
	    e.addToCallingContext(func);
	    throw e;
        } catch (ParseException e) {
	    e.addToCallingContext(func);
	    throw e;
	}
	//        } catch (SymbolAlreadyDefinedException e) {
	//            e.addToCallingContext(func);
	//	    throw e;
	//        }
    }
}
