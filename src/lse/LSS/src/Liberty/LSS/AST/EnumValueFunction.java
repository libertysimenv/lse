package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;
import Liberty.LSS.IMR.StructAdd;

public class EnumValueFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType enumValueType;

    static {
	Vector v = new Vector();
	v.add(Types.typeType);
	v.add(Types.literalType);

	/* This is a giant hack.  The function's return type is
	 * actually polymorphic */
	enumValueType = new FunctionType(v, Types.voidType);
    }

    public EnumValueFunction(ExecutionContext c) {
	super(enumValueType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	FunctionType ft = ((FunctionType)getType());
	
	if(argValues.size() != 2)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");
	
	try {
	    Vector v = new Vector();
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(0)));
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(1)));
	    argValues = v;
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}
	
	int i = 0;
	EnumType enumType;
	StringValue enumValue;
	try {
	    enumType = (EnumType)argValues.elementAt(i++);
	    enumValue = (StringValue)argValues.elementAt(i++);
	} catch(ClassCastException e2) {
	    throw new FunctionException("Argument " + 
					Integer.toString(i) + " in call to " +
					"function with type " + 
					getType() + " must have type " +
					ft.getArgumentType(i-1));
	} 

	EnumValue[] values = enumType.getValues();
	for(i = 0; i < values.length; i++) {
	    if(values[i].toString().equals(enumValue.getFlattenedValue()))
		return values[i];
	}
	throw new FunctionException(enumValue.getValue() + 
				    " is not a value for the type " + 
				    enumType);
    }
}
