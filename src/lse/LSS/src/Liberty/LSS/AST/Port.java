package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class Port extends ValueBase implements Indexable, Aggregate
{
    public final static int INPUT = 0;
    public final static int OUTPUT = 1;

    protected Type type;
    protected String name;
    protected LockableAttributeCollection attributes;
    protected int direction;
    protected Liberty.LSS.IMR.Port imrPort;
    protected Vector externalConnectivity;
    
    public Port(String n, int dir,
		Type c,
	        Collection assignmentLog,
		Collection implicitAssignmentLog) 
	throws UndefinedSymbolException,
	       TypeException,
	       UninitializedVariableException {
	super(Types.portRefType);
	type = c;
	name = n;
	
	if(dir != INPUT && dir != OUTPUT) {
	    throw new RuntimeTypeException("Unknown port direction");
	} else {
	    direction = dir;
	}

	externalConnectivity = new Vector();

	/* If you add an attribute, make sure you update the type
	 * definition */
	attributes = new LockableAttributeCollection();
	try {
	    attributes.addVariable("independent", Types.boolType, null);
	    attributes.addVariable("handler", Types.boolType, null);
	    attributes.addVariable("control", Types.controlPointType, null);
	    attributes.addVariable("width", Types.numericIntType, 
				   new NumericIntValue(0));
	    attributes.addVariable("connected", Types.boolType, 
				   new BoolValue(false));
	} catch(SymbolAlreadyDefinedException e) {
	    /* This can't happen */
	    throw new RuntimeTypeException(e.getMessage());
	} catch(TypeException e) {
	    /* This can't either :) */
	    throw new RuntimeTypeException(e.getMessage());
	}
	
	processConnectedAssignments(implicitAssignmentLog.iterator());
	
	/* Process the implicit assignment log followed by
	   the actual assignment log.  Implicit assignments
	   are overridden by real assignments */
	Parameter.readUnknowns(implicitAssignmentLog.iterator(), name,
			       getType().getLValue(this));
	Parameter.readUnknowns(assignmentLog.iterator(), name, 
			       getType().getLValue(this));

	/* All future assignments are default */
	attributes.lock("control");
	
	/* No internal assignments are allowed */
	attributes.hardlock("width");
	attributes.hardlock("connected");
    }

    protected void processConnectedAssignments(Iterator log) {
	while(log.hasNext()) {
	    AssignmentLogEntry e = (AssignmentLogEntry)log.next();
	    
	    /* Get the name of the variable chopping off the instance
	     * name */
	    UnknownValue v = e.getVariable().chop();
	    Vector names = v.getNames();
	    CodeObtainable assignment = e.getAssignment();

	    /* Check to see if this assignment belongs to us */
	    if(name.equals(names.firstElement())) {
		/* We are looking for the pattern
		 * name[index].connected, we have already verified the
		 * name matches so we will skip the first element.
		 * Then we will check the next few elements.  Any
		 * mismatch and we skip this log entry */

		Iterator i = names.iterator();
		i.next(); /* Skip first element */

		Object o;
		int index;

		if(!i.hasNext()) continue;
		o = i.next();
		if(!(o instanceof Integer)) continue;
		index = ((Integer)o).intValue();

		if(!i.hasNext()) continue;
		o = i.next();
		if(!(o.equals("connected"))) continue;

		if(i.hasNext()) continue;

		if(index >= externalConnectivity.size())
		    externalConnectivity.setSize(index+1);
		BoolValue bv = (BoolValue)e.getValue();
		externalConnectivity.set(index, new Boolean(bv.getValue()));
		log.remove();
	    }
	}
    }

    public void setIMRPort(Liberty.LSS.IMR.Port p) {
	imrPort = p;
    }

    public Liberty.LSS.IMR.Port getIMRPort() {
	return imrPort;
    }

    public Type getPortType() {
	return type;
    }

    public int getDirection() {
	return direction;
    }

    public String getName() {
	return name;
    }

    public LockableAttributeCollection getAttributes() {
	return attributes;
    }
    
    public String toString() {
	return name;
    }

    public Value getSubValue(int index) {
	boolean connected = false;
	if(index < externalConnectivity.size() &&
	   externalConnectivity.get(index) != null) {
	    Boolean b = (Boolean)externalConnectivity.get(index);
	    connected = b.booleanValue();
	}
	return new PortInstance(this, index, connected);
    }

    public Value getSubValue(String field)
	throws TypeException,
	       UninitializedVariableException {
	try {
	    attributes.hardlock(field);
	    if(attributes.getVariableValue(field) == null)
		throw new UninitializedVariableException("Symbol " + name + 
							 "." + field +
							 " has not been " + 
							 "initialized.");
	    return attributes.getVariableValue(field);
	} catch(UndefinedSymbolException e) {
	    throw new TypeException(field + 
				    " is not a valid attribute on port " + 
				    getName());
	}
    }

    public LValue getSubLValue(String field) 
	throws TypeException {
	try {
	    LValue base = new IdentifierLValue(attributes,
					       field);
	    return base.getType().getLValue(base);
	} catch(UndefinedSymbolException e) {
	    throw new TypeException(field + 
				    " is not a valid attribute on port " + 
				    getName());
	}
    }

    public BoolValue equalTo(Value y) {
	return new BoolValue(this == y);
    }
}
