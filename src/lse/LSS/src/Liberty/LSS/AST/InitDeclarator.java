package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class InitDeclarator implements CodeObtainable {
    private Identifier ident;
    private TokenInfo assign;
    private Expression init;

    public InitDeclarator(Identifier id) {
	ident = id;
	assign = null;
	init = null;
    }

    public InitDeclarator(Identifier id, TokenInfo a, Expression i) {
	this(id);
	assign = a;
	init = i;
    }

    public String getOriginalCode() {
	String s = ident.getOriginalCode();
	if(assign != null)
	    s += assign.getOriginalCode() + init.getOriginalCode();
	return s;
    }
    
    public String toString() {
	String s = ident.toString();
	if(assign != null)
	    s += " " + assign.toString() + " " + init.toString();
	return s;
    }
    
    public int getLine() {
	return ident.getLine();
    }

    public String getFileName() {
	return ident.getFileName();
    }

    public Expression getInitializer() {
	return init;
    }

    public Identifier getIdentifier() {
	return ident;
    }
}
