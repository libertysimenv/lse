package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

interface Executable {
    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException;
}
