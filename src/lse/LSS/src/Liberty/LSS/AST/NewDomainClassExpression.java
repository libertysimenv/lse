package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;

public class NewDomainClassExpression extends ExpressionBase {
    private TokenInfo newToken, domainToken;
    private TokenInfo lparen, rparen;
    private Expression domainNameExpr;

    public NewDomainClassExpression(TokenInfo nt, TokenInfo d, TokenInfo lp,
				    Expression dne, TokenInfo rp) {
	newToken = nt;
	domainToken = d;
	lparen = lp;
	rparen = rp;
	domainNameExpr = dne;
    }

    public String getOriginalCode() {
	return newToken.getOriginalCode() +
	    domainToken.getOriginalCode() +
	    lparen.getOriginalCode() +
	    domainNameExpr.getOriginalCode() +
	    rparen.getOriginalCode();
    }

    public String toString() {
	return newToken.toString() + " " +
	    domainToken.toString() +
	    lparen.toString() +
	    domainNameExpr.toString() +
	    rparen.toString();
    }

    public int getLine() {
	return newToken.getLine();
    }

    public String getFileName() {
	return newToken.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Value argument = domainNameExpr.evaluate(c);

	if(!TypeRelations.isSubtype(argument.getType(), Types.stringType))
	    argument = TypeRelations.attemptDeref(argument,this);

	if(!TypeRelations.isSubtype(argument.getType(), Types.stringType))
	    throw new TypeException(this, "domain class name in " +
				    "new expression " + 
				    "must have type string not " + 
				    argument.getType());

	String domainClassName = ((StringValue)argument).getFlattenedValue();
	c.instanceSpace.addDomainClass(domainClassName);
	return new DomainInstanceCreate(c, domainClassName);
    }
}
