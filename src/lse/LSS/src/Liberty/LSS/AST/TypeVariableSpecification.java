package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class TypeVariableSpecification extends TypeSpecification
{
    private TypeVar var;
    private TokenInfo star;

    public TypeVariableSpecification(TypeVar v) {
	var = v;
	star = null;
    }

    public TypeVariableSpecification(TokenInfo s) {
	star = s;
	var = null;
    }

    public String toString() {
	return (var != null) ? var.toString() : star.toString();
    }
    
    public String getOriginalCode() {
	return (var != null) ? var.getOriginalCode() : star.getOriginalCode();
    }

    public int getLine() {
	return (var != null) ? var.getLine() : star.getLine();
    }

    public String getFileName() {
	return (var != null) ? var.getFileName() : star.getFileName();
    }

    public Type getType(ExecutionContext c) 
	throws TypeException,
	       UninitializedVariableException {
	InstanceEnvironment e = c.instanceSpace.getCurrentEnvironment();
	String fqn = e.getFullyQualifiedName();
	String name;
	boolean mustResolve = false;

	if(fqn == null)
	    fqn = "";
	else
	    fqn = fqn + ".";
	
	if(var != null) {
	    name = fqn + var.getIdentifier();
	    if(var.getType() == TypeVar.PORT) {
		mustResolve = true;
	    }
	} else {
	    name = "''" + fqn + c.instanceSpace.getUniqueTypeVarName();
	}

	if(var == null || var.getType() == TypeVar.TYPE || 
	   var.getType() == TypeVar.PORT) 
	    return new TypeVariable(name, mustResolve);
	else if(var.getType() == TypeVar.NUMERIC) 
	    return new NumericTypeVariable(name);
	else 
	    throw new RuntimeTypeException("Bogus typevar received");
    }
}

