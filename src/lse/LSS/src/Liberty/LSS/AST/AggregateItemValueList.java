package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class AggregateItemValueList implements CodeObtainable {
    private AggregateItemValue[] items;
    private TokenInfo[] commas;

    public AggregateItemValueList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	items = new AggregateItemValue[(v.size()+1)/2];
	commas = new TokenInfo[(v.size()-1)/2];

	try {
	    items[0] = (AggregateItemValue)v.firstElement();
	    for(int i = 1; i < v.size(); i+=2) {
		commas[(i-1)/2] = (TokenInfo)v.elementAt(i);
		items[(i+1)/2] = (AggregateItemValue)v.elementAt(i+1);
	    }
	} catch(ClassCastException e) {
	    throw new ArrayStoreException("Cannot convert given vector into an AggregateItemValueList");
	}
    }

    public Vector getAggregateItemValueVector() {
	Vector v = new Vector();

	v.add(items[0]);
	for(int i = 1; i < items.length; i++) {
	    v.add(commas[i-1]);
	    v.add(items[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new AggregateItemValueListIterator(items);
    }

    public String getOriginalCode() {
	String s = items[0].getOriginalCode();
	for(int i = 1; i < items.length; i++) {
	    s += commas[i-1].getOriginalCode() + 
		items[i].getOriginalCode();
	}
	return s;
    }

    public String toString() {
	String s = items[0].toString();
	for(int i = 1; i < items.length; i++) {
	    s += commas[i-1].toString() + " " +
		items[i].toString();
	}
	return s;
    }

    public int getLine() {
	return items[0].getLine();
    }

    public String getFileName() {
	return items[0].getFileName();
    }
}

class AggregateItemValueListIterator implements Iterator {
    AggregateItemValue[] list;
    int pos;

    AggregateItemValueListIterator(AggregateItemValue[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    AggregateItemValue d = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    AggregateItemValue d = list[pos];
	    pos++;
	    return d;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
