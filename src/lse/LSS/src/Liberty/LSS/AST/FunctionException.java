package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class FunctionException extends LSSException
{
    public FunctionException(String m) {
	super(m);
    }

    public FunctionException(CodeObtainable src, String message) {
	super(src, message);
    }

}
