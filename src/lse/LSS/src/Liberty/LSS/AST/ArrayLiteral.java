package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ArrayLiteral extends ExpressionBase {
    public ExpressionList initializers;
    public TokenInfo lbrace, rbrace;

    public ArrayLiteral(TokenInfo lb, ExpressionList il, TokenInfo rb) {
	lbrace = lb;
	rbrace = rb;
	initializers = il;
    }

    public String getOriginalCode() {
	return lbrace.getOriginalCode() + 
	    initializers.getOriginalCode() + 
	    rbrace.getOriginalCode();
    }

    public String toString() {
	return lbrace.toString() + 
	    initializers.toString() + 
	    rbrace.toString();
    }

    public int getLine() {
	return lbrace.getLine();
    }

    public String getFileName() {
	return lbrace.getFileName();
    }

    public Value evaluate(ExecutionContext c)
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Vector v = new Vector();
	Iterator i = initializers.iterator();

	/* Evaluate all the initializers */
	while(i.hasNext()) {
	    Expression init = (Expression)i.next();
	    Value element = init.evaluate(c);
	    v.add(element);
	}

	/* Look at the set of types of all the values */
	Set types = new LinkedHashSet();
	i = v.iterator();
	while(i.hasNext()) {
	    Value element = (Value)i.next();
	    types.add(element.getType());
	}

	/* Do we have more than one type? */
	Vector typedValues = null;
	Type type = null;
	if(types.size() != 1) {
	    i = types.iterator();
	    while(i.hasNext()) {
		type = (Type)i.next();
		try {
		    typedValues = new Vector();
		    Iterator values = v.iterator();
		    while(values.hasNext()) {
			Value element = (Value)values.next();
			element = Coerce.assignCoerce(element, type, this);
			typedValues.add(element);
		    }
		    break;
		} catch(TypeException e) {
		    typedValues = null;
		    continue;
		}
	    }
	} else {
	    i = types.iterator();
	    type = (Type)i.next();
	    typedValues = v;
	}

	if(typedValues == null) {
	    throw new TypeException(this, "All values in an array literal " + 
				    "must have the same type");
	}

	try {
	    ArrayType arrayType = new ArrayType(type, typedValues.size());
	    Value array = new ArrayValue(arrayType, typedValues);
	    return array;
	} catch(TypeException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
    }
}
