package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import java.util.*;

public class BinaryPipeExpression extends BinaryOpExpression
{
    public BinaryPipeExpression(Expression e1, TokenInfo o, Expression e2) {
        super(e1, o, e2);
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Type t1 = TypeSpecification.getType(c, getLeftExpression());
	Type t2 = TypeSpecification.getType(c, getRightExpression());
	Vector types = new Vector();
	
	if(t1 instanceof UnionType) {
	    types.addAll(((UnionType)t1).getOptions());
	} else {
	    types.add(t1);
	}

	if(t2 instanceof UnionType) {
	    types.addAll(((UnionType)t2).getOptions());
	} else {
	    types.add(t2);
	}

	return new UnionType(types);
    }
}
