package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class NewInstanceArrayExpression extends ExpressionBase {
    private TokenInfo newToken, instanceToken;
    private TokenInfo lparen, rparen, comma;
    private TokenInfo lbrack, rbrack;
    private Expression instanceNameExpr;
    private Expression numberExpr;
    private Identifier moduleName;

    public NewInstanceArrayExpression(TokenInfo n, TokenInfo i,
				      TokenInfo lb, Expression ne, 
				      TokenInfo rb, TokenInfo lp, 
				      Expression e, TokenInfo c, 
				      Identifier m, TokenInfo rp) {
	newToken = n;
	instanceToken = i;
	lbrack = lb;
	numberExpr = ne;
	rbrack = rb;
	lparen = lp;
	instanceNameExpr = e;
	comma = c;
	moduleName = m;
	rparen = rp;
    }

    public String getOriginalCode() {
	return newToken.getOriginalCode() +
	    instanceToken.getOriginalCode() +
	    lbrack.getOriginalCode() +
	    numberExpr.getOriginalCode() +
	    rbrack.getOriginalCode() +
	    lparen.getOriginalCode() +
	    instanceNameExpr.getOriginalCode() +
	    comma.getOriginalCode() +
	    moduleName.getOriginalCode() +
	    rparen.getOriginalCode();
    }

    public String toString() {
	return newToken.toString() + " " +
	    instanceToken.toString() +
	    lbrack.toString() +
	    numberExpr.toString() +
	    rbrack.toString() +
	    lparen.toString() +
	    instanceNameExpr.toString() +
	    comma.toString() + " " +
	    moduleName.toString() +
	    rparen.toString();
    }

    public int getLine() {
	return newToken.getLine();
    }

    public String getFileName() {
	return newToken.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Value v;

	v = numberExpr.evaluate(c);
	if(!TypeRelations.isSubtype(v.getType(), Types.numericIntType))
	    v = TypeRelations.attemptDeref(v,this);

	if(!TypeRelations.isSubtype(v.getType(), Types.numericIntType))
	    throw new UndefinedSymbolException(this, "instance width in new" + 
					       " expression " + 
					       "must have type " +
					       Types.numericIntType + " not " + 
					       v.getType());
	int width = (int)((NumericIntValue)v).getValue();

	v = instanceNameExpr.evaluate(c);
	if(!TypeRelations.isSubtype(v.getType(), Types.stringType))
	    v = TypeRelations.attemptDeref(v,this);

	if(!TypeRelations.isSubtype(v.getType(), Types.stringType))
	    throw new UndefinedSymbolException(this, "instance name in new" + 
					       " expression " + 
					       "must have type " +
					       Types.stringType + " not " + 
					       v.getType());
	String name = ((StringValue)v).getFlattenedValue();
	
	Vector insts = new Vector();
	for(int i = 0; i < width; i++) {
	    insts.add(InstanceDeclaration.newInstance(c, this, 
						      moduleName.toString(),
						      name + i));
	}

	ArrayType t = new ArrayType(Types.instanceRefType, width);
	return new ArrayValue(t, insts);
    }
}
