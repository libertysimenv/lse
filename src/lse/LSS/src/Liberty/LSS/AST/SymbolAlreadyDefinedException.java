package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class SymbolAlreadyDefinedException extends LSSException
{
    public SymbolAlreadyDefinedException() {
	super();
    }
    
    public SymbolAlreadyDefinedException(String message) {
	super(message);
    }

    public SymbolAlreadyDefinedException(CodeObtainable src, String message) {
	super(src, message);
    }
}
