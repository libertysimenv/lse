package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public abstract class TypeModifier implements CodeObtainable {
    public abstract Type getModifiedType(Type t);
}
