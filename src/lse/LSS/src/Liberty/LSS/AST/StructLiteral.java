package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class StructLiteral extends ExpressionBase
{
    private TokenInfo lbrace, rbrace;
    private AggregateItemValueList values;

    public StructLiteral(TokenInfo lb, AggregateItemValueList v,
			 TokenInfo rb) {
	lbrace = lb;
	rbrace = rb;
	values = v;
    }

    public String getOriginalCode() {
	return lbrace.getOriginalCode() + values.getOriginalCode() +
	    rbrace.getOriginalCode();
    }

    public String toString() {
	return lbrace.toString() + values.toString() + rbrace.toString();
    }

    public int getLine() {
	return lbrace.getLine();
    }

    public String getFileName() {
	return lbrace.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Iterator i = values.iterator();
	HashMap valueMap = new HashMap();
	Vector names = new Vector();
	Vector types = new Vector();

	while(i.hasNext()) {
	    AggregateItemValue v = (AggregateItemValue)i.next();
	    String name = v.getName().toString();
	    Expression expr = v.getExpression();
	    Value value = expr.evaluate(c);
	    valueMap.put(name, value);
	    names.add(name);

	    /* Make sure numbers always are ints not numeric ints */
	    if(value.getType().equals(Types.numericIntType))
		types.add(Types.intType);
	    else 
		types.add(value.getType());
	}

	try {
	    StructType t = new StructType(types, names);
	    return new StructValue(t, valueMap);
	} catch(TypeException e) {
	    throw new TypeException(this, e.getMessage());
	}
    }
}
