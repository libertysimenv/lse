package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class DottedIdentList implements CodeObtainable {
    private Identifier[] names;
    private TokenInfo[] dots;

    public DottedIdentList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	names = new Identifier[(v.size()+1)/2];
	dots = new TokenInfo[(v.size()-1)/2];

	try {
	    names[0] = (Identifier)v.firstElement();
	    for(int i = 1; i < v.size(); i+=2) {
		dots[(i-1)/2] = (TokenInfo)v.elementAt(i);
		names[(i+1)/2] = (Identifier)v.elementAt(i+1);
	    }
	} catch(ClassCastException e) {
	    throw new ArrayStoreException("Cannot convert given " + 
					  "vector into an DottedIdentList");
	}
    }

    public Vector getIdentVector() {
	Vector v = new Vector();

	v.add(names[0]);
	for(int i = 1; i < names.length; i++) {
	    v.add(dots[i-1]);
	    v.add(names[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new DottedIdentListIterator(names);
    }

    public String getOriginalCode() {
	String s = names[0].getOriginalCode();
	for(int i = 1; i < names.length; i++) {
	    s += dots[i-1].getOriginalCode() + 
		names[i].getOriginalCode();
	}
	return s;
    }

    public String toString() {
	String s = names[0].toString();
	for(int i = 1; i < names.length; i++) {
	    s += dots[i-1].toString() +
		names[i].toString();
	}
	return s;
    }

    public int getLine() {
	return names[0].getLine();
    }

    public String getFileName() {
	return names[0].getFileName();
    }

    public Vector getNames() {
	Vector v = new Vector();
	Iterator i = iterator();
	while(i.hasNext()) {
	    Identifier id = (Identifier)i.next();
	    v.add(id.toString());
	}
	return v;
    }
}

class DottedIdentListIterator implements Iterator {
    Identifier[] list;
    int pos;

    DottedIdentListIterator(Identifier[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    Identifier d = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    Identifier d = list[pos];
	    pos++;
	    return d;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
