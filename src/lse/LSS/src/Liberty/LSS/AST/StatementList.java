package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class StatementList implements CodeObtainable, Executable {
    private Statement[] statements;

    public StatementList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	statements = new Statement[v.size()];
	statements = (Statement[])v.toArray(statements);
    }

    public Vector getStatementVector() {
	Vector v = new Vector();
	for(int i = 0; i < statements.length; i++) {
	    v.add(statements[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new StatementListIterator(statements);
    }

    public String getOriginalCode() {
	String s = "";
	for(int i = 0; i < statements.length; i++) {
	    s = s + statements[i].getOriginalCode();
	}
	return s;
    }

    public int getLine() {
	return statements[0].getLine();
    }

    public String getFileName() {
	return statements[0].getFileName();
    }

    public String toString() {
	String s = "";
	for(int i = 0; i < statements.length; i++) {
	    s = s + "\n" + statements[i];
	}
	return s;
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Iterator i = this.iterator();
	while(i.hasNext()) {
	    Executable e = (Executable)i.next();
	    e.execute(c);
	}
    }
}

class StatementListIterator implements Iterator {
    Statement[] list;
    int pos;

    StatementListIterator(Statement[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    Statement s = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    Statement s = list[pos];
	    pos++;
	    return s;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
