package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;

import Liberty.LSS.types.inference.*;

public class ConstrainFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType constrainType;

    static {
	Vector v = new Vector();
	v.add(Types.typeType);
	v.add(Types.typeType);
	
	constrainType = new FunctionType(v, Types.voidType);
    }

    public ConstrainFunction(ExecutionContext c) {
	super(constrainType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, final CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	FunctionType ft = ((FunctionType)getType());
	
	if(argValues.size() != 2)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");
	
	try {
	    Vector v = new Vector();
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(0)));
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(1)));
	    argValues = v;
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}
	
	int i = 0;
	Type typeVarType;
	Type typeVarConstraint;
	try {
	    typeVarType = (Type)argValues.elementAt(i++);
	    typeVarConstraint = (Type)argValues.elementAt(i++);
	} catch(ClassCastException e2) {
	    throw new FunctionException("Argument " + 
					Integer.toString(i) + " in call to " +
					"function with type " + 
					getType() + " must have type " +
					ft.getArgumentType(i-1));
	} 

	ConstraintSource constraintSrc = new ConstraintSource() {
		public String getIdentifierString() {
		    return "explicit constraint on line " + 
			callSite.getLine() + " in " + callSite.getFileName();
		}
	    };
	Constraint cons = new Constraint(typeVarType, 
					 typeVarConstraint, 
					 constraintSrc);
	c.instanceSpace.getCurrentEnvironment().addConstraint(cons);
	
	return new VoidValue();
    }
}
