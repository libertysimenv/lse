package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ForStatement extends Statement {
    private TokenInfo forToken;
    private TokenInfo semi1, semi2;
    private TokenInfo lparen, rparen;
    private Expression init, condition, increment;
    private CompoundStatement body;

    public ForStatement(TokenInfo f, TokenInfo lp, Expression e1,
			TokenInfo s1, Expression e2,
			TokenInfo s2, Expression e3,
			TokenInfo rp, CompoundStatement s) {
	forToken = f;
	semi1 = s1;
	semi2 = s2;
	lparen = lp;
	rparen = rp;
	init = e1;
	condition = e2;
	increment = e3;
	body = s;
    }

    public String getOriginalCode() {
	String s = forToken.getOriginalCode() + lparen.getOriginalCode();
	
	if(init != null)
	    s += init.getOriginalCode();

	s += semi1.getOriginalCode() + condition.getOriginalCode() +
	    semi2.getOriginalCode();

	if(increment != null)
	    s += increment.getOriginalCode();

	s += rparen.getOriginalCode() + body.getOriginalCode();
	return s;
    }

    public String toString() {
	String s = forToken.toString() + lparen.toString();
	
	if(init != null)
	    s += init.toString();

	s += semi1.toString() + condition.toString() +  
	    semi2.toString();

	if(increment != null)
	    s += increment.toString();

	s += rparen.toString() + " ...";
	return s;
    }

    public int getLine() {
	return forToken.getLine();
    }

    public String getFileName() {
	return forToken.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	c = (ExecutionContext)c.enterScope();
	if(init != null) init.evaluate(c);

	while(true) {
	    Value v = condition.evaluate(c);

	    if(!TypeRelations.isSubtype(v.getType(), Types.boolType))
		v = TypeRelations.attemptDeref(v, this);
	
	    if(TypeRelations.isSubtype(v.getType(), Types.boolType)) {
		boolean cond = ((BoolValue)v).getValue();
		if(cond) {
		    try {
			body.execute(c);
		    } catch(BreakException e) {
			return;
		    }
		    if(increment != null) increment.evaluate(c);
		} else
		    return;
	    } else {
		throw new TypeException(condition, 
					"For condition must have " + 
					"type boolean, not " +
					v.getType());
	    }
	}
    }
}
