package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class Parameter extends ValueBase implements Reference
{
    private LockableAttributeCollection context;
    private String name;
    private boolean exportFlag;

    public Parameter(String n, Type t,
		     LockableAttributeCollection c, 
		     Collection assignmentLog,
		     boolean importFlag,
		     boolean exportFlag) 
	throws SymbolAlreadyDefinedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException {
	super(Types.parameterRefType);
	context = c;
	name = n;
	c.addVariable(n,t,null);
	if(importFlag) {
	    readUnknowns(assignmentLog.iterator(), name, getLValue());
	    try {
		/* All future assignments are default */
		context.lock(name);
	    } catch(UndefinedSymbolException e) {
		throw new RuntimeTypeException(e.getMessage());
	    }
	}
	this.exportFlag = exportFlag;
    }

    public static void readUnknowns(Iterator assignmentLog, String name,
				    LValue lvBase) 
	throws TypeException,
	       UninitializedVariableException {
	LValue lv;

	while(assignmentLog.hasNext()) {
	    lv = lvBase;
	    AssignmentLogEntry e = (AssignmentLogEntry)assignmentLog.next();
	    
	    /* Get the name of the variable chopping off the instance
	     * name */
	    UnknownValue v = e.getVariable().chop();
	    Vector names = v.getNames();
	    CodeObtainable assignment = e.getAssignment();

	    /* Check to see if this assignment belongs to us */
	    if(name.equals(names.firstElement())) {
		assignmentLog.remove();
		
		Iterator i = names.iterator();
		i.next(); /* Skip first element */
		while(i.hasNext()) {
		    Object o = i.next();
		    if(o instanceof String) {
			String field = (String)o;
			lv = SubfieldExpression.getSubLValue(lv, field,
							     assignment);
		    } else if(o instanceof Integer) {
			int index = ((Integer)o).intValue();
			lv = ArrayIndexExpression.getSubLValue(lv, index,
							       assignment);
		    } else {
			String msg = "Unknown value has bogus name";
			throw new RuntimeTypeException(msg);
		    }
		}
		
		try {
		    lv.assign(e.getValue());
		} catch(TypeException ex) {
		    throw new TypeException(assignment, ex.getMessage());
		}
	    }
	}
    }

    public String getName() {
	return name;
    }

    public boolean getExportFlag() {
	return exportFlag;
    }

    public Value derefNoLock() 
	throws UninitializedVariableException {
	try {
	    Value v = context.getVariableValue(name);
	    if(v == null) {
		throw new UninitializedVariableException("Symbol " + name + 
							 " has not been " +
							 "initialized");
	    }
	    return v;
	} catch(UndefinedSymbolException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public Value deref() 
	throws UninitializedVariableException {
	try {
	    context.hardlock(name);
	    Value v = context.getVariableValue(name);
	    if(v == null) {
		throw new UninitializedVariableException("Symbol " + name + 
							 " has not been " +
							 "initialized");
	    }
	    return v;
	} catch(UndefinedSymbolException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public LValue getLValue() 
	throws TypeException {
	try {
	    LValue base = new IdentifierLValue(context, name);
	    return base.getType().getLValue(base);
	} catch(UndefinedSymbolException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public boolean equals(Object o) {
	if(o == null) return false;
	if(!(o instanceof Parameter)) return false;

	Parameter p = (Parameter)o;
	if(name.equals(p.name) &&
	   context.equals(p.context)) return true;

	return false;
    }

    public int hashCode() {
	return name.hashCode() + context.hashCode();
    }
}
