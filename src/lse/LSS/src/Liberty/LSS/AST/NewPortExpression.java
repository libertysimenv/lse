package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class NewPortExpression extends ExpressionBase {
    private TokenInfo newToken, portToken;
    private TokenInfo lparen, rparen, comma;
    private Expression portNameExpr;
    private Expression types;

    public NewPortExpression(TokenInfo n, TokenInfo p,
			     TokenInfo lp, Expression e,
			     TokenInfo c, Expression t,
			     TokenInfo rp) {
	newToken = n;
	portToken = p;
	lparen = lp;
	portNameExpr = e;
	comma = c;
	types = t;
	rparen = rp;
    }

    public String getOriginalCode() {
	return newToken.getOriginalCode() +
	    portToken.getOriginalCode() +
	    lparen.getOriginalCode() +
	    portNameExpr.getOriginalCode() +
	    comma.getOriginalCode() +
	    types.getOriginalCode() +
	    rparen.getOriginalCode();
    }

    public String toString() {
	return newToken.toString() + " " +
	    portToken.toString() +
	    lparen.toString() +
	    portNameExpr.toString() +
	    comma.toString() + " " +
	    types.toString() +
	    rparen.toString();
    }

    public int getLine() {
	return newToken.getLine();
    }

    public String getFileName() {
	return newToken.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Value v = portNameExpr.evaluate(c);
	if(!TypeRelations.isSubtype(v.getType(), Types.stringType))
	    v = TypeRelations.attemptDeref(v,this);

	if(!TypeRelations.isSubtype(v.getType(), Types.stringType))
	    throw new UndefinedSymbolException(this, "port name in new" + 
					       " expression " + 
					       "must have type string not " + 
					       v.getType());
	String name = ((StringValue)v).getFlattenedValue();
	return PortDeclaration.newPort(c, this, name, types, portToken);
    }
}
