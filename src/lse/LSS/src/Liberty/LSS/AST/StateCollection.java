package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.util.*;
import Liberty.LSS.IMR.Event;

import java.util.*;
import java.io.*;


public class StateCollection extends AbstractVariableCollection
{
    private ScopedSymbolTable variableTypingContext;
    private ScopedSymbolTable variableValueContext;
    private ScopedSymbolTable moduleContext;
    private ScopedSymbolTable eventContext;

    public StateCollection() 
    { 
	this(true);
    }	

    public StateCollection(boolean initScope)
    {
	if(initScope) {
	    variableTypingContext = new ScopedSymbolTable();
	    variableValueContext = new ScopedSymbolTable();
	    moduleContext = new ScopedSymbolTable();
	    eventContext = new ScopedSymbolTable();
	    init(variableTypingContext);
	}
    }

    /* Copy Constructor */
    protected StateCollection(StateCollection c) {
	variableTypingContext = c.variableTypingContext;
	variableValueContext = c.variableValueContext;
	moduleContext = c.moduleContext;
	eventContext = c.eventContext;
	init(variableTypingContext);
    }

    protected StateCollection cloneState() {
	return new StateCollection(this);
    }

    /* Returned StateCollection cannot have new symnols added to it in
       this scope */
    public StateCollection checkpointState() {
	StateCollection c = cloneState();
	if(!emptyContext()) {
	    variableTypingContext = variableTypingContext.checkpoint();
	    variableValueContext = variableValueContext.checkpoint();
	    moduleContext = moduleContext.checkpoint();
	    eventContext = eventContext.checkpoint();
	    init(variableTypingContext);
	}
	return c;
    }

    public void reset() {
	variableTypingContext = null;
	variableValueContext = null;
	moduleContext = null;
	eventContext = null;
    }

    public boolean variableDefined(String var) {
	if(emptyContext()) return false;
	return super.variableDefined(var);
    }

    public boolean canDefineVar(String var) {
	return !variableTypingContext.symbolDefinedCurrentScope(var);
    }

    public Value getVariableValue(String var) 
	throws UndefinedSymbolException {
	if(emptyContext() || !variableDefined(var))
	    throw new UndefinedSymbolException("Symbol " + 
					       qualifySymbol(var) + 
					       " is not defined");
	return (Value)variableValueContext.get(var);
    }

    public void addVariable(String var, Type t, Value v)
	throws SymbolAlreadyDefinedException,
	       TypeException {
	if(variableTypingContext.symbolDefinedCurrentScope(var))
	    throw new SymbolAlreadyDefinedException("Symbol " + 
						    qualifySymbol(var) + 
						    " already defined");
	variableTypingContext.add(var,t);

	variableValueContext.add(var,null);
	try {
	    initVariable(var,v);
	} catch(UndefinedSymbolException e) {
	    /* We know the symbol is defined, so ... */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public void overloadFunctionValue(String var, Value v)
	throws UndefinedSymbolException,
	       EmptyContextException,
	       TypeException {
	Value oldValue = (Value)variableValueContext.get(var);
	Value newValue;
	if(oldValue == null)
	    newValue = v;
	else {
	    Collection c = new Vector();
	    if(oldValue.getType() instanceof FunctionType) {
		c.add(oldValue);
	    } else if(oldValue.getType() instanceof OverloadedFunctionType) {
		c.addAll(((OverloadedFunctionValue)oldValue).getFunctions());
	    } else {
		throw new RuntimeTypeException("old type not a function, " + 
					       "but overload attempted");
	    }
	    c.add(v);

	    Type oft = getVariableType(var);
	    oft = oft.getBaseType(); /* Strip of const */
	    newValue = 
		new OverloadedFunctionValue((OverloadedFunctionType)oft, c);
	}
	initVariable(var,newValue);
    }

    public void initVariable(String var, Value v) 
	throws TypeException,
	       UndefinedSymbolException {
	try {
	    setVariableValueInternal(var,v);
	} catch(TypeException e) {
	    Type t = getVariableType(var);
	    throw new TypeException("Cannot initialize symbol " + 
				    qualifySymbol(var) + 
				    " of type " + t +
				    " with value of type " + v.getType());
	}
    }

    public void setVariableValue(String var, Value v)
	throws UndefinedSymbolException,
	       TypeException {
	Type varType = getVariableType(var);
	if(isConst(varType)) {
	    throw new TypeException("Cannot assign to symbol " 
				    + qualifySymbol(var) + ":" + varType);
	}
	setVariableValueInternal(var, v);
    }

    private void setVariableValueInternal(String var, Value v)
	throws UndefinedSymbolException,
	       TypeException {
	if(v != null)
	    typeCheck(var, v);
	variableValueContext.set(var,v);
    }	

    public void addType(String name, Type t)
	throws SymbolAlreadyDefinedException {
	try {
	    addVariable(name, new ConstType(Types.typeType), t);
	} catch(TypeException e) {
	    /* We know we had type compatibilty for the new value
	     * so something bad has happened... */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public Type getType(String name) 
	throws UndefinedSymbolException {
	Type typeType = getVariableType(name);
	if(!typeType.equals(Types.typeType))
	    throw new UndefinedSymbolException("Symbol " + 
					       qualifySymbol(name) + 
					       " is not a type");
	return (Type)getVariableValue(name);
    }

    public void addModule(Module m)
	throws SymbolAlreadyDefinedException {
	moduleContext.add(m.getName(),m);
    }

    public void addModule(String name) 
	throws SymbolAlreadyDefinedException {
	moduleContext.add(name,null);
    }

    public void setModule(Module m) 
	throws UndefinedSymbolException {
	moduleContext.set(m.getName(),m);
    }

    public Module getModule(String name) 
	throws UndefinedSymbolException {
	if(emptyContext() || !moduleContext.symbolDefined(name))
	    throw new UndefinedSymbolException("Symbol " + 
					       qualifySymbol(name) + 
					       " is not defined");
	return (Module)moduleContext.get(name);
    }

    public void addEvent(Event e)
	throws SymbolAlreadyDefinedException {
	eventContext.add(e.getName(),e);
    }

    public void addEvent(String name) 
	throws SymbolAlreadyDefinedException {
	eventContext.add(name,null);
    }

    public void setEvent(Event e) 
	throws UndefinedSymbolException {
	eventContext.set(e.getName(),e);
    }
			
    public Event getEvent(String name) 
	throws UndefinedSymbolException {
	if(emptyContext() || !eventContext.symbolDefined(name))
	    throw new UndefinedSymbolException("Symbol " + 
					       qualifySymbol(name) + 
					       " is not defined");
	return (Event)eventContext.get(name);
    }

    public StateCollection enterScope() {
	StateCollection c = cloneState();
	if(!emptyContext()) {
	    c.variableTypingContext = variableTypingContext.enterScope();
	    c.variableValueContext = variableValueContext.enterScope();
	    c.moduleContext = moduleContext.enterScope();
	    c.eventContext = eventContext.enterScope();
	} else {
	    c.variableTypingContext = new ScopedSymbolTable();
	    c.variableValueContext = new ScopedSymbolTable();
	    c.moduleContext = new ScopedSymbolTable();
	    c.eventContext = new ScopedSymbolTable();
	}
	c.init(c.variableTypingContext);
	return c;
    }

    public boolean emptyContext() {
	if(variableTypingContext == null ||
	   variableValueContext == null ||
	   moduleContext == null ||
	   eventContext == null) {
	    return true;
	}
	return false;
    }

    public StateCollection leaveScope() 
	throws EmptyContextException {
	if(emptyContext()) {
	    throw new EmptyContextException();
	} else {
	    StateCollection c = cloneState();
	    c.variableTypingContext = variableTypingContext.leaveScope();
	    c.variableValueContext = variableValueContext.leaveScope();
	    c.moduleContext = moduleContext.leaveScope();
	    c.eventContext = eventContext.leaveScope();
	    c.init(c.variableTypingContext);
	    return c;
	}
    }

    public String toString() {
	String v = "Variables: \n" + 
	    ( (variableValueContext != null) ? 
	      variableValueContext.toString() + "\n\n" : "(null)\n");
	String m = "Modules: \n" + 
	    ( (moduleContext != null) ? 
	      moduleContext.toString() + "\n\n" : "(null)\n");
	String e = "Events: \n" + 
	    ( (eventContext != null) ? eventContext.toString() : "(null)\n" );
	return "StateCollection:\n" + v+m+e;
    }
}
