package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class AggregateItemValue implements CodeObtainable
{
    public Identifier id;
    public TokenInfo assign;
    public Expression expr ;

    public AggregateItemValue(Identifier i, TokenInfo a,
			      Expression e) {
	id = i;
	expr = e;
	assign = a;
    }

    public String getOriginalCode() {
	return id.getOriginalCode() + assign.getOriginalCode() +
	    expr.getOriginalCode();
    }

    public String toString() {
	return id.toString() + " " + assign.toString() +
	    " " + expr.toString();
    }

    public int getLine() {
	return id.getLine();
    }

    public String getFileName() {
	return id.getFileName();
    }

    public Identifier getName() {
	return id;
    }

    public Expression getExpression() {
	return expr;
    }
}
