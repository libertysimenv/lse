package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.IMR.Event;

public class ExportStatement extends Statement
{
    private TokenInfo exportToken, asToken, semi;
    private Expression expr;
    private Identifier exportName;

    public ExportStatement(TokenInfo x, Expression e, TokenInfo a, 
			   Identifier id, TokenInfo s) {
	exportToken = x;
	expr = e;
	asToken = a;
	exportName = id;
	semi = s;
    }

    public String getOriginalCode() {
	return exportToken.getOriginalCode() + expr.getOriginalCode() +
	    asToken.getOriginalCode() + exportName.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return exportToken.toString() + expr.toString() +
	    asToken.toString() + exportName.toString() +
	    semi.toString();
    }

    public int getLine() {
	return exportToken.getLine();
    }

    public String getFileName() {
	return exportToken.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Type type = TypeSpecification.getType(c, expr);
	InstanceEnvironment i = c.instanceSpace.getCurrentEnvironment();
	try {
	    if(!type.isPolymorphic())
		type.getBackendType();
	} catch(RuntimeTypeException ex) {
	    throw new TypeException(this, "Cannot export type " + type + 
				    " since it has no backend type");
	}

	try {
	    i.addExportedType(exportName.toString(), type);
	} catch(SymbolAlreadyDefinedException ex) {
	    throw new SymbolAlreadyDefinedException(this, ex.getMessage());
	}

    }
}
