package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class BinaryModExpression extends BinaryOpExpression
{
    public BinaryModExpression(Expression e1, TokenInfo o, Expression e2) {
        super(e1, o, e2);
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value v1 = getLeftExpression().evaluate(c);
	Value v2 = getRightExpression().evaluate(c);
	try {
	    return Operate.mod(v1,v2);
	} catch(TypeException e) {
	    throw new TypeException("Error on line " + getLine() +
				    " of " + getFileName() +
				    ": " + e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new TypeException("Error on line " + this.getLine() + 
				    " of " + this.getFileName() + 
				    ": " + e.getMessage());
	}
    }
}
