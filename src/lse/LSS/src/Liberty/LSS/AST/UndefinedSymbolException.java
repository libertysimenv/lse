package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class UndefinedSymbolException extends LSSException
{
    public UndefinedSymbolException() {
	super();
    }
    
    public UndefinedSymbolException(String message) {
	super(message);
    }

    public UndefinedSymbolException(CodeObtainable src, String message) {
	super(src, message);
    }
}
