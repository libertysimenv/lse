package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;
import Liberty.LSS.IMR.StructAdd;

public class StructCreateFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType structCreateType;

    static {
	Vector v = new Vector();
	ArrayType names = new ArrayType(Types.literalType);
	ArrayType types = new ArrayType(Types.typeType);
	v.add(names);
	v.add(types);
	
	structCreateType = new FunctionType(v, Types.typeType);
    }

    public StructCreateFunction(ExecutionContext c) {
	super(structCreateType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	FunctionType ft = ((FunctionType)getType());
	
	if(argValues.size() != 2)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");
	
	try {
	    Vector v = new Vector();
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(0)));
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(1)));
	    argValues = v;
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}
	
	int i = 0;
	ArrayValue nameArray, typeArray;
	try {
	    nameArray = (ArrayValue)argValues.elementAt(i++);
	    Type nameArrayElementType = 
		((ArrayType)nameArray.getType()).getElementType();
	    if(!TypeRelations.isSubtype(nameArrayElementType, 
					Types.literalType))
		throw new ClassCastException();

	    typeArray = (ArrayValue)argValues.elementAt(i++);
	    Type typeArrayElementType = 
		((ArrayType)typeArray.getType()).getElementType();
	    if(!TypeRelations.isSubtype(typeArrayElementType, 
					Types.typeType))
		throw new ClassCastException();
	} catch(ClassCastException e2) {
	    throw new FunctionException("Argument " + 
					Integer.toString(i) + " in call to " +
					"function with type " + 
					getType() + " must have type " +
					ft.getArgumentType(i-1));
	} 

	Vector names = new Vector();
	Iterator namesIter = nameArray.getValue().iterator();
	while(namesIter.hasNext()) {
	    StringValue v = (StringValue)namesIter.next();
	    names.add(v.getFlattenedValue());
	}

	try {
	    Type newType = new StructType(typeArray.getValue(), 
					  names);
	    c.instanceSpace.addGlobalType(newType);
	    return newType;
	} catch(TypeException e) {
	    throw new FunctionException(e.getMessage());
	}
    }
}
