package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class Identifier extends IdBase implements Expression {
    public Identifier(String v, TokenInfo ti) {
	super(v,ti);
    }

    public Value evaluate(ExecutionContext c) 
	throws UndefinedSymbolException,
	       UninitializedVariableException {
	try {
	    Value v = c.getVariableValue(this.toString());
	    if(v == null) {
		throw new UninitializedVariableException(this, "Symbol " + 
							 toString() + 
							 " has not " +
							 "been initialized");
	    }
	    return v;
	} catch(UndefinedSymbolException e) {
	    throw new UndefinedSymbolException(this, e.getMessage());
	}
    }

    public LValue getLValue(ExecutionContext c) 
	throws UndefinedSymbolException,
	       TypeException {
	LValue lv;

	try {
	    lv = new IdentifierLValue(c,this.toString());
	} catch(UndefinedSymbolException e) {
	    throw new UndefinedSymbolException(this, e.getMessage());
	}

	/* Wrap the LValue with the type-specific LValue */
	try {
	    return lv.getType().getLValue(lv);
	} catch(TypeException e) {
	    throw new TypeException(this, e.getMessage());
	}
    }
}
