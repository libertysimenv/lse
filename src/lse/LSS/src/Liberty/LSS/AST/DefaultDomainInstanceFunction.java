package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class DefaultDomainInstanceFunction extends ValueBase 
    implements Callable {
    private ExecutionContext context;

    private static FunctionType ddiType;

    static {
	Vector v = new Vector();
	v.add(Types.stringType);
	ddiType = new FunctionType(v, Types.domainInstanceRefType);
    }

    public DefaultDomainInstanceFunction(ExecutionContext c) {
	super(ddiType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	if(argValues.size() != 1)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + argValues.size() +
					" arguments");
	
	int i = 0;
	StringValue s;
	try {
	    Value v = (Value)argValues.elementAt(0);
	    s = (StringValue)TypeRelations.attemptDeref(v);
	} catch(ClassCastException e2) {
	    throw new FunctionException("Argument 1 in call to " +
					"function with type " + getType() + 
					" must have type " +
					Types.stringType);
	} catch(UninitializedVariableException e2) {
	    throw new FunctionException(e2.getMessage());
	}
	
	InstanceEnvironment env = c.instanceSpace.getCurrentEnvironment();
	Parameter p;
	DomainInstanceMapValue map;
	try {
	    p = env.getParameter("LSE_domain");
	    map = (DomainInstanceMapValue)p.deref();
	} catch(UndefinedSymbolException e) {
	    throw new RuntimeTypeException(e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
	
	try {
	    return map.getSubValue(s.getFlattenedValue());
	} catch(TypeException e) {
	    throw new FunctionException(e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}
    }
}
