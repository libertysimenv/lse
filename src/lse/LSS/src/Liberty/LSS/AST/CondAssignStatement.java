package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class CondAssignStatement extends Statement
{
    private TokenInfo operator;
    private Expression expr1,expr2;

    public CondAssignStatement(Expression e1, TokenInfo o, Expression e2) {
	operator = o;
	expr1 = e1;
	expr2 = e2;
    }

    public String getOriginalCode() {
	return expr1.getOriginalCode() + 
	    operator.getOriginalCode() + 
	    expr2.getOriginalCode();
    }

    public String toString() {
	return expr1.toString() + " " +
	    operator.toString() + " " +
	    expr2.toString();
    }

    public int getLine() {
	return operator.getLine();
    }
    
    public String getFileName() {
	return operator.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Value v = null;
	try {
	    v = expr2.evaluate(c);
	} catch(UninitializedVariableException e) {
	    /* The data on the right side is uninitialized, don't assign */
	    return;
	}

	if(TypeRelations.isSubtype(v.getType(), Types.parameterRefType)) {
	    /* Try derefing the parameter */
	    try {
		v = ((Reference)v).deref();
	    } catch(UninitializedVariableException e) {
		/* The parameter is unset, don't assign */
		return;
	    }
	}

	LValue lv = expr1.getLValue(c);
	
	try {
	    lv.assign(v);
	} catch(TypeException e) {
	    throw new TypeException(this, e.getMessage());
	}
    }
}
