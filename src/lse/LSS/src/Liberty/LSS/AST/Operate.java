package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import java.util.*;

public class Operate
{
    private static Value functionAdd(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        CallableType xt = (CallableType)x.getType();
        CallableType yt = (CallableType)y.getType();

        /* Make sure that our return types agree */
        if(!xt.getReturnType().equals(yt.getReturnType())) {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }

        Vector types = new Vector();
        Vector functions = new Vector();

        if(xt instanceof OverloadedFunctionType) {
            types.addAll(((OverloadedFunctionType)xt).getFunctionTypes());
            functions.addAll(((OverloadedFunctionValue)x).getFunctions());
        } else if(xt instanceof FunctionType) {
            types.add(xt);
            functions.add(x);
        } else {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }

        if(yt instanceof OverloadedFunctionType) {
            types.addAll(((OverloadedFunctionType)yt).getFunctionTypes());
            functions.addAll(((OverloadedFunctionValue)y).getFunctions());
        } else if(yt instanceof FunctionType) {
            types.add(yt);
            functions.add(y);
        } else {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }

        try {
            OverloadedFunctionType t = new OverloadedFunctionType(types);
            Value v = new OverloadedFunctionValue(t, functions);
            return v;
        } catch(TypeException e) {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }
    }

    private static Value functionTypeAdd(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        CallableType xt = (CallableType)x;
        CallableType yt = (CallableType)y;

        /* Make sure that our return types agree */
        if(!xt.getReturnType().equals(yt.getReturnType())) {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }

        Vector types = new Vector();

        if(xt instanceof OverloadedFunctionType) {
            types.addAll(((OverloadedFunctionType)xt).getFunctionTypes());
        } else if(xt instanceof FunctionType) {
            types.add(xt);
        } else {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }

        if(yt instanceof OverloadedFunctionType) {
            types.addAll(((OverloadedFunctionType)yt).getFunctionTypes());
        } else if(yt instanceof FunctionType) {
            types.add(yt);
        } else {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }

        try {
            OverloadedFunctionType t = new OverloadedFunctionType(types);
            return t;
        } catch(TypeException e) {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }
    }


    public static Value add(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* Are we dealing with functions? */
        if(x.getType() instanceof CallableType &&
           y.getType() instanceof CallableType) {
            return functionAdd(x,y);
        }

        /* Are we dealing with function types? */
        if(x instanceof CallableType &&
           y instanceof CallableType) {
            return functionTypeAdd(x,y);
        }

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), y.getType()))
           x = Coerce.subtypeCoerce(x, y.getType());
        else if(TypeRelations.isSubtype(y.getType(), x.getType()))
           y = Coerce.subtypeCoerce(y, x.getType());

        /* Check the various types that can be added */
        /* Ints */
        if(x.getType().equals(y.getType()) &&
           x.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            return new NumericIntValue(xl + yl);

        }

        /* Floats */
        else if(x.getType().equals(y.getType()) &&
                  x.getType().equals(Types.floatType)) {
            double xl = ((FloatValue)x).getValue();
            double yl = ((FloatValue)y).getValue();
            return new FloatValue(xl + yl);
        }

        /* Strings */
        else if(x.getType().equals(y.getType()) &&
                  x.getType().equals(Types.stringType)) {
            Vector xl = ((StringValue)x).getValue();
            Vector yl = ((StringValue)y).getValue();
            Vector v = new Vector(xl);
            v.addAll(yl);
            return new StringValue(v);
        }

        /* Literals */
        else if(x.getType().equals(y.getType()) &&
                x.getType().equals(Types.literalType)) {
            Vector xl = ((LiteralValue)x).getValue();
            Vector yl = ((LiteralValue)y).getValue();
            Vector v = new Vector(xl);
            v.addAll(yl);
            return new LiteralValue(v);
        }

        /* Otherwise */
        else {
            throw new TypeException(x.getType() + " + " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value subtract(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), y.getType()))
           x = Coerce.subtypeCoerce(x, y.getType());
        else if(TypeRelations.isSubtype(y.getType(), x.getType()))
           y = Coerce.subtypeCoerce(y, x.getType());

        /* Check the various types that can be subtracted */
        if(x.getType().equals(y.getType()) &&
           x.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            return new NumericIntValue(xl - yl);
        } else if(x.getType().equals(y.getType()) &&
                  x.getType().equals(Types.floatType)) {
            double xl = ((FloatValue)x).getValue();
            double yl = ((FloatValue)y).getValue();
            return new FloatValue(xl - yl);
        } else {
            throw new TypeException(x.getType() + " - " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value multiply(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), y.getType()))
           x = Coerce.subtypeCoerce(x, y.getType());
        else if(TypeRelations.isSubtype(y.getType(), x.getType()))
           y = Coerce.subtypeCoerce(y, x.getType());

        /* Check the various types that can be multiplied */
        if(x.getType().equals(y.getType()) &&
           x.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            return new NumericIntValue(xl * yl);
        } else if(x.getType().equals(y.getType()) &&
                  x.getType().equals(Types.floatType)) {
            double xl = ((FloatValue)x).getValue();
            double yl = ((FloatValue)y).getValue();
            return new FloatValue(xl * yl);
        } else {
            throw new TypeException(x.getType() + " * " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value divide(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), y.getType()))
           x = Coerce.subtypeCoerce(x, y.getType());
        else if(TypeRelations.isSubtype(y.getType(), x.getType()))
           y = Coerce.subtypeCoerce(y, x.getType());

        /* Check the various types that can be divided */
        if(x.getType().equals(y.getType()) &&
           x.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            return new NumericIntValue(xl / yl);
        } else if(x.getType().equals(y.getType()) &&
                  x.getType().equals(Types.floatType)) {
            double xl = ((FloatValue)x).getValue();
            double yl = ((FloatValue)y).getValue();
            return new FloatValue(xl / yl);
        } else {
            throw new TypeException(x.getType() + " / " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value mod(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType))
           x = Coerce.subtypeCoerce(x, Types.numericIntType);
        else if(TypeRelations.isSubtype(y.getType(), Types.numericIntType))
           y = Coerce.subtypeCoerce(y, Types.numericIntType);

        /* Only ints can be mod'ed by ints */
        if(x.getType().equals(Types.numericIntType) &&
           y.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            long zl = xl % yl;
            return new NumericIntValue(zl);
        } else {
            throw new TypeException(x.getType() + " % " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value shortCircuitLogicalAnd(ExecutionContext c,
                                               Expression x,
                                               Expression y)
        throws UnimplementedException,
               TypeException,
               UndefinedSymbolException,
               SymbolAlreadyDefinedException,
               UninitializedVariableException,
               ParseException {
        /* First deal with x */

        /* Try dereffing values (mainly for parameters) */
        Value xv = x.evaluate(c);
        xv = TypeRelations.attemptDeref(xv);

        /* First we check subtypes to see if x needs to be coerced */
        if(TypeRelations.isSubtype(xv.getType(), Types.boolType))
           xv = Coerce.subtypeCoerce(xv, Types.boolType);

        if(xv.getType().equals(Types.boolType)) {
            boolean xl = ((BoolValue)xv).getValue();
            if(!xl) return new BoolValue(false);
        } else {
            throw new TypeException(x, xv.getType() + " || any-type" +
                                    " is not defined.");
        }

        /* x was true, so now try y */

        /* Try dereffing values (mainly for parameters) */
        Value yv = y.evaluate(c);
        yv = TypeRelations.attemptDeref(yv);

        /* First we check subtypes to see if y needs to be coerced */
        if(TypeRelations.isSubtype(yv.getType(), Types.boolType))
           yv = Coerce.subtypeCoerce(yv, Types.boolType);

        if(yv.getType().equals(Types.boolType)) {
            boolean yl = ((BoolValue)yv).getValue();
            return new BoolValue(yl);
        } else {
            throw new TypeException(y, xv.getType() + " && " + yv.getType() +
                                    " is not defined.");
        }
    }

    public static Value logicalAnd(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), Types.boolType))
           x = Coerce.subtypeCoerce(x, Types.boolType);
        else if(TypeRelations.isSubtype(y.getType(), Types.boolType))
           y = Coerce.subtypeCoerce(y, Types.boolType);

        /* Only bools can be and'ed by bools */
        if(x.getType().equals(Types.boolType) &&
           y.getType().equals(Types.boolType)) {
            boolean xl = ((BoolValue)x).getValue();
            boolean yl = ((BoolValue)y).getValue();
            boolean zl = xl && yl;
            return new BoolValue(zl);
        } else {
            throw new TypeException(x.getType() + " && " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value shortCircuitLogicalOr(ExecutionContext c,
                                              Expression x,
                                              Expression y)
        throws UnimplementedException,
               TypeException,
               UndefinedSymbolException,
               SymbolAlreadyDefinedException,
               UninitializedVariableException,
               ParseException {
        /* First deal with x */

        /* Try dereffing values (mainly for parameters) */
        Value xv = x.evaluate(c);
        xv = TypeRelations.attemptDeref(xv);

        /* First we check subtypes to see if x needs to be coerced */
        if(TypeRelations.isSubtype(xv.getType(), Types.boolType))
           xv = Coerce.subtypeCoerce(xv, Types.boolType);

        if(xv.getType().equals(Types.boolType)) {
            boolean xl = ((BoolValue)xv).getValue();
            if(xl) return new BoolValue(true);
        } else {
            throw new TypeException(x, xv.getType() + " || any-type" +
                                    " is not defined.");
        }

        /* x was false, so now try y */

        /* Try dereffing values (mainly for parameters) */
        Value yv = y.evaluate(c);
        yv = TypeRelations.attemptDeref(yv);

        /* First we check subtypes to see if y needs to be coerced */
        if(TypeRelations.isSubtype(yv.getType(), Types.boolType))
           yv = Coerce.subtypeCoerce(yv, Types.boolType);

        if(yv.getType().equals(Types.boolType)) {
            boolean yl = ((BoolValue)yv).getValue();
            return new BoolValue(yl);
        } else {
            throw new TypeException(y, xv.getType() + " || " + yv.getType() +
                                    " is not defined.");
        }
    }

    public static Value logicalOr(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), Types.boolType))
           x = Coerce.subtypeCoerce(x, Types.boolType);
        else if(TypeRelations.isSubtype(y.getType(), Types.boolType))
           y = Coerce.subtypeCoerce(y, Types.boolType);

        /* Only bools can be or'ed by bools */
        if(x.getType().equals(Types.boolType) &&
           y.getType().equals(Types.boolType)) {
            boolean xl = ((BoolValue)x).getValue();
            boolean yl = ((BoolValue)y).getValue();
            boolean zl = xl || yl;
            return new BoolValue(zl);
        } else {
            throw new TypeException(x.getType() + " || " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value leftShift(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType))
           x = Coerce.subtypeCoerce(x, Types.numericIntType);
        else if(TypeRelations.isSubtype(y.getType(), Types.numericIntType))
           y = Coerce.subtypeCoerce(y, Types.numericIntType);

        /* Only ints can be shifted by ints */
        if(x.getType().equals(Types.numericIntType) &&
           y.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            long zl = xl << yl;
            return new NumericIntValue(zl);
        } else {
            throw new TypeException(x.getType() + " << " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value rightShift(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType))
           x = Coerce.subtypeCoerce(x, Types.numericIntType);
        else if(TypeRelations.isSubtype(y.getType(), Types.numericIntType))
           y = Coerce.subtypeCoerce(y, Types.numericIntType);

        /* Only ints can be shifted by ints */
        if(x.getType().equals(Types.numericIntType) &&
           y.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            long zl = xl >> yl;
            return new NumericIntValue(zl);
        } else {
            throw new TypeException(x.getType() + " >> " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value bitwiseOr(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType))
           x = Coerce.subtypeCoerce(x, Types.numericIntType);
        else if(TypeRelations.isSubtype(y.getType(), Types.numericIntType))
           y = Coerce.subtypeCoerce(y, Types.numericIntType);

        /* Only ints can be bitwise-OR-ed with ints */
        if(x.getType().equals(Types.numericIntType) &&
           y.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            long zl = xl | yl;
            return new NumericIntValue(zl);
        } else {
            throw new TypeException(x.getType() + " << " + y.getType() +
                                    " is not defined.");
        }
    }


    public static Value bitwiseAnd(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType))
           x = Coerce.subtypeCoerce(x, Types.numericIntType);
        else if(TypeRelations.isSubtype(y.getType(), Types.numericIntType))
           y = Coerce.subtypeCoerce(y, Types.numericIntType);

        /* Only ints can be bitwise-AND-ed with ints */
        if(x.getType().equals(Types.numericIntType) &&
           y.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            long zl = xl & yl;
            return new NumericIntValue(zl);
        } else {
            throw new TypeException(x.getType() + " << " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value bitwiseXor(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType))
           x = Coerce.subtypeCoerce(x, Types.numericIntType);
        else if(TypeRelations.isSubtype(y.getType(), Types.numericIntType))
           y = Coerce.subtypeCoerce(y, Types.numericIntType);

        /* Only ints can be bitwise-XOR-ed with ints */
        if(x.getType().equals(Types.numericIntType) &&
           y.getType().equals(Types.numericIntType)) {
            long xl = ((NumericIntValue)x).getValue();
            long yl = ((NumericIntValue)y).getValue();
            long zl = xl ^ yl;
            return new NumericIntValue(zl);
        } else {
            throw new TypeException(x.getType() + " << " + y.getType() +
                                    " is not defined.");
        }
    }

    public static BoolValue lessThan(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), y.getType()))
           x = Coerce.subtypeCoerce(x, y.getType());
        else if(TypeRelations.isSubtype(y.getType(), x.getType()))
           y = Coerce.subtypeCoerce(y, x.getType());

        /* Only ints or floats or strings can be compared */
        if(x.getType().equals(y.getType()) &&
           x.getType().equals(Types.numericIntType)) {
            long v1 = ((NumericIntValue)x).getValue();
            long v2 = ((NumericIntValue)y).getValue();
            return new BoolValue(v1 < v2);
        } else if(x.getType().equals(y.getType()) &&
                  x.getType().equals(Types.floatType)) {
            double v1 = ((FloatValue)x).getValue();
            double v2 = ((FloatValue)y).getValue();
            return new BoolValue(v1 < v2);
        } else if(x.getType().equals(y.getType()) &&
                  x.getType().equals(Types.stringType)) {
            String v1 = ((StringValue)x).getFlattenedValue();
            String v2 = ((StringValue)y).getFlattenedValue();
            return new BoolValue(v1.compareTo(v2) < 0 ? true : false);
        } else {
            throw new TypeException(x.getType() + " < " + y.getType() +
                                    " is not defined.");
        }
    }

    public static BoolValue lessThanEqual(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        try {
            BoolValue v1 = lessThan(x,y);
            BoolValue v2 = equalTo(x,y);
            return new BoolValue(v1.getValue() || v2.getValue());
        } catch(TypeException e) {
            throw new TypeException(x.getType() + " <= " + y.getType() +
                                    " is not defined.");
        }
    }

    public static BoolValue greaterThan(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        try {
            BoolValue v = lessThanEqual(x,y);
            return new BoolValue(!v.getValue());
        } catch(TypeException e) {
            throw new TypeException(x.getType() + " > " + y.getType() +
                                    " is not defined.");
        }
    }

    public static BoolValue greaterThanEqual(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        try {
            BoolValue v = lessThan(x,y);
            return new BoolValue(!v.getValue());
        } catch(TypeException e) {
            throw new TypeException(x.getType() + " >= " + y.getType() +
                                    " is not defined.");
        }
    }

    public static BoolValue equalTo(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        /* Try dereffing values (mainly for parameters) */
        x = TypeRelations.attemptDeref(x);
        y = TypeRelations.attemptDeref(y);

        /* First we check subtypes to see if x or y need to be coerced */
        if(TypeRelations.isSubtype(x.getType(), y.getType()))
           x = Coerce.subtypeCoerce(x, y.getType());
        else if(TypeRelations.isSubtype(y.getType(), x.getType()))
           y = Coerce.subtypeCoerce(y, x.getType());

        /* Only equal types can be compared */
        if(x.getType().equals(y.getType())) {
            return new BoolValue(x.equals(y));
        } else {
            throw new TypeException(x.getType() + " == " + y.getType() +
                                    " is not defined.");
        }
    }

    public static BoolValue notEqualTo(Value x, Value y)
        throws TypeException,
               UninitializedVariableException {
        try {
            BoolValue v = equalTo(x,y);
            return new BoolValue(!v.getValue());
        } catch(TypeException e) {
            throw new TypeException(x.getType() + " != " + y.getType() +
                                    " is not defined.");
        }
    }

    public static Value unaryPlus(Value x)
        throws TypeException,
               UninitializedVariableException {
        x = TypeRelations.attemptDeref(x);
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType)) {
            return x;
        } else if(TypeRelations.isSubtype(x.getType(), Types.floatType)) {
            return x;
        } else {
            throw new TypeException("Unary plus is not defined for type " +
                                    x.getType());
        }
    }

    public static Value unaryMinus(Value x)
        throws TypeException,
               UninitializedVariableException {
        x = TypeRelations.attemptDeref(x);
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType)) {
            return new NumericIntValue(-((NumericIntValue)x).getValue());
        } else if(TypeRelations.isSubtype(x.getType(), Types.floatType)) {
            return new FloatValue(-((FloatValue)x).getValue());
        } else {
            throw new TypeException("Unary minus is not defined for type " +
                                    x.getType());
        }
    }

    public static Value unaryNot(Value x)
        throws TypeException,
               UninitializedVariableException {
        x = TypeRelations.attemptDeref(x);
        if(TypeRelations.isSubtype(x.getType(), Types.boolType)) {
            return new BoolValue(!((BoolValue)x).getValue());
        } else {
            throw new TypeException("Unary NOT is not defined for type " +
                                    x.getType());
        }
    }

    public static Value unaryBitwiseNot(Value x)
        throws TypeException,
               UninitializedVariableException {
        x = TypeRelations.attemptDeref(x);
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType)) {
            return new NumericIntValue(~((NumericIntValue)x).getValue());
        } else {
            throw new TypeException("Unary Bitwise NOT is not defined for type " +
                                    x.getType());
        }
    }


    public static Value increment(Value x)
        throws TypeException,
               UninitializedVariableException {
        x = TypeRelations.attemptDeref(x);
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType)) {
            return new NumericIntValue(((NumericIntValue)x).getValue() + 1);
        } else {
            throw new TypeException("Increment is not defined for type " +
                                    x.getType());
        }
    }

    public static Value decrement(Value x)
        throws TypeException,
               UninitializedVariableException {
        x = TypeRelations.attemptDeref(x);
        if(TypeRelations.isSubtype(x.getType(), Types.numericIntType)) {
            return new NumericIntValue(((NumericIntValue)x).getValue() - 1);
        } else {
            throw new TypeException("Decrement is not defined for type " +
                                    x.getType());
        }
    }
}
