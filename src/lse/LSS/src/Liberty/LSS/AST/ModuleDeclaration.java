package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ModuleDeclaration extends Statement
{
    private TokenInfo moduleToken;
    private TokenInfo semi;
    private Identifier name;
    private CompoundStatement body;

    public ModuleDeclaration(TokenInfo m, Identifier n, CompoundStatement b,
			     TokenInfo s) {
	moduleToken = m;
	semi = s;
	name = n;
	body = b;
    }

    public String getOriginalCode() {
	return moduleToken.getOriginalCode() +
	    name.getOriginalCode() + 
	    body.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return moduleToken.toString() + " " +
	    name.toString() + " " +
	    body.toString() + semi.toString();
    }
    
    public int getLine() {
	return moduleToken.getLine();
    }

    public String getFileName() {
	return moduleToken.getFileName();
    }

    public CompoundStatement getBody() {
	return body;
    }

    public void execute(ExecutionContext c) throws SymbolAlreadyDefinedException,
						   UndefinedSymbolException {
	try {
	    c.addModule(name.toString());
	} catch(SymbolAlreadyDefinedException e) {
	    throw new SymbolAlreadyDefinedException("Error on line " + getLine() +
						    " of " + getFileName() +
						    ": " + e.getMessage());
	}
	Module m = new Module(name.toString(), this, c);
	c.setModule(m);
    }
}
