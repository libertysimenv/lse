package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.util.*;
import Liberty.LSS.IMR.*;
import java.util.*;

public class RealInstanceEnvironment extends BaseInstanceEnvironment
{
    private Vector assignmentLog;
    private Vector implicitAssignmentLog;
    private HashMap queries;
    private HashMap ports;
    private HashMap structAdds;
    private HashMap events;
    private HashMap exportedTypes;
    private InstanceEnvironment parent;
    private String name;
    private Module module;
    private Instance instance;
    private List hierDomainSearchPath;
    private CodeObtainable instantiationLocation;

    /****************/
    /* Constructors */
    /****************/
    
    public RealInstanceEnvironment() {
	super();
	assignmentLog = new Vector();
	implicitAssignmentLog = new Vector();
	ports = new HashMap();
	queries = new HashMap();
	structAdds = new HashMap();
	events = new HashMap();
	exportedTypes = new HashMap();
	hierDomainSearchPath = new Vector();
	parent = null;
	name = null;
	parent = null;
	module = null;
	instance = null;
    }

    public RealInstanceEnvironment(String n, Module m, CodeObtainable il) {
	this();
	name = n;
	module = m;
	instantiationLocation = il;
    }

    public RealInstanceEnvironment(String n, Module m, InstanceEnvironment e,
				   CodeObtainable il) {
	this(n,m,il);
	parent = e;
	hierDomainSearchPath.addAll(parent.getHierDomainSearchPath());

	Iterator j = parent.getLocalDomainSearchPath().iterator();
	
	while (j.hasNext()) {
	    DomainSearchPathEntry spe = (DomainSearchPathEntry)j.next();
	    
	    ListIterator i = hierDomainSearchPath.listIterator();
	    boolean found = false;

	    while (i.hasNext() && !found) {
		DomainSearchPathEntry entry = (DomainSearchPathEntry)i.next();
		if (spe.getDomainClass().compareTo(entry.getDomainClass())==0){
		    i.set(spe);
		    found = true;
		}
	    }
	    if (!found) {
		hierDomainSearchPath.add(spe);
	    }
	}
    }
    
    /******************/
    /* Name functions */
    /******************/
    
    public String toString() {
	return getFullyQualifiedName();
    }

    public String getName() {
	return name;
    }

    public String getFullyQualifiedName() {
	String parentName;

	if(parent != null)
	    parentName = parent.getFullyQualifiedName();
	else
	    parentName = null;

	if(parentName == null)
	    return name;
	else
	    return parentName + "." + name;
    }

    /******************/
    /* Port Functions */
    /******************/

    public Port addPort(String name, int dir, Type c) 
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       TypeException,
	       UninitializedVariableException {
	Port p = new Port(name, dir, c, getAssignmentLog(),
			  getImplicitAssignmentLog());
	if(members.containsKey(name))
	    throw new SymbolAlreadyDefinedException("Symbol " + p + 
						    " is already defined");
	ports.put(name,p);
	members.put(name, null);
	return p;
    }

    public Port getPort(String name)
	throws UndefinedSymbolException {
	if(ports.containsKey(name))
	    return (Port)ports.get(name);
	else
	    throw new UndefinedSymbolException("No such port " + name);
    }

    public Collection getPorts() {
	return ports.values();
    }

    /*******************/
    /* Query functions */
    /*******************/
    public void addQuery(Query q)
	throws SymbolAlreadyDefinedException {
	if(queries.containsKey(q.getName()))
	    throw new SymbolAlreadyDefinedException("Symbol " + q.getName() + 
						    " is already defined");
	queries.put(q.getName(),q);
    }

    public Collection getQueries() {
	return queries.values();
    }

    /***********************/
    /* StructAdd functions */
    /***********************/
    public void addStructAdd(StructAdd a) throws SymbolAlreadyDefinedException {
	HashMap ds;

	if(!structAdds.containsKey(a.getDataStructure().getValue())) {
	    ds = new HashMap();
	    structAdds.put(a.getDataStructure().getValue(), ds);
	} else {
	    ds = (HashMap)structAdds.get(a.getDataStructure().getValue());
	}
	
	if(ds.containsKey(a.getFieldName().getValue())) {
	    throw new SymbolAlreadyDefinedException("");
	}

	ds.put(a.getFieldName().getValue(), a);
    }

    public Collection getStructAdds() {
	HashMap m;
	HashSet sas;
	Iterator i = structAdds.values().iterator();
	sas = new HashSet();

	while(i.hasNext()) {
	    m = (HashMap)i.next();
	    sas.addAll(m.values());
	}

	return sas;
    }

    /**********************/
    /* Subfield functions */
    /**********************/

    private UnknownValue makeUnknown(String field) {
	Vector v = new Vector();
	v.add(getName());
	v.add(field);
	return new UnknownValue(this, v);
    }
    
    public Value getSubValue(String field) 
	throws TypeException {
	/* Is field "instance_name" */
	if(field.equals("instance_name"))
	    return new StringValue(getFullyQualifiedName());

	/* Check parameters */
	try {
	    return super.getSubValue(field);
	} catch(TypeException e) {
	    /* ignore this... */
	}
		
	/* Check ports */
	if(ports.containsKey(field))
	    return (Value)ports.get(field);
	
	if(isProcessed()) {
	    throw new TypeException(field + " is not a subfield of " +
				    "instance " + getFullyQualifiedName());
	}

	/* Otherwise return an unknown */
	return makeUnknown(field);
    }

    public LValue getSubLValue(String field) 
	throws TypeException {
	/* Is field "instance_name" */
	if(field.equals("instance_name"))
	    throw new TypeException("Cannot assign to " +
				    getFullyQualifiedName() + 
				    ".instance_name");

	/* Check parameters */
	try {
	    return super.getSubLValue(field);
	} catch(TypeException e) {
	    /* ignore this... */
	}

	/* Check ports */
	if(ports.containsKey(field)) {
	    Value port = (Value)ports.get(field);
	    return port.getType().getLValue(port);
	}

	if(isProcessed()) {
	    throw new TypeException(field + " is not a subfield of " +
				    "instance " + getFullyQualifiedName());
	}

	/* Otherwise return an unknown */
	UnknownValue u = makeUnknown(field);
	return u.getType().getLValue(u);
    }

    /******************/
    /* Assignment Log */
    /******************/
    public void addAssignment(AssignmentLogEntry e) {
	assignmentLog.add(e);
    }

    public void addImplicitAssignment(AssignmentLogEntry e) {
	implicitAssignmentLog.add(e);
    }

    public Collection getAssignmentLog() {
	return assignmentLog;
    }

    public Collection getImplicitAssignmentLog() {
	return implicitAssignmentLog;
    }

    public boolean assignmentLogIsEmpty() {
	return assignmentLog.isEmpty();
    }

    public boolean implicitAssignmentLogIsEmpty() {
	return implicitAssignmentLog.isEmpty();
    }

    /*******************/
    /* Event functions */
    /*******************/
    public void addEvent(Event e)
	throws SymbolAlreadyDefinedException {
	if(events.containsKey(e.getName()))
	    throw new SymbolAlreadyDefinedException("Event " + e.getName() + 
						    " is already emitted");
	events.put(e.getName(), e);
    }

    public Collection getEvents() {
	return events.values();
    }

    /******************/
    /* Exported Types */
    /******************/
    public void addExportedType(String name, Type type)
	throws SymbolAlreadyDefinedException {
	if(exportedTypes.containsKey(name))
	    throw new SymbolAlreadyDefinedException(name + " has already " +
						    "been used in another " + 
						    "export");
	exportedTypes.put(name, type);
    }

    public Map getExportedTypes() {
	return exportedTypes;
    }

    /***************/
    /* Domain Path */
    /***************/

    public List getHierDomainSearchPath() {
	return hierDomainSearchPath;
    }
    
    /**********/
    /* Others */
    /**********/

    public Module getModule() {
	return module;
    }

    public InstanceEnvironment getParent() {
	return parent;
    }

    public Instance getInstance() {
	return instance;
    }

    public void setInstance(Instance i) {
	instance = i;
    }

    public CodeObtainable getInstantiationLocation() {
	return instantiationLocation;
    }
}
