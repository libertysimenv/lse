package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class InitializedExpression extends ExpressionBase
{
    private TokenInfo initialized,lparen,rparen;
    private Expression expr;

    public InitializedExpression(TokenInfo d, TokenInfo lp,
				 Expression e, TokenInfo rp) {
	initialized = d;
	expr = e;
	lparen = lp;
	rparen = rp;
    }

    public String getOriginalCode() {
	return initialized.getOriginalCode() + 
	    lparen.getOriginalCode() + 
	    expr.getOriginalCode() +
	    rparen.getOriginalCode();
    }

    public String toString() {
	return initialized.toString() + 
	    lparen.toString() + 
	    expr.toString() +
	    rparen.toString();
    }

    public int getLine() {
	return initialized.getLine();
    }
    
    public String getFileName() {
	return initialized.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value v = null;
	try {
	    v = expr.evaluate(c);
	} catch(UninitializedVariableException e) {
	    /* The data on the right side is uninitialized, don't assign */
	    return new BoolValue(false);
	}

	if(TypeRelations.isSubtype(v.getType(), Types.parameterRefType)) {
	    /* Try derefing the parameter */
	    try {
		v = ((Reference)v).deref();
	    } catch(UninitializedVariableException e) {
		/* The parameter is unset, don't assign */
		return new BoolValue(false);
	    }
	}
	return new BoolValue(true);
    }
}
