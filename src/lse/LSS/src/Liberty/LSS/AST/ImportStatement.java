package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.io.*;
import java.util.regex.*;
import java.util.*;

public class ImportStatement extends IncludeStatement {
    public DottedIdentList packageName;
    public TokenInfo importToken;
    public TokenInfo semi;

    public ImportStatement(TokenInfo importToken,
			   DottedIdentList pn,
			   TokenInfo s) {
	this.packageName = pn;
	this.importToken = importToken;
	this.semi = s;
    }

    public String getOriginalCode() {
	return importToken.getOriginalCode() +
	    packageName.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return importToken.toString() + " " +
	    packageName.toString() + " " + 
	    semi.toString();
    }

    public String getFileName() {
	return importToken.getFileName();
    }

    public int getLine() {
	return importToken.getLine();
    }

    public String findFile(File prefix, String fn) 
	throws FileNotFoundException {
	
	Pattern dot = Pattern.compile("\\.");
	String[] pieces = dot.split(fn);

	String qualifedFilename;
	for(int dotPos = pieces.length - 1; dotPos >= 0; dotPos --) {
	    qualifedFilename = "";
	    for(int i = 0; i < pieces.length; i++) {
		if(i != 0) {
		    if(i > dotPos) qualifedFilename += '.';
		    else qualifedFilename += '/';
		}
		qualifedFilename += pieces[i];
	    }
	    qualifedFilename += ".lss";

	    try {
		return ExecutionContext.getFileName(prefix, qualifedFilename);
	    } catch(FileNotFoundException e) {
		/* Ignore this, we'll try other file names */
	    }
	}	    
	
	throw new FileNotFoundException("Cannot find package " + fn);
    }

    public String findFile(ExecutionContext c, String fn)
	throws FileNotFoundException {
	List searchPath = c.getModulePath();
	Iterator i = searchPath.iterator();
	while(i.hasNext()) {
	    try {
		return findFile((File)i.next(), fn);
	    } catch(FileNotFoundException e) {
		/* Ignore this, we'll try other file names */
	    }
	}
	throw new FileNotFoundException("Cannot find package " + fn);
    }

    public void execute(ExecutionContext c)
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	String pkgName = packageName.toString();
	
	/* If this package is already defined do nothing */
	try {
	    c.instanceSpace.getPackage(pkgName);
	    return;
	} catch(UndefinedSymbolException e) {
	    /* Package doesn't exist, so lets continue...*/
	}

	/* Check to see if this package is already being imported */
	Stack packageProcessStack = c.instanceSpace.getPackageProcessStack();
	if(packageProcessStack.search(pkgName) != -1) {
	    packageProcessStack.push(pkgName);
	    throw new TypeException(this, "Recursive package inclusion.  " + 
				    "Import stack is: " + packageProcessStack);
	}
	
	/* Seems like we're okay */
	Package p = new Package(pkgName);
	ExecutionContext packageContext = new ExecutionContext(c);
	packageContext.reset();
	packageContext.setCurrentPackage(p);
	List newSearchList = new Vector(packageContext.getPackageSearchList());
        newSearchList.add(0, p);
	packageContext.setPackageSearchList(newSearchList);

	packageProcessStack.push(pkgName);
	runFile(packageContext, pkgName);
	packageProcessStack.pop();

	c.instanceSpace.addPackage(p);
    }
}
