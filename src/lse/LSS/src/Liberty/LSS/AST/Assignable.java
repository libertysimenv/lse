package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

interface Assignable {
    public LValue getLValue(ExecutionContext c)
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException;
}

