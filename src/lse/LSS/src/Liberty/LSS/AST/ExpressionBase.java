package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public abstract class ExpressionBase implements Expression {
    public LValue getLValue(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException, 
	       ParseException,
	       SymbolAlreadyDefinedException {

	Value v = this.evaluate(c);
	try {
	    return v.getType().getLValue(v);
	} catch(TypeException e) {
	    throw new TypeException(this, e.getMessage());
	}
    }
}
