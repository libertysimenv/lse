package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class BreakStatement extends Statement {
    private TokenInfo breakToken,semi;

    public BreakStatement(TokenInfo b, TokenInfo s) {
	breakToken = b;
	semi = s;
    }

    public String getOriginalCode() {
	return breakToken.getOriginalCode() + semi.getOriginalCode();
    }

    public String toString() {
	return breakToken.toString() + semi.toString();
    }

    public int getLine() {
	return breakToken.getLine();
    }
    
    public String getFileName() {
	return breakToken.getFileName();
    }

    public void execute(ExecutionContext c) throws BreakException {
	throw new BreakException(this);
    }
}
