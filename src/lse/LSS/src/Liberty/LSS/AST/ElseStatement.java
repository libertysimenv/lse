package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ElseStatement extends Statement {
    private TokenInfo elseToken;
    private CompoundStatement body;

    public ElseStatement(TokenInfo e, CompoundStatement b) {
	elseToken = e;
	body = b;
    }

    public String getOriginalCode() {
	return elseToken.getOriginalCode() + 
	    body.getOriginalCode();
    }

    public String toString() {
	return elseToken.toString() + " ...";
    }

    public int getLine() {
	return elseToken.getLine();
    }

    public String getFileName() {
	return elseToken.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	body.execute(c);
    }
}
