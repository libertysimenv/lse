package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class IfStatement extends Statement {
    private TokenInfo ifToken;
    private TokenInfo lparen, rparen;
    private Expression conditional;
    private CompoundStatement thenBody;
    private Statement elseClause;

    public IfStatement(TokenInfo i, TokenInfo lp, Expression e, 
		       TokenInfo rp, CompoundStatement b1,
		       Statement ec) {
	ifToken = i;
	lparen = lp;
	rparen = rp;
	conditional = e;
	thenBody = b1;
	elseClause = ec;
    }

    public IfStatement(TokenInfo i, TokenInfo lp, Expression e, 
		       TokenInfo rp, CompoundStatement b1) {
	this(i,lp,e,rp,b1,null);
    }

    public String getOriginalCode() {
	String s = ifToken.getOriginalCode() + lparen.getOriginalCode() + 
	    conditional.getOriginalCode() + rparen.getOriginalCode() + 
	    thenBody.getOriginalCode();
	
	if(elseClause != null) {
	    s += elseClause.getOriginalCode();
	}
	return s;
    }

    public String toString() {
	String s = ifToken.toString() + lparen.toString() + 
	    conditional.toString() + rparen.toString() + " ...";
	
	if(elseClause != null) {
	    s += " " + elseClause.toString();
	}
	return s;
    }

    public int getLine() {
	return ifToken.getLine();
    }

    public String getFileName() {
	return ifToken.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Value v = conditional.evaluate(c);
	
	if(!TypeRelations.isSubtype(v.getType(), Types.boolType))
		v = TypeRelations.attemptDeref(v, this);

	if(TypeRelations.isSubtype(v.getType(), Types.boolType)) {
	    boolean cond = ((BoolValue)v).getValue();
	    if(cond)
		thenBody.execute(c);
	    else
		if(elseClause != null) elseClause.execute(c);
	} else {
	    throw new TypeException("Error on line " + conditional.getLine() + 
				    " of " + conditional.getFileName() + 
				    ": if condition must have type boolean, not " +
				    v.getType());
	}
    }
}
