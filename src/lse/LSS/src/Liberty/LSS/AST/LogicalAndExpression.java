package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class LogicalAndExpression extends BinaryOpExpression
{
    public LogicalAndExpression(Expression e1, TokenInfo o, Expression e2) {
        super(e1, o, e2);
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
        return Operate.shortCircuitLogicalAnd(c, getLeftExpression(), getRightExpression());
    }
}
