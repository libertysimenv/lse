package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.IMR.Event;

public class EmitStatement extends Statement
{
    private TokenInfo emitToken, semi;
    private EventDeclaration eventDecl;
    private Identifier eventName;

    public EmitStatement(TokenInfo e, Identifier n, TokenInfo s) {
	emitToken = e;
	eventName = n;
	semi = s;
    }

    public EmitStatement(TokenInfo e, EventDeclaration d) {
	emitToken = e;
	eventDecl = d;
	semi = null;
    }

    public String getOriginalCode() {
	if(eventDecl != null)
	    return emitToken.getOriginalCode() +
		eventDecl.getOriginalCode();
	else
	    return emitToken.getOriginalCode() +
		eventName.getOriginalCode() + semi.getOriginalCode();
    }

    public String toString() {
	if(eventDecl != null)
	    return emitToken.toString() + " " +
		eventDecl.toString();
	else
	    return emitToken.toString() + " " +
		eventName.toString() + semi.toString();
    }

    public int getLine() {
	return emitToken.getLine();
    }

    public String getFileName() {
	return emitToken.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	String name;
	if(eventDecl != null) {
	    eventDecl.execute(c);
	    name = eventDecl.getEventName();
	} else
	    name = eventName.toString();

	Event e;
	try {
	    e = c.getEvent(name);
	} catch(UndefinedSymbolException ex) {
	    throw new UndefinedSymbolException(this, ex.getMessage());
	}

	InstanceEnvironment i = c.instanceSpace.getCurrentEnvironment();
	try {
	    i.addEvent(e);
	} catch(SymbolAlreadyDefinedException ex) {
	    throw new SymbolAlreadyDefinedException(this, ex.getMessage());
	}
    }
}
