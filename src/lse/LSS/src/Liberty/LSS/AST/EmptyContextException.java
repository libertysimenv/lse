package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class EmptyContextException extends LSSRuntimeException
{
    public EmptyContextException() {
	super();
    }
    
    public EmptyContextException(String message)
    {
	super(message);
    }
}
