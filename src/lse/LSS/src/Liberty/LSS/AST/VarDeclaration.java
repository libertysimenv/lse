package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class VarDeclaration extends Statement {
    private TokenInfo varToken;
    private TokenInfo colon, semi;
    private Expression typeSpec;
    private InitDeclaratorList declList;

    public VarDeclaration(TokenInfo v, InitDeclaratorList d, TokenInfo c, 
			  Expression t, TokenInfo s) {
	varToken = v;
	colon = c;
	semi = s;
	typeSpec = t;
	declList = d;
    }

    public String getOriginalCode() {
	return varToken.getOriginalCode() + declList.getOriginalCode() + 
	    colon.getOriginalCode() + typeSpec.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return varToken.toString() + " " + declList.toString() + 
	    " " + colon.toString() + " " + typeSpec.toString() +
	    semi.toString();
    }

    public int getLine() {
	return varToken.getLine();
    }

    public String getFileName() {
	return varToken.getFileName();
    }

    public void execute(ExecutionContext cp) 
	throws 	SymbolAlreadyDefinedException,
		UndefinedSymbolException, 
		UnimplementedException, 
		TypeException,
		UninitializedVariableException,
		ParseException {
	Iterator i = declList.iterator();
	while(i.hasNext()) {
	    InitDeclarator decl = (InitDeclarator)i.next();
	    Type t = TypeSpecification.getType(cp,typeSpec);
	    if(t.isPolymorphic()) {
		throw new TypeException(this, "Cannot define a variable " +
					"with polymorphic type " + t);
	    }

	    Type baset = t;
	    while(!baset.isBaseType())
		baset = baset.getBaseType();

	    try {
		Value v = null;
		
		Expression init  = decl.getInitializer();
		if(init != null) {
		    v = init.evaluate(cp);
		    v = Coerce.assignCoerce(v, baset, this);
		}
		cp.addVariable(decl.getIdentifier().toString(), t, v);
	    } catch(SymbolAlreadyDefinedException e) {
		throw new SymbolAlreadyDefinedException(decl, e.getRawMessage());
	    } catch(TypeException e) {
                if(e.hasLineNumber()) throw e;
                else throw new TypeException(decl, e.getMessage());
	    }
	}
    }
}
