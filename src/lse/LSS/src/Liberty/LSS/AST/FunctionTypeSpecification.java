package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class FunctionTypeSpecification extends TypeSpecification {
    private TokenInfo fun, lparen, rparen;
    private TokenInfo returns;
    private ExpressionList arguments;
    private Expression typeSpec;

    public FunctionTypeSpecification(TokenInfo f,
				     TokenInfo lp,TokenInfo rp, 
				     TokenInfo r, Expression t) {
	fun = f;
	lparen = lp;
	rparen = rp;
	returns = r;
	typeSpec = t;
	arguments = null;
    }

    public FunctionTypeSpecification(TokenInfo f,
				     TokenInfo lp,ExpressionList l, 
				     TokenInfo rp, 
				     TokenInfo r, Expression t) {
	this(f,lp,rp,r,t);
	arguments = l;
    }


    public String getOriginalCode() {
	String argStr = arguments == null ? "" : 
	    arguments.getOriginalCode();
	return fun.getOriginalCode() + 
	    lparen.getOriginalCode() +
	    argStr +
	    rparen.getOriginalCode() +
	    returns.getOriginalCode() +
	    typeSpec.getOriginalCode();
    }

    public String toString() {
	String argStr = arguments == null ? "" : 
	    arguments.toString();
	return fun.toString() + " " + 
	    lparen.toString() +
	    argStr +
	    rparen.toString() + " " +
	    returns.toString() + " " +
	    typeSpec.toString();
    }

    public int getLine() {
	return fun.getLine();
    }

    public String getFileName() {
	return fun.getFileName();
    }

    public Type getType(ExecutionContext cp) 
	throws TypeException,
	       UninitializedVariableException {
	Vector argTypes = new Vector();
	Type returnType;

	returnType = TypeSpecification.getType(cp, typeSpec);
	if(returnType.isPolymorphic()) {
	    throw new TypeException(this, "Cannot define a function " +
				    "with polymorphic return type " + 
				    returnType);
	}

	if(arguments != null) {
	    Iterator i = arguments.iterator();
	    while(i.hasNext()) {
		Expression ts = (Expression)i.next();
		Type type = 
		    TypeSpecification.getType(cp, ts);
		if(type.isPolymorphic()) {
		    throw new TypeException(this, "Cannot define a function " +
					    "with polymorphic argument type " +
					    type);
		}
		argTypes.add(type);
	    }
	}

	FunctionType vtype = new FunctionType(argTypes, returnType);
	return vtype;
    }
}
