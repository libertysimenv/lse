package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ParameterDeclaration extends Statement {
    private TokenInfo parameterToken;
    private TokenInfo colon, semi;
    private TokenInfo modifier;
    private Expression typeSpec;
    private InitDeclaratorList declList;

    public ParameterDeclaration(TokenInfo m,
				TokenInfo p, InitDeclaratorList d, 
				TokenInfo c, Expression t, 
				TokenInfo s) {
	modifier = m;
	parameterToken = p;
	colon = c;
	semi = s;
	typeSpec = t;
	declList = d;
    }

    public String getOriginalCode() {
	return (modifier == null ? "" : modifier.getOriginalCode()) + 
	    parameterToken.getOriginalCode() + declList.getOriginalCode() + 
	    colon.getOriginalCode() + typeSpec.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return (modifier == null ? "" : modifier.toString()) + 
	    parameterToken.toString() + " " + declList.toString() + 
	    " " + colon.toString() + " " + typeSpec.toString() +
	    semi.toString();
    }

    public int getLine() {
	return parameterToken.getLine();
    }

    public String getFileName() {
	return parameterToken.getFileName();
    }

    public static Parameter newParameter(ExecutionContext cp, 
					 CodeObtainable src,
					 String name, 
					 Expression typeSpec) 
	throws TypeException,
	       SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       UninitializedVariableException {
	return newParameter(cp, src, name, typeSpec, null);
    }

    public static Parameter newParameter(ExecutionContext cp, 
					 CodeObtainable src,
					 String name, 
					 Expression typeSpec,
					 TokenInfo modifier) 
	throws TypeException,
	       SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       UninitializedVariableException {
	Type t = TypeSpecification.getType(cp,typeSpec);
	return newParameter(cp, src, name, t, modifier);
    }

    public static Parameter newParameter(ExecutionContext cp,
					 CodeObtainable src,
					 String name, Type t) 
	throws TypeException,
	       SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       UninitializedVariableException {
	return newParameter(cp, src, name, t, true, true);
    }

    public static Parameter newParameter(ExecutionContext cp,
					 CodeObtainable src,
					 String name, Type t,
					 TokenInfo modifier)
	throws TypeException,
	       SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       UninitializedVariableException {
	boolean local = false, internal = false;
	if(modifier != null) {
	    if(modifier.toString().equals("local")) local = true;
	    if(modifier.toString().equals("internal")) internal = true;
	    if(modifier.toString().equals("runtimeable")) 
		t = new RuntimeParmType(t);
	}
	return newParameter(cp, src, name, t, !local, !internal);
    }
    
    public static Parameter newParameter(ExecutionContext cp,
					 CodeObtainable src,
					 String name, 
					 Type t,
					 boolean importFlag,
					 boolean exportFlag)
	throws TypeException,
	       SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       UninitializedVariableException {
	try {
	    if(t.isPolymorphic() && !(t instanceof UserPointType)) {
		throw new TypeException(src, "Cannot define a parameter " +
					"with polymorphic type " + t);
	    }

	    InstanceEnvironment e = 
		cp.instanceSpace.getCurrentEnvironment();
	    Parameter p = e.addParameter(name, t, importFlag, exportFlag);
	    return p;
	} catch(SymbolAlreadyDefinedException e) {
	    throw new SymbolAlreadyDefinedException("Error on line " + 
						    src.getLine() + 
						    " of " + 
						    src.getFileName() + 
						    ": " + e.getMessage());
	}
    }

    public void execute(ExecutionContext cp) 
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException, 
	       UnimplementedException, 
	       TypeException,
	       UninitializedVariableException,
	       ParseException {
	Iterator i = declList.iterator();
	Parameter p;

	while(i.hasNext()) {
	    InitDeclarator decl = (InitDeclarator)i.next();
	    Type t = TypeSpecification.getType(cp,typeSpec);
	    p = newParameter(cp, this, decl.getIdentifier().toString(), t,
			     modifier);

	    try {
		cp.addVariable(decl.getIdentifier().toString(), 
			       new ConstType(Types.parameterRefType), p);
	    } catch(SymbolAlreadyDefinedException e) {
		throw new SymbolAlreadyDefinedException("Error on line " + 
							decl.getLine() + 
							" of " + 
							decl.getFileName() + 
							": " + e.getMessage());
	    } catch(TypeException e) {
		throw new TypeException("Error on line " + decl.getLine() + 
					" of " + decl.getFileName() + 
					": " + e.getMessage());
	    }

	    try {
		Expression init  = decl.getInitializer();
		if(init != null) {
		    Value v = init.evaluate(cp);
		    p.getLValue().assign(v);
		}
	    } catch(TypeException e) {
  		throw new TypeException("Error on line " + decl.getLine() + 
  					" of " + decl.getFileName() + 
  					": " + e.getMessage());
	    }
	}
    }
}
