package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.Scanner;
import Liberty.LSS.Parser;
import java.io.*;

public class IncludeStatement extends Statement
{
    private TokenInfo includeTok;
    private StringLiteral filename;
    private TokenInfo semi;

    public IncludeStatement(TokenInfo i, StringLiteral f, TokenInfo s) {
	includeTok = i;
	filename = f;
	semi = s;
    }

    protected IncludeStatement() {
	includeTok = null;
	filename = null;
	semi = null;
    }

    public String getOriginalCode() {
	return includeTok.getOriginalCode() +
	    filename.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return includeTok.toString() + " " +
	    filename.toString() + " " + 
	    semi.toString();
    }

    public String getFileName() {
	return includeTok.getFileName();
    }

    public int getLine() {
	return includeTok.getLine();
    }

    public static StatementList parseFile(Reader r, String filename) 
	throws ParseException {
	Scanner s;
	Parser p;
	StatementList sl;
	
	try {
	    s = new Scanner(r, filename);
	    p = new Parser(s);
	    sl = (StatementList)p.parse().value;
	    return sl;
	} catch(Exception e) { /* CUP throws this */
	    /* CUP's error message is useless... we'll craft our own */
	    String msg = "Could not parse file " + filename;
	    throw new ParseException(msg);
	}
    }

    public String findFile(ExecutionContext c, String fn) 
	throws FileNotFoundException {
	return c.getFileName(fn);
    }

    public void runFile(ExecutionContext c, String fn)
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Reader r;
	StatementList sl;
	try {
	    fn = findFile(c, fn);
	    r = new FileReader(fn);
	    sl = parseFile(r, fn);
	} catch(FileNotFoundException e) {
	    throw new ParseException(this, e.getMessage());
	} catch(ParseException e) {
	    throw new ParseException(this, e.getMessage());
	}
	if(sl != null)
	    sl.execute(c);
    }

    public void execute(ExecutionContext c)
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	StringValue fnValue = (StringValue)filename.evaluate(c);
        String fnString = fnValue.getFlattenedValue();
	runFile(c, fnString.replace('/',File.separatorChar));
    }
}
