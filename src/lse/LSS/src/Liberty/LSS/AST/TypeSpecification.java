package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public abstract class TypeSpecification extends ExpressionBase
{
    public abstract Type getType(ExecutionContext c) 
	throws TypeException,
	       UninitializedVariableException;
 
    public Value evaluate(ExecutionContext c) 
    throws TypeException,
	   UninitializedVariableException {
	return getType(c);
    }

    public static Type getType(ExecutionContext c, Expression expr) 
	throws TypeException,
	       UninitializedVariableException {
	Value v;
	try {
	    v = expr.evaluate(c);
	} catch(LSSException e) {
	    if(e instanceof TypeException)
		throw (TypeException)e;
	    else if(e instanceof UninitializedVariableException)
		throw (UninitializedVariableException)e;
	    else {
		if(e.hasLineNumber())
		    throw new TypeException(e.getSourceLine(), 
					    e.getRawMessage());
		else
		    throw new TypeException(expr, e.getRawMessage());
	    }
	}

	v = TypeRelations.attemptDeref(v);
	v = Coerce.noaryInvoke(v, expr);

	if(!v.getType().equals(Types.typeType))
	    throw new TypeException(expr, expr.toString() + " is not a type");

	/* FIXME: This can technically only guarantee returning a
	 * type, not a Type */
	return (Type)v;
    }

    public LValue getLValue(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException, 
	       ParseException,
	       SymbolAlreadyDefinedException {
	throw new TypeException(this, "Type specifications are not LValues");
    }
}
