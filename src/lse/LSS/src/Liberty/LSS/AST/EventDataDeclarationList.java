package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class EventDataDeclarationList implements CodeObtainable {
    private EventDataDeclaration[] dataDeclarations;

    public EventDataDeclarationList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	dataDeclarations = new EventDataDeclaration[v.size()];
	dataDeclarations = (EventDataDeclaration[])v.toArray(dataDeclarations);
    }

    public Vector getEventDataDeclarationVector() {
	Vector v = new Vector();
	for(int i = 0; i < dataDeclarations.length; i++) {
	    v.add(dataDeclarations[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new EventDataDeclarationListIterator(dataDeclarations);
    }

    public String getOriginalCode() {
	String s = "";
	for(int i = 0; i < dataDeclarations.length; i++) {
	    s = s + dataDeclarations[i].getOriginalCode();
	}
	return s;
    }

    public int getLine() {
	return dataDeclarations[0].getLine();
    }

    public String getFileName() {
	return dataDeclarations[0].getFileName();
    }

    public String toString() {
	String s = "";
	for(int i = 0; i < dataDeclarations.length; i++) {
	    s = s + "  " + dataDeclarations[i] + "\n";
	}
	return s;
    }
}

class EventDataDeclarationListIterator implements Iterator {
    EventDataDeclaration[] list;
    int pos;

    EventDataDeclarationListIterator(EventDataDeclaration[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    EventDataDeclaration s = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    EventDataDeclaration s = list[pos];
	    pos++;
	    return s;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
