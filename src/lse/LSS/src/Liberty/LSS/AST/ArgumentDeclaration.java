package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ArgumentDeclaration implements CodeObtainable
{
    public Identifier id;
    public TokenInfo colon;
    public Expression type;

    public ArgumentDeclaration(Identifier i, TokenInfo c,
			       Expression t) {
	id = i;
	type = t;
	colon = c;
    }

    public String getOriginalCode() {
	return id.getOriginalCode() + colon.getOriginalCode() +
	    type.getOriginalCode();
    }

    public String toString() {
	return id.toString() + " " + colon.toString() +
	    " " + type.toString();
    }

    public int getLine() {
	return id.getLine();
    }

    public String getFileName() {
	return id.getFileName();
    }

    public Expression getTypeSpecification() {
	return type;
    }

    public Identifier getName() {
	return id;
    }
}
