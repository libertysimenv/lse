package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ModifiedTypeSpecification extends TypeSpecification
{
    private Expression typeSpec;
    private TypeModifierList modifiers;

    public ModifiedTypeSpecification(TypeModifierList l, Expression t) {
	typeSpec = t;
	modifiers = l;
    }

    public String getOriginalCode() {
	return modifiers.getOriginalCode() + typeSpec.getOriginalCode();
    }
    
    public String toString() {
	return modifiers.toString() + " " + typeSpec.toString();
    }

    public int getLine() {
	return modifiers.getLine();
    }

    public String getFileName() {
	return modifiers.getFileName();
    }

    public Type getType(ExecutionContext c) 
	throws TypeException,
	       UninitializedVariableException {
	Iterator i = modifiers.iterator();
	Type t = TypeSpecification.getType(c,typeSpec);
	while(i.hasNext()) {
	    TypeModifier m = (TypeModifier)i.next();
	    t = m.getModifiedType(t);
	}
	c.instanceSpace.addGlobalType(t);
	return t;
    }
}
