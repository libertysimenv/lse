package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class CompoundStatement extends Statement {
    private TokenInfo lbrace,rbrace;
    private StatementList statements;

    public CompoundStatement(TokenInfo lb, StatementList stmts, TokenInfo rb) {
	lbrace = lb;
	statements = stmts;
	rbrace = rb;
    }

    public String getOriginalCode() {
	Statement stmt;
	String s;

	s = lbrace.getOriginalCode();
	if(statements != null) {
	    Iterator i = statements.iterator();
	    while(i.hasNext()) {
		stmt = (Statement)i.next();
		s += stmt.getOriginalCode();
	    }
	}
	s += rbrace.getOriginalCode();
	    
	return s;
    }

    public String toString() {
	if(statements != null) {
	    String s = "";
	    Iterator i = statements.iterator();
	    Statement stmt;
	    while(i.hasNext()) {
		stmt = (Statement)i.next();
		s += stmt + "\n";
	    }
	    return s;
	} else {
	    return "";
	}
    }

    public int getLine() {
	return lbrace.getLine();
    }

    public String getFileName() {
	return lbrace.getFileName();
    }

    public StatementList getStatementList() {
	return statements;
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	c = (ExecutionContext)c.enterScope();
	if(statements != null)
	    statements.execute(c);
    }
}
