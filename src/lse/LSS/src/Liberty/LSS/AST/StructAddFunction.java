package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;
import Liberty.LSS.IMR.StructAdd;

public class StructAddFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType structAddType;

    static {
	Vector v = new Vector();
	v.add(Types.instanceRefType);
	v.add(Types.stringType);
	v.add(Types.stringType);
	v.add(Types.stringType);
	
	structAddType = new FunctionType(v, Types.voidType);
    }

    public StructAddFunction(ExecutionContext c) {
	super(structAddType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	FunctionType ft = ((FunctionType)getType());
	
	if(argValues.size() != 4)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");
	
	try {
	    Vector v = new Vector();
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(0)));
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(1)));
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(2)));
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(3)));
	    argValues = v;
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}
	
	int i = 0;
	InstanceEnvironment e;
	StringValue dataStructure, fieldType, fieldName;
	try {
	    e = (InstanceEnvironment)argValues.elementAt(i++);
	    dataStructure = (StringValue)argValues.elementAt(i++);
	    fieldType = (StringValue)argValues.elementAt(i++);
	    fieldName = (StringValue)argValues.elementAt(i++);
	} catch(ClassCastException e2) {
	    throw new FunctionException("Argument " + 
					Integer.toString(i) + " in call to " +
					"function with type " + 
					getType() + " must have type " +
					ft.getArgumentType(i-1));
	} 

	try {
	    StructAdd sa = new StructAdd(dataStructure, fieldType, fieldName);
	    e.addStructAdd(sa);
	} catch(SymbolAlreadyDefinedException e2) {
	    throw new FunctionException("Instance " + 
					e.getFullyQualifiedName() + 
					" already has added " +
					"a field named " + 
					fieldName.getValue() + 
					" to the structure " +
					dataStructure.getValue());
	}
	
	return new VoidValue();
    }
}
