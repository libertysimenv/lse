package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ScopedIdentifier extends Identifier 
    implements Expression {
    protected DottedIdentList pkgName;
    protected Identifier ident;
    protected TokenInfo seperator;

    public ScopedIdentifier(DottedIdentList pkg, TokenInfo sep, Identifier id) {
	super(id.toString(), id.getToken());
	pkgName = pkg;
	seperator = sep;
	ident = id;
    }

    public String toString() {
	return pkgName.toString() + 
	    seperator.toString() + 
	    ident.toString();
    }
    
    public String getOriginalCode() {
	return pkgName.getOriginalCode() +
	    seperator.getOriginalCode() +
	    ident.getOriginalCode();
    }
}
