package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public abstract class BinaryOpExpression extends ExpressionBase
{
    private TokenInfo operator;
    private Expression expr1,expr2;

    public BinaryOpExpression(Expression e1, TokenInfo o, Expression e2) {
	operator = o;
	expr1 = e1;
	expr2 = e2;
    }

    public String getOriginalCode() {
	return expr1.getOriginalCode() + 
	    operator.getOriginalCode() + 
	    expr2.getOriginalCode();
    }

    public String toString() {
	return expr1.toString() + " " +
	    operator.toString() + " " +
	    expr2.toString();
    }

    public Expression getLeftExpression() {
	return expr1;
    }

    public Expression getRightExpression() {
	return expr2;
    }

    public int getLine() {
	return operator.getLine();
    }
    
    public String getFileName() {
	return operator.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	throw new UnimplementedException("Error on line " + 
					 operator.getLine() +
					 " of " + operator.getFileName() +
					 ": Expression type not " +
					 "yet implemented");
    }
}
