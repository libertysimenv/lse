package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class CollectorDefinition implements CodeObtainable
{
    private Identifier attribute;
    private TokenInfo assign, semi;
    private StringLiteral code;

    public CollectorDefinition(Identifier i, TokenInfo a,
			       StringLiteral c, TokenInfo s) {
	attribute = i;
	assign = a;
	code = c;
	semi = s;
    }

    public String getOriginalCode() {
	return attribute.getOriginalCode() +
	    assign.getOriginalCode() +
	    code.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return attribute.toString() + " " +
	    assign.toString() + " " +
	    code.toString() +
	    semi.toString();
    }

    public int getLine() {
	return attribute.getLine();
    }

    public String getFileName() {
	return attribute.getFileName();
    }

    public void addToCollector(Collector c, ExecutionContext context) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	StringValue value = (StringValue)code.evaluate(context);

	try {
	    c.set(attribute.toString(), value);
	} catch(TypeException e) {
	    throw new TypeException("Error on line " + this.getLine() + 
				    " of " + this.getFileName() + 
				    ": " + e.getMessage());
	} catch(UndefinedSymbolException e) {
	    throw new UndefinedSymbolException("Error on line " + 
					       this.getLine() + 
					       " of " + this.getFileName() + 
					       ": " + e.getMessage());
	}
    }
}
