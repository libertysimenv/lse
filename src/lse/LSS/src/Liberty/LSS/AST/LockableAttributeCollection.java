package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.util.*;

import java.util.*;

/* We need to claim HashMap implements the BasicMap interface */
class BasicHashMap extends HashMap implements BasicMap { }

public class LockableAttributeCollection extends AbstractVariableCollection
{
    private BasicHashMap typingContext;
    private HashMap valueContext;
    private HashMap defaultValueContext;
    private HashMap lockedAttributes;
    private HashMap hardlockedAttributes;

    public LockableAttributeCollection() {
	typingContext = new BasicHashMap();
	init(typingContext);

	valueContext = new HashMap();
	defaultValueContext = new HashMap();
	lockedAttributes = new HashMap();
	hardlockedAttributes = new HashMap();
    }

    public void hardlock(String var)
	throws UndefinedSymbolException {
	if(variableDefined(var))
	    hardlockedAttributes.put(var, null);
	else
	    throw new UndefinedSymbolException("Symbol " + var + 
					       " not defined");
    }

    public void lock(String var)
	throws UndefinedSymbolException {
	if(variableDefined(var))
	    lockedAttributes.put(var, null);
	else
	    throw new UndefinedSymbolException("Symbol " + var + 
					       " not defined");
    }

    public Value getVariableValue(String var) 
	throws UndefinedSymbolException {
	if(variableDefined(var))
	    if(valueContext.containsKey(var))
		return (Value)valueContext.get(var);
	    else
		return (Value)defaultValueContext.get(var);
	else
	    throw new UndefinedSymbolException("Symbol " + var + 
					       " not defined");
    }

    public Value getVariableDefaultValue(String var) 
	throws UndefinedSymbolException {
	if(variableDefined(var))
	    return (Value)defaultValueContext.get(var);
	else
	    throw new UndefinedSymbolException("Symbol " + var + 
					       " not defined");
    }

    public Value getVariableAssignedValue(String var)
	throws UndefinedSymbolException {
	if(variableDefined(var))
	    return (Value)valueContext.get(var);
	else
	    throw new UndefinedSymbolException("Symbol " + var + 
					       " not defined");
    }

    public void setVariableValue(String var, Value v)
	throws UndefinedSymbolException,
	       TypeException {
	if(isConst(getVariableType(var)))
	    throw new TypeException("Cannot assign to symbol "
				    + var + ":" + getVariableType(var));
	
	setVariableValueInternal(var,v);
    }

    private void setVariableValueInternal(String var, Value v) 
	throws UndefinedSymbolException,
	       TypeException {
	if(v != null)
	    typeCheck(var, v);

	/* Throw type exception for hardlocked entries */
	if(hardlockedAttributes.containsKey(var))
	    throw new TypeException("Cannot change value of attribute " + 
				    var + " once it is read");

	/* If the variable is locked store the new value as a default
	   value */
	if(lockedAttributes.containsKey(var))
	    defaultValueContext.put(var,v);
	else
	    valueContext.put(var,v);
    }

    public void initVariable(String var, Value v) 
	throws TypeException,
	       UndefinedSymbolException {
	if(v != null)
	    setVariableValueInternal(var,v);
    }

    public void addVariable(String var, Type t, Value v)
	throws SymbolAlreadyDefinedException,
	       TypeException {
	super.addVariable(var, t, v);
	defaultValueContext.put(var,null);
	try {
	    initVariable(var, v);
	} catch(UndefinedSymbolException e) {
	    /* This shouldn't be able to happen */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    public void overloadFunctionValue(String var, Value v)
	throws UndefinedSymbolException,
	       EmptyContextException,
	       TypeException {
	Value oldValue = (Value)valueContext.get(var);
	Value newValue;
	if(oldValue == null)
	    newValue = v;
	else {
	    Collection c = new Vector();
	    if(oldValue.getType() instanceof FunctionType) {
		c.add(oldValue);
	    } else if(oldValue.getType() instanceof OverloadedFunctionType) {
		c.addAll(((OverloadedFunctionValue)oldValue).getFunctions());
	    } else {
		throw new RuntimeTypeException("old type not a function, " + 
					       "but overload attempted");
	    }
	    c.add(v);

	    Type oft = getVariableType(var);
	    oft = oft.getBaseType(); /* Strip of const */
	    newValue = 
		new OverloadedFunctionValue((OverloadedFunctionType)oft, c);
	}
	initVariable(var,newValue);
    }

    public String toString() {
	return "{Values = " + valueContext.toString() +
	    ", Default Values = " + 
	    defaultValueContext.toString() + "}";
    }
}
