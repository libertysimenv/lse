package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class AggregateItemList implements CodeObtainable {
    private ArgumentDeclaration[] items;
    private TokenInfo[] semis;

    public AggregateItemList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	items = new ArgumentDeclaration[v.size()/2];
	semis = new TokenInfo[v.size()/2];

	items[0] = (ArgumentDeclaration)v.firstElement();
	for(int i = 0; i < v.size()/2; i+=1) {
	    items[i] = (ArgumentDeclaration)v.elementAt(2*i);
	    semis[i] = (TokenInfo)v.elementAt(2*i+1);
	}
    }

    public Vector getAggregateItemVector() {
	Vector v = new Vector();

	for(int i = 0; i < items.length; i++) {
	    v.add(items[i]);
	    v.add(semis[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new AggregateItemListIterator(items);
    }

    public String getOriginalCode() {
	String s = "";
	for(int i = 0; i < items.length; i++) {
	    s += items[i].getOriginalCode() + 
		semis[i].getOriginalCode();		
	}
	return s;
    }

    public String toString() {
	String s = "";
	for(int i = 0; i < items.length; i++) {
	    s += items[i].toString() + " " +
		semis[i].toString();
	}
	return s;
    }

    public int getLine() {
	return items[0].getLine();
    }

    public String getFileName() {
	return items[0].getFileName();
    }
}

class AggregateItemListIterator implements Iterator {
    ArgumentDeclaration[] list;
    int pos;

    AggregateItemListIterator(ArgumentDeclaration[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    ArgumentDeclaration d = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    ArgumentDeclaration d = list[pos];
	    pos++;
	    return d;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
