package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class IntLiteral extends Literal {
    private long value;

    public IntLiteral(long v, TokenInfo ti) {
	super(ti);
	value = v;
    }

    public IntLiteral(Integer v, TokenInfo ti) {
	this(v.intValue(), ti);
    }

    public IntLiteral(Long v, TokenInfo ti) {
	this(v.longValue(), ti);
    }

    public long getValue() {
	return value;
    }

    public String toString() {
	return Long.toString(value);
    }

    public Value evaluate(ExecutionContext c) {
	return new NumericIntValue(this.getValue());
    }
}
