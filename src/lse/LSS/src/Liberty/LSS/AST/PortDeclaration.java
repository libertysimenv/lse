package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class PortDeclaration extends Statement {
    private TokenInfo portToken;
    private TokenInfo colon, semi;
    private Expression typeSpec;
    private IdentifierList portList;

    public PortDeclaration(TokenInfo p, IdentifierList l, TokenInfo c, 
			   Expression t, TokenInfo s) {
	portToken = p;
	colon = c;
	semi = s;
	typeSpec = t;
	portList = l;
    }

    public String getOriginalCode() {
	return portToken.getOriginalCode() + portList.getOriginalCode() + 
	    colon.getOriginalCode() + typeSpec.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return portToken.toString() + " " + portList.toString() + 
	    " " + colon.toString() + " " + typeSpec.toString() +
	    semi.toString();
    }

    public int getLine() {
	return portToken.getLine();
    }

    public String getFileName() {
	return portToken.getFileName();
    }

    public static Port newPort(ExecutionContext cp,
			       CodeObtainable src,
			       String name, Expression typeSpec, 
			       TokenInfo direction) 
    throws TypeException,
	   SymbolAlreadyDefinedException,
	   UndefinedSymbolException,
	   UninitializedVariableException {
	int dir = direction.getText().equals("inport") ? Port.INPUT :
	    Port.OUTPUT;
	return newPort(cp, src, name, typeSpec, dir);
    }

    public static Port newPort(ExecutionContext cp,
			       CodeObtainable src,
			       String name, Expression typeSpec,
			       int dir) 
	throws TypeException, 
	       SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       UninitializedVariableException {
	try {
	    Type pc = TypeSpecification.getType(cp, typeSpec);
	    InstanceEnvironment e = 
		cp.instanceSpace.getCurrentEnvironment();
	    Port p = e.addPort(name, dir, pc);
	    
	    /* Add port events to this instance */
	    addPortEvents(e,p);
	    
	    return p;
	} catch(TypeException e) {
	    if(e.hasLineNumber()) throw e;
	    else throw new TypeException(src, e.getMessage());
	} catch(SymbolAlreadyDefinedException e) {
	    if(e.hasLineNumber()) throw e;
	    else throw new SymbolAlreadyDefinedException(src, e.getMessage());
	} 
    }

    public void execute(ExecutionContext cp) 
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException, 
	       UnimplementedException, 
	       TypeException,
	       UninitializedVariableException {
	Iterator i = portList.iterator();
	while(i.hasNext()) {
	    Identifier port = (Identifier)i.next();
	    try {
		Port p = newPort(cp, this, port.toString(), typeSpec, portToken);
		cp.addVariable(port.toString(), 
			       new ConstType(Types.portRefType), p);
		
	    } catch(SymbolAlreadyDefinedException e) {
		throw new SymbolAlreadyDefinedException("Error on line " + 
							getLine() + 
							" of " + 
							getFileName() + 
							": " + e.getMessage());
	    } 
	}
    }

    public static void addPortEvents(InstanceEnvironment e, Port p) {
	Liberty.LSS.IMR.Event ev;

	try {
	    StringValue portType = new StringValue("LSE_port_type(" + 
						   p.getName() + ") *");

	    /* Global versions */
	    ev = new Liberty.LSS.IMR.Event(p.getName() + ".resolved");
	    ev.addData("porti", new StringValue("int"));
	    ev.addData("status", new StringValue("LSE_signal_t"));
	    ev.addData("prevstatus", new StringValue("LSE_signal_t"));
	    ev.addData("id", new StringValue("LSE_dynid_t"));
	    ev.addData("datap", portType);
	    e.addEvent(ev);
	    	    
	    /* Local versions */
	    ev = new Liberty.LSS.IMR.Event(p.getName() + ".localresolved");
	    ev.addData("porti", new StringValue("int"));
	    ev.addData("status", new StringValue("LSE_signal_t"));
	    ev.addData("prevstatus", new StringValue("LSE_signal_t"));
	    ev.addData("id", new StringValue("LSE_dynid_t"));
	    ev.addData("datap", portType);
	    e.addEvent(ev);
	    
	} catch(SymbolAlreadyDefinedException ex) {
	    throw new RuntimeTypeException("Error adding port events: " +
					   ex.getMessage());
	} catch(TypeException ex) {
	    throw new RuntimeTypeException("Error adding port events: " +
					   ex.getMessage());
	}
    }
}
