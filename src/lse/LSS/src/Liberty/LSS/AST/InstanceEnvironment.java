package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.util.*;
import Liberty.LSS.IMR.*;
import java.util.*;

public abstract class InstanceEnvironment 
    extends ValueBase implements Aggregate
{
    public InstanceEnvironment() {
	super(Types.instanceRefType);
	processed = false;
    }

    private boolean processed;
    
    public void setProcessedFlag() {
	processed = true;
    }

    public boolean isProcessed() {
	return processed;
    }

    /******************/
    /* Name functions */
    /******************/
    public abstract String getName();
    public abstract String getFullyQualifiedName();

    /**************************/
    /* Sub-instance functions */
    /**************************/
    public abstract Liberty.util.Queue getPendingInstances();
    public abstract InstanceEnvironment addInstance(String name, Module m, 
						    CodeObtainable il)
	throws SymbolAlreadyDefinedException;
    public abstract InstanceEnvironment getInstance(String name) 
	throws UndefinedSymbolException;

    /******************/
    /* Port Functions */
    /******************/
    public abstract Port addPort(String name, int dir, Type c) 
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       TypeException,
	       UninitializedVariableException;
    public abstract Collection getPorts();
    public abstract Port getPort(String name)
	throws UndefinedSymbolException;
    public abstract void addConnection(Connection c);
    public abstract Liberty.util.Queue getConnectionLog();

    /***********************/
    /* Parameter Functions */
    /***********************/
    public abstract Parameter addParameter(String name, Type t, 
					   boolean importFlag,
					   boolean exportFlag)
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       TypeException,
	       UninitializedVariableException;
    public abstract LockableAttributeCollection getParameterValues();
    public abstract Parameter getParameter(String name)
	throws UndefinedSymbolException;

    /*******************/
    /* Query functions */
    /*******************/
    public abstract void addQuery(Query q)
	throws SymbolAlreadyDefinedException,
	       TypeException;

    public abstract Collection getQueries();

    /***********************/
    /* StructAdd functions */
    /***********************/
    public abstract void addStructAdd(StructAdd a) 
	throws SymbolAlreadyDefinedException,
	       TypeException;

    public abstract Collection getStructAdds();

    /**********************/
    /* Subfield functions */
    /**********************/
    public abstract Value getSubValue(String field)
	throws TypeException;
    public abstract LValue getSubLValue(String field) 
	throws TypeException;

    /******************/
    /* Assignment Log */
    /******************/
    public abstract void addAssignment(AssignmentLogEntry e)
	throws TypeException;
    public abstract Collection getAssignmentLog();
    public abstract boolean assignmentLogIsEmpty();

    public abstract void addImplicitAssignment(AssignmentLogEntry e)
	throws TypeException;
    public abstract Collection getImplicitAssignmentLog();
    public abstract boolean implicitAssignmentLogIsEmpty();

    /*******************/
    /* Event functions */
    /*******************/
    public abstract void addEvent(Event e)
	throws SymbolAlreadyDefinedException,
	       TypeException;
    public abstract Collection getEvents();
    public abstract void addCollector(Collector c)
	throws TypeException;
    public abstract List getCollectors();

    /******************/
    /* Exported Types */
    /******************/
    public abstract void addExportedType(String name, Type type)
	throws SymbolAlreadyDefinedException,
	       TypeException;
    public abstract Map getExportedTypes();

    /***************/
    /* Domain Path */
    /***************/
    public abstract void addToDomainSearchPath(DomainSearchPathEntry spe);
    public abstract List getLocalDomainSearchPath();
    public abstract List getHierDomainSearchPath();

    /******************************/
    /* Type Inference Constraints */
    /******************************/
    public abstract void addConstraint(Liberty.LSS.types.inference.Constraint c);
    public abstract Set getConstraints();

    /**********/
    /* Others */
    /**********/
    public abstract Module getModule();
    public abstract InstanceEnvironment getParent();
    public abstract Instance getInstance();
    public abstract void setInstance(Instance i);
    public abstract CodeObtainable getInstantiationLocation();

    /*******************/
    /* Debug functions */
    /*******************/
    public abstract void dump();
}
