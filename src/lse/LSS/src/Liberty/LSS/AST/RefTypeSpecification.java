package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class RefTypeSpecification extends TypeSpecification
{
    private TokenInfo typeToken;
    private TokenInfo refToken;

    public RefTypeSpecification(TokenInfo t, TokenInfo r) {
	typeToken = t;
	refToken = r;
    }

    public String getOriginalCode() {
	return typeToken.getOriginalCode() + 
	    refToken.getOriginalCode();
    }
    
    public String toString() {
	return typeToken.toString() + " " +
	    refToken.toString();
    }

    public int getLine() {
	return typeToken.getLine();
    }

    public String getFileName() {
	return typeToken.getFileName();
    }

    public Type getType(ExecutionContext c) {
	if(typeToken.toString().equals("parameter"))
	    return Types.parameterRefType;
	if(typeToken.toString().equals("port"))
	    return Types.portRefType;
	if(typeToken.toString().equals("instance"))
	    return Types.instanceRefType;
	if(typeToken.toString().equals("runtime_var"))
	    return Types.runtimeVarRefType;
	if(typeToken.toString().equals("domain"))
	    return Types.domainInstanceRefType;
	return null;
    }
}
