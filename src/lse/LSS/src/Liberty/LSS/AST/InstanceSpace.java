package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.IMR.*;

import java.util.*;

public class InstanceSpace
{
    /* Current Instance */
    private InstanceEnvironment currentEnv;

    /* Package stuff */
    private HashMap packages; 
    private Stack packageProcessStack;

    /* Types stuff */
    private LinkedHashMap globalTypes;
    private int nextType;
    private int nextTypeVar;

   /* Runtime Var stuff */
    private LinkedList runtimeVars;
    private int nextVar;

    /* Top-level event/collector stuff */
    private Map events;
    private Vector collectors;

    /* Domains stuff */
    private Map domainInstances;
    private Set domainClasses;

    public InstanceSpace() {
	packages = new HashMap();
	packageProcessStack = new Stack();
	globalTypes = new LinkedHashMap();
	runtimeVars = new LinkedList();
	events = new HashMap();
	domainInstances = new LinkedHashMap();
	domainClasses = new LinkedHashSet();
	collectors = new Vector();
	nextType = 0;
	nextTypeVar = 0;
	nextVar = 0;
    }

    public void setCurrentEnvironment(InstanceEnvironment e) {
	currentEnv = e;
    }

    public InstanceEnvironment getCurrentEnvironment() {
	return currentEnv;
    }

    /* Packages */
    public void addPackage(Package p) 
	throws SymbolAlreadyDefinedException {
	String name = p.getName();
	if(packages.containsKey(name))
	    throw new SymbolAlreadyDefinedException("Package " + name + 
						    " already exists");
	else
	    packages.put(name, p);
    }

    public Package getPackage(String name)
	throws UndefinedSymbolException {
	if(packages.containsKey(name))
	    return (Package)packages.get(name);
	else
	    throw new UndefinedSymbolException("Package " + name + 
					       " does not exist");
    }

    /* Package Stack */
    public Stack getPackageProcessStack() {
	return packageProcessStack;
    }    

    /* Global Types */
    public String getUniqueTypeVarName() {
	String s = "var" + nextTypeVar;
	nextTypeVar++;
	return s;
    }

    public void addGlobalType(Type t) {
	if(!globalTypes.containsKey(t)) {
	    /* Pick a unique name */
	    String name = "LSEut_" + nextType;
	    nextType++;
	    
	    /* Add the type */
	    globalTypes.put(t, name);
	}
    }
    
    public static LinkedHashMap filterGlobalTypes(Map from) {
	LinkedHashMap to = new LinkedHashMap();
	Iterator i = from.entrySet().iterator();

	while(i.hasNext()) {
	    Map.Entry entry = (Map.Entry)i.next();
	    Type t = (Type)entry.getKey();

	    /* We want to skip over types with no backend types.
	     * This is necessary for function types */
	    try {		
		t.getBackendType();
		to.put(entry.getKey(), entry.getValue());
	    } catch(RuntimeTypeException e) {
	    }
	}
	return to;
    }


    /* Note that in this function, the order of elements from the
     * iterator IS significant.  That is why we are returning a
     * LinkedHashMap rather than a Map.  The order reflects the a
     * particular full ordering of the partial ordering type
     * dependences.  In the future, we may have to remember a DAG of
     * type dependences */
    public LinkedHashMap getGlobalTypes() {
	return globalTypes;
    }

    /* Runtime Vars */
    public String getUniqueVarName() {
	String name =  "LSEuv_" + nextVar;
	nextVar++;
	return name;
    }

    public void addRuntimeVar(RuntimeVarRefValue rv) {
	runtimeVars.add(rv);
    }

    public List getRuntimeVars() {
	LinkedList returnValue = new LinkedList();
	Iterator i = runtimeVars.iterator();
	while(i.hasNext()) {
	    RuntimeVarRefValue astrv = (RuntimeVarRefValue)i.next();
	    RuntimeVar imrrv = new RuntimeVar(astrv.getUserName(),
					      astrv.getUniqueName(),
					      astrv.getVarType());
	    returnValue.add(imrrv);
	}
	return returnValue;
    }


    /* Toplevel events/collectors */
    public Event getEvent(String name) {
	return (Event)events.get(name);
    }
    public void addEvent(Event e) {
	events.put(e.getName(), e);
    }
    public Collection getEvents() {
	return events.values();
    }
    public void addCollector(Liberty.LSS.IMR.Collector c) {
	collectors.add(c);
    }
    public Collection getCollectors() {
	return collectors;
    }

    /* Domains */
    public void addDomainInstance(DomainInstanceRefValue dirv) 
	throws SymbolAlreadyDefinedException {
	String qualName = dirv.getQualifiedName();
	if(domainInstances.containsKey(qualName)) {
	    throw new SymbolAlreadyDefinedException("Domain instance " + 
						    qualName + 
						    " already exists");
	}
	
	domainInstances.put(qualName, dirv);
    }

    public Collection getDomainInstances() {
	return domainInstances.values();
    }

    public Map getDomainInstanceMap() {
	return domainInstances;
    }

    public void addDomainClass(String name) {
	domainClasses.add(name);
    }

    public Set getDomainClasses() {
	return domainClasses;
    }
}
