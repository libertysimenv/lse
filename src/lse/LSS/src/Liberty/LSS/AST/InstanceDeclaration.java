package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
public class InstanceDeclaration extends Statement {
    private TokenInfo instToken;
    private TokenInfo colon, semi;
    private Identifier name;
    private Identifier moduleName;

    public InstanceDeclaration(TokenInfo i, Identifier n, TokenInfo c, 
			       Identifier m, TokenInfo s) {
	instToken = i;
	colon = c;
	semi = s;
	name = n;
	moduleName = m;
    }

    public String getOriginalCode() {
	return instToken.getOriginalCode() + name.getOriginalCode() + 
	    colon.getOriginalCode() + moduleName.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return instToken.toString() + " " + name.toString() + 
	    " " + colon.toString() + " " + moduleName.toString() +
	    semi.toString();
    }

    public int getLine() {
	return instToken.getLine();
    }

    public String getFileName() {
	return instToken.getFileName();
    }

    public static InstanceEnvironment newInstance(ExecutionContext c,
						  CodeObtainable src,
						  String moduleName,
						  String name) 
	throws UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       TypeException {
	/* Sanity check the instance name.  We don't allow '.' in the
	 * instance name, '__', '___'*/
	int index;
	index = name.indexOf(".");
	if(index != -1)
	    throw new TypeException(src,"Instance names cannot contain '.'");
	index = name.indexOf("___");
	if(index != -1)
	    throw new TypeException(src,"Instance names cannot contain '___'");
	index = name.indexOf("__");
	if(index != -1)
	    throw new TypeException(src, "Instance names cannot contain '__'");

	try {
	    Module module = c.getModule(moduleName);
	    InstanceEnvironment e = c.instanceSpace.getCurrentEnvironment();
	    e = e.addInstance(name, module, src);
	    return e;
	} catch(UndefinedSymbolException e) {
	    throw new UndefinedSymbolException("Error on line " + 
					       src.getLine() + 
					       " of " + src.getFileName() + 
					       ": " + e.getMessage());
	} catch(SymbolAlreadyDefinedException e) {
	    throw new SymbolAlreadyDefinedException("Error on line " + 
						    src.getLine() + 
						    " of " + 
						    src.getFileName() + 
						    ": " + e.getMessage());
	}
    }

    public void execute(ExecutionContext c) 
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException, 
	       UnimplementedException, 
	       TypeException {

	InstanceEnvironment e = newInstance(c,this,moduleName.toString(),
					    name.toString());
	try {
	    c.addVariable(name.toString(), 
			  new ConstType(Types.instanceRefType), e);
	} catch(SymbolAlreadyDefinedException ex) {
	    throw new SymbolAlreadyDefinedException("Error on line " + 
						    getLine() + 
						    " of " + getFileName() + 
						    ": " + ex.getMessage());
	}
    }
}
