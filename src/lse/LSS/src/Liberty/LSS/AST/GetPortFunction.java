package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;

import Liberty.LSS.types.inference.*;

public class GetPortFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType myType;

    static {
	Vector v = new Vector();
	v.add(Types.instanceRefType);
	v.add(Types.stringType);
	
	myType = new FunctionType(v, Types.portRefType);
    }

    public GetPortFunction(ExecutionContext c) {
	super(myType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, final CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	FunctionType ft = ((FunctionType)getType());
	
	if(argValues.size() != 2)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");
	
	try {
	    Vector v = new Vector();
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(0)));
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(1)));
	    argValues = v;
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}
	
	int i = 0;
	InstanceEnvironment instance;
	StringValue field;
	try {
	    instance = (InstanceEnvironment)argValues.elementAt(i++);
	    field = (StringValue)argValues.elementAt(i++);
	} catch(ClassCastException e2) {
	    throw new FunctionException("Argument " + 
					Integer.toString(i) + " in call to " +
					"function with type " + 
					getType() + " must have type " +
					ft.getArgumentType(i-1));
	} 

	return instance.getSubValue(field.getFlattenedValue());
    }
}
