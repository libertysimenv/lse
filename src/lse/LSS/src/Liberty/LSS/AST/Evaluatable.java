package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

interface Evaluatable {
    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException;
}
