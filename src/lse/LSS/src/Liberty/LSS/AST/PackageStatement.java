package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class PackageStatement extends Statement
{
    private TokenInfo packageToken, semi;
    private DottedIdentList name;

    public PackageStatement(TokenInfo packageToken, DottedIdentList name,
			    TokenInfo semi) {
	this.packageToken = packageToken;
	this.name = name;
	this.semi = semi;
    }

    public String getOriginalCode() {
	return packageToken.getOriginalCode() +
	    name.getOriginalCode() + 
	    semi.getOriginalCode();
    }

    public String toString() {
	return packageToken.toString() + 
	    " " + name.toString() + semi.toString();
    }

    public int getLine() {
	return packageToken.getLine();
    }

    public String getFileName() {
	return packageToken.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	/* This statement is basically an optional assertion of
	 * package name */
	String currentPackageName = c.getCurrentPackage().getName();
	if(currentPackageName == null)
	    currentPackageName = "(default)";
	if(!currentPackageName.equals(name.toString())) {
	    throw new TypeException(this,
				    "File " + getFileName() + 
				    " defines package " + name + 
				    " but was imported as " + 
				    currentPackageName);
	}
    }
}
