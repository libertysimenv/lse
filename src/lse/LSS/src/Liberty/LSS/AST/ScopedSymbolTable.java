package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.util.*;

import java.util.*;

public class ScopedSymbolTable implements BasicMap
{
    ScopedSymbolTable prev;
    CheckpointMap m;

    public ScopedSymbolTable() {
	prev = null;
	m = new CheckpointHashMap();
    }

    private ScopedSymbolTable(ScopedSymbolTable st) {
	this();
	prev = st;
    }

    /* Returns a new ScopedSymbolTable which is identical to this one.
       However, this ScopedSymbolTable gets locked at the current scoping
       level and lower.  That is to say, no new variables can be added.
       You can still enter new scopes with the locked ScopedSymbolTable */
    public ScopedSymbolTable checkpoint() {
	ScopedSymbolTable s;
	if(prev != null)
	    s = new ScopedSymbolTable(prev.checkpoint());
	else
	    s = new ScopedSymbolTable();
	s.m = m.checkpoint();
	return s;
    }

    public boolean symbolDefined(String sym) {
	if(m.containsKey(sym)) return true;
	if(prev != null) return prev.symbolDefined(sym);
	return false;
    }

    public boolean symbolDefinedCurrentScope(String sym) {
	return m.containsKey(sym);
    }

    public Object get(String sym) 
	throws UndefinedSymbolException {
	if(m.containsKey(sym)) return m.get(sym);
	else if(prev != null) return prev.get(sym);
	else throw new UndefinedSymbolException("Symbol " + 
						sym + " not defined");
    }
    
    public void set(String sym, Object v)
	throws UndefinedSymbolException {
	if(m.containsKey(sym)) m.put(sym,v);
	else if(prev != null) prev.set(sym,v);
	else throw new UndefinedSymbolException("Symbol " + sym + " not defined");
    }

    public void add(String sym, Object v) 
	throws SymbolAlreadyDefinedException {
	if(m.containsKey(sym))
	    throw new SymbolAlreadyDefinedException("Symbol " + sym + 
						    " already defined");
	m.put(sym,v);
    }

    public ScopedSymbolTable enterScope() {
	return new ScopedSymbolTable(this);
    }

    public ScopedSymbolTable leaveScope() {
	return prev;
    }

    public int scopeDepth() {
	if(prev == null) return 1;
	else return 1+prev.scopeDepth();
    }
    
    public String toString() {
	String s = "Scope Depth " + scopeDepth() + ": \n";
	s += m.entrySet().toString();
	if(prev != null) s += "\n" + prev.toString();
	return s;
    }

    /* BasicMap interface functions */

    public boolean containsKey(Object key) {
	return symbolDefined(key.toString());
    }

    public Object get(Object key) {
	try {
	    return get(key.toString());
	} catch(UndefinedSymbolException e) {
	    return null;
	}
    }
    
    public Object put(Object key, Object value) {
	try {
	    Object prev = get(key.toString());
	    set(key.toString(), value);
	    return prev;
	} catch(UndefinedSymbolException e) {
	    try {
		add(key.toString(), value);
		return null;
	    } catch(SymbolAlreadyDefinedException e2) {
		/* Hmmmm.... */
		throw new RuntimeTypeException(e2.getMessage());
	    }
	}
    }

    public Set keySet() {
	if(prev == null) {
	    Set s = m.keySet();
	    return new HashSet(s);
	} else {
	    Set s = prev.keySet();
	    s.addAll(m.keySet());
	    return s;
	}
    }
}
