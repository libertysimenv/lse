package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.util.*;
import Liberty.LSS.IMR.*;
import java.util.*;

public class BaseInstanceEnvironment extends InstanceEnvironment
{
    protected Liberty.util.Queue pendingInstances;
    protected Liberty.util.Queue connectionLog;
    protected HashMap instances;
    protected HashMap members;
    protected HashMap parameters;
    protected Vector collectors;
    protected LockableAttributeCollection parameterValues;
    protected List localDomainSearchPath;
    protected Set constraints;

    public BaseInstanceEnvironment() {
	super();
	pendingInstances = new Liberty.util.Queue();
	connectionLog = new Liberty.util.Queue();
	instances = new HashMap();
	members = new HashMap();
	parameters = new HashMap();
	collectors = new Vector();
	parameterValues = new LockableAttributeCollection();
	localDomainSearchPath = new Vector();
	constraints = new HashSet();
    }
    
    /******************/
    /* Name functions */
    /******************/
    public String getName() { 
	return null; 
    }

    public String getFullyQualifiedName() { 
	return null; 
    }

    /**************************/
    /* Sub-instance functions */
    /**************************/
    public Liberty.util.Queue getPendingInstances() {
	return pendingInstances;
    }

    public InstanceEnvironment addInstance(String name, Module m, 
					   CodeObtainable instantiationLocation)
	throws SymbolAlreadyDefinedException {
	
	InstanceEnvironment child = 
	    new RealInstanceEnvironment(name, m, this, instantiationLocation);
	if(members.containsKey(name))
	    throw new SymbolAlreadyDefinedException("Symbol " + 
						    child.getName() + 
						    " is already defined");
	pendingInstances.enqueue(child);
	instances.put(name, child);
	members.put(name,null);
	return child;
    }

    public InstanceEnvironment getInstance(String name) 
	throws UndefinedSymbolException {
	if(instances.containsKey(name))
	    return (InstanceEnvironment)instances.get(name);
	else
	    throw new UndefinedSymbolException("No such sub-instance " + 
					       name);
    }

    /******************/
    /* Port Functions */
    /******************/
    public Port addPort(String name, int dir, Type c) 
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       TypeException,
	       UninitializedVariableException {
	throw new TypeException("Cannot add ports to the toplevel");
    }

    public Collection getPorts() {
	HashMap m = new HashMap();
	return m.values();
    }

    public Port getPort(String name)
	throws UndefinedSymbolException {
	throw new UndefinedSymbolException("No such port " + name);
    }

    public void addConnection(Connection c) {
	connectionLog.enqueue(c);
    }

    public Liberty.util.Queue getConnectionLog() {
	return connectionLog;
    }

    /***********************/
    /* Parameter Functions */
    /***********************/

    public Parameter addParameter(String name, Type t, 
				  boolean importFlag, boolean exportFlag)
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       TypeException,
	       UninitializedVariableException {
	Parameter p = new Parameter(name, t, parameterValues,
				    getAssignmentLog(),
				    importFlag,exportFlag);
	if(members.containsKey(name))
	    throw new SymbolAlreadyDefinedException("Symbol " + p + 
						    " is already defined");
	parameters.put(name,p);
	members.put(name,null);
	return p;
    }

    public LockableAttributeCollection getParameterValues() {
	return parameterValues;
    }

    public Parameter getParameter(String name)
	throws UndefinedSymbolException {
	if(parameters.containsKey(name))
	    return (Parameter)parameters.get(name);
	else
	    throw new UndefinedSymbolException("No parameter named " + 
					       name + " exists");
    }

    /*******************/
    /* Query functions */
    /*******************/
    public void addQuery(Query q)
	throws SymbolAlreadyDefinedException,
	       TypeException {
	throw new TypeException("Cannot add queries to the toplevel");
    }

    public Collection getQueries() {
	HashMap m = new HashMap();
	return m.values();
    }

    /***********************/
    /* StructAdd functions */
    /***********************/
    public void addStructAdd(StructAdd a) 
	throws SymbolAlreadyDefinedException,
	       TypeException {
	throw new TypeException("Cannot add to structures at the toplevel");
    }

    public Collection getStructAdds() {
	HashMap m = new HashMap();
	return m.values();
    }

    /**********************/
    /* Subfield functions */
    /**********************/
    public Value getSubValue(String field) 
	throws TypeException {
	/* Check parameters */
	if(parameters.containsKey(field))
	    return (Value)parameters.get(field);
	
	throw new TypeException("toplevel instance has no " + 
				"member named " + field);
    }

    public LValue getSubLValue(String field) 
	throws TypeException {
	/* Check parameters */
	if(parameters.containsKey(field)) {
	    Value parm = (Value)parameters.get(field);
	    return parm.getType().getLValue(parm);
	}

	throw new TypeException("toplevel instance has no " + 
				"member named " + field);
    }

    /******************/
    /* Assignment Log */
    /******************/
    public void addAssignment(AssignmentLogEntry e)
	throws TypeException {
	throw new TypeException("Cannot add assignment to the toplevel");
    }

    public Collection getAssignmentLog() {
	return new Vector();
    }

    public boolean assignmentLogIsEmpty() {
	return true;
    }

    public void addImplicitAssignment(AssignmentLogEntry e)
	throws TypeException {
	throw new TypeException("Cannot add implict assignment to the " + 
				"toplevel");
    }

    public Collection getImplicitAssignmentLog() {
	return new Vector();
    }

    public boolean implicitAssignmentLogIsEmpty() {
	return true;
    }

    /*******************/
    /* Event functions */
    /*******************/
    public void addEvent(Event e)
	throws SymbolAlreadyDefinedException,
	       TypeException {
	throw new TypeException("Cannot add events to the toplevel");
    }

    public Collection getEvents() {
	HashMap m = new HashMap();
	return m.values();
    }

    public void addCollector(Collector c)
	throws TypeException {
	collectors.add(c);
    }

    public List getCollectors() {
	return collectors;
    }

    /******************/
    /* Exported Types */
    /******************/
    public void addExportedType(String name, Type type)
	throws TypeException,
	       SymbolAlreadyDefinedException {
	throw new TypeException("Cannot export types at the toplevel");
    }

    public Map getExportedTypes() {
	return new HashMap();
    }

    /***************/
    /* Domain Path */
    /***************/
    public void addToDomainSearchPath(DomainSearchPathEntry spe) {

	ListIterator i = localDomainSearchPath.listIterator();
	
	while (i.hasNext()) {
	    DomainSearchPathEntry entry = (DomainSearchPathEntry)i.next();
	    if (spe.getDomainClass().compareTo(entry.getDomainClass())==0) {
		i.set(spe);
		return;
	    }
	}
	localDomainSearchPath.add(spe);
    }

    public List getLocalDomainSearchPath() {
	return localDomainSearchPath;
    }

    public List getHierDomainSearchPath() {
	return new Vector();
    }

    /******************************/
    /* Type Inference Constraints */
    /******************************/
    public void addConstraint(Liberty.LSS.types.inference.Constraint c) {
	constraints.add(c);
    }

    public Set getConstraints() {
	return constraints;
    }

    /**********/
    /* Others */
    /**********/
    public Module getModule() {
	return null;
    }

    public InstanceEnvironment getParent() {
	return null;
    }

    public Instance getInstance() {
	return null;
    }

    public void setInstance(Instance i) {
	/* Do nothing */
    }

    public CodeObtainable getInstantiationLocation() {
	return null;
    }

    /*******************/
    /* Debug functions */
    /*******************/
    public void dump() {
    }
}
