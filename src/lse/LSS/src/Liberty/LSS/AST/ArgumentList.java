package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ArgumentList implements CodeObtainable {
    private ArgumentDeclaration[] arguments;
    private TokenInfo[] commas;

    public ArgumentList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	arguments = new ArgumentDeclaration[(v.size()+1)/2];
	commas = new TokenInfo[(v.size()-1)/2];

	try {
	    arguments[0] = (ArgumentDeclaration)v.firstElement();
	    for(int i = 1; i < v.size(); i+=2) {
		commas[(i-1)/2] = (TokenInfo)v.elementAt(i);
		arguments[(i+1)/2] = (ArgumentDeclaration)v.elementAt(i+1);
	    }
	} catch(ClassCastException e) {
	    throw new ArrayStoreException("Cannot convert given " + 
					  "vector into an ArgumentList");
	}
    }

    public Vector getArgumentVector() {
	Vector v = new Vector();

	v.add(arguments[0]);
	for(int i = 1; i < arguments.length; i++) {
	    v.add(commas[i-1]);
	    v.add(arguments[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new ArgumentListIterator(arguments);
    }

    public String getOriginalCode() {
	String s = arguments[0].getOriginalCode();
	for(int i = 1; i < arguments.length; i++) {
	    s += commas[i-1].getOriginalCode() + 
		arguments[i].getOriginalCode();
	}
	return s;
    }

    public String toString() {
	String s = arguments[0].toString();
	for(int i = 1; i < arguments.length; i++) {
	    s += commas[i-1].toString() + " " +
		arguments[i].toString();
	}
	return s;
    }

    public int getLine() {
	return arguments[0].getLine();
    }

    public String getFileName() {
	return arguments[0].getFileName();
    }
}

class ArgumentListIterator implements Iterator {
    ArgumentDeclaration[] list;
    int pos;

    ArgumentListIterator(ArgumentDeclaration[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    ArgumentDeclaration d = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    ArgumentDeclaration d = list[pos];
	    pos++;
	    return d;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
