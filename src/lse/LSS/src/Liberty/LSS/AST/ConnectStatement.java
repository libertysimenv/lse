package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ConnectStatement extends Statement
{
    private Expression from,to;
    private TokenInfo connectToken;
    private TokenInfo lbrack,rbrack,semi;
    private Expression constraint;

    public ConnectStatement(Expression e1, TokenInfo c,
			    Expression e2, TokenInfo s) {
	from = e1;
	to = e2;
	connectToken = c;
	semi = s;
	lbrack = null;
	rbrack = null;
	constraint = null;
    }

    public ConnectStatement(Expression e1, TokenInfo c,
			    TokenInfo lb,
			    Expression ts,
			    TokenInfo rb,
			    Expression e2, TokenInfo s) {
	this(e1,c,e2,s);
	lbrack = lb;
	rbrack = rb;
	constraint = ts;
    }

    public String getOriginalCode() {
	if(lbrack == null || rbrack == null || constraint == null) {
	    return from.getOriginalCode() + 
		connectToken.getOriginalCode() +
		to.getOriginalCode() + 
		semi.getOriginalCode();
	} else {
	    return from.getOriginalCode() + 
		connectToken.getOriginalCode() +
		lbrack.getOriginalCode() +
		constraint.getOriginalCode() +
		rbrack.getOriginalCode() +
		to.getOriginalCode() + 
		semi.getOriginalCode();
	}
    }

    public String toString() {
	if(lbrack == null || rbrack == null || constraint == null) {
	    return from.toString() + " " +
		connectToken.toString() + " " +
		to.toString() + 
		semi.toString();
	} else {
	    return from.toString() + " " +
		connectToken.toString() +
		lbrack.toString() +
		constraint.toString() + 
		rbrack.toString() + " " +
		to.getOriginalCode() + 
		semi.getOriginalCode();
	}
    }

    public int getLine() {
	return connectToken.getLine();
    }

    public String getFileName() {
	return connectToken.getFileName();
    }

    public void execute(ExecutionContext c)  
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Value fromVal = from.evaluate(c);
	Value toVal = to.evaluate(c);
	Type constraintType = null;
	if(constraint != null) 
	    constraintType = TypeSpecification.getType(c, constraint);
		
	Connection con = new Connection(fromVal, toVal, constraintType,
					this);
	InstanceEnvironment e = c.instanceSpace.getCurrentEnvironment();
	e.addConnection(con);		
    }
}
	
	
