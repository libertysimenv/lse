package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class CollectorDeclaration extends Statement
{
    private TokenInfo collectorToken;
    private TokenInfo on, semi;
    private TokenInfo lbrace,rbrace;

    private DottedIdentList eventName;

    private Expression instanceName;

    private CollectorDefinitionList definitions;

    public CollectorDeclaration(TokenInfo co, DottedIdentList e, TokenInfo o,
				Expression i, TokenInfo lb, 
				CollectorDefinitionList d,
				TokenInfo rb, TokenInfo s) {
	collectorToken = co;
	eventName = e;
	on = o;
	instanceName = i;
	lbrace = lb;
	definitions = d;
	rbrace = rb;
	semi = s;
    }


    public String getOriginalCode() {
	String defStr = definitions == null ? "" :
	    definitions.getOriginalCode();

	String iname = null;
	
	if(on != null) 
	    iname = instanceName.getOriginalCode();

	return collectorToken.getOriginalCode() + 
	    eventName.getOriginalCode() + 
	    (on == null ? "" : (on.getOriginalCode() + iname)) + 
	    lbrace.getOriginalCode() +
	    defStr +
	    rbrace.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	String defStr = definitions == null ? "" :
	    definitions.toString();

	String iname = null;

	if(on != null)
	    iname = instanceName.toString();

	return collectorToken.toString() + " " +
	    eventName.toString() + " " +
	    (on == null ? "" : (on.toString() + " " + iname + " ")) +
	    lbrace.toString() +
	    defStr + "\n" + 
	    rbrace.toString() +
	    semi.toString();
    }

    public int getLine() {
	return collectorToken.getLine();
    }

    public String getFileName() {
	return collectorToken.getFileName();
    }

    public void execute(ExecutionContext context)
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	String[] nameList;

	if(on == null) {
	    nameList = new String[0];
	} else {
	    Value v = instanceName.evaluate(context);
	    v = TypeRelations.attemptDeref(v);
	    if(!TypeRelations.isSubtype(v.getType(), Types.stringType)) {
		throw new TypeException(this, "Instance name in collector " + 
					"definition must have type " + 
					Types.stringType);
	    }
	    String name = ((StringValue)v).getFlattenedValue();
	    nameList = name.split("\\.");
	}

	String eventNameString = eventName.toString();
	Collector c = new Collector(this, eventNameString, nameList);
	if(definitions != null)
	    definitions.addToCollector(c, context);

	InstanceEnvironment env =
	    context.instanceSpace.getCurrentEnvironment();
	env.addCollector(c);
    }
}
