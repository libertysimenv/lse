package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class IdentifierList implements CodeObtainable {
    private Identifier[] identifiers;
    private TokenInfo[] commas;

    public IdentifierList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	identifiers = new Identifier[(v.size()+1)/2];
	commas = new TokenInfo[(v.size()-1)/2];

	try {
	    identifiers[0] = (Identifier)v.firstElement();
	    for(int i = 1; i < v.size(); i+=2) {
		commas[(i-1)/2] = (TokenInfo)v.elementAt(i);
		identifiers[(i+1)/2] = (Identifier)v.elementAt(i+1);
	    }
	} catch(ClassCastException e) {
	    throw new ArrayStoreException("Cannot convert given vector into " +
					  "an IdentifierList");
	}
    }

    public Vector getIdentifierVector() {
	Vector v = new Vector();

	v.add(identifiers[0]);
	for(int i = 1; i < identifiers.length; i++) {
	    v.add(commas[i-1]);
	    v.add(identifiers[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new IdentifierListIterator(identifiers);
    }

    public String getOriginalCode() {
	String s = identifiers[0].getOriginalCode();
	for(int i = 1; i < identifiers.length; i++) {
	    s += commas[i-1].getOriginalCode() + 
		identifiers[i].getOriginalCode();
	}
	return s;
    }

    public String toString() {
	String s = identifiers[0].toString();
	for(int i = 1; i < identifiers.length; i++) {
	    s += commas[i-1].toString() + " " +
		identifiers[i].toString();
	}
	return s;
    }

    public int getLine() {
	return identifiers[0].getLine();
    }

    public String getFileName() {
	return identifiers[0].getFileName();
    }
}

class IdentifierListIterator implements Iterator {
    Identifier[] list;
    int pos;

    IdentifierListIterator(Identifier[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    Identifier d = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    Identifier d = list[pos];
	    pos++;
	    return d;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
