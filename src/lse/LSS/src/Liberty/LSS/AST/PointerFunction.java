package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class PointerFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType pointerType;

    static {
	Vector v = new Vector();
	v.add(Types.typeType);
	pointerType = new FunctionType(v, Types.typeType);
    }

    public PointerFunction(ExecutionContext c) {
	super(pointerType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	if(argValues.size() != 1)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + argValues.size() +
					" arguments");
	
	Type type;
	Value v = (Value)argValues.elementAt(0);
	try {
	    v = TypeRelations.attemptDeref(v);
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}

	if(TypeRelations.isSubtype(v.getType(), Types.typeType)) {
	    type = (Type)v;
	} else {
	    throw new FunctionException("Argument 1" + 
					" in call to " +
					"function with type " + 
					getType() +
					" must have type " +
					Types.typeType);
	}

	Vector stringv = StringValue.buildVector(type);
	Vector temp = StringValue.buildVector(" *");
	stringv.addAll(temp);
	ExternalType newType = new ExternalType(new StringValue(stringv));
	c.instanceSpace.addGlobalType(newType);
	return newType;
    }
}
