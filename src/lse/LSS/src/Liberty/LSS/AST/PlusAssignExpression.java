package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class PlusAssignExpression extends AssignExpression
{
    public PlusAssignExpression(Expression e1, TokenInfo o, Expression e2) {
        super(e1, o, e2);
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value v2 = getRightExpression().evaluate(c);
	LValue v1 = getLeftExpression().getLValue(c);
	Value v;
	try {
	    v = Operate.add(v1.getValue(),v2);
	} catch(TypeException e) {
	    throw new TypeException("Error on line " + getLine() +
				    " of " + getFileName() +
				    ": " + e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new TypeException("Error on line " + this.getLine() + 
				    " of " + this.getFileName() + 
				    ": " + e.getMessage());
	}
	assign(v1,v);
	return v;
    }
}
