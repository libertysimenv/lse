package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.IMR.DomainSearchPathEntry;
import Liberty.LSS.types.*;

import java.util.*;

public class AddToDomainSearchPathFunction extends ValueBase 
    implements Callable {
    private ExecutionContext context;

    private static FunctionType dspType;

    static {
	Vector v = new Vector();
	v.add(Types.domainInstanceRefType);
	dspType = new FunctionType(v, Types.voidType);
    }

    public AddToDomainSearchPathFunction(ExecutionContext c) {
	super(dspType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	if(argValues.size() != 1)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + argValues.size() +
					" arguments");
	
	Value v = (Value)argValues.elementAt(0);
	try {
	    v = TypeRelations.attemptDeref(v);
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}
	
	if(!v.getType().equals(Types.domainInstanceRefType)) {
	    throw new FunctionException("Argument 1 in call to " +
					"function with type " + getType() + 
					" must have type " +
					Types.domainInstanceRefType);
	}
	   
	DomainInstanceRefValue di = (DomainInstanceRefValue)v;
	String domainClass = di.getClassName();
	DomainSearchPathEntry spe = new DomainSearchPathEntry(domainClass, di);
	InstanceEnvironment env = c.instanceSpace.getCurrentEnvironment();
	env.addToDomainSearchPath(spe);

	return new VoidValue();
    }
}
