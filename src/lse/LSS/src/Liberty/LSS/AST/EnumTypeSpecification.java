package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class EnumTypeSpecification extends TypeSpecification
{
    private TokenInfo enumToken;
    private TokenInfo lbrace,rbrace;
    private IdentifierList enumerators;

    private EnumType enumType;

    public EnumTypeSpecification(TokenInfo e, TokenInfo lb, 
				 IdentifierList il, TokenInfo rb) {
	enumToken = e;
	lbrace = lb;
	rbrace = rb;
	enumerators = il;

	Iterator i = il.iterator();
	Vector ids = new Vector();
	while(i.hasNext()) {
	    Identifier id = (Identifier)i.next();
	    ids.add(id.toString());
	}
	enumType = new EnumType(ids);
    }

    public String getOriginalCode() {
	return enumToken.getOriginalCode() + 
	    lbrace.getOriginalCode() +
	    enumerators.getOriginalCode() +
	    rbrace.getOriginalCode();
    }
    
    public String toString() {
	return enumToken.toString() + " " +
	    lbrace.toString() + 
	    enumerators.toString() +
	    rbrace.toString();
    }

    public int getLine() {
	return enumToken.getLine();
    }

    public String getFileName() {
	return enumToken.getFileName();
    }

    public Type getType(ExecutionContext c) throws TypeException {
	EnumValue[] vals = enumType.getValues();
	Type constEnum = new ConstType(enumType);

	try {
	    for(int i = 0; i < vals.length; i++) {
		c.addVariable(vals[i].toString(), constEnum, vals[i]);
	    }
	} catch(SymbolAlreadyDefinedException e) {
	    throw new TypeException(enumToken, e.getMessage());
	} catch(TypeException e) {
	    throw new TypeException(enumToken, e.getMessage());
	}
	c.instanceSpace.addGlobalType(enumType);
	return enumType;
    }
}
