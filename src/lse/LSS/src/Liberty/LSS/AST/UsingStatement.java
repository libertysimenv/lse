package Liberty.LSS.AST;

import java.util.*;
import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class UsingStatement extends Statement {
    public DottedIdentList packageName;
    public TokenInfo usingToken;
    public TokenInfo semi;

    public UsingStatement(TokenInfo usingToken,
			   DottedIdentList pn,
			   TokenInfo s) {
	this.packageName = pn;
	this.usingToken = usingToken;
	this.semi = s;
    }

    public String getOriginalCode() {
	return usingToken.getOriginalCode() +
	    packageName.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return usingToken.toString() + " " +
	    packageName.toString() + " " + 
	    semi.toString();
    }

    public String getFileName() {
	return usingToken.getFileName();
    }

    public int getLine() {
	return usingToken.getLine();
    }

    public void execute(ExecutionContext c)
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	String pkgName = packageName.toString();
	Package pkg;
	try {
	    pkg = c.instanceSpace.getPackage(pkgName);
	} catch(UndefinedSymbolException e) {
	    /* This package doesn't exist.  Let's try importing it */
	    Statement s = new ImportStatement(usingToken, packageName, semi);
	    s.execute(c);
	    try {
		pkg = c.instanceSpace.getPackage(pkgName);
	    } catch(UndefinedSymbolException e2) {
		/* Huh?  We just added this package */
		throw new RuntimeTypeException(e.getMessage());
	    }
	}

	List searchList = c.getPackageSearchList();
	searchList.add(pkg);
    }
}
