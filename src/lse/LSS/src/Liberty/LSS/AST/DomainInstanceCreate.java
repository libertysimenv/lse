package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;

public class DomainInstanceCreate extends ValueBase implements Callable {
    private ExecutionContext context;
    private String domainClassName;

    protected static FunctionType domainInstanceCreateType;
    static {
	Vector v = new Vector();
	v.add(Types.stringType);
	v.add(Types.stringType);
	v.add(Types.stringType);
	domainInstanceCreateType = 
	    new FunctionType(v, Types.domainInstanceRefType);
    }

    public DomainInstanceCreate(ExecutionContext c,
				String domainClassName) {
	super(domainInstanceCreateType);
	context = c.checkpoint();
	this.domainClassName = domainClassName;
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite) 
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();

	if(argValues.size() != 3) {
	    throw new FunctionException("Must call domain instance " +
					"constructor with exactly " + 
					"3 arguments");
	}
	
	StringValue arguments[] = new StringValue[3];

	for(int i = 0; i < 3; i++) {
	    Value argument = (Value)argValues.elementAt(i);
    
	    if(!TypeRelations.isSubtype(argument.getType(), 
					Types.stringType)) {
		throw new FunctionException("Argument " + (i+1) +  
					    " in call to " +
					    "function with type " + 
					    getType() + " must have type " +
					    Types.stringType);
	    }
	    arguments[i] = ((StringValue)argument);
	}

	InstanceEnvironment env = c.instanceSpace.getCurrentEnvironment();
	String instanceName = env.getFullyQualifiedName();
	if(instanceName == null)
	    instanceName = "";

	DomainInstanceRefValue dirv;
	dirv = new DomainInstanceRefValue(domainClassName,
					  arguments[1], /* build args */
					  instanceName,
					  arguments[0], /* name */
					  arguments[2]  /* runtime args */);
	
	
	/* Add this domain instance to the list of domain instances */
	try {
	    c.instanceSpace.addDomainInstance(dirv);
	} catch(SymbolAlreadyDefinedException e) {
	    throw new FunctionException(e.getMessage());
	}

	/* Make sure that this becomes the default instance of this
	 * emulator domain if one does not already exist */
	DomainInstanceMapValue v;
	Parameter p;
	try {
	    p = env.getParameter("LSE_domain");
	    v = (DomainInstanceMapValue)p.derefNoLock();
	} catch(UndefinedSymbolException e) {
	    throw new RuntimeTypeException(e.getMessage());
	} catch(UninitializedVariableException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
	
	try {
	    v.getSubValue(domainClassName);
	} catch(UninitializedVariableException e) {
	    /* Okay, this parameter has no value yet, so let's set one... */
	    try {
		AggregateLValue alv = (AggregateLValue)p.getLValue();
		LValue lv = alv.getSubLValue(domainClassName);
		lv.assign(dirv);
	    } catch(TypeException e2) {
		/* Ignore this exception... if the parameter is
		 * locked, then it will except and this should not be
		 * a problem. */
	    }
	}
	    
	return dirv;
    }
}
