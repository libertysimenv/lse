package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class ParseException extends LSSException
{
    public ParseException(String m) {
	super(m);
    }

    public ParseException(CodeObtainable src, String message) {
	super(src, message);
    }
}
