package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ToStringFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType toStringType;

    static {
	Vector v = new Vector();
	v.add(Types.stringType);
	toStringType = new FunctionType(v, Types.stringType);
    }

    public ToStringFunction(ExecutionContext c) {
	super(toStringType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	if(argValues.size() != 1)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + argValues.size() +
					" arguments");
	
	String str;
	Value v = (Value)argValues.elementAt(0);
	try {
	    v = TypeRelations.attemptDeref(v);
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}

	if(TypeRelations.isSubtype(v.getType(), Types.stringType)) {
	    StringValue s = (StringValue)v;
	    return v;
	} else {
	    return new StringValue(v.toString());
	}
    }
}
