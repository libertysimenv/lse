package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class NewParameterExpression extends ExpressionBase {
    private TokenInfo newToken, parmToken;
    private TokenInfo lparen, rparen, comma;
    private Expression parmNameExpr;
    private Expression type;
    private TokenInfo modifier;

    public NewParameterExpression(TokenInfo n, TokenInfo m, TokenInfo p,
				 TokenInfo lp, Expression e,
				 TokenInfo c, Expression t,
				 TokenInfo rp) {
	newToken = n;
	parmToken = p;
	lparen = lp;
	parmNameExpr = e;
	comma = c;
	type = t;
	rparen = rp;
	modifier = m;
    }

    public String getOriginalCode() {
	return newToken.getOriginalCode() +
	    parmToken.getOriginalCode() +
	    lparen.getOriginalCode() +
	    parmNameExpr.getOriginalCode() +
	    comma.getOriginalCode() +
	    type.getOriginalCode() +
	    rparen.getOriginalCode();
    }

    public String toString() {
	return newToken.toString() + " " +
	    parmToken.toString() +
	    lparen.toString() +
	    parmNameExpr.toString() +
	    comma.toString() + " " +
	    type.toString() +
	    rparen.toString();
    }

    public int getLine() {
	return newToken.getLine();
    }

    public String getFileName() {
	return newToken.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Value v = parmNameExpr.evaluate(c);
	if(!TypeRelations.isSubtype(v.getType(), Types.stringType))
	    v = TypeRelations.attemptDeref(v,this);

	if(!TypeRelations.isSubtype(v.getType(), Types.stringType))
	    throw new UndefinedSymbolException(this, "parameter name in new" + 
					       " expression " + 
					       "must have type string not " + 
					       v.getType());
	String name = ((StringValue)v).getFlattenedValue();
	return ParameterDeclaration.newParameter(c, this, name,
						 type, modifier);
    }
}
