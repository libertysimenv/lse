package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class AssignmentLogEntry {
    private UnknownValue var;
    private Value val;
    private CodeObtainable statement;

    public AssignmentLogEntry(UnknownValue l, Value r) {
	var = l;
	val = r;
	statement = null;
    }

    public AssignmentLogEntry(UnknownValue l, Value r,
		       CodeObtainable o) {
	this(l,r);
	statement = o;
    }

    public UnknownValue getVariable() {
	return var;
    }

    public Value getValue() {
	return val;
    }

    public CodeObtainable getAssignment() {
	return statement;
    }

    public String toString() {
	String code = statement == null ? "" : statement.toString();
	return var.toString() + " = " + val.toString() + " (" + code + ")";
    }
}

