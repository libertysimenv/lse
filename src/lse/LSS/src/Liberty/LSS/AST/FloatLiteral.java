package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class FloatLiteral extends Literal {
    private double value;

    public FloatLiteral(double v, TokenInfo ti) {
	super(ti);
	value = v;
    }

    public FloatLiteral(Double v, TokenInfo ti) {
	this(v.doubleValue(), ti);
    }

    public FloatLiteral(Float v, TokenInfo ti) {
	this(v.floatValue(), ti);
    }

    public double getValue() {
	return value;
    }

    public String toString() {
	return Double.toString(value);
    }

    public Value evaluate(ExecutionContext c) {
	return new FloatValue(this.getValue());
    }
}
