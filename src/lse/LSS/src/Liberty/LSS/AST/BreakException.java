package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class BreakException extends ControlTransferException {
    private BreakStatement breakStatement;

    public BreakException(BreakStatement s) {
	breakStatement = s;
    }

    public String getMessage() {
	return "Error: break statement on line " + breakStatement.getLine() +
	    " of " + breakStatement.getFileName() + " is not inside a loop";
    }
}
