package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class NewRuntimeParmExpression extends ExpressionBase {
    private TokenInfo newToken, runtimeParmToken;
    private TokenInfo lparen, rparen, comma1, comma2, comma3;
    private Expression parmType;
    private Expression parmDefaultValue;
    private Expression parmCmdLineName;
    private Expression parmCmdLineDesc;

    public NewRuntimeParmExpression(TokenInfo n, TokenInfo r,
				    TokenInfo lp, Expression t,
				    TokenInfo c1, Expression v,
				    TokenInfo c2, Expression nm,
				    TokenInfo c3, Expression d,
				    TokenInfo rp) {
	newToken = n;
	runtimeParmToken = r;
	lparen = lp;
	parmType = t;
	parmDefaultValue = v;
	parmCmdLineName = nm;
	parmCmdLineDesc = d;
	comma1 = c1;
	comma2 = c2;
	comma3 = c3;
	rparen = rp;
    }

    public String getOriginalCode() {
	return newToken.getOriginalCode() +
	    runtimeParmToken.getOriginalCode() +
	    lparen.getOriginalCode() + 	    
	    parmType.getOriginalCode() +
	    comma1.getOriginalCode() +
	    parmDefaultValue.getOriginalCode() +
	    comma2.getOriginalCode() +
	    parmCmdLineName.getOriginalCode() +
	    comma3.getOriginalCode() +
	    parmCmdLineDesc.getOriginalCode() +
	    rparen.getOriginalCode();
    }

    public String toString() {
	return newToken.toString() + " " +
	    runtimeParmToken.toString() +
	    lparen.toString() + 	    
	    parmType.toString() +
	    comma1.toString() + " " +
	    parmDefaultValue.toString() +
	    comma2.toString() + " " +
	    parmCmdLineName.toString() +
	    comma3.toString() + " " +
	    parmCmdLineDesc.toString() +
	    rparen.toString();
    }

    public int getLine() {
	return newToken.getLine();
    }

    public String getFileName() {
	return newToken.getFileName();
    }


    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	String cmdName, cmdDesc;
	Type type;

	type = TypeSpecification.getType(c, parmType);

	Value defaultValue = parmDefaultValue.evaluate(c);

	Value cmdNameValue = parmCmdLineName.evaluate(c);
	Value cmdDescValue = parmCmdLineDesc.evaluate(c);
	try {
	    cmdNameValue = TypeRelations.attemptDeref(cmdNameValue);
	    cmdDescValue = TypeRelations.attemptDeref(cmdDescValue);
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(this, e.getMessage());
	}

	if(TypeRelations.isSubtype(cmdNameValue.getType(), Types.stringType)) {
	    StringValue s = (StringValue)cmdNameValue;
	    cmdName = s.getFlattenedValue();
	} else {
	    throw new TypeException(this, "Command line name" + 
				    " in new expression" +
				    " must have type " +
				    Types.stringType);
	}

	if(TypeRelations.isSubtype(cmdDescValue.getType(), Types.stringType)) {
	    StringValue s = (StringValue)cmdDescValue;
	    cmdDesc = s.getFlattenedValue();
	} else {
	    throw new TypeException(this, "Command line description" + 
				    " in new expression" +
				    " must have type " +
				    Types.stringType);
	}

	try {
	    if(!type.isPolymorphic())
		type.getBackendType();
	} catch(RuntimeTypeException e) {
	    throw new TypeException("Type "  + type + 
				    " cannot be used" +
				    " on a runtime parameter");
	}

	defaultValue = Coerce.assignCoerce(defaultValue, type, this);
	return new RuntimeParmValue(true, new RuntimeParmType(type),
				    defaultValue, cmdName, cmdDesc);
    }
}
