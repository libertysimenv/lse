package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class NewRuntimeVarExpression extends ExpressionBase {
    private TokenInfo newToken, runtimeVarToken;
    private TokenInfo lparen, rparen, comma;
    private Expression varNameExpr;
    private Expression varTypeExpr;

    public NewRuntimeVarExpression(TokenInfo n, TokenInfo rv,
				   TokenInfo lp, Expression e,
				   TokenInfo c, Expression t,
				   TokenInfo rp) {
	newToken = n;
	runtimeVarToken = rv;
	lparen = lp;
	varNameExpr = e;
	comma = c;
	varTypeExpr = t;
	rparen = rp;
    }

    public String getOriginalCode() {
	return newToken.getOriginalCode() +
	    runtimeVarToken.getOriginalCode() +
	    lparen.getOriginalCode() + 	    
	    varNameExpr.getOriginalCode() +
	    comma.getOriginalCode() +
	    varTypeExpr.getOriginalCode() +
	    rparen.getOriginalCode();
    }

    public String toString() {
	return newToken.toString() + " " +
	    runtimeVarToken.toString() +
	    lparen.toString() +
	    varNameExpr.toString() +
	    comma.toString() + " " +
	    varTypeExpr.toString() +
	    rparen.toString();
    }

    public int getLine() {
	return newToken.getLine();
    }

    public String getFileName() {
	return newToken.getFileName();
    }


    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	String name;
	Type type;
	Value nameValue = varNameExpr.evaluate(c);
	try {
	    nameValue = TypeRelations.attemptDeref(nameValue);
	} catch(UninitializedVariableException e) {
	    throw new UninitializedVariableException(this, e.getMessage());
	}
	type = TypeSpecification.getType(c, varTypeExpr);

	if(TypeRelations.isSubtype(nameValue.getType(), Types.stringType)) {
	    StringValue s = (StringValue)nameValue;
	    name = s.getFlattenedValue();
	} else {
	    throw new TypeException(this, "Variable name" + 
				    " in new expression" +
				    " must have type " +
				    Types.stringType);
	}

	try {
	    if(!type.isPolymorphic())
		type.getBackendType();
	} catch(RuntimeTypeException e) {
	    throw new TypeException("Type "  + type + 
				    " cannot be used" +
				    " on a runtime variable");
	}

	String uniqueName = c.instanceSpace.getUniqueVarName();
	RuntimeVarRefValue rvrv = new RuntimeVarRefValue(name, uniqueName,
							 type);
	c.instanceSpace.addRuntimeVar(rvrv);
	return rvrv;
    }
}
