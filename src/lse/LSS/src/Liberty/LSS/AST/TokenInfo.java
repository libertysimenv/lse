package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class TokenInfo implements CodeObtainable {
    private String fileName;
    private String text;
    private String precedingWhiteSpace;
    private int line;
    private int charBegin;
    private int charEnd;
    
    public TokenInfo(String fn, String pws, String text, 
		     int line, int cb, int ce)
    {
	this.fileName = fn;
	this.precedingWhiteSpace = pws;
	this.text = text;
	this.line = line;
	this.charBegin = cb;
	this.charEnd = ce;
    }

    public String getFileName() { return fileName; }
    public int getLine() { return line; }
    public int getCharBegin() { return charBegin; }
    public int getCharEnd() { return charEnd; }
    public String getPrecedingWhiteSpace() { return precedingWhiteSpace; }
    public String getText() { return text; }
    public String getOriginalCode() { return precedingWhiteSpace + text; }
    public String toString() {return text; }
}

