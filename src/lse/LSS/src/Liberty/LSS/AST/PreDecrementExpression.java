package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class PreDecrementExpression extends UnaryOpExpression
{
    public PreDecrementExpression(TokenInfo o, Expression e) {
	super(o,e);
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	try {
	    LValue lv = getExpression().getLValue(c);
	    Value v = Operate.decrement(lv.getValue());
	    lv.assign(v);
	    return v;
	} catch(TypeException e) {
	    throw new TypeException("Error on line " + this.getLine() + 
				    " of " + this.getFileName() + 
				    ": " + e.getMessage());
	}
    }
}
