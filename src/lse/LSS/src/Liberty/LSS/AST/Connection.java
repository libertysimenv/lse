package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class Connection {
    private Value from,to;
    private Type constraint;
    private CodeObtainable statement;
    
    Connection(Value l, Value r,
	       Type c,
	       CodeObtainable o) {
	from = l;
	to = r;
	constraint = c;
	statement = o;
    }
    
    public String toString() {
	String code =  statement.toString();
	return from.toString() + " -> " + to.toString() + " (" + code + ")";
    }

    public Value getFrom() {
	return from;
    }

    public Value getTo() {
	return to;
    }

    public Type getConstraint() {
	return constraint;
    }

    public CodeObtainable getCode() {
	return statement;
    }
}
