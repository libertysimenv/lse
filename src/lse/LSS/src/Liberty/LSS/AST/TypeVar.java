package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class TypeVar extends IdBase {
    public static final int TYPE = 1;
    public static final int NUMERIC = 2;
    public static final int PORT = 3;

    private int type;

    public TypeVar(String v, int type, TokenInfo ti) {
	super(v,ti);
	this.type = type;
    }

    public int getType() {
	return type;
    }

    public String getIdentifier() {
	return super.toString();
    }
    
    public String toString() {
	if(type == TYPE)
	    return "'" + super.toString();
	else if(type == NUMERIC)
	    return "#" + super.toString();
	else 
	    return "''" + super.toString();
    }
}
