package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class UnimplementedException extends LSSException
{
    public UnimplementedException(String message) {
	super(message);
    }

    public UnimplementedException() {
	super();
    }
}
