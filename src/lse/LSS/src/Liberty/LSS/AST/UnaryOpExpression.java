package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public abstract class UnaryOpExpression extends ExpressionBase
{
    protected TokenInfo operator;
    protected Expression expr;

    public UnaryOpExpression(TokenInfo o, Expression e) {
	operator = o;
	expr = e;
    }

    public String getOriginalCode() {
	return operator.getOriginalCode() + expr.getOriginalCode();
    }

    public String toString() {
	return operator.toString() + expr.toString();
    }

    public Expression getExpression() {
	return expr;
    }

    public int getLine() {
	return operator.getLine();
    }
    
    public String getFileName() {
	return operator.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	throw new UnimplementedException("Expression type not yet implemented");
    }
}
