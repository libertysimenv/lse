package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;

import Liberty.LSS.types.inference.*;

public class IsRuntimedFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType myType;

    static {
	Vector v = new Vector();
	v.add(Types.parameterRefType);
	
	myType = new FunctionType(v, Types.boolType);
    }

    public IsRuntimedFunction(ExecutionContext c) {
	super(myType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, final CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	FunctionType ft = ((FunctionType)getType());
	
	if(argValues.size() != 1)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");

	// ensure that the parameter has had a definition before we check it
        try {
            TypeRelations.attemptDeref((Value)argValues.elementAt(0));
        } catch(UninitializedVariableException e) {
            throw new FunctionException(e.getMessage());
        }
	
	int i = 0;
        Parameter parameter;
	try {
	    parameter = (Parameter)argValues.elementAt(i++);
	} catch(ClassCastException e2) {
	    throw new FunctionException("Argument " + 
					Integer.toString(i) + " in call to " +
					"function with type " + 
					getType() + " must have type " +
					ft.getArgumentType(i-1));
	} 

	// Now, can we cast it to a runtime value?
	try {
	    Value v = parameter.deref();
	    RuntimeParmValue rv = (RuntimeParmValue)v;
	    return new BoolValue(rv.getRuntimed());
        } catch(UninitializedVariableException e3) { // should not happen
	} catch(ClassCastException e4) {
	    return new BoolValue(false);
	}
	throw new FunctionException("Strange code behavior in is_runtimed");
    }
}
