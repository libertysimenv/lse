package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class TypedefDeclaration extends Statement {
    private TokenInfo typedefToken;
    private TokenInfo colon, semi;
    private Expression typeSpec;
    private Identifier typeName;

    public TypedefDeclaration(TokenInfo td, Identifier i, TokenInfo c, 
			      Expression t, TokenInfo s) {
	typedefToken = td;
	colon = c;
	semi = s;
	typeSpec = t;
	typeName = i;
    }

    public String getOriginalCode() {
	return typedefToken.getOriginalCode() + typeName.getOriginalCode() + 
	    colon.getOriginalCode() + typeSpec.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	return typedefToken.toString() + " " + typeName.toString() + 
	    " " + colon.toString() + " " + typeSpec.toString() +
	    semi.toString();
    }

    public int getLine() {
	return typedefToken.getLine();
    }

    public String getFileName() {
	return typedefToken.getFileName();
    }

    public void execute(ExecutionContext cp) 
	throws SymbolAlreadyDefinedException,
	       TypeException,
	       UninitializedVariableException {
	try {
	    Type type = TypeSpecification.getType(cp, typeSpec);
	    cp.addType(typeName.toString(), type);
	} catch(SymbolAlreadyDefinedException e) {
	    throw new SymbolAlreadyDefinedException(typeName, e.getMessage());
	}
    }
}
