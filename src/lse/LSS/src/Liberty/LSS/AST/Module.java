package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.util.*;
import java.util.*;
import java.io.FileNotFoundException;

import Liberty.LSS.IMR.Event;
import Liberty.LSS.IMR.StructAdd;
import Liberty.LSS.IMR.Query;
import Liberty.LSS.IMR.Instance;
import Liberty.LSS.IMR.RuntimeVar;

import Liberty.LSS.IMR.PortDirectionException;
import Liberty.LSS.IMR.PortIndexException;

import Liberty.LSS.IMR.DomainSearchPathEntry;

public class Module implements Executable
{
    private String name;
    private ModuleDeclaration moduleCode;
    private CompoundStatement body;
    private ExecutionContext context;

    public Module(String n, ModuleDeclaration m, ExecutionContext c) {
	name = n;
	moduleCode = m;
	body = m.getBody();
	context = c.checkpoint();
    }

    public String getName() {
	return name;
    }

    public String toString() {
	return "module " + name;
    }

    public void execute(ExecutionContext cp) 
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	InstanceEnvironment myEnv =
	    context.instanceSpace.getCurrentEnvironment();
	if(myEnv.getFullyQualifiedName() != null)
	    System.err.println("Processing Instance " + 
			       myEnv.getFullyQualifiedName());

	/* Execute the body */
	/* Ignore the passed in context.  We want to used the saved one */
	ExecutionContext c = (ExecutionContext)context.enterScope();
	try {
	    Parameter p = myEnv.getParameter("LSE_domain");
	    c.addVariable("this", new ConstType(Types.instanceRefType), myEnv);
	    c.addVariable("LSE_domain", new ConstType(Types.parameterRefType), 
			  p);
	} catch(SymbolAlreadyDefinedException e) {
	    throw new RuntimeTypeException(e.getMessage());
	} catch(UndefinedSymbolException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
	bootstrapInternalAttributes(c);
	body.execute(c);

	/* Make sure the assignments table is empty */
	if(!myEnv.assignmentLogIsEmpty()) {
	    Iterator i = myEnv.getAssignmentLog().iterator();
	    String msg = "";
	    while(i.hasNext()) {
		AssignmentLogEntry e = (AssignmentLogEntry)i.next();
		i.remove();
		if(e.getAssignment() != null) {
		    msg += "Error on line " + e.getAssignment().getLine() +
			" in " + e.getAssignment().getFileName() + 
			": Attribute " +
			e.getVariable() + " on instance " + 
			myEnv.getFullyQualifiedName() + " is not defined\n";
		} else {
		    /* Why no line number??? */
		    throw new RuntimeTypeException("Entry " + e.getVariable() +
					       " = " + e.getValue() + " was " +
					       "not removed from the " +
					       "assignment log and has no " +
					       "line number info");
		}
	    }

	    if(!msg.equals(""))
		throw new TypeException(msg);
	}

	/* Begin building instances */
	Liberty.LSS.IMR.Instance inst = buildInstance(c, myEnv);

	/* Calculate child port widths */
	calculateImplicitPortValues(myEnv);

	/* Process child instances */
	Liberty.util.Queue pendingInstances = myEnv.getPendingInstances();
	if(inst.isLeaf()) {
	    if(!pendingInstances.empty()) {
		String msg = "Error on line " + body.getLine() + 
		    " of " + body.getFileName() + ": Leaf instance " +
		    myEnv.getFullyQualifiedName() + 
		    "cannot have sub-instances";
		throw new TypeException(msg);
	    }
	} else {
	    while(!pendingInstances.empty()) {
		InstanceEnvironment e = 
		    (InstanceEnvironment)pendingInstances.dequeue();
		e.setProcessedFlag();
		context.instanceSpace.setCurrentEnvironment(e);
		propagateDomainParameter(cp,myEnv);
		e.getModule().execute(context);
		inst.addSubInstance(e.getInstance());
	    }
	}

	/* Copy parameters into instance */
	copyParameters(inst, myEnv);
	copyFuncHeader(inst, myEnv);
	copyModuleBody(inst, myEnv);
	copyExtension(inst, myEnv);
	copyQueries(inst,myEnv);
	copyStructAdds(inst,myEnv);
	copyPorts(inst,myEnv);
	copyEvents(inst,myEnv);
	copyExportedTypes(inst,myEnv);
	copyCollectors(inst.getSubInstanceMap(),c, myEnv);
	copyDomainSearchPath(inst, myEnv);
    }

    public static void propagateDomainParameter(ExecutionContext cp,
						InstanceEnvironment fromEnv) {
	/* Domain parameters */
	Set dc = cp.instanceSpace.getDomainClasses();
	Type dimt = new DomainInstanceMapType(dc);
	InstanceEnvironment env = cp.instanceSpace.getCurrentEnvironment();

	try { 
	    /* We can't use addAttribute here because we don't want to
	     * add the variable to the current execution context.
	     * Instead, we will add the parameter to the instance and
	     * then let the instance add the variable to its execution
	     * context. */
	    Parameter p = env.addParameter("LSE_domain", dimt, true, false);
	    Value parentValue = 
		fromEnv.getParameter("LSE_domain").deref();
	    p.getLValue().assign(parentValue);
	} catch(UninitializedVariableException e) {
	    /* Oh well, no big loss...*/
	} catch(UndefinedSymbolException e) { 
	    /* Bad things are afoot */
	    throw new RuntimeTypeException(e.getMessage());
	} catch(SymbolAlreadyDefinedException e) { 
	    /* Bad things are afoot */
	    throw new RuntimeTypeException(e.getMessage());
	} catch(TypeException e) {
	    /* Bad things are afoot */
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    /********************************************/
    /* Functions to add some default attributes */
    /********************************************/
    private static void addAttribute(String name, Type t, 
			      ExecutionContext cp,
			      boolean external,
			      boolean export)
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       TypeException,
	       UninitializedVariableException {
	InstanceEnvironment e = cp.instanceSpace.getCurrentEnvironment();
	Parameter p = e.addParameter(name, t, external, export);
	cp.addVariable(name, new ConstType(Types.parameterRefType), p);
    }

    private void bootstrapInternalAttributes(ExecutionContext cp)
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException,
	       TypeException,
	       UninitializedVariableException {
	addAttribute("tar_file", Types.stringType, cp, false, true);
	addAttribute("phase_start", Types.boolType, cp, false, true);
	addAttribute("phase", Types.boolType, cp, false, true);
	addAttribute("phase_end", Types.boolType, cp, false, true);
	addAttribute("reactive", Types.boolType, cp, false, true);
	addAttribute("port_dataflow", Types.stringType, cp, true, true);

	Type t1 = new UserPointType(new StringValue("void"),
				    new StringValue("void"));
	Type t2 = new UserPointType(new StringValue("void"),
				    new StringValue("void"));
	addAttribute("funcheader", Types.stringType, cp, true, true);
	addAttribute("modulebody", Types.stringType, cp, true, true);
	addAttribute("extension", Types.stringType, cp, true, true);
	addAttribute("start_of_timestep", t1, cp, true, true);
	addAttribute("end_of_timestep", t2, cp, true, true);
	addAttribute("init", t2, cp, true, true);
	addAttribute("finish", t2, cp, true, true);
   
	/* Visualizer parameters */
	addAttribute("lvlString", Types.stringType, cp, true, false);

	/* Set some default values */
	InstanceEnvironment e = cp.instanceSpace.getCurrentEnvironment();
	try {
	    Parameter p;

	    Value empty = new StringValue("");
	    p = e.getParameter("lvlString");
	    p.getLValue().assign(empty);

	    Value fullDeps = new StringValue("[ ('*','*', '1') ]");
	    p = e.getParameter("port_dataflow");
	    p.getLValue().assign(fullDeps);

	    p = e.getParameter("funcheader");
	    p.getLValue().assign(empty);

	    p = e.getParameter("modulebody");
	    p.getLValue().assign(empty);

	    p = e.getParameter("extension");
	    p.getLValue().assign(empty);

	    p = e.getParameter("init");
	    p.getLValue().assign(empty);

	    p = e.getParameter("finish");
	    p.getLValue().assign(empty);

	    p = e.getParameter("start_of_timestep");
	    p.getLValue().assign(empty);

	    p = e.getParameter("end_of_timestep");
	    p.getLValue().assign(empty);
	} catch(TypeException ex) {
	    /* Umm... */
	    throw new RuntimeTypeException(ex.getMessage());
	}

	/* And add LSE_domain.LSE_clock to the domain search path */

	try { 
	    /* We can't use addAttribute here because we don't want to
	     * add the variable to the current execution context.
	     * Instead, we will add the parameter to the instance and
	     * then let the instance add the variable to its execution
	     * context. */
	    Aggregate dVal = (Aggregate)e.getParameter("LSE_domain").deref();
	    DomainInstanceRefValue dInst;

	    try {
		Value v = dVal.getSubValue("LSE_clock");
		dInst = (DomainInstanceRefValue)v;
	    } catch (TypeException exc) {
		/* Oh well, couldn't find the parameter... */
		throw new UninitializedVariableException("");
	    }

	    DomainSearchPathEntry spe = new DomainSearchPathEntry("LSE_clock",
								  dInst);
	    e.addToDomainSearchPath(spe);

	} catch(UninitializedVariableException exc) {
	    /* Oh well, no big loss...*/
	} catch(UndefinedSymbolException exc) { 
	    /* Bad things are afoot */
	    throw new RuntimeTypeException(exc.getMessage());
	}
 
    }

    /******************************/
    /* Function to build instance */
    /******************************/
    private Liberty.LSS.IMR.Instance buildInstance(ExecutionContext context,
						   InstanceEnvironment myEnv) 
	throws UninitializedVariableException,
	       TypeException {
	Liberty.LSS.IMR.Instance parent = myEnv.getParent().getInstance();
	Liberty.LSS.IMR.Instance inst;

	String tarball = getTarball(context, myEnv);
	if(tarball == null) {	    
	    inst = new 
		Liberty.LSS.IMR.Instance(parent, myEnv.getName(),
					 getName(), moduleCode,
					 myEnv.getInstantiationLocation(),
					 myEnv.getConstraints());
	} else {
	    boolean phase = getBoolean(myEnv, "phase");
	    boolean phaseStart = getBoolean(myEnv, "phase_start");
	    boolean phaseEnd = getBoolean(myEnv, "phase_end");
	    boolean reactive = getBoolean(myEnv, "reactive");
	    String portDataFlow = getString(myEnv, "port_dataflow");
	    boolean strict = false;
	    inst = new 
		Liberty.LSS.IMR.Instance(parent, myEnv.getName(),
					 getName(),  moduleCode,
					 myEnv.getInstantiationLocation(),
					 tarball, phaseStart, phase,
					 phaseEnd, reactive, strict, 
					 portDataFlow,
					 myEnv.getConstraints());
	}
	myEnv.setInstance(inst);
	return inst;
    }


    /*****************************************/
    /* Functions to set implicit port values */
    /*****************************************/
    static class ImplicitPortData {
	public int width;
	public boolean connected;
	private Vector instConnected;
	
	public ImplicitPortData() {
	    width = 0;
	    connected = false;
	    instConnected = new Vector();
	}

	public void setInstConnected(int inst) {
	    if(instConnected.size() < inst + 1) {
		instConnected.setSize(inst+1);
	    }
	    instConnected.set(inst, new Boolean(true));
	}

	public boolean isInstConnected(int inst) {
	    Boolean b = (Boolean)instConnected.get(inst);
	    if(b == null)
		return false;
	    else 
		return b.booleanValue();
	}

	public int getInstConnectedWidth() {
	    return instConnected.size();
	}
    }

    public static void 	calculateImplicitPortValues(InstanceEnvironment e) {
	Liberty.util.Queue connectionLog = new Liberty.util.Queue(e.getConnectionLog());
	HashMap portDataMap = new HashMap();
	
	/* Calculate all port's actual widths */
	while(!connectionLog.empty()) {
	    Connection conn = (Connection)connectionLog.dequeue();
	    CodeObtainable connCode = conn.getCode();

	    updateImplicitPortValues(conn.getFrom(), e, connCode, portDataMap);
	    updateImplicitPortValues(conn.getTo(), e, connCode, portDataMap);
	}

	/* Set width to -1 for entries in the map which already have
	 * explicit width's set */
	Liberty.util.Queue pendingInstances = new Liberty.util.Queue(e.getPendingInstances());
	while(!pendingInstances.empty()) {
	    InstanceEnvironment child = 
		(InstanceEnvironment)pendingInstances.dequeue();
	    Iterator assignments = child.getAssignmentLog().iterator();
	    while(assignments.hasNext()) {
		AssignmentLogEntry assignment = 
		    (AssignmentLogEntry)assignments.next();
		deletePortWidth(child, assignment, portDataMap);
	    }
	}

	/* Add entries to assignment logs for the implicit width
	   calcuations */
	Iterator instances = portDataMap.entrySet().iterator();
	while(instances.hasNext()) {
	    Map.Entry childEntry = (Map.Entry)instances.next();
	    InstanceEnvironment child = 
		(InstanceEnvironment)childEntry.getKey();
	    Iterator ports = 
		((Map)childEntry.getValue()).entrySet().iterator();
	    while(ports.hasNext()) {
		Map.Entry portInfoEntry = (Map.Entry)ports.next();
		ImplicitPortData portInfo;
		portInfo = (ImplicitPortData)portInfoEntry.getValue();

		Vector baseName = new Vector();
		baseName.add(child.getName());
		baseName.add(portInfoEntry.getKey());

		Vector name;
		UnknownValue uv;
		Vector assignments = new Vector();

		name = new Vector(baseName);
		name.add("width");
		uv = new UnknownValue(child, name);
		NumericIntValue iv = new NumericIntValue(portInfo.width);
		assignments.add(new AssignmentLogEntry(uv, iv));

		name = new Vector(baseName);
		name.add("connected");
		uv = new UnknownValue(child, name);
		BoolValue bv = new BoolValue(portInfo.connected);
		assignments.add(new AssignmentLogEntry(uv, bv));

		for(int i = 0; i < portInfo.getInstConnectedWidth(); i++) {
		    name = new Vector(baseName);
		    name.add(new Integer(i));
		    name.add("connected");
		    uv = new UnknownValue(child,name);
		    bv = new BoolValue(portInfo.isInstConnected(i));
		    assignments.add(new AssignmentLogEntry(uv, bv));
		}

		Iterator assignmentIterator = assignments.iterator();
		while(assignmentIterator.hasNext()) {
		    AssignmentLogEntry entry;
		    entry = (AssignmentLogEntry)assignmentIterator.next();
		    try {
			child.addImplicitAssignment(entry);
		    } catch(TypeException ex) {
			/* shouldn't happen */
			throw new RuntimeTypeException(ex.getMessage());
		    }
		}
	    }
	}
    }

    private static void deletePortWidth(InstanceEnvironment e,
					AssignmentLogEntry assignment,
					Map portDataMap) {
	UnknownValue lvalue = assignment.getVariable();
	Vector names = lvalue.getNames();

	/* Is assignment a port width assignment? */
	if(names.size() == 3 &&
	   names.elementAt(2).equals("width")) {
	    Map m = (Map)portDataMap.get(e);
	    if(m != null) {
		ImplicitPortData entry;
		Object portName = names.elementAt(1);
		entry = (ImplicitPortData)m.get(portName);
		if(entry != null) {
		    entry.width = -1;
		}
	    }
	}
    }

    private static void updateImplicitPortValues(Value vPort, 
						 InstanceEnvironment e, 
						 CodeObtainable connCode, 
						 Map portDataMap) {
	try {
	    if(vPort instanceof UnknownValue) {
		PortInfo port = getPortInfo((UnknownValue)vPort, e, connCode);
		Map m = (Map)portDataMap.get(port.instance);
		if(m == null) {
		    m = new HashMap();
		    portDataMap.put(port.instance, m);
		}
		
		ImplicitPortData entry;
		entry = (ImplicitPortData)m.get(port.portName);
		if(entry == null) {
		    entry = new ImplicitPortData();
		    m.put(port.portName, entry);
		}

		/* Calculate the port index and new width */
		int index = port.porti;
		int width = entry.width;
		if(port.porti == -1) {
		    index = entry.width;
		    width = entry.width+1;
		} else if(port.porti >= entry.width) {
		    width = port.porti + 1;
		}
		
		/* Update the entry */
		entry.width = width;
		entry.connected = true;
		entry.setInstConnected(index);
	    }
	} catch(TypeException ex) {
	    /* Ignore any excepting port */
	}
    }

    /********************************/
    /* Functions to read some parms */
    /********************************/
    private boolean getBoolean(InstanceEnvironment e, String field)
	throws UninitializedVariableException {
	LockableAttributeCollection c = e.getParameterValues();
	Value v;

	try {
	    v = c.getVariableValue(field);
	} catch(UndefinedSymbolException ex) {
	    v = null;
	}

	if(v == null)
	    throw new UninitializedVariableException("Error on line " + 
						     body.getLine() + 
						     " of " +
						     body.getFileName() +
						     ": " + field + 
						     " not set for instance " +
						     e.getFullyQualifiedName());
	else if(TypeRelations.isSubtype(v.getType(), Types.boolType))
	    return ((BoolValue)v).getValue();
	else 
	    throw new RuntimeTypeException("Error on line " + 
					   body.getLine() + 
					   " of " + body.getFileName() +
					   ": " + field +
					   " does not have type " +
					   "bool for instance " + 
					   e.getFullyQualifiedName());
    }

    private String getString(InstanceEnvironment e, String field)
	throws UninitializedVariableException {
	LockableAttributeCollection c = e.getParameterValues();
	Value v;

	try {
	    v = c.getVariableValue(field);
	} catch(UndefinedSymbolException ex) {
	    v = null;
	}

	if(v == null)
	    throw new UninitializedVariableException("Error on line " + 
						     body.getLine() + 
						     " of " +
						     body.getFileName() +
						     ": " + field + 
						     " not set for instance " +
						     e.getFullyQualifiedName());
	else if(TypeRelations.isSubtype(v.getType(), Types.stringType))
	    return ((StringValue)v).getFlattenedValue();
	else 
	    throw new RuntimeTypeException("Error on line " + 
					   body.getLine() + 
					   " of " + body.getFileName() +
					   ": " + field +
					   " does not have type " +
					   "string for instance " + 
					   e.getFullyQualifiedName());
    }

    private String getTarball(ExecutionContext context, 
			      InstanceEnvironment e) 
	throws TypeException {
	LockableAttributeCollection c = e.getParameterValues();
	Value v;

	try {
	    v = c.getVariableValue("tar_file");
	} catch(UndefinedSymbolException ex) {
	    return null;
	}

	if(v == null)
	    return null;
	else if(v instanceof StringValue) {
	    String vstr = ((StringValue)v).getFlattenedValue();

	    try {
		/* Is this a script ? */
		if(vstr.charAt(0) == '-') {
		    int index = vstr.indexOf(" ");
		    String scriptName, scriptArgs;
		    scriptName = (index == -1) ? vstr.substring(1) : 
			vstr.substring(1,index);
		    scriptArgs = (index == -1) ? "" : 
			vstr.substring(index);
		    scriptName = context.getFileName(scriptName);
		    return "-" + scriptName + scriptArgs;
		}
	    } catch(FileNotFoundException ex) {
		throw new TypeException(body, "tar_file " + 
					((StringValue)v).getValue() +
					" cannot be found for instance " + 
					e.getFullyQualifiedName());
	    }

	    /* need to somehow split string into list */
	    int lastIndex = 0;
	    int thisIndex;
	    int useIndex;

	    String createdName = new String("");

	    while (true) {
		thisIndex = vstr.indexOf(' ',lastIndex);
		if (thisIndex >= 0 && thisIndex == lastIndex) {
		    /* handle multiple spaces */
		    lastIndex = thisIndex+1;
		    continue;
		}
		if (thisIndex < 0) { /* handle last time */
		    useIndex = vstr.length();
		} else {
		    useIndex = thisIndex;
		}
		
		String fpiece = vstr.substring(lastIndex,useIndex);
		String tname;

		try {
		    tname = context.getFileName(fpiece + ".tar");
		} catch(FileNotFoundException ex) {
		    /* Ignore this since we will try not appending .tar
		       and searching for the file */
		
		    try {
			tname = context.getFileName(fpiece);
		    } catch(FileNotFoundException ex2) {
			throw new TypeException(body, "tar_file " + 
						((StringValue)v).getValue() +
					  " cannot be found for instance " + 
						e.getFullyQualifiedName());
		    }
		}

		lastIndex = thisIndex;
		if (thisIndex < 0) { 
		    createdName = createdName.concat(tname);
		    break; 
		} else {
		    createdName = createdName.concat(tname + " ");
		}
	    } /* while (true) */

	    return createdName;

	} else 
	    throw new RuntimeTypeException(body, "tar_file " +
					   " does not have type " +
					   "string for instance " + 
					   e.getFullyQualifiedName());
    }

    private void copyDomainSearchPath(Liberty.LSS.IMR.Instance inst,
				      InstanceEnvironment e) {
	inst.setLocalDomainSearchPath(e.getLocalDomainSearchPath());
	inst.setHierDomainSearchPath(e.getHierDomainSearchPath());
    }

    private void copyFuncHeader(Liberty.LSS.IMR.Instance inst,
				InstanceEnvironment e) {
	LockableAttributeCollection c = e.getParameterValues();
	Value v;

	try {
	    v = c.getVariableValue("funcheader");
	} catch(UndefinedSymbolException ex) {
	    return;
	}

	if(v == null)
	    return;
	else if(v instanceof StringValue)
	    inst.setFuncHeader((StringValue)v);
	else 
	    throw new RuntimeTypeException("Error on line " + 
					   body.getLine() + 
					   " of " + body.getFileName() +
					   ": funcheader " +
					   " is not a code value " +
					   "for instance " + 
					   e.getFullyQualifiedName());
    }

    private void copyModuleBody(Liberty.LSS.IMR.Instance inst,
				InstanceEnvironment e) {
	LockableAttributeCollection c = e.getParameterValues();
	Value v;

	try {
	    v = c.getVariableValue("modulebody");
	} catch(UndefinedSymbolException ex) {
	    return;
	}

	if(v == null)
	    return;
	else if(v instanceof StringValue)
	    inst.setModuleBody((StringValue)v);
	else 
	    throw new RuntimeTypeException("Error on line " + 
					   body.getLine() + 
					   " of " + body.getFileName() +
					   ": modulebody " +
					   " is not a code value " +
					   "for instance " + 
					   e.getFullyQualifiedName());
    }

    private void copyExtension(Liberty.LSS.IMR.Instance inst,
				InstanceEnvironment e) {
	LockableAttributeCollection c = e.getParameterValues();
	Value v;

	try {
	    v = c.getVariableValue("extension");
	} catch(UndefinedSymbolException ex) {
	    return;
	}

	if(v == null)
	    return;
	else if(v instanceof StringValue)
	    inst.setExtension((StringValue)v);
	else 
	    throw new RuntimeTypeException("Error on line " + 
					   body.getLine() + 
					   " of " + body.getFileName() +
					   ": extension " +
					   " is not a code value " +
					   "for instance " + 
					   e.getFullyQualifiedName());
    }

    private void copyParameters(Liberty.LSS.IMR.Instance inst,
				InstanceEnvironment e)
	throws UninitializedVariableException,
	       TypeException {
	LockableAttributeCollection c = e.getParameterValues();
	Iterator i = c.getVariableSet().iterator();
	while(i.hasNext()) {
	    String var = (String)i.next();
	    /* Skip certain variables */
	    if(var.equals("tar_file") ||
	       var.equals("phase_start") ||
	       var.equals("phase") ||
	       var.equals("phase_end") ||
	       var.equals("reactive") ||
	       var.equals("funcheader") ||
	       var.equals("modulebody") ||
	       var.equals("extension") ||
	       var.equals("port_dataflow")) {
		continue;
	    }
	    	    
	    Value val, defVal;
	    Type t;
	    boolean exportFlag;
	    try {
		val = c.getVariableValue(var);
		defVal = c.getVariableDefaultValue(var);
		t = c.getVariableType(var);
		exportFlag = e.getParameter(var).getExportFlag();
	    } catch(UndefinedSymbolException ex) {
		/* This can't happen */
		throw new RuntimeTypeException(ex.getMessage());
	    }

	    while(!t.isBaseType())
		t = t.getBaseType();


	    if(val == null && 
	       (inst.isLeaf() || t instanceof UserPointType) && 
	       exportFlag) {
		String msg = "Error on line " + body.getLine() +
		    " of " + body.getFileName() + ": " + 
		    "Parameter " + var + " on instance " + 
		    inst.getFullyQualifiedName() + " must be set";
		throw new UninitializedVariableException(msg);
	    }

	    if(t instanceof UserPointType && exportFlag) {
		Liberty.LSS.IMR.CodePoint p = 
		    new Liberty.LSS.IMR.CodePoint(var, t, val, defVal, 
						  exportFlag);
		inst.addCodePoint(p);
	    } else {
		if(inst.isLeaf() && exportFlag) {
		    try {
			t.getBackendType();

			/* We can't handle these types on the backend */
 			if(t instanceof ArrayType ||
 			   t instanceof StructType)
 			    throw new RuntimeTypeException("Array or Struct");
		    } catch(RuntimeTypeException ex) {
			throw new TypeException(body, "Type " + t + 
						" cannot be used for an " +
						"exported parameter on a " +
						"leaf instance");
		    }
		}
		
		Liberty.LSS.IMR.Parameter p = 
		    new Liberty.LSS.IMR.Parameter(var, t, val, exportFlag);
		inst.addParameter(p);
	    }
	}
    }

    /********************************/
    /* Function to copy queries     */
    /********************************/
    private void copyQueries(Liberty.LSS.IMR.Instance inst,
			     InstanceEnvironment e) {
	Collection c = e.getQueries();
	Iterator i = c.iterator();
	
	while(i.hasNext()) {
	    inst.addQuery((Query)i.next());
	}
    }

    /********************************/
    /* Function to copy StructAdds  */
    /********************************/
    private void copyStructAdds(Liberty.LSS.IMR.Instance inst,
				InstanceEnvironment e) {
	Collection c = e.getStructAdds();
	Iterator i = c.iterator();
	
	while(i.hasNext()) {
	    inst.addStructAdd((StructAdd)i.next());
	}
    }

    /**************************/
    /* Function to copy Ports */
    /**************************/
    public static TypeException notAPort(Value v,
					 CodeObtainable code) {
	String msg = "Error on line " + code.getLine() +
	    " of " + code.getFileName() + ": " + 
	    v + " is not a port or port instance";
	return new TypeException(msg);
    }

    
    public static class PortInfo {
	public InstanceEnvironment instance;
	public String portName;
	public int porti;

	public PortInfo(InstanceEnvironment inst, String p, int i) {
	    instance = inst;
	    portName = p;
	    porti = i;
	}
    }

    private static PortInfo getPortInfo(UnknownValue portValue,
					InstanceEnvironment e,
					CodeObtainable code)
	throws TypeException {
	String portName;
	int porti;

	Vector v = ((UnknownValue)portValue).getNames();
	try {
	    if((v.size() == 2 || v.size() == 3) &&
	       v.elementAt(0) instanceof String &&
	       v.elementAt(1) instanceof String) {
		e = e.getInstance((String)v.elementAt(0));
		portName = (String)v.elementAt(1);
		if(v.size() == 3) {
		    if(v.elementAt(2) instanceof Integer) {
			porti = ((Integer)v.elementAt(2)).intValue();
			return new PortInfo(e,portName,porti);
		    } else
			throw notAPort(portValue, code);
		} else {
		    return new PortInfo(e,portName,-1);
		}
	    } else
		throw notAPort(portValue, code);
	} catch(UndefinedSymbolException ex) {
	    throw notAPort(portValue, code);
	}
    }

    public static PortInstance getPortInstance(Value portValue, 
					       InstanceEnvironment e,
					       CodeObtainable code) 
	throws TypeException {

	/* This function assumes the returned port instance is
	 * connected */

	if(portValue instanceof PortInstance)
	    return (PortInstance)portValue;
	else if(portValue instanceof Port)
	    return new PortInstance((Port)portValue, -1, true);
	else if(portValue instanceof UnknownValue) {
	    try { 
		PortInfo pi = getPortInfo((UnknownValue)portValue, e,
					  code);
		Port p = pi.instance.getPort(pi.portName);
		return new PortInstance(p, pi.porti, true);
	    } catch(UndefinedSymbolException ex) {
		throw notAPort(portValue, code);
	    }
	} else {
	    throw notAPort(portValue, code);
	}
    }

    public static void copyConnections(Liberty.LSS.IMR.Instance inst,
				       InstanceEnvironment e,
				       HashMap typeVars) 
	throws TypeException {
	Liberty.util.Queue connectionLog = e.getConnectionLog();
	
	while(!connectionLog.empty()) {
	    Connection conn = (Connection)connectionLog.dequeue();
	    Type constraint = conn.getConstraint();
	    
	    Value vFrom = conn.getFrom();
	    Value vTo = conn.getTo();
	    CodeObtainable connCode = conn.getCode();
	    
	    PortInstance from = getPortInstance(vFrom, e, connCode);
	    PortInstance to = getPortInstance(vTo, e, connCode);

	    /* Are these connections internal */
	    boolean fInternal = 
		(from.getPort().getIMRPort().getInstance() == inst);
	    boolean tInternal = 
		(to.getPort().getIMRPort().getInstance() == inst);

	    try {
		Liberty.LSS.IMR.Connection.connect(from.getPort().getIMRPort(),
						   from.getIndex(), fInternal,
						   to.getPort().getIMRPort(),
						   to.getIndex(), tInternal,
						   constraint);
	    } catch(PortIndexException ex) {
		throw new TypeException("Error on line " + connCode.getLine() +
					" of " + connCode.getFileName() + 
					": " + ex.getMessage());
	    } catch(PortDirectionException ex) {
		throw new TypeException("Error on line " + connCode.getLine() +
					" of " + connCode.getFileName() + 
					": " + ex.getMessage());
	    }
	}
    }

    private void copyPorts(Liberty.LSS.IMR.Instance inst,
			   InstanceEnvironment e) 
	throws TypeException {
	Collection c = e.getPorts();
	Iterator i = c.iterator();

	HashMap typeVars = new HashMap();

	while(i.hasNext()) {
	    Port p = (Port)i.next();

	    /* Get allowable types keeping track of type variables */
	    Type type = p.getPortType();

	    /* Get attributes table */
	    LockableAttributeCollection attrs = p.getAttributes();

	    ControlPointValue ctrl, defCtrl;
	    BoolValue handlerValue, indepValue;
	    NumericIntValue widthValue;

	    try {
		ctrl = (ControlPointValue)attrs.getVariableValue("control");
		defCtrl = (ControlPointValue)attrs.getVariableDefaultValue("control");
		handlerValue = (BoolValue)attrs.getVariableValue("handler");
		indepValue = (BoolValue)attrs.getVariableValue("independent");
		widthValue = (NumericIntValue)attrs.getVariableValue("width");
	    } catch(UndefinedSymbolException ex) {
		/* This shouldn't happen, but if it does... */
		throw new RuntimeTypeException("Error on line " + 
					       body.getLine() + 
					       " of " + body.getFileName() +
					       ": Port " + p.getName() +
					       " is missing an internal " +
					       "attribute for instance " + 
					       e.getFullyQualifiedName());
	    }
	    
	    Liberty.LSS.IMR.Port port;
	    if(widthValue == null) {
		throw new TypeException("Error on line " + body.getLine() +
					" of " + body.getFileName() +
					": Attribute width must be set " +
					"for port " + p.getName() + " on " +
					"instance " + 
					e.getFullyQualifiedName());
	    }
	    int width = (int)widthValue.getValue();

	    if(!inst.isLeaf()) {
		if(handlerValue != null || indepValue != null) {
		    throw new TypeException("Error on line " + 
					    body.getLine() + 
					    " of " + body.getFileName() +
					    ": Attributes handler and " +
					    "independent should BOTH be not " +
					    "set for port " + p.getName() +
					    " on HIERARCHICAL instance " +
					    e.getFullyQualifiedName());
		}

		port = new Liberty.LSS.IMR.Port(inst, p.getName(),
						width, p.getDirection(),
						type, ctrl, defCtrl);
	    } else {
		boolean handler = false, indep = false;
		if(handlerValue != null)
		    handler = handlerValue.getValue();
		if(indepValue != null)
		    indep = indepValue.getValue();

		port = new Liberty.LSS.IMR.Port(inst, p.getName(),
						width, p.getDirection(),
						type, ctrl, defCtrl,
						indep, handler);
	    }

	    p.setIMRPort(port);
	    inst.addPort(port);
	}

	copyConnections(inst, e, typeVars);
    }

    private void copyEvents(Liberty.LSS.IMR.Instance inst,
			    InstanceEnvironment e) {
	Collection c = e.getEvents();
	Iterator i = c.iterator();
	while(i.hasNext())
	    inst.addEvent((Event)i.next());
    }

    private void copyExportedTypes(Liberty.LSS.IMR.Instance inst,
				   InstanceEnvironment e) {
	Map m = e.getExportedTypes();
	Iterator i = m.entrySet().iterator();
	while(i.hasNext()) {
	    Map.Entry entry = (Map.Entry)i.next();
	    inst.addExportedType((String)entry.getKey(), 
				 (Type)entry.getValue());
	}
    }

    public static void copyCollectors(HashMap insts,
				      ExecutionContext c) 
	throws UndefinedSymbolException {
	copyCollectors(insts, c, null);
    }
    
    public static void copyCollectors(HashMap insts,
				      ExecutionContext c,
				      InstanceEnvironment e)
	throws UndefinedSymbolException {
	if(e == null) {
	    e = c.instanceSpace.getCurrentEnvironment();
	}

	List astCollectors = e.getCollectors();
	Iterator i = astCollectors.iterator();
	Vector imrCollectors = new Vector();
	
	while(i.hasNext()) {
	    Liberty.LSS.AST.Collector astCollector = 
		(Liberty.LSS.AST.Collector)i.next();
	    String[] instanceName = astCollector.getInstanceName();

	    int startIndex = 0;
	    if(instanceName.length > 0 && instanceName[0].equals("")) {
		String fqn[] = e.getFullyQualifiedName().split("\\.");
		
		for(int j = 0;  j < fqn.length; j++) {
		    if(!fqn[j].equals(instanceName[j+1])) {
			CodeObtainable code = astCollector.getDeclaration();
			String pqn = null;
			for(int index = 1; index <= j+1; index++) {
			    pqn = (pqn == null) ? "" : (pqn + ".");
			    pqn = pqn + instanceName[index];
			}

			throw new 
			    UndefinedSymbolException(code, 
						     "Instance " + pqn + 
						     " is not a subinstance "+
						     "of "+ 
						     e.getFullyQualifiedName()+
						     ".");
		    }
		}
		startIndex = fqn.length + 1;
	    }

	    Instance inst = null;
	    for(int j = startIndex; j < instanceName.length; j++) {
		if(j == startIndex) 
		    inst = (Instance)insts.get(instanceName[j]);
		else if(!inst.isLeaf()) 
		    inst = inst.getSubInstance(instanceName[j]);
		else
		    inst = null;

		if(inst == null) {
		    CodeObtainable code = astCollector.getDeclaration();
		    String pqn = null;
		    for(int index = startIndex > 0 ? 1 : 0; 
			index <= j; index++) {
			pqn = (pqn == null) ? "" : (pqn + ".");
			pqn = pqn + instanceName[index];
		    }
		    throw new UndefinedSymbolException(code, 
						       "Instance " + pqn + 
						       " does not exist.");
		}
	    }

	    Event event;
	    if(inst != null) {
		event = inst.getEvent(astCollector.getEventName());
		if(event == null) {
		    CodeObtainable code = astCollector.getDeclaration();
		    String eventName = astCollector.getEventName();
		    String pqn = null;
		    for(int index = startIndex > 0 ? 1 : 0; 
			index < instanceName.length; index++) {
			pqn = (pqn == null) ? "" : (pqn + ".");
			pqn = pqn + instanceName[index];
		    }		    
		    throw new UndefinedSymbolException(code, "Instance " + 
						       pqn +
						       " does not emit event " +
						       eventName);
		}
	    } else {
		/* Top level event collector */
		event = c.instanceSpace.getEvent(astCollector.getEventName());
		if(event == null) {
		    CodeObtainable code = astCollector.getDeclaration();
		    String eventName = astCollector.getEventName();
		    throw new UndefinedSymbolException(code, "Toplevel " + 
						       " does not emit event " +
						       eventName);
		}
	    }
	    
	    StringValue header = astCollector.get("header");
	    StringValue decl = astCollector.get("decl");
	    StringValue init = astCollector.get("init");
	    StringValue record = astCollector.get("record");
	    StringValue report = astCollector.get("report");
	    
	    Liberty.LSS.IMR.Collector imrCollector = new
		Liberty.LSS.IMR.Collector(event, inst, header, decl, init,
					  record, report);

	    if(inst != null) {
		inst.addCollector(imrCollector);
	    } else {
		/* Top level event collector */
		c.instanceSpace.addCollector(imrCollector);
	    }
	}
    }
}

