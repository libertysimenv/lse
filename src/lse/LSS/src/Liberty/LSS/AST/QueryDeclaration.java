package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.IMR.Query;

public class QueryDeclaration extends Statement
{
    private TokenInfo queryTok;
    private TokenInfo returnTok;
    private TokenInfo lparen, rparen;
    private TokenInfo colon, semi;
    private TokenInfo locked;
    private IdentifierList queries;
    private StringLiteral arguments, returnType;

    public QueryDeclaration(TokenInfo q, IdentifierList l, TokenInfo c,
			    TokenInfo lp, StringLiteral a, TokenInfo r, 
			    StringLiteral rt, TokenInfo rp, TokenInfo sc) {
	queryTok = q;
	queries = l;
	colon = c;
	locked = null;
	lparen = lp;
	arguments = a;
	returnTok = r;
	returnType = rt;
	rparen = rp;
	semi = sc;
    }

    public QueryDeclaration(TokenInfo k,
			    TokenInfo q, IdentifierList l, TokenInfo c,
			    TokenInfo lp, StringLiteral a, TokenInfo r, 
			    StringLiteral rt, TokenInfo rp, TokenInfo sc) {
	queryTok = q;
	queries = l;
	colon = c;
	locked = k;
	lparen = lp;
	arguments = a;
	returnTok = r;
	returnType = rt;
	rparen = rp;
	semi = sc;
    }


    public String getOriginalCode() { 
	String lockStr;
	if(locked != null) 
	    lockStr = locked.getOriginalCode();
	else
	    lockStr = "";
	return lockStr + 
	    queryTok.getOriginalCode() +
	    queries.getOriginalCode() +
	    colon.getOriginalCode() +
	    lparen.getOriginalCode() +
	    arguments.getOriginalCode() +
	    returnTok.getOriginalCode() +
	    returnType.getOriginalCode() +
	    rparen.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	String lockStr;
	if(locked != null) 
	    lockStr = locked.toString();
	else
	    lockStr = "";
	return lockStr + " " +
	    queryTok.toString() + " " +
	    queries.toString() + " " +
	    colon.toString() + " " +
	    lparen.toString() +
	    arguments.toString() + " " +
	    returnTok.toString() + " " +
	    returnType.toString() +
	    rparen.toString() +
	    semi.toString();
    }

    public int getLine() {
	return queryTok.getLine();
    }

    public String getFileName() {
	return queryTok.getFileName();
    }

    public void execute(ExecutionContext cp) 
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException, 
	       UnimplementedException, 
	       TypeException,
	       UninitializedVariableException,
	       ParseException {
	Iterator i = queries.iterator();
	while(i.hasNext()) {
	    Identifier queryName = (Identifier)i.next();
	    StringValue argValue = (StringValue)arguments.evaluate(cp);
	    StringValue retVal = (StringValue)returnType.evaluate(cp);

	    Query q = new Query(queryName.toString(), 
				(queryTok.toString().equals("query")) ? 
				Query.QUERY : Query.METHOD,
				(locked != null) ? true : false,
				argValue, retVal);
	    try {
		InstanceEnvironment e = cp.instanceSpace.getCurrentEnvironment();
		e.addQuery(q);
	    } catch(SymbolAlreadyDefinedException e) {
		throw new SymbolAlreadyDefinedException("Error on line " + 
							getLine() + 
							" of " + 
							getFileName() + 
							": " + e.getMessage());
	    } catch(TypeException e) {
		throw new TypeException("Error on line " + 
					getLine() + 
					" of " + 
					getFileName() + 
					": " + e.getMessage());
	    }
	}
    }
}
