package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class TypeModifierList implements CodeObtainable {
    private TypeModifier[] modifiers;

    public TypeModifierList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	modifiers = new TypeModifier[v.size()];
	modifiers = (TypeModifier[])v.toArray(modifiers);
    }

    public Vector getTypeModifierVector() {
	Vector v = new Vector();
	for(int i = 0; i < modifiers.length; i++) {
	    v.add(modifiers[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new TypeModifierListIterator(modifiers);
    }

    public String getOriginalCode() {
	String s = "";
	for(int i = 0; i < modifiers.length; i++) {
	    s = s + modifiers[i].getOriginalCode();
	}
	return s;
    }

    public int getLine() {
	return modifiers[0].getLine();
    }

    public String getFileName() {
	return modifiers[0].getFileName();
    }

    public String toString() {
	String s = "";
	for(int i = 0; i < modifiers.length; i++) {
	    s = s + " " + modifiers[i];
	}
	return s;
    }
}

class TypeModifierListIterator implements Iterator {
    TypeModifier[] list;
    int pos;

    TypeModifierListIterator(TypeModifier[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    TypeModifier m = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    TypeModifier m = list[pos];
	    pos++;
	    return m;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
