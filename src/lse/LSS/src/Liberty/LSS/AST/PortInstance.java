package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class PortInstance extends ValueBase
    implements Aggregate
{
    private Port port;
    private int index;
    private boolean connected;

    public PortInstance(Port p, int i, boolean c) {
	super(Types.portInstanceRefType);
	port = p;
	index = i;
	connected = c;
    }

    public Port getPort() {
	return port;
    }

    public int getIndex() {
	return index;
    }

    public Value getSubValue(String field) 
	throws TypeException,
	       UninitializedVariableException {
	if(field.equals("connected"))
	    return new BoolValue(connected);
	else
	    throw new TypeException(field + 
				    " is not a valid attribute on type " + 
				    this.getType());
    }

    public boolean equals(Value y) {
	if(y == null) return false;

	if(!(y instanceof PortInstance)) {
	    return false;
	}

	PortInstance pi = (PortInstance)y;
	return (getPort().equals(pi.getPort()) &&
		getIndex() == pi.getIndex());
    }

    public int hashCode() {
	return port.hashCode() + index;
    }
}
