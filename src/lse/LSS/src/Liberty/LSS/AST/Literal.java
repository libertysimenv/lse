package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public abstract class Literal extends ExpressionBase {
    private TokenInfo token;

    public Literal(TokenInfo ti) {
	token = ti;
    }

    public TokenInfo getToken() {
	return token;
    }

    public String getOriginalCode() {
	return token.getOriginalCode();
    }

    public int getLine() {
	return token.getLine();
    }

    public String getFileName() {
	return token.getFileName();
    }
}
