package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ReturnStatement extends Statement {
    private TokenInfo returnToken,semi;
    private Expression value;

    public ReturnStatement(TokenInfo r, Expression e, TokenInfo s) {
	returnToken = r;
	semi = s;
	value = e;
    }

    public ReturnStatement(TokenInfo r, TokenInfo s) {
	this(r,null,s);
    }

    public String getOriginalCode() {
	if(value != null) {
	    return returnToken.getOriginalCode() + value.getOriginalCode() + 
		semi.getOriginalCode();
	} else {
	    return returnToken.getOriginalCode() + semi.getOriginalCode();
	}
    }

    public String toString() {
	if(value != null) {
	    return returnToken.toString() + " " + value.toString() + 
		semi.toString();
	} else {
	    return returnToken.toString() + semi.toString();
	}
    }

    public int getLine() {
	return returnToken.getLine();
    }

    public String getFileName() {
	return returnToken.getFileName();
    }

    public void execute(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	if(value == null)
	    throw new ReturnException(this);
	else 
	    throw new ReturnException(value.evaluate(c), this);
    }
}
