package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.types.*;
import Liberty.LSS.IMR.StructAdd;

public class EnumCreateFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType enumCreateType;

    static {
	Vector v = new Vector();
	ArrayType names = new ArrayType(Types.literalType);
	v.add(names);
	
	enumCreateType = new FunctionType(v, Types.typeType);
    }

    public EnumCreateFunction(ExecutionContext c) {
	super(enumCreateType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	FunctionType ft = ((FunctionType)getType());
	
	if(argValues.size() != 1)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + 
					Integer.toString(argValues.size()) +
					" arguments");
	
	try {
	    Vector v = new Vector();
	    v.add(TypeRelations.attemptDeref((Value)argValues.elementAt(0)));
	    argValues = v;
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}
	
	int i = 0;
	ArrayValue nameArray;
	try {
	    nameArray = (ArrayValue)argValues.elementAt(i++);
	    Type nameArrayElementType = 
		((ArrayType)nameArray.getType()).getElementType();
	    if(!TypeRelations.isSubtype(nameArrayElementType, 
					Types.literalType))
		throw new ClassCastException();
	} catch(ClassCastException e2) {
	    throw new FunctionException("Argument " + 
					Integer.toString(i) + " in call to " +
					"function with type " + 
					getType() + " must have type " +
					ft.getArgumentType(i-1));
	} 

	Vector names = new Vector();
	Iterator namesIter = nameArray.getValue().iterator();
	while(namesIter.hasNext()) {
	    StringValue v = (StringValue)namesIter.next();
	    String name = v.getFlattenedValue();
	    names.add(name);
	}

	try {
	    EnumType newType = new EnumType(names);
	    EnumValue[] vals = newType.getValues();
	    Type constEnum = new ConstType(newType);
	    for(i = 0; i < vals.length; i++) {
		c.addVariable(vals[i].toString(), constEnum, vals[i]);
	    }
	    c.instanceSpace.addGlobalType(newType);
	    return newType;
	} catch(TypeException e) {
	    throw new FunctionException(e.getMessage());
	} catch(SymbolAlreadyDefinedException e) {
	    throw new FunctionException(e.getMessage());
	}
    }
}
