package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class AssignExpression extends BinaryOpExpression
{
    public AssignExpression(Expression e1, TokenInfo o, Expression e2) {
	super(e1, o, e2);
    }

    protected void assign(LValue lv, Value v)
	throws UndefinedSymbolException,
	       TypeException {
	try {
	    lv.assign(v,this);
	} catch(TypeException e) {
	    throw new TypeException(this, e.getMessage());
	}
    }	

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value v = getRightExpression().evaluate(c);
	LValue lv = getLeftExpression().getLValue(c);
	
	assign(lv,v);  
	return v;
    }
}
