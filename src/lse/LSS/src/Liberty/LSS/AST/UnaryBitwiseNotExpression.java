package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class UnaryBitwiseNotExpression extends UnaryOpExpression
{
    public UnaryBitwiseNotExpression(TokenInfo o, Expression e) {
        super(o,e);
    }

    public Value evaluate(ExecutionContext c)
        throws UnimplementedException,
               TypeException,
               UndefinedSymbolException,
               UninitializedVariableException,
               ParseException,
               SymbolAlreadyDefinedException {
        Value v = this.getExpression().evaluate(c);
        try {
            return Operate.unaryBitwiseNot(v);
        } catch(TypeException e) {
            throw new TypeException("Error on line " + this.getLine() +
                                    " of " + this.getFileName() +
                                    ": " + e.getMessage());
        } catch(UninitializedVariableException e) {
            throw new TypeException("Error on line " + this.getLine() +
                                    " of " + this.getFileName() +
                                    ": " + e.getMessage());
        }
    }
}
