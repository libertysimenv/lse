package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class UserPointTypeSpecification extends TypeSpecification
{
    private TokenInfo userpointToken,returnsToken;
    private TokenInfo lparen,rparen;
    private Expression arguments, returnType;

    public UserPointTypeSpecification(TokenInfo u, TokenInfo lp, 
				      Expression a, TokenInfo r,
				      Expression rt, TokenInfo rp) {
	userpointToken = u;
	returnsToken = r;
	lparen = lp;
	rparen = rp;
	arguments = a;
	returnType = rt;
    }

    public String getOriginalCode() {
	return userpointToken.getOriginalCode() + 
	    lparen.getOriginalCode() +
	    arguments.getOriginalCode() +
	    returnsToken.getOriginalCode() +
	    returnType.getOriginalCode() +
	    rparen.getOriginalCode();
    }
    
    public String toString() {
	return userpointToken.toString() + 
	    lparen.toString() +
	    arguments.toString() + " " +
	    returnsToken.toString() +
	    returnType.toString() + 
	    rparen.toString();
    }

    public int getLine() {
	return userpointToken.getLine();
    }

    public String getFileName() {
	return userpointToken.getFileName();
    }

    public Type getType(ExecutionContext c) throws TypeException {
	Value args, ret;
	try {
	    args = arguments.evaluate(c);
	    ret = returnType.evaluate(c);
	} catch(LSSException e) {
	    throw new TypeException(e.getSourceLine(),
				    e.getRawMessage());
	}
	
	if(!TypeRelations.isSubtype(args.getType(), Types.stringType))
	    throw new TypeException(this, 
				    "userpoint arguments must have type " +
				    Types.stringType);
	
	if(!TypeRelations.isSubtype(ret.getType(), Types.stringType))
	    throw new TypeException(this, 
				    "userpoint return type must have type " + 
				    Types.stringType);
		
	Type newType = new UserPointType((StringValue)args,
					     (StringValue)ret);
	c.instanceSpace.addGlobalType(newType);
	return newType;
    }
}
