package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class InitDeclaratorList implements CodeObtainable {
    private InitDeclarator[] initDeclarators;
    private TokenInfo[] commas;

    public InitDeclaratorList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	initDeclarators = new InitDeclarator[(v.size()+1)/2];
	commas = new TokenInfo[(v.size()-1)/2];

	try {
	    initDeclarators[0] = (InitDeclarator)v.firstElement();
	    for(int i = 1; i < v.size(); i+=2) {
		commas[(i-1)/2] = (TokenInfo)v.elementAt(i);
		initDeclarators[(i+1)/2] = (InitDeclarator)v.elementAt(i+1);
	    }
	} catch(ClassCastException e) {
	    throw new ArrayStoreException("Cannot convert given vector into an InitDeclaratorList");
	}
    }

    public Vector getInitDeclaratorVector() {
	Vector v = new Vector();

	v.add(initDeclarators[0]);
	for(int i = 1; i < initDeclarators.length; i++) {
	    v.add(commas[i-1]);
	    v.add(initDeclarators[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new InitDeclaratorListIterator(initDeclarators);
    }

    public String getOriginalCode() {
	String s = initDeclarators[0].getOriginalCode();
	for(int i = 1; i < initDeclarators.length; i++) {
	    s += commas[i-1].getOriginalCode() + 
		initDeclarators[i].getOriginalCode();
	}
	return s;
    }

    public String toString() {
	String s = initDeclarators[0].toString();
	for(int i = 1; i < initDeclarators.length; i++) {
	    s += commas[i-1].toString() + " " +
		initDeclarators[i].toString();
	}
	return s;
    }

    public int getLine() {
	return initDeclarators[0].getLine();
    }

    public String getFileName() {
	return initDeclarators[0].getFileName();
    }
}

class InitDeclaratorListIterator implements Iterator {
    InitDeclarator[] list;
    int pos;

    InitDeclaratorListIterator(InitDeclarator[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    InitDeclarator d = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    InitDeclarator d = list[pos];
	    pos++;
	    return d;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
