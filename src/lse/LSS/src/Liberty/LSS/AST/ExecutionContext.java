package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.AST.*;
import Liberty.util.*;
import java.util.regex.*;
import java.util.*;
import java.io.*;
import Liberty.LSS.IMR.Event;

public class ExecutionContext extends StateCollection
{
    public InstanceSpace instanceSpace;
    private Vector modulePath;

    private Package currentPackage;
    private Vector packageSearchList;

    /****************/
    /* Constructors */
    /****************/
    public ExecutionContext()
    {
	super(false);

	instanceSpace = new InstanceSpace();
	instanceSpace.setCurrentEnvironment(new BaseInstanceEnvironment());

	currentPackage = null;
	packageSearchList = new Vector();
    }

    /* Copy Constructor */
    protected ExecutionContext(ExecutionContext c) {
	super(c);
	instanceSpace = c.instanceSpace;
	modulePath = c.modulePath;
	currentPackage = c.currentPackage;
	packageSearchList = c.packageSearchList;
    }

    /************************************/
    /* State backup and cloning methods */
    /************************************/
    protected StateCollection cloneState() {
	return new ExecutionContext(this);
    }

    public StateCollection checkpointState() {
	ExecutionContext c = (ExecutionContext)super.checkpointState();
	c.currentPackage = currentPackage.checkpoint();
	return c;
    }

    public ExecutionContext checkpoint() {
	return (ExecutionContext)checkpointState();
    }


    /******************************/
    /* Package Management methods */
    /******************************/
    public void reset() {
	super.reset();
	resetPackageSearchList();
    }

    public Package getCurrentPackage() {
	return currentPackage;
    }

    public void setCurrentPackage(Package p) {
	currentPackage = p;
    }
    
    public List getPackageSearchList() {
	return packageSearchList;
    }

    public void setPackageSearchList(List l) {
	packageSearchList = new Vector(l);
    }

    public void resetPackageSearchList() {
	try {
	    Package p = instanceSpace.getPackage("LSS");
	    packageSearchList = new Vector();
	    packageSearchList.add(p);
	} catch(UndefinedSymbolException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
    }

    /******************************/
    /* Module Search Path methods */
    /******************************/
    public void setModulePath(Vector v) {
	modulePath = v;
    }

    public List getModulePath() {
	return modulePath;
    }

    public static String getFileName(File prefix, String name) 
	throws FileNotFoundException {
	/* Check if this is an absolute path name */
	File absf = new File(name);
	if(absf.isAbsolute()) {
	    if(absf.exists())
		return absf.getAbsolutePath();
	} else {
	    File t = new File(prefix,name);
	    if(t.exists()) return t.getAbsolutePath();
	}
	throw new FileNotFoundException("Can not find file " + name);
    }

    public String getFileName(String name)
	throws FileNotFoundException {
	
	Iterator i = modulePath.iterator();
	while(i.hasNext()) {
	    File f = (File)i.next();
	    try {
		return getFileName(f, name);
	    } catch(FileNotFoundException e) {
		/* Ignore this, we'll try another */
	    }
	}
	throw new FileNotFoundException("Can not find file " + name);
    }

    /*******************/
    /* Utility Methods */
    /*******************/
    protected String[] splitName(String name) {
	String symbol, pkg;

	Pattern splitPattern = Pattern.compile("(.*)::(.*)");
	Matcher m = splitPattern.matcher(name);
	if(!m.matches()) {
	    pkg = null;
	    symbol = name;
	} else {
	    pkg = m.group(1);
	    symbol = m.group(2);
	}
	
	String[] bundle = new String[2];
	bundle[0] = pkg;
	bundle[1] = symbol;
	return bundle;
    }

    protected Package getPackage(String pkgName) {
	try {
	    /* This method ensures that the local copy of the current
	     * package is used rather than any stored in the instance
	     * space */

	    if(pkgName.equals(currentPackage.getName()))
		return currentPackage;
	    else
		return instanceSpace.getPackage(pkgName);
	} catch(UndefinedSymbolException e) {
	    return null;
	}
    }

    protected List getPotentialPackageList(String pkgName) {
	if(pkgName == null) {
	    return getFindList();
	} else {
	    Package pkg;
	    Vector ppl = new Vector();
	    
	    /* Assume package name is fully qualified */
	    pkg = getPackage(pkgName);
	    if(pkg != null) ppl.add(pkg);

	    /* Prepend package names from the search list to given
	     * package name */
	    Iterator packageIter = getFindList().iterator();
	    while(packageIter.hasNext()) {
		Package searchPkg = (Package)packageIter.next();
		pkg = getPackage(searchPkg.getName() + "." + pkgName);
		if(pkg != null) ppl.add(pkg);
	    }
	    return ppl;
	}
    }

    protected List getFindList() {
	Vector v = new Vector(packageSearchList);

	/* This method ensures that the local copy of the current
	 * package is used rather than any stored in the instance
	 * space */

	/* Remove the current package from the list */
	Iterator i = v.iterator();
	while(i.hasNext()) {
	    Package p = (Package)i.next();
	    if(p.getName().equals(currentPackage.getName()))
		i.remove();
	}
	
	/* Add the current package to the start of the search list */
	v.add(0, currentPackage);

	return v;
    }

    protected Package findVariable(Package pkg, String var) {
	if(pkg == null) return null;
	if(pkg.variableDefined(var)) return pkg;
	return null;
    }

    protected Package findVariable(String var) {
	return findVariable((String)null, var);
    }

    protected Package findVariable(String pkgName, String var) {
	Package pkg;
	Iterator packageIter = getPotentialPackageList(pkgName).iterator();
	while(packageIter.hasNext()) {
	    pkg = (Package)packageIter.next();
	    if(findVariable(pkg, var) != null) return pkg;
	}

	/* No luck */
	return null;
    }

    protected Package findType(Package pkg, String var) {
	try {
	    pkg.getType(var);
	    return pkg;
	} catch(UndefinedSymbolException e) { 
	    return null;
	}
    }

    protected Package findType(String var) {
	return findType((String)null, var);
    }

    protected Package findType(String pkgName, String var) {
	Package pkg;
	Iterator packageIter = getPotentialPackageList(pkgName).iterator();
	while(packageIter.hasNext()) {
	    pkg = (Package)packageIter.next();
	    if(findType(pkg, var) != null) return pkg;
	}

	/* No luck */
	return null;
    }

    protected Package findModule(Package pkg, String var) {
	try {
	    pkg.getModule(var);
	    return pkg;
	} catch(UndefinedSymbolException e) { 
	    return null;
	}
    }

    protected Package findModule(String var) {
	return findModule((String)null, var);
    }

    protected Package findModule(String pkgName, String var) {
	Package pkg;
	Iterator packageIter = getPotentialPackageList(pkgName).iterator();
	while(packageIter.hasNext()) {
	    pkg = (Package)packageIter.next();
	    if(findModule(pkg, var) != null) return pkg;
	}

	/* No luck */
	return null;
    }

    protected Package findEvent(Package pkg, String var) {
	try {
	    pkg.getEvent(var);
	    return pkg;
	} catch(UndefinedSymbolException e) { 
	    return null;
	}
    }	

    protected Package findEvent(String var) {
	return findEvent((String)null, var);
    }

    protected Package findEvent(String pkgName, String var) {
	Package pkg;
	Iterator packageIter = getPotentialPackageList(pkgName).iterator();
	while(packageIter.hasNext()) {
	    pkg = (Package)packageIter.next();
	    if(findEvent(pkg, var) != null) return pkg;
	}

	/* No luck */
	return null;
    }

    public void noSuchSymbol(String var, 
			     UndefinedSymbolException superEx) 
	throws UndefinedSymbolException{
	if(superEx != null) 
	    throw superEx;
	else
	    throw new UndefinedSymbolException("Symbol " + 
					       qualifySymbol(var) + 
					       " is not defined");
    }


    /*************************************************************/
    /* Re-implement methods of StateCollection to do appropriate */
    /* package searching                                         */
    /*************************************************************/
    public boolean variableDefined(String var) {
	boolean retval = false;

	String[] bundle = splitName(var);

	if(!emptyContext() && bundle[0] == null) 
	    retval = super.variableDefined(var);
	
	Package p = findVariable(bundle[0], bundle[1]);
	if(p != null) 
	    retval |= p.variableDefined(bundle[1]);

	return retval;
    }

    public Type getVariableType(String var) 
	throws UndefinedSymbolException {
	String[] bundle = splitName(var);

	UndefinedSymbolException superEx = null;
	if(!emptyContext() && bundle[0] == null) {
	    try {
		return super.getVariableType(var);
	    } catch(UndefinedSymbolException e) {
		superEx = e;
	    }
	}

	Package p = findVariable(bundle[0], bundle[1]);
	if(p == null)
	    noSuchSymbol(var, superEx);

	try {
	    return p.getVariableType(bundle[1]);
	} catch(UndefinedSymbolException e) {
	    if(superEx != null) throw superEx;
	    else throw e;
	}
    }

    public Value getVariableValue(String var) 
	throws UndefinedSymbolException {
	String[] bundle = splitName(var);

	UndefinedSymbolException superEx = null;
	if(!emptyContext() && bundle[0] == null) {
	    try {
		return super.getVariableValue(var);
	    } catch(UndefinedSymbolException e) {
		superEx = e;
	    }
	}

	Package p = findVariable(bundle[0], bundle[1]);
	if(p == null)
	    noSuchSymbol(var, superEx);
	try {
	    return p.getVariableValue(bundle[1]);
	} catch(UndefinedSymbolException e) {
	    if(superEx != null) throw superEx;
	    else throw e;
	}
    }

    public void addVariable(String var, Type t, Value v)
	throws SymbolAlreadyDefinedException,
	       TypeException {
	if(emptyContext()) {
	    currentPackage.addVariable(var,t,v);
	} else {
	    super.addVariable(var,t,v);
	}
    }

    public void overloadFunctionType(String var, Type t)
	throws SymbolAlreadyDefinedException,
	       EmptyContextException,
	       TypeException {
	if(emptyContext()) {
	    currentPackage.overloadFunctionType(var,t);
	} else {
	    super.overloadFunctionType(var,t);
	}
    }

    public void overloadFunctionValue(String var, Value v)
	throws UndefinedSymbolException,
	       EmptyContextException,
	       TypeException {
	if(emptyContext()) {
	    currentPackage.overloadFunctionValue(var,v);
	} else {
	    super.overloadFunctionValue(var,v);
	}
    }

    public void initVariable(String var, Value v) 
	throws TypeException,
	       UndefinedSymbolException {
	if(emptyContext()) {
	    currentPackage.initVariable(var,v);
	} else {
	    super.initVariable(var,v);
	}
    }

    public void setVariableValue(String var, Value v)
	throws UndefinedSymbolException,
	       TypeException {
	String[] bundle = splitName(var);

	UndefinedSymbolException superEx = null;
	if(!emptyContext() && bundle[0] == null) {
	    try {
		super.setVariableValue(var,v);
		return;
	    } catch(UndefinedSymbolException e) {
		superEx = e;
	    }
	}

	Package p = findVariable(bundle[0], bundle[1]);
	if(p == null)
	    noSuchSymbol(var, superEx);
	try {
	    p.setVariableValue(bundle[1],v);
	    return;
	} catch(UndefinedSymbolException e) {
	    if(superEx != null) throw superEx;
	    else throw e;
	}
    }

    public void addType(String name, Type t)
	throws SymbolAlreadyDefinedException {
	if(emptyContext()) {
	    currentPackage.addType(name,t);
	} else {
	    super.addType(name,t);
	}
    }

    public Type getType(String name) 
	throws UndefinedSymbolException {
	String[] bundle = splitName(name);

	UndefinedSymbolException superEx = null;
	if(!emptyContext() && bundle[0] == null) {
	    try {
		return super.getType(name);
	    } catch(UndefinedSymbolException e) {
		superEx = e;
	    }
	}

	Package p = findType(bundle[0], bundle[1]);
	if(p == null)
	    noSuchSymbol(name, superEx);
	try {
	    return p.getType(bundle[1]);
	} catch(UndefinedSymbolException e) {
	    if(superEx != null) throw superEx;
	    else throw e;
	}
    }

    public void addModule(Module m)
	throws SymbolAlreadyDefinedException {
	if(emptyContext()) {
	    currentPackage.addModule(m);
	} else {
	    super.addModule(m);
	}
    }

    public void addModule(String name) 
	throws SymbolAlreadyDefinedException {
	if(emptyContext()) {
	    currentPackage.addModule(name);
	} else {
	    super.addModule(name);
	}
    }

    public void setModule(Module m) 
	throws UndefinedSymbolException {
	if(emptyContext()) {
	    currentPackage.setModule(m);
	} else {
	    super.setModule(m);
	}
    }

    public Module getModule(String name) 
	throws UndefinedSymbolException {
	String[] bundle = splitName(name);

	UndefinedSymbolException superEx = null;
	if(!emptyContext() && bundle[0] == null) {
	    try {
		return super.getModule(name);
	    } catch(UndefinedSymbolException e) {
		superEx = e;
	    }
	}

	Package p = findModule(bundle[0], bundle[1]);
	if(p == null)
	    noSuchSymbol(name, superEx);
	try {
	    return p.getModule(bundle[1]);
	} catch(UndefinedSymbolException e) {
	    if(superEx != null) throw superEx;
	    else throw e;
	}
    }

    public void addEvent(Event e)
	throws SymbolAlreadyDefinedException {
	if(emptyContext()) {
	    currentPackage.addEvent(e);
	} else {
	    super.addEvent(e);
	}
    }

    public void addEvent(String name) 
	throws SymbolAlreadyDefinedException {
	if(emptyContext()) {
	    currentPackage.addEvent(name);
	} else {
	    super.addEvent(name);
	}
    }

    public void setEvent(Event e) 
	throws UndefinedSymbolException {
	if(emptyContext()) {
	    currentPackage.setEvent(e);
	} else {
	    super.setEvent(e);
	}
    }
			
    public Event getEvent(String name) 
	throws UndefinedSymbolException {
	String[] bundle = splitName(name);

	UndefinedSymbolException superEx = null;
	if(!emptyContext() && bundle[0] == null) {
	    try {
		return super.getEvent(name);
	    } catch(UndefinedSymbolException e) {
		superEx = e;
	    }
	}

	Package p = findEvent(bundle[0], bundle[1]);
	if(p == null)
	    noSuchSymbol(name, superEx);
	try {
	    return p.getEvent(bundle[1]);
	} catch(UndefinedSymbolException e) {
	    if(superEx != null) throw superEx;
	    else throw e;
	}
    }
}
