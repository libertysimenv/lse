package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class FunctionDeclaration extends Statement {
    private TokenInfo funcToken;
    private TokenInfo lparen, rparen;
    private TokenInfo returns, semi;
    private Identifier name;
    private ArgumentList arguments;
    private Expression typeSpec;
    private CompoundStatement body;

    public FunctionDeclaration(TokenInfo f, Identifier i, TokenInfo lp,
			       TokenInfo rp, TokenInfo r, Expression t,
			       CompoundStatement c, TokenInfo s) {
	funcToken = f;
	name = i;
	lparen = lp;
	rparen = rp;
	returns = r;
	typeSpec = t;
	body = c;
	semi = s;
	arguments = null;
    }

    public FunctionDeclaration(TokenInfo f, Identifier i, TokenInfo lp,
			       ArgumentList l, TokenInfo rp, TokenInfo r, 
			       Expression t, 
			       CompoundStatement c, TokenInfo s) {
	this(f,i,lp,rp,r,t,c,s);
	arguments = l;
    }


    public String getOriginalCode() {
	String argStr = arguments == null ? "" : 
	    arguments.getOriginalCode();
	return funcToken.getOriginalCode() + 
	    name.getOriginalCode() +
	    lparen.getOriginalCode() +
	    argStr +
	    rparen.getOriginalCode() +
	    returns.getOriginalCode() +
	    typeSpec.getOriginalCode() +
	    body.getOriginalCode() +
	    semi.getOriginalCode();
    }

    public String toString() {
	String argStr = arguments == null ? "" : 
	    arguments.toString();
	return funcToken.toString() + " " +
	    name.toString() +
	    lparen.toString() +
	    argStr +
	    rparen.toString() + " " +
	    returns.toString() + " " +
	    typeSpec.toString() + " " +
	    body.toString() +
	    semi.toString();
    }

    public int getLine() {
	return funcToken.getLine();
    }

    public String getFileName() {
	return funcToken.getFileName();
    }

    public void execute(ExecutionContext cp) 
	throws SymbolAlreadyDefinedException,
	       UndefinedSymbolException, 
	       UnimplementedException, 
	       TypeException,
	       UninitializedVariableException {
	Vector argTypes = new Vector();
	Vector argNames = new Vector();
	Type returnType;

	returnType = TypeSpecification.getType(cp, typeSpec);
	if(returnType.isPolymorphic()) {
	    throw new TypeException(this, "Cannot define a function " +
				    "with polymorphic return type " + 
				    returnType);
	}

	if(arguments != null) {
	    Iterator i = arguments.iterator();
	    while(i.hasNext()) {
		ArgumentDeclaration ad = (ArgumentDeclaration)i.next();
		Expression ts = ad.getTypeSpecification();
		Type type = 
		    TypeSpecification.getType(cp, ts);
		if(type.isPolymorphic()) {
		    throw new TypeException(this, "Cannot define a function " +
					    "with polymorphic argument type " +
					    type);
		}
		argTypes.add(type);
		argNames.add(ad.getName().toString());
	    }
	}

	FunctionType vtype = new FunctionType(argTypes, returnType);

	try {
	    cp.overloadFunctionType(name.toString(), vtype);
	} catch(TypeException e) {
	    throw new TypeException(this, e.getMessage());
	} catch(SymbolAlreadyDefinedException e) {
	    throw new SymbolAlreadyDefinedException(this, e.getMessage());
	}

	cp.overloadFunctionValue(name.toString(), 
				 new FunctionValue(vtype, argNames, body, cp));
    }
}
