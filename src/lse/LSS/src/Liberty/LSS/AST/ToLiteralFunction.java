package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ToLiteralFunction extends ValueBase implements Callable {
    private ExecutionContext context;

    private static FunctionType toLiteralType;

    static {
	Vector v = new Vector();
	v.add(Types.literalType);
	toLiteralType = new FunctionType(v, Types.stringType);
    }

    public ToLiteralFunction(ExecutionContext c) {
	super(toLiteralType);
	context = c.checkpoint();
    }

    public String toString() {
	return "{ ... }";
    }

    public Value call(Vector argValues, CodeObtainable callSite)
	throws FunctionException,
	       TypeException {
	ExecutionContext c = (ExecutionContext)context.enterScope();
	if(argValues.size() != 1)
	    throw new FunctionException("Cannot call function with type " + 
					getType() +
					" with " + argValues.size() +
					" arguments");
	
	String str;
	Value v = (Value)argValues.elementAt(0);
	try {
	    v = TypeRelations.attemptDeref(v);
	} catch(UninitializedVariableException e) {
	    throw new FunctionException(e.getMessage());
	}

	if(TypeRelations.isSubtype(v.getType(), Types.stringType)) {
	    StringValue s = (StringValue)v;
	    Value l = Coerce.subtypeCoerce(s, Types.literalType);
	    return l;
	} else {
	    throw new FunctionException("Argument 1 in call to " +
					"function with type " + getType() + 
					" must have type " +
					Types.stringType);
	}
    }
}
