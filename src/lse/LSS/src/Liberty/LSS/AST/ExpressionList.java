package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class ExpressionList implements CodeObtainable {
    private Expression[] expressions;
    private TokenInfo[] commas;

    public ExpressionList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	expressions = new Expression[(v.size()+1)/2];
	commas = new TokenInfo[(v.size()-1)/2];

	try {
	    expressions[0] = (Expression)v.firstElement();
	    for(int i = 1; i < v.size(); i+=2) {
		commas[(i-1)/2] = (TokenInfo)v.elementAt(i);
		expressions[(i+1)/2] = (Expression)v.elementAt(i+1);
	    }
	} catch(ClassCastException e) {
	    throw new ArrayStoreException("Cannot convert given vector into " +
					  "an ExpressionList");
	}
    }

    public Vector getExpressionVector() {
	Vector v = new Vector();

	v.add(expressions[0]);
	for(int i = 1; i < expressions.length; i++) {
	    v.add(commas[i-1]);
	    v.add(expressions[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new ExpressionListIterator(expressions);
    }

    public String getOriginalCode() {
	String s = expressions[0].getOriginalCode();
	for(int i = 1; i < expressions.length; i++) {
	    s += commas[i-1].getOriginalCode() + 
		expressions[i].getOriginalCode();
	}
	return s;
    }

    public String toString() {
	String s = expressions[0].toString();
	for(int i = 1; i < expressions.length; i++) {
	    s += commas[i-1].toString() + " " +
		expressions[i].toString();
	}
	return s;
    }

    public int getLine() {
	return expressions[0].getLine();
    }

    public String getFileName() {
	return expressions[0].getFileName();
    }
}

class ExpressionListIterator implements Iterator {
    Expression[] list;
    int pos;

    ExpressionListIterator(Expression[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    Expression d = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    Expression d = list[pos];
	    pos++;
	    return d;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
