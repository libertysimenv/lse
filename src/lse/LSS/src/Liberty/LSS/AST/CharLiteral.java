package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class CharLiteral extends Literal {
    private char value;
    private String valString;

    public CharLiteral(String v, TokenInfo ti) {
	super(ti);
	valString = v;
	if(v.length() == 1)
	    value = v.charAt(0);
	else if(v.length() == 2) {
	    switch(v.charAt(1)) {
	    case '\\':
		value = '\\';
		break;
	    case '\n':
		value = '\n';
		break;
	    case '\t':
		value = '\t';
		break;
	    case '\r':
		value = '\r';
		break;
	    }
	} else
	    value = '\0';
    }

    public char getValue() {
	return value;
    }

    public String toString() {
	return valString;
    }

    public Value evaluate(ExecutionContext c) {
	return new CharValue(this.getValue());
    }
}
