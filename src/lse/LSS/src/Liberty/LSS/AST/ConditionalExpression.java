package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ConditionalExpression extends ExpressionBase {
    private TokenInfo question, colon;
    private Expression conditional, trueExpr, falseExpr;

    public ConditionalExpression(Expression cond, TokenInfo q, Expression t, 
				 TokenInfo c, Expression f) {
	question = q;
	colon = c;
	conditional = cond;
	trueExpr = t;
	falseExpr = f;
    }

    public String getOriginalCode() {
	return conditional.getOriginalCode() + 
	    question.getOriginalCode() +
	    trueExpr.getOriginalCode() +
	    colon.getOriginalCode() +
	    falseExpr.getOriginalCode();
    }

    public String toString() {
	return conditional.toString() + " " +
	    question.toString() + " " +
	    trueExpr.toString() + " " +
	    colon.toString() + " " +
	    falseExpr.toString();
    }

    public int getLine() {
	return conditional.getLine();
    }

    public String getFileName() {
	return conditional.getFileName();
    }

    public Value evaluate(ExecutionContext c) 
	throws UnimplementedException,
	       TypeException,
	       UndefinedSymbolException,
	       UninitializedVariableException,
	       ParseException,
	       SymbolAlreadyDefinedException {
	Value v = conditional.evaluate(c);

	if(!TypeRelations.isSubtype(v.getType(), Types.boolType))
		v = TypeRelations.attemptDeref(v, this);
	
	if(TypeRelations.isSubtype(v.getType(), Types.boolType)) {
	    boolean cond = ((BoolValue)v).getValue();
	    if(cond)
		return trueExpr.evaluate(c);
	    else
		return falseExpr.evaluate(c);
	} else {
	    throw new TypeException(this, "Condition must have type " + 
				    Types.boolType + ", not " +
				    v.getType());
	}
    }
}
