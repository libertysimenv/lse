package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;

public class CollectorDefinitionList implements CodeObtainable {
    private CollectorDefinition[] definitions;

    public CollectorDefinitionList(Vector v) throws EmptyListException {
	if(v.size() == 0) throw new EmptyListException();
	definitions = new CollectorDefinition[v.size()];
	definitions = (CollectorDefinition[])v.toArray(definitions);
    }

    public Vector getCollectorDefinitionVector() {
	Vector v = new Vector();
	for(int i = 0; i < definitions.length; i++) {
	    v.add(definitions[i]);
	}
	return v;
    }

    public Iterator iterator() {
	return new CollectorDefinitionListIterator(definitions);
    }

    public String getOriginalCode() {
	String s = "";
	for(int i = 0; i < definitions.length; i++) {
	    s = s + definitions[i].getOriginalCode();
	}
	return s;
    }

    public int getLine() {
	return definitions[0].getLine();
    }

    public String getFileName() {
	return definitions[0].getFileName();
    }

    public String toString() {
	String s = "";
	for(int i = 0; i < definitions.length; i++) {
	    s = s + "\n" + definitions[i];
	}
	return s;
    }

    public void addToCollector(Collector c, ExecutionContext context) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Iterator i = this.iterator();
	while(i.hasNext()) {
	    CollectorDefinition d = (CollectorDefinition)i.next();
	    d.addToCollector(c, context);
	}
    }
}

class CollectorDefinitionListIterator implements Iterator {
    CollectorDefinition[] list;
    int pos;

    CollectorDefinitionListIterator(CollectorDefinition[] l) {
	list = l;
	pos = 0;
    }

    public boolean hasNext() {
	try {
	    CollectorDefinition s = list[pos];
	    return true;
	} catch (ArrayIndexOutOfBoundsException e) {
	    return false;
	}
    }

    public Object next() {
	try {
	    CollectorDefinition s = list[pos];
	    pos++;
	    return s;
	} catch (ArrayIndexOutOfBoundsException e) {
	    throw new NoSuchElementException();
	}
    }

    public void remove() {
	throw new UnsupportedOperationException();
    }
}
