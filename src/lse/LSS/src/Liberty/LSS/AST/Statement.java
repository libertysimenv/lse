package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public abstract class Statement implements CodeObtainable, Executable {
    public void execute(ExecutionContext c) 
	throws UnimplementedException, 
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	throw new UnimplementedException("Statement type not yet implemented");
    }
}
