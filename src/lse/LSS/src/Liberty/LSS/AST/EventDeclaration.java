package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.*;
import Liberty.LSS.IMR.Event;


public class EventDeclaration extends Statement {
    private TokenInfo eventToken;
    private TokenInfo semi;
    private TokenInfo lbrace, rbrace;
    private EventDataDeclarationList data;
    private Identifier eventName;

    public EventDeclaration(TokenInfo e, Identifier i, TokenInfo lb, 
			    EventDataDeclarationList l, TokenInfo rb,
			    TokenInfo s) {
	eventToken = e;
	eventName = i;
	lbrace = lb;
	data = l;
	rbrace = rb;
	semi = s;
    }

    public String getEventName() {
	return eventName.toString();
    }

    public String getOriginalCode() {
	String dataStr;
	if(data == null) dataStr = "";
	else dataStr = data.getOriginalCode();
	return eventToken.getOriginalCode() + eventName.getOriginalCode() + 
	    lbrace.getOriginalCode() + dataStr +
	    rbrace.getOriginalCode() + semi.getOriginalCode();
    }

    public String toString() {
	String dataStr;
	if(data == null) dataStr = "";
	else dataStr = data.toString();
	return eventToken.toString() + " " + eventName.toString() + 
	    " " + lbrace.toString() + "\n" + dataStr +
	    rbrace.toString() + semi.toString();
    }

    public int getLine() {
	return eventToken.getLine();
    }

    public String getFileName() {
	return eventToken.getFileName();
    }

    public void execute(ExecutionContext cp) 
	throws UnimplementedException,
	       TypeException,
	       BreakException,
	       ReturnException,
	       UndefinedSymbolException,
	       SymbolAlreadyDefinedException,
	       UninitializedVariableException,
	       ParseException {
	Event e = new Event(eventName.toString());
	if(data != null) {
	    Iterator i = data.iterator();
	    while(i.hasNext()) {
		EventDataDeclaration d = (EventDataDeclaration)i.next();
		String name = d.getName();
		StringValue type = d.getType(cp);
		try {
		    e.addData(name, type);
		} catch(SymbolAlreadyDefinedException ex) {
		    throw new SymbolAlreadyDefinedException(d,ex.getMessage());
		}
	    }
	}
	    
	try {
	    cp.addEvent(e);
	} catch(SymbolAlreadyDefinedException ex) {
	    throw new SymbolAlreadyDefinedException(this,ex.getMessage());
	}
    }
}
