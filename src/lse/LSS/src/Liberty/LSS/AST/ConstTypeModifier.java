package Liberty.LSS.AST;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class ConstTypeModifier extends TypeModifier {
    private TokenInfo constToken;

    public ConstTypeModifier(TokenInfo c) {
	constToken = c;
    }

    public String getOriginalCode() {
	return constToken.getOriginalCode();
    }

    public String toString() {
	return constToken.toString();
    }

    public int getLine() {
	return constToken.getLine();
    }

    public String getFileName() {
	return constToken.getFileName();
    }

    public TokenInfo getConstToken() {
	return constToken;
    }

    public Type getModifiedType(Type t) {
	return new ConstType(t);
    }
}
