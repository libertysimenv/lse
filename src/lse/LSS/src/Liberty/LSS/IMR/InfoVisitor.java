package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public interface InfoVisitor {
    public String structAdd(StructAdd n);
    public String query(Query n);
    public String parameter(Parameter n);
    public String event(Event n);
    public String connection(Connection n);
    public String codePoint(CodePoint n);
    public String port(Port n);
    public String runtimeVar(RuntimeVar v);
    public String domainInstance(DomainInstance i);
    public String domainSearchPathEntry(DomainSearchPathEntry e);
}
