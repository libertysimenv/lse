package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.InstanceSpace;
import Liberty.LSS.AST.SymbolAlreadyDefinedException;
import java.util.*;

public class Event implements InfoEntry
{
    public class DataItem 
    {
	private String name;
	private StringValue type;

	private DataItem(String n, StringValue t) {
	    name = n;
	    type = t;
	}

	public String getName() { return name; }
	public StringValue getType() { return type; }
	public void substitute(Map typeVariableContext, InstanceSpace space)
	    throws TypeInferenceException {
	    type = ConnectionManagement.substituteString(type, 
							 typeVariableContext, 
							 space);
	}
    }

    private String name;
    private Vector data;
    private HashMap dataNames;

    public Event(String n) {
	name = n;
	data = new Vector();
	dataNames = new HashMap();
    }

    public String getName() {
	return name;
    }

    public void addData(String n, StringValue t) 
	throws SymbolAlreadyDefinedException {
	if(dataNames.containsKey(n)) {
	    throw new SymbolAlreadyDefinedException("Event " + name 
						    + " already contains " +
						    "item named " + n);
	}
	DataItem item = new DataItem(n, t);
	data.add(item);
	dataNames.put(n, null);
    }

    public String getDataItemNames() {
	String s = null;
	Iterator i = data.iterator();
	while(i.hasNext()) {
	    DataItem d = (DataItem)i.next();
	    if(s == null) s = d.getName();
	    else s += "/" + d.getName();
	}
	if(s == null) return "";
	else return s;
    }

    public String getDataItemTypes() {
	String s = null;
	Iterator i = data.iterator();
	while(i.hasNext()) {
	    DataItem d = (DataItem)i.next();
	    if(s == null) s = d.getType().getFlattenedValue().trim();
	    else s += "/" + d.getType().getFlattenedValue().trim();
	}
	if(s == null) return "";
	else return s;
    }

    public String infoAccept(InfoVisitor v) {
	return v.event(this);
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {
	Iterator i = data.iterator();
	while(i.hasNext()) {
	    DataItem d = (DataItem)i.next();
	    d.substitute(typeVariableContext, space);
	}
    }
}
