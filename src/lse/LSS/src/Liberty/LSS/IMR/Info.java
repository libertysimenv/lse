package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.util.regex.*;
import java.util.*;
import java.io.*;

import org.jutil.java.collections.TransitiveClosure;

public class Info
{
    private PrintWriter instanceInfo;
    private PrintWriter parmInfo;
    private PrintWriter codePointInfo;
    private PrintWriter queryInfo;
    private PrintWriter structInfo;
    private PrintWriter portInfo;
    private PrintWriter eventInfo;
    private PrintWriter typeInfo;
    private PrintWriter varInfo;
    private PrintWriter domainInfo;

    private class Visitor implements InfoVisitor {
	private Instance instance;
	private Map typesToNames;

	private Visitor(Instance inst,
			Map nameMap) {
	    instance = inst;
	    typesToNames = nameMap;
	}

	public String structAdd(StructAdd n) {
	    return "\t(" + 
		Info.pyQuote(n.getFieldType().getFlattenedValue().trim())+"," +
		Info.pyQuote(n.getFieldName().getFlattenedValue().trim())+"),";
	}

	public String query(Query n) {
	    if(n.getKind() == Query.QUERY) {
		return "add_query_to_inst(" +
		    Info.pyQuote(instance.getFullyQualifiedName()) + "," +
		    Info.pyQuote(n.getName()) + "," +
		    Info.pyQuote(n.getReturnType().getFlattenedValue().trim()) + "," +
		    Info.pyQuote(n.getArguments().getFlattenedValue().trim()) + ")";
	    } else if(n.getKind() == Query.METHOD) {
		return "add_method_to_inst(" +
		    Info.pyQuote(instance.getFullyQualifiedName()) + "," +
		    Info.pyQuote(n.getName()) + "," +
		    Info.pyQuote(n.getReturnType().getFlattenedValue().trim()) + "," +
		    Info.pyQuote(n.getArguments().getFlattenedValue().trim()) + "," +
		    Info.boolToString(n.getLocked()) + ")";
	    }
	    return "";
	}

	public String parameter(Parameter n) {
	    /* We need to handle certain types specially */
	    String valStr;
	    String addParm = "";
	    int runtimed = 0;
	    int runtimeable = 0;

	    String cmdName = "";
	    String cmdDesc = "";

	    Type type = n.getType();
	    Value value = n.getValue();

	    if(n.getType() instanceof RuntimeParmType) {
		runtimeable = 1;
		type = ((RuntimeParmType)type).getParmType();
		runtimed = ((RuntimeParmValue)value).getRuntimed() ? 1 : 0;
		if(runtimed == 1) {
		    cmdName = ((RuntimeParmValue)value).getCommandLineName();
		    cmdDesc = ((RuntimeParmValue)value).getCommandLineDesc();
		}
		value = ((RuntimeParmValue)value).getDefaultValue();
	    } 

	    String typeName = (String)typesToNames.get(type);
	    if(typeName == null) { 
		try {
		    typeName = type.getBackendType(typesToNames);
		} catch(RuntimeTypeException e) {
		    String fqn = instance == null ? "" : 
			instance.getFullyQualifiedName();
		    
		    System.err.println("Parameter " + n.getName() + 
				       " has no backend type on instance " + 
				       fqn);
		    throw e;
		}
	    }
	    
	    if(type instanceof BoolType) {
		if(((BoolValue)value).getValue())
		    valStr = "1";
		else
		    valStr = "0";
	    } else if(type instanceof LiteralType) {
		valStr = value.toString();
		/*
		if(valStr.startsWith("\""))
		    valStr = "\\" + valStr;
		if(valStr.endsWith("\""))
		    valStr = valStr.substring(0,valStr.length()-1) + "\\\"";
		*/
		valStr = "r\"\"\"" + valStr + "\"\"\"";
	    } else if(type instanceof StringType) {
		valStr = "'''" + value.toString() + "'''";
	    } else if(type instanceof EnumType) {
		valStr = "'LSEut_enumref(" + typeName + "," +
		    value.toString() + ")'";
	    } else if (type instanceof InstanceRefType) {
		valStr = "'" + value.toString() + "'";
	    } else if (type instanceof DomainInstanceRefType) {
		DomainInstanceRefValue v = 
		    (DomainInstanceRefValue)value;
		valStr = "'" + v.getQualifiedName() + "'";
	    } else if (type instanceof ExternalType) {
		valStr = "'''" + value.toString() + "'''";
	    } else {
		valStr = value.toString();
	    }
	    
	    /* Should we be checking for types not supported by the
	       backend? */
	    
	    String fqn = instance == null ? "" : 
		instance.getFullyQualifiedName();

	    addParm += "add_parm_to_inst('" + fqn + "','" + n.getName() + 
		"'," + 
		valStr + 
		",'" + typeName + "', " + 
		runtimeable + ", " +
		runtimed + ", " + 
		pyQuote(cmdName) + ", " + 
		pyQuote(cmdDesc) + ")";
	    return addParm;
	}

	public String event(Event n) {
	    String name;
	    if(instance == null)
		name = "";
	    else
		name = instance.getFullyQualifiedName();

	    return "add_event_to_inst(" +
		pyQuote(name) + "," +
		pyQuote(n.getName()) + "," +
		pyQuote(n.getDataItemNames()) + "," +
		pyQuote(n.getDataItemTypes()) + ")";
	}

	private String endpointString(Port p, int i) {
	    return p.getInstance().getFullyQualifiedName() + ":" + 
		p.getName() + "[" + Integer.toString(i) + "]";
	}

	public String connection(Connection n) {
	    return "connect_ports(" + 
		Info.pyQuote(endpointString(n.getSourcePort(), 
					    n.getSourceInstance())) + "," +
		Info.pyQuote(endpointString(n.getDestPort(),
					    n.getDestInstance())) + ")";
	}

	public String codePoint(CodePoint n) {
	    String point, defaultPoint;

	    Value value = n.getValue();
	    Value defaultValue = n.getDefaultValue();
	    Type type = n.getType();
	    String name = n.getName();

	    String valueStr = null;
	    String defaultValueStr = null;
	    if(value != null) {
		StringValue cv = (StringValue)value;
		valueStr = cv.getFlattenedValue();
	    } else {
		valueStr = "";
	    }
	    if(defaultValue != null) {
		StringValue cv = (StringValue)defaultValue;
		defaultValueStr = cv.getFlattenedValue();
	    } else {
		defaultValueStr = "";
	    }

	    if (type instanceof UserPointType) {

		return "add_codepoint_to_inst(" +
		    Info.pyQuote(instance.getFullyQualifiedName()) + "," +
		    Info.pyQuote(name) + "," +
		    "LSE_PointUser,\n" +
		    Info.pyQuote(valueStr) + ",\n" +
		    Info.pyQuote(defaultValueStr) + ",\n" +
		    Info.pyQuote(((UserPointType)type).getReturnType().
				 getFlattenedValue()) + "," +
		    Info.pyQuote(((UserPointType)type).getArguments().
				 getFlattenedValue()) + "," +
		    Info.boolToString(n.getMakeMethod()) +
		    ")";

	    } else {

		return "add_codepoint_to_inst(" +
		    Info.pyQuote(instance.getFullyQualifiedName()) + "," +
		    Info.pyQuote(name) + "," +
		    "LSE_PointControl,\n" + 
		    Info.pyQuote(valueStr) + ",\n" +
		    Info.pyQuote(defaultValueStr) + 
		    ")";

	    }
	}

        private String getBackendType(Type type) {
	    String name = (String)typesToNames.get(type);
	    if(name == null)
		name = type.getBackendType(typesToNames);
	    return name;
        }

	public String port(Port n) {
	    return "add_port_to_inst(" +
		Info.pyQuote(instance.getFullyQualifiedName()) + "," +
		Info.pyQuote(n.getName()) + "," +
		n.getWidth() + "," + /* Width */
		Info.pyQuote(n.getDirection() == Port.INPUT ? 
			     "input" : "output") + "," +
		Info.pyQuote(getBackendType(n.getType())) + "," + 
		Info.boolToString(n.getIndependent()) + "," +
		Info.boolToString(n.getHandler()) + ")";
	}

	public String runtimeVar(RuntimeVar v) {
	    String name = (String)typesToNames.get(v.getType());
	    if(name == null) 
		name = v.getType().getBackendType(typesToNames);
	    return "add_var(" + 
		Info.pyQuote(v.getUniqueName()) + "," +
		Info.pyQuote(v.getUserName()) + "," +
		Info.pyQuote(name) + ")";
	}

	public String domainInstance(DomainInstance d) {
	    DomainInstanceRefValue dirv = d.getDomainInstanceRefValue();
	    return "add_domain_instance(" + 
		Info.pyQuote(dirv.getClassName()) + "," +
		Info.pyQuote(dirv.getBuildArgs()) + "," +
		Info.pyQuote(dirv.getQualifiedName()) + "," +
		Info.pyQuote(dirv.getRunArgs()) + ")";
	}

	public String domainSearchPathEntry(DomainSearchPathEntry spe) {
	    DomainInstanceRefValue dirv = spe.getDomainInstance();
	    return "(" + 
		Info.pyQuote(spe.getDomainClass()) + "," +
		Info.pyQuote(dirv.getQualifiedName()) + ")";
	}
    }
    
    public Info(String infoDir) 
	throws IOException {
	File infoDirectory = new File(infoDir);
	File f; 

	infoDirectory.mkdirs();

	f = new File(infoDirectory, "SIM_instance_info.py");
	instanceInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_parm_info.py");
	parmInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_codepoint_info.py");
	codePointInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_query_info.py");
	queryInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_struct_info.py");
	structInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_port_info.py");
	portInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_event_info.py");
	eventInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_type_info.py");
	typeInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_var_info.py");
	varInfo = new PrintWriter(new FileOutputStream(f));

	f = new File(infoDirectory, "SIM_domain_info.py");
	domainInfo = new PrintWriter(new FileOutputStream(f));
    }

    public void close() {
	instanceInfo.close();
	parmInfo.close();
	codePointInfo.close();
	queryInfo.close();
	structInfo.close();
	portInfo.close();
	eventInfo.close();
	typeInfo.close();
	varInfo.close();
	domainInfo.close();
    }
    
    public void printInstances(Collection instances, 
			       Map typesToNames) {
	Iterator i = instances.iterator();
	while(i.hasNext()) {
	    Instance inst = (Instance)i.next();
	    if(inst.isLeaf()) {
		instanceInfo.println(inst);
		Visitor v = new Visitor(inst, typesToNames);
		printCollection(parmInfo, v, inst.getParameters());
		printCollection(queryInfo, v, inst.getQueries());
		printCollection(eventInfo, v, inst.getEvents());
		printPorts(v, inst.getPorts());
		printCodePoints(inst, typesToNames);
		printStructAdds(inst, typesToNames);
		printCollectors(inst.getCollectors());
		printExportedTypes(inst, typesToNames);
		printDomainSearchPath(inst,v);
	    }

	    if(!inst.isLeaf()) {
		/* Output a mapping between port instances on this
		 * module and port instances on leafs */
		printHierarchicalPortAliases(inst);

		/* need to get runtime parameters at intermediate levels
		 * out there if those levels did not put out their
		 * parameters already due to being adapters
		 */
		if (!Adapter.needsAdapter(inst)) {
		    Visitor v = new Visitor(inst, typesToNames);
		    printRuntimeParms(parmInfo, v, inst.getParameters());
		}

		printInstances(inst.getSubInstances(), typesToNames);
	    }
	}
    }

    private void printDomainSearchPath(Visitor v, String instName,
				       List local, List hier) {
	String quotedName = (instName == null) ? "None" : 
	    Info.pyQuote(instName);
	domainInfo.print("add_domain_searchpath(" +  
			 quotedName + "," +
			 "[");
	Iterator i = local.iterator();
	boolean first = true;
	while(i.hasNext()) {
	    if(!first) domainInfo.print(",");
	    if(first) first = false;
	    printItem(domainInfo, v, (InfoEntry)i.next());
	}

	domainInfo.print("], [");
	i = hier.iterator();
	first = true;
	while(i.hasNext()) {
	    if(!first) domainInfo.print(",");
	    if(first) first = false;
	    printItem(domainInfo, v, (InfoEntry)i.next());
	}
	domainInfo.print("])\n");
    }
    private void printDomainSearchPath(Instance inst, Visitor v) {
	printDomainSearchPath(v, inst.getFullyQualifiedName(),
			      inst.getLocalDomainSearchPath(),
			      inst.getHierDomainSearchPath());
    }
    
    public void printDomainSearchPath(List local, List hier) {
	Visitor v = new Visitor(null, null);
	printDomainSearchPath(v, null, local, hier);
    }

    private void printHierarchicalPortAliases(Instance inst) {
	Iterator ports = inst.getPorts().iterator();
	while(ports.hasNext()) {
	    Port port = (Port)ports.next();
	    portInfo.println("alias_ports(" + 
			     pyQuote(inst.getFullyQualifiedName() + ":" +
				     port.getName()) + "," + 
			     Info.pyQuote(port.getDirection() == Port.INPUT ? 
					  "input" : "output") + 
			     ", [ ");

	    for(int instno = 0; instno < port.getWidth(); instno++) {
		Connection internal, external;
		try {
		    internal = port.getInternalConnectionAt(instno);
		} catch(PortIndexException e) {
		    internal = null;
		}

		try {
		    external = port.getConnectionAt(instno);
		} catch(PortIndexException e) {
		    external = null;
		}

		if(internal == null && external == null) {
		    /* No connection => no mapping */
		    portInfo.println("\tNone,");
		    continue;
		}

		Port mappedPort;
		int mappedInstno;

		/* Give preference to the source of the data as the
		 * mapped port */
		if(port.getDirection() == Port.INPUT) {
		    if(external != null) {
			mappedPort = external.getUltimateSourcePort();
			mappedInstno = external.getUltimateSourceInstance();

			/* If the external connection lead nowhere,
			 * follow the connection internally */
			if(mappedPort == null) {
			    if(internal == null) {
				/* This shouldn't be able to happen.  This
				 * means that an external connection was made,
				 * with no corresponding internal
				 * connection */
				throw new RuntimeTypeException("Port mapping discovered an " + 
							       "external connection with no " + 
							       "internal connection on  instance " + 
							       inst.getFullyQualifiedName() + 
							       " on port " + port + "[" + instno + "]");
			    }
			    mappedPort = internal.getUltimateDestPort();
			    mappedInstno = internal.getUltimateDestInstance();
			}
		    } else {
			mappedPort = internal.getUltimateDestPort();
			mappedInstno = internal.getUltimateDestInstance();
		    }
		} else {
		    if(internal != null) {
			mappedPort = internal.getUltimateSourcePort();
			mappedInstno = internal.getUltimateSourceInstance();
		    } else {
			/* This shouldn't be able to happen.  This
			 * means that an external connection was made,
			 * with no corresponding internal
			 * connection */
			throw new RuntimeTypeException("Port mapping discovered an " + 
						       "external connection with no " + 
						       "internal connection on  instance " + 
						       inst.getFullyQualifiedName() + 
						       " on port " + port + "[" + instno + "]");
		    }
		}
		
		Instance mappedInst = mappedPort.getInstance();
		portInfo.println(pyQuote(mappedInst.getFullyQualifiedName() 
					 + ":" +
					 mappedPort.getName() + 
					 "[" + mappedInstno + "]") + ",");

	    }
	    portInfo.println("])");
	}
    }

    private void printExportedTypes(Instance inst, Map typesToNames) {
	Map exportedTypes = inst.getExportedTypes();
	Iterator i = exportedTypes.entrySet().iterator();
	while(i.hasNext()) {
	    Map.Entry entry = (Map.Entry)i.next();
	    Type type = (Type)entry.getValue();
	    String typeName;

	    if(typesToNames.containsKey(type))
		typeName = (String)typesToNames.get(type);
	    else
		typeName = type.getBackendType(typesToNames);

	    typeInfo.println("add_type_mapping_to_inst(" + 
			     pySingleQuote(inst.getFullyQualifiedName()) + 
			     ", " + pySingleQuote((String)entry.getKey()) +  
			     ", " + pyQuote(typeName) + ")");
	}
    }

    private void printItem(PrintWriter w, Visitor v, InfoEntry e) {
	w.println(e.infoAccept(v));
    }

    private void printCollection(PrintWriter w, Visitor v, Collection c) {
	Iterator i = c.iterator();
	while(i.hasNext()) {
	    InfoEntry e = (InfoEntry)i.next();
	    printItem(w,v,e);
	}
    }

    private void printRuntimeParms(PrintWriter w, Visitor v, Collection c) {
	Iterator i = c.iterator();
	while(i.hasNext()) {
	    InfoEntry e = (InfoEntry)i.next();
	    Parameter p = (Parameter)e;
	    Type t = p.getType();

	    if (t instanceof RuntimeParmType) {
		RuntimeParmValue val = (RuntimeParmValue)p.getValue();
		if (val.getRuntimed()) {
		    printItem(w,v,e);
		}
	    }
	}
    }

    public void printTypes(LinkedHashMap types) {
	class DependenceClosure extends TransitiveClosure {
	    public Set getConnectedNodes(Object node) {
		Type type = (Type)node;
		return type.dependsOn();
	    }
	}

	DependenceClosure dependenceClosure = new DependenceClosure();
	Set sortTypes = dependenceClosure.closureFromAll(types.keySet());

	/* Topologically sort types according to there dependences */
	Stack dfsTypes = new Stack();
	Stack dfsIterators = new Stack();
	Liberty.util.Queue typeOrder = new Liberty.util.Queue();
	boolean leaf;

	while(sortTypes.size() > 0) {
	    Iterator i = sortTypes.iterator();
	    Type type;
	    Iterator successors;

	    // Initialize the stacks (use null as sentinels)
	    dfsTypes.push(null);
	    dfsIterators.push(null);

	    // Set up the seed node to do DFS
	    type = (Type)i.next();
	    successors = type.dependsOn().iterator();
	    sortTypes.remove(type);
	    //System.out.println("START" + type);
	    
	    // Do DFS iteratively
	    while(type != null) {
		dfsTypes.push(type);
		dfsIterators.push(successors);

		leaf = true;
		while(successors.hasNext()) {
		    // Move Forward
		    type = (Type)successors.next();
		    if(sortTypes.contains(type)) {
			successors = type.dependsOn().iterator();
			sortTypes.remove(type);
			leaf = false;
			//System.out.println("START" + type);
			break;
		    }
		}

		if(leaf) {
		    // The current node (top of the stack is done)
		    type = (Type)dfsTypes.pop();
		    //System.out.println("STOP" + type);
		    dfsIterators.pop();
		    typeOrder.enqueue(type);

		    // Back track
		    type = (Type)dfsTypes.pop();
		    successors = (Iterator)dfsIterators.pop();
		}
	    }
	}
	
	while(!typeOrder.empty()) {
	    Type type;
	    type = (Type)typeOrder.dequeue();

	    if(types.containsKey(type)) {
		String name = (String)types.get(type);
		String backendType;
		try {
		    backendType = type.getBackendType(types);
		    typeInfo.println("add_type(" + 
				     pySingleQuote(name) + ", " +
				     pyQuote(backendType) +
				     ")");
		} catch(RuntimeTypeException e) {
		    /* We want to skip over types with no backend types.
		     * This is necessary for function types */
		}
	    }
	}
    }

    public void printParameters(Collection parameters, Map typesToNames) {
	printCollection(parmInfo, 
			new Visitor(null, typesToNames),
			parameters);
    }

    public void printDomainInstances(Collection dinstances) {
	printCollection(domainInfo, new Visitor(null, null), dinstances);
    }

    private void printStructAdds(Instance inst, Map typesToNames) {
	Visitor v = new Visitor(inst, typesToNames);
	HashMap structAdds = inst.getStructAdds();
	Collection c = structAdds.entrySet();
	Iterator i = c.iterator();
	while(i.hasNext()) {
	    Map.Entry me = (Map.Entry)i.next();

	    structInfo.println("add_to_struct(" + 
			       Info.pyQuote((String)me.getKey()) + "," +
			       Info.pyQuote(inst.getFullyQualifiedName()) + 
			       ", [");
	    HashMap m = (HashMap)me.getValue();
	    printCollection(structInfo, v, m.values());
	    structInfo.println("])");
	}
    }

    public void printCodePoints(Instance inst, Map typesToNames) {
	StringValue fh = inst.getFuncHeader();
	if(fh != null) {
	    codePointInfo.println("add_funcheader_to_inst(" +
				  pyQuote(inst.getFullyQualifiedName()) + ","+
				  pyQuote(fh.getFlattenedValue()) + ")");
	}

	StringValue mb = inst.getModuleBody();
	if(mb != null) {
	    codePointInfo.println("add_body_to_inst(" +
				  pyQuote(inst.getFullyQualifiedName()) + ","+
				  pyQuote(mb.getFlattenedValue()) + ")");
	}

	StringValue et = inst.getExtension();
	if(et != null) {
	    codePointInfo.println("add_extension_to_inst(" +
				  pyQuote(inst.getFullyQualifiedName()) + ","+
				  pyQuote(et.getFlattenedValue()) + ")");
	}

	Visitor v = new Visitor(inst, typesToNames);
	/* Handle user points */
	Collection codePoints = inst.getCodePoints();
	printCollection(codePointInfo, v, codePoints);

	/* Handle control points */
	Collection ports = inst.getPorts();
	Iterator i = ports.iterator();
	while(i.hasNext()) {
	    Port port = (Port)i.next();
	    printItem(codePointInfo, v, port.getControl());
	}
    }

    private void printPorts(Visitor v, Collection ports) {
	Iterator i = ports.iterator();
	while(i.hasNext()) {
	    Port port = (Port)i.next();
	    printItem(portInfo, v, port);

	    /* Get connections and print them */
	    /* We only want to print each connection once,
	       so we will do it from the source side */
	    if(port.getDirection() == Port.OUTPUT) {
		Collection connections = port.getConnections();
		Iterator i2 = connections.iterator();
		while(i2.hasNext()) {
		    Connection conn = (Connection)i2.next();
		    Port src = conn.getSourcePort();
		    int srci = conn.getSourceInstance();

		    Port dest = conn.getUltimateDestPort();
		    int desti = conn.getUltimateDestInstance();

		    if(dest != null) {
			printItem(portInfo, v, new Connection(src, srci,
							      dest, desti));
		    }
		}
	    }
	}
    }

    public void printEvents(Collection events, Map typesToNames) {
	printCollection(eventInfo, new Visitor(null, typesToNames), events);
    }

    public void printCollectors(Collection collectors) {
	Iterator i = collectors.iterator();
	while(i.hasNext()) {
	    Collector c = (Collector)i.next();
	    eventInfo.println(c);
	}
    }

    public void printRuntimeVars(Collection runtimeVars, Map typesToNames) {
	printCollection(varInfo, new Visitor(null, typesToNames), runtimeVars);
    }

    private final static String openQuote = new Character('\037').toString();
    private final static String closeQuote = new Character('\036').toString();;

    public static String m4Quote(String s) {
	return openQuote + s + closeQuote;
    }

    public static String pySingleQuote(String s) {
	return "\"" + s + "\"";
    }

    private final static String openRawTQuote = new String("r\"\"\"");
    private final static String closeRawTQuote = new String("\"\"\"");
    private final static Pattern tQPattern = Pattern.compile(closeRawTQuote);

    public static String pyQuote(String s) {
	// To be totally safe, we need to replace any triple-quotes in s with
	// closeRawTQuote + '"""' + openRawTQuote
	Matcher m = tQPattern.matcher(s);
	String fixedS = m.replaceAll(closeRawTQuote + " + '" + closeRawTQuote +
				     "' + " + openRawTQuote);
	return openRawTQuote + fixedS + closeRawTQuote;
    }

    public static String boolToString(boolean b) {
	return b ? "1" : "0";
    }
}

