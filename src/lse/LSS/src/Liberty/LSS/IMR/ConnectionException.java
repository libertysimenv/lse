package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class ConnectionException extends LSSException
{
    boolean fatal;

    public ConnectionException() {
	super();
	fatal = false;
    }

    public ConnectionException(String m) {
	super(m);
	fatal = false;
    }

    public ConnectionException(String m, boolean b) {
	super(m);
	fatal = b;
    }

    public boolean isFatal() {
	return fatal;
    }
}
