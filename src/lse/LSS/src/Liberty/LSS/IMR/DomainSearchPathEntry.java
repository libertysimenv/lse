package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public class DomainSearchPathEntry implements InfoEntry {
    private String domainClass;
    private DomainInstanceRefValue domainInstance;

    public DomainSearchPathEntry(String className, 
				 DomainInstanceRefValue instance) {
	domainClass = className;
	domainInstance = instance;
    }

    public String getDomainClass() {
	return domainClass;
    }

    public DomainInstanceRefValue getDomainInstance() {
	return domainInstance;
    }

    public String infoAccept(InfoVisitor v) {
	return v.domainSearchPathEntry(this);
    }
}
