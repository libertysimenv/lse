package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.InstanceSpace;
import Liberty.LSS.AST.SymbolAlreadyDefinedException;
import java.util.*;

public class RuntimeVar implements InfoEntry
{
    private String uniqueName;
    private String userName;
    private Type type;

    public RuntimeVar(String userName, String uniqueName,
		      Type t) {
	this.userName = userName;
	this.uniqueName = uniqueName;
	type = t;
    }

    public String getUserName() {
	return userName;
    }
    
    public String getUniqueName() {
	return uniqueName;
    }

    public Type getType() {
	return type;
    }

    public String infoAccept(InfoVisitor v) {
	return v.runtimeVar(this);
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {
	type = type.substitute(typeVariableContext);
	if(type.isPolymorphic()) {
	    throw new TypeInferenceException("Type " + type + 
					     " is not fully constrained");
	}
	try {
	    type.getBackendType();
	} catch(RuntimeTypeException e) {
	    throw new TypeInferenceException("Type "  + type + 
					     " cannot be used" +
					     " on a runtime variable");
	}
    }
}
