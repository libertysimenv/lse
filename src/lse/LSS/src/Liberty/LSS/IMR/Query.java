package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.InstanceSpace;
import java.util.*;

public class Query implements InfoEntry
{
    public static final int QUERY = 1;
    public static final int METHOD = 2;

    private String name;
    private boolean locked;
    private int kind;
    private StringValue arguments, returnType;

    public Query(String n, int k, boolean l,
		 StringValue a, StringValue r) {
	name = n;
	kind = k;
	locked = l;
	arguments = a;
	returnType = r;
    }

    public String getName() {
	return name;
    }

    public StringValue getArguments() {
	return arguments;
    }

    public StringValue getReturnType() {
	return returnType;
    }

    public boolean getLocked() {
	return locked;
    }

    public int getKind() {
	return kind;
    }

    public String toString() {
	String lock = locked ? "locked" :
	    "";
	String kindStr = (kind == QUERY) ? "query" : "method";

	return lock + " " + kindStr + "(" + 
	    arguments + " => " + 
	    returnType + ")";
    }

    public String infoAccept(InfoVisitor v) {
	return v.query(this);
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {

	arguments = ConnectionManagement.substituteString(arguments, 
							  typeVariableContext, 
							  space);
	returnType = ConnectionManagement.substituteString(returnType, 
							   typeVariableContext,
							   space);
    }
}
