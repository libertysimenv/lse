package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.InstanceSpace;
import java.util.*;

public class Instance
{
    private Instance parent;
    private String name;
    private String moduleName;
    private CodeObtainable moduleCode;
    private CodeObtainable instantiationLocation;

    private HashMap instances;
    private HashMap parameters;
    private HashMap codePoints;
    private HashMap queries;
    private HashMap structAdds;
    private HashMap ports;
    private HashMap events;
    private HashMap exportedTypes;
    private Vector collectors;
    private Vector runtimeVars;
    private StringValue funcHeader;
    private StringValue moduleBody;
    private StringValue extension;
    private List localDomainSearchPath;
    private List hierDomainSearchPath;

    private Set constraints;

    private boolean phase,phaseStart,
	phaseEnd, strict, reactive;
    private String portDataFlow;

    private String tarball;

    private Instance() {
	parameters = new HashMap();
	codePoints = new HashMap();
	queries = new HashMap();
	structAdds = new HashMap();
	ports = new HashMap();
	events = new HashMap();
	funcHeader = null;
	moduleBody = null;
	extension = null;
	collectors = new Vector();
	exportedTypes = new HashMap();
	runtimeVars = new Vector();
	constraints = new HashSet();
    }

    public Instance(Instance pt, String n, String m, CodeObtainable mc, 
		    CodeObtainable il, Set cons) {
	this();
	parent = pt;
	name = n;
	moduleName = m;
	moduleCode = mc;
	instantiationLocation = il;
	tarball = null;
	instances = new HashMap();
	constraints.addAll(cons);
    }

    public Instance(Instance pt, String n, String m, Set cons) {
	this(pt, n, m, null, null, cons);
    }

    public Instance(Instance pt, String n, String m, CodeObtainable mc,
		    CodeObtainable il, 
		    String t, boolean ps, boolean p,
		    boolean pe, boolean r, boolean s,
		    String pdf, Set cons) {
	this();
	phase = p;
	phaseStart = ps;
	phaseEnd = pe;
	reactive = r;
	strict = s;
	portDataFlow = pdf;

	parent = pt;
	name = n;
	moduleName = m;
	moduleCode = mc;
	instantiationLocation = il;
	tarball = t;
	instances = null;
	constraints.addAll(cons);
    }

    public Instance(Instance pt, String n, String m,
		    String t, boolean ps, boolean p,
		    boolean pe, boolean r, boolean s,
		    String pdf, Set cons) {
	this(pt,n,m,null,null,t,ps,p,pe,r,s,pdf,cons);
    }


    public Instance getParent() {
	return parent;
    }

    public CodeObtainable getModuleCode() {
	return moduleCode;
    }

    public CodeObtainable getInstantiationLocation() {
	return instantiationLocation;
    }

    public boolean isLeaf() {
	if(instances == null)
	    return true;
	else
	    return false;
    }

    public String getFullyQualifiedName() {
	String parentName;

	if(parent != null)
	    parentName = parent.getFullyQualifiedName();
	else
	    parentName = null;

	if(parentName == null)
	    return name;
	else
	    return parentName + "." + name;
    }

    public String getName() {
	return name;
    }

    public String getModuleName() {
	return moduleName;
    }

    public void addSubInstance(Instance i) {
	instances.put(i.getName(), i);
    }

    public Instance getSubInstance(String name) {
	return (Instance)instances.get(name);
    }
    
    public Collection getSubInstances() {
	return instances.values();
    }

    public HashMap getSubInstanceMap() {
	return instances;
    }

    public void addParameter(Parameter p) {
	parameters.put(p.getName(), p);
    }

    public Parameter getParameter(String name) {
        return (Parameter)parameters.get(name);
    }

    public Collection getParameters() {
	return parameters.values();
    }

    public void addCodePoint(CodePoint c) {
	codePoints.put(c.getName(), c);
    }

    public Collection getCodePoints() {
	return codePoints.values();
    }

    public CodePoint getCodePoint(String name) {
	return (CodePoint)codePoints.get(name);
    }

    public void addQuery(Query q) {
	queries.put(q.getName(), q);
    }

    public Collection getQueries() {
	return queries.values();
    }

    public void addPort(Port p) {
	ports.put(p.getName(), p);
    }

    public Port getPort(String name) {
	return (Port)ports.get(name);
    }

    public Collection getPorts() {
	return ports.values();
    }

    public void addEvent(Event e) {
	events.put(e.getName(), e);
    }

    public Event getEvent(String name) {
	return (Event)events.get(name);
    }

    public Collection getEvents() {
	return events.values();
    }

    public void addStructAdd(StructAdd a) {
	HashMap ds;

	if(!structAdds.containsKey(a.getDataStructure().getFlattenedValue())) {
	    ds = new HashMap();
	    structAdds.put(a.getDataStructure().getFlattenedValue(), ds);
	} else {
	    ds = 
		(HashMap)structAdds.get(a.getDataStructure().
					getFlattenedValue());
	}
	
	ds.put(a.getFieldName().getValue(), a);
    }

    public HashMap getStructAdds() {
	return structAdds;
    }

    public String getTarball()
    {
	return tarball;
    }

    public boolean getPhase()
    {
	return phase;
    }

    public boolean getPhaseStart()
    {
	return phaseStart;
    }

    public boolean getPhaseEnd()
    {
	return phaseEnd;
    }

    public boolean getReactive()
    {
	return reactive;
    }

    public boolean getStrict()
    {
	return strict;
    }

    public void setFuncHeader(StringValue v) {
	funcHeader = v;
    }

    public StringValue getFuncHeader() {
	return funcHeader;
    }

    public void setModuleBody(StringValue v) {
	moduleBody = v;
    }

    public StringValue getModuleBody() {
	return moduleBody;
    }

    public void setExtension(StringValue v) {
	extension = v;
    }

    public StringValue getExtension() {
	return extension;
    }

    public void addCollector(Collector c) {
	collectors.add(c);
    }

    public Collection getCollectors() {
	return collectors;
    }

    public void addExportedType(String name, Type type) {
	exportedTypes.put(name, type);
    }

    public Map getExportedTypes() {
	return exportedTypes;
    }

    public void setPortDataFlow(String pdf) {
	portDataFlow = pdf;
    }

    public void addRuntimeVar(RuntimeVar v) {
	runtimeVars.add(v);
    }

    public Collection getRuntimeVars() {
	return runtimeVars;
    }

    public void setLocalDomainSearchPath(List l) {
	localDomainSearchPath = new Vector(l);
    }

    public void setHierDomainSearchPath(List l) {
	hierDomainSearchPath = new Vector(l);
    }

    public List getLocalDomainSearchPath() {
	return localDomainSearchPath;
    }

    public List getHierDomainSearchPath() {
	return hierDomainSearchPath;
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {
	Iterator i;

	if(!isLeaf()) {
	    i = instances.values().iterator();
	    while(i.hasNext()) {
		Instance n = (Instance)i.next();
		n.substitute(typeVariableContext, space);
	    }
	}

	i = codePoints.values().iterator();
	while(i.hasNext()) {
	    CodePoint c = (CodePoint)i.next();
	    c.substitute(typeVariableContext, space);
	}

	i = parameters.values().iterator();
	while(i.hasNext()) {
	    Parameter p = (Parameter)i.next();
	    p.substitute(typeVariableContext, space);
	}

	i = queries.values().iterator();
	while(i.hasNext()) {
	    Query q = (Query)i.next();
	    q.substitute(typeVariableContext, space);
	}

	i = structAdds.values().iterator();
	while(i.hasNext()) {
	    Map m = (Map)i.next();
	    Iterator j = m.values().iterator();
	    while(j.hasNext()) {
		StructAdd s = (StructAdd)j.next();
		s.substitute(typeVariableContext, space);
	    }
	}

	i = ports.values().iterator();
	while(i.hasNext()) {
	    Port p = (Port)i.next();
	    p.substitute(typeVariableContext, space);
	}

	i = events.values().iterator();
	while(i.hasNext()) {
	    Event e = (Event)i.next();
	    e.substitute(typeVariableContext, space);
	}

	i = exportedTypes.entrySet().iterator();
	Map m = new HashMap();
	while(i.hasNext()) {
	    Map.Entry entry = (Map.Entry)i.next();
	    Type t = (Type)entry.getValue();
	    t = t.substitute(typeVariableContext);
	    i.remove();

	    if(t.isPolymorphic()) {
		throw new TypeInferenceException(t + 
						 " is still does not have a backend type " +
						 "and therefore cannot be exported by instance " + 
						 getFullyQualifiedName());
	    }
	    
	    try {
		t.getBackendType();
	    } catch(RuntimeTypeException e) {
		throw new TypeInferenceException(t + 
						 " does not have a backend type " +
						 "and therefore cannot be exported by instance " +
						 getFullyQualifiedName());
	    }
	    m.put(entry.getKey(), t);
	}
	exportedTypes.putAll(m);

	i = collectors.iterator();
	while(i.hasNext()) {
	    Collector c = (Collector)i.next();
	    c.substitute(typeVariableContext, space);
	}

	i = runtimeVars.iterator();
	while(i.hasNext()) {
	    RuntimeVar r = (RuntimeVar)i.next();
	    r.substitute(typeVariableContext, space);
	}

	if(funcHeader != null)
	    funcHeader = 
		ConnectionManagement.substituteString(funcHeader, 
						      typeVariableContext, 
						      space);

	if(moduleBody != null)
	    moduleBody = 
		ConnectionManagement.substituteString(moduleBody, 
						      typeVariableContext, 
						      space);
	if(extension != null)
	    extension = 
		ConnectionManagement.substituteString(extension, 
						      typeVariableContext, 
						      space);
    }

    public Set getConstraints() {
	return constraints;
    }

    public String toString() {
	String add_inst = "add_inst(" + 
	    Info.pyQuote(getFullyQualifiedName()) + "," +
	    Info.pyQuote(getModuleName()) + "," + 
	    Info.boolToString(phaseStart) + "," +
	    Info.boolToString(phase) + "," +
	    Info.boolToString(phaseEnd) + "," +
	    Info.boolToString(strict) + "," +
	    Info.boolToString(reactive) + "," +
	    Info.pyQuote(tarball) + ")";
	String add_dep = "add_dep_annotation(" +
	    Info.pyQuote(getFullyQualifiedName()) + "," +
	    portDataFlow + ")";
	return add_inst + "\n" + add_dep;
    }
}
