package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import java.util.*;

public class Connection implements InfoEntry, ConstraintSource
{
    private Port src, dest;
    private int srci, desti;
    private Type constraint;
    
    public Connection(Port s, int si, Port d, int di) {
	src = s;
	dest = d;
	srci = si;
	desti = di;
	constraint = null;
    }

    private Connection(Port s, Port d, Type c) {
	src = s;
	dest = d;
	constraint = c;
    }

    public Port getSourcePort() {
	return src;
    }

    public Port getDestPort() {
	return dest;
    }

    public int getSourceInstance() {
	return srci;
    }

    public int getDestInstance() {
	return desti;
    }

    public Port getUltimateSourcePort() {
	Connection c = getUltimateSourceConnection();
	if(c != null)
	    return c.getSourcePort();
	else
	    return null;
    }

    public Port getUltimateDestPort() {
	Connection c = getUltimateDestConnection();
	if(c != null)
	    return c.getDestPort();
	else
	    return null;
    }

    public int getUltimateSourceInstance() {
	Connection c = getUltimateSourceConnection();
	if(c != null)
	    return c.getSourceInstance();
	else
	    return -1;
    }

    public int getUltimateDestInstance() {
	Connection c = getUltimateDestConnection();
	if(c != null)
	    return c.getDestInstance();
	else
	    return -1;
    }

    public Type getConstraint() {
	return constraint;
    }

    public void checkCycles() 
	throws ConnectionException {
	Set visitedConnections = new LinkedHashSet();
	checkCycles(visitedConnections);
    }

    private void checkCycles(Set visitedConnections) 
	throws ConnectionException {
	if(dest.isLeaf())
	    return;
	else {
	    try {
		if(visitedConnections.contains(this)) {
		    visitedConnections.add(this);
		    throw new 
			ConnectionException("Error: A connection cycle " + 
					    "exists along the path " + 
					    visitedConnections,
					    true);
		}
		visitedConnections.add(this);

		Connection c;
		if(dest.getDirection() == Port.INPUT)
		    c = dest.getInternalConnectionAt(desti);
		else
		    c = dest.getConnectionAt(desti);
		if(c == null) return;
		c.checkCycles(visitedConnections);
	    } catch(PortIndexException e) {
		return;
	    }
	}
    }

    private Connection getUltimateSourceConnection() {
	if(src.isLeaf())
	    return this;
	else {
	    try {
		Connection c;
		if(src.getDirection() == Port.OUTPUT)
		    c = src.getInternalConnectionAt(srci);
		else
		    c = src.getConnectionAt(srci);
		if(c == null) return null;
		return c.getUltimateSourceConnection();
	    } catch(PortIndexException e) {
		return null;
	    }
	}
    }

    private Connection getUltimateDestConnection() {
	if(dest.isLeaf())
	    return this;
	else {
	    try {
		Connection c;
		if(dest.getDirection() == Port.INPUT)
		    c = dest.getInternalConnectionAt(desti);
		else
		    c = dest.getConnectionAt(desti);
		if(c == null) return null;
		return c.getUltimateDestConnection();
	    } catch(PortIndexException e) {
		return null;
	    }
	}
    }

    private static void verifyDirection(int sdir, int ddir)
	throws PortDirectionException {
	if(sdir != Port.OUTPUT || ddir != Port.INPUT)
	    throw new PortDirectionException("Port directions do not match");
    }

    private static int invertDirection(int dir) 
	throws PortDirectionException {
	if(dir == Port.INPUT) return Port.OUTPUT;
	if(dir == Port.OUTPUT) return Port.INPUT;
	throw new PortDirectionException("Unknown direction");
    }

    private boolean isInternal(int side, Port p) {
	if(p.getDirection() == side) return false;
	else return true;
    }

    public void disconnect() {
	if(isInternal(Port.OUTPUT, src))
	    src.removeInternalConnection(srci);
	else
	    src.removeConnection(srci);

	if(isInternal(Port.INPUT, dest))
	    dest.removeInternalConnection(desti);
	else
	    dest.removeConnection(desti);
    }

    public void reconnect() {
	if(isInternal(Port.OUTPUT, src))
	    src.addInternalConnectionNoCheck(this, srci);
	else
	    src.addConnectionNoCheck(this, srci);

	if(isInternal(Port.INPUT, dest))
	    dest.addInternalConnectionNoCheck(this, desti);
	else
	    dest.addConnectionNoCheck(this, desti);
    }

    public static Connection connect(Port s, int si, boolean sInternal,
				     Port d, int di, boolean dInternal,
				     Type t)
	throws PortDirectionException,
	       PortIndexException,
	       NotAHierarchicalInstanceException {
	Connection c = new Connection(s,d,t);
	int sdir = s.getDirection();
	int ddir = d.getDirection();

	if(sInternal) sdir = invertDirection(sdir);
	if(dInternal) ddir = invertDirection(ddir);
	verifyDirection(sdir,ddir);
	if(sInternal) {
	    if(si == -1)
		c.srci = s.addInternalConnection(c);
	    else
		c.srci = s.addInternalConnection(c, si);
	} else {
	    if(si == -1)
		c.srci = s.addConnection(c);
	    else
		c.srci = s.addConnection(c, si);
	}

	if(dInternal) {
	    if(di == -1)
		c.desti = d.addInternalConnection(c);
	    else
		c.desti = d.addInternalConnection(c, di);
	} else {
	    if(di == -1)
		c.desti = d.addConnection(c);
	    else
		c.desti = d.addConnection(c, di);
	}

	return c;
    }

    public String infoAccept(InfoVisitor v) {
	return v.connection(this);
    }

    public String toString() {
	return src.getInstance().getFullyQualifiedName() + "." +
	    src.getName() + "[" + srci + "]" + " -> " + 
	    dest.getInstance().getFullyQualifiedName() + "." +
	    dest.getName() + "[" + desti + "]";
    }

    public String getIdentifierString() {
	return "connection " + this.toString();
    }
}
