package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

public interface InfoEntry {
    public String infoAccept(InfoVisitor v);
}
