package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.InstanceSpace;
import java.util.*;

public class CodePoint extends Parameter
{
    protected Value defaultValue;
    protected boolean makeMethod;

    public CodePoint(String n, Type t, Value v, Value d, boolean e) {
	super(n,t,v, e);
	defaultValue = d;
	makeMethod = false;
    }

    public CodePoint(String n, Type t, Value v, Value d, 
		     boolean e, boolean m) {
	super(n,t,v, e);
	defaultValue = d;
	makeMethod = m;
    }

    public CodePoint(CodePoint c) {
	super(c);
	defaultValue = c.defaultValue;
    }

    public CodePoint makeMethodVersion() {
	if(!makeMethod) {
	    CodePoint c = new CodePoint(this);
	    c.makeMethod = true;
	    return c;
	} else {
	    return this;
	}
    }

    public String getName() {
	return name;
    }

    public Value getDefaultValue() {
	return defaultValue;
    }

    public boolean getMakeMethod() {
	return makeMethod;
    }

    public boolean isEmpty() {
	if((getValue() == null || 
	    ((StringValue)getValue()).getFlattenedValue().equals("")) &&
	   (defaultValue == null || 
	    ((StringValue)defaultValue).getFlattenedValue().equals("")))
	    return true;
	else
	    return false;
    }

    public String infoAccept(InfoVisitor v) {
	return v.codePoint(this);
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {
	super.substitute(typeVariableContext, space);
	defaultValue = ConnectionManagement.substitute(defaultValue, 
						       typeVariableContext, 
						       space);
    }
}
