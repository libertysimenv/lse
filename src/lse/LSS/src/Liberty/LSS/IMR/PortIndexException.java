package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class PortIndexException extends LSSRuntimeException
{
    public PortIndexException() {
	super();
    }

    public PortIndexException(String m) {
	super(m);
    }
}
