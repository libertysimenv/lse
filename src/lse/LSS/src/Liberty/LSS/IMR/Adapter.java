package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import java.io.*;
import java.util.*;

public class Adapter {
    public class PortPair {
	private String nameIn;
	private String nameOut;

	private PortPair(String in, String out) {
	    nameIn = in;
	    nameOut = out;
	}

	public String toString() {
	    return "LSS_port_pair(" +
		Info.m4Quote(nameIn) + "," +
		Info.m4Quote(nameOut) + ")m4_dnl\n";
	}
    }

    private Instance instance;
    private Vector portPairs;
    private File adapterDir;
    private File adapterFile;

    private Adapter(File a, Instance i) {
	String portDataFlow = "[ ('*','*', '0'),\n";

	adapterDir = a;
	adapterFile = new File(adapterDir, i.getFullyQualifiedName());
	adapterDir.mkdirs();
	
	instance = new Instance(i.getParent(), i.getName(), i.getModuleName(), 
				"+LSEfw_adapter.clm",
				/*"-lb-build-adapters " + "\"" +
				  adapterFile.getAbsoluteFile() + "\" ",*/
				false, false, false, true, false, null,
				i.getConstraints());
	instance.setFuncHeader(i.getFuncHeader());
	instance.setModuleBody(i.getModuleBody());
	instance.setExtension(i.getExtension());
	
	/* Copy code points */
	Iterator j = i.getCodePoints().iterator();
	while(j.hasNext()) {
	    CodePoint c = (CodePoint)j.next();
	    if(c.isExported()) {
		String name = c.getName();
		UserPointType ct = (UserPointType)c.getType();
		
		/* TODO: ensure this doesn't blow away an existing method */
		if(!name.equals("init") &&
		   !name.equals("start_of_timestep") &&
		   !name.equals("end_of_timestep") &&
		   !name.equals("finish")) {
		    instance.addCodePoint(c.makeMethodVersion());
		    instance.addQuery(new Query(name, Query.METHOD, false, 
						ct.getArguments(),
						ct.getReturnType()));
		} else {
		    instance.addCodePoint(c);
		}
	    }
	}

	/* Copy struct adds */
	j = i.getStructAdds().values().iterator();
	while(j.hasNext()) {
	    Iterator k = ((Map)j.next()).values().iterator();
	    while(k.hasNext())
		instance.addStructAdd((StructAdd)k.next());
	}

	/* Copy parameters */
	j = i.getParameters().iterator();
	while (j.hasNext()) {
	    Parameter p = (Parameter)j.next();
	    /* Don't copy parameters without values */
	    if(p.getValue() != null) 
		instance.addParameter(p);
	}

	/* Copy events */
	j = i.getEvents().iterator();
	while(j.hasNext()) {
	    instance.addEvent((Event)j.next());
	}

	/* Copy methods and queries */
	j = i.getQueries().iterator();
	while(j.hasNext()) {
	    instance.addQuery((Query)j.next());
	}

	/* Copy collectors */
	HashMap portCollectorMap = new HashMap();
	j = i.getCollectors().iterator();
	while(j.hasNext()) {
	    Collector c = (Collector)j.next();
	    instance.addCollector(c);

	    /* Build a map from port names(?) -> event names */
	    String eventName = c.getEvent().getName();
	    StringTokenizer eventTok = new StringTokenizer(eventName, ".");
	    String firstPart = eventTok.nextToken();
	    portCollectorMap.put(firstPart, eventName);
	}

	/* Copy runtime variables */
	j = i.getRuntimeVars().iterator();
	while(j.hasNext()) {
	    instance.addRuntimeVar((RuntimeVar)j.next());
	}

	/* Copy domain search path */
	instance.setLocalDomainSearchPath(i.getLocalDomainSearchPath());
	instance.setHierDomainSearchPath(i.getHierDomainSearchPath());

	/* Copy appropriate ports and connections */
	portPairs = new Vector();
	j = i.getPorts().iterator();
	while(j.hasNext()) {
	    Port p = (Port)j.next();
	    CodePoint control = p.getControl();
	    
	    /* See if this port needs to be copied */
	    boolean needCopy = !control.isEmpty();
	    if(portCollectorMap.containsKey(p.getName())) {
		String eventName = (String)portCollectorMap.get(p.getName());
		/* Is this a builtin port event? */
		if(eventName.equals(p.getName() + ".resolved") ||
		   eventName.equals(p.getName() + ".localresolved"))
		    needCopy = true;
	    }

	    /* If so, copy the port */
	    if(needCopy) {
		String nameIn = (p.getDirection() == Port.INPUT ? 
				 p.getName() : p.getName() + "_in");
		String nameOut = (p.getDirection() == Port.OUTPUT ?
				  p.getName() : p.getName() + "_out");
		portPairs.add(new PortPair(nameIn, nameOut));
		Port in = 
		    new Port(instance, nameIn, p.getWidth(),
			     Port.INPUT, p.getType(),
			     (p.getDirection() == Port.INPUT ?
			      (ControlPointValue)control.getValue() :
			      new ControlPointValue("")),
			     (p.getDirection() == Port.INPUT ?
			      (ControlPointValue)control.getDefaultValue() :
			      new ControlPointValue("")),
			     false, true);
		Port out = 
		    new Port(instance, nameOut, p.getWidth(),
			     Port.OUTPUT, p.getType(),
			     (p.getDirection() == Port.OUTPUT ?
			      (ControlPointValue)control.getValue() :
			      new ControlPointValue("")),
			     (p.getDirection() == Port.OUTPUT ?
			      (ControlPointValue)control.getDefaultValue() :
			      new ControlPointValue("")),
			     false, true);
		instance.addPort(in);
		instance.addPort(out);

		portDataFlow += "(" + Info.pySingleQuote(nameIn + ".data") + 
		    "," +
		    Info.pySingleQuote(nameOut + ".data") + "," +
		    Info.pySingleQuote("isporti == osporti") + "),\n";
		portDataFlow += "(" + Info.pySingleQuote(nameIn + ".en") + 
		    "," +
		    Info.pySingleQuote(nameOut + ".en") + "," +
		    Info.pySingleQuote("isporti == osporti") + "),\n";
		portDataFlow += "(" + Info.pySingleQuote(nameOut + ".ack") + 
		    "," +
		    Info.pySingleQuote(nameIn + ".ack") + "," +
		    Info.pySingleQuote("isporti == osporti") + "),\n";

		/* Redo connections */
		Iterator k = p.getConnections().iterator();
		while(k.hasNext()) {
		    Connection external = (Connection)k.next();
		    /* Verify that this connection has ultimate
		       leaf end points */
		    if(external.getUltimateSourcePort() == null || 
		       external.getUltimateDestPort() == null)
			continue;

		    if(p == external.getSourcePort()) {
			Port dest = external.getDestPort();
			int fari = external.getDestInstance();
			int neari = external.getSourceInstance();
			Connection c = new Connection(out, neari, dest, fari);
			external.disconnect();
			c.reconnect();

			Connection internal = p.getInternalConnectionAt(neari);
			Port src = internal.getSourcePort();
			fari = internal.getSourceInstance();
			c = new Connection(src, fari, in, neari);
			internal.disconnect();
			c.reconnect();
		    } else {
			Port src = external.getSourcePort();
			int fari = external.getSourceInstance();
			int neari = external.getDestInstance();
			Connection c = new Connection(src, fari, in, neari);
			external.disconnect();
			c.reconnect();

			Connection internal = p.getInternalConnectionAt(neari);
			Port dest = internal.getDestPort();
			fari = internal.getDestInstance();
			c = new Connection(out, neari, dest, fari);
			internal.disconnect();
			c.reconnect();
		    }
		}
	    }
	}
	portDataFlow += "]";
	instance.setPortDataFlow(portDataFlow);

	String scriptString = "";
	Iterator ports = portPairs.iterator();
	while(ports.hasNext())
	    scriptString += ports.next();

	LiteralType tp = new LiteralType();
	LiteralValue lv = new LiteralValue(scriptString);
	Liberty.LSS.IMR.Parameter p = 
	    new Liberty.LSS.IMR.Parameter("script_string", tp, lv, true);
	instance.addParameter(p);
    }

    public static Adapter build(File dir, Instance instance) {
	if(needsAdapter(instance)) 
	    return new Adapter(dir, instance);
	else return null;
    }

    public static boolean needsAdapter(Instance inst) {
	/* Leaves don't need adapters :) */
	if(inst.isLeaf()) return false;

	/* Here we check to see if any of our ports have control
	   functions or if we have any default code points filled
	   in */
	Iterator i = inst.getCodePoints().iterator();
	while(i.hasNext()) {
	    CodePoint cp = (CodePoint)i.next();
	    if(cp.isExported()) {
		return true;
	    }
	}

	i = inst.getPorts().iterator();
	while(i.hasNext()) {
	    Port p = (Port)i.next();
	    if(!p.getControl().isEmpty()) return true;
	}

	if(!inst.getEvents().isEmpty()) return true;

	if(!inst.getCollectors().isEmpty()) return true;

	if(!inst.getRuntimeVars().isEmpty()) return true;

	if(!inst.getQueries().isEmpty()) return true;

	return false;
    }


    public Instance getInstance() {
	return instance;
    }
    
    public void output() throws FileNotFoundException {
	PrintWriter w = 
	    new PrintWriter(new FileOutputStream(adapterFile));
	Iterator ports = portPairs.iterator();
	while(ports.hasNext())
	    w.println(ports.next());

	w.close();
    }
    
    public static void output(Collection c) throws FileNotFoundException {
	Iterator i = c.iterator();
	while(i.hasNext()) {
	    Adapter a = (Adapter)i.next();
	    a.output();
	}
    }
}
