package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.InstanceSpace;
import java.util.*;

public class Collector
{
    private Instance instance;
    private Event event;
    private StringValue decl, init, record, report, header;

    public Collector(Event e, Instance i, StringValue h,
		     StringValue d, StringValue in,
		     StringValue rec, StringValue rep) {
	event = e;
	instance = i;
	header = ((h == null) ? new StringValue("") : h);
	decl = ((d == null) ? new StringValue("") : d);
	init = ((in == null) ? new StringValue("") : in);
	record = ((rec == null) ? new StringValue("") : rec);
	report = ((rep == null) ? new StringValue("") : rep);
    }

    public Event getEvent() {
	return event;
    }

    public String toString() {
	String iname;
	if(instance == null)
	    iname = "";
	else
	    iname = instance.getFullyQualifiedName();

	String actualrecord = record.getFlattenedValue();

	if(System.getProperty("Liberty.LSS.fraternite").equals("yes")) {
	    /* Here is a hack for Fraternite, to make the resolved
	     * collectors have Fraternite backend syntax */
	    if(event.getName().endsWith(".resolved") ||
	       event.getName().endsWith(".localresolved")) {
		
		String portName = event.getName().substring(0, event.getName().indexOf("."));
		actualrecord = "FraterniteKludge::PortKludge<LSE_port_type(" + portName + ")> " +
		    portName + "(datap, status, prevstatus); { " + record.getFlattenedValue() + "}";
	    }
	}

	return "add_collector_to_inst(" +
	    Info.pyQuote(iname) + "," +
	    Info.pyQuote(event.getName()) + "," +
	    Info.pyQuote(header.getFlattenedValue()) + "," +
	    Info.pyQuote(decl.getFlattenedValue()) + "," +
	    Info.pyQuote(init.getFlattenedValue()) + "," +
	    Info.pyQuote(actualrecord) + "," +
	    Info.pyQuote(report.getFlattenedValue()) + ")";
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {
	header = ConnectionManagement.substituteString(header, typeVariableContext, 
						     space);
	decl = ConnectionManagement.substituteString(decl, typeVariableContext, 
						     space);
	init = ConnectionManagement.substituteString(init, typeVariableContext, 
						     space);
	record = ConnectionManagement.substituteString(record, typeVariableContext, 
						       space);
	report = ConnectionManagement.substituteString(report, typeVariableContext, 
						       space);
    }
}
