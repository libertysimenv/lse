package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;

import Liberty.LSS.*;

public class PortDirectionException extends LSSRuntimeException
{
    public PortDirectionException() {
	super();
    }

    public PortDirectionException(String m) {
	super(m);
    }
}
