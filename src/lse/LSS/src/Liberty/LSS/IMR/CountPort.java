package Liberty.LSS.IMR;

import java.util.*;

interface InstanceVisitor {
    public boolean instance(Instance inst);
    public boolean port(Port port);
    public boolean internalConnection(Connection conn);
    public boolean externalConnection(Connection conn);
}

class InstanceTraversal
{
    protected InstanceVisitor visitor;

    public InstanceTraversal(InstanceVisitor v) {
	visitor = v;
    }
    
    public void visit(Instance inst) {
	if(visitor.instance(inst)) {
	    visitPorts(inst.getPorts());
	    if(!inst.isLeaf()) {
		visitInstances(inst.getSubInstances());
	    }
	}
    }

    public void visit(Port port) {
	if(visitor.port(port)) {
	    visitExternalConnections(port.getConnections());
	    if(!port.isLeaf()) {
		visitInternalConnections(port.getInternalConnections());
	    }
	}
    }

    public void visitInternalConnection(Connection conn) {
	if(visitor.internalConnection(conn)) {
	}
    }

    public void visitExternalConnection(Connection conn) {
	if(visitor.externalConnection(conn)) {
	}
    }

    public void visitInstances(Collection insts) {
	Iterator i = insts.iterator();
	while(i.hasNext()) {
	    Instance inst = (Instance)i.next();
	    visit(inst);
	}
    }

    public void visitPorts(Collection ports) {
	Iterator i = ports.iterator();
	while(i.hasNext()) {
	    Port port = (Port)i.next();
	    visit(port);
	}
    }

    public void visitExternalConnections(Collection conns) {
	Iterator i = conns.iterator();
	while(i.hasNext()) {
	    Connection conn = (Connection)i.next();
	    visitExternalConnection(conn);
	}
    }

    public void visitInternalConnections(Collection conns) {
	Iterator i = conns.iterator();
	while(i.hasNext()) {
	    Connection conn = (Connection)i.next();
	    visitInternalConnection(conn);
	}
    }
}

public class CountPort implements InstanceVisitor {
    int ports = 0;
    int connections = 0;
    Map moduleCounts = new HashMap();
    Map moduleTypes = new HashMap();

    public static void go(Collection insts) {
	CountPort counter = new CountPort();
	InstanceTraversal it = new InstanceTraversal(counter);
	it.visitInstances(insts);
	System.err.println("----------------------------");
	System.err.println("Ports: " + counter.ports);
	System.err.println("Connections: " + counter.connections);
	
	Iterator i = counter.moduleCounts.keySet().iterator();
	while(i.hasNext()) {
	    String key = (String)i.next();
	    System.err.println(key + ": " + counter.moduleCounts.get(key) +
			       " " +
			       (((Boolean)counter.moduleTypes.get(key)).
			       booleanValue() ? "leaf" : "hierarchical"));
	    
	}
	System.err.println("----------------------------");
    }

    public boolean instance(Instance inst) {
	Boolean b = (Boolean)moduleTypes.get(inst.getModuleName());
	if(b == null) {
	    b = new Boolean(inst.isLeaf());
	    moduleTypes.put(inst.getModuleName(), b);
	} else {
	    if((!b.booleanValue() && inst.isLeaf()) ||
	       (b.booleanValue() && !inst.isLeaf())) {
		System.out.println("****LEAF MISMATCH on " + 
				   inst.getModuleName());
	    }
	}

 	Integer i = (Integer)moduleCounts.get(inst.getModuleName());
  	if(i == null) i = new Integer(0);
  	i = new Integer(i.intValue() + 1);
  	moduleCounts.put(inst.getModuleName(), i);
  	return true;
    }
    
    public boolean port(Port port) {
	ports++;
	if(port.getDirection() == Port.OUTPUT)
	    return true;
	else
	    return false;
    }

    public boolean internalConnection(Connection conn) {
	return true;
    }

    public boolean externalConnection(Connection conn) {
	connections++;
	return true;
    }
}
