package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.InstanceSpace;
import java.util.*;

public class Parameter implements InfoEntry
{
    protected String name;
    protected Type type;
    protected Value value;
    protected boolean exportFlag;

    public Parameter(String n, Type t, Value v, boolean e) {
	name = n;
	type = t;
	value = v;
	exportFlag = e;
    }

    public Parameter(Parameter p) {
	this(p.name, p.type, p.value, p.exportFlag);
    }

    public String getName() {
	return name;
    }

    public Value getValue() {
	return value;
    }
    
    public Type getType() {
	return type;
    }

    public boolean isExported() {
	return exportFlag;
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {

	type = type.substitute(typeVariableContext, space.getGlobalTypes());

	if(type instanceof UserPointType) {
	    StringValue arguments = ((UserPointType)type).getArguments();
	    StringValue returnType = ((UserPointType)type).getReturnType();

	    arguments = 
		ConnectionManagement.substituteString(arguments,
						      typeVariableContext,
						      space);
	    returnType =
		ConnectionManagement.substituteString(returnType,
						      typeVariableContext,
						      space);
	    type = new UserPointType(arguments, returnType);
	}
	
	if(type.isPolymorphic()) {
	    throw new TypeInferenceException("Type " + type + 
					     " is not fully constrained");
	}
	value = ConnectionManagement.substitute(value, typeVariableContext, 
						space);
    }

    public String infoAccept(InfoVisitor v) {
	if(exportFlag) {
	    return v.parameter(this);
	} else {
	    return ""; /* Don't export this parm */
	}
    }
}
