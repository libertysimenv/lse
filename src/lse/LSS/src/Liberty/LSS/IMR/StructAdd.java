package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.InstanceSpace;
import java.util.*;

public class StructAdd implements InfoEntry
{
    private StringValue dataStructure, fieldType, fieldName;

    public StructAdd(StringValue d, StringValue t, StringValue n) {
	dataStructure = d;
	fieldType = t;
	fieldName = n;
    }

    public StringValue getFieldName() {
	return fieldName;
    }

    public StringValue getFieldType() {
	return fieldType;
    }

    public StringValue getDataStructure() {
	return dataStructure;
    }

    public String toString() {
	return "Structadd(" + 
	    dataStructure + ", " + 
	    fieldType + ", " + 
	    fieldName + ")";
    }

    public String infoAccept(InfoVisitor v) {
	return v.structAdd(this);
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {
	fieldType = ConnectionManagement.substituteString(fieldType, 
							  typeVariableContext, 
							  space);
    }
}
