package Liberty.LSS.IMR;

import Liberty.LSS.types.*;
import java.util.*;

public class DomainInstance implements InfoEntry
{
    /* This class is just a wrapper for DomainInstanceRefValue that
     * implements InfoEntry.  The value type does not implement the
     * interface to keep the types package independent of the IMR
     * package. */

    protected DomainInstanceRefValue dirv;

    public DomainInstance(DomainInstanceRefValue dirv) {
	this.dirv = dirv;
    }

    public DomainInstanceRefValue getDomainInstanceRefValue() {
	return dirv;
    }

    public String infoAccept(InfoVisitor v) {
	return v.domainInstance(this);
    }

    public static Collection wrapCollection(Collection c) {
	Iterator i = c.iterator();
	ArrayList l = new ArrayList();

	while(i.hasNext()) {
	    DomainInstanceRefValue dirv = (DomainInstanceRefValue)i.next();
	    l.add(new DomainInstance(dirv));
	}
	return l;
    }
}
