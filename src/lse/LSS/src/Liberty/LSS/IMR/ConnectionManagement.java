package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.AST.InstanceSpace;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;

import java.util.*;

public class ConnectionManagement
{
    public static Map inferTypes(Map insts,
				 InstanceSpace space,
				 Collection topLevelParms,
				 Collection runtimeVars,
				 Set baseConstraints)
	throws TypeInferenceException {
	
        long inferenceStartTime, inferenceEndTime;

	Set constraints = new HashSet(baseConstraints);
	collectConstraints(constraints, insts.values());
	constraints = fixConstraints(constraints, insts);
	
	System.out.println("Performing Type Inference");
	inferenceStartTime=System.currentTimeMillis();
	Map typeVariableContext = new HashMap();

	//TypeInferenceEngine.simpleInferTypes(constraints, typeVariableContext);
        TypeInferenceEngine.inferTypes(constraints, typeVariableContext);
	inferenceEndTime = System.currentTimeMillis();

        System.out.println("Total type inference time: " + 
			   (inferenceEndTime-inferenceStartTime)/1000.0 + "s");

	/* Update type related information */
	augmentGlobalTypes(typeVariableContext, space);
	placeTypes(typeVariableContext, insts.values(), space);

	/* Handle top-level type placement */
	Iterator i;
	i = space.getEvents().iterator();
	while(i.hasNext()) {
	    Event e = (Event)i.next();
	    e.substitute(typeVariableContext, space);
	}

	i = space.getCollectors().iterator();
	while(i.hasNext()) {
	    Collector c = (Collector)i.next();
	    c.substitute(typeVariableContext, space);
	}

	i = topLevelParms.iterator();
	while(i.hasNext()) {
	    Parameter p = (Parameter)i.next();
	    p.substitute(typeVariableContext, space);
	}

	i = runtimeVars.iterator();
	while(i.hasNext()) {
	    RuntimeVar r = (RuntimeVar)i.next();
	    r.substitute(typeVariableContext, space);
	}

	i = space.getDomainInstanceMap().entrySet().iterator();
	Map m = new HashMap();
	while(i.hasNext()) {
	    Map.Entry entry = (Map.Entry)i.next();
	    i.remove();
	    DomainInstanceRefValue d = 
		(DomainInstanceRefValue)entry.getValue();
	    d = substituteDomainInst(d, typeVariableContext, space);
	    m.put(entry.getKey(), d);
	}
	space.getDomainInstanceMap().putAll(m);
	
	return typeVariableContext;
    }

    /* These functions verify that all type variables correctly have
     * the mustResolve flag set */
    public static Set fixConstraints(Set constraints, Map instances) {
	Constraint c;
	Iterator i = constraints.iterator();
	Set newConstraints = new HashSet();
	while(i.hasNext()) {
	    c = fixConstraint((Constraint)i.next(), instances);
	    newConstraints.add(c);
	}
	return newConstraints;
    }

    public static Constraint fixConstraint(Constraint c, Map instances) {
	Type pair[];
	pair = c.getPair();
	for(int i = 0; i < 2; i++) {
	    pair[i] = fixTypeVariable(pair[i], instances);
	}
	return new Constraint(pair[0], pair[1], c.getOrigin());
    }

    public static Type fixTypeVariable(Type t, Map instances) {
	if(!(t instanceof TypeVariable))
	    return t;

	TypeVariable var = (TypeVariable)t;
	if(!var.toString().startsWith("'''") && 
	   var.toString().startsWith("''")) {
	    String name = var.toString().substring(2);
	    Instance inst = null; 
	    int dot;
	    while((dot = name.indexOf('.')) != -1) {
		inst = (Instance)instances.get(name.substring(0, dot));
		instances = inst.getSubInstanceMap();
		name = name.substring(dot+1);
	    }
	    return inst.getPort(name).getTypeVariable();
	} else return var;
    }


    /* Grab the constraints off the ports */
    public static void collectConstraints(Set constraints,
					  Collection insts) {
	Iterator i = insts.iterator();
	while(i.hasNext()) {
	    collectConstraints(constraints, (Instance)i.next());
	}
    }

    public static void collectConstraints(Set constraints,
					  Instance inst) {
	Collection ports = inst.getPorts();
	
	constraints.addAll(inst.getConstraints());

	if(!inst.isLeaf()) {
	    collectConstraints(constraints, inst.getSubInstances());
	}
	    
	Iterator i = ports.iterator();
	while(i.hasNext()) {
	    collectConstraints(constraints, (Port)i.next());
	}
    }

    public static void collectConstraints(Set constraints, Port p) {
	Type t = p.getType();

	constraints.add(new Constraint(p.getTypeVariable(), t, p));
	collectConstraintsFromConnections(constraints, p.getConnections());
	if(!p.isLeaf()) {
	    collectConstraintsFromConnections(constraints,
					      p.getInternalConnections());
	}
    }

    private static void 
	collectConstraintsFromConnections(Set constraints,
					  Collection connections) {
	Iterator i = connections.iterator();
	while(i.hasNext()) {
	    collectConstraints(constraints,
			       (Connection)i.next());
	}
    }

    public static void collectConstraints(Set constraints, Connection c) {
	Type connType;
	TypeVariable fromVar;
	TypeVariable toVar;

	fromVar = c.getSourcePort().getTypeVariable();
	toVar = c.getDestPort().getTypeVariable();
	connType = c.getConstraint();

	constraints.add(new Constraint(fromVar, toVar, c));
	if(connType != null) {
	    constraints.add(new Constraint(fromVar, connType, c));
	    constraints.add(new Constraint(connType, toVar, c));
	}
    }

    /* Replace the types on the ports */
    public static void placeTypes(Map typeVariableContext,
				  Collection insts,
				  InstanceSpace space) 
	throws TypeInferenceException {
	Iterator i = insts.iterator();
	while(i.hasNext()) {
	    placeTypes(typeVariableContext, (Instance)i.next(), space);
	}
    }

    public static void placeTypes(Map typeVariableContext,
				  Instance inst, 
				  InstanceSpace space)
	throws TypeInferenceException {
	inst.substitute(typeVariableContext, space);
	Collection ports = inst.getPorts();

	if(!inst.isLeaf()) {
	    placeTypes(typeVariableContext, inst.getSubInstances(), space);
	}
	    
	Iterator i = ports.iterator();
	while(i.hasNext()) {
	    placeTypes(typeVariableContext, (Port)i.next());
	}
    }

    public static void placeTypes(Map typeVariableContext, Port p) {
	Type t = (Type)typeVariableContext.get(p.getTypeVariable());
	p.setType(t);
    }

    /* Make sure connections are okay */
    public static boolean connectionsOk(Collection insts) 
	throws ConnectionException {
	Iterator i = insts.iterator();
	boolean ok = true;
	boolean fatal = false;
	String msg = "";
	
	while(i.hasNext()) {
	    boolean currentOk;
	    try {
		currentOk = connectionsOk((Instance)i.next());
	    } catch(ConnectionException e) {
		if(!msg.equals("")) msg += "\n";
		msg += e.getMessage();
		currentOk = false;
		if(e.isFatal()) {
		    ok = false;
		    fatal = true;
		    break;
		}
	    }
	    ok = ok && currentOk;
	}

	if(!ok) throw new ConnectionException(msg, fatal);
	return ok;
    }

    public static boolean connectionsOk(Instance inst) 
	throws ConnectionException {
	boolean ok = true;
	boolean fatal = false;
	String msg = "";

	if(!inst.isLeaf()) {
	    try {
		boolean currentOk = connectionsOk(inst.getSubInstances());
		ok = ok && currentOk;
	    } catch(ConnectionException e) {
		msg = e.getMessage();
		ok = false;
		if(e.isFatal())
		    fatal = true;
	    }
	}

	if(!fatal) {
	    Iterator i = inst.getPorts().iterator();
	    
	    while(i.hasNext()) {
		boolean currentOk;
		try {
		    Port p = (Port)i.next();
		    currentOk = p.connectionsOk();
		} catch(ConnectionException e) {
		    if(!msg.equals("")) msg += "\n";
		    msg += e.getMessage();
		    currentOk = false;
		    if(e.isFatal()) {
			ok = false;
			fatal = true;
			break;
		    }
		}
		ok = ok && currentOk;
	    }
	}

	if(!ok) throw new ConnectionException(msg, fatal);
	return ok;
    }

    /* Augment global types */
    public static void augmentGlobalTypes(Map typeVariableContext,
					  InstanceSpace space) {
	Map globalTypes = space.getGlobalTypes();
	Iterator i = globalTypes.entrySet().iterator();
	Set newTypes = new HashSet();

	while(i.hasNext()) {
	    Map.Entry entry = (Map.Entry)i.next();
	    Type t = (Type)entry.getKey();
	    if(t.isPolymorphic()) {
		t = t.substitute(typeVariableContext);
		newTypes.add(t);
	    }
	}

	i = newTypes.iterator();
	while(i.hasNext()) {
	    Type t = (Type)i.next();
	    space.addGlobalType(t);
	}
    }

    public static Value substitute(Value value, 
				   Map typeVariableContext, 
				   InstanceSpace space) 
	throws TypeInferenceException {
	if(value == null) return null;
	if(value instanceof StringValue) {
	    value = substituteString((StringValue)value, 
				     typeVariableContext,
				     space);
	} else if(value instanceof NumericTypeVariable) {
	    value = substituteInt((NumericTypeVariable)value,
				  typeVariableContext);
	}
	return value;
    }

    public static DomainInstanceRefValue 
	substituteDomainInst(DomainInstanceRefValue drv, 
			     Map typeVariableContext,
			     InstanceSpace space)
	throws TypeInferenceException {
	String className, instName;
	StringValue buildArgs, name, runArgs;
	try {
	    className = drv.getClassName();
	    instName = drv.getInstName();
	    buildArgs = (StringValue)drv.getSubValue("buildArgs");
	    name = (StringValue)drv.getSubValue("name");
	    runArgs = (StringValue)drv.getSubValue("runArgs");
	} catch(TypeException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}

	buildArgs = substituteString(buildArgs, typeVariableContext, space);
	runArgs = substituteString(runArgs, typeVariableContext, space);
	drv = new DomainInstanceRefValue(className, buildArgs, instName, 
					 name, runArgs);
	return drv;
    }

    public static NumericIntValue substituteInt(NumericTypeVariable ntv,
						Map typeVariableContext) 
	throws TypeInferenceException {
	if(ntv == null) return null;
	Type t = ntv.substitute(typeVariableContext);
	if(t instanceof ArraySizeType) {
	    int val = ((ArraySizeType)t).getSize();
	    return new NumericIntValue(val);
	} else {
	    throw new TypeInferenceException("Type variable" + ntv + 
					     " is not fully constrained");
	}
    }

    public static StringValue substituteString(StringValue str,
					       final Map typeVariableContext,
					       final InstanceSpace space) 
	throws TypeInferenceException {
	return substituteString(str,
				typeVariableContext,
				space.getGlobalTypes());
    }

    public static StringValue substituteString(StringValue str,
					       final Map typeVariableContext,
					       final Map globalTypes) 
	throws TypeInferenceException {
	if(str == null) return null;
	class Manipulator implements StringValue.PieceManipulator {
	    public Vector pieces = new Vector();
	    Map typesToNames;
	    TypeInferenceException except = null;

	    public Manipulator() {
		typesToNames = globalTypes;
	    }
	    public void handleString(String s) {
		pieces.add(StringValue.Piece.create(s));
	    }
	    public void handleType(Type t) {
		t = t.substitute(typeVariableContext);

		String piece;
		if(t instanceof ArraySizeType) {
		    int val = ((ArraySizeType)t).getSize();
		    piece = (new Integer(val)).toString();
		} else {
		    if(t.isPolymorphic()) {
			/* We can't throw due to interface */
			except = new TypeInferenceException("Type " + t + 
							    " is not fully " + 
							    "constrained");
			return;
		    }

		    if(typesToNames.containsKey(t)) {
			piece = "LSEut_ref(" + 
			    (String)typesToNames.get(t) + ")";
		    } else {
			/* If it doesn't exist, emit the backend type */
			try {
			    piece = t.getBackendType(typesToNames);
			} catch(RuntimeTypeException e) {
			    /* There is no backend type, so we should
			     * flag an error */
			    /* We can't throw due to interface */
			    except = 
				new TypeInferenceException(t + 
							   " does not have a" +
							   " backend type " +
							   "and therefore " + 
							   "cannot be used " + 
							   "inside of ${}.");
			    return;
			}
		    }
		}
		pieces.add(StringValue.Piece.create(piece));
	    }
	    public void checkException()
		throws TypeInferenceException {
		if(except != null) {
		    TypeInferenceException e = except;
		    except = null;
		    throw e;
		}
	    }
	}

	Manipulator manip = new Manipulator();
	Iterator i = str.getValue().iterator();
	while(i.hasNext()) {
	    StringValue.Piece p = (StringValue.Piece)i.next();
	    p.manipulate(manip);
	    manip.checkException();
	}

	/* The coercion is to handle subtypes of string */
	try {
	    StringValue value = new StringValue(manip.pieces);
	    return (StringValue)Coerce.subtypeCoerce(value,str.getType());
	} catch(TypeException e) {
	    throw new RuntimeTypeException(e.getMessage());
	}
    }
}
