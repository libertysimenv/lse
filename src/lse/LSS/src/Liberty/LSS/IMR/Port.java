package Liberty.LSS.IMR;

import Liberty.LSS.*;
import Liberty.LSS.types.*;
import Liberty.LSS.types.inference.*;
import Liberty.LSS.AST.*;

import java.util.*;

public class Port implements InfoEntry, ConstraintSource
{
    public final static int INPUT = 0;
    public final static int OUTPUT = 1;

    private Instance instance;
    private String name;
    private boolean independent;
    private boolean handler;
    private CodePoint control;
    private int direction;
    private Type type;
    private Vector connections;
    private Vector internalConnections;
    private boolean indexInferable;
    private boolean indexInferred;
    private int inferredIndex;
    private boolean internalIndexInferable;
    private boolean internalIndexInferred;

    public Port(Instance i, String n,  int width, int dir,
		Type t,	ControlPointValue ctrl, 
		ControlPointValue defCtrl) {
	/* A port on a hierarchical module */
	if(i.isLeaf())
	    throw new NotAHierarchicalInstanceException();
	init(i,n,width,dir,t,ctrl,defCtrl);

	internalConnections = new Vector();
	internalIndexInferable = true;
	internalIndexInferred = false;
    }

    public Port(Instance i, String n, int width, int dir, 
		Type t,	ControlPointValue ctrl, 
		ControlPointValue defCtrl, boolean indep, 
		boolean h) {
	/* A port on a leaf module */
	if(!i.isLeaf())
	    throw new NotALeafInstanceException();
	init(i,n,width,dir,t,ctrl,defCtrl);
	independent = indep;
	handler = h;
    }

    private void init(Instance i, String n, int width,
		      int dir, Type t,
		      ControlPointValue ctrl, ControlPointValue defCtrl) 
	throws PortDirectionException {
	instance = i;
	name = n;

	if(dir == INPUT ||
	   dir == OUTPUT)
	    direction = dir;
	else
	    throw new PortDirectionException("Unknown direction on port " +
					     instance.getFullyQualifiedName() +
					     "." + name);

	if(t != null)
	    type = t;
	else
	    throw new NullPointerException();	

	control = new CodePoint(name, Types.controlPointType, 
				ctrl, defCtrl, true);

	connections = new Vector();
	connections.setSize(width);
	indexInferable = true;
	indexInferred = false;
	inferredIndex = 0;
    }

    public boolean isLeaf() {
	return (internalConnections == null);
    }

    public Instance getInstance() {
	return instance;
    }

    public String getName() {
	return name;
    }

    public Type getType() {
	return type;
    }

    public void setType(Type t) {
	type = t;
    }

    public CodePoint getControl() {
	return control;
    }

    public int getDirection() {
	return direction;
    }

    public boolean getIndependent() {
	return independent;
    }

    public boolean getHandler() {
	return handler;
    }

    int addConnection(Connection c) 
	throws PortIndexException {
	int index;

	if(!indexInferable)
	    throw new PortIndexException("Cannot infer port index on port " +
					 instance.getFullyQualifiedName() +
					 "." + name);
	index = addConnectionNoCheck(c, inferredIndex);
        indexInferable = true;
	indexInferred = true;
	inferredIndex++;
	return index;
    }

    int addConnectionNoCheck(Connection c, int i)
        throws PortIndexException {

	if(i >= connections.size())
	    throw new PortIndexException("Port index " + i + 
					 " greater than port width = " +
					 connections.size() + 
					 " on port " + 
					 instance.getFullyQualifiedName() +
					 "." + name);

	indexInferable = false;
	return addConn(connections, c, i);
    }

    int addConnection(Connection c, int i) 
	throws PortIndexException {
	if(indexInferred)
	    throw new PortIndexException("Cannot mix inferred and " + 
					 "explict port indexes on port " + 
					 instance.getFullyQualifiedName() + 
					 "." + name);
        return addConnectionNoCheck(c, i);
    }

    int addInternalConnection(Connection c) 
	throws PortIndexException,
	       NotAHierarchicalInstanceException {
	if(isLeaf())
	    throw new NotAHierarchicalInstanceException();
	if(!internalIndexInferable)
	    throw new PortIndexException("Cannot infer port index on port " +
					 instance.getFullyQualifiedName() +
					 "." + name);
	internalConnections.add(c);
	internalIndexInferred = true;
	return(internalConnections.size() - 1);
    }

    int addInternalConnection(Connection c, int i) 
	throws NotAHierarchicalInstanceException,
	       PortIndexException {
	if(internalIndexInferred)
	    throw new PortIndexException("Cannot mix inferred and " + 
					 "explict port indexes on port " + 
					 instance.getFullyQualifiedName() + 
					 "." + name);

        return addInternalConnectionNoCheck(c,i);
    }

    int addInternalConnectionNoCheck(Connection c, int i) 
	throws NotAHierarchicalInstanceException,
	       PortIndexException {
	if(isLeaf())
	    throw new NotAHierarchicalInstanceException();
	internalIndexInferable = false;
	return addConn(internalConnections, c, i);
    }

    private int addConn(Vector v, Connection c, int i)
	throws PortIndexException {
	Connection oldc;

	if(i >= v.size()) {
	    v.setSize(i+1);
	}

	oldc = (Connection)v.set(i,c);
	if(oldc != null) {
	    v.set(i, oldc);
	    throw new PortIndexException("Port instance " + i + 
					 " already connected on port " + 
					 instance.getFullyQualifiedName() +
					 "." + name);
	}
	return i;
    }

    public void removeConnection(int i) 
	throws PortIndexException {
	indexInferable = false;
	removeConn(connections, i);
    }

    public void removeInternalConnection(int i) 
	throws NotAHierarchicalInstanceException,
	       PortIndexException {
	if(isLeaf())
	    throw new NotAHierarchicalInstanceException();
	internalIndexInferable = false;
	removeConn(internalConnections, i);
    }

    private void removeConn(Vector v, int i)
	throws PortIndexException {
	try {
	    v.set(i, null);
	} catch(ArrayIndexOutOfBoundsException e) {
	    throw new PortIndexException(e.getMessage());
	}
    }
   
    private Connection getConn(Vector v, int i) 
	throws PortIndexException {
	try {
	    return (Connection)v.get(i);
	} catch(ArrayIndexOutOfBoundsException e) {
	    throw new PortIndexException(e.getMessage());
	}
    }

    public Connection getInternalConnectionAt(int i) 
	throws PortIndexException, 
	       NotAHierarchicalInstanceException {
	if(isLeaf())
	    throw new NotAHierarchicalInstanceException();
	return getConn(internalConnections, i);
    }

    public Connection getConnectionAt(int i) 
	throws PortIndexException {
	return getConn(connections, i);
    }

    private Set getConns(Vector v) {
	Set s = new HashSet(v);
	s.remove(null); /* Get rid of unconnected port instances */
	return s;
    }

    public Set getInternalConnections() 
	throws NotAHierarchicalInstanceException {
	if(isLeaf())
	    throw new NotAHierarchicalInstanceException();
	return getConns(internalConnections);
    }

    public Set getConnections() {
	return getConns(connections);
    }

    public boolean connectionsOk() 
	throws ConnectionException {
	if(isLeaf()) return true;
	for(int i = 0; i < connections.size(); i++) {
	    try {
		Connection c1 = getConnectionAt(i);
		Connection c2 = getInternalConnectionAt(i);
		if(c1 != null && c2 == null)
		    throw new PortIndexException("");
		else if (c1 != null)
		    c1.checkCycles();
	    } catch(PortIndexException e) {
		String msg = "Error: Port " + 
		    instance.getFullyQualifiedName() +
		    "." + getName() + "[" + i + "] is connected " +
		    "externally but not internally";

		throw new ConnectionException(msg);
	    }
	}
	return true;
    }

    /* Returns true if this port has any external
       connections */
    public boolean isConnected() {
	Set s = getConnections();
	return (s.size() > 0);
    }

    /* Returns true if this port has any external
       connections which ultimately reach a 
       leaf instance */
    public boolean isGloballyConnected() {
	Set s = getConnections();
	Iterator i = s.iterator();
	while(i.hasNext()) {
	    Connection c = (Connection)i.next();
	    if(direction == INPUT) {
		if(c.getUltimateSourcePort() != null)
		    return true;
	    } else {
		if(c.getUltimateDestPort() != null)
		    return true;
	    }
	}
	return false;
    }

    public int getWidth() {
	return connections.size();
    }

    public String infoAccept(InfoVisitor v) {
	if(!isLeaf())
	    throw new NotALeafInstanceException();

	return v.port(this);
    }

    public TypeVariable getTypeVariable() {
	String name = getInstance().getFullyQualifiedName() + "." + 
	    getName();
	boolean gc = isGloballyConnected();
	return new TypeVariable("'" + name, gc);
    }

    public String toString(){
	return name;
    }

    public String getIdentifierString() {
	return "port " + getInstance().getFullyQualifiedName() + "." + name;
    }

    public void substitute(Map typeVariableContext, InstanceSpace space)
	throws TypeInferenceException {
	/* Connections and type are handled elsewhere */
	if(control != null)
	    control.substitute(typeVariableContext, space);
    }
}
