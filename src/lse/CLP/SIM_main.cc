/* 
 * Copyright (c) 2002 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Main loop for the simulator, including a command-line interface
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This code holds the main routine for a simulator/emulator combination.
 * It is responsible for calling simulator and emulator initialization
 * routines and calling the simulator main loop.  It also includes
 * a command-line interface using the Python interpreter.
 *
 * TODO:  Actually make the command-line interface do something!
 *
 */
/* USE_PYTHON is temporarily here to allow python to be present/absent; we
 * will need to create different CLP libraries if we want python to be put 
 * back in.  The point of doing this is that if python is removed, we
 * can link the library for standard universe Condor jobs
 */
#ifdef USE_PYTHON
#include <Python.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <SIM_clp_interface.h>

static char *pname;
#ifdef USE_PYTHON
static int usepython; 
#endif
static int cleanenv=0;
static char **cleanenvp = NULL;

static void build_cleanenv()
{
  cleanenvp = (char**)malloc(sizeof(char*));
  *cleanenvp = NULL;
}

static void add_to_cleanenv(char *str)
{
  int i = 0;
  while(cleanenvp[i]) i++;
  cleanenvp = (char **)realloc(cleanenvp, sizeof(char*) * (i+2));
  cleanenvp[i] = str;
  cleanenvp[i+1] = NULL;
}

static void destroy_cleanenv()
{
  if(cleanenvp)
    free(cleanenvp);
}


static int usage() {
  fprintf(LSE_stderr,
	  "\n%s <options> [ [(--|binary)] <program options>]\n",
	  pname);
  fprintf(LSE_stderr,"\t-c|--cleanenv\t\tClean all environments\n");
  fprintf(stderr,"\t--env:VAR=VALUE\t\tAdd VAR to environment\n\n");
#ifdef USE_PYTHON
  fprintf(LSE_stderr,"\t--script:<file>\t\tScript file to run\n");
  fprintf(LSE_stderr,"\t-i\t\t\tEnter interactive mode\n");
  fprintf(LSE_stderr,
	  "   NOTE: No program options or binary can be set on the command\n");
  fprintf(LSE_stderr,
	  "         line if interactive mode or a script is used.\n");
#endif
  fprintf(LSE_stderr,"\n Simulator options (prefix with `--sim:')\n");
  LSE_sim_print_usage();
  exit(1);
}

int main(int argc, char *argv[], char *envp[])
{
  int i,numused;
#ifdef USE_PYTHON
  int scriptfile = -1;   /* scriptfile position in argument list */
  int dointeract = 0;    /* do interactive? */
#endif
  int rstat;             /* return status */

  pname = argv[0];

  /* initialize the exit status */
  LSE_sim_exit_status = 0;

  /* initialize the error file to point to stderr */
  LSE_stderr = stderr;

  /* initialize the simulator(and domains) */

  if ((rstat = LSE_sim_initialize())) { /* exit on error... */
    fprintf(LSE_stderr,
	    "CLP: Error %d on call to LSE_sim_initialize(), exiting\n",
	    rstat);
    exit(1);
  }

  /* read command-line arguments */

  for (i=1;i<argc;) {
    if (!strcmp(argv[i],"--")) {
      i++;
      break;
    }
    if (!strncmp(argv[i],"--sim:",6)) {
      numused = LSE_sim_parse_arg(argc-i,argv[i]+6,argv+i+1);
      if (numused <= 0) {
	fprintf(LSE_stderr,"LSE: Error: unrecognized argument %s\n", argv[i]);
	usage();
      }
      i+= numused;
    }
    else if (!strncmp(argv[i],"--dom:",6)) {
      char *ind;
      ind = strchr(argv[i]+6,':');
      if (ind==NULL) {
	fprintf(LSE_stderr,
		"Missing domain instance or class name in argument %s\n",
		argv[i]);
	usage();
      }
      *ind = '\0';  /* end the domain instance name */
      numused = LSE_domain_parse_arg(argv[i]+6,argc-i,ind+1,argv+i+1);
      if (numused <= 0) {
	fprintf(LSE_stderr,"LSE: Error: unrecognized domain argument\n");
	usage();
      }
      i+= numused;
    }
    else {
#ifdef USE_PYTHON
     if (!strncmp(argv[i],"--script:",9)) {
	scriptfile=i;
      }
      else if (!strcmp(argv[i],"-i")) dointeract=1;
      else 
#endif
      if (!strcmp(argv[i],"-cleanenv") ||
	  !strcmp(argv[i],"--cleanenv") ||
	  !strcmp(argv[i],"-c")) {
	cleanenv=1;
	build_cleanenv();
      } else if(!strncmp(argv[i],"--env:",6)) {
	if(!cleanenv) {
          fprintf(stderr, "Only use --env with -c or --cleanenv\n");
          usage();
        } else {
          add_to_cleanenv(argv[i]+6);
        }
      }
      else if (!strcmp(argv[i],"--help")) usage();
      else if (!strcmp(argv[i],"-help")) usage();
      else if (argv[i][0]!='-') break;
      else {
	fprintf(LSE_stderr,"LSE: Error: unrecognized argument\n");
	usage();
      }
      i++;
    }

  } /* for arguments */
  /* Exit condition: i = start of program options or potentially benchmark */

  if ((rstat = LSE_sim_parse_leftovers(argc-i,argv+i,
				       cleanenv ? cleanenvp : envp))) {
    if (rstat<0) usage();
    exit(1);
  }

#ifdef USE_PYTHON
  /* Check for program being there... */
  usepython = scriptfile >= 0 || dointeract;

  /* initialize python */

  if (usepython) {
    Py_SetProgramName(argv[0]);
    Py_Initialize();
    /* Am not going to set argc/argv */
    if (PyRun_SimpleString("import sys, os, readline")<0) {
      fprintf(LSE_stderr,
	      "LSE: Error: Unable to import standard Python libraries\n");
      exit(1);
    }
  }

  /* determine what to do with scripts or interpreted or just go.... */

  if (usepython) {
    if (scriptfile >= 0) {
      FILE *fp;
      fp = fopen(argv[scriptfile]+8,"r");
      if (fp==NULL) {
	fprintf(LSE_stderr,"LSE: Error: Unable to open scriptfile %s.\n",
		argv[scriptfile]+8);
	exit(1);
      }
      printf("LSE: Running script %s\n",argv[scriptfile]+8);
      PyRun_AnyFile(fp, argv[scriptfile]+8);
      fclose(fp);
    }
    if (dointeract) {
      printf("LSE: Entering interactive mode: (Control-D to exit)\n");
      PyRun_AnyFile(stdin,"<stdin>");
    }
  } else {

    /* Start simulator */

    if ((rstat = LSE_sim_start())) {
      fprintf(LSE_stderr,
	      "CLP: Error %d returned from LSE_sim_start\n",rstat);
      exit(1);
    }

    /* run the simulator */

    if ((rstat = LSE_sim_engine()) < 0) {
      fprintf(LSE_stderr,
	      "CLP: Error %d returned from LSE_sim_engine\n",rstat);

      /* If the simulator didn't give us an exit code, let's set one */
      if (!LSE_sim_exit_status)
	LSE_sim_exit_status = rstat;

      /* Do not exit here because we want to finalize properly.... */
    }

  }

  /* Finalize Python */

  if (usepython) {
    Py_Finalize();
  }
#else
  {

    /* Start simulator */

    if ((rstat = LSE_sim_start())) {
      fprintf(LSE_stderr,
	      "CLP: Error %d returned from LSE_sim_start\n",rstat);
      exit(1);
    }

    /* run the simulator */

    if ((rstat = LSE_sim_engine()) < 0) {
      fprintf(LSE_stderr,
	      "CLP: Error %d returned from LSE_sim_engine\n",rstat);

      /* If the simulator didn't give us an exit code, let's set one */
      if (!LSE_sim_exit_status)
	LSE_sim_exit_status = rstat;

      /* Do not exit here because we want to finalize properly.... */
    }

  }

#endif

  /* finish simulation */

  if ((rstat = LSE_sim_finish(TRUE))) {
    fprintf(LSE_stderr,
	    "CLP: Error %d returned from LSE_sim_finish\n",rstat);
    exit(1);
  }

  /* finalize simulator */

  if ((rstat = LSE_sim_finalize())) {
    fprintf(LSE_stderr,
	    "CLP: Error %d returned from LSE_sim_finalize\n",rstat);
    exit(1);
  }

  destroy_cleanenv();
  return LSE_sim_exit_status;
}

