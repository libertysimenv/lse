/* -*-c-*-
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Module to supply a scheduled query for testing
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * The basic idea is that this looks like a wire with a phase function.
 *
 * There are two queries:
 *
 * The zero-cycle tester is odd... it basically is a port query and 
 * returns the status of the requested input port instance.  This allows us
 * to see within-cycle wakeup logic.
 *
 * The timing tester only returns a value when data is known on the input
 * port, but has the additional side effect of returning TRUE only when
 * the current time mod the requested amount is 0.  In addition, it schedules
 * whole simulation wakeup at the proper time for that to work...
 *
 */
class query_supplier : public LSE_module {

/*{
 * Module Variables
 */
 private:

/*} */

/*{
 * Utility functions
 */

/*} */

/*{
 * Actual operation functions 
 */
 public:

/*# 
 * Peform initialization
 *
 * Requires:
 *
 * Side Effects: None
 *
 * Parameters: 
 * 
 * Returns: TRUE on success, FALSE on failure
 *
 * See Also:  
 *
 * Bugs:
 *
 * To Do: 
 *        
 *
 */
boolean FUNC(init, void)
{
  int i;

  if (LSE_port_width(in) != LSE_port_width(out)) {
    LSE_report_err("Input and output port widths do not match");
  }

  return TRUE;
}

/*# 
 * Perform basic post simulation cleanup:
 *
 * Requires:
 *
 * Side Effects: 
 *
 * Parameters:
 * 
 * Returns: 
 *
 * Bugs:
 *
 * To Do: 
 *
 */
void FUNC(finish,void)
{
}

/*# 
 * Main work loop 
 *
 * Requires:
 *
 * Side Effects: 
 *
 * Parameters:
 * 
 * Returns: 
 *
 * Bugs:
 *
 * To Do: Add all the possible options.
 *
 */
void 
FUNC(phase, void)
{
  int porti;
  LSE_signal_t sig,sig2;
  LSE_dynid_t id;
  LSE_port_type(in) *datap;

  for (porti = 0; porti < LSE_port_width(in); porti++) {
    sig2 = LSE_port_get(out[porti].ack);
    LSE_port_set(in[porti].ack, sig2);
    sig = LSE_port_get(in[porti], &id, &datap);
    LSE_port_set(out[porti],sig,id,datap);

    if (LSE_signal_data_known(sig) && !LSE_signal_data_known(sig2) &&
	(LSE_query_used(get_port) || LSE_query_used(timing))) {
      LSE_query_results_changed();
    }
  }

}

/*{
 *  A query function
 */

/*#
 * Query which returns a port value
 */

LSE_signal_t
FUNC(get_port, int porti)
{
  return LSE_port_get(in[porti].data,NULL,NULL);
}
  
/*#
 * Query which causes time to skip
 */

int
  FUNC(timing, int porti, int period)
{
  
  LSE_signal_t sig;
  sig = LSE_port_get(in[porti].data,NULL,NULL);
  if (!LSE_signal_data_known(sig)) return -1;
  if (LSE_time_now % period) {
    ;
    return 0;
  }
  ;
  return 1;
}
  

/*}*/
}; /* class sample_module */

