/*
 * test file for multi-cycle timing of queries (which is really nothing more
 * than zero-cycle with an ).  But this time, we share code.
 *
 * expected output (with trace on) is something every 11 or 7 cycles for 65
 * total cycles
 *
 */
using corelib;
using testlib;

LSE_share_module_code_threshold = 0;
LSE_share_module_code_percent_threshold = 0.0;

LSE_schedule_generate_static = FALSE; // really test wakeups

runtimeable parameter dotrace = new runtime_parm(boolean, FALSE,
						 "trace",
						 "See the output") : boolean;

module middle {

 internal parameter period = 15 : int;

instance src     : corelib::source;
instance queries : testlib::query_supplier;
instance snk     : corelib::sink;

src.out -> [int64] queries.in;
queries.out -> snk.in;

var icount = new runtime_var("icount",int64) : runtime_var ref;

src.init = <<<{
  ${icount} = 0;
}>>>;

src.create_data = <<<{
  if (${icount} < 10) {
    *data = ${icount};
    return LSE_signal_enabled | LSE_signal_something ;
  } else {
    LSE_sim_terminate_now = 1;
    return LSE_signal_all_no;
  }
}>>>;

src.out.control = <<<{
  int dec;
  dec = LSE_query_call(${queries}:timing,0,0,${period});
  if (dec < 0) {
    return LSE_signal_something;
  } else if (dec) {
    return LSE_signal_all_yes;
  } else {
    return LSE_signal_something | LSE_signal_nack | LSE_signal_disabled;
  }
}>>>;

snk.sink_func = <<<{
  if (LSE_signal_enable_present(status) && LSE_signal_data_present(status)) {
    ${icount}++;
    ;
    if (${dotrace}) {
      fprintf(LSE_stderr, LSE_time_print_args(LSE_time_now));
      fprintf(LSE_stderr, ": %" PRId64 "\n", *data);
    }
  }
}>>>;

};

instance m1 : middle;
instance m2 : middle;
m1.period = 11;
m2.period = 7;
