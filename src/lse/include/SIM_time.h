/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * This file is the header file for simulation time
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines the time variable and the operations.  There are three different
 * ways of representing time implemented; generally the straight 64-bit
 * integer is going to be the way to go. 
 *
 * All time APIs (i.e. LSE_time_*) are defined in this file
 *
 */

#ifndef _LSE_SIM_TIME_H
#define _LSE_SIM_TIME_H

/* Must already have included SIM_types.h, which has stdint, SIM_config.h */

#include "LSE_time.h"

typedef uint64_t LSE_time_numticks_t;

#define LSE_time_zero_ticks (UINT64_C(0))
#define LSE_time_one_cycle LSE_time_construct(0,1)

extern LSE_time_numticks_t LSE_time_ticks_now;

#define LSEti_phasesPerCycle LSEfw_PARM(LSE_phases)

#define LSE_time_construct(cyc, phas) \
        (((LSE_time_t)(cyc))*LSEti_phasesPerCycle + (LSE_time_t)phas)

#define LSE_time_get_cycle(ta) ((ta)/LSEti_phasesPerCycle)
#define LSE_time_get_phase(ta) ((ta)%LSEti_phasesPerCycle)

#define LSE_time_ticks(ta) (ta)

#define LSE_time_print_args(ta) "%" PRIu64 "/%" PRIu64,\
                                    (ta)/LSEti_phasesPerCycle, \
                                    (ta)%LSEti_phasesPerCycle

#endif /* _LSE_SIM_TIME_H */
