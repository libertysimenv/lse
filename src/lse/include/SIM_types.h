/* 
 * Copyright (c) 2000-2003 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Base simulator data types 
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines the base data types and operators on them for the simulator.  
 * No polymorphic types are included here.
 *
 * There are forward declarations for structures with user-defined 
 * fields and domain-defined attributes, but the actual structure
 * definition is not present.
 *
 */

#ifndef _LSE_TYPES_H_
#define _LSE_TYPES_H_

#ifndef __GNUC__
/*
 *  Deal with the magic inline thing... ANSI C doesn't have inline, GCC does,
 * but GCC conveniently gives us an alternate keyword we can define away when 
 * we feel an urge to do ANSI C.  This macro is the *only* gcc extension we 
 * use.
 *
 * Note: we really do not want to use any compiler that does not either 
 * offer an inline keyword (like GCC) or do an intelligent job of inlining 
 * (like the Liberty compiler will)
 *
 */
#define inline
#endif

#include <LSE_inttypes.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <SIM_config.h>

#ifdef __cplusplus
#include <iostream>
using namespace std;
#endif

/**************************** Booleans *****************************/

#if !defined(LSE_BOOL)
#define LSE_BOOL
#ifdef __cplusplus
//typedef bool boolean;
typedef int boolean;
#else
typedef int boolean;
#endif
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif
#endif

/**************** Signal values *************************/

typedef unsigned int LSE_signal_t;

#define LSE_signal_unknown   0000
#define LSE_signal_nothing   0001
#define LSE_signal_something 0003
#define LSE_signal_nack      0010
#define LSE_signal_ack       0030
#define LSE_signal_disabled  0100
#define LSE_signal_enabled   0300
#define LSE_signal_all_no    0111
#define LSE_signal_all_yes   0333

#define LSEty_signal_data_mask           0003
#define LSEty_signal_data_known_mask     0001
#define LSEty_signal_data_present_mask   0002
#define LSEty_signal_ack_mask            0030
#define LSEty_signal_ack_known_mask      0010
#define LSEty_signal_ack_present_mask    0020
#define LSEty_signal_enable_mask         0300
#define LSEty_signal_enable_known_mask   0100
#define LSEty_signal_enable_present_mask 0200
#define LSEty_signal_any_mask            0333
#define LSEty_signal_any_known_mask      0111
#define LSEty_signal_any_present_mask    0222

#define LSE_signal_extract_data(x) ((x)&LSEty_signal_data_mask)
#define LSE_signal_extract_ack(x) ((x)&LSEty_signal_ack_mask)
#define LSE_signal_extract_enable(x) ((x)&LSEty_signal_enable_mask)

#define LSE_signal_data_known(x) ((x)&LSEty_signal_data_known_mask)
#define LSE_signal_ack_known(x) ((x)&LSEty_signal_ack_known_mask)
#define LSE_signal_enable_known(x) ((x)&LSEty_signal_enable_known_mask)

/* For safety's sake, we will do the compare, but it may be that the
 * 10 bit pattern never occurs...  but gcc seems to be pretty good at
 * working its way through this....
 */
#ifdef LSE_NOT_SO_SAFE
#define LSE_signal_data_present(x) ((x)&LSEty_signal_data_present_mask)
#define LSE_signal_ack_present(x) ((x)&LSEty_signal_ack_present_mask)
#define LSE_signal_enable_present(x) ((x)&LSEty_signal_enable_present_mask)
#else
#define LSE_signal_data_present(x) \
   (((x)&LSEty_signal_data_mask)==LSE_signal_something)
#define LSE_signal_ack_present(x) \
   (((x)&LSEty_signal_ack_mask)==LSE_signal_ack)
#define LSE_signal_enable_present(x) \
   (((x)&LSEty_signal_enable_mask)==LSE_signal_enabled)
#endif

#define LSE_signal_ack2enable(x) (((x)<<3)&LSEty_signal_enable_mask)
#define LSE_signal_ack2data(x) (((x)>>3)&LSEty_signal_data_mask)
#define LSE_signal_enable2ack(x) (((x)>>3)&LSEty_signal_ack_mask)
#define LSE_signal_enable2data(x) (((x)>>6)&LSEty_signal_data_mask)
#define LSE_signal_data2enable(x) (((x)<<6)&LSEty_signal_enable_mask)
#define LSE_signal_data2ack(x) (((x)<<3)&LSEty_signal_ack_mask)

inline void LSE_signal_print(FILE *fp,LSE_signal_t st) {
  const static char *values="UNUYInIy";
  fprintf(fp,"d%ce%ca%c", values[st&7], values[(st>>6)&7], values[(st>>3)&7]);
}

/************* three-valued logic functions *************/

inline LSE_signal_t LSE_signal_and(LSE_signal_t x, LSE_signal_t y) {
  return (((x)&(y)) | (( ( (x) & ~((x)>>1) ) | ( (y) & ~((y)>>1))) \
		       & LSEty_signal_any_known_mask));
}

inline LSE_signal_t LSE_signal_or(LSE_signal_t x, LSE_signal_t y) {
  return (((x)|(y))&  ( LSEty_signal_any_present_mask \
                  | ( ((x)|((y)>>1))&((y)|((x)>>1))  )));
}

inline LSE_signal_t LSE_signal_not(LSE_signal_t x) {
  return (( ~(x) & ((x)<<1) & LSEty_signal_any_present_mask ) | \
          ( (x) & LSEty_signal_any_known_mask));
}

/************* firing support macros *************/

/* For the firings, we want to be sure that we get a "No" constant from the
 * control function propagated.  We do *not* need to propate a "no" constant
 * from the input port if the control function didn't try to propagate it.
 * And of couse, propagation applies *only* to data, but this is only used
 * for data....
 * 
 * x is the old status and y is the control function status
 *
 *  x  y  res
 * ----------
 *  u  u   u
 *  u  n   n   -- this case is the special one
 *  u  y   u
 *  n  u   n/u -- this one we do not care about
 *  n  n   n
 *  n  y   n
 *  y  u   u
 *  y  n   n
 *  y  y   y
 */
#define LSEty_port_status_firing_and(x,y) \
       (((x)&(y)) | (( ( (y) & ~((y)>>1))) & LSEty_signal_any_known_mask))

/* this one masks new data with ~ old known to prevent change after known */
#if LSEfw_PARM(LSE_schedule_protect_signals)
#define LSEty_status_merge(o,n) ( (~((o)<<1) & (n)) | (o))
#else
#define LSEty_status_merge(o,n) ((o)|(n))
#endif

/******************** Resolutions and dynids **************/

typedef struct LSEre_resolution_info_s *LSE_resolution_t;
typedef struct LSEdy_dynid_info_s *LSE_dynid_t;
typedef uint64_t LSE_dynid_num_t;

/******************** Miscellaneous other *******************/

typedef void *LSE_instanceref_t;
typedef void *LSE_domain_t;
typedef void *LSE_literal_t;
typedef void *LSE_type_none;
#define LSE_type_none_NULL ((void *)NULL)

#endif /* _LSE_TYPES_H_ */

