/* 
 * Copyright (c) 2002-2003 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header for simulator-CLP interface
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines the prototypes for the simulator-CLP interface routines.
 * All of these functions and variables are supplied by the simulator
 * to the CLP; the CLP in turn is responsible to call them in the
 * correct order.
 *
 */
#ifndef _LSE_CLP_INTERFACE_H_
#define _LSE_CLP_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#if !defined(LSE_BOOL)
#define LSE_BOOL
typedef int boolean;
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif
#endif

  /* argument parsing */
  extern int LSE_sim_parse_arg(int, char *, char **);
  extern int LSE_sim_parse_leftovers(int, char **, char **);
  extern int LSE_domain_parse_arg(char *, int, char *, char **);
  extern void LSE_sim_print_usage(void);

  /* simulator init/start/engine functions */

  extern int LSE_sim_initialize(void);
  extern int LSE_sim_start(void);
  extern int LSE_sim_engine(void);
  extern int LSE_sim_do_timestep(void);
  extern int LSE_sim_finish(boolean);
  extern int LSE_sim_finalize(void);

#ifdef __cplusplus
}
#endif

  /* Variables available */

  extern FILE *LSE_stderr;
  extern int LSE_sim_exit_status;
  extern int LSE_sim_terminate_now;
  extern int LSE_sim_terminate_count;


#endif /* _LSE_CLP_INTERFACE_H_ */

