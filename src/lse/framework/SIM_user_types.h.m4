/* 
 * Copyright (c) 2000-2003 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Types defined by modules and configurations
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines all types which modules and configurations have specified to
 * LSS.  Also generates the type for field structures
 *
 */
m4_include(SIM_quotes.m4)m4_dnl
LSEm4_push_diversion(-1)
m4_include(SIM_database.m4)
LSEm4_pop_diversion()m4_dnl
#ifndef _LSE_USER_TYPES_H
#define _LSE_USER_TYPES_H

/* Must have already included SIM_types.h, which gives stdio,SIM_config.h */

#if (LSEfw_PARM(LSE_mp_multithreaded))
#define LSEfw_volatile volatile
#else
#define LSEfw_volatile
#endif

/************* Types from LSS **********/

m4_pythonfile(
for t in LSE_db.globalTypes:
    tdef = str(t[1])
    if string.find(tdef,"??")>=0:
        print "typedef %s;" % (string.replace(tdef,"??",t[2]))
    else:
        print "typedef " + tdef + " " + t[2] + ";"
)m4_dnl

/************* Results of structadds ***********/

m4_pythonfile(
def print_struct(db,s):
    ename = "LSEfw_%s_extension" % s[4:]
    if (not len(db.structAdds[s])): 
	print "struct %s { " % ename
        print "  int dummy;"
        print "};"
    else: 
	il = db.structAdds[s].items(); il.sort()
        for obj in il: # going across instances
	    inst=db.getInst(obj[0])
	    print ("""m4_python(LSEpy_instance=LSE_db.getInst("%s"))m4_dnl"""
				% obj[0])
            nl = obj[1].items(); nl.sort() # get individual stuff
            print "struct %s { %s };" % \
                  (LSEcg_structadd_type_name(obj[0],s),
                   reduce(lambda x,y:x + ("%s %s;\n" % (y[1][0],y[1][1])),
                          nl,""))
	print "struct %s { " % ename
        for obj in il: # going across instances
            print "  struct %s %s;" % (LSEcg_structadd_type_name(obj[0],s),
                                       LSEcg_flatten(obj[0]))
        print "};"
)

m4_python(print_struct(LSE_db,"LSE_dynid_t"))

m4_python(print_struct(LSE_db,"LSE_resolution_t"))

#endif /* _LSE_USER_TYPES_H */

