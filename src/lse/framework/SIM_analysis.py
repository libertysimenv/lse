# /* 
#  * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python database analysis code
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This code performs analysis of the database.  It is called from
#  * SIM_database_build and SIM_create_files
#  *
#  * The API macro definitions are intended to be a "middle" layer between the
#  *  API macro definitions in SIM_database_build and the API definitions in
#  *  SIM_apidefs.  Thus, these macros may be called from SIM_database_build
#  *  and may call SIM_apidefs.
#  *
#  * TODO: unify the two tokenizers....
#  *
#  */
import sys

from SIM_codegen import * # imports tokenizer and database
import SIM_apidefs  # to get access to the internal routines

###########################################################################
# LSEan_assign_cblocks(db)                                                #
#   Make list of all codeblocks in the system and record their numbering  #
#                                                                         #
#   Must occur after controlEmpty has been calculated for ports           #
###########################################################################
def LSEan_assign_cblocks(db):

    def insert_cblock(type,iname,oname,instno,db=db):
        key=(type,iname,oname,instno)
        db.cblocksBacklist[key] = len(db.cblocksList)
        db.cblocksList.append(key)

    # the null block
    insert_cblock(LSEdb_CblockDynamic, "DYN",None,0)

    # Generate all the code blocks

    for inst in db.instanceOrder:
        inst.firstCblock = len(db.cblocksList)
        if inst.phase:
            insert_cblock(LSEdb_CblockPhase,inst.name,"phase",0)

        if inst.start or not inst.codepoints['start_of_timestep'].isEmpty:
            insert_cblock(LSEdb_CblockPhaseStart,inst.name,"",0)
            
        if inst.end:
            insert_cblock(LSEdb_CblockPhaseEnd,inst.name,"",0)
        if not inst.codepoints['end_of_timestep'].isEmpty:
            insert_cblock(LSEdb_CblockPhaseEnd,inst.name,"",1)
            
        for p in inst.portOrder:
            if (not p.width): continue
            if (not p.controlEmpty):
                for i in range(p.width):
                    insert_cblock(LSEdb_CblockFiring,inst.name,p.name,i)
            if p.handler:
                for i in range(p.width):
                    insert_cblock(LSEdb_CblockHandler,inst.name,p.name,i)
        inst.lastCblock = len(db.cblocksList)
        
    db.totalCblocks = len(db.cblocksList)

    # sys.stderr.write("%s\n" % db.cblocksList)
    # sys.stderr.write("%s\n" % db.cblocksBacklist)
    # sys.stderr.write("total=%d\n" % db.totalCblocks)

##########################################################################
#  LSEan_analyze_ports(db)
#     - marks control points empty or not empty
#     - determines connection strides
##########################################################################

def LSEan_analyze_ports(db):
    for inst in db.instanceOrder:
        for p in inst.portOrder:
            cp = inst.codepoints.get(p.name)
            p.controlEmpty = (not cp or cp.type != LSEdb_PointControl or
                              cp.isEmpty)
            _LSEan_find_connection_stride(p)
            p.schedIndependent = p.independent and \
				 db.getParmVal('LSE_schedule_use_independent')
            
def _LSEan_find_connection_stride(port):
    if port.width == 0:
        port.connectionStride = 0
        return
    if port.width == 1 and port.connections[0]: # watch out for
        # unconnected width 1 ports
        port.connectionStride = 1
        return
    cl = port.connections[0]
    stride = 0
    if not cl:
        port.connectionStride = 0
        return
    for ci in port.connections:
        if not cl:
            cl = ci
            continue
        if not ci:
            port.connectionStride = 0
            return
        if ci[0] != cl[0]:
            port.connectionStride = 0
            return
        if not stride:
            stride = ci[1] - cl[1]
        else:
            if (ci[1]-cl[1] != stride):
                port.connectionStride = 0
                return
        cl = ci
    port.connectionStride = stride
    

##########################################################################
#  LSEan_analyze_a_cf(db)
#     - Figure out data dependencies of control functions
#
#  Note that most of this code is using extremely old parsing routines.
#  It should be updated to use the tokenized code, but I don't have the
#  time to be rearranging algorithms that work....
#
##########################################################################
def LSEan_analyze_a_cf(db,p,pnt):
    p.queryReachable = pnt.hasIQuery or \
                       (pnt.hasWord("LSE_user_invoke")
                        and p.inst.hasIQueryingUserPoint)

    if (not db.getParmVal('LSE_schedule_analyze_cfs')):
        p.controlRefsIdata = 1
        p.controlRefsIstatus = 1
        p.controlRefsOstatus = 1
        p.controlDeps = (027,027,027,0)
    else:
        p.controlRefsIdata = (pnt.hasWord("data") or
                              pnt.hasWord("id"))
        p.controlRefsIstatus = (pnt.hasWord("istatus") or
                                pnt.hasWord(
            "LSE_controlpoint_call_empty"))
        p.controlRefsOstatus = (pnt.hasWord("ostatus") or
                                pnt.hasWord(
            "LSE_controlpoint_call_empty"))

        if pnt.usesDefault:
            analysisTokens = pnt.current.toWordList()
            analysisTokens.extend(pnt.default.toWordList())
        else:
            analysisTokens = pnt.current.toWordList()

        unparseable = 020
        if (p.controlRefsIstatus or
            p.controlRefsIdata): unparseable = unparseable | 05
        if p.controlRefsOstatus: unparseable = unparseable | 02
        unparsevec = (unparseable, unparseable, unparseable, 0)
        
        p.controlDeps = _LSEan_parse_cf(analysisTokens, unparsevec)
        
##########################################################################
#  LSEan_analyze_port_deps(db)
#     - Figure out data dependencies of control functions and a few more
#       tidbits
#     - determine what constants should be on ports....
#
#  Note that most of this code is using extremely old parsing routines.
#  It should be updated to use the tokenized code, but I don't have the
#  time to be rearranging algorithms that work....
#
##########################################################################

def LSEan_analyze_port_deps(db):
    for inst in db.instanceOrder:

        for cp in inst.codepointOrder:
            if cp.type==LSEdb_PointUser and cp.name != "end_of_timestep" and \
               cp.name != "start_of_timestep":
                if cp.hasIQuery:
                    inst.hasIQueryingUserPoint = 1
                    break
                
        # at this point the words found during analysis are no longer needed
        # we can remove them to make the database smaller (and thus faster)
        for cp in inst.codepointOrder:
            cp.currentWords = None
            cp.defaultWords = None

    # now that we know dependencies, we can handle port constants, with
    # propagation...
    
    _LSEan_assign_port_constants(db)
    
# Output meaning:
#   (routed to, routed from, constant used, constant value, otherflag)
#    for each one, numbers are octal and digits are (en, ack, data)
#    for routings, it is a bit mask:  4 = en, 2=ack, 1 = data
#    for constants, it is the normal port status encoding
_LSEan_decisionconst={
    'LSEty_signal_enable_mask'      : (010,010,010,0300),
    'LSEty_signal_ack_mask'         : (010,010,010,0030),
    'LSEty_signal_data_mask'        : (010,010,010,0003),
    
    'LSE_signal_unknown' : (010,010,010,0),
    'LSE_signal_nothing' : (010,010,010,0001),
    'LSE_signal_something' : (010,010,010,0003),
    'LSE_signal_nack' : (010,010,010,0010),
    'LSE_signal_ack' : (010,010,010,0030),
    'LSE_signal_disabled' : (010,010,010,0100),
    'LSE_signal_enabled' : (010,010,010,0300),
    
    'LSE_signal_all_no' : (010,010,010,0111),
    'LSE_signal_all_yes' : (010,010,010,0333),
    
    }

_LSEan_decisionapi={
                                #  and  or   and  or   and  or   val  sh
    'LSE_signal_extract_enable' :(~000, 000, 000, 010, 000, 010, 0300, 0),
    'LSE_signal_extract_ack'    :( 000, 010,~000, 000, 000, 010, 0030, 0),
    'LSE_signal_extract_data'   :( 000, 010, 000, 010,~000, 000, 0003, 0),
    'LSE_signal_enable2ack'     :(~000, 000, 000, 010, 000, 010, 0300, -1),
    'LSE_signal_enable2data'    :(~000, 000, 000, 010, 000, 010, 0300, -2),
    'LSE_signal_ack2enable'     :( 000, 010,~000, 000, 000, 010, 0030, 1),
    'LSE_signal_ack2data'       :( 000, 010,~000, 000, 000, 010, 0030, -1),
    'LSE_signal_data2enable'    :( 000, 010, 000, 010,~000, 000, 0003, 2),
    'LSE_signal_data2ack'       :( 000, 010, 000, 010,~000, 000, 0003, 1)
    }

def _LSEan_remove_matching_paren(t):
    pcount=0
    while (len(t) and not (pcount==0 and t[0]==')')):
        if t[0] == '(': pcount=pcount+1
        if t[0] == ')': pcount=pcount-1
        t.pop(0)

# make API call a mask
# add ~
def _LSEan_do_and(v0,c0,v1,c1,shft):
    if (v0 & v1 & 010): # both constants
        return (v0 | v1, c0 & c1 & (3<<(3*shft)))
    elif (v0 & 010): # value 0 is a constant
        r = (c0 >> (3*shft)) & 3
        if (r == 0): return (010, 0)
        elif (r == 3): return (v1, 0)
        else: return ((v0 | v1 | 020) & ~010,0) # weird and
    elif (v1 & 010): # value 1 is a constant
        r = (c1 >> (3*shft)) & 3
        if (r == 0): return (010, 0)
        elif (r == 3): return (v0, 0)
        else: return ((v0 | v1 | 020) & ~010,0) # weird and
    else: return ((v0 | v1), 0)

def _LSEan_do_or(v0,c0,v1,c1,shft):
    if (v0 & v1 & 010): # both constants
        return (v0 | v1, (c0 | c1) & (3<<(3*shft)))
    elif (v0 & 010): # value 0 is a constant
        r = (c0 >> (3*shft)) & 3
        if (r == 0): return (v1, 0)
        else: return (010, r << (3*shft))
    elif (v1 & 010): # value 1 is a constant
        r = (c1 >> (3*shft)) & 3
        if (r == 0): return (v0, 0)
        else: return (010, r << (3*shft))
    else: return ((v0 | v1), 0)

def _LSEan_parse_cf_expr(t,upvec):
    # find term
    con = _LSEan_decisionconst.get(t[0])
    api = _LSEan_decisionapi.get(t[0])
    if t[0] == '(':
        t.pop(0)
        try:
            tv = _LSEan_parse_cf_expr(t,upvec)
        except:
            _LSEan_remove_matching_paren(t)
            tv = upvec
        if t[0] != ')': raise Exception("")
        t.pop(0)
    elif con:
        tv = con
        t.pop(0)
    elif api:
        if t[1] == '(':
            tvm = api
            del t[0:2]
            try:
                tv = _LSEan_parse_cf_expr(t,upvec)
                if t[0] != ')': 
                    _LSEan_remove_matching_paren(t)
                    tv = upvec
            except:
                _LSEan_remove_matching_paren(t)
                tv = upvec
            if t[0] != ')': raise Exception("")
            i = (0+3+tvm[7])%3
            new0 = (tv[i] & tvm[i*2])|tvm[i*2+1]
            i = (1+3+tvm[7])%3
            new1 = (tv[i] & tvm[i*2])|tvm[i*2+1]
            i = (2+3+tvm[7])%3
            new2 = (tv[i] & tvm[i*2])|tvm[i*2+1]
            if (tvm[7]<0):
                new3 = (tvm[6] & tv[3])>>(-tvm[7]*3)
            else:
                new3 = (tvm[6] & tv[3])<<(tvm[7]*3)
            tv = (new0, new1, new2, new3)
            t.pop(0)
        else: raise Exception("")
    elif t[0]=='istatus':
        tv = (004,020,001,0)
        t.pop(0)
    elif t[0]=='ostatus':
        tv = (020,002,020,0)
        t.pop(0)
    else: raise Exception("")
    # now see if term continues
    if (t[0] == '|'):
        t.pop(0)
        tv2 = _LSEan_parse_cf_expr(t,upvec)
        # combine tv and tv2 somehow into tv
        # Basically need to find out when we have effectively
        # masked off whatever was in the input
        (new0,new0c) = _LSEan_do_or(tv[0],tv[3],tv2[0],tv2[3],2)
        (new1,new1c) = _LSEan_do_or(tv[1],tv[3],tv2[1],tv2[3],1)
        (new2,new2c) = _LSEan_do_or(tv[2],tv[3],tv2[2],tv2[3],0)
        tv = (new0, new1, new2, new0c | new1c | new2c)
    elif (t[0] == '&'):
        t.pop(0)
        tv2 = _LSEan_parse_cf_expr(t,upvec)
        (new0,new0c) = _LSEan_do_and(tv[0],tv[3],tv2[0],tv2[3],2)
        (new1,new1c) = _LSEan_do_and(tv[1],tv[3],tv2[1],tv2[3],1)
        (new2,new2c) = _LSEan_do_and(tv[2],tv[3],tv2[2],tv2[3],0)
        tv = (new0, new1, new2, new0c | new1c | new2c)
    return tv

def _LSEan_merge_results(this,running):
    if running == (0,0,0,0):
        return this
    diff_cons = this[3]^running[3]
    new3 = 0
    # if one constant but other is not, output is not a constant.
    if (this[0] & 010) != (running[0] & 010):
        new0 = (running[0] | this[0] | 020) & ~010
    # both constants
    elif (this[0] & 010):
        if (diff_cons & 0300): # constants do not agree
            new0 = (running[0] | this[0] | 020) & ~010
        else:
            new0 = (running[0] | this[0])
            new3 = new3 | (this[3] & 0300)
    else:
        new0 = (running[0] | this[0])
    # if one constant but other is not, output is not a constant.
    if (this[1] & 010) != (running[1] & 010):
        new1 = (running[1] | this[1] | 020) & ~010
    # both constants
    elif (this[1] & 010):
        if (diff_cons & 0030): # constants do not agree
            new1 = (running[1] | this[1] | 020) & ~010
        else:
            new1 = (running[1] | this[1])
            new3 = new3 | (this[3] & 0030)
    else:
        new1 = (running[1] | this[1])
    # if one constant but other is not, output is not a constant.
    if (this[2] & 010) != (running[2] & 010):
        new2 = (running[2] | this[2] | 020) & ~010
    # both constants
    elif (this[2] & 010):
        if (diff_cons & 0003): # constants do not agree
            new2 = (running[2] | this[2] | 020) & ~010
        else:
            new2 = (running[2] | this[2])
            new3 = new3 | (this[3] & 003)
    else:
        new2 = (running[2] | this[2])
    return (new0, new1, new2, new3)

def _LSEan_parse_cf(tokens, upvec):
    t = tokens * 1
    sawistatus = 0
    sawostatus = 0
    totalval = (0,0,0,0) # start with no knowledge
    if not len(t):
        return upvec
    try:
        s = t[0]
        if (s == "LSE_controlpoint_call_empty"):
            return (004,002,001,000)
        sawistatus = (0,0,0,0)
        sawostatus = (0,0,0,0)
        while (len(t)):
            s = t[0]
            if (s=='return'): 
                t.pop(0)
                tv = _LSEan_parse_cf_expr(t,upvec)
                if t[0] != ';': raise Exception("")
                totalval=_LSEan_merge_results(tv,totalval)
            elif (s=='LSE_signal_extract_data' or
                  s=='LSE_signal_data_present' or
                  s=='LSE_signal_data_known'):
                if t[1:4] == ['(','istatus',')']:
                    del t[0:3]
                    sawistatus = _LSEan_merge_results((001,001,001,000),
                                                      sawistatus)
                if t[1:4] == ['(','ostatus',')']:
                    del t[0:3]
            elif (s=='LSE_signal_extract_enable' or
                  s=='LSE_signal_enable_present' or
                  s=='LSE_signal_enable_known'):
                if t[1:4] == ['(','istatus',')']:
                    del t[0:3]
                    sawistatus = _LSEan_merge_results((004,004,004,000),
                                                      sawistatus)
                if t[1:4] == ['(','ostatus',')']:
                    del t[0:3]
            elif (s=='LSE_signal_extract_ack' or
                  s=='LSE_signal_ack_present' or
                  s=='LSE_signal_ack_known'):
                if t[1:4] == ['(','ostatus',')']:
                    del t[0:3]
                    sawostatus = _LSEan_merge_results((002,002,002,000),
                                                      sawistatus)
                if t[1:4] == ['(','istatus',')']:
                    del t[0:3]
            elif (s=='istatus'): # possible control dependency
                sawistatus = _LSEan_merge_results((005,005,005,000),
                                                  sawistatus)
            elif (s=='id' or s=='data'):
                sawistatus = _LSEan_merge_results((001,001,001,000),
                                                  sawistatus)
            elif (s=='ostatus'): # possible control dependency
                sawostatus = (002,002,002,000)
            t.pop(0)
        tv = sawistatus
        tv2 = sawostatus
        # now add status control dependencies
        # this works because a control dependency that selects
        # between passing through data and not passing through
        # will yield a "something else" merging.  Also, a
        # control flow where there is just one return statement
        # after the if-statement will still be understood.  Mismatched
        # constants also say "something else"
        if (totalval[0] & 010) or not (totalval[0] & 020):
            new0 = totalval[0]
        else: new0 = totalval[0] | tv[0] | tv2[0]
        if (totalval[1] & 010) or not (totalval[1] & 020):
            new1 = totalval[1]
        else: new1 = totalval[1] | tv[1] | tv2[1]
        if (totalval[2] & 010) or not (totalval[2] & 020):
            new2 = totalval[2]
        else: new2 = totalval[2] | tv[2] | tv2[2]

        return (new0, new1, new2, totalval[3])
    except:
        pass
    # there was an error; conclude with all possible dependencies
    if ('istatus' in tokens): tv=(025,025,025,000)
    else: tv=(020,020,020,0)
    if ('ostatus' in tokens): tv2=(022,022,022,000)
    else: tv2=(020,020,020,0)
    return (027 & (tv[0] | tv2[0]),
            027 & (tv[1] | tv2[1]),
            027 & (tv[2] | tv2[2]),
            0)

#
# assign constants to ports
#
def _LSEan_assign_port_constants(db):

   reportedDict = {}
    
   def do_global(x,p):
      # calculate global
      dv = ev = av = 0
      if (p.dir == "output"):
         if (p.controlDeps[0] & 010):
             ev = (p.controlDeps[3]>>6)&3
             if not ev:
                 key = (p.inst.name,p.name,1)
                 if not reportedDict.has_key(key):
                     reportedDict[key] = 1
                     db.reportErr("Control function for %s:%s produces"
                                  " unknown enable signal" % \
                                  (p.inst.name,p.name))
         if (p.controlDeps[2] & 010):
             dv = (p.controlDeps[3]>>0)&3
             if not dv:
                 key = (p.inst.name,p.name,0)
                 if not reportedDict.has_key(key):
                     reportedDict[key] = 1
                     db.reportErr("Control function for %s:%s produces"
                                  " unknown data signal" % \
                                  (p.inst.name,p.name))
      else:
         if (p.controlDeps[1] & 010):
             av = (p.controlDeps[3]>>3)&3
             if not av:
                 key = (p.inst.name,p.name,2)
                 if not reportedDict.has_key(key):
                     reportedDict[key] = 1
                     db.reportErr("Control function for %s:%s produces"
                                  " unknown ack signal" % \
                                  (p.inst.name,p.name))
      if not x:
         if (p.dir == "output"):
            av = 3
         else:
            dv = ev = 2
      else:
         q=x[0]
         if (p.dir == "output"):
            if (q.controlDeps[1] & 010):
                av = (q.controlDeps[3]>>3)&3
                if not av:
                    key = (q.inst.name,q.name,2)
                    if not reportedDict.has_key(key):
                        reportedDict[key] = 1
                        db.reportErr("Control function for %s:%s produces"
                                     " unknown ack signal" % \
                                     (q.inst.name,q.name))
         else:
            if (q.controlDeps[0] & 010):
                ev = (q.controlDeps[3]>>6)&3
                if not ev:
                    key = (q.inst.name,q.name,1)
                    if not reportedDict.has_key(key):
                        reportedDict[key] = 1
                        db.reportErr("Control function for %s:%s produces"
                                     " unknown enable signal" % \
                                     (q.inst.name,q.name))

            if (q.controlDeps[2] & 010):
                dv = (q.controlDeps[3]>>0)&3
                if not dv:
                    key = (p.inst.name,p.name,0)
                    if not reportedDict.has_key(key):
                        reportedDict[key] = 1
                        db.reportErr("Control function for %s:%s produces"
                                     " unknown data signal" % \
                                     (q.inst.name,q.name)) 
      if dv == 3: dv = 0
      return (dv,ev,av)

   def do_local(x,p):
      # calculate local
      dv = ev = av = 0
      if (p.dir == "output"):
         if (p.controlDeps[1] & 010):
             av = (p.controlDeps[3]>>3)&3
             if not av:
                 key = (p.inst.name,p.name,2)
                 if not reportedDict.has_key(key):
                     reportedDict[key] = 1
                     db.reportErr("Control function for %s:%s produces"
                                  " unknown ack signal" % \
                                  (p.inst.name,p.name))
      else:
         if (p.controlDeps[0] & 010):
             ev = (p.controlDeps[3]>>6)&3
             if not ev:
                 key = (p.inst.name,p.name,1)
                 if not reportedDict.has_key(key):
                     reportedDict[key] = 1
                     db.reportErr("Control function for %s:%s produces"
                                  " unknown enable signal" % \
                                  (p.inst.name,p.name))

         if (p.controlDeps[2] & 010):
             dv = (p.controlDeps[3]>>0)&3
             if not dv:
                 key = (p.inst.name,p.name,0)
                 if not reportedDict.has_key(key):
                     reportedDict[key] = 1
                     db.reportErr("Control function for %s:%s produces"
                                  " unknown data signal" % \
                                  (p.inst.name,p.name))

      #if not x:
      #   if (p.dir == "output"):
      #      av = 3
      #   else:
      #      dv = 2
      #      ev = 2
      if dv == 3: dv = 0
      return (dv,ev,av)

   for inst in db.instanceOrder:
      for p in inst.portOrder:
         if not p.width: continue
         plist = []
         if (not p.controlEmpty):
            p.localConstants = map(do_local,p.connections,[p]*p.width)
         p.globalConstants = map(do_global,p.connections,[p]*p.width)

#######################################################################
# LSEan_analyze_port_scheduling(db)                                   #
#######################################################################
#    Analyze scheduling characteristics of ports
#
# The information coming out of the analysis is a list of:
#
# connInfoList: (other instance name, other port name, other port instno,
#                other code block, (other port callers:data,enable,ack,any), 
#                other scheduling code, 
#                other side uses signal?, 
#                other side has port events we'll need to call,
#                mask of dropped signals, mask of constant signals)
#        - While a structure would be cleaner, a tuple is a bit faster
#
# otherAnyStatic: are any code blocks of other instances to be static
# otherAnyDynamic: are any code blocks of other instances to be dynamic
# otherSchedCode: scheduling code for code blocks of other instances 
#                 (-1 if heterogeneous)
# otherCBlockTypeCode: type of code blocks of other instances 
#                       (-1 if heterogeneous)
#
# firingCodeList: scheduling codes for firings
# fireSchedCode: scheduling code for firings of this instance 
#                (-1 if heterogeneous)
# fireAnyStatic: are any firings of this instance to be static?
#
# selfCodeList:   scheduling codes for self (phase or handler?)
# selfSchedCode: scheduling code for phase function/handler of this instance 
#                (-1 if heterogeneous)
# selfAnyDynamic: are any invocations of this instance to be dynamic
#
# Note that scheduling codes are:
#  0 = static or nothing to run - do nothing
#  1 = dynamic - add to dynamic schedule
#  2 = immediate - call immediately
#  3 = immediate checking for re-entrance
#################
def LSEan_analyze_port_scheduling(db):

    
    def find_glocs(pi,signalno,db=db):
        ## Find locations where signals generated from this port
        ## are generated.
        def getcalls(x,y,pi=pi,db=db):
            return x + (db.signalsCalls[y+pi] + 
                        db.signalsDynSects[y+pi])

        callglocs=reduce(getcalls,signalno,[])
        callglocs.sort()
        return callglocs
    
    def find_rlocs(pi,signalno,db=db):
        ## Find locations where signals generated from this port
        ## are received.
        def getcalls(x,y,pi=pi,db=db):
            rc = db.signalList[y+pi].recvcno
            if rc: return x + db.cblocksCalls[rc] + db.cblocksDynSects[rc]
            else: return x

        callrlocs=reduce(getcalls,signalno,[])
        callrlocs.sort()
        return callrlocs
    
    def find_isstatic(callglocs,cno,db=db):

        ## Find first dynamic and static location after each call.
        ## If dynamic is ever before static, we cannot ever do 
        ## static scheduling.
        isstatic = 1
        def getorder(x):
            return (db.block_sched_graph[x].singleOrder,x)
        callglocs = map(getorder,callglocs)
        callglocs.sort()
        for (call,cid) in callglocs:
            firststatic = len(db.block_sched_list)+1
            firstdyn = len(db.block_sched_list)+2
            cbc = map(getorder,db.cblocksCalls[cno])
            cbc.sort()
            for (fs,fsid) in cbc:
                if (fs > call): 
                    firststatic=fs
                    break
            cds = map(getorder,db.cblocksDynSects[cno])
            cds.sort()
            for (fs,fsid) in cds:
                if (fs >= call): 
                    # check whether it is being run statically
                    if (fs > call
                        and cno in db.dynamicExtras[fsid]):
                        if (fs < firststatic): firststatic=fs
                    else: firstdyn=fs
                    break
            # if first dynamic is before the first static, 
            # cannot do static
            if (firstdyn <= firststatic):
                isstatic = 0
                break # can exit
        return isstatic

    def check_other(pi,signalno,cno,db=db):
        glocs = find_glocs(pi,signalno)
        isstatic = find_isstatic(glocs,cno)
        return isstatic
        
    def is_event_filled(i,n):
        return i.events.has_key(n) and i.events[n].isFilled

    dynamic_schedule_type = db.getParmVal("LSE_DAP_dynamic_schedule_type")

    # create flag mapping
    if dynamic_schedule_type != 0:
        cnolev2flag={}
        tl = map(lambda x:(x[1],x[0]),db.cblocksLECtlevels.items())
        tl.sort()
        for (llist,cno) in tl:
            for i in llist:
                cnolev2flag[(cno,i)] = len(cnolev2flag)+1
        del tl
        db.cnolev2flag = cnolev2flag # keep for mainloop to use when appending
    else: db.cnolev2flag = {}

    usedLEC = {}
    # idea is to prevent cblock from being remapped into multiple
    # sub-blocks in the same LEC schedule.  This is good for
    # situations where the static schedule repeats a cblock
    # many times
    def handle_remap(l,recvcno):
        remap = db.LECsubblocks.get((recvcno,l),None)
        if remap and remap.parent:
            a = usedLEC.get((remap.parent,recvcno),(-1,-1))
            if a[0] < 0:
                remap.subcnos[recvcno] = 1
                usedLEC[(remap.parent,recvcno)] = (remap.cno,l)
                recvcno = remap.cno
            else: (recvcno,l) = a
        return (recvcno,l)

    ismt = db.getParmVal("LSE_mp_multithreaded")
    
    for inst in db.instanceOrder:
        for p in inst.portOrder:

            # make certain all ports have this information
            p.connInfoList = []
            p.firingCodeList = []
            p.selfCodeList = []
            p.otherAnyStatic = 1
            p.otherAnyDynamic = 0
            p.otherSchedCode = 0
            p.otherCBlockTypeCode = 0
            p.fireSchedCode = 0
            p.fireAnyStatic = 1
            p.selfSchedCode = 0
            p.selfAnyDynamic = 0
            p.localResolveCodeList = []
            p.localResolveCode = 0
            p.globalResolveCodeList = []
            p.globalResolveCode = 0
            
            if (not p.width): continue

            ####### Define a lookup table for destinations from the port

            constufflist = []
            cblocktypelist = []

            for pi in range(0,p.width):
                p2 = p.connections[pi]

                if p.dir == "output":
                    if p.controlEmpty:
                        offsets = [LSEdb_sigoff_data, LSEdb_sigoff_en,
                                   -1, -1, -1, -1]
                    else:
                        offsets = [LSEdb_sigoff_ofdata,
                                   LSEdb_sigoff_ofen,
                                   LSEdb_sigoff_data, LSEdb_sigoff_en,
                                   LSEdb_sigoff_ofack, -1]
                else:
                    if p.controlEmpty:
                        offsets = [LSEdb_sigoff_ack, -1,
                                   -1, -1, -1, -1]
                    else:
                        offsets = [LSEdb_sigoff_ifack, -1,
                                   LSEdb_sigoff_ack, -1,
                                   LSEdb_sigoff_ifdata,
                                   LSEdb_sigoff_ifen]
                if not p2: offsets[0:2] = [-1, -1]
                
                signals = []
                for o in offsets:
                    if o >= 0: signals.append(p.basesignalno + p.width * o)
                    else: signals.append(-1)

                # deal with LECsim stuff....
                if dynamic_schedule_type != 0:
                    LEClist = []
                    for sno in signals:
                        if sno < 0:
                            LEClist.extend([0,0,0])
                            continue
                        
                        s = db.signalList[sno+pi]
                        recvcno = s.recvcno
                        cbllist = db.cblocksLECtlevels.get(recvcno,[])
                        for l in cbllist:
                            if l > s.LECtlevel:
                                (recvcno,l) = handle_remap(l,recvcno)
                                LEClist.append(l)
                                LEClist.append(cnolev2flag[(recvcno,l)])
                                LEClist.append(recvcno)
                                break
                        else:
                            if len(cbllist):
                                l = cbllist[0]
                                (recvcno,l) = handle_remap(l,recvcno)
                                LEClist.append(l)
                                LEClist.append(cnolev2flag[(recvcno,l)])
                                LEClist.append(recvcno)
                            else:
                                LEClist.extend([0,0,0])
                else:
                    LEClist = [0] * 18
                    
	   	if (p2):

                    (tp, ti) = p2
                    if not tp.controlEmpty:
                        cblock = (LSEdb_CblockFiring,tp.inst.name,tp.name,ti)
                    elif tp.handler:
                        cblock = (LSEdb_CblockHandler,tp.inst.name,tp.name,ti)
                    elif tp.inst.phase:
                        cblock = (LSEdb_CblockPhase,tp.inst.name,"phase",0)
                    else: cblock = db.cblocksList[0]

                    cno = db.cblocksBacklist[cblock]

                    # figure out specials for the cblock
                    # static = always scheduled statically after us
                    # noimm = cannot schedule immediately.  Happens for
                    #	    firings with queries/user calls, all phase,
                    #	    handlers with userpoints with queries, and
                    #       when possibly multithreaded
                    # final code:
                    #  0 = static
                    #  1 = dynamic
                    #  2 = immediate

                    ### determine scheduling info for each instance
                    signalno = filter(lambda x:x>=0,signals[0:2])
                    isstatic = check_other(pi,signalno,cno)
                    
                    op = p2[0]
                    instno = op.inst.instno
                    
                    # find scheduling style and whether immediate is possible
                    if (cblock[0] == LSEdb_CblockFiring):
	             	style = db.getParmVal("LSE_schedule_style_firing")
	             	noimm = (ismt or
                                 (op.getControlPoint().queryReachable or 
                                  (style == 2 and db.requiredStatic[cno])))
                        if op.inst.instno <= inst.instno: noimm = 1
                    elif (cblock[0] == LSEdb_CblockPhase):
		   	style = 0
			noimm = 1
                    else:
	             	style = db.getParmVal("LSE_schedule_style_handler")
	             	noimm = (ismt or op.inst.hasIQueryingUserPoint
                                 or op.inst.instno <= inst.instno)
                        
                    # and now make the scheduling decision
                    if style == 0:
		     	if isstatic: code = 0
			else: code = 1
                    elif style == 1:
                        if isstatic: code = 0
                        elif not noimm: code = 2
                        else: code = 1
                    else:
	        	if (not noimm): code = 2
	  		elif (isstatic): code= 0
                	else: code = 1

                    constufflist.append(code)
                    cblocktypelist.append(cblock[0])

                    # TODO: need to drop anything immediate from the schedule
                    
                    # figure out queries needing support
                    gda = (op.getCallers("global.data") != None)
                    gen = (op.getCallers("global.enable") != None)
                    gac = (op.getCallers("global.ack") != None)
                    gan = (op.getCallers("global.any") != None)

                    # connection is used by the other side if it is not an
                    # input port (here) or the other control function is empty
                    # or the other control function references ostatus
                    used = (p.dir != 'input' or op.controlEmpty or 
                            op.controlRefsOstatus)

                    # the other side's port status event facing us is filled
                    eventuses = (
                        is_event_filled(op.inst,op.name+".resolved"),
                        (op.controlEmpty and
                         is_event_filled(op.inst,op.name+".localresolved")
                         ))

                    # figure out ignoring and constantness....
                    nc = p.getConnectedSignals(pi)
                    if p.controlEmpty: nc=nc[0]
                    else: nc=nc[1]
                    mask = 0
                    if db.signalList[nc[0]].dropreason ==\
                       LSEdb_sigdropped_notused: mask = 1
                    if db.signalList[nc[1]].dropreason ==\
                       LSEdb_sigdropped_notused: mask = mask | 2
                    if db.signalList[nc[2]].dropreason ==\
                       LSEdb_sigdropped_notused:
                        mask = mask | 4
                    cmask = 0
                    if db.signalList[nc[0]].constant: cmask = 1
                    if db.signalList[nc[1]].constant: cmask = cmask | 2
                    if db.signalList[nc[2]].constant: cmask = cmask | 4
                    
                    p.connInfoList.append((op.inst.name,op.name,p2[1],
                                           cblock, (gda, gen, gac, gan),
                                           code, used,
                                           eventuses,mask,cmask,
                                           tuple(LEClist)))

	   	else: # unconnected instance
                    p.connInfoList.append((None,None,0,
                                           db.cblocksList[0],(0,0,0,0),
                                           0,0,
                                           (0,0),0,7,
                                           tuple(LEClist)))

            # is any scheduling code equal throughout?

            # note that if constufflist or code blocktypelist are of
            # zero length, we have a port with a width but no connections.
            # Loads of fun.
            if (len(constufflist)):
                code = reduce(lambda x,y: x * (x == y+1), 
                              constufflist,constufflist[0]+1) - 1
            else: code = 0 # static, so not explicitly scheduled
            p.otherAnyStatic = reduce(lambda x,y: x or y==0, constufflist,0)
            p.otherAnyDynamic = reduce(lambda x,y: x or y==1, constufflist,0)
            p.otherSchedCode = code
            if (len(cblocktypelist)):
                p.otherCBlockTypeCode = reduce(lambda x,y: x * (x == y+1), 
                                                cblocktypelist,
                                                cblocktypelist[0]+1) - 1
            else: p.otherCBlockTypeCode = 0

            ############ figure out whether we need to do wakeups
            # an even finer-grained calculation would check also whether
            # a particular signal is actually queried UNDER THIS NAME
            # before checking dynamic sections.  Right now a query under
            # the other name also triggers the check.
            
            ll = []
            gl = []
            for pi in range(p.width):
                signalno = p.getConnectedSignals(pi)
                
                hasdyn = 0
                for s in signalno[0]:
                    for qs in db.signalList[s].usedbyquery:
                        if db.signalsDynSects[qs]:
                            hasdyn=1
                            break
                    else: continue
                    break
                ll.append(hasdyn)

                hasdyn = 0
                for s in signalno[1]:
                    for qs in db.signalList[s].usedbyquery:
                        if db.signalsDynSects[qs]:
                            hasdyn=1
                            break
                    else: continue
                    break
                gl.append(hasdyn)

            p.localResolveCodeList = ll
            p.localResolveCode = reduce(lambda x,y: x or y==1, ll,0)
            if (len(ll)):
                p.localResolveCode = reduce(lambda x,y: x * (x == y+1),
                                            ll,ll[0]+1) - 1
            else: p.localResolveCode = 0
            
            if p.controlEmpty:
                p.globalResolveCodeList = p.localResolveCodeList
                p.globalResolveCode = p.localResolveCode
                p.firingCodeList = [0] * p.width
                p.selfCodeList = [None] * p.width
                continue

            p.globalResolveCodeList = gl
            p.globalResolveCode = reduce(lambda x,y: x or y==1, gl,0)
            if (len(gl)):
                p.globalResolveCode = reduce(lambda x,y: x * (x == y+1),
                                             gl,gl[0]+1) - 1
            else: p.globalResolveCode = 0

            ### END NEW
            if 0:
                l = []
                for pi in range(p.width):
                    if (p.dir == "output"):
                        signalno = [p.basesignalno+p.width*LSEdb_sigoff_data,
                                    p.basesignalno+p.width*LSEdb_sigoff_en]
                    else: signalno = [p.basesignalno+p.width*LSEdb_sigoff_ack]
                    hasdyn = 0
                    for s in signalno:
                        for qs in db.signalList[s].usedbyquery:
                            if db.signalsDynSects[qs]:
                                hasdyn=1
                                break
                        else: continue
                        break
                    l.append(hasdyn)

                p.localResolveCodeList = l
                p.localResolveCode = reduce(lambda x,y: x or y==1, l,0)
                if (len(l)):
                    p.localResolveCode = reduce(lambda x,y: x * (x == y+1),
                                                l,l[0]+1) - 1
                else: p.localResolveCode = 0

                if p.controlEmpty:
                    p.globalResolveCodeList = p.localResolveCodeList
                    p.globalResolveCode = p.localResolveCode
                    p.firingCodeList = [0] * p.width
                    p.selfCodeList = [None] * p.width
                    continue

                l = []
                for pi in range(p.width):
                    if (p.dir == "output"):
                        signalno = [p.basesignalno+p.width*LSEdb_sigoff_ofdata,
                                    p.basesignalno+p.width*LSEdb_sigoff_ofen]
                    else: signalno = [p.basesignalno+p.width*LSEdb_sigoff_ifack]
                    hasdyn = 0
                    for s in signalno:
                        for qs in db.signalList[s].usedbyquery:
                            if db.signalsDynSects[qs]:
                                hasdyn=1
                                break
                        else: continue
                        break
                    l.append(hasdyn)
                p.globalResolveCodeList = l
                p.globalResolveCode = reduce(lambda x,y: x or y==1, l,0)
                if (len(l)):
                    p.globalResolveCode = reduce(lambda x,y: x * (x == y+1),
                                                l,l[0]+1) - 1
                else: p.globalResolveCode = 0

            ############### Deal with firings of the port

            # base code block
            cblock = (LSEdb_CblockFiring, inst.name, p.name, 0)
            cno = db.cblocksBacklist[cblock]

            ### determine scheduling info for each instance
            if (p.dir == "output"):
                signalno = [p.basesignalno+p.width*LSEdb_sigoff_data,
                            p.basesignalno+p.width*LSEdb_sigoff_en]
            else: signalno = [p.basesignalno+p.width*LSEdb_sigoff_ack]

            for pi in range(0,p.width):

                isstatic = check_other(pi,signalno,cno+pi)
                
                # find scheduling style and whether immediate is possible
                style = db.getParmVal("LSE_schedule_style_firing")
                noimm = ismt or p.getControlPoint().queryReachable or \
                        (style==2 and db.requiredStatic[cno+pi])

                # and now make the scheduling decision
                if style == 0:
                    if isstatic: code = 0
                    else: code = 1
                elif style == 1:
                    if isstatic: code = 0
                    elif not noimm: code = 2
                    else: code = 1
                else:
                    if (not noimm): code = 2
                    elif (isstatic): code= 0
                    else: code=1

                p.firingCodeList.append(code)

            p.fireSchedCode = reduce(lambda x,y: x * (x == y+1), 
                                     p.firingCodeList,
                                     p.firingCodeList[0]+1) - 1
            p.fireAnyStatic = reduce(lambda x,y: x or y==0,
                                     p.firingCodeList,0)

            ########### And deal with the module itself #############
            #### (only if there is a firing)

            ### determine scheduling info for each instance
            p.selfCodeList = []
            if (p.dir == "input"):
                signalno = [p.basesignalno+p.width*LSEdb_sigoff_ifdata,
                            p.basesignalno+p.width*LSEdb_sigoff_ifen]
            else: signalno = [p.basesignalno+p.width*LSEdb_sigoff_ofack]

            if not p.handler:
                if not p.inst.phase: 
                    p.selfSchedCode = 0
                    continue # nothing to call
                cno = db.cblocksBacklist[(LSEdb_CblockPhase,p.inst.name,
                                          "phase",0)]
            else:
                basecno = db.cblocksBacklist[(LSEdb_CblockHandler,
                                              p.inst.name,
                                              p.name,0)]

            for pi in range(0,p.width):

                if p.handler: cno = basecno + pi

                isstatic = check_other(pi, signalno, cno)
                
                # find scheduling style and whether immediate is possible
                if p.handler:
                    style = db.getParmVal("LSE_schedule_style_handler")
                    noimm = ismt or p.inst.hasIQueryingUserPoint
                else:
                    style = 0
                    noimm = 1

                # and now make the scheduling decision
                if style == 0:
                    if isstatic: code = 0
                    else: code = 1
                elif style == 1:
                    if isstatic: code = 0
                    elif not noimm: code = 2
                    else: code = 1
                else:
                    if (not noimm): code = 2
                    elif (isstatic): code= 0
                    else: code=1

                p.selfCodeList.append(code)

            p.selfSchedCode = reduce(lambda x,y: x * (x == y+1), 
                                     p.selfCodeList,p.selfCodeList[0]+1) - 1
            p.selfAnyDynamic = reduce(lambda x,y: x or y==1, p.selfCodeList,0)

            # If you cannot figure it out, we are either stuck between static
            # and immediate in style 1 or static and dynamic in style 0 or 2,
            # since noimm is constant across port instances here.  Resolve by
            # going with either immediate or static, depending on the
            # situation.
            if (p.selfSchedCode<0): 
                if (p.selfAnyDynamic): p.selfSchedCode = 1 # latter situation
                else: p.selfSchedCode = 2                  # former situation

#######################################################################
# Instance merging analysis                                           #
#######################################################################
# Care about differences in:
# 1. port names/widths/types/directions/controlEmpty
# 2. codepoint name/contents/type/signature
# 3. query/method signatures
# 4. event signatures
# 5. parameters if they are not runtimeable
# 6. funcheader contents
# 7. moduleBody contents
# 8. domainSearchpath
# 9. exported types
# 10. structadds

# Do not care about differences in:
# 1. module name
# 2. strictness, reactiveness
# 3. port dataflow
# 4. phase/phase_start/phase_end presence
# 5. port independence, handlers, connections, controlDeps, stride
# 6. collectors

def _LSEan_oredmap(fn,l1,l2):
    # should be slightly faster than this:
    #return (len(l1) != len(l2) or
    #        reduce(lambda x,y:x or y,map(fn,l1,l2),0))
    if len(l1) != len(l2): return 1
    for i in map(fn,l1,l2):
        if i: return 1
    return 0

# TODO: handle contents differences differently
def _LSEan_codepoint_different_for_mmerge(oldp, newp):
    return (oldp.name != newp.name or
            (oldp.current != newp.current and
             oldp.type == LSEdb_PointControl) or
            oldp.default != newp.default or oldp.type != newp.type or
            oldp.isDefault != newp.isDefault or
            oldp.returnType != newp.returnType or oldp.params != newp.params)

def _LSEan_codepoint_different_for_imerge(oldp, newp):
    return (oldp.current != newp.current)

def _LSEan_port_different_for_merge(oldp, newp):
    return (oldp.name != newp.name or oldp.type != newp.type or
            oldp.width != newp.width or oldp.dir != newp.dir or
            oldp.controlEmpty != newp.controlEmpty)

def _LSEan_queries_different_for_merge(self,other):
    return (self.name != other.name or
            self.params != other.params or
            self.returnType != other.returnType or
            self.scheduled != other.scheduled)

def _LSEan_collectors_different_for_merge(self,other):
    return (self.decl != other.decl or
            self.report != other.report or
            self.init != other.init or
            self.record != other.record)

def _LSEan_events_different_for_mmerge(self,other):
    return (self.name != other.name or
            self.argString != other.argString)

def _LSEan_events_different_for_imerge(self,other):
    return _LSEan_oredmap(_LSEan_collectors_different_for_merge,
                          self.filled, other.filled)

def _LSEan_parms_different_for_merge(op,np):
    # different if names are different, types are different, runtime-status
    # is different, runtimeability is different,
    # or non-runtimed values are different
    return (op[0] != np[0] or op[1][1:] != np[1][1:] or
            (op[1][0] != np[1][0] and not op[1][3]))

def LSEan_inst_not_imergeable(oldi,newi,db):
    if _LSEan_oredmap(_LSEan_codepoint_different_for_imerge,
                      oldi.codepointOrder, newi.codepointOrder):
        return "codepoints different"

    if _LSEan_oredmap(_LSEan_events_different_for_imerge,
                      oldi.eventOrder, newi.eventOrder):
        return "collectors different"

    if oldi.extension != newi.extension:
        return "extensions different"

    return ""

def LSEan_inst_not_mmergeable(oldi,newi,db):
    if _LSEan_oredmap(_LSEan_codepoint_different_for_mmerge,
                      oldi.codepointOrder, newi.codepointOrder):
        return "codepoints different"

    if _LSEan_oredmap(_LSEan_port_different_for_merge,
                      oldi.portOrder, newi.portOrder):
        return "ports different"

    if _LSEan_oredmap(_LSEan_queries_different_for_merge,
                      oldi.queryOrder, newi.queryOrder):
        return "queries different"

    if _LSEan_oredmap(_LSEan_events_different_for_mmerge,
                      oldi.eventOrder, newi.eventOrder):
        return "events different"

    if _LSEan_oredmap(_LSEan_parms_different_for_merge,
                      oldi.parmOrder, newi.parmOrder):
        return "parms different"
    
    if oldi.funcheader != newi.funcheader:
        return "funcheaders different"

    if oldi.moduleBody != newi.moduleBody:
        return "module bodies different"

    if oldi.domainSearchPath[0] != newi.domainSearchPath[0]:
        return "domain search paths different"
    
    if oldi.typeMap != newi.typeMap:
        return "type maps different"

    if reduce(lambda x,y:\
              x or (db.structAdds.get(y).get(oldi.name) !=
                    db.structAdds.get(y).get(newi.name)),
              db.structAdds.keys(),0):
        return "struct adds different"

    return ""

#######################################################################
# Structadd types analysis                                            #
#######################################################################

def LSEan_analyze_structadds(db):
    for s in db.structAdds.keys():
        items = {}
        bm = {}
        il = db.structAdds.get(s).items(); il.sort()
        for mi in il:
            nl = mi[1].items(); nl.sort()
            nl = tuple(nl)
            bm[mi[0]] = items.setdefault(nl,mi[0])
        db.structAddMaps[s] = bm
    return

#######################################################################
# Port status structure analysis                                      #
#######################################################################
# Idea is to create a list of all instance names and port status
# indicating whether there is an initial value as well as thread
# owner; we will then select by thread and initial-value neediness to
# layout the structure in a very nice fashion...
#
# [ ( iname, pname, width, emptiness, hasInitial, thread), ... ]
#
def LSEan_analyze_data_layout(db):
    # now figure out information about each port status
    plist = []
    for inst in db.instanceOrder:
        for p in inst.portOrder:
            if (not p.width): continue
            plist.append((inst.name,p.name,p.width,p.controlEmpty))
    
    db.pstatList = plist
    return

#######################################################################
#        Internal helper functions                                    #
#######################################################################

_LSEan_ignoredUserpoints = {
    "start_of_timestep" : 1,
    "end_of_timestep" : 1,
    "init" : 1,
    "finish" : 1,
    }

#
# record a call from codepoint or data collector cp to
# (apiclass, targinst, objname) with index
#
def _LSEan_record_call(db, cp, apiclass, instname, objname, obj, index,
                       codeloc):

    key = (apiclass,instname,objname)
    
    if codeloc == LSEcg_in_datacoll:
        callerI = cp.event.inst
        if not callerI:
            callerI = db
        callername = ""
    elif codeloc == LSEcg_in_modulebody:
        callerI = cp
        callername = ""
    elif codeloc == LSEcg_in_funcheader:
        callerI = cp
        callername = ""
    elif codeloc == LSEcg_in_extension:
        callerI = cp
        callername = ""
    else: # in a codepoint
        callerI = cp.inst
        callername = cp.name
        if not callerI:
            callerI = db
        else:
            if (cp.type == LSEdb_PointUser):
                # record that a user point of the instance made a call....
                if not callerI.userPointCalls.has_key(key):
                    callerI.userPointCalls[key]=[]
                callerI.userPointCalls[key].append(index)
                if not _LSEan_ignoredUserpoints.has_key(cp.name):
                    if not callerI.userPointCallsFiltered.has_key(key):
                        callerI.userPointCallsFiltered[key]=[]
                    callerI.userPointCallsFiltered[key].append(index)
                    
                
        # record what the code point called on a per-code-point basis
        if not cp.calls.has_key(key): cp.calls[key]=[]
        cp.calls[key].append(index)
        
    # marking that the instance/top-level made a call
    
    if not callerI.calls.has_key(key): callerI.calls[key]=[]
    callerI.calls[key].append(index)

    # now recording who made the call...
    
    if type(obj) is types.InstanceType: # not a port alias....
        targinst = obj.inst
        key = (apiclass, objname)
        tmp = targinst.calledby.get(key)
        if not tmp:
            targinst.calledby[key] = {}
            tmp = targinst.calledby[key]
        cpk = (callerI.name, callername)
        tmp2 = tmp.get(cpk)
        if tmp2:
            tmp2[1].append(index)
        else:
            tmp[cpk] = (len(tmp),[index])

    else: # it was a port alias, need to mark everything affected...
        for al in obj[1]:
            if not al: continue # unconnected alias instance
            targinst = db.getInst(al[0])
            key = (apiclass, al[1])
            tmp = targinst.calledby.get(key)
            if not tmp:
                targinst.calledby[key] = {}
                tmp = targinst.calledby[key]
            cpk = (callerI.name, callername)
            tmp2 = tmp.get(cpk)
            if tmp2:
                tmp2[1].append(index)
            else:
                tmp[cpk] = (len(tmp),[index])

#######################################################################
#  Analysis API macros                                                #
#######################################################################

#
# LSE_controlpoint_call_empty
#
def _LSEan_controlpoint_call_empty(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    if env.codeloc == LSEcg_in_controlpoint:
        if subinst[1]: subinst[0].defaultWords[args[0]] = 1
        else: subinst[0].currentWords[args[0]] = 1
 
    return SIM_apidefs._LSEap_controlpoint_call_empty(tinfo,args,extra,env)

#
# LSE_event_record
#
def _LSEan_event_record(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    # returns call replacement and replaces args[0] with
    # (apiname, iname, pointer to query)
    qcall = SIM_apidefs._LSEap_event_record(tinfo,args,extra,env)
    q = args[0][2]

    # mark down the call
    if q: _LSEan_record_call(db, subinst[0],"LSE_event_record",q.inst.name,
                             q.name,q,"dummy",codeloc)
    
    return qcall

#
# LSE_method_call
#
def _LSEan_method_call(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    # returns call replacement and replaces args[0] with
    # (apiname, iname, pointer to query)
    qcall = SIM_apidefs._LSEap_method_call(tinfo,args,extra,env)
    q = args[0][2]

    # mark down the call
    if q: _LSEan_record_call(db, subinst[0],"LSE_method_call",q.inst.name,
                             q.name,q,"dummy",codeloc)
    
    return qcall

#
# LSE_port_query_status
#
def _LSEan_port_query(tinfo,args,extra,env):
    (db, currinst, subinst, codeloc, rexc) = env.getStandard()

    # replaces args[0] with (args[0],iname,pname,p,part,index)
    qcall = SIM_apidefs._LSEap_port_query(tinfo,args,extra,env)

    (dummy,iname,pname,p,part,index) = args[0]
    
    # mark down the call if there was no error...
    if p:
        _LSEan_record_call(db,subinst[0],part,iname,pname,p,index,codeloc)

    return qcall

#
# LSE_query_call
#
def _LSEan_query_call(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()

    # just record word presence; needed for noting whether query reached...
    if codeloc == LSEcg_in_controlpoint or codeloc == LSEcg_in_userpoint:
        if subinst[1]: subinst[0].defaultWords[args[0]] = 1
        else: subinst[0].currentWords[args[0]] = 1
        
    # returns call replacement and replaces args[0] with
    # (apiname, iname, pointer to query)
    qcall = SIM_apidefs._LSEap_query_call(tinfo,args,extra,env)
    q = args[0][2]

    # mark down the call
    if q: _LSEan_record_call(db, subinst[0], "LSE_query_call",q.inst.name,
                             q.name,q,str(args[2]), codeloc)
    
    return qcall

#
# Register use of words for control function arguments
#
def _LSEan_register_cword(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    if codeloc == LSEcg_in_controlpoint:
        # Here's a bit of ugliness.... we want the word to be bare, not
        # part of a structure or something.  Now, if we get it wrong, it
        # just means that we put too many data dependencies in the control
        # point.  For most of the words, this happens too infrequently to
        # care.
        #
        # But for "data", the word is used often because of port queries.  So,
        # we'll try to be clever and look for the preceeding ".".  Since we
        # do it for one, we might as well do it for all...
        try:
            # look for the last thing the parent had...
            # this will be thrown off by whitespace, but it is conservative
            # and should work for most of the uses of ".data" ...
            if type(tinfo[3][-1].contents) == types.ListType:
                if tinfo[3][-1].contents[-1].contents == ".":
                    return None
        except: pass
        if subinst[1]: subinst[0].defaultWords[args[0]] = 1
        else: subinst[0].currentWords[args[0]] = 1
    return None # leave word alone

#
# LSE_userpoint_invoke
#
def _LSEan_userpoint_invoke(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    if codeloc == LSEcg_in_controlpoint:
        if subinst[1]: subinst[0].defaultWords[args[0]] = 1
        else: subinst[0].currentWords[args[0]] = 1
 
    return SIM_apidefs._LSEap_userpoint_invoke(tinfo,args,extra,env)

#######################
#   Dictionaries      #
#######################

LSEan_APIs = {
    "data" : [(LSEtk_Tok_macro, LSEtk_NoArgs, _LSEan_register_cword,None)],
    "id" : [(LSEtk_Tok_macro, LSEtk_NoArgs, _LSEan_register_cword,None)],
    "istatus" : [(LSEtk_Tok_macro, LSEtk_NoArgs, _LSEan_register_cword,None)],
    "ostatus" : [(LSEtk_Tok_macro, LSEtk_NoArgs, _LSEan_register_cword,None)],
    "LSE_controlpoint_call_empty" : [(LSEtk_Tok_macro, 0,
                              _LSEan_controlpoint_call_empty,None)],
    "LSE_event_record" : [(LSEtk_Tok_macro, 0, _LSEan_event_record,None)],
    "LSE_method_call" : [(LSEtk_Tok_macro, 0, _LSEan_method_call,None)],
    "LSE_port_query" : [(LSEtk_Tok_macro, 0, _LSEan_port_query,None)],
    "LSE_query_call" : [(LSEtk_Tok_macro, 0, _LSEan_query_call,None)],
    "LSE_userpoint_invoke" : [(LSEtk_Tok_macro, 0,
                              _LSEan_userpoint_invoke, None)],
    }

