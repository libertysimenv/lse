/* 
 * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * This file generates module code
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file generates lots of code for the module:
 * - Read the database
 * - Include necessary type headers
 * - Stubs for external routines called
 * - Stubs for code points
 * - Internal code for port queries
 * - Global code for port query wakeups
 * - Internal user points/control points
 * - Global wrappers for input/output handling
 * - Internal send/receive/status API calls
 * - Internal data collectors
 *
 */
m4_divert(-1)
m4_dnl Read the database
m4_dnl SIM_inst_name.m4 will include LSE_quotes
m4_include(SIM_inst_name.m4)m4_divert(0)LSEm4_push_diversion(-1)
m4_include(SIM_database.m4)
m4_pythonfile(
import SIM_apidefs
LSEpy_minstance = LSE_db.getInst("LSEm4_unflat_instance_name")
if LSEpy_minstance.isMultiInst:
  LSEpy_instance = LSEpy_minstance.subInstanceList[0]
  LSEpy_miList = LSEpy_minstance.subInstanceList
  LSEpy_gList = LSEpy_minstance.subGroups
else: 
  LSEpy_instance = LSEpy_minstance
  LSEpy_miList = [ LSEpy_instance ]
  LSEpy_gList = [ [ LSEpy_instance ] ]
LSEpy_doCSpecialize = (LSE_db.getParmVal("LSE_DAP_specialize_single") and
		       (not LSEpy_minstance.isMultiInst or 
		       len(LSEpy_minstance.subGroups)==1))
LSEpy_doISpecialize = (not LSEpy_minstance.isMultiInst and \
		       LSE_db.getParmVal("LSE_DAP_specialize_single"))
LSEpy_checkAPIs = int(LSE_db.getParmVal("LSE_check_api_at_runtime"))
LSEpy_indir_pstatus = not int(LSE_db.getParmVal("LSE_use_direct_port_status"))
LSEpy_threaded = int(LSE_db.getParmVal("LSE_mp_multithreaded"))
if LSEpy_threaded: LSEpy_align = "LSEfw_attributes(aligned(LSEfw_cache_align))"
else: LSEpy_align = "" # no need to align if not multi-threaded

LSEpy_fstyle = int(LSE_db.getParmVal("LSE_schedule_style_firing"))>0
LSEpy_hstyle = int(LSE_db.getParmVal("LSE_schedule_style_handler"))>0

LSEpy_dsstyle = int(LSE_db.getParmVal("LSE_DAP_dynamic_schedule_type"))
LSEpy_acyclic = int(LSE_db.getParmVal("LSE_DAP_static_schedule_type")==1 or 
	            LSE_db.getParmVal("LSE_DAP_static_schedule_type")==2)
LSEpy_nonoblivious = not LSE_db.getParmVal("LSE_DAP_schedule_oblivious")

# very useful definitions!

def mdecl():
    if not LSEpy_doISpecialize:
        if LSEpy_doCSpecialize:
            return "LSEfw_local_emodule_t *LSEfw_obj =  " \
                   "static_cast<LSEfw_local_emodule_t *>(LSEfw_iptr);" 
        else:
            return "LSEfw_local_module_t *LSEfw_obj =  " \
                   "static_cast<LSEfw_local_module_t *>(LSEfw_iptr);" 
    else: return ""

def tdecl():
    return "LSEfw_iptr_t LSEfw_iptr = this;"
    
def mprefix():
    if LSEpy_doISpecialize:
        return "%s." % (LSEcg_instdata_name(LSEpy_instance.name))
    else: return "LSEfw_obj->"

def oprefix():
    if LSEpy_doISpecialize:
        return "%s." % (LSEcg_instdata_name(LSEpy_instance.name))
    else: return "LSEfw_obj->"

def do_trampoline(proto, callp, needReturn=0):
    if needReturn: rstring="return "
    else: rstring=""
    print """%s {
      %s
      %s%s%s;
    }""" % (proto, mdecl(), rstring, mprefix(), callp)
    
def do_itrampoline(proto, callp, needReturn=0):
    if needReturn: rstring="return "
    else: rstring=""
    print """%s {
      %s
      %s%s;
    }""" % (proto, tdecl(), rstring, callp)

def prep_callstring(base, pstring):
    plist = LSEcg_parse_params(pstring)
    if plist != []:
        pstr = base + " " + reduce(lambda x,y : x + ", " + y, plist)
    else: pstr = base + "_void"
    return pstr

SIMpy_void_re = re.compile("^\s*void\s*$")
SIMpy_param_re = re.compile(".*\W(\w+)\s*$")

def query_is_void(s):
	return s == "" or SIMpy_void_re.match(s)!=None

# analyze parameters
LSEpy_parms = {}
for p in LSEpy_instance.parmOrder:
    if not p[1][3]: # not runtimeable... PARM() took care of it.
        LSEpy_parms[p[0]] = 0
    elif p[1][2]: # runtimed; very special
        LSEpy_parms[p[0]] = 1
    else: # runtimeable, but not runtimed...
        for mi in LSEpy_miList[1:]:
            if mi.parms[p[0]][0] != p[1][0]: # not constant
                LSEpy_parms[p[0]] = 2
                break
        else: # it is constant
            if p[1][1] != "char *": 
                LSEpy_parms[p[0]] = 3
            else:
                LSEpy_parms[p[0]] = 4

)


m4_dnl ##################### And start the code #######################

LSEm4_pop_diversion()

#define LSEfw_isMultiInst m4_python(print "%d" % LSEpy_minstance.isMultiInst)
/* point instance name at appropriate field */

/* ALREADY included through types: stdlib, stdio, LSE_config */
#include <SIM_all_types.h>
#include <SIM_control.h>

extern "C" {
#include <unistd.h>
}

/******************** PREFIX EXTRAS ************************/

m4_python(print LSE_db.getParmVal("LSE_prefix_extras"))

/************* our prototypes and references ***********/

m4_pythonfile(
codeBlocksSeen={}
instanceDataSeen = {}
pinfoSeen={}

def get_pstype(p):  # used when initializing classes
    if p.controlEmpty: return "LSEfw_PORT_empty_t"
    else: return "LSEfw_PORT_nodef_t"
    
LSEpy_portsFiltered = filter(lambda x:x.width,LSEpy_instance.portOrder)

# local data structures
for mi in LSEpy_miList:
    instanceDataSeen[mi.name] = 1
    if not LSE_db.getParmVal("LSE_specialize_codeblock_numbers"):
        print "extern int " + LSEcg_codeblock_base(mi.name) + ";"
    for p in LSEpy_portsFiltered:
       pinfoSeen[mi.name, p.name] = 1

       # global info
       print "extern struct LSEfw_portinst_ginfo_s<%s> %s[%d];" % \
             (p.type, LSEcg_portinst_ginfo_name(mi.name,p.name), p.width)

       # local info
       print "extern struct LSEfw_portinst_info_s<%s> %s[%d];" % \
             ( p.type, LSEcg_portinst_info_name(mi.name,p.name), p.width)
       
       print

# local instance structures (declared forward)
gcnt = 0
for g in LSEpy_gList:
   ename = "LSEfw_class_%d_%s" % (gcnt, LSEpy_instance.module)
   gcnt += 1
   nsname = LSEcg_build_name("LSEfw_ns","",LSEpy_minstance.name,"")
   print "namespace %s { class %s; };" % (nsname,ename)
   print "using namespace %s;" % nsname
   for mi in g:
      # note: extern is needed because we can't yet define it
      print "extern class %s %s;\n" % (ename, LSEcg_instdata_name(mi.name));

# This instance code blocks (seen externally)

for p in LSEpy_portsFiltered:

    if not p.controlEmpty:
	ct = (LSEdb_CblockFiring,LSEpy_instance.name,p.name,0)
	pts = LSEcg_codeblock_name(LSE_db,ct,0,1)
	if not codeBlocksSeen.has_key(pts):
	   codeBlocksSeen[pts] = 1
	   print LSE_db.getParmVal("LSE_inline_port_firings") + " " + \
                 LSEcg_codeblock_prototype(LSE_db,ct,1) + ";"
        
    if p.handler: 
    	ct = (LSEdb_CblockHandler,LSEpy_instance.name,p.name,0)
    	pts = LSEcg_codeblock_name(LSE_db,ct,0,1)
    	if not codeBlocksSeen.has_key(pts):
    	    codeBlocksSeen[pts] = 1
    	    print LSEcg_codeblock_prototype(LSE_db,ct,1) + ";"

if LSEpy_instance.phase:
    ct = LSEdb_phaseKey(LSEpy_instance.name)
    pts = LSEcg_codeblock_name(LSE_db,ct,0,1)
    if not codeBlocksSeen.has_key(pts):
        codeBlocksSeen[pts] = 1
        print LSEcg_codeblock_prototype(LSE_db,ct,1) + ";"

print

)

/************* connected prototypes and references ***********/

m4_pythonfile(
# External information about connected instances.  We need to see:
# 1) codeblocks (for immediate call support)
# 2) pointers to modules (for immediate call support and for trampolines)
# 3) port information structures (for port APIs)
# 4) event trampoline functions (for reporting events)
#
# Changes here are seen by connInfoList, which is checked for
# rebuild, but types do not change
#codeBlocksSeen = {} # since we need an external signature for connections
for mi in LSEpy_miList:
  for p in mi.portOrder:
    for pci in p.connInfoList:
	if pci[LSEdb_cil_InstName]:
	    # code blocks
	    ct = pci[LSEdb_cil_CBlock]
	    nm = LSEcg_codeblock_name(LSE_db,ct,0,1)
	    if not codeBlocksSeen.has_key(nm) and ct[0] != LSEdb_CblockDynamic:
		codeBlocksSeen[nm] = 1
		print "extern " + LSEcg_codeblock_prototype(LSE_db,ct,1) + ";"
		key = ct[1]
		if not instanceDataSeen.has_key(key):
		    instanceDataSeen[key] = 1
		    print "extern class LSEfw_module %s;" % \
					LSEcg_instdata_name(key)

            # per-port
            key = pci[LSEdb_cil_InstName]
            if not instanceDataSeen.has_key(key):
		    instanceDataSeen[key] = 1
		    print "extern class LSEfw_module %s;" % \
					LSEcg_instdata_name(key)
            key = (pci[LSEdb_cil_InstName],
                   pci[LSEdb_cil_PortName])
            if not pinfoSeen.has_key(key):
              # port information structures
		pinfoSeen[key] = 1
		print "extern struct LSEfw_portinst_info_s<%s> %s[]; " % \
                      (p.type,
                       LSEcg_portinst_info_name(pci[LSEdb_cil_InstName],
                                                pci[LSEdb_cil_PortName]))

del codeBlocksSeen
del instanceDataSeen
del pinfoSeen
print
)

/*************** referenced through queries ***************************/

m4_pythonfile(if 1:
  LSEcg_print_query_externs(LSE_db,LSEpy_minstance)
  if 0: 
    for mi in LSEpy_miList:
      LSEcg_print_query_externs(LSE_db,mi)
)

LSEm4_push_diversion(-1)

/**************** BEGIN port API functions ************************/
/*  These functions are *NOT* available to control functions, but */
/*  are available to user functions....                           */
/******************************************************************/


m4_dnl LSEm4_PORT_name 
m4_dnl   get the port name out of $1
m4_define(LSEm4_PORT_name,m4_regexp($1,^\([^[]*\),\1))

m4_dnl LSEm4_PORT_build_name
m4_dnl   basically $1 __ instance __ port name in ($2)
m4_define(LSEm4_PORT_build_name,
          LSE_module::$1__LSEm4_PORT_name($2))

/*********************************************************************
 * Here we define a variety of Python and m4 macros for doing pieces *
 * of the port APIs                                                  *
 *********************************************************************/

m4_pythonfile(

def get_cno(block):
    cno = LSE_db.cblocksBacklist[block]
    if 0 or LSE_db.getParmVal("LSE_specialize_codeblock_numbers"):
        cbn = "(%d)" % cno
    else:
        cbn = "(LSEfw_CBBASE+%d)" % \
              (cno - LSE_db.getInst(block[1]).firstCblock)
    return cbn

########## useful stuff for scheduling ##############

def start_calls_apis(i):
	# can be even more accurate by looking for API calls
	cp = i.codepoints["start_of_timestep"]
	return i.start or not cp.isEmpty

def lec_calls(istring, sflag):
    return """
    if (%s & 1) {
      int level = %s[0].blevel;
      int fno = %s[0].fno;
      LSEfw_schedule_LEC_t *head;
      
      LSEfw_schedule_mutex.lock();
      if (!LSEfw_schedule_LEC_flags[fno]) {
        LSEfw_schedule_LEC_flags[fno]=1;
        head = LSEfw_schedule_LEC_levels[level].head;
        head->cno = %s[0].cno;
        head->fno = fno;
        if (LSEfw_PARM(LSE_debug_codeblock_calls))
           fprintf(LSE_stderr,"Scheduling %%d@%%d from %%d@%%d\\n",
                   head->cno, level,
                   LSEfw_schedule_calling_cblock,
                   LSEfw_schedule_LEClevel);
        if (level <= LSEfw_schedule_LEClevel) {
          LSEfw_schedule_LECrunagain = TRUE;
        }
        LSEfw_schedule_LEC_levels[level].head = head->next;
      }
      LSEfw_schedule_mutex.unlock();
    }
    if (%s & 2) {
      int level = %s[1].blevel;
      int fno = %s[1].fno;
      LSEfw_schedule_LEC_t *head;
      
      LSEfw_schedule_mutex.lock();
      if (!LSEfw_schedule_LEC_flags[fno]) {
        LSEfw_schedule_LEC_flags[fno]=1;
        head = LSEfw_schedule_LEC_levels[level].head;
        head->cno = %s[1].cno;
        head->fno = fno;
        if (LSEfw_PARM(LSE_debug_codeblock_calls))
           fprintf(LSE_stderr,"Scheduling %%d@%%d from %%d@%%d\\n",
                   head->cno, level,
                   LSEfw_schedule_calling_cblock,
                   LSEfw_schedule_LEClevel);
        if (level <= LSEfw_schedule_LEClevel) {
          LSEfw_schedule_LECrunagain = TRUE;
        }
        LSEfw_schedule_LEC_levels[level].head = head->next;
      }
      LSEfw_schedule_mutex.unlock();
    }
    """ % (sflag, istring, istring, istring,
           sflag, istring, istring, istring)
              
###########################
# schedule_other_code
#   pname = port name
#   instno = string with our port instance number -- may not be known at 
#            compile time...
#   fromhandler = called from a handler/phase function (i.e. and API)
#                 if 0, called from a firing function
#   hasfollow = there are additional things following in the code block
#               (so we cannot forget the code block)
#
#   This routine is used in both API calls and firing functions to schedule
#   things on the other side of connections.
###########################

def schedule_other_code(pname,instno,fromhandler,hasfollow,sflag):

    iname = LSEpy_minstance.name
    p = LSE_db.getPort(iname,pname)

    # figure out scheduling

    if LSEpy_acyclic: schedpart = "1"
    elif (p.otherSchedCode < 0): # do not know
        schedpart = LSEcg_portinst_ginfo_name("",pname)+\
                    ("[%s].schedcode" % instno)
    else: 
        schedpart = "%d" % p.otherSchedCode

    # if all destinations the same with a regular pattern, figure out 
    # constants, otherwise, use variables

    if p.connectionStride and LSEpy_doISpecialize:
        cblock = p.connInfoList[0][LSEdb_cil_CBlock]
        
        # detect the null code block
        if not LSE_db.cblocksBacklist[cblock]:
            print "/* following code block is null */"
            return

        # since we have to do indirection to get a code block number,
        # we might as well use the information we already have...
        pigin = LSEcg_portinst_ginfo_name("",pname)
        cnostring = pigin + "[%s].cno" % instno

        # if following is static, nothing to do
        if p.otherSchedCode == 0 and not LSEpy_acyclic: 
            print "/* following code block statically scheduled */"
            if LSEpy_nonoblivious:
                print "LSEfw_schedule_flags[%s] = -1;" % cnostring
            return

        funcname = LSEcg_codeblock_name(LSE_db,cblock,0,1)
        firingstring = "&" + funcname
        # because we do not have a forward reference to ourselves
	if p.connInfoList[0][LSEdb_cil_InstName]==LSEpy_instance.name:
            inststring = "this"
	else:
            inststring = "&" + LSEcg_instdata_name(p.connInfoList[0]\
                                                   [LSEdb_cil_InstName])
        instnostring = "%d+%d*%s" % (p.connInfoList[0][LSEdb_cil_PortInstno],
                                     p.connectionStride,instno)

    else:

        piin = LSEcg_portinst_info_name("",pname)
        pigin = LSEcg_portinst_ginfo_name("",pname)
        firingstring = pigin + "[%s].firing" % instno
        instnostring = pigin + "[%s].instno" % instno
        cnostring = pigin + "[%s].cno" % instno
	inststring = pigin + "[%s].ptr" % instno

        # if following is static, nothing to do
        if p.otherSchedCode == 0 and not LSEpy_acyclic:
            print "/* following code block statically scheduled */"
            if LSEpy_nonoblivious:
                print "LSEfw_schedule_flags[%s] = -1;" % cnostring
            return

    levstring = pigin + "[%s].LECo" % instno

    # can we just ignore this scheduling?  we can if it is an input and
    # we know that the other side never uses the ack -- right now we only
    # check this if all connections are regular.

    if (p.connectionStride and LSEpy_doISpecialize and 
        not p.connInfoList[0][LSEdb_cil_UsesSignal]):
        return

    print "/* schedule other side */"

    # if anything to call is static, check it

    if LSEpy_acyclic:
        print "if (LSEfw_schedule_in_acyclic_finish) {"
    elif p.otherAnyStatic:
        print "if (%s) {" % schedpart

    # look for conditions that prevent immediate scheduling

    condition = []

    # Forced to be dynamic or unknown; only do this check if there is
    # known to be something dynamic out there.

    if (p.otherAnyDynamic and (p.otherSchedCode == 1 or p.otherSchedCode<0)): 
        condition.append("(" + schedpart + "==1)")

    canimm = not LSEpy_threaded and (LSEpy_fstyle or LSEpy_hstyle) and not \
             LSEpy_acyclic

    # If there is some kind of start function, it might call APIs, and so
    # we must check for being in phase_start, since that prevents immediate
    # scheduling.
    if (canimm and start_calls_apis(LSEpy_instance)): 
        condition.append("LSEfw_schedule_in_phase_start")

    # figure out what to print
    doif = condition != []
    dofalse = 1
    if not canimm: # only allow dynamic
        dofalse = 0
	doif = 0
        print "{"
	if LSEpy_dsstyle == 0:
            print "  LSEfw_schedule_append(&LSEfw_schedule_timestep,%s);" % \
                  cnostring
        else:
            print lec_calls(levstring, sflag)
        print "}"

    if (doif):
        print "if (%s) {" % reduce(lambda x,y:x+"||"+y,condition)
	if LSEpy_dsstyle == 0:
            print "  LSEfw_schedule_append(&LSEfw_schedule_timestep,%s);" % \
                  cnostring
        else:
            print lec_calls(levstring, sflag)
        print "} else"
    if (dofalse):
        print "{"
        print "  int t = %s;" % cnostring;
        scb="LSEfw_schedule_calling_cblock"
        if (hasfollow):
            print "  int save_cblock = %s;" % scb;
        print """  if (LSEfw_PARM(LSE_debug_codeblock_calls))
                    fprintf(LSE_stderr,
               "Immediately calling code block %%d from %%d\\n",t,%s);""" % scb
        print "  %s = t;" % scb
        print """ if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram))
                    LSEfw_cblock_histogram[t]++;"""

        if (p.otherCBlockTypeCode == LSEdb_CblockPhase):
            print "  (*(LSEfw_phase_t)(%s))(%s LSEfw_tf_callc_void);" % \
                  (firingstring,inststring)
        elif (p.otherCBlockTypeCode == LSEdb_CblockHandler):
            print "  (*(LSEfw_handler_t)(%s))(%s LSEfw_tf_callc %s);" % \
                  (firingstring,inststring,instnostring)
        else:
            print "  (*(LSEfw_firing_t)(%s))(%s LSEfw_tf_callc %s);" % \
                  (firingstring,inststring,instnostring)
        if (hasfollow):
            print "  %s = save_cblock;" % scb
        print "}"

    # close brace

    if (p.otherAnyStatic or LSEpy_acyclic):
        if LSEpy_acyclic:
            scb="LSEfw_schedule_calling_cblock"
            print """} else {
              int t = %s;
              LSEfw_schedule_flags[t] = -1;
              if (LSEfw_PARM(LSE_debug_codeblock_calls))
                fprintf(LSE_stderr, "Marking codeblock %%d from %%d\\n",t,%s);
            } """ % (cnostring, scb)
        elif LSEpy_nonoblivious:
            print """} else {
            LSEfw_schedule_flags[%s] = -1;
            } """ % cnostring
        else: print "}"
        
###########################
# schedule_our_firing:
#   iname = instance name
#   pname = port name
#   instno = string with the instance number -- may not be known at 
#            compile time...
#   hasfollow = there are additional things following in the code block
#               (so we cannot forget the code block)
#
#   This routine is only used in API calls when the firing is not empty
###########################

def schedule_our_firing(pname,instno,sflag): 
    
    iname = LSEpy_instance.name
    p = LSEpy_minstance.getPort(pname)

    # Base code block for the firings...
    cblock = (LSEdb_CblockFiring,iname,pname,0)
    cno = get_cno(cblock)

    # We need only save/restore the code block if we can end up doing
    # queries or user points calling queries from the firing function OR
    # we might call back to a handler immediately.
    #
    # Port queries in the control function are dealt with by plugging
    # in the context number more directly, so they do not force this
    hasfollow = p.getControlPoint().queryReachable or \
                p.handler and p.selfSchedCode>=2

    # Figure out scheduling info

    code = p.fireSchedCode
    if LSEpy_acyclic: schedpart = "1"
    elif (code < 0):
        schedpart = LSEcg_portinst_ginfo_name("",p.name) +\
                    "[%s].fschedcode" % instno
    else: schedpart = "%d" % code

    # do nothing if all invocations of the firing are static

    if (code == 0 and not LSEpy_acyclic): 
	print "/* following code block statically scheduled */"
        if LSEpy_nonoblivious:
            print "LSEfw_schedule_flags[%s+%s] = -1;" % (cno,instno)
	return

    # some static, but mixed
    if LSEpy_acyclic: print "if (LSEfw_schedule_in_acyclic_finish) {"
    elif (p.fireAnyStatic): print "if (%s) {" % schedpart

    # figure out conditions to be dynamic (vs. immediate)

    condition=[]

    # Forced to be dynamic or unknown

    if (code == 1 or code < 0): 
        condition.append("(" + schedpart + "==1)")

    canimm = not LSEpy_threaded and (LSEpy_fstyle or LSEpy_hstyle) and \
             not LSEpy_acyclic

    # If there is some kind of start function, it might call APIs, and so
    # we must check for being in phase_start, since that prevents immediate
    # scheduling.

    if (canimm and start_calls_apis(p.inst)): 
        condition.append("LSEfw_schedule_in_phase_start")

    # Figure out what things to print

    doif = (condition != [] and condition[0] != "1") and not LSEpy_threaded
    dotrue = doif or (condition != [] and condition[0] == "1") or \
             LSEpy_threaded or LSEpy_acyclic
    dofalse = doif or not (condition != [] and condition[0] == "1") \
               and not LSEpy_threaded and not LSEpy_acyclic
    
    # And print....

    if (doif):
        print "if (%s) " % reduce(lambda x,y: x+"||"+y,condition)

    if (dotrue):
        if LSEpy_dsstyle == 0:
            print "{ LSEfw_schedule_append(&LSEfw_schedule_timestep,%s+%s); }"\
                  % (cno, instno)
        else:
            pigin = LSEcg_portinst_ginfo_name("",pname)
            levstring = pigin + "[%s].LECf" % instno
            print "{ %s }" % lec_calls(levstring, sflag)
            
    if (doif): print "else"

    if (dofalse):
        print "{"
        print "  int t = %s + %s;" % (instno, cno)
	scb="LSEfw_schedule_calling_cblock"
        print """if (LSEfw_PARM(LSE_debug_codeblock_calls))
                  fprintf(LSE_stderr,
                      "Immediately calling code block %%d from %%d\\n",
                          t, %s);""" % scb
        if (hasfollow):
            print "  int save_cblock = %s;" % scb
            print "  %s = t;" % scb
        print """if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram))
                  LSEfw_cblock_histogram[t]++;"""
        print "%s(LSEfw_f_call %s);" % \
	         (LSEcg_codeblock_name(LSE_db,cblock,0,0),instno)
        if (hasfollow):
            print "  %s = save_cblock;" % scb
        print "}"

    if p.fireAnyStatic or LSEpy_acyclic: # some static, but mixed
        if LSEpy_nonoblivious or LSEpy_acyclic:
            print """} else {
            LSEfw_schedule_flags[%s+%s] = -1;
            }""" % (cno,instno)
        else:
            print "}"
            
###########################
# schedule_our_code:
#   pname = port name
#   instno = string with the instance number -- may not be known at 
#            compile time...
#   hasfollow = there are additional things following in the code block
#               (so we cannot forget the code block) - redundant for now
#
#   This routine is only used by firing functions after they have
#   called the control function.  As such, they have no more need of their
#   code block number and do not need to remember it.  Immediate callers of
#   firing functions (basically API calls) must be sure to save/restore the
#   code block number if there is a handler and the self-scheduling code is
#   immediate
#   
###########################

def schedule_our_code(pname,instno,sflag):

    iname = LSEpy_instance.name
    p = LSEpy_minstance.getPort(pname)
    hasfollow = 0 # we do not need to remember our code block number

    if p.schedIndependent: return
    if (not p.selfSchedCode and not LSEpy_acyclic): 
	print "/* following code block statically scheduled */"
        if LSEpy_nonoblivious:
            if p.handler:
                cblock = (LSEdb_CblockHandler,iname,pname,0)
                cno = get_cno(cblock) + ("+%s" % instno)
            elif LSEpy_instance.phase:
                cno = get_cno(LSEdb_phaseKey(LSEpy_instance.name))
            else: return
            print "LSEfw_schedule_flags[%s] = -1;" % cno
	return

    if LSEpy_acyclic:
        print "if (LSEfw_schedule_in_acyclic_finish) {"
        
    if (p.handler):
        cblock = (LSEdb_CblockHandler,iname,pname,0)
        cno = get_cno(cblock)
        cno = cno + ("+%s" % instno)
        if (p.selfSchedCode==1 or LSEpy_acyclic):
            if LSEpy_dsstyle == 0:
                print "  LSEfw_schedule_prepend(&LSEfw_schedule_timestep," \
                                                "%s);" % cno
            else:
                pigin = LSEcg_portinst_ginfo_name("",pname)
                levstring = pigin + "[%s].LECs" % instno
                print lec_calls(levstring, sflag)
        if p.selfSchedCode>=2:
            print "{"
            print "  int t = %s;" % cno
	    scb="LSEfw_schedule_calling_cblock"
            if hasfollow:
                print "int save_cblock = %s;" % scb
            print """ if (LSEfw_PARM(LSE_debug_codeblock_calls))
                       fprintf(LSE_stderr,
                   "Immediately calling code block %%d from %%d\\n",t,%s);""" \
                   % scb
            print "%s = t;" % scb
            print """ if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram))
                    LSEfw_cblock_histogram[t]++;"""
            print LSEcg_codeblock_name(LSE_db,(LSEdb_CblockHandler,
                                        LSEpy_instance.name,
                                        pname,0),0,1) + \
                                        ("(this LSEfw_tf_callc %s)" \
	                                % instno) + ";"
            if hasfollow: print "%s = save_cblock;" % scb
            print "}"
    else: # we know it will not be immediate
        if LSEpy_instance.phase:
            # our own code block number
            cno = get_cno(LSEdb_phaseKey(LSEpy_instance.name))
            if LSEpy_dsstyle == 0:
                print "LSEfw_schedule_append(&LSEfw_schedule_timestep,%s);" \
                      % cno
            else:
                pigin = LSEcg_portinst_ginfo_name("",pname)
                levstring = pigin + "[%s].LECs" % instno
                print lec_calls(levstring, sflag)

    if LSEpy_acyclic:
        print """} else {
        LSEfw_schedule_flags[%s] = -1;
        }""" % cno

checkunk = LSE_db.getParmVal("LSE_check_ports_for_unknown")
tpr = LSE_db.getParmVal("LSE_check_ports_trace_resolution")

def schedule_port_resolution(pname, level, piece, ino):
    if level=="global":
        p = LSEpy_instance.ports[pname]
        if p.dir == "output" and piece=="data": igno = 0
        elif p.dir == "output" and piece=="enable": igno = 1
        elif p.dir == "input" and piece=="ack": igno = 2
        else: igno = -1
	if igno >= 0:
            maskany = 0
            maskall = 7
            for mi in LSEpy_miList:
                p2 = mi.ports[pname]
                for pic in p2.connInfoList:
                    if pic[LSEdb_cil_InstName]:
                        maskany = maskany | pic[LSEdb_cil_IgnoreMask] | \
                                  pic[LSEdb_cil_ConstantMask]
                        maskall = maskall & (pic[LSEdb_cil_IgnoreMask] | \
                                             pic[LSEdb_cil_ConstantMask])
                    else:
                        maskany = 7
            anyignore = maskany & (1<<igno)
            allignore = maskall & (1<<igno)
         
            # note: if LSE_check_ports_for_unknown is runtimed, it is better
            # to just go ahead and decrement the count rather than do an if.
            # Because runtimed vars show up as True in Python, we do not need
            # to do a special test for runtiming...
            if checkunk and not allignore: 
                if anyignore:
                    print "if (!(%s[%s].ignoremask & (1<<%d))) {" % \
                          (LSEcg_portinst_ginfo_name("",pname),
                           ino,igno)

                if tpr: 
                    print "if (%s)" % tpr
                    print "  sigs_resolved[-1-" \
                          "(unknown_signal_count-1)]=%s[%s].signo[%d];" \
                          % (LSEcg_portinst_ginfo_name("",pname),ino,igno)
		      	
                print "unknown_signal_count--;"

		if anyignore: print "}"

    cp=reduce(lambda x,y: x or y.calledby.get((level+"."+piece,pname)),
              LSEpy_miList,0)
    
    if (LSEpy_acyclic): rc = 1 # force wakeup call
    elif level == "global":
        rc = LSEpy_instance.ports[pname].globalResolveCode
    else:
        rc = LSEpy_instance.ports[pname].localResolveCode
    if cp and rc:
        if rc < 0:
            print "if (%s[%s].%sresolvecode) {" % \
                  (LSEcg_portinst_info_name("",pname),ino,level)
        else: print "{"
        print "  LSEfw_port_query_wakeup(&" + \
              LSEcg_port_wakeup_ref("", pname, ino, level + "." + piece) + ");"
        print "}"

sopr_pieces = { "data": 0, "enable" : 1, "ack" : 2, "any" : 3 }

def schedule_other_port_resolution(pname, piece, localino):
    index = sopr_pieces.get(piece)
    doit = 0 # LSEpy_acyclic # force wakeup call
    for mi in LSEpy_miList:
        doit = reduce(lambda x,y,index=index:x or \
                      y[LSEdb_cil_PortCallers][index],
                      mi.getPort(pname).connInfoList,doit)
    if not doit: return

    # no point in looking up globalresolvecode of other port since wakeup
    # structure will do just as well
    print """{
    LSEfw_query_instance_t *qi = &%s[%s].other_info->wakeups.wu[%d];
    if (qi->index != -1) /* valid */
      LSEfw_port_query_wakeup(qi);
  }""" % (LSEcg_portinst_ginfo_name("",pname),localino,
				   LSEcg_wunos["global",piece])

# report resolution events declared on other ports
def report_other_resolution(pname, instno, status, prevstatus, id, datap):
    doit = 0
    for mi in LSEpy_miList:
        doit = reduce(lambda x,y : x or y[LSEdb_cil_EventUses][0] or \
                      y[LSEdb_cil_EventUses][1],
                      mi.getPort(pname).connInfoList, doit)
    if not doit: return
    bdict = {}
    bdict["pii"] = LSEcg_portinst_info_name("",pname)
    bdict["pig"] = LSEcg_portinst_ginfo_name("",pname)
    bdict["instno"] = instno
    bdict["status"] = status
    bdict["prevstatus"] = prevstatus
    bdict["id"] = id
    bdict["datap"] = datap
    
    print ("""
     if (%(pig)s[%(instno)s].presolve) {
        (*%(pig)s[%(instno)s].presolve)(%(pig)s[%(instno)s].ptr LSEfw_tf_callc
                      %(pig)s[%(instno)s].instno, %(status)s, %(prevstatus)s, 
		      %(id)s,(void *)%(datap)s);
     }
     if (%(pig)s[%(instno)s].plresolve) {
        (*%(pig)s[%(instno)s].plresolve)(%(pig)s[%(instno)s].ptr LSEfw_tf_callc
                      %(pig)s[%(instno)s].instno, %(status)s, %(prevstatus)s, 
		      %(id)s,(void *)%(datap)s);
     }
    """ % bdict)
)

m4_pythonfile(

######## Access to other-side port status ################

def set_other_status(pname,ino,nstat):
    p = LSEpy_instance.getPort(pname)
    if p.connectionStride and LSEpy_doISpecialize:
        print (LSEcg_portstatus_name(p.connInfoList[0][LSEdb_cil_InstName],
                                     p.connInfoList[0][LSEdb_cil_PortName],
                                     LSEpy_indir_pstatus) + \
               "[%d+%d*(%s)].global = %s;" % 
	        (p.connInfoList[0][LSEdb_cil_PortInstno],
                p.connectionStride,ino,nstat))
    else:
        print "*(" + LSEcg_portinst_ginfo_name("",pname) + \
              "[%s].ostatus) = %s;" % (ino,nstat)
)
    
/************************** GENERATORS *******************************/

m4_pythonfile(
def check_port_constant(iname,pname,stype):
    p = LSE_db.getPort(iname,pname)
    # note.... this does not have rebuild implications because constant
    # status of these signals (control func driven signals are the only
    # only ones used here) only change constant status when the functions
    # change....
    return int(p.signals[p.width * LSEdb_sigtype_to_offset[stype]].constant
               != 0)

################# Code for an unconnected port ###########################

def gen_noconn_port(p):
    print "/* port %s not connected */" % p.name
    print """inline %s {
        return (LSE_signal_nothing|LSE_signal_enabled|LSE_signal_ack);
      } """ % LSEcg_portapi_proto(p,"GET_DATA","LSE_module")
    if p.dir == "output":
        print "inline %s {} " % LSEcg_portapi_proto(p,"SET_DATAEN","LSE_module")
    else:
        print "inline %s {} " % LSEcg_portapi_proto(p,"SET_ACK","LSE_module")
    print
)m4_dnl Lots of code definitions

m4_dnl  ************************** EMPTY OUTPUT ****************************
m4_dnl  ****
m4_dnl  **** Code for an output port with an empty control function
m4_dnl  ****

m4_define(LSEm4_gen_empty_output_port,

m4_dnl
m4_define(LSEm4_pstat_name,
m4_python(print LSEcg_portstatus_name("","$2",1)))m4_dnl
m4_dnl
m4_define(LSEm4_pii_name,
m4_python(print LSEcg_portinst_info_name("","$2")))m4_dnl
m4_define(LSEm4_pig_name,
m4_python(print LSEcg_portinst_ginfo_name("","$2")))m4_dnl
m4_dnl

m4_dnl  ******* APIs for output ports

m4_python(print LSE_db.getParmVal("LSE_inline_port_apis")) void
LSEm4_PORT_build_name(LSE_MPORT_SET_DATAEN,$2)(
LSEfw_f_def int instno, LSE_signal_t stat,LSE_dynid_t id, $4 *datap)
{
  LSE_signal_t status,rstatus,pstatus, pstatusSF, rstatusSF;
  struct LSEfw_portinst_info_s< $4> *pii;
  struct LSEfw_portinst_ginfo_s< $4> *pig;
  int sflag;

#if (m4_python(print LSEpy_checkAPIs))
  if (instno < 0 || instno > $3) {
    LSE_report_err("Port index out of range in "
		     "LSE_port_set($2[%d]...)",instno);
    return;
  }
#endif

  pii = &(LSEm4_pii_name[instno]);
  pig = &(LSEm4_pig_name[instno]);
  pstatusSF = LSEm4_pstat_name[instno].global;
  pstatus = LSEfw_readstatus(pstatusSF);

  /* if send already occurred, do not repeat it */
  if (stat & ~pstatus & LSEty_signal_data_known_mask) {

    status = stat & LSEty_signal_data_mask;
    rstatus = (pstatus | status);
    rstatusSF = (pstatusSF & ~LSEty_signal_double_data_mask) | 
                 (status << LSEfw_signal_time_mask);

    /* store data.. */
    if (LSE_signal_data_present(stat)) {
        pig->other_info->id = pii->id = id;
        m4_ifelse($4,LSE_type_none,,
        pii->datap = pig->other_info->datap = datap;
        )m4_dnl
#if (m4_python(print LSEpy_checkAPIs))
        if (!id) {
          LSE_report_err("Null dynid with data in "
		         "LSE_port_set($2[%d]...)",instno);
        }
#endif
    }

    /* if enable/disable already occurred, do not update */
    if (stat & ~pstatus & LSEty_signal_enable_known_mask) {
      status |= stat & LSEty_signal_enable_mask;
      rstatus = (rstatus | status);
      rstatusSF = (rstatusSF & ~LSEty_signal_double_enable_mask) |
                  (status << LSEfw_signal_time_mask);

      LSEm4_pstat_name[instno].global = rstatusSF;
      m4_pythonfile(set_other_status("$2","instno","rstatusSF"));

      m4_pythonfile(schedule_port_resolution("$2","global",
                                             "enable","instno"));
      m4_pythonfile(schedule_port_resolution("$2","global","any","instno"));
      m4_pythonfile(schedule_other_port_resolution("$2","enable","instno"));
      m4_pythonfile(schedule_other_port_resolution("$2","any","instno"));
      sflag = 3;
    } else {
        LSEm4_pstat_name[instno].global = rstatusSF;
        m4_pythonfile(set_other_status("$2","instno","rstatusSF"));
        sflag = 1;
    }

    m4_pythonfile(schedule_port_resolution("$2","global","data","instno"));
    m4_pythonfile(schedule_port_resolution("$2","global","any","instno"));
    m4_pythonfile(schedule_other_port_resolution("$2","data","instno"));
    m4_pythonfile(schedule_other_port_resolution("$2","any","instno"));

    LSE_event_record($2.localresolved,instno,rstatus,pstatus,id,datap);
    LSE_event_record($2.resolved,instno,rstatus,pstatus,id,datap); 
    m4_pythonfile(report_other_resolution("$2","instno", "rstatus", "pstatus",
                                          "id", "datap"))
  }
    /* if enable/disable already occurred, drop out quickly */
  else if (!(stat & ~pstatus & LSEty_signal_enable_known_mask)) {
      return;
  } else {
    status = stat & LSEty_signal_enable_mask;
    rstatus = (pstatus | status);
    rstatusSF = (pstatusSF & ~LSEty_signal_double_enable_mask) |
                (status << LSEfw_signal_time_mask);

    LSEm4_pstat_name[instno].global = rstatusSF;
    m4_pythonfile(set_other_status("$2","instno","rstatusSF"));

    m4_pythonfile(schedule_port_resolution("$2","global","enable","instno"));
    m4_pythonfile(schedule_port_resolution("$2","global","any","instno"));
    m4_pythonfile(schedule_other_port_resolution("$2","enable","instno"));
    m4_pythonfile(schedule_other_port_resolution("$2","any","instno"));

    LSE_event_record($2.localresolved,instno,rstatus,pstatus,
                     LSEm4_pii_name[instno].id,LSEm4_pii_name[instno].datap);
    LSE_event_record($2.resolved,instno,rstatus,
                     pstatus,
                     LSEm4_pii_name[instno].id,LSEm4_pii_name[instno].datap);

    m4_pythonfile(report_other_resolution("$2","instno","rstatus","pstatus",
                                          "LSEm4_pii_name[instno].id",
                                          "LSEm4_pii_name[instno].datap"))
    sflag = 2;
  }

  /* wake up other side */
  m4_pythonfile(schedule_other_code("$2","instno",1,1,"sflag"))
}

m4_python(
print LSE_db.getParmVal("LSE_inline_port_apis")) LSE_signal_t
LSEm4_PORT_build_name(LSE_MPORT_GET_DATA,$2)(
int instno, LSE_dynid_t *id, $4 **datap) 
{

#if (m4_python(print LSEpy_checkAPIs))
  if (instno < 0 || instno > $3) {
    LSE_report_err("Port index out of range in "
                     "LSE_port_get_data($2[%d])",instno);
    return (LSE_signal_nothing|LSE_signal_disabled|LSE_signal_ack);
  }
#endif

  /* get the data */
  if (id != NULL) *id = LSEm4_pii_name[instno].id;
  /* get the data */
  m4_ifelse($4,LSE_type_none,
    	    if (datap != NULL) *datap = &LSEfw_none_ptr;,
    	    if (datap != NULL) *datap = LSEm4_pii_name[instno].datap;)
  return LSEfw_readstatus(LSEm4_pstat_name[instno].global);
} 

) m4_dnl LSEm4_gen_empty_output_port

m4_dnl  ************************** OUTPUT **********************************
m4_dnl  ****
m4_dnl  **** Code for an output port with a non-empty control function
m4_dnl  ****

m4_define(LSEm4_gen_controlled_output_port,

m4_dnl
m4_define(LSEm4_pstat_name,
m4_python(print LSEcg_portstatus_name("","$2",1)))
m4_dnl
m4_define(LSEm4_pii_name,
m4_python(print LSEcg_portinst_info_name("","$2")))
m4_define(LSEm4_pig_name,
m4_python(print LSEcg_portinst_ginfo_name("","$2")))m4_dnl
m4_dnl

	/*****************************************
 	 * This is the really important function *
	 *****************************************/

m4_python(print LSE_db.getParmVal("LSE_inline_port_firings") + \
		" " + \
	        LSEcg_codeblock_prototype(LSE_db,(LSEdb_CblockFiring,
                                          LSEpy_instance.name,"$2",0),0,
					  "LSE_module"))
{
  LSE_signal_t cntldec;
  LSE_signal_t preinstat, preoutstat, postinstat, postoutstat, decstat;
  boolean rundata, runenable, checkdata, checken;
  int sflag=0;

  LSE_signal_t preinstatSF, preoutstatSF;

  preoutstatSF = LSEm4_pstat_name[instno].global;
  preinstatSF = LSEm4_pstat_name[instno].local;
  preoutstat = LSEfw_readstatus(preoutstatSF);
  preinstat = LSEfw_readstatus(preinstatSF);

  cntldec = m4_python(
    print LSEcg_codefunc_name_str(LSEpy_minstance.name,"$2",
                                  LSEdb_PointControl))(LSEfw_f_call
				  instno,
				    preinstat,preoutstat,
				    LSEm4_pii_name[instno].id,
				    LSEm4_pii_name[instno].datap);

  /****************************************************************
   *  Now, what needs to go?  Only if there have been changes should
   *  we go...
   *
   * NOTE: if the control function call can do things which make other
   * control functions run and this process continues until this 
   * function runs again, prestatus cannot be used.
   ****************************************************************/

  decstat = LSEty_port_status_firing_and(preinstat, cntldec);

  m4_ifelse(m4_python(check_port_constant("$1","$2",LSEdb_sigtype_ofdata))
            ,1,
  rundata=FALSE; /* constant data */,
  if ((rundata = (~preoutstat & decstat & LSEty_signal_data_known_mask))) {
      m4_pythonfile(schedule_port_resolution("$2","global","data","instno"));
      m4_pythonfile(schedule_other_port_resolution("$2","data","instno"));
      sflag = 1;
    })

  m4_ifelse(m4_python(check_port_constant("$1","$2",LSEdb_sigtype_ofen)),1,
  runenable=FALSE; /* constant enable */,
  if ((runenable = (~preoutstat & cntldec & LSEty_signal_enable_known_mask))) {
    m4_pythonfile(schedule_port_resolution("$2","global","enable","instno"));
    m4_pythonfile(schedule_other_port_resolution("$2","enable","instno"));
    sflag |= 2;
  })
  
  if (rundata | runenable) {

    /*********** Forward through output port ***************/

    postoutstat = LSEty_status_merge(preoutstat,
 				     (decstat & LSEty_signal_data_mask) |
				     (cntldec & LSEty_signal_enable_mask));

    LSE_signal_t smask = (rundata ? LSEty_signal_double_data_mask : 0) |
    		         (runenable ? LSEty_signal_double_enable_mask : 0);
    LSE_signal_t rstatusSF = (preoutstatSF & ~smask) |
    		 	     (postoutstat << LSEfw_signal_time_mask);

    LSEm4_pstat_name[instno].global = rstatusSF;
    m4_pythonfile(set_other_status("$2","instno","rstatusSF"));
    
    /* These next two if statements will be empty if there is no one
     * querying this port
     */

      m4_pythonfile(schedule_port_resolution("$2","global","any","instno"));
      m4_pythonfile(schedule_other_port_resolution("$2","any","instno"));
    
      LSE_event_record($2.resolved,instno,postoutstat,preoutstat,
	               LSEm4_pii_name[instno].id,LSEm4_pii_name[instno].datap);

      m4_pythonfile(report_other_resolution("$2", "instno", "postoutstat",
                                            "preoutstat", 
                                            "LSEm4_pii_name[instno].id",
	                                    "LSEm4_pii_name[instno].datap"))
      m4_pythonfile(schedule_other_code("$2","instno",0,0,"sflag"))

      /* we may have been fired again by the other code, in which case ack
       * is already handled by that invocation.  To prevent an extra
       * ack handling, we reload the preinstat with the current local stat...
       */
      preinstatSF = LSEm4_pstat_name[instno].local;
      preinstat = LSEfw_readstatus(preinstatSF);
    
  } /* if (condition true to run forward) */

  m4_ifelse(m4_python(check_port_constant("$1","$2",LSEdb_sigtype_ofack)),1,
  /* constant ack */,
  if (~preinstat & cntldec & LSEty_signal_ack_known_mask) {
    boolean cond;

    /*********** Backward from output port ***************/

      /* do not need to be clever here... */
      postinstat = preinstat | (cntldec & LSEty_signal_ack_mask);

      LSE_signal_t smask = LSEty_signal_double_ack_mask;
      LSEm4_pstat_name[instno].local = (preinstatSF & ~smask) |
       		 	               (postinstat << LSEfw_signal_time_mask);

      m4_pythonfile(schedule_port_resolution("$2","local","ack","instno"));
      m4_pythonfile(schedule_port_resolution("$2","local","any","instno"));

      LSE_event_record($2.localresolved,instno,postinstat,preinstat,
	               LSEm4_pii_name[instno].id,LSEm4_pii_name[instno].datap);

      m4_pythonfile(schedule_our_code("$2","instno","1"))
  } /* if can run backwards */)

}

m4_dnl  ******* APIs for output ports

m4_python(print LSE_db.getParmVal("LSE_inline_port_apis")) void
LSEm4_PORT_build_name(LSE_MPORT_SET_DATAEN,$2)(
LSEfw_f_def int instno, LSE_signal_t stat,LSE_dynid_t id, $4 *datap)
{
  LSE_signal_t status, rstatus, pstatus, pstatusSF, rstatusSF;
  struct LSEfw_portinst_info_s< $4> *pii;
  struct LSEfw_portinst_ginfo_s< $4> *pig;
  int sflag;

#if (m4_python(print LSEpy_checkAPIs))
  if (instno < 0 || instno > $3) {
    LSE_report_err("Port index out of range in "
		     "LSE_port_set($2[%d]...)",instno);
    return;
  }
#endif

  pii = &(LSEm4_pii_name[instno]);
  pig = &(LSEm4_pig_name[instno]);
  pstatusSF = LSEm4_pstat_name[instno].local;
  pstatus = LSEfw_readstatus(pstatusSF);

  /* if send already occurred, do not repeat it */
  if (stat & ~pstatus & LSEty_signal_data_known_mask) {

    status = stat & LSEty_signal_data_mask;
    rstatus = (pstatus | status);
    rstatusSF = (pstatusSF & ~LSEty_signal_double_data_mask) | 
                (status << LSEfw_signal_time_mask);

    /* store data.. */
    if (LSE_signal_data_present(stat)) {
        pig->other_info->id = pii->id = id;
        m4_ifelse($4,LSE_type_none,,
        pii->datap = pig->other_info->datap = datap;
        )m4_dnl
    }

    /* if enable/disable already occurred, do not update */
    if (stat & ~pstatus & LSEty_signal_enable_known_mask) {
      status |= stat & LSEty_signal_enable_mask;
      rstatus = (rstatus | status);
      rstatusSF = (rstatusSF & ~LSEty_signal_double_enable_mask) |
                  (status << LSEfw_signal_time_mask);

      LSEm4_pstat_name[instno].local = rstatusSF;
      m4_pythonfile(schedule_port_resolution("$2","local",
                                             "enable","instno"));
      m4_pythonfile(schedule_port_resolution("$2","local","any","instno"));
      sflag = 3;
    } else {
        LSEm4_pstat_name[instno].local = rstatusSF;
 	sflag = 1;
    }

    m4_pythonfile(schedule_port_resolution("$2","local","data","instno"));
    m4_pythonfile(schedule_port_resolution("$2","local","any","instno"));

    LSE_event_record($2.localresolved,instno,rstatus,pstatus,id,datap);
  }
    /* if enable/disable already occurred, drop out quickly */
  else if (!(stat & ~pstatus & LSEty_signal_enable_known_mask)) {
    return;
  } else {
    status = stat & LSEty_signal_enable_mask;
    rstatus = (pstatus | status);
    rstatusSF = (pstatusSF & ~LSEty_signal_double_enable_mask) |
                (status << LSEfw_signal_time_mask);

    LSEm4_pstat_name[instno].local = rstatusSF;

    m4_pythonfile(schedule_port_resolution("$2","local","enable","instno"));
    m4_pythonfile(schedule_port_resolution("$2","local","any","instno"));

    LSE_event_record($2.localresolved,instno,rstatus,pstatus,
                     LSEm4_pii_name[instno].id,LSEm4_pii_name[instno].datap);
    sflag = 2;
  }

  /* wake up other side */
  m4_pythonfile(schedule_our_firing("$2","instno","sflag"))
}

m4_python(
print LSE_db.getParmVal("LSE_inline_port_apis")) LSE_signal_t
LSEm4_PORT_build_name(LSE_MPORT_GET_DATA,$2)(
int instno, LSE_dynid_t *id, $4 **datap) 
{

#if (m4_python(print LSEpy_checkAPIs))
  if (instno < 0 || instno > $3) {
    LSE_report_err("Port index out of range in "
		     "LSE_port_get_data($2[%d])",instno);
    return (LSE_signal_nothing|LSE_signal_disabled|LSE_signal_ack);
  }
#endif

  /* get the data */
  if (id != NULL) *id = LSEm4_pii_name[instno].id;
  /* get the data */
  m4_ifelse($4,LSE_type_none,
    	    if (datap != NULL) *datap = &LSEfw_none_ptr;,
    	    if (datap != NULL) *datap = LSEm4_pii_name[instno].datap;)
  return LSEfw_readstatus(LSEm4_pstat_name[instno].local);
}

)m4_dnl LSEm4_gen_controlled_output_port


m4_dnl ************************ EMPTY INPUT ******************************
m4_dnl  ****
m4_dnl  **** Code for an input port with an empty control function
m4_dnl  ****

m4_define(LSEm4_gen_empty_input_port,

m4_define(LSEm4_pstat_name,
m4_python(print LSEcg_portstatus_name("","$2",1)))
m4_define(LSEm4_pii_name,
m4_python(print LSEcg_portinst_info_name("","$2")))
m4_define(LSEm4_pig_name,
m4_python(print LSEcg_portinst_ginfo_name("","$2")))m4_dnl
m4_dnl

m4_dnl  ************ APIs for input ports

m4_python(print LSE_db.getParmVal("LSE_inline_port_apis")) void
LSEm4_PORT_build_name(LSE_MPORT_SET_ACK,$2)
(LSEfw_f_def int instno, LSE_signal_t stat)
{
  LSE_signal_t status, rstatus, pstatus, pstatusSF, rstatusSF;

#if (m4_python(print LSEpy_checkAPIs))
  if (instno < 0 || instno > $3) {
    LSE_report_err("Port index out of range in "
		     "LSE_port_set_ack($2[%d])",instno);
    return;
  }
#endif

  /* if receive/retain already occurred, drop out quickly */
  pstatusSF = LSEm4_pstat_name[instno].global;
  pstatus = LSEfw_readstatus(pstatusSF);

  if (!(stat & ~pstatus & LSEty_signal_ack_known_mask))
    return;

  status= stat & LSEty_signal_ack_mask;
  rstatus = (pstatus | status);
  rstatusSF = (pstatusSF & ~LSEty_signal_double_ack_mask) | 
              (status << LSEfw_signal_time_mask);

  LSEm4_pstat_name[instno].global = rstatusSF;

  m4_pythonfile(set_other_status("$2","instno","rstatusSF"));

  m4_pythonfile(schedule_port_resolution("$2","global","ack","instno"));
  m4_pythonfile(schedule_port_resolution("$2","global","any","instno")); 
  m4_pythonfile(schedule_other_port_resolution("$2","ack","instno"));
  m4_pythonfile(schedule_other_port_resolution("$2","any","instno"));

  LSE_event_record($2.localresolved,instno,rstatus,pstatus,
                   LSEm4_pii_name[instno].id, LSEm4_pii_name[instno].datap);
  LSE_event_record($2.resolved,instno,rstatus,pstatus,
                   LSEm4_pii_name[instno].id, LSEm4_pii_name[instno].datap);

  m4_pythonfile(report_other_resolution("$2", "instno", "rstatus", "pstatus",
                                        "LSEm4_pii_name[instno].id",
					"LSEm4_pii_name[instno].datap"))

  m4_pythonfile(schedule_other_code("$2","instno",1,1,"1"))
}

m4_python(
print LSE_db.getParmVal("LSE_inline_port_apis")) LSE_signal_t
LSEm4_PORT_build_name(LSE_MPORT_GET_DATA,$2)(
int instno, LSE_dynid_t *id, $4 **datap) 
{

#if (m4_python(print LSEpy_checkAPIs))
  if (instno < 0 || instno > $3) {
    LSE_report_err("Port index out of range in "
		     "LSE_port_get_data($2[%d])",instno);
    return (LSE_signal_nothing|LSE_signal_disabled|LSE_signal_ack);
  }
#endif

  if (id != NULL) *id = LSEm4_pii_name[instno].id;
  /* get the data */
  m4_ifelse($4,LSE_type_none,
    	    if (datap != NULL) *datap = &LSEfw_none_ptr;,
    	    if (datap != NULL) *datap = LSEm4_pii_name[instno].datap;)

  return LSEfw_readstatus(LSEm4_pstat_name[instno].global);
}

)m4_dnl LSEm4_gen_empty_input_port


m4_dnl ************************ NONEMPTY INPUT ******************************
m4_dnl  ****
m4_dnl  **** Code for an input port with a non-empty control function
m4_dnl  ****

m4_define(LSEm4_gen_controlled_input_port,

m4_define(LSEm4_pstat_name,
m4_python(print LSEcg_portstatus_name("","$2",1)))
m4_define(LSEm4_pii_name,
m4_python(print LSEcg_portinst_info_name("","$2")))
m4_define(LSEm4_pig_name,
m4_python(print LSEcg_portinst_ginfo_name("","$2")))m4_dnl

	/*****************************************
 	 * This is the really important function *
	 *****************************************/

m4_python(print LSE_db.getParmVal("LSE_inline_port_firings") + " " + \
                LSEcg_codeblock_prototype(LSE_db,(LSEdb_CblockFiring,
                                           LSEpy_instance.name,"$2",0),0,
					  "LSE_module"))
{
  LSE_signal_t cntldec;
  LSE_signal_t preinstat, preoutstat, postinstat, postoutstat, decstat;
  boolean rundata, runenable;
  int sflag=0;
  LSE_signal_t preinstatSF, preoutstatSF;

  preoutstatSF = LSEm4_pstat_name[instno].local;
  preinstatSF = LSEm4_pstat_name[instno].global;
  preinstat = LSEfw_readstatus(preinstatSF);
  preoutstat = LSEfw_readstatus(preoutstatSF);

    cntldec = m4_python(
      print LSEcg_codefunc_name_str(LSEpy_minstance.name,"$2",
                                    LSEdb_PointControl))(LSEfw_f_call instno,
                                                     preinstat,preoutstat,
                                                     LSEm4_pii_name[instno].id,
						LSEm4_pii_name[instno].datap);

  /****************************************************************
   *  Now, what needs to go?  Only if there have been changes should
   *  we go...
   *
   * NOTE: if the control function call can do things which make other
   * control functions run and this process continues until this 
   * function runs again, prestatus cannot be used.
   ****************************************************************/

  decstat = LSEty_port_status_firing_and(preinstat, cntldec);

  m4_ifelse(m4_python(check_port_constant("$1","$2",LSEdb_sigtype_ifdata)),1,
  rundata=FALSE; /* constant data */,
  if ((rundata = (~preoutstat & decstat & LSEty_signal_data_known_mask))) {
      m4_pythonfile(schedule_port_resolution("$2","local","data","instno"));
      sflag = 1;
    })

  m4_ifelse(m4_python(check_port_constant("$1","$2",LSEdb_sigtype_ifen)),1,
  runenable=FALSE; /* constant enable */,
  if ((runenable=(~preoutstat & cntldec & LSEty_signal_enable_known_mask))) {
      m4_pythonfile(schedule_port_resolution("$2","local","enable","instno"));
      sflag |= 2;
    })

  if (rundata | runenable) {
    
     /************ Forward from input port ************/

      postoutstat = LSEty_status_merge(preoutstat,
 				       (decstat & LSEty_signal_data_mask) |
				       (cntldec & LSEty_signal_enable_mask));

      LSE_signal_t smask = (rundata ? LSEty_signal_double_data_mask : 0) |
    		         (runenable ? LSEty_signal_double_enable_mask : 0);
      LSEm4_pstat_name[instno].local = (preoutstatSF & ~smask) |
             		 	     (postoutstat << LSEfw_signal_time_mask);

      m4_pythonfile(schedule_port_resolution("$2","local","any","instno"));
      m4_pythonfile(schedule_our_code("$2","instno","sflag"))

      LSE_event_record($2.localresolved,instno,postoutstat,preoutstat,
                       LSEm4_pii_name[instno].id, 
		       LSEm4_pii_name[instno].datap);

      /* we may have been fired again by other code, in which case ack
       * is already handled by that invocation.  To prevent an extra
       * ack handling, we reload the preinstat with the current local stat...
       */
      preinstatSF = LSEm4_pstat_name[instno].global;
      preinstat = LSEfw_readstatus(preinstatSF);
  }

  m4_ifelse(m4_python(check_port_constant("$1","$2",LSEdb_sigtype_ifack)),1,
  /* constant ack - no backwards work needed */,
  if (~preinstat & cntldec & LSEty_signal_ack_known_mask) {
    /* no need to be clever here */
    postinstat = preinstat | (cntldec & LSEty_signal_ack_mask);

    /*********** Backward through input port ************/

      LSE_signal_t smask = LSEty_signal_double_ack_mask;
      LSE_signal_t rstatusSF = (preinstatSF & ~smask) |
      		   	       (postinstat << LSEfw_signal_time_mask);

      LSEm4_pstat_name[instno].global = rstatusSF;
      m4_pythonfile(set_other_status("$2","instno","rstatusSF"));
    
      m4_pythonfile(schedule_port_resolution("$2","global","ack","instno"));
      m4_pythonfile(schedule_port_resolution("$2","global","any","instno"));
      m4_pythonfile(schedule_other_port_resolution("$2","ack","instno"));
      m4_pythonfile(schedule_other_port_resolution("$2","any","instno"));

      LSE_event_record($2.resolved,instno,postinstat,preinstat,
                       LSEm4_pii_name[instno].id,
		       LSEm4_pii_name[instno].datap);

    m4_pythonfile(report_other_resolution("$2","instno", "postinstat",
                                          "preinstat",
                                          "LSEm4_pii_name[instno].id",
                                          "LSEm4_pii_name[instno].datap"))

    m4_pythonfile(schedule_other_code("$2","instno",0,0,"1"))
  })
}

m4_dnl  ************ APIs for input ports

m4_python(print LSE_db.getParmVal("LSE_inline_port_apis")) void
LSEm4_PORT_build_name(LSE_MPORT_SET_ACK,$2)
(LSEfw_f_def int instno, LSE_signal_t stat)
{
  LSE_signal_t status, rstatus, pstatus, pstatusSF, rstatusSF;

#if (m4_python(print LSEpy_checkAPIs))
  if (instno < 0 || instno > $3) {
    LSE_report_err("Port index out of range in "
		     "LSE_port_set($2[%d])",instno);
    return;
  }
#endif

  /* if receive/retain already occurred, drop out quickly */
  pstatusSF = LSEm4_pstat_name[instno].local;
  pstatus = LSEfw_readstatus(pstatusSF);

  if (!(stat & ~pstatus & LSEty_signal_ack_known_mask))
    return;

  status= stat & LSEty_signal_ack_mask;
  rstatus = (pstatus | status);
  rstatusSF = (pstatusSF & ~LSEty_signal_double_ack_mask) | 
              (status << LSEfw_signal_time_mask);
  LSEm4_pstat_name[instno].local = rstatusSF;

  m4_pythonfile(schedule_port_resolution("$2","local","ack","instno"));
  m4_pythonfile(schedule_port_resolution("$2","local","any","instno")); 

  LSE_event_record($2.localresolved,instno,rstatus,pstatus,
                   LSEm4_pii_name[instno].id,
                   LSEm4_pii_name[instno].datap);
  
  m4_pythonfile(schedule_our_firing("$2","instno","1"))
}

m4_python(
print LSE_db.getParmVal("LSE_inline_port_apis")) LSE_signal_t
LSEm4_PORT_build_name(LSE_MPORT_GET_DATA,$2)(
int instno, LSE_dynid_t *id, $4 **datap) 
{

#if (m4_python(print LSEpy_checkAPIs))
  if (instno < 0 || instno > $3) {
    LSE_report_err("Port index out of range in "
		     "LSE_port_get_data($2[%d])",instno);
    return (LSE_signal_nothing|LSE_signal_disabled|LSE_signal_ack);
  }
#endif

  if (id != NULL) *id = LSEm4_pii_name[instno].id;
  /* get the data */
  m4_ifelse($4,LSE_type_none,
  	    if (datap != NULL) *datap = &LSEfw_none_ptr;,
    	    if (datap != NULL) *datap = LSEm4_pii_name[instno].datap;)

  return LSEfw_readstatus(LSEm4_pstat_name[instno].local);
}

)m4_dnl LSEm4_gen_controlled_input_port

LSEm4_pop_diversion()m4_dnl

/********************** Port query templates ***********************/
/* These are in the global namespace so they can be shared if they happen
 * to not get inlined.  However, we expect that they will be inlined.
 * 
 * The odd order of parameters is because the sharing resolution code
 * needs to see framework parameters first.   While the signature could
 * be different for each version, I have chosen to make it the same and
 * to let the compiler do the work of optimizing away the unneeded parameters
 * as it inlines.  Note that we will get code sharing relative to one
 * of these per instance even with inlining because shared modules will not
 * have to use indirect calls to different functions.
 *
 */
/* S should be LSEfw_PORT_nodef_t or LSEfw_PORT_empty_t */

template <class T, class S, int width>
inline LSE_signal_t
LSEmi_PQUERY_global(
    S *P, 
    struct LSEfw_portinst_info_s<T> *PI,
#if (m4_python(print LSEpy_checkAPIs))
    const char *iname, const char *pname,
#endif
    unsigned int LSEfw_calling_id,
    int LSEfw_instno, int indc, LSE_signal_t mask, LSE_signal_t kmask,
    int wuno,
    LSE_dynid_t *idp, T **datapp
  ) {

#if (m4_python(print LSEpy_checkAPIs))
    if (LSEfw_instno < 0 || LSEfw_instno >= width) {
       LSEfw_report_err(iname,
                       "Port index out of range in LSE_port_query"
                       "(%s:%s[%d],...)", iname,pname,LSEfw_instno);
      return LSE_signal_unknown;
    }
#endif

    if (width == 0) 
      return mask & (LSE_signal_disabled|LSE_signal_ack|LSE_signal_nothing);

    LSE_signal_t status = LSEfw_readstatus(P[LSEfw_instno].global) & mask;

    if (!indc && ((status & mask) != mask) &&
        (m4_python(print LSEpy_acyclic) || PI[LSEfw_instno].globalresolvecode))
    {
      LSEfw_port_query_sleep(&PI[LSEfw_instno].wakeups.wu[wuno],
                             LSEfw_calling_id, LSEfw_instno);
    }

    if (idp != NULL) *idp = PI[LSEfw_instno].id;
    if (datapp != NULL) *datapp = PI[LSEfw_instno].datap;

    return status;
  }

template <class T, class S, int width>
inline LSE_signal_t
LSEmi_PQUERY_local(
    S *P, 
    struct LSEfw_portinst_info_s<T> *PI, 
#if (m4_python(print LSEpy_checkAPIs))
    const char *iname, const char *pname,
#endif
    unsigned int LSEfw_calling_id,
    int LSEfw_instno, int indc, LSE_signal_t mask, LSE_signal_t kmask,
    int wuno,
    LSE_dynid_t *idp, T **datapp
  ) {

#if (m4_python(print LSEpy_checkAPIs))
    if (LSEfw_instno < 0 || LSEfw_instno >= width) {
      LSEfw_report_err(iname,
                       "Port index out of range in LSE_port_query"
                       "(%s:%s[%d],...)",iname,pname,LSEfw_instno);
      return LSE_signal_unknown;
    }
#endif

    if (width == 0) 
      return mask & (LSE_signal_disabled|LSE_signal_ack|LSE_signal_nothing);

    LSE_signal_t status = LSEfw_readstatus(P[LSEfw_instno].local) & mask;

    if (!indc && ((status & mask) != mask) &&
        (m4_python(print LSEpy_acyclic) || PI[LSEfw_instno].localresolvecode))
    {
      LSEfw_port_query_sleep(&PI[LSEfw_instno].wakeups.wu[wuno],
                             LSEfw_calling_id, LSEfw_instno);
    }

    if (idp != NULL) *idp = PI[LSEfw_instno].id;
    if (datapp != NULL) *datapp = PI[LSEfw_instno].datap;

    return status;
  }

template <class T, int width>
inline LSE_signal_t
LSEmi_PAQUERY(
    struct LSEfw_port_alias_s<T> *pab, /* note: pointer to base of structure */
#if (m4_python(print LSEpy_checkAPIs))
    const char *iname, const char *pname,
#endif
    unsigned int LSEfw_calling_id, 
    int LSEfw_instno, int indc, LSE_signal_t mask, LSE_signal_t kmask,
    int wuno,
    LSE_dynid_t *idp, T **datapp
  ) {

#if (m4_python(print LSEpy_checkAPIs))
    if (LSEfw_instno < 0 || LSEfw_instno >= width) {
       LSEfw_report_err(iname,
                       "Port index out of range in LSE_port_query"
                       "(%s:%s[%d],...)", iname,pname,LSEfw_instno);
      return LSE_signal_unknown;
    }
#endif

    if (width == 0) 
      return mask & (LSE_signal_disabled|LSE_signal_ack|LSE_signal_nothing);

    struct LSEfw_port_alias_s<T> *pa=&pab[LSEfw_instno];
    LSE_signal_t status = LSEfw_readstatus(*(pa->globalstat)) & mask;

    if (!indc && ((status & mask) != mask) && 
        (m4_python(print LSEpy_acyclic) || pa->pinfo->globalresolvecode))
    {
      LSEfw_port_query_sleep(&(pa->pinfo->wakeups.wu[wuno]),
                             LSEfw_calling_id, LSEfw_instno);
    }

    if (idp != NULL) *idp = pa->pinfo->id;
    if (datapp != NULL) *datapp = pa->pinfo->datap;

    return status;
  }

/*************************** CLASSES ***************************************/
/* One thing that annoys me is the fact that non-integral static
 * consts cannot be initialized inside of a class.  However, it does
 * appear that gcc manages to realize that it is a constant and that
 * knows the value, so we do get reasonable constant propagation.
 * Whew!  If that had not been the case, I would have had to resort to
 * m4 or C-pre-processor macros in order to specialize properly.
 * Ugh...  
 */
namespace m4_python(print nsname) { 

  /* defined in suffix */
  inline void QUERY_RUN_NOW(LSEfw_stf_def_void);
            
  /* in case code is outside of a class */
  const char *const LSE_instance_name = \
            m4_python(print "\"Module code of %s\"" % \
                      LSEpy_instance.tarball);

  /*********** code point and event trampoline prototypes ****************/

    m4_pythonfile(if 1:
    if LSEpy_doCSpecialize:
        for cp in LSEpy_instance.codepointOrder:
            if cp.type == LSEdb_PointUser or \
               LSEpy_instance.ports[cp.name].width and \
               not LSEpy_instance.ports[cp.name].controlEmpty:	   
                print LSEcg_codefunc_prototype(cp,0,1) + ";"

        for e in LSEpy_minstance.eventOrder:
            if e.isFilled:
                print "%s;" % LSEcg_event_prototype(e,"init2",1)
                print "%s;" % LSEcg_event_prototype(e,"record2",1)
                print "%s;" % LSEcg_event_prototype(e,"report2",1)
 
)m4_dnl

  class LSE_module : public LSEfw_module {
  protected:

     /**************** fields used by APIs ****************/

    m4_pythonfile(if 1:
    cargs = []

    # port information
    if LSEpy_portsFiltered:
       if not LSEpy_doISpecialize:
           aargs = reduce(lambda x,y:x + "\n" + y,
                          map(lambda x:\
                              "struct LSEfw_portinst_info_s<%s> *%s;\n" \
                              "struct LSEfw_portinst_ginfo_s<%s> *%s;\n" \
                              "%s *%s;" % \
                              (x.type, LSEcg_portinst_info_name("",x.name),
                               x.type, LSEcg_portinst_ginfo_name("",x.name),
                               get_pstype(x),
                               LSEcg_portstatus_name("",x.name,1),
                               ),
                              LSEpy_portsFiltered))
           print aargs
       else:
           aargs = reduce(lambda x,y:x + "\n" + y,
                          map(lambda x:"static " \
                              "struct LSEfw_portinst_info_s<%s> *const %s;\n"\
                              "static " \
                              "struct LSEfw_portinst_ginfo_s<%s> *const %s;\n"\
                              "static %s * const %s;" % \
                              (x.type, LSEcg_portinst_info_name("",x.name),
                               x.type, LSEcg_portinst_ginfo_name("",x.name),
                               get_pstype(x),
                               LSEcg_portstatus_name("",x.name,1),
                               ),
                              LSEpy_portsFiltered))
           print aargs             

    # phase cblock number
    if not LSEpy_instance.phase:
        print "static const int LSEfw_CBPHASE = 0;"
    elif LSEpy_doISpecialize:
        if LSE_db.getParmVal("LSE_specialize_codeblock_numbers"):
            cno = LSE_db.getPhaseCblockNo(inst.name)
            print "static const int LSEfw_CBPHASE = %d;" % cno
        else:
            print "const int LSEfw_CBPHASE;"
            cno = LSE_db.getPhaseCblockNo(LSEpy_instance.name)
            cargs.append("LSEfw_CBPHASE(%s+%d)" % 
                         (LSEcg_codeblock_base(LSEpy_instance.name),
                          cno-LSEpy_instance.firstCblock))
    else:
        print "int LSEfw_CBPHASE;"

    # base cblock number
    if LSEpy_doISpecialize: 
        if LSE_db.getParmVal("LSE_specialize_codeblock_numbers"):
            print "static const int LSEfw_CBBASE = %d;" % inst.firstCblock
        else:
            print "const int LSEfw_CBBASE;"
            cargs.append("LSEfw_CBBASE(%s)" % \
                         LSEcg_codeblock_base(LSEpy_instance.name))
    else: 
        print "int LSEfw_CBBASE;"

    # structadd offset number
    if LSEpy_instance.name in LSE_db.structAdds["LSE_dynid_t"]:
        if LSEpy_doISpecialize: 
            if LSE_db.getParmVal("LSE_use_direct_field_access"):
                print "static const size_t LSEfw_SOFFSET_LSE_dynid_t = " \
                      "offsetof(LSEdy_dynid_info_t, fields.%s);" % \
                      LSEcg_flatten(LSEpy_instance.name)                  
            else:
                print "const size_t LSEfw_SOFFSET_LSE_dynid_t;"
                cargs.append("LSEfw_SOFFSET_LSE_dynid_t(%s)" % \
                             LSEcg_structadd_offset_name(LSEpy_instance.name,
                                                         "LSE_dynid_t"))
        else: 
            print "size_t LSEfw_SOFFSET_LSE_dynid_t;"

    if LSEpy_instance.name in LSE_db.structAdds["LSE_resolution_t"]:
        if LSEpy_doISpecialize: 
            if LSE_db.getParmVal("LSE_use_direct_field_access"):
                print "static const size_t LSEfw_SOFFSET_LSE_resolution_t = " \
                      "offsetof(LSEdy_resolution_info_t, fields.%s);" % \
                      LSEcg_flatten(LSEpy_instance.name)                  
            else:
                print "const size_t LSEfw_SOFFSET_LSE_resolution_t;"
                cargs.append("LSEfw_SOFFSET_LSE_resolution_t(%s)" % \
                             LSEcg_structadd_offset_name(LSEpy_instance.name,
                                                         "LSE_resolution_t"))
        else: 
            print "size_t LSEfw_SOFFSET_LSE_resolution_t;"

    # API variables
    for f1 in filter(lambda x:x[0][1]==LSEcg_in_modulebody,
                     LSEpy_instance.toFinishOrder):
        key = f1[0]
        fnc = SIM_apidefs.LSEap_fkey_pieces_funcs[key[0]]
        for f2 in f1[1]:
            s = fnc(LSE_db, key, f2, 0)
            if s: print s

)

  public:

    /*********************** query manipulation structures *************/
    
    m4_pythonfile(if 1:
    for q in LSEpy_minstance.queryOrder:
        if not q.isCalled or not q.scheduled: continue
        print """
        LSEfw_query_info_t LSEfw_query__%s;
        typedef struct LSEfw_query_entry_s__%s {
          int currwaiters;
          int allocwaiters;
          %s returnval;""" % (q.name, q.name, q.returnType)
        if not query_is_void(q.params):
            print "struct LSEfw_query_entry_s2__%s {" % q.name
            p = q.params
            plist=string.split(p,",")
            for p in plist:
                print (p + ";")
            print "} parms;"
        print "unsigned int waiters[4];"
        print ("} LSEfw_query_entry_t__%s;" % (q.name))
)

    /************************* parameters ****************************/
    
    m4_pythonfile(if 1:
    for p in LSEpy_instance.parmOrder:
        pinfo = LSEpy_parms[p[0]]
        if pinfo==0: # not runtimeable... PARM() took care of it.
            pass
        elif pinfo==1: # runtimed; very special
            pass
        elif pinfo==2: # not constant across all instances...
            print "%s %s;" % (p[1][1],p[0])
        elif pinfo==3: # constant non-char *
            print "static %s const %s = %s;" % (p[1][1],p[0],p[1][0])
        else: # constant char *
            print "const static %s const %s;" % (p[1][1],p[0])
)m4_dnl

    m4_pythonfile(if 1:
    # do the constructor if necessary
    if cargs:
        print "LSE_module(void) :\n %s {}\n" % \
              reduce(lambda x,y:x + ",\n" + y, cargs)
)m4_dnl

  /* Codepoints and data collectors are tricky; all of the class
   * attributes in the .clm, module body, and extension must be
   * in-scope.  Thus we really need for them to be inside the
   * extension class.  However, they need to be called from the
   * .clm/module body.  This would seem to lead to virtual methods.
   * However, it would be nice to remove the cost of virtual methods
   * when there really is only one method to call.  Furthermore,
   * there are hopes to get rid of the LSE_userpoint_invoke call
   * entirely in the future, meaning that we will not be able to
   * mmanipulate object pointers directly.  So, to achieve
   * specialization, we use a pair of trampolines, one in the base
   * class and one in the extension class, to get the right code
   * called and we hope that the compiler inlines intelligently.  
   */

  /******************* BEGIN code point prototypes *******************/

    m4_pythonfile(if 1:
    if not LSEpy_doCSpecialize:
        for cp in LSEpy_instance.codepointOrder:
            if cp.type == LSEdb_PointUser or \
               LSEpy_instance.ports[cp.name].width and \
               not LSEpy_instance.ports[cp.name].controlEmpty:	   
                print "virtual " + LSEcg_codefunc_prototype(cp) + " = 0;"
		LSEcg_print_point_method(cp,LSE_db)
    else:
        for cp in LSEpy_instance.codepointOrder:
            if cp.type == LSEdb_PointUser or \
               LSEpy_instance.ports[cp.name].width and \
               not LSEpy_instance.ports[cp.name].controlEmpty:
                do_itrampoline(LSEcg_codefunc_prototype(cp),
                               "%s(%s)" % (LSEcg_codefunc_name(cp,0,1),
                                           prep_callstring("LSEfw_stf_call",
                                                           cp.params)),
                               cp.returnType != "void")
		LSEcg_print_point_method(cp,LSE_db)
)m4_dnl

  /************* BEGIN event prototypes *******************/

  m4_pythonfile(if 1:
  if not LSEpy_doCSpecialize:
      for e in LSEpy_minstance.eventOrder:
          if e.isFilled:
              print "virtual %s = 0;" % LSEcg_event_prototype(e,"init",0)
              print "virtual %s = 0;" % LSEcg_event_prototype(e,"record",0)
              print "virtual %s = 0;" % LSEcg_event_prototype(e,"report",0)
  else:
      for e in LSEpy_minstance.eventOrder:
          if e.isFilled:
              do_itrampoline(LSEcg_event_prototype(e,"init",0),
                             "%s(LSEfw_stf_call_void)" % \
                             (LSEcg_event_name(e,"init2",1)))
              do_itrampoline(LSEcg_event_prototype(e,"record",0),
                             "%s(%s)" % (LSEcg_event_name(e,"record2",1),
                                         prep_callstring("LSEfw_stf_call",
                                                         e.argString)))
              do_itrampoline(LSEcg_event_prototype(e,"report",0),
                             "%s(LSEfw_stf_call_void)" % \
                             (LSEcg_event_name(e,"report2",1)))
)m4_dnl

  /**************** BEGIN port API prototypes ********/

  m4_pythonfile(
for p in LSEpy_minstance.portOrder:
    print "%s;" % LSEcg_portapi_proto(p,"GET_DATA")
    if p.dir == "output":
        print "%s;" % LSEcg_portapi_proto(p,"SET_DATAEN")
    else:
        print "%s;" % LSEcg_portapi_proto(p,"SET_ACK")
    if p.width and not p.controlEmpty:
	ct = (LSEdb_CblockFiring, LSEpy_instance.name, p.name, 0)
	print "%s;" % LSEcg_codeblock_prototype(LSE_db,ct,0)
)

  }; /* class LSE_module */

   // Redeclare static constants which were initialized in the class; this
   // needs to be done in case some code takes their address.  For example,
   // using std::max and referring to a parameter will end up taking the
   // address (even though it gets optimized away) because std::max is 
   // defined using references.

    m4_pythonfile(if 1:
    for p in LSEpy_instance.parmOrder:
        pinfo = LSEpy_parms[p[0]]
        if pinfo==0: # not runtimeable... PARM() took care of it.
            pass
        elif pinfo==1: # runtimed; very special
            pass
        elif pinfo==2: # not constant across all instances...
	    pass
        elif pinfo==3: # constant non-char *
            print "%s const LSE_module::%s;" % (p[1][1],p[0])
        else: # constant char *
	    pass
)m4_dnl

  /***** non-integral static const initializations ****/
  
  m4_pythonfile(if 1:
    # port information
    if LSEpy_portsFiltered and LSEpy_doISpecialize:
           aargs = reduce(lambda x,y:x + "\n" + y,
                          map(lambda x:\
                              "struct LSEfw_portinst_info_s<%s> *const " \
                              "LSE_module::%s = &%s[0];\n" \
                              "struct LSEfw_portinst_ginfo_s<%s> *const " \
                              "LSE_module::%s = &%s[0];\n" \
                              "%s * const LSE_module::%s = &%s[0];" % \
                              (x.type, LSEcg_portinst_info_name("",x.name),
                               LSEcg_portinst_info_name(LSEpy_instance.name,
                                                        x.name),
                               x.type, LSEcg_portinst_ginfo_name("",x.name),
                               LSEcg_portinst_ginfo_name(LSEpy_instance.name,
                                                         x.name),
                               get_pstype(x),
                               LSEcg_portstatus_name("",x.name,1),
                               LSEcg_portstatus_name(LSEpy_instance.name,
                                                     x.name,
                                                     LSEpy_indir_pstatus),
                               ),
                              LSEpy_portsFiltered))
           print aargs

    for p in LSEpy_instance.parmOrder:
        if LSEpy_parms[p[0]] == 4:
            print "const %s const LSE_module::%s = %s;" % (p[1][1],p[0],p[1][0])
)

  /**************** BEGIN port APIs *****************/

  m4_define(LSE_event_record,
m4_pythonfile(
ev = LSEpy_minstance.getEvent("$1")
if ev.isFilled:
  print LSEcg_event_name(ev,"record",0) + r"""(LSEfw_f_call m4_shift($@))"""
))m4_dnl
   m4_pythonfile(
for p in LSEpy_minstance.portOrder:
	if (p.width):
	  if (p.dir == "output"):
	    if (not p.controlEmpty):
		print ("LSEm4_gen_controlled_output_port(%s,%s,%d,%s)" % 
			(LSEpy_minstance.name, p.name, p.width, p.type))
	    else:
		print ("LSEm4_gen_empty_output_port(%s,%s,%d,%s)" % 
			(LSEpy_minstance.name, p.name, p.width, p.type))
	  else:
	    if (not p.controlEmpty):
		print ("LSEm4_gen_controlled_input_port(%s,%s,%d,%s)" % 
			(LSEpy_minstance.name, p.name, p.width, p.type))
 	    else:
		print ("LSEm4_gen_empty_input_port(%s,%s,%d,%s)" % 
			(LSEpy_minstance.name, p.name, p.width, p.type))
	else:
            gen_noconn_port(p)
)m4_dnl
   m4_undefine(LSE_event_record)m4_dnl

};

/***** END prefix *****/

/***************** BEGIN module code ***********************/

m4_dnl ######### Now set the type mappings for the instance .clm...
m4_dnl 
namespace m4_python(print nsname) {

  m4_pythonfile( 
for m in LSEpy_instance.typeMap.items():
  if m[0] != m[1]:
    print "typedef %s %s;" % (m[1], m[0])

fname = r"""LSEm4_filename"""
myenv = LSEcg_Environment()
myenv.db = LSE_db
myenv.inst = LSEpy_instance
myenv.codeloc = LSEcg_in_clm
myenv.reporter = LSEcg_report_error
tok = LSEcg_get_domain_tokenizer(LSE_db,[LSEap_APIs],myenv)
LSEcg_domain_set_inst_rc(LSE_db, LSEpy_instance, 1)
LSEcg_init_errors() 
try:
    rt = tok.processInput("#LSE include %s\n" % repr(fname),None,
    		          [],sys.stdout)
except LSEtk_TokenizeException, v:
    print "LSEm4_error(\037" + str(v) + "\n\036)"
LSEcg_check_errors()
)m4_dnl

m4_dnl  Deal with adapter nonsense
m4_ifelse(m4_python(print LSEpy_minstance.clmname),LSEfw_adapter,
m4_pushdef(LSE_module,LSEfw_adapter))

m4_pythonfile(
if LSEpy_minstance.clmname == "LSEfw_adapter" and \
   not LSEpy_instance.moduleBody:
        print "class %s : public LSE_module { public: }; " % \
	       LSEpy_instance.module
print LSEpy_instance.moduleBody
)

m4_ifelse(m4_python(print LSEpy_minstance.clmname),LSEfw_adapter,
m4_popdef(LSE_module))

m4_dnl ######### Now unset the type mappings for the instance .clm...
m4_dnl 
m4_pythonfile(
for m in LSEpy_instance.typeMap.items():
  print ("m4_popdef(\037%s\036)" % m[0])
)m4_dnl

}; /* namespace */

/***************** END module code ***********************/

/*************** BEGIN header code from FUNCHEADER tag *****************/
/* This code comes after LSS-visible prototypes, which cannot use types 
 * dependent upon the funcheader, but before their bodies, which can 
 */

m4_pythonfile(print LSEpy_instance.funcheader)

/*************** BEGIN header code from data collectors *****************/

m4_pythonfile(
for e in g[0].eventOrder:
        # must put something in if it was filled in any instance
        if LSEpy_minstance.events[e.name].isFilled:
            for s in e.filled:
                print s.header
)m4_dnl

/***************** instance definitions ******************************/

/* Now we need to put together instance-specific stuff; things that
 * will eventually be filling up virtual functions.  What we have
 * are:
 *
 * 1) extensions
 * 2) user points (eventually virtual)
 * 3) data collectors (eventually virtual)
 * 4) firing functions?
 */

namespace m4_python(print nsname) {

m4_pythonfile(
gcnt = 0
gdict = {}
for g in LSEpy_gList:
    mname = g[0].module
    ename = "LSEfw_class_%d_%s" % (gcnt, mname)
    gcnt += 1
    
    print """class %s : public %s {

    private:

    /*************** API implementation variables ************/
    """ % (ename,mname)

    for f1 in filter(lambda x:x[0][1]!=LSEcg_in_modulebody,
                     g[0].toFinishOrder):
      key = f1[0]
      fnc = SIM_apidefs.LSEap_fkey_pieces_funcs[key[0]]
      for f2 in f1[1]:
        s = fnc(LSE_db, key, f2, 0)
        if s: print s

    print """
    /*************** BEGIN extension code *****************/
    """
    
    print g[0].extension

    print """
    /************ BEGIN user functions ***************************/
  public:
    """

    for p in filter(lambda x:x.type==LSEdb_PointUser,g[0].codepointOrder):
      LSEcg_print_point(p,LSE_db)

    print """
    /************ BEGIN control functions ***************************/
    """
      
    for cp in g[0].codepointOrder:
      if cp.type == LSEdb_PointControl and \
         g[0].ports[cp.name].width and \
         not g[0].ports[cp.name].controlEmpty:
        LSEcg_print_point(cp,LSE_db)
        
    print """
    /**************** BEGIN data collectors ***************************/
    """

    for e in g[0].eventOrder:
        # must put something in if it was filled in any instance
        if LSEpy_minstance.events[e.name].isFilled:
            for s in e.filled:
                print s.decl

            print LSEcg_event_prototype(e,"init",0) + "{"
            for s in e.filled:
                print "{",s.init,"}"
            print "}"

            print LSEcg_event_prototype(e,"record",0) + "{"
            for s in e.filled:
                print "{",s.record,"}"
            print "}"

            print LSEcg_event_prototype(e,"report",0) + "{"
            for s in e.filled:
                print "{",s.report,"}"
            print "}"

    print """
    /**************** create a constructor *****************************/
    """
    
    # instance name
    aargs = ["const char *LSEmip_iname"]
    atext = ["LSE_instance_name = const_cast<char *>(LSEmip_iname);"]
    acons = [ ]

    # port information
    if LSEpy_portsFiltered and not LSEpy_doISpecialize:
        aargs.extend(map(lambda x:\
                         "struct LSEfw_portinst_info_s<%s> *LSEmip_I_%s,\n" \
                         "struct LSEfw_portinst_ginfo_s<%s> *LSEmip_GI_%s,\n" \
                         "%s *LSEmip_S_%s" \
                         % (x.type, x.name,
                            x.type, x.name,
                            get_pstype(x), x.name),
                         LSEpy_portsFiltered))
        atext.extend(map(lambda x:"%s = LSEmip_I_%s;\n%s = LSEmip_GI_%s;\n" \
                         "%s = LSEmip_S_%s;"% \
                         (LSEcg_portinst_info_name("",x.name),x.name,
                          LSEcg_portinst_ginfo_name("",x.name),x.name,
                          LSEcg_portstatus_name("",x.name,1),x.name),
                         LSEpy_portsFiltered))

    # phase cblock number
    if g[0].phase and not LSEpy_doISpecialize:
        aargs.append("int LSEmip_CBPHASE");
        atext.append("LSEfw_CBPHASE = LSEmip_CBPHASE;");

    # base cblock number
    if not LSEpy_doISpecialize:
        aargs.append("int LSEmip_CBBASE");
        atext.append("LSEfw_CBBASE = LSEmip_CBBASE;");

    # structadd offset numbers
    if not LSEpy_doISpecialize:
        if LSEpy_instance.name in LSE_db.structAdds["LSE_dynid_t"]:
            aargs.append("size_t LSEmip_DSOFFSET");
            atext.append("LSEfw_SOFFSET_LSE_dynid_t = LSEmip_DSOFFSET;");
        if LSEpy_instance.name in LSE_db.structAdds["LSE_resolution_t"]:
            aargs.append("size_t LSEmip_RSOFFSET");
            atext.append("LSEfw_SOFFSET_LSE_resolution_t = LSEmip_RSOFFSET;");
        
    # parameters
    for p in LSEpy_instance.parmOrder:
        pinfo = LSEpy_parms[p[0]]
        if pinfo == 2:
            aargs.append("%s LSEmip_PARM_%s" % (p[1][1],p[0]))
	    atext.append("%s = LSEmip_PARM_%s;" % (p[0],p[0]))

    for f1 in filter(lambda x:1 or x[0][1]!=LSEcg_in_modulebody,
                     g[0].toFinishOrder):
        key = f1[0]
        fnc = SIM_apidefs.LSEap_fkey_pieces_funcs[key[0]]
        for f2 in f1[1]:
            s = fnc(LSE_db, key, f2, 1)
            if s:
                if s[0]: aargs.append(s[0])
                if s[1]: atext.append(s[1])
                if s[2]: acons.append(s[2])

    # query structure initializtion
    for q in LSEpy_minstance.queryOrder:
        if not q.isCalled or not q.scheduled: continue
        atext.append("LSEfw_query__%s.numinst = 0;" % q.name)
        atext.append("LSEfw_query__%s.instances=NULL;" % q.name)

    if acons: consstr = " :\n" + reduce(lambda x,y:x + ",\n" + y, acons)
    else: consstr = "" 
    print """%s(\n%s)%s\n{
      %s
    }""" % (ename,reduce(lambda x,y:x + ",\n" + y,aargs),consstr, 
            reduce(lambda x,y:x + "\n" + y,atext))

    print """
       } %s; /* class %s */
    """ % (LSEpy_align,ename)

    print "typedef class %s LSEfw_local_module_t;" % mname
    if LSEpy_doCSpecialize:
        print "typedef class %s LSEfw_local_emodule_t;" % ename 

)

}; /* namespace */

/******************** instantiate the objects ********************/

m4_pythonfile(
gcnt = 0
for g in LSEpy_gList:
  ename = "LSEfw_class_%d_%s" % (gcnt, LSEpy_instance.module)
  gcnt += 1
  for mi in g:

    # instance name
    aparm = ["\"" + mi.name + "\""]

    # port information
    if LSEpy_portsFiltered and not LSEpy_doISpecialize:

        aparm.extend(map(lambda x:"&%s[0],\n&%s[0],\n&%s[0]" % \
                         (LSEcg_portinst_info_name(mi.name,x.name),
                          LSEcg_portinst_ginfo_name(mi.name,x.name),
                          LSEcg_portstatus_name(mi.name,x.name,
                                                LSEpy_indir_pstatus),
                          ),
                         LSEpy_portsFiltered))

    # phase cblock number
    if LSEpy_instance.phase and not LSEpy_doISpecialize:
        cno = LSE_db.getPhaseCblockNo(mi.name)
        aparm.append("%s+%d" % 
                     (LSEcg_codeblock_base(mi.name),cno-mi.firstCblock))

    # base cblock number
    if not LSEpy_doISpecialize:
        aparm.append("%s" % LSEcg_codeblock_base(mi.name))

    # structadd offset number
    if not LSEpy_doISpecialize:
        if LSEpy_instance.name in LSE_db.structAdds["LSE_dynid_t"]:
            if LSE_db.getParmVal("LSE_use_direct_field_access"):
                aparm.append("offsetof(LSEdy_dynid_info_t, fields.%s)" % 
                             LSEcg_flatten(mi.name))
            else:
                aparm.append(LSEcg_structadd_offset_name(mi.name,
                                                         "LSE_dynid_t"))
        if LSEpy_instance.name in LSE_db.structAdds["LSE_resolution_t"]:
            if LSE_db.getParmVal("LSE_use_direct_field_access"):
                aparm.append("offsetof(LSEdy_resolution_info_t, fields.%s)" % 
                             LSEcg_flatten(mi.name))
            else:
                aparm.append(LSEcg_structadd_offset_name(mi.name,
                                                         "LSE_resolution_t"))
                
    # parameter instantiations
    for p in mi.parmOrder:
        pinfo = LSEpy_parms[p[0]]
        if pinfo == 2:
	    aparm.append("%s" % LSEcg_fixQuote(p[1][0]))

    for f1 in filter(lambda x:1 or x[0][1]!=LSEcg_in_modulebody,
                     mi.toFinishOrder):
        key = f1[0]
        fnc = SIM_apidefs.LSEap_fkey_pieces_funcs[key[0]]
        for f2 in f1[1]:
            s = fnc(LSE_db, key, f2, 2)
            if s: aparm.append(LSEcg_fixQuote(s))

    print "class %s::%s %s(\n%s);\n" % \
          (nsname,ename,LSEcg_instdata_name(mi.name),reduce(lambda x,y:x + ",\n" + y,
                                                     aparm))
)

/**************** internal trampolines ******************/

namespace m4_python(print nsname) {

  m4_python(if 1:
  if LSEpy_doCSpecialize:
      for cp in LSEpy_instance.codepointOrder:
          if cp.type == LSEdb_PointUser or \
             LSEpy_instance.ports[cp.name].width and \
             not LSEpy_instance.ports[cp.name].controlEmpty:
              do_trampoline("inline " + LSEcg_codefunc_prototype(cp,0,1),
                            "%s(%s)" % (LSEcg_codefunc_name(cp,0,0),
                                        prep_callstring("LSEfw_f_call",
                                                        cp.params)),
                            cp.returnType != "void")
              
      for e in LSEpy_minstance.eventOrder:
          if e.isFilled:
              do_trampoline("inline " + LSEcg_event_prototype(e,"init2",1),
                            "%s(LSEfw_f_call_void)" % \
                            (LSEcg_event_name(e,"init",0),)
                            )
              
              do_trampoline("inline " + LSEcg_event_prototype(e,"record2",1),
                            "%s(%s)" % (LSEcg_event_name(e,"record",0),
                                        prep_callstring("LSEfw_f_call",
                                                        e.argString))
                            )

              do_trampoline("inline " + LSEcg_event_prototype(e,"report2",1),
                            "%s(LSEfw_f_call_void)" % \
                            (LSEcg_event_name(e,"report",0),)
                            )

)

}; /* anonymous namespace */

/************* fill in our port information structures ***********/
/* done late so we get our own class pointers */

m4_pythonfile(
# define this instance port information structures
for mi in LSEpy_miList:
  for p in mi.portOrder:
    if not p.width: continue

    # local info 

)

/*************************************************************************/

m4_dnl  Put this here because this file does *NOT* get parsed with API 
m4_dnl  definitions...
m4_dnl  FUNC
m4_dnl     Define/call instance methods
m4_define(FUNC,
m4_python(print LSEcg_func_name(LSEpy_instance.name,
                                    r"""$1""",0))(m4_shift($@)))
m4_define(FUNCDEF,
m4_python(print LSEcg_func_name(LSEpy_minstance.name,
				    r"""$1""",1))(m4_shift($@)))

/************************ BEGIN query trampolines **************************/

/* There is an assumption that the parameters will have no newlines
 * coming out of LSS
 */

m4_define(LSEm4_gen_query,

  LSEfw_local_module_t *LSEfw_obj =
                   static_cast<LSEfw_local_module_t *>(LSEfw_iptr);
  int LSE_par_index, LSE_newsize,i;
  LSEfw_local_module_t::LSEfw_query_entry_t__$2 *entry;
  LSEfw_query_instance_t *instance;

  /* Do we have enough space? -- expand if necessary */

  LSE_par_index = LSE_point + $5 * LSEfw_instno;

  if (LSE_par_index >= LSEfw_obj->LSEfw_query__$2.numinst) 
    LSEfw_query_info_grow(&LSEfw_obj->LSEfw_query__$2,LSE_par_index,$5);

  instance = &LSEfw_obj->LSEfw_query__$2.instances[LSE_par_index];

  /* Find the query entry */

  if (!instance->entry) {
    /* generate a new entry */
    entry = (LSEfw_local_module_t::LSEfw_query_entry_t__$2 *)
    instance->entry= malloc(sizeof(LSEfw_local_module_t::
                                   LSEfw_query_entry_t__$2));
    /* entry->currwaiters = 0; */ /* redundant */
    entry->allocwaiters = 4;
  }
  else {
    entry = (LSEfw_local_module_t::LSEfw_query_entry_t__$2 *)instance->entry;
  }

  /* If not a valid entry, set the currwaiters to 0; this prevents
   * us from having to clear all the currwaiters at the end of the time
   * step.
   */
  if (instance->index == -1) entry->currwaiters = 0;
  instance->index = LSEfw_instno;

  /* Now save the query parameters */

  m4_pythonfile(
if not query_is_void("""$4"""):
        p="""$4"""
	plist=string.split(p,",")
	nlist=map(lambda x:SIMpy_param_re.match(x).group(1) , plist)
	for n in nlist:
    		print ("entry->parms.%s = %s;" % (n,n))
)

  /* Insert a waiting code block */

  for (i=0;i<entry->currwaiters;i++) 
    if (entry->waiters[i] == LSEfw_calling_id) goto doneinsertingwaiter;
  
  if (entry->currwaiters >= entry->allocwaiters) {
    entry->allocwaiters = entry->allocwaiters*2;
    instance->entry 
      = (void *)entry = (LSEfw_local_module_t::
                         LSEfw_query_entry_t__$2 *)realloc(entry,
			     sizeof(LSEfw_local_module_t::
                                    LSEfw_query_entry_t__$2)+
			     sizeof(int) * entry->allocwaiters-4);
  }

  entry->waiters[entry->currwaiters++] = LSEfw_calling_id;

doneinsertingwaiter:

  /* save the return value and return */

  return (entry->returnval= LSEfw_obj->FUNC($2,
     m4_pythonfile(
if not query_is_void("""$4"""):
		#sys.stderr.write("%s\n" % """$4""")
		p="""$4"""
		plist=string.split(p,",")
		nlist=map(lambda x:SIMpy_param_re.match(x).group(1) , 
			  plist)
		print "LSEfw_f_call " + string.join(nlist,",")
else: print "LSEfw_f_call_void"
)m4_dnl
      ));

)m4_dnl

m4_pythonfile(
fname = LSEcg_flatten(LSEpy_instance.name)
for q in LSEpy_instance.queryOrder:
    callers=q.getCallers()
    # no need to do anything if no caller or not schedulable
    if not q.isCalled or not q.scheduled: continue

    # Now define the function

    print LSEcg_query_tramp_prototype(LSE_db,LSEpy_instance.name,q.name,
				      q.params, q.returnType,1) + "{"
    print "LSEm4_gen_query(%s,%s,%s,\037%s\036,%d)}\n" \
	  % (LSEcg_flatten(LSEpy_minstance.name),
	     q.name, q.returnType, q.params, len(callers))
)m4_dnl

/********************** BEGIN query waker *******************************/

m4_define(LSEm4_gen_query,

  int i, j;
  LSEfw_local_module_t::LSEfw_query_entry_t__$2 *entry;
  LSEfw_query_instance_t *instance;
  $3 return_val;

  for (i=0;i<LSEfw_obj->LSEfw_query__$2.numinst;i++) {
    instance = &LSEfw_obj->LSEfw_query__$2.instances[i];
    if (instance->index != -1) {
      entry = (LSEfw_local_module_t::LSEfw_query_entry_t__$2 *)instance->entry;
      /* no need to set the thread, as it is the same thread */
      /* call the query again */
      return_val= LSEfw_obj->FUNC($2,
         m4_pythonfile(
if not query_is_void("""$4"""):
		#sys.stderr.write("%s\n" % """$4""")
		p="""$4"""
		plist=string.split(p,",")
		nlist=map(lambda x:SIMpy_param_re.match(x).group(1) , 
			  plist)
		print "LSEfw_f_call " + \
		      string.join(map(lambda x: ("entry->parms.") + x, nlist),
				  ",")
else: print "LSEfw_f_call_void"
)m4_dnl
         );

      if (return_val != entry->returnval) {
	/* entry->returnval = return_val; */
        /* wakeup anyone waiting */
        for (j=0;j<entry->currwaiters;j++) {
	  LSEfw_schedule_append(&LSEfw_schedule_timestep,entry->waiters[j]);
 #if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
          LSEfw_schedule_LECrunagain = TRUE;
 #endif
	}
        instance->index = -1; /* invalidate the entries */
      }
    } /* instance index != -1 */
  } /* for loop */
)

namespace m4_python(print nsname) {

  inline void QUERY_RUN_NOW(LSEfw_stf_def_void) {
    LSEfw_local_module_t *LSEfw_obj =
                   static_cast<LSEfw_local_module_t *>(LSEfw_iptr);

m4_pythonfile(
fname = LSEcg_flatten(LSEpy_minstance.name)
for q in LSEpy_minstance.queryOrder:
	callers=q.getCallers()
	# no need to do anything if no caller or not schedulable
	if not q.isCalled or not q.scheduled: continue

	# Now define the function
	
	print ("{LSEm4_gen_query(%s,%s,%s,\037%s\036,%d)}\n" 
		% 
		(LSEcg_flatten(LSEpy_minstance.name),
		 q.name, q.returnType, q.params, len(callers)))
)m4_dnl

}

}; /* end anonymous namespace */

/*************** BEGIN extra init stub *********************/

void 
FUNCDEF(LSE_do_init,LSEfw_stf_def_void) {
  m4_pythonfile(if 1:
  print mdecl()
  print mprefix() + "FUNC(init, LSEfw_f_call_void);"
  print mprefix() + "%s(LSEfw_f_call_void);" % \
		 LSEcg_codefunc_name(LSEpy_instance.codepoints["init"],0)
  for e in LSEpy_minstance.eventOrder:
    if e.isFilled:
	print mprefix() + LSEcg_event_name(e,"init",0) + \
			"(LSEfw_f_call_void);"
)
}

/*************** BEGIN extra finish stubs *********************/

void 
FUNCDEF(LSE_finish_stats,LSEfw_stf_def_void) {
  m4_pythonfile(if 1:
  print mdecl()
  for e in LSEpy_minstance.eventOrder:
    if e.isFilled:
	print mprefix() + LSEcg_event_name(e,"report",0) + \
		"(LSEfw_f_call_void);"
)
}

void 
FUNCDEF(LSE_do_finish,LSEfw_stf_def_void) {
  m4_pythonfile(if 1:
  print mdecl()
  print mprefix() + "%s(LSEfw_f_call_void);" % \
	LSEcg_codefunc_name(LSEpy_instance.codepoints["finish"],0)
  print mprefix() + "FUNC(finish, LSEfw_f_call_void);"
)
}

/*************** BEGIN extra start of timestep stub *********************/

void
FUNCDEF(LSE_start_of_timestep, LSEfw_stf_def_void)
{
  m4_pythonfile(if 1:
  print mdecl()

  if not LSEpy_minstance.codepoints["start_of_timestep"].isEmpty:
    print mprefix() + "%s(LSEfw_f_call_void);" % \
       LSEcg_codefunc_name(LSEpy_instance.codepoints["start_of_timestep"],0)
  if LSEpy_instance.start:
     print mprefix() + "FUNC(phase_start, LSEfw_f_call_void);"
)
}

/*************** BEGIN extra end of timestep stub *********************/
void
FUNCDEF(LSE_end_of_timestep, LSEfw_stf_def_void) {
  m4_pythonfile(if 1:
  print mdecl()
  print mprefix() + "%s(LSEfw_f_call_void);" % \
    LSEcg_codefunc_name(LSEpy_instance.codepoints["end_of_timestep"],0)
)
}

/************* BEGIN trampolines ***********/
 
m4_pythonfile(
codeBlocksSeen={}

# This instance code blocks

for p in LSEpy_portsFiltered:
    if not p.controlEmpty:
	ct = (LSEdb_CblockFiring,LSEpy_instance.name,p.name,0)
	pts = LSEcg_codeblock_name(LSE_db,ct,0,0)
	if not codeBlocksSeen.has_key(pts):
	   codeBlocksSeen[pts] = 1
           do_trampoline(LSEcg_codeblock_prototype(LSE_db,ct,1),
                         "%s(LSEfw_f_call instno)" % pts)

    if p.handler: 
	ct = (LSEdb_CblockHandler,LSEpy_instance.name,p.name,0)
	pts = LSEcg_codeblock_name(LSE_db,ct,0,0)
	if not codeBlocksSeen.has_key(pts):
	    codeBlocksSeen[pts] = 1
            do_trampoline(LSEcg_codeblock_prototype(LSE_db,ct,1),
                         "%s(LSEfw_f_call instno)" % pts)

if LSEpy_instance.phase:
    ct = LSEdb_phaseKey(LSEpy_instance.name)
    pts = LSEcg_codeblock_name(LSE_db,ct,0,0)
    if not codeBlocksSeen.has_key(pts):
        codeBlocksSeen[pts] = 1
        do_trampoline(LSEcg_codeblock_prototype(LSE_db,ct,1),
                      "%s(LSEfw_f_call_void)" % pts)

if LSEpy_instance.start:
    do_trampoline("void FUNCDEF(phase_start, LSEfw_stf_def_void)",
                  "FUNC(phase_start, LSEfw_f_call_void)")

if LSEpy_instance.end:
    do_trampoline("void FUNCDEF(phase_end, LSEfw_stf_def_void)",
                  "FUNC(phase_end, LSEfw_f_call_void)")

for q in LSEpy_instance.queryOrder:
   if LSEpy_instance.codepoints.has_key(q.name) and \
      LSEpy_instance.codepoints[q.name].type == LSEdb_PointUser and \
	LSEpy_instance.codepoints[q.name].makeMethod: continue

   do_trampoline(LSEcg_query_prototype(LSEpy_minstance.name,q.name,q.params,
                                       q.returnType,1),
                 "%s(%s)" % \
                 (LSEcg_query_name(mi.name,q.name,0),
                  prep_callstring("LSEfw_f_call", q.params)),
                 q.returnType != "void")

for cp in filter(lambda x:x.type==LSEdb_PointUser and x.makeMethod,
		 LSEpy_minstance.codepointOrder): 

   do_trampoline(LSEcg_method_prototype(LSEpy_minstance.name,cp.name,cp.params,
                                        cp.returnType,1),
                 "%s(%s)" % (LSEcg_codefunc_name(cp,0),
                             prep_callstring("LSEfw_f_call",cp.params)),
                 cp.returnType != "void")

for e in LSEpy_minstance.eventOrder:
   if not e.isFilled: 
	#print """%s {}\n""" % LSEcg_event_prototype(e,"record",1)
	continue

   do_trampoline(LSEcg_event_prototype(e,"record",1),
                 "%s(%s)" % (LSEcg_event_name(e,"record",0),
                             prep_callstring("LSEfw_f_call",e.argString)))

)


/************** END trampolines ******************/
