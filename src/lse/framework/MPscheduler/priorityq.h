#ifndef PRIORITYQ_H
#define PRIORITYQ_H

#include <map>
#include <vector>
#include <algorithm>
#include <cassert>

namespace LSE_scheduler {

/* An implementation of std::priority_queue but with resorting capability and iterators */
  template<typename Val, typename Cmp = std::less< typename std::vector<Val>::value_type > >
class PriorityQ {
private:
  typedef typename std::vector<Val>        queue_t;
  
public:
  typedef typename queue_t::iterator       iterator;
  typedef typename queue_t::const_iterator const_iterator;
  typedef typename queue_t::size_type      size_type;
  
  /* Constructors */
  PriorityQ(const Cmp &cmp = Cmp()) : cmp(cmp), valid(true) {}
  // TODO: some initializing constructors, eventually

  /* Queue information */
  size_type size()  { return queue.size();  }
  bool      empty() { return queue.empty(); }
  
  /* Get the highest priority */
  const Val& top()
  {
    // Resort and get the highest priority
    if ( ! valid ) reheap();
    return queue.front();
  }
  
  /* Add to the queue */
  void push(const Val &v)
  {
    queue.push_back(v);
    // Let's keep things valid if they already were
    if ( valid )
      std::push_heap(queue.begin(), queue.end(), cmp);
  }
  
  /* Remove the highest priority */
  void pop()
  {
    // Normally, one would do a top() before popping, so let's do a
    // check to make sure we're sorted, bailing out otherwise
    assert ( valid && "Error: Popping when the list hasn't been resorted!\n");
    //if ( ! valid ) reheap();  TODO: or should we do this?
    std::pop_heap(queue.begin(), queue.end(), cmp);
    queue.pop_back();
  }
  
  void erase(const Val &v) {
    queue.erase(std::find(queue.begin(),queue.end(),v));
  }

  /* Iterators */
  iterator       begin()       { return queue.begin(); }
  const_iterator begin() const { return queue.begin(); }
  iterator       end()         { return queue.end();   }
  const_iterator end()   const { return queue.end();   }
  
  /* Invalidate the sort: call this after modifying the priority of any item */
  void invalidateSort() { valid = false; }
  void forceResort() { reheap(); }

  /* Clear the queue and an empty list is a priority que valid = true */
  void clear(){queue.clear();  valid = true; }

private:
  /* Private members */
  Cmp     cmp;
  queue_t queue;
  bool    valid;
    
  /* Helper functions */
  void reheap() 
  {
    std::make_heap(queue.begin(), queue.end(), cmp);
    valid = true;
  }
};

/* A helper class for the mapped priority queue */
/* Works similar to std::pair, but with an explicit pointer as .second and operator< defined */
template<typename Key, typename Val>
struct Manager
{
  Key  key;
  Val *data;
  Manager(Key key = Key(), Val *data = NULL ) : key(key), data(data) { }
  bool operator<(const Manager &rh) const { return *data < *rh.data; }
};

/* A PriorityQ with map for random access to data members*/
template<typename Key, typename Val>
class MappedPQ : public PriorityQ< Manager<Key,Val> >
{
private:
  typedef typename std::map<Key,Val>             map_t;
  typedef          PriorityQ< Manager<Key,Val> > base_t;
  
public:
  /* Pushing needs a key */
  
  void push(const Key &k, const Val &v)
  {
    // Add key to map, passing the address of the result to the priority queue
    base_t::push(Manager<Key,Val>(k,&(map[k] = v)));
  }
  
  /* Popping requires removing the key */
  void pop()
  {
    map.erase(base_t::top().key);
    base_t::pop();
  }
  void clear()
  {
    map.clear();
    base_t::clear();
  }
  
  /* Random access to the map via key value */
  Val& operator[](const Key &k) { return map.find(k)->second; } // Uses .at for safety
  
private:
  map_t map;
};

} // namespace Packager

#endif
