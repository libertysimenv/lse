#include "LSEPostScheduler.h"
//#include "BinarySemaphore.h"
#include <map>
#include <list>
#include <algorithm>
#include <queue>
#include <vector>

namespace LSE_scheduler
{

  ipoint_t LSEPostScheduler::find_ipoint(Schedulable* node) {
    ipoint_t curr_ipoint;
    curr_ipoint.thread = -1;
    // Note: only works for homogenous costs at present.
    int nodeCost = policy->nodeCost(node);
    int occupancy;
    int savethread = node->getThread();

    /* This for loop runs through each thread to check if that
       is a valid thread to schedule this node on
       It does this by 
       1. finding the schedule time of each predecessor and figuring out 
       the 1st schedule time
       at which this node can validly be scheduled -- this is earliestStart
       2. It then checks where the node can fit.
       a. It tacks on the current end of the schedule just perfectly
       b. It must go after the current end and so creates a new hole
       c. It could go earlier and so either gets put on the end of goes 
       in a hole
       3. Once a spot is found then a new ipoint is made and compared to the 
       current best ipoint selecting the better of the two.
       */
    for (int currentThread = 0; currentThread < numThreads; currentThread++) {
      if (!policy->validThread(node, currentThread, graph)) continue;

      node->setThread(currentThread); // temporary to get cost/conflicts right

      schedtime_t earliestStart  = 0;

      if (P.isPush) {
        // this for loop finds the earliest time at which
        // all of the predecessors for this node have finished
        // and stores the value in earliestStart
        for (schedulableIterator it = node->pred_begin(), 
	       e = node->pred_end(); it != e; ++it) {
          schedtime_t tempCommCost = ((*it)->getThread() == currentThread) 
	    ? 0 : (policy->naiveCommCost(*it, node));
          schedtime_t newtime = (*it)->getFinishTime() + tempCommCost;
	  if (logfile)
	    fprintf(logfile, "\tnewtime: %lld %lld %lld\n", newtime,
		    (*it)->getFinishTime(), tempCommCost);
          earliestStart = std::max(newtime, earliestStart);
        } // for predecessors

        occupancy = nodeCost;

      } else {
        schedtime_t costsum = nodeCost;
        //  this for loop finds the earliest time at which
        //  all of the predecessors for this node have finished
        //  and stores the value in earliestStart
        for (schedulableIterator it = node->pred_begin(), e = node->pred_end();
            it != e; ++it) {
          schedtime_t tempCommCost = ((*it)->getThread() == currentThread) 
	    ? 0 : (policy->naiveCommCost(*it, node));
          costsum += tempCommCost;
          schedtime_t newtime = (*it)->getFinishTime();
          earliestStart = std::max(newtime, earliestStart);
        }

        occupancy = costsum;
      }

      if (earliestStart == mkspan[currentThread]) {
        ipoint_t newip = ipoint_t(currentThread, earliestStart+1, occupancy);
	adjustIpoint(node, newip);
        if (policy->betterIpoint(newip, curr_ipoint))
          curr_ipoint = newip;
      }
      else if (earliestStart > mkspan[currentThread]) {
        //I a new hole will be created 
        //because it can't start until a while after this thread is done
        //with all currently assigned work
        ipoint_t newip = ipoint_t(currentThread, earliestStart, occupancy);
	adjustIpoint(node, newip);

        if (policy->betterIpoint(newip, curr_ipoint))
          curr_ipoint = newip;

      } else { // check within holes
        //This node is ready before this thread is done with it's currently
        //executing stuff. This means we go look for a hole to stuff 
        //it in earlier
        if (policy->allowThreadHoleFilling()) {

          for(holes_t::iterator j = holes[currentThread].begin(),
              je = holes[currentThread].end(); j != je; ++j) {

	    if (logfile)
	      fprintf(logfile,"Trying hole %lld %lld %lld %lld\n", 
		      j->first, j->second, earliestStart,
		      earliestStart + occupancy - 1);

	    // WARNING: the following test was removed at some time, but 
	    // really should not be there, as the following analysis shows:
	    //        window             first    second
	    //      xxxxxxxxxx           T        F/T
	    //   xxxx                    T        T
	    //                 xxxx      F        F/T     into next window
	    //    xxxxxxx                T        T
	    //            xxxxxxxxx      F        F/T       into hole
	    //          xx               F        T

	    // For now, we leave it in to match Python behavior
	    //if (j->first < earliestStart) continue;

            // earliestHoleStart is the earliest time at which the node 
	    // can start, given that it can only start within a hole

            schedtime_t earliestHoleStart = std::max(j->first, earliestStart);

            // Hole is no good if node ends after the hole ends
            if (j->second < earliestHoleStart + occupancy - 1) continue; 

            ipoint_t newip(currentThread, earliestHoleStart, occupancy);
	    adjustIpoint(node, newip);

	    // double check that skewing didn't push it out of the whole
            if (j->second < newip.start + occupancy - 1) continue; 

            if (policy->betterIpoint(newip, curr_ipoint))
              curr_ipoint = newip;
          }
        }

        ipoint_t newip(currentThread, mkspan[currentThread]+1, occupancy);
	adjustIpoint(node, newip);

        //We have now found the best hole in which to stick the node,
        //but let's just check to see if it would be better to stick the
        //node at the end of the thread
        if (policy->betterIpoint(newip, curr_ipoint))
          curr_ipoint = newip;

      } // check within holes

      node->setThread(savethread); // restore thread number

    } // for pth 


    return curr_ipoint;
  }

  void LSEPostScheduler::adjustIpoint(Schedulable *node, ipoint_t &newip) { 
    // Set the conflicts field of the ipoint to closeness and presence
    // of conflicts.  
    //     0 = not close, not present.
    //     1 = not close, present
    //     2 = close (always would be present)  NO LONGER NEEDED
    if (P.lockHandling == threadingParms::ignore) return;
    newip.conflicts = 0;
    node->setStartTime(newip.start); // so covered works

    // iterate across conflicts
  startover:
    for (schedulableIterator s = node->sibling_begin(),
	   se = node->sibling_end(); s != se ; ++s) {

      //fprintf(logfile, "Checking conflict with %d\n", (*s)->getCluster());

      if ((*s)->getStartTime() < 0 || covered(*s,node)) continue;

      //fprintf(logfile, "Found conflict with %d\n", (*s)->getCluster());

      newip.conflicts |= 1;

      if (P.lockHandling != threadingParms::skew) break;

      // handle skew calculations

      // is it far enough away already?
      if ((*s)->getStartTime() > 
	  ( policy->nodeCost(*s) * P.lockOffsetFactor + newip.start)) continue;

      schedtime_t nloc = std::max(newip.start, 
				  (*s)->getStartTime() + 
				  policy->nodeCost(*s) * P.lockOffsetFactor);
      if (nloc != newip.start) {
	newip.start = nloc;
	node->setStartTime(nloc);
	goto startover;
      }
    }

    node->setStartTime(-1); // and put it back
  }


  namespace {
    bool comp(const Schedulable* a, const Schedulable* b){ 
      return a->getStartTime() < b->getStartTime();
    }
  }

  bool LSEPostScheduler::covered(Schedulable *fromi, Schedulable *toi) {
    int tth = toi->getThread(), fth = fromi->getThread();

    if (fromi->getThread() == toi->getThread()) return true;

    //fprintf(stderr,"Testing covered conflict from %d/%d to %d/%d\n",
    //	    fromi->getID(), fth, toi->getID(), tth);
    
    crosscomm_t::iterator lh 
      = crossheads[tth].upper_bound(toi->getStartTime());

    if (lh != crossheads[tth].begin()) {
      --lh;
      if (lh->second.iknown[fth] >= fromi->getStartTime()) return true;
    }

    lh = crossheads[fth].upper_bound(fromi->getStartTime());

    if (lh != crossheads[fth].begin()) {
      --lh;
      if (lh->second.iknown[tth] >= toi->getStartTime()) return true;
    }

    //fprintf(stderr,"Found covered conflict from %d/%d to %d/%d\n",
    //fromi->getID(), fth, toi->getID(), tth);
    return false;
  }

  void LSEPostScheduler::finalize()
  {
    //Create the task order data structure by assigning 
    //the threads to the correct vector and then sorting on the 
    //start time of each Schedulable
    SchedulableDAG::TaskOrder & taskOrder = graph->getTaskOrder();
    taskOrder.clear();
    taskOrder.resize(numThreads);

    for(schedulableIterator graphIter = graph->begin(), end = graph->end();
        graphIter != end; ++graphIter) {
      taskOrder[(*graphIter)->getThread()].push_back(*graphIter);
    }

    for(SchedulableDAG::TaskOrder::iterator thread = taskOrder.begin(), 
        end = taskOrder.end();
        thread != end; ++thread)
    {
      std::sort((*thread).begin(),(*thread).end(), comp );
    }

    // calculate locks by looking at siblings

    typedef std::map<Schedulable *, int> conflicts_by_sp_t;
    conflicts_by_sp_t conflicts_by_sp;
    typedef std::map<Schedulable *, int> conflictsp_t;
    typedef std::map<int, conflictsp_t> conflicts_t;
    conflicts_t conflicts;

    for(schedulableIterator graphIter = graph->begin(), end = graph->end();
        graphIter != end; ++graphIter) {
      Schedulable &n = **graphIter;

      for (schedulableIterator s = (*graphIter)->sibling_begin(),
	     se = (*graphIter)->sibling_end(); s != se ; ++s) {

	if (covered(&n,*s)) continue;

	int numconflicts = conflicts.size();

	//fprintf(stderr,"Numconflicts = %d\n", numconflicts);
	Schedulable *item = &n, *og = *s;

	if (conflicts_by_sp.find(item) != conflicts_by_sp.end()) {

	  // merge node into a previous conflict set

	  int n1 = conflicts_by_sp[item];
	  int n2 = (  (conflicts_by_sp.find(og) == conflicts_by_sp.end()) ?
		      n1 : conflicts_by_sp[og] );
	  conflicts[n1][og] = og->getThread();
	  conflicts_by_sp[og] = n1;

	  //fprintf(stderr,"Adding %d to %d other=%d\n", og->getID(), n1, n2);

	  if (n1 != n2) { // merge them
	    for (conflictsp_t::iterator i = conflicts[n2].begin(),
		   ie = conflicts[n2].end(); i != ie ; ++i) {
	      conflicts_by_sp[i->first] = n1;
	      //fprintf(stderr,"Moving %d to %d\n", i->first->getID(), n1);
	      conflicts[n1][i->first] = i->second;
	    }
            if (n2 != numconflicts-1) {
	      conflicts[n2] = conflicts[numconflicts-1];
	      for (conflictsp_t::iterator i = conflicts[n2].begin(),
		     ie = conflicts[n2].end(); i != ie ; ++i) {
		conflicts_by_sp[i->first] = n2;
		//fprintf(stderr,"End moving %d from %d to %d\n", 
		//	i->first->getID(), numconflicts-1, n2);
	      }
	    }
	    conflicts.erase(numconflicts-1);
	  }

	} else if (conflicts_by_sp.find(og) != conflicts_by_sp.end()) {
	  
	  // merge node into a previous conflict set
	  
	  int n1 = conflicts_by_sp[og];
	  int n2 = (  (conflicts_by_sp.find(item) == conflicts_by_sp.end()) ?
		      n1 : conflicts_by_sp[item] );
	  conflicts[n1][item] = item->getThread();
	  conflicts_by_sp[item] = n1;
	  
	  //fprintf(stderr,"Adding %d to %d other=%d\n", item->getID(), n1, n2);

	  if (n1 != n2) { // merge them
	    for (conflictsp_t::iterator i = conflicts[n2].begin(),
		   ie = conflicts[n2].end(); i != ie ; ++i) {
	      conflicts_by_sp[i->first] = n1;
	      //fprintf(stderr,"Moving %d to %d\n", i->first->getID(), n1);
	      conflicts[n1][i->first] = i->second;
	    }
	    if (n2 != numconflicts-1) {
	      conflicts[n2] = conflicts[numconflicts-1];
	      for (conflictsp_t::iterator i = conflicts[n2].begin(),
		     ie = conflicts[n2].end(); i != ie ; ++i) {
		conflicts_by_sp[i->first] = n2;
		//fprintf(stderr,"End moving %d from %d to %d\n", 
		//	i->first->getID(), numconflicts-1, n2);
	      }
	    }
	    conflicts.erase(numconflicts-1);
	  }
	} else { // new conflict

	  //fprintf(stderr,"Adding %d to %d\n", og->getID(), numconflicts);
	  //fprintf(stderr,"Adding %d to %d\n", item->getID(), numconflicts);
	  
	  conflicts[numconflicts][item] = item->getThread();
	  conflicts[numconflicts][og] = og->getThread();
	  conflicts_by_sp[item] = numconflicts;
	  conflicts_by_sp[og] = numconflicts;
	  
	}
      } // siblings
    } // nodes

    for(schedulableIterator graphIter = graph->begin(), end = graph->end();
        graphIter != end; ++graphIter) 
      if (conflicts_by_sp.find(*graphIter) != conflicts_by_sp.end())
	(*graphIter)->addLock(conflicts_by_sp[*graphIter]);

#ifdef DOINGDEBUG
    fprintf(stderr,"Number of conflicts is: %d\n", conflicts.size());

    for (conflicts_t::iterator i = conflicts.begin(), ie = conflicts.end();
	 i != ie; ++i) {
      fprintf(stderr, "Set %d:", i->first);
      for (conflictsp_t::iterator j = i->second.begin(), je=i->second.end() ; 
	   j != je ; ++j)
	fprintf(stderr, " %d", j->first->getID());
      fprintf(stderr,"\n");
    }
#endif

    graph->setLockCnt(conflicts.size());
    graph->setSemCnt(semCnt);
    graph->setScheduled(true);
  }

  namespace {
    class arc_t {
    public:
      Schedulable *first; 
      schedtime_t second;
      arc_t(Schedulable *f, schedtime_t s) : first(f), second(s) {}
      bool operator<(const arc_t &x) const {
	int tno = first ? first->getID() : 0;
	int xno = x.first ? x.first->getID() : 0;
	return tno < xno || tno == xno && second < x.second;
      }
    };
  }

  void LSEPostScheduler::run(SchedulableDAG& graph, int numThreads, 
			     FILE* logfile)
  {
    graph.setNumThreads(numThreads);

    if(logfile) fprintf(logfile,"====Starting LSE Post Scheduler====\n");

    // Initialize for run

    this->numThreads = numThreads;
    this->logfile = logfile;
    this->graph = &graph;
    std::map<Schedulable*, int> semnos;     // semaphores: [id -> #]
    std::list<ipoint_t> ipoints;            // potential insertion points

    // creates and initializes the finish time for each node to -1
    mkspan.clear();
    mkspan.resize(numThreads,-1);

    // creates a structure to hold the holes for each thread 
    holes.clear();
    holes.resize(numThreads);

    // Create a struct to hold information about the nodes
    nodeflags.clear();

    // Set up the structure that keeps information on
    // what thread is synchronized to what thread at what point
    crossheads.clear();
    crossheads.resize(numThreads); 

    semCnt = 0;

    // Allow the policy to do some setup work on priorities
    // this should be used to back propagate scheduling decisions
    policy->CalcInitialPriorities(graph);

    /**
     * Build the ready list
     */
    for (schedulableIterator it = graph.begin(), end = graph.end(); 
	 it != end; ++it)
    {
      nodeflags_t & flags= nodeflags[*it];

      // In the initial setup, all preds are unscheduled, 
      // so just copy the number of preds over.
      flags.unscheduled_preds = (*it)->getPredecessorCount();

      // A node is ready when there are no predecessors or when all 
      // predecessors have gone.
      flags.ready = !nodeflags[*it].unscheduled_preds;

      //If it's ready, add it to the ready list
      if (flags.ready) {

        bool locality = policy->addReady(*it,(*it)->getThread());
        if (logfile) {
          fprintf(logfile, 
		  "Added %s to the ready list.  "
		  "The size of the ready list is %d "
		  "and we used locality %d\n", (*it)->getName().c_str(),
		  (int)policy->readyListSize(),locality);
        }    
      }//this is where we actually add things to the ready list
      //this is also where we might add the histogram
    }

    //this should be used to back propagate scheduling decisions
    policy->NewReadyListCallback();
    int scheduled = 0;

    // Now consider each node in turn
    while (policy->readyListSize()) 
    {
      //while we still have ready nodes
      Schedulable* node = policy->readyListTop();

      // Find best insertion point among threads considered viable for this
      // node.  
      ipoint_t ipoint = find_ipoint(node);

      scheduled ++;
      nodeflags[node].ready = false;

      // insert into the hole structures

      if (ipoint.start == mkspan[ipoint.thread]) { 

	// startime overlaps with end of schedule, go one past and increase
	// makespan

        ipoint.start += 1;
        mkspan[ipoint.thread] = ipoint.start + ipoint.length - 1; 

      } else if (ipoint.start > mkspan[ipoint.thread]) { 

	// starttime comes much after end of schedule: make a new hole and 
	// incrase makespan

        if (ipoint.start - 1 >= mkspan[ipoint.thread]+1)
          holes[ipoint.thread][mkspan[ipoint.thread]+1] = ipoint.start - 1;
        mkspan[ipoint.thread] = ipoint.start + ipoint.length - 1;

      } else { // starttime is in a hole; find it.
        schedtime_t lastloc = ipoint.start + ipoint.length - 1;
        holes_t & holeToFill = holes[ipoint.thread];

        holes_t::iterator hi = holeToFill.lower_bound(ipoint.start+1);

	if (hi != holeToFill.begin()) --hi;

	if (!(hi->first <= ipoint.start &&
	      hi->second >= ipoint.start + ipoint.length - 1)) {
	  if (hi == holeToFill.end()) 
	    fprintf(stderr,"End %lld %lld\n", ipoint.start, 
		    mkspan[ipoint.thread]);
	  else fprintf(stderr,"%lld %lld %lld %lld\n", 
		       hi->first, hi->second, ipoint.start,
		       ipoint.start + ipoint.length - 1);
          for(holes_t::iterator j = holes[ipoint.thread].begin(),
              je = holes[ipoint.thread].end(); j != je; ++j) {
	    fprintf(stderr, "\t%lld %lld\n", j->first, j->second);
	  }
	}

        assert(hi->first <= ipoint.start &&
	       hi->second >= ipoint.start + ipoint.length - 1);

	schedtime_t holeStart = hi->first, holeFinish = hi->second;
	holeToFill.erase(hi);
	if (holeStart < ipoint.start) holeToFill[holeStart] = ipoint.start-1;
	if (holeFinish > lastloc) holeToFill[lastloc+1] = holeFinish;

      }
      node->setStartTime(ipoint.start);
      node->setFinishTime(ipoint.start + ipoint.length);
      node->setThread(ipoint.thread);

      if (logfile) {
        fprintf(logfile, "Post Scheduled %s\n  cost %d\n  time %d\n  "
		"thread %d\n  cluster %d\n  schedule length %d\n",
		node->getName().c_str(), ipoint.length, 
		ipoint.start, ipoint.thread,node->getCluster(),
		*std::max_element(mkspan.begin(),mkspan.end()));
      } 

      /* update cross-thread communication arcs
       * The trick here is that we want to only add arcs that convey
       * additional information and want to remove arcs which have been
       * superceded.  We should never add more than one arc per thread
       * in this step and it should always be the one with the youngest
       * tail to the arc.  To prevent us from having to delete arcs, it
       * is best to pre-filter this....  We can also delete arcs which
       * are superseded transitively, as in 1->2->3  kills 1->3 just fine.
       * This can be done by looking backwards to see whether new information
       * is conveyed by the arc.
       *
       * The crossheads/tails data structures are mappings from
       * (src thread, dest thread) => edge info
       * edge info is:
       * (ordering stuff, (src id, src order, dest id, dest order),
       *  list of information conveyed by edge)
       */

      // Find the youngest dependence arc to this task from each thread

      // Data structure for arc
      // index into vector is the thread
      // Schedulable* in the pair is the pred creating the arc
      // schedtime is the time synchronized at

      std::vector<arc_t> arcs(numThreads, arc_t((Schedulable*)0,-1));
      bool hasarc = false;

      // Find all of the communication arcs leading into the scheduled node
      for (schedulableIterator i = node->pred_begin(), end = node->pred_end(); 
	   i != end; ++i) {
        Schedulable* pred = *i;
        int pred_thread = pred->getThread();
        // if we're on different threads, and the scheduled node makes a new arc
        if (pred_thread != ipoint.thread && 
	    arcs[pred_thread].second < pred->getStartTime()) {
          arcs[pred_thread] = arc_t(pred, pred->getStartTime());
          hasarc = true;
        }
      }

      if (hasarc) {

        // How much information do we know before the edges?

        std::vector<schedtime_t> iknown;

        crosscomm_t::iterator 
          lh = crossheads[ipoint.thread].upper_bound(ipoint.start-1);
        if (lh != crossheads[ipoint.thread].begin()) {
          --lh;
          iknown = lh->second.iknown;
        } else iknown = std::vector<schedtime_t>(numThreads, -1);
        iknown[ipoint.thread] = ipoint.start;

        std::vector<schedtime_t> niknown(iknown);

        std::sort(arcs.begin(), arcs.end());

        for (std::vector<arc_t>::iterator i = arcs.begin(), ie = arcs.end(); 
	     i != ie; ++i) 
        {

          Schedulable* dno = (*i).first;
          if (dno == NULL) continue;

          int dth = dno->getThread();

          //Ensure it has actually been scheduled
          assert (dth >= 0 && dth < numThreads);

          // how much information is conveyed by the edge?
          std::vector<schedtime_t> ilearned;

          crosscomm_t::iterator lh 
	    = crossheads[dth].upper_bound(dno->getStartTime());

          if (lh != crossheads[dth].begin()) {
            --lh;
            ilearned = lh->second.iknown;
          } else ilearned = std::vector<schedtime_t>(numThreads, -1);
          ilearned[dth] = dno->getStartTime();

          bool learnedsomething = false;

          for (int j = 0; j < numThreads ; ++j ) {
            niknown[j] = std::max(niknown[j], ilearned[j]);
            if (niknown[j] > iknown[j]) learnedsomething=true;
          }

          if (!learnedsomething) continue;

          // assign the semaphore number based upon the tail so there
          // is only one semaphore send per block

          int semno=0;

          std::map<Schedulable*, int>::iterator ns = semnos.find(dno);
          if (ns == semnos.end()) {
            semno = semnos[dno] = semCnt++;
          } else semno = ns->second;

          crossheads[ipoint.thread][ipoint.start] 
	    = crosscomm_ent_t(niknown, dno, dno->getStartTime(),
              node, node->getStartTime(), 
              semno);

	  // Must prevent duplicates

	  if (find(node->getReceiveSemaphores().begin(),
		   node->getReceiveSemaphores().end(),semno)
	      == node->getReceiveSemaphores().end())
	    node->getReceiveSemaphores().push_back(semno);

	  std::sort(node->getReceiveSemaphores().begin(), 
		    node->getReceiveSemaphores().end());

	  if (find(dno->getSendSemaphores().begin(),
		   dno->getSendSemaphores().end(),semno)
	      == dno->getSendSemaphores().end())
	    dno->getSendSemaphores().push_back(semno);

          if (logfile) {
            fprintf(logfile, "Adding Arc:\n  Src:Thread=%d Node=%s "
		    "Time=%d\n  Dst:Thread=%d Node=%s Time=%d\n",
		    dth,dno->getName().c_str(),dno->getStartTime(),
		    ipoint.thread, node->getName().c_str(), ipoint.start);
            fprintf(logfile, "  niknown[");
            for (int j = 0; j < numThreads;j++) {
              if (j) fprintf(logfile, ", ");
              fprintf(logfile, "%d", niknown[j]);
            }
            fprintf(logfile, "]\n  ilearned[");
            for (int j = 0; j < numThreads;j++) {
              if (j) fprintf(logfile, ", ");
              fprintf(logfile, "%d", ilearned[j]);
            }
            fprintf(logfile, "], %d)\n", semno);
          }

          iknown = niknown;
        }
      }//if(hasarcs)

      // Report the scheduling
      graph.reportNodeScheduled(node);
      graph.setClusterThread(node->getCluster(), node->getThread());

      policy->readyListPop();

      //Now that this node is scheduled, see if it frees up any
      //other nodes to put on its ready list
      for (schedulableIterator i = node->succ_begin(), end = node->succ_end();
          i != end; ++i) {
        Schedulable* succ = *i;
        nodeflags_t& flags = nodeflags[succ];
        if (!flags.ready && ! (-- flags.unscheduled_preds)) {
          flags.ready = true;
          int locality = policy->addReady(succ,succ->getThread());
          if (logfile) {
            fprintf(logfile, "Added %s to the ready list. "
		    "The size of the ready list is %d "
		    "and we used locality %d\n", succ->getName().c_str(),
		    (int)policy->readyListSize(),locality);
          }
        }
      }

    } // while(policy->readyListSize());

    finalize();
  }

} // namespace LSE_scheduler
