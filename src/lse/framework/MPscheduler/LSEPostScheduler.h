/* 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * List scheduler for the LSE environment
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * This file is a port of LSE's python scripts, through several revisions.
 *
 * This post scheduler performs several tasks.
 * 1) It maps clusters to threads.  Not wisely, but it does so.
 * 2) Trys to discover when locks are not needed for crossthread comm
 * 3) Assigns the SchedulableDAG the final schedule, divided into threads
 * 4) Actually Instantiates the locks
 *
 */

#ifndef _LSE_POST_SCHEDULER_H_
#define _LSE_POST_SCHEDULER_H_

#include "PostScheduler.h"
#include "SchedulableDAG.h"
#include "LSEtasks.h"
#include "stdio.h"
#include <vector>
#include <map>

namespace LSE_scheduler {

  /**
   * This class is used to track cross thread communication
   **/
  class crosscomm_ent_t {
    public:
      std::vector<schedtime_t> iknown;
      Schedulable* src;
      schedtime_t src_start;
      Schedulable* dst;
      schedtime_t dst_start;
      int semno;
      crosscomm_ent_t() {}
      crosscomm_ent_t(std::vector<schedtime_t> &ik, 
          Schedulable* si, 
          schedtime_t ss, 
          Schedulable* di, 
          schedtime_t ds, 
          int sn) 
        : iknown(ik), 
        src(si), 
        src_start(ss), 
        dst(di), 
        dst_start(ds), 
        semno(sn)
      {}
  };

  class LSEPostScheduler : public PostScheduler
  {
    public: 


      // used to track holes.  The first parameter is the start time
      // of the hole, the second is the end time
      typedef std::map<schedtime_t, schedtime_t> holes_t;

      // used to track extra information about the schedulables
      struct nodeflags_t{ 
        int  unscheduled_preds; //the number of unscheduled predecessors
        bool ready;
      };

      // used to map a time in the schedule to the cross 
      // thread communication occuring at that time.
      // the index into the vector is the thread on which the cross comm 
      // is occuring

      typedef std::map<schedtime_t, crosscomm_ent_t> crosscomm_t;
      typedef std::vector<crosscomm_t> crosscomms_t;

    private: //Members
      FILE* logfile; 
      int semCnt;               // semaphore counter
      int numThreads;           // The number of threads to schedule onto.
      std::map<Schedulable*, nodeflags_t> nodeflags;      // node information
      std::vector<schedtime_t> mkspan;  // per-thread  current makespan
      std::vector<holes_t> holes;      // per-thread holes in schedules
      crosscomms_t crossheads;    //what thread is synchronized to what thread
      SchedulableDAG * graph;
      threadingParms P;

    private: //Methods

      /**
       * Find an insertion point into the schedule.  If the policy allows for hole 
       * filling, look into the hole struct for reasonable times.
       */
      ipoint_t find_ipoint(Schedulable* node);
      void adjustIpoint(Schedulable *node, ipoint_t &newip);

      /**
       * Takes the threads, and puts them into the SchedulableDAGs
       * Schedule.  Also, set up the locks
       */
      void finalize();
      bool covered(Schedulable *, Schedulable *);

    public:
    LSEPostScheduler(const threadingParms &pi) : P(pi), logfile(NULL), semCnt(0)
    {
    }
      /**
       * Runs the actual post scheduling routine.  This routine uses a 
       * list scheduler to put cluster's onto threads in a semi-intellegent
       * fashion.  It then produces the final schedule that will be run
       */
      void run(SchedulableDAG& graph , int numThreads, FILE* logfile);
  };
}


#endif
