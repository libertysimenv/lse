#ifndef COMMUNICATION_MINIMIZATION_REDUCER_H
#define COMMUNICATION_MINIMIZATION_REDUCER_H

#include "Reducer.h"
#include "Policy.h"
#include "LSEtasks.h"
#include "Schedulable.h"

#include <list>
#include <map>
#include <vector>
#include <queue>
#include <algorithm>

namespace LSE_scheduler {
  class CommunicationMinimizationReducer : public Reducer {
    private:
      SchedulableDAG* graph;
      FILE* logfile;
      size_t numThreads;

      typedef std::map< std::pair<int, int> , schedtime_t > speaking_t;
      speaking_t speaking;

      struct clustDataElem {
        clustDataElem():size(0), thread(-1) {}
        schedtime_t size;
        int thread;
      };

      typedef std::map<int,clustDataElem> clustData_t;
      clustData_t clustData;

      typedef std::list<Schedulable*> members_t;
      typedef std::map< int, members_t > clustersTracker_t;
      clustersTracker_t clusters;

      struct commClustCostElem  {
        commClustCostElem(int clust1, int clust2, schedtime_t agg_cost): 
          clust1(clust1), clust2(clust2), agg_cost(agg_cost){}

        int clust1, clust2;
        schedtime_t agg_cost;

        bool operator< (const commClustCostElem &rhs) const {
          return agg_cost < rhs.agg_cost;
        }

        void print() {
          std::cout<< "clust1: " << clust1 << " clust2: " << clust2 
            << " agg_couts: "<< agg_cost << "\n";
        }
      };

      struct threadUseElem {
        threadUseElem(): use(0), idno(-1){}
        threadUseElem(int idno) : use(0), idno(idno) {}
        int use;
        int idno;

        void print(){
          std::cout<< "use: "<< use << " idno: " << idno << "\n";
        }
      };

      struct compare_threadUseElem {
        bool operator()(const threadUseElem &lhs, const threadUseElem &rhs)
        {
          if ( lhs.use == rhs.use )
            return lhs.idno < rhs.idno;
          else
            return lhs.use < rhs.use;
        }
      };

      std::map<int,bool> assigned;

      typedef std::vector<threadUseElem> threadUse_t;
      threadUse_t threadUse;

      std::priority_queue<commClustCostElem> commClustCosts;

      void doCommunicationMinimization();
      int formClusters();
      void reportThreads();

    public:
      CommunicationMinimizationReducer();

      void run(SchedulableDAG& graph, int numThreads, FILE* logfile);
  };
}

#endif

