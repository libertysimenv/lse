#include "CHASM.h"

namespace LSE_scheduler {
  std::map<Schedulable*, CHASM::nodeInfo_t> CHASM::nodeInfo;
  SchedulableDAG* CHASM::graph;
  int CHASM::clusterInfo_t::runningClusterNumber;

  CHASM::CHASM() : Clusterer() {}

  void CHASM::doCHASM() {
    setM(1);
    step1(graph);
    step2(graph);
    steps3and4(graph);
    step5(graph);
    reportBack(graph);
  }

  void CHASM::run(SchedulableDAG& graph, FILE* logfile) {
    this->graph = &graph;
    if(logfile) fprintf(logfile, "====Start CHASM Clusterer====\n");
    this->logfile = logfile;
    doCHASM();
    std::cout << "Finished clustering" << std::endl;
  }

  void CHASM::setM(unsigned int M) {
    this->M = M;
  }

  void CHASM::step1(SchedulableDAG* tg) {
    //For each node in the graph
    for(schedulableIterator i = tg->begin(),
        ie = tg->end(); i != ie; ++i) {
      Schedulable* nodeSched = *i;
      nodeInfo[nodeSched].set_node(nodeSched);
      nodeInfo[nodeSched].set_policy(policy);
      nodeInfo[nodeSched].set_M(M);
    }

    for(schedulableIterator i = tg->begin(),
        ie = tg->end(); i != ie; ++i) {
      Schedulable* nodeSched = *i;

      //Fill in the list of edges
      for(schedulableIterator j = nodeSched->pred_begin(),
          je = nodeSched->pred_end(); j != je; ++j) {
        nodeInfo[nodeSched].addEdge(&nodeInfo[*j]);
      }

      nodeInfo[nodeSched].update_tc();
    }
  }

  void CHASM::step2(SchedulableDAG* tg) {
    std::queue<Schedulable*> readyList;
    std::map<Schedulable*, int> unusedPreds;

    //Calculate the start time of each node
    for(schedulableIterator i = tg->begin(),
        ie = tg->end(); i != ie; ++i) {
      //Check for source nodes
      Schedulable* nodeSched = *i;
      int preds = nodeSched->getPredecessorCount();
      unusedPreds[nodeSched] = preds;
      if(!preds) {
        readyList.push(nodeSched);
      }
    }

    //Operate on the ready list
    while(!readyList.empty()) {
      //Get a node to work on
      Schedulable* nodeSched = readyList.front();
      readyList.pop();

      //Find the latest finish time of a predecessor
      schedtime_t latestFinish = 0;
      schedtime_t commCosts = 0;
      for(schedulableIterator i = nodeSched->pred_begin(),
          ie = nodeSched->pred_end(); i != ie; ++i) {
        latestFinish = std::max(latestFinish, nodeInfo[*i].finish);
        commCosts += policy->naiveCommCost(*i, nodeSched);
      }

      //Sanity check
      assert(commCosts == nodeInfo[nodeSched].get_sc() && "Your communication costs are screwed up");

      //Start nodeSched as its latest predecessor finishes
      //TODO: Determine if this should be latestFinish+1
      nodeInfo[nodeSched].set_start(latestFinish);

      //Set the finish time of nodeSched
      nodeInfo[nodeSched].set_finish(latestFinish + 
				     policy->nodeCost(nodeSched) + 
				     nodeInfo[nodeSched].get_tc());

      //Refresh the ready list
      for(schedulableIterator i = nodeSched->succ_begin(),
          ie = nodeSched->succ_end(); i != ie; ++i) {
        Schedulable* succ = *i;
        int preds = unusedPreds[succ];
        preds--;
        unusedPreds[succ] = preds;
        if(!preds) {
          readyList.push(succ);
        }
      }
    }
  }

  void CHASM::steps3and4(SchedulableDAG* tg) {
    //Keep track of clusters
    clusterInfo_t::runningClusterNumber = 0;

    //Prep things for step 4
    std::map<Schedulable*, int> unusedSuccs;
    std::queue<Schedulable*> readyList;

    //Calculate f values for the sink nodes
    for(schedulableIterator i = tg->begin(),
        ie = tg->end(); i != ie; ++i) {
      Schedulable* nodeSched = *i;
      int succs = nodeSched->getSuccessorCount();
      unusedSuccs[nodeSched] = succs;
      //If this is a sink node
      if(!succs) {
        int cluster = clusterInfo_t::runningClusterNumber++;
        nodeInfo[nodeSched].update_f();

        //Assign sink nodes to unique clusters
        assert(clusterInfo.find(cluster) == clusterInfo.end());
        clusterInfo[cluster].setClusterNumber(cluster);
        clusterInfo[cluster].addNode(nodeSched);
        nodeInfo[nodeSched].set_cluster(cluster);

        //Prep things for step 4
        readyList.push(nodeSched);
      }
    }

    while(!readyList.empty()) {
      //Get the first ready node
      Schedulable* nodeSched = readyList.front();
      readyList.pop();

      //Calculate the height
      unsigned int height = 0;
      for(schedulableIterator i = nodeSched->succ_begin(),
          ie = nodeSched->succ_end(); i != ie; ++i) {
        height = std::max(height, nodeInfo[*i].get_height()+1);
      }
      nodeInfo[nodeSched].set_height(height);

      //Step 4
      //If nodeSched's children are all sink nodes
      if(height == 1) {
        //Find the dominant successor
        for(schedulableIterator j = nodeSched->succ_begin(),
            je = nodeSched->succ_end(); j != je; ++j) {
          Schedulable* succ = *j;
          //Make a new priority tuple
          priority_tuple tup;
          tup.u = &nodeInfo[nodeSched];
          tup.v = &nodeInfo[succ];
          tup.r = policy->naiveCommCost(nodeSched, succ)/
	    (policy->nodeCost(nodeSched) + nodeInfo[nodeSched].get_sc());
          tup.l = nodeInfo[succ].get_start() + nodeInfo[succ].get_f();

          CL.push(tup);
        }

        //Calculate f
        nodeInfo[nodeSched].update_f();
      }

      //Refresh the ready list
      for(schedulableIterator i = nodeSched->pred_begin(),
          ie = nodeSched->pred_end(); i != ie; ++i) {
        Schedulable* pred = *i;
        int succs = unusedSuccs[pred];
        succs--;
        unusedSuccs[pred] = succs;
        if(!succs)
          readyList.push(pred);
      }
    }
  }

  void CHASM::step5(SchedulableDAG* tg) {
    std::map<Schedulable*, int> unscheduledSuccs;
    for(schedulableIterator i = tg->begin(),
        ie = tg->end(); i != ie; ++i) {
      Schedulable* nodeSched = *i;
      unscheduledSuccs[nodeSched] = nodeSched->getSuccessorCount();
    }

    //Reflect the effect of already having scheduled the sink nodes
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      Schedulable* currNode = *i;
      if(unscheduledSuccs[currNode] == 0) {
        for(schedulableIterator j = currNode->pred_begin(),
            je = currNode->pred_end(); j != je; ++j) {
          int succs = unscheduledSuccs[*j];
          succs--;
          unscheduledSuccs[*j] = succs;
        }
      }
    }

    while(!CL.empty()) {
      //Get the top-priority tuple
      priority_tuple currentTuple = CL.top();
      CL.pop();

      Schedulable* nodeSched = currentTuple.u->node;
      Schedulable* domSucc = currentTuple.v->node;

      //Calculate tc and f of nodeSched so we have something to compare against
      //nodeInfo[nodeSched].update_tc();
      //nodeInfo[nodeSched].update_f();

      //If u has already been considered
      int clusterNum = nodeInfo[nodeSched].get_cluster();
      if(nodeInfo[nodeSched].get_cluster() > -1) { //This can only happen when v is a sink node and all u's successors are sink nodes
        assert(clusterInfo.find(clusterNum) != clusterInfo.end());
        //If clustering v into the cluster of u immediately following u is acceptable
        if(acceptableToClusterSink(nodeSched, domSucc, &clusterInfo[clusterNum])) {
          consumeSink(nodeSched, domSucc, &clusterInfo[clusterNum]);
        }
        //Do not let these nodes go on and try to refresh the CL.
        //Doing so would wreak severe havoc on the heuristic.
        continue;
      }
      //Else if clustering u at the top of the cluster of v is acceptable
      else if(schedtime_t new_f = acceptableToCluster(nodeSched, domSucc, &clusterInfo[nodeInfo[domSucc].get_cluster()])) {
        combineWithSucc(nodeSched, domSucc, &clusterInfo[nodeInfo[domSucc].get_cluster()], new_f);
      }
      //Else assign u to its own cluster
      else {
        //Assign nodeSched to its own cluster
        assert(nodeInfo[nodeSched].get_cluster() == -1);
        assert(clusterInfo.find(clusterInfo_t::runningClusterNumber) == clusterInfo.end());

        int cluster = clusterInfo_t::runningClusterNumber++;
        clusterInfo_t* newCluster = &clusterInfo[cluster];
        newCluster->setClusterNumber(cluster);
        newCluster->addNode(nodeSched);
        nodeInfo[nodeSched].set_cluster(cluster);
      }

      //Find the new current nodes and stick them in CL
      for(schedulableIterator i = nodeSched->pred_begin(),
          ie = nodeSched->pred_end(); i != ie; ++i) {
        Schedulable* pred = *i;
        int succs = unscheduledSuccs[pred];
        succs--;
        unscheduledSuccs[pred] = succs;
        if(!succs) {
          //Find the dominant successor
          Schedulable* newDomSucc = NULL;
          schedtime_t succ_f = 0;
          for(schedulableIterator j = pred->succ_begin(),
              je = pred->succ_end(); j != je; ++j) {
            Schedulable* predSucc = *j;
            assert(nodeInfo[predSucc].get_cluster() > -1);
            schedtime_t temp_f = nodeInfo[predSucc].get_f();
            if(temp_f > succ_f) {
              succ_f = temp_f;
              newDomSucc = predSucc;
            }
          }

          //Make sure the dominant successor has a real cluster
          assert(nodeInfo[newDomSucc].get_cluster() > -1);

          //Make a new priority tuple
          nodeInfo[pred].update_tc();
          nodeInfo[pred].update_f();
          priority_tuple tup;
          tup.u = &nodeInfo[pred];
          tup.v = &nodeInfo[newDomSucc];
          tup.r = policy->naiveCommCost(pred, newDomSucc)/
	    (policy->nodeCost(pred) + nodeInfo[pred].get_sc());
          tup.l = nodeInfo[newDomSucc].get_start() + 
	    nodeInfo[newDomSucc].get_f();

          //Stick the tuple on the Current List
          CL.push(tup);
        }
      }
    }
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
    }
  }

  void CHASM::reportBack(SchedulableDAG* tg) {
    //It is entirely possible that a number of clusters in the
    //sequential ordering got deleted.  Therefore, we re-map
    //all clusters to a continugous sequence of cluster numbers.
    std::map<int, int> clusterMap;
    int n = 0;
    for(int i = 0, ie = clusterInfo_t::runningClusterNumber; i < ie; ++i) {
      std::map<int, clusterInfo_t>::iterator curr = clusterInfo.find(i);
      if(curr == clusterInfo.end())
        continue;
      clusterMap[i] = n++;
    }

    tg->setClusterCount(n);
    for(schedulableIterator i = tg->begin(),
        ie = tg->end(); i != ie; ++i) {
      Schedulable* nodeSched = *i;
      int cluster = clusterMap[nodeInfo[nodeSched].get_cluster()];
      nodeSched->setCluster(cluster);
      //std::cout << "Node " << (i - tg->begin()) << " cluster " << cluster << std::endl;
    }
    //std::cout << "Cluster count is " << n << std::endl;
  }

  //Helper functions
  schedtime_t CHASM::speculate_new_f(Schedulable* node, Schedulable* domSucc, clusterInfo_t* cluster) const {
    schedtime_t edgeWeight = policy->naiveCommCost(node, domSucc);

    //Various possible fs to compare
    schedtime_t f_direct = 0;
    schedtime_t f_cluster = 0;

    //Cluster properties
    Schedulable* clusterTop = cluster->getTopNode();
    schedtime_t cluster_f = nodeInfo[clusterTop].get_f();

    schedtime_t domSucc_f = nodeInfo[domSucc].get_f();

    //See what would happen if node were clustered with cluster
    bool domSuccFirst = (domSucc == clusterTop);
    //If the dominant successor was at the top of the cluster
    if(domSuccFirst) {
      domSucc_f -= edgeWeight;

      //Find the new dominant successor
      schedtime_t succ_f = 0;
      for(schedulableIterator i = node->succ_begin(),
          ie = node->succ_end(); i != ie; ++i) {
        schedtime_t temp_f = nodeInfo[*i].get_f();
        if(*i == domSucc)
          temp_f = domSucc_f;
        succ_f = std::max(succ_f, temp_f);
      }

      //See how node's f is affected
      f_direct = succ_f + policy->nodeCost(node) + nodeInfo[node].get_tc();
    }
    else {
      //Find the f from putting node at the top of the cluster
      f_cluster = cluster_f + policy->nodeCost(node) + nodeInfo[node].get_tc();
    }

    schedtime_t node_f = std::max(f_cluster, f_direct);
    return node_f;
  }

  schedtime_t CHASM::acceptableToCluster(Schedulable* node, Schedulable* domSucc, clusterInfo_t* cluster) {
    schedtime_t current_node_f = nodeInfo[node].get_f();
    schedtime_t new_node_f = speculate_new_f(node, domSucc, cluster);

    if(new_node_f <= current_node_f)
      return new_node_f;
    else
      return 0;
  }

  bool CHASM::acceptableToClusterSink(Schedulable* node, Schedulable* sink, clusterInfo_t* cluster) {
    int sinkCluster = nodeInfo[sink].get_cluster();

    //If the sink has already been combined with other nodes, it's not acceptable to cluster
    if(clusterInfo[sinkCluster].getSize() > 1)
      return false;

    int nodeIndex = cluster->getNodePosition(node);
    int nextIndex = nodeIndex - 1;

    assert(node == cluster->getNodeAt(nodeIndex));

    //If there is no next node in the cluster, there is not room for the sink
    if(nextIndex < 0)
      return false;

    Schedulable* nodeNext = cluster->getNodeAt(nextIndex);

    //TODO: Are the f values for both node and nodeNext correct?
    schedtime_t g = nodeInfo[node].get_f();
    schedtime_t h = nodeInfo[nodeNext].get_f();
    schedtime_t tau_u = policy->nodeCost(node);
    schedtime_t tau_v = policy->nodeCost(sink);
    schedtime_t tc_u = nodeInfo[node].get_tc();

    //Find the value tc_v will take on if clustering is successful
    schedtime_t tc_v_new = 0;
    schedtime_t maxEdge = 0;

    if(nodeInfo[sink].edgesCount() > 1) {
      std::list<nodeInfo_t*>::iterator maxPred = --nodeInfo[sink].edges_end();
      if((*maxPred)->get_node() == node)
        maxPred--;

      maxEdge = policy->naiveCommCost((*maxPred)->get_node(), sink);

      schedtime_t new_sc = nodeInfo[sink].get_sc() - policy->naiveCommCost(node, sink);
      schedtime_t parComm = new_sc / M;
      if(M * parComm < (unsigned)new_sc)
        parComm++;

      //tc_v_new <- max(maxEdge, parComm)
      if(maxEdge > parComm)
        tc_v_new = maxEdge;
      else
        tc_v_new = parComm;
    }
    else
      tc_v_new = 0;

    //Check that the sink node will fit in the cluster after "node"
    if((g - tau_u - tc_u - h) < (tau_v + tc_v_new))
      return false; //If it won't fit

    //Check that moving sink into the cluster will not increase f_sink
    if(h + tau_v + tc_v_new > nodeInfo[sink].get_f())
      return false;

    //Otherwise, we're good
    return true;
  }

  void CHASM::consumeSink(Schedulable* nodeSched, Schedulable* sink, clusterInfo_t* cluster) {
    //Get the node that will soon be following sink
    int nodePosition = cluster->getNodePosition(nodeSched);
    int followingPosition = nodePosition-1;
    assert(followingPosition > -1);
    Schedulable* following = NULL;
    following = cluster->getNodeAt(followingPosition);

    //Assign sink to nodeSched's cluster
    assert(clusterInfo[nodeInfo[sink].get_cluster()].getSize() == 1);
    clusterInfo.erase(clusterInfo.find(nodeInfo[sink].get_cluster()));
    nodeInfo[sink].set_cluster(cluster->getClusterNumber());
    cluster->addNode(sink, nodeSched);

    //Just a quick assert check
    Schedulable* check = cluster->getNodeAt(followingPosition);
    assert(following == check);

    //Update sc and incomingEdges of sink
    nodeInfo[sink].eraseEdge(&nodeInfo[nodeSched]);

    //Update sink's tc
    nodeInfo[sink].update_tc();

    //Update sink's f (cannot use the update_f() method)
    schedtime_t following_f = nodeInfo[following].get_f();
    schedtime_t new_f = following_f + policy->nodeCost(sink) 
      + nodeInfo[sink].get_tc();
    nodeInfo[sink].set_f(new_f);
  }

  void CHASM::combineWithSucc(Schedulable* nodeSched, Schedulable* succ, clusterInfo_t* cluster, schedtime_t new_f) {
    assert(nodeInfo[nodeSched].get_cluster() == -1);

    //Find out the first node of the cluster
    Schedulable* topNode = cluster->getTopNode();
    bool succWasTop = (topNode == succ);

    //Assign nodeSched to sink's cluster
    assert(cluster->getClusterNumber() == nodeInfo[succ].get_cluster());
    nodeInfo[nodeSched].set_cluster(cluster->getClusterNumber());
    cluster->addNode(nodeSched);

    //Update sc and e of succ
    nodeInfo[succ].eraseEdge(&nodeInfo[nodeSched]);

    //If succ was the first node of the cluster
    if(succWasTop) {
      schedtime_t tc_old = nodeInfo[succ].get_tc();
      schedtime_t tc_new = nodeInfo[succ].update_tc();
      nodeInfo[succ].set_f(nodeInfo[succ].get_f() - (tc_old - tc_new));
    }

    //Update nodeSched's f
    nodeInfo[nodeSched].set_f(new_f);
  }
}

