#ifndef LOAD_BALANCE_REDUCER_H
#define LOAD_BALANCE_REDUCER_H

#include "Reducer.h"
#include "Policy.h"
#include "LSEtasks.h"
#include "Schedulable.h"

#include <set>
#include <vector>
#include <map>
#include <queue>

namespace LSE_scheduler {
  class LoadBalanceReducer : public Reducer {
    private:
      struct cluster_t {
        schedtime_t execution;
        std::set<Schedulable*> members;
        std::set<cluster_t*> adjacentClusters;
        std::set<cluster_t*> consumedClusters;
        bool valid;

        cluster_t() : execution(0), valid(true) {}

        void addTask(Schedulable* task, Policy* policy) {
          bool added = members.insert(task).second;
          if(added)
            execution += policy->nodeCost(task);
        }

        void addAdjacentCluster(cluster_t* adjacentCluster) {
          if(adjacentCluster != this)
            adjacentClusters.insert(adjacentCluster);
        }
      };

      struct clusterPriority {
        bool operator()(const cluster_t* lhs, const cluster_t* rhs) const {
          if(lhs->execution == rhs->execution)
            return lhs->members < rhs->members;
          else
            return lhs->execution < rhs->execution;
        }
      };

      std::vector<cluster_t> clusters;
      std::map<Schedulable*, cluster_t*> task2cluster;

      SchedulableDAG* graph;
      size_t numThreads;
      FILE* logfile;

      void doLoadBalancing();

      void formClusters();
      void combineClusters();
      void reportThreads();
      void visitForThreads(cluster_t &, int);

    public:
      LoadBalanceReducer();

      void run(SchedulableDAG& graph, int numThreads, FILE* logfile);
  };
}

#endif

