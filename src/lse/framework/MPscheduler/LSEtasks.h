/* 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Top-level MP scheduling for the simulator.
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * The main loop of the simulator
 *
 * TODO:  
 *
 */
#ifndef _LSETASKS_H_
#define _LSETASKS_H_

#include <LSE_inttypes.h>
#include "SchedulableDAG.h"
#include <set>
#include <sstream>
#include <string>

namespace LSE_scheduler {

  class threadingParms {
  public:

    int  numThreads;      // obviously the number of threads to use

    enum lockhandling_t { ignore, avoid, skew };
    lockhandling_t lockHandling;
    int lockOffsetFactor;
    
    // Uniform: cost = blockCost
    enum blockcost_t { BlockUniform, BlockMeasure };
    blockcost_t blockCostModel;
    int blockCost;

    // Uniform = all have commCost.
    // Bandwidth = all have # signals * thread_comm_cost;
    enum commcost_t { CommUniform, CommBandwidth };
    commcost_t commCostModel;
    int commCost;

    enum clustering_t { none, inst, DSC, forest, CHASM };
    clustering_t clusterHandling;

    enum reduction_t { noneR, instR, balanceR, RACH, LoadBalanceReducer, CommunicationMinimizationReducer };
    reduction_t reductionHandling;

    bool isPush;

    int trainingLength;          // 0 = none
    double trainingConvergence;  // 0 = no convergence check

    threadingParms()
      : numThreads(1), lockHandling(ignore), lockOffsetFactor(10), 
      blockCostModel(BlockUniform), blockCost(1),
      commCostModel(CommUniform), commCost(1),
      clusterHandling(none), reductionHandling(noneR),
      isPush(true),
      trainingLength(10), 
      trainingConvergence(0.0)
      { }
  };

  class schednode_t : public LSE_scheduler::Schedulable {
   public:
    uint64_t critDistance;
    int singleOrder;  // serves as its ID as well.
    int order;
    std::set<int> locks, sends, recvs;
    int db_InstNo;
    std::string name;

    // needs to be recomputed each time
    std::set<int> validThreads;

    schednode_t(int ID, int SO, int ino, const std::string &n) : 
    LSE_scheduler::Schedulable(ID), singleOrder(SO), db_InstNo(ino),
      name(n){ }

    std::string getName() {
      std::ostringstream ss;
      ss << "Node " << getID() << "(" << singleOrder << ") " << name;
      return ss.str();
    }

    void addPredecessor(schednode_t* pred) { 
      LSE_scheduler::Schedulable::addPredecessor(pred); 
    }
    void addSuccessor(schednode_t * succ) { 
      LSE_scheduler::Schedulable::addSuccessor(succ); 
    }
    void addLocalitySibling(schednode_t * sib) { 
      LSE_scheduler::Schedulable::addLocalitySibling(sib); 
    }
    size_t getAllocSize() { return 0; }
    void embed(void *) {}
    LSE_scheduler::std_hookup getHookup(Schedulable* sink) {
      return 0; 
    }
  };

  class TaskGraph : public LSE_scheduler::SchedulableDAG {
    edge_weight_t edges2;

  public:
    typedef LSE_scheduler::schedulableIterator iterator;

    void addTask(LSE_scheduler::Schedulable *t) { tasks.push_back(t); }
    void addEdge(int f, int t, int w, int w2) { 
      SchedulableDAG::addEdge(tasks[f], tasks[t], w);
      ((schednode_t *)(tasks[f]))->addSuccessor((schednode_t *)(tasks[t]));
      ((schednode_t *)(tasks[t]))->addPredecessor((schednode_t *)(tasks[f]));
      edges2[tasks[f]].insert(std::make_pair(tasks[t], w2));
    }
    // Note: this gets the task in the order it was added, not its ID order.
    // we use block_sched_list order for order to add and original scheduler
    // ID for the ID (so we get the same schedules we did in Python)
    schednode_t *getTask(int i) { return (schednode_t *)(tasks[i]); }

    int getEdgeWeight2(Schedulable* source, Schedulable* sink) {
      std::map<Schedulable*, std::map<Schedulable*, int> >::iterator 
	rowIter = edges2.find(source);
      if(rowIter == edges2.end())
	return -1;
      
      std::map<Schedulable*, int>* row = &rowIter->second;
      std::map<Schedulable*, int>::iterator colIter = row->find(sink);
      if(colIter == row->end())
	return -1;
      
      return colIter->second;
      
      //return edges[source][sink];
    }
  };

}

extern LSE_scheduler::threadingParms LSEfw_threadingParms;

extern void LSEfw_perform_scheduling(LSE_scheduler::TaskGraph &, 
				     LSE_scheduler::TaskGraph &, 
				     LSE_scheduler::threadingParms &);

#endif
