#include "CommunicationMinimizationReducer.h"

namespace LSE_scheduler {
  CommunicationMinimizationReducer::CommunicationMinimizationReducer() : Reducer() {}

  void CommunicationMinimizationReducer::run(SchedulableDAG& graph, int numThreads, FILE* logfile) {
    this->graph = &graph;
    this->numThreads = numThreads;
    if(logfile) fprintf(logfile, "====Start Communication Minimization Reducer====\n");
    this->logfile = logfile;
    doCommunicationMinimization();
    std::cout << "Finished reduction" << std::endl;
  }

  void CommunicationMinimizationReducer::doCommunicationMinimization() {
    formClusters();

    // Initialize the cluster sizes
    for(clustersTracker_t::iterator i = clusters.begin(), 
        ie = clusters.end(); i != ie; ++i ) {
      for(members_t::iterator j = i->second.begin(), 
          je = i->second.end(); j != je; ++j) {
        clustData[i->first].size += policy->nodeCost(*j); //(*j)->getCost();
      }
      assigned[i->first] = false;
      clustData[i->first].thread = i->first;
    }

    // Initialize the thread use structures
    for(int i = 0; i < numThreads; i++ )
      threadUse.push_back(threadUseElem(i));

    int numClusters = clusters.size();
    assert(numClusters == clustData.size());
    while(numClusters > numThreads && !commClustCosts.empty()) {
      //the top communicating pair of clusters
      commClustCostElem toCluster = commClustCosts.top();
      commClustCosts.pop();
      threadUse_t::iterator thread;

      //assigned is a mapping of cluster id's to a bool to indicate
      //whether or not is has been assigned
      if(assigned[toCluster.clust1] && assigned[toCluster.clust2])
        continue;
      if(assigned[toCluster.clust1]) {
        int tno = clustData[toCluster.clust1].thread;
        threadUse[tno].use += clustData[toCluster.clust2].size;
        clustData[toCluster.clust2].thread = tno;
        assigned[toCluster.clust2] = true;
      }
      else if(assigned[toCluster.clust2]) {
        int tno = clustData[toCluster.clust2].thread;
        threadUse[tno].use += clustData[toCluster.clust1].size;
        clustData[toCluster.clust1].thread = tno;
        assigned[toCluster.clust1] = true;
      }
      else {
        thread = (std::min_element(threadUse.begin(), threadUse.end(), compare_threadUseElem()));
        thread->use += clustData[toCluster.clust1].size + clustData[toCluster.clust2].size;
        assigned[toCluster.clust1] = true;
        assigned[toCluster.clust2] = true;
        clustData[toCluster.clust1].thread = thread->idno;
        clustData[toCluster.clust2].thread = thread->idno;	      
      }

      numClusters--;
    }

    /*// Fix any remaining stray clusters
    while(numClusters > numThreads) {

    }*/

    // Remap threads
    std::map<int,int> threadRemapping;
    int currThread = 0;
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      Schedulable* currNode = *i;
      int clusterNumber = currNode->getCluster();
      int threadNumber = clustData[clusterNumber].thread;

      std::map<int,int>::iterator remap = threadRemapping.find(threadNumber);
      if(remap == threadRemapping.end()) {
        threadRemapping[threadNumber] = currThread;
        currThread++;
      }
    }
    //printf("Final number of threads: %d\n", threadRemapping.size());
    //printf("currThread: %d\n", currThread);

    // copy into graph
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      Schedulable* currNode = *i;
      int clusterNumber = currNode->getCluster();
      int threadNumber = clustData[clusterNumber].thread;
      int actualThread = threadRemapping.find(threadNumber)->second;
      if(actualThread < numThreads) {
        currNode->setThread(actualThread);
      }
    }
  }

  int CommunicationMinimizationReducer::formClusters() {
    //Push all nodes in a cluster in clusters
    // Also find the amount of communication between each cluster
    for(schedulableIterator i = graph->begin(), 
        e = graph->end(); i != e; ++i) {
      Schedulable* currNode = *i;

      //Put in clusters
      clusters[currNode->getCluster()].push_back(currNode);

      //find if it communicates with any other clusters
      for(schedulableIterator j = currNode->succ_begin(), 
          je = currNode->succ_end(); j != je; ++j) {
        Schedulable* succNode = *j;
        if(currNode->getCluster() != succNode->getCluster()) {
          std::pair<int,int> talking;
          if(currNode->getCluster() > succNode->getCluster())
            talking = std::make_pair(currNode->getCluster(), succNode->getCluster());
          else
            talking = std::make_pair(succNode->getCluster(), currNode->getCluster());

          if(speaking.find(talking) == speaking.end())
            speaking[talking] = 0;
          speaking[talking] += policy->naiveCommCost(currNode, succNode);
        }
      }
    } // for i in nodes

    //Put all of the found aggregate costs onto the commClustCosts 
    for(speaking_t::iterator i = speaking.begin(),
        ie = speaking.end(); i != ie; ++i) {
      commClustCosts.push(commClustCostElem(i->first.first, i->first.second, i->second) );
    }

    return clusters.size();
  }

  void CommunicationMinimizationReducer::reportThreads() {

  }
}

