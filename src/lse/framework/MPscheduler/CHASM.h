#ifndef CHASM_H
#define CHASM_H

#include <vector>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <algorithm>
#include <iostream>

#include "LSEtasks.h"
#include "Policy.h"
#include "Clusterer.h"
#include "Schedulable.h"

namespace LSE_scheduler {
  class CHASM : public Clusterer {
    private:

      //very temporary function
      bool clustersOccupied() {
        for(std::map<int, clusterInfo_t>::iterator i = clusterInfo.begin(),
            ie = clusterInfo.end(); i != ie; ++i) {
          if(i->second.getSize() == 0)
            return false;
        }

        return true;
      }

      struct nodeInfo_t {
        Schedulable* node;
        Policy* policy;
        schedtime_t sumOfCommunications; //sc_v in the algorithm
        std::list<nodeInfo_t*> incomingEdges; //e_v in the algorithm
        double tc; //tc_v in the algorithm
        schedtime_t start; //s_v in the algorithm
        schedtime_t finish;
        schedtime_t f; //f_v in the algorithm
        int cluster;
        unsigned int M; //The magic number
        unsigned int height;

        //Comparison struct for the set of iterators
        struct iterComp {
          bool operator()(const std::list<nodeInfo_t*>::iterator& lhs, const std::list<nodeInfo_t*>::iterator& rhs) const {
            return &lhs < &rhs;
          }
        };

        /**
         * Constructor
         */
        nodeInfo_t() : sumOfCommunications(0), cluster(-1), M(1) {}

        /**
         * Takes care of the details of adding a predecessor to this node.
         */
        void addEdge(nodeInfo_t* pred) {
          schedtime_t commCost = policy->naiveCommCost(pred->node, node);
          std::list<nodeInfo_t*>::iterator i = incomingEdges.begin();
          std::list<nodeInfo_t*>::iterator ie = incomingEdges.end();
          for(; i != ie; ++i) {
            schedtime_t tempCost = policy->naiveCommCost((*i)->node, node);
            if(commCost <= tempCost)
              break;
          }

          //Add the edge
          incomingEdges.insert(i, pred);

          //Add edge weight to sumOfCommunications
          sumOfCommunications += commCost;
        }

        /**
         * Takes care of the details of zeroing an edge coming into this node.
         */
        void eraseEdge(nodeInfo_t* pred) {
          schedtime_t commCost = policy->naiveCommCost(pred->get_node(), node);

          std::list<nodeInfo_t*>::iterator i = incomingEdges.begin();
          for(std::list<nodeInfo_t*>::iterator ie = incomingEdges.end(); i != ie; ++i) {
            if(*i == pred)
              break;
          }
          assert(i != incomingEdges.end());
          incomingEdges.erase(i);
          sumOfCommunications -= commCost;
        }

        /**
         * Automatic update methods
         */
        schedtime_t update_tc() {
          if(incomingEdges.size() == 0) {
            assert(sumOfCommunications == 0);

            tc = 0;
          }
          else {
            schedtime_t maxEdge = policy->naiveCommCost((*incomingEdges.rbegin())->get_node(), node);
            assert(maxEdge > -1);
            schedtime_t parComm = sumOfCommunications / M;
            assert(parComm > -1);

            //Round up for parComm
            if(M * parComm < sumOfCommunications)
              parComm++;

            //Take the greater of the two
            if(maxEdge > parComm)
              tc = maxEdge;
            else
              tc = parComm;
          }

          return tc;
        }

        schedtime_t update_f() {
          //Find the max f of a child
          schedtime_t succ_f = 0;
          for(schedulableIterator i = node->succ_begin(),
              ie = node->succ_end(); i != ie; ++i) {
            schedtime_t temp_f = nodeInfo[*i].get_f();
            succ_f = std::max(succ_f, temp_f);
          }

          f = succ_f + node->getCost() + tc;

          return f;
        }

        /**
         * Accessor methods.
         */
        void set_node(Schedulable* n) {
          node = n;
        }

        Schedulable* get_node() {
          return node;
        }

        void set_policy(Policy* p) {
          policy = p;
        }

        Policy* get_policy() {
          return policy;
        }

        std::list<nodeInfo_t*>::iterator edges_begin() {
          return incomingEdges.begin();
        }

        std::list<nodeInfo_t*>::iterator edges_end() {
          return incomingEdges.end();
        }

        int edgesCount() {
          return incomingEdges.size();
        }

        void set_tc(schedtime_t new_tc) {
          tc = new_tc;
        }

        schedtime_t get_tc() {
          return tc;
        }

        void set_sc(schedtime_t sc) {
          sumOfCommunications = sc;
        }

        schedtime_t get_sc() {
          return sumOfCommunications;
        }

        void set_start(schedtime_t s) {
          start = s;
        }

        schedtime_t get_start() {
          return start;
        }

        void set_finish(schedtime_t fin) {
          finish = fin;
        }

        schedtime_t get_finish() {
          return finish;
        }

        void set_f(schedtime_t new_f) {
          f = new_f;
        }

        schedtime_t get_f() {
          return f;
        }

        void set_cluster(int c) {
          cluster = c;
        }

        int get_cluster() {
          return cluster;
        }

        void set_M(unsigned int m) {
          M = m;
        }

        int get_M() {
          return M;
        }

        void set_height(unsigned int h) {
          height = h;
        }

        unsigned int get_height() {
          return height;
        }
      };
      static std::map<Schedulable*, nodeInfo_t> nodeInfo;

      struct clusterInfo_t {
        static int runningClusterNumber;
        int clusterNumber;

        //Keep track of nodes and execution orders
        std::vector<Schedulable*> orderedNodes; //Stored in reverse execution order

        /**
         * Constructor
         */
        clusterInfo_t() : clusterNumber(-1) {
          orderedNodes.reserve(graph->size());
        }

        void setClusterNumber(int n) {
          clusterNumber = n;
        }

        int getClusterNumber() {
          return clusterNumber;
        }

        void addNode(Schedulable* newNode) {
          orderedNodes.push_back(newNode);
        }

        void addNode(Schedulable* newNode, Schedulable* afterNode) {
          std::vector<Schedulable*>::iterator iter = orderedNodes.end();
          for(std::vector<Schedulable*>::iterator i = orderedNodes.begin(),
              ie = orderedNodes.end(); i != ie; ++i) {
            if(*i == afterNode) {
              iter = i;
              break;
            }
          }
          assert(iter != orderedNodes.end());
          orderedNodes.insert(iter, newNode);
        }

        Schedulable* getTopNode() {
          assert(orderedNodes.size() > 0);
          return orderedNodes.back();
        }

        unsigned int getNodePosition(Schedulable* node) {
          Schedulable** nodeLoc = NULL;
          for(std::vector<Schedulable*>::iterator i = orderedNodes.begin(),
              ie = orderedNodes.end(); i != ie; ++i) {
            if(*i == node) {
              nodeLoc = &*i;
              break;
            }
          }
          assert(nodeLoc != NULL);
          int pos = nodeLoc - &orderedNodes[0];
          assert(pos >= 0);
          assert(orderedNodes.size() > (unsigned)pos);
          return pos;
        }

        Schedulable* getNodeAt(unsigned int position) {
          assert(orderedNodes.size() > position);
          return orderedNodes[position];
        }

        int getSize() {
          return orderedNodes.size();
        }
      };
      std::map<int, clusterInfo_t> clusterInfo;

      struct priority_tuple {
        nodeInfo_t* u;
        nodeInfo_t* v;
        float r;
        schedtime_t l;

        bool operator<(const priority_tuple& other) const {
          return (r < other.r) ||
            (r == other.r && l < other.l) ||
            (r == other.r && l == other.l && u < other.u);
        }
      };
      std::priority_queue<priority_tuple> CL;

      //Private variables
      static SchedulableDAG* graph;
      FILE* logfile;
      unsigned int M;

    public:
      //Constructor
      CHASM();

      //Some accessor functions
      void setM(unsigned int M);

      //Algorithm steps
      void step1(SchedulableDAG* tg);
      void step2(SchedulableDAG* tg);
      void steps3and4(SchedulableDAG* tg);
      void step5(SchedulableDAG* tg);
      void reportBack(SchedulableDAG* tg);

      //Helper functions
      schedtime_t speculate_new_f(Schedulable* node, Schedulable* domSucc, clusterInfo_t* cluster) const;
      schedtime_t acceptableToCluster(Schedulable* node, Schedulable* domSucc, clusterInfo_t* cluster);
      bool acceptableToClusterSink(Schedulable* node, Schedulable* sink, clusterInfo_t* cluster);

      void consumeSink(Schedulable* nodeSched, Schedulable* sink, clusterInfo_t* cluster);
      void combineWithSucc(Schedulable* nodeSched, Schedulable* succ, clusterInfo_t* cluster, schedtime_t new_f);

      //Running CHASM
      void doCHASM();

      void run(SchedulableDAG& graph, FILE* logfile);
  };
}

#endif

