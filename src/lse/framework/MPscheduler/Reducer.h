/* 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
#ifndef _REDUCER_H
#define _REDUCER_H

#include <vector>
#include <iostream>

#include "SchedulableDAG.h"
#include "Policy.h"

namespace LSE_scheduler {
  class Reducer {
    public:
      /**
       * Constructor
       */
      Reducer() {}

      /**
       * Destructor
       */
      virtual ~Reducer() {}

      /**
       * Reduces the clusters formed by the clustering step
       */
      virtual void run(SchedulableDAG& graph, int numThreads, FILE* logfile) {
        std::cout << "Empty reduction" << std::endl;
      }
      void setPolicy(Policy * value) { policy = value; }
      Policy* getPolicy() { return policy; }

    protected:
      Policy* policy;
      FILE* logfile;
  };
}

#endif

