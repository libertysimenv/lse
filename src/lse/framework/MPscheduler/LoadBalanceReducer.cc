#include "LoadBalanceReducer.h"

#include <limits.h>

namespace LSE_scheduler {
  LoadBalanceReducer::LoadBalanceReducer() : Reducer() {}

  void LoadBalanceReducer::run(SchedulableDAG& graph, int numThreads, FILE* logfile) {
    this->graph = &graph;
    this->numThreads = numThreads;
    if(logfile) fprintf(logfile, "====Start LoadBalance Reducer====\n");
    this->logfile = logfile;
    doLoadBalancing();
    std::cout << "Finished reduction" << std::endl;
  }

  void LoadBalanceReducer::doLoadBalancing() {
    formClusters();
    combineClusters();
    reportThreads();
  }

  void LoadBalanceReducer::formClusters() {
    int numClusters = graph->getClusterCount();
    clusters.resize(numClusters);
    //Add tasks to their respective clusters
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      Schedulable* currTask = *i;
      int cluster = currTask->getCluster();
      assert(cluster > -1);
      cluster_t* currCluster = &clusters.at(cluster);
      currCluster->addTask(currTask, policy);

      //Check all adjacent clusters
      for(schedulableIterator j = currTask->pred_begin(),
          je = currTask->pred_end(); j != je; ++j) {
        Schedulable* predTask = *j;
        cluster_t* predCluster = &clusters.at(predTask->getCluster());
        currCluster->addAdjacentCluster(predCluster);
      }
      for(schedulableIterator j = currTask->succ_begin(),
          je = currTask->succ_end(); j != je; ++j) {
        Schedulable* succTask = *j;
        cluster_t* succCluster = &clusters.at(succTask->getCluster());
        currCluster->addAdjacentCluster(succCluster);
      }
    }
  }

  void LoadBalanceReducer::combineClusters() {
    std::set<cluster_t*, clusterPriority> clusterQueue;
    //Put all clusters in the queue
    for(std::vector<cluster_t>::iterator i = clusters.begin(),
        ie = clusters.end(); i != ie; ++i) {
      cluster_t* currCluster = &*i;
      assert(currCluster->valid);
      clusterQueue.insert(currCluster);
    }
    assert(clusterQueue.size() == clusters.size());

    //Keep merging until we only have numThreads clusters left
    int remainingClusters = clusters.size();
    while(remainingClusters > numThreads) {
      cluster_t* smallestCluster = *clusterQueue.begin();

      //Find the smallest of smallestCluster's neighbors
      schedtime_t minimumCost = std::numeric_limits<schedtime_t>::max();
      cluster_t* smallestNeighbor = NULL;
      for(std::set<cluster_t*>::iterator i = smallestCluster->adjacentClusters.begin(),
          ie = smallestCluster->adjacentClusters.end(); i != ie; ++i) {
        cluster_t* neighbor = *i;
        if(!neighbor->valid)
          continue;

        schedtime_t neighborCost = neighbor->execution;
        //Look for a smaller neighbor
        if(neighborCost < minimumCost) {
          minimumCost = neighborCost;
          smallestNeighbor = neighbor;
        }
      }

      //If smallestCluster has no neighbors, merge it with the next-smallest cluster
      if(smallestNeighbor == NULL) {
        std::set<cluster_t*>::iterator clustIter = clusterQueue.begin();
        clustIter++;
        smallestNeighbor = *clustIter;
        assert(smallestNeighbor->valid);
        assert(smallestNeighbor != smallestCluster);
      }

      assert(smallestNeighbor->execution >= smallestCluster->execution);

      //Remove both clusters from the queue
      clusterQueue.erase(smallestCluster);
      clusterQueue.erase(smallestNeighbor);

      //Merge smallestCluster with smallestNeighbor
      int totalNeighbors = smallestCluster->adjacentClusters.size() + smallestNeighbor->adjacentClusters.size();
      //smallestNeighbor->members.insert(smallestCluster->members.begin(), smallestCluster->members.end());
      smallestNeighbor->consumedClusters.insert(smallestCluster);
#ifdef NOMORE
      smallestNeighbor->consumedClusters.insert(smallestCluster->consumedClusters.begin(), smallestCluster->consumedClusters.end());
#endif
      smallestNeighbor->adjacentClusters.insert(smallestCluster->adjacentClusters.begin(), smallestCluster->adjacentClusters.end());
      smallestNeighbor->execution += smallestCluster->execution;
      smallestCluster->valid = false;

      //Remove the involved clusters from being neighbors of themselves
      smallestNeighbor->adjacentClusters.erase(smallestCluster);
      smallestNeighbor->adjacentClusters.erase(smallestNeighbor);
      remainingClusters--;

      //Put smallestNeighbor back in
      assert(smallestNeighbor->valid);
      clusterQueue.insert(smallestNeighbor);
    }
  }

  void LoadBalanceReducer::visitForThreads(cluster_t &C, int threadNumber) {
    for(std::set<Schedulable*>::iterator j = C.members.begin(),
          je = C.members.end(); j != je; ++j) {
      Schedulable* currNode = *j;
      currNode->setThread(threadNumber);
    }
    for(std::set<cluster_t*>::iterator j = C.consumedClusters.begin(),
	  je = C.consumedClusters.end() ; j != je ; ++j) 
      visitForThreads(**j, threadNumber);
  }

  void LoadBalanceReducer::reportThreads() {
    int threadNumber = 0;
    for(std::vector<cluster_t>::iterator i = clusters.begin(),
        ie = clusters.end(); i != ie; ++i) {
      cluster_t& currCluster = *i;
      if(!currCluster.valid) {
        continue;
      }
      visitForThreads(currCluster, threadNumber);

#ifdef NOMORE

      for(std::set<Schedulable*>::iterator j = currCluster.members.begin(),
          je = currCluster.members.end(); j != je; ++j) {
        Schedulable* currNode = *j;
        currNode->setThread(threadNumber);
      }

      //Also get any consumed clusters
      for(std::set<cluster_t*>::iterator j = currCluster.consumedClusters.begin(),
          je = currCluster.consumedClusters.end(); j != je; ++j) {
        cluster_t& consumedCluster = **j;
        for(std::set<Schedulable*>::iterator k = consumedCluster.members.begin(),
            ke = consumedCluster.members.end(); k != ke; ++k) {
          Schedulable* currNode = *k;
          currNode->setThread(threadNumber);
        }
      }
#endif
      threadNumber++;
    }

    printf("threads: %d\n", threadNumber);
  }
}

