#include "RACH.h"

#include <queue>
#include <pthread.h>
#include <limits>
#include <stack>
#include <algorithm>
#include <time.h>
#include <stdexcept>

namespace {
  template <class K,class V> V& doat(std::map<K, V> &M, K &key) {
    if (!M.count(key)) throw std::out_of_range("map::at");
    else return M[key];
  }
}

namespace LSE_scheduler {
  /**
   * nodeInfo_t sub-class methods
   */
  RACH::nodeInfo_t::nodeInfo_t() : start(-1), finish(-1) {}

  void RACH::nodeInfo_t::setTLevel(schedtime_t tlevel) {
    t_level = tlevel;
  }

  schedtime_t RACH::nodeInfo_t::getTLevel() {
    return t_level;
  }



  /**
   * cluster_t sub-class methods
   */

  RACH::cluster_t::cluster_t() : execution(0), communication(0), thread(-1) {}

  /**
   * Add a task
   */
  //O(log(n))
  bool RACH::cluster_t::addTask(Schedulable* task, std::map<Schedulable*, cluster_t*>& task2cluster) {
    assert(task2cluster.find(task) == task2cluster.end());
    bool success = members.insert(task).second; //O(log(n))
    if(success) {
      task2cluster[task] = this; //O(log(n))
      order.push_back(task); //O(1)
    }
    return success;
  }

  /**
   * Delete a task
   */
  bool RACH::cluster_t::removeTask(Schedulable* task, std::map<Schedulable*, cluster_t*>& task2cluster) {
    int erased = members.erase(task);
    if(erased) {
      task2cluster.erase(task);
    }
    return erased;
  }

  /**
   * Sums and categorizes the total communication costs.
   * Should only be run as the final step in creating taskGroups,
   * i.e. there should be no adding or removing tasks in ANY cluster_t
   * after analyzeCommunications has been run in even a single cluster_t.
   */
  //m is the number of members; p is the number of edges from those members
  //O(m+s*log(s))
  void RACH::cluster_t::analyzeCommunications(Policy* policy, std::vector<cluster_t*>& clustersVector, std::map<Schedulable*, cluster_t*>& task2cluster) {
    execution = 0;
    //Go over each member of this cluster
    for(std::set<Schedulable*>::iterator i = begin(),
        ie = end(); i != ie; ++i) {
      Schedulable* currTask = *i;
      execution += policy->nodeCost(currTask);
      //Go over each of the predecessors of currTask
      for(schedulableIterator j = currTask->pred_begin(),
          je = currTask->pred_end(); j != je; ++j) {
        Schedulable* predTask = *j;
        schedtime_t commCost = policy->naiveCommCost(predTask, currTask);
        assert(task2cluster.find(predTask) != task2cluster.end()); //Delete
        cluster_t* predCluster = task2cluster[predTask]; //O(log(n))
        assert(std::find(clustersVector.begin(), clustersVector.end(), predCluster) != clustersVector.end());
        assert(predCluster != NULL); //Delete
        //If the currTask and predTask are in different clusters,
        //update the communication costs for both clusters
        if(predCluster != this) {
          if(predComms.find(predCluster) == predComms.end()) //O(log(s))
            predComms[predCluster] = 0;
          predComms[predCluster] = predComms[predCluster] + commCost; //O(log(s))
          //Also track total communication cost
          communication += commCost;
          if(predCluster->succComms.find(this) == predCluster->succComms.end())
            predCluster->succComms[this] = 0;
          predCluster->succComms[this] = predCluster->succComms[this] + commCost;
        }
      }
    }
  }

  void RACH::cluster_t::addPredecessor(cluster_t* predCluster) {
    predecessorClusters.insert(predCluster);
    predecessorClusters.insert(predCluster->predecessorClusters.begin(),
        predCluster->predecessorClusters.end());

    //Update predCluster's successors
    predCluster->successorClusters.insert(this);
  }

  void RACH::cluster_t::addPredecessor(std::set<cluster_t*>* toCluster, cluster_t* predCluster) {
    toCluster->insert(predCluster);
    toCluster->insert(predCluster->predecessorClusters.begin(),
        predCluster->predecessorClusters.end());
  }

  bool RACH::cluster_t::removePredecessor(cluster_t* predCluster) {
    return predecessorClusters.erase(predCluster);
  }

  bool RACH::cluster_t::hasAsPredecessor(cluster_t* predCluster) {
    return predecessorClusters.count(predCluster);
  }

  /**
   * Splits the cluster at currNode.  currNode will be
   * a part of the new cluster.
   * Returns a pointer to the new cluster_t.
   */
  RACH::cluster_t* RACH::cluster_t::splitCluster(Schedulable* currNode, std::map<Schedulable*, nodeInfo_t>& nodeInfo, std::list<cluster_t>& clusters, std::vector<cluster_t*>& clustersVector, std::map<Schedulable*, cluster_t*>& task2cluster, SchedulableDAG* graph) {
    //Create the new cluster
    clusters.push_back(cluster_t());
    cluster_t* newCluster = &clusters.back();
    clustersVector.push_back(newCluster);
    assert(newCluster == clustersVector.back());

    //Put in all tasks including and after currNode
    std::vector<Schedulable*>::iterator start = std::find(order.begin(), order.end(), currNode);
    for(std::vector<Schedulable*>::iterator i = start,
        ie = order.end(); i != ie; ++i) {
      Schedulable* currTask = *i;
      assert(task2cluster.find(currTask) != task2cluster.end());
      assert(nodeInfo.find(currTask) != nodeInfo.end());

      //Remove the task from this cluster
      removeTask(currTask, task2cluster);
      assert(task2cluster.find(currTask) == task2cluster.end());

      //Add the task to the new cluster
      bool successfullyAdded = newCluster->addTask(currTask, task2cluster);
      assert(successfullyAdded);
    }
    //Let SchedulableDAG know about the changes
    assert(graph->getClusterCount()+1 == clusters.size());
    int clusterNumber = graph->getClusterCount();
    graph->setClusterCount(graph->getClusterCount()+1);
    for(std::set<Schedulable*>::iterator i = newCluster->begin(),
        ie = newCluster->end(); i != ie; ++i) {
      Schedulable* currTask = *i;
      currTask->setCluster(clusterNumber);
      assert(task2cluster.find(currTask) != task2cluster.end());
      assert(task2cluster[currTask] == newCluster);
    }

    //Erase the invalid part of order
    order.erase(start, order.end());

    return newCluster;
  }

  /**
   * Report communications with predecessor clusters
   * Complexity: O(log(e)) ; reducible to O(1)
   */
  schedtime_t RACH::cluster_t::predComm(cluster_t* predGroup) {
    std::map<cluster_t*, schedtime_t>::iterator comm = predComms.find(predGroup);
    if(comm == predComms.end()) {
      return 0;
    }
    return comm->second;
  }

  /**
   * Report communications with successor clusters
   */
  schedtime_t RACH::cluster_t::succComm(cluster_t* succGroup) {
    std::map<cluster_t*, schedtime_t>::iterator comm = succComms.find(succGroup);
    if(comm == succComms.end()) {
      return 0;
    }
    return comm->second;
  }

  schedtime_t RACH::cluster_t::getExecution() {
    return execution;
  }

  /**
   * Returns the total naive communication cost
   */
  schedtime_t RACH::cluster_t::getCommunication() {
    return communication;
  }

  /**
   * Iterator over the members
   */
  std::set<Schedulable*>::iterator RACH::cluster_t::begin() {
    return members.begin();
  }

  /**
   * End iterator
   */
  std::set<Schedulable*>::iterator RACH::cluster_t::end() {
    return members.end();
  }

  /**
   * Iterator over predecessors
   */
  std::map<RACH::cluster_t*, schedtime_t>::iterator RACH::cluster_t::pred_begin() {
    return predComms.begin();
  }

  std::map<RACH::cluster_t*, schedtime_t>::iterator RACH::cluster_t::pred_end() {
    return predComms.end();
  }

  /**
   * Iterator over successors
   */
  std::map<RACH::cluster_t*, schedtime_t>::iterator RACH::cluster_t::succ_begin() {
    return succComms.begin();
  }

  /**
   * End iterator over successors
   */
  std::map<RACH::cluster_t*, schedtime_t>::iterator RACH::cluster_t::succ_end() {
    return succComms.end();
  }


  /**
   * thread_t members
   */
  RACH::thread_t::thread_t() : communication(0), execution(0), start(0), finish(0) {}

  RACH::length_t RACH::thread_t::getCosts() {
    length_t costs = { execution, communication };
    return costs;
  }

  schedtime_t RACH::thread_t::getExecution() {
    return execution;
  }

  schedtime_t RACH::thread_t::getCommunication() {
    return communication;
  }

  /**
   * Adds a cluster_t to this thread_t.
   * Complexity: O(e*(log(e) + log(n))) ; reducible to O(e)
   */
  bool RACH::thread_t::addCluster(cluster_t* cluster, std::map<cluster_t*, thread_t*>& cluster2thread) {
    if(members.find(cluster) != members.end())
      return false;

    //See what would happen by adding the cluster
    length_t result = speculateAddCluster(cluster, cluster2thread);
    execution = result.execution;
    communication = result.communication;

    cluster2thread[cluster] = this;
    members.insert(cluster).second;

    return true;
  }

  /**
   * Complexity: O(e*(log(e) + log(n))) ; reducible to O(e)
   */
  bool RACH::thread_t::removeCluster(cluster_t* cluster, std::map<cluster_t*, thread_t*>& cluster2thread) {
    if(members.find(cluster) == members.end())
      return false;

    length_t result = speculateRemoveCluster(cluster, cluster2thread);
    execution = result.execution;
    communication = result.communication;

    cluster2thread.erase(cluster);
    members.erase(cluster);

    return true;
  }

  /**
   * Calculates what would happen to communication and execution
   * if cluster were added to this thread_t.
   * Complexity: O(e*(log(e) + log(n))) ; reducible to O(e)
   */
  RACH::length_t RACH::thread_t::speculateAddCluster(cluster_t* cluster, std::map<cluster_t*, thread_t*>& cluster2thread) {
    length_t result = { execution, communication };
    //Complexity: O(log(n)) ; reducible to O(1)
    if(members.find(cluster) != members.end()) {
      return result;
    }

    //Local copies of parameters
    schedtime_t specExec = execution;
    schedtime_t specComm = communication;

    //Check communications with all other clusters in the thread_t
    //Complexity: O(e*(log(e)+log(n))) ; reducible to O(e)
    schedtime_t newComms = cluster->getCommunication();
    for(std::map<cluster_t*, schedtime_t>::iterator i = cluster->pred_begin(),
        ie = cluster->pred_end(); i != ie; ++i) {
      cluster_t* pred = i->first;
      if(members.find(pred) != members.end()) //Complexity: O(log(n)) ; reducible to O(1)
        newComms -= cluster->predComm(pred); //Complexity: O(log(e)) ; reducible to O(1)
    }
    specComm += newComms;
    specExec += cluster->getExecution();

    //Any successor clusters also need to check on their communications
    //Complexity: O(e*(log(e)+log(n))) ; reducible to O(e)
    for(std::map<cluster_t*, schedtime_t>::iterator i = cluster->succ_begin(),
        ie = cluster->succ_end(); i != ie; ++i) {
      cluster_t* succ = i->first;
      thread_t* succThread = cluster2thread[succ]; //Complexity: O(log(n)) ; reducible to O(1)
      //If the successor is in this thread_t, communications are zeroed.
      //Otherwise, the successor is in another thread_t, wherein communications
      //are already accounted for.
      if(succThread == this) {
        specComm -= cluster->succComm(succ); //Complexity: O(log(e)) ; reducible to O(1)
      }
    }

    result.execution = specExec;
    result.communication = specComm;
    return result;
  }

  /**
   * Calculates what would happen to communication and execution
   * if cluster were removed from this thread_t.
   * Complexity: O(e*(log(e) + log(n))) ; reducible to O(e)
   */
  RACH::length_t RACH::thread_t::speculateRemoveCluster(cluster_t* cluster, std::map<cluster_t*, thread_t*>& cluster2thread) {
    length_t result = { execution, communication };
    //Complexity: O(log(n)) ; reducible to O(1)
    if(members.find(cluster) == members.end())
      return result;

    //Local copies of parameters
    schedtime_t specExec = execution;
    schedtime_t specComm = communication;

    //Restore communications with all other clusters in the thread_t
    //Complexity: O(e*log(e)*log(n)) ; reducible to O(e)
    schedtime_t newComms = cluster->getCommunication();
    for(std::map<cluster_t*, schedtime_t>::iterator i = cluster->pred_begin(),
        ie = cluster->pred_end(); i != ie; ++i) {
      cluster_t* pred = i->first;
      if(members.find(pred) != members.end()) //Complexity: O(log(n)) ; reducible to O(1)
        newComms -= cluster->predComm(pred); //Complexity: O(log(e)) ; reducible to O(1)
    }
    specComm -= newComms;
    specExec -= cluster->getExecution();

    //Any successor clusters also need to check on their communications
    //Complexity: O(e*(log(e)+log(n))) ; reducible to O(e)
    for(std::map<cluster_t*, schedtime_t>::iterator i = cluster->succ_begin(),
        ie = cluster->succ_end(); i != ie; ++i) {
      cluster_t* succ = i->first;
      thread_t* succThread = cluster2thread[succ]; //Complexity: O(log(n)) ; reducible to O(1)
      //If the successor is in this thread_t, the total communications
      //for the thread_t need to reflect this (adding it on).
      if(succThread == this) {
        specComm += cluster->succComm(succ); //Complexity: O(log(e)) ; reducible to O(1)
      }
    }

    result.execution = specExec;
    result.communication = specComm;

    return result;
  }

  std::set<RACH::cluster_t*>::iterator RACH::thread_t::begin() {
    return members.begin();
  }

  std::set<RACH::cluster_t*>::iterator RACH::thread_t::end() {
    return members.end();
  }

  std::set<RACH::cluster_t*>::reverse_iterator RACH::thread_t::rbegin() {
    return members.rbegin();
  }

  std::set<RACH::cluster_t*>::reverse_iterator RACH::thread_t::rend() {
    return members.rend();
  }


  /**
   * RACH members
   */
  //std::map<Schedulable*, RACH::nodeInfo_t> RACH::nodeInfo;
  //std::list<RACH::cluster_t> RACH::clusters;
  //std::vector<RACH::cluster_t*> RACH::clustersVector;
  //std::map<Schedulable*, RACH::cluster_t*> RACH::task2cluster;
  //std::map<RACH::cluster_t*, RACH::thread_t*> RACH::cluster2thread;

  /**
   * RACH primary class methods
   */
  RACH::RACH() : Reducer(), singleThreadMakespan(0), multiThreadMakespan(0) {}

  void RACH::run(SchedulableDAG& graph, int numThreads, FILE* logfile) {
    this->graph = &graph;
    this->numThreads = numThreads;
    if(logfile) fprintf(logfile, "====Start RACH Clusterer====\n");
    this->logfile = logfile;
    doRACH();
    std::cout << "Finished reduction" << std::endl;
  }

  void RACH::doRACH() {
    prepareGraphStructures(); //O(n*log(n))
    formClusters();           //O((n+e)*log(n))
    //breakClusters();
    while(splitClusters());   //O()
    finalizeClusters();       //O(n+e*log(e))

    //Count the number of runs we will perform
    int runCount = 0;
    for(int i = numThreads; i > 1; i /= 2) {
      runCount++;
    }

    //Prepare the various versions of data structures needed for the different runs
    std::vector< std::vector<thread_t> > threadsVersions(runCount);
    std::vector< int > makespanVersions(runCount);

    int bestRun = -1;
    int bestMakespan = std::numeric_limits<schedtime_t>::max();
    int numThreadsThisRun = numThreads;
    for(int i = 0; i < 1; ++i) {
      printf("Running pass %d\n", i+1);
      makespanVersions[i] = runPass2(numThreadsThisRun);

      //Keep track of best run so far
      if(makespanVersions[i] < bestMakespan) {
        bestRun = i;
        bestMakespan = makespanVersions[i];
        winningThreadCount = numThreadsThisRun;
      }

      //Update the number of threads for the next run
      numThreadsThisRun /= 2;

      //Keep track of thread assignments
      threadsVersions[i] = threads;

      //Reset data structures
      //nodeInfo
      for(std::map<Schedulable*, nodeInfo_t>::iterator j = nodeInfo.begin(),
          je = nodeInfo.end(); j != je; ++j) {
        j->second.start = -1;
        j->second.finish = -1;
      }
      //threads
      for(std::vector<thread_t>::iterator j = threads.begin(),
          je = threads.end(); j != je; ++j) {
        j->members.clear();
        j->communication = 0;
        j->execution = 0;
        j->start = 0;
        j->finish = 0;
      }
      //clusters
      for(std::list<cluster_t>::iterator j = clusters.begin(),
          je = clusters.end(); j != je; ++j) {
        j->thread = -1;
      }
      //cluster2thread
      cluster2thread.clear();
    }

    //Choose the best run
    threads = threadsVersions[bestRun];
    multiThreadMakespan = bestMakespan;

    reportThreads();          //O(n)
  }

  /**
   * Readies the nodeInfo structure
   * O(n*log(n))
   */
  void RACH::prepareGraphStructures() {
    //std::queue<Schedulable*> readyList;
    //std::map<Schedulable*, int> unusedPreds;

    //Populate the readyList with source nodes
    //O(n*log(n))
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      Schedulable* nodeSched = *i;
      //int predCount = nodeSched->getPredecessorCount();
      //unusedPreds[nodeSched] = predCount;
      //if(!predCount)
        //readyList.push(nodeSched);

      //Add a nodeInfo_t entry for nodeSched
      nodeInfo.insert(std::pair<Schedulable*, nodeInfo_t>(nodeSched, nodeInfo_t()));
    }

    //Operate on and re-populate the readyList
    /*while(!readyList.empty()) {
      //Pop the first item from the front of the queue
      Schedulable* nodeSched = readyList.front();
      readyList.pop();

      //Operate on nodeSched


      //Re-populate readyList
      for(schedulableIterator i = nodeSched->succ_begin(),
          ie = nodeSched->succ_end(); i != ie; ++i) {
        Schedulable* succ = *i;
        int predCount = unusedPreds[succ];
        predCount--;
        if(!predCount)
          readyList.push(succ);
      }
    }*/
  }

  /**
   * Creates clusters
   * O((n+e)*log(n))
   */
  void RACH::formClusters() {
    clusters.resize(graph->getClusterCount()); //O(n)
    clustersVector.resize(graph->getClusterCount()); //O(n)
    int idx = 0;
    //Set up clustersVector to point at the right things
    //O(n)
    for(std::list<cluster_t>::iterator i = clusters.begin(),
        ie = clusters.end(); i != ie; ++i) {
      cluster_t* currCluster = &*i;
      clustersVector[idx++] = currCluster;
    }

    //Form internally represented clusters
    std::queue<Schedulable*> readyList;
    std::map<Schedulable*, int> unvisitedPreds;

    //Initialize unvisitedPreds and readyList
    //O(n*log(n))
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      //Record the number of predecessors of each node
      Schedulable* currNode = *i;
      int preds = currNode->getPredecessorCount();
      if(!preds)
        readyList.push(currNode);
      unvisitedPreds[currNode] = preds; //O(log(n))
    }

    //Operate on each Schedulable* in order
    //O((n+e)*log(n))
    while(!readyList.empty()) {
      //Get the first item
      Schedulable* currNode = readyList.front();
      readyList.pop();

      //Work on currNode
      int clust = currNode->getCluster(); //Delete
      assert(clust < clustersVector.size()); //Delete
      clustersVector[clust]->addTask(currNode, task2cluster); //O(log(n))

      //Put new ready nodes on the ready list
      for(schedulableIterator i = currNode->succ_begin(),
          ie = currNode->succ_end(); i != ie; ++i) {
        Schedulable* succNode = *i;
        int preds = unvisitedPreds[succNode];
        unvisitedPreds[succNode] = --preds;
        if(!preds)
          readyList.push(succNode);
      }
    }

    //TODO: Delete this for-loop
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      Schedulable* task = *i;
      assert(task2cluster.find(task) != task2cluster.end());
    }
  }

  /**
   * Discovers clusters for splitting
   */
  std::set< std::vector<RACH::cluster_t*> > RACH::discoverCycles() {
    //Some bookkeeping
    int nodesInCycles = 0;

    //Reset data structures
    for(std::list<cluster_t>::iterator i = clusters.begin(),
        ie = clusters.end(); i != ie; ++i) {
      i->predecessorClusters.clear();
      i->successorClusters.clear();
    }

    //Map from cluster_t to DFS_data
    std::map<cluster_t*, DFS_data> DFSMap;

    //Construct a graph of the clusters
    //O(n*log(n)+e)
    std::set<cluster_t*> remainingClusters;
    Schedulable* rootTask = NULL;
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      //Get the cluster associated with each node
      Schedulable* currNode = *i;
      //assert(task2cluster.find(currNode) != task2cluster.end());
      cluster_t* currCluster = doat(task2cluster,currNode);
      //If this cluster hasn't been inserted into DFSMap yet
      //O(log(n))
      if(DFSMap.find(currCluster) == DFSMap.end())
        DFSMap.insert(std::make_pair<cluster_t*, DFS_data>(currCluster, DFS_data()));

      //Track the current cluster
      //O(log(n))
      if(remainingClusters.find(currCluster) == remainingClusters.end())
        remainingClusters.insert(currCluster);

      //Choose a root task
      if(rootTask == NULL && currNode->getPredecessorCount() == 0)
        rootTask = currNode;

      //Add predecessors
      //O(e)
      for(schedulableIterator j = currNode->pred_begin(),
          je = currNode->pred_end(); j != je; ++j) {
        Schedulable* predNode = *j;
        //assert(task2cluster.find(predNode) != task2cluster.end());
        cluster_t* predCluster = doat(task2cluster,predNode);
        if(predCluster == currCluster)
          continue;

        currCluster->predecessorClusters.insert(predCluster);
        predCluster->successorClusters.insert(currCluster);
      }
    }

    //Conduct a DFS, looking for cycles
    std::set< std::vector<cluster_t*> > cycles;
    std::stack<cluster_t*> DFSStack;
    cluster_t* startingCluster = task2cluster[rootTask];
    DFSStack.push(startingCluster);
    //unsigned int time = 0;
beginDFS:
    while(!DFSStack.empty()) {
      //O(1)
      cluster_t* currCluster = DFSStack.top();
      //O(1)
      DFSStack.pop();
      //O(log(n))
      remainingClusters.erase(currCluster);

      //If this is the first time seeing the cluster
      if(DFSMap[currCluster].visited == 0) { //O(log(n))
        //Mark it as visited
        DFSMap[currCluster].visited = 1; //Mark as having been visited, but not completed
        //DFSMap[currCluster].entryTime = ++time;
        //Put it back on the stack
        DFSStack.push(currCluster); //O(1)

        //Put all descendents on the stack
        //O(n^2)
        for(std::set<cluster_t*>::iterator i = currCluster->successorClusters.begin(),
            ie = currCluster->successorClusters.end(); i != ie; ++i) {
          cluster_t* succCluster = *i;
          //If the descendent hasn't been visited
          if(DFSMap[succCluster].visited == 0) { //O(log(n))
            DFSStack.push(succCluster); //O(1)
            DFSMap[succCluster].parent = currCluster; //O(1)
          }
          //If it has been visited, but not completed, we have a cycle
          else if(DFSMap[succCluster].visited == 1) { //O(log(n))
            //Isolate the cycle
            std::vector<cluster_t*> cycle;
            cycle.push_back(succCluster);
            cluster_t* parent = currCluster;
            while(parent != succCluster) { //O(n*log(n))
              nodesInCycles++;
              cycle.push_back(parent);
              parent = DFSMap[parent].parent; //O(log(n))
            }

            //Store the cycle
            cycles.insert(cycle);
          }
        }
      }
      //Visited, but not completed
      else if(DFSMap[currCluster].visited == 1) {
        //DFSMap[currCluster].exitTime = ++time;
        DFSMap[currCluster].visited = 2; //Mark as completed
      }
    }
    //O(1)
    if(!remainingClusters.empty()) {
      DFSStack.push(*remainingClusters.begin());
      goto beginDFS;
    }

    printf("Nodes in cycles: %d\n", nodesInCycles);

    return cycles;
  }

  /**
   * Breaks cycles
   */
  void RACH::breakCycles(std::set< std::vector<cluster_t*> >& cycles) {
    //Keep track of some figures
    int innerCalls = 0;
    int nodesInCycles = 0;

    //Go over the collected cycles
    for(std::set< std::vector<cluster_t*> >::iterator i = cycles.begin(),
        ie = cycles.end(); i != ie; ++i) {
      const std::vector<cluster_t*>& cycle = *i;
      //Go over the cycle in order
      cluster_t* currCluster = cycle[cycle.size()-1];
      cluster_t* prevCluster = cycle[0];
      cluster_t* nextCluster = NULL;
      for(int j = cycle.size()-1; j >= 0; --j) {
        int nextIndex = j-1;
        if(nextIndex < 0) nextIndex = cycle.size()-1;
        nextCluster = cycle[nextIndex];
        //We are looking for any cluster_t in which the exit edge comes before the entry edge
        int entryIdx = -1;
        int exitIdx = -1;
        //Go over the nodes in this cluster, in order
        for(std::vector<Schedulable*>::iterator k = currCluster->order.begin(),
            ke = currCluster->order.end(); k != ke; ++k) {
          Schedulable* currNode = *k;
          nodesInCycles++;

          //Look at currNode's successors to see if this is the node with the offending edge
          for(schedulableIterator h = currNode->succ_begin(),
              he = currNode->succ_end(); h != he; ++h) {
            innerCalls++;
            Schedulable* succNode = *h;
            cluster_t* succCluster = task2cluster[succNode];
            if(succCluster == nextCluster) {
              exitIdx = &*k - &currCluster->order[0];
            }
          }

          //Look at currNode's predecessors to see if this is the node with the offending edge
          for(schedulableIterator h = currNode->pred_begin(),
              he = currNode->pred_end(); h != he; ++h) {
            Schedulable* predNode = *h;
            cluster_t* predCluster = task2cluster[predNode];
            if(predCluster == prevCluster) {
              entryIdx = &*k - &currCluster->order[0];
            }
          }
        }

        //If the exit happens before the entry
        if(exitIdx > -1 && entryIdx > -1 && exitIdx < entryIdx) {
          //Break the cluster
          cluster_t* newCluster = currCluster->splitCluster(currCluster->order[entryIdx], nodeInfo, clusters, clustersVector, task2cluster, graph);

          goto cycleBroken;
        }

        //Update the cluster pointers
        prevCluster = currCluster;
        currCluster = nextCluster;
      }

cycleBroken:
      ;
    }

    printf("Nodes in cycles: %d\n", nodesInCycles);
    printf("Cycles: %d\n", cycles.size());
    printf("Inner calls: %d\n", innerCalls);
  }

  /**
   *
   */
  void RACH::breakClusters() {

  }

  /**
   * Splits clusters to ensure that there are no two-way
   * communications between any two clusters.
   */
  bool RACH::splitClusters() {
    /*//Reset data structures
    for(std::list<cluster_t>::iterator i = clusters.begin(),
        ie = clusters.end(); i != ie; ++i) {
      i->predecessorClusters.clear();
      i->successorClusters.clear();
    }

    //Track whether any clusters were split
    bool graphChanged = false;

    //Map from cluster_t to DFS_data
    std::map<cluster_t*, DFS_data> DFSMap;

    //Construct a graph of the clusters
    //O(n*log(n)+e)
    std::set<cluster_t*> remainingClusters;
    Schedulable* rootTask = NULL;
    for(schedulableIterator i = graph->begin(),
        ie = graph->end(); i != ie; ++i) {
      //Get the cluster associated with each node
      Schedulable* currNode = *i;
      //assert(task2cluster.find(currNode) != task2cluster.end());
      cluster_t* currCluster = doat(task2cluster,currNode);
      //If this cluster hasn't been inserted into DFSMap yet
      //O(log(n))
      if(DFSMap.find(currCluster) == DFSMap.end())
        DFSMap.insert(std::make_pair<cluster_t*, DFS_data>(currCluster, DFS_data()));

      //Track the current cluster
      //O(log(n))
      if(remainingClusters.find(currCluster) == remainingClusters.end())
        remainingClusters.insert(currCluster);

      //Choose a root task
      if(rootTask == NULL && currNode->getPredecessorCount() == 0)
        rootTask = currNode;

      //Add predecessors
      //O(e)
      for(schedulableIterator j = currNode->pred_begin(),
          je = currNode->pred_end(); j != je; ++j) {
        Schedulable* predNode = *j;
        //assert(task2cluster.find(predNode) != task2cluster.end());
        cluster_t* predCluster = task2cluster[predNode];
        if(predCluster == currCluster)
          continue;

        currCluster->predecessorClusters.insert(predCluster);
        predCluster->successorClusters.insert(currCluster);
      }
    }

    //Conduct a DFS, looking for cycles
    std::set< std::vector<cluster_t*> > cycles;
    std::stack<cluster_t*> DFSStack;
    cluster_t* startingCluster = task2cluster[rootTask];
    DFSStack.push(startingCluster);
    //unsigned int time = 0;
beginDFS:
    while(!DFSStack.empty()) {
      //O(1)
      cluster_t* currCluster = DFSStack.top();
      //O(1)
      DFSStack.pop();
      //O(log(n))
      remainingClusters.erase(currCluster);

      //If this is the first time seeing the cluster
      if(DFSMap[currCluster].visited == 0) { //O(log(n))
        //Mark it as visited
        DFSMap[currCluster].visited = 1; //Mark as having been visited, but not completed
        //DFSMap[currCluster].entryTime = ++time;
        //Put it back on the stack
        DFSStack.push(currCluster); //O(1)

        //Put all descendents on the stack
        //O(n^2)
        for(std::set<cluster_t*>::iterator i = currCluster->successorClusters.begin(),
            ie = currCluster->successorClusters.end(); i != ie; ++i) {
          cluster_t* succCluster = *i;
          //If the descendent hasn't been visited
          if(DFSMap[succCluster].visited == 0) { //O(log(n))
            DFSStack.push(succCluster); //O(1)
            DFSMap[succCluster].parent = currCluster; //O(1)
          }
          //If it has been visited, but not completed, we have a cycle
          else if(DFSMap[succCluster].visited == 1) { //O(log(n))
            //Isolate the cycle
            std::vector<cluster_t*> cycle;
            cycle.push_back(succCluster);
            cluster_t* parent = currCluster;
            while(parent != succCluster) { //O(n*log(n))
              cycle.push_back(parent);
              parent = DFSMap[parent].parent; //O(log(n))
            }

            //Store the cycle
            cycles.insert(cycle);
            graphChanged = true;
          }
        }
      }
      //Visited, but not completed
      else if(DFSMap[currCluster].visited == 1) {
        //DFSMap[currCluster].exitTime = ++time;
        DFSMap[currCluster].visited = 2; //Mark as completed
      }
    }
    //O(1)
    if(!remainingClusters.empty()) {
      DFSStack.push(*remainingClusters.begin());
      goto beginDFS;
    }*/

    //Go over the collected cycles
    /*for(std::set< std::vector<cluster_t*> >::iterator i = cycles.begin(),
        ie = cycles.end(); i != ie; ++i) {
      const std::vector<cluster_t*>& cycle = *i;
      //Go over the cycle in order
      cluster_t* currCluster = cycle[cycle.size()-1];
      cluster_t* prevCluster = cycle[0];
      cluster_t* nextCluster = NULL;
      for(int j = cycle.size()-1; j >= 0; --j) {
        int nextIndex = j-1;
        if(nextIndex < 0) nextIndex = cycle.size()-1;
        nextCluster = cycle[nextIndex];
        //We are looking for any cluster_t in which the exit edge comes before the entry edge
        int entryIdx = -1;
        int exitIdx = -1;
        //Go over the nodes in this cluster, in order
        for(std::vector<Schedulable*>::iterator k = currCluster->order.begin(),
            ke = currCluster->order.end(); k != ke; ++k) {
          Schedulable* currNode = *k;

          //Look at currNode's successors to see if this is the node with the offending edge
          for(schedulableIterator h = currNode->succ_begin(),
              he = currNode->succ_end(); h != he; ++h) {
            Schedulable* succNode = *h;
            cluster_t* succCluster = task2cluster[succNode];
            if(succCluster == nextCluster) {
              exitIdx = &*k - &currCluster->order[0];
            }
          }

          //Look at currNode's predecessors to see if this is the node with the offending edge
          for(schedulableIterator h = currNode->pred_begin(),
              he = currNode->pred_end(); h != he; ++h) {
            Schedulable* predNode = *h;
            cluster_t* predCluster = task2cluster[predNode];
            if(predCluster == prevCluster) {
              entryIdx = &*k - &currCluster->order[0];
            }
          }
        }

        //If the exit happens before the entry
        if(exitIdx > -1 && entryIdx > -1 && exitIdx < entryIdx) {
          //Break the cluster
          cluster_t* newCluster = currCluster->splitCluster(currCluster->order[entryIdx], nodeInfo, clusters, clustersVector, task2cluster, graph);

          goto cycleBroken;
        }

        //Update the cluster pointers
        prevCluster = currCluster;
        currCluster = nextCluster;
      }

cycleBroken:
      ;
    }

    return graphChanged;*/

    std::set< std::vector<cluster_t*> > cycles = discoverCycles();
    bool graphChanged = (cycles.size() == 0) ? false : true;
    breakCycles(cycles);
    return graphChanged;
  }

  //O(n+e*log(e)) ; using a hash map this could be reduced to O(n+e)
  void RACH::finalizeClusters() {
    //We go over c clusters, m nodes per cluster, m*c=n
    for(std::list<cluster_t>::iterator i = clusters.begin(),
        ie = clusters.end(); i != ie; ++i) {
      i->analyzeCommunications(policy, clustersVector, task2cluster); //O(m+s*log(s))
    }
  }

  /**
   * Indicates whether the graph is acyclic
   */
  /**
   * Generally available function to compute how a
   * thread subsuming the clusterOfInterest would
   * impact its running time.
   */
  void RACH::computeThreadImpact(cluster_t* clusterOfInterest, thread_t* myThread, int threadNum, const std::map<Schedulable*, nodeInfo_t>& nodeInfo, const std::map<Schedulable*, cluster_t*>& task2cluster, const std::map<cluster_t*, thread_t*>& cluster2thread, Policy* policy, cluster_position* result, std::map<Schedulable*, nodeInfo_t>& localNodeInfo) {
    //Calculate the impact of subsuming this cluster
    //Look at when each node within the cluster can execute
    int nodeIdx = 0;
    for(std::vector<Schedulable*>::iterator i = clusterOfInterest->order.begin(),
        ie = clusterOfInterest->order.end(); i != ie; ++i, ++nodeIdx) {
      Schedulable* currNode = *i;
      Schedulable* dominantPred = NULL;
      schedtime_t dominantFinish = 0;

      //Find the latest-finishing predecessor
      schedtime_t totalPredComm = 0;
      for(schedulableIterator j = currNode->pred_begin(),
          je = currNode->pred_end(); j != je; ++j) {
        Schedulable* pred = *j;
        //Find the proper version of pred's nodeInfo
        schedtime_t predFinish;
        if(localNodeInfo.find(pred) != localNodeInfo.end()) {
          predFinish = localNodeInfo[pred].finish;
        }
        else {
          assert(nodeInfo.find(pred) != nodeInfo.end());
          predFinish = nodeInfo.find(pred)->second.finish;
        }
        schedtime_t predComm = policy->naiveCommCost(pred, currNode);

        //Figure out the actual communication cost
        assert(nodeInfo.find(pred) != nodeInfo.end());
        assert(task2cluster.find(pred) != task2cluster.end());
        cluster_t* predCluster = task2cluster.find(pred)->second;

        assert(task2cluster.find(currNode)->second == clusterOfInterest);
        if(predCluster == clusterOfInterest || cluster2thread.find(predCluster)->second == myThread)
          predComm = 0;
        totalPredComm += predComm;
        if(predFinish > dominantFinish) {
          dominantFinish = predFinish;
          dominantPred = pred;
        }
      }

      //Figure out when currNode can start, based on finish time of
      //its predecessors and occupancy of the thread.
      schedtime_t nodeStart = -1;
      if(nodeIdx > 0) { //If this is not the first task in the cluster
        //Then this task cannot start until previous tasks have finished
        Schedulable* lastTask = clusterOfInterest->order.at(nodeIdx-1);
        nodeStart = localNodeInfo[lastTask].finish;
      }
      else {
        //Otherwise it cannot start until there is space in the thread
        nodeStart = myThread->finish;
      }
      assert(nodeStart >= 0);
      //Ultimately, currNode cannot start until its predecessors have finished
      localNodeInfo[currNode].start = std::max(dominantFinish, nodeStart);
      localNodeInfo[currNode].finish = localNodeInfo[currNode].start + policy->nodeCost(currNode) + totalPredComm;
    }

    result->start = localNodeInfo[clusterOfInterest->order[0]].start;
    result->finish = localNodeInfo[clusterOfInterest->order.back()].finish;
  }

  /**
   * Assumes all clusters are in the same thread_t.
   * Considers each cluster for exclusion from the
   * main thread_t and accepts it if the makespan
   * is shortened.
   */
  int RACH::runPass2(int numThreads) {
    threads.resize(numThreads); //O(p)
    //Start out by placing all clusters in the primary thread_t
    thread_t& primary = threads.front();
    //Complexity: O(n+e*(log(e)+log(n))) ; reducible to O(n+e)
    for(std::list<cluster_t>::iterator i = clusters.begin(),
        ie = clusters.end(); i != ie; ++i) {
      primary.addCluster(&*i, cluster2thread);
    }
    singleThreadMakespan = primary.execution;

    /*
     * Thread creation and preparation
     */

    //Create the pthreads representing actual cores
    pthread_t pthreads[numThreads];

    //Data to pthreads
    threadData thread_args[numThreads-1];

    //The cluster currently being worked on
    cluster_t* clusterP = NULL;

    //Barriers for synchronization
    pthread_barrier_t barrierBegin;
    pthread_barrier_t barrierEnd;
    pthread_barrier_init(&barrierBegin, NULL, numThreads);
    pthread_barrier_init(&barrierEnd, NULL, numThreads);

    //Indicates when we have completed
    volatile bool done = false;

    //Results from pthreads
    std::vector<cluster_position> results(numThreads);
    std::vector< std::map<Schedulable*, nodeInfo_t> > localNodeInfos(numThreads);

    //Set up the arguments for the pthreads
    for(size_t i = 1; i < numThreads; ++i) { //O(p)
      thread_args[i-1].policy = policy;
      thread_args[i-1].nodeInfo = &nodeInfo;
      thread_args[i-1].task2cluster = &task2cluster;
      thread_args[i-1].cluster2thread = &cluster2thread;
      thread_args[i-1].threadNumber = i;
      thread_args[i-1].myThread = &threads.at(i);
      thread_args[i-1].currCluster = &clusterP;
      thread_args[i-1].barrierBegin = &barrierBegin;
      thread_args[i-1].barrierEnd = &barrierEnd;
      thread_args[i-1].done = &done;

      //For returning values
      thread_args[i-1].result = &results.at(i);
      thread_args[i-1].localNodeInfo = &localNodeInfos.at(i);

      //Create each thread and ensure creation was successful
      int status = pthread_create(&pthreads[i], NULL, RACH::evaluateThread, (void*)&thread_args[i-1]);
      assert(!status);
    }

    //Get all pthreads to the same point
    pthread_barrier_wait(&barrierEnd);

    /* Consider the clusters, one at a time, for separation */
    std::stack<cluster_t*> readyList;
    std::map<cluster_t*, int> unscheduledPreds;
    for(std::list<cluster_t>::iterator i = clusters.begin(), //O(c*log(c))
        ie = clusters.end(); i != ie; ++i) {
      cluster_t* currCluster = &*i;
      int predCount = currCluster->predComms.size();
      assert(predCount == currCluster->predecessorClusters.size());
      //int predCount = currCluster->predecessorClusters.size();
      //if(predCount != currCluster->predComms.size()) fprintf(stderr, "Actual: %d, Supposed: %d\n", predCount, currCluster->predComms.size());
      unscheduledPreds[currCluster] = predCount;
      if(!predCount)
        readyList.push(currCluster);
    }

    //Local, limited version of nodeInfo
    std::map<Schedulable*, nodeInfo_t> localNodeInfo;

    while(!readyList.empty()) {
      //Get the first cluster out of the ready list
      cluster_t* currCluster = readyList.top(); //O(1)
      readyList.pop(); //O(1)

      //Let the pthreads go ahead and calculate impact
      clusterP = currCluster;
      if(currCluster->order.begin() == currCluster->order.end()) //Then we have an empty cluster
        continue;
      pthread_barrier_wait(&barrierBegin);

      localNodeInfo.clear();

      //Look at when each node within the cluster can execute
      computeThreadImpact(currCluster, &threads.at(0), 0, nodeInfo, task2cluster, cluster2thread, policy, &results.at(0), localNodeInfos.at(0));

      //Wait for all pthreads to finish their calculations
      pthread_barrier_wait(&barrierEnd);

      //Determine which pthread had the best result
      schedtime_t finishTime = std::numeric_limits<schedtime_t>::max();
      int threadNum = -1;
      for(int i = 0; i < numThreads; ++i) {
        cluster_position& result = results[i];
        if(result.finish < finishTime) {
          finishTime = result.finish;
          threadNum = i;
        }
      }
      assert(threadNum >= 0);

      //Implement the results from the best pthread
      if(threadNum != 0) {
        threads[threadNum].addCluster(currCluster, cluster2thread);
      }

      //Copy the best localNodeInfo results over to nodeInfo
      std::map<Schedulable*, nodeInfo_t>& winningNodeInfo = localNodeInfos.at(threadNum);
      for(std::vector<Schedulable*>::iterator i = currCluster->order.begin(),
          ie = currCluster->order.end(); i != ie; ++i) {
        Schedulable* taskAtHand = *i;
        nodeInfo[taskAtHand].start = winningNodeInfo[taskAtHand].start;
        nodeInfo[taskAtHand].finish = winningNodeInfo[taskAtHand].finish;
      }
      //Reflect the occupancy of the thread
      threads.at(threadNum).start = nodeInfo[currCluster->order.at(0)].start;
      threads.at(threadNum).finish = nodeInfo[currCluster->order.back()].finish;

      //Keep track of schedule length
      multiThreadMakespan = std::max(multiThreadMakespan, threads.at(threadNum).finish);

      //Refresh the ready list
      for(std::set<cluster_t*>::iterator i = currCluster->successorClusters.begin(),
          ie = currCluster->successorClusters.end(); i != ie; ++i) {
        cluster_t* succCluster = *i;
        int remainingPreds = unscheduledPreds[succCluster];
        remainingPreds--;
        unscheduledPreds[succCluster] = remainingPreds;
        if(!remainingPreds)
          readyList.push(succCluster);
      }
    }

    //Indicate that all clusters have been evaluated
    done = true;
    pthread_barrier_wait(&barrierBegin);

    for(size_t i = 1; i < numThreads; ++i) {
      pthread_join(pthreads[i], NULL);
    }

    return multiThreadMakespan;
  }

  void* RACH::evaluateThread(void* args) {
    //Cast args to a threadData*
    threadData* data = (threadData*)args;

    //Extract arguments
    Policy* policy = data->policy;
    const std::map<Schedulable*, nodeInfo_t>& nodeInfo = *data->nodeInfo;
    const std::map<Schedulable*, cluster_t*>& task2cluster = *data->task2cluster;
    const std::map<cluster_t*, thread_t*>& cluster2thread = *data->cluster2thread;
    int threadNum = data->threadNumber;
    thread_t* myThread = data->myThread;
    cluster_t** clusterPP = data->currCluster;
    pthread_barrier_t* barrierBegin = data->barrierBegin;
    pthread_barrier_t* barrierEnd = data->barrierEnd;
    volatile bool* done = data->done;

    cluster_position* result = data->result;

    //Local, limited version of nodeInfo
    std::map<Schedulable*, nodeInfo_t>& localNodeInfo = *data->localNodeInfo;

    //Ensure everyone is to this point
    pthread_barrier_wait(barrierEnd);
    while(true) {
      //Wait for the cluster to be made ready
      pthread_barrier_wait(barrierBegin);
      if(*done)
        break;

      //Figure out the imapct on this thread of taking on the cluster
      RACH::computeThreadImpact(*clusterPP, myThread, threadNum, nodeInfo, task2cluster, cluster2thread, policy, result, localNodeInfo);

      //Indicate to the main thread that my work is done
      pthread_barrier_wait(barrierEnd);
    }

    return (void*)NULL;
  }

  /**
   * Reports the threads for use in the post-scheduler
   */
  //O(n)
  void RACH::reportThreads() {
    //printf("Makespans: %d vs %d\n", singleThreadMakespan, multiThreadMakespan);
    if(singleThreadMakespan < multiThreadMakespan) {
      for(schedulableIterator i = graph->begin(),
          ie = graph->end(); i != ie; ++i) {
        Schedulable* currTask = *i;
        currTask->setThread(0);
      }
    }
    else {
      if(winningThreadCount < numThreads) {
        fprintf(stderr, "Winning thread count under maximum: %d vs %d\n", winningThreadCount, numThreads);
      }

      int thNum = 0;
      for(std::vector<thread_t>::iterator i = threads.begin(),
          ie = threads.end(); i != ie; ++i, ++thNum) {
        thread_t& currThread = *i;
        for(std::set<cluster_t*>::iterator j = currThread.begin(),
            je = currThread.end(); j != je; ++j) {
          cluster_t& currCluster = **j;
          for(std::set<Schedulable*>::iterator k = currCluster.begin(),
              ke = currCluster.end(); k != ke; ++k) {
            Schedulable* currTask = *k;
            currTask->setThread(thNum);
          }
        }
      }
    }
  }
}

