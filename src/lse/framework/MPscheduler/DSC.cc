#include "Clusterer.h"
#include "Policy.h"
#include "DSC.h"

namespace LSE_scheduler {
  //*********************************************************
  //*               Definition of Node class                *
  //*********************************************************

  /**
   * Constructor
   */
  DSC::Node::Node(int id, schedtime_t cost)
    : id(id), ClusterID(-id), tlevel(0), ptlevel(0),
    predCount(0), succCount(0), blevel(cost), cost(cost), 
    examined(false), pFree(false), locked(false), movable(true) {}

  DSC::Node::Node() {}

  void DSC::Node::printNode() {
    std::cout << "\nNodePrint: " << id << "\n";
    std::cout << "ClusterID: " << ClusterID <<
      "    tlevel: " << tlevel;
  }

  //*********************************************************
  //*          Definition of compare_PRIO struct            *
  //*********************************************************

  DSC::compare_PRIO::compare_PRIO() {}

  bool DSC::compare_PRIO::operator() (Schedulable *a, Schedulable *b) {
    return b->getPriority() > a->getPriority() || 
      (b->getPriority() == a->getPriority() && a->getID() > b->getID());
  }

  //*********************************************************
  //*        Definition of compare_costComm struct          *
  //*********************************************************

  /**
   * Constructor
   */
  DSC::compare_costComm::compare_costComm(DSC& dsc, const Schedulable* to)
    : dsc(dsc), to(to) {}

  bool DSC::compare_costComm::operator()(Schedulable* a, Schedulable* b) {
    schedtime_t ca = dsc.nodeInfo[a].tlevel + dsc.nodeInfo[a].cost +
      dsc.costOfComm(a, to);
    schedtime_t cb = dsc.nodeInfo[b].tlevel + dsc.nodeInfo[b].cost +
      dsc.costOfComm(b, to);
    return (cb > ca || (cb == ca && dsc.nodeInfo[a].id > dsc.nodeInfo[b].id));
  }

  //*********************************************************
  //*         Definition of compare_tlevel struct           *
  //*********************************************************

  DSC::compare_tlevel::compare_tlevel(DSC& dsc) : dsc(dsc) {}

  bool DSC::compare_tlevel::operator()(const Node *a, const Node *b) const {
    return b->tlevel < a->tlevel || (b->tlevel == a->tlevel && a->id > b->id);
  }

  //*********************************************************
  //*          Definition of clusterInf_t struct            *
  //*********************************************************

  DSC::clusterInf_t::clusterInf_t(int s, int e) : size(s), endtime(e) {}
  
  //*********************************************************
  //*            Definition of arrive_t struct              *
  //*********************************************************

  DSC::arrive_t::arrive_t(schedtime_t a, Node *p) : arrival(a), node(p) {}

  //*********************************************************
  //*         Definition of compare_arrival struct          *
  //*********************************************************

  DSC::compare_arrival::compare_arrival(DSC &dsc) : dsc(dsc) {}

  bool DSC::compare_arrival::operator()(const arrive_t &a, 
					const arrive_t &b) const {
    return (b.arrival > a.arrival || 
        (b.arrival == a.arrival && a.node->id > b.node->id));
  }


  //*********************************************************
  //*               Definition of DSC class                 *
  //*********************************************************

  /**
   * Constructor
   */
  DSC::DSC() : Clusterer(), clusterCount(0) {}

  /**
   * Runs DSC on the supplied graph
   */
  void DSC::run(SchedulableDAG& graph, FILE* logfile) {
    this->graph = &graph;
    if(logfile) fprintf(logfile, "====Start DSC Clusterer====\n");
    this->logfile = logfile;
    doDSC();
    reassignClusters();
    graph.setClusterCount(clusterCount);
  }

  bool DSC::isReducible(Schedulable* ny, bool dolocks) {
    schedtime_t oldstart = nodeInfo[ny].tlevel, newstart;
    bool foundNonMovable = false;
    int tryCluster = -1;

    costpq2_t DSnodes(compare_tlevel(*this));
    nptrlist_t nl;

    for ( schedulableIterator i = ny->pred_begin(),
        ie = ny->pred_end(); i != ie; ++i) {
      Node &pred = nodeInfo[*i];

      // find previously scheduled DS node
      if ( pred.examined == false ) continue;
      if ( (pred.tlevel + pred.cost + costOfComm(*i, ny))
          != nodeInfo[ny].tlevel ) continue;

      // predecessor already locked for a DS node
      if (nodeInfo[ny].id != locked_ny &&
          (pred.locked || pred.ClusterID == locked_cluster))
        return false;

      if (!pred.movable || (*i)->getSuccessorCount() > 1) {
        // we can consider this cluster, but not any other
        tryCluster = pred.ClusterID;
        if (foundNonMovable) return(false);
        foundNonMovable = true;
      }
      else {
        DSnodes.push(&pred);
        nl.push_back(&pred);
      }
    } // for i in predecessors of ny

    //errs() << "\tDSnodes:" << DSnodes.size() << "\n";

    // note: tryCluster will be set from the predecessor if DSnodes is empty
    if (tryCluster == -1) {
      tryCluster = DSnodes.top()->ClusterID;
    }

    schedtime_t avail = clusters[tryCluster].endtime;
    while (!DSnodes.empty()) {
      Node *i = DSnodes.top();
      DSnodes.pop();
      if (i->ClusterID != tryCluster) {
        avail = std::max(avail, i->tlevel) + i->cost;
        if (avail > oldstart) break;
      }
    }
    newstart = avail;
    if (newstart >= oldstart) return(false);

    if (dolocks) {
      locked_cluster = tryCluster;
      locked_is_reducible = true;
      locked_currtime = newstart;
      //errs() << "\tlock is reducible on cluster " << tryCluster 
      //	<< " at time " << locked_currtime << "\n";

      for (nptrlist_t::iterator ii=nl.begin(), e=nl.end();
          ii != e; ++ii) {
        (*ii)->locked = true;
        //errs() << "\tLocking " << (*ii)->id << "\n";
      }
    }

    nodeInfo[ny].ClusterID = tryCluster;
    if (logfile)
      fprintf(logfile, "\tAssigning %s to cluster %d\n",
	      ny->getName().c_str(), tryCluster);

    return true;
  }

  /**
   * Main algorithmic function of DSC
   */
  void DSC::doDSC() {
    initial();
    locked_ny = locked_cluster = -1;

    while ( numToExamine > 0 ) {
      Schedulable* topNode = freeList.top();
      freeList.pop();
      Node* nx = &nodeInfo[topNode];

      numToExamine--;

      //errs() << "Considering node " << nx->id << "\n";

      // Handle the warrant calculations

      //errs() << "\tPRIO = " << nx->PRIO << "\n";
      //if (!partialFreeList.empty() && locked_ny == -1) 
      //errs() << "\tpartialID = " << partialFreeList.top()->id 
      //	  << " pPRIO = " << partialFreeList.top()->PRIO
      //	  << "\n";

      if(!partialFreeList.empty() && locked_ny == -1 &&
          topNode->getPriority() < partialFreeList.top()->getPriority()) {

        // Find out which clusters and nodes must be locked.  Note that
        // the paper talks about locking clusters, but not nodes.

        Schedulable* ny = partialFreeList.top();

        locked_ny = nodeInfo[ny].id;
        //errs() << "\tSetting locked_ny to " << locked_ny << "\n";

        locked_is_reducible = isReducible(ny, true);

      } // if needed to look at a partially free node

      // Now try to cluster the node with one or more predecessors

      // if it's a DS node that's still waiting to free up or it has no
      // predecessor to cluster to, then give it a new cluster.

      if (nx->locked || !topNode->getPredecessorCount()) {
        nx->ClusterID = clusters.size();
        clusters.push_back(clusterInf_t(1,nx->tlevel+nx->cost));
	if (logfile)
	  fprintf(logfile,"\tCreating cluster %d for %s because of %s\n",
		  nx->ClusterID, topNode->getName().c_str(),
		  (nx->locked ? "locking" : "no predecessors"));
      }

      // fast special case when only one predecessor
      else if (topNode->getPredecessorCount()==1) {
        Schedulable* predSched = *(topNode->pred_begin());
        Node &pred = nodeInfo[predSched];
        int cluster = pred.ClusterID;

        if (cluster == locked_cluster ||
            clusters[cluster].endtime >= nx->tlevel) { // new cluster

          nx->ClusterID = clusters.size();
          clusters.push_back(clusterInf_t(1,nx->tlevel+nx->cost));

	if (logfile)
	  fprintf(logfile,"\tCreating cluster %d for %s because of %s\n",
		  nx->ClusterID, topNode->getName().c_str(),
		  (cluster==locked_cluster ? "locked single predecessor" 
		   : "late single predecessor"));
        } else {
          nx->ClusterID = cluster;
          nx->tlevel = clusters[cluster].endtime;
	  if (logfile)
	    fprintf(logfile, "\tAdding %s to cluster %d tlevel=%lld\n",
		    topNode->getName().c_str(), cluster, nx->tlevel);
          clusters[cluster].endtime += nx->cost;
          clusters[cluster].size++;
          nx->movable = false;
        }
      }

      // this is ny, at last and it's not reducible
      else if (nx->id == locked_ny && !locked_is_reducible) {
        // unlock
        locked_ny = -1;
        locked_cluster = -1;
        // Note: no point in unlocking nodes as they've already been scheduled

        nx->ClusterID = clusters.size();
        clusters.push_back(clusterInf_t(1,nx->tlevel+nx->cost));
	if (logfile)
	  fprintf(logfile,"\tCreating cluster %d for %s"
		  " because the node was partially free but not reducible\n",
		  nx->ClusterID, topNode->getName().c_str());
      }

      // either it's ny and already reducible, or we need to figure out
      // whether it is.  Reducible means that clustering it makes sense.
      else {

        if (! ((nx->id == locked_ny) || isReducible(topNode,false))) {

          nx->ClusterID = clusters.size();
          clusters.push_back(clusterInf_t(1,nx->tlevel+nx->cost));

	  if (logfile)
	    fprintf(logfile, "\tCreating cluster %d for %s"
		    " because clustering didn't help makespan\n",
		    nx->ClusterID, topNode->getName().c_str());

        } else { // do the clustering

          // Note: isReducible set nx->clusterID either in the previous call OR
          // when the node became locked.

          // The algorithm differs considerably from the DSC paper in that
          // the DSC paper seems to imply that we would break from the 
          // iterator as soon as there is a predecessor with > 1 successor and
          // that we need to iterate in the decending arrival time order.

          // Calculate movable predecessors and the lower bound of when the
          // task has to start, which is the later of the start time and
          // any arrival time from a node which doesn't move.

          schedtime_t lowbound = clusters[nx->ClusterID].endtime;
          std::vector<arrive_t> tlist; 

          for ( schedulableIterator i = topNode->pred_begin(),
              e = topNode->pred_end(); i != e; ++i ) {
            Node &pred = nodeInfo[*i];
            if (nx->ClusterID == pred.ClusterID) continue;

            schedtime_t arrive = pred.tlevel + pred.cost + costOfComm(*i,topNode);
            if (!pred.movable || (*i)->getSuccessorCount()>1
                || ( nx->id != locked_ny && (pred.locked ||
                    pred.ClusterID == locked_cluster)))
              lowbound = std::max(lowbound, arrive);

            else tlist.push_back(arrive_t(arrive,&nodeInfo[*i]));

          } // for i in predecessors

          if (!tlist.size()) { // no predecessors to reduce, just schedule it.

            nx->tlevel = lowbound;
            if (clusters[nx->ClusterID].endtime < lowbound)
              clusters[nx->ClusterID].endtime = lowbound + nx->cost;
            else clusters[nx->ClusterID].endtime += nx->cost;
            clusters[nx->ClusterID].size++;

            nx->movable = false;

	    if (logfile)
	      fprintf(logfile, "\tMerging %s to cluster %d tlevel=%lld\n",
		      topNode->getName().c_str(), nx->ClusterID, nx->tlevel);
          } else {

            // The next two lists only hold predecessors that can move that
            // aren't already on the same cluster and that are arriving later
            // than the current bound on the start time.  (If they arrive 
            // earlier, there's no point in clustering.)

            // list sorted by decreasing arrival time
            arrivepq_t plist = arrivepq_t(compare_arrival(*this));

            for (std::vector<arrive_t>::iterator i = tlist.begin(), 
                ie = tlist.end();
                i != ie; ++i) {

              if (i->arrival > lowbound && i->node->ClusterID != nx->ClusterID) {
                plist.push(*i);
              }
            }

            // at this point, lowbound contains the earliest time we could start,
            // not taking into account the edges we are considering.  Now the
            // paper binary searches, but because we don't expect insane
            // numbers of edges, I'll go ahead and linear search, since I 
            // understand that better.

            // We're trying to find the place where zeroing no longer helps.
            // However, zeroing along the DS always helps. (or at least, 
            // the released code always does it.)
            std::vector<Node *> todo;

            // zero all DS edges
            while (!plist.empty()) {
              arrive_t n = plist.top();
              if (n.arrival < nx->tlevel) break;
              todo.push_back(n.node);
              plist.pop();
            }

            std::sort(todo.begin(), todo.end(), compare_tlevel(*this));

            if (!plist.empty()) {
              bool firstiter=true;
              bool finaliter=false;
              arrive_t lastNode(0,0);
              do {
                // invariant: the todo list has been sorted
                if (plist.empty()) finaliter = true;

                // figure out the current tradeoff..

                // what the next communication edge forces
                schedtime_t lastarrive = (finaliter ? lowbound : 
                    std::max(plist.top().arrival, lowbound));

                // what the end of cluster enforces
                schedtime_t clusterlen = clusters[nx->ClusterID].endtime;

                // sort by increasing tlevel
                for (std::vector<Node *>::iterator i = todo.begin(), 
                    ie = todo.end();
                    i != ie; ++i)
                  clusterlen = std::max(clusterlen, (*i)->tlevel) + (*i)->cost;

                if (clusterlen >= lastarrive) { 
                  // went too far, drop last one if not on the first time 
                  // through (in that case, todo has all DS nodes)
                  if (!firstiter) {
                    todo.erase(std::find(todo.begin(),todo.end(),
                          lastNode.node));
                    std::sort(todo.begin(), todo.end(), compare_tlevel(*this));
                    plist.push(lastNode);
                  }
                  break; // went too far!
                }

                if (finaliter) break; // can't do the rest of this!

                todo.push_back(plist.top().node);
                std::sort(todo.begin(), todo.end(), compare_tlevel(*this));

                lastNode = plist.top();
                plist.pop();
                firstiter = false;

              } while (!finaliter);
            }

            // invariant: the todo list has been sorted

            // Now zero the edges
            schedtime_t clusterlen = clusters[nx->ClusterID].endtime;
            for (std::vector<Node *>::iterator i = todo.begin(), 
                ie = todo.end();
                i != ie; ++i) {
              Node *np = *i;

              int ocl = np->ClusterID;
              clusters[nx->ClusterID].size--;
              np->ClusterID = nx->ClusterID;
              clusters[nx->ClusterID].size++;

              np->tlevel = std::max(np->tlevel, clusterlen);
              clusterlen = np->tlevel + np->cost;

	      if (logfile)
		fprintf(logfile,"\tMoving %d from cluster %d to cluster %d"
			" tlevel=%lld\n",
			np->id, ocl, np->ClusterID, np->tlevel);
            }

            // what the next communication edge forces
            schedtime_t lastarrive = (plist.empty() ? lowbound : 
                std::max(plist.top().arrival, lowbound));

            nx->tlevel = std::max(lastarrive, clusterlen);
            clusters[nx->ClusterID].endtime = nx->tlevel + nx->cost;
            clusters[nx->ClusterID].size++;
            nx->movable = false;

	    if (logfile)
	      fprintf(logfile, "\tMerging %s to cluster %d tlevel=%lld\n",
		      topNode->getName().c_str(), nx->ClusterID, nx->tlevel);

          } // else complex merge

          if (nx->id == locked_ny) {
            for ( schedulableIterator i = topNode->pred_begin(),
                e = topNode->pred_end(); i != e; ++i ) 
              nodeInfo[*i].locked = false;

            locked_ny = -1;
            locked_cluster = -1;
          }
        }
      }

      nx->examined = true; // can't do it before

      // recalculate priorities of successors; add it to fl or pfl.  
      // Even more fun; we really don't need a separate ptlevel, because
      // it's just the tlevel calculated based on what we know.  Until
      // we know everything, tlevel is meaningless anyway....

      for ( schedulableIterator i = topNode->succ_begin(),
          ie = topNode->succ_end(); i != ie; ++i )
      {
        Node &node = nodeInfo[*i];

        int oldtlevel = node.tlevel;
        int arrival = nx->tlevel + nx->cost + costOfComm(topNode, *i);
        int newtlevel = std::max(oldtlevel, arrival);
        node.tlevel = newtlevel;

        //errs() << "Updating " << node.id << " tlevel to "  << newtlevel
        //    << "\n";

        if (nodeInfo[*i].id == locked_ny && locked_is_reducible && oldtlevel <= arrival) {
          // check whether the locked_ny needs updating because a predecessor
          // has scheduled.  (Update the lock status for scheduled nodes).

          if (!nx->movable || topNode->getPredecessorCount()>1) { // not reducible

            for ( schedulableIterator j = (*i)->pred_begin(),
                je = (*i)->pred_end(); j != je; ++j ) 
              nodeInfo[*j].locked = false;
            locked_is_reducible = false;
            locked_cluster = -1;
            // leave locked_ny!
          } else {
            schedtime_t ns = std::max(locked_currtime, nx->tlevel) + nx->cost;
            if (ns >= oldtlevel) { // irreducible
              for ( schedulableIterator j = (*i)->pred_begin(),
                  je = (*i)->pred_end(); j != je; ++j ) 
                nodeInfo[*j].locked = false;
              locked_is_reducible = false;
              locked_cluster = -1;
            } else { // still reducible, update the time and lock the node
              locked_currtime = ns;
              nx->locked = true; 
            }
          }
        } // need to update partial reducibility

        node.predCount--;
        if(node.predCount == 0) { //add to free list
          (*i)->setPriority(newtlevel + node.blevel); //node.PRIO = newtlevel + node.blevel;

          //errs() << "Adding " << node.id << " to FL tlevel=" 
          //      << newtlevel << " PRIO=" << node.PRIO << "\n";

          freeList.push(*i);
          // must ensure that node is in the pfl, because prioq crashes
          // if it's not there.
          if (node.pFree) partialFreeList.erase(*i);
        }
        else { // add to partially free list

          (*i)->setPriority(node.blevel + newtlevel); //node.PRIO = node.blevel + newtlevel;

          //errs() << "Adding " << node.id << " to pFL ptlevel=" 
          //      << newtlevel << " pPRIO=" << node.PRIO << "\n";

          if ( !node.pFree ) partialFreeList.push(*i);
          else {
            partialFreeList.erase(*i);
            partialFreeList.push(*i);
          }
          node.pFree = true;
        }
      } // for i in successors

    } // while numToExamine > 0

    //results.nodeinf.resize(tg.getNumNodes());
    for ( schedulableIterator i = graph->begin(),
       e = graph->end(); i != e; ++i) {
      (*i)->setCluster(nodeInfo[*i].ClusterID);
      (*i)->setTLevel(nodeInfo[*i].tlevel);
      (*i)->setBLevel(nodeInfo[*i].blevel);
    }
    //results.clusterinf.resize(tg.getNumNodes());

    //printDSC("AfterDo.dot");
  }

  bool DSC::initial() {
    //Initialize the nodeInfo structure: map each node (Schedulable*) to a DSC Node
    int idnum = 0;
    for ( schedulableIterator node = graph->begin(), end = graph->end();
        node != end; ++node, ++idnum ) {
      schedtime_t cost = policy->nodeCost(*node);
      nodeInfo[*node] = Node(idnum,cost);
      (*node)->setPriority(cost);
    }

    numToExamine = graph->size();

    std::stack<Schedulable*> ready;
    std::map<Schedulable*, int> successors;
    //Find the sink nodes
    for ( schedulableIterator i = graph->begin(), e = graph->end();
        i != e; i++ ) {
      successors[*i] = (*i)->getSuccessorCount();
      if ( successors[*i] == 0 )
        ready.push(*i);
    }

    while ( !ready.empty() ) {
      Schedulable* topNode = ready.top();
      Node &node = nodeInfo[topNode];
      ready.pop();

      //The blevel is the longest distance to exit the graph
      node.blevel = 0;
      for ( schedulableIterator i = topNode->succ_begin(),
          e = topNode->succ_end(); i != e; ++i ) {
        Node &succ = nodeInfo[*i];
        schedtime_t blevel = costOfComm(topNode, *i) + succ.blevel;
        node.blevel = std::max(blevel, node.blevel);
      }
      node.blevel += node.cost;
      //errs() << "Set blevel " << node.id << "=" << node.blevel << "\n";

      //Any predecessor whose successors' blevels have all been determined is ready
      for ( schedulableIterator i = topNode->pred_begin(),
          e = topNode->pred_end(); i != e; ++i ) {
        successors[*i]--;
        if ( successors[*i] == 0 )
          ready.push(*i);
      }

      topNode->setPriority(node.blevel); //node.PRIO = node.blevel;

      //cTODO: pull out? *Can't be pulled out because node changes and is a
      //                  reference for each iteration

      //freeList gets filled with source nodes
      node.predCount = topNode->getPredecessorCount();
      node.succCount = topNode->getSuccessorCount();
      if ( topNode->getPredecessorCount() == 0 ) {
        freeList.push(topNode);
      }
    }

    // printPT();
    return true;
  }

  schedtime_t DSC::costOfComm(const Schedulable* from, const Schedulable* to){
    // Cheat by casting to (Schedulable*) since we know we won't 
    // change it anyways
    if (from == to || 
	(nodeInfo[(Schedulable*)from].ClusterID == 
	 nodeInfo[(Schedulable*)to].ClusterID /*&&
         from->getCluster() >= 0 && to->getCluster() >= 0*/) )
      return 0;
    else
      return policy->naiveCommCost(from, to);
  }

  int DSC::numClusters() {
    std::set<int> clusters;
    for(schedulableIterator i = graph->begin(),
        e = graph->end(); i != e ; ++i)
      clusters.insert(nodeInfo[*i].ClusterID);
    return clusters.size();
  }

  /**
   * Clusters can initially be arbitrary large numbers.
   * This changes clusters to increase sequentially from 0 to 
   * the number of clusters minus one and marks the real cluster numbers.
   */
  void DSC::reassignClusters() {
    std::map<int, int> clusterMap;
    clusterCount = 0;

    graph->setClusterCount(graph->size()+1);

    for(schedulableIterator i = graph->begin(),
        e = graph->end(); i != e; ++i) {
      int currentCluster = (*i)->getCluster();

      if(clusterMap.find(currentCluster) == clusterMap.end())
        clusterMap[currentCluster] = clusterCount++;

      graph->assignNodeToCluster(*i,clusterMap[currentCluster]);
    }
    graph->setClusterCount(clusterCount);
  }

  /*bool DSC::printDSC(const char* file) {
    std::map<int, int> clusterColor;

    std::ofstream out(file, std::ios::trunc);
    out << "digraph xDSC {\n";    
    for ( clusters_t::iterator i = clusters.begin(), ie = clusters.end(); i != ie; ++i)
    {
      int color = rand() % 100000 + 1;
      if(color < 10000)
        color += 10000;
      clusterColor.insert(std::make_pair((*i).first, color));
    }

    for ( typename nodes_t::iterator j = nodeInfo.begin(), ej = nodeInfo.end(); j != ej; ++j)
    {
      Node &n = *j;
      out << n.id << "[label=\"" << n.id 
        << ":" << n.cost << ": t=" << n.tlevel
        << " b=" << n.blevel 
        <<" cID=" << n.ClusterID
        <<"\" style=\"filled\" color=\"#" << clusterColor[n.ClusterID] << "\"];\n";
      for ( link_iterator k = tg.NodeSuccBegin(j->id), ke = tg.NodeSuccEnd(j->id); k != ke; ++k )
        out << n.id << " -> " << nodeInfo[k->id].id << "[label=\""
          << costOfComm(n,nodeInfo[k->id]) << "\"];\n";
    }

    out << "\n}\n";
    out.close();
    return true;
  }*/

  /*void DSC::fileName() {
    std::stringstream ss;
    ss << "stepDot" << numToExamine << ".dot";
    std::cout << ss.str() << "\n";
    printDSC(ss.str());
  }*/

  /*void DSC::printPFL() {
    std::cout << "\nList: ";

    for ( pFL_t::iterator i = partialFreeList.begin(),
        e = partialFreeList.end(); i != e; ++i)
      std::cout<< i->second << ":" << i->first << ", ";
    std::cout<<"\n";
  }*/

  /*void DSC::printPT() {
    schedtime_t PT = 0;

    for ( typename nodes_t::iterator i = nodeInfo.begin(), e = nodeInfo.end(); i != e;++i)
      PT = (PT > (*i).blevel )? PT:(*i).blevel;

    std::cout << "\n The parrallel schedule depth is PT: " << PT << std::endl;
  }*/

  /*void DSC::printFL(priority_q_dsc& list) {
    typename priority_q_dsc::iterator fl_it;

    std::cout << "\nList: ";
    for(fl_it = list.begin(); fl_it != list.end(); fl_it++)
      std::cout << (*fl_it)->id << ":" << (*fl_it)->PRIO << ", ";
    std::cout<<"\n";
  }*/

  /*void DSC::printPL(costpq_t& list, Node &to) {
    typename costpq_t::iterator plist_it;

    std::cout<<"pList: ";
    for(plist_it = list.begin();plist_it != list.end(); plist_it++)
      std::cout << (*plist_it)->id << ": " << (*plist_it)->cost + costOfComm(*(*plist_it), to) <<", ";
    std::cout<<"\n";

    costpq_t listTMP(compare_costComm(*this, &to));
    while(!list.empty())
    {
      Node* top = list.top();
      listTMP.push(list.top());
      list.pop();
      std::cout<< top->id << ": " << top->cost + costOfComm((*top), to) << ", ";
    }
    std::cout<<"\n";

    while(!listTMP.empty())
    {
      list.push(listTMP.top());
      listTMP.pop();
    }

  }*/

}

