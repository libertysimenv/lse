/* 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
#ifndef _SCHEDULABLE_DAG_H_
#define _SCHEDULABLE_DAG_H_
#include "Schedulable.h"
#include <assert.h>
#include <vector>
#include <iostream>
#include <map>
#include <limits>
#include <stdio.h>

/**
 * This file defines the SchedulableDAG class.  This class is used to expose a (dag.
 * certain interface to the scheduler.  The Schedulers schedule SchedulableDAGs.
 * This class represents both an unscheduled DAG, and a scheduled DAG.
 * When the DAG has been scheduled, it will set it's scheduled property to true
 * and will have the schedulethreads member populated with a valid schedule.
 */
namespace LSE_scheduler {

  class SchedulableDAG
  {
    public: //Types
      struct clusterinf_t {
        int thread;
        clusterinf_t()
          : thread(-1), start(std::numeric_limits<schedtime_t>::max()), 
	  finish(0), tlevel(0), blevel(0) {}

        schedtime_t start;
        schedtime_t finish;
        schedtime_t tlevel;
        schedtime_t blevel;

        schedtime_t execCost;
        schedtime_t commCost;
	std::set<Schedulable *> nodes;
      };

      typedef std::vector<clusterinf_t> clusters_t;

      /**
       * This type is the order that the schedulables should appear in a thread
       * It is set in the scheduling process, and then turned into a 
       * RuntimeSchedule by the allocator
       */
      typedef std::vector<Schedulable*> ThreadTaskOrder;

      /**
       * While the ThreadTaskOrder holds the information of the order of 
       * scheduables in a single thread, the TaskOrder holds all of the 
       * threads ThreadTaskOrders
       */
      typedef std::vector<ThreadTaskOrder> TaskOrder;

    protected://Members
      typedef std::map<Schedulable*, std::map<Schedulable*, int> > edge_weight_t;
      edge_weight_t edges;
      std::vector<Schedulable*> tasks;
      std::vector<clusterinf_t> clusterinf;
      int numThreads;
      //Tells everyone if this has been scheduled or not.
      bool scheduled;

      //The order that tasks should be run in on the different threads
      //this is the final output of the schedulers
      TaskOrder taskOrder;

      int semCnt;
      int lockCnt;

    protected: //Methods
      SchedulableDAG() : numThreads(-1), scheduled(false) {}
      ~SchedulableDAG() {
        for(std::vector<Schedulable*>::iterator i = tasks.begin(),
            ie = tasks.end(); i != ie; ++i) {
          Schedulable* sched = *i;
          delete sched;
        }
      }

    public:
      //Allows the SchedulableDAG to internally keep track of edge weights
      void addEdge(Schedulable* source, Schedulable* sink, int weight) {
        edges[source].insert(std::make_pair(sink, weight));
      }

      int getEdgeWeight(Schedulable* source, Schedulable* sink) {
        std::map<Schedulable*, std::map<Schedulable*, int> >::iterator 
	  rowIter = edges.find(source);
        if(rowIter == edges.end())
          return -1;

        std::map<Schedulable*, int>* row = &rowIter->second;
        std::map<Schedulable*, int>::iterator colIter = row->find(sink);
        if(colIter == row->end())
          return -1;

        return colIter->second;

        //return edges[source][sink];
      }

      schedulableIterator begin() { return tasks.begin(); }
      schedulableIterator end() { return tasks.end(); }
      size_t size() { return tasks.size(); }

      void setClusterCount(int clusterCount) { clusterinf.resize(clusterCount); }
      size_t getClusterCount()const { return clusterinf.size(); }

      bool getScheduled() {return scheduled; }
      void setScheduled(bool value) { scheduled = value;}
      
      int getNumThreads() const { return numThreads; }
      void setNumThreads(int value) { numThreads = value; }

      int getSemCnt() const { return semCnt; }
      void setSemCnt(int value) { semCnt = value; }

      int getLockCnt() const { return lockCnt; }
      void setLockCnt(int value) { lockCnt = value; }

      int getClusterThread(int cluster) const {
        assert(cluster < (int)clusterinf.size() && 
	       "Querying the thread for an invalid cluster");
        return clusterinf[cluster].thread; 
      }
      void setClusterThread(int cluster, int thread) {
        assert(cluster < (int)clusterinf.size() && 
	       "Assigning the thread for an invalid cluster");
        clusterinf[cluster].thread = thread;
      }

      int assignNodeToCluster(Schedulable *node, int cluster) {
        assert(cluster < (int)clusterinf.size() && 
	       "Assigning a node to an invalid thread");
	clusterinf[cluster].nodes.insert(node);
	node->setCluster(cluster);
      }

      std::vector<clusterinf_t>* getClusterVector() { return &clusterinf; }

      void reportNodeScheduled(Schedulable* node) {}

      /**
       * returns a reference to the task order.  The task order is 2D vector.
       * The first index is the thread, the second index is the order
       * in which Schedulables are to be run.  EX: to get the first 
       * Schedulable to be run in the second thread getScheduleThreads()[2][1];
       */
      TaskOrder& getTaskOrder() { return taskOrder; }
  };
}
#endif
