#ifndef RACH_H
#define RACH_H

#include "Reducer.h"
#include "Policy.h"
#include "LSEtasks.h"
#include "Schedulable.h"

#include <set>
#include <map>
#include <vector>
#include <list>

namespace LSE_scheduler {
  class RACH : public Reducer {
    private:
      struct length_t {
        schedtime_t execution;
        schedtime_t communication;
      };

      struct cluster_position {
        schedtime_t start;
        schedtime_t finish;
      };

      class cluster_t;
      class nodeInfo_t {
        public:
          schedtime_t t_level;
          schedtime_t execution;
          schedtime_t communication;

          schedtime_t start;
          schedtime_t finish;

          nodeInfo_t();

          void setTLevel(schedtime_t tlevel);

          schedtime_t getTLevel();
      };
      std::map<Schedulable*, nodeInfo_t> nodeInfo;

      class cluster_t {
        public:
          std::set<Schedulable*> members;
          std::vector<Schedulable*> order;
          std::map<cluster_t*, schedtime_t> predComms;
          std::map<cluster_t*, schedtime_t> succComms;
          schedtime_t execution;
          schedtime_t communication;
          int thread;
          std::set<cluster_t*> predecessorClusters;
          std::set<cluster_t*> successorClusters;
          int name;

          //Constructor
          cluster_t();

          //Add a task
          bool addTask(Schedulable* task, std::map<Schedulable*, cluster_t*>& task2cluster);

          //Delete a task
          bool removeTask(Schedulable* task, std::map<Schedulable*, cluster_t*>& task2cluster);

          //Sums and categorizes the total communication costs
          void analyzeCommunications(Policy* policy, std::vector<cluster_t*>& clustersVector, std::map<Schedulable*, cluster_t*>& task2cluster);

          //Adds clusters which are predecessors of this cluster
          void addPredecessor(cluster_t* predCluster);

          static void addPredecessor(std::set<cluster_t*>* toCluster, cluster_t* predCluster);

          //Removes cluster as a predecessor of this cluster
          bool removePredecessor(cluster_t* predCluster);

          //Indicates whether predCluster is a predecessor of this cluster
          bool hasAsPredecessor(cluster_t* predCluster);

          //Splits the cluster at currNode, placing currNode in the new cluster
          cluster_t* splitCluster(Schedulable* currNode, std::map<Schedulable*, nodeInfo_t>& nodeInfo, std::list<cluster_t>& clusters, std::vector<cluster_t*>& clustersVector, std::map<Schedulable*, cluster_t*>& task2cluster, SchedulableDAG* graph);

          //Report communications with predecessor clusters
          schedtime_t predComm(cluster_t* predGroup);

          //Report communications with successor groups
          schedtime_t succComm(cluster_t* succGroup);

          //Returns the execution cost
          schedtime_t getExecution();

          //Returns the total naive communication cost
          schedtime_t getCommunication();

          //Iterator over the members
          std::set<Schedulable*>::iterator begin();

          //End iterator
          std::set<Schedulable*>::iterator end();

          //Iterator over predecessors
          std::map<cluster_t*, schedtime_t>::iterator pred_begin();

          //End iterator over predecessors
          std::map<cluster_t*, schedtime_t>::iterator pred_end();

          //Iterator over successors
          std::map<cluster_t*, schedtime_t>::iterator succ_begin();

          //End iterator over successors
          std::map<cluster_t*, schedtime_t>::iterator succ_end();
      };
      std::list<cluster_t> clusters;
      std::vector<cluster_t*> clustersVector;
      std::map<Schedulable*, cluster_t*> task2cluster;

      struct DFS_data {
        public:
          char visited;
          cluster_t* parent;
          DFS_data() :
            visited(0),
            parent(NULL) {}
      };

      class thread_t {
        public:
          std::set<cluster_t*> members;
          schedtime_t communication;
          schedtime_t execution;
          schedtime_t start;
          schedtime_t finish;

          thread_t();

          length_t getCosts();

          schedtime_t getExecution();

          schedtime_t getCommunication();

          bool addCluster(cluster_t* cluster, std::map<cluster_t*, thread_t*>& cluster2thread);

          bool removeCluster(cluster_t* cluster, std::map<cluster_t*, thread_t*>& cluster2thread);

          length_t speculateAddCluster(cluster_t* cluster, std::map<cluster_t*, thread_t*>& cluster2thread);

          length_t speculateRemoveCluster(cluster_t* cluster, std::map<cluster_t*, thread_t*>& cluster2thread);

          std::set<cluster_t*>::iterator begin();

          std::set<cluster_t*>::iterator end();

          std::set<cluster_t*>::reverse_iterator rbegin();

          std::set<cluster_t*>::reverse_iterator rend();
      };
      std::vector<thread_t> threads;
      std::map<cluster_t*, thread_t*> cluster2thread;

      struct threadData {
        Policy* policy;
        const std::map<Schedulable*, nodeInfo_t>* nodeInfo;
        const std::map<Schedulable*, cluster_t*>* task2cluster;
        const std::map<cluster_t*, thread_t*>* cluster2thread;
        int threadNumber;
        thread_t* myThread;
        cluster_t** currCluster;
        pthread_barrier_t* barrierBegin;
        pthread_barrier_t* barrierEnd;
        volatile bool* done;
        length_t* primary;
        cluster_position* result;
        std::map<Schedulable*, nodeInfo_t>* localNodeInfo;
      };

      SchedulableDAG* graph;
      size_t numThreads;
      FILE* logfile;
      schedtime_t singleThreadMakespan;
      schedtime_t multiThreadMakespan;
      int winningThreadCount;

      void doRACH();

      void prepareGraphStructures();

      void formClusters();

      void finalizeClusters();

      bool noCycles();

      std::set< std::vector<RACH::cluster_t*> > discoverCycles();

      void breakCycles(std::set< std::vector<RACH::cluster_t*> >& cycles);

      void breakClusters();

      bool splitClusters();

      int runPass2(int numThreads);

      static void computeThreadImpact(cluster_t* clusterOfInterest, thread_t* myThread, int threadNum, const std::map<Schedulable*, nodeInfo_t>& nodeInfo, const std::map<Schedulable*, cluster_t*>& task2cluster, const std::map<cluster_t*, thread_t*>& cluster2thread, Policy* policy, cluster_position* result, std::map<Schedulable*, nodeInfo_t>& localNodeInfo);

      static void* evaluateThread(void* args);

      void reportThreads();

    public:
      //Constructor
      RACH();

      //Makes the whole thing go
      void run(SchedulableDAG& graph, int numThreads, FILE* logfile);
  };
}

#endif

