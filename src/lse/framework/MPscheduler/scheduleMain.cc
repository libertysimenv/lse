/* 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Top-level MP scheduling for the simulator.
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * The main loop of the simulator
 *
 * TODO:  
 *
 */

#include "Scheduler.h"
#include "LSEtasks.h"
#include "LSEPostScheduler.h"
#include "DSC.h"
#include "CHASM.h"
#include "RACH.h"
#include "LoadBalanceReducer.h"
#include "CommunicationMinimizationReducer.h"
#include <iterator>
#include <map>
#include <queue>
#include <set>

using namespace LSE_scheduler;

namespace {

  template <class T> class ready_elem_t {
  public:
    T prio;              // Python's criticalDistance
    Schedulable * task;
    ready_elem_t(T p, Schedulable* s) : prio(p), task(s) { }
    
    // NOTE: the strange reversal of the comparison for the idno is because
    // we were trying to match the original Python code, for which the
    // priority queue pulls off the lowest priority item
    bool operator<(const ready_elem_t &t2) const {
      return ((prio < t2.prio));
    }
  };
  
  class DummyPolicy : public Policy {
    schedtime_t nodeCost(Schedulable *node) { return 1; }
    schedtime_t naiveCommCost(const Schedulable *src, 
			      const Schedulable *dst) { return 0; }
    schedtime_t actualCommCost(Schedulable *src, 
			       Schedulable *dst) { return 0; }
    void CalcInitialPriorities(SchedulableDAG& dag) {}
    int addReady(Schedulable* ready, int threadno) { return 0; }
    size_t readyListSize() { return 0; }
    Schedulable* readyListTop() { return 0; }
    void readyListPop()  {}
    void NewReadyListCallback() {}
    bool validThread(Schedulable* item, int candidateThread, 
		     SchedulableDAG* graph) { return true; }
    bool betterIpoint(const ipoint_t &n, const ipoint_t &o) { return true; }
    bool allowThreadHoleFilling() { return false; }
  };

  struct distIDpair {
    uint64_t dist;
    int ID;
    distIDpair(uint64_t d, int i) : dist(d), ID(i) {}
    bool operator<(const distIDpair &p2) const {
      return (dist < p2.dist || dist == p2.dist && ID > p2.ID);
    }
  };

  class LSEListPolicy : public DummyPolicy {
  private:
    typedef ready_elem_t<distIDpair> ready_t;
    typedef std::priority_queue<ready_t> readyList_t;
    readyList_t readyList;
    bool fixzero;
    threadingParms P;

  public:
    LSEListPolicy(bool fz, threadingParms &iP) : fixzero(fz), P(iP) {}

    schedtime_t nodeCost(Schedulable *node) { 
      switch (P.blockCostModel) {
      case threadingParms::BlockUniform: return P.blockCost;
      case threadingParms::BlockMeasure: return node->getCost();
      default: break;
      }
      assert (0 && "Invalid node cost model\n");
    }

    schedtime_t naiveCommCost(const Schedulable *src, 
			      const Schedulable *dst) { 

      if (src->getCluster() == dst->getCluster() && src->getCluster()>=0)
	return 0;
      if (P.commCostModel == threadingParms::CommUniform) 
	return P.commCost;
      else 
	// Note: Python code had the cost just as the edge weight for
	// phase end.  But the edge weight ought to be zero anyway!
	return P.commCost * 
	  graph->getEdgeWeight(const_cast<Schedulable *>(src),
			       const_cast<Schedulable *>(dst));
    }

    schedtime_t actualCommCost(const Schedulable *src, 
			       const Schedulable *dst) { 

      if (src->getThread() >= 0 && src->getThread() == dst->getThread())
	return 0;
      return naiveCommCost(src,dst);
    }
    
    bool validThread(Schedulable* item, int candidateThread, 
		     SchedulableDAG* graph) {
      schednode_t &n= *(schednode_t *)(item);
      if (n.getThread() >= 0) return candidateThread == n.getThread();
      if (n.getCluster() >= 0) {
	int ct = graph->getClusterThread(n.getCluster());
	if (ct >= 0) return candidateThread == ct;
      }
      return (!n.validThreads.size() || 
	      n.validThreads.count(candidateThread) != 0);
    }

    bool allowThreadHoleFilling() { return true; }

    bool betterIpoint(const ipoint_t &newer, const ipoint_t &older) { 
      // TODO: (close, 0, conflicts, loc, 0, pth, loc)
      schedtime_t newend = newer.start + newer.length;
      schedtime_t oldend = older.start + older.length;
      return (older.thread == -1 ||
	      newer.conflicts < older.conflicts ||
	      newer.conflicts == older.conflicts &&
	      (newend < oldend || 
	       newend == oldend && newer.thread < older.thread));
    }

    void CalcInitialPriorities(SchedulableDAG& dag) {
      TaskGraph &tg = *static_cast<TaskGraph *>(&dag);
      for (int i = tg.size() - 1 ; i >= 0 ; --i) {
	schednode_t &n= *(schednode_t *)tg.getTask(i);
	n.critDistance = 1/*nodeCost(&n)*/;
      }
      for (int i = tg.size() - 1 ; i >= 0 ; --i) {
	schednode_t &n= *(schednode_t *)tg.getTask(i);
	uint64_t mcd = n.critDistance;
	for (schedulableIterator j = n.pred_begin(),
	       je = n.pred_end(); j != je ; ++j) {
	  schednode_t &bl = *(schednode_t *)(*j);
	  bl.critDistance = std::max(bl.critDistance, mcd + 1/*nodeCost(&n)*/);
	}
      }

      if (fixzero) {
	schednode_t &n0 = *(schednode_t *)tg.getTask(0);
	n0.critDistance = 1;
	for (int i = tg.size() - 1 ; i >= 1 ; --i) {
	  schednode_t &n= *(schednode_t *)tg.getTask(i);
	  n0.critDistance = std::max(n0.critDistance, n.critDistance+1);
	}
      }
    } // CalcInitialPriorities

    int addReady(Schedulable* ready, int threadno)
    {
      readyList.push(ready_t(distIDpair(static_cast<schednode_t *>
					(ready)->critDistance, ready->getID()), 
			     ready));
      return NO_LOCALITY;
    } 
    
    size_t readyListSize()
    {
      return readyList.size();
    }
    
    Schedulable* readyListTop()
    {
      return readyList.top().task;
    }

    void readyListPop()
    {
      readyList.pop();
    }

    virtual void setGraph(SchedulableDAG* graph) { 
      DummyPolicy::setGraph(graph);
    }

  };

  /************************** Clusterers *****************************/

  class EmptyClusterer : public Clusterer {
  public:
    void run(SchedulableDAG& graph, FILE* logfile) {
      TaskGraph &tg = *static_cast<TaskGraph *>(&graph);

      graph.setClusterCount(graph.size());
      for (int i = 0, ie = tg.size(); i != ie ; ++i) {
	schednode_t &n= *(schednode_t *)tg.getTask(i);
	graph.assignNodeToCluster(&n, n.getID());
      }
    } // run
  };


  class InstClusterer : public Clusterer {
  public:
    void run(SchedulableDAG& graph, FILE* logfile) {
      TaskGraph &tg = *static_cast<TaskGraph *>(&graph);

      int maxno = 0;
      graph.setClusterCount(graph.size()+1);
      for (int i = 0, ie = tg.size(); i != ie ; ++i) {
	schednode_t &n= *(schednode_t *)tg.getTask(i);
	graph.assignNodeToCluster(&n, n.db_InstNo+1);
	maxno = std::max(maxno, n.db_InstNo+1);
      }
      graph.setClusterCount(maxno+1);
    } // run
  };

  class ForestClusterer : public Clusterer {
  public:
    void run(SchedulableDAG& graph, FILE* logfile) {
      TaskGraph &tg = *static_cast<TaskGraph *>(&graph);

      bool visited[tg.size()+1];
      for (int i = 0; i < tg.size(); ++i) visited[i] = false;

      std::deque<schednode_t *> SQ;

      int maxno = 0;
      graph.setClusterCount(graph.size()+1);

      // breadth-first search of the graph to form clusters

      for (int i = 0, ie = tg.size(); i != ie ; ++i) {
	schednode_t &n= *(schednode_t *)tg.getTask(i);

	if (visited[n.singleOrder]) continue;

	SQ.push_back(&n);
	visited[n.singleOrder] = true;

	while (SQ.size()) {
	  schednode_t &nn = *SQ.front();
	  SQ.pop_front();
	  graph.assignNodeToCluster(&nn, maxno);
	  
	  if (logfile) 
	    fprintf(logfile,"Assigning %s to cluster %d\n", 
		    nn.getName().c_str(), maxno);

	  for (schedulableIterator j = nn.pred_begin(), je = nn.pred_end(); 
	       j !=je; ++j) {
	    schednode_t &nnn = *(schednode_t *)*j;
	    if (visited[nnn.singleOrder]) continue;
	    if (!tg.getEdgeWeight2(&nnn, &nn)) continue;
	    if (logfile) 
	      fprintf(logfile,"\tAdding %s\n", nnn.getName().c_str());
	    SQ.push_back(&nnn);
	    visited[nnn.singleOrder] = true;
	  }
	  for (schedulableIterator j = nn.succ_begin(), je = nn.succ_end(); 
	       j !=je; ++j) {
	    schednode_t &nnn = *(schednode_t *)*j;
	    if (visited[nnn.singleOrder]) continue;
	    if (!tg.getEdgeWeight2(&nn, &nnn)) continue;
	    if (logfile) 
	      fprintf(logfile,"\tAdding %s\n", nnn.getName().c_str());
	    SQ.push_back(&nnn);
	    visited[nnn.singleOrder] = true;
	  }
	}
	++maxno;
      }

      graph.setClusterCount(maxno+1);
    } // run
  };

  /************************* Reducers ****************************/

  class EmptyReducer : public Reducer {
  public:
    void run(SchedulableDAG& graph, int numThread, FILE* logfile) {}
  };

  class ClusterSizeBalancingReducer : public Reducer {

    struct clusterinfo {
      int size, id;
      bool operator<(const clusterinfo &x) const {
	return size > x.size || size==x.size && id > x.id;
      }
    };

    struct threadinfo {
      int load, id;
      bool operator< (const threadinfo &x) const {
	return load < x.load || load==x.load && id < x.id;
      }
    };

    threadingParms P;

  public:
    ClusterSizeBalancingReducer(threadingParms &ip) : P(ip) {}

    void run(SchedulableDAG& graph, int numThread, FILE* logfile) {
      std::vector<clusterinfo> sortedClusters;
      threadinfo sortedThreads[numThread];

      SchedulableDAG::clusters_t &cv = *graph.getClusterVector();

      // First, sort the clusters in descending order of (size,id)

      for (int i = 1, ie = cv.size(); i != ie; ++i) {
	if (!cv[i].nodes.size()) continue;
	schedtime_t size = 0;
	for (std::set<Schedulable *>::iterator j = cv[i].nodes.begin(),
	       je = cv[i].nodes.end(); j != je; ++j) {
	  switch (P.blockCostModel) {
	  case threadingParms::BlockUniform: size += P.blockCost; break;
	  case threadingParms::BlockMeasure: size += (*j)->getCost();
	  default: break;
	  }
	}
	clusterinfo c = { size, i };
	sortedClusters.push_back(c);
      }
      std::sort(sortedClusters.begin(), sortedClusters.end());

      // Next, create initial thread loading

      for (int i = 0; i != numThread; ++i) {
	sortedThreads[i].load = 0;
	sortedThreads[i].id = i;
      }

      for (std::vector<clusterinfo>::const_iterator i = sortedClusters.begin(),
	     ie = sortedClusters.end(); i != ie ; ++i) {
	if (logfile)
	  fprintf(logfile,
		  "Assigning cluster %d size %d to thread %d with size %d\n", 
		  i->id, i->size, sortedThreads[0].id, 
		  sortedThreads[0].load);
	graph.setClusterThread(i->id, sortedThreads[0].id);
	sortedThreads[0].load += i->size;
	std::sort(&sortedThreads[0], &sortedThreads[numThread]);
      }
    }
  };

  class InstanceReducer : public Reducer {

    struct clusterinfo {
      int size, id;
      bool operator<(const clusterinfo &x) const {
	return size > x.size || size==x.size && id > x.id;
      }
    };

    struct threadinfo {
      int load, id;
      bool operator< (const threadinfo &x) const {
	return load < x.load || load==x.load && id < x.id;
      }
    };

    threadingParms P;

  public:

    InstanceReducer(threadingParms &ip) : P(ip) {}

    void run(SchedulableDAG& graph, int numThread, FILE* logfile) {
      std::vector<clusterinfo> sortedClusters;
      threadinfo sortedThreads[numThread];

      if (logfile)
	fprintf(logfile,"=============== Start instance reducer ========\n");

      SchedulableDAG::clusters_t &cv = *graph.getClusterVector();

      // First, sort the clusters in descending order of (size,id)

      for (int i = 1, ie = cv.size(); i != ie; ++i) {
	if (!cv[i].nodes.size()) continue;
	schedtime_t size = 0;
	for (std::set<Schedulable *>::iterator j = cv[i].nodes.begin(),
	       je = cv[i].nodes.end(); j != je; ++j) {
	  switch (P.blockCostModel) {
	  case threadingParms::BlockUniform: size += P.blockCost; break;
	  case threadingParms::BlockMeasure: size += (*j)->getCost();
	  default: break;
	  }
	}
	clusterinfo c = { size, i };
	sortedClusters.push_back(c);
      }
      std::sort(sortedClusters.begin(), sortedClusters.end());

      // Next, create initial thread loading

      for (int i = 0; i != numThread; ++i) {
	sortedThreads[i].load = 0;
	sortedThreads[i].id = i;
      }
      sortedThreads[0].load = 1;  // dynamic section at start...TODO: other dyn
      std::sort(&sortedThreads[0], &sortedThreads[numThread]);

      std::map<int, std::set<int> > inst2Threads;

      for (std::vector<clusterinfo>::const_iterator i = sortedClusters.begin(),
	     ie = sortedClusters.end(); i != ie ; ++i) {

	std::set<int> usedThreads;

	for (std::set<Schedulable *>::iterator k =
	       cv[i->id].nodes.begin(), ke = cv[i->id].nodes.end() ;
	     k != ke ; ++k) {
	  schednode_t &n= *(schednode_t *)*k;
	  if (n.db_InstNo <= 0) continue;
	  std::copy(inst2Threads[n.db_InstNo].begin(),
		    inst2Threads[n.db_InstNo].end(),
		    std::inserter(usedThreads,usedThreads.begin()));
	  //fprintf(logfile,"Copying %d size %d\n", n.db_InstNo,
	  //	  inst2Threads[n.db_InstNo].size());
	}

	//fprintf(logfile,"Used threads size %d\n", usedThreads.size());

	int j;
	for (j = 0; j < numThread; ++j) {
	  if (usedThreads.count(sortedThreads[j].id)) break;
	}
	if (j==numThread) j = 0;
	if (logfile)
	  fprintf(logfile,
		  "Assigning cluster %d size %d to thread %d with size %d\n", 
		  i->id+numThread-1, i->size, sortedThreads[j].id, 
		  sortedThreads[j].load);
	graph.setClusterThread(i->id, sortedThreads[j].id);
	sortedThreads[j].load += i->size;

	for (std::set<Schedulable *>::iterator k =
	       cv[i->id].nodes.begin(), ke = cv[i->id].nodes.end() ;
	     k != ke ; ++k) {
	  schednode_t &n= *(schednode_t *)*k;
	  if (n.db_InstNo <= 0) continue;
	  inst2Threads[n.db_InstNo].insert(sortedThreads[j].id);
	}

	std::sort(&sortedThreads[0], &sortedThreads[numThread]);

      }
    }
  };

  class PhaseEndReducer : public Reducer {
    TaskGraph *phasegraph;
  public:
    PhaseEndReducer(TaskGraph &pg) : phasegraph(&pg) {}

    void run(SchedulableDAG& graph, int numThread, FILE* logfile) {
      TaskGraph &tg = *static_cast<TaskGraph *>(&graph);
      std::map<int, std::set<int> > inst2thread;

      for (schedulableIterator i=phasegraph->begin(), 
	     ie = phasegraph->end(); i != ie ; ++i) {
	schednode_t &n= *(schednode_t *)*i;
	if (n.db_InstNo >= 0 && n.getThread() >= 0) {
	  inst2thread[n.db_InstNo].insert(n.getThread());
	}
      }

      for (schedulableIterator i=graph.begin(), 
	     ie = graph.end(); i != ie ; ++i) {
	schednode_t &n= *(schednode_t *)*i;
	if (n.getThread() >= 0 || n.db_InstNo < 0) continue;
	n.validThreads = inst2thread[n.db_InstNo];
      }
    } // run
  };

}

void LSEfw_perform_scheduling(TaskGraph &phase, TaskGraph &phase_end,
			      threadingParms &P) {

  Clusterer *PC;
  Reducer *PR;

  if (P.clusterHandling == threadingParms::inst) PC = new InstClusterer();
  else if (P.clusterHandling == threadingParms::DSC) PC = new DSC();
  else if (P.clusterHandling == threadingParms::CHASM) PC = new CHASM();
  else if (P.clusterHandling == threadingParms::forest) 
    PC = new ForestClusterer();
  else PC = new EmptyClusterer();

  if (P.reductionHandling == threadingParms::balanceR)
    PR = new ClusterSizeBalancingReducer(P);
  else if (P.reductionHandling == threadingParms::RACH)
    PR = new RACH();
  else if (P.reductionHandling == threadingParms::LoadBalanceReducer)
    PR = new LoadBalanceReducer();
  else if (P.reductionHandling == threadingParms::CommunicationMinimizationReducer)
    PR = new CommunicationMinimizationReducer();
  else if (P.reductionHandling == threadingParms::instR)
    PR = new InstanceReducer(P);
  else PR = new EmptyReducer();

  LSEListPolicy PD(true, P);
  LSEPostScheduler PP(P);
  Scheduler PS(&PD, PC, PR, &PP);
  PS.run(phase, P.numThreads, "phase.log");
  delete PC;
  delete PR;

  EmptyClusterer PEC;
  PhaseEndReducer PER(phase);
  LSEListPolicy PED(false, P);
  LSEPostScheduler PEP(P);
  Scheduler PES(&PED, &PEC, &PER, &PEP);
  PES.run(phase_end, P.numThreads, "end.log");

}
