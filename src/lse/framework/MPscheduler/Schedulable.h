/* 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Packaging of parallelism for the LSE environment
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * This file contains the header for code which schedules tasks onto threads.
 *
 */
#ifndef _SCHEDULABLE_H
#define _SCHEDULABLE_H

#include <set>
#include <vector>
#include <string>
#include <algorithm>

/**
 * This file defines the Schedulable class.  This class is the base class for 
 * any item that can be scheduled.  The scheduelers operate entirely on
 * schedulables.  This class guarentees a sucessor/predecessors style
 * interface, and contains members that the schedulers will use/populate.
 */

namespace LSE_scheduler {

  class Schedulable;

  typedef int64_t schedtime_t;
  typedef std::vector<Schedulable*>::iterator schedulableIterator;
  typedef void (*std_hookup)(void * srcData, void * destData);

  /**
   * Anything that is to be run through the scheduler
   * must inherit from the Schedulable class.
   */
  class Schedulable {
    public:
      typedef int semaphore_id;



    private:
      unsigned int ID;
      int cluster;
      int thread;

      std::vector<Schedulable*> predecessors;
      std::vector<Schedulable*> successors;
      std::vector<Schedulable*> siblings;
      std::set<int> elocks;

      schedtime_t cost;
      int priority;

      schedtime_t tlevel;
      schedtime_t blevel;
      schedtime_t start;
      schedtime_t finish;

      static unsigned int IDcounter; //Static counter used to uniquly identify Schedulables

      //These semaphores must be recieved before entering this item
      std::vector<semaphore_id> sems_to_receive;
      //These semaphores must be sent before exiting this item
      std::vector<semaphore_id> sems_to_send;

    protected:
      Schedulable():cluster(-1), thread(-1), cost(1), start(-1),finish(-1) {
	ID = IDcounter++;
      }
      Schedulable(unsigned int id):cluster(-1), thread(-1), cost(1), start(-1),
	finish(-1) {ID = id;}

      void addPredecessor(Schedulable* pred) {predecessors.push_back(pred);}
      void addSuccessor(Schedulable* succ) {successors.push_back(succ);}
      void addLocalitySibling(Schedulable* sibling) {siblings.push_back(sibling);}

    public:
      virtual ~Schedulable() {};

      /* Access methods */
      unsigned int getID() {return ID;}

      void setCluster(int c) {cluster = c;}
      int getCluster() const {return cluster;}

      void setThread(int t) {thread = t;}
      int getThread() const {return thread;}

      void setCost(schedtime_t c) {cost = c;}
      schedtime_t getCost() const {return cost;}

      void setPriority(int prio) {priority = prio;}
      int getPriority() {return priority;}

      void setTLevel(schedtime_t tl) {tlevel = tl;}
      schedtime_t getTLevel() {return tlevel;}
      void setBLevel(schedtime_t bl) {blevel = bl;}
      schedtime_t getBLevel() {return blevel;}
      virtual std::string getName(){ return ""; }

      void setStartTime(schedtime_t st) {start = st;}
      schedtime_t getStartTime() const {return start;}
      void setFinishTime(schedtime_t fi) {finish = fi;}
      schedtime_t getFinishTime() const {return finish;}

      /**
       * This is the size neccessary to embed this item.  See the allocator for more
       * info
       */
      virtual size_t getAllocSize()=0;

      /**
       * Tells the schedulable to put whatever information it will need when it runs
       * into the allocation table.  This information should be the size of 
       * getAllocSize() in bytes.
       */
      virtual void embed(void * data)=0;

      /**
       * A bit of a kluge for now, but we need a way to 
       * get hookup functions on a schedulable level
       * Gets the hookup function from this schedulable
       * to the sink schedulable
       */
      virtual std_hookup getHookup(Schedulable* sink)=0;

      std::vector<semaphore_id>& getReceiveSemaphores() { 
	return sems_to_receive; 
      }
      std::vector<semaphore_id>& getSendSemaphores() { 
	return sems_to_send; 
      }

      /* Predecessor and successor information */
      int getPredecessorCount() const { return predecessors.size(); }
      int getSuccessorCount() const { return successors.size(); }
      int getSiblingCount() const { return siblings.size(); }

      /* Iterators over predecessors and successors */
      schedulableIterator pred_begin() { return predecessors.begin(); }
      schedulableIterator pred_end() { return predecessors.end(); }
      schedulableIterator succ_begin() { return successors.begin(); }
      schedulableIterator succ_end() { return successors.end(); }

      schedulableIterator sibling_begin() { return siblings.begin(); }
      schedulableIterator sibling_end() { return siblings.end(); }

      std::vector<Schedulable*>& getSiblings() { return siblings; }

      void addLock(int c) { 
	elocks.insert(c); 
      }
      std::set<int>& getLocks(){ return elocks; }
 };
}

#endif

