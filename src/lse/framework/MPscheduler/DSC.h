#ifndef DSC_h
#define DSC_h

//#include "../sparse/taskgraph.h"
#include <map>
#include <stack>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstring>
#include <sstream>
#include <fstream>
#include "priorityq.h"
#include <stdlib.h>
//#include "llvm/Support/Debug.h"

#include "LSEtasks.h"
#include "Policy.h"
#include "Clusterer.h"

namespace LSE_scheduler {

  class DSC : public Clusterer
  {
    public:
      struct Node
      {
        int id, ClusterID;
	schedtime_t tlevel, ptlevel, blevel, cost;
        int predCount,succCount;
        bool examined, pFree, locked, movable;

        Node();
        Node(int id, schedtime_t cost);
        void printNode();
      };

    private:
      //*********************************************************
      //*      Structs used for comparisons in priorityq        *
      //*********************************************************

      struct compare_PRIO {
        DSC* dsc;

        compare_PRIO();
        bool operator()(Schedulable* a, Schedulable* b);
      };

      struct compare_costComm {
        DSC& dsc;
        const Schedulable* to;

        compare_costComm(DSC& dsc, const Schedulable* to);
        bool operator()(Schedulable* a, Schedulable* b);
      };

      struct compare_tlevel {
        DSC &dsc;

        compare_tlevel(DSC& dsc);
        bool operator()(const Node* a, const Node* b) const;
      };

      typedef PriorityQ<Schedulable*, compare_PRIO> priority_q_dsc;
      priority_q_dsc freeList;

      typedef PriorityQ<Schedulable*, compare_PRIO> pFL_t;
      pFL_t partialFreeList;

      typedef PriorityQ<Node*, compare_costComm> costpq_t;
      typedef PriorityQ<Node*, compare_tlevel> costpq2_t;

      int numToExamine;

      int locked_ny, locked_cluster;
      bool locked_is_reducible;
      schedtime_t locked_currtime;

      struct clusterInf_t {
        int size;
        int endtime;

        clusterInf_t(int s, int e);
      };

      typedef std::vector<clusterInf_t> clusters_t;
      clusters_t clusters;

      typedef std::vector<Node *> nptrlist_t;

      struct arrive_t {
        schedtime_t arrival;
        Node *node;

        arrive_t(schedtime_t a, Node *p);
      };

      struct compare_arrival {
        DSC &dsc;

        compare_arrival(DSC &dsc);
        bool operator()(const arrive_t &a, const arrive_t &b) const;
      };

      typedef PriorityQ<arrive_t, compare_arrival> arrivepq_t;

      //*********************************************************
      //*               Methods of the DSC class                *
      //*********************************************************

    private:
      SchedulableDAG* graph;
      FILE* logfile;

      bool isReducible(Schedulable* ny, bool dolocks);

      schedtime_t costOfComm(const Schedulable* from, const Schedulable* to);

      int clusterCount;

      void reassignClusters();

    public:

      /**
       * Constructor
       */
      DSC();

      void run(SchedulableDAG& graph, FILE* logfile);

      typedef std::map<Schedulable*, Node> nodes_t;
      nodes_t nodeInfo;

      void doDSC();

      bool initial();

      //*********************************************************
      //*                  Printing functions                   *
      //*********************************************************

      int numClusters();

      //bool printDSC(const char* file);

      //void fileName();

      //void printPFL();

      //void printPT();

      //void printFL(priority_q_dsc& list);

      //void printPL(costpq_t& list, Node &to);
  };

} // End namespace Packager

#endif
