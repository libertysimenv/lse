/* 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
#ifndef _POLICY_H
#define _POLICY_H

#include "Schedulable.h"
#include "SchedulableDAG.h"

#include <assert.h>
#include <iostream>

#define NO_LOCALITY 0
#define LOCALITY_USED 1

namespace LSE_scheduler {

  /**
   * A struct that represents a insertion point into a scheduler
   * thread: The thread the task should be inserted into
   * start: the time the the task should start at
   * length: the run time of the task
   */
  struct ipoint_t {
    ipoint_t(): thread(-1), start(0), length(0), conflicts(0) {}
    ipoint_t(int thread, schedtime_t start,
	     schedtime_t length): thread(thread), start(start), length(length),
      conflicts(0) {}

    int thread;
    schedtime_t start, length;
    int conflicts;
  };

  class Policy {
    protected:
      SchedulableDAG* graph;

      /**
       * Constructor
       */
      Policy():graph(NULL) {}

    public:
      /**
       * Destructor
       */
      virtual ~Policy() {}

      /**
       * Associates a particular SchedulableDAG with this policy.
       * Only used with some policies.
       */
      virtual void setGraph(SchedulableDAG* graph) { this->graph = graph; }

      /**
       * Returns the cost of executing a node
       */
      virtual schedtime_t nodeCost(Schedulable* node) = 0;

      /**
       * Returns the cost of communication between two nodes
       * assuming they are running on two different threads
       */
      virtual schedtime_t naiveCommCost(const Schedulable* src, 
					const Schedulable* dst) = 0;

      /**
       * Returns the cost of communication between two nodes
       * taking into account whether they are running on
       * different threads
       */
      virtual schedtime_t actualCommCost(Schedulable* src, Schedulable* dst) = 0;

      //*********************************************************
      //*              Priority Policy Interface                *
      //*********************************************************

      /**
       * A hook to allow priority policies to do setup work on task
       * priorities.  Used for the ready queue of a list scheduler
       */
      virtual void CalcInitialPriorities(SchedulableDAG& dag) = 0;

      /**
       * Notify the policy that the schedulable has become ready
       * typically this will insert into the ready queue
       * @ready the schedulable item that has become ready
       * @threadno the thread it is ready on.  For now, we don't really use 
       * this, we  assume it has become ready on all threads
       * @returns LOCALITY_USED or NO_LOCALITY
       */
      virtual int addReady(Schedulable* ready, int threadno)=0;

      /**
       *@returns the size of the ready List
       */
      virtual size_t readyListSize() =0;

      virtual Schedulable* readyListTop() =0;

      virtual void readyListPop() =0;

      /**
       * I Have no idea what this is used for, but it was never 
       * actually implemented
       */
      virtual void NewReadyListCallback()=0;

      //*********************************************************
      //*       Functions related to clusters and threads       *
      //*********************************************************

      virtual bool validThread(Schedulable* item, int candidateThread, 
			       SchedulableDAG* graph) =0;

      virtual bool betterIpoint(const ipoint_t &s1, const ipoint_t &s2) =0;

      virtual bool allowThreadHoleFilling() =0;

  };

}

#endif

