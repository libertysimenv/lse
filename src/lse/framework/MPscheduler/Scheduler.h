/* 
 * Copyright (c) 2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
#ifndef _SCHEDULER_H
#define _SCHEDULER_H

#include <vector>
#include <iostream>
#include <stdio.h>
#include <string.h>

#include "Schedulable.h"
#include "SchedulableDAG.h"
#include "Policy.h"
#include "Clusterer.h"
#include "Reducer.h"
#include "PostScheduler.h"


/**
 * This file defines the Scheduler class.  This class is used to wrap the different
 * elements of a scheduler.  Currently there are three phases to schedulings:
 * 1) Clustering.  clustering is like scheduling for an infinate number of 
 * processors.  
 * 2) Reduction.  Reduction is the process of assigning clusters to an actual
 * processor.  It reduces the infinate number of processors to the finate number
 * available.
 * 3) Post Scheduling.  Aftera the cluster and reducer is done, something needs 
 * to combine all of this data into an actual schedule.  This is the job of a post
 * scheduler.  The post scheduler must also discover and deal with cross thread 
 * communication
 */
namespace LSE_scheduler {
  class Scheduler {
    public:
      /**
       * Constructor
       */
      Scheduler(Policy* p, Clusterer* c, Reducer* r, PostScheduler* ps)
        : policy(p), clusterer(c), reducer(r), ps(ps) {
          clusterer->setPolicy(policy);
          reducer->setPolicy(policy);
          ps->setPolicy(policy);
        }

      /**
       * Destructor
       */
      ~Scheduler() {}

      /**
       * Schedule the schedulable objects onto the number of threads specified.
       * @param graph is the DAG to schedule
       * @param numThreads is the number of threads to schedule onto
       * @param logfile is the name of a file to dump logging output to.  
       * If null or empty, don't * dump anything.
       */
      void run(SchedulableDAG& graph, int numThreads, const char * logfile) 
      {
        FILE* log;
        if(logfile != NULL && strlen(logfile) > 0)
        {
          log = fopen(logfile,"w");
        }

        policy->setGraph(&graph);
        clusterer->run(graph, log);
        reducer->run(graph, numThreads, log);
        ps->run(graph, numThreads, log);
        if(log)
          fclose(log);
      }

    protected:
      Policy* policy;

    private:
      Clusterer* clusterer;
      Reducer* reducer;
      PostScheduler* ps;
  };
}

#endif

