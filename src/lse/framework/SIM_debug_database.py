# /* 
#  * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python database debug code
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This code simply loads the database for debugging
#  *
#  */
import sys, os, time, cStringIO
sys.path.append(os.environ["LSE"] + "/share/domains" )
sys.path.append(os.environ["LSE"] + "/share/lse/framework" )
sys.path.append(os.environ["LSE"] + "/lib" )

try:
    import readline
except ImportError:
    print "Module readline not available... tab-completion will not work"
else:
    try:
        import rlcompleter
    except ImportError:
        print "Module rlcompleter not available... "\
              "tab completion will not work"
    else:
        readline.parse_and_bind("tab: complete")
        
# put together environment as it exists for code generation
import LSE_domain
from SIM_apidefs import * # imports codegen, database, schedule, tokenizer

def load_db(fname):
  global LSE_db
  starttime=time.time()
  LSE_db=LSEdb_Design(fname)
  endtime=time.time()
  sys.stderr.write("Elapsed database load time: %f\n" % (endtime-starttime))
  LSE_db.loadDomains(globals())

load_db("SIM_schedule.dat")

### Routines to traverse the signal graph

def signal_dfs_int(sno,osno,level,db,marks,dir,dowhat):
    sig=db.signalList[sno]
    if dir:
        ltu = sig.realdepson
    else:
        ltu = sig.usedby
    for s in ltu:
        if s<0: continue
        if marks[s]<=0: es = None
        else: es = (marks[s]-marks[sno])
        dowhat(db,level,sno,s,osno,es)
        if s!=osno:
            if not marks[s]: # white
                marks[s] = marks[sno]+1 # grey
                signal_dfs_int(s,osno,level+1,db,marks,dir,dowhat)

    marks[sno]= -1 # black

def signal_bfs_int(osno,db,marks,dir,dowhat):
    sstk = [(osno,-1,1)]
    maxlev = 0
    while (len(sstk)):
	   sstki = sstk[0]
	   sno = sstki[0]
	   if sno >= 0:
               if sstki[2] > maxlev: maxlev = sstki[2]
               dowhat(db,sstki[2],sstki[1],sno,osno,None)
               if marks[sno]<2:
                  marks[sno] = 2
                  sig=db.signalList[sno]
                  if dir:
                      slist = filter(lambda x,m=marks,osno=osno:\
                                     not m[x] or x==osno,
                                     sig.realdepson)
                  else:
                      slist = filter(lambda x,m=marks,osno=osno:\
                                     not m[x] or x==osno,
                                     sig.usedby)
                  for s in slist: marks[s] = marks[s]|1
                  sstk.extend(map(lambda x,p=sno,l=sstki[2]+1:\
                                  (x,p,l),slist))

	   del sstk[0]
    return maxlev

def print_forward(db,lev,fsno,tsno,osno,es):
    if tsno==osno:
        print "%s%s(%d) => *****" % (" "*lev,
                                     db.sigName(fsno),fsno)
    else:
        print "%s%s(%d) => %s(%d) %s" % (" "*lev,db.sigName(fsno),fsno,
                                         db.sigName(tsno),tsno,es)

def print_backward(db,lev,fsno,tsno,osno,es):
    if tsno==osno:
        print "%s%s(%d) <= *****" % (" "*lev,
                                     db.sigName(fsno),fsno)
    else:
        print "%s%s(%d) <= %s(%d) %s" % (" "*lev,db.sigName(fsno),fsno,
                                         db.sigName(tsno),tsno,es)
    
def signal_reaches_to(sno,db,fname=None,depthfirst=0):
	if fname:
		f = open(fname,"w")
		sys.stdout = f
	marks = [0] * len(db.signalList)
        if depthfirst:
            signal_dfs_int(sno,sno,1,db,marks,0,print_forward)
        else:
            signal_bfs_int(sno,db,marks,0,print_forward)
	if fname:
		sys.stdout = sys.__stdout__
		f.close()

def signal_reached_by(sno,db,fname=None,depthfirst=0):
	if fname:
		f = open(fname,"w")
		sys.stdout = f
	marks = [0] * len(db.signalList)
        if depthfirst:
            marks[sno] = 1
            signal_dfs_int(sno,sno,1,db,marks,1,print_backward)
        else:
            signal_bfs_int(sno,db,marks,1,print_backward)
	if fname:
		sys.stdout = sys.__stdout__
		f.close()

def critical_paths(db,numtop,fname=None):
    
    def do_nothing(db,lev,fsno,tsno,osno,es): pass
    
    pathlens = []
    for sno in range(len(db.signalList)):
        marks = [0] * len(db.signalList)
        pathlens.append((signal_bfs_int(sno,db,marks,1,do_nothing),sno))
        if ((sno & 0x3f) == 0): sys.stderr.write("%d\n" % sno)
    pathlens.sort()
    pathlens.reverse()
    if numtop > len(db.signalList) or numtop < 0: numtop = len(db.signalList)
    if fname:
            f = open(fname,"w")
            sys.stdout = f
            
        
    for i in range(numtop):
        print "\n############ Number:%d  Signal:%d  Depth:%d #########\n" % \
              (i,pathlens[i][1],pathlens[i][0])
        marks = [0] * len(db.signalList)
        signal_bfs_int(pathlens[i][1],db,marks,1,print_forward)
    if fname:
            sys.stdout = sys.__stdout__
            f.close()

import bisect

def report_schedule_props(db):
    for i in db.block_sched_graph.values():
  	cno = i.cno
	cb = db.cblocksList[cno]
	if cb[0] == LSEdb_CblockDynamic: name = "dynamic"
	else: name = db.instances[cb[1]].module
	print "M%d,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d" % (i.id,name,
		len(db.cblocksSensInputSignals[cno]),
		len(db.cblocksUsefulInputSignals[cno]),
		len(db.cblocksPotentialInputSignals[cno]),
		len(db.cblocksPotentialOutputSignals[cno]),
		len(db.cblocksWantedOutputSignals[cno]),
		len(i.generates),
		len(i.couldGenerate), 
		len(i.final), 
		len(i.deps))

    for i in db.end_bsg.values():
  	cno = i.cno
	cb = db.cblocksList[cno]
	if cb[0] == LSEdb_CblockDynamic: name = "dynamic"
	else: name = db.instances[cb[1]].module
	print "E%d,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d" % (i.id,name,
		len(db.cblocksSensInputSignals[cno]),
		len(db.cblocksUsefulInputSignals[cno]),
		len(db.cblocksPotentialInputSignals[cno]),
		len(db.cblocksPotentialOutputSignals[cno]),
		len(db.cblocksWantedOutputSignals[cno]),
		len(i.generates),
		0, # len(i.couldGenerate), 
		len(i.final), 
		len(i.deps))

def print_task_graph(db,fname=None):
	if fname:
		f = open(fname,"w")
		sys.stdout = f
        print "digraph mine { "
        for sp in LSE_db.block_sched_list:
            cb = LSE_db.cblocksList[sp.cno]
            print 'n%d [label="%s"];' % (sp.singleOrder, cb[1])
        for sp in LSE_db.block_sched_list:
            saw={}
            for d in sp.deps:
                if saw.has_key(d): continue
                saw[d] = 1
                print "n%d -> n%d;" % (LSE_db.block_sched_graph[d].singleOrder, sp.singleOrder)
        print "}"

	if fname:
		sys.stdout = sys.__stdout__
		f.close()
