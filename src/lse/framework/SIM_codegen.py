# /* 
#  * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python code generation module
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This module holds Python utility routines to do code generation.
#  * API implementations are found in SIM_apidefs.py, which uses this module
#  *
#  */

import re, string, sys, LSE_domain, traceback, types
from SIM_tokenizer import *
from SIM_database import *

########################## Exceptions ##########################

class LSEcg_Exception(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return self.value

######################### Class for environment #################
    
class LSEcg_Environment:
    def __init__(self):
        self.db = None
        self.inst = None
        self.subobj = None
        self.codeloc = None
        self.reporter = None

    def getStandard(self):
        return (self.db, self.inst, self.subobj, self.codeloc, self.reporter)
    
########################## Constants ############################

LSEcg_in_clm = 0          # important that this be 0....
LSEcg_in_modulebody = 1
LSEcg_in_extension = 2
LSEcg_in_controlpoint = 3
LSEcg_in_userpoint = 4
LSEcg_in_funcheader = 5
LSEcg_in_datacoll = 6
LSEcg_in_domaincode = 7
LSEcg_in_text = 7     # end of text definitions
LSEcg_in_structadd = 8
LSEcg_in_def = 9
LSEcg_in_domaindef = 10
LSEcg_in_toplevel = 11 # for error reporting only; not actually set

LSEcg_locnames = [
    "in .clm files",
    "in module bodies",
    "in control functions",
    "in user functions",
    "in funcheaders",
    "in data collectors",
    "in domain code",
    "in structadds",
    "in definitions",
    "outside of module instances"
    ]

######################### New token types #######################

class LSEcg_TokenFullPortName(LSEtk_Token_compound):
    tlabel = "FULLPORTNAME"
    tid = "FullPortName"
    
class LSEcg_TokenPortName(LSEtk_Token_compound):
    tlabel = "PORTNAME"
    tid = "PortName"

####################### Utility routines ##########################
    
def LSEcg_flatten(name):
	return string.replace(name,".","__")

def LSEcg_fixQuote(val):
    s = str(val)
    if len(s) and s[0] == '"': return "const_cast<char *>(%s)" % s
    else: return s

######################## Fixup handling ###########################

_LSEcg_fixups = []

def LSEcg_register_fixup(func,tok,extraargs):
    _LSEcg_fixups.append((func,tok,extraargs))

def LSEcg_run_fixups():
    map(lambda x:x[0](x[1],x[2]),_LSEcg_fixups)
    del _LSEcg_fixups[0:len(_LSEcg_fixups)]

LSEcg_fkey_event_record = 0
LSEcg_fkey_method_call = 1
LSEcg_fkey_uv_ref = 2
LSEcg_fkey_port_query = 3
LSEcg_fkey_struct_field = 4
LSEcg_fkey_qtramp_call = 5
LSEcg_fkey_port_alias_query = 6
LSEcg_fkey_parm = 7

def LSEcg_fkey(codeloc, subinst, api):
    if codeloc == LSEcg_in_datacoll:
        return (api, codeloc,
                "%s%d" % (LSEcg_flatten(subinst[0].event.name),subinst[1]))
    elif codeloc == LSEcg_in_controlpoint or codeloc == LSEcg_in_userpoint:
        return (api, codeloc, "%s%d" % (subinst[0].name,subinst[1]))
    elif codeloc == LSEcg_in_extension:
        return (api, codeloc, "ext")
    elif codeloc == LSEcg_in_modulebody:
        return (api, codeloc, "mb")
    elif codeloc == LSEcg_in_funcheader:
        return (api, codeloc, "fh")
    return ()

_LSEcg_klocs = [ # must update when location numbers change
    "clm", "mb", "cp", "up", "fh", "coll", "dmc", "txt", "sa", "def", "tl",
    ]
_LSEcg_kapis = [ # must update when fkey numbers change
    "er", "mc", "uv", "pq", "sfd", "qt", "paq", "pm"
    ]
def LSEcg_apivar_name(key, idno, piece):
    return "LSEmi_%s%d%s_%s_%s" % (_LSEcg_klocs[key[1]], idno,
                                   _LSEcg_kapis[key[0]], piece,
                                   key[2])


############### Make values safe/pretty for C Strings  ###################

def LSEcg_print_boolean(value):
    if value:
        return "true"
    else:
        return "false"

def LSEcg_print_string(value):
    value = re.sub('\\\\','\\\\\\\\', value)
    value = re.sub('%', '%%', value)
    value = re.sub('"', '\\"', value)
    return value

def LSEcg_print_string2(value):
    return ("const_cast<char *>(%s)" % str(value))

######################## Types good for run-timing ######################

LSEcg_runtimeable_types = {
  "boolean" : ("LSEfw_scan_boolean","true|false",LSEcg_print_boolean,str),
  "int" : ("LSEfw_scan_int","<int>",str,str),
  "char *" : ("LSEfw_scan_charstar","<string>",LSEcg_print_string,
		LSEcg_print_string2),
  "float" : ("LSEfw_scan_float","<float>",str,str),
  "double" : ("LSEfw_scan_double","<double>",str,str),

  "int64_t" : ("LSEfw_scan_int64_t","<64-bit int>",str,
               lambda x : "INT64_C(%s)" % str(x)),
  "int32_t" : ("LSEfw_scan_int32_t","<32-bit int>",str,str),
  "int16_t" : ("LSEfw_scan_int16_t","<16-bit int>",str,str),
  "int8_t" : ("LSEfw_scan_int8_t",  "<8-bit int>",str,str),
  "uint64_t" : ("LSEfw_scan_uint64_t", "<64-bit uint>",str,
                lambda x : "UINT64_C(%s)" % str(x)),
  "uint32_t" : ("LSEfw_scan_uint32_t", "<32-bit uint>",str,str),
  "uint16_t" : ("LSEfw_scan_uint16_t", "<16-bit uint>",str,str),
  "uint8_t" : ("LSEfw_scan_uint8_t",  "<8-bit uint>",str,str),
  }

#########################################################################
#
#       Naming routines
#
#########################################################################

############## Generic naming #####################

def LSEcg_build_name(prefix,purpose,instance,object):
    if (purpose): s = prefix + "_" + purpose + "__"
    else: s = prefix + "__"
    if instance:
        s = s + string.replace(instance,".","__")
        if object: s = s + "___" + string.replace(object,".","__")
    else: s = s + string.replace(object,".","__")
    return s

############### .clm naming ##################

# FUNC/FUNCPTR
def LSEcg_func_name(iname,fname,isExternal):
    if isExternal:
        return LSEcg_build_name("LSEmi","",iname,string.strip(fname))
    else:
        return LSEcg_build_name("LSEmi","","",string.strip(fname))
        
# GLOB/GLOBDEF
def LSEcg_glob_name(iname,gname):
    return LSEcg_build_name("LSEmi","","",string.strip(gname))

# HANDLER
def LSEcg_handler_name(iname,hname,isExternal):
    if isExternal:
        return LSEcg_build_name("LSEmi","HANDLER",iname,string.strip(hname))
    else:
        return LSEcg_build_name("LSEmi","HANDLER","",string.strip(hname))
        
############## Shared code naming ############################

# instance data structure
def LSEcg_instdata_name(iname):
    return LSEcg_build_name("LSEmi","IDATA",iname,"")

def LSEcg_iname_transform(db, iname):
    inst = db.getInst(iname)
    return inst.multiInst.name

############## Naming of code blocks ############################
# code block identifier is:
#   (type, instance name, port name, portid)

def LSEcg_firing_name(iname,pname,isExternal):
    if isExternal:
        return LSEcg_build_name("LSEmi","FIRING",iname,pname)
    else:
        return LSEcg_build_name("LSEmi","FIRING","",pname)
        
def LSEcg_dynsect_name(kind,dno):
    return LSEcg_build_name("LSEfw","DYNSECT",kind,"%s" % dno)

_names = { LSEdb_CblockGen_Send : "send",
           LSEdb_CblockGen_Recv : "recv",
           LSEdb_CblockGen_Lock : "lock",
           LSEdb_CblockGen_Unlock : "unlock",
           }

def LSEcg_gen_name(iname, pname, dno):
    if _names.has_key(iname): return LSEcg_build_name("LSEfw_GEN",
                                                      _names[iname],"","ni")
    else: return LSEcg_build_name("LSEfw",iname,pname,"%s" % dno)
    
def LSEcg_codeblock_name(db,ct,allowdyn0, isExternal):
    if (not ct or (not allowdyn0 and
                   ct[0] == LSEdb_CblockDynamic and
                   ct[3] == 0)):
        return "LSEfw_null_firing"
    if (ct[0] == LSEdb_CblockPhase):
        inst = db.getInst(ct[1])
        return LSEcg_func_name(LSEcg_iname_transform(db,ct[1]),ct[2],
                               isExternal)
    elif (ct[0] == LSEdb_CblockPhaseStart):
        inst = db.getInst(ct[1])
        return LSEcg_func_name(LSEcg_iname_transform(db,ct[1]),
                               "LSE_start_of_timestep",
                               isExternal)
    elif (ct[0] == LSEdb_CblockPhaseEnd):
        inst = db.getInst(ct[1])
        if ct[3]:
            return LSEcg_func_name(LSEcg_iname_transform(db,ct[1]),
                                   "LSE_end_of_timestep",
                                   isExternal)
        else:
            return LSEcg_func_name(LSEcg_iname_transform(db,ct[1]),
                                   "phase_end",
                                   isExternal)
    elif (ct[0] == LSEdb_CblockFiring):
        inst = db.getInst(ct[1])
        return LSEcg_firing_name(LSEcg_iname_transform(db,ct[1]),ct[2],
                                 isExternal)
    elif (ct[0] == LSEdb_CblockHandler):
        inst = db.getInst(ct[1])
        return LSEcg_handler_name(LSEcg_iname_transform(db,ct[1]),ct[2],
                                  isExternal)
    elif (ct[0] == LSEdb_CblockGen):
        return LSEcg_gen_name(ct[1],ct[2],ct[3])
    else:
        return LSEcg_dynsect_name(ct[1],ct[3])

def LSEcg_codeblock_prototype(db,ct,isExternal,classname=""):
    if isExternal: extraparm = "LSEfw_stf"
    else: extraparm = "LSEfw_f"
    if classname: classname = classname +"::"
    funcname = classname + LSEcg_codeblock_name(db,ct,1,isExternal)
    if not ct:
        # null code block must be treated as a firing because that
        # has the most general parameter list
        return "void " + funcname + ("(%s_def int instno)" % extraparm)
    if (ct[0]==LSEdb_CblockPhase):
	return "void " + funcname + ("(%s_def_void)" % extraparm)
    elif (ct[0]==LSEdb_CblockPhaseStart):
	return "void " + funcname + ("(%s_def_void)" % extraparm)
    elif (ct[0]==LSEdb_CblockPhaseEnd):
	return "void " + funcname + ("(%s_def_void)" % extraparm)
    elif ct[0]==LSEdb_CblockFiring:
	return "void " + funcname + ("(%s_def int instno)" % extraparm)
    elif ct[0]==LSEdb_CblockHandler:
	return "void " + funcname + ("(%s_def int instno)" % extraparm)
    elif ct[0]==LSEdb_CblockGen:
        return "void " + funcname + ("(%s_def int sno)" % extraparm)
    else: # dynamic
	return "void " + funcname + ("(%s_def_void)" % extraparm)

def LSEcg_codeblock_base(iname):
    return LSEcg_build_name("LSEmi","CBBASE","",iname)

################### Naming of code functions ######################
    
def LSEcg_codefunc_name(cp,isdefault=0,isExternal=0):
    if cp.type == LSEdb_PointUser:
        purpose = "UPOINT"
    else:
        purpose = "CPOINT"
    if isdefault: purpose = purpose+"DFLT"
    if isExternal:
        return LSEcg_build_name("LSEmi",purpose,cp.inst.multiInst.name,cp.name)
    else:
        return LSEcg_build_name("LSEmi",purpose,"",cp.name)

def LSEcg_codefunc_name_str(iname,cpname,type):
    if type == LSEdb_PointUser:
        purpose = "UPOINT"
    else:
        purpose = "CPOINT"
    return LSEcg_build_name("LSEmi",purpose,"",cpname)

def LSEcg_codefunc_prototype(cp,isdefault=0,isExternal=0):
    rt = cp.returnType
    if isExternal: base = "LSEfw_stf_def"
    else: base = "LSEfw_f_def"
    if cp.params == "" or cp.params == "void": p = base + "_void"
    else: p = base + " " + cp.params
    return rt + " " + LSEcg_codefunc_name(cp,isdefault,isExternal) \
           + "(" + p + ")"

################## Event naming ###################################

def LSEcg_event_name(ev,part,isExternal):
    if isExternal:
        return LSEcg_build_name("LSEmi","event_"+ part,
                                ev.inst.multiInst.name,ev.name)
    else:
        return LSEcg_build_name("LSEmi","event_"+ part,"",ev.name)
        
def LSEcg_event_name_str(iname,ename,part,isExternal):
    if isExternal:
        return LSEcg_build_name("LSEmi","event_"+ part,iname,ename)
    else:
        return LSEcg_build_name("LSEmi","event_"+ part,"",ename)

def LSEcg_event_prototype(ev,part,isExternal):
    if part=="record" or part=="record2":
        plist = ev.argString
        plist = string.strip(plist)
        if plist != "void" and plist != "":
            if isExternal:
                plist = "LSEfw_stf_def " + plist
            else:
                plist = "LSEfw_f_def " + plist
        else:
            if isExternal:
                plist = "LSEfw_stf_def_void"
            else:
                plist = "LSEfw_f_def_void"
    else:
        if isExternal:
            plist = "LSEfw_stf_def_void"
        else:
            plist = "LSEfw_f_def_void"
    return ("void " + LSEcg_event_name(ev,part,isExternal) +
            "(" + plist + ")")

def LSEcg_port_event_prototype(iname, pname, part, dtype, isExternal):
    name = LSEcg_event_name_str(iname, pname + "." + part,"record", isExternal)
    return """void %s(LSEfw_stf_def int instno, LSE_signal_t status,
                      LSE_signal_t prevstatus, LSE_dynid_t id, %s *datap)""" \
        % (name, dtype)

################### Method names ##################################

def LSEcg_method_name(iname, qname, isExternal):
    if isExternal:
        return LSEcg_build_name("LSEmi","",iname,qname)
    else:
        return LSEcg_build_name("LSEmi","","",qname)

def LSEcg_method_prototype(iname, qname, params,rtype,isExternal):
    if params == "" or params == "void":
        if isExternal:
            params = "LSEfw_stf_def_void"
        else:
            params = "LSEfw_f_def_void"
    else:
        if isExternal:
            params = "LSEfw_stf_def " + params
        else:
            params = "LSEfw_f_def " + params
    return rtype + " " + LSEcg_method_name(iname, qname, isExternal) + \
           "(" + params + ")"

################## Port-related names #############################

def LSEcg_port_wakeup_ref(iname,pname,instno,apiclass):
    nlist= string.split(apiclass,".")
    # want %s for instno so we convert to string
    return (LSEcg_portinst_info_name(iname,pname) + ("[%s]" % instno) +
            (".wakeups.wu[%d]" % LSEcg_wunos[tuple(nlist)]))

def LSEcg_port_alias_name(iname,pname):
    return LSEcg_build_name("LSEpi","ALIAS",iname,pname)

def LSEcg_portinst_info_name(iname,pname):
    return LSEcg_build_name("LSEpi","INFO",iname,pname)

def LSEcg_portinst_info_ref(iname,pname,instno):
    return LSEcg_portinst_info_name(iname,pname) + ("[%s]" % instno)

def LSEcg_portinst_ginfo_name(iname,pname):
    return LSEcg_build_name("LSEpi","GINFO",iname,pname)

def LSEcg_portstatus_field_name(iname, pname):
    return "%s___%s" % (LSEcg_flatten(iname),pname)

def LSEcg_portstatus_name(iname, pname, isRef=1):
    if isRef:
        return LSEcg_build_name("LSEpi","PSTAT",iname,pname)
    else:
        return "LSEfw_PORT_status.%s" % LSEcg_portstatus_field_name(iname,
                                                                    pname)
    
################### Port API prototypes ###########################

_LSEcg_portapis = {
    "GET_DATA" : ("LSE_signal_t", 1,
                  "int instno, LSE_dynid_t *id, %s **datap"),
    "GET_FLAG" : ("boolean", 0, "int instno"),
    "SET_FLAG" : ("void", 0, "int instno"),
    "SET_DATAEN" : ("void", 1, 
                    "LSEfw_f_def int instno, LSE_signal_t stat, "
                    "LSE_dynid_t id, %s *datap"),
    "SET_ACK" : ("void", 0, 
                 "LSEfw_f_def int instno, LSE_signal_t stat"),
    }

def LSEcg_portapi_name(p, apiname):
    return LSEcg_build_name("LSE_MPORT_"+apiname,"","",p.name)

def LSEcg_portapi_proto(p, apiname, classname=""):
    api = _LSEcg_portapis[apiname]
    if api[1]: parms = api[2] % p.type
    else: parms = api[2]
    if classname: classname = classname + "::"
    return "%s %s%s(%s)" % (api[0], classname,
                            LSEcg_portapi_name(p,apiname), parms)

################### Query names ###################################

def LSEcg_query_name(iname, qname, isExternal):
    if isExternal:
        return LSEcg_build_name("LSEmi","",iname,qname)
    else:
        return LSEcg_build_name("LSEmi","","",qname)
        
def LSEcg_query_prototype(iname, qname, params,rtype,isExternal):
    params = string.strip(params)
    if params != "void" and params != "":
        if isExternal:
            params = "LSEfw_stf_def " + params
        else:
            params = "LSEfw_f_def " + params
    else:
        if isExternal:
            params = "LSEfw_stf_def_void"
        else:
            params = "LSEfw_f_def_void"
    return rtype + " " + LSEcg_query_name(iname,qname,isExternal) + \
           "(" + params + ")"

def LSEcg_query_tramp_name(db, iname, qname, isExternal):
    return LSEcg_build_name("LSEmi","QTRAMP",
                            LSEcg_iname_transform(db,iname),qname)

def LSEcg_query_tramp_prototype(db, iname,qname,params,rtype,isExternal):
    if params != "void":
        return rtype + " " + LSEcg_query_tramp_name(db,iname,qname,
                                                    isExternal) + \
               "(LSEfw_stf_def int LSE_point, " + \
               "unsigned int LSEfw_calling_id, " + \
               "int LSEfw_instno, " + params + ")"
    else:
        return rtype + " " + LSEcg_query_tramp_name(db, iname, qname,
                                                    isExternal)+\
               "(LSEfw_stf_def int LSE_point, " + \
               "unsigned int LSEfw_calling_id, " + \
               "int LSEfw_instno)"

################### Structadd names ###################################

def LSEcg_structadd_offset_name(iname, sname):
    return LSEcg_build_name("LSEmi","SOFFSET",iname,sname)

def LSEcg_structadd_type_name(iname, sname):
    return LSEcg_build_name("LSEmi","STYPE",iname,sname)

#######################################################################
#
#  Domain identifier resolution and searchpaths....
#
#######################################################################

_LSEcg_domain_searchpath=[]

#
# create a domain identifier name
#
def LSEcg_domain_namespace_mangler(name,isclass):
    if (isclass): return name # will assume unique class names out there
    else: return name # "LSEdi_" + name

def LSEcg_domain_mangler(name,name2,idtype, isdef=0):

    # prevent re-mangling
    if name[:7] == "LSEdi__" or name[:7] == "LSEdc__" or name[:7] == "LSEdm__":
        try: raise
        except:
            sys.stderr.write("Re-entrant name %s\n" % name)
            traceback.print_stack(file=sys.stderr)
        loc = string.find(name,"(")
        if loc >= 0: return name[:loc]
        else: return name

    if (idtype & (LSE_domain.LSE_domainID_m4macro |
                  LSE_domain.LSE_domainID_tokmacro |
                  LSE_domain.LSE_domainID_cmacro)):

        if (idtype & LSE_domain.LSE_domainID_inst):  # domain instance mangling
            return "LSEdi__" + name2[1] + "__" + name
        elif (idtype & LSE_domain.LSE_domainID_impl):  # domain impl. mangling
            return "LSEdm__" + name2[1] + "__" + name
        else:
            return "LSEdc__" + name2[0] + "__" + name
    else:
        if (isdef):
            if (idtype & LSE_domain.LSE_domainID_inst):
                return name
            else:
                return name
        # NOTE: the leading space is necessary to prevent C lexer ugliness
        # where <: is interpreted as [
        if (idtype & LSE_domain.LSE_domainID_inst):  # domain instance mangling
            return " ::" + LSEcg_domain_namespace_mangler(name2[1],0) + \
                   "::" + name
        elif (idtype & LSE_domain.LSE_domainID_impl): # domain impl mangling
            return " ::" + LSEcg_domain_namespace_mangler(name2[1],2) + \
                   "::" + name
        else:
            return " ::" + LSEcg_domain_namespace_mangler(name2[0],1) + \
                   "::" + name
                            
#
# find domain information based upon the name
#
#def LSEcg_get_domain_from_name(db, mname):
#    try:
#        if mname[:7] == "LSEdi__":
#            loc = string.find(mname[7:],"__")
#            if loc >= 0:
#                din = mname[7:loc+7]
#                di = db.domainInstances[din]
#                dc = db.domains[di.className][0]
#                return (dc,di)
#            else: return (None,None)
#        if mname[:7] == "LSEdc__":
#            loc = string.find(mname[7:],"__")
#            if loc >= 0:
#                din = mname[7:loc+7]
#                dc = db.domains[din][0]
#                return (dc,None)
#            else: return (None,None)
#        return (None,None)
#    except:
#        return (None,None)
    

#
# set the domain search path
#
def LSEcg_domain_set_rc(db, sp):
    global _LSEcg_domain_searchpath
    _LSEcg_domain_searchpath = map(lambda x,db=db:\
                                   (x[0],db.domainInstances.get(x[1])),sp)

#
# set the domain search path to that of a module instance
#
def LSEcg_domain_set_inst_rc(db, inst, doboth):
    global _LSEcg_domain_searchpath
    if inst is None: inst = db
    if doboth:
        _LSEcg_domain_searchpath = map(lambda x,db=db:\
                                       (x[0],db.domainInstances.get(x[1])),
                                       inst.domainSearchPath[0] +
                                       inst.domainSearchPath[1])
    else:
        _LSEcg_domain_searchpath = map(lambda x,db=db:\
                                       (x[0],db.domainInstances.get(x[1])),
                                       inst.domainSearchPath[0])

#
# Resolve domains for a domain ID
# called with normal tokenizer macro arguments...
#
def LSEcg_domain_resolve(tinfo,args,id,env,retinfo=None):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    global _LSEcg_domain_searchpath

    parent = tinfo[3][-1]
    if (type(parent.contents) == types.ListType and len(parent.contents)>0):
        #sys.stderr.write("%s\n" % repr(parent.contents))

        # look for the last two "other" tokens; any thing else should
        # stop us cold in the search
        i = 1
        prev = ""

        while (i <= len(parent.contents)):
            #sys.stderr.write("%s\n" % parent.contents[-i].__class__)
            if parent.contents[-i].__class__ is LSEtk_TokenEmpty:
                i = i+1
            elif parent.contents[-i].__class__ is LSEtk_TokenOther:
                prev = parent.contents[-i].canonicalStr() + prev
                i = i + 1
                break
            else: break
        while (i <= len(parent.contents)):
            #sys.stderr.write("%s\n" % parent.contents[-i].__class__)
            if parent.contents[-i].__class__ is LSEtk_TokenEmpty:
                i = i+1
            elif parent.contents[-i].__class__ is LSEtk_TokenOther:
                prev = parent.contents[-i].canonicalStr() + prev
                i = i + 1
                break
            else: break
            
        # shouldn't translate if subfield or other such goody
        #sys.stderr.write("'%s' %s\n" % (prev, id))
        
        if (prev[-2:] == " ." or prev == " - >" or prev == " : :"):
            return None
        
        
    if codeloc > LSEcg_in_text:
        wantidtype = LSE_domain.LSE_domainID_const | \
                     LSE_domain.LSE_domainID_type | \
                     LSE_domain.LSE_domainID_cmacro | \
                     LSE_domain.LSE_domainID_tokmacro | \
                     LSE_domain.LSE_domainID_inlinefunc
    else:
        wantidtype = LSE_domain.LSE_domainID_all

    try:
        if len(args)>1: # possibly has a domain argument....
            
            # flatten name, but make sure we haven't mangled already.
            dinstname = string.strip(str(args[1])).replace(".","__")
            if dinstname[:7]=="[LSEdi_": dinstname = "["+dinstname[7:]

            matcher = _LSEcg_dr_match2.match(dinstname)
            mustsearch = not matcher
            parmtok = LSEtk_TokenPList(
                reduce(lambda x,y:x+[LSEtk_TokenOther(","),y],
                       args[2:],[args[1]]))
            if not mustsearch:
                del parmtok.contents[0] # delete the domain instance
        else: 
            mustsearch = 1
            parmtok = None
            
        if mustsearch:
            #params = "\037" + string.join(map(lambda x:str(x),args[1:]),
            #                              "\036,\037") + "\036" 
            for (dom,dinst) in _LSEcg_domain_searchpath:
                    
                # will assume the path is good....
                if dinst == None: udinst = db.domains[dom][0]
                else: udinst = dinst

                matchtype = 0
                if (codeloc == LSEcg_in_domaincode or
                    codeloc == LSEcg_in_domaindef) and \
                    env.name[0]==dom and env.name[1] == udinst.instName:
                    if dinst==None: matchtype = 1 # class
                    else: matchtype = 2 # inst

                rval = _LSEcg_handle_domain_id(id,wantidtype,1,
                                               udinst,len(args)-1,parmtok,
                                               matchtype)
                
                # note: we are forced to do something...
                if rval is not None:
                    if retinfo is not None:
                        if dinst == None: retinfo.extend([dom,None])
                        else: retinfo.extend([dom,dinst.instName])
                    return rval

            # if not found, leave text alone...
            return None

        else:		   # domain instance specified

            # find instance name
            dinstname = string.strip(matcher.group(1))   
            dinst = db.domainInstances.get(dinstname)
            wasclass = 0
            if not dinst:
                try:
                    wasclass = 1
                    dinst = db.domains[dinstname][0]
                    dom = dinstname
                except KeyError:
                    raise LSEtk_TokenizeException("Domain instance '%s' not "
                                                  "found" % dinstname,
                                                  tinfo[0])
            else: dom = dinst.className

            # adjust parameters
            #params = "\037" + string.join(map(lambda x:str(x),args[2:]),
            #                              "\036,\037") + "\036"

            matchtype = 0
            if (codeloc == LSEcg_in_domaincode or
                codeloc == LSEcg_in_domaindef) and \
                env.name[0]==dom and env.name[1] == dinst.instName:
                if wasclass: matchtype = 1 # class
                else: matchtype = 2 # inst

            rval = _LSEcg_handle_domain_id(id,wantidtype,0,dinst,
                                           len(args)-2, parmtok, matchtype)

            if rval is None:
                if (not dinst.identifiers.get(id)):
                    raise LSEtk_TokenizeException("Identifier '%s' not found "
                                                  "in domain instance '%s'"
                                                  % (id, dinstname), tinfo[0])
                else:
                    raise LSEtk_TokenizeException("Identifier '%s' in domain "
                                                  "instance '%s' not "
                                                  "valid for domain "
                                                  "resolution code block" %
                                                  (id, dinstname), tinfo[0])

            if retinfo is not None:
                if wasclass:
                    retinfo.extend([dinstname,None])
                else:
                    retinfo.extend([dinst.className,dinst.instName])
            return rval
        
    except (LSEcg_Exception,LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,args[0],tinfo[0],rexc)
        return []

def LSEcg_domain_tok_invoke(tinfo,args,extraargs,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    (name, impl, dinfo) = extraargs
    try:
        return impl[0](tinfo,args,name,dinfo,impl[1],env)
    except (LSEcg_Exception,LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,name,tinfo[0],rexc)
        return []

#
# Resolve a token matching a domain class name
#
def LSEcg_domain_class_resolve(tinfo,args,id,env,retinfo=None):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    global _LSEcg_domain_searchpath

    parent = tinfo[3][-1]
    if (type(parent.contents) == types.ListType and len(parent.contents)>0):
        #sys.stderr.write("%s\n" % repr(parent.contents))

        # look for the last two "other" tokens; any thing else should
        # stop us cold in the search
        i = 1
        prev = ""

        while (i <= len(parent.contents)):
            #sys.stderr.write("%s\n" % parent.contents[-i].__class__)
            if parent.contents[-i].__class__ is LSEtk_TokenEmpty:
                i = i+1
            elif parent.contents[-i].__class__ is LSEtk_TokenOther:
                prev = parent.contents[-i].canonicalStr() + prev
                i = i + 1
                break
            else: break
        while (i <= len(parent.contents)):
            #sys.stderr.write("%s\n" % parent.contents[-i].__class__)
            if parent.contents[-i].__class__ is LSEtk_TokenEmpty:
                i = i+1
            elif parent.contents[-i].__class__ is LSEtk_TokenOther:
                prev = parent.contents[-i].canonicalStr() + prev
                i = i + 1
                break
            else: break
            
        # shouldn't translate if subfield or other such goody
        #sys.stderr.write("'%s' %s\n" % (prev, id))
        # NOTE: we don't look for :: here because these class names
        # are supposed to appear only at the left and a leading ::
        # is perfectly fine.
        if (prev[-2:] == " ." or prev == " - >"):
            return None
        
    try:
        for (dom,dinst) in _LSEcg_domain_searchpath:
            if dom != id: continue
            # search path defined, but default instance
            if dinst == None: udinst = db.domains[dom][0]
            else: udinst = dinst

            if ((codeloc == LSEcg_in_domaincode or 
                 codeloc == LSEcg_in_domaindef) and
                (env.name == None or 
                 env.name[0]==dom and 
                 (dinst is None and env.name[1] == None or
                  dinst is not None and env.name[1] == dinst.instName))):
                return None

            if retinfo is not None:
                if not dinst: retinfo.extend([dom,None])
                else: retinfo.extend([dom,dinst.instName])

            return [ LSEtk_TokenIdentifier(udinst.instName) ] 

        # not on searchpath; leave it alone
        return None
    except (LSEcg_Exception,LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,args[0],tinfo[0],rexc)
        return []
    
def _LSEcg_do_quote(tok):
    if (isinstance(tok,LSEtk_Token_atomic) and tok.contents == ','):
        return str(tok)
    else:
        s = str(tok)
        t = s.strip()
        if t: return "\037" + s + "\036"
        else: return s

def _LSEcg_quote_tokens(tlist):
    if tlist is None: return ""
    return ( "(" + reduce(lambda x,y:x+y,
                          map(lambda z:_LSEcg_do_quote(z),tlist.contents)) +
             ")" )

#
# actually turn the id into a string with appropriate parameter nonsense...
# the tricky part is that sometimes we want to change the id and sometimes
# we do not because of C++ namespacing rules
#
def _LSEcg_handle_domain_id(id,wantidtype,hadtosearch,dinst,numarg,parmtok,
                            matchtype=0):

    idtype = dinst.identifiers.get(id)
    if not idtype or not (idtype & wantidtype):
        return None   # ID not found

    mname = (dinst.className,dinst.instName)

    newname = LSEcg_domain_mangler(id, mname, idtype,
                                   matchtype == 1 or
                                   (matchtype == 2 and
                                    (idtype & LSE_domain.LSE_domainID_inst)))
    
    #sys.stderr.write("%s => %s %d\n" % (id, newname,
    #                                    isinstance(newname,types.StringType)))
    
    # if we did not have to search (user gave instance), just return the
    # new name, making certain no extra text was there...
    if not hadtosearch:
        if numarg > 0:
            raise LSEcg_Exception("Extra tokens in domain instance")
        if idtype & LSE_domain.LSE_domainID_m4macro:
            return newname
        elif idtype & LSE_domain.LSE_domainID_tokmacro:
            return newname
        else:
            return [ LSEtk_TokenIdentifier(newname) ]
    else: # had to search...
        # at this point, the parameter list has already gone through
        # the tokenizer and there has been expansion.  So I need to
        # quote the arguments if I'm going to expand to a new macro name,
        # which I want to do as we may wish to further execute the macro.
        if idtype & LSE_domain.LSE_domainID_m4macro:
            return newname + _LSEcg_quote_tokens(parmtok)
            #return "%s%s" % (newname, parmtok)
        elif idtype & LSE_domain.LSE_domainID_tokmacro:
            return newname + _LSEcg_quote_tokens(parmtok)
        else:
            if numarg > 0: return [ LSEtk_TokenIdentifier(newname),
                                    parmtok ]
            else:
                return [ LSEtk_TokenIdentifier(newname) ]

_LSEcg_dr_match2 = re.compile("\[\s*(.*)\s*\]\s*$",re.MULTILINE)

#########################################################################
#
#        Domain hook/generation support                                 
#
#########################################################################

def LSEcg_domain_hook_loop(hook, classCode, instCode, db):

    myenv = LSEcg_Environment()
    myenv.db = db
    myenv.inst = None
    myenv.codeloc = LSEcg_in_domaincode
    myenv.reporter = LSEcg_report_error
    myenv.name = None

    tok = LSEcg_get_domain_tokenizer(db,[{}],myenv)
    tok.startInput([],sys.stdout)
    
    # must save before tokenizing because individual prints need to
    # be complete blocks of code

    savestdout = sys.stdout

    for dclass in db.domainOrder:
        tok.pythonEnv["LSE_domain_class"] = dclass[0]
        tok.pythonEnv["LSE_domain_inst"] = None
        if classCode: 
            if hook in dclass[0].classHooks:
                sys.stdout = f = cStringIO.StringIO()
                classCode(dclass[0].className,1)
                try:
                    tok.processInput(f.getvalue(),"in domain hook loop", [],
                                     savestdout)
                except LSEtk_TokenizeException, a:
                    LSEcg_report_error(str(a))

        if instCode:
            for dinst in dclass[1]:
                tok.pythonEnv["LSE_domain_inst"] = dinst
                if hook in dinst.instHooks:
                    sys.stdout = f = cStringIO.StringIO()
                    instCode(dinst.instName,0)
                    try:
                        tok.processInput(f.getvalue(),"in domain hook loop",
                                         [], savestdout)
                    except LSEtk_TokenizeException, a:
                        LSEcg_report_error(str(a))
            
    sys.stdout = savestdout
    
#################
# LSEcg_domain_generate_ids(db, apis, myenv)
#
#    Generate a set of identifiers for all domain classes/instances
#
#    - For each class:
#      - Generates class IDs, then class extras (from macrofile),
#      - For each instance in class:
#        - Generates instance IDs, then instance extras (from macrofile)
#    parts: 0 = header types, 1 = header vars/prototypes, 2 = code, 3 = macros
#
#  When parts=3 there should be only m4 definitions in the macrofile.
#
#  The apis argument is used when creating the tokenizer to set other apis
#  (such as the standard ones) in the search path.
#
#  Basic outline of the stuff we generate:
#
#  0) In the domain header file:
#       for each domain class,
#          define its external namespaces (classNamespaces)
#              using namespace on each chained domain class
#          include its headers
#          using namespace on each external namespace
#          define its types, constants, and C macros
#          include the macrofile header section
#       for each domain instance,
#          define its external namespaces (implNamespaces)
#              using namespace on each chained domain class
#          include its headers
#          using namespace on its class namespace
#          using namespace on each external namespace
#          define its types, constants, and C macros
#          include the macrofile header section
#       for each domain class,
#          define its merged types, constants, and C macros
#
#  1) in the control.h file
#       for each domain class,
#          declare its variables as extern
#       for each domain instance,
#          declare its variables as extern
#       for each domain class,
#          declare its merged variables as extern
## 
#  2) in the code file:
#       for each domain class,
#          define its variables
#          include the macrofile code section
#       for each domain instance,
#          define its variables
#          include the macrofile code section
#       for each domain class,
#          define its merged variables
#
#  3) in the macro file:
#       for each domain class,
#          define its m4 macros
#          include the macrofile macro section
#       for each domain instance,
#          define its m4 macros
#          include the macrofile macro section
#       for each domain class,
#          define its m4 macros
#
#################
def _LSEcg_make_datatype(t,n):
    res = re.subn('\?\?',n,t)
    if (not res[1]): return t + " " + n
    else: return res[0]

def _LSEcg_makedef(d,n):
    return re.sub(r'\?\?',n,d)

def _LSEcg_start_namespace(n):
    s = string.split(n,"::")
    for i in s:
        sys.stdout.write("namespace %s { " % i)
    sys.stdout.write("\n")
    
def _LSEcg_end_namespace(n):
    s = string.split(n,"::")
    for i in range(len(s)):
        sys.stdout.write("} ")
    print " // namespace %s" % n


def LSEcg_domain_generate_ids(db,apis,myenv,piece):

    # This needs to be done up here so that the iddef routines can use it
    # as a default parameter value....

    print "#define LSE_instance_name \"<domain headers>\""
    
    tok = LSEcg_get_domain_tokenizer(db,apis,myenv)

    ################### Routines ####################################

    #
    # print out types and constants (SIM_domain_types.h)
    #
    def print_iddef_types_h(dummy,idinfo,tok=tok,myenv=myenv):
        (name,idtype,impl) = idinfo

        if impl == None: pass

        elif idtype & LSE_domain.LSE_domainID_const:
            name = LSEcg_domain_mangler(name,myenv.name,idtype,1)
            impl = _LSEcg_domain_impl_translate(tok,idinfo)
            if isinstance(idinfo[2], types.StringType):
                impl = [ "char *", impl ]
            elif isinstance(idinfo[2], types.IntType):
                impl = [ "int", impl ]
            #sys.stderr.write("%s %s\n" % (idinfo,impl))
            print ("const %s %s = %s;" % (impl[0],name,impl[1]))

        elif idtype & LSE_domain.LSE_domainID_type:
            name = LSEcg_domain_mangler(name,myenv.name,idtype,1)
            impl = _LSEcg_domain_impl_translate(tok,idinfo)
            print ("typedef %s;" % _LSEcg_make_datatype(impl, name))

        elif idtype & LSE_domain.LSE_domainID_cmacro:
            name = LSEcg_domain_mangler(name,myenv.name,idtype,1)
            impl = _LSEcg_domain_impl_translate(tok,idinfo)
            print ("#"
                    "define %s%s %s" % (name, impl[0], impl[1]))

        elif idtype & LSE_domain.LSE_domainID_inlinefunc:
            name = LSEcg_domain_mangler(name,myenv.name,idtype,1)
            impl = _LSEcg_domain_impl_translate(tok,idinfo)
            print "#ifdef __cplusplus\ninline %s\n#endif" % \
                  re.sub('\?\?',name,impl)

        return None
    
    #
    # print out variables and prototypes (SIM_control.h)
    #
    def print_iddef_control_h(dummy,idinfo,tok=tok,myenv=myenv):
        (name,idtype,impl) = idinfo
        if impl == None: pass

        elif idtype & LSE_domain.LSE_domainID_var:
            name = LSEcg_domain_mangler(name,myenv.name,idtype,1)
            impl = _LSEcg_domain_impl_translate(tok,idinfo)
            print ("extern %s;" % _LSEcg_make_datatype(impl[0], name))

        return None

    #
    # print out variables and function defs (SIM_control.c)
    #
    def print_iddef_control_c(dummy,idinfo,tok=tok,myenv=myenv):
        (name,idtype,impl) = idinfo

        if impl == None: pass

        elif idtype & LSE_domain.LSE_domainID_var:
            name = LSEcg_domain_mangler(name,myenv.name,idtype,1)
            impl = _LSEcg_domain_impl_translate(tok,idinfo)
            if impl[1]!=None:
                print "%s = %s;" % (_LSEcg_make_datatype(impl[0],name),
                                    impl[1])
            else: print "%s;" % _LSEcg_make_datatype(impl[0],name)

        return None

    #
    # print out macro definitions (into dictionary)
    #
    def print_iddef_defines(dummy,idinfo,tok=tok,myenv=myenv,db=db):
        (name,idtype,impl) = idinfo
        if (idtype & LSE_domain.LSE_domainID_m4macro):
            if (idtype & LSE_domain.LSE_domainID_inst):
                name2 = LSEcg_domain_mangler("final__" + name,myenv.name,
                                             idtype,1)
                name = LSEcg_domain_mangler(name,myenv.name,idtype,1)
                #sys.stderr.write("Defined %s in iddef_defines\n" % name)
                db.domainAPIs[name] = [ (LSEtk_m4_macro,0,
                                         "%s(%s,$@)" % (name2,myenv.name[1]),
                                         None) ]
                #sys.stderr.write("\t%s\n" % db.domainAPIs[name])
                if impl != None:
                    #sys.stderr.write("Defined %s in iddef_defines\n" % name2)
                    #sys.stderr.write("\t%s\n" % impl)
                    db.domainAPIs[name2] = [ (LSEtk_m4_macro,0,impl, None) ]
            else:
                if impl != None:
                    name = LSEcg_domain_mangler(name,myenv.name,idtype,1)
                    db.domainAPIs[name] =  [ (LSEtk_m4_macro,0,impl, None) ]
        return None

    print_iddef = [ print_iddef_types_h,
                    print_iddef_control_h,
                    print_iddef_control_c,
                    print_iddef_defines
                    ][piece]

    ################### And the real code ################################
    #
    # we do class identifiers, then instance identifiers, then merged
    # identifiers
    #
    # handle domain class stuff
    
    tokpaths = tok.incPath

    for d in db.domainOrder:
        dclass = d[0]
        dom = d[0].className

        print ("    /*** Domain: %s ***/\n" % dom)

        if not piece:             # if we are in the header file section
            for name in dclass.classHeaders:
                print "#include <%s>" % name

        if piece != 3 and not dclass.classNamespaces: continue

        tok.incPath = tokpaths + dclass.classLibPath
        LSEcg_domain_set_rc(db,dclass.searchPath)

        myenv.name = (dom,None)
        tok.startInput([])

        if piece != 3:
            _LSEcg_start_namespace(dclass.classNamespaces[0])

        if dclass.classIdentifiers:
            tok.pushInputSource(("in domain class '%s'" % dom,None))
            reduce(print_iddef,dclass.classIdentifiers,[])
            tok.popInputSource()

        if dclass.classHeaderText and piece == 1:
            loc = "in classHeaderText for '%s'" % dom
            s = dclass.classHeaderText
        elif dclass.classCodeText and piece == 2:
            loc = "in classCodeText for '%s'" % dom
            s = dclass.classCodeText
        elif dclass.classMacroText and piece == 3:
            loc = "in classMacroText for '%s'" % dom
            s = dclass.classMacroText
        else: s = None
        if s:
            try:
                tok.pythonEnv["LSE_domain_class"] = dclass
                tok.pythonEnv["LSE_domain_inst"] = None
                tok.processInput(s, loc, [], sys.stdout)
            except LSEtk_TokenizeException, v:
                LSEcg_report_error(v)

        if piece != 3:
            _LSEcg_end_namespace(dclass.classNamespaces[0])


    # Domain implementation stuff

    didImpl = {}
    for dinst in db.domainInstanceOrder:
        dom = dinst.className
        dclass = db.domains[dom][0]

        key = (dclass, dinst.implName)
        if didImpl.has_key(key): continue
        didImpl[key] = 1

        print ("    /*** Domain: %s Implementation: %s ***/\n" % 
               (dom, dinst.implName))


        if not piece:
            for name in dinst.implHeaders:
                print "#include <%s>" % dinst.renamedHeaders.get(name,name)

        if piece != 3 and not dinst.implNamespaces: continue

        tok.incPath = tokpaths + dinst.implLibPath
        LSEcg_domain_set_rc(db,dinst.searchPath)
        dominame = dinst.instName
        myenv.name = (dom,dominame)

        dd = {}
        for ns in dinst.renamedNamespaces.items():
            dd[ns[0]] = [ (LSEtk_m4_macro, LSEtk_NoArgs, ns[1], None) ]
        for ns in dinst.implFrontRename:
            dd[ns] = [ (LSEtk_m4_macro, LSEtk_NoArgs,
                        "LSEdri_" + dinst.instName + "__" + ns, None) ]

        if piece != 3:
            implNS = dinst.renamedNamespaces[dinst.implNamespaces[0]]
            _LSEcg_start_namespace(implNS)

        if not piece:

            for cn in dinst.classNamespaces:
                if cn != implNS:
                    print "  using namespace %s;" % cn
                
            for rq in dinst.searchPath[1:]:
                if rq[1]:
                    di = db.domainInstances[rq[1]]
                    for cn in di.implNamespaces:
                        print "using namespace %s;" % di.renamedNamespaces[cn]
                    if not di.implNamespaces:
                        dc = db.domains[rq[0]][0]
                        for cn in dc.classNamespaces:
                            print "using namespace %s;" % cn
                else:
                    dc = db.domains[rq[0]][0]
                    for cn in dc.classNamespaces:
                        print "using namespace %s;" % cn

        if dinst.implIdentifiers:

            tok.pythonEnv["LSE_domain_class"] = dclass
            tok.pythonEnv["LSE_domain_inst"] = dinst
            tok.startInput([dd])
            tok.pushInputSource(("in domain implementation '%s'" % dominame,
                                 None))
            reduce(print_iddef,dinst.implIdentifiers,[])
            tok.popInputSource()

        if dinst.implHeaderText and piece == 1:
            loc = "in implHeaderText for '%s'" % dom
            s = dinst.implHeaderText
        elif dinst.implCodeText and piece == 2:
            loc = "in implCodeText for '%s'" % dom
            s = dinst.implCodeText
        elif dinst.implMacroText and piece == 3:
            loc = "in implMacroText for '%s'" % dom
            s = dinst.implMacroText
        else: s = None
        if s:
            try:
                tok.processInput(s, loc, [], sys.stdout)
            except LSEtk_TokenizeException, v:
                raise
                LSEcg_report_error(v)

        if piece != 3:
            _LSEcg_end_namespace(implNS)

    # Domain instance stuff
    
    for dinst in db.domainInstanceOrder:
        dom = dinst.className
        dclass = db.domains[dom][0]

        #if dinst.suppressed:
        #    print ("    /*** Domain: %s Instance: %s same as %s ***/\n" % 
        #       (dom, dinst.buildArgs, dinst.instName, dinst.useName))
        #    continue
        
        print ("    /*** Domain: %s Build Args: %s Instance: %s ***/\n" % 
               (dom, dinst.buildArgs, dinst.instName))

        if not piece:
            for name in dinst.instHeaders:
                print "#include <%s>" % name

        tok.incPath = tokpaths + dinst.instLibPath
        LSEcg_domain_set_rc(db,dinst.searchPath)
        dominame = dinst.instName
        myenv.name = (dom,dominame)
        dd = {}
        for ns in dinst.renamedNamespaces.items():
            dd[ns[0]] = [ (LSEtk_m4_macro, LSEtk_NoArgs, ns[1], None) ]
        for ns in dinst.implFrontRename:
            dd[ns] = [ (LSEtk_m4_macro, LSEtk_NoArgs,
                        "LSEdri_" + dinst.instName + "__" + ns, None) ]

        tok.startInput([dd])

        tok.pythonEnv["LSE_domain_class"] = dclass
        tok.pythonEnv["LSE_domain_inst"] = dinst

        if piece != 3:
            print "namespace %s {" % \
                  LSEcg_domain_namespace_mangler(dinst.instName,0)
        
        if not piece:

            for cn in dinst.implNamespaces:
                print "  using namespace %s;" % dinst.renamedNamespaces[cn]

            if not dinst.implNamespaces:
                if dclass.classNamespaces:
                    print "  using namespace %s;" % dclass.classNamespaces[0]
            for rq in dinst.searchPath[1:]:
                if rq[1]:
                    print "using namespace %s;" % \
                        LSEcg_domain_namespace_mangler(rq[1],0)
                else:
                    print "using namespace %s;" % \
                        LSEcg_domain_namespace_mangler(rq[0],1)

        if dinst.instIdentifiers:
            tok.pushInputSource(("in domain instance '%s'" % dominame,
                                 None))
            reduce(print_iddef,dinst.instIdentifiers,[])
            tok.popInputSource()

        if dinst.instHeaderText and piece == 1:
            loc = "in instHeaderText for '%s'" % dom
            s = dinst.instHeaderText
        elif dinst.instCodeText and piece == 2:
            loc = "in instCodeText for '%s'" % dom
            s = dinst.instCodeText
        elif dinst.instMacroText and piece == 3:
            loc = "in instMacroText for '%s'" % dom
            s = dinst.instMacroText
        else: s = None
        if s:
            try:
                tok.processInput(s, loc, [], sys.stdout)
            except LSEtk_TokenizeException, v:
                raise
                LSEcg_report_error(v)

        if piece != 3:
            print "} // namespace %s" % \
                  LSEcg_domain_namespace_mangler(dinst.instName,0)

    # and now do merged identifiers for each domain class
        
    for d in db.domainOrder:
        dclass = d[0]
        dom = d[0].className

        print ("    /*** Domain: %s merged identifiers ***/\n" % dom)

        LSEcg_domain_set_rc(db,dclass.searchPath)
        myenv.name = (dom,None)
        tok.startInput([])
        if dclass.mergedIdentifiers:
            print "namespace %s {" % LSEcg_domain_namespace_mangler(dom,1)
            tok.pushInputSource(("in domain class '%s'" % dom,None))
            reduce(print_iddef,dclass.mergedIdentifiers,[])
            tok.popInputSource()
            print "} // namespace %s" % LSEcg_domain_namespace_mangler(dom,1)

    print "#undef LSE_instance_name"

#
# Translate an implementation of a domain to get the names happy and resolve
# any APIs that might be part of it; the tokenizer needs to be set up with
# the proper environment and dictionaries and the input needs to be started.
#
def _LSEcg_domain_impl_translate(tok,idinfo):
    (name,idtype,impl) = idinfo
    if impl and not callable(impl):
        
        if type(impl) is types.TupleType or type(impl) is types.ListType:
            # there is no harm in translating lists to tuples, since
            # really these should have been tuples to start with...
            #
            # translate each thing in the tuple/list
            def ugly(x,i=idtype,n=name,t=tok):
                return _LSEcg_domain_impl_translate(t,(n,i,x))
            il = map(ugly,impl)
            return tuple(map(lambda x:x,il))
        else:
            loc = "in implementation of identifier '%s'" % name
            try:
                toks = tok.continueInput(str(impl),(loc,None),1)
                return str(toks)
            except LSEtk_TokenizeException, v:
                LSEcg_report_error(v)
                return impl
            
    else: return impl
    

#################
# LSEcg_get_domain_tokenizer(db, apis, myenv, indomaindefs)
#
# Get a tokenizer that is able to deal with domain classes' use of
# m4 macros
#
# the apis passed in are put in the searchpath after the domain APIs from
# the database, which are always put in.  When indomaindefs is non-zero,
# additional macros used in domain macrofiles are added to the searchpath.
#
def LSEcg_get_domain_tokenizer(db,apis,myenv,indomaindefs=1):
    
    #
    # Get the tokenizer set up properly....
    #
    if indomaindefs: dlist = [db.domainAPIs] + apis + [_LSEcg_dtok_dictionary]
    else: dlist = [db.domainAPIs] + apis
    
    tok = LSEtk_Tokenizer(LSEtk_flag_All|LSEtk_flag_UseM4Pref,dlist,myenv)
    
    LSE = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))

    tok.incPath = [ LSE + "/share/lse/framework", db.topDir + "/include" ]

    # set up the Python environment
    
    tok.pythonEnv["LSE_db"] = db
    exec ("""
import sys, os, cPickle, time,string
sys.path.append("%s" +"/domains")
sys.path.append("%s" + "/lse/framework" )

import LSE_domain
from SIM_apidefs import * # imports codegen, database, schedule, tokenizer
LSE_db.loadDomains(globals())""" % (LSE,LSE)) in tok.pythonEnv

    # set up the m4 environment
    
    starter = "m4_include(SIM_quotes.m4)"
	
    tok.processInput(starter,("",None))
    
    return tok

########### domain support API implementations #################

#
# A class identifier name
#
def _LSEcg_CLASSID(tinfo, args, extraargs, env):
    try:
        if (len(args)) != 2:
            raise LSEtk_TokenizeException(args[0] + " should have 1 "
                                          "argument",tinfo[0])
        name = string.strip(args[1])
        cname = tinfo[0].pythonEnv["LSE_domain_class"].className
        return LSEtk_TokenIdentifier(\
            LSEcg_domain_mangler(name,(cname,""),
                                 LSE_domain.LSE_domainID_class,
                                 env.name[1] is None))
    except (LSEcg_Exception, LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,args[0],tinfo[0],env.reporter)
        return []

#
# an instance identifier name
#
def _LSEcg_INSTID(tinfo, args, extraargs, env):
    try:
        if (len(args)) != 2:
            raise LSEtk_TokenizeException(args[0] + " should have 1 "
                                          "argument",tinfo[0])
        name = string.strip(args[1])
        cname = tinfo[0].pythonEnv["LSE_domain_inst"].instName
        return LSEtk_TokenIdentifier(\
            LSEcg_domain_mangler(name,("",cname),
                                 LSE_domain.LSE_domainID_inst,1))
    except (LSEcg_Exception, LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,args[0],tinfo[0],env.reporter)
        return []


#
# define an m4 macro for a class identifier
#
def _LSEcg_domain_class_define(tinfo, args, extraargs, env):
    try:
        if (len(args)) != 3:
            raise LSEtk_TokenizeException(args[0] + " should have 2 "
                                          "arguments",tinfo[0])
        name = string.strip(args[1])
        cname = tinfo[0].pythonEnv["LSE_domain_class"].className
        apiname = LSEcg_domain_mangler(name,(cname,""),
                                        LSE_domain.LSE_domainID_class|
                                        LSE_domain.LSE_domainID_m4macro,
                                        1)
        env.db.domainAPIs[apiname] = [ (LSEtk_m4_macro,0,args[2],None) ]
        #sys.stderr.write("Defined %s in class_define\n" % apiname)
        #sys.stderr.write("\t%s\n" % args[2])
    except (LSEcg_Exception, LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,args[0],tinfo[0],env.reporter)
        return []

#
# define a domain hook
#
def _LSEcg_domain_hook(tinfo, args, extraargs, env):
    try:
        if (len(args)) != 2:
            raise LSEtk_TokenizeException(args[0] + " should have 1 "
                                          "argument",tinfo[0])
        
        dclass = tinfo[0].pythonEnv["LSE_domain_class"]
        dinst = tinfo[0].pythonEnv["LSE_domain_inst"]
        hook = string.strip(args[1])

        if (dinst):
            mname = (dclass.className,dinst.instName)
            idtype =  hook in dinst.instHooks
            if idtype: 
                idtype = LSE_domain.LSE_domainID_hook | \
                         LSE_domain.LSE_domainID_inst
        else:
            dinst = dclass
            mname = (dclass.className,"")
            idtype = hook in dinst.classHooks
            if idtype:
                idtype = LSE_domain.LSE_domainID_hook | \
                         LSE_domain.LSE_domainID_class

        if idtype: return LSEcg_domain_mangler(hook,mname,idtype,1)
        else:
            raise LSEcg_Exception("Hook %s seems to be undefined for %s:%s\n"
                                  % (hook, dinst.className, dinst.instName))

    except (LSEcg_Exception, LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,args[0],tinfo[0],env.reporter)
        return []

#
# call a domain hook
#
def _LSEcg_domain_hook_call(tinfo, args, extraargs, env):
    try:
        if (len(args)) != 2:
            raise LSEtk_TokenizeException(args[0] + " should have 1 "
                                          "argument",tinfo[0])
        
        dclass = tinfo[0].pythonEnv["LSE_domain_class"]
        dinst = tinfo[0].pythonEnv["LSE_domain_inst"]
        hook = string.strip(args[1])

        if (dinst):
            mname = (dinst.className,dinst.instName)
            idtype =  hook in dinst.instHooks
            if idtype: 
                idtype = LSE_domain.LSE_domainID_hook | \
                         LSE_domain.LSE_domainID_inst
        else:
            dinst = dclass
            mname = (dclass.className,"")
            idtype = hook in dinst.classHooks
            if idtype:
                idtype = LSE_domain.LSE_domainID_hook | \
                         LSE_domain.LSE_domainID_class

        if idtype: return LSEcg_domain_mangler(hook,mname,idtype)
        else:
            raise LSEcg_Exception("Hook %s seems to be undefined for %s:%s\n"
                                  % (hook, dinst.className, dinst.instName))

    except (LSEcg_Exception, LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,args[0],tinfo[0],env.reporter)
        return []

#
# define an m4 macro for an instance identifier
#
def _LSEcg_domain_inst_define(tinfo, args, extraargs, env):
    try:
        if (len(args)) != 3:
            raise LSEtk_TokenizeException(args[0] + " should have 2 "
                                          "arguments",tinfo[0])
        name = string.strip(args[1])
        cname = tinfo[0].pythonEnv["LSE_domain_inst"].instName

        apiname = LSEcg_domain_mangler("final__" + name,("",cname),
                                       LSE_domain.LSE_domainID_inst|
                                       LSE_domain.LSE_domainID_m4macro,
                                       1)
        env.db.domainAPIs[apiname] = [ (LSEtk_m4_macro,0,args[2],None) ]
        #sys.stderr.write("Defined %s in inst_define\n" % apiname)        
        #sys.stderr.write("\t%s\n" % args[2])
    except (LSEcg_Exception, LSEtk_TokenizeException), v:
        _LSEcg_report_error(v,args[0],tinfo[0],env.reporter)
        return []

#
# domain class name
# Must be prefixed because the actual name is turned into a macro
#
def _LSEcg_domain_class_name(tinfo,args,extraargs,env):
    return "LSEcl_" + tinfo[0].pythonEnv["LSE_domain_class"].className

#
# domain instance name
#
def _LSEcg_domain_inst_name(tinfo,args,extraargs,env):
    dinst = tinfo[0].pythonEnv["LSE_domain_inst"]
    if dinst: return dinst.instName
    else: return []

    
#
# domain support APIs; only used when parsing domain macrofiles....
#
_LSEcg_dtok_dictionary = {
    "CLASSID" : [ (LSEtk_m4_macro,0,_LSEcg_CLASSID,None) ],
    "INSTID" : [ (LSEtk_m4_macro,0,_LSEcg_INSTID,None) ],
    "LSE_domain_class_define" : [ (LSEtk_m4_macro,0,
                                   _LSEcg_domain_class_define,None) ],
    "LSE_domain_inst_define" : [ (LSEtk_m4_macro,0,
                                  _LSEcg_domain_inst_define,None) ],
    "LSE_domain_hook" : [ (LSEtk_m4_macro,0,_LSEcg_domain_hook, None) ],
    "LSE_domain_hook_call" : [ (LSEtk_m4_macro,0,_LSEcg_domain_hook_call, None) ],
    "LSE_domain_class_name" : [ (LSEtk_m4_macro,LSEtk_NoArgs,
                                 _LSEcg_domain_class_name,None) ],
    "LSE_domain_inst_name" : [ (LSEtk_m4_macro,LSEtk_NoArgs,
                                 _LSEcg_domain_inst_name,None) ],
    }

    
############################################################################
#                                                                          #
#   Error handling for APIs                                                #
#                                                                          #
############################################################################

_LSEcg_error_count = 0

#########
# LSEcg_init_errors():
#########
def LSEcg_init_errors():
    global _LSEcg_error_count
    _LSEcg_error_count = 0
    
#########
# LSEcg_report_error(exception)
#   Print out an error message, but do not terminate immediately
#########
def LSEcg_report_error(v):
    global _LSEcg_error_count
    sys.stderr.write("Error: " + str(v)+"\n")
    _LSEcg_error_count = _LSEcg_error_count + 1
    if _LSEcg_error_count > 100:
        sys.stderr.write("Exiting due to too many code generation errors\n")
        raise SystemExit(3)

def _LSEcg_report_error(v,apiname,tinst,reportfunc):
    if not isinstance(v,LSEtk_TokenizeException):
        v = LSEtk_TokenizeException(str(v) + " in " + apiname,tinst)
    if reportfunc: reportfunc(v)

def LSEcg_report_error_int(v,apiname,tinst,reportfunc):
    if not isinstance(v,LSEtk_TokenizeException):
        v = LSEtk_TokenizeException(str(v) + " in " + apiname,tinst)
    if reportfunc: reportfunc(v)

########
# LSEcg_check_errors
#    stop if there were any errors
########
def LSEcg_check_errors():
    global _LSEcg_error_count
    if _LSEcg_error_count:
        sys.stderr.write("There were %d errors in code generation\n" %
                         _LSEcg_error_count)
        raise SystemExit(3)

############################################################################
#                                                                          #
#   Code for implementing APIs                                             #
#                                                                          #
############################################################################


############################################################################
#                                                                          #
#   Code generation routines                                               #
#                                                                          #
############################################################################

########
# Print out a code point
########

_LSEcg_parse_params_re = \
                       re.compile("[^,]+[&* \t\n\r\x0c\x0b]+(?P<arg>[_a-zA-Z0-9]+)\\s*(?:(?:\\[[^\\]]+\\]\\s*)*\\[\\s*\\])?\\s*(?:$|,)")

def LSEcg_parse_params(params):
    paramList = _LSEcg_parse_params_re.findall(params)
    return paramList

def LSEcg_print_point_method(cp,db):
    inst = cp.inst
    # Output code functions
    if(cp.makeMethod):
        print LSEcg_method_prototype(cp.inst.name,
                                     cp.name,
                                     cp.params,
                                     cp.returnType,0)
        print "{"
        if cp.returnType != "void": print "return "
        print LSEcg_codefunc_name(cp) + "("
        params = LSEcg_parse_params(cp.params)
        if params != []:
            print "LSEfw_f_call " + \
                  reduce(lambda x,y : x + ", " + y, params)
        else: print "LSEfw_f_call_void"
        print ");"
        print "}"
    
def LSEcg_print_point(cp,db):

    inst = cp.inst
    # Output code functions
    name = LSEcg_codefunc_name(cp)
    dfltname = LSEcg_codefunc_name(cp,1)
    
    if (cp.type == LSEdb_PointUser):
        inlining = db.getParmVal("LSE_inline_user_funcs") + " "
    elif (cp.type == LSEdb_PointControl):
        inlining = db.getParmVal("LSE_inline_control_funcs") + " "

    if (0 and cp.usesDefault):
        print inlining + LSEcg_codefunc_prototype(cp,1) + "{"
	print cp.default
	print "}"

    print inlining + LSEcg_codefunc_prototype(cp,0) + "{"
    print cp.current
    print "}"

    if(cp.makeMethod):
        print LSEcg_method_prototype(cp.inst.name,
                                     cp.name,
                                     cp.params,
                                     cp.returnType,0)
        print "{"
        if cp.returnType != "void": print "return "
        print LSEcg_codefunc_name(cp) + "("
        params = LSEcg_parse_params(cp.params)
        if params != []:
            print "LSEfw_f_call " + \
                  reduce(lambda x,y : x + ", " + y, params)
        else: print "LSEfw_f_call_void"
        print ");"
        print "}"
    
########
# Print code to implement all queries called by an instance/top/level
########

LSEcg_wunos = {
    ('global','data')   : 0,
    ('global','enable') : 1,
    ('global','ack')    : 2,
    ('global','any')    : 3,
    ('local','data')   : 4,
    ('local','enable') : 5,
    ('local','ack')    : 6,
    ('local','any')    : 7,
}

def LSEcg_print_query_externs(db,qparent):
  tlist=[] # do not repeat the protos....
  ininst = 1 # isinstance(qparent,LSEdb_MInstance)
  pinfoSeen = []
  minfoSeen = {}

  if qparent.isMultiInst: il = qparent.subInstanceList
  else: il = [ qparent ]

  # we do not put a forward declaration in for anything in the same module
  for qparent in il: minfoSeen[qparent.name] = 1

  for qparent in il:

    ql = qparent.calls.keys()
    ql.sort()

    # now deal with our calls...
    
    for qc in ql:

        (apiclass,instname,objname) = qc # original key

        # put instance data in place
        if not minfoSeen.has_key(instname):
            minfoSeen[instname] = 1
            print "extern class LSEfw_module %s; " % \
                  LSEcg_instdata_name(instname);

        # trampolined

        if apiclass == "LSE_query_call" and ininst:

            q=db.getQuery(qc[1],qc[2])
            # if q.inst == qparent: continue   # do we schedule self queries?
            key = (LSEcg_iname_transform(db,qc[1]),qc[2],"qc")
            if key not in tlist:
                tlist.append(key)
                print "extern %s;" % LSEcg_query_tramp_prototype(db,
                                                                 qc[1],qc[2],
                                                                 q.params,
                                                                 q.returnType,
                                                                 1)
        # not trampolined
        elif apiclass == "LSE_method_call" or \
             (not ininst and apiclass=="LSE_query_call"):

            q=db.getMethodOrQuery(qc[1],qc[2])
            if q.inst == qparent: continue
            key = (LSEcg_iname_transform(db,qc[1]),qc[2],"mc")
            if key not in tlist:
                tlist.append(key)
                print "extern %s;" % LSEcg_method_prototype(key[0],qc[2],
                                                            q.params,
                                                            q.returnType,1)

        elif apiclass == "LSE_event_record":
            q=db.getEvent(qc[1],qc[2])
            if q.inst == qparent: continue
            key = (LSEcg_iname_transform(db,qc[1]),qc[2],"er")
            if key not in tlist:
                tlist.append(key)
                print "extern %s;" % LSEcg_event_prototype(q,"record",1)

        # port query
        else:
            p = db.getPort(instname,objname,1)
            if p: # a real port
                if p.width:
                    nlist = string.split(apiclass,".")
                    dodata = nlist[1] == "data" or nlist[1] == "any"
                    LSEcg_do_portquery_externs(p,dodata,pinfoSeen)
            else:
                _LSEcg_print_port_alias_externs(db,qparent,p,qc,pinfoSeen)

#
# external definitions for port/alias queries
#
def LSEcg_do_portquery_externs(p,dodata,pinfoSeen):
    key = (p.inst.name, p.name)
    if key in pinfoSeen: return
    pinfoSeen.append(key)
    
    print "extern struct LSEfw_portinst_info_s<%s> %s[];" % \
          (p.type, LSEcg_portinst_info_name(p.inst.name,p.name))

#
# port alias externs    piname = 

#
def _LSEcg_print_port_alias_externs(db,qparent,p,key,pinfoSeen):
    (apiclass,instname,objname) = key
    
    pa = db.getPortAlias(instname,objname)

    if not len(pa[1]): # empty port alias
        return

    # external reference to portinst info structures
    # also find the data type...
    #  must initialize to something in case no instances connected...
    qdtype = "LSE_type_none"
    for c in pa[1]:
        if c:
            p = db.getPort(c[0],c[1])
            qdtype = p.type
            break

    key2 = (instname,objname,"painfo")
    if key2 not in pinfoSeen:
        pinfoSeen.append(key2)
        print "extern struct LSEfw_port_alias_s<%s> %s[];" % \
              (qdtype, LSEcg_port_alias_name(instname,objname))

    return
