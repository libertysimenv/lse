/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * This file is the header file for reference counting
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Provides a base reference-counted structure type.
 *
 */

#ifndef _LSE_REFCOUNT_H
#define _LSE_REFCOUNT_H

/* Must have already included <SIM_refcount_types.h> */

inline LSE_refcount_t *LSE_refcount_alloc(size_t s, 
					         LSE_refcount_t **l) {
  LSE_refcount_t *t;
  if (l == NULL || *l==NULL) t=(LSE_refcount_t *)malloc(s);
  else { 
	t = *l;
	*l = t->next;
  }
  if (t!=NULL) {
     t->count = 1;
     t->dynamic = TRUE;
  }
  return(t);
}

inline void LSE_refcount_init(LSE_refcount_t *s) {
  s->count = 1;
  s->dynamic = FALSE;
}

#define LSE_refcount_register(s) do{((LSE_refcount_t *)(s))->count++);}while(0)

inline void LSE_refcount_cancel(LSE_refcount_t *p,
				       LSE_refcount_t **l) {
  if (!(--(p->count)) && p->dynamic) {
    if (l!=NULL) { 
      p->next = *l;
      *l = p;
    }
    else free(p);
  }
}

#define LSE_refcount_get(s) (((LSE_refcount_t *)(s))->count)

#endif /* _LSE_REFCOUNT_H */

