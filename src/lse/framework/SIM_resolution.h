/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Definition of the speculation resolution type
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines the speculation resolution type and a few APIs
 *
 */
#ifndef _LSE_RESOLUTION_H_
#define _LSE_RESOLUTION_H_

#define LSE_dynid_cancel(i) LSEdy_dynid_internal_cancel(i,\
                              "<resolution> ",__FILE__,__LINE__)
#define LSE_dynid_register(i) LSEdy_dynid_internal_register(i,\
                              "<resolution> ",__FILE__,__LINE__)

#ifndef LSEfw_IN_CONTROL_C
extern 
#endif
LSEfw_mutex_t LSEre_resolution_mutex;

inline LSE_resolution_t 
LSE_resolution_create(LSE_dynid_t resolved_inst,
		      LSE_resolution_class_t rclass)
{
  LSE_resolution_t t;

  LSEre_resolution_mutex.lock();
  if (LSEre_resolution_free_list==NULL) {
    t=(LSE_resolution_t)malloc(LSEre_resolution_size);
    t->super.next = (LSE_refcount_t *)LSEre_resolution_master_list;
    LSEre_resolution_master_list = t;
    t->alloc_size=0;
    t->ids=NULL;
  } else {
    t=LSEre_resolution_free_list;
    LSEre_resolution_free_list=(LSE_resolution_t)t->super.next;
    t->super.next = (LSE_refcount_t *)LSEre_resolution_master_list;
    LSEre_resolution_master_list = t;
  }
  LSEre_resolution_mutex.unlock();
  t->super.count=1;
  t->num_ids=0;
  t->rclass = rclass;
  t->resolved_inst = resolved_inst;
  if (resolved_inst) LSE_dynid_register(resolved_inst);
  memset(&t->fields, 0, sizeof(t->fields));
  memset((((char *)t)+LSEre_resolution_field_offset),0,
	 LSEre_resolution_field_size);
  return(t);
}

#define LSE_resolution_register(r) \
  do {LSEfw_atomic_inc(&((r)->super.count),&LSEre_resolution_mutex)}while(0)
#define LSE_resolution_cancel(r) \
  do {LSEfw_atomic_dec(&((r)->super.count),&LSEre_resolution_mutex)}while(0)

/* This is special because it has to register the dynid */
inline void LSEre_resolution_set_resolved_instr(LSE_resolution_t r, 
							   LSE_dynid_t d) {
  LSE_dynid_register(d);
  if (r->resolved_inst) LSE_dynid_cancel(r->resolved_inst);
  r->resolved_inst = d;
}

#define LSE_resolution_BLOCK_FACTOR 256

inline void 
LSE_resolution_add_dynid(LSE_resolution_t r, LSE_dynid_t id, 
			 boolean do_register) 
{
  if ((r->num_ids % LSE_resolution_BLOCK_FACTOR)==0 && 
      (r->alloc_size * LSE_resolution_BLOCK_FACTOR) == r->num_ids) {
    r->ids=(LSE_dynid_t *)realloc(r->ids,
		     sizeof(LSE_dynid_t) * (++r->alloc_size) *
		     LSE_resolution_BLOCK_FACTOR);
  }
  r->ids[r->num_ids++]=id;
  if (do_register) LSE_dynid_register(id);
}

#undef LSE_dynid_cancel
#undef LSE_dynid_register

#endif /* _LSE_RESOLUTION_H_ */
