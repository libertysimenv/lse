# #! /bin/sh
# #! perl
#eval '(exit $?0)' && eval 'exec perl -S -x $0 ${1+"$@"}'
#                    & eval 'exec perl -S -x $0 $argv:q'
#                            if $running_under_some_shell;
# /* 
#  * Copyright (c) 2005 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Script to do some level of indentation
#  *
#  * Authors: David Penry <dpenry@cs.princeton.edu>
#  *
#  */

$depth = 0;
while (<>) {
   s/^[\t ]*//;
   s/#current_line/"# " . ($.+1) . " \"$ARGV\""/eg;
   if (/^#/ || /^$/) {
   } else {
     if (/^}/ || /^\)/) {
       for (2..$depth) { print "  "; }
     } else {
       for (1..$depth) { print "  "; }
     }
   }
   print or die "-p destination: $!\n";
   $i = index $_, "{", 0;
   while ($i >= 0) {
     $depth++; 
     $i = index $_, "{", ($i+1);
   }
   $i = index $_, "(", 0;
   while ($i >= 0) {
     $depth++;
     $i = index $_, "(", ($i+1);
   }
   $i = index $_, "}", 0;
   while ($i >= 0) {
     $depth--;
     $i = index $_, "}", ($i+1);
   }
   $i = index $_, ")", 0;
   while ($i >= 0) {
     $depth--;
     $i = index $_, ")", ($i+1);
   }
  if ($depth < 0) { $depth = 0; }
}

#| sed -e 's/ *#/#/g' > sc.c ; echo > /dev/null
#perl -i -npe 's/\#current_line/"\# " . ($.+1) . " \"$ARGV\""/eg;' sc.c

