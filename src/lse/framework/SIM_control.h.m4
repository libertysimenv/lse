/*
 * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * This file is processed to become SIM_control.h
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Declarations of global signal and schedule control types, variables, and
 * macros.  Basically, these are the "core" data structures that generated 
 * module code and framework code manipulates.
 *
 */
m4_include(SIM_quotes.m4)
LSEm4_push_diversion(-1)
m4_include(SIM_database.m4)
LSEm4_pop_diversion()
#ifndef _LSE_CONTROL_H_
#define _LSE_CONTROL_H_

/* Must include SIM_all_types.h first, which contains 
 * stdlib, LSE_config, LSE_time, LSE_types
 */
extern "C" {
#include <stddef.h> /* to get offsetof macro */
}

#ifdef __GNUC__
#define LSEfw_attributes(args...) __attribute__ ((args))
#else
#define LSEfw_attributes(args...) 
#endif

#if (LSEfw_PARM(LSE_mp_multithreaded))
#define LSEfw_cache_align LSEfw_PARM(LSE_cache_line_size)
#else
#define LSEfw_cache_align 1
#endif

#define LSEfw_ALIGN LSEfw_attributes(aligned(LSEfw_cache_align))

/************* Variables from LSS **********/

m4_pythonfile(
for t in LSE_db.globalVars:
    if t[3]: # if no one used it to give it a name, do not create it
	print "extern %s %s;" % (t[2], t[3][0])

rtps = LSE_db.runtimeParms.items()
rtps.sort()
for rtp in rtps:
    print "extern %s %s;" % (rtp[1][2], rtp[1][0])
)m4_dnl

/**************** Threading types and code ********************/
/* must come after variables because of run-time parameters in the code */

typedef enum LSEfw_thread_command_e {
  LSEfw_thread_command_None,
  LSEfw_thread_command_MemReclaim,
  LSEfw_thread_command_Reschedule,
  LSEfw_thread_command_Finish
} LSEfw_thread_command_t;

/* idea is to group all the stuff between timesteps into one $ line*/
typedef struct LSEfw_mainloop_info_s { 
  LSE_time_numticks_t timesteps;
  LSEfw_thread_command_t thread_command;
} LSEfw_mainloop_info_t;

extern LSEfw_mainloop_info_t LSEfw_info;

extern int LSEfw_num_threads;

#include <SIM_threading.h>

/******************** crazy macros ********************************/

#define LSEfw_stf_def 	        LSEfw_iptr_t LSEfw_iptr,
#define LSEfw_stf_def_void 	LSEfw_iptr_t LSEfw_iptr
#define LSEfw_stf_call	        LSEfw_iptr,
#define LSEfw_stf_call_void	LSEfw_iptr
#define LSEfw_stf_call0         LSEfw_iptr,
#define LSEfw_stf_call0_void    LSEfw_iptr
#define LSEfw_stf_proto         LSEfw_iptr_t, 
#define LSEfw_stf_proto_void    LSEfw_iptr_t

#define LSEfw_tf_def 
#define LSEfw_tf_def_void 	  void
#define LSEfw_tf_call	  
#define LSEfw_tf_call_void	  
#define LSEfw_tf_call0         
#define LSEfw_tf_call0_void    
#define LSEfw_tf_proto         
#define LSEfw_tf_proto_void    void
#define LSEfw_tf_callc         ,
#define LSEfw_tf_callc_void    

#define LSEfw_f_def 
#define LSEfw_f_def_void      void
#define LSEfw_f_call	  
#define LSEfw_f_call_void	  
#define LSEfw_f_call0         
#define LSEfw_f_call0_void    
#define LSEfw_f_proto         
#define LSEfw_f_proto_void    void
#define LSEfw_f_callc         ,
#define LSEfw_f_callc_void    

#define LSEfw_sf_def           LSEfw_iptr_t LSEfw_iptr,
#define LSEfw_sf_def_void      LSEfw_iptr_t LSEfw_iptr
#define LSEfw_sf_call          LSEfw_iptr, 
#define LSEfw_sf_call_void     LSEfw_iptr
#define LSEfw_sf_proto         LSEfw_iptr_t,
#define LSEfw_sf_proto_void    LSEfw_iptr_t

/*************** scheduling structures ******************/

extern LSEfw_oneshot_barrier_t LSEfw_mainloop_barrier;
extern LSEfw_oneshot_barrier_t LSEfw_status_barrier;
extern LSEfw_oneshot_barrier_t LSEfw_phase_barrier;
extern LSEfw_oneshot_barrier_t LSEfw_phase_end_barrier;
extern LSEfw_mutex_t           LSEfw_sigcheck_mutex;
extern LSEfw_syncsig_t         *LSEfw_sync_signals;
extern LSEfw_mutex_t           *LSEfw_cblock_mutexes;
extern int                     LSEfw_num_syncs;
extern int                     LSEfw_num_locks;

typedef void *LSEfw_iptr_t;
typedef void (*LSEfw_firing_t)(LSEfw_stf_def int);

struct LSEfw_static_schedule_t {
  LSEfw_firing_t cont;
  int instno;
  unsigned int cblockno;
  LSEfw_iptr_t ptr;
#if (!LSEfw_PARM(LSE_DAP_schedule_oblivious))
  boolean forcerun;
#endif
};

typedef void (*LSEfw_end_func_t)(LSEfw_stf_proto int);

struct LSEfw_static_end_t {
  LSEfw_end_func_t cont;
  int instno;
  LSEfw_iptr_t ptr;
};

struct LSEfw_signal_checker_t {
  int goodat;
  int sno;
  int sloc;
  LSE_signal_t *p;
  const char *s;
  LSE_signal_t mask;
};

struct LSEfw_timing_record_t {
 uint64_t clocks;
 uint64_t clocks2;
 uint64_t maxclocks, maxclocks2;
};

/*************** Per-thread information structures ******/

// This structure contains information that must be shared so that 
// reductions can be performed or so that thread controls can be done.
extern struct LSEfw_sharedthread_info_t {

#if (LSEfw_PARM(LSE_mp_multithreaded))
  struct reclaim_s {
    int dcount;
    boolean had_neg_dynid;
    boolean did_something;
  } reclaim_reports LSEfw_ALIGN;  /* used to handle dynid reclamation */
#endif

  /* count of unknown signals resolved; counts down from 0 */
  int *unknown_signal_countP;  		// pointer to unknown signal count
  int *sigs_resolved;   /* must be indirect so we do not need recompilation of
	                   everything when the number of signals changes */

  /* thread id from threading system */
  LSEfw_threadID_t id;     /* needed to join threads at end */
  int idno;                /* internal ID number */

  /* list of master lists of dynids */
  LSE_dynid_t *LSEdy_dynid_master_listP;

  LSEfw_static_schedule_t *thread_schedule;
  LSEfw_static_end_t *thread_end_schedule;
  LSEfw_signal_checker_t *signal_checker;
  int schedule_length;
  int end_schedule_length;
  unsigned int firstsig, lastsig;

} *LSEfw_shthreads;

/******************** Refcount handling ***********************/

#include <SIM_refcount.h>

extern LSE_dynid_num_t LSEdy_dynid_global_num; /* last allocated idno */
extern LSE_dynid_t LSE_dynid_default;          // dummy dynid

#if (!LSEfw_parallelize_dynids)
extern LSE_dynid_t LSEdy_dynid_free_list;      /* free dynids */
extern LSE_dynid_t LSEdy_dynid_master_list;    /* unfree dynids */
#else
extern LSEfw_tprivate LSE_dynid_t LSEdy_dynid_free_list;
extern LSEfw_tprivate  LSE_dynid_t LSEdy_dynid_master_list;
#endif /* !LSEfw_parallelize_dynids */

extern LSEfw_tprivate int unknown_signal_count;
extern LSEfw_tprivate int *sigs_resolved;

#if (!LSEfw_PARM_constant(LSE_debug_dynid_refs))
extern boolean LSEfw_PARM(LSE_debug_dynid_refs);
#endif

#include <SIM_dynid.h>

#include <SIM_resolution.h>

/******************** Declare the module class ******************/

class LSEfw_module {
public:
  const char *LSE_instance_name; /* instance name of the module instance */
}; /* class LSEfw_module */

/* This is a pointer to void * rather than to class LSEfw_module because
 * we really know nothing about the class at these places and want a pointer
 * to the start of the object.  If we use the class thing, it might not
 * actually point to the right start of object if the eventual derived
 * class is virtual
 */

/******************** Declare the structures needed *******************/

/* Standard error file pointer */
extern FILE *LSE_stderr;

/* Simulator return status */
extern int LSE_sim_exit_status;

/* Flag to terminate simulation */
extern int LSE_sim_terminate_now;

/* Count to terminate simulation */
extern int LSE_sim_terminate_count;

/* measured wall clock time during actual execution in microseconds */
extern uint64_t LSE_sim_wall_time_elapsed;

/* Code calling code block */
extern LSEfw_tprivate unsigned int LSEfw_schedule_calling_cblock;

/* In phase_start flag */
extern boolean LSEfw_schedule_in_phase_start;

typedef void (*LSEfw_phase_t)(LSEfw_stf_def_void);
typedef void (*LSEfw_handler_t)(LSEfw_stf_def int);
typedef void (*LSEfw_portevent_t)(LSEfw_stf_def int, LSE_signal_t, 
				  LSE_signal_t, LSE_dynid_t, void *);

typedef struct LSEfw_schedule_cblock_s {
  LSEfw_firing_t cont;
  LSEfw_iptr_t ptr;
  int instno;
} LSEfw_schedule_cblock_t;

typedef struct LSEfw_schedule_s {
  int counter;
  int currsize;
  int pointer;
  unsigned int *cblocks;
} LSEfw_schedule_t;

/* Schedule flags -- indicates what has already been scheduled */
extern int LSEfw_schedule_flags[];

/* Common dummy variables and routines */
extern LSE_signal_t LSEfw_null_port_status;
extern m4_python(print LSEcg_codeblock_prototype(LSE_db,None,1));
extern LSE_type_none LSEfw_none_ptr;

/* Scheduling list */
extern LSEfw_schedule_t LSEfw_schedule_timestep;
extern LSEfw_mutex_t LSEfw_schedule_mutex;

#if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
extern int LSEfw_schedule_LEClevel;
extern boolean LSEfw_schedule_LECrunagain;

typedef struct LSEfw_schedule_LEC_s {
  struct LSEfw_schedule_LEC_s *next;
  int cno;
  int fno;
} LSEfw_schedule_LEC_t;

typedef struct LSEfw_schedule_LEC_levels_s {
  LSEfw_schedule_LEC_t *head;
  LSEfw_schedule_LEC_t *tail;
} LSEfw_schedule_LEC_levels_t;

extern LSEfw_schedule_LEC_levels_t LSEfw_schedule_LEC_levels[];
extern LSEfw_schedule_LEC_t LSEfw_schedule_LEC[];
extern boolean LSEfw_schedule_LEC_flags[];
#endif

#if (LSEfw_PARM(LSE_DAP_static_schedule_type)==1||\
     LSEfw_PARM(LSE_DAP_static_schedule_type)==2)
extern boolean LSEfw_schedule_in_acyclic_finish;
#endif

/* Debugging histogram */
extern int LSEfw_cblock_histogram[];

/* Code Blocks */
extern LSEfw_schedule_cblock_t LSEfw_schedule_cblock[];

/******************* schedule manipulation code *******************/

#ifdef LSEfw_IN_CONTROL_C

/* Functions to grow a schedule */

void LSEfw_schedule_grow(LSEfw_schedule_t *p) {
  if (p->currsize == 0) p->currsize=2;
  else p->currsize *= 2;
  /* fprintf(LSE_stderr,"Growing to %d",p->currsize); */
  p->cblocks = (unsigned int *)realloc(p->cblocks, 
				       sizeof(unsigned int) * p->currsize);
}
void LSEfw_schedule_grow_to(LSEfw_schedule_t *p,int size) {
  p->currsize = size;
  p->cblocks = (unsigned int *)realloc(p->cblocks, 
				       sizeof(unsigned int) * size);
}

#else /* LSEfw_IN_CONTROL_C */
extern void LSEfw_schedule_grow(LSEfw_schedule_t *pp);
extern void LSEfw_schedule_grow_to(LSEfw_schedule_t *pp, int size);
#endif /* LSEfw_IN_CONTROL_C */

#if !LSEfw_PARM(LSE_schedule_share_code) || defined(LSEfw_IN_CONTROL_C)

#if !LSEfw_PARM(LSE_schedule_share_code)
static 
#endif
LSEfw_PARM(LSE_inline_schedule_code) LSEfw_schedule_t 
*LSEfw_schedule_create(int size) {
  LSEfw_schedule_t *p;
  p=(LSEfw_schedule_t *)malloc(sizeof(struct LSEfw_schedule_s));
  p->cblocks=(unsigned int *)malloc(sizeof(unsigned int) * size);
  p->counter=0;
  p->pointer=0;
  p->currsize=size;
  return p;
}

#if !LSEfw_PARM(LSE_schedule_share_code)
static
#endif
LSEfw_PARM(LSE_inline_schedule_code) void
LSEfw_schedule_append(LSEfw_schedule_t *pp, unsigned int c) {
   int n;
   LSEfw_schedule_t *p;
   LSEfw_schedule_mutex.lock();
   p = pp;
   if (FALSE || 
#if (LSEfw_PARM(LSE_DAP_schedule_oblivious))
       !LSEfw_schedule_flags[c]
#else
       LSEfw_schedule_flags[c]<=0
#endif
       ) {
     if (p->counter == p->currsize) {
	LSEfw_schedule_grow(pp);
     }
     n=p->counter;
     p->cblocks[n] = c;
     p->counter++;
     LSEfw_schedule_flags[c] = 1;
     if (LSEfw_PARM(LSE_debug_codeblock_calls)) {
       fprintf(LSE_stderr, "Schedule appending %u\n", c);
     }
   }
   LSEfw_schedule_mutex.unlock();
}

#if !LSEfw_PARM(LSE_schedule_share_code)
static
#endif
LSEfw_PARM(LSE_inline_schedule_code) void
LSEfw_schedule_prepend(LSEfw_schedule_t *pp, unsigned int c) {
   int n;
   LSEfw_schedule_t *p;
   LSEfw_schedule_mutex.lock();
   p = pp;
   if (FALSE || 
#if (LSEfw_PARM(LSE_DAP_schedule_oblivious))
       !LSEfw_schedule_flags[c]
#else
       LSEfw_schedule_flags[c]<=0
#endif
       ) {
     if (TRUE || p->pointer <= 0) { /* append it */
       if (p->counter == p->currsize) {
	  LSEfw_schedule_grow(pp);
       }
       n=p->counter;
       p->cblocks[n] = c;
       p->counter++;
       LSEfw_schedule_flags[c] = 1;
       LSEfw_schedule_mutex.unlock();
       if (LSEfw_PARM(LSE_debug_codeblock_calls)) {
         fprintf(LSE_stderr, "Schedule appending %u\n", c);
       }
       return;
     } else { /* put at beginning */
       n=p->pointer;
       p->cblocks[n] = c;
       p->pointer--;
       LSEfw_schedule_flags[c] = 1;
       if (LSEfw_PARM(LSE_debug_codeblock_calls)) {
	 fprintf(LSE_stderr, "Schedule prepending %u\n", c);
       }
     }
   }
   LSEfw_schedule_mutex.unlock();
}

#else /* LSEfw_IN_CONTROL_C */
extern void LSEfw_schedule_append(LSEfw_schedule_t *p, unsigned int c);
extern void LSEfw_schedule_prepend(LSEfw_schedule_t *p, unsigned int c);
extern LSEfw_schedule_t *LSEfw_schedule_create(int size);
#endif /* LSEfw_IN_CONTROL_C */

#define LSEfw_schedule_pointer(p) p.pointer
#define LSEfw_schedule_counter(p) p.counter
#define LSEfw_schedule_piece(p,i) p.cblocks[i]

typedef struct LSEfw_query_instance_s {
  int index; /* = -1 if not valid */
  void *entry;
} LSEfw_query_instance_t;

typedef struct LSEfw_query_info_s {
  int numinst;
  LSEfw_query_instance_t *instances; /* will be organized as 2-d array */
} LSEfw_query_info_t;

typedef struct LSEfw_query_port_entry_s {
  int currwaiters;
  int allocwaiters;
  unsigned int waiters[4];
} LSEfw_query_port_entry_t;

extern void LSEfw_port_query_sleep(LSEfw_query_instance_t *instance,
				   unsigned int LSEfw_calling_id, 
				   int LSEfw_instno);
extern void LSEfw_port_query_wakeup(LSEfw_query_instance_t *instance);


/************************ Port structures ************************/

#define LSEfw_status_storage_type LSE_signal_t

typedef struct LSEfw_PORT_nodef_s {
  LSEfw_status_storage_type global;
  LSEfw_status_storage_type local;
} LSEfw_PORT_nodef_t;
typedef struct LSEfw_PORT_empty_s {
  LSEfw_status_storage_type global;
} LSEfw_PORT_empty_t;

typedef struct LSEfw_PSTAT_structure_s {
m4_pythonfile(if 1:
  if not LSE_db.pstatList:
    print "int LSE_dummy;"
  else:
    for pd in LSE_db.pstatList:
      if pd[3]: stype = "LSEfw_PORT_empty_t"
      else: stype = "LSEfw_PORT_nodef_t"
      print "%s %s[%d];" % (stype, LSEcg_portstatus_field_name(pd[0], pd[1]),
                            pd[2])
)m4_dnl
} LSEfw_PSTAT_structure_t;

extern LSEfw_PSTAT_structure_t LSEfw_PORT_initial_status,
	                       LSEfw_PORT_ignore_status, LSEfw_PORT_status;

m4_pythonfile(
for inst in LSE_db.instanceOrder:
    for p in inst.portOrder:
        if not p.width: continue
	if 0:
            if p.controlEmpty:
                print "extern LSEfw_PORT_empty_t " + \
                      LSEcg_portstatus_name(inst.name,p.name,1)+\
                      ("[%d];" % p.width)
            else:
                print "extern LSEfw_PORT_nodef_t " + \
                      LSEcg_portstatus_name(inst.name,p.name,1)+\
                  ("[%d];" % p.width)
        else:
            if p.controlEmpty:
                print "extern LSEfw_PORT_empty_t *%s;" % \
                      LSEcg_portstatus_name(inst.name,p.name,1)
            else:
                print "extern LSEfw_PORT_nodef_t *%s;" % \
                      LSEcg_portstatus_name(inst.name,p.name,1)
            
)m4_dnl

typedef struct LSEfw_port_wakeup_s {
  LSEfw_query_instance_t wu[8];
} LSEfw_port_wakeup_t;

/**** Port instance structure fields:
 * Once upon a time these structures divided up stuff which uses global
 * information at initialization and stuff which does not.  The stuff which
 * was not was initialized in the generated module code and the stuff which
 * used global went in SIM_control.cc.  This was good for reducing rebuild
 * time and the like.  Unfortunately, this also led to two problems:
 * 1. layout of the data was causing false sharing between threads
 * 2. The non-global stuff included fields which are written to, which 
 *    was causing false write sharing
 *
 * So, the obvious solution is to build them all in one place so we have
 * some control of layout; once this is done, we can separate read-only from
 * read-write fields.  Going further, we can merge the data pointer into
 * the read-write fields.
 */

template<class T> struct LSEfw_portinst_info_s { 
  /* This structure is for read/write fields, but we leave resolve code
   * in so that we do not have to access another structure to do a 
   * port query
   */
  LSEfw_port_wakeup_t wakeups;   /* our query wakeup structures */
  const int localresolvecode;    /* local resolution */
  const int globalresolvecode;   /* global resolution */

  /* stuff that is not initialized */
  LSE_dynid_t id;
  T *datap;
};

extern struct LSEfw_portinst_info_s<LSE_type_none> LSEfw_null_portinst_info;

#if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
typedef struct LSEfw_LEC_sinfo_s {
  int blevel;
  int fno;
  int cno;
} LSEfw_LEC_sinfo_t;
#endif

template<class T> struct LSEfw_portinst_ginfo_s {
  /* this structure is for read-only fields */
  /* Initializers which use values which are assigned globally
   * and thus need to be regenerated on every build that might change
   * those values or which use values that change due to third-party
   * interactions
   */
  LSE_signal_t *const ostatus;     /* port status of other side */
  const int instno;          /* port instance number of other side */
  const unsigned int cno;    /* code block number of the other side */
  const int signo[3];        /* signal numbers (data,enable,ack) */

#if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
  const LSEfw_LEC_sinfo_t LECo[2]; /* LEC info for other side */
  const LSEfw_LEC_sinfo_t LECf[2]; /* LEC info for firing */
  const LSEfw_LEC_sinfo_t LECs[2]; /* LEC info for self */
#endif

  /* non-global initializers */

  const LSEfw_firing_t firing;    /* firing function of other side */
  const LSEfw_iptr_t ptr;         /* instance pointer of other side */
  const LSEfw_portevent_t presolve; /* port resolve event */ 
  const LSEfw_portevent_t plresolve; /* port local resolve event */ 
  const int schedcode;           /* how should the other side be scheduled? 
                                  * 0=always static, 1 = dynamic
			          * 2=immediate, 3=immediate/check reent */
  const int fschedcode;          /* how should the firing be scheduled? */
  const int ignoremask;          /* signals to ignore */

  struct LSEfw_portinst_info_s<T> *const other_info;/* pointer to other info */
};

template<class T> struct LSEfw_port_alias_s {
  LSE_signal_t *const globalstat;
  struct LSEfw_portinst_info_s<T> *const pinfo; /* pointer to port info */
};

/******************* instance cblock callers ********************/


/****************** standard debug status routine ***************/

extern void LSEfw_debug_status_standard(const char *, int, LSE_signal_t);

/****************** memory management ****************************/

extern void LSE_memory_reclaim_master(void);
#if (LSEfw_parallelize_dynids)
extern void LSE_memory_reclaim_slave(int tid);
#endif

/******************* Offsets into structures *****************/

m4_pythonfile(
if not LSE_db.getParmVal("LSE_use_direct_field_access"):
    for s in ["LSE_dynid_t","LSE_resolution_t"]:
        il = LSE_db.structAdds[s].keys(); il.sort()
        for i in il:
	    print "extern const size_t " + \
		  LSEcg_structadd_offset_name(i,s) + ";"
)

/******************* Datatype refcounting ***************************/

m4_pythonfile(
def do_refcount(tentry, oper):
    slist = [ "inline void %s_%s(%s *p) {\n" % \
              (tentry[2],oper,tentry[2]) ]
    pinfo = tentry[7]
    if pinfo[1] == LSEdb_type_is_array:
        aentry = pinfo[2]
        subtname = aentry[1]
        slist.append("int i;\nfor (i=0;i<%d;i++) {" % aentry[0])
	if subtname == "LSE_dynid_t":
            slist.append("if (p->elements[i]) "
                         "LSEdy_dynid_internal_%s(p->elements[i], "
                         "\"<no instance>\",__FILE__,__LINE__);\n" % oper)
	elif subtname == "LSE_resolution_t":
            slist.append("if (p->elements[i]) %s_%s(p->elements[i]);\n" % \
                         (subtname[:-2],oper))
        else:
            slist.append("  %s_%s(&(p->elements[i]));\n" % (subtname,oper))
        slist.append("}")
    elif pinfo[1] == LSEdb_type_is_struct:
        aentry = tentry[7][2]
        for f in tentry[7][2]:
            if f[0]:
                subtname = f[1]
                if subtname == "LSE_dynid_t":
                    slist.append("if (p->%s) LSEdy_dynid_internal_%s(p->%s,"
                                 "\"<no instance>\",__FILE__,__LINE__);\n" %\
                                 (f[2],oper,f[2]))
                elif subtname == "LSE_resolution_t":
                    slist.append("  if (p->%s) %s_%s(p->%s);\n" % \
                                 (f[2],subtname[:-2],oper,f[2]))
                else:
                    slist.append("  %s_%s(&(p->%s));\n" % (subtname,oper,
                                                           f[2]))
    slist.append("}")
    print string.join(slist)

for t in LSE_db.globalTypes:
    if t[7][0]: # we need to generate code...
	do_refcount(t,"cancel")
	do_refcount(t,"register")
        print
)m4_dnl

/****************** Hook prototypes ************************/

m4_pythonfile(
def prologue(dinst, isclass):
  print "namespace %s {" % LSEcg_domain_namespace_mangler(dinst, isclass)
def epilogue(dinst, isclass):
  print "} // namespace %s" % LSEcg_domain_namespace_mangler(dinst, isclass)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """void LSE_domain_hook(dynid_dump)(LSE_dynid_t);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("dynid_dump",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """void LSE_domain_hook(dynid_reclaim)(LSE_dynid_t);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("dynid_reclaim",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """void LSE_domain_hook(dynid_allocate)(LSE_dynid_t);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("dynid_reclaim",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print ("""void LSE_domain_hook(end_of_timestep)(void);""")
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("end_of_timestep",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """int LSE_domain_hook(finalize)(void);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("finalize",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """int LSE_domain_hook(finish)(void);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("finish",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """int LSE_domain_hook(init)(void);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("init",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """int LSE_domain_hook(parse_arg)(int, char *, char **);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("parse_arg",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """int LSE_domain_hook(parse_leftovers)(int, char **, char **);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("parse_leftovers",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print ("""void LSE_domain_hook(resolution_reclaim)"""
	 """(LSE_resolution_t);""")
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("resolution_reclaim",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """int LSE_domain_hook(start)(void);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("start",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print ("""void LSE_domain_hook(start_of_timestep)(void);""")
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("start_of_timestep",doit,doit,LSE_db)

def doit(dinst, isclass):
  prologue(dinst,isclass)
  print """void LSE_domain_hook(usage)(void);"""
  epilogue(dinst, isclass)
LSEcg_domain_hook_loop("usage",doit,doit,LSE_db)
)

/****************** internal global API protos and statics *******/

inline void 
LSEfw_report_warn(const char *inst, const char *fmt, ...) {
  va_list ap;
  va_start(ap,fmt);
  fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
  fprintf(LSE_stderr," : %s : ", inst);
  vfprintf(LSE_stderr, fmt, ap);
  fprintf(LSE_stderr,"\n");
  va_end(ap);
}

extern void LSEfw_report_err(const char *inst, const char *fmt, ...);

inline boolean 
LSEfw_port_connected(int porti, int size, const char *conns,
		     const char *iname, const char *pname) {

#if (m4_python(print LSE_db.getParmVal("LSE_check_api_at_runtime")))
  if (porti < 0 || porti > size) {
    LSEfw_report_err(iname,"Port index out of range in "
		     "\037LSE\036_port_connected(%s[%d]...)",pname,porti);
    return FALSE;
  }
#endif

  /* interesting note.... gcc will give the same code inlining this as when
   * doing "connstring"[porti] without the call, except when porti is a 
   * constant; it doesn't seem to track the string's value in, though it
   * does track the string's address and so the code isn't bad...
   * interestingly, it has the same problem even if the string is
   * a constant inside the function and porti is a constant, which is the
   * "normal" case with checking....  So, explicit code specialization 
   * only hurts clarity without helping performance...
   */
  return (conns[porti] == 'Y');
}

inline void 
LSEfw_query_info_grow(LSEfw_query_info_t *p, int index, int width) {
  int i, newsize;

  newsize = index+width;
  p->instances = (LSEfw_query_instance_t *)realloc(p->instances, 
			 sizeof (LSEfw_query_instance_t) * newsize);

  /* clear newly allocated space */
  for (i=p->numinst; i<newsize;i++) {
    p->instances[i].index = -1;
    p->instances[i].entry = NULL;
  }
  p->numinst = newsize;
}

/****************** Domain header stuff **********************/

m4_pythonfile(
# set up the environment and look for ids
myenv = LSEcg_Environment()
myenv.db = LSE_db
myenv.inst = None
myenv.codeloc = LSEcg_in_domaincode
myenv.reporter = LSEcg_report_error
myenv.name = None
LSEcg_domain_generate_ids(LSE_db,[LSEap_APIs],myenv,1)
)

/******************** status nonsense **********************/

extern unsigned int LSEfw_signal_time_mask;

inline LSE_signal_t LSEfw_readstatus(const LSE_signal_t pstatus) {
  return (pstatus >> LSEfw_signal_time_mask) & 0333;
}

#define LSEty_signal_double_data_mask \
  (LSEty_signal_data_mask | (LSEty_signal_data_mask << 12))
#define LSEty_signal_double_enable_mask \
  (LSEty_signal_enable_mask | (LSEty_signal_enable_mask << 12))
#define LSEty_signal_double_ack_mask \
  (LSEty_signal_ack_mask | (LSEty_signal_ack_mask << 12))

/**************** and we are done *********************/

m4_dnl  This is done in a separate section to maximize the amount of file
m4_dnl  output after an error...
m4_pythonfile(LSEcg_check_errors())

#endif /* _LSE_CONTROL_H_ */
