# /* 
#  * Copyright (c) 2000-2003 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Makefile for the framework directory of a machine  
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Lists the files that need to be built and gives the simple
#  * 'all' and 'clean' targets
#  *
#  */

TOPSRCDIR=..
include ../include/Make_include.mk   # This is necessary

OBJFILES = SIM_control.o SIM_initfinish.o SIM_mainloop.o
BSRCFILES = SIM_control.cc SIM_initfinish.cc SIM_mainloop.cc

CFILES = $(OBJFILES) $(BSRCFILES) libframework.a

# BSRCFILES is put into all to prevent make from deleting the .c
# files after it has compiled the code.  Why it feels a need to do that,
# I do not know!
all: $(BSRCFILES) libframework.a

libframework.a:  $(OBJFILES)
	-rm -f libframework.a
	$(AR) cru libframework.a $(OBJFILES)
	$(RANLIB) libframework.a

clean: 
	/bin/rm -f $(CFILES)

