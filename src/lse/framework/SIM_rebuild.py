# /* 
#  * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python rebuild module
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This module calculates whether rebuilds of portions of
#  * the design are necessary.
#  *
#  */

import re, string, sys, LSE_domain, pdb
from SIM_codegen import *

################### rebuild routines ##############################

def LSErb_oredmap(fn,l1,l2):
    # should be slightly faster than this:
    #return (len(l1) != len(l2) or
    #        reduce(lambda x,y:x or y,map(fn,l1,l2),0))
    if len(l1) != len(l2): return 1
    for i in map(fn,l1,l2):
        if i: return 1
    return 0

def LSErb_signal_changes_for_schedule(self,other):
    return (self.num != other.num or
            self.inst != other.inst or
            self.port != other.port or
            self.instno != other.instno or
            self.dropreason != other.dropreason or
            self.type != other.type or
            self.cno != other.cno or
            self.depson != other.depson)

########
# This routine is used to make some checks which will be cached for later
#
def LSErb_prepare_for_rebuild_checks(olddb, newdb):

    # mark whether the domain instances have changed...
    
    for y in newdb.domainInstanceOrder:
        #y.changed = 0

        # anything we depended on changed?
        for sp in y.searchPath[1:]:
            if newdb.domainInstances[sp[1]].changed:
                y.changed = 1
                
        # mark self change

        x = olddb.domainInstances.get(y.instName)
        if not x:
            y.changed = 1
            continue
        elif LSErb_domain_inst_changed(x,y):
            y.changed = 1
            continue
        
    # mark whether the domain classes have changed...
    for y in newdb.domainOrder:
        #y[0].changed = 0
        for sp in y[0].classRequiresDomains:
            if newdb.domains[sp][0].changed:
                y[0].changed = 0

        x = olddb.domains.get(y[0].className)
        if not x:
            y[0].changed = 1
            continue
        elif LSErb_domain_class_changed(x,y):
            y[0].changed = 1
            continue
            
    ##### type change information
    
    # we only care about the types we actually have in the new database; if
    # there used to be more, we do not need to rebuild to get rid of them as
    # long as the instance's check says that if a type was used, the instance
    # is rebuilt.
    for tentry in newdb.globalTypes:
        oldtentry = olddb.typeBEMap.get(tentry[2],(1,1))
        tentry[5][0] = (oldtentry[1] != tentry[1])
        if tentry[5][0]: continue
        # look for changes due to domains
        for n in tentry[4].keys():
            if n[0]==1: # a domain type
                if (n[2] is None): # domain class identifier
                    if newdb.domains[n[1]][0].changed:
                        tentry[5][0] = 1
                        break
                    elif newdb.domainInstances[n[2]].changed:
                        tentry[5][0] = 1
                        break

    #### variable change information

    # changes occur if a new variable name does not map in the old world to
    # something with the same user name and backend type name; differences
    # in backend type implementations are taken care of through the type
    # change discovery mechanism
    for ventry in newdb.globalVars:
        if ventry[3]:
            # if a global variable becomes unused, I do not need to rebuild;
            # I can just leave the old one around...
            oldventry = olddb.varBEMap.get(ventry[3][0],(1,1,1,1))
            ventry[5] = (ventry[1] != oldventry[1] or ventry[2] != oldventry[2]
                         or ventry[3] != oldventry[3])
    
########
# Rerun-condition for ls-schedule
# 1) Signal list changed (except for realdeps)
# 2) Parameters changed
def form_sched_parms(db):
    return (db.parms['LSE_schedule_generate_static'],
            db.parms['LSE_schedule_coalesce_static'],
            db.parms['LSE_DAP_static_coalesce_type'],
            db.parms['LSE_DAP_dynamic_schedule_type'],
            db.parms['LSE_DAP_static_schedule_type'],
            db.parms['LSE_schedule_very_large_component_size'],
            db.parms['LSE_schedule_small_component_size'],
            db.parms['LSE_schedule_max_unrolled_size'],
            )

def LSErb_needs_reschedule(olddb, newdb):
    #### Figure out if we need to reschedule
    #
    # 1) We need to if the signal lists have changed or any of the signals or
    #    dependencies are different; different signal orderings also count
    # 2) Also need to look at parameters

    # 1) signal list changed
    if LSErb_oredmap(LSErb_signal_changes_for_schedule,
                     olddb.signalList,
                     newdb.signalList):
        return "signal list changed"

    # 2) parameter change
    if form_sched_parms(olddb) != form_sched_parms(newdb):
       return "scheduling parameters changed"

    return 0

#
#
#
def LSErb_domain_inst_changed(old,new):
    return (old.className != new.className or
            old.searchPath != new.searchPath or
            old.instName != new.instName or
            old.useName != new.useName or
            old.suppressed != new.suppressed or
            old.buildArgs != new.buildArgs or
            old.runArgs != new.runArgs or
            old.instAttributes != new.instAttributes or
            old.implRename != new.implRename or
            old.implHeaders != new.implHeaders or
            old.implNamespaces != new.implNamespaces or
            old.implIdentifiers != new.implIdentifiers or
            old.implLibraries != new.implLibraries or
            old.implLibPath != new.implLibPath or
            old.instMacroText != new.instMacroText or
            old.instHeaderText != new.instHeaderText or
            old.implMacroText != new.implMacroText or
            old.implHeaderText != new.implHeaderText or
            old.instLibPath != new.instLibPath or
            old.instLibraries != new.instLibraries or
            old.instHeaders != new.instHeaders or
            old.instIdentifiers != new.instIdentifiers)
    
def LSErb_domain_inst_text_changed(old,new):
    return (old.instCodeText != new.instCodeText or
            old.implCodeText != new.implCodeText)
    
#
#
#
def LSErb_domain_inst_achanged(old,new):
    if (old.instAttributes != new.instAttributes or
        old.attributeTypeUses != new.attributeTypeUses):
        return 1

    for n in new.attributeTypeUses:
        if n[0]==1: # a domain type
            if (n[2] is None): # domain class identifier
                if newdb.domains[n[1]][0].changed: return 1
                elif newdb.domainInstances[n[2]].changed: return 1
        elif n[0]==0: # a normal type
            if newdb.typeMap[n[1]][5][0]: return 1

    return 0
    
def LSErb_domain_class_changed(old,new):
    return (old[0].className != new[0].className or
            (old[0].searchPath != new[0].searchPath) or
            (old[0].classLibPath != new[0].classLibPath) or
            (old[0].classLibraries != new[0].classLibraries) or
            (old[0].classHeaders != new[0].classHeaders) or
            (old[0].classCompileFlags != new[0].classCompileFlags) or 
            (old[0].changesTerminateCount != new[0].changesTerminateCount) or
            (old[0].classNamespaces != new[0].classNamespaces) or
            (old[0].classIdentifiers != new[0].classIdentifiers) or
            (old[0].classAttributes != new[0].classAttributes) or
            old[0].classMacroText != new[0].classMacroText or
            old[0].classHeaderText != new[0].classHeaderText or
            (old[0].mergedIdentifiers != new[0].mergedIdentifiers))       
    
def LSErb_domain_class_text_changed(old,new):
    return (old[0].classCodeText != new[0].classCodeText)
    
def LSErb_domain_changes(olddb, newdb, incode):
    # domain implementations
    #    - If domainAPIs have changed, there has been a change
    #    - If domain instances do not agree in any of the following, they
    #      have changed:
    #         class name, instance name, implementation, parameters,
    #         attributes, instance headers, instance identifiers,
    #         compile flags
    #         (hooks are not included because they end up in framework)
    #    - If domain classes do not agree in any of the following, they
    #      have changed:
    #         name, class headers, class identifiers, merged identifiers,
    #         compile flags
    #         (hooks are not included because they end up in framework)
    #
    # TODO: need to make the attribute comparison safe when the
    #       attributes implementations are Python functions
    #
    if (olddb.domainAPIs != newdb.domainAPIs):
        return 1

    return (olddb.domainAPIs != newdb.domainAPIs or
            LSErb_oredmap(LSErb_domain_inst_changed,
                          newdb.domainInstanceOrder,
                          olddb.domainInstanceOrder) or 
            LSErb_oredmap(LSErb_domain_class_changed,
                          newdb.domainOrder,
                          olddb.domainOrder) or
            (incode and (LSErb_oredmap(LSErb_domain_inst_text_changed,
                                       newdb.domainInstanceOrder,
                                       olddb.domainInstanceOrder) or 
                         LSErb_oredmap(LSErb_domain_class_text_changed,
                                       newdb.domainOrder,
                                       olddb.domainOrder)))
            )

def LSErb_type_uses_changed(newdb, newuse):
    for n in newuse.keys():
        if not n[0]: # a normal type
            if newdb.typeBEMap.get(n[1])[5][0]: return 1
    return 0

def LSErb_domain_uses_changed(newdb, newuse):
    for n in newuse.keys():
        if n[0]==1: # a domain identifier
            if (n[2] is None): # domain class identifier
                if newdb.domains[n[1]][0].changed:
                    return 1
            elif newdb.domainInstances[n[2]].changed:
                return 1
    return 0

def LSErb_structadd_changes(olddb, newdb):
    # structure changes
    #    - Attributes have been covered in point #1 (they are only
    #      instance-specific)
    #    - Names of fields are somewhat easy; we can compare as a string, but
    #      we need to check used types as well...
    # types are checked by caller
    return (olddb.structAdds != newdb.structAdds)

def LSErb_inst_name_changes(olddb, newdb):
    # instance name changes
    return (LSErb_oredmap(lambda x,y: x.name != y.name or \
                          x.multiInst.name != y.multiInst.name,
                          olddb.instanceOrder,
                          newdb.instanceOrder))

def LSErb_evfilled(evlist):
    return reduce(lambda x,y:x or y.isFilled, evlist, 0)

def LSErb_inst_pieces_changes(olddb, newdb):
    # instance name or pieces changes
    return (LSErb_oredmap(lambda x,y: (x.name != y.name or
                                       x.multiInst.name != y.multiInst.name or
                                       x.withCode != y.withCode or
                                       x.start != y.start or
                                       x.phase != y.phase or
                                       (x.getPoint("start_of_timestep",1).\
                                        isEmpty != \
                                        y.getPoint("start_of_timestep",1).\
                                        isEmpty) or
                                       (x.getPoint("end_of_timestep",1).
                                        isEmpty != \
                                        y.getPoint("end_of_timestep",1).\
                                        isEmpty) or
                                       (x.getPoint("init",1).isEmpty != \
                                        y.getPoint("init",1).isEmpty) or
                                       (x.getPoint("finish",1).isEmpty != \
                                        y.getPoint("finish",1).isEmpty) or
                                       (LSErb_evfilled(x.eventOrder) !=
                                        LSErb_evfilled(y.eventOrder)) or
                                       x.end != y.end),
                            olddb.instanceOrder,
                            newdb.instanceOrder))

def LSErb_port_changes1(x1,y1):
    return (x1.name != y1.name or x1.type != y1.type or x1.dir != y1.dir or
            x1.width != y1.width or x1.independent != y1.independent or
            x1.controlEmpty != y1.controlEmpty)

def LSErb_port_changes2(x1,y1):
    return (x1.name != y1.name or x1.type != y1.type or x1.dir != y1.dir or
            x1.width != y1.width or x1.independent != y1.independent or
            x1.controlEmpty != y1.controlEmpty or x1.handler != y1.handler)

def LSErb_port_changes3(x1,y1):
    return (x1.connInfoList != y1.connInfoList)

def LSErb_port_changes(olddb, newdb):
    # port changes
    #    - Ports are not equal if names, types, widths, or independence change
    return (LSErb_oredmap(lambda x,y:
                          (x.name != y.name or
                           LSErb_oredmap(LSErb_port_changes1,
                                         x.portOrder,
                                         y.portOrder)),
                          olddb.instanceOrder,
                          newdb.instanceOrder))

def LSErb_port_connectivity_info_changes(olddb, newdb):
    # port changes
    #    - Ports are not equal if names, types, widths, or independence change
    return (LSErb_oredmap(lambda x,y:
                          (x.name != y.name or
                           LSErb_oredmap(LSErb_port_changes3,
                                         x.portOrder,
                                         y.portOrder)),
                          olddb.instanceOrder,
                          newdb.instanceOrder))

def LSErb_global_type_changes(olddb, newdb):
    for n in newdb.globalTypes:
        if n[5][0]: return 1
    return 0

def LSErb_global_var_changes(olddb, newdb):
    for ventry in newdb.globalVars:
        if ventry[5]: return 1
    return 0

def LSErb_cblocks_list_changes(olddb, newdb):
    # code block list changes
    return olddb.cblocksList != newdb.cblocksList

def LSErb_signal_list_changes(olddb, newdb):
    # signal list changes
    return LSErb_oredmap(lambda x,y: x.__dict__ != y.__dict__,
                         olddb.signalList,
                         newdb.signalList)

def LSErb_final_schedule_changes(olddb, newdb):
    # final schedule changes
    return (#olddb.unrolledSched != newdb.unrolledSched or
            #olddb.cblocksDynSects != newdb.cblocksDynSects or
            #olddb.cblocksCalls != newdb.cblocksCalls or
            olddb.dynamicExtras != newdb.dynamicExtras or
            olddb.cblocksList != newdb.cblocksList
            )

def LSErb_top_parm_changes(olddb, newdb):
    # top-level parameters
    return olddb.parms != newdb.parms

LSErb_safe_parms = {
    # seen only in framework directory
    "LSE_synchronize_stdio" : None,
    "LSE_garbage_collection_interval" : None,
    "LSE_debug_dynid_limit" : None,
    "LSE_debug_resolution_limit" : None,
    "LSE_check_ports_incrementally" : None,
    "LSE_show_port_statuses" : None,
    # "LSE_check_ports_for_unknown" : None, # no longer safe

    "LSE_DAP_inline_barriers" : None,
    "LSE_mp_use_yield" : None,
    "LSE_mp_slow_spin" : None,
    "LSE_DAP_record_invocation_timing" : None,

    # safe because inserts data collectors, which cause rebuild
    "LSE_show_port_statuses_changes" : None, 
    "LSE_show_port_statuses_start_cycle" : None,
    "LSE_show_port_statuses_start_phase" : None,
    "LSE_show_port_statuses_end_cycle" : None,
    "LSE_show_port_statuses_end_phase" : None,
    
    # control of the build process itself
    "LSE_share_module_code_threshold" : None,
    "LSE_share_module_code_percent_threshold" : None,
    "LSE_DAP_force_shared_apis" : None,
    
    # changes in these scheduling parameters show up in port schedule info
    # if the schedule is changed
    "LSE_schedule_generate_static" : None,
    "LSE_schedule_coalesce_static" : None,
    "LSE_schedule_style_firing" : None,
    "LSE_schedule_style_handler" : None,
    "LSE_schedule_analyze_cfs" : None,
    "LSE_schedule_analyze_modules" : None,
    "LSE_schedule_use_independent" : None,
    #"LSE_DAP_dynamic_schedule_type" : None, # no longer safe
    #"LSE_DAP_static_schedule_type" : None, # no longer safe
    "LSE_DAP_static_coalesce_type" : None,
    #"LSE_DAP_schedule_oblivious", : None # definitely global
    'LSE_schedule_very_large_component_size' : None,
    'LSE_schedule_small_component_size' : None,
    'LSE_schedule_max_unrolled_size' : None,
    
    # modules never see the threading schedules
    "LSE_mp_constraint_file" : None,
   }

def LSErb_top_parm_global_changes(olddb, newdb):
    # top-level parameters that could make a mess of things
    # these are the ones beginning with "LSE_" and not listed above
    oldp = filter(lambda x:(x[0][:4]=="LSE_" and
                            LSErb_safe_parms.get(x[0],1)),
                  olddb.parms.items())
    oldp.sort()
    newp = filter(lambda x:(x[0][:4]=="LSE_" and
                            LSErb_safe_parms.get(x[0],1)),
                  newdb.parms.items())
    newp.sort()
    return oldp != newp

def LSErb_inst_sharing_changes(olddb,newdb):
    return ((olddb.wantCodeSharing != newdb.wantCodeSharing) or
            LSErb_oredmap(lambda x,y:LSErb_oredmap(lambda a,b:\
                                                   a.name != b.name,
                                                   x.subInstanceList,
                                                   y.subInstanceList),
                          olddb.multiInstList,
                          newdb.multiInstList))

def LSErb_port_alias_changes(oldi, newi, olddb, newdb):
    olda = oldi.aliasSeen.keys() ; olda.sort()
    newa = newi.aliasSeen.keys() ; newa.sort()
    olda = map(lambda x:olddb.getPortAlias(x[0],x[1]),olda)
    newa = map(lambda x:newdb.getPortAlias(x[0],x[1]),newa)
    return LSErb_oredmap(lambda x,y:x[1] != y[1],olda,newa)

########
# Include directory rebuilds occur when:
# 1) Tarball changed
# 2) Domain implementations changed
# 3) Module instance names change
# 4) Structadd changes
# 5) Any port names, types, widths, dir, independence change
# 6) Any global type changes
# 7) top-level parameter changes
# 8) Any global var changes
# 9) Any runtime parameter definition change
# 10) port status data structure changes
def LSErb_needs_include_rebuild(olddb, newdb):

    # 1) tarball changes (checked elsewhere)

    # 2) domain implementations
    if LSErb_domain_changes(olddb, newdb, 0):
        return "domain instances or implementation changed"

    # 3) module instance name changes
    if LSErb_inst_name_changes(olddb, newdb):
        return "module instance names changed"

    # 4) structadd changed
    if LSErb_structadd_changes(olddb, newdb):
        return "structadds changed"

    # 5) port name changes
    if LSErb_port_changes(olddb, newdb):
        return "port names/types/widths/dir/independence changed"

    # 6) global type changes
    if LSErb_global_type_changes(olddb, newdb):
        return "a user-defined type changed"

    # 7) top-level parameter changes
    if LSErb_top_parm_changes(olddb, newdb):
        return "top-level parameters changed"

    # 8) global var changes
    if LSErb_global_var_changes(olddb, newdb):
        return "user-defined variables changed"

    # 9) run-time parameter changes
    if olddb.runtimeParms != newdb.runtimeParms:
        return "run-time parameter definitions changed"

    # 10) port status data structure changes
    if olddb.pstatList != newdb.pstatList:
        return "port status data structure changed"

    return 0

########
# Framework directory rebuilds occur when:
# 1) include tarball changed
# 2) tarball changed
# 3) Domain implementations changed
# 4) Module instance names/phase pieces/presence/absence of start_of_timestep
#    or end_of_timestep or init or finish or presence of collectors changed
# 5) Structures or attributes changed (because types change)
# 6) Any port names, types, widths, independence, handler change
# 7) Any global type changes
# 8) top-level parameters change
# 9) Any global var changes
# 10) Any runtime parameter definition
# 11) code block list changes
# 12) schedule changes
# 13) signal list changes
# 14) top-level events or collectors change
# 15) signatures of any method/query called by a top-level collector or 
#     userpoint changes; this one is tricky, so we will just look for
#     any signature changes in methods/queries.  We'll cover it in 4
# 16) number of locks or semaphores changes
# 17) instance sharing changes
# 18) port alias information changes
# 19) port status data structure changes
# 20) Port connectivity or scheduling info changes
# 21) LEC information changes
def LSErb_needs_framework_rebuild(olddb, newdb):

    # 1) include tarball changes (checked elsewhere)

    # 2) tarball changes (checked elsewhere)

    # 3) domain implementations
    if LSErb_domain_changes(olddb, newdb, 1):
        return "domain instances or implementation changed"

    # 4) module instance pieces changes
    if LSErb_inst_pieces_changes(olddb, newdb):
        return "module instance information changed"

    # 5) structure/attribute changes
    if LSErb_structadd_changes(olddb, newdb):
        return "structadds changed"

    # 6) port changes
    if LSErb_port_changes(olddb, newdb):
        return "port names/types/widths/dir/control/independence changed"

    # 7) global type changes
    if LSErb_global_type_changes(olddb, newdb):
        return "a user-defined type changed"

    # 8) top-level parameter changes
    if LSErb_top_parm_changes(olddb, newdb):
        return "top-level parameters changed"

    # 9) global var changes
    if LSErb_global_var_changes(olddb, newdb):
        return "user-defined variables changed"

    # 10) run-time parameter changes
    if olddb.runtimeParms != newdb.runtimeParms:
        return "run-time parameter definitions changed"

    # 11) code blocks list changes
    if LSErb_cblocks_list_changes(olddb, newdb):
        return "code block list changed"

    # 12) schedule changes
    if LSErb_final_schedule_changes(olddb, newdb):
        return "final schedule changed"

    # 13) signal list changes
    if LSErb_signal_list_changes(olddb, newdb): 
        return "signal list changed"

    # 14) top-level event changes
    if LSErb_oredmap(lambda x,y:x.isFilled != y.isFilled,
                     olddb.getInst("LSEfw_top").eventOrder,
                     newdb.getInst("LSEfw_top").eventOrder):
        return "top-level event/data collector changed"

    # 15) method/query signatures changed
    if LSErb_oredmap(lambda x,y: LSErb_oredmap(LSErb_compare_queries,
                                               x.queryOrder,y.queryOrder),
                     olddb.instanceOrder,
                     newdb.instanceOrder):
        return "a query/method definition or use changed"

    # 17) module instance pieces changes
    if LSErb_inst_sharing_changes(olddb, newdb):
        return "instance sharing changed"

    # 18) port alias information changed
    if LSErb_oredmap(lambda x,y: LSErb_port_alias_changes(x,y,olddb,newdb),
                     olddb.instanceOrder,
                     newdb.instanceOrder):
        return "port alias information changed"

    # 19) port status data structure changes
    if olddb.pstatList != newdb.pstatList:
        return "port status data structure changed"
                                               
    # 20) Port connectivity/scheduling info changes
    if LSErb_port_connectivity_info_changes(olddb, newdb):
        return "Port connectivity/scheduling information changed"

    # 21) LECsim level information changes
    if (olddb.LEClevelSizes != newdb.LEClevelSizes or
        olddb.cblocksLECtlevels != newdb.cblocksLECtlevels):
        return "LEC scheduling level information changed"
                                               
    return 0

#########
# Global rebuilds occur when:
# 1) The include directory tarball changed
# 2) Structures changed
#    - Global because these affect layouts of global variables of type struct
#    - A parameter can negate this condition
# 3) top-level parameter changes
# 4) switches between threaded and multi-threaded
# 5) switches between code-shared and non-code-shared
def LSErb_needs_global_rebuild(olddb, newdb):

    # 1) include tarball changes (checked elsewhere)

    # 2) structadd changes


    if newdb.getParmVal("LSE_use_direct_field_access"):

        if LSErb_structadd_changes(olddb, newdb): 
            return "structadds changed in some instance"

        if LSErb_type_uses_changed(newdb,newdb.structAddUsesTypes):
            return "a type used in a structadd changed"
    
        if LSErb_domain_uses_changed(newdb,newdb.structAddUsesTypes):
            return "a domain used in a structadd changed"

    if LSErb_oredmap(LSErb_domain_inst_achanged,
                     filter(lambda x:x.instAttributes,
                            newdb.domainInstanceOrder),
                     filter(lambda x:x.instAttributes,
                            olddb.domainInstanceOrder)):
        return "a domain attribute on a structure has changed"
    
    # 3) top-level parameter changes

    if LSErb_top_parm_global_changes(olddb, newdb) or \
        olddb.prefix_extras != newdb.prefix_extras:
        return "Top-level parameters changed"

    # 4) switch between shared and non-shared
    if (olddb.wantCodeSharing != newdb.wantCodeSharing):
        return "Switched between code-sharing and no code-sharing"

    # 5) port status data structure changes
    if newdb.getParmVal("LSE_use_direct_port_status"):
        if olddb.pstatList != newdb.pstatList:
            return "port status data structure changed"

    return 0

def LSErb_compare_codepoints(self,other):
    return (self.name != other.name or
            self.current != other.current or
            self.default != other.default or
            self.type != other.type or
            self.returnType != other.returnType or
            self.params != other.params)

def LSErb_compare_collectors(self,other):
    return (self.header != other.header or
            self.decl != other.decl or
            self.report != other.report or
            self.init != other.init or
            self.record != other.record)

def LSErb_compare_eventsandcollectors(self,other):
    return (self.name != other.name or
            self.argString != other.argString or
            LSErb_oredmap(LSErb_compare_collectors,
                          self.filled, other.filled))

def LSErb_compare_events(self,other):
    return (self.name != other.name or
            self.argString != other.argString)

def LSErb_compare_queries(self,other):
    return (self.name != other.name or
            self.params != other.params or
            self.returnType != other.returnType or
            self.scheduled != other.scheduled or
            self.isCalled != other.isCalled)

def LSErb_compare_connectivity_sched(self,other): # two ports
    # it is not necessary to check other scheduling information because its
    # values are all convenience values derived from the lists...  this may
    # not be true if we have a parameter to always do indirect scheduling;
    # if we do indirect scheduling, connInfoList should be mostly irrelevant
    # (just a few things in it matter)
    #
    return (self.name != other.name or
            self.connInfoList[:LSEdb_cil_LECinfo] !=
            other.connInfoList[:LSEdb_cil_LECinfo] or
            self.firingCodeList != other.firingCodeList or
            self.localResolveCodeList != other.localResolveCodeList or
            self.globalResolveCodeList != other.globalResolveCodeList or
            self.selfCodeList != other.selfCodeList)

def LSErb_compare_connectivity_sharing(olddb,newdb,oldinst,newinst):
    # relies on having already checked that port presence and
    # widths are the same
    for (self,other) in map(lambda x,y:(x,y),
                            oldinst.portOrder,newinst.portOrder):
        for i in range(self.width):
            pcis = self.connInfoList[i]
            pcio = other.connInfoList[i]
            if not pcis[LSEdb_cil_InstName]: pcis=None
            if not pcio[LSEdb_cil_InstName]: pcio=None
            if pcis and pcio:
                if olddb.instances[pcis[LSEdb_cil_InstName]].multiInst.name !=\
                   newdb.instances[pcio[LSEdb_cil_InstName]].multiInst.name:
                    return 1
            elif not pcis and not pcio:
                continue
            else: return 1
    return 0

def LSErb_compare_parms(op,np,olddb,newdb):
    # compare names and values, but not types because those are the bogus
    # front-end ones; real type differences show up later...
    return op[0] != np[0] or op[1][0] != np[1][0]

def LSErb_instance_var_changes(newdb, newinst):
    for n in newinst.usesVars.keys():
        ventry = newdb.varMap.get(n)
        if ventry[5]: return 1
    return 0

def LSErb_referenced_changes(olddb, newdb, oldinst, newinst):
    # do things referenced across instances change?
    # - should be port names/width/type, queries, structure, and merging
    # - Note that type changes are covered because of normal type comparison
    if oldinst==None or newinst==None:
        return "Referred-to instance existence changed"
    if oldinst.multiInst.name != newinst.multiInst.name:
        return "Code sharing in a referred-to instance changed"
    if LSErb_oredmap(lambda x,y:(x.name != y.name or
                                 x.controlEmpty != y.controlEmpty or
                                 x.width != y.width),
                     oldinst.portOrder,
                     newinst.portOrder):
        return "a port in a referred-to instance changed"
    if LSErb_oredmap(LSErb_compare_queries,
                     oldinst.queryOrder,
                     newinst.queryOrder):
        return "a query/method in a referred-to instance changed"
    if (LSErb_oredmap(LSErb_compare_events,
                      oldinst.eventOrder, newinst.eventOrder)):
        return "an event in a referred-to instance changed"
    if reduce(lambda x,y,olddb=olddb,newdb=newdb,name=oldinst.name:\
              x or (olddb.structAdds.get(y).get(name) !=
                    newdb.structAdds.get(y).get(name)),
              olddb.structAdds.keys(),0):
        return "a structure field in a referred-to instance changed"
    return 0
    
def LSErb_referenceable_changes(olddb, newdb, oldinst, newinst):
    # id things referenceable by someone else change for these instances
    #  covers structure
    if oldinst==None or newinst==None:
        return "Instance ref existence changed"

    if oldinst.multiInst.name != newinst.multiInst.name:
        return "an instance accessible through an instance ref changed sharing"
    
    if reduce(lambda x,y,olddb=olddb,newdb=newdb,name=oldinst.name:\
              x or (olddb.structAdds.get(y).get(name) !=
                    newdb.structAdds.get(y).get(name)),
              olddb.structAdds.keys(),0):
        return "a structure field accessible through an instance ref changed"
    
    if LSErb_type_uses_changed(newdb,newinst.potentialInstRefUsesTypes):
        return "a type potentially accessible through an instance ref changed"

    if LSErb_domain_uses_changed(newdb,newinst.potentialInstRefUsesTypes):
        return "a domain potentially accessible " \
               "through an instance ref changed"

    return 0

def LSErb_alias_changes(olddb, newdb, oldal, newal):
    return oldal[1] != newal[1]

def LSErb_literal_changes(olddb, newdb):
    # things referenceable by someone else changed anywhere
    #   covers structure fields
    return (LSErb_type_uses_changed(newdb,newdb.potentialLiteralUsesTypes) or
            olddb.structAdds != newdb.structAdds or
            LSErb_type_uses_changed(newdb,newdb.structAddUsesTypes) or
            LSErb_domain_uses_changed(newdb,newdb.potentialLiteralUsesTypes) or
            LSErb_domain_uses_changed(newdb,newdb.structAddUsesTypes))

def LSErb_domain_on_sp_changed(db,newsp):
    if newsp[1]:
        return (db.domains[newsp[0]][0].changed or
                db.domainInstances[newsp[1]].changed)
    else:
        return db.domains[newsp[0]][0].changed
    
########
#  A module needs to be rebuilt if:
#   1) It is new
#   2) The tarball name or contents changed
#      - If the tarball name is a script which is not lb-build-adapters
#        (which we understand well enough to figure out the rebuild needs
#         here), then it must always be rebuilt.
#      - contents checking is in SIM_create_files.py
#   3) Port names, types, widths, independence, controlEmpty, handlers changed
#   4) Types used by the module changed (including types of ports or query
#      signatures)
#   5) Changes to instance parameters, pieces, events, stats, funcheader,
#      codepoint values, domain search path, callers, queries, structadds,
#      module bodies or module extensions
#   6) Variables used by the module changed
#   7) Signals driven by control functions change constant status; but this
#      implies a change of the control function text, and so is covered by #7
#   8) Port connectivity/scheduling information changed
#   9) Code block numbers changed -- a parameter can negate this condition
#  10) Cross-instance referenced items changed.  Not all need cause rebuild:
#      just port width, port having control point, structure fields,
#      queries, and multi-instance names need be checked.
#      Aliases are also part of this.
#  11) Items in instances pointed to by an instance ref changed.  The items
#      of concern here are just structure fields, as the .clm file can only
#      play with them at present.
#  12) Literal parameter present and anything in the system which could be
#      seen in the .clm file using a literal parameter changed.  At present,
#      just the structure fields are visible to .clm.
#  13) Changes in domain identifiers used
#  14) Changes in domains in module search path
#  15) Changes in shared status
#  16) Instances connected through ports change multi-instance names
def LSErb_needs_instance_rebuild(olddb, newdb, oldinst, newinst):

    # 1) new module
    
    if not oldinst:
        return "instance is new"

    # 2) changes in tar file name
    
    if oldinst.tarball != newinst.tarball:
        return "tarball name changed"
    if (len(newinst.tarball) and newinst.tarball[0]=='-' and
        newinst.tarball[:18]!="-lb-build-adapters"):
        return "tarball is a script"

    # 3) port information changed
    
    if LSErb_oredmap(LSErb_port_changes2,
                       oldinst.portOrder,
                       newinst.portOrder):
        return ("port names/types/widths/dir/control/independence/handler "
                "changed ")

    # 4) changes to visible types
    
    if (oldinst.typeMap != newinst.typeMap or
        LSErb_type_uses_changed(newdb,newinst.usesTypes)):
        return "there were type changes"

    # 5) changes to lots of instance things ordered by appproximate likelihood
    
    if (LSErb_oredmap(LSErb_compare_eventsandcollectors,
                      oldinst.eventOrder, newinst.eventOrder)):
        return "an event or data collector changed"

    if (oldinst.parmOrder != newinst.parmOrder):
        return "an instance parameter changed"

    if (LSErb_oredmap(LSErb_compare_codepoints,
                      oldinst.codepointOrder,newinst.codepointOrder)):
        return "a codepoint changed"

    if (oldinst.funcheader != newinst.funcheader):
        return "the module instance funcheader changed"

    if (oldinst.moduleBody != newinst.moduleBody):
        return "the module body changed"

    if (oldinst.extension != newinst.extension):
        return "the module extension changed"

    if (oldinst.calledby != newinst.calledby or
        LSErb_oredmap(LSErb_compare_queries,
                      oldinst.queryOrder,newinst.queryOrder)):
        return "a query/method definition or use changed"

    if (reduce(lambda x,y,olddb=olddb,newdb=newdb,name=newinst.name:\
               x or (olddb.structAdds.get(y).get(name) !=
                     newdb.structAdds.get(y).get(name)),
               newdb.structAdds.keys(),0)):
        return "a structadd of this instance changed"

    # only look at search path 0 because anything found along path 1 will
    # have been marked as a use
    if (oldinst.domainSearchPath[0] != newinst.domainSearchPath[0] or
        oldinst.start != newinst.start or
        oldinst.phase != newinst.phase or
        oldinst.end != newinst.end or
        oldinst.reactive != newinst.reactive or
        oldinst.strict != newinst.strict or
        oldinst.module != newinst.module # probably already hit in #2
        ):
        return "an instance attribute changed"

    # 6) changes to visible variables (done globally for now)
    
    if LSErb_instance_var_changes(newdb, newinst):
        return "a variable used by the instance changed"

    # 7) signals driven by control functions changed constant status: redundant

    # 8) port scheduling/connectivity changed
    
    if LSErb_oredmap(LSErb_compare_connectivity_sched,
                     oldinst.portOrder, newinst.portOrder):
        return "port scheduling information changed"

    # 9) code block numbers changed
    
    if (oldinst.firstCblock != newinst.firstCblock and
        olddb.getParmVal('LSE_specialize_codeblock_numbers')):
        return "code block numbers changed"

    # 10) cross-referenced stuff changes
    for y in newinst.instSeen.keys():
        rr=LSErb_referenced_changes(olddb, newdb,
                                    olddb.getInst(y,1),
                                    newdb.getInst(y,1))
        if rr: return rr
    for y in newinst.aliasSeen.keys():
        rr=LSErb_alias_changes(olddb, newdb,
                               olddb.getPortAlias(y[0],y[1],1),
                               newdb.getPortAlias(y[0],y[1],1))
        if rr: return "a used port alias changed"
        
    # 11) instanceref parameters
    #      must do this after parameter check
    for y in newinst.parms.items():
        if y[1][1]=="LSE_instanceref_t":
            rr=LSErb_referenceable_changes(olddb, newdb,
                                           olddb.getInst(y[1][0],1),
                                           newdb.getInst(y[1][0],1))
            if rr: return rr

    # 12) literal parameters 
    if (reduce(lambda x,y:(x or y[1][1]=="LSE_literal_t"),
               newinst.parms.items(),0) and
        LSErb_literal_changes(olddb,newdb)):
        return "objects accessible through a literal parameter changed"

    # 13) changes to domain identifiers used
    
    if (LSErb_domain_uses_changed(newdb,newinst.usesTypes)):
        return "there were changes to domain identifiers"

    # 14) changes to domains along the first half of the domain search path
    if (reduce(lambda x,y,db=newdb: (x or
                                     LSErb_domain_on_sp_changed(db,y)),
               newinst.domainSearchPath[0],0)):
        return "there were changes to domains in the search path"

    # 15) change in shared status
    if ((oldinst.shareID[0] < 0) != (newinst.shareID[0]<0)) or \
       oldinst.multiInst.name != newinst.multiInst.name:
        return "change in code sharing"

    # 16) change in connected port sharing
    if LSErb_compare_connectivity_sharing(olddb, newdb, oldinst, newinst):
        return "sharing information changed on connected instance"

    return 0

########
#  A multi-inst module needs to be rebuilt if:
#   1) Instances sharing the module have changed.  This is indicated by:
#      a) old instance not present OR
#      b) name changed OR
#      c) instance list names changed
#   2) Any of the constituent instances have changed in such a way as to
#      require their own rebuild
def LSErb_needs_multiinst_rebuild(olddb, newdb, oldinst, newinst):

    # 1) instance list name change
    if (not oldinst or
        oldinst.name != newinst.name or
        LSErb_oredmap(lambda x,y : x.name != y.name,
                      oldinst.subInstanceList, newinst.subInstanceList)):
        return "instances sharing code have changed"

    # 2) constituent instances have changed
    rval = LSErb_oredmap(lambda x,y : \
                         LSErb_needs_instance_rebuild(olddb, newdb,
                                                      x, y),
                         oldinst.subInstanceList, newinst.subInstanceList)

    if rval: return "constituent instance properties changed"

    return 0


