# /* 
#  * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python code generation module
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This module holds Python routines to do code generation.  We try to
#  * put as much as we can here because this file at least gets compiled
#  * to byte codes once per installation; the less Python code that has
#  * to be interpreted from scratch as we process the files, the better the
#  * speed
#  *
#  */

import re, string, sys, LSE_domain
from SIM_codegen import * # imports database, tokenizer, schedule

########################## Exceptions ##########################

class LSEap_Exception(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return self.value

#####################################################################
#              a few useful internal routines                       #
#####################################################################

def _LSEap_report_badusage(apiname,codeloc,tinst):
    raise LSEtk_TokenizeException(apiname + " cannot be used " +
                                  LSEcg_locnames[codeloc],tinst)

def LSEap_report_badargs(apiname,numargs,tinst):
    if numargs != 1:
        raise LSEtk_TokenizeException(apiname + " should have %d arguments" %\
                                      numargs, tinst)
    else:
        raise LSEtk_TokenizeException(apiname + " should have 1 argument",
                                      tinst)

def _LSEap_report_error(v,apiname,tinst,reportfunc):
    if not isinstance(v,LSEtk_TokenizeException):
        v = LSEtk_TokenizeException(str(v) + " in " + apiname,tinst)
    if reportfunc: reportfunc(v)
    else: raise v
    
#
# find first word, skipping parenthesis
#  Returns: (parent of word, depth of parenthesis skipped)
#
def _LSEap_find_first_paren(t):
    pcount = 0
    pstack = []
    while (1):
        if isinstance(t,LSEtk_TokenTopLevel):
            if not len(t.contents): raise LSEap_Exception("")
            pstack.append(t)
            t=t.contents[0]
        elif isinstance(t,LSEtk_TokenPList):
            if not len(t.contents): raise LSEap_Exception("")
            pstack.append(t)
            t=t.contents[0]
            pcount = pcount + 1
        else:
            return (pstack,pcount) # parent of the first non-parenthesis

#
# Remove parenthesis on the left, e.g.: ((((foo).bar).baz).hi)->foo.bar.baz.hi
#
def _LSEap_remove_left_paren(t):
    while(1):
        if isinstance(t,LSEtk_TokenTopLevel):
            if not len(t.contents): return
            parent = t
            t = t.contents[0]
        elif isinstance(t,LSEtk_TokenPList):
            if not len(t.contents): raise LSEap_Exception("")
            parent.contents[0:1] = t.contents
            parent = t
            t = t.contents[0]
        else: return

#
# parse out a hierarchical name from within parenthesis
#
#  Returns: (name token, parent, pstack)
#
#  invariant: no embedded top-levels
def _LSEap_parse_name(parent, pstack, stopatunmatched, allowdot):
    tok = LSEtk_TokenIdentifier("")
    iname = ""
    lastwasid = 0
    startr = 0
    while (1):
        for r in range(startr,len(parent.contents)):
            t = parent.contents[r]
            if isinstance(t,LSEtk_TokenIdentifier):
                if lastwasid: raise LSEap_Exception("")
                if not iname: tok.begWhite = t.begWhite
                iname = iname + t.contents
                lastwasid = 1
            elif isinstance(t,LSEtk_TokenEmpty):
                pass # since only happens at end, is not a problem....
            else:
                if not lastwasid and iname: raise LSEap_Exception("")
                if isinstance(t,LSEtk_TokenOther) and t.contents == "." \
                   and allowdot:
                    iname = iname + "."
                else:
                    if stopatunmatched:
                        if startr and r == startr:
                            # we did not add on the last parenthesis, so
                            # we need to put it back...
                            nt = LSEtk_TokenPList([ tok ])
                            parent.contents[0:r] = [ nt ]
                            parent = nt
                            pstack.append(nt)
                        else:
                            parent.contents[0:r] = [ tok ]
                        tok.contents = iname
                        return (tok,parent,pstack)
                    else: raise LSEap_Exception("")
                lastwasid = 0
        if not lastwasid and iname: raise LSEap_Exception("")
        if pstack:
            parent = pstack[-1]
            del pstack[-1]
            startr = 1
        else:
            # no need to do add back parenthesis if we ran out of tokens...
            parent.contents = [ tok ]
            tok.contents = iname
            return (tok,parent,pstack)

#
# parse out ' [ name : ] name '
#  Returns: (name1tok, name2tok, parent, pstack)
#
#  invariant: no embedded top-levels
def _LSEap_parse_hname_colon_sname(parent, pstack, stopatunmatched):

    (name1tok,parent,pstack) = _LSEap_parse_name(parent,pstack,1,1)
    
    # at this point, parent.contents[0] contains name1tok
    
    while (1):
        if len(parent.contents)>1:
            t = parent.contents[1]
            if t.contents == ":": break
            elif isinstance(t, LSEtk_TokenEmpty): pass # drop end space
            elif stopatunmatched: return (None, name1tok, parent, pstack)
            else: raise LSEap_Exception("")
            
        # closing parenthesis....
        if pstack:
            parent = pstack[-1]
            del pstack[-1]
            parent.contents[0] = name1tok
        else: # no colon to be found
            return (None, name1tok, parent, pstack)

    del parent.contents[0:2] # remove name1 and ":"
    
    (name2tok, parent, pstack) = _LSEap_parse_name(parent,pstack,
                                                   stopatunmatched,0)
    if name1tok.contents:
        parent.contents[0:1] = [name1tok, t, name2tok]
        return (name1tok, name2tok, parent, pstack)
    else:
        return (None, name2tok, parent, pstack)

#
# parse out ' [ name : ] name '
#  Returns: (name1tok, name2tok, parent, pstack)
#
#  invariant: no embedded top-levels
def _LSEap_parse_hname_colon_hname(parent, pstack, stopatunmatched):

    (name1tok,parent,pstack) = _LSEap_parse_name(parent,pstack,1,1)
    
    # at this point, parent.contents[0] contains name1tok
    
    while (1):
        if len(parent.contents)>1:
            t = parent.contents[1]
            if t.contents == ":": break
            elif isinstance(t, LSEtk_TokenEmpty): pass # drop end space
            elif stopatunmatched: return (None, name1tok, parent, pstack)
            else: raise LSEap_Exception("") # bad stuff at end...
            
        # closing parenthesis....
        if pstack:
            parent = pstack[-1]
            del pstack[-1]
            parent.contents[0] = name1tok
        else: # no colon to be found
            return (None, name1tok, parent, pstack)

    del parent.contents[0:2] # remove name1 and ":"
    
    (name2tok, parent, pstack) = _LSEap_parse_name(parent,pstack,
                                                   stopatunmatched,1)
    if name1tok.contents:
        parent.contents[0:1] = [name1tok, t, name2tok]
        return (name1tok, name2tok, parent, pstack)
    else:
        return (None, name2tok, parent, pstack)

#
# parse out ' [ name ":" ] name "[portno]" [ "." signal ]'
#  Returns: (name1tok, name2tok, portnotok, signaltok, parent, pstack)
#
#  invariant: no embedded top-levels
#  NOTE: does not attempt to replace the name with anything useful...
def _LSEap_parse_port_name(parent, pstack, stopatunmatched):

    (name1tok,parent,pstack) = _LSEap_parse_name(parent,pstack,1,1)

    pntok = LSEcg_TokenPortName([ LSEtk_TokenEmpty(""),
                                  name1tok,
                                  LSEtk_TokenEmpty(""),
                                  LSEtk_TokenEmpty("") ])
    
    # at this point, parent.contents[0] contains name1tok
    
    while (1):
        if len(parent.contents)>1:
            t = parent.contents[1]
            if t.contents == ":": break
            elif isinstance(t, LSEtk_TokenSBList): break
            elif isinstance(t, LSEtk_TokenEmpty): pass # drop end space
            elif stopatunmatched:
                parent.contents[0] = pntok
                return (None, name1tok, None, None, parent, pstack)
            else: raise LSEap_Exception("") # bad stuff at end...
            
        # closing parenthesis....
        if pstack:
            parent = pstack[-1]
            del pstack[-1]
            parent.contents[0] = name1tok
        else: # no colon to be found
            parent.contents[0] = pntok
            return (None, name1tok, None, None, parent, pstack)

    # at this point, t points at either a : or square brackets
    
    if t.contents == ":":
        del parent.contents[0:2] # remove name1 and ":"
    
        (name2tok, parent, pstack) = _LSEap_parse_name(parent,pstack,1,1)

        pntok.contents[0] = name1tok
        pntok.contents[1] = name2tok
        if not name2tok or not name2tok.contents: raise LSEap_Exception("")
    else:
        name2tok = name1tok
        name1tok = None
        if not name2tok or not name2tok.contents: raise LSEap_Exception("")

    # at this point, parent.contents[0] contains name2tok

    while (1):
        if len(parent.contents)>1:
            t = parent.contents[1]
            if isinstance(t, LSEtk_TokenSBList): break
            elif isinstance(t, LSEtk_TokenEmpty): pass # drop end space
            elif stopatunmatched:
                parent.contents[0] = pntok
                return (name1tok, name2tok, None, None, parent, pstack)
            else: raise LSEap_Exception("") # bad stuff at end...
            
        # closing parenthesis....
        if pstack:
            parent = pstack[-1]
            del pstack[-1]
            parent.contents[0] = name2tok
        else: # no colon to be found
            parent.contents[0] = pntok
            return (name1tok, name2tok, None, None, parent, pstack)

    # now pointing at square bracket; change it into a parenthesis
    pntok.contents[2] = sbtok = t
    
    del parent.contents[0:2] # remove name2 and brackets

    if not len(parent.contents):
        parent.contents = [ pntok ]
        return (name1tok, name2tok, sbtok, None, parent, pstack)

    if len(parent.contents)==1 or parent.contents[0].contents != ".":
        if stopatunmatched:
            parent.contents[0] = pntok
            return (name1tok, name2tok, sbtok, None, parent, pstack)
        else: raise LSEap_Exception("")

    del parent.contents[0]  # remove '.'

    (name3tok, parent, pstack) = _LSEap_parse_name(parent,pstack,
                                                   stopatunmatched,1)
    pntok.contents[3] = name3tok
    parent.contents[0] = pntok
    return (name1tok, name2tok, sbtok, name3tok, parent, pstack)

############################################################################
#                                                                          #
#   Shared code for implementing APIs                                      #
#                                                                          #
############################################################################

def _LSEap_reduceargs(alist):
    return reduce(LSEtk_reducestr,alist,"")
    
## Hook routines ##
def _LSEap_handle_resolution_resolved_instr(alist,argparent,fargs,val):
    if val != None: # it was a set
        return [ LSEtk_TokenOther("LSEre_resolution_set_resolved_instr(%s,%s)"%
                                  (alist[1],val)) ]
    else: # a get; handle in standard fashion
        return None

_LSEap_struct_elements={
    
    # dynid names
    
    "LSE_dynid_t" : {
        "idno" : None,
        },

    # resolution names

    "LSE_resolution_t" : {
        "resolved_inst" : _LSEap_handle_resolution_resolved_instr,
        "rclass" : None,
        "num_ids" : None,
        "ids" : None,
        },
    }

#
# Figure out the string for accessing an attribute
#
_LSEap_killspace1_re = re.compile("\s+(\W)") # whitespace before a non-word
_LSEap_killspace2_re = re.compile("(\W)\s+") # whitespace after a non-word

def _LSEap_attr_resolve(stype, dinstname, attrname, isget, db, modinst):
    #sys.stderr.write("_LSEap_attr_resolve: <%s> <%s> <%s> <%d>\n" %
    #                 (stype, dinstname, attrname,isget))
    
    # clear out extra spaces, but do not merge two identifiers together
    attrname = string.strip(attrname)
    attrname = _LSEap_killspace1_re.sub("\g<1>",attrname)
    attrname = _LSEap_killspace2_re.sub("\g<1>",attrname)
    
    if not dinstname:  # no domain instance specified
        # TODO: search domain classes too
        for (dom,mname) in modinst.domainSearchPath[0]+\
            modinst.domainSearchPath[1]:
            dinst = db.domainInstances[mname]
            try:
                fullname = dinst.checkAttribute(stype,attrname)
            except LSE_domain.LSE_DomainException, v:
                raise LSEap_Exception(str(v))
            if fullname: return mname + "." + fullname
        raise LSEap_Exception("Attribute '%s' not found in "
                                    "structure %s for any "
                                    "domain instance"
                                    % (attrname, stype))
    
    else:		   # domain instance specified
        if dinstname[:6] == "LSEdi_": dinstname = dinstname[6:]
        dinst = db.domainInstances.get(dinstname)
        if not dinst: # may be a class
            try:
                dclass = db.domains[dinstname][0]
                mname = dclass.className
            except KeyError:
                raise LSEap_Exception("Domain instance '%s' not found"
                                      % dinstname)
            try:
                mifunc = sys.modules[dclass.__module__].checkAttribute
            except:
                mifunc = LSE_domain.checkAttribute

            try:
                fullname = mifunc(dclass, stype,attrname)
            except LSE_domain.LSE_DomainException, v:
                raise LSEap_Exception(str(v))
        else:
            mname = dinst.instName
            try:
                fullname = dinst.checkAttribute(stype,attrname)
            except LSE_domain.LSE_DomainException, v:
                raise LSEap_Exception(str(v))
        if fullname: return mname + "." + fullname
        else: raise LSEap_Exception("Attribute '%s' not found in "
                                    "structure %s for "
                                    "domain instance '%s'"
                                    % (attrname, stype, dinstname))

#
# come up with a reference to a field
#
def _LSEap_translate_field_ref(s, args, argparent, pstack, db, currinst,
                               codeloc, subinst):

    try:
        (itok,ftok,argparent,
         pstack) = _LSEap_parse_hname_colon_sname(argparent,pstack,1)
    except LSEap_Exception, v:
        raise LSEap_Exception("Syntax error in field name")

    if itok:
        # non-local; for local there is nothing to do....
        toff = 3
        try:
            inst = db.getInst(itok.contents)
        except LSEdb_Exception,v :
            raise LSEap_Exception(str(v))
    else:
        toff = 1
        if not currinst:
            raise LSEap_Exception("Must specify instance for field")
        inst = currinst

    # check that field name is good for the instance here...

    try:
        # need to deal with the fact that the field might be a structure/union
        # and we want to check only up to the first name....
        dotind = string.find(ftok.contents,".")
        if dotind >=0:
            dummy = db.structAdds[s][inst.name][ftok.contents[:dotind]]
        else:
            dummy = db.structAdds[s][inst.name][ftok.contents]
    except:
        raise LSEap_Exception("Unable to find field '%s' for instance '%s'" % \
                              (ftok.contents,inst.name))

    if 0 and db.getParmVal("LSE_use_direct_field_access"):
        ntok = LSEtk_TokenIdentifier("fields." + \
                                     LSEcg_flatten(inst.name) + \
                                     "." + ftok.contents)
        nt = LSEtk_TokenPList([LSEtk_TokenPList(args[1].contents),
                               LSEtk_TokenOther("-"),
                               LSEtk_TokenOther(">"),
                               ntok] + argparent.contents[toff:])

        argparent.contents[0:toff] = [ nt ]
        return (inst,ftok.contents,argparent,pstack,0)
    else:
        # this is so messy that it is easier to reparse it!
        # this is the string:
        # "( ((struct %s *)( ((char *)(%s))+%s))->%s)"
        if inst == currinst:
            offset = "LSEfw_SOFFSET_%s" % s
            ns = "( ((struct %s *)( ((char *)(%s))+%s))->%s)" % \
                 (LSEcg_structadd_type_name(db.structAddMaps[s][inst.name],s),
                  str(args[1]), offset, ftok.contents)
        else:

            if codeloc == LSEcg_in_clm or codeloc >= LSEcg_in_domaincode:

                
                if db.getParmVal("LSE_use_direct_field_access"):
                    ns = "((struct %s *)( offsetof(%s,fields.%s) + " \
                         "((char *)(%s))))->%s)" % \
                         (LSEcg_structadd_type_name(inst.name,s),
                          s, LSEcg_flatten(inst.name),
                          str(args[1]), ftok.contents)
                else:
                    ns = "((struct %s *)( %s " \
                         "((char *)(%s))))->%s)" % \
                         (LSEcg_structadd_type_name(inst.name,s),
                          LSEcg_structadd_offset_name(inst.name,s),
                          str(args[1]), ftok.contents)

                # must do immediately
                argparent.contents[0:toff] = [ LSEtk_TokenRaw(ns) ]
                return (inst,ftok.contents,argparent,pstack,0)
            
            key = LSEcg_fkey(codeloc, subinst, LSEcg_fkey_struct_field)
            fl = currinst.toFinish.setdefault(key,[])
            for f in fl:
                if f[1] == inst.name and f[2] == s:
                    idno = f[0]
                    break
            else:
                idno = len(fl)
                fl.append([idno, inst.name, s, db.structAddMaps[s][inst.name],
                           0])
                
            ns = "( LSEfi_sfd(({%d,%s,%s}+((char *)(%s))))->%s)" % \
                 (idno, s, db.structAddMaps[s][inst.name], str(args[1]),
                  ftok.contents)

            #offset = LSEcg_structadd_offset_name(inst.name,s)
            #ns = "( ((struct %s *)( ((char *)(%s))+%s))->%s)" % \
            #     (LSEcg_structadd_type_name(db.structAddMaps[s][inst.name],s),
            #      str(args[1]), offset, ftok.contents)
            
        argparent.contents[0:toff] = [ LSEtk_TokenRaw(ns)]
        return (inst,ftok.contents,argparent,pstack,1)

# we determine how to implement the call by how similar the different
# call sites are to one another.
# same instance and event:       direct call with direct pointer
# same multi-instance and event: direct call through pointer from table
# otherwise:                     indirect call through pointer from table
def _LSEap_DECIDE_struct_field(db, fkey, tfrecs):
    (idno, iname, s, mapname, impltype) = tfrecs[0]
    sameinst = reduce(lambda x,y:x and y[1]==iname,tfrecs,1)
    if sameinst: impl = 0 # all the same
    else: impl = 1
    for f in tfrecs:
        f[4] = impl
        
def _LSEap_REPLACE_struct_field(db, key, tfrec):
    (idno, iname, s, mapname, impltype) = tfrec
    if impltype==0:
        if db.getParmVal("LSE_use_direct_field_access"):
            return "((struct %s *)( %s" % (LSEcg_structadd_type_name(iname,s),
                                           "offsetof(%s,fields.%s)" % \
                                           (s,LSEcg_flatten(iname)))
        else:
            return "((struct %s *)( %s" % \
                   (LSEcg_structadd_type_name(iname,s),
                    LSEcg_structadd_offset_name(iname,s))
    else:
        return "((struct %s *)( %s" % (LSEcg_structadd_type_name(mapname,s),
                                       LSEcg_apivar_name(key, idno, "offset"))

def _LSEap_PIECES_struct_field(db, key, tfrec, part):
    (idno, iname, s, mapname, impltype) = tfrec
    if impltype!=0:
        vname = LSEcg_apivar_name(key,idno,"offset")
        if part == 0: # var defs
            return "size_t %s;" % vname
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:                
                return ("size_t LSE_%s" % vname,
                        "%s = LSE_%s;" % (vname,vname),"") 
            else:
                return ("size_t LSE_%s" % vname, "",
                        "%s(LSE_%s)" % (vname,vname))
        else: # instantiation
            if db.getParmVal("LSE_use_direct_field_access"):
                return "offsetof(%s,fields.%s)" % (s,LSEcg_flatten(iname))
            else:
                return "%s" % LSEcg_structadd_offset_name(iname,s)
    return None

#
# come up with a reference to an attribute
#
def _LSEap_translate_attr_ref(s, args, argparent, pstack, db, currinst):
    
    try:
        (itok,ftok,argparent,
         pstack) = _LSEap_parse_hname_colon_sname(argparent,pstack,1)
    except LSEap_Exception, v:
        raise LSEap_Exception("Syntax error in attribute name")

    # we turn argument names back into strings for now, because
    # we do not want the domain class code messing with tokens
    # quite yet....

    if itok:
        del argparent.contents[0:2] # remove the instance name
        diname = itok.contents
    else:
        diname = ""
        if not currinst:
            raise LSEap_Exception("Must specify domain instance for attribute")

    name = _LSEap_attr_resolve(s,diname,str(args[2]),1,db,currinst)

    nt = LSEtk_TokenPList([LSEtk_TokenPList(args[1].contents),
                           LSEtk_TokenOther("-"),
                           LSEtk_TokenOther(">"),
                           LSEtk_TokenIdentifier("attrs"),
                           LSEtk_TokenOther("."),
                           LSEtk_TokenOther(name),
                           ])
    
    args[2].contents = [ nt ]
    
    return (diname, args[2], 0, 0)


############################################################################
#                                                                          #
#   Code for implementing APIs                                             #
#                                                                          #
#   The environment is made up of the following items:                     #
#       [0] Pointer to database                                            #
#       [1] Pointer to instance, None if at top-level                      #
#       [2] (Pointer to object, extrainfo), None if no object              #
#       [3] Kind of code being parsed                                      #
#       [4] Function for reporting errors; None means to just raise errors #
#                                                                          #
############################################################################

######
# FUNC(fname,args...)
######
def _LSEap_FUNC(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])

        if (len(args)<2):
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])
            
        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (utok, argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in function name")

        nm = LSEcg_func_name(currinst.name,utok.contents,0)
        
        if len(args) > 2:
            al = args[2].contents
            
            # handle void by itself
            if len(al)==1 and al[0].canonicalStr() == " void":
                if len(args)==3:
                    tlist = [LSEtk_TokenIdentifier("LSEfw_f_def_void")]
                    rv = [ LSEtk_TokenIdentifier(nm),
                           LSEtk_TokenPList(tlist) ]
                    return rv
                else:
                    isnotcall = 1
            else:
		# really not a very good parse of what is a definition and
		# what isn't.  Namespaces are a hack and so are function
		# pointers.  Really ought to do a proper match.
                isnotcall = (len(al) > 1 and
                             isinstance(al[0],LSEtk_TokenIdentifier) and
                             (isinstance(al[1],LSEtk_TokenIdentifier) or
                              isinstance(al[1],LSEtk_TokenPList) or
                              (isinstance(al[1],LSEtk_TokenOther)
                               and (al[1].contents=='*' or
                                    al[1].contents=='&' or
				    al[1].contents==':'))))
                    
            if isnotcall:
                tlist = [ LSEtk_TokenOther("LSEfw_f_def ") ]
            else:
                tlist = [ LSEtk_TokenOther("LSEfw_f_call ") ]

            rv = [ LSEtk_TokenIdentifier(nm),
                   LSEtk_TokenPList(reduce(lambda x,y: x + \
                                           [ LSEtk_TokenOther(",") ] + \
                                           y.contents,
                                           args[3:],
                                           tlist + args[2].contents))
                   ]
        else:
            thtok = [ LSEtk_TokenIdentifier("LSEfw_f_call_void") ]
            rv = [ LSEtk_TokenIdentifier(nm),
                   LSEtk_TokenPList(thtok) ]

        return rv

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

######
# FUNCPTR(fname)
######
def _LSEap_FUNCPTR(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (gtok, argparent,pstack) = \
                   _LSEap_parse_name(argparent, pstack,1,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in function name")

        return [ LSEtk_TokenIdentifier(LSEcg_func_name(currinst.name,
                                                       gtok.contents,0))
                 ] 

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

######
# FUNCPTRCALL(fptr,args...)
######
def _LSEap_FUNCPTRCALL(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])

        if (len(args)<2):
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])
            
        if len(args) > 2:
            tlist = [ LSEtk_TokenOther("LSEfw_f_call ") ]

            rv = [ args[1],
                   LSEtk_TokenPList(reduce(lambda x,y: x + \
                                           [ LSEtk_TokenOther(",") ] + \
                                           y.contents,
                                           args[3:],
                                           tlist + args[2].contents))
                   ]
        else:
            thtok = [ LSEtk_TokenIdentifier("LSEfw_f_call_void") ]
            rv = [ args[1],
                   LSEtk_TokenPList(thtok) ]

        return rv

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

######
# FUNCPTRDEF(ptr,args...)
######
def _LSEap_FUNCPTRDEF(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])

        if (len(args)<2):
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])
            
        if len(args) > 2:
            al = args[2].contents
            
            # handle void by itself
            if len(al)==1 and al[0].canonicalStr() == " void":
                if len(args)==3:
                    tlist = [LSEtk_TokenIdentifier("LSEfw_f_def_void")]
                    rv = [ args[1],
                           LSEtk_TokenPList(tlist) ]
                    return rv
            tlist = [ LSEtk_TokenOther("LSEfw_f_def ") ]

            rv = [ args[1],
                   LSEtk_TokenPList(reduce(lambda x,y: x + \
                                           [ LSEtk_TokenOther(",") ] + \
                                           y.contents,
                                           args[3:],
                                           tlist + args[2].contents))
                   ]
        else:
            thtok = [ LSEtk_TokenIdentifier("LSEfw_f_def_void") ]
            rv = [ args[1],
                   LSEtk_TokenPList(thtok) ]

        return rv

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

######
# GLOB(gname)
######
def _LSEap_GLOB(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (gtok, argparent,pstack) = \
                   _LSEap_parse_name(argparent, pstack,1,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in variable name")

        return [ LSEtk_TokenIdentifier(LSEcg_glob_name(currinst.name,
                                                       gtok.contents))
                 ] + argparent.contents[1:] # anything remaining of the name

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

######
# GLOBDEF(type, gname)
######
def _LSEap_GLOBDEF(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
        if (len(args)!=3): LSEap_report_badargs(args[0],2,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[2])
            argparent = args[2]
            pstack = []
            (gtok, argparent,pstack) = \
                   _LSEap_parse_name(argparent, pstack,1,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in variable name")

        nv = LSEtk_TokenIdentifier(LSEcg_glob_name(currinst.name,
                                                   gtok.contents))
        nv.begWhite = " "
        return args[1].contents + [ nv ] + \
               argparent.contents[1:] # anything remaining of the name

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

######
# HANDLER(hname, arg)
######
def _LSEap_HANDLER(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
        if (len(args)!=3): LSEap_report_badargs(args[0],2,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (htok, argparent,pstack) = \
                   _LSEap_parse_name(argparent, pstack,1,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in handler name")

        al = args[2].contents

        # handle void by itself -- should not happen anyway
        if len(al)==1 and al[0].canonicalStr() == " void":
            if len(args)==3:
                tlist = [LSEtk_TokenIdentifier("LSEfw_f_def_void")]
                rv = [ LSEtk_TokenIdentifier(nm),
                       LSEtk_TokenPList(tlist) ]
                return rv
            else:
                isnotcall = 1
        else:
            isnotcall = (len(al) > 1 and
                         isinstance(al[0],LSEtk_TokenIdentifier) and 
                         (isinstance(al[1],LSEtk_TokenIdentifier) or
                          isinstance(al[1],LSEtk_TokenPList) or
                          (isinstance(al[1],LSEtk_TokenOther) and
                           al[1].contents=='*')))

        if isnotcall:
            tlist = [ LSEtk_TokenOther("LSEfw_f_def ") ]
        else:
            tlist = [ LSEtk_TokenOther("LSEfw_f_call ") ]

        rv = [ LSEtk_TokenIdentifier(LSEcg_handler_name(currinst.name,
                                                        htok.contents,0)),
               LSEtk_TokenPList(tlist + args[2].contents)
               ]

        return rv
    
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

#######
# LSE_controlpoint_call_default()
# LSE_userpoint_call_default(args....)
#######
def _LSEap_point_call_default(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if args[0] == "LSE_userpoint_call_default":
            if codeloc != LSEcg_in_userpoint:
                _LSEap_report_badusage(args[0],codeloc,tinfo[0])

            params = subinst[0].params
            if not params or params == "void": nparam = 0
            else: nparam = string.count(params,",")+1
            if len(args)!=nparam+1:
                LSEap_report_badargs(args[0],nparam,tinfo[0])
            
            if nparam > 0:
                rv = [ LSEtk_TokenIdentifier(LSEcg_codefunc_name(subinst[0],
                                                                 1)),
                       LSEtk_TokenPList([LSEtk_TokenOther("LSEfw_f_call ")]
                                        +
                                        reduce(lambda x,y: x + \
                                               [ LSEtk_TokenOther(",") ] +
                                               y.contents,
                                               args[2:], args[1].contents))
                       ]
            else:
                rv = [ LSEtk_TokenIdentifier(LSEcg_codefunc_name(subinst[0],
                                                                 1)),
                       LSEtk_TokenPList([ LSEtk_TokenOther(
                                          "LSEfw_f_call_void")])
                       ]

            return rv
        else:
            if codeloc != LSEcg_in_controlpoint:
                _LSEap_report_badusage(args[0],codeloc,tinfo[0])
                
            if (len(args) != 2 or len(args[1].contents)):
                raise LSEtk_TokenizeException(args[0] + " should have an empty"
                                              " argument list",tinfo[0])

            rv = [ LSEtk_TokenIdentifier(LSEcg_codefunc_name(subinst[0],1)),
                   LSEtk_TokenPList([ LSEtk_TokenOther("LSEfw_f_call "),
                                      LSEtk_TokenIdentifier("instno"),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenIdentifier("istatus"),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenIdentifier("ostatus"),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenIdentifier("id"),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenIdentifier("data"),
                                      ])
                   ]
            return rv

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

#######
# LSE_controlpoint_call_empty()
#######
def _LSEap_controlpoint_call_empty(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc != LSEcg_in_controlpoint:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])

        if (len(args)!=2 or len(args[1].contents)):
            raise LSEtk_TokenizeException(args[0] + " should have an empty"
                                          " argument list",tinfo[0])

        return [ LSEtk_TokenOther("((istatus & (LSEty_signal_enable_mask | " \
                                  "LSEty_signal_data_mask)) | " \
                                  "(ostatus & LSEty_signal_ack_mask))") ]

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

#######
# LSE_data_cancel(type, pointer to data)
#######
def _LSEap_data_cancel(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 3): LSEap_report_badargs(args[0],2,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (ttok,argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")

        tname = ttok.contents
        if currinst: # handle type aliases
            aname = currinst.typeMap.get(tname)
            if aname: tname = aname
        
        if tname == "LSE_dynid_t":
            return "LSE_dynid_cancel(*(\037%s\036))" % str(args[2])
        elif tname == "LSE_resolution_t":
            return "LSE_resolution_cancel(*(\037%s\036))" % str(args[2])
        else:
            tentry = db.typeBEMap.get(tname)
            if tentry and tentry[7][0]:
                return "%s_cancel(\037%s\036);" % (tentry[2], str(args[2]))
            else:
                return [] # nothing to be done
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []
            
#######
# LSE_data_copy(type, pointer to dest, pointer to source)
#######
def _LSEap_data_copy(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 4): LSEap_report_badargs(args[0],3,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (ttok,argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")

        tname = ttok.contents
        if currinst: # handle type aliases
            aname = currinst.typeMap.get(tname)
            if aname: tname = aname

        if tname == "LSE_type_none": return []
        else:
            s = """do {
            \037%s\036 *LSE_dp=(\037%s\036);
            *(\037%s\036) = *LSE_dp;
            """ % (tname,str(args[3]),str(args[2]))
            if tname == "LSE_dynid_t":
                s = s + "if (*LSE_dp) LSE_dynid_register(*LSE_dp)\n"
            elif tname == "LSE_resolution_t":
                s = s + "if (*LSE_dp) LSE_resolution_register(*LSE_dp)\n"
            else:
                tentry = db.typeBEMap.get(tname)
                if tentry and tentry[7][0]:
                    s = s + "%s_register(LSE_dp);\n" % (tentry[2])
            s = s + "}while(0)\n"
            return s

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

#######
# LSE_data_move(type, pointer to dest, pointer to source)
#######
def _LSEap_data_move(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 4): LSEap_report_badargs(args[0],3,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (ttok,argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")

        tname = ttok.contents
        if currinst: # handle type aliases
            aname = currinst.typeMap.get(tname)
            if aname: tname = aname

        if tname != "LSE_type_none":
            return "do{\037*(%s) = *(%s);\036}while(0)" % \
                   (str(args[2]),str(args[3]))
        else:
            return [] # nothing to be done

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []
            
#######
# LSE_data_register(type, pointer to data)
#######
def _LSEap_data_register(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 3): LSEap_report_badargs(args[0],2,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (ttok,argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")

        tname = ttok.contents
        if currinst: # handle type aliases
            aname = currinst.typeMap.get(tname)
            if aname: tname = aname

        if tname == "LSE_dynid_t":
            return "LSE_dynid_register(\037*%s\036)" % str(args[2])
        elif tname == "LSE_resolution_t":
            return "LSE_resolution_register(\037*%s\036)" % str(args[2])
        else:
            tentry = db.typeBEMap.get(tname)
            if tentry and tentry[7][0]:
                return "%s_register(\037%s\036);" % (tentry[2], str(args[2]))
            else:
                return [] # nothing to be done

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []
            
            
#######
# LSE_dynid_clone()
#######
def _LSEap_dynid_clone(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()

    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 2): LSEap_report_badargs(args[0],1,tinfo[0])
        if db.getParmVal('LSE_mp_multithreaded'):
            thtok = [ LSEtk_TokenOther("LSEfw_tf_call ") ]
        else: thtok = []
        return [ LSEtk_TokenOther("LSEdy_dynid_internal_clone(") ] + \
               thtok + args[1].contents + \
               [ LSEtk_TokenOther(",LSE_instance_name,__FILE__,__LINE__)") ]

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []


#######
# LSE_dynid_cancel(dynid)
#######
def _LSEap_dynid_cancel(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()

    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 2): LSEap_report_badargs(args[0],1,tinfo[0])

        return [ LSEtk_TokenOther("LSEdy_dynid_internal_cancel(") ] + \
               args[1].contents + \
               [ LSEtk_TokenOther(",LSE_instance_name,__FILE__,__LINE__)") ]

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

#######
# LSE_dynid_create()
#######
def _LSEap_dynid_create(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()

    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 2): LSEap_report_badargs(args[0],1,tinfo[0])

        if db.getParmVal('LSE_mp_multithreaded'):
            thtok = [ LSEtk_TokenOther("LSEfw_tf_call ") ]
        else: thtok = []
        return [ LSEtk_TokenOther("LSEdy_dynid_internal_create(") ] + \
               thtok + \
               [ LSEtk_TokenOther("LSE_instance_name, __FILE__,__LINE__)") ]

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []


#######
# LSE_dynid_element_ptr(dynid, element)
# LSE_resolution_element_ptr(resolution, element)
#######
def _LSEap_struct_element_ptr(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    rval = _LSEap_struct_get(tinfo,args,extra,env)
    if rval:
        if type(rval) is types.StringType:
            return [ LSEtk_TokenOther("&" + rval) ] 
        else:
            rval[0:0] = [ LSEtk_TokenOther("&") ]
            return rval
    
#######
# LSE_dynid_element_ptr(dynid, element)
# LSE_dynid_get(dynid, element)
# LSE_resolution_element_ptr(resolution, element)
# LSE_resolution_get(resolution, element)
#
#  In either case, the element argument is the one that needs parsing.
#   The forms allowed are:
#        field ':' [ module instance ':' ] <field name> ...
#        attr ':' [ domain instance ':' ] <field name> ...
#        <field name> ...
#   Any parenthesis opened at the beginning of the element must completely
#   enclose the field name and no parenthesis may be opened between that
#   point and the field name.  Thus, the following are not legal:
#     (field:)inst:mine
#     field:(inst):mine
#   but the following is legal:
#     (field:inst:mine)[34]
#######
def _LSEap_struct_get(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)!=3): LSEap_report_badargs(args[0],2,tinfo[0])

        s=args[0][4:string.index(args[0],"_",4)]
        s = "LSE_" + s + "_t" # get the structure name...

        # just get rid of left-parenthesis from the element name; they should
        # not be needed to group anything properly...
        try:
            _LSEap_remove_left_paren(args[2])
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in element name")

        argparent = args[2]
        pstack = []
        useargs = argparent.contents
        
        # look for fields....
        
        if len(useargs)>=2 and useargs[1].contents == ":" and \
           useargs[0].contents == "field": # we are dealing with a field!

            del argparent.contents[0:2] # remove the "field:" tag

            (inst,fname,argparent,pstack,
             reparse) = _LSEap_translate_field_ref(s, args, argparent,
                                                   pstack, db, currinst,
                                                   codeloc, subinst)
            
            args[0] = (args[0], "field", inst, fname)

            if reparse:
                return str(args[2]) # is this wise?
            else:
                return args[2].contents

        # look for attributes
        elif len(useargs)>=2 and useargs[1].contents == ":" and \
           useargs[0].contents == "attr": # we are dealing with an attribute!

            del argparent.contents[0:2] # remove the "attr:" tag

            (diname, argparent, pstack,
             reparse) = _LSEap_translate_attr_ref(s, args, argparent,
                                                  pstack, db, currinst)

            args[0] = (args[0], "attr", diname, "")

            if reparse:
                return str(args[2]) # is this wise?
            else:
                return args[2].contents

        # blank?
        elif not len(useargs) or isinstance(useargs[0],LSEtk_TokenEmpty):
            raise LSEap_Exception("Empty element name")
        else:
            if not isinstance(useargs[0],LSEtk_TokenIdentifier):
                raise LSEap_Exception("Syntax error in element name")
            try:
                doer = _LSEap_struct_elements[s][useargs[0].contents]
            except:
                raise LSEap_Exception("Cannot find element '%s' of %s" \
                                      % (useargs[0].contents,s))

            if doer: # do call
                nt = doer(args, argparent, useargs, None)
                if nt: return nt
            # want to put the dereference inside the current parenthesis
            nt = LSEtk_TokenPList([LSEtk_TokenPList(args[1].contents),
                                   LSEtk_TokenOther("-"),
                                   LSEtk_TokenOther(">"),
                                   useargs[0],
                                   ])
            
            argparent.contents[0] = nt

            args[0] = (args[0], "other", None, useargs[0].contents)
            
            return args[2].contents # argument after transformation...
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        args[0] = (args[0], "", None, None)
        _LSEap_report_error(v,args[0][0],tinfo[0],rexc)
        return []

#######
# LSE_dynid_recreate(dynid)
#######
def _LSEap_dynid_recreate(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()

    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 2): LSEap_report_badargs(args[0],1,tinfo[0])

        return [ LSEtk_TokenOther("LSEdy_dynid_internal_recreate(") ] + \
               args[1].contents + \
               [ LSEtk_TokenOther(",LSE_instance_name, __FILE__,__LINE__)") ]

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

#######
# LSE_dynid_register(dynid)
#######
def _LSEap_dynid_register(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()

    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args) != 2): LSEap_report_badargs(args[0],1,tinfo[0])

        return [ LSEtk_TokenOther("LSEdy_dynid_internal_register(") ] +\
               args[1].contents + \
               [ LSEtk_TokenOther(",LSE_instance_name,__FILE__,__LINE__)") ]

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []


#######
# LSE_dynid_set(dynid, element, value)
# LSE_resolution_set(resolution, element, value)
#
#  See the notes for LSE_dynid_get
#######
def _LSEap_struct_set(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)!=4): LSEap_report_badargs(args[0],3,tinfo[0])

        s=args[0][4:string.index(args[0],"_",4)]
        s = "LSE_" + s + "_t" # get the structure name...

        # just get rid of left-parenthesis from the element name; they should
        # not be needed to group anything properly...
        try:
            _LSEap_remove_left_paren(args[2])
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in element name")

        argparent = args[2]
        pstack = []
        useargs = argparent.contents
        
        # look for fields....
        
        if len(useargs)>=2 and useargs[1].contents == ":" and \
           useargs[0].contents == "field": # we are dealing with a field!

            del argparent.contents[0:2] # remove the "field:" tag

            (inst,fname,argparent,pstack,
             reparse) = _LSEap_translate_field_ref(s, args, argparent,
                                                   pstack, db, currinst,
                                                   codeloc, subinst)
            
            args[0] = (args[0], "field", inst, fname)

            if reparse:
                return "do { %s = (\037%s\036);} while(0)" % (args[2],args[3])
            else:
                rt = LSEtk_TokenPList(args[3].contents)
                rt.begWhite = " "
                rt = LSEtk_TokenCBList([ args[2],
                                         LSEtk_TokenOther("="," "),
                                         rt, LSEtk_TokenOther(";", "") ])
                rt.begWhite = " "
                rt = LSEtk_TokenTopLevel([ LSEtk_TokenIdentifier("do"),
                                           rt, LSEtk_TokenOther("while(0)")])
                return rt
                                         
        # look for attributes
        elif len(useargs)>=2 and useargs[1].contents == ":" and \
           useargs[0].contents == "attr": # we are dealing with an attribute!

            del argparent.contents[0:2] # remove the "attr:" tag

            (diname, argparent, pstack,
             reparse) = _LSEap_translate_attr_ref(s, args, argparent,
                                                  pstack, db, currinst)

            args[0] = (args[0], "attr", diname, "")

            if reparse:
                return "do { %s = (\037%s\036);} while(0)" % (args[2],args[3])
            else:
                rt = LSEtk_TokenPList(args[3].contents)
                rt.begWhite = " "
                rt = LSEtk_TokenCBList([ args[2],
                                         LSEtk_TokenOther("="," "),
                                         rt, LSEtk_TokenOther(";") ])
                rt.begWhite = " "
                rt = LSEtk_TokenTopLevel([ LSEtk_TokenIdentifier("do"),
                                           rt, LSEtk_TokenOther("while(0)")])

                return rt

        # blank?
        elif not len(useargs) or isinstance(useargs[0],LSEtk_TokenEmpty):
            raise LSEap_Exception("Empty element name")
        
        else:
            if not isinstance(useargs[0],LSEtk_TokenIdentifier):
                raise LSEap_Exception("Syntax error in element name")
            try:
                doer = _LSEap_struct_elements[s][useargs[0].contents]
            except:
                raise LSEap_Exception("Cannot find element '%s' of %s" \
                                      % (useargs[0].contents,s))

            if doer: # do call
                nt = doer(args, argparent, useargs, None)
                if nt: return nt
            # want to put the dereference inside the current parenthesis
            nt = LSEtk_TokenPList([LSEtk_TokenPList(args[1].contents),
                                   LSEtk_TokenOther("-"),
                                   LSEtk_TokenOther(">"),
                                   useargs[0],
                                   ])

            args[0] = (args[0], "other", None, useargs[0].contents)

            argparent.contents[0] = nt
            rt = LSEtk_TokenOther("="); rt.begWhite = " "
            rt2 = LSEtk_TokenPList(args[3].contents); rt2.begWhite = " "
            rt = LSEtk_TokenCBList([ args[2], rt, rt2, LSEtk_TokenOther(";") ])
            rt = LSEtk_TokenTopLevel([ LSEtk_TokenIdentifier("do"),
                                       rt, LSEtk_TokenOther("while(0)")])

            return rt
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        args[0] = (args[0], "", None, None)
        _LSEap_report_error(v,args[0][0],tinfo[0],rexc)
        return []

    return None

#######
# LSE_enum_value(typealias,element)
#######
def _LSEap_enum_value(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
        if (len(args)!=3): LSEap_report_badargs(args[0],2,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (ttok, argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in type name")
        try:
            _LSEap_remove_left_paren(args[2])
            argparent = args[2]
            pstack = []
            (vtok, argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in value")

       # does the type exist?
        tm = currinst.typeMap.get(ttok.contents)
        if not tm:
            raise LSEap_Exception("Unable to find type alias '%s'" %
                                  ttok.contents)

        # does the enumerated value exist?
        ev = db.typeBEMap[tm][6].get(vtok.contents)
        if not ev:
            if db.typeBEMap[tm][6] == {}:
                raise LSEap_Exception("Type '%s' is not an enumerated type" %
                                      ttok.contents)
            else:
                raise LSEap_Exception("Enumerated type '%s' does not have "
                                      "value '%s'" % \
                                      (ttok.contents,vtok.contents))

        return [
            LSEtk_TokenIdentifier("%s__%s" % (tm,vtok.contents))
            ]
    
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEkt_TokenNumber("0") ]

########
# LSE_event_filled(event)
########
def _LSEap_event_filled(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
            
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, etok, argparent, pstack) = \
                   _LSEap_parse_hname_colon_hname(argparent,pstack,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in event name")

        try:
            if not itok:
                if currinst: ev = currinst.getEvent(etok.contents)
                else: ev = db.getEvent("",etok.contents)
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name "
                                      "in a .clm file")
            else:
                ev = db.getEvent(itok.contents,etok.contents)
                iname = itok.contents
                # only allow calls to parent events!
                if not currinst or currinst.name[:len(iname)] != iname:
                    raise LSEap_Exception("LSE_event_filled can only be "
                                          "called on self or parent instances")

        except LSEdb_Exception, v:
            # but maybe there was a port alias????
            # This code may actually be moot; the only code that could ask
            # about this event would force the adapter to actually exist,
            # thus removing the port alias?
            rloc = string.rfind(etok.contents,".")
            if currinst and rloc >= 0:
                lname = etok.contents[:rloc]
                rname = etok.contents[rloc+1:]
                #sys.stderr.write("%s %s\n" % (lname, rname))
                p = db.getPortAlias(currinst.name,lname,1)
                if p and rname in ['data','nodata','enable','ack',
                                   'localdata', 'localnodata', 'localenable',
                                   'localack' ]:
                    # must be a port alias that did not get an event...
                    return [ LSEtk_TokenNumber("0") ]
            raise LSEap_Exception(str(v))

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]

    return [ LSEtk_TokenNumber(str(int(ev.isFilled))) ]

#######
# LSE_event_record(name,args....)
#######
def _LSEap_event_record(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)<2):
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, etok, argparent, pstack) = \
                   _LSEap_parse_hname_colon_hname(argparent,pstack,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in event name")

        try:
            if not itok:
                if currinst:
                    ev = currinst.multiInst.getEvent(etok.contents)
                    iname = currinst.name # not multiinst or we get bad calls
                else:
                    ev = db.getEvent("",etok.contents)
                    iname = ""
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name "
                                      "in a .clm file")
            else:
                ev = db.getEvent(itok.contents,etok.contents)
                iname = itok.contents
                # only allow calls to parent events!
                if not currinst or currinst.name[:len(iname)] != iname:
                    raise LSEap_Exception("LSE_event_record can only be "
                                          "called on self or parent instances")

        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

        if len(args) != ev.numArgs + 2:
            LSEap_report_badargs(args[0],ev.numArgs+1,tinfo[0])

        if not ev.isFilled:
            args[0] = (args[0], "","",None)
            return [] # drop it if no data collectors...

        # calls to self can be expanded immediately
        if iname == currinst.name:
            if ev.numArgs > 0:
                callparm = [ LSEtk_TokenIdentifier(" LSEfw_f_call ") ]
            else:
                callparm = [ LSEtk_TokenIdentifier(" LSEfw_f_call_void") ]
            isExt = 0
            calltok = LSEtk_TokenIdentifier(LSEcg_event_name(ev,"record",
                                                             isExt))
        else: # calls to others may be deferred
            isExt = 1
            calltok = LSEtk_TokenIdentifier(LSEcg_event_name(ev,"record",
                                                             isExt))
            if ev.numArgs > 0:
                callparm = [ LSEtk_TokenOther("(LSEfw_iptr_t)&%s" 
                                              % LSEcg_instdata_name(iname)),
                             LSEtk_TokenIdentifier(" LSEfw_tf_callc ") ]
            else:
                callparm = [ LSEtk_TokenOther("(LSEfw_iptr_t)&%s"
                                              % LSEcg_instdata_name(iname)),
                             LSEtk_TokenIdentifier(" LSEfw_tf_callc_void") ]


        # calls to self and calls in .clm resolve immediately; all others
        # get deferred...
        
        if ev.numArgs > 0:
            rv = [ calltok, 
                   LSEtk_TokenPList(callparm + \
                                    reduce(lambda x,y: x + \
                                           [ LSEtk_TokenOther(",") ] + \
                                           y.contents,
                                           args[3:], args[2].contents))
                   ]
        else:
            rv = [ calltok, LSEtk_TokenPList(callparm) ]

        args[0] = (args[0],iname,ev)

        if isExt and codeloc != LSEcg_in_clm and codeloc < LSEcg_in_domaincode:
            # Take out the identifier and instance pointer, remembering
            # what we took out.  Put in the signature of the event; this
            # forces us to merge code only when signatures match.
            key = LSEcg_fkey(codeloc, subinst, LSEcg_fkey_event_record)
            fl = currinst.toFinish.setdefault(key,[])
            for f in fl:
                if f[1] == iname and f[2] == ev.name:
                    idno = f[0]
                    break
            else:
                idno = len(fl)
                fl.append([idno, iname, ev.name, 0])
                
            rv[0].contents = "LSEfi_er"
            #rv[1].contents[0].contents = ev.argString
            rv[1].contents[0] = LSEtk_TokenOther("{%d,%s}" % \
                                                 (idno,ev.argString))

        return rv
        
    except (LSEap_Exception,LSEtk_TokenizeException),v:
        args[0] = (args[0], "","",None)
        _LSEap_report_error(v,args[0][0],tinfo[0],rexc)
        return []

# we determine how to implement the call by how similar the different
# call sites are to one another.
# same instance and event:       direct call with direct pointer
# same multi-instance and event: direct call through pointer from table
# otherwise:                     indirect call through pointer from table
def _LSEap_DECIDE_event_record(db, fkey, tfrecs):
    (idno, iname, ename, impltype) = tfrecs[0]
    miname = db.getInst(iname).multiInst.name
    sameinst = reduce(lambda x,y:x and y[1]==iname,tfrecs,1)
    sameevent = reduce(lambda x,y:x and y[2]==ename,tfrecs,1)
    if sameevent and sameinst: impl = 0 # all the same
    elif reduce(lambda x,y:x and db.getInst(y[1]).multiInst.name==miname,
                tfrecs, 1):
        impl = 1 # same multi-inst
    else:
        impl = 2 # different functions
    for f in tfrecs:
        f[3] = impl
        
def _LSEap_REPLACE_event_record(db, key, tfrec):
    (idno, iname, ename, impltype) = tfrec
    ev = db.getEvent(iname,ename)
    if impltype==0:
        return "%s((LSEfw_iptr_t) &%s " % (LSEcg_event_name(ev,"record",1),
                                           LSEcg_instdata_name(iname))
    elif impltype==1:
        # TODO: point at multi-inst
        return "%s(%s " % (LSEcg_event_name(ev,"record",1),
                           LSEcg_apivar_name(key,idno,"ptr"))
    else:
        return "%s(%s " % (LSEcg_apivar_name(key,idno,"func"),
                           LSEcg_apivar_name(key,idno,"ptr"))

def _LSEap_PIECES_event_record(db, key, tfrec, part):
    (idno, iname, ename, impltype) = tfrec
    if impltype==1:
        vname = LSEcg_apivar_name(key,idno,"ptr")
        if part == 0: # var defs
            return "LSEfw_iptr_t %s;" % vname
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:                
                return ("LSEfw_iptr_t LSE_%s" % vname,
                        "%s = LSE_%s;" % (vname,vname),"") 
            else:
                return ("LSEfw_iptr_t LSE_%s" % vname, "",
                        "%s(LSE_%s)" % (vname,vname))
        else: # instantiation
            return "(LSEfw_iptr_t)&%s" % LSEcg_instdata_name(iname)
    elif impltype==2:
        ev = db.getEvent(iname,ename)
        fname = LSEcg_apivar_name(key,idno,"func")
        vname = LSEcg_apivar_name(key,idno,"ptr")
        if ev.argString != "void": ps = "LSEfw_stf_def " + ev.argString
        else: ps = "LSEfw_stf_def_void"
        if part == 0: # var defs
            return "void (*%s)(%s);\nLSEfw_iptr_t %s;" % (fname,ps,vname)
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:                
                return ("void (*LSE_%s)(%s),\nLSEfw_iptr_t LSE_%s" % 
                        (fname,ps,vname),
                        "%s = LSE_%s;\n%s = LSE_%s;" % \
                        (fname,fname,vname,vname),
                        "")
            else:
                return ("void (*LSE_%s)(%s),\nLSEfw_iptr_t LSE_%s" % 
                        (fname,ps,vname),
                        "",
                        "%s(LSE_%s),\n%s(LSE_%s)" % \
                        (fname,fname,vname,vname))
        else: # instantiation
            return "%s,(LSEfw_iptr_t)&%s" % \
                   (LSEcg_event_name(ev,"record",1),
                    LSEcg_instdata_name(iname))
    return None
    
#######
# LSE_LOOP_END
#######
def _LSEap_loop_end(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

    return "}"

#######
# LSE_LOOP_OVER_PORT(varname, instance : port)
#######
def _LSEap_loop_over_port(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)!=3): LSEap_report_badargs(args[0],2,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[2])
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in port name")
        argparent = args[2]
        pstack = []

        try:
            (itok, ptok, argparent,
             pstack) = _LSEap_parse_hname_colon_hname(argparent, pstack,0)
        except LSEap_Exception, v:
            raise LSEap_Exception("Unable to parse port name")
        
        try:
            if not itok:
                if currinst:
                    iname = currinst.name
                else:
                    raise LSEap_Exception("Must specify instance name")
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name in "
                                      "a .clm file")
            else: iname = itok.contents
            p = db.getPort(iname,ptok.contents,0)
            width = p.width
        except LSEdb_Exception, v:
            # maybe the port was an alias....
            p = db.getPortAlias(iname,ptok.contents,0)
            if not p:
                raise LSEap_Exception(str(v))
            width = len(p[1]) # aliases' port size is its list size
            
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return "{" # so we do not get a mismatch on the end

    args[0] = (args[0],iname, ptok.contents,p)
    return "{ int \037%s\036; for (\037%s=0;%s<%d;%s++\036)" % \
           (args[1],args[1],args[1],width,args[1])

#######
# LSE_eval(expr)
#######
def _LSEap_eval(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()

    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])

        if (len(args)<1):
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])
        
        if len(args)>1:
            argstr = reduce(lambda x,y: x + "," + str(y),
                            args[2:], str(args[1]))
        else: argstr = ""

        bdict = {}
        try:
            rval = eval(argstr,bdict,bdict)
        except Exception:
            raise LSEtk_TokenizeException("Unable to evaluate expression '%s'"
                                          "in %s" % (argstr,args[0]),tinfo[0])

        return str(rval)

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

########
# LSE_method_call(instance, query, args)
########
def _LSEap_method_call(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc not in [LSEcg_in_userpoint, LSEcg_in_controlpoint,
                           LSEcg_in_extension,
                           LSEcg_in_datacoll, LSEcg_in_modulebody]:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)<2):
            # we raise a LSEtk_TokenizeException so we get nice line #s 
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, qtok, argparent,
             pstack) = _LSEap_parse_hname_colon_hname(argparent, pstack,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in method name")

        try:
            if not itok:
                if currinst:
                    iname = currinst.name
                else:
                    raise LSEap_Exception("Must specify instance name")
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name "
                                      "in a .clm file")
            else:
                iname = itok.contents
                
            #if codeloc == LSEcg_in_modulebody and \
            #   iname[:len(currinst.name)] != currinst.name:
            #   raise LSEap_Exception("Cannot call a non-descendant instance's"
            #                          " method from a module body")
                
            qname = qtok.contents
            q = db.getMethod(iname,qname)

            if q.locked and q.inst != currinst:
                raise LSEap_Exception("Method '%s' of instance '%s' "
                                      "can only be called from "
                                      "within its own instance" % \
                                      (iname,qname))
            
            params = q.params
            if not params or params == "void": nparam = 0
            else: nparam = string.count(params,",")+1
            if len(args)!=nparam+2:
                if nparam:
                    raise LSEtk_TokenizeException(args[0] + " of " +
                                                  iname + ":" + qname +
                                                  " should have %d arguments"\
                                                  % (nparam + 1) + "args:" +
                                                  ("%s" % args[1:]), tinfo[0])
                else:
                    raise LSEtk_TokenizeException(args[0] + " of " +
                                                  iname + ":" + qname +
                                                  " should have 1 argument",
                                                  tinfo[0])

            if iname == currinst.name:
                if nparam > 0:
                    callparm = [ LSEtk_TokenIdentifier(" LSEfw_f_call ") ]
                else:
                    callparm = [ LSEtk_TokenIdentifier(" LSEfw_f_call_void") ]
                isExt = 0
            else:
                if nparam > 0:
                    callparm = [ LSEtk_TokenOther("(LSEfw_iptr_t)&%s" % \
                                                  LSEcg_instdata_name(iname)),
                                 LSEtk_TokenIdentifier(" LSEfw_tf_callc ") ]
                else:
                    callparm = [ LSEtk_TokenOther("(LSEfw_iptr_t)&%s" % \
                                                  LSEcg_instdata_name(iname)),
                                 LSEtk_TokenIdentifier(" LSEfw_tf_callc_void")
                                 ]
                isExt = 1

        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

        # should check on number of arguments, and all sorts of goodies...
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        args[0] = (args[0], "","",None)
        _LSEap_report_error(v,args[0][0],tinfo[0],rexc)
        return []

    args[0] = (args[0],iname,q)

    # arg1, arg2, ...
    rv = callparm
    if nparam > 0:
        rv.extend(reduce(lambda x,y: x + \
                         [ LSEtk_TokenOther(",") ] + y.contents,
                         args[3:], args[2].contents))
    
    # queryname(arg1, arg2, ...)
    rv = [ LSEtk_TokenIdentifier(\
             LSEcg_method_name(LSEcg_iname_transform(db,iname),qname,isExt)),
           LSEtk_TokenPList(rv),
           ]
    
    if isExt: # will be in good location
        key = LSEcg_fkey(codeloc, subinst, LSEcg_fkey_method_call)
        fl = currinst.toFinish.setdefault(key,[])
        for f in fl:
            if f[1] == iname and f[2] == q.name:
                idno = f[0]
                break
        else:
            idno = len(fl)
            fl.append([idno, iname, q.name, 0])
            
        rv[0].contents = "LSEfi_mc"
        #rv[1].contents[0].contents = q.returnType + ":" + q.params
        rv[1].contents[0] = LSEtk_TokenOther("{%d,%s,%s}" % \
                                             (idno,q.returnType,q.params))
        
    return rv

# we determine how to implement the call by how similar the different
# call sites are to one another.
# same instance and event:       direct call with direct pointer
# same multi-instance and event: direct call through pointer from table
# otherwise:                     indirect call through pointer from table
def _LSEap_DECIDE_method_call(db, fkey, tfrecs):
    (idno, iname, qname, impltype) = tfrecs[0]
    miname = db.getInst(iname).multiInst.name
    sameinst = reduce(lambda x,y:x and y[1]==iname,tfrecs,1)
    samemethod = reduce(lambda x,y:x and y[2]==qname,tfrecs,1)
    if samemethod and sameinst: impl = 0 # all the same
    elif reduce(lambda x,y:x and db.getInst(y[1]).multiInst.name==miname,
                tfrecs, 1):
        impl = 1 # same multi-inst
    else:
        impl = 2 # different functions
    for f in tfrecs:
        f[3] = impl
        
def _LSEap_REPLACE_method_call(db, key, tfrec):
    (idno, iname, qname, impltype) = tfrec
    if impltype==0:
        return "%s((LSEfw_iptr_t)&%s " % \
               (LSEcg_method_name(LSEcg_iname_transform(db,iname),qname,1),
                LSEcg_instdata_name(iname))
    elif impltype==1:
        return "%s(%s " % (LSEcg_method_name(LSEcg_iname_transform(db,iname),
                                             qname,1),
                           LSEcg_apivar_name(key,idno,"ptr"))
    else:
        return "%s(%s " % (LSEcg_apivar_name(key,idno,"func"),
                           LSEcg_apivar_name(key,idno,"ptr"))

def _LSEap_PIECES_method_call(db, key, tfrec, part):
    (idno, iname, qname, impltype) = tfrec
    if impltype==1:
        vname = LSEcg_apivar_name(key,idno,"ptr")
        if part == 0: # var defs
            return "LSEfw_iptr_t %s;" % vname
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:                
                return ("LSEfw_iptr_t LSE_%s" % vname,
                        "%s = LSE_%s;" % (vname,vname),"")
            else:
                return ("LSEfw_iptr_t LSE_%s" % vname,
                        "", "%s(LSE_%s)" % (vname, vname))
        else: # instantiation
            return "(LSEfw_iptr_t)&%s" % LSEcg_instdata_name(iname)
    elif impltype==2:
        q = db.getMethod(iname,qname)
        fname = LSEcg_apivar_name(key,idno,"func")
        vname = LSEcg_apivar_name(key,idno,"ptr")
        if q.params != "void": ps = "LSEfw_stf_def " + q.params
        else: ps = "LSEfw_stf_def_void"
        if part == 0: # var defs
            return "%s (*%s)(%s);\nLSEfw_iptr_t %s;" % (q.returnType,
                                                        fname,ps,vname)
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:                
                return ("%s (*LSE_%s)(%s),\nLSEfw_iptr_t LSE_%s" % 
                        (q.returnType, fname,ps,vname),
                        "%s = LSE_%s;\n%s = LSE_%s;" % \
                        (fname,fname,vname,vname),
                        "")
            else: 
                return ("%s (*LSE_%s)(%s),\nLSEfw_iptr_t LSE_%s" % 
                        (q.returnType, fname,ps,vname),
                        "",
                        "%s(LSE_%s),\n%s(LSE_%s)" % \
                        (fname,fname,vname,vname))
               
        else: # instantiation
            return "%s,(LSEfw_iptr_t)&%s" % \
                   (LSEcg_method_name(LSEcg_iname_transform(db,iname),qname,1),
                    LSEcg_instdata_name(iname))
    return None
    
########
# LSE_method_used(method)
########
def _LSEap_method_used(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
            
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (qtok, argparent, pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")

        try:
            if not currinst:
                raise LSEtk_TokenizeException(args[0] + " cannot be called"
                                              " outside of a module instance",
                                              tinfo[0])
            q = db.getMethod(currinst.name,qtok.contents)
            uses = currinst.calledby.has_key(('LSE_method_call',q.name))

        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]

    return [ LSEtk_TokenNumber(str(int(uses))) ]

#######
# LSE_nameequal(name1,name2)
#######
def _LSEap_nameequal(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()

    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])

        if (len(args)!=3): LSEap_report_badargs(args[0],2,tinfo[0])

        str1 = string.strip(str(args[1]))
        try:
            nstr = currinst.typeMap[str1]
            str1 = nstr
        except:
            pass
        str2 = string.strip(str(args[2]))
        try:
            nstr = currinst.typeMap[str2]
            str2 = nstr
        except:
            pass

        return [ LSEtk_TokenNumber(str(int(str1 == str2))) ]

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]

######
# LSE_parm_constant(instance : parameter)
######
def _LSEap_parm_constant(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in parameter name")
        argparent = args[1]
        pstack = []
        
        try:
            (itok, ptok, argparent,
             pstack) = _LSEap_parse_hname_colon_sname(argparent, pstack,0)
        except LSEap_Exception, v:
            raise LSEap_Exception("Unable to parse parameter name")
        
        try:
            if not itok:
                if currinst:
                    inst = currinst
                else:
                    # we do not search top-level for PARM macros;
                    # top-level PARMs must be done with ${}
                    raise LSEap_Exception("Must specify instance name")
            else:
                inst = db.getInst(itok.contents)
            pname = ptok.contents
            pval = inst.getParm(pname,1)
            if pval == None:
                raise LSEap_Exception("Unable to find parameter '%s' "
                                      "in instance '%s'" % (pname,inst.name))
            return [ LSEtk_TokenNumber(str(int(not pval[2]))) ]
        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

    #return str(pval) # important.... allows the contents to be reparsed

########
# LSE_port_connected(instance : port [ portno ])
########
def _LSEap_port_connected(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, ptok, portnotok, sigtok, argparent,
             pstack) = _LSEap_parse_port_name(argparent, pstack,0)
            if sigtok: raise LSEap_Exception("")
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in port name")

        try:
            if not itok:
                if currinst:
                    iname = currinst.name
                else:
                    raise LSEap_Exception("Must specify instance name")
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name "
                                      "in a .clm file")
            else: iname = itok.contents
            pname = ptok.contents
            p = db.getPort(iname,pname)
            nc = p.getNumConnected()
            if not portnotok and codeloc > LSEcg_in_text:
                raise LSEtk_TokenizeException(args[0] + " with a port index"
                                              " cannot be used " +
                                              LSEcg_locnames[codeloc],
                                              tinfo[0])
            if not p.width or nc==0:
                rv = [ LSEtk_TokenPList([ LSEtk_TokenNumber("0")]) ]
            else:
                if portnotok and nc != p.width:
                    # we do not specialize on a per-port basis because there
                    # is a range check involved; that requires an inlined
                    # function (so that we don't double-evaluate the porti
                    # expression, which could have side-effects).  But
                    # once there's inlining, we might as well use a generic
                    # inlined function...
                    
                    cstring = '"' + p.getConnString() + '"'
                    
                    # porti,width,conn,iname,pname
                    rv = portnotok.contents + \
                         [ LSEtk_TokenOther(","),
                           LSEtk_TokenNumber(str(p.width)),
                           LSEtk_TokenOther(","),
                           LSEtk_TokenQuote(cstring),
                           LSEtk_TokenOther(","),
                           LSEtk_TokenQuote('"' + iname + '"'),
                           LSEtk_TokenOther(","),
                           LSEtk_TokenQuote('"' + ptok.contents + '"'),
                           ]
                    # LSEfw_port_connected(porti,width,conn,iname,pname)
                    rv = [ LSEtk_TokenIdentifier("LSEfw_port_connected"),
                           LSEtk_TokenPList(rv) ]
                    rv = [ LSEtk_TokenPList(rv) ]
                else:
                    rv = [ LSEtk_TokenPList([ LSEtk_TokenNumber("1")] ) ]
        except LSEdb_Exception, v:
            # now, maybe the port was an alias....
            p = db.getPortAlias(iname,pname,1)
            # for now, I think all port instances of aliases must be connected
            rv = [ LSEtk_TokenPList([
                LSEtk_TokenNumber(str(int(not not filter(None, p[1])))) ])
                   ]
            
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

    return rv

########
# LSE_port_get(port [ porti ] . signal , idp, datapp)
#   a really fun one....
#     - no instance is allowed (for now)
#     - porti is required
#     - signal is optional
#     - id and value's presence depend upon signal and port type
########
def _LSEap_port_get(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc not in [LSEcg_in_clm, LSEcg_in_userpoint,
                           LSEcg_in_extension, 
                           LSEcg_in_funcheader, LSEcg_in_modulebody]:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
            
        if (len(args)<2): raise LSEap_Exception("Missing port name")

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, ptok, portnotok, stok, argparent,
             pstack) = _LSEap_parse_port_name(argparent, pstack,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in port name")

        if itok: raise LSEap_Exception("Cannot specify instance name")
        if not portnotok: raise LSEap_Exception("Must specify port instance")
        portnotok = LSEtk_TokenTopLevel(portnotok.contents)
        try:
            p = currinst.getPort(ptok.contents)
        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

        # check the signal name and argument length

        if stok:
            sname = stok.contents
            if sname not in ["data","enable","ack"]:
                raise LSEap_Exception("Invalid signal name '%s'" % sname)
            mask = "LSEty_signal_%s_mask & " % sname
            if sname == "data":
                if len(args) != 4:
                    raise LSEap_Exception("Missing id or data arguments")
            else:
                if len(args) != 2: raise LSEap_Exception("Extra arguments")
                args.extend(["NULL", "NULL"])
        else:
            mask = ""
            if len(args) != 4:
                raise LSEap_Exception("Missing id or data arguments")

        # and now do it....
        
        return [ LSEtk_TokenOther("(%s%s(%s,%s,%s))" % \
                                  (mask,
                                   LSEcg_portapi_name(p,"GET_DATA"),
                                   portnotok, args[2], args[3])) ]
                                  
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]

########
# LSE_port_num_connected(instance : port)
########
def _LSEap_port_num_connected(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, ptok, argparent,
             pstack) = _LSEap_parse_hname_colon_hname(argparent, pstack,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in port name")

        try:
            if not itok:
                if currinst:
                    iname = currinst.name
                else:
                    raise LSEap_Exception("Must specify instance name")
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name "
                                      "in a .clm file")
            else: iname = itok.contents
            pname = ptok.contents
            p = db.getPort(iname,pname)
            width = p.getNumConnected()
        except LSEdb_Exception, v:
            # now, maybe the port was an alias....
            p = db.getPortAlias(iname,pname,1)
            if not p: raise LSEap_Exception(str(v))
            width = len(filter(None, p[1]))

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]
        
    args[0] = (args[0],iname, pname, p) # include port/port alias for rebuild
    return [ LSEtk_TokenNumber(str(width)) ]

########
# LSE_port_query(instance : port [ porti ] . signal , idp, datapp)
#   a really fun one....
#     - porti is required
#     - signal is optional
#     - id and value's presence depend upon signal and port type
########
def _LSEap_port_query(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc not in [LSEcg_in_userpoint, LSEcg_in_controlpoint,
                           LSEcg_in_extension,
                           LSEcg_in_datacoll, LSEcg_in_modulebody]:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)<2): raise LSEap_Exception("Missing port name")

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, ptok, portnotok, stok, argparent,
             pstack) = _LSEap_parse_port_name(argparent, pstack,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in port name")

        if not portnotok: raise LSEap_Exception("Must specify port instance")
        portnotok = LSEtk_TokenTopLevel(portnotok.contents)

        try:
            if not itok:
                if currinst:
                    iname = currinst.name
                else:
                    raise LSEap_Exception("Must specify instance name")
            elif not codeloc:
                    raise LSEap_Exception("Cannot specify instance name "
                                          "in a .clm file")
            else: iname = itok.contents
            pname = ptok.contents
            p = db.getPort(iname,pname)

            if codeloc == LSEcg_in_modulebody and \
               iname[:len(currinst.name)] != currinst.name:
                raise LSEap_Exception("Cannot call a non-descendant instance's"
                                      " port query from a module body")

        except LSEdb_Exception, v:
            # now, maybe the port was an alias....
            p = db.getPortAlias(iname,pname,1)
            if not p: raise LSEap_Exception(str(v))

        # check the signal name and argument length
        
        if stok:
            sname = stok.contents
            if sname[0:5] == "local": spieces = ["local", sname[5:]]
            else: spieces = ["global", sname]
            if spieces[1] == "": spieces[1] = "any"

            if spieces[1] == "data" or spieces[1] == "any":
                if len(args) != 4:
                    raise LSEap_Exception("Missing id or data arguments")
            elif spieces[1] == "enable" or spieces[1] == "ack":
                if len(args) != 2: raise LSEap_Exception("Extra arguments")
            else:
                raise LSEap_Exception("Invalid signal name '%s'" % sname)
        else:
            spieces = ["global","any"]
            if len(args) != 4:
                raise LSEap_Exception("Missing id or data arguments")

        # no locals for empty control points or aliases
        if type(p) != types.InstanceType or p.controlEmpty:
            spieces[0] = "global"

        # if the port is not connected, then just insert the constant
        # values
        if type(p) == types.InstanceType and not p.width:
            rv = LSEtk_TokenPList([LSEtk_TokenIdentifier("LSE_signal_nothing"),
                                   LSEtk_TokenOther("|"),
                                 LSEtk_TokenIdentifier("LSE_signal_disabled"),
                                   LSEtk_TokenOther("|"),
                                   LSEtk_TokenIdentifier("LSE_signal_ack")])
            args[0] = (None, None, None, None, None, None)
            return rv
        
        args[0] = (args[0], iname, pname, p, spieces[0] + "." + spieces[1],
                   str(portnotok))

        # and now do it....
        indc = (codeloc == LSEcg_in_datacoll)

        # figure out the calling block number
        if not currinst or isinstance(subinst[0],LSEdb_Collector):
            cbn = str(0)
        elif isinstance(subinst[0],LSEdb_CodePoint) and \
                        subinst[0].type == LSEdb_PointControl:
            cno = db.cblocksBacklist[(LSEdb_CblockFiring,
                                      currinst.name,subinst[0].name,0)]
            cbn = "(instno+LSEfw_CBBASE+%d)" % (cno - currinst.firstCblock)
            #if db.getParmVal("LSE_specialize_codeblock_numbers"):
            #    cbn = "(instno+%d)" % cno
            #else:
            #    cbn = "(instno+%s+%d)" % (LSEcg_codeblock_base(currinst.name),
            #                              cno - currinst.firstCblock)
        else:
            cbn = "LSEfw_schedule_calling_cblock"

        if db.getParmVal("LSE_check_api_at_runtime"):
            checks = [ LSEtk_TokenOther("LSE_instance_name") +
                       LSEtk_TokenOther(",") +
                       LSEtk_TokenOther(p.name) +
                       LSEtk_TokenOther(",") ]
        else:
            checks = []

        mask = "LSEty_signal_%s_mask" % spieces[1]
        kmask = "LSEty_signal_%s_known_mask" % spieces[1]
        wuno = LSEcg_wunos[tuple(spieces)]

        if type(p) == types.InstanceType: # not an alias
            if p.controlEmpty: st = "LSEfw_PORT_empty_t"
            else: st = "LSEfw_PORT_nodef_t"
            pis = LSEcg_portstatus_name(iname,pname)
            pin = LSEcg_portinst_info_name(iname,pname)

            if spieces[1] == "any" or spieces[1] =="data":
                idp = args[2].contents
                datap = args[3].contents
            else:
                idp = [ LSEtk_TokenOther("NULL") ]
                datap = [ LSEtk_TokenOther("NULL") ]

            rv = [ LSEtk_TokenIdentifier("LSEmi_PQUERY_%s<%s,%s,%d>" %
                                         (spieces[0],p.type, st, p.width)),
                   LSEtk_TokenPList([ LSEtk_TokenOther("\n&%s[0]" % pis),
                                      LSEtk_TokenOther(",\n"),
                                      LSEtk_TokenOther("&%s[0]" % pin),
                                      LSEtk_TokenOther(",\n"),
                                      ] + checks +
                                    [
                                      LSEtk_TokenOther(cbn),
                                      LSEtk_TokenOther(",\n") ] +
                                    portnotok.contents +
                                    [ LSEtk_TokenOther(",\n"),
                                      LSEtk_TokenNumber("%d" % indc),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenOther(mask),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenOther(kmask),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenOther("%d" % wuno),
                                      LSEtk_TokenOther(",\n"),
                                      ] + idp +
                                    [ LSEtk_TokenOther(",") ] +
                                    datap)
                   ]

            # save the parameters; we will need them later!
            st = (rv[0].contents, pis, pin, st)
            key = LSEcg_fkey(codeloc, subinst, LSEcg_fkey_port_query)
            fl = currinst.toFinish.setdefault(key,[])
            for f in fl:
                if f[1] == iname and f[2] == pname and f[3] == spieces:
                    idno = f[0]
                    break
            else:
                idno = len(fl)
                fl.append([idno, iname, pname, spieces, p.type, st, 0])

            rv[0].contents = "LSEfi_pq"
            rv[1].contents[0:3] = [ LSEtk_TokenOther("{%d,%d,%d,%s}" % \
                                             (idno,p.controlEmpty,p.width,
                                              p.type))]
        
        else: # is an alias

            ptype = "LSE_type_none"
            for c in p[1]:
                if c: 
                    ptype  = db.getPort(c[0],c[1]).type
                    break

            if spieces[1] == "any" or spieces[1] =="data":
                idp = args[2].contents
                datap = args[3].contents
            else:
                idp = [ LSEtk_TokenOther("NULL") ]
                datap = [ LSEtk_TokenOther("NULL") ]
                
            pia = LSEcg_port_alias_name(iname,pname)
            if len(p[1]):
                aliasl = [ LSEtk_TokenOther("\n&%s[0]" % pia),
                           LSEtk_TokenOther(",\n"),
                           ]
            else:
                aliasl = [ LSEtk_TokenOther("\nNULL,\n") ]
            rv = [ LSEtk_TokenIdentifier("LSEmi_PAQUERY<%s,%d>" %
                                         (ptype, len(p[1]))),
                   LSEtk_TokenPList(aliasl + checks +
                                    [
                                      LSEtk_TokenOther(cbn),
                                      LSEtk_TokenOther(",\n") ] +
                                    portnotok.contents +
                                    [ LSEtk_TokenOther(",\n"),
                                      LSEtk_TokenNumber("%d" % indc),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenOther(mask),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenOther(kmask),
                                      LSEtk_TokenOther(","),
                                      LSEtk_TokenOther("%d" % wuno),
                                      LSEtk_TokenOther(",\n"),
                                      ] + idp +
                                    [ LSEtk_TokenOther(",") ] +
                                    datap)
                   ]

            if len(p[1]): # only defer if the query is to a non-empty port
                st = (rv[0].contents, pia)
                key = LSEcg_fkey(codeloc, subinst, LSEcg_fkey_port_alias_query)
                fl = currinst.toFinish.setdefault(key,[])
                for f in fl:
                    if f[1] == iname and f[2] == pname and f[3] == spieces:
                        idno = f[0]
                        break
                else:
                    idno = len(fl)
                    fl.append([idno, iname, pname, spieces, ptype, st, 0 ])

                rv[0].contents = "LSEfi_paq"
                rv[1].contents[0:1] = [ LSEtk_TokenOther("{%d,%d,%s}" % \
                                                 (idno,len(p[1]),ptype)) ]
        return [ LSEtk_TokenPList(rv) ]
                                
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        args[0] = (args[0], "","",None,"","")
        _LSEap_report_error(v,args[0][0],tinfo[0],rexc)
        return []

# we determine how to implement the call by how similar the different
# call sites are to one another.
# same instance, port:       direct call with direct pointer
# same multi-instance and event: direct call through pointer from table
# otherwise:                     indirect call through pointer from table
def _LSEap_DECIDE_port_query(db, fkey, tfrecs):
    (idno, iname, pname, spieces, ptype, templ, impltype) = tfrecs[0]
    miname = db.getInst(iname).multiInst.name
    sameinst = reduce(lambda x,y:x and y[1]==iname,tfrecs,1)
    sameport = reduce(lambda x,y:x and y[2]==pname,tfrecs,1)
    if sameinst and sameport: impl = 0 # all the same
    else: impl = 1 # different data
    for f in tfrecs:
        f[6] = impl

def _LSEap_REPLACE_port_query(db, key, tfrec):
    (idno, iname, pname, spieces, ptype, templ, impltype) = tfrec
    if impltype==0:
        return "%s(\n&%s[0],\n&%s[0]" % \
               (templ[0],templ[1],templ[2])
    else:
        return "%s(%s,%s" % (templ[0],
                             LSEcg_apivar_name(key,idno,"pis"),
                             LSEcg_apivar_name(key,idno,"pin"))

def _LSEap_PIECES_port_query(db, key, tfrec, part):
    (idno, iname, pname, spieces, ptype, templ, impltype) = tfrec
    if impltype != 0:
        fname = LSEcg_apivar_name(key,idno,"func")
        
        if part == 0: # var defs
            return "%s *%s;\nstruct LSEfw_portinst_info_s<%s> *%s;" \
                   % (templ[3], LSEcg_apivar_name(key,idno,"pis"),
                      ptype, LSEcg_apivar_name(key,idno,"pin"))
            
        elif part == 1: # constructor pieces
            pis = LSEcg_apivar_name(key,idno,"pis")
            pin = LSEcg_apivar_name(key,idno,"pin")
            return ( "%s *LSE_%s,\n" \
                     "struct LSEfw_portinst_info_s<%s> *LSE_%s" \
                     % (templ[3], pis, ptype, pin),
                     "",
                     "%s(LSE_%s),\n%s(LSE_%s)" % (pis,pis,pin,pin))
        else: # instantiation
            return "%s,\n%s" % (templ[1],templ[2])
    return None
 
# we determine how to implement the call by how similar the different
# call sites are to one another.
# same instance, port:       direct call with direct pointer
# same multi-instance and event: direct call through pointer from table
# otherwise:                     indirect call through pointer from table
def _LSEap_DECIDE_port_alias_query(db, fkey, tfrecs):
    (idno, iname, pname, spieces, ptype, templ, impltype) = tfrecs[0]
    miname = db.getInst(iname).multiInst.name
    sameinst = reduce(lambda x,y:x and y[1]==iname,tfrecs,1)
    sameport = reduce(lambda x,y:x and y[2]==pname,tfrecs,1)
    if sameinst and sameport: impl = 0 # all the same
    else: impl = 1 # different locations
    for f in tfrecs:
        f[6] = impl

def _LSEap_REPLACE_port_alias_query(db, key, tfrec):
    (idno, iname, pname, spieces, ptype, templ, impltype) = tfrec
    if impltype==0:
        return "%s(\n&%s[0]" % (templ[0], templ[1])
    else:
        return "%s(%s" % (templ[0],LSEcg_apivar_name(key,idno,"pia"))

def _LSEap_PIECES_port_alias_query(db, key, tfrec, part):
    (idno, iname, pname, spieces, ptype, templ, impltype) = tfrec
    if impltype!=0:
        fname = LSEcg_apivar_name(key,idno,"pia")
        if part == 0: # var defs
            return "struct LSEfw_port_alias_s<%s> *%s;" % (ptype, fname)
        elif part == 1: # constructor pieces
            return ( "struct LSEfw_port_alias_s<%s> *LSE_%s" % (ptype,fname),
                     "",
                     "%s(LSE_%s)" % (fname,fname))
        else: # instantiation
            return "%s" % templ[1]
    return None
 
########
# LSE_port_set(port [ porti ] . signal , sigval, id, data)
#   a really fun one....
#     - no instance is allowed
#     - porti is required
#     - signal is optional and could be "mask"
#     - id and value's presence depend upon signal and port type
########
def _LSEap_port_set(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc not in [LSEcg_in_clm, LSEcg_in_userpoint,
                           LSEcg_in_extension,
                           LSEcg_in_funcheader, LSEcg_in_modulebody]:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
            
        if (len(args)<2): raise LSEap_Exception("Missing port name")
        if (len(args)<3): raise LSEap_Exception("Missing signal value")

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, ptok, portnotok, stok, argparent,
             pstack) = _LSEap_parse_port_name(argparent, pstack,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in port name")

        if itok: raise LSEap_Exception("Cannot specify instance name")
        if not portnotok: raise LSEap_Exception("Must specify port instance")
        portnotok = LSEtk_TokenTopLevel(portnotok.contents)
        try:
            p = currinst.getPort(ptok.contents)
        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

        # check the signal name and argument length
        
        if stok:
            sname = stok.contents

            if sname not in ["data","enable","ack"]:
                raise LSEap_Exception("Invalid signal name '%s'" % sname)
            if p.dir == "output":
                mask = "LSEty_signal_%s_mask &" % sname
                if sname == "ack":
                    raise LSEap_Exception("Cannot set 'ack' on output port")
                elif sname == "data":
                    if len(args) != 5:
                        raise LSEap_Exception("Missing id or data arguments")
                else:
                    if len(args) != 3: raise LSEap_Exception("Extra arguments")
                    args.extend(["NULL", "NULL"])
            else:
                if sname != "ack":
                    raise LSEap_Exception("Cannot set '%s' on input port" % \
                                          sname)
                if len(args) != 3: raise LSEap_Exception("Extra arguments")
        else:
            if p.dir == "output":
                mask = ""
                if len(args) != 5:
                    raise LSEap_Exception("Missing id or data arguments")
            else: 
                if len(args) != 3: raise LSEap_Exception("Extra arguments")

        # and now do it....
        if p.dir == "output":
            return [ LSEtk_TokenOther("""%s(LSEfw_f_call %s,%s(%s),%s,%s)""" % 
                                      (LSEcg_portapi_name(p,"SET_DATAEN"),
                                       portnotok, mask, args[2], args[3],
                                       args[4])) ]
        else:
            return [ LSEtk_TokenOther("%s(LSEfw_f_call %s,%s)" % \
                                      (LSEcg_portapi_name(p,"SET_ACK"),
                                       portnotok, args[2])) ]
            
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []
        
########
# LSE_port_type(instance : port)
########
def _LSEap_port_type(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, ptok, argparent,
             pstack) = _LSEap_parse_hname_colon_hname(argparent, pstack,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in port name")
        
        try:
            if not itok:
                if currinst:
                    iname = currinst.name
                else:
                    raise LSEap_Exception("Must specify instance name")
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name "
                                      "in a .clm file")
            else: iname = itok.contents
            pname = ptok.contents
            p = db.getPort(iname,pname)
            tname = [ LSEtk_TokenIdentifier(p.type) ] # backend name
        except LSEdb_Exception, v:
            # now, maybe the port was an alias....
            p = db.getPortAlias(iname,pname,1)
            if not p: raise LSEap_Exception(str(v))
            for conn in p[1]:
                if conn:
                    tname = [ LSEtk_TokenIdentifier(db.getPort(conn[0],
                                                               conn[1]).type) ]
                    break
            else:
                # unconnected has any type!
                tname = [ LSEtk_TokenIdentifier("LSE_type_none") ]
                
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []
        
    return tname

########
# LSE_port_width(instance : port)
########
def _LSEap_port_width(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, ptok, argparent,
             pstack) = _LSEap_parse_hname_colon_hname(argparent, pstack,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in port name")
        
        try:
            if not itok:
                if currinst:
                    iname = currinst.name
                else:
                    raise LSEap_Exception("Must specify instance name")
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name "
                                      "in a .clm file")
            else: iname = itok.contents
            pname = ptok.contents
            p = db.getPort(iname,pname)
            width = p.width
        except LSEdb_Exception, v:
            # now, maybe the port was an alias....
            p = db.getPortAlias(iname,pname,1)
            if not p: raise LSEap_Exception(str(v))
            width = len(p[1]) # alias port size = size of list
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]
        
    args[0] = (args[0],iname, pname, p) # include port/port alias for rebuild
    return [ LSEtk_TokenNumber(str(width)) ]

########
# LSE_query_used(query)
########
def _LSEap_query_used(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
            
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (qtok, argparent, pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")

        try:
            if not currinst:
                raise LSEtk_TokenizeException(args[0] + " cannot be called"
                                              " outside of a module instance",
                                              tinfo[0])
            q = db.getQuery(currinst.name,qtok.contents)
            uses = currinst.calledby.has_key(('LSE_query_call',q.name))

        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]

    return [ LSEtk_TokenNumber(str(int(uses))) ]

########
# LSE_query_call(instance, query, qinstno, args)
########
def _LSEap_query_call(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc not in [LSEcg_in_userpoint, LSEcg_in_controlpoint,
                           LSEcg_in_extension,
                           LSEcg_in_datacoll, LSEcg_in_modulebody]:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)<2):
            # we raise a LSEtk_TokenizeException so we get nice line #s 
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (itok, qtok, argparent,
             pstack) = _LSEap_parse_hname_colon_hname(argparent, pstack,0)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")

        try:
            if not itok:
                if currinst:
                    iname = currinst.name
                else:
                    raise LSEap_Exception("Must specify instance name")
            elif not codeloc:
                raise LSEap_Exception("Cannot specify instance name "
                                      "in a .clm file")
            else:
                iname = itok.contents
            qname = qtok.contents
            q = db.getQuery(iname,qname)

            if codeloc == LSEcg_in_modulebody and \
               iname[:len(currinst.name)] != currinst.name:
                raise LSEap_Exception("Cannot call a non-descendant instance's"
                                      " query from a module body")
                
            params = q.params
            if not params or params == "void": nparam = 0
            else: nparam = string.count(params,",")+1
            if len(args)!=nparam+3:
                LSEap_report_badargs(args[0],nparam+2,tinfo[0])

        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

        # should check on number of arguments, and all sorts of goodies...
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        args[0] = (args[0], "","",None)
        _LSEap_report_error(v,args[0][0],tinfo[0],rexc)
        return []

    args[0] = (args[0],iname,q)

    # If the caller is a code point, then we call the query trampoline.
    # If it is a data collector, we call the query directly...
    if iname == currinst.name:
        if nparam > 0:
            callparm = [ LSE_TokenIdentifier("LSEfw_f_call ") ]
        else:
            callparm = [ LSE_TokenIdentifier("LSEfw_f_call_void ") ]
        isExt = 0
    else:
        if nparam > 0: 
            callparm = [ LSEtk_TokenOther("(LSEfw_iptr_t)&%s "
                                          % LSEcg_instdata_name(iname)),
                         LSEtk_TokenOther("LSEfw_tf_callc ") ]
        else:
            callparm = [ LSEtk_TokenOther("(LSEfw_iptr_t)&%s "
                                          % LSEcg_instdata_name(iname)),
                         LSEtk_TokenOther("LSEfw_tf_callc_void ") ]
        isExt = 1
    
    if codeloc == LSEcg_in_datacoll:
        qfname = LSEcg_query_name(iname,qname,isExt)
    else:
        qfname = LSEcg_query_tramp_name(db,iname,qname,isExt)
        
        # now things get tricky.... the call site is not assigned until
        # *AFTER* this function has run, so how do we get it?  The way
        # we do that is we register a fixer-upper function that gets run
        # later.... how much later is "after the tokenizing is over"...
        #
        cinstname = currinst.name
        cp = subinst[0]
        tok = LSEtk_TokenNumber("-1")
        LSEcg_register_fixup(_LSEap_query_site,tok,(q,(cinstname,cp.name)))
        
        if cp.type == LSEdb_PointControl:
            cblock = (LSEdb_CblockFiring,currinst.name,cp.name,0)
            cno = db.cblocksBacklist[cblock]
            cbn = "(instno+LSEfw_CBBASE+%d)" % (cno - currinst.firstCblock)
        else:
            cbn="LSEfw_schedule_calling_cblock"
                
        rv = callparm + \
             [ tok,
               LSEtk_TokenOther(","),
               LSEtk_TokenOther(cbn),
               LSEtk_TokenOther(","),
               ]
        rv.extend(args[2].contents)
        if nparam > 0: rv.append(LSEtk_TokenOther(","))

    # arg1, arg2, ...
    if nparam > 0:
        rv.extend(reduce(lambda x,y: x + \
                         [ LSEtk_TokenOther(",") ] + y.contents,
                         args[4:], args[3].contents))
    # queryname(arg1, arg2, ...)
    rv = [ LSEtk_TokenIdentifier(qfname),
           LSEtk_TokenPList(rv),
           ]

    if isExt: # will be in good location
        key = LSEcg_fkey(codeloc, subinst, LSEcg_fkey_qtramp_call)
        fl = currinst.toFinish.setdefault(key,[])
        for f in fl:
            if f[1] == iname and f[2] == q.name:
                idno = f[0]
                break
        else:
            idno = len(fl)
            fl.append([idno, iname, q.name, 0])
        rv[0].contents = "LSEfi_qt"
        rv[1].contents[0] = LSEtk_TokenOther("{%d,%s,%s}" % \
                                             (idno,q.returnType,q.params))
        
    return rv

def _LSEap_query_site(tok, args):
    tok.contents = str(args[0].getCallSite(args[1])[0])

# we determine how to implement the call by how similar the different
# call sites are to one another.
# same instance and event:       direct call with direct pointer
# same multi-instance and event: direct call through pointer from table
# otherwise:                     indirect call through pointer from table
def _LSEap_DECIDE_qtramp_call(db, fkey, tfrecs):
    (idno, iname, qname, impltype) = tfrecs[0]
    miname = db.getInst(iname).multiInst.name
    sameinst = reduce(lambda x,y:x and y[1]==iname,tfrecs,1)
    samemethod = reduce(lambda x,y:x and y[2]==qname,tfrecs,1)
    if samemethod and sameinst: impl = 0 # all the same
    elif reduce(lambda x,y:x and db.getInst(y[1]).multiInst.name==miname,
                tfrecs, 1):
        impl = 1 # same multi-inst
    else:
        impl = 2 # different functions
    for f in tfrecs:
        f[3] = impl
        
def _LSEap_REPLACE_qtramp_call(db, key, tfrec):
    (idno, iname, qname, impltype) = tfrec
    if impltype==0:
        return "%s((LSEfw_iptr_t)&%s " % \
               (LSEcg_query_tramp_name(db, iname, qname, 1),
                LSEcg_instdata_name(iname))
    elif impltype==1:
        return "%s(%s " % (LSEcg_query_tramp_name(db, iname, qname,1),
                           LSEcg_apivar_name(key,idno,"ptr"))
    else:
        return "%s(%s " % (LSEcg_apivar_name(key,idno,"func"),
                           LSEcg_apivar_name(key,idno,"ptr"))

def _LSEap_PIECES_qtramp_call(db, key, tfrec, part):
    (idno, iname, qname, impltype) = tfrec
    if impltype==1:
        vname = LSEcg_apivar_name(key,idno,"ptr")
        if part == 0: # var defs
            return "LSEfw_iptr_t %s;" % vname
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:                
                return ("LSEfw_iptr_t LSE_%s" % vname,
                        "%s = LSE_%s;" % (vname,vname),"")
            else:
                return ("LSEfw_iptr_t LSE_%s" % vname,
                        "", "%s(LSE_%s)" % (vname, vname))
        else: # instantiation
            return "(LSEfw_iptr_t)&%s" % LSEcg_instdata_name(iname)
    elif impltype==2:
        q = db.getQuery(iname,qname)
        fname = LSEcg_apivar_name(key,idno,"func")
        vname = LSEcg_apivar_name(key,idno,"ptr")
        if q.params != "void":
            ps = "LSEfw_stf_def int LSE_point, " \
                 "unsigned int LSEfw_calling_id, int LSEfw_instno, " + q.params
        else: ps = "LSEfw_stf_def int LSE_point, " \
                   "unsigned int LSEfw_calling_id, int LSEfw_instno"
        if part == 0: # var defs
            return "%s (*%s)(%s);\nLSEfw_iptr_t %s;" % (q.returnType,
                                                        fname,ps,vname)
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:                
                return ("%s (*LSE_%s)(%s),\nLSEfw_iptr_t LSE_%s" % 
                        (q.returnType, fname,ps,vname),
                        "%s = LSE_%s;\n%s = LSE_%s;" % \
                        (fname,fname,vname,vname),
                        "")
            else: 
                return ("%s (*LSE_%s)(%s),\nLSEfw_iptr_t LSE_%s" % 
                        (q.returnType, fname,ps,vname),
                        "",
                        "%s(LSE_%s),\n%s(LSE_%s)" % \
                        (fname,fname,vname,vname))
               
        else: # instantiation
            return "%s,(LSEfw_iptr_t)&%s" % \
                   (LSEcg_query_tramp_name(db,iname,qname,1),
                    LSEcg_instdata_name(iname))
    return None
    
#########
# LSE_query_results_changed()
#########
def _LSEap_query_results_changed(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc not in [ LSEcg_in_funcheader, LSEcg_in_clm,
                            LSEcg_in_extension,
                            LSEcg_in_modulebody ]:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
            
        if (len(args) != 2 or len(args[1].contents)):
            raise LSEtk_TokenizeException(args[0] + " should have an empty"
                                          " argument list",tinfo[0])

        rv = [ LSEtk_TokenIdentifier("QUERY_RUN_NOW"),
               LSEtk_TokenPList([ LSEtk_TokenIdentifier("this "),
                                  LSEtk_TokenOther("LSEfw_tf_callc_void")
                                  ]),
               ]
        
        return rv

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

########
# LSE_report_err(message.....)
########
def _LSEap_report_err(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])

        if (len(args)<1):
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

    if currinst: iname = currinst.name
    else: iname = "<Top-level>"

    rv = [ LSEtk_TokenIdentifier("LSEfw_report_err"),
           LSEtk_TokenPList([ LSEtk_TokenIdentifier("LSE_instance_name") ] +
                            reduce(lambda x,y: x + \
                                   [ LSEtk_TokenOther(",") ] +
                                   y.contents,
                                   args[1:], []))
           ]
    
    return rv
    
########
# LSE_report_err_at_codegen(message.....)
########
def _LSEap_report_err_at_codegen(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []
    
    if len(args)>1:
        argstr = reduce(lambda x,y: x + "," + str(y), args[2:], str(args[1]))
    else: argstr = ""

    v = LSEtk_TokenizeException(argstr,tinfo[0])
    if rexc: rexc(v)
    else: raise v
    return []

########
# LSE_report_err_at_cpp(message.....)
########
def _LSEap_report_err_at_cpp(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

    if currinst: iname = currinst.name
    else: iname = "<Top-level>"

    if len(args)>1:
        argstr = reduce(lambda x,y: x + "," + str(y), args[2:], str(args[1]))
    else: argstr = ""

    # prevent reparsing of the message...
    return [ LSEtk_TokenIdentifier("""\n#error %s : %s\n""" % (iname,argstr)) ]

########
# LSE_report_warn(message.....)
########
def _LSEap_report_warn(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])

        if (len(args)<1):
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

    if currinst: iname = currinst.name
    else: iname = "<Top-level>"

    rv = [ LSEtk_TokenIdentifier("LSEfw_report_warn"),
           LSEtk_TokenPList([ LSEtk_TokenIdentifier("LSE_instance_name") ] +
                            reduce(lambda x,y: x + \
                                   [ LSEtk_TokenOther(",") ] +
                                   y.contents,
                                   args[1:], []))
           ]
    
    return rv
    
########
# LSE_report_warn_at_codegen(message.....)
########
def _LSEap_report_warn_at_codegen(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []
    
    if len(args)>1:
        argstr = reduce(lambda x,y: x + "," + str(y), args[2:], str(args[1]))
    else: argstr = ""

    v = LSEtk_TokenizeException(argstr,tinfo[0])
    sys.stderr.write("Warning: %s\n" % str(v))
    return []

########
# LSE_report_warn_at_cpp(message.....)
########
def _LSEap_report_warn_at_cpp(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

    if currinst: iname = currinst.name
    else: iname = "<Top-level>"

    if len(args)>1:
        argstr = reduce(lambda x,y: x + "," + str(y), args[2:], str(args[1]))
    else: argstr = ""

    # prevent reparsing of the message...
    return [ LSEtk_TokenIdentifier("""\n#warning %s : %s\n""" % (iname,argstr))
             ]

########
# LSE_signal_form(data, enable, ack)
# done as a tokenizer macro so that it doesn't have to be analyzed specially
########
def _LSEap_signal_form(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        #if not currinst:
        #    _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
            
        if (len(args)!=4): LSEap_report_badargs(args[0],3,tinfo[0])
        
        rv = [ LSEtk_TokenPList([LSEtk_TokenIdentifier("LSE_signal_extract_data"),
				 LSEtk_TokenPList(args[1].contents), LSEtk_TokenOther("|"),
				 LSEtk_TokenIdentifier("LSE_signal_extract_enable"),
				 LSEtk_TokenPList(args[2].contents), LSEtk_TokenOther("|"),
				 LSEtk_TokenIdentifier("LSE_signal_extract_ack"),
				 LSEtk_TokenPList(args[3].contents)]) ]

        return rv
        
    except (LSEap_Exception,LSEtk_TokenizeException),v :
        args[0] = (args[0], "","",None)
        _LSEap_report_error(v,args[0][0],tinfo[0],rexc)
        return []

#######
# LSE_userpoint_invoke(name,args....)
#######
def _LSEap_userpoint_invoke(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
        if (len(args)<2):
            raise LSEtk_TokenizeException(args[0] + " should have at "
                                          "least 1 argument",tinfo[0])
            
        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (utok, argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")
        
        try:
            pnt = currinst.getPoint(str(utok))
            if pnt.type != LSEdb_PointUser:
                raise LSEap_Exception("'%s' is not a user point" % pnt.name)
        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

        params = pnt.params
        if not params or params == "void": nparam = 0
        else: nparam = string.count(params,",")+1

        if len(args) != nparam + 2:
            LSEap_report_badargs(args[0],nparam+1,tinfo[0])

        if nparam > 0:
            rv = [ LSEtk_TokenIdentifier(LSEcg_codefunc_name(pnt,0)),
                   LSEtk_TokenPList([ LSEtk_TokenOther("LSEfw_f_call ") ] +
                                    reduce(lambda x,y: x + \
                                           [ LSEtk_TokenOther(",") ] +
                                           y.contents,
                                           args[3:], args[2].contents))
                   ]
        else:
            rv = [ LSEtk_TokenIdentifier(LSEcg_codefunc_name(pnt,0)),
                   LSEtk_TokenPList([LSEtk_TokenOther("LSEfw_f_call_void")
                                     ]) ]

        return rv
        
    except (LSEap_Exception,LSEtk_TokenizeException),v:
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

#######
# LSE_userpoint_defaulted(args....)
#######
def _LSEap_userpoint_defaulted(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (utok, argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")

        try:
            pnt = currinst.getPoint(str(utok))
            if pnt.type != LSEdb_PointUser:
                raise LSEap_Exception("'%s' is not a user point" % pnt.name)

        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]

    return [ LSEtk_TokenNumber(str(int(pnt.isDefault))) ]

#######
# LSE_userpoint_empty(args....)
#######
def _LSEap_userpoint_empty(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc > LSEcg_in_text:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if not currinst:
            _LSEap_report_badusage(args[0],LSEcg_in_toplevel,tinfo[0])
            
        try:
            _LSEap_remove_left_paren(args[1])
            argparent = args[1]
            pstack = []
            (utok, argparent,pstack) = _LSEap_parse_name(argparent,pstack,1,1)
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in query name")
        
        try:
            pnt = currinst.getPoint(str(utok))
            if pnt.type != LSEdb_PointUser:
                raise LSEap_Exception("'%s' is not a user point" % pnt.name)

        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return [ LSEtk_TokenNumber("0") ]

    return [ LSEtk_TokenNumber(str(int(pnt.isEmpty)))]

######
# PARM(instance : parameter)
######
def _LSEap_PARM(tinfo,args,extra,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if codeloc == LSEcg_in_def:
            _LSEap_report_badusage(args[0],codeloc,tinfo[0])
        if (len(args)!=2): LSEap_report_badargs(args[0],1,tinfo[0])

        try:
            _LSEap_remove_left_paren(args[1])
        except LSEap_Exception,v:
            raise LSEap_Exception("Syntax error in parameter name")
        argparent = args[1]
        pstack = []
        
        try:
            (itok, ptok, argparent,
             pstack) = _LSEap_parse_hname_colon_sname(argparent, pstack,0)
        except LSEap_Exception, v:
            raise LSEap_Exception("Unable to parse parameter name")
        
        try:
            if not itok:
                if currinst:
                    inst = currinst
                else:
                    # we do not search top-level for PARM macros;
                    # top-level PARMs must be done with ${}
                    raise LSEap_Exception("Must specify instance name")
            else:
                inst = db.getInst(itok.contents)
            pname = ptok.contents
            parm = inst.getParm(pname,1)
            if parm == None:
                raise LSEap_Exception("Unable to find parameter '%s' "
                                      "in instance '%s'" % (pname,inst.name))
        except LSEdb_Exception, v:
            raise LSEap_Exception(str(v))

    except (LSEap_Exception,LSEtk_TokenizeException),v :
        _LSEap_report_error(v,args[0],tinfo[0],rexc)
        return []

    # if not runtimeable or actually runtimed, put out the text of the value
    # otherwise, put out just the name of the parameter.
    if (not parm[3] or parm[2]):
        return str(parm[0]) # important.... allows the contents to be reparsed
    else:
        if inst != currinst:
            iname = inst.name
            key = LSEcg_fkey(codeloc, subinst, LSEcg_fkey_parm)
            fl = currinst.toFinish.setdefault(key,[])
            for f in fl:
                if f[1] == iname and f[2] == pname:
                    idno = f[0]
                    break
            else:
                idno = len(fl)
                fl.append([idno, iname, pname, parm[1], str(parm[0]), 0])
            return [ LSEtk_TokenIdentifier("LSEfi_pm({%d,%s})" %
                                           (idno, parm[1])) ]
        else:
            return [ LSEtk_TokenIdentifier(pname) ]
        
def _LSEap_DECIDE_parm(db, fkey, tfrecs):
    (idno, iname, pname, ptype, val, impltype) = tfrecs[0]
    sameinst = reduce(lambda x,y:x and y[1]==iname,tfrecs,1)
    sameparm = reduce(lambda x,y:x and y[2]==pname,tfrecs,1)
    sameval = reduce(lambda x,y:x and y[3]==val,tfrecs,1)
    if (sameinst and sameparm) or sameval: impl = 0
    else: impl = 1
    for f in tfrecs:
        f[5] = impl
        
def _LSEap_REPLACE_parm(db, key, tfrec):
    (idno, iname, pname, ptype, val, impltype) = tfrec
    if impltype==0:
        return "(%s" % val
    else:
        return "(%s" % LSEcg_apivar_name(key,idno,"pm")

def _LSEap_PIECES_parm(db, key, tfrec, part):
    (idno, iname, pname, ptype, val, impltype) = tfrec
    if impltype==1:
        pname = LSEcg_apivar_name(key,idno,"pm")
        if part == 0: # var defs
            return "%s %s;" % (ptype, pname)
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:
                return ("%s LSE_%s" % (ptype,pname),
                        "%s = LSE_%s;" % (pname,pname),"")
            else:
                return ("%s LSE_%s" % (ptype,pname), "",
                        "%s(LSE_%s)" % (pname,pname))
        else: # instantiation
            return val
    return None

######################### Internal APIs ###########################
    
######
# LSEuv_ref
######
# This API was initially handled back in SIM_database_build rather
# than chaining through analysis and apidefs.
#
# we determine how to implement the reference by how similar the different
# reference sites are to one another.
# same var: direct reference
# otherwise: indirect reference through pointer in table

def _LSEap_DECIDE_uv_ref(db, fkey, tfrecs):
    (idno, vname, impltype) = tfrecs[0]
    samevar = reduce(lambda x,y:x and y[1]==vname,tfrecs,1)
    if samevar: impl = 0
    else: impl = 1
    for f in tfrecs:
        f[2] = impl
        
def _LSEap_REPLACE_uv_ref(db, key, tfrec):
    (idno, vname, impltype) = tfrec
    if impltype==0:
        return vname
    else:
        if key[1]==LSEcg_in_modulebody:
            return "(*%s)" % LSEcg_apivar_name(key,idno,"var")
        else:
            return LSEcg_apivar_name(key,idno,"var")

def _LSEap_PIECES_uv_ref(db, key, tfrec, part):
    (idno, vname, impltype) = tfrec
    if impltype==1:
        nname = LSEcg_apivar_name(key,idno,"var")
        vtype = db.varBEMap[vname][2]
        if part == 0: # var defs
            if key[1]==LSEcg_in_modulebody:
                return "%s *%s;" % (vtype, nname)
            else:
                return "%s &%s;" % (vtype, nname)
        elif part == 1: # constructor pieces
            if key[1]==LSEcg_in_modulebody:                
                return ("%s *LSE_%s" % (vtype,nname),
                        "%s = LSE_%s;" % (nname,nname),"") 
            else:
                return ("%s &LSE_%s" % (vtype,nname), "",
                        "%s(LSE_%s)" % (nname,nname))
        else: # instantiation
            if key[1]==LSEcg_in_modulebody:
                return "&%s" % vname
            else:
                return vname
    return None
    
###########################################################################

LSEap_APIs = {
    "FALSE" : [(LSEtk_m4_macro,LSEtk_NoArgs,"(0)",None)],
    "FUNC" : [(LSEtk_Tok_macro,0, _LSEap_FUNC, None)],
    "FUNCPTR" : [(LSEtk_Tok_macro,0, _LSEap_FUNCPTR, None)],
    "FUNCPTRCALL" : [(LSEtk_Tok_macro,0, _LSEap_FUNCPTRCALL, None)],
    "FUNCPTRDEF" : [(LSEtk_Tok_macro,0, _LSEap_FUNCPTRDEF, None)],
    "GLOB" : [(LSEtk_Tok_macro,0, _LSEap_GLOB, None)],
    "GLOBDEF" : [(LSEtk_Tok_macro,0, _LSEap_GLOBDEF, None)],
    "HANDLER" : [(LSEtk_Tok_macro,0, _LSEap_HANDLER, None)],
    "LSE_controlpoint_call_default" : [(LSEtk_Tok_macro, 0,
                                       _LSEap_point_call_default,None)],
    "LSE_controlpoint_call_empty" : [(LSEtk_Tok_macro, 0,
                                     _LSEap_controlpoint_call_empty,None)],
    "LSE_data_cancel" : [(LSEtk_Tok_macro,0,_LSEap_data_cancel,None)],
    "LSE_data_copy" : [(LSEtk_Tok_macro,0,_LSEap_data_copy,None)],
    "LSE_data_move" : [(LSEtk_Tok_macro,0,_LSEap_data_move,None)],
    "LSE_data_register" : [(LSEtk_Tok_macro,0,_LSEap_data_register,None)],
    "LSE_dynid_cancel" : [(LSEtk_Tok_macro, 0, _LSEap_dynid_cancel, None)],
    "LSE_dynid_clone" : [(LSEtk_Tok_macro, 0, _LSEap_dynid_clone, None)],
    "LSE_dynid_create" : [(LSEtk_Tok_macro, 0, _LSEap_dynid_create, None)],
    "LSE_dynid_element_ptr" : [(LSEtk_Tok_macro, 0,
                               _LSEap_struct_element_ptr, None)],
    "LSE_dynid_get" : [(LSEtk_Tok_macro, 0, _LSEap_struct_get, None)],
    "LSE_dynid_recreate" : [(LSEtk_Tok_macro, 0, _LSEap_dynid_recreate, None)],
    "LSE_dynid_register" : [(LSEtk_Tok_macro, 0, _LSEap_dynid_register, None)],
    "LSE_dynid_set" : [(LSEtk_Tok_macro, 0, _LSEap_struct_set, None)],
    "LSE_enum_value" : [(LSEtk_Tok_macro, 0, _LSEap_enum_value, None)],
    "LSE_eval" : [(LSEtk_Tok_macro, 0, _LSEap_eval, None)],
    "LSE_event_filled" : [(LSEtk_Tok_macro, 0, _LSEap_event_filled, None)],
    "LSE_event_record" : [(LSEtk_Tok_macro, 0, _LSEap_event_record, None)],
    "LSE_LOOP_OVER_PORT" : [(LSEtk_Tok_macro, 0, _LSEap_loop_over_port, None)],
    "LSE_LOOP_END" : [(LSEtk_Tok_macro, 0, _LSEap_loop_end, None)],
    "LSE_method_call" : [(LSEtk_Tok_macro, 0, _LSEap_method_call, None)],
    "LSE_method_used" : [(LSEtk_Tok_macro, 0, _LSEap_method_used, None)],
    "LSE_nameequal" : [(LSEtk_Tok_macro, 0, _LSEap_nameequal, None)],
    "LSE_parm_constant" : [(LSEtk_Tok_macro, 0, _LSEap_parm_constant, None)],
    "LSE_port_connected" : [(LSEtk_Tok_macro, 0, _LSEap_port_connected, None)],
    "LSE_port_get" : [(LSEtk_Tok_macro, 0, _LSEap_port_get, None)],
    "LSE_port_num_connected" : [(LSEtk_Tok_macro, 0,
                                _LSEap_port_num_connected, None)],
    "LSE_port_query" : [(LSEtk_Tok_macro, 0, _LSEap_port_query,None)],
    "LSE_port_set" : [(LSEtk_Tok_macro, 0, _LSEap_port_set, None)],
    "LSE_port_type" : [(LSEtk_Tok_macro, 0, _LSEap_port_type, None)],
    "LSE_port_width" : [(LSEtk_Tok_macro, 0, _LSEap_port_width, None)],
    "LSE_query_call" : [(LSEtk_Tok_macro, 0, _LSEap_query_call, None)],
    "LSE_query_used" : [(LSEtk_Tok_macro, 0, _LSEap_query_used, None)],
    "LSE_query_results_changed" : [(LSEtk_Tok_macro, 0,
                                   _LSEap_query_results_changed,None)],
    "LSE_report_err" : [(LSEtk_Tok_macro, 0, _LSEap_report_err,None)],
    "LSE_report_err_at_codegen" : [(LSEtk_Tok_macro, LSEtk_KeepLeadWS,
                                   _LSEap_report_err_at_codegen,None)],
    "LSE_report_err_at_cpp" : [(LSEtk_Tok_macro, LSEtk_KeepLeadWS,
                               _LSEap_report_err_at_cpp,None)],
    "LSE_report_warn" : [(LSEtk_Tok_macro, 0, _LSEap_report_warn,None)],
    "LSE_report_warn_at_codegen" : [(LSEtk_Tok_macro, LSEtk_KeepLeadWS,
                                   _LSEap_report_warn_at_codegen,None)],
    "LSE_report_warn_at_cpp" : [(LSEtk_Tok_macro, LSEtk_KeepLeadWS,
                               _LSEap_report_warn_at_cpp,None)],
    "LSE_resolution_element_ptr" : [(LSEtk_Tok_macro, 0,
                                    _LSEap_struct_element_ptr, None)],
    "LSE_resolution_get" : [(LSEtk_Tok_macro, 0, _LSEap_struct_get, None)],
    "LSE_resolution_set" : [(LSEtk_Tok_macro, 0, _LSEap_struct_set, None)],
    "LSE_signal_form" : [(LSEtk_Tok_macro, 0, _LSEap_signal_form, None)],
    "LSE_userpoint_invoke" : [(LSEtk_Tok_macro, 0,
                              _LSEap_userpoint_invoke,None)],
    "LSE_userpoint_defaulted" : [(LSEtk_Tok_macro, 0,
                                 _LSEap_userpoint_defaulted, None)],
    "LSE_userpoint_empty" : [(LSEtk_Tok_macro, 0,
                             _LSEap_userpoint_empty, None)],
    "LSE_userpoint_call_default" : [(LSEtk_Tok_macro, 0,
                                    _LSEap_point_call_default,None)],
    "PARM" : [(LSEtk_Tok_macro,0,_LSEap_PARM,None)],
    "TRUE" : [(LSEtk_m4_macro,LSEtk_NoArgs,"(!0)",None)],
    }

######### table of functions to finish API calls ############

# decision functions see the database, the key (so we know what we
# are modifying), and the list of records across the instances for the
# particular call site.
LSEap_fkey_decide_funcs = {
    LSEcg_fkey_event_record : _LSEap_DECIDE_event_record,
    LSEcg_fkey_method_call : _LSEap_DECIDE_method_call,
    LSEcg_fkey_uv_ref : _LSEap_DECIDE_uv_ref,
    LSEcg_fkey_port_query : _LSEap_DECIDE_port_query,
    LSEcg_fkey_struct_field : _LSEap_DECIDE_struct_field,
    LSEcg_fkey_qtramp_call : _LSEap_DECIDE_qtramp_call,
    LSEcg_fkey_port_alias_query : _LSEap_DECIDE_port_alias_query,
    LSEcg_fkey_parm : _LSEap_DECIDE_parm,
    }

LSEap_fkey_replace_funcs = {
    LSEcg_fkey_event_record : _LSEap_REPLACE_event_record,
    LSEcg_fkey_method_call : _LSEap_REPLACE_method_call,
    LSEcg_fkey_uv_ref : _LSEap_REPLACE_uv_ref,
    LSEcg_fkey_port_query : _LSEap_REPLACE_port_query,
    LSEcg_fkey_struct_field : _LSEap_REPLACE_struct_field,
    LSEcg_fkey_qtramp_call : _LSEap_REPLACE_qtramp_call,
    LSEcg_fkey_port_alias_query : _LSEap_REPLACE_port_alias_query,
    LSEcg_fkey_parm : _LSEap_REPLACE_parm,
    }

LSEap_fkey_pieces_funcs = {
    LSEcg_fkey_event_record : _LSEap_PIECES_event_record,
    LSEcg_fkey_method_call : _LSEap_PIECES_method_call,
    LSEcg_fkey_uv_ref : _LSEap_PIECES_uv_ref,
    LSEcg_fkey_port_query : _LSEap_PIECES_port_query,
    LSEcg_fkey_struct_field : _LSEap_PIECES_struct_field,
    LSEcg_fkey_qtramp_call : _LSEap_PIECES_qtramp_call,
    LSEcg_fkey_port_alias_query : _LSEap_PIECES_port_alias_query,
    LSEcg_fkey_parm : _LSEap_PIECES_parm,
    }
