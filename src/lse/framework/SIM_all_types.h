/* 
 * Copyright (c) 2000-2003 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Include all the types which might be needed by a module or piece
 * of the framework
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Includes all the base types, domain types, and user types.
 *
 */
#ifndef _LSE_ALL_TYPES_H_
#define _LSE_ALL_TYPES_H_

extern "C" {
#include <stdio.h>
#include <string.h>
}
/* The order of these matters since we're trying to reduce compile time
 * by not including files over and over...
 */
#include <SIM_config.h>
#include <SIM_types.h>
#include <SIM_time.h>
#include <SIM_domain_types.h>
#include <SIM_user_types.h>
#include <SIM_refcount_types.h>
#include <SIM_dynid_types.h>
#include <SIM_resolution_types.h>
#include <SIM_control.h> /* should become control types */

#endif /* _LSE_ALL_TYPES_H_ */

