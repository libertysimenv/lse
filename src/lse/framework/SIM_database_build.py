# /* 
#  * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * This file builds the Python database for the machine
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Reads things in from info files into a given LSE_db structure, doing
#  * tokenizing and macro expansion of various APIs along the way to note
#  * things like type usage, cross-instance API calls, etc...
#  *
#  */

# We do an indirection to get to LSE_db so that we do not have any
# problems where we have a local LSE_db that does not match the global
# LSE_db.  The indirection goes through a list because those are mutable.
#  and while we're at it, we can keep track of instances and code types
#  to pass on to the code generation code

import sys, os, re, imp, string, LSE_hashwrap, types, copy, pdb, shutil

from SIM_codegen import *  # imports tokenizer, database, schedule
import SIM_analysis, SIM_apidefs, SIM_threading # to get internal routines
import LSE_domain, SIM_schedule

LSE_DATA=os.path.dirname(os.path.dirname(__file__))
LSE=os.path.dirname(os.path.dirname(LSE_DATA))

############### some module variables ###############

# Need to pull these in because the info files use them.

LSE_PointUser = LSEdb_PointUser
LSE_PointControl = LSEdb_PointControl

_LSEbl_readpass = 0
_LSEbl_typeLog = {}  # log for types as we do processing of some text

############## and error reporting ###################

def _LSEbl_report_error(v):
    _LSEbl_env.db.reportErr("Error: " + str(v) + "\n")

_LSEbl_env = LSEcg_Environment()
_LSEbl_env.reporter = _LSEbl_report_error

class LSEbl_Exception(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return self.value

############### Tok macros for APIs we deal with at build ##########

#
# macro to handle an embedded type reference; replaces the embedded
# reference with the "true" type name
#
#   typelog here is a type log to log the usage
#
def _LSEbl_enum_ref(tinfo,args,typelog,env):
    try:
        if (len(args)!=3):
            # we raise a LSEtk_TokenizeException so we get nice line #s 
            raise LSEtk_TokenizeException("LSEut_enumref should have "
                                          "2 arguments",
                                          tinfo[0])

        tname = str(args[1])
        tentry = env.db.typeMap.get(tname)
        if not tentry:
            raise LSEtk_TokenizeException("Unable to resolve type '%s' "
                                          "in LSEut_enumref" % args[1],
                                          tinfo[0])
        # mark that type was used
        typelog[(0,tentry[2])] = tname
        return LSEtk_TokenIdentifier(tentry[2]+"__" + str(args[2]))
    except LSEtk_TokenizeException, v:
        _LSEbl_report_error(v)
        return []

#
# macro to handle event recording
#
def _LSEbl_event_record(tinfo,args,typelog,env):

    # returns call replacement and replaces args[0] with
    # (apiname, iname, pointer to event)
    qcall = SIM_analysis._LSEan_event_record(tinfo,args,None,env)

    if not args[0][2]: return qcall # was an error
    
    # remember types
    typelog.update(args[0][2].usesTypes)

    # mark that event was used
    if env.inst:
        env.inst.instSeen[args[0][1]] = 1
    
    return qcall
    
#
# macro to handle method calls
#
def _LSEbl_method_call(tinfo,args,typelog,env):

    # returns call replacement and replaces args[0] with
    # (apiname, iname, pointer to method)
    qcall = SIM_analysis._LSEan_method_call(tinfo,args,None,env)

    if not args[0][2]: return qcall # was an error
    
    # remember types
    typelog.update(args[0][2].usesTypes)

    # mark that method was used
    if env.inst:
        env.inst.instSeen[args[0][1]] = 1
    
    return qcall

#
# macro to handle calls to default control/user functions
#
def _LSEbl_point_call_default(tinfo,args,iscontrol,env):

    rv = SIM_apidefs._LSEap_point_call_default(tinfo,args,None,env)
    
    env.subobj[0].usesDefault = 1

    return rv

#
# macro to handle port query calls
#
def _LSEbl_port_query(tinfo,args,typelog,env):

    # replaces args[0] with (args[0],iname,pname,p,part,index)
    qcall = SIM_analysis._LSEan_port_query(tinfo,args,None,env)

    p = args[0][3]
    
    # leave if error...
    if not p: return qcall
    
    # deal with the data type of the port

    if isinstance(p,LSEdb_Port): ptype = p.type
    else:
        for conn in p[1]:
            if conn:
                ptype = env.db.getPort(conn[0],conn[1]).type
                break
        else:
            ptype = "LSE_type_none" # unconnected has any type!
    tentry = env.db.typeBEMap.get(ptype)
    if tentry: # exclude standard types
        typelog.update(tentry[4])

    # and the width stuff

    if env.inst:
        if isinstance(p,LSEdb_Port):
            env.inst.instSeen[args[0][1]] = 1
        else:
            env.inst.aliasSeen[(args[0][1],args[0][2])] = 1

    return qcall

#
# macro to handle port type calls
#
def _LSEbl_port_type(tinfo,args,typelog,env):

    # returns a type name
    tname = SIM_apidefs._LSEap_port_type(tinfo,args,None,env)

    if tname:
        tentry = env.db.typeBEMap.get(tname[0].contents)
        if tentry: # exclude standard types
            typelog.update(tentry[4])
    
    return tname

#
# macro to handle an embedded variable reference; updates the type usage of 
#  the instance (if any) and remembers that the instance has used this
#  variable.  Assigns a variable name if it has not been assigned yet.
#
def _LSEbl_runtimeparm_ref(tinfo,args,typelog,env):
    try:
        if (len(args)!=2):
            # we raise a LSEtk_TokenizeException so we get nice line #s 
            raise LSEtk_TokenizeException("LSEuv_rp_ref should have 1 "
                                          "argument", tinfo[0])

        rpname = str(args[1])
        rpentry = env.db.runtimeParms.get(rpname)
        if not rpentry:
            raise LSEtk_TokenizeException("Unable to find runtime "
                                          "parameter '%s'" % \
                                          rpname, tinfo[0])

        # mark types used
        typelog.update(rpentry[4])

        tok = LSEtk_TokenIdentifier(rpentry[0])
        
    except LSEtk_TokenizeException, v:
        _LSEbl_report_error(v)
        return []
        
    return tok

#
# macro to handle query calls
#
def _LSEbl_query_call(tinfo,args,typelog,env):
    # returns call replacement and replaces args[0] with
    # (apiname, iname, pointer to query)
    qcall = SIM_analysis._LSEan_query_call(tinfo,args,None,env)

    if not args[0][2]: return qcall # was an error
    
    # remember types
    typelog.update(args[0][2].usesTypes)

    # mark that query was used
    if env.inst:
        env.inst.instSeen[args[0][1]] = 1

    if env.subobj: env.subobj[0].hasIQuery = 1
    
    return qcall

#
# macro to handle structure gets
#
def _LSEbl_struct_get(tinfo,args,extraargs,env):

    # returns replacement and replaces args[0] with
    # (apiname,"kind of element", instance/domain inst pointer, name)
    sg = SIM_apidefs._LSEap_struct_get(tinfo,args,None,env)

    if env.inst:
        if args[0][1] == "field":
            # mark that we used the instance and its structadd types
            env.inst.instSeen[args[0][2].name] = 1
            env.inst.usesTypes.update(args[0][2].structAddUsesTypes)
            
    return sg

#
# macro to handle structure sets
#  this macro only looks for field instance names; it does not do a full
#  check of errors in the call.  For example, it does not check that the
#  field actually exists.
#
def _LSEbl_struct_set(tinfo,args,extraargs,env):

    # returns replacement and replaces args[1] with
    # ("kind of element", instance pointer/domain inst pointer, name)
    sg = SIM_apidefs._LSEap_struct_set(tinfo,args,None,env)

    if env.inst:
        if args[0][1] == "field":
            # mark that we used the instance and its structadd types
            env.inst.instSeen[args[0][2].name] = 1
            env.inst.usesTypes.update(args[0][2].structAddUsesTypes)
            
    return sg

#
# macro to handle an embedded type reference; replaces the embedded
# reference with the "true" type name
#
#   extraargs here is a type log to log the usage
#
def _LSEbl_type_ref(tinfo,args,typelog,env):
    try:
        if (len(args)!=2):
            # we raise a LSEtk_TokenizeException so we get nice line #s 
            raise LSEtk_TokenizeException("LSEut_ref should have 1 "
                                          "argument",tinfo[0])

        tname = str(args[1])
        tentry = env.db.typeMap.get(tname)
        if not tentry:
            raise LSEtk_TokenizeException("Unable to resolve type '%s'" \
                                          % args[0], tinfo[0])

        # mark that type and its descendants were used
        typelog.update(tentry[4])

    except LSEtk_TokenizeException, v:
        _LSEbl_report_error(v)
        return []

    return LSEtk_TokenIdentifier(tentry[2]) # new name

#
# macro to handle an embedded variable reference; updates the type usage of 
#  the instance (if any) and remembers that the instance has used this
#  variable.  Assigns a variable name if it has not been assigned yet.
#
def _LSEbl_var_ref(tinfo,args,typelog,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    try:
        if (len(args)!=2):
            # we raise a LSEtk_TokenizeException so we get nice line #s 
            raise LSEtk_TokenizeException("LSEuv_ref should have 1 argument",
                                          tinfo[0])

        vname = str(args[1])
        ventry = db.varMap.get(vname)
        if not ventry:
            raise LSEtk_TokenizeException("Unable to find variable '%s'" % \
                                          args[1], tinfo[0])

        # mark types used
        typelog.update(ventry[4])
        ventry[6][currinst.name] = None
        
        # Find out whether the variable has a name already.  If it does not,
        # assign the name by hashing the first instance to use it as we
        # traverse elements of the database.  The traversal is deterministic,
        # so the name is deterministic.
        #        
        if (not ventry[3]):
            if currinst: iname = env.inst.name
            else: iname = "LSEtl"
            hash = LSE_hashwrap.hashit(iname)[:16]
            hashval = hash + ("_%s" % ventry[1])
            seqno = _LSEbl_varHash.get(hashval,0)
            _LSEbl_varHash[hashval] = seqno+1
            newname = "LSEuv_be_%s_%d" % (hashval, seqno)
            ventry[3] = (newname,hash)
            db.varBEMap[newname] = ventry
            tok = LSEtk_TokenIdentifier(newname)
        else:
            tok = LSEtk_TokenIdentifier(ventry[3][0])

        if codeloc != LSEcg_in_clm and codeloc < LSEcg_in_domaincode:
            key = LSEcg_fkey(codeloc, subinst, LSEcg_fkey_uv_ref)
            fl = currinst.toFinish.setdefault(key,[])
            # because runtime vars are generally used multiple times in
            # a particular context, we save a lot of unnecessary pointers
            # if we keep a list of tokens to change for this particular
            # runtime variable.
            for f in fl:
                if f[1] == tok.contents:
                    idno = f[0]
                    break
            else:
                idno = len(fl)
                fl.append([idno, tok.contents, 0 ])
            tok = LSEtk_TokenOther("LSEfi_uv{%d,%s}" % \
                                   (idno,ventry[2]))
            
    except LSEtk_TokenizeException, v:
        _LSEbl_report_error(v)
        return []
        
    return tok

#
# macro to handle an embedded type reference; replaces the embedded
# reference with the "true" type name
#
#   extraargs here is a type log to log the usage
#
def _LSEbl_domaininst_ref(tinfo,args,typelog,env):
    try:
        if (len(args)!=2):
            # we raise a LSEtk_TokenizeException so we get nice line #s 
            raise LSEtk_TokenizeException("LSEdi_ref should have 1 "
                                          "argument",tinfo[0])

        iname = str(args[1]).replace(".","__")

        dinst = env.db.domainInstances.get(iname)
        if not dinst:
            raise LSEtk_TokenizeException("Unable to find domain instance '%s'"
                                          % args[1], tinfo[0])

        dinst.used = 1
    
    except LSEtk_TokenizeException, v:
        _LSEbl_report_error(v)
        return []

    return LSEtk_TokenIdentifier(LSEcg_domain_namespace_mangler(iname,0))


################### SIM_codepoint_info.py ##################

_LSEbl_codePointDefDict = {
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEuv_ref" : [(LSEtk_Tok_macro,0,_LSEbl_var_ref,_LSEbl_typeLog)],
    "LSEuv_rp_ref" : [(LSEtk_Tok_macro,0,_LSEbl_runtimeparm_ref,
                       _LSEbl_typeLog)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    "LSE_port_type" : [(LSEtk_Tok_macro,0,_LSEbl_port_type,_LSEbl_typeLog)],
    }

#
#  Add funcheader text to an instance
#     Checks for parenthesis/quote/comment nesting problems
#
def add_funcheader_to_inst(iname, htext):
    inst = _LSEbl_env.db.getInst(iname)
    inst.funcheader.append(htext) # will process later
    
#
#  Add module body text to an instance
#     Checks for parenthesis/quote/comment nesting problems
#
def add_body_to_inst(iname, htext):
    inst = _LSEbl_env.db.getInst(iname)
    inst.moduleBody.append(htext) # will process later
    
#
#  Add extension text to an instance
#     Checks for parenthesis/quote/comment nesting problems
#
def add_extension_to_inst(iname, htext):
    inst = _LSEbl_env.db.getInst(iname)
    inst.extension.append(htext) # will process later

#
#  Add codepoint text to an instance
#    Checks whether there are parenthesis/quote/comment nesting problems
#
_LSEbl_emptycp = "return LSE_controlpoint_call_empty();\n"

def add_codepoint_to_inst(iname,cpname, type, text,
                          dtext, rtype="", params="", makemethod=0):

    inst = _LSEbl_env.db.getInst(iname)

    # create the point here so that we can use it as context
    point = inst.codepoints.get(cpname)
    if (point==None):
        point = LSEdb_CodePoint(inst,cpname,type)
    else:
        _LSEbl_report_error("Multiple definitions of code point '%s' "
                            "in instance '%s'" %
                            (cpname, iname))
        return # just leave it out
    
    _LSEbl_env.inst = inst
    _LSEbl_env.subobj = (point,None)
    _LSEbl_env.codeloc = LSEcg_in_def
    _LSEbl_typeLog.clear()

    # set up the attributes properly for a control point...
    if (type == LSE_PointControl):
        makemethod = 0
        rtype = "LSE_signal_t"
        params = ( "int instno, const LSE_signal_t istatus, " +
                  "const LSE_signal_t ostatus, LSE_dynid_t id, " +
                  "LSE_port_type("+cpname+") *data")
        if (text == ""): text = _LSEbl_emptycp;
        if (dtext == ""): dtext = _LSEbl_emptycp;

    try:
        # parse the definition pieces
        
        dlist = [ {}, _LSEbl_codePointDefDict]
        loc = " of codepoint '%s' in instance '%s'" % (cpname,iname)
        LSEcg_domain_set_inst_rc(_LSEbl_env.db,inst,0)
        
        rtoks = _LSEbl_tokenizer.processInput(rtype,
                                              "in return type" + loc,
                                              dlist)
        
        ptoks = _LSEbl_tokenizer.processInput(params,
                                              "in parameter list" + \
                                              loc, dlist)

    except LSEtk_TokenizeException, a:
        _LSEbl_report_error(str(a))
        return # just leave out this codepoint
    
    inst.codepoints[cpname] = point
    point.returnType = str(rtoks)
    point.params = string.strip(str(ptoks))
    point.current = text  # keep the text for reprocessing later
    point.default = dtext # keep the text for reprocessing later
    point.isDefault = point.current == point.default
    point.isEmpty = (not string.strip(text)) or \
                    (type == LSE_PointControl and
                     text == _LSEbl_emptycp)
    point.makeMethod = makemethod;

_LSEbl_funcHeaderDict = {
    "LSEut_enumref" : [(LSEtk_Tok_macro,0,_LSEbl_enum_ref,_LSEbl_typeLog)],
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEuv_ref" : [(LSEtk_Tok_macro,0,_LSEbl_var_ref,_LSEbl_typeLog)],
    "LSEuv_rp_ref" : [(LSEtk_Tok_macro,0,_LSEbl_runtimeparm_ref,
                       _LSEbl_typeLog)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    "LSE_port_type" : [(LSEtk_Tok_macro,0,_LSEbl_port_type,_LSEbl_typeLog)], 
    "LSE_dynid_get" : [(LSEtk_Tok_macro,0,_LSEbl_struct_get,None)],
    "LSE_dynid_set" : [(LSEtk_Tok_macro,0,_LSEbl_struct_set,None)],
    "LSE_resolution_get" : [(LSEtk_Tok_macro,0,_LSEbl_struct_get,None)],
    "LSE_resolution_set" : [(LSEtk_Tok_macro,0,_LSEbl_struct_set,None)],
    }

_LSEbl_codePointTextDict = {
    "LSEut_enumref" : [(LSEtk_Tok_macro,0,_LSEbl_enum_ref,_LSEbl_typeLog)],
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEuv_ref" : [(LSEtk_Tok_macro,0,_LSEbl_var_ref,_LSEbl_typeLog)],
    "LSEuv_rp_ref" : [(LSEtk_Tok_macro,0,_LSEbl_runtimeparm_ref,
                       _LSEbl_typeLog)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    "LSE_controlpoint_call_default" : [(LSEtk_Tok_macro,0,
                                   _LSEbl_point_call_default,1)],
    "LSE_event_record" : [(LSEtk_Tok_macro,0,
                           _LSEbl_event_record,_LSEbl_typeLog)],
    "LSE_method_call" : [(LSEtk_Tok_macro,0,
                          _LSEbl_method_call,_LSEbl_typeLog)],
    "LSE_port_query" : [(LSEtk_Tok_macro,0,_LSEbl_port_query,_LSEbl_typeLog)], 
    "LSE_port_type" : [(LSEtk_Tok_macro,0,_LSEbl_port_type,_LSEbl_typeLog)], 
    "LSE_query_call" : [(LSEtk_Tok_macro,0,_LSEbl_query_call,_LSEbl_typeLog)],
    "LSE_dynid_get" : [(LSEtk_Tok_macro,0,_LSEbl_struct_get,None)],
    "LSE_dynid_set" : [(LSEtk_Tok_macro,0,_LSEbl_struct_set,None)],
    "LSE_resolution_get" : [(LSEtk_Tok_macro,0,_LSEbl_struct_get,None)],
    "LSE_resolution_set" : [(LSEtk_Tok_macro,0,_LSEbl_struct_set,None)],
    "LSE_userpoint_call_default" : [(LSEtk_Tok_macro,0,
                                _LSEbl_point_call_default,0)],
    }

def _LSEbl_parse_codepoints(db):
    for inst in db.instanceOrder:
        _LSEbl_typeLog.clear()
        _LSEbl_env.inst = inst

        # fix up the funcheader

        _LSEbl_env.subobj = (inst,None) # in a funcheader
        _LSEbl_env.codeloc = LSEcg_in_funcheader
        htoklist = []
        dlist = [{},_LSEbl_funcHeaderDict]

        try:
            LSEcg_domain_set_inst_rc(db,inst,1)
            for h in inst.funcheader:
                htoks = _LSEbl_tokenizer.processInput(h, "in funcheader of " \
                                                      "instance '%s'" % \
                                                      (inst.name), dlist)
                htoklist.append(htoks)
                # we know that queries had some....
                LSEcg_run_fixups()
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            return # just leave out the funcheader

        inst.funcheader = reduce(lambda x,y:x+str(y),htoklist,"")

        # fix up the module body

        _LSEbl_env.subobj = (inst,None) # in a module body
        _LSEbl_env.codeloc = LSEcg_in_modulebody
        htoklist = []
        dlist = [ {}, _LSEbl_codePointTextDict ]

        try:
            LSEcg_domain_set_inst_rc(db,inst,1)
            for h in inst.moduleBody:
                htoks = _LSEbl_tokenizer.processInput(h, "in module body of " \
                                                      "instance '%s'" % \
                                                      (inst.name), dlist)
                htoklist.append(htoks)
                # we know that queries had some....
                LSEcg_run_fixups()
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            return # just leave out the funcheader

        inst.moduleBody = reduce(lambda x,y:x+str(y),htoklist,"")

        # fix up the extensions

        _LSEbl_env.subobj = (inst,None) # in an extension
        _LSEbl_env.codeloc = LSEcg_in_extension
        htoklist = []
        dlist = [ {}, _LSEbl_codePointTextDict ]

        try:
            LSEcg_domain_set_inst_rc(db,inst,1)
            for h in inst.extension:
                htoks = _LSEbl_tokenizer.processInput(h, "in extension of " \
                                                      "instance '%s'" % \
                                                      (inst.name), dlist)
                htoklist.append(htoks)
                # we know that queries had some....
                LSEcg_run_fixups()
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            return # just leave out the funcheader

        inst.extension = reduce(lambda x,y:x+str(y),htoklist,"")

        # now do codepoints
        
        dlist = [ {}, _LSEbl_codePointTextDict ]
        
        for point in inst.codepointOrder:

            if point.type == LSE_PointControl:
                _LSEbl_env.codeloc = LSEcg_in_controlpoint
                if not inst.ports[point.name].width:
                    # do not process control points of unconnected ports
                    point.current = "" # LSEtk_TokenTopLevel([])
                    point.default = "" # LSEtk_TokenTopLevel([])
                    continue
            else:
                _LSEbl_env.codeloc = LSEcg_in_userpoint

            try:
                # now reparse the contents of the point...
        
                # process the current point before the default so that I can
                # know whether the default is used as I process APIs in the
                # default
                loc = " of codepoint '%s' in instance '%s'" % (point.name,
                                                               inst.name)
                LSEcg_domain_set_inst_rc(db,inst,1)
                _LSEbl_env.subobj = (point,0)

                ttoks = _LSEbl_tokenizer.processInput(point.current,
                                                      "in text" + loc,dlist)

                if (point.usesDefault):
                    LSEcg_domain_set_inst_rc(db,inst,0)
                    _LSEbl_env.subobj = (point,1)
                    dtoks = _LSEbl_tokenizer.processInput(point.default,
                                                          "in default text" \
                                                          + loc,
                                                          dlist)
                else:
                    dtoks = LSEtk_TokenTopLevel([])
                    
                    # we know that queries had some....
                LSEcg_run_fixups()
            except LSEtk_TokenizeException, a:
                _LSEbl_report_error(str(a))
                ttoks = LSEtk_TokenTopLevel([])
                dtoks = LSEtk_TokenTopLevel([])

            point.current = ttoks
            point.default = dtoks

            if point.type == LSE_PointControl:
                SIM_analysis.LSEan_analyze_a_cf(db,inst.ports[point.name],
                                                point)

            point.current = str(point.current)
            point.default = str(point.default)
            
        # back to instance level
        inst.usesTypes.update(_LSEbl_typeLog)
            
################ SIM_domain_info.py ######################

standardpaths=[LSE+"/include/lse", LSE+"/include/domains"]

def _LSEbl_find_header(i,dinst,dodefault=1):
    f = LSE_domain.findFile(i,dinst.implLibPath + standardpaths)
    #sys.stderr.write("%s in %s => %s\n" % (i,dinst.implLibPath+standardpaths,
    #                                       f))
    if f is None and dodefault: return i
    else: return f

#
# Add a domain instance; may also need to add the domain class
#
def add_domain_instance(dclassname, dbuildargs, dinstname, drunargs):
    dinstname = _LSEbl_flatten_domain_name(dinstname);
    
    if _LSEbl_readpass==0:
        LSE_db = _LSEbl_env.db

        savewd = os.getcwd()
        
        try:
            # make the domain class exist
            dmod = LSE_db.domains.get(dclassname)
            if not dmod:
                dom = _LSEbl_init_domain_class(LSE_db,dclassname)
                if not dom: return # class was not loaded
            else:
                dom = dmod[0]

            # TODO: this should be done as a new implementation, and we
            # ought to pass in something to help (in pname parm?)...

            # create and store domain object
            pname = LSE_db.topDir + "/domains/I/" + dinstname
            #os.chdir(LSE_db.topDir)
            os.chdir(LSE_db.workDir)
            dinst = dom(dinstname, dbuildargs, drunargs, pname)

            dinst.implLibPath.extend(dom.classLibPath)
                
            LSE_db.domainInstances[dinstname] = dinst
            LSE_db.domains[dclassname][1].append(dinst)

            # translate implementation headers
            dinst.implHeaders = map(lambda x,dinst=dinst:\
                                        _LSEbl_find_header(x,dinst,1),
                                    dinst.implHeaders)            

            # translate instance headers
            dinst.instHeaders = map(lambda x,dinst=dinst:\
                                    LSE_domain.findFile(x,dinst.instLibPath,1),
                                    dinst.instHeaders)        

            # generate renaming maps
            dinst.renamedNamespaces = {}
            for ns in dinst.implNamespaces:
                dinst.renamedNamespaces[ns] = ns

            dinst.renamedHeaders = {}  
            if dinst.implRename:
                dinst.implName = dinst.instName + "__" + dinst.implName
                dinst.renameWorklist = []
                for header in dinst.implRenameHeaders:
                    dir = os.path.dirname(header)
                    newdir = LSE_db.topDir + "/include/LSE_dimpl/" + \
                        dinst.instName + "/" + dir
                    newfile = newdir + "/" + os.path.basename(header)
                    realhead = _LSEbl_find_header(header,dinst,0)
                    if realhead:
                        dinst.renamedHeaders[realhead] = "LSE_dimpl/" + \
                            dinst.instName + "/" + header
                        dinst.renameWorklist.append((newdir, realhead, newfile))
                    else:
                        raise LSE_domain.LSE_DomainException(\
                            "Unable to find header %s" % header)
                for ns in dinst.implRenameNamespaces:
                    dinst.renamedNamespaces[ns] = dinst.instName + "__" + ns

            os.chdir(savewd)

            # fix up the identifiers
            dinst.implIdentifiers = map(\
                lambda x:(x[0],x[1]|LSE_domain.LSE_domainID_impl,x[2]),
                dinst.implIdentifiers)

            dinst.instIdentifiers = map(\
                lambda x:(x[0],x[1]|LSE_domain.LSE_domainID_inst,x[2]),
                dinst.instIdentifiers)

            findshared = LSE_db.dinstanceMap.get((dclassname,dbuildargs,
                                                  drunargs), None)

            if findshared is not None and not findshared.implRename:
                dinst.useName = findshared.instName
                dinst.suppressed = 1
            else:
                LSE_db.dinstanceMap[(dclassname,dbuildargs,drunargs)] = dinst
                dinst.suppressed = 0
                dinst.useName = dinstname

        except LSE_domain.LSE_DomainException,v:
            _LSEbl_report_error(str(v))
            os.chdir(savewd)
            return

def _LSEbl_flatten_domain_name(s):
    return s.replace(".","__")

def _LSEbl_filter_searchpath(l,h):
    found = {}
    nh = []
    nl = []
    for i in l:
        if not found.get(i[0]):
            found[i[0]]=1
            nl.append((i[0],_LSEbl_flatten_domain_name(i[1])))
    for i in h:
        if not found.get(i[0]):
            found[i[0]]=1
            nl.append((i[0],_LSEbl_flatten_domain_name(i[1])))
    return (nl, nh)
    
#
# Add a domain searchpath for an module instance
#
def add_domain_searchpath(iname, lpath, hpath):
    if _LSEbl_readpass==1:
        if iname:
            inst = _LSEbl_env.db.getInst(iname)
        else:
            inst = _LSEbl_env.db.getInst("LSEfw_top")

        # filter the search paths
        found = {}
        nh = []
        nl = []
        for i in lpath:
            if not found.get(i[0]):
                found[i[0]]=1
                nn = _LSEbl_flatten_domain_name(i[1])
                nl.append((i[0],nn))
                _LSEbl_env.db.domainInstances[nn].used = 1
        for i in hpath:
            if not found.get(i[0]):
                found[i[0]]=1
                nn = _LSEbl_flatten_domain_name(i[1])
                nl.append((i[0],nn))
                _LSEbl_env.db.domainInstances[nn].used = 1
        inst.domainSearchPath = (nl,nh)
        
_LSEbl_splitpattern = re.compile("(LSE_domain_macros|LSE_domain_header|"
                                 "LSE_domain_code|LSE_domain_inst_defs|"
                                 "LSE_domain_class_defs)")

#
# load the domain class; create a proper object for it
#
def _LSEbl_init_domain_class(db,dclassname):
    LSE_db = _LSEbl_env.db

    # read in domain class definition (the module)
    try:
        minfo = (None)
        minfo = imp.find_module(dclassname)
        mobj = imp.load_module(dclassname,minfo[0],minfo[1],
                               minfo[2])
        if minfo[0]: minfo[0].close()
    except ImportError:
        if minfo and minfo[0]: minfo[0].close()
        _LSEbl_report_error("Unable to load Python module for domain "
                            "class %s" % dclassname)
        return None

    savewd = os.getcwd()
    
    dom = mobj.LSE_DomainObject
    dom.classLibPath += [ os.path.dirname(mobj.__file__),
                         LSE_DATA + "/domains/" + dclassname ]

    # translate class headers
    nih = []
    for i in dom.classHeaders:
        f = LSE_domain.findFile(i, dom.classLibPath)
        if f is None: nih.append(i)
        else: nih.append(f)
    dom.classHeaders = nih

    dom.classIdentifiers = map(\
        lambda x:(x[0],x[1]|LSE_domain.LSE_domainID_class,x[2]),
        dom.classIdentifiers)

    db.domains[dclassname]=(dom,[])
    
    return dom

#
# tag type usage
#
def _LSEbl_domain_resolve(tinfo, args, id, env):
    retinfo = []
    rval = LSEcg_domain_resolve(tinfo,args,id,env,retinfo)
    if rval:
        _LSEbl_typeLog[(1,retinfo[0],retinfo[1])] = id # mark a domain use
        if retinfo[1] is None:
            env.db.domains[retinfo[0]][0].used = 1
        else:
            env.db.domainInstances.get(retinfo[1]).used = 1
            
    return rval

#
# tag type usage
#
def _LSEbl_domain_class_resolve(tinfo, args, id, env):
    retinfo = []
    rval = LSEcg_domain_class_resolve(tinfo,args,id,env,retinfo)
    if rval:
        _LSEbl_typeLog[(1,retinfo[0],retinfo[1])] = id # mark a domain use
        if retinfo[1] is None:
            env.db.domains[retinfo[0]][0].used = 1
        else:
            env.db.domainInstances.get(retinfo[1]).used = 1
            
    return rval

#
# Create the domain API dictionary to be used in later parsing
#
def _LSEbl_create_domain_apis(db):

    known_ids = {}
    ds = ""
    us = ""

    # Domain class constants and types

    for dom in db.domainOrder:
        dclass = dom[0]
        dclass.identifiers = {}
        dclass.impls = {}
        dclass.tokAPIs = {}
        
        db.domainAPIs[dclass.className] = [ (LSEtk_Tok_macro, LSEtk_NoArgs,
                                             _LSEbl_domain_class_resolve, 
                                             dclass.className) ]
        for (name,idtype,impl) in dclass.classIdentifiers:
            nidt = idtype | LSE_domain.LSE_domainID_class
            dclass.identifiers[name] = nidt
            db.domainAPIs[name] = [ (LSEtk_Tok_macro, LSEtk_NoWhiteSpace,
                                     _LSEbl_domain_resolve, name) ]
            #sys.stderr.write("Defined %s in create_domain_apis\n" % name)
            if (idtype & LSE_domain.LSE_domainID_tokmacro):
                dclass.impls[name] = impl
                newname = LSEcg_domain_mangler(name,(dclass.className,""),
                                               idtype)
                #sys.stderr.write("Defined %s in create_domain_apis\n" % newname)
                db.domainAPIs[newname] = [ (LSEtk_Tok_macro,
                                            LSEtk_NoWhiteSpace,
                                            LSEcg_domain_tok_invoke,
                                            (name,impl,
                                             (dclass.className,None)))]
                    
        # create the merged information
        try:
            mifunc = sys.modules[dclass.__module__].createMergedInfo
        except:
            mifunc = LSE_domain.createMergedInfo
            
        try:
            mifunc(dclass,dom[1])
        except LSE_domain.LSE_DomainException, v:
            _LSEbl_report_error(str(v))
            dclass.mergedIdentifiers = []
        else:
            dclass.mergedIdentifiers = map(
                lambda x:(x[0],x[1]|LSE_domain.LSE_domainID_class,x[2]),
                dclass.mergedIdentifiers)
    
        for (name,idtype,impl) in dclass.mergedIdentifiers:
            nidt = idtype | LSE_domain.LSE_domainID_class
            dclass.identifiers[name] = idtype | LSE_domain.LSE_domainID_class
            #sys.stderr.write("Defined %s in create_domain_apis\n" % name)
            db.domainAPIs[name] = [ (LSEtk_Tok_macro, LSEtk_NoWhiteSpace,
                                     _LSEbl_domain_resolve, name) ]
            if (idtype & LSE_domain.LSE_domainID_tokmacro):
                dclass.impls[name] = impl
                newname = LSEcg_domain_mangler(name,(dclass.className,""),nidt)
                #sys.stderr.write("Defined %s in create_domain_apis\n" % newname)
                db.domainAPIs[newname] = [ (LSEtk_Tok_macro,
                                            LSEtk_NoWhiteSpace,
                                            LSEcg_domain_tok_invoke,
                                            (name,impl,
                                             (dclass.className,None)))]

        # Domain instance constants, types, and APIs

        for dinst in dom[1]:
            dinst.tokAPIs = copy.copy(dclass.tokAPIs)
            
            dinst.identifiers = {}
            dinst.impls = {}
            # pull in class identifiers
            dinst.identifiers.update(dclass.identifiers)
            dinst.impls.update(dclass.impls)
            
            for (name,idtype,impl) in dinst.instIdentifiers:
                nidt = idtype | LSE_domain.LSE_domainID_inst
                dinst.identifiers[name] = nidt
                #sys.stderr.write("Defined %s in create_domain_apis\n" % name)
                db.domainAPIs[name] = [ (LSEtk_Tok_macro, LSEtk_NoWhiteSpace,
                                         _LSEbl_domain_resolve, name) ]
                if (idtype & LSE_domain.LSE_domainID_tokmacro):
                    dinst.impls[name] = impl
                    #sys.stderr.write("Defined %s in create_domain_apis\n" % newname)
                    newname = LSEcg_domain_mangler(name,("",dinst.instName),
                                                   nidt)
                    db.domainAPIs[newname] = [ (LSEtk_Tok_macro,
                                                LSEtk_NoWhiteSpace,
                                                LSEcg_domain_tok_invoke,
                                                (name,impl,
                                                 (dinst.className,
                                                  dinst.instName)))]

    #####################################################################
    # and take care of macro implementations that are m4 macros deferred
    # to the macrofile....  This is done by expanding the macrofile section
    # for macros and storing the results in the domainAPIs dictionary
    # as well....  note that this routine generates bogus output; so I want
    # to make that go to the bit bucket!
    savestdout = sys.stdout
    sys.stdout = open("/dev/null","w")
    LSEcg_domain_generate_ids(db,[],_LSEbl_env,3)
    sys.stdout = savestdout
    
#
# Record types used by the domain attributes added to dynids
#
def _LSEbl_mark_domain_attributes(db):
    for dinst in db.domainInstanceOrder:
        _LSEbl_typeLog.clear() # clear the log
        for k in dinst.instAttributes.items():
            loc = "in '%s' attribute of domain instance '%s'" % \
                  (k[0],dinst.instName)
            try:
                toks = _LSEbl_tokenizer_light.processInput(k[1],loc,
                                                            [{}])
            except LSEtk_TokenizeException, a:
                _LSEbl_report_error(str(a))

        dinst.attributeTypeUses = _LSEbl_typeLog.copy()
    
################### SIM_event_info.py #####################

_LSEbl_eventDefDict = {
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    "LSE_port_type" : [(LSEtk_Tok_macro,0,_LSEbl_port_type,_LSEbl_typeLog)],
    }

#
# add an event
#
def add_event_to_inst(iname,ename,nstring,rstring):
    if _LSEbl_readpass==0:
        if iname:
            mstring = " in instance '%s'" % iname
            inst = _LSEbl_env.db.getInst(iname)
        else:
            mstring = " in top-level"
            inst = _LSEbl_env.db.getInst("LSEfw_top")

        ev = LSEdb_Event(inst,ename)

        _LSEbl_env.inst = inst
        _LSEbl_env.subobj = (ev, None)
        _LSEbl_typeLog.clear()
        _LSEbl_env.codeloc = LSEcg_in_def

        LSEcg_domain_set_inst_rc(_LSEbl_env.db,inst,0)
        dlist = [{},_LSEbl_eventDefDict]

        try:
            ntoks = _LSEbl_tokenizer.processInput(nstring,
                                                  ("in argument names of "
                                                   "event '%s'" +
                                                   mstring) % ename,dlist)
            rtoks = _LSEbl_tokenizer.processInput(rstring,
                                                  ("in argument types of "
                                                   "event '%s'" +
                                                   mstring) % ename,dlist)

        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            return # leave out this event

        ev.addDefInfo(str(ntoks),str(rtoks))
        ev.usesTypes = copy.copy(_LSEbl_typeLog)

        inst.events[ename]=ev
        inst.usesTypes.update(_LSEbl_typeLog)

#
# add a data collector
#
def add_collector_to_inst(iname,ename,headstring,declstring,
                          initstring,recstring,repstring):
    if _LSEbl_readpass==0:
        if iname:
            inst = _LSEbl_env.db.getInst(iname)
            ev = inst.events[ename]
        else:
            inst = _LSEbl_env.db.getInst("LSEfw_top")
            ev = inst.events[ename]

        # we do not tokenize yet...
        coll = LSEdb_Collector(ev, headstring, declstring,initstring,
                               recstring,repstring)
        ev.filled.append(coll)

#
# and deal with fixing it up once we know what user points are there...
#
_LSEbl_eventTextDict = {
    "LSEut_enumref" : [(LSEtk_Tok_macro,0,_LSEbl_enum_ref,_LSEbl_typeLog)],
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    "LSE_event_record" : [(LSEtk_Tok_macro,0,
                           _LSEbl_event_record,_LSEbl_typeLog)],
    "LSE_method_call" : [(LSEtk_Tok_macro,0,
                          _LSEbl_method_call,_LSEbl_typeLog)],
    "LSE_port_query" : [(LSEtk_Tok_macro,0,_LSEbl_port_query,_LSEbl_typeLog)], 
    "LSE_port_type" : [(LSEtk_Tok_macro,0,_LSEbl_port_type,_LSEbl_typeLog)],
    "LSEuv_ref" : [(LSEtk_Tok_macro,0,_LSEbl_var_ref,_LSEbl_typeLog)],
    "LSEuv_rp_ref" : [(LSEtk_Tok_macro,0,_LSEbl_runtimeparm_ref,
                       _LSEbl_typeLog)],
    "LSE_dynid_get" : [(LSEtk_Tok_macro,0,_LSEbl_struct_get,
                        ("LSE_dynid_t",_LSEbl_typeLog))],
    "LSE_dynid_set" : [(LSEtk_Tok_macro,0,_LSEbl_struct_set,
                        ("LSE_dynid_t",_LSEbl_typeLog))],
    "LSE_resolution_get" : [(LSEtk_Tok_macro,0,_LSEbl_struct_get,
                             ("LSE_resolution_t",_LSEbl_typeLog))],
    "LSE_resolution_set" : [(LSEtk_Tok_macro,0,_LSEbl_struct_set,
                             ("LSE_resolution_t",_LSEbl_typeLog))],
    }

def _LSEbl_handle_collector(inst,ev,coll):
        if inst: mstring = "in instance '%s'" % inst.name
        else: mstring = "in top-level"
        
        _LSEbl_env.inst = inst
        #_LSEbl_typeLog.clear()
        _LSEbl_env.codeloc = LSEcg_in_datacoll

        loc = " of collector for event '%s' %s" % (ev.name, mstring)
        LSEcg_domain_set_inst_rc(_LSEbl_env.db,inst,1)
        dlist = [{},_LSEbl_eventTextDict]
        try:
            _LSEbl_env.subobj = (coll,0)
            htoks = _LSEbl_tokenizer.processInput(coll.header,
                                                  "in header section" +\
                                                  loc,dlist)
            _LSEbl_env.subobj = (coll,0)
            dtoks = _LSEbl_tokenizer.processInput(coll.decl,
                                                  "in declaration section" +\
                                                  loc,dlist)
            _LSEbl_env.subobj = (coll,1)
            itoks = _LSEbl_tokenizer.processInput(coll.init,
                                                  "in init section" + loc,
                                                  dlist)
            _LSEbl_env.subobj = (coll,2)
            ctoks = _LSEbl_tokenizer.processInput(coll.record,
                                                  "in record section" + loc,
                                                  dlist)
            _LSEbl_env.subobj = (coll,3)
            rtoks = _LSEbl_tokenizer.processInput(coll.report,
                                                  "in report section" + loc,
                                                  dlist)
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            coll.header = ""
            coll.decl = ""
            coll.init = ""
            coll.record = ""
            coll.report = ""
            return
        coll.header = str(htoks)
        coll.decl = str(dtoks)
        coll.init = str(itoks)
        coll.record = str(ctoks)
        coll.report = str(rtoks)
        
#
# Actually expand the data collectors....
#
def _LSEbl_parse_events(db):
    _LSEbl_typeLog.clear()
    for e in db.eventOrder:
        for c in e.filled:
            _LSEbl_handle_collector(None,e,c)
    for i in db.instanceOrder:
        _LSEbl_typeLog.clear()
        for e in i.eventOrder:
            for c in e.filled:
                _LSEbl_handle_collector(i,e,c)
        i.usesTypes.update(_LSEbl_typeLog)

################## SIM_instance_info.py ######################

def add_inst(iname, mtype, start, phase, end,strict,reactive, tarball):
    if _LSEbl_readpass & 1:
        LSEdb_MInstance(_LSEbl_env.db,iname,mtype,
                        start,phase,end,strict,reactive,tarball)

def add_dep_annotation(iname, annotation):
    if _LSEbl_readpass & 2:
        inst = _LSEbl_env.db.getInst(iname)
        inst.dependencyAnnotation = annotation
    
###################### SIM_parm_info.py #######################

#  we track:
#   enumerated values
#   types
#   we can even find port width and type used through literal parms...
# This dictionary must contain APIs which are not going to be in dictionaries
# when the .clm file is being processed.  These APIs are the internal ones
# coming out of LSS...
_LSEbl_parmDict = {
    "LSEut_enumref" : [(LSEtk_Tok_macro,0,_LSEbl_enum_ref,_LSEbl_typeLog)],
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    "LSEuv_ref" : [(LSEtk_Tok_macro,0,_LSEbl_var_ref,_LSEbl_typeLog)],
    "LSEuv_rp_ref" : [(LSEtk_Tok_macro,0,_LSEbl_runtimeparm_ref,
                       _LSEbl_typeLog)],
    }

#
#  Add a parameter to an instance
#    NOTE: we ignore any parameter on an instance that is just "hierarchy"
#
def add_parm_to_inst(i,p,v,t,runtimeable=0,rtflag=0,clname=None,desc=None):
    
    db = _LSEbl_env.db
    if i:
        inst = db.getInst(i,1)
        if not inst:
            # An intermediate level of hierarchy; continue,
            # but do not remember the parms in the database except as
            # runtime parms
            plist = {}
            i = ""
            _LSEbl_env.inst = inst = None
        else:
            plist = inst.parms
            _LSEbl_env.inst = inst
    else:
        plist = db.parms
        _LSEbl_env.inst = inst = None

    _LSEbl_env.subobj = None         # no object....
    _LSEbl_env.codeloc = LSEcg_in_def # really does not matter because we are
                                 # only doing global var and type refs...

    # once upon a time we had to deal with redefinitions; that is no longer
    # true because LSS resolves parameters nicely for us...

    _LSEbl_typeLog.clear() # clear the type log

    # parse value if it is a string
    if type(v) == types.StringType:
        try:
            # the full tokenizer is OK because we are in a definition
            # context; things like port widths and types can safely be
            # put into literal parameters, though we don't necessarily
            # encourage such bizarreness....
            loc = "in parameter '%s' value in instance '%s'" % (p,i)
            vtoks = _LSEbl_tokenizer.processInput(v,loc,
                                                  [{},_LSEbl_parmDict])
            v = str(vtoks) # back to a string for later processing...
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            v = ""
            
    # translate type to the backend type and mark the use
    tentry = _LSEbl_env.db.typeMap.get(t)
    if tentry: # mark that type and its dependents were used
        tname = tentry[2]
        if inst: inst.usesTypes.update(tentry[4])
    else:
        loc = "in type of parameter '%s' in instance '%s'" % (p,i)
        dlist = [{}]

        LSEcg_domain_set_inst_rc(_LSEbl_env.db,inst,1)
            
        try:
            # we use a light tokenizer as we really do not want to expand
            # anything but what we have declared here...
            toks = _LSEbl_tokenizer_light.processInput(t,loc,dlist)
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            toks = "LSE_type_none"

        tname = str(toks)
    
    if inst: # mark any types used in translating the value
        inst.usesTypes.update(_LSEbl_typeLog)

    if rtflag: # if run-timed....
        if (not LSEcg_runtimeable_types.has_key(tname)):
            _LSEbl_report_error("Unable to make parameter '%s' runtimeable\n" %
                                 p)
            plist[p] = (v, tname, 0, 0)
        else:
            rtp = db.runtimeParms.get(clname)
            # hashing gives both a stable name and one that can include
            # non-identifier characters (i.e. ':')
            vname = "LSEuv_rt_%s" % LSE_hashwrap.hashit(clname)[:16]
            if not rtp:
                rtp = db.runtimeParms[clname] = (vname,v,tname,desc,{})
            if tentry:
                rtp[4].update(tentry[4])
            plist[p] = (vname,tname,1,1)
    else:
        plist[p] = (v, tname, 0, runtimeable)

    if t == "LSE_domain_t":
        db.domainInstances[v.replace(".","__")].used = 1
        
    #sys.stderr.write("Added parameter %s %s\n" % (p,plist[p]))

###################### SIM_port_info.py #######################

def add_port_to_inst(iname, pname, width, dir, type, indep, handler):
    if _LSEbl_readpass==0:
        inst = _LSEbl_env.db.getInst(iname)

        tentry = _LSEbl_env.db.typeMap.get(type)
        if tentry:
            tname = tentry[2]
            inst.usesTypes.update(tentry[4])
            #_LSEbl_env.db.potentialLiteralUsesTypes.update(tentry[4])
        else:
            loc = "in type of port '%s' in instance '%s'" % (pname,iname)
            dlist = [{}]
            _LSEbl_env.subobj = None         # no object....
            _LSEbl_env.codeloc = LSEcg_in_def

            LSEcg_domain_set_rc(_LSEbl_env.db,[])
            try:
                # we use a light tokenizer as we really do not want to expand
                # anything but what we have declared here...
                toks = _LSEbl_tokenizer_light.processInput(type,loc,dlist)

            except LSEtk_TokenizeException, a:
                _LSEbl_report_error(str(a))
                toks = "LSE_type_none"

            tname = str(toks)
            
        inst.ports[pname]=LSEdb_Port(inst,pname,width,dir,tname,
                                     indep,handler)

def _LSEbl_base_port_name(pname):
	loc1 = string.find(pname,"[")
	loc2 = string.rfind(pname,"]")
	if (loc1 < 0 and loc2 < 0): return pname
	if (loc1 >=0 and loc2 >= 0 and loc1 < loc2):
		return pname[:loc1]
	raise LSEdb_Exception("Malformed port name "+pname)

def _LSEbl_port_index(pname):
    loc1 = string.find(pname,"[")
    loc2 = string.rfind(pname,"]")
    if (loc1 < 0 and loc2 < 0):
        raise LSEdb_Exception("Malformed port name " + pname)
    if (loc1 >=0 and loc2 >= 0 and loc1 < loc2 and (loc2-loc1) > 1):
        return pname[loc1+1:loc2]
    raise LSEdb_Exception("Malformed port name "+pname)

def connect_ports(sname, tname):
    if _LSEbl_readpass==1:
        sparts=string.split(sname,":")
        tparts=string.split(tname,":")
        sinst = _LSEbl_env.db.getInst(sparts[0])
        tinst = _LSEbl_env.db.getInst(tparts[0])
        sport = sinst.ports[_LSEbl_base_port_name(sparts[1])]
        tport = tinst.ports[_LSEbl_base_port_name(tparts[1])]
        sindex = string.atoi(_LSEbl_port_index(sparts[1]))
        tindex = string.atoi(_LSEbl_port_index(tparts[1]))
        sport.connections[sindex]=(tport,tindex)
        tport.connections[tindex]=(sport,sindex)
        #sys.stderr.write("Connected %s:%s[%d] to %s:%s[%d]\n" %
        #		 (sport.inst.name,sport.name,sindex,
        #		  tport.inst.name,tport.name,tindex))

def alias_ports(hname, dir, rports):
    if _LSEbl_readpass==1:
        hparts = string.split(hname,":")
        # do not alias real ports
        inst = _LSEbl_env.db.instances.get(hparts[0],0)
        if inst and inst.ports.get(hparts[1]): return

        # create connection list
        clist = []
        for r in rports:
            if r:
                rparts = string.split(r,":")
                rinst = _LSEbl_env.db.getInst(rparts[0])
                rport = rinst.ports[_LSEbl_base_port_name(rparts[1])]
                rindex = string.atoi(_LSEbl_port_index(rparts[1]))
                clist.append( (rinst.name,rport.name,rindex) )
            else:
                clist.append(None)
                
        # now add to alias list
        aentry = _LSEbl_env.db.portAliases.get(hparts[0])
        if not aentry:
            aentry = (hparts[0],[])
            _LSEbl_env.db.portAliases[hparts[0]] = aentry
        aentry[1].append((hparts[1],clist,dir))
        
########################## SIM_query_info.py #####################

# We need to translate type references, port types, port widths
#

_LSEbl_queryDict = {
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    "LSE_port_type" : [(LSEtk_Tok_macro,0,_LSEbl_port_type,_LSEbl_typeLog)],
    }

def add_query_to_inst(iname, qname, rtype, params):
    inst = _LSEbl_env.db.getInst(iname)

    q = LSEdb_Query(inst,qname,1)
    
    _LSEbl_env.inst = inst
    _LSEbl_env.subobj = (q)
    _LSEbl_env.codeloc = LSEcg_in_def
    _LSEbl_typeLog.clear()
    
    LSEcg_domain_set_inst_rc(_LSEbl_env.db,inst, 0)
    dlist = [{},_LSEbl_queryDict]
    
    try:
        rtoks = _LSEbl_tokenizer.processInput(rtype,
                                              "in return type of query "
                                              "'%s' in instance '%s'" %
                                              (qname, iname),dlist)
    except LSEtk_TokenizeException, a:
        _LSEbl_report_error(str(a))
        return # just leave out

    try:
        ptoks = _LSEbl_tokenizer.processInput(params,
                                              "in parameters of query "
                                              "'%s' in instance '%s'" %
                                              (qname, iname),dlist)
    except LSEtk_TokenizeException, a:
        _LSEbl_report_error(str(a))
        return # just leave out

    inst.queries[qname]=q
    q.setInfo(str(rtoks),string.strip(str(ptoks)))
    
    if (rtype=="void"):
        _LSEbl_report_error("Query '%s' in instance '%s' has no return type" %
                            (qname,inst.name))

    q.usesTypes = copy.copy(_LSEbl_typeLog)
    inst.usesTypes.update(_LSEbl_typeLog)
    #_LSEbl_env.db.potentialLiteralUsesTypes.update(_LSEbl_typeLog)

def add_method_to_inst(iname, qname, rtype, params, locked):
    inst = _LSEbl_env.db.getInst(iname)
    
    q = LSEdb_Method(inst,qname, locked)
    
    _LSEbl_env.inst = inst
    _LSEbl_env.subobj = (q)
    _LSEbl_env.codeloc = LSEcg_in_def
    _LSEbl_typeLog.clear()

    LSEcg_domain_set_inst_rc(_LSEbl_env.db,inst, 0)
    dlist = [{},_LSEbl_queryDict]
    
    try:
        rtoks = _LSEbl_tokenizer.processInput(rtype,
                                              "in return type of method "
                                              "'%s' in instance '%s'" %
                                              (qname, iname),dlist)
    except LSEtk_TokenizeException, a:
        _LSEbl_report_error(str(a))
        return # just leave out

    try:
        ptoks = _LSEbl_tokenizer.processInput(params,
                                              "in parameters of method "
                                              "'%s' in instance '%s'" %
                                              (qname, iname),dlist)
    except LSEtk_TokenizeException, a:
        _LSEbl_report_error(str(a))
        return # just leave out

    inst.queries[qname]=q
    q.setInfo(str(rtoks),string.strip(str(ptoks)))
    
    q.usesTypes = copy.copy(_LSEbl_typeLog)
    inst.usesTypes.update(_LSEbl_typeLog)
    #_LSEbl_env.db.potentialLiteralUsesTypes.update(_LSEbl_typeLog)

########################## SIM_struct_info.py #####################

# We need to translate type references and port types and widths
#

_LSEbl_structDict = {
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    "LSE_port_type" : [(LSEtk_Tok_macro,0,_LSEbl_port_type,_LSEbl_typeLog)],
    }


_LSEbl_word_re = re.compile("\w+")
_LSEbl_remove_dotspace = re.compile("\s*\.\s*")

#
# add fields to structures
#
def add_to_struct(s,i,flist):
    LSE_db = _LSEbl_env.db
    s = string.strip(s)
    i = _LSEbl_remove_dotspace.sub(".",string.strip(i))
    
    if (s not in LSE_db.structAdds.keys()):
        _LSEbl_report_error("instance %s attempts to add fields to unknown " 
                            "structure %s" % (i,s))
        return
    inst = _LSEbl_env.db.getInst(i)

    _LSEbl_env.inst = inst
    _LSEbl_env.subobj = None # structadds have no context....
    _LSEbl_env.codeloc = LSEcg_in_def

    coll = LSE_db.structAdds[s]
    simap = coll.get(i)
    if not simap: simap = coll[i] = {}

    _LSEbl_typeLog.clear()

    LSEcg_domain_set_inst_rc(LSE_db,inst, 1)
    
    # should map in port types here as well....
    flist.sort(lambda x,y:cmp(x[1],y[1]))
    for f in flist:
        try:
            ttoks = _LSEbl_tokenizer.processInput(f[0],
                                                  "in structadd(<<<%s>>>,"
                                                  "<<<%s>>>,...)" % (s,i),
                                                  [{},_LSEbl_structDict])
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            return # just leave out of the structure

        try:
            ntoks = _LSEbl_tokenizer.processInput(f[1],
                                                  "in structadd(<<<%s>>>,"
                                                  "<<<%s>>>,...)" % (s,i),
                                                  [{},_LSEbl_structDict])
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            return # just leave out of the structure

        name = str(ntoks)
        smatch = _LSEbl_word_re.search(name) # get name; it's the first id
        simap[smatch.group()] = (str(ttoks),name)

    # instance uses these types....
    inst.usesTypes.update(_LSEbl_typeLog)
    inst.structAddUsesTypes.update(_LSEbl_typeLog)
    inst.potentialInstRefUsesTypes.update(_LSEbl_typeLog)
    _LSEbl_env.db.structAddUsesTypes.update(_LSEbl_typeLog)
    
##################### SIM_type_info.py ################
#
# If we assume that the types are sorted so that definitions of types
# come before their use, we can go ahead and resolve the type names as
# we process their definitions...

_LSEbl_enumLog = {}

#
#  Tok macro for enumerated type value definitions.... only used here.
#
def _LSEbl_enum_def(tinfo,args,enumlog,env):
    if (len(args)!=2):
        # we raise a LSEtk_TokenizeException so we get nice line #s 
        raise LSEtk_TokenizeException("LSEut_enumdef should have 1 argument",
                                tinst)
    _LSEbl_enumLog[str(args[1])] = 1
    return None # no substitution

#
# dictionary to use while parsing the types
#
_LSEbl_typeDefDict = { # identifier dictionary for reading type names
    "LSEut_ref" : [(LSEtk_Tok_macro,0,_LSEbl_type_ref,_LSEbl_typeLog)],
    "LSEut_enumdef" : [(LSEtk_Tok_macro,0,_LSEbl_enum_def,None)],
    "LSEdi_ref" : [(LSEtk_Tok_macro,0,_LSEbl_domaininst_ref,_LSEbl_typeLog)],
    }

_LSEbl_typeHash = { }  # type hashes; used to check for hash conflicts
_LSEbl_varHash = { }  # var hashes; used to check for hash conflicts

#
# requires that the parameters of arraydef/structdef have some very
# peculiar syntax.
# for arraydef: LSEut_arraydef (size , ( type ) )
# for structdef: LSEut_structdef ( ( type ) fname , ... )
#  the extra parenthesis are important...

def _LSEbl_typeparse(db,toks):
    tname = None
    if type(toks.contents) == types.ListType:
        if len(toks.contents)==2:  # arrays and structs will have two elements
            tname = toks.contents[0].contents
            if tname == "LSEut_arraydef":
                flds = toks.contents[1].contents
                tname = flds[2].contents
                tentry = db.typeBEMap.get(tname)
                if tentry: ihaveres = tentry[7][0]
                elif tname in ["LSE_dynid_t", "LSE_resolution_t"]:
                    ihaveres = 1
                else:
                    ihaveres = 0
                return (ihaveres,LSEdb_type_is_array,
                        (int(flds[0].contents),tname))
            elif tname == "LSEut_structdef":
                flds = toks.contents[1].contents
                flist = []
                allihaveres = 0
                for i in range(0,len(flds),3):
                    tname = flds[i].contents[0].contents
                    tentry = db.typeBEMap.get(tname)
                    if tentry: ihaveres = tentry[7][0]
                    elif tname in ["LSE_dynid_t", "LSE_resolution_t"]:
                        ihaveres = 1
                    else:
                        ihaveres = 0
                    flist.append((ihaveres,tname,flds[i+1].contents))
                    allihaveres = allihaveres or ihaveres
                return (allihaveres, LSEdb_type_is_struct, flist)
    return (0,LSEdb_type_is_boring,None)
    
#
#  Add a global type
#   checks for mismatched {} in definition
#
def add_type(name, definition):
    LSE_db = _LSEbl_env.db
    # environment does not need to be set because no functions using it
    # will be called...
    
    _LSEbl_typeLog.clear() # clear the log
    _LSEbl_enumLog.clear()
    
    loc = "in definition of global type '%s'" % name
    try:
        # we use a light tokenizer as we really do not want to expand
        # anything but what we have declared here and the domain types...
        # (expanding domain types here makes it possible to have changes in
        # domain types appear magically in the rebuild conditions)
        toks = _LSEbl_tokenizer_light.processInput(definition,loc,
                                                   [{},
                                                    _LSEbl_typeDefDict])
    except LSEtk_TokenizeException, a:
        _LSEbl_report_error(str(a))
        toks = LSEtk_TokenTopLevel([]) # dummy value

    # We hash the contents to get stable naming so global rebuilds are not
    # needed and debugging is still possible, but this does not completely
    # solve our problem: if two types that hash to the
    # same value reverse order, we should rebuild users of those types, which
    # requires logging of use of types in modules rather than just simply
    # substituting the stable backend type names and checking for differences.
    # However, conflicts do not propagate, if type B depends upon type A which
    # has had a reversal of order, then type B hashes to a different value
    #
    # But there is another problem: suppose type A's old value hash-conflicts
    # with it's new value.  In such a case, we still need to check that the
    # types have not changed....
    #
    # So: name change => type changed or hash-conflict order reversal, but
    #     type change does not => name change
    #         which means:  name same does not => no type change!
    #
    # What this means is that we need to do logging of types used in instances
    # and compare them all.  Sigh....  But we might as well translate to the
    # new names while we're doing the logging.
    #
    # enums are funny: we want the value names to go into the type definition
    # hash, but the final value name depends upon the hash.  So.... we
    # hash without fixing the value names and then fix up the names using
    # the hashed value
    #

    definition = str(toks) # ready to hash
    #hashval = name
    # grab only 64 bits of hash
    hashval = LSE_hashwrap.hashit(definition)[:16]
    seqno = _LSEbl_typeHash.get(hashval,0)
    _LSEbl_typeHash[hashval] = seqno + 1
    newname = "LSEut_be_%s_%d_t" % (hashval,seqno)
    newnameS = "LSEut_be_%s_%d_s" % (hashval,seqno)

    def temp(tinfo,args,extraargs,env):
        return extraargs + str(args[1])

    def temp2(tinfo,args,extraargs,env):
        return "LSE_ArrayWrap<%s,%s>" % (LSEtk_TokenTopLevel(args[2].contents),
                                         args[1])
        #return "struct %s { %s elements[%s]; }" % \
        #       (newnameS,LSEtk_TokenTopLevel(args[2].contents),args[1])
    
    def temp3(tinfo,args,extraargs,env):
        slist = [ "struct %s {\n" % newnameS ]
        for arg in args[1:-1]:
            fdef = str(LSEtk_TokenTopLevel(arg.contents[0].contents))
            fname = str(LSEtk_TokenTopLevel(arg.contents[1:]))
            if string.find(fdef,"??")>=0:
                slist.append("  %s;\n" % (string.replace(fdef,"??",fname)))
            else:
                slist.append("  %s %s;\n" % (fdef, fname))
        slist.append("}")
        return string.join(slist)

    #  At this point, we should figure out the structure, before we remove
    #  the array type and structtype definitions.....

    parseInfo = _LSEbl_typeparse(LSE_db,toks)
        
    tempdict = { "LSEut_enumdef" : [(LSEtk_Tok_macro,0,temp,newname+"__")],
                 "LSEut_arraydef" : [(LSEtk_Tok_macro,0,temp2,None)],
                 "LSEut_structdef" : [(LSEtk_Tok_macro,0,temp3,None)],
                 }

    # put nothing on the domain search path, except that we want to
    # restrict ourselves to types and constants...
    LSEcg_domain_set_rc(LSE_db,[])
    
    try:
        # again, a light tokenizer is needed, but now it is okay to have
        # domain types
        toks = _LSEbl_tokenizer_light.processInput(definition,loc,
                                                   [tempdict])
    except LSEtk_TokenizeException, a:
        _LSEbl_report_error(str(a))
        toks = LSEtk_TokenTopLevel([]) # dummy value

    _LSEbl_typeLog[(0,newname)] = name

    # next-to-last entry is a place for stashing rebuild information
    tentry = (name,toks,newname,hashval,_LSEbl_typeLog.copy(),[0],
              _LSEbl_enumLog.copy(),parseInfo)
    
    #sys.stderr.write("Adding type %s\n" % (tentry,))
    _LSEbl_env.db.globalTypes.append(tentry)
    _LSEbl_env.db.typeMap[name] = tentry
    _LSEbl_env.db.typeBEMap[newname] = tentry
    
#
#  Add a type mapping for an instance
#    iname = instance name
#    lname = local type name
#    gname = global type name
#
def add_type_mapping_to_inst(iname, lname, gname):
    inst = _LSEbl_env.db.getInst(iname,1)
    if not inst:
        _LSEbl_report_error("Unable to find instance %s while adding "
                            "type mapping" % iname)
        return
    tentry = _LSEbl_env.db.typeMap.get(gname)
    if tentry:
        inst.typeMap[lname]=tentry[2] # backend name
        inst.usesTypes.update(tentry[4])
    else:

        loc = "in type mapping '%s' for instance '%s'" % (lname,iname)
        dlist = [{}]
        
        # put nothing on the domain search path, except that we want to
        # restrict ourselves to types and constants...
        LSEcg_domain_set_rc(_LSEbl_env.db,[])
        try:
            # we use a light tokenizer as we really do not want to expand
            # anything but what we have declared here...
            toks = _LSEbl_tokenizer_light.processInput(gname,loc,dlist)
            
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            toks = "LSE_type_none"

        inst.typeMap[lname] = str(toks)

##################### SIM_var_info.py ################

# we translate type references directly; no need to tokenize; precludes
# use of LSE_port_type on these, but LSS doesn't allow that anyway; you have
# to use an LSS type (even if it is just an external type definition)

#  The globalVars entries are lists instead of tuples because we need to
#  plug in the names and uses later, so the entry must be mutable.  The
#  fields are:
#   global name, user's name, backend type name,
#    backend var name(not known yet), types used,
#    rebuild flag, map of instances referencing it
#
#  Add a global variable
#   checks for mismatched {} in definition
def add_var(globalname, localname, tname):
    
    tentry = _LSEbl_env.db.typeMap.get(tname)
    
    if not tentry: # standard type or domain type; parse to find out...

        loc = "in type of global variable '%s'" % globalname
        dlist = [{}]
        # put nothing on the domain search path, except that we want to
        # restrict ourselves to types and constants...
        LSEcg_domain_set_rc(_LSEbl_env.db,[])
        
        try:
            # we use a light tokenizer as we really do not want to expand
            # anything but what we have declared here...
            toks = _LSEbl_tokenizer_light.processInput(tname,loc,dlist)
            
        except LSEtk_TokenizeException, a:
            _LSEbl_report_error(str(a))
            toks = "LSE_type_none"

        ventry = [globalname, localname, str(toks), None, {},0, {}]
    else:
        usetype={}
        usetype.update(tentry[4])
        ventry = [globalname, localname, tentry[2], None, usetype,0,{}]
        
    _LSEbl_env.db.globalVars.append(ventry)
    _LSEbl_env.db.varMap[globalname] = ventry


##################### SIM_prefix_extras.py ################

def add_extras_to_prefix(str):
    _LSEbl_env.db.prefix_extras = str


#######################################################################
#                                                                     #
#        HERE IS THE BUILD ROUTINE                                    #
#                                                                     #
#######################################################################

def LSEbl_do_build(topdir, workdir):
    
    # things we write that have module scope
    global _LSEbl_readpass, _LSEbl_env
    db = LSEdb_Design()
    _LSEbl_env.db = db
    db.topDir = topdir
    db.workDir = workdir
    
    global _LSEbl_tokenizer, _LSEbl_tokenizer_light
    # create the tokenizer with the code generation dictionary on the
    # path so that code generation can occur for functions we do not
    # need to analyze...
    # The light tokenizer is for very restricted parsing environments
    _LSEbl_tokenizer = LSEtk_Tokenizer(LSEtk_flag_COnly,
                                       [SIM_analysis.LSEan_APIs,
                                        SIM_apidefs.LSEap_APIs],_LSEbl_env)
    _LSEbl_tokenizer_light = LSEtk_Tokenizer(LSEtk_flag_COnly,[],_LSEbl_env)
    _LSEbl_env.inst = None
    _LSEbl_env.subobj = None

    ################# Read in the prefix extras ###############
    # Read in a file that will affect SIM_prefix.m4.  Do it now
    # since it has no related side-effects
    #
    filedict = {}
    execfile("SIM_prefix_extras.py",globals(),filedict)
    
    ############# Read in the domain information ###############
    # First we must read in the domain information, because that can
    # affect user types but is not affected by them...
    #
    # However, we must only define the domain instances here; the
    # domain search paths must wait until module instances are around...
    #
    filedict = {}
    _LSEbl_readpass = 0
    db.dinstanceMap = {}
    execfile("SIM_domain_info.py",globals(),filedict)

    #
    # Determine domain requirements for classes and put classes into the
    # proper topologically sorted order.  The topological sort is needed
    # so that types come out in the right order.  Stability is required
    # to reduce the amount of rebuilding.
    #
    # Also need to create a search path...
    #
    def domain_check_requirements(dname,db,missing,dfound):
        mno = dfound.get(dname)
        if mno is not None: return mno
        dfound[dname] = 0
        mno = 0
        d = db.domains.get(dname)
        if d is None:
            mobj = _LSEbl_init_domain_class(db,dname)
            if not mobj:
                if dname not in missing: missing.append(dname)
                return -1
            else:
                d = db.domains.get(dname)

        sp = []
        for rq in d[0].classRequiresDomains:
            a = domain_check_requirements(rq,db,missing,dfound)
            if (a > mno): mno = a

        d[0].searchPath = [(dname,"")] + \
                          map(lambda x:(x,""),d[0].classRequiresDomains)
        dfound[dname] = mno
        db.domainOrder.append(d)
        return mno
    
    dfound={}
    missing = []
    t = db.domains.values()
    t.sort(lambda x,y:cmp(x[0].className,y[0].className))
    db.domainOrder = []
    for d in t:
        domain_check_requirements(d[0].className,db,missing,dfound)

    #
    # Determine domain requirements for instances and put instances in
    # a stable, topological order.
    #
    # We check first to see whether the required class exists; if not,
    # we attempt to load it.  We then check to see whether any instance
    # of the class approves of this domain's build parameters.  If none
    # does, and createIfRequired is true, we create an instance,
    # otherwise we report an error.

    def instance_check_requirements(di,db,missing,ifound,dfound,acl):
        if ifound.get(di) is not None: return
        ifound[di] = 0

        sp = [(di.className,di.instName)]

        for rq in di.implRequiresDomains + di.instRequiresDomains:

            # check the class
            a = domain_check_requirements(rq[0],db,missing,dfound)
            if (a < 0):  # did not find the domain and could not load it
                continue # just move ahead; missing already contains domain
            dc = db.domains.get(rq[0])
            if dc is None:
                continue # must be an error...

            # see if we can approve this instance
            for dj in dc[1]:
                if dj == di: continue # cannot depend upon self
                if dj.approveRequirement(rq[1]): break # it is good
            else: # did not find it
                if not dc[0].createIfRequired:
                    if rq not in missing: missing.append(rq)
                    continue # so we do not attempt to move down...
                else: # can create an instance
                    iname = "LSEnc_%d" % acl[0]
                    add_domain_instance(rq[0],rq[1],iname,None)
                    acl[0] = acl[0] + 1
                    dj = db.domainInstances[iname]

            instance_check_requirements(dj,db,missing,ifound,dfound,acl)
            if not ( (dj.className, dj.instName) in sp):
                     sp.append((dj.className, dj.instName))
            
        db.domainInstanceOrder.append(di)
        di.searchPath = sp
        
    ifound={}
    # missing we will keep from the domain run...
    t = db.domainInstances.values()
    t.sort(lambda x,y:cmp(x.instName,y.instName))
    db.domainInstanceOrder=[]
    alist = [0] # important to do this through a variable for some reason!
    for i in t:
        instance_check_requirements(i,db,missing,ifound,dfound,alist)

    for rq in missing:
        if type(rq) is types.TupleType:
            _LSEbl_report_error(("Unable to find domain instance matching "
                                 "class '%s', build parameters '%s'") %
                                (rq[0],rq[1]))
        else:
            _LSEbl_report_error("Unable to find required domain class '%'" %
                                rq)

    del db.dinstanceMap

    # and re-order so we can put them in 
    ############ Read in module instances ##################
    #
    # This can only be done before reading in the global types if
    # the file does not contain anything which is type-dependent.  At
    # present, that seems to be the case.
    #
    filedict = {}
    _LSEbl_readpass = 3
    execfile("SIM_instance_info.py",globals(),filedict)

    # sort the instances
    db.instanceOrder = db.instances.values(); 
    db.instanceOrder.sort(lambda x,y:cmp(x.name,y.name))

    # add the top-level instance
    tl = LSEdb_MInstance(db, "LSEfw_top","LSEfw_top",0,0,0,0,0,
                         "+LSEfw_adapter.clm")
    db.instanceOrder.insert(0,tl)

    # assign instance numbers
    c = 1
    for i in db.instanceOrder:
        i.instno = c
        c = c + 1

    ########### Read in domain search paths #############

    filedict = {}
    _LSEbl_readpass = 1
    execfile("SIM_domain_info.py",globals(),filedict)

    db.domainSearchPath = ([],[]) # for now...

    ############ and set up the domain API dictionaries ######

    _LSEbl_create_domain_apis(db)

    #### See whether we had errors in domains.  We cannot allow processing
    #### to go further because we will just get lots of meaningless errors
    #### or even Python exceptions if the domain instances expected by
    #### the configuration were not able to be built...

    if (db.buildErrCount):
        sys.stderr.write(
          "Encountered %d error(s) while building the design database\n" % 
          db.buildErrCount)
        raise SystemExit(1)
    
    ############ Now we need to add the domain APIs to the #####
    ### searchpath and be able to resolve m4 stuff in the tokenizer;
    ### the easiest way to do that is to create a new tokenizer, and
    ### the codegen module provides one
    
    _LSEbl_tokenizer = LSEcg_get_domain_tokenizer(db,
                                                  [SIM_analysis.LSEan_APIs,
                                                   SIM_apidefs.LSEap_APIs],
                                                  _LSEbl_env,0)

    _LSEbl_tokenizer_light = LSEcg_get_domain_tokenizer(db,
                                                        [],
                                                        _LSEbl_env,0)

    ############ Record type usage of the attribute structures ###########
    #
    # This is needed so we know whether the types defined in the attribute
    # structures have changed...
    # Basically, we need to run each attribute through the tokenizer...
    _LSEbl_mark_domain_attributes(db)
    
    ############ Read in the type information #################

    global _LSEbl_typeHash

    _LSEbl_typeHash.clear()
    
    filedict = {}
    execfile("SIM_type_info.py",globals(),filedict)
    
    _LSEbl_typeHash.clear() # remove the references...

    ############ Read in the var information ##########

    filedict = {}
    _LSEbl_varHash.clear()
    
    execfile("SIM_var_info.py",globals(),filedict)

    ############ Read in parameters ##########
    # Must come before structadd so that domain types are good

    filedict = {}
    execfile("SIM_parm_info.py",globals(),filedict)

    add_parm_to_inst('LSEfw_top','script_string',"",'LSE_literal_t',0,0,"","")

    # sort the parms
    for i in db.instanceOrder:
        i.parmOrder = i.parms.items()
        i.parmOrder.sort()
        
    ############## Read in module port info ##################
    # Must come before structadd if LSE_port_type() is to work
    
    filedict = {}
    _LSEbl_readpass = 0
    execfile("SIM_port_info.py",globals(),filedict)

    # now sort the ports
    for i in db.instanceOrder:
        i.portOrder = i.ports.values(); 
        i.portOrder.sort(lambda x,y:cmp(x.name,y.name))

    ############## Connect up ports and handle aliases #######

    # connections and aliases
    filedict = {}
    _LSEbl_readpass = 1
    execfile("SIM_port_info.py",globals(),filedict)

    # sort the portAliases
    db.portAliasOrder = db.portAliases.values()
    db.portAliasOrder.sort()

    for a in db.portAliasOrder:
        a[1].sort()

    ############ Read in the structadd information ##########

    # need to collect per-instance and global structadd stuff
    _LSEbl_env.db.structAddUsesTypes = {}
    filedict = {}
    execfile("SIM_struct_info.py",globals(),filedict)

    ############ Read in query info ##################

    filedict = {}
    execfile("SIM_query_info.py",globals(),filedict)

    # now sort the queries
    for i in db.instanceOrder:
        i.queryOrder = i.queries.values(); 
        i.queryOrder.sort(lambda x,y:cmp(x.name,y.name))

    ############# Read in codepoints #################

    filedict = {}
    execfile("SIM_codepoint_info.py",globals(),filedict)

    # add "default" codepoints to top-level
    add_codepoint_to_inst("LSEfw_top","start_of_timestep",LSE_PointUser,
                          "","","void","void",0)
    add_codepoint_to_inst("LSEfw_top","end_of_timestep",LSE_PointUser,
                          "","","void","void",0)
    add_codepoint_to_inst("LSEfw_top","init",LSE_PointUser,
                          "","","void","void",0)
    add_codepoint_to_inst("LSEfw_top","finish",LSE_PointUser,
                          "","","void","void",0)
    add_funcheader_to_inst("LSEfw_top","")
    add_body_to_inst("LSEfw_top","");
    
    # now sort the codepoints
    for i in db.instanceOrder:
        i.codepointOrder = i.codepoints.values(); 
        i.codepointOrder.sort(lambda x,y:cmp(x.name,y.name))

    ############ Analyze the ports so we know how to add default events ####
        
    SIM_analysis.LSEan_analyze_ports(db)     # emptiness/strides/independence

    ############### Read in the event information #############

    # events
    filedict = {}
    _LSEbl_readpass = 0
    execfile("SIM_event_info.py",globals(),filedict)

    # collectors
    filedict = {}
    _LSEbl_readpass = 1
    execfile("SIM_event_info.py",globals(),filedict)

    # If any module instance is short any port stats (such as adapters on
    # renamed ports!), define them so we do not have to special case
    # the per-instance code generation
    _LSEbl_readpass = 0 # important because we're doing add_event_to_inst
    
    def try_to_add(i,p,en):
        if not i.events.has_key(p.name+"."+en):
            add_event_to_inst(i.name,p.name+"."+en,
                              "porti/status/prevstatus/id/datap",
                              "int/LSE_signal_t/LSE_signal_t/LSE_dynid_t/"
                              "LSE_port_type(%s) *" %
                              p.name)

    for i in db.instanceOrder:
        for p in i.portOrder:
            try_to_add(i,p,"resolved")
            try_to_add(i,p,"localresolved")

    #  Now add statistics for tracing 

    if db.getParmVal("LSE_show_port_statuses_changes"):
        def try_to_add(i,p,en):
            add_collector_to_inst(i.name,p.name+"."+en,"","","",
                               ("""LSEfw_debug_status_standard("%s:%s.%s","""
                                   """porti,status);\n"""
                                   % (i.name,p.name,en)),"")

        for i in db.instanceOrder:
            for p in i.portOrder:
                try_to_add(i,p,"resolved")
                if not p.controlEmpty:
                    try_to_add(i,p,"localresolved")

    # now sort the events
    for i in db.instanceOrder:
        i.eventOrder = i.events.values(); 
        i.eventOrder.sort(lambda x,y:cmp(x.name,y.name))
        for ev in i.eventOrder:
            ev.filled.sort()  # use the comparison operator for collectors
            ev.isFilled = (len(ev.filled)>0)

    ##### Need some analysis so that we can generate proper API calls... ####

    SIM_analysis.LSEan_assign_cblocks(db)    # assign code blocks
    SIM_analysis.LSEan_analyze_structadds(db) # types for structadds
    
    ###### Now generate codepoint/funcheader/modulebody/collector code #####
    #
    # OLD:
    # Generation takes places in two steps: the code is first parsed into
    # tokens.  However, some APIs are not ready to generate code until the
    # whole block of code being parsed has been parsed.  To deal with this,
    # they generate a dummy token and register a callback routine which
    # is able to replace the token with something useful after the block is
    # parsed.  After this replacement, the tokens are replaced with a string
    # to save database space.
    #
    # NEW:
    # The translation code is rather complicated and runs in two phases:
    # In the first phase, we translate the code to tokens.  Numerous APIs
    # whose code cannot yet be determined because they need to see the
    # entire block of code or because they are affected by code sharing
    # logic cannot translate entirely.  So, what they do is add a token
    # to be replaced and register a routine to do the replacing once the
    # correct code is understood.  It's ugly, but necessary....  Also,
    # by choosing the dummy token consistently, we can do comparisons
    # between the code to be generated that allow us to know easily whether
    # two module instances may be combined.  After this replacement, the
    # tokens are replaced with a string to save database space.
    # Control function dataflow is analyzed in the process
    
    _LSEbl_parse_codepoints(db) # also do analysis....
    _LSEbl_parse_events(db)

    #### Remove type logging hook from the domainAPI dictionary #######

    for d in db.domainAPIs.values():
        if (d[0][2] == _LSEbl_domain_resolve):
            d[0] = (d[0][0], d[0][1], LSEcg_domain_resolve, d[0][3])
        elif (d[0][2] == _LSEbl_domain_class_resolve):
            d[0] = (d[0][0], d[0][1], LSEcg_domain_class_resolve, d[0][3])

    ######### Now drop unused domain instances and classes #############

    #sys.stderr.write("%s\n" %( db.domains,))
    #for i in db.domainInstances.items():
    #    sys.stderr.write("\t%s %d\n" % (i[1].instName,i[1].used))
        
    changed = 1
    while changed:
        changed = 0
        for (cname,clist) in db.domains.items():
            for x in clist[1]:
                if x.used:
                    for j in x.searchPath:
                        r = db.domainInstances.get(j[1])
                        if r and not r.used:
                            r.used = 1
                            changed = 1

    def myfilt(x,db=db):
        if not x.used:
            #sys.stderr.write("Drop %s\n" % x.instName)
            del db.domainInstances[x.instName]
            db.domainInstanceOrder.remove(x)
        #sys.stderr.write("%s\n" % x.searchPath)
        return x.used
                                
    def myfilt2(x,db=db):
        return x.used
                                
    for (cname,clist) in db.domains.items():
        nlist = filter(myfilt,clist[1])
        if len(nlist):
            db.domains[cname] = (clist[0],nlist)
        else:
            if clist[0].used:
                db.domains[cname] = (clist[0],[])
            else:
                db.domains[cname] = (clist[0],[])
                # del db.domains[cname]

    dol = []
    for clist in db.domainOrder:
        nlist = filter(myfilt2,clist[1])
        if len(nlist):
            dol.append( (clist[0],nlist) )
        else:
            if clist[0].used:
                dol.append( (clist[0], []) )
            else:
                dol.append( (clist[0], []) )
                # del db.domains[cname]
    db.domainOrder = dol
    
    #sys.stderr.write("%s\n" %( db.domains,))
    #sys.stderr.write("%s\n" %( db.domainInstances,))
    #sys.stderr.write("%s\n" %( db.domainOrder,))
    #sys.stderr.write("%s\n" %( db.domainInstanceOrder,))
        
    ############# Instance merging ##############
    # can do instance merging right here!
    if not db.buildErrCount:
        tarMap = {}
        shareMap = {}
    
        for i in db.instanceOrder:
            
            # deal with empty modules and commands which generate code
            # by not merging them
            if not len(i.tarball) or (i.tarball[0]=='-'):
                groups = []
            else:
                groups = tarMap.setdefault(i.tarball,[])
                
            for x in groups:
                iList = shareMap[x]
                # check compatibility; if not compatible, continue
                if SIM_analysis.LSEan_inst_not_mmergeable(iList[0][0],i,db):
                    continue
                for y in iList:
                    if SIM_analysis.LSEan_inst_not_imergeable(y[0],i,db):
                        continue
                    y.append(i)
                    break
                else:
                    iList.append([ i ])
                break
            else:
                nid = len(shareMap)
                groups.append(nid)
                shareMap[nid] = [ [ i ] ]

        # this map now says who is merged where
        db.shareMap = shareMap
        del tarMap

    #### Analyze port constants ####

    if not db.buildErrCount:
        SIM_analysis.LSEan_analyze_port_deps(db) # dependency analysis

    ##### Scheduling analysis work...
    # uses calls logged when parsing codepoints; note that port width
    # should have been translated when _LSEbl_parse_codepoints was called
    # so as to make it a constant.  This implies LSE_port_width differences
    # must prevent instance merging.

    if not db.buildErrCount:
        SIM_schedule.LSEsc_count_signals(db)
        SIM_schedule.LSEsc_find_dependencies(db)

    # set the isCalled flags
    for i in db.instanceOrder:
        for q in i.queryOrder:
            q.isCalled = not not q.getCallers()
            
    #### See whether we had errors ####

    if (db.buildErrCount):
        sys.stderr.write(
          "Encountered %d error(s) while building the design database\n" % 
          db.buildErrCount)
        raise SystemExit(1)

    _LSEbl_tokenizer = None  # release the tokenizer

    return db

#######################
# LSEbl_finish_sharing
#
# relies upon SIM_database setting shouldGenCode to 1, shareID to -1,
# and withCode to 1
# as instances are created
########################
def LSEbl_finish_sharing(db, oldwanted, oldnotwanted, cleanbuild):

    ni = len(db.instances)
    mfract = (100.0 * (ni - len(db.shareMap))) / ni
    want = (oldwanted or
            (ni >= db.getParmVal('LSE_share_module_code_threshold') and
             mfract >= db.getParmVal('LSE_share_module_code_percent_threshold')
             ))
    db.wantCodeSharing = want

    if want:
        # create the multi-inst list
        miList = []
        hcount = {}
        milCount = 0
        l = db.shareMap.items()
        l.sort()
        for sm in l:
            # list of instances in shareMap
            il = reduce(lambda x,y:x+y,sm[1],[])
            if len(il)>1:

                if 0:
                    hasher = LSE_hashwrap.gethasher()
                    for ji in range(len(il)):
                        j = il[ji]
                        j.shareID = (milCount,ji)
                        j.withCode = 0
                        hasher.update(j.name)
                    hashval = "LSE_shared_" + hasher.hexdigest()[:16]
                    if hcount.has_key(hashval):
                        hcount[hashval] += 1
                        hashval += ("_%d" % hcount[hashval])
                    else:
                        hcount[hashval] = 0
                else:
                    for ji in range(len(il)):
                        j = il[ji]
                        j.shareID = (milCount,ji)
                        j.withCode = 0
                    # an easier-to-understand name
                    hashval = "LSE_share." + il[0].name
                
                newi = LSEdb_MInstance(db, hashval, il[0].module,
                                       0,0,0,0,0,
                                       il[0].tarball,1)
                newi.subInstanceList = il
                newi.shareID = (milCount, -1)
                newi.withCode = 1
                newi.must_rebuild = cleanbuild
                newi.subGroups = sm[1]
                
                # do I need to put together the typemap for rebuild?
                
                # build the instance properly enough for build to work
                
                def doevent(x):
                    y = LSEdb_Event(newi, x.name)
                    y.usesTypes = {}
                    y.argString = x.argString
                    y.numArgs = x.numArgs
                    y.filled = [] # do not want copies of text
                    newi.events[y.name] = y
                    y.isFilled = reduce(lambda a,b:a or b.isFilled,
                                        map(lambda x:x.events[y.name],il),0)
                    return y
                newi.eventOrder = map(doevent,il[0].eventOrder)

                def doport(x):
                    y = LSEdb_Port(newi, x.name, x.width, x.dir, x.type,
                                   x.independent, x.handler)
                    y.controlEmpty = x.controlEmpty # note: implies same cp
                    y.connectionStride = 0
                    y.type = x.type
                    y.signals = x.signals  # note: implies same cp
                    y.schedIndependent = x.schedIndependent

                    mports = map(lambda x:x.ports[y.name],il)
                    y.otherAnyStatic = reduce(lambda a,b:a or b.otherAnyStatic,
                                              mports,0)
                    y.otherAnyDynamic = reduce(lambda a,b:a or \
                                               b.otherAnyDynamic,
                                               mports,0)
                    y.otherSchedCode = reduce(lambda a,b:a * \
                                              (a == b.otherSchedCode+1),
                                              mports,
                                              mports[0].otherSchedCode+1) - 1
                    y.fireSchedCode = reduce(lambda a,b:a * \
                                              (a == b.fireSchedCode+1),
                                              mports,
                                              mports[0].fireSchedCode+1) - 1
                    y.fireAnyStatic = reduce(lambda a,b:a or b.fireAnyStatic,
                                              mports,0)
                    y.selfSchedCode = reduce(lambda a,b:a * \
                                              (a == b.selfSchedCode+1),
                                              mports,
                                              mports[0].selfSchedCode+1) - 1
                    y.selfAnyDynamic = reduce(lambda a,b:a or b.selfAnyDynamic,
                                              mports,0)
                    y.otherCBlockTypeCode = reduce(lambda a,b:a * \
                                                   (a ==
                                                    b.otherCBlockTypeCode+1),
                                                   mports,
                                                   mports[0].
                                                   otherCBlockTypeCode+1
                                                   ) - 1

                    y.localResolveCode = reduce(lambda a,b:a * \
                                                (a == b.localResolveCode+1),
                                                mports,
                                              mports[0].localResolveCode+1) - 1
                    y.globalResolveCode = reduce(lambda a,b:a * \
                                                (a == b.globalResolveCode+1),
                                                mports,
                                            mports[0].globalResolveCode+1) - 1

                    newi.ports[y.name] = y
                    return y
                newi.portOrder = map(doport,il[0].portOrder)

                def dopoint(x):
                    y = LSEdb_CodePoint(newi, x.name, x.type)
                    y.__dict__.update(x.__dict__)
                    y.inst = newi
                    y.isEmpty = reduce(lambda a,b:a and b.isEmpty,
                                       map(lambda x:x.codepoints[y.name],il),1)
                    newi.codepoints[y.name] = y
                    return y
                newi.codepointOrder = map(dopoint, il[0].codepointOrder)
                
                #newi.parms = il[0].parms
                #newi.typeMap = il[0].typeMap

                def doquery(x):
                    y = x.copyToNewInst(newi)
                    newi.queries[y.name] = y
                    # need to set q.callers to the largest length...
                    m = 0
                    ic = 0
                    for z in il:
                        b = z.queries[y.name]
                        ic = ic or b.isCalled
                        callers = b.getCallers()
                        if callers: m = max(m,len(callers))
                    y.isCalled = ic
                    newi.calledby[("LSE_query_call",y.name)] = [0] * m
                    return y
                newi.queryOrder = map(doquery,il[0].queryOrder)

                #newi.funcheader = il[0].funcheader
                #newi.moduleBody = il[0].moduleBody

                for j in il:
                    j.multiInst = newi
                    # do not need this because rebuild is only user and
                    # it will catch changes as constituent changes
                    #newi.usesTypes.update(j.usesTypes) # for rebuild
                miList.append(newi)
                milCount += 1

                # figure out implementations for API calls now that we
                # have done the grouping.  If we were to do nothing with
                # them, then we would get the default (original)
                # implementation.  If we set the implementation to -1, we
                # get the least specialized implementation.  Parameter does
                # not need to force rebuild because changes in the impl will
                # do so at the replace stage (which is before rebuild checks)
                spec_APIs = db.getParmVal("LSE_DAP_specialize_apis")
                for g in newi.subGroups:
                    toFinish = g[0].toFinish.items()
                    if spec_APIs:
                        for f1 in toFinish:
                            key = f1[0]
                            fnc = SIM_apidefs.LSEap_fkey_decide_funcs[key[0]]
                            flist = f1[1]
                            for f2 in range(len(flist)):
                                fnc(db, key, map(lambda x:x.toFinish[key][f2],
                                                 g))
                    else: # set impl to "least specialized"
                        for f1 in ToFinish:
                            flist = f1[1]
                            for f2 in flist:
                                f2[-1] = -1

                # APIs in the module body must be handled differently; they
                # need to be calculated across all groups
                if spec_APIs:
                    toFinish = il[0].toFinish.items()
                    for f1 in toFinish:
                        key = f1[0]
                        if key[1]==LSEcg_in_modulebody:
                            fnc = SIM_apidefs.LSEap_fkey_decide_funcs[key[0]]
                            flist = f1[1]
                            for f2 in range(len(flist)):
                                fnc(db, key, map(lambda x:x.toFinish[key][f2],
                                                 il))
                
        del db.shareMap
        db.multiInstList = miList
    else:
        db.multiInstList = []
        
    # Now we need to fix up all the code before we put it in the database
    # At one time I thought we could register fixup-like functions and
    # modify the tokens directly.  Unfortunately, that does not work; some
    # API calls stringify their arguments and then turn things back to tokens
    # after re-parsing, making the token pointers not work.  So, we're going
    # to deal with everything as text, but use a magic re function to make
    # this "reasonably" efficient.
    for i in db.instanceOrder:

        for e in i.eventOrder:
            for c in e.filled:
                #c.saveRecord = c.record
                c.header = _LSEbl_reparse_for_sharing(db, c.header,
                                                    LSEcg_in_datacoll, i,
                                                    (c, 0))
                c.decl = _LSEbl_reparse_for_sharing(db, c.decl,
                                                    LSEcg_in_datacoll, i,
                                                    (c, 0))
                c.init = _LSEbl_reparse_for_sharing(db, c.init,
                                                    LSEcg_in_datacoll, i,
                                                    (c, 1))
                c.record = _LSEbl_reparse_for_sharing(db, c.record,
                                                      LSEcg_in_datacoll, i,
                                                      (c, 2))
                c.report = _LSEbl_reparse_for_sharing(db, c.report,
                                                      LSEcg_in_datacoll, i,
                                                      (c, 3))

        #i.saveModuleBody = i.moduleBody

        i.extension = _LSEbl_reparse_for_sharing(db, i.extension,
                                                 LSEcg_in_extension, i,
                                                 (i, None))
        i.moduleBody = _LSEbl_reparse_for_sharing(db, i.moduleBody,
                                                 LSEcg_in_modulebody, i,
                                                 (i, None))
        i.funcheader = _LSEbl_reparse_for_sharing(db, i.funcheader,
                                                  LSEcg_in_funcheader, i,
                                                  (i, None))
        
        for p in i.codepointOrder:
            if p.type == LSE_PointControl: codeloc = LSEcg_in_controlpoint
            else: codeloc = LSEcg_in_userpoint

            #p.saveCurrent = p.current
            #p.saveDefault = p.default
            
            p.current = _LSEbl_reparse_for_sharing(db, p.current,
                                                   codeloc, i,
                                                   (p,0))
            p.default = _LSEbl_reparse_for_sharing(db, p.default,
                                                   codeloc, i,
                                                   (p,1))

        nml = i.toFinish.items()
        nml.sort()
        i.toFinishOrder = nml
        del i.toFinish

    return

_LSEbl_shareapi_pat = re.compile("(LSEfi_er|LSEfi_mc|LSEfi_uv|LSEfi_pq"
                                 "|LSEfi_sfd|LSEfi_qt|LSEfi_paq|LSEfi_pm)")
_LSEbl_nc_idno_pat = re.compile("\{(\d+)[^}]*}(.*)", re.DOTALL)
_LSEbl_idno_pat = re.compile("\(+\{(\d+)[^}]*}(.*)", re.DOTALL)

# redo the parsing for 
def _LSEbl_reparse_for_sharing(db, s, codeloc, currinst, subinst):
    if not s or not currinst.toFinish: return s # quick exit

    slist = _LSEbl_shareapi_pat.split(s)
        
    if len(slist)>1:
        # keys for APIs
        keys = {
            LSEcg_fkey_uv_ref : LSEcg_fkey(codeloc, subinst,
                                           LSEcg_fkey_uv_ref),
            LSEcg_fkey_event_record : LSEcg_fkey(codeloc, subinst,
                                                 LSEcg_fkey_event_record),
            LSEcg_fkey_method_call : LSEcg_fkey(codeloc, subinst,
                                                LSEcg_fkey_method_call),
            LSEcg_fkey_port_query : LSEcg_fkey(codeloc, subinst,
                                               LSEcg_fkey_port_query),
            LSEcg_fkey_port_alias_query : LSEcg_fkey(codeloc, subinst,
                                                 LSEcg_fkey_port_alias_query),
            LSEcg_fkey_struct_field : LSEcg_fkey(codeloc, subinst,
                                                 LSEcg_fkey_struct_field),
            LSEcg_fkey_qtramp_call : LSEcg_fkey(codeloc, subinst,
                                                LSEcg_fkey_qtramp_call),
            LSEcg_fkey_parm : LSEcg_fkey(codeloc, subinst,
                                         LSEcg_fkey_parm),
            }

    # note that we can move from left to right because nested calls
    # are not an issue here.  We keep track of the last word for efficiency
    lw = ""
    words = []
    for w in slist:
        if lw=='LSEfi_uv':
            mg =  _LSEbl_nc_idno_pat.match(w)
            idno = int(mg.group(1))
            key = keys[LSEcg_fkey_uv_ref]
            #print currinst.name
            #print currinst.toFinish
            for f in filter(lambda x:x[0]==idno,currinst.toFinish[key]):
                s2 = SIM_apidefs._LSEap_REPLACE_uv_ref(db,key,f)
                words.append(s2)
                break
            lw = mg.group(2)
            if lw is None: lw = ""
        elif lw=='LSEfi_er':
            mg =  _LSEbl_idno_pat.match(w)
            idno = int(mg.group(1))
            key = keys[LSEcg_fkey_event_record]
            #print currinst.name
            #print currinst.toFinish
            for f in filter(lambda x:x[0]==idno,currinst.toFinish[key]):
                s2 = SIM_apidefs._LSEap_REPLACE_event_record(db,key,f)
                words.append(s2)
                break
            lw = mg.group(2)
            if lw is None: lw = ""
        elif lw=='LSEfi_mc':
            mg =  _LSEbl_idno_pat.match(w)
            idno = int(mg.group(1))
            key = keys[LSEcg_fkey_method_call]
            #print currinst.name
            #print currinst.toFinish
            for f in filter(lambda x:x[0]==idno,currinst.toFinish[key]):
                s2 = SIM_apidefs._LSEap_REPLACE_method_call(db,key,f)
                words.append(s2)
                break
            lw = mg.group(2)
            if lw is None: lw = ""
        elif lw=='LSEfi_qt':
            mg =  _LSEbl_idno_pat.match(w)
            idno = int(mg.group(1))
            key = keys[LSEcg_fkey_qtramp_call]
            #print currinst.name
            #print currinst.toFinish
            for f in filter(lambda x:x[0]==idno,currinst.toFinish[key]):
                s2 = SIM_apidefs._LSEap_REPLACE_qtramp_call(db,key,f)
                words.append(s2)
                break
            lw = mg.group(2)
            if lw is None: lw = ""
        elif lw=='LSEfi_pq':
            mg =  _LSEbl_idno_pat.match(w)
            idno = int(mg.group(1))
            key = keys[LSEcg_fkey_port_query]
            #print currinst.name
            #print currinst.toFinish
            for f in filter(lambda x:x[0]==idno,currinst.toFinish[key]):
                s2 = SIM_apidefs._LSEap_REPLACE_port_query(db,key,f)
                words.append(s2)
                break
            lw = mg.group(2)
            if lw is None: lw = ""
        elif lw=='LSEfi_paq':
            mg =  _LSEbl_idno_pat.match(w)
            idno = int(mg.group(1))
            key = keys[LSEcg_fkey_port_alias_query]
            #print currinst.name
            #print currinst.toFinish
            for f in filter(lambda x:x[0]==idno,currinst.toFinish[key]):
                s2 = SIM_apidefs._LSEap_REPLACE_port_alias_query(db,key,f)
                words.append(s2)
                break
            lw = mg.group(2)
            if lw is None: lw = ""
        elif lw=='LSEfi_sfd':
            mg =  _LSEbl_idno_pat.match(w)
            idno = int(mg.group(1))
            key = keys[LSEcg_fkey_struct_field]
            #print currinst.name
            #print currinst.toFinish
            for f in filter(lambda x:x[0]==idno,currinst.toFinish[key]):
                s2 = SIM_apidefs._LSEap_REPLACE_struct_field(db,key,f)
                words.append(s2)
                break
            lw = mg.group(2)
            if lw is None: lw = ""
        elif lw=='LSEfi_pm':
            mg =  _LSEbl_idno_pat.match(w)
            idno = int(mg.group(1))
            key = keys[LSEcg_fkey_parm]
            #print currinst.name
            #print currinst.toFinish
            for f in filter(lambda x:x[0]==idno,currinst.toFinish[key]):
                s2 = SIM_apidefs._LSEap_REPLACE_parm(db,key,f)
                words.append(s2)
                break
            lw = mg.group(2)
            if lw is None: lw = ""
        else:
            words.append(lw)
            lw = w

    words.append(lw)

    return string.join(words,"")
