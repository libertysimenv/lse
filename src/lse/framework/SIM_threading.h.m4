/* 
 * Copyright (c) 2005-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Template for threading functions
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file is processed to become SIM_domain_types.h.  It contains all the
 * constants and types from domains in their fully-uniquified form
 *
 * TODO: create embedded type name things... (like function pointers)
 *
 */
m4_include(SIM_quotes.m4)
LSEm4_push_diversion(-1)
m4_include(SIM_database.m4)
LSEm4_pop_diversion()
#ifndef _SIM_THREADING_H_
#define _SIM_THREADING_H_

#if (LSEfw_PARM(LSE_mp_multithreaded))

#define LSEfw_tprivate __thread
#define LSEfw_parallelize_dynids 1

#if (!LSEfw_PARM(LSE_mp_must_use_pthreads) && defined(__GNUC__) && \
      ( (defined(__linux__) && \
	 (defined(__i386__) || defined(__x86_64__) || \
	  defined(__ia64__) || defined(__sparc__))) || \
	(defined(__svr4__) && defined(__sun__) && defined(__sparc__))))
#define LSEfw_special_sync 1
#else
#define LSEfw_special_sync 0
#endif

#if (LSEfw_PARM(LSE_DAP_inline_barriers))
#define LSEfw_do_barrier inline
#else
#if (defined(LSEfw_IN_CONTROL_C))
#define LSEfw_do_barrier
#endif /* IN_CONTROL_C */
#endif /* !inline barriers */

  /* we have real threads to deal with */
extern "C" {
#include <pthread.h>
}
#if defined(__sparc__) && defined(__sun__) && defined(__svr4__)
#include <atomic.h>
#endif

/******** thread manipulation **********/

typedef pthread_t LSEfw_threadID_t;

#define LSEfw_thread_create(tp,f,p) pthread_create(tp,NULL,f,p)
#define LSEfw_thread_join(t,rp) pthread_join(t,rp)

#if (!LSEfw_special_sync)

/******** mutexes ******************/

class LSEfw_mutex_c {
 public:
  pthread_mutex_t mtx; /* really should declare a friend function */

  inline void init(void) { pthread_mutex_init(&mtx,NULL); }
  inline void lock(void) { pthread_mutex_lock(&mtx); }
  inline void unlock(void) { pthread_mutex_unlock(&mtx); }
  inline int trylock(void) { return pthread_mutex_trylock(&mtx); }
  inline int destroy(void) { pthread_mutex_destroy(&mtx); }
};

typedef class LSEfw_mutex_c LSEfw_mutex_t;

/* unfortunately, barriers were a late addition to the pthreads library,
   * and so I cannot trust them to be present... 
   */
class LSEfw_barrier_c {
  LSEfw_mutex_t lock;
  pthread_cond_t cond;
  volatile int count;
  int num;
 public:
  inline void init(int n) { 
    count = 0;
    pthread_cond_init(&cond, NULL);
    num = n;
    lock.init();
  }
  inline void reset(void) { } 
  void wait(void);
  void nowait(void);
  inline void destroy(void) { 
    pthread_cond_destroy(&cond);
    lock.destroy();
  }
};

typedef class LSEfw_barrier_c LSEfw_barrier_t;
typedef class LSEfw_barrier_c LSEfw_oneshot_barrier_t;

#ifdef LSEfw_do_barrier

/*** WARNING: These barriers are really half barriers; they do not
     ensure that everyone has left the barrier before letting anyone
     into the barrier again.  Thus you can't use the same barrier twice
     in a row.
***/

LSEfw_do_barrier void 
LSEfw_barrier_c::wait(void) {
  int mc;
  lock.lock();
  mc = ++count;
  if (mc < num) {
    do {
      pthread_cond_wait(&cond, &lock.mtx);
    } while (count); // handle spurious wakeups
  } else {
    count = 0;
    pthread_cond_broadcast(&cond);
  }
  lock.unlock();
}

LSEfw_do_barrier void 
LSEfw_barrier_c::nowait(void) {
  int mc;
  lock.lock();
  mc = ++count;
  if (mc < num) {
  } else {
    count = 0;
    pthread_cond_broadcast(&cond);
  }
  lock.unlock();
}
#endif

typedef struct LSEfw_syncsig_s {
  LSEfw_mutex_t lock;
  pthread_cond_t cond;
  int ctrue;
} LSEfw_syncsig_t;

inline void LSEfw_syncsig_init(LSEfw_syncsig_t *sp) {
  pthread_cond_init(&sp->cond, NULL);
  sp->lock.init();
  sp->ctrue = FALSE;
}

inline void LSEfw_syncsig_reset(LSEfw_syncsig_t *sp) {
  pthread_cond_init(&sp->cond, NULL);
  sp->lock.init();
  sp->ctrue = FALSE;
}

inline void LSEfw_syncsig_wait(LSEfw_syncsig_t *sp) {
  if (sp->ctrue) return; // no need to lock if true long before
  sp->lock.lock();
  while (!sp->ctrue) pthread_cond_wait(&sp->cond, &sp->lock.mtx);
  sp->lock.unlock();
}

inline void LSEfw_syncsig_signal(LSEfw_syncsig_t *sp) {
  sp->lock.lock();
  sp->ctrue = TRUE;
  pthread_cond_broadcast(&sp->cond);
  sp->lock.unlock();
}

inline void LSEfw_atomic_inc(volatile int *cp, LSEfw_mutex_t *mp) {
#if defined(__sparc__) && defined(__sun__) && defined(__svr4__)
  atomic_inc_32((volatile uint32_t *)cp);
#else
  mp->lock();
  (*cp)++;
  mp->unlock();  
#endif
}

inline void LSEfw_atomic_dec(volatile int *cp, LSEfw_mutex_t *mp) {
#if defined(__sparc__) && defined(__sun__) && defined(__svr4__)
  atomic_dec_32((volatile uint32_t *)cp);
#else
  mp->lock();
  (*cp)--;
  mp->unlock();  
#endif
}

inline uint64_t
LSEfw_atomic_inc_u64(volatile uint64_t *cp, LSEfw_mutex_t *mp) {
  uint64_t t;
#if defined(__sparc__) && defined(__sun__) && defined(__svr4__)
  t=atomic_inc_64_nv(cp);
  --t;
#else
  mp->lock();
  t = (*cp)++;
  mp->unlock();  
#endif
  return t;
}

inline uint64_t
LSEfw_atomic_dec_u64(volatile uint64_t *cp, LSEfw_mutex_t *mp) {
  uint64_t t;
#if defined(__sparc__) && defined(__sun__) && defined(__svr4__)
  t= atomic_dec_64_nv(cp);
  ++t;
#else
  mp->lock();
  t = (*cp)--;
  mp->unlock();  
#endif
  return t;
}

#else /* above was using pthreads, below will not */

inline void delayit(void) {
  int cnt;
  for (cnt=0;cnt < LSEfw_PARM(LSE_mp_slow_spin);cnt++) {
#if (defined(__i386__) || defined(__x86_64__))
      asm __volatile__ ("\n" "or %eax, %eax");
#elif (defined(__sparc__))
      asm __volatile__ ("\n" "nop");	
#endif
  }
}

class LSEfw_mutex_c {
  volatile int mtx;

 public:
  inline void init(void) { mtx = 0; }

  inline void lock(void) { 
    register int nv, cnt=0;

    do {
      /* if contending, do not go even bother with atomic op.  */
      while (mtx) {
        if (LSEfw_PARM(LSE_mp_use_yield)) {
          if (++cnt > 30) { sched_yield(); cnt = 0; }
        } else delayit();
      }
      nv = 1;
#if (defined(__i386__) || defined(__x86_64__))
      asm __volatile__ ("\n"
		    "xchgl %0, %1"
		     : "+r" (nv), "+m" (mtx));
#elif (defined(__ia64__))
      asm __volatile__ ("\n"
		    "mov ar.ccv = r0\n\n"
		    ";;\n"
		    "cmpxchg4.acq %0 = [%1],%0,ar.ccv\n"
		    : "+r" (nv) : "r" (&mtx) : "memory"); 
#elif (defined(__sparc__))
      asm __volatile__ ("\n" "casa [%1] #ASI_PRIMARY, %%g0, %0\n"
			: "+r" (nv) : "r" (&mtx) : "memory");
#endif
    } while (nv);
  }

  inline void unlock(void) { mtx = 0; }
  inline int trylock(void) { return 0; }
  inline void destroy(void) { }

};

typedef class LSEfw_mutex_c LSEfw_mutex_t;

/*********** atomic ops ******************/

inline void LSEfw_atomic_inc(volatile int *cp, LSEfw_mutex_t *mp) {
#if (defined(__ia64__))
  int dummy;
  asm __volatile__ ("\n"
		"fetchadd4.acq %0 = [%1], 1\n"
		";;\n"
		: "=r"(dummy): "r"(cp) : "memory");
#elif (defined(__sparc__))
  register int dummy, ndummy;
  do {
    dummy = *cp;
    ndummy = dummy + 1;
    asm __volatile__("casa [%1] #ASI_PRIMARY, %2, %0"
                     : "+r"(ndummy) : "r"(cp), "r"(dummy) : "memory");
  } while (ndummy != dummy);
  
#else
  asm __volatile__("lock; incl %0" : "+m" (*cp));
#endif
}

inline void LSEfw_atomic_dec(volatile int *cp, LSEfw_mutex_t *mp) {
#if (defined(__ia64__))
  int dummy;
  asm __volatile__ ("\n"
		"fetchadd4.acq %0 = [%1], -1\n"
		";;\n"
		: "=r"(dummy): "r"(cp) : "memory");
#elif (defined(__sparc__))
  register int dummy, ndummy;
  do {
    dummy = *cp;
    ndummy = dummy - 1;
    asm __volatile__("casa [%1] #ASI_PRIMARY, %2, %0"
                     : "+r"(ndummy) : "r"(cp), "r"(dummy) : "memory");
  } while (ndummy != dummy);

#else
  asm __volatile__("lock; decl %0" : "+m" (*cp));
#endif
}

inline uint64_t 
LSEfw_atomic_inc_u64(volatile uint64_t *cp, LSEfw_mutex_t *mp) {
  uint64_t t;
#if (defined(__ia64__))
  asm __volatile__ ("\n"
		"fetchadd8.acq %0 = [%1], 1\n"
		";;\n"
		: "=r"(t): "r"(cp) : "memory");
#elif (defined(__sparc__))
  // This scary code is because gcc somehow believes that the registers
  // are 32 bits instead of 64 bits in most configurations.  So I need
  // to construct my 64 bit value myself.
  register uint32_t dummy, ndummy;
  asm __volatile__ ("1: ldx [%2], %1;\n"
                "  add %1, 1, %0; \n"
                "  casxa [%2] #ASI_PRIMARY, %1, %0\n"
                "  cmp %1, %0\n"
                "  bne,pn %%xcc, 1b\n"
                "  nop\n"
                "  srlx %1, 32, %0\n"
: "=r"(dummy), "=r"(ndummy) :  "r"(cp) : "memory","cc");
  t = (((uint64_t)dummy)<<32) | ndummy;
#elif (defined(__x86_64__))
  // asm __volatile__("lock; incq %0" : "=m" (*cp) : "m" (*cp));
  t = 1;
  asm __volatile__("lock; xaddq %1,%0" : "+m" (*cp), "+r" (t));
#else
  mp->lock();
  t = (*cp)++;
  mp->unlock();
#endif
  return t;
}

inline uint64_t 
LSEfw_atomic_dec_u64(volatile uint64_t *cp, LSEfw_mutex_t *mp) {
  uint64_t t;
#if (defined(__ia64__))
  asm __volatile__ ("\n"
		"fetchadd8.acq %0 = [%1], -1\n"
		";;\n"
		: "=r"(t): "r"(cp) : "memory");
#elif (defined(__sparc__))
  register uint32_t dummy, ndummy;
  asm __volatile__ ("1: ldx [%2], %1;\n"
                "  add %1, -1, %0; \n"
                "  casxa [%2] #ASI_PRIMARY, %1, %0\n"
                "  cmp %1, %0\n"
                "  bne,pn %%xcc, 1b\n"
                "  nop\n"
                "  srlx %1, 32, %0\n"
: "=r"(dummy), "=r"(ndummy) :  "r"(cp) : "memory","cc");
  t = (((uint64_t)dummy)<<32) | ndummy;
#elif (defined(__x86_64__))
  t = (uint64_t)-1;
  asm __volatile__("lock; xaddq %1,%0" : "+m" (*cp), "+r" (t));
#else
  mp->lock();
  t = (*cp)--;
  mp->unlock();
#endif
  return t;
}

class LSEfw_barrier_c {
  LSEfw_mutex_t lock;
  volatile int count;    /* how many are waiting */
  volatile int bino;  /* which dynamic instance of the barrier */
  int num;
 public:
  inline void init(int n) { 
    count = 0;
    bino = 0;
    num = n;
    lock.init();
  }
  inline void reset(void) { }
  void wait(void);
  void nowait(void);
  inline void destroy(void) { 
    lock.destroy();
  }
};

typedef class LSEfw_barrier_c LSEfw_barrier_t;

#ifdef LSEfw_do_barrier
LSEfw_do_barrier void 
LSEfw_barrier_c::wait(void) {
  int cbno, cc;
  register int wcount;

  lock.lock();
  cbno = bino;
  cc = count++;
  /* the trick here is that by making the last one to the barrier and all
   * others take a different code path, we know who will reset the barrier.
   * the instance number change is then used like a signal to cause the
   * others to exit their spin loop.  It also prevents too-fast threads
   * from blasting through the barrier again....
   */
  if (cc == num-1) {
    count = 0; /* set for next time around */
    ++bino;
    lock.unlock();
  } else { 
    lock.unlock();
    /* wait for barrier to be finished */
    if (LSEfw_PARM(LSE_mp_use_yield))  {
      wcount = 0;
      while (cbno == bino) if (++wcount>5) { sched_yield(); wcount = 0; }
    } else while (cbno == bino) delayit();
  }
}

LSEfw_do_barrier void 
LSEfw_barrier_c::nowait(void) {
  int cbno, cc;
  register int wcount;

  lock.lock();
  cbno = bino;
  cc = count++;
  /* the trick here is that by making the last one to the barrier and all
   * others take a different code path, we know who will reset the barrier.
   * the instance number change is then used like a signal to cause the
   * others to exit their spin loop.  It also prevents too-fast threads
   * from blasting through the barrier again....
   */
  if (cc == num-1) {
    count = 0; /* set for next time around */
    ++bino;
    lock.unlock();
  } else { 
    lock.unlock();
  }
}
#endif /* LSEfw_do_barrier */

class LSEfw_oneshot_barrier_c {
/* key here is that 64-bit architectures can easily do atomic 64-bit ops 
 * and so we can use the hugeness of 64-bit numbers to our advantage to
 * never have to reset the barrier...
 */
#if (defined(__x86_64__) || defined(__ia64__) || defined(__sparc__))
  volatile uint64_t count;
#else
  volatile int count;    /* how many are waiting */
#endif
  int num;
  LSEfw_mutex_t lock;    /* just in case */
 public:
  inline void init(int n) { 
    count = n;
    num = n;
  }
#if (defined(__x86_64__) || defined(__ia64__) || defined(__sparc__))
  inline void reset(void) {}
#else
  inline void reset(void) { count = num; }
#endif
  void wait(void);
  void nowait(void);
  inline void destroy(void) { }
};

typedef class LSEfw_oneshot_barrier_c LSEfw_oneshot_barrier_t;

#ifdef LSEfw_do_barrier
LSEfw_do_barrier void 
LSEfw_oneshot_barrier_c::wait(void) {
  register int wcount;
#if (defined(__ia64__))
  int dummy;
  asm __volatile__ ("\n"
		"fetchadd8.rel %0 = [%1], 1\n"
		";;\n"
		: "=r"(dummy): "r"(&count) : "memory");

#elif (defined(__x86_64__))
  //asm __volatile__("lock; incq %0" : "=m" (count) : "m" (count));
  uint64_t t = 1;
  asm __volatile__("lock; xaddq %1,%0" : "+m" (count), "+r" (t));
#elif (defined(__sparc__))
  LSEfw_atomic_inc_u64(&count,&lock);
#else // x86
  asm __volatile__("lock; decl %0" : "=m" (count) : "m" (count));

#endif
#if (defined(__x86_64__) || defined(__ia64__) || defined(__sparc__))
  if (count % LSEfw_num_threads) { 
    /* count refetch is acquire in gcc ia64 */
    if (LSEfw_PARM(LSE_mp_use_yield))  {
      wcount = 0;
      while (count % LSEfw_num_threads)
	if (++wcount>5) { sched_yield(); wcount = 0; }
    } else while (count % LSEfw_num_threads) delayit();
  }
#else
  if (count) {
    /* count refetch is acquire in gcc ia64 */
    if (LSEfw_PARM(LSE_mp_use_yield))  {
      wcount = 0;
      while (count)
	if (++wcount>5) { sched_yield(); wcount = 0; }
    } else while (count) delayit();
  }
#endif
#if (defined(__x86_64__))
  asm __volatile__("lfence");
#elif (defined(__sparc__))
  asm __volatile__("membar #LoadStore | #LoadLoad");
#elif (defined(__i386__))
  asm __volatile__("lock; addl $0,0(%esp)"); /* load fence */
#endif
}

LSEfw_do_barrier void 
LSEfw_oneshot_barrier_c::nowait(void) {
#if (defined(__ia64__))
  int dummy;
  asm __volatile__ ("\n"
		"fetchadd8.rel %0 = [%1], 1\n"
		";;\n"
		: "=r"(dummy): "r"(&count) : "memory");
#elif (defined(__x86_64__))
  //asm __volatile__("lock; incq %0" : "=m" (count) : "m" (count));
  uint64_t t = 1;
  asm __volatile__("lock; xaddq %1,%0" : "+m" (count), "+r" (t));
#elif (defined(__sparc__))
  LSEfw_atomic_inc_u64(&count,&lock);
#else
  asm __volatile__("lock; decl %0" : "=m" (count) : "m" (count));
#endif
}
#endif /* LSEfw_do_barrier */

typedef struct LSEfw_syncsig_s {
  volatile unsigned int val;
} LSEfw_syncsig_t;

inline void LSEfw_syncsig_init(LSEfw_syncsig_t *sp) {
  sp->val = 0;
}

inline void LSEfw_syncsig_reset(LSEfw_syncsig_t *sp) {
#ifdef OLD
  sp->val = 0;
#endif
}

inline void LSEfw_syncsig_wait(LSEfw_syncsig_t *sp) {
  register int wcount=0;
#ifdef OLD
  if (LSEfw_PARM(LSE_mp_use_yield)) {
     while (!sp->val) if (++wcount>5) { sched_yield(); wcount=0; }
  } else while (!sp->val) delayit();
#if (defined(__x86_64__))
  asm __volatile__("lfence");
#elif (defined(__ia64__))
  /* no need to do this acquire because gcc does all the sp->val
   * accesses as acquires
  int dummy;
  asm __volatile__ ("\n"
		"ld4.acq %0 = [%1]\n"
		";;\n"
		: "=r"(dummy) : "r" (&sp->val) : "memory");
  */
#endif
#else
  if (LSEfw_PARM(LSE_mp_use_yield)) {
     while (sp->val != (UINT32_MAX & LSEfw_info.timesteps)) 
	if (++wcount>5) { sched_yield(); wcount=0; }
  } else while (sp->val != (UINT32_MAX & LSEfw_info.timesteps)) delayit();
#if (defined(__x86_64__))
  asm __volatile__("lfence");
#elif (defined(__sparc__))
  asm __volatile__("membar #LoadStore | #LoadLoad");
#elif (defined(__i386__))
  asm __volatile__("lock; addl $0,0(%esp)"); /* load fence */
#endif
#endif
}

inline void LSEfw_syncsig_signal(LSEfw_syncsig_t *sp) {
#ifdef OLD
#if (defined(__x86_64__))
      asm __volatile__("sfence");
#endif
#if (defined(__ia64__))
  asm __volatile__ ("\n"
		"st4.rel [%0] = %1\n"
		";;\n"
		:: "r" (&sp->val) , "r" (1) : "memory"); 
#else
  sp->val = 1;
#endif
#else
#if (defined(__x86_64__))
      asm __volatile__("sfence");
#elif (defined(__sparc__))
  asm __volatile__("membar #StoreStore | #StoreLoad");
#endif
  sp->val = LSEfw_info.timesteps;
#endif
}

#endif /* must not use pthreads */

#else /* only one thread */

#define LSEfw_tprivate
#define LSEfw_parallelize_dynids 0

  /***** thread manipulation *********/
  typedef int LSEfw_threadID_t;
#define LSEfw_thread_create(tp,f,p)
#define LSEfw_thread_join(t,rp)

  /*********** mutexes *****************/

class LSEfw_mutex_c {
 public:
  inline void init(void) { }
  inline void lock(void) { }
  inline void unlock(void) { }
  inline int trylock(void) { return 0; }
  inline void destroy(void) { }
};

typedef class LSEfw_mutex_c LSEfw_mutex_t;

  /*********** barriers ****************/

class LSEfw_barrier_c {
 public:
  inline void init(int n) { }
  inline void reset(void) { }
  inline void wait(void) { }
  inline void nowait(void) { }
  inline void destroy(void) { }
};

typedef class LSEfw_barrier_c LSEfw_barrier_t;
typedef class LSEfw_barrier_c LSEfw_oneshot_barrier_t;

  /*********** synchronizing signals *********/

  typedef int LSEfw_syncsig_t;

#define LSEfw_syncsig_init(sp) 
#define LSEfw_syncsig_reset(sp) 
#define LSEfw_syncsig_wait(sp) 
#define LSEfw_syncsig_signal(sp) 

  /************ atomic operators ***************/

#define LSEfw_atomic_inc(cp,mp) ((*(cp))++)
#define LSEfw_atomic_dec(cp,mp) ((*(cp))--)
#define LSEfw_atomic_inc_u64(cp,mp) ((*(cp))++)
#define LSEfw_atomic_dec_u64(cp,mp) ((*(cp))--)

#endif /* more than one thread */
  
#endif /* _SIM_THREADING_H_ */
