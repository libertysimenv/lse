/*  
 * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * This file is processed to become LSE_initfinish.c
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Initialization, finalization, start, and finish routines for the simulator.
 * These routines also call the corresponding routines for domain objects.
 *
 */
m4_include(SIM_quotes.m4)
LSEm4_push_diversion(-1)
m4_include(SIM_database.m4)
m4_pythonfile(
import shlex
)
LSEm4_pop_diversion()m4_dnl

#include <string.h>
#include <ctype.h>
#include <SIM_all_types.h>
#include <SIM_control.h>
#include <SIM_clp_interface.h>
#include <SIM_threading.h>
#if (LSEfw_PARM(LSE_mp_multithreaded)) 
#include <MPscheduler/LSEtasks.h>
#endif

/**************** A little argument library..... *******************/

namespace {

int LSEfw_scan_unsigned(char *s, int bits, uint64_t *p) {
  uint64_t val=0;
  int bcount=0;
  char c;

  if (!*s) return 0;  /* return an error */

  if (*s == '0') {
    if (s[1] == 'x') { 
      s = s + 2;
      while ((c=(*s))) { /* hex */
	if (c <= '9' && c >= '0')
	  val = val * 16 + (c-'0');
	else if (c >= 'A' && c <= 'F') 
	  val = val * 16 + (c-'A'+10);
	else if (c >= 'a' && c <= 'f') 
	  val = val * 16 + (c-'a'+10);
	else return 0;
	bcount += 4;
	s++;
	if (bcount > bits) return 0;
      }
      *p = val;
      return 1;
    } else if (s[1] == 'b') { /* binary */
      s = s + 2;
      while ((c=(*s))) {
	if (c == '1') 
	  val = val * 2 + 1;
	else if (c == '0') 
	  val = val * 2;
	else return 0;
	bcount += 1;
	s++;
	if (bcount > bits) return 0;
      }
      *p = val;
      return 1;
    }
    else { /* octal */
      s = s + 1;
      while ((c=(*s))) { 
	if (c <= '7' && c >= '0') 
	  val = val * 8 + (c-'0');
	else return 0;
	bcount += 3;
	s++;
	if (bcount > bits) return 0;
      }
      *p = val;
      return 1;
    }
  }
  while ((c=(*s))) { 
    if (c <= '9' && c >= '0') 
      val = val * 10 + (c-'0');
    else return 0;
    bcount += 3;
    s++;
    if (bcount > bits) return 0;
  }
  *p = val;
  return 1;
}

int LSEfw_scan_uint64_t(char *s, uint64_t *p) {
  return LSEfw_scan_unsigned(s, 64, p);
}
int LSEfw_scan_uint32_t(char *s, uint32_t *p) {
  uint64_t p2;
  int rval;
  rval = LSEfw_scan_unsigned(s, 32, &p2);
  *p = p2;
  return rval;
}
int LSEfw_scan_uint16_t(char *s, uint16_t *p) {
  uint64_t p2;
  int rval;
  rval = LSEfw_scan_unsigned(s, 16, &p2);
  *p = p2;
  return rval;
}
int LSEfw_scan_uint8_t(char *s, uint8_t *p) {
  uint64_t p2;
  int rval;
  rval = LSEfw_scan_unsigned(s, 8, &p2);
  *p = p2;
  return rval;
}
int LSEfw_scan_int64_t(char *s, int64_t *p) {
  uint64_t p2;
  int rval;
  if (*s == '-') {
    rval = LSEfw_scan_unsigned(s+1, 64, &p2);
    *p = -p2;
    return rval;
  } else {
    rval = LSEfw_scan_unsigned(s, 64, &p2);
    *p = p2;
    return rval;
  }
}
int LSEfw_scan_int32_t(char *s, int32_t *p) {
  uint64_t p2;
  int rval;
  if (*s == '-') {
    rval = LSEfw_scan_unsigned(s+1, 32, &p2);
    *p = -p2;
    return rval;
  } else {
    rval = LSEfw_scan_unsigned(s, 32, &p2);
    *p = p2;
    return rval;
  }
}
int LSEfw_scan_int16_t(char *s, int16_t *p) {
  uint64_t p2;
  int rval;
  if (*s == '-') {
    rval = LSEfw_scan_unsigned(s+1, 16, &p2);
    *p = -p2;
    return rval;
  } else {
    rval = LSEfw_scan_unsigned(s, 16, &p2);
    *p = p2;
    return rval;
  }
}
int LSEfw_scan_int8_t(char *s, int8_t *p) {
  uint64_t p2;
  int rval;
  if (*s == '-') {
    rval = LSEfw_scan_unsigned(s+1, 8, &p2);
    *p = -p2;
    return rval;
  } else {
    rval = LSEfw_scan_unsigned(s, 8, &p2);
    *p = p2;
    return rval;
  }
}

int LSEfw_scan_int(char *s, int *p) {
  uint64_t p2;
  int rval;
  if (*s == '-') {
    rval = LSEfw_scan_unsigned(s+1, 32, &p2);
    *p = -p2;
    return rval;
  } else {
    rval = LSEfw_scan_unsigned(s, 32, &p2);
    *p = p2;
    return rval;
  }
}

int LSEfw_scan_charstar(char *s, char **p) {
  *p = s;
  return 1;
}

int LSEfw_scan_float(char *s, float *p) {
  return sscanf(s,"%f",p);
}
int LSEfw_scan_double(char *s, double *p) {
  return sscanf(s,"%lf",p);
}
  
int LSEfw_scan_boolean(char *s, boolean *p) {
  if (!strcasecmp(s,"TRUE") || !strcmp(s,"1")) {
    *p = TRUE;
    return 1;
  } else if (!strcasecmp(s,"FALSE") || !strcmp(s,"0")) {
    *p = FALSE;
    return 1;
  }
  return 0;
}
 
} // anonymous namespace
 
/**************** BEGIN command-line parsing **********************/

extern "C" {
 volatile int LSE_waitdebugger;
}

extern "C" int LSE_sim_parse_arg(int argc, char *arg, char *argv[]) {
  int ntrans=0; 
  size_t glen = strcspn(arg,"=");

  if (!strncmp(arg,"waitdebugger",glen)) {
    /* wait for debugger */
    LSE_waitdebugger = 1;
    while (LSE_waitdebugger);
    return 1;
  }

#if (LSEfw_PARM(LSE_mp_multithreaded)) 
  if (!strncmp(arg,"mt:",3)) {
    // Parse an MP argument
    char *currarg = arg+3;
    while (1) {
      char *comma=strchr(currarg, ',');
      if (!comma) comma = currarg + strlen(currarg);
      int l = comma - currarg;

      if (!l) {}
      else if (isdigit(currarg[0])) {
	char *argend;
	int nthreads = strtol(currarg, &argend, 0);
	LSEfw_threadingParms.numThreads = nthreads;
	if (argend != comma) {
	  fprintf(stderr,"Misformatted MP thread argument: %s\n", currarg);
	}
      } else if (l == 5+4 && !strncmp(currarg,"lock=none",9)) 
	LSEfw_threadingParms.lockHandling 
	  = LSE_scheduler::threadingParms::ignore;
      else if (l == 5+5 && !strncmp(currarg,"lock=avoid",10)) 
	LSEfw_threadingParms.lockHandling 
	  = LSE_scheduler::threadingParms::avoid;
      else if (l == 5+4 && !strncmp(currarg,"lock=skew",9)) 
	LSEfw_threadingParms.lockHandling 
	  = LSE_scheduler::threadingParms::skew;
      else if (l >= 11 && !strncmp(currarg,"lockoffset=",11))
	LSEfw_threadingParms.lockOffsetFactor = strtol(currarg+11,0,0);

      else if (l == 12 && !strncmp(currarg,"cluster=inst",12))
	LSEfw_threadingParms.clusterHandling
	  = LSE_scheduler::threadingParms::inst;
      else if (l == 12 && !strncmp(currarg,"cluster=none",12))
	LSEfw_threadingParms.clusterHandling
	  = LSE_scheduler::threadingParms::none;
      else if (l == 11 && !strncmp(currarg,"cluster=dsc",11))
	LSEfw_threadingParms.clusterHandling
	  = LSE_scheduler::threadingParms::DSC;
      else if (l == 14 && !strncmp(currarg,"cluster=forest",14))
	LSEfw_threadingParms.clusterHandling
	  = LSE_scheduler::threadingParms::forest;
      else if (l == 13 && !strncmp(currarg,"cluster=chasm",13))
	LSEfw_threadingParms.clusterHandling
	  = LSE_scheduler::threadingParms::CHASM;

      else if (l == 11 && !strncmp(currarg,"reduce=none",11))
	LSEfw_threadingParms.reductionHandling
	  = LSE_scheduler::threadingParms::noneR;
      else if (l == 11 && !strncmp(currarg,"reduce=inst",11))
	LSEfw_threadingParms.reductionHandling
	  = LSE_scheduler::threadingParms::instR;
      else if (l == 14 && !strncmp(currarg,"reduce=balance",14))
	LSEfw_threadingParms.reductionHandling
	  = LSE_scheduler::threadingParms::balanceR;
      else if (l == 11 && !strncmp(currarg,"reduce=rach",11))
	LSEfw_threadingParms.reductionHandling
	  = LSE_scheduler::threadingParms::RACH;
      else if (l == 25 && !strncmp(currarg,"reduce=loadbalancereducer",25))
	LSEfw_threadingParms.reductionHandling
	  = LSE_scheduler::threadingParms::LoadBalanceReducer;
      else if (l == 32 && !strncmp(currarg,"reduce=communicationminimizationreducer",32))
	LSEfw_threadingParms.reductionHandling
	  = LSE_scheduler::threadingParms::CommunicationMinimizationReducer;

      else if (l == 11 && !strncmp(currarg,"blockcost=m",11))
	LSEfw_threadingParms.blockCostModel 
	  = LSE_scheduler::threadingParms::BlockMeasure;
      else if (l >= 10 && !strncmp(currarg,"blockcost=",10)) {
	LSEfw_threadingParms.blockCostModel
	  = LSE_scheduler::threadingParms::BlockUniform;
	LSEfw_threadingParms.blockCost = strtol(currarg+10,0,0);
      }

      else if (l >= 15 && !strncmp(currarg,"commcost=fixed:",15)) {
	LSEfw_threadingParms.commCostModel 
	  = LSE_scheduler::threadingParms::CommUniform;
	LSEfw_threadingParms.commCost = strtol(currarg+15,0,0);
      } else if (l >= 18 && !strncmp(currarg,"commcost=crossing:",18)) {
	LSEfw_threadingParms.commCostModel 
	  = LSE_scheduler::threadingParms::CommBandwidth;
	LSEfw_threadingParms.commCost = strtol(currarg+18,0,0);
      }

      else if (l == 4 && !strncmp(currarg,"push",4)) 
	LSEfw_threadingParms.isPush = true;
      else if (l == 4 && !strncmp(currarg,"pull",4)) 
	LSEfw_threadingParms.isPush = false;

      else if (l >= 9 && !strncmp(currarg, "training=", 9)) 
	sscanf(currarg+9, "%d:%g", &LSEfw_threadingParms.trainingLength, 
	       &LSEfw_threadingParms.trainingConvergence);

      else {
	fprintf(stderr,"Unrecognized MP argument: %s\n", currarg);
      }
      if (!*comma) break;
      currarg = comma+1;
    }
  return 1;
  }
#endif

m4_pythonfile(

rtp_template = r"""
  if (!strncmp(arg,"%(clname)s",glen)) {
    if (arg[glen]) {
      ntrans = %(scanner)s(arg+glen+1,&%(vname)s);
      if (!ntrans) return 0;
      return 1;    
    } else {
      if (argc < 2) return 0;
      ntrans = %(scanner)s(argv[0],&%(vname)s);
      if (!ntrans) return 0;
      return 2;
    }
  }"""

rtps = LSE_db.runtimeParms.items()
rtps.sort()
pdict={}
for rtp in rtps:

    ptype = rtp[1][2]
   
    try:
      pscan = LSEcg_runtimeable_types.get(ptype)[0]
    except:
        sys.stderr.write("LSE Error: Sorry, unable to scan command line for "
			 "runtime parameters of type '%s'." % ptype)
        raise SystemError(1)
    
    pdict = { "clname" : rtp[0],
              "vname"  : rtp[1][0],
              "scanner" : pscan }
    print rtp_template % pdict;
)

 return 0;
}

extern "C" int LSE_domain_parse_arg(char *dname, int argc, char *arg, 
				    char *argv[]) {

  m4_pythonfile(
def doit(mname,isclass):
  print """if (!*dname || !strcmp("%s",dname))
             return LSE_domain_hook_call(parse_arg)(argc,arg,argv);
        """ % mname
LSEcg_domain_hook_loop("parse_arg",doit,doit,LSE_db)
)

  return 0;
}

extern "C" int LSE_sim_parse_leftovers(int argc, char *argv[], char **envp) {
  int numused = 0, rval=0;

  m4_pythonfile(
def doit(dinst,dummy):
  print """rval = LSE_domain_hook_call(parse_leftovers)(argc-numused,
                                      argv+numused,envp);
	   if (rval < 0) return -1;
           numused += rval;
	   if (numused > argc) numused = argc;"""
LSEcg_domain_hook_loop("parse_leftovers", doit, doit, LSE_db)
)

  if (numused<argc) {
    fprintf(LSE_stderr,"LSE: Extra command-line arguments\n");
    return -1;
  }
  return 0;
}

extern "C" void LSE_sim_print_usage(void) {
  fprintf(LSE_stderr,"\t%-33s\t%s\n", "waitdebugger", 
	  "Enter debugger infinite loop");

#if (LSEfw_PARM(LSE_mp_multithreaded)) 
  fprintf(LSE_stderr,"\t%-33s\t%s\n", "mt:[param][,param]*", 
	 "Multithreading parameters");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", "#", "Number of threads");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", 
	  "cluster=[none|inst|dsc|forest|chasm]", "Clustering algorithm");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", 
	  "reduce=[none|inst|balance|rach]", "Reduction algorithm");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", 
	  "lock=[none|avoid|skew]", "Lock algorithm");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", 
	  "lockoffset=#", "Lock offset factor");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", 
	  "blockcost=[#|m]", "Block cost model (m = measured)");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", 
	  "commcost=[fixed|crossing]:#", "Communication cost model");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", 
	  "push|pull", "Execution model to use for post-scheduler");
  fprintf(LSE_stderr,"\t   %-30s\t  %s\n", 
	  "training=<length>:<convergence>", "Training length and convergence");
#endif


m4_pythonfile(
for rtp in rtps:  # still around from arg parsing...
    print """fprintf(LSE_stderr,"\\t%-33s\\t%s [%s]\\n");""" % \
        ( rtp[0] + "=" + LSEcg_runtimeable_types.get(rtp[1][2])[1], rtp[1][3], LSEcg_runtimeable_types.get(rtp[1][2])[2](rtp[1][1]))
)

  /* domain usage hook */

  m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(usage)();"
LSEcg_domain_hook_loop("usage", doit, doit, LSE_db)
)

}

/******************** Init/finish prototypes ***************************/

m4_pythonfile(
for i in LSE_db.instanceOrder:
    if i.withCode:

        print "extern void %s(LSEfw_stf_def_void);" % \
              LSEcg_func_name(i.name,"LSE_do_init",1)

        if reduce(lambda x,y:x or y.isFilled,i.eventOrder,0):
            print "extern void %s(LSEfw_stf_def_void);" % \
                  LSEcg_func_name(i.name,"LSE_finish_stats",1)

        print "extern void %s(LSEfw_stf_def_void);" % \
              LSEcg_func_name(i.name,"LSE_do_finish",1)

for mi in LSE_db.multiInstList:
    print "extern void %s(LSEfw_stf_def_void);" % \
          LSEcg_func_name(mi.name,"LSE_do_init",1)

    if reduce(lambda a,i: a or \
              reduce(lambda x,y:x or y.isFilled,i.eventOrder,0),
              mi.subInstanceList,0):
       print "extern void %s(LSEfw_stf_def_void);" % \
             LSEcg_func_name(mi.name,"LSE_finish_stats",1)

    print "extern void %s(LSEfw_stf_def_void);" % \
          LSEcg_func_name(mi.name,"LSE_do_finish",1)
)

/***************** Instance pointers ********************************/

m4_pythonfile(
for i in LSE_db.instanceOrder:
    print "extern class LSEfw_module %s;" % LSEcg_instdata_name(i.name)
)

/**************** BEGIN init function calls ************************/

#if (LSEfw_PARM(LSE_mp_multithreaded)) 
LSE_scheduler::threadingParms LSEfw_threadingParms;
#endif

extern void LSEfw_prepare_scheduling();
extern void LSEfw_do_scheduling();
extern void LSEfw_undo_scheduling();
extern void LSEfw_remove_scheduling();

extern "C" int LSE_sim_initialize(void) {
  int rval, i;

  LSE_sim_exit_status = 0;

  cout.sync_with_stdio((bool)LSEfw_PARM(LSE_synchronize_stdio));
  cerr.sync_with_stdio((bool)LSEfw_PARM(LSE_synchronize_stdio));
  cin.sync_with_stdio((bool)LSEfw_PARM(LSE_synchronize_stdio));

  /* initialize run-time parameter variables */

  m4_pythonfile(
for rtp in rtps:  # already created in earlier code
    print "%s = %s;" % (rtp[1][0], LSEcg_runtimeable_types[rtp[1][2]][3](rtp[1][1]))
)

  /* initialize domains */

  m4_pythonfile(
def doit(dinst,isclass): 
    print "if ((rval=LSE_domain_hook_call(init)())) return rval;"
LSEcg_domain_hook_loop("init", doit, doit, LSE_db)
)

  /* pass in runtime arguments */

  m4_pythonfile(
def doit(dinst,isclass):
    if isclass: return
    runArgs = LSE_db.domainInstances[dinst].runArgs
    alist = shlex.split(runArgs)
    if not alist: return
    print "{ char* argv[] = { "
    for a in alist:
        sys.stdout.write('  "')
        for j in a:
            if j == '"': sys.stdout.write('\\"')
            elif j == "\\": sys.stdout.write("\\\\")
            elif j in string.printable: sys.stdout.write(j)
            else: sys.stdout.write("\0%o" % ord(j))
        sys.stdout.write('",\n')
    print """  };
  int argc = %d;
  for (int i = 0; i < argc;) { 
    int numused = LSE_domain_hook_call(parse_arg)(argc-i,argv[i],argv+i+1);
    if (numused <= 0) return 1;
    i += numused;
  }
}""" % (len(alist))
LSEcg_domain_hook_loop("parse_arg", doit, doit, LSE_db)
)

  return 0;
}

typedef void (*LSEfw_initfinish_func_t)(LSEfw_stf_proto_void);

typedef struct LSEfw_initfinish_tab_s {
  LSEfw_initfinish_func_t func;
  LSEfw_iptr_t ptr;
} LSEfw_initfinish_tab_t;

namespace {

LSEfw_initfinish_tab_t LSEfw_init_tab[] = {
m4_pythonfile(
for i in LSE_db.instanceOrder:
    name = "LSE_do_init"
    if i.withCode: fname = i.name
    elif i.shareID[0]>=0: fname = i.multiInst.name
    else: continue
    print "{ %s, (LSEfw_iptr_t)&%s }," % (LSEcg_func_name(fname, name, 1),
                                          LSEcg_instdata_name(i.name))
)
};

LSEfw_initfinish_tab_t LSEfw_finish_stats_tab[] = {
m4_pythonfile(
for i in LSE_db.instanceOrder:
   if reduce(lambda x,y:x or y.isFilled,i.eventOrder,0):
       name = "LSE_finish_stats"
       if i.withCode: fname = i.name
       elif i.shareID[0]>=0: fname = i.multiInst.name
       else: continue
       print "{ %s, (LSEfw_iptr_t)&%s }," % (LSEcg_func_name(fname, name, 1),
                                             LSEcg_instdata_name(i.name))
)
};

LSEfw_initfinish_tab_t LSEfw_finish_tab[] = {
m4_pythonfile(
for i in LSE_db.instanceOrder:
    name = "LSE_do_finish"
    if i.withCode: fname = i.name
    elif i.shareID[0]>=0: fname = i.multiInst.name
    else: continue
    print "{ %s, (LSEfw_iptr_t)&%s }," % (LSEcg_func_name(fname, name, 1),
                                          LSEcg_instdata_name(i.name))
)
};

#if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
#if (m4_python(len(LSE_db.LEClevelSizes)))
struct LSEfw_LECinfo_s {
  int first, last;
} LECinfo[]={
  m4_pythonfile(if 1:
     ic = 0
     il = LSE_db.LEClevelSizes.items()
     il.sort()
     for i in il:
        print "{%d,%d}," % (ic,ic+2*i[1])
	ic = ic + 2*i[1] + 1
)
};
#endif
#endif
} // anonymous namespace

extern void *LSEfw_thread_func(void *);

extern "C" int LSE_sim_start() 
{

  unsigned int i,j;
  int rval=0;
  LSEfw_iptr_t LSEfw_iptr;

  /*** Initialize things.... ****/

  LSEfw_prepare_scheduling();

#if (LSEfw_PARM(LSE_mp_multithreaded))
  if (!LSEfw_threadingParms.trainingLength &&
      LSEfw_threadingParms.trainingConvergence == 0)
    LSEfw_do_scheduling();
#else
    LSEfw_do_scheduling();
#endif

  for (i = 0 ; i < LSEfw_num_threads ; i++) {
    LSEfw_shthreads[i].idno = i;
    LSEfw_shthreads[i].sigs_resolved 
     = new int[m4_python(len(LSE_db.signalList)+1)];
  }

  /* TODO: move all ids and resolutions on master lists to free lists */
  LSEdy_dynid_global_num = 0;
  LSE_dynid_default = LSEdy_dynid_initial_create();

  LSE_sim_terminate_now = 0;
  LSE_sim_terminate_count = m4_pythonfile(
if (reduce(lambda x,y:x+y[0].changesTerminateCount,
	   LSE_db.domainOrder,0)+
    reduce(lambda x,y:x+y.changesTerminateCount,
	   LSE_db.domainInstanceOrder,0)): sys.stdout.write("0")
else: sys.stdout.write("1"));
  LSE_sim_exit_status = 0;

  LSEfw_info.timesteps = LSE_time_zero_ticks;
  LSE_time_now = LSE_time_zero;
  LSEfw_schedule_flags[0] = 1;

#if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
  /* set up schedule arrays */
  for (j = 0 ; j < m4_python(print ic) ; j++) {
    LSEfw_schedule_LEC[j].next = &LSEfw_schedule_LEC[j+1];
  }
#if (m4_python(len(LSE_db.LEClevelSizes)))
  for (j = 0 ; j < sizeof(LECinfo)/sizeof(LECinfo[0]) ; j++) {
    LSEfw_schedule_LEC[LECinfo[j].last].next 
      = LSEfw_schedule_LEC_levels[j].head 
      = LSEfw_schedule_LEC_levels[j].tail 
      = &LSEfw_schedule_LEC[LECinfo[j].first];
  }
#endif
  memset(&LSEfw_schedule_LEC_flags[0],0,
	 sizeof(LSEfw_schedule_LEC_flags[0]*m4_python(print ic)));
#endif

  /* initialize mutexes, barriers, etc. */

  LSEfw_schedule_mutex.init();
  LSEfw_sigcheck_mutex.init();
  LSEfw_mainloop_barrier.init(LSEfw_num_threads);
  LSEfw_status_barrier.init(LSEfw_num_threads);
  LSEfw_phase_barrier.init(LSEfw_num_threads);
  LSEfw_phase_end_barrier.init(LSEfw_num_threads);

  /* start domains */

  m4_pythonfile(
def doit(dinst,dummy): print "if ((rval=LSE_domain_hook_call(start)())) return rval;"
LSEcg_domain_hook_loop("start", doit, doit, LSE_db)
)

  LSEfw_schedule_grow_to(&LSEfw_schedule_timestep,
		     	  m4_python(len(LSE_db.cblocksList)));

  /* start simulator modules */

  unsigned int ui;

  for (ui=0;ui<sizeof(LSEfw_init_tab)/sizeof(LSEfw_initfinish_tab_t);ui++) {
    LSEfw_iptr = LSEfw_init_tab[ui].ptr;
    (*LSEfw_init_tab[ui].func)(LSEfw_stf_call0_void);
  }

  /* start threads if necessary */
  LSEfw_info.thread_command = LSEfw_thread_command_None;

#if (LSEfw_PARM(LSE_mp_multithreaded))
  for (int j = 1 ; j < LSEfw_num_threads; ++j) {
    rval = LSEfw_thread_create(&LSEfw_shthreads[j].id,
			       LSEfw_thread_func, (void *)&LSEfw_shthreads[j]);
    if (rval) { /* error handling... */
    }
  }
#endif

  /* and initialization for the zeroth thread */

  LSEfw_schedule_calling_cblock = 0;
  LSEdy_dynid_free_list = 0;
  LSEdy_dynid_master_list = 0;
  LSEfw_shthreads[0].LSEdy_dynid_master_listP = &LSEdy_dynid_master_list;
  LSEfw_shthreads[0].unknown_signal_countP = &unknown_signal_count;
  sigs_resolved = LSEfw_shthreads[0].sigs_resolved;

  // Temporary until I get rid of the initial value array entirely.
  LSEfw_PORT_status = LSEfw_PORT_initial_status;

  LSE_sim_wall_time_elapsed = 0;

  return 0;

}

/**************** BEGIN finish function calls ************************/

namespace {
const char *LSEfw_codeblock_names[] = {
m4_pythonfile(if 1:
    typeclist="PFHSEDG"
    for c in LSE_db.cblocksList:
	if (c[0]==LSEdb_CblockPhase or \
            c[0]==LSEdb_CblockPhaseStart or \
            c[0]==LSEdb_CblockPhaseEnd):
            print """\"%s %-20s   \",""" % (typeclist[c[0]],c[1])
	elif c[0]==LSEdb_CblockDynamic:
            print """\"%s %-20s %-20s %-2d\",""" % (typeclist[c[0]],c[1],c[2],
						   c[3])
	elif c[0]==LSEdb_CblockGen:
	    # not really interesting to see number of times a generated
	    # block is called either, as it is fixed...
 	    pass
	else:
            print """\"%s %-20s %-20s %-2d\",""" % (typeclist[c[0]],c[1],c[2],
						   c[3])
)
};
}

extern "C" int LSE_sim_finish(boolean dostats) 
{
  unsigned int i;
  LSEfw_iptr_t LSEfw_iptr;

  /* stop threads if necessary */

   LSEfw_info.thread_command = LSEfw_thread_command_Finish;
   LSEfw_mainloop_barrier.wait();

#if (LSEfw_PARM(LSE_mp_multithreaded))
  for (int j = 1 ; j < LSEfw_num_threads; ++j) {
    LSEfw_thread_join(LSEfw_shthreads[j].id, 0);
  }
#endif

  if (dostats) {
    for (i=0 ; i < (sizeof(LSEfw_finish_stats_tab) / 
                    sizeof(LSEfw_initfinish_tab_t)) ; i++) {
      LSEfw_iptr = LSEfw_finish_stats_tab[i].ptr;
      (*LSEfw_finish_stats_tab[i].func)(LSEfw_stf_call0_void);
    }
  }

  for (i=0 ; i < (sizeof(LSEfw_finish_tab) / 
                    sizeof(LSEfw_initfinish_tab_t)) ; i++) {
    LSEfw_iptr = LSEfw_finish_tab[i].ptr;
    (*LSEfw_finish_tab[i].func)(LSEfw_stf_call0_void);
  }

  if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram)) {
    fprintf(LSE_stderr,"---- BEGIN Code block histogram ----\n");
    for (i=0;i<sizeof(LSEfw_codeblock_names)/sizeof(char *);i++) {
      fprintf(LSE_stderr,"%s %10d %6.3f\n",
	      LSEfw_codeblock_names[i],LSEfw_cblock_histogram[i],
	      (float)LSEfw_cblock_histogram[i]/LSEfw_info.timesteps);
    }
    fprintf(LSE_stderr,"---- END Code block histogram ----\n");
  }

  /* And now print out the time */

  fprintf(LSE_stderr,"Finish time: ");
  fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
  fprintf(LSE_stderr,"\tHost time: %" PRIu64 ".%06" PRIu64 " sec",
          LSE_sim_wall_time_elapsed/1000000,
	  LSE_sim_wall_time_elapsed%1000000);
  fprintf(LSE_stderr,"\n");

  LSEdy_dynid_initial_cancel(LSE_dynid_default);

/*  free(LSEfw_schedule_timestep.cblocks); */

  /* finish domains */

m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(finish)();"
LSEcg_domain_hook_loop("finish", doit, doit, LSE_db)
)

  for (int i = 0 ; i < LSEfw_num_threads ; i++) 
    delete[] LSEfw_shthreads[i].sigs_resolved;

  LSEfw_undo_scheduling();
  LSEfw_remove_scheduling();

  return 0;
}

extern "C" int LSE_sim_finalize(void) {

  /* finalize domains */
m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(finalize)();"
LSEcg_domain_hook_loop("finalize", doit, doit, LSE_db)
)

  return 0;
}
