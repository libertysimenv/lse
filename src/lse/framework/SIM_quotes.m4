m4_dnl /* 
m4_dnl  * Copyright (c) 2000-2003 The Liberty Computer Architecture Research
m4_dnl  * Group. (http://www.liberty-research.org)
m4_dnl  *
m4_dnl  * All rights reserved.
m4_dnl  * 
m4_dnl  * Permission is hereby granted, without written agreement and without
m4_dnl  * license or royalty fees, to use, copy, modify, and distribute this
m4_dnl  * software and its documentation for any purpose, provided that the
m4_dnl  * above copyright notice and the following two paragraphs appear in
m4_dnl  * all copies of this software.
m4_dnl  * 
m4_dnl  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
m4_dnl  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
m4_dnl  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
m4_dnl  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
m4_dnl  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
m4_dnl  * 
m4_dnl  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
m4_dnl  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
m4_dnl  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
m4_dnl  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
m4_dnl  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
m4_dnl  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
m4_dnl  * OR MODIFICATIONS.
m4_dnl  *
m4_dnl  */
m4_dnl /*%
m4_dnl  * 
m4_dnl  * Set up comments and quotes as we need them to be.  Set up some
m4_dnl  * useful macros as well.
m4_dnl  *
m4_dnl  * Authors: David A. Penry <dpenry@cs.princeton.edu>
m4_dnl  *
m4_dnl  * We use control character quotes (\037 and \036) because the 
m4_dnl  * normal quotes are used in both Python and C code and we do not
m4_dnl  * want to get confused by them.
m4_dnl  */
m4_changecom(/*,*/)m4_dnl
m4_changequote(,)m4_dnl
m4_dnl
m4_dnl  LSEm4_push_diversion
m4_dnl    $1 = new diversion
m4_define(LSEm4_push_diversion,
m4_pushdef(LSEm4_divnum,m4_divnum)m4_divert($1))m4_dnl
m4_dnl
m4_dnl  LSEm4_pop_diversion
m4_define(LSEm4_pop_diversion,
m4_divert(LSEm4_divnum)m4_popdef(LSEm4_divnum))m4_dnl
m4_dnl
m4_define(LSEm4_divnum,0)m4_dnl
m4_dnl
m4_dnl
m4_dnl  LSEm4_error
m4_dnl     Report error and exit
m4_dnl     $1 = error string
m4_dnl     $2 = error code (optional)
m4_define(LSEm4_error,m4_divert
m4_errprint(LSEcodegen: Error at m4___file__ : m4___line__ => $1)
"m4_error:$1"
m4_m4exit(m4_ifelse($2,,1,$2)))
m4_dnl
