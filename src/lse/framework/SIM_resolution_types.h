/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Definition of the speculation resolution type
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines the speculation resolution type and a few APIs
 *
 */
#ifndef _LSE_RESOLUTION_TYPES_H_
#define _LSE_RESOLUTION_TYPES_H_

/* Must have already included LSE_types (which has stdlib), LSE_refcount, 
 * LSE_dynid
 */ 

typedef enum LSE_resolution_class_e {
  LSE_resolution_confirm,
  LSE_resolution_redecide,
} LSE_resolution_class_t;

typedef struct LSEre_resolution_info_s {
  LSE_refcount_t super;
  LSE_dynid_t resolved_inst;   /* resolved instruction */
  LSE_resolution_class_t rclass;

  int num_ids;       /* do not modify directly */
  int alloc_size;    /* This is a purely internal element */
  LSE_dynid_t *ids;  /* do not modify directly */

  LSEfw_resolution_t_attr attrs;               /* domain-specific pieces */
  struct LSEfw_resolution_t_extension fields;  /* config-specific pieces */
} LSEre_resolution_info_t;

extern LSE_resolution_t LSEre_resolution_free_list;    /* free resolutions */
extern LSE_resolution_t LSEre_resolution_master_list;  /* unfree resolutions */

extern void LSE_resolution_dump(void);

extern size_t LSEre_resolution_size;
extern size_t LSEre_resolution_field_size;
extern size_t LSEre_resolution_field_offset;

#endif /* _LSE_RESOLUTION_TYPES_H_ */
