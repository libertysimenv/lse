# /* 
#  * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Include file for simulator building 
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Defines how to get things through m4.
#  * After going through m4, the indentation is pretty random, so we
#  * pass through indent; on systems without indent, this just maps to
#  * cat.  Note that there are always two lines of commands; this is so
#  * that an error in m4 doesn't get dropped in the pipe.
#  *
#  */
# because autoconf/automake doesn't have m4 as a source
M4 = @M4@
AUTOMAKE_OPTIONS=no-dependencies
datadir = @datadir@
includedir = @includedir@

include $(TOPSRCDIR)/include/Make_for_this.mk

%.cc:%.c.m4
	$(M4) -I $(TOPSRCDIR)/include --prefix-builtins $< >$@
	perl -i $(datadir)/lse/framework/LSE_indent.pl $@

%.h:%.h.m4
	$(M4) -I $(TOPSRCDIR)/include --prefix-builtins $< >$@
	perl -i $(datadir)/lse/framework/LSE_indent.pl $@

%.cc:%.clm 
	$(M4) -I $(TOPSRCDIR)/include --prefix-builtins $(TOPSRCDIR)/include/SIM_prefix.m4 -DLSEm4_filename=$< >$@
	perl -i $(datadir)/lse/framework/LSE_indent.pl $@

#%.c:%.c.tmp
##	$(CAT) $< | $(INDENT) > $@ ; echo > /dev/null
#	$(CAT) $< | sed -e 's/ *#/#/g' > $@ ; echo > /dev/null
#	perl -i -npe 's/\#current_line/"\# " . ($$.+1) . " \"$$ARGV\""/eg;' $@

#%.h:%.h.tmp
#	$(CAT) $< | $(INDENT) > $@ ; echo > /dev/null

INCLUDES = -I $(TOPSRCDIR)/include -I$(includedir)/lse -I$(includedir)/domains $(DOMINC)
#INCLUDES = -I $(TOPSRCDIR)/include -I$(includedir)/lse -x c++ -finline-limit=1000000000

