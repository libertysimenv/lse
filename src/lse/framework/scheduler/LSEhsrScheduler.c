/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Python extension module which interacts with the scheduler code
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file contains a Python extension model which interfaces to the
 * scheduler code.
 *
 */

#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS  /* so that in C++ we get the macros */
#endif /* __STDC_CONSTANT_MACROS */
#include <stdarg.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#define DEBUG_SCHEDULE
#include "Python.h"
#ifndef PyMODINIT_FUNC
#define PyMODINIT_FUNC void
#endif
#define IN_PYTHON
/*#include "scheduler.c"*/
#include "scheduler.h"

static struct { /* design structures - all are borrowed references */
  int numsigs; /* number of signals */
  PyObject *LSE_signal_list;
  PyObject *schedule, *unrolledSchedule;
  sched_graph *deps;
  sched_schedule *sched;
  sched_schedule *unrolled_sched;
} design;

static char *stypes[] = {
  "d","e","a","od","oe","oa","ox","id","ie","ia","ix","px","pe","uq",
};

static void build_graph(int *valids, int debug_schedule)
{
  PyObject *res, *sig, *deplist;
  PyObject *pname, *iname;

  int i, j, porti, stype,cno,depwidth, othersig, dropreason;
  
  design.deps=sched_graph_create(design.numsigs);

  /* iterate across the signals */

  for (i=0;i<design.numsigs;i++) {

    sig = PyList_GET_ITEM(design.LSE_signal_list,i); /* borrowed */
    res = PyObject_GetAttrString(sig, "instno"); /* new */
    porti = PyInt_AS_LONG(res);
    Py_DECREF(res);
    res = PyObject_GetAttrString(sig, "type"); /* new */
    stype = PyInt_AS_LONG(res);
    Py_DECREF(res);
    res = PyObject_GetAttrString(sig, "cno"); /* new */
    cno = PyInt_AS_LONG(res);
    Py_DECREF(res);
    res = PyObject_GetAttrString(sig, "dropreason"); /* new */
    dropreason = PyInt_AS_LONG(res);
    Py_DECREF(res);
    deplist = PyObject_GetAttrString(sig, "depson"); /* new */

    if (debug_schedule) {
      iname = PyObject_GetAttrString(sig,"inst"); /* new */
      printf("%d ",i);
      PyObject_Print(iname,stdout,0);
      Py_DECREF(iname);
      printf(" ");
      if (stype<11) {
	pname = PyObject_GetAttrString(sig, "port"); /* new */
	PyObject_Print(pname,stdout,0);
	Py_DECREF(pname);
      } else {
	printf("<dummy>");
      }
      
      printf(" %2d %2s ",
	     porti,stypes[stype]);
      
      printf("cno=%d\n",cno);
    }

    design.deps->n[i].driver = cno;
    design.deps->n[i].badnode = (stype==13);
    /* do not actually drop unused signals from the schedule because 
     * we do need to compute them.  However, we still report them as
     * "dropped" so that we can ignore them.
     */
    design.deps->n[i].dropped = (dropreason != 0 && dropreason != 3);

    depwidth = PyList_Size(deplist);
    for (j=0;j<depwidth;j++) {
      res = PyList_GET_ITEM(deplist,j); /* borrowed */
      othersig = PyInt_AS_LONG(res);
      if (!valids || valids[othersig]) {
	if (debug_schedule)
	  printf("\t%d -> %d\n",othersig,i);
	sched_graph_add_edge(design.deps,othersig,i);
      }
    }

    Py_DECREF(deplist);

  } /* for signals */

  return;

#ifdef NOTNEEDED
 bad_error:
  fprintf(stderr,"Internal error in scheduler.\n");
  exit(2);
#endif
}

#ifdef VERIFY_GRAPH
static void verify_graph(void)
{
  PyObject *res, *sig, *deplist;
  int i, j, depwidth, othersig;
  int bad = 0;

  design.deps=sched_graph_create(design.numsigs);

  /* iterate across the signals */

  for (i=0;i<design.numsigs;i++) {

    sig = PyList_GET_ITEM(design.LSE_signal_list,i); /* borrowed */
    res = PyObject_GetAttrString(sig, "instno"); /* new */
    deplist = PyObject_GetAttrString(sig, "depson"); /* new */
    
    depwidth = PyList_Size(deplist);
    for (j=0;j<depwidth;j++) {
      res = PyList_GET_ITEM(deplist,j); /* borrowed */
      othersig = PyInt_AS_LONG(res);
      if (!sched_graph_has_edge(design.deps,othersig,i)) {
	fprintf(stderr,"Lost edge %d -> %d\n",othersig,i);
	bad = 1;
      }
#ifdef DEBUG_SCHEDULE
      printf("\t%d -> %d\n",othersig,i);
#endif
      sched_graph_add_edge(design.deps,othersig,i);
    }

    Py_DECREF(deplist);

  } /* for signals */
  if (bad) goto bad_error;
  return;

 bad_error:
  fprintf(stderr,"Internal error in scheduler.\n");
  exit(2);

}
#endif

static void do_scheduling(int doit, int edwards_coalesce,
			  long verylargesize,
			  long smallsize,
			  long unrolledsize)
{
  sched_set *S;
  sched_schedule *sched, *unrolled_sched;
  cost_t cost;
  int need_dynamic = !doit;

  S=sched_set_create(design.numsigs);
  sched_set_include_all(S);
  sched = sched_schedule_create(design.numsigs);

  if (doit) {
    cost=find_static_schedule(design.deps,
			      S,sched,SCHED_INFINITE(design.numsigs),0,
			      verylargesize, smallsize);
    if(cost == SCHED_INFINITE(design.numsigs)) {
      need_dynamic = 1;

    } else {
#ifdef DEBUG_SCHEDULE
      printf("\nCost is: %" PRId64 "\n\n",cost);
      sched_dump(design.deps,sched);
      sched_print_sched(design.deps,sched);
      printf("\n");
#endif
      
      if (edwards_coalesce) {
	printf("Doing Edwards coalescing\n");
	sched_edwards_optimize(design.deps,sched);

      } /* edwards_coalesce */
#ifdef DEBUG_SCHEDULE
	sched_dump(design.deps,sched);
	sched_print_sched(design.deps,sched);
	printf("\n");
#endif

    }
  }
  
  if(need_dynamic) { /* create one big dynamic schedule */
    sched->numparts = 0;
    sched->i[sched->numparts].item = -1; /* down */
    sched->i[sched->numparts].flag = -S->num_on;
    sched->i[sched->numparts+S->num_on+1].item = -3; /* dot */
    sched->i[sched->numparts+S->num_on+1].flag = -S->num_on;
    sched->i[sched->numparts+S->num_on+2].item = -2; /* up */
    sched->i[sched->numparts+S->num_on+2].flag = -S->num_on;
    sched_set_mark(S,sched,sched->numparts+1,0); /* dynamic/repeated */
    sched->numparts += S->num_on+3;

#ifdef DEBUG_SCHEDULE
    sched_dump(design.deps,sched);
    sched_print_sched(design.deps,sched);
    printf("\n");
#endif

  }

  sched_set_destroy(S);

  /* unroll the schedule */
  unrolled_sched = sched_unroll(sched, design.deps, unrolledsize);
#ifdef DEBUG_SCHEDULE
    sched_dump(design.deps,unrolled_sched);
    sched_print_sched(design.deps,unrolled_sched);
    printf("\n");
#endif

  design.sched = sched;
  design.unrolled_sched = unrolled_sched;
}

static void unbuild_schedule(void)
{
  int i;
  PyObject *val;

  for (i=0;i<design.sched->numparts;i++) {
    PyList_Append(design.schedule,
		  val = 
		   Py_BuildValue("(ii)",
				 design.sched->i[i].item,
				 design.sched->i[i].flag));
    Py_DECREF(val);
  }
  for (i=0;i<design.unrolled_sched->numparts;i++) {
    PyList_Append(design.unrolledSchedule,
		  val = 
		   Py_BuildValue("(ii)",
				 design.unrolled_sched->i[i].item,
				 design.unrolled_sched->i[i].flag));
    Py_DECREF(val);
  }
}

/* generate_schedule(signal_list, genstatic, coalesceold)
   returns (schedule, unrolledSchedule)
*/
static PyObject *generate_schedule(PyObject *self, PyObject *args)
{
  long gs, co;
  PyObject *rv;
  long verylargesize;
  long smallsize;
  long unrolledsize;

  /* get the inputs */
  if (!PyArg_ParseTuple(args, "Olllll", &design.LSE_signal_list, &gs, &co,
			&verylargesize, &smallsize, &unrolledsize))
    return NULL;

  design.numsigs = PyList_Size(design.LSE_signal_list);
  design.schedule = PyList_New(0);
  if (!design.schedule) {
    return NULL;
  }
  design.unrolledSchedule = PyList_New(0);
  if (!design.unrolledSchedule) {
    Py_DECREF(design.schedule);
    return NULL;
  }

#ifdef DEBUG_SCHEDULE
  printf("Number of signals = %d\n",design.numsigs);
#endif

  build_graph(NULL,1);
  do_scheduling(gs, co, verylargesize, smallsize, unrolledsize);
  unbuild_schedule();

  rv = PyTuple_New(2);
  if (rv) {
    PyTuple_SET_ITEM(rv,0,design.schedule);
    PyTuple_SET_ITEM(rv,1,design.unrolledSchedule);
  } else {
    Py_DECREF(design.schedule);
    Py_DECREF(design.unrolledSchedule);
  }
  return rv;

}

static void do_lecsim_scheduling(int *valids,
				 int debug_schedule, int dosubs)
{
  sched_set *S;
  sched_schedule *sched, *unrolled_sched;
  cost_t cost;

  S=sched_set_create(design.numsigs);
  if (valids) {
    int i;
    for (i=0;i<design.numsigs;i++) {
      if (valids[i]) {
	S->elements[i] = 1;
	S->listelem[S->num_on++] = i;
      }
    }
  } else {
    sched_set_include_all(S);
  }
  sched = sched_schedule_create(design.numsigs);

  cost=find_lecsim_schedule(design.deps,S,sched,dosubs);

  if (debug_schedule) {
    printf("\nCost is: %" PRId64 "\n\n",cost);
    sched_dump(design.deps,sched);
    sched_print_sched(design.deps,sched);
    printf("\n");
  }
      
  /* unroll the schedule */
  unrolled_sched = sched_unroll(sched, design.deps, 16);
  if (debug_schedule) {
    sched_dump(design.deps,unrolled_sched);
    sched_print_sched(design.deps,unrolled_sched);
    printf("\n");
  }

  sched_set_destroy(S);

  design.sched = sched;
  design.unrolled_sched = unrolled_sched;
}


/* generate_lecsim_schedule(signal_list, restricted signal list, dosubs,
 *                          doprint)
 *   returns (schedule, unrolledSchedule)
 */
static PyObject *generate_lecsim_schedule(PyObject *self, PyObject *args)
{
  PyObject *rv;
  PyObject *realList, *res;
  long doprint;
  long dosubs;
  int *valids;

  /* get the inputs */
  if (!PyArg_ParseTuple(args, "OOll", &design.LSE_signal_list, &realList,
			&dosubs, &doprint))
    return NULL;

  design.numsigs = PyList_Size(design.LSE_signal_list);
  design.schedule = PyList_New(0);
  if (!design.schedule) {
    return NULL;
  }
  design.unrolledSchedule = PyList_New(0);
  if (!design.unrolledSchedule) {
    Py_DECREF(design.schedule);
    return NULL;
  }

  /* determine how to restrict the schedule */
  if (realList == Py_None) {
    valids = NULL;
  } else {
    int i,rlsize,sig;

    valids = (int *)calloc(design.numsigs,sizeof(int));
    rlsize = PyList_Size(realList);
    for (i=0;i<rlsize;i++) {
      res = PyList_GET_ITEM(realList,i); /* borrowed */
      sig = PyInt_AS_LONG(res);
      valids[sig] = 1;
    }
  }

  if (doprint)
    printf("Number of signals = %d\n",design.numsigs);

  build_graph(valids,doprint);
  do_lecsim_scheduling(valids,doprint,dosubs);
  unbuild_schedule();
  sched_graph_destroy(design.deps);
  sched_schedule_destroy(design.sched);
  sched_schedule_destroy(design.unrolled_sched);

  if (valids) {
    free(valids);
  }
  rv = PyTuple_New(2);
  if (rv) {
    PyTuple_SET_ITEM(rv,0,design.schedule);
    PyTuple_SET_ITEM(rv,1,design.unrolledSchedule);
  } else {
    Py_DECREF(design.schedule);
    Py_DECREF(design.unrolledSchedule);
  }
  return rv;

}

static void do_acyclic_scheduling(int debug_schedule, int doedwards)
{
  sched_set *S;
  sched_schedule *sched, *unrolled_sched;
  cost_t cost;

  S=sched_set_create(design.numsigs);
  sched_set_include_all(S);
  sched = sched_schedule_create(design.numsigs);

  cost=find_acyclic_schedule(design.deps,S,sched,0,debug_schedule);

  if (debug_schedule) {
    printf("\nCost is: %" PRId64 "\n\n",cost);
    sched_dump(design.deps,sched);
    sched_print_sched(design.deps,sched);
    printf("\n");
  }
      
  /* unroll the schedule */
  unrolled_sched = sched_unroll(sched, design.deps, 16);
  if (debug_schedule) {
    sched_dump(design.deps,unrolled_sched);
    sched_print_sched(design.deps,unrolled_sched);
    printf("\n");
  }

  if (doedwards) {
    printf("Doing Edwards coalescing\n");
    sched_edwards_optimize(design.deps,sched);
  } /* edwards_coalesce */
  if (debug_schedule) {
    sched_dump(design.deps,sched);
    sched_print_sched(design.deps,sched);
    printf("\n");
  }


  sched_set_destroy(S);

  design.sched = sched;
  design.unrolled_sched = unrolled_sched;
}


/* generate_acyclic_schedule(signal_list, doprint)
 *   returns (schedule, unrolledSchedule)
 */
static PyObject *generate_acyclic_schedule(PyObject *self, PyObject *args)
{
  PyObject *rv;
  long doprint;
  long doedwards;

  /* get the inputs */
  if (!PyArg_ParseTuple(args, "Oll", &design.LSE_signal_list, 
			&doedwards, &doprint))
    return NULL;

  design.numsigs = PyList_Size(design.LSE_signal_list);
  design.schedule = PyList_New(0);
  if (!design.schedule) {
    return NULL;
  }
  design.unrolledSchedule = PyList_New(0);
  if (!design.unrolledSchedule) {
    Py_DECREF(design.schedule);
    return NULL;
  }

  if (doprint)
    printf("Number of signals = %d\n",design.numsigs);

  build_graph(NULL,doprint);
  do_acyclic_scheduling(doprint, doedwards);
  unbuild_schedule();
  sched_graph_destroy(design.deps);

  rv = PyTuple_New(2);
  if (rv) {
    PyTuple_SET_ITEM(rv,0,design.schedule);
    PyTuple_SET_ITEM(rv,1,design.unrolledSchedule);
  } else {
    Py_DECREF(design.schedule);
    Py_DECREF(design.unrolledSchedule);
  }
  return rv;

}

static void do_improved_acyclic_scheduling(int debug_schedule, int doedwards)
{
  sched_set *S;
  sched_schedule *sched, *unrolled_sched;
  cost_t cost;

  S=sched_set_create(design.numsigs);
  sched_set_include_all(S);
  sched = sched_schedule_create(design.numsigs);

  cost=find_acyclic_schedule(design.deps,S,sched,1,debug_schedule);

  if (debug_schedule) {
    printf("\nCost is: %" PRId64 "\n\n",cost);
    sched_dump(design.deps,sched);
    sched_print_sched(design.deps,sched);
    printf("\n");
  }
      
  /* unroll the schedule */
  unrolled_sched = sched_unroll(sched, design.deps, 16);
  if (debug_schedule) {
    sched_dump(design.deps,unrolled_sched);
    sched_print_sched(design.deps,unrolled_sched);
    printf("\n");
  }

  if (doedwards) {
    printf("Doing Edwards coalescing\n");
    sched_edwards_optimize(design.deps,sched);
  } /* edwards_coalesce */
  if (debug_schedule) {
    sched_dump(design.deps,sched);
    sched_print_sched(design.deps,sched);
    printf("\n");
  }


  sched_set_destroy(S);

  design.sched = sched;
  design.unrolled_sched = unrolled_sched;
}


/* generate_improved_acyclic_schedule(signal_list, doprint)
 *   returns (schedule, unrolledSchedule)
 */
static PyObject *generate_improved_acyclic_schedule(PyObject *self, 
						    PyObject *args)
{
  PyObject *rv;
  long doprint;
  long doedwards;

  /* get the inputs */
  if (!PyArg_ParseTuple(args, "Oll", &design.LSE_signal_list, 
			&doedwards, &doprint))
    return NULL;

  design.numsigs = PyList_Size(design.LSE_signal_list);
  design.schedule = PyList_New(0);
  if (!design.schedule) {
    return NULL;
  }
  design.unrolledSchedule = PyList_New(0);
  if (!design.unrolledSchedule) {
    Py_DECREF(design.schedule);
    return NULL;
  }

  if (doprint)
    printf("Number of signals = %d\n",design.numsigs);

  build_graph(NULL,doprint);
  do_improved_acyclic_scheduling(doprint, doedwards);
  unbuild_schedule();
  sched_graph_destroy(design.deps);

  rv = PyTuple_New(2);
  if (rv) {
    PyTuple_SET_ITEM(rv,0,design.schedule);
    PyTuple_SET_ITEM(rv,1,design.unrolledSchedule);
  } else {
    Py_DECREF(design.schedule);
    Py_DECREF(design.unrolledSchedule);
  }
  return rv;

}

static PyMethodDef LSEhsrSchedulerMethods[] = {

  { "generate_schedule", generate_schedule, METH_VARARGS, 
    "Generate a schedule" },

  { "generate_lecsim_schedule", generate_lecsim_schedule, METH_VARARGS, 
    "Generate a LECSIM schedule" },

  { "generate_acyclic_schedule", generate_acyclic_schedule, METH_VARARGS, 
    "Generate an acyclic schedule" },

  { "generate_improved_acyclic_schedule", 
    generate_improved_acyclic_schedule, METH_VARARGS, 
    "Generate an improved acyclic schedule" },

  { NULL, NULL, 0, NULL }   /* Sentinel */
};

PyMODINIT_FUNC 
initLSEhsrScheduler(void) {
  (void) Py_InitModule("LSEhsrScheduler", LSEhsrSchedulerMethods);
  /* fprintf(stderr,"LSE HSR scheduler initialized\n"); */
}
