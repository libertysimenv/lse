/* 
 * Copyright (c) 2002-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Main program for testing the scheduler
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This code simply includes the scheduler and compiles
 *
 */
#define DEVEL
#include <stdlib.h>
#include <string.h>
#include "scheduler.h"

int main(int argc, char *argv[])
{
  sched_graph *F;
  sched_set *S;
  sched_schedule *sched, *unrolled_sched;
  int numnodes=0,i,j;
  int newc,offset;
  cost_t cost;
  int dolecsim=0,doacyclic=0;

  if (argc > 1 && !strcmp(argv[1],"--lecsim")) {
    dolecsim=1;
  }
  if (argc > 1 && !strcmp(argv[1],"--acyclic")) {
    doacyclic=1;
  }
  newc = getchar();
  while (1) {
    if (newc == EOF) exit(1);
    if (newc == '#') {
      do { 
	newc = getchar(); 
      } while (newc != '\n' && newc != EOF);
      newc = getchar();
    }
    else {
      ungetc(newc, stdin);
      break;
    }
  }
  scanf("%d ",&numnodes);
  F=sched_graph_create(numnodes);
  for (i=0;i<numnodes;i++) {
    char hmm;
    scanf("%1c",&hmm);
    F->n[i].driver = hmm;
    F->n[i].badnode = (hmm=='*');
    F->n[i].dropped = (hmm=='!');
  }
  S=sched_set_create(numnodes);
  sched_set_include_all(S);
  sched = sched_schedule_create(numnodes);

  while (1) {
    scanf("%d", &i);
    if (i<0) break;
    while (1) {
      scanf("%d", &j);
      if (j<0) break;
      sched_graph_add_edge(F,i,j);
    }
  }

  offset=numnodes;
  if (dolecsim) {
    cost=find_lecsim_schedule(F,S,sched,1);
  } else if (doacyclic) {
    //cost=find_acyclic_schedule(F,S,sched,1);
  } else {
    cost=find_static_schedule(F,S,sched,SCHED_INFINITE(numnodes),0, 160, 16);
  }
  printf("\nCost is: %" PRId64 "\n\n",cost);
  sched_dump(F,sched);
  sched_print_sched(F,sched);
  printf("\n");

  if (!dolecsim && !doacyclic) {
    sched_edwards_optimize(F,sched);
    sched_dump(F,sched);
    sched_print_sched(F,sched);
    printf("\n");
  }

  unrolled_sched = sched_unroll(sched,F,16);
  sched_dump(F, unrolled_sched);
  sched_print_sched(F, unrolled_sched);
  printf("\n");

  return 0;
}

