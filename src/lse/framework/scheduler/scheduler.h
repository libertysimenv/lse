/* 
 * Copyright (c) 2002 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Static scheduler code for the simulator - header file
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Data structure definitions for the static scheduler
 *
 */
#ifndef _SCHEDULER_H
#define _SCHEDULER_H

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif
#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif
#ifndef __GNUC__
#define __inline__
#endif

#define SCHED_INFINITE(n) (((cost_t)n)*((cost_t)n)+1)

typedef int64_t cost_t;

typedef struct {
  int num_nodes;
  int num_on;
  int *listelem;
  char elements[1];
} sched_set;

typedef struct {
  int num_nodes, num_masked;
  int max_inedges, max_outedges;
  /* adjacency list */
  int *inedges;
  int *outedges;
  struct {
    int driver;
    int badnode;
    int dropped; /* signal that will be dropped later... */
    int inedgecnt;
    int outedgecnt;
  } n[1];
} sched_graph;

typedef struct {
  int time;
  int num_heads;
  int num_on;
  int fcnt;
  struct {
    int marker;  /* holds start */
    int heads;
    int headsize;
    int lastnodeforhead;
    int nextnode; /* moving backwards */
    int f;       /* holds finished */
    int forder;
    int compno;
    int pos;     /* used for opt */
  } v[1];
} sched_dfs;

typedef struct {
  int item; /* >=0: signal, -1=down, -2=up -3=. */
  int flag; /* signal: -1 = first //, -2 = last //, 
	     *         -3 = inside //, -4 = dynamic 
	     * down/up: # of iters 
	     */
} sched_schedule_part;

typedef struct {
  int numparts;
  sched_schedule_part i[1];
} sched_schedule;

extern sched_graph *sched_graph_create(int size);
extern void sched_graph_destroy(sched_graph *G);

extern sched_set *sched_set_create(int size);
extern void sched_set_destroy(sched_set *S);
extern void sched_set_include_all(sched_set *S);
extern void sched_set_include_none(sched_set *S);
extern void sched_set_print(sched_set *S);
extern sched_set *sched_set_copy(sched_set *s);

extern sched_schedule *sched_schedule_create(int size);
extern void sched_schedule_destroy(sched_schedule *R);

extern void sched_set_mark(sched_set *, sched_schedule *, int, int);

extern void sched_dump(sched_graph *F, sched_schedule *sched);

extern void sched_edwards_optimize(sched_graph *F, sched_schedule *R);
extern sched_schedule *sched_unroll(sched_schedule *R, sched_graph *G,
				    long unrolledsize);

extern cost_t find_static_schedule(sched_graph *F, sched_set *S, 
				   sched_schedule *R,
				   cost_t B, int force_sep,
				   long verylargesize, long smallsize);

extern cost_t find_lecsim_schedule(sched_graph *F, sched_set *S, 
				   sched_schedule *R, int dosubs);

extern cost_t find_acyclic_schedule(sched_graph *F, sched_set *S, 
				    sched_schedule *R, int do_improved,
				    int dodebug);

#if defined(DEBUG_OPTIMIZER) || defined(DEBUG_SCHEDULE) || defined(DEVEL) \
  || defined(DEBUG_UNROLLING)
static void sched_print_sched(sched_graph *F, sched_schedule *R) 
{
  int i, lasttype, flag, lastflag, itemval, lastdown;
  lastflag = 0;
  lasttype=0;
  lastdown = 0;

  for (i=0;i<R->numparts;i++) {
    itemval = R->i[i].item;
    flag = R->i[i].flag;
    switch (itemval) {
    case -1 : /* down */
      if (flag >= 0) printf("( ");
      else printf("{ ");
      lastdown = flag;
      break;
    case -2 : /* up */
      if (flag >= 0) printf(")^%d ",flag);
      else printf("} ");
      break;
    case -3 : /* dot */
      if (lastdown >= 0) printf(". ");
      break;
    default:
      if (flag == -1) printf("[ ");
#ifdef DEVEL
      printf("%d%c ",itemval,F->n[itemval].driver);
#else
      printf("%d/%d ",itemval,F->n[itemval].driver);
#endif
      if (flag == -2) printf("] ");
    }
  }
  printf("\n");
}
#endif

static __inline__ int 
sched_graph_has_edge(sched_graph *G, int from, int to) {
  int i;
  if (G->n[from].outedgecnt < G->n[to].inedgecnt) {
    for (i=0;i<G->n[from].outedgecnt;i++) {
      if (G->outedges[from+G->num_nodes*i] == to) return 1;
    }
  } else {
    for (i=0;i<G->n[to].inedgecnt;i++) {
      if (G->inedges[to+G->num_nodes*i] == from) return 1;
    }
  }
  return 0;
}

static __inline__ void sched_graph_add_edge(sched_graph *G, int from, int to) {
  if (!sched_graph_has_edge(G,from,to)) {
    if (G->n[from].outedgecnt==G->max_outedges) {
      G->max_outedges+=8;
      G->outedges=realloc(G->outedges,
                          (G->num_nodes*G->max_outedges+1)*sizeof(int));
      /* printf("Expanding outedges to %d\n",G->max_outedges); */
    }
    if (G->n[to].inedgecnt==G->max_inedges) {
      G->max_inedges+=8;
      G->inedges=realloc(G->inedges,
                          (G->num_nodes*G->max_inedges+1)*sizeof(int));
      /* printf("Expanding inedges to %d\n",G->max_inedges); */
    }
    G->outedges[from+G->num_nodes*(G->n[from].outedgecnt++)]=to;
    G->inedges[to+G->num_nodes*(G->n[to].inedgecnt++)]=from;
  }
}

static __inline__ int 
sched_graph_get_inedge(sched_graph *G, int node, int num) {
  return G->inedges[node+G->num_nodes*num];
}

static __inline__ int 
sched_graph_get_outedge(sched_graph *G, int node, int num) {
  return G->outedges[node+G->num_nodes*num];
}

static __inline__ void
sched_graph_squash_outedge(sched_graph *G, int node, int num) {
  /*fprintf(stderr,"Squashing %d,%d\n",node,num);*/
  G->outedges[node+G->num_nodes*num] = -1;
}

static __inline__ void
sched_graph_squash_inedge(sched_graph *G, int node, int num) {
  /*fprintf(stderr,"Squashing %d,%d\n",node,num);*/
  G->inedges[node+G->num_nodes*num] = -1;
}

#endif
