# /* 
#  * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python scheduling helper code
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This code makes it easier to get things out of Python into C.  It 
#  * figures out data dependencies.  It also unrolls and post-processes
#  * schedules.
#  * 
#  */

import sys, time, types
from SIM_database import *
import LSEhsrScheduler
try:
   import gc
   HAS_GC = 1
except:
   HAS_GC = 0

#####################################################################
#  LSEsc_count_signals
#
#     - create all the signal data structures in the database,
#       annotating instances and ports with the lowest signal number
#       assigned to them.
#####################################################################
def LSEsc_count_signals(db):

    #
    # create a port-full of signals
    #
    def do_driving(i,p,t,cno,constant,isglobal,slist=db.signalList,db=db):
	for j in range(0,p.width):
            if (t>=LSEdb_sigtype_ofdata):
                # context number depends upon port instance number if not
                # driven by module instance
                slist.append(LSEdb_Signal(len(slist),i,p,j,t,cno+j,
                                          constant,isglobal,db))
            else: slist.append(LSEdb_Signal(len(slist),i,p,j,t,cno,
                                            constant,isglobal,db))

    currCnt = 0
    for i in db.instanceOrder:
        realinstancebase = currCnt

        # context of phase function, if any
	if (i.phase): mcno = db.getPhaseCblockNo(i.name)
	else: mcno = 0
        
	for p in i.portOrder:
            
            deps = p.controlDeps
	    p.basesignalno = currCnt
	    if (p.width):
                if (p.dir == "output"):
                    
                    # signals driven by module instance
                    currCnt = currCnt + 2 * p.width
                    do_driving(i,p,LSEdb_sigtype_data,mcno,0,p.controlEmpty)
                    do_driving(i,p,LSEdb_sigtype_en,mcno,0,p.controlEmpty)

                    # signals driven by control point
                    if (not p.controlEmpty):
                        cno = db.getCblockNo(LSEdb_CblockFiring,i.name,p.name,0)
                        currCnt = currCnt + 3 * p.width
                        do_driving(i, p, LSEdb_sigtype_ofdata, cno,
                                   (deps[2]&010) and ((deps[3] & 003) != 3),1)
                        do_driving(i, p, LSEdb_sigtype_ofen, cno,
                                   deps[0] & 010,1)
                        do_driving(i, p, LSEdb_sigtype_ofack, cno,
                                   deps[1] & 010,0)

                        # extra to represent any internal state and force
                        # a run
                        if (p.controlRefsOstatus or p.controlRefsIstatus
                            or p.controlRefsIdata):
                            currCnt = currCnt + p.width
                            do_driving(i, p, LSEdb_sigtype_ofextra, cno, 0,0) 
                else:

                    # signals driven by module instance
                    currCnt = currCnt + p.width
                    do_driving(i,p,2,mcno,0,p.controlEmpty) # ack by module

                    # signals driven by control point
                    if (not p.controlEmpty):
                        cno = db.getCblockNo(LSEdb_CblockFiring,i.name,p.name,0)
                        currCnt = currCnt + 3 * p.width
                        do_driving(i, p, LSEdb_sigtype_ifdata, cno,
                                   (deps[2]&010) and ((deps[3] & 003) != 3),0)
                        do_driving(i, p, LSEdb_sigtype_ifen, cno,
                                   deps[0] & 010,0)
                        do_driving(i, p, LSEdb_sigtype_ifack,
                                   cno, deps[1] & 010,1)

                        # extra to represent any internal state and force
                        # a run

                        if (p.controlRefsOstatus or p.controlRefsIstatus
                            or p.controlRefsIdata):
                            currCnt = currCnt + p.width
                            do_driving(i, p, LSEdb_sigtype_ifextra, cno, 0,0)

            # store references to the signals of the port so global
            # list not needed in module instance code
            p.signals = db.signalList[p.basesignalno:currCnt]

        # add extra signal to represent phase internal state
        if (i.phase):
            i.basesignalno = currCnt
            currCnt = currCnt + 1
            db.signalList.append(LSEdb_Signal(len(db.signalList),i,None,0,
                                              LSEdb_sigtype_pextra,mcno,0,
                                              0,db))
            
        # we care about *all* input signals getting computed if there
        # is a phase end or end_of_timestep user functions.  To ensure
        # that these all get computed (if they aren't used by anything,
        # they get dropped), we create a signal which will
        # use all the inputs, but never be computed itself.
        if (i.end or i.getPoint("end_of_timestep").current):
            i.basesignalno = currCnt
            currCnt = currCnt + 1
            db.signalList.append(LSEdb_Signal(len(db.signalList),i,None,0,
                                              LSEdb_sigtype_end,mcno,0,
                                              0,db))

        # are any scheduled queries called?  if so, add a signal to
        # represent those...
        hassched = filter(lambda y:y[0] == 'LSE_query_call',i.calledby.keys())
        if hassched:
            i.querysigno = currCnt
            currCnt = currCnt + 1
            db.signalList.append(LSEdb_Signal(len(db.signalList),i,None,0,
                                              LSEdb_sigtype_uncquery,
                                              mcno,0,0,db))

        # and store references to the instance's signals
        i.signals = db.signalList[realinstancebase:currCnt]

    return db.signalList

###########################################################################
# LSEsc_find_dependencies
#
#    - find all data dependencies between signals
###########################################################################

# Reminder:  the format of control dependencies is:
#   ( how to get enable, ack, data, constants)
#   each field uses bits: 4 = don't know
#                         3 = constant, 2 = enable, 0 = data, 1 = ack

#  translates the signal type to control dependency number
_LSEsc_sigtype2depno = [ -1, -1, -1, 2, 0, 1, -1, 2, 0, 1, -1, -1, -1 ]

#
# Figure out data dependencies for a signal driven by an output control point
#   deps/realdeps are used to stash the dependencies
#
def _LSEsc_handle_output_cp(db,p,porti,stype,deps,realdeps, depsq):

    depno = _LSEsc_sigtype2depno[stype]
    if depno < 0:  # not one of data/enable/ack
        myoutputdep = 0
        if (p.controlRefsIstatus or
            p.controlRefsIdata): myoutputdep = myoutputdep | 05
        if p.controlRefsOstatus: myoutputdep = myoutputdep | 02
    else: # grab previous analysis results
        myoutputdep = p.controlDeps[depno]
            
    # queries only have effect if there is some "unknown" component
    if ((myoutputdep & 020) or depno < 0):
        _LSEsc_handle_queries(deps,realdeps,depsq,p.inst,{"instno":porti},
                              p.inst.codepoints[p.name].calls,db)

        # filter out dependencies upon outputs of this code block
        iglist = [ p.basesignalno+porti+p.width*LSEdb_sigoff_ofdata,
                   p.basesignalno+porti+p.width*LSEdb_sigoff_ofen,
                   p.basesignalno+porti+p.width*LSEdb_sigoff_ofack ]
        for ig in iglist:
            if ig in deps: deps.remove(ig)
            if ig in realdeps: realdeps.remove(ig)
    
    # Data from module
    # if depends upon data or this is data and there's a constant yes...
    if ((myoutputdep & 001) or
        # these next cases deal with the fact that yes means there is
        # a dependency, while no and unknown mean there is not.  If there
        # is no constant, there is a dependency on data as well (through
        # the merge)
        (stype == LSEdb_sigtype_ofdata and not (myoutputdep & 010)) or
        (stype == LSEdb_sigtype_ofdata and (myoutputdep & 010) and
         ((p.controlDeps[3] & 003) == 3))
        ):
        sno=p.basesignalno+porti+p.width*LSEdb_sigoff_data
        if not db.signalList[sno].constant: deps.append(sno)
        realdeps.append(sno)

    # Enable from module
    # if depends upon enable
    if ((myoutputdep & 004)
        ):
        sno=p.basesignalno+porti+p.width*LSEdb_sigoff_en
        if not db.signalList[sno].constant: deps.append(sno)
        realdeps.append(sno)

    # Ack from external
    # if depends upon ack
    ci = p.connections[porti]
    if (ci==None):
        realdeps.append(-1)
        return
    if (ci[0].controlEmpty): magic=LSEdb_sigoff_ack
    else: magic=LSEdb_sigoff_ifack
    if ((myoutputdep & 002)
        ):
        sno = ci[0].basesignalno+ci[1]+ci[0].width*magic
        if not db.signalList[sno].constant: deps.append(sno)
        realdeps.append(sno)

#
# Figure out data dependencies for a signal driven by an input control point
#   deps/realdeps are used to stash the dependencies
#
def _LSEsc_handle_input_cp(db,p,porti,stype,deps,realdeps,depsq):

    depno = _LSEsc_sigtype2depno[stype]
    if depno < 0:  # not one of data/enable/ack
        myoutputdep = 0
        if (p.controlRefsIstatus or
            p.controlRefsIdata): myoutputdep = myoutputdep | 05
        if p.controlRefsOstatus: myoutputdep = myoutputdep | 02
    else: # grab previous analysis results
        myoutputdep = p.controlDeps[depno]

    # queries only have effect if there is some "unknown" component
    if ((myoutputdep & 020) or depno < 0):
        _LSEsc_handle_queries(deps,realdeps,depsq,p.inst,{"instno":porti},
                              p.inst.codepoints[p.name].calls,db)

        # filter out dependencies upon outputs of this code block
        iglist = [ p.basesignalno+porti+p.width*LSEdb_sigoff_ifdata,
                   p.basesignalno+porti+p.width*LSEdb_sigoff_ifen,
                   p.basesignalno+porti+p.width*LSEdb_sigoff_ifack ]
        for ig in iglist:
            if ig in deps: deps.remove(ig)
            if ig in realdeps: realdeps.remove(ig)
    
    # Ack from module
    # if depends upon ack
    if ((myoutputdep & 002)
        ):
        sno = p.basesignalno+porti+p.width*LSEdb_sigoff_ack
        if not db.signalList[sno].constant: deps.append(sno)
        realdeps.append(sno)


    ci = p.connections[porti]
    if (ci==None):
        realdeps.append(-1)
        return
    if (ci[0].controlEmpty): magic=LSEdb_sigoff_data
    else: magic=LSEdb_sigoff_ofdata

    # Data from external
    # if depends upon data or this
    # is data and there's a constant yes...
    if ((myoutputdep & 001) or
        # these next cases deal with the fact that yes means there is
        # a dependency, while no and unknown mean there is not.  If there
        # is no constant, there is a dependency on data as well (through
        # the merge)
        (stype == LSEdb_sigtype_ifdata and not (myoutputdep & 010)) or
        (stype == LSEdb_sigtype_ifdata and (myoutputdep & 010) and
         ((p.controlDeps[3] & 003) == 3))
        ):
        sno = ci[0].basesignalno+ci[1]+ci[0].width*magic
        if not db.signalList[sno].constant: deps.append(sno)
        realdeps.append(sno)

    # Enable from external
    # if depends upon enable
    if ((myoutputdep & 004)
        ):
        sno = ci[0].basesignalno+ci[1]+ci[0].width*(magic+1)
        if not db.signalList[sno].constant: deps.append(sno)
        realdeps.append(sno)

#
# Check a potential dependency between an input signal and an output signal
#  of a module or phase instance
#
def _LSEsc_try_dep(inst,isport,ispiece,isporti,osport,ospiece,
                   osporti,invert=0):
    connsigs=isport.getConnectedSignals(isporti)[0]
    
    try:
        dep = inst.dependencies[(isport.name+'.'+ispiece,
                                 osport.name+'.'+ospiece)]
    except:
        if invert: return (None, None, None)
        return connsigs

    try:
        eres = eval(dep[0],locals(),locals())
        #print ("Dep. %s(%s) %s.%s[%d] used by %s.%s[%d] result=%d" %
        #       (inst.name, inst.module,
        #	isport.name,ispiece,isporti,
        #	osport.name,ospiece,osporti,eres))
        if invert:
            if eres: return (None,None,None)
        else:
            if not eres: return (None,None,None)
    except:
        #print (("Dep. %s(%s) %s.%s[%d] used by " +
        #	"%s.%s[%d] result=Exception") %
        #       (inst.name, inst.module,
        #	isport.name,ispiece,isporti,
        #	osport.name,ospiece,osporti))
        sys.stderr.write("Warning: Exception while analyzing signal "
                         "dependencies for module instance '%s'\n"
                         "The annotation was '%s'\n"
                         "\tCheck the port_dataflow attribute of this module\n"
                         % (inst.name,dep[1]))
        if invert: return (None,None,None)
    return connsigs

_LSEsc_ignoredUserpoints = {
    "start_of_timestep" : 1,
    "end_of_timestep" : 1,
    "init" : 1,
    "finish" : 1,
    }

#
# Check a potential dependency between a userpoint and an output signal
#
def _LSEsc_try_userpoint_outdep(inst,upname,osport,ospiece,
                                osporti,invert=0):

    if _LSEsc_ignoredUserpoints.has_key(upname): return 0
    #print "Not ignoring %s %s" % (inst.name, upname)
 
    try:
        dep = inst.userpointDeps[(upname,osport.name+'.'+ospiece)]
    except:
        if invert: return 0
        return 1

    try:
        eres = eval(dep[0],locals(),locals())
        #print ("Dep. %s(%s) %s used by %s.%s[%d] result=%d" %
        #       (inst.name, inst.module,
        #	upname,
        #	osport.name,ospiece,osporti,eres))
        if invert:
            if eres: return 0
        else:
            if not eres: return 0
    except:
        #print (("Dep. %s(%s) %s.%s[%d] used by " +
        #	"%s.%s[%d] result=Exception") %
        #       (inst.name, inst.module,
        #	isport.name,ispiece,isporti,
        #	osport.name,ospiece,osporti))
        sys.stderr.write("Warning: Exception while analyzing userpoint "
                         "dependencies for module instance '%s'\n"
                         "The userpoint annotation was '%s'\n"
                         "\tCheck the port_dataflow attribute of this module\n"
                         % (inst.name,dep[1]))
        if invert: return 0
    return 1


_LSEsc_stype_names=['data','en','ack']

#
# Figure out data dependencies for signals driven by a phase function or
#  handlers
#
def _LSEsc_handle_phase(db,inst,port,porti,stype,cno,deps,realdeps,depsq,sno,
                        hlist,s):
    name = _LSEsc_stype_names[stype]

    # Deal with queries in user points
    for up in filter(lambda x:x.type==LSEdb_PointUser, inst.codepointOrder):

        # find out whether the userpoint can affect this signal instance
        # this doesn't include any information about userpoint parameters
        a = _LSEsc_try_userpoint_outdep(inst,up.name,port,name,porti,0)

        # if the userpoint can affect this signal instance, append everything
        # it calls to the dependency list.
        if a:
            s.hasUserpoint = 1
            if up.calls: 
                _LSEsc_handle_queries(deps,realdeps,depsq,inst,{},up.calls,db)

    if inst.dependencies and db.getParmVal('LSE_schedule_analyze_modules'):
        for p in inst.portOrder:
            for pi in range(0,p.width):
                # do not skip unconnected because of
                # control points...
                if (p.dir=="input"):
                    a = _LSEsc_try_dep(inst,p,'data',pi,port,name,porti)[0]
                    if a!=None:
                        if p.handler and sno != a: 
                            hlist.append((2, inst.name, p.name, pi))
                        if a>=0 and not db.signalList[a].constant:
                            deps.append(a)
                        if a not in realdeps: realdeps.append(a)
                    a = _LSEsc_try_dep(inst,p,'en',pi,port,name,porti)[1]
                    if a!=None:
                        if p.handler and sno != a: 
                            hlist.append((2, inst.name, p.name, pi))
                        if a>=0 and not db.signalList[a].constant:
                            deps.append(a)
                        if a not in realdeps: realdeps.append(a)
                else:		
                    a = _LSEsc_try_dep(inst,p,'ack',pi,port,name,porti)[2]
                    if a!=None:
                        if p.handler and sno != a: 
                            hlist.append((2, inst.name, p.name, pi))
                        if a>=0 and not db.signalList[a].constant:
                            deps.append(a)
                        if a not in realdeps: realdeps.append(a)
        return
    else:
        deps.extend(filter(lambda x,db=db: not db.signalList[x].constant,
                           inst.inputSigs))
        realdeps.extend(inst.inputSigs)
        for p in inst.portOrder:
            if p.handler:
                for pi in range(0,p.width):
                    hlist.append((2, inst.name, p.name, pi))
                
    # filter out dependencies upon ourself...
    if sno in deps: deps.remove(sno)
    if sno in realdeps: realdeps.remove(sno)


#
# Figure out data dependencies for the extra signal representing internal
#  state updates....
#
def _LSEsc_handle_extra_phase(db,inst,port,porti,stype,cno,deps,realdeps,sno,
                              depsq):
    # Deal with queries
    _LSEsc_handle_queries(deps,realdeps,depsq,inst,{},
                          inst.userPointCallsFiltered,db)
    deps.extend(filter(lambda x,db=db: not db.signalList[x].constant,
                       inst.inputSigs))
    realdeps.extend(inst.inputSigs)


# translate API class to signal number(s).
# within each, the list is: ( (input nocp, input cp),(output nocp, output cp))
# Negative numbers indicate connected ports (-1=data,-2=enable,-3=ack)
# should be found... Note that the extra commas are *very* important to
# ensure that we get singleton tuples.
#
# Invariants: local == global when no control functions
#             >=0 when driven by module or module's CP
_LSEsc_apiclass2signo={
	'global.data'   : ( ((-1,),(-1,))          ,((0,),(2,))),
	'global.enable' : ( ((-2,),(-2,))          ,((1,),(3,))),
	'global.ack'    : ( ((0,),(3,))            ,((-3,),(-3,))),
	'global.any'    : ( ((-1,-2,0),(-1,-2,3))  ,((0,1,-3),(2,3,-3))),
	'local.data'    : ( ((-1,),(1,))           ,((0,),(0,))),
	'local.enable'  : ( ((-2,),(2,))           ,((1,),(1,))),
	'local.ack'     : ( ((0,),(0,))            ,((-3,),(4,))),
	'local.any'     : ( ((-1,-2,0),(1,2,0))    ,((0,1,-3),(0,1,4))),
	}

#
# Add a dependency upon a group of signals on a port.  How this works is
#  that the user has already called apiclass2signo to figure out what
#   kinds of signals on a port instance are the right ones.  Then this
#   routine takes those "kinds" of signals and translates them into the
#   appropriate signal number.
#
def _LSEsc_add_deps(port, pi, sigs, realdeps, deps, depsq):
    # grab the ones on the other side of the control point
    connsigs = port.getConnectedSignals(pi)[1]
    for sig in sigs:
        if sig<0:  # signal driven by the connected port
            sno = connsigs[-sig - 1]
        else:    # signal driven by us.
            sno = (port.basesignalno +pi+sig*port.width)

        if (sno != -1 and sno not in realdeps):
            realdeps.append(sno)
            deps.append(sno)
            depsq.append(sno)

#
# make the current signal being worked on depend upon queries called in
#   a codeblock.
#
#   deps/realdeps are used to stash the dependencies
#   calls has the calls made by the codeblock
#
def _LSEsc_handle_queries(deps,realdeps,depsq,inst,vcontext,calls,db):

    for cv in calls.items():
        if (cv[0][0] == 'LSE_method_call' or
            cv[0][0] == 'LSE_event_record'): continue
        if (cv[0][0] == 'LSE_query_call'):
            oi=db.getInst(cv[0][1])
            sno = oi.querysigno
            if sno not in realdeps:
                realdeps.append(sno)
                deps.append(sno)
            continue
        # print cv
        port = db.getPort(cv[0][1],cv[0][2],1)
        if not port: # port alias
            pa = db.getPortAlias(cv[0][1],cv[0][2])
            for callinst in cv[1]:
                try:
                    res=eval(callinst,vcontext,vcontext)
                    # print res
                    # print sigs
                    # Range check resulting index
                    if (res >=0 and res < len(pa[1])):
                        pi = pa[1][res]
                        if pi:
                            port = db.getPort(pi[0],pi[1])
                            sigs = _LSEsc_apiclass2signo[cv[0][0]]\
                                   [port.dir=='output'][0] # no control
                            _LSEsc_add_deps(port,pi[2],sigs,realdeps,deps,
                                            depsq)
                    else:
                        # we know the code is bad, but we'll let it pass
                        pass
                except:
                    for pi in pa[1]:
                        if pi:
                            port = db.getPort(pi[0],pi[1])
                            sigs = _LSEsc_apiclass2signo[cv[0][0]]\
                                   [port.dir=='output'][0] # no control
                            _LSEsc_add_deps(port,pi[2],sigs,realdeps,deps,
                                            depsq)
        else: # real port
            sigs=_LSEsc_apiclass2signo[cv[0][0]][port.dir=='output'
                                          ][not port.controlEmpty]
            #sys.stderr.write("hq: dict is %s for %s\n" %
            #                 (vcontext,cv[1]))
            for callinst in cv[1]:
                try:
                    # will fail if involves an expression we do not
                    # understand just from instance number; in such
                    # a case we make dependent upon all.
                    res=eval(callinst,vcontext,vcontext)
                    # print res
                    # print sigs
                    # Range check resulting index
                    if (res >=0 and res < port.width):
                        _LSEsc_add_deps(port,res,sigs,realdeps,deps,depsq)
                    else:
                        # At this point we know the code is bad, but
                        # we'll let it pass for now....
                        pass
                except:
                    for pi in range(0,port.width):
                        _LSEsc_add_deps(port,pi,sigs,realdeps,deps,depsq)

_LSEsc_default_userpoint_deps = [
    ('USERPOINT(*)','*','1'),
    #('USERPOINT(end_of_timestep)','*','0'),
    #('USERPOINT(init)','*','0'),
    #('USERPOINT(finish)','*','0'),
    #('USERPOINT(start_of_timestep)','*','0'),
    ]

#
# expand dependency notations for an instance
#
def _LSEsc_dlist_parse(inst):
    dlist=inst.dependencyAnnotation
    if dlist == None:
        dlist = [('*','*','1')] + _LSEsc_default_userpoint_deps
    else:
        dlist[0:0] = _LSEsc_default_userpoint_deps

    tmap = {}
    umap = {}
    for i in dlist:
        if not i: continue
        sawuserpoint = 0
        llist = []
        if i[0][0:10]=='USERPOINT(':  # handle userpoint parsing
            sawuserpoint = 1
            uname = i[0][10:-1]
            if not uname: continue
            elif (uname=='*'):
                llist = map(lambda x:x.name,
                            filter(lambda x:x.type==LSEdb_PointUser,
                                   inst.codepointOrder))
            else: llist = [uname]
        else: 
            # normal signal parsing
            llist = []
            if (i[0]=='*'):
                for p in inst.portOrder:
                    if (p.dir=="input"):
                        llist.append(p.name+".data")
                        llist.append(p.name+".en")
                    else:
                        llist.append(p.name+".ack")
            elif (i[0]=='*.data'):
                for p in inst.portOrder:
                    if (p.dir=="input"):
                        llist.append(p.name+".data")
            elif (i[0]=='*.en'):
                for p in inst.portOrder:
                    if (p.dir=="input"):
                        llist.append(p.name+".en")
            elif (i[0]=='*.ack'):
                for p in inst.portOrder:
                    if (p.dir=="output"):
                        llist.append(p.name+".ack")
            else: llist=[i[0]]
            
        rlist = []
        if i[1][0:10]=='USERPOINT(':  # handle userpoint parsing
            sawuserpoint = 1
            uname = i[1][10:-1]
            if not uname: continue
            elif (uname=='*'):
                rlist = map(lambda x:x.name,
                            filter(lambda x:x.type==LSEdb_PointUser,
                                   inst.codepointOrder))
            else: rlist = [uname]
        else: 
            if (i[1]=='*'):
                for p in inst.portOrder:
                    if (p.dir=="output"):
                        rlist.append(p.name+".data")
                        rlist.append(p.name+".en")
                    else:
                        rlist.append(p.name+".ack")
            elif (i[1]=='*.data'):
                for p in inst.portOrder:
                    if (p.dir=="output"):
                        rlist.append(p.name+".data")
            elif (i[1]=='*.en'):
                for p in inst.portOrder:
                    if (p.dir=="output"):
                        rlist.append(p.name+".en")
            elif (i[1]=='*.ack'):
                for p in inst.portOrder:
                    if (p.dir=="input"):
                        rlist.append(p.name+".ack")
            else: rlist=[i[1]]

        # now put them together....
        for l in llist:
            for r in rlist:
                annot = str(i[2])
                if (sawuserpoint): umap[(l,r)] = (compile(annot,annot,
                                                          'eval'),annot)
                else: tmap[(l,r)] = (compile(annot,annot,'eval'),annot)
    return (tmap,umap)

#
# Finally, the main function for finding dependencies....
#   This function also marks which code blocks can generate a signal
#   and which input signals are used by a code block
#
def LSEsc_find_dependencies(db):

    # signals we plan to run for
    db.cblocksSensInputSignals = map(lambda x:[],range(db.totalCblocks))

    # signals we did through a query
    db.cblocksQueriedSignals = map(lambda x:[],range(db.totalCblocks))
    
    # signals we are likely to need to look at, so they had better be good
    db.cblocksUsefulInputSignals = map(lambda x:[],range(db.totalCblocks))
    
    # signals we might look at .. do as a shared array to make it easier
    db.cblocksPotentialInputSignals = {}
    tmap = {}
    amap = {}
    for i in db.instanceOrder:
        tmap[i.name] = []
        amap[i.name] = []
    for cno in range(0,db.totalCblocks):
        c = db.cblocksList[cno]
        if c[0] == LSEdb_CblockDynamic: 
            db.cblocksPotentialInputSignals[cno] = []
        elif c[0] == LSEdb_CblockFiring:
            db.cblocksPotentialInputSignals[cno] = []
        elif c[0] == LSEdb_CblockPhaseStart:
            db.cblocksPotentialInputSignals[cno] = []
        elif c[0] == LSEdb_CblockPhaseEnd:
            db.cblocksPotentialInputSignals[cno] = []
        else:
            db.cblocksPotentialInputSignals[cno] = tmap[c[1]]

    # signals we have scheduled to compute
    db.cblocksWantedOutputSignals = map(lambda x:[],range(db.totalCblocks))

    # signals we could compute ... done as shared array 
    tmap = {}
    db.cblocksPotentialOutputSignals = {}
    for i in db.instanceOrder:
        tmap[i.name] = []
    for cno in range(0,db.totalCblocks):
        c = db.cblocksList[cno]
        if c[0] == LSEdb_CblockDynamic: 
            db.cblocksPotentialOutputSignals[cno] = []
        elif c[0] == LSEdb_CblockFiring:
            db.cblocksPotentialOutputSignals[cno] = []
        elif c[0] == LSEdb_CblockPhaseStart:
            db.cblocksPotentialOutputSignals[cno] = []
        elif c[0] == LSEdb_CblockPhaseEnd:
            db.cblocksPotentialOutputSignals[cno] = []
        else:
            db.cblocksPotentialOutputSignals[cno] = tmap[c[1]]

    # set up instances for dependency analysis
    
    for i in db.instanceOrder:
        i.inputSigs = i.getAllInputSignals()
        if not i.dependencies:
            dp = _LSEsc_dlist_parse(i)
            i.dependencies = dp[0]
            i.userpointDeps = dp[1]
            #sys.stderr.write("%s %s %s\n" % (i.name,i.dependencies,
            #                                 i.userpointDeps))

    for s in db.signalList:
        sinst = db.getInst(s.inst)
	if s.port: sport = sinst.getPort(s.port)
	else: sport = None
        depsonq=[]
        if (s.type<=LSEdb_sigtype_ack): # from phase
            handlers = []
            _LSEsc_handle_phase(db,sinst,sport,s.instno,s.type,s.cno,
                                s.depson,s.realdepson, depsonq, s.num,handlers,
                                s)

            # standard stuff...
            if sinst.phase:
                cb = db.getPhaseCblockNo(s.inst)
                s.generators = [cb]
                db.cblocksWantedOutputSignals[cb].append(s.num)
                cis = db.cblocksSensInputSignals[cb]
                cis2 = db.cblocksUsefulInputSignals[cb]
                cis3 = db.cblocksPotentialInputSignals[cb]
                for dq in s.realdepson:
                    if dq not in cis: cis.append(dq)
                    if dq not in cis2: cis2.append(dq)
                    if dq not in cis3: cis3.append(dq)
            else: s.generators = []

            if sinst.end:
                # everything we produce is a potential end of timestep thing
                cb = db.getPhaseEndCblockNo(s.inst,0)
                if s not in db.cblocksPotentialInputSignals[cb]:
                    db.cblocksPotentialInputSignals[cb].append(s)

##             # find handlers relevant to this signal
##             for d in s.realdepson:
##                 dsig = db.signalList[d]
##                 drcb = db.cblocksList[dsig.recvcno]
##                 if drcb[0] == LSEdb_CblockHandler and drcb[1] == s.inst:
##                     if dsig.recvcno not in s.generators:
##                         s.generators.append(dsig.recvcno)
##                         db.cblocksWantedOutputSignals[dsig.recvcno].\
##                                                               append(s.num)
##                     cis = db.cblocksSensInputSignals[dsig.recvcno]
##                     cis2 = db.cblocksUsefulInputSignals[dsig.recvcno]
##                     cis3 = db.cblocksPotentialInputSignals[dsig.recvcno]
##                     if d not in cis: cis.append(d)
##                     for dq in depsonq:
##                         if dq not in cis: cis.append(dq)
##                         if dq not in cis2: cis2.append(dq)
##                         if dq not in cis3: cis3.append(dq)
##                     for dq in s.realdepson:
##                         if dq not in cis2: cis2.append(dq)
##                         if dq not in cis3: cis3.append(dq)

            # any handler receiving a signal that is actually used
            # becomes sensitive to it
            for d in s.realdepson:
                dsig = db.signalList[d]
                drcb = db.cblocksList[dsig.recvcno]
                if drcb[0] == LSEdb_CblockHandler and drcb[1] == s.inst:
                    cis = db.cblocksSensInputSignals[dsig.recvcno]
                    if d not in cis: cis.append(d)
                    
            # all handlers who the data dependencies (as reported from
            # _LSE_sc_handle_phase) say would produce this output must
            # be marked as generators.  Any queries become sensitive inputs
            # all queries as well as all input signals upon which the
            # output signal is dependent are potential and useful input
            # signals.  (Remember that potential is shared, so we are
            # collecting all of the inputs used by any output signal of block)
            for h in handlers:
                rcno = db.cblocksBacklist[h]
                if rcno not in s.generators:
                    s.generators.append(rcno)
                    db.cblocksWantedOutputSignals[rcno].append(s.num)
                cis = db.cblocksSensInputSignals[rcno]
                cis2 = db.cblocksUsefulInputSignals[rcno]
                cis3 = db.cblocksPotentialInputSignals[rcno]
                for dq in depsonq:
                    if dq not in cis: cis.append(dq)
                    if dq not in cis2: cis2.append(dq)
                    if dq not in cis3: cis3.append(dq)
                for dq in s.realdepson:
                    if dq not in cis2: cis2.append(dq)
                    if dq not in cis3: cis3.append(dq)
                    
            # as this list is shared, just append to something
            # it is tempting to prevent this when the signal has no input
            # dependencies, but we do not know that the signal is actually
            # generated in phase_start rather than in phase or a handler
            if len(s.generators):
                db.cblocksPotentialOutputSignals[s.generators[0]].append(s.num)

            # note: we do not put phase_start on the generators list because
            # it does not go through the HSR scheduling algorithm.  But
            # we do mark this signal as a potential output of phase
            cno = db.getPhaseStartCblockNo(s.inst,1)
            if cno >= 0:
                db.cblocksPotentialOutputSignals[cno].append(s.num)
                if (not s.cno or not s.hasUserpoint) and s.realdepson == []:
                    db.cblocksWantedOutputSignals[cno].append(s.num)
                    
        elif (s.type<=LSEdb_sigtype_ofextra): # from output cp
            
            _LSEsc_handle_output_cp(db,sport,
                                    s.instno,s.type,s.depson,s.realdepson,
                                    depsonq)
            cb = db.getCblockNo(LSEdb_CblockFiring,s.inst,s.port,s.instno)
            s.generators = [cb]
            db.cblocksWantedOutputSignals[cb].append(s.num)
            # TOOD: could not add if constant?
            db.cblocksPotentialOutputSignals[cb].append(s.num)
            cis = db.cblocksSensInputSignals[cb]
            cis2 = db.cblocksUsefulInputSignals[cb]
            cis3 = db.cblocksPotentialInputSignals[cb]
            for dq in s.realdepson:
                if dq not in cis: cis.append(dq)
                if dq not in cis2: cis2.append(dq)
                if dq not in cis3: cis3.append(dq)
            
        elif (s.type<=LSEdb_sigtype_ifextra):
            
            _LSEsc_handle_input_cp(db,sport,
                                   s.instno,s.type,s.depson,s.realdepson,
                                   depsonq)
            cb = db.getCblockNo(LSEdb_CblockFiring,s.inst,s.port,s.instno)
            s.generators = [cb]
            db.cblocksWantedOutputSignals[cb].append(s.num)
            # TOOD: could not add if constant?
            db.cblocksPotentialOutputSignals[cb].append(s.num)
            cis = db.cblocksSensInputSignals[cb]
            cis2 = db.cblocksUsefulInputSignals[cb]
            cis3 = db.cblocksPotentialInputSignals[cb]
            for dq in s.realdepson:
                if dq not in cis: cis.append(dq)
                if dq not in cis2: cis2.append(dq)
                if dq not in cis3: cis3.append(dq)
            
        else: # extra signal or phase
            
            _LSEsc_handle_extra_phase(db,sinst,sport,s.instno,s.type,s.cno,
                                      s.depson,s.realdepson,s.num,
                                      depsonq)
            
            # standard stuff...
            if sinst.phase:
                cb = db.getPhaseCblockNo(s.inst)
                s.generators = [cb]
                db.cblocksWantedOutputSignals[cb].append(s.num)
                db.cblocksPotentialOutputSignals[cb].append(s.num)
                cis = db.cblocksSensInputSignals[cb]
                cis2 = db.cblocksUsefulInputSignals[cb]
                cis3 = db.cblocksPotentialInputSignals[cb]
                for dq in s.realdepson:
                    if dq not in cis: cis.append(dq)
                    if dq not in cis2: cis2.append(dq)
                    if dq not in cis3: cis3.append(dq)
            else: s.generators = []
            
            # no need to touch handlers here (extra phase only exists if
            # there is a phase method, and "end" doesn't ever get scheduled)
            
        for d in s.depson:
            db.signalList[d].usedby.append(s.num)

        for d in depsonq:
            # note: it might be possible to exclude those signals
            # which are queried but in fact part of the normal
            # dataflow, but I seem to have merged that all previously
            # and at this point i do not want to rock the boat.
            db.signalList[d].usedbyquery.append(s.num)
            sig = db.signalList[d]
            for g in s.generators:
                ql = db.cblocksQueriedSignals[g]
                if d not in ql: ql.append(d)
                    
    # pull in all queries reachable by end_of_timestep/phase_end
    # right now does not do reachability analysis
    for inst in db.instanceOrder:
        deps = []
        realdeps = []
        depsq = []
        _LSEsc_handle_queries(deps,realdeps,depsq,inst,{},
                              inst.userPointCalls,db)
        cno = db.getPhaseEndCblockNo(inst.name, 1, 1)
        if cno >= 0:
            db.cblocksPotentialInputSignals[cno].extend(realdeps)
        cno = db.getPhaseEndCblockNo(inst.name, 0, 1)
        if cno >= 0:
            db.cblocksPotentialInputSignals[cno].extend(realdeps)

    # mark uses of independent inputs so that we do not drop them from
    # computation (since they are not otherwise used.)  If we are not
    # using independence for scheduling, we need to assume that all
    # inputs can be used so that we do not drop the signals from
    # computation.....
    for inst in db.instanceOrder:
        
        # delete the dependency information from the module instances,
        # as we do not need it any more and it is kind of bulky...
        del inst.dependencies
        del inst.inputSigs
        del inst.userpointDeps
        
        for p in inst.portOrder:
            if (p.width and (p.schedIndependent or
                             not db.getParmVal('LSE_schedule_use_independent')
                             )):
                for pi in range(0,p.width):
                    connsigs=p.getConnectedSignals(pi)[0]
                    if (p.dir=="output"):
                        if connsigs[2] != -1:
                            db.signalList[connsigs[2]].usedby.append(-1)
                    else:
                        if connsigs[0] != -1:
                            db.signalList[connsigs[0]].usedby.append(-1)
                        if connsigs[1] != -1:
                            db.signalList[connsigs[1]].usedby.append(-1)

    # sort to help debugging
    for s in db.signalList:
        s.usedby.sort()
        s.depson.sort()
        s.realdepson.sort()
        s#.usedbyquery.sort()
        
    # figure out signals which can be dropped; they are retained until after
    # scheduling so that we can force runs of each code block
    #
    # 1) unused output signals which were not inserted to force runs
    #    Note: we do want to drop the "end" signal so that we do not
    #    schedule it, since it just is there to make all *inputs* available
    #    by the end of the cycle.  
    # 2) signals without input dependencies *before* constant propagation.
    #    Cannot be done after constant propagation because that could
    #    result in the signal not being computed before it is needed.
    #    What we are really after is what is available at phase start.
    #    Note that this is only good for signals coming from the module.
    #    BUT: if there was a userpoint in phase_start which affects this
    #    signal we actually should not drop it.
    #
    #    Problem: unconnected port instances are constants that show
    #    up before constant propagation!
    # 3) constant signals.  Note that this really should be a subset of 2,
    #    but in case the dependencies got too conservative because of
    #    queries...
    for s in db.signalList:
        if (s.usedby==[] and s.type != LSEdb_sigtype_ofextra and
            s.type != LSEdb_sigtype_ifextra and s.type !=
            LSEdb_sigtype_pextra):
            s.dropreason=LSEdb_sigdropped_notused
        elif s.realdepson == [] and s.type <= LSEdb_sigtype_ack:
            # no dependencies before constant prop
            if (not s.cno # no context, so phase_start must have provided it
                or not s.hasUserpoint # no userpoints affecting it
                ): 
                s.dropreason=LSEdb_sigdropped_noinputdeps
        elif (s.constant):
            s.dropreason=LSEdb_sigdropped_constant
        else:
            s.dropreason=0 

##############################################################################
#  Schedule post-processing
#
#    This monster code does any post-processing of the schedule that
#    is needed.  Unrolling and dropping has moved into ls-schedule, so
#    all that's needed are coalescing and sorting
#    
#    It would be nice if we could make coalescing faster!
#
##############################################################################

# Need to drop constant outputs of firing functions since they do not
# need to be invoked to be created and we do not want them showing
# up in the analysis as static locations.  Also need to drop anything
# without an input dependency, since it must be done at phase start.

def LSEsc_post_process(db):

    # report why signals were dropped
    for s in db.signalList:
        if s.dropreason:
            print "Dropped signal %d -- %s" % \
                  (s.num, LSEdb_sigdropped_text[s.dropreason])

    dt = db.getParmVal('LSE_DAP_dynamic_schedule_type')
    gs = db.getParmVal('LSE_schedule_generate_static')

    # do block coalescing and make the parallel sections look nice...
    LSEsc_block_coalesce(db, db.unrolledSched, 1)
    _LSEsc_sort_parallel_pieces(db,db.unrolledSched)
    LSEsc_schedule_print(db,db.unrolledSched)

    # Wrap LECsim in a big dynamic section
    if not gs and dt != 0:
        # wrap in a big dynamic section
        lsched = len(db.unrolledSched)
        db.unrolledSched[0:0] = [(-1, -lsched)]
        db.unrolledSched.append((-2,-lsched))

    #print "\nTrying new coalescer\n"
    #ns = _LSEsc_block_coalesce2(db, db.unrolledSched)
    #LSEsc_schedule_print(db,ns)


##############################################################################
#  Schedule use
#
#    This code annotates everybody with the scheduling information.  This
#    code is a large part of the meat of what makes embedded dynamic sections
#    possible.  It used to be more complex, but some careful (though late)
#    thought has simplified it.
#
#    Preliminaries:
#    --------------
#    - We are given an already unrolled and coalesced schedule
#    - We are given what code blocks can drive each signal and what
#      inputs each code block is sensitive to
#
#    What we are trying to compute:
#    ------------------------------
#    - Where in the schedule each signal is statically and dynamically computed
#    - Additional insertions of handlers into the static schedule, as they
#      do not have "official" locations yet
#    - What blocks must be run statically before they are run dynamically
#    - What blocks must have a forced dynamic run
#
#    Static portions:
#    ----------------
#    Static scheduling is pretty easy.  The basic idea is that you always
#    mark the signal as computed at this point and remember that its main
#    driver ran at this point.  If it has additional drivers (handlers),
#    then for each driver, insert that driver and mark it as running at
#    this point only if one of its input signals has changed since the last
#    time it ran.
#
#    By setting initial values of "last time block ran" and "last time
#    signal computed" appropriately, propagation of constants to users happens
#    automatically.
#
#    Dynamic portions:
#    -----------------
#    This "should" be easier, but is in fact harder.  The problem is that
#    receiving signals of a value must be prodded when the signal changes
#    value, but this does not happen automatically in the following cases:
#    1) the signal is a constant and
#       a) is connected to a port OR
#       b) is queried
#    2) a port instance is unconnected; handlers have no signal to
#       depend upon, but they need to run.
#    3) A port queried by a block in a dynamic portion resolves before the
#       dynamic portion runs.
#    4) Module queries resolving before the dynamic portion.
#       Perhaps we will define this as "be careful" and make
#       it not a problem.  We certainly have no way to resolve this at this
#       point in time as we don't know how to get the query output or even
#       how many there were.
#    5) A signal may be provided by a control function which was
#       unsuccessfully analyzed; this function needs to be forced to run
#       because it may generate a constant.
#    6) A signal may be provided by a userpoint which has to be run during
#       phase.  This used to be taken care of by an explicit call in the
#       phase_start method and an always-present dynamic section at the
#       start for both static and dynamic cases, but we're going to
#       get rid of that.  Thus we will need to force certain phase functions
#       to run as if they received constants
#
#    In the cases of the "constants", we must also ensure that later steps
#    do not turn a static scheduling into an immediate scheduling.
#
#    Numbers 1,2,5,6 are taken care of by computing the contexts which must
#    be "prodded" into executing.  When a dynamic section comes along, we
#    insert them as dynamic "extras" if they've not already executed.
#
#    Number 4 is ignored for now.
#
#    Number 3 is a blast.  The idea here is that if a generator for a signal
#    in a dynamic section has an input signal which has changed since the
#    last time the generator was run and any generator for the signal queried
#    that input (a bit conservative; more strict would be if that generator
#    performed the query, but we never know that anyway), the generator must
#    be run.
#
#    And now, we have two different ways to do dynamic portions.  The first
#    is the old way, which just chaotic scheduling from a single queue.  The
#    second is LECSIM, which has a neat queueing structure that tries to
#    evaluate in a "good" order using tlevels.  Therefore, we need to compute
#    the tlevel on which a receiving context should be woken up for each
#    signal.  And, because LECSIM can have little hierarchically scheduled
#    blocks which come out of the scheduler like dynamic sections, we need
#    to allow two levels of dynamic section hierarchy.
#
#    LECSIM tlevels are computed across all dynamic sections; in other words,
#    we do not reset the level counter.
#
##############################################################################

class LSEsc_schedNode:
    def __init__(self, id, cno, generates,forcerun):
        self.id = id
        self.cno = cno
        self.generates = generates
        self.final = []
        self.deps = []
        self.thread = -1
        self.conflicts = {}
        self.singleOrder = -1
        self.subcnos = {}
        self.forceRun = forcerun
        
    def __str__(self):
        return "SN %s" % self.__dict__

def _LSEsc_find_constant_receivers(db):

    # A flag for each code block indicating whether it received a constant
    #   and needs to be run at least once as a result.
    needRunForConstant = [0] * db.totalCblocks

    # remember: this assignment is by reference...
    cblocksSensInputSignals = db.cblocksSensInputSignals
    cblocksWantedOutputSignals = db.cblocksWantedOutputSignals
    cblocksUsefulInputSignals = db.cblocksUsefulInputSignals
    cblocksPotentialInputSignals = db.cblocksPotentialInputSignals
    cblocksPotentialOutputSignals = db.cblocksPotentialOutputSignals

    # Now let's find the blocks which must be prodded

    #
    # 
    # constant input signals (case 1a & 1b)
    #
    for sig in db.signalList:
        for g in sig.generators:
            for sno in cblocksSensInputSignals[g]:
                if sno < 0: continue
                if db.signalList[sno].constant:
                    needRunForConstant[g] = 1

    #
    # unconnected port instances are constants
    #
    for inst in db.instanceOrder:
        for p in inst.portOrder:
            if not p. width: continue
            for i in range(0,p.width):
                ci = p.connections[i]
                if ci: continue
                if p.controlEmpty:
                    if p.handler:
                        cno = db.getCblockNo(LSEdb_CblockHandler,inst.name,
                                             p.name, i)
                        needRunForConstant[cno] = 1
                    else:
                        if inst.phase:
                            cno = db.getPhaseCblockNo(inst.name)
                            needRunForConstant[cno] = 1
                else:
                    cno = db.getCblockNo(LSEdb_CblockFiring,inst.name,
                                         p.name, i)
                    needRunForConstant[cno] = 1

    #
    # control points with unanalyzed dependencies may generate constants...
    #
    for inst in db.instanceOrder:
        for p in inst.portOrder:
            if not p.width or p.controlEmpty: continue

            # base is offset from basesignalno, *not* base signaltype
            if (p.dir == "output"): base=LSEdb_sigoff_ofdata
            else: base=LSEdb_sigoff_ifdata
            d = p.controlDeps

            # anything unanalyzed in cp's
            cno = db.getCblockNo(LSEdb_CblockFiring,inst.name,p.name,0)
            if (d[2] & 020): # data
                for i in range(0,p.width): needRunForConstant[cno+i]=1
            if (d[1] & 020): # enable
                for i in range(0,p.width): needRunForConstant[cno+i]=1
            if (d[0] & 020): # ack
                for i in range(0,p.width): needRunForConstant[cno+i]=1

    #
    # 
    # input signals driven by a forced phase
    #
    for s in db.signalList:
        if s.realdepson == [] and s.type <= LSEdb_sigtype_ack \
           and s.dropreason != LSEdb_sigdropped_noinputdeps:
            needRunForConstant[s.cno] = 1

    return needRunForConstant


def LSEsc_use_schedule(db):

    ############# Now that we have a good schedule, ##################
    ############# let us figure out                 ##################
    ############# how to use it.....                ##################

    #### Set temporaries

    # The last time each signal was calculated
    lastSignalCall = [-1] * len(db.signalList)

    # The last time each signal was generated
    lastSignalGen = {}
    
    # The last time each code block was run.  The default is less than
    # that of signals so that we can force code blocks to run if constants
    # are around
    lastCblockCall = [-2] * db.totalCblocks
    
    # A flag for each code block indicating whether it received a constant
    #   and needs to be run at least once as a result.
    needRunForConstant = _LSEsc_find_constant_receivers(db)

    # remember: this assignment is by reference...
    cblocksSensInputSignals = db.cblocksSensInputSignals
    cblocksWantedOutputSignals = db.cblocksWantedOutputSignals
    cblocksUsefulInputSignals = db.cblocksUsefulInputSignals
    cblocksPotentialInputSignals = db.cblocksPotentialInputSignals
    cblocksPotentialOutputSignals = db.cblocksPotentialOutputSignals
    cblocksQueriedSignals = db.cblocksQueriedSignals

    # Initialize outputs

    db.requiredStatic=needRunForConstant * 1
    
    # use map to get fresh empty lists...
    db.cblocksCalls=map(lambda x:[],range(db.totalCblocks))
    db.cblocksDynSects=map(lambda x:[],range(db.totalCblocks))
    db.signalsCalls=map(lambda x:[],range(len(db.signalList)))
    db.signalsDynSects=map(lambda x:[],range(len(db.signalList)))

    # code blocks that must be run on entrance to a dynamic section
    #  key = location of dyn. section in schedule,
    #  values = list of code block numbers
    db.dynamicExtras={} # contains schedule: [code block numbers]
    db.cblocksLECtlevels = {}
    db.LECsubblocks = {}

    fr = not db.getParmVal("LSE_DAP_schedule_oblivious")

    # Now go over the schedule

    def make_node(sl, bg, bl, cno, gen, forcerun):
        nn = LSEsc_schedNode(sl,cno,gen,forcerun)
        bg[sl] = nn
        bl.append(nn)
        #print "Making node %d for %d" % (sl,cno)
        return sl+1

    def add_cblock(cblock):
        ncb = len(db.cblocksList)
        db.cblocksBacklist[cblock] = ncb
        db.cblocksList.append(cblock)
        cblocksSensInputSignals.append([])
        cblocksUsefulInputSignals.append([])
        cblocksPotentialInputSignals[ncb] = []
        cblocksWantedOutputSignals.append([])
        cblocksPotentialOutputSignals[ncb] = []
        cblocksQueriedSignals.append([])
        db.cblocksCalls.append([])    # for safety
        db.cblocksDynSects.append([])
        return ncb
    
    dynloc = -1 # current dynamic section (-1 = not in a dynamic section)
    dynloccount=0 # depth into dynamic sections
    j=0         # count of items in final schedule
    numstat = 0 # number of statically scheduled signals
    numstatpar = 0 # number of statically scheduled parallel sections
    numdyn = 0  # number of dynamically scheduled signals
    globaltlevel = 0 #
    ts = []
    bg = {}     # graph
    bl = []     # order
    parlist = []
    
    # put 0th node in front so we get happy dynamic section numbering; we
    # will skip this node eventually as it is not necessary.
    j = make_node(j, bg, bl, 0, [], 1)

    dynamic_schedule_type = db.getParmVal('LSE_DAP_dynamic_schedule_type')
    lec_ran = not db.getParmVal('LSE_schedule_generate_static') and \
                      dynamic_schedule_type != 0

    #LSEsc_schedule_print(db,db.unrolledSched)
    #print db.unrolledSched
    
    for i in db.unrolledSched:
	if (i[0]==-1): # entering dynamic section
            
            if dynloccount:
                bg[dynloc].sublist.append(i)
            else:
                #print "entering dynsect %d" % j
                dynloc = j
                db.dynamicExtras[dynloc] = []
                if dynamic_schedule_type == 0:
                    cblock = (LSEdb_CblockDynamic, "DYN",None,j)
                else:
                    cblock = (LSEdb_CblockDynamic, "LEC",None,j)
                ncb = add_cblock(cblock)
                j = make_node(j, bg, bl, ncb, [], 1)
                bg[dynloc].sublist = []
                if dynamic_schedule_type:
                    bg[dynloc].dyntype = 1
                else:
                    bg[dynloc].dyntype = 0
                bg[dynloc].parent = 0
                
            dynloccount = dynloccount + 1
            
	elif (i[0]==-2): # exiting dynamic section

            dynloccount = dynloccount - 1
            
            if not dynloccount:
                db.dynamicExtras[dynloc].sort()
                #print "leaving dynsect %d" % dynloc
                
                if bg[dynloc].dyntype == 1: # handle LEC section

                    # invariant: within a sublist, a signal will appear
                    # only once.  This will allow us to easily say that
                    # a signal has a level.

                    sl = bg[dynloc].sublist

                    if not lec_ran:
                        # Need to do LECsim scheduling here on the list
                        # we have found.  This is going to be fun....
                        print "Doing LEC subscheduling for %d" % dynloc
                        #print sl 
                        
                        (ugh, unrolled) = \
                              LSEhsrScheduler.\
                              generate_lecsim_schedule(db.signalList,
                                                       map(lambda x:x[0],sl),
                                                       dynamic_schedule_type
                                                       != 2, 0)
                        
                        LSEsc_schedule_print(db,unrolled)

                        _LSEsc_tlevel_block_coalesce(db, unrolled, 0)
                            
                        LSEsc_schedule_print(db,unrolled)
                        _LSEsc_sort_parallel_pieces(db, unrolled)
                        
                        LSEsc_schedule_print(db,unrolled)
                        sl = unrolled

                    nl = []
                    sdynloc = -1
                    sbm = {}
                    
                    stlevs = {} # signal local tlevels
                    btlevs = {} # block local tlevels
                    maxlev = 1  # local tlevel; count *up* 
                    
                    for si in sl:
                        # embedded dynamic section
                        
                        if si[0] == -1:

                            #print "entering sub dynsect %d" % j
                            
                            sdynloc = j
                            db.dynamicExtras[j] = []
                            cblock = (LSEdb_CblockDynamic, "LECSUB",None,j)
                            ncb = add_cblock(cblock)
                            j = make_node(j, bg, nl, ncb, [], 1)
                            bg[sdynloc].sublist = []
                            bg[sdynloc].dyntype = 2
                            bg[sdynloc].order = -1
                            bg[sdynloc].parent = dynloc
                            
                        elif (si[0] == -2):

                            #print "leaving sub dynsect %d" % sdynloc
                            
                            # handle end of sub-block
                            # idea is that all signals in sub-block will
                            # be in same tlevel.  All generators go at the
                            # same tlevel.  Later, during analysis, we will
                            # determine which generators actually receive
                            # any work.
                            tlev = 0
                            for sno in bg[sdynloc].sublist:
                                sig = db.signalList[sno]
                                if len(sig.depson):
                                    ll = map(lambda x:stlevs.get(x,-1),
                                             sig.depson)
                                    tlev = max(tlev,max(ll) + 1)
                                    
                            #prs = {}
                            #for sno in map(lambda x:x[0],
                            #               filter(lambda x:x[0]>=0,
                            #                      bg[dynloc].sublist)):
                            #    sig = db.signalList[sno]
                            #    prs.setdefault(sig.recvcno,[]).append(sig)

                            bg[sdynloc].subcnos = {}
                            for sno in bg[sdynloc].sublist:
                                sig = db.signalList[sno]
                                stlevs[sno] = tlev
                                for h in sig.generators:
                                    ol = btlevs.setdefault(h,[])
                                    if tlev not in ol:
                                        ol.append(tlev)
                                        sbm[(h,tlev)] = bg[sdynloc]
                            
                            btlevs[ncb] = [tlev]
                            maxlev = max(maxlev,tlev)

                            #print "Generating sub-schedule %d" % sdynloc
                            #sublist = map(lambda x:(x,0),bg[sdynloc].sublist)
                            #_LSEsc_tlevel_block_coalesce(db, sublist, 0)
                            #LSEsc_schedule_print(db,sublist)

                            sdynloc = -1
                            
                        else:
                            sno = si[0]
                            sig = db.signalList[sno]
                            cno = sig.cno
                            inst = db.getInst(sig.inst)

                            if sdynloc < 0: # in normal dynamic

                                parlist.append(si[0])
                                if si[1] == 0 or si[1] == -2:
                                    # finished up parallel section
                                    # take maximum tlevel across
                                    tlev = 0
                                    for sno in parlist:
                                        sig = db.signalList[sno]
                                        if len(sig.depson):
                                            ll = map(lambda x:stlevs.get(x,-1),
                                                     sig.depson)
                                            tlev = max(tlev,max(ll) + 1)
                                    for sno in parlist:
                                        sig = db.signalList[sno]
                                        stlevs[sno] = tlev
                                        for h in sig.generators:
                                            ol = btlevs.setdefault(h,[])
                                            ol.append(tlev)
                                    maxlev = max(maxlev,tlev)
                                    parlist = []
                                    
                            else: # in sub-dynamic

                                bg[sdynloc].sublist.append(si[0])

                    bg[dynloc].firsttlevel = globaltlevel+1
                    globaltlevel = globaltlevel + 1

                    # set signal tlevels
                    for (sno,lev) in stlevs.items():
                        db.signalList[sno].LECtlevel = lev + globaltlevel

                    # set code block tlevels
                    for (cbno, llist) in btlevs.items():
                        ol = db.cblocksLECtlevels.setdefault(cbno,[])
                        for l in llist:
                            if (l + globaltlevel) not in ol:
                                ol.append(l + globaltlevel)

                    # set codeblock tlevel mapping
                    for ((h,tlev),bgsdynloc) in sbm.items():
                        db.LECsubblocks[(h,tlev + globaltlevel)] = bgsdynloc

                    globaltlevel = globaltlevel + maxlev
                    bg[dynloc].lasttlevel = globaltlevel

                    del sbm
                                
                    stlevs={}  # cannot delete because used in lambda
                    del btlevs
                   
		    if HAS_GC: gc.collect()
 
                    globaltlevel = globaltlevel + 1
                    
                dynloc = -1

            else: # if not dynloccount:
                bg[dynloc].sublist.append(i)
                
        elif (i[0]==-4): pass # removed as not needed
	else:
	    sno = i[0]
            sig = db.signalList[sno]
	    cno = sig.cno
	    inst = db.getInst(sig.inst)
            sig.LECtlevel = globaltlevel
            
	    if (dynloc < 0): # in a static section

                # so that handler logic doesn't choke for dynamic and static
                lastCblockCall[cno] = j
                lastSignalCall[sno] = j

                # There is an assumption that the reported cno is always
                # the first one in the generators list

                if cno: tl = sig.generators[1:]
                else: tl = sig.generators[0:]
                    
                for h in tl:
                    # really want to filter this as:
                    # if any input signal to the handler (including
                    # queried ones) has changed since the last time the
                    # handler was run, then we run the handler
                    #
                    # we also should go if the handler has not run at all
                    # and we have an unconnected input
                    lasttime = lastCblockCall[h]

                    for hs in cblocksSensInputSignals[h]:
                        if hs < 0 : continue
                        if lastSignalCall[hs] > lasttime: break
                    else:
                        if lasttime >= 0:
                            if sno not in bg[lasttime].generates:
                                bg[lasttime].generates.append(sno)
                            lastSignalGen[sno] = max(lasttime,
                                                     lastSignalGen.get(sno,-1))
                            if db.dynamicExtras.get(lasttime) == None:
                                if lasttime not in db.signalsCalls[sno]:
                                    db.signalsCalls[sno].append(lasttime)
                            else:
                                if lasttime not in db.signalsDynSects[sno]:
                                    db.signalsDynSects[sno].append(lasttime)
                            continue
                        elif not needRunForConstant[h]: continue
                        #elif -1 not in sig.realdepson: continue

                    if fr:
                        for hs in cblocksQueriedSignals[h]:
                            if hs < 0 : continue
                            if lastSignalCall[hs] > lasttime:
                                needRunForConstant[h] = 1
                        
                    #sys.stderr.write("  Inserting %d\n" % h)
                    db.cblocksCalls[h].append(j)
                    lastCblockCall[h] = j
                    db.signalsCalls[sno].append(j)
                    lastSignalCall[sno] = j
                    lastSignalGen[sno] = j
                    j = make_node(j, bg, bl, h, [sno], needRunForConstant[h])
                    needRunForConstant[h] = 0

                if cno:


                    if i[1] == 0 or i[1] == -2:
                        
                        # mark that code block is called here
                        db.cblocksCalls[cno].append(j)
                        lastCblockCall[cno] = j
                        j = make_node(j, bg, bl, cno, [sno],
                                      needRunForConstant[cno])
                        needRunForConstant[cno] = 0

                        if i[1] == -2:
                            for s in parlist:
                                # mark that signal is called here
                                db.signalsCalls[s].append(j-1)
                                lastSignalCall[s] = j-1
                                lastSignalGen[s] = j-1
                                bl[-1].generates.append(s)
                            bl[-1].generates.sort()
                            parlist = []

                        db.signalsCalls[sno].append(j-1)
                        lastSignalCall[sno] = j-1
                        lastSignalGen[sno] = j-1
                        numstatpar = numstatpar + 1
                    else:
                        parlist.append(i[0])
                        
                    if fr:
                        if len(db.cblocksCalls[cno])>1:
                            lasttime = db.cblocksCalls[cno][-2]
                        else: lasttime = -1
                        for hs in cblocksQueriedSignals[cno]:
                            if hs < 0 : continue
                            if lastSignalCall[hs] > lasttime:
                                bl[-1].forceRun = 1
                                break

                else:
                    # temporary
                    db.cblocksCalls[0].append(j)
                    # had better be good after initial dynamic section!
                    lastSignalGen[sno] = lastSignalGen.get(sno,1)
                    
		numstat = numstat + 1
                
	    else: # dynamic

                if dynamic_schedule_type !=0:
                    bg[dynloc].sublist.append(i)
                    
                # mark that codeblock is called here
		fd = db.cblocksDynSects[cno]
		if dynloc not in fd: fd.append(dynloc)
                lastCblockCall[cno] = dynloc

                # mark that signal is called here
		fd = db.signalsDynSects[sno]
		if dynloc not in fd: fd.append(dynloc)
                lastSignalCall[sno] = dynloc

                # dynamic section generates the signal...
                bg[dynloc].generates.append(sno)
                cblocksWantedOutputSignals[-1].append(sno)
                cblocksPotentialOutputSignals[ncb].append(sno)
                lastSignalGen[sno] = dynloc

                # and update what the dynamic section does
                for d in sig.realdepson:
                    if d not in cblocksSensInputSignals[-1]:
                        cblocksSensInputSignals[-1].append(d)
                    if d not in cblocksUsefulInputSignals[-1]:
                        cblocksUsefulInputSignals[-1].append(d)
                        
                # mark that generators run in the codeblock
                # There is an assumption that the reported cno is always
                # the first one in the generators list

                if cno: tl = sig.generators[1:]
                else: tl = sig.generators[0:]

                for h in tl:
                    # really want to filter this as:
                    # if any input signal to the handler (including
                    # queried ones) has changed since the last time the
                    # handler was run, then the handler should run in this
                    # dynamic section.  But this is not enough, because
                    # we could also be running due to things later in
                    # the dynamic section!  We'll check for that later...
                    lasttime = lastCblockCall[h]

                    for hs in cblocksSensInputSignals[h]:
                        if hs < 0: continue
                        if lastSignalCall[hs] > lasttime: break
                    else:
                        if lasttime >= 0:
                            if sno not in bg[lasttime].generates:
                                bg[lasttime].generates.append(sno)
                            #lastSignalGen[sno] = max(lasttime,
                            #                        lastSignalGen.get(sno,-1))
                            if db.dynamicExtras.get(lasttime) == None:
                                if lasttime not in db.signalsCalls[sno]:
                                    db.signalsCalls[sno].append(lasttime)
                            else:
                                if lasttime not in db.signalsDynSects[sno]:
                                    db.signalsDynSects[sno].append(lasttime)
                            continue
                        elif not needRunForConstant[h]: continue

                    fd = db.cblocksDynSects[h]
                    if dynloc not in fd: fd.append(dynloc)
                    lastCblockCall[h] = dynloc
                    
                    # we know we would want to re-run in this dynamic
                    # section.  But if it's been achieved through a query,
                    # we won't know to do so.  So, let's force it.
                    for hs in cblocksSensInputSignals[h]:
                        if hs < 0: continue
                        if lastSignalCall[hs] > lasttime and \
                           sno in db.signalList[hs].usedbyquery:
                            break
                    else: continue

                    db.requiredStatic[h] = 0 # static run no longer required
                    needRunForConstant[h] = 0
                    if h not in db.dynamicExtras[dynloc]:
                        db.dynamicExtras[dynloc].append(h)

                # For dynamic sections, because the code driving signals
                # is responsible for scheduling the run in the dynamic
                # section, we need schedule only those contexts which
                # receive constants or which were firings with unanalyzed
                # outputs (which might drive constants)
                
		if (needRunForConstant[cno]):
		    needRunForConstant[cno] = 0
                    db.requiredStatic[cno] = 0     # no static was required
		    db.dynamicExtras[dynloc].append(cno)

                # as mentioned before, if resolving this signal affects
                # signals earlier in this dynamic section, we need to be
                # certain that their generators are also in the dynamic
                # section...
                for user in sig.usedby:
                    if user<0 or lastSignalCall[user] != dynloc: continue
                    tl = db.signalList[user].generators
                    for h in tl:
                        if sno in cblocksSensInputSignals[h]:
                            fd = db.cblocksDynSects[h]
                            if dynloc not in fd: fd.append(dynloc)
                            lastCblockCall[h] = dynloc
                            
		numdyn = numdyn + 1

        # end for i in db.unrolledSched

    # sort tlevels
    if dynamic_schedule_type != 0:
        lls = {}
        for i in range(globaltlevel):
            lls[i] = 0
        for i in db.cblocksLECtlevels.values():
            i.sort()
        for i in db.cblocksLECtlevels.values():
            for ii in i:
                lls[ii] = lls[ii] + 1
        db.LEClevelSizes = lls
    else:
        db.LEClevelSizes = {}
        
    # it is important to insert any code blocks still receiving constants
    # into the mix right here before the first dynamic section
    # .... sigh.... all the nice in-orderness goes away...
    nl = []
    cno = -1
    for nr in needRunForConstant:
        cno += 1
        if not nr or not cno: continue
	db.cblocksCalls[cno].append(j)
        lastCblockCall[cno] = j
        #sys.stderr.write("Inserting nrc %d\n" % cno)
        j = make_node(j, bg, nl, cno, [], 1)
        
    if nl:
        bl[1:1] = nl
    
    # And put any code blocks which didn't run at all at the end of the
    # schedule
    cno = 0
    for lasttime in lastCblockCall:
        if lasttime < 0 and cno:
            cb = db.cblocksList[cno]
            if cb[0] == LSEdb_CblockPhaseStart or cb[0]==LSEdb_CblockPhaseEnd:
		cno += 1
                continue
            #sys.stderr.write("Inserting no run %d\n" % cno)
            db.cblocksCalls[cno].append(j)
            j = make_node(j, bg, bl, cno, [], 1)
        cno += 1

    # now take the lastSignalGen list and use it to update the node final
    # lists
    for sno, lastone in lastSignalGen.items():
        bg[lastone].final.append(sno)

    if 0:
        # signals which were dropped from calculation because they have
        # no input deps should be available after the first dynamic section
        for s in db.signalList:
            sno = s.num
            if s.dropreason == LSEdb_sigdropped_noinputdeps and s.isGlobal:
                # should be there after first dynamic section
                bg[0].final.append(sno)
                bg[0].generates.append(sno)

    # Now insert the phase_start work to be done so that we can get
    # the ordering and all that jazz right...  We also mark all contexts
    # of the module as depending upon phase_start for the module
    sbl = []
    for i in db.instanceOrder:
        cno = db.getPhaseStartCblockNo(i.name,1)
        if cno < 0: continue
        sj = j
        j = make_node(j, bg, sbl, cno, [], 1)

        # make all contexts of instance dependent upon this one...
        for cno2 in range(db.instances[i.name].firstCblock,
                          db.instances[i.name].lastCblock):
            if cno2 == cno: continue
            for b in db.cblocksCalls[cno2]:
                bg[b].deps.append(sj)
            for b in db.cblocksDynSects[cno2]:
                bg[b].deps.append(sj)
        
        db.cblocksCalls[cno] = [ sj ]
        # and what about signals generated by start?
        for sno in cblocksWantedOutputSignals[cno]:
            db.signalsCalls[sno] = [ sj ]
            bg[sj].generates.append(sno)
            bg[sj].final.append(sno)
            lastSignalGen[sno] = sj
            
    # make phase_start go in hierarchical order
    tcno = db.getPhaseStartCblockNo("LSEfw_top",1)
    if tcno >= 0: dl = db.cblocksCalls[tcno]
    else: dl = []
    for i in sbl:
        s = db.cblocksList[i.cno][1]
        while 1:
            k = string.rfind(s,".")
            if k == -1: break
            cno2 = db.getPhaseStartCblockNo(s[:k],1)
            if cno2 >= 0:
                for b in db.cblocksCalls[cno2]:
                    i.deps.append(b)
            s = s[:k]
        i.deps.extend(dl)
    bl[1:1] = sbl

    # and need to figure out dependencies.... fortunately, we've got
    # all we need to do that....  Note that it is NOT safe to use id 
    # numbers for the comparisons because of the blocks added at the 
    # front of the list
    #
    # VERY IMPORTANT.... bl needs to be in a valid execution order!
    i = 0
    for sp in bl: 
	sp.singleOrder = i
	i += 1
    bg[0].singleOrder = 0

    for sp in bl:
        nm = {}
        for hs in cblocksUsefulInputSignals[sp.cno]:
            if hs < 0: continue
            for n in db.signalsCalls[hs]:
                if bg[n].singleOrder < sp.singleOrder:
                    #if n == 0: sys.stderr.write("C1 %s\n" % sp)
                    nm[n]=None
            for n in db.signalsDynSects[hs]:
                if bg[n].singleOrder < sp.singleOrder:
                    #if n == 0: sys.stderr.write("D1 %s\n" % sp)
                    nm[n]=None
        sp.deps.extend(nm.keys())
        sp.deps.sort()
                    
    print ("Static invocations: %d(%d)  Dynamic invocations: %d in %d " \
           "sections" \
           "\nHandler insertions: %d  Dynamic extras: %d" \
	       % (numstat, numstatpar, numdyn, len(db.dynamicExtras),0,
                  reduce(lambda x,y:x+len(y),db.dynamicExtras.values(),0),
                  ))
    db.block_sched_list = bl
    db.block_sched_graph = bg
    db.lastSignalGen = lastSignalGen # for debugging

    ### now... we need to figure out which blocks could generate signals
    # which we have dropped due to lack of interest and assign the last 
    # invocation to be the "true" generator.
    for s in db.signalList:
        if lastSignalGen.has_key(s.num): continue
        for g in s.generators:
            nl = map(lambda x:(db.block_sched_graph[x].singleOrder, x),
                     db.cblocksCalls[g]+db.cblocksDynSects[g])
            nl.sort()
            if not nl: continue # signal generators have no calls?
            lastSignalGen[s.num] = nl[-1][1]
    
    ### determine true potential output signals of schedule item
    # since after a signal has been determined finally, it is no longer
    # going to be changing.  For multi-threading we will have to put
    # a dependency in place to make sure final goes last
    for item in db.block_sched_list:
        item.couldGenerate = cg = []
        pos = db.cblocksPotentialOutputSignals.get(item.cno,[])
        for s in pos:
            gno = lastSignalGen.get(s,0)
            if gno and bg[gno].singleOrder >= item.singleOrder:
                cg.append(s)

    # acyclic scheduling needs a final special dynamic section at the end.
    # everything up to here is set up to believe that the static schedule
    # is valid.  If it is not, the acyclic section should catch it.
    # However, all of the "final" calculations are quite wrong and would
    # get us into trouble if we were to rely upon them for incremental
    # signal checking, so we actually force it to be off in SIM_mainloop
    if (db.getParmVal("LSE_DAP_static_schedule_type")==1 or 
        db.getParmVal("LSE_DAP_static_schedule_type")==2):
        dynloc = j
        db.dynamicExtras[dynloc] = []
        cblock = (LSEdb_CblockDynamic, "ACYCLIC",None,j)
        ncb = add_cblock(cblock)
        j = make_node(j, bg, bl, ncb, [], 1)
        bl[-1].singleOrder = bl[-2].singleOrder+1
        bg[dynloc].dyntype = 3
        
############################################################################
# BLOCK COALESCING
############################################################################
def LSEsc_block_coalesce(db, unrolledSched, report):
    # post-process
    if not db.getParmVal("LSE_schedule_coalesce_static"):
        return

    ct = db.getParmVal("LSE_DAP_static_coalesce_type")
    if not ct: return # Edwards must have been done at ls-schedule

    if ct == 1:
        _LSEsc_dag_block_coalesce(db, unrolledSched, report)
    elif ct == 2:
        _LSEsc_blevel_block_coalesce(db, unrolledSched, report)
    elif ct == 3:
        _LSEsc_tlevel_block_coalesce(db, unrolledSched, report)
    
#
# Make things actually disappear from the schedule (before they were just
# marked to drop), being careful not to mess up parallel sections...
#
def _LSEsc_reduce_after_drop(sched):
    needsstart = 0
    lastgood = 0
    count = 0
    for i in range(0,len(sched)):
        spi = sched[i]
        if (spi[0] == -1 or spi[0] == -2 or spi[0] == -3 or spi[1]>=0):
            continue

        if (spi[0] >= 0):
            count = count + 1
            lastgood = i

        if (spi[1] == -1):
            needsstart = spi[0] == -4
        elif (spi[1] == -2):
            if (count > 1):
                sched[lastgood] = (sched[lastgood][0],-2)
            elif (count==1):
                sched[lastgood] = (sched[lastgood][0],0)
            needsstart = 0
            count = 0
        else:
            if (needsstart and spi[0] >= 0):
                sched[i] = (sched[i][0],-1)            
                needsstart = 0

    return filter(lambda x: x[0] != -4, sched)

#
# The DAG BLOCK COALESCING algorithm.
#
# Improve the schedule by moving pieces of it backwards.  In some ways
# this would be easier in C, but C is actually too early, since I want
# to delay unrolling until Python.
#
# Invariant: no non-separable partitions left which are not dynamic
#            no nested partitions
#            no -4 signals (so schedule size does not change)
#
# The algorithm is:
# scan the schedule from left to right
# for each signal:
#   call the signal sig
#   initialize dependency list to []
#   scan backwards:
#      if sig depends upon the previous signal or anything in the dependency
#      list, prepend previous signal to dependency list, unless we had already
#      found a place to merge the drivers.
#      if the previous signal's driver is the same as sig's driver, remember
#      location.
#   If the previous signal found in the backwards scan is not in the
#     dependency list, move things in the dependency list before the
#     previous signal and merge sig with the previous signal.
#   Special cases:
#     1) When scanning encounters a parallel section, all signals with the
#        same driver must either be added or not added to the dependency list.
#        This prevents previously combined things from being broken up.
#     2) When scanning encounters a dynamic section, merge only if the
#        same driver is found and there are no other invocations being carried
#        along with the one we're trying to move.
#        Of course, that increases the number of potential
#        invocations in the dynamic section, but it is guaranteed to be <=
#        the number with the static.  There may be a little more scheduling
#        cost for signals coming into the dynamic section.  It is a good idea
#        to keep searching past the dynamic section; that would yield fun
#        static things if they are there.
#
#        It would be better if we were to merge if all invocations being
#        carried can find a matching driver.  Also, signals driven by handlers
#        should be allowed to merge into dynamic sections with impunity
#
#     3) Cannot move out of dynamic sections
#     4) handler dependencies are interesting: the merge should happen with
#        something having the same instance number
#     5) At present, cannot start within a parallel section (so cannot
#        move already existing parallel sections....)
def _LSEsc_dag_block_coalesce(db, unrolledSched,report=1):
    i = 0
    ls = len(unrolledSched)
    earliest_driv = [-1] * len(db.cblocksList)
    
    print "Doing DAG-based coalescing"
    
    while (i < ls):
        # Some unnecessary tests for production code....  but still nice
        # to have if debugging...
        #if (reduce(lambda x,y:x or y[0]==-4, unrolledSched,0)):
        #    print unrolledSched
        #    raise "-4 found!"
        #if (ls != len(unrolledSched)):
        #    raise "Length changed!"
        spi = unrolledSched[i]
        if (spi[0] == -1): # dynamic/repeated section
            i = i + 1
            level = 1
            # skip dynamic/repeated section
            while (1):
                spi = unrolledSched[i]
                sig = spi[0]
                if sig == -1: level = level + 1
                elif (sig == -2):
                    level = level - 1
                    if not level: break
                elif sig>0:
                    driver = db.signalList[sig].cno
                    if (earliest_driv[driver]<0): earliest_driv[driver]=i
                i = i + 1
            i = i + 1
            continue
        if (spi[0] <0): # anything special 
            i=i+1
            continue

        sig = spi[0]
        driver = db.signalList[sig].cno
        signaldeps = {}
        for stuff in db.signalList[sig].depson:
            #print "Initial adding %d to signaldeps" % stuff
            signaldeps[stuff] = 1
        cleardeps = 0
        locstomove=[i]
        tlist = [spi]

        j = i-1
        jlimit = earliest_driv[driver]

        found_dynamic = -1
        found_driver = 0

        if (jlimit < 0):
            # if there is no previous driver, do not try, but remember this
            # spot
            earliest_driv[driver]=i
            #print "No previous for %d, %s, %d" % (i,spi,driver)
            i = i + 1
            continue

        if (driver == 0): # no point merging handlers, though it affects the
            i = i + 1
            continue

        if (spi[1] < 0): # anything already parallel we cannot deal with
            i = i + 1
            continue

        #print ("Trying loc %d, driver=%d piece=%s, limit=%d" %
        #       (i, driver,spi,jlimit))

        while (j>=jlimit):
            spj = unrolledSched[j]
            spj0 = spj[0]
            if (spj0 == -4): # skipped
                j = j - 1
                continue
            elif (spj0 == -2): # dynamic/repeated section
                j = j - 1
                found_dynamic_loc = -1
                hasdeps = 0
                #if (driver==0): print "sig %d tlist is %s" % (spi[0],tlist)
                while (1):
                    spj = unrolledSched[j]
                    spj0 = spj[0]
                    if (spj0 == -1): break
                    if (spj0 >= 0):
                        # do we have dependencies from the dynamic section?
                        hasdeps = hasdeps or signaldeps.has_key(spj0)
                        # only try to merge if it is a single node to merge
                        if ((db.signalList[spj0].cno==driver or driver==0)
                            and len(tlist)==1):
                            found_dynamic_loc = j
                    j = j - 1
                if (found_dynamic_loc >= 0): # remember what we found
                    found_dynamic = j
                    save_dynamic_tlist = tlist * 1
                    save_dynamic_locstomove = locstomove * 1
                if (hasdeps): # cannot go further because of dependencies
                    break
                else: # ensure that we keep searching
                    if (found_dynamic >= 0): jlimit = 0
                j = j - 1
                continue
            elif (spj0 < 0):
                j = j - 1
                continue
            elif (spj[1]<0):
                # deal with parallel section as if it were one signal
                hasdeps = 0
                haddriver = 0
                right = j
                #while (unrolledSched[right][1] != -2):
                #    right=right+1
                left=j
                while (unrolledSched[left][1] != -1):
                    left=left-1

                for k in range(right,left-1,-1):
                    sk0 = unrolledSched[k][0]
                    hasdeps = hasdeps or signaldeps.has_key(sk0)
                    haddriver = haddriver or db.signalList[sk0].cno==driver
                    #if signaldeps.has_key(sk0):
                    #    print "Found dep on signal %d" % sk0

                if hasdeps:
                    for k in range(right,left-1,-1):
                        sk0 = unrolledSched[k][0]
                        for stuff in db.signalList[sk0].depson:
                            #print "Parallel adding %d to signaldeps" % stuff
                            signaldeps[stuff]=1                        
                    locstomove = range(left,right+1) + locstomove
                    tlist = unrolledSched[left:right+1] + tlist
                    #print "Adding %s to tlist"% unrolledSched[left:right+1]
                    
                j = left
                # want to fall-through
            else:
                hasdeps = signaldeps.has_key(spj0)
                #if signaldeps.has_key(spj0):
                #    print "Found dep on signal %d" % spj0
                haddriver = db.signalList[spj0].cno==driver
                if (hasdeps):
                    #if (found_driver): # do not move extra stuff once found!
                    #    break # j
                    locstomove = [j] + locstomove
                    for stuff in db.signalList[spj0].depson:
                        #print "Single adding %d to signaldeps" % stuff
                        signaldeps[stuff]=1
                    tlist = [unrolledSched[j]] + tlist
                    #print "Adding %s to tlist" % (unrolledSched[j],)
                    
            if haddriver:
                # if there are no self-dependencies, a merge would work
                if not hasdeps:
                    if report:
                        print "Found good %d@%d to %d@%d driver=%d" % \
                              (spi[0],i,spj0,j,driver)
                    found_driver = 1
                    found_driver_loc = j
                    findinfo=(j,tlist*1,locstomove*1)
                #else:
                #    print "Self-dep for %d@%d to %d@%d driver=%d %s" % \
                #          (spi[0],i,spj0,j,driver,tlist)
            j = j - 1

        # arbitrate among dynamic find and static find
        if (found_driver and found_dynamic >=0):
            if (found_driver_loc > found_dynamic):
                found_driver = 0

        if (found_driver):

            # restore the found information
            j = found_driver_loc
            tlist = findinfo[1]
            locstomove=findinfo[2]
            
            #print "Trying %d@%d %s" % (spi[0],j,tlist)

            # need to go all the way across a parallel section at the
            # destination.  If we cannot, mark it by saying that we
            # are trying to move the place we ended up at.
            if (unrolledSched[j][1] < -1):
                while(1):
                    j = j - 1
                    # if cannot go across the section, complain.
                    if (signaldeps.has_key(unrolledSched[j][0])):
                        locstomove.append(j) # say we are trying to move
                        break
                    if (unrolledSched[j][1] >= -1): break

            # cannot move before self; also covers case where we have
            # had dependency problems
            if j not in locstomove:

                if report:
                    print ("Could move %s to %d @ %d" %
                           (map(lambda x,db=db:(x,unrolledSched[x][0]),
                                locstomove),unrolledSched[j][0],j))

                # LSEsc_schedule_print(db,unrolledSched[j:i+1])

                # do the merge
                # invariant: j is not in the things to move, i is; no skipped
                # signals in the list
                saveitem = unrolledSched[j]
                list=[]
                for k in locstomove:
                    spk = unrolledSched[k]
                    # dummy out stuff that moved
                    unrolledSched[k] = (-4,spk[1])
                
                # should be faster than using +
                nsched = unrolledSched[0:j]
                nsched.extend(tlist)
                nsched.extend(_LSEsc_reduce_after_drop(unrolledSched[j:i+1]))
                nsched.extend(unrolledSched[i+1:])
                unrolledSched[:] = nsched
                
                #unrolledSched[:] = (unrolledSched[0:j] + tlist +
                #                    _LSEsc_reduce_after_drop(\
                #    unrolledSched[j:]))
                
                newloc = j + len(tlist) # new following
                if (saveitem[0] >= 0 and saveitem[1] <0):
                    unrolledSched[newloc] = (saveitem[0],-3)
                else:
                    unrolledSched[newloc] = (saveitem[0],-2)
                t = unrolledSched[newloc-1]
                if (t[0]>=0 and t[1] < 0):
                    unrolledSched[newloc-1] = (t[0], -3)
                else: unrolledSched[newloc-1] = (t[0], -1)

                #LSEsc_schedule_print(db,tlist)
                #LSEsc_schedule_print(db,unrolledSched[j:i+1])
                #LSEsc_schedule_print(db,unrolledSched)

                found_dynamic = -1 # so we do not merge twice
                # recalculate earliest_driv
                for k in range(i,j-1,-1):
                    spk = unrolledSched[k]
                    if (spk[0]>=0):
                        cno = db.signalList[spk[0]].cno
                        if (earliest_driv[cno]>=j):
                            earliest_driv[cno]=k


        # was there a dynamic section to go with?
        # found_dynamic points to the location of the marker
        if (found_dynamic >= 0):
            j = found_dynamic
            lt = len(save_dynamic_tlist)
            if report:
                print ("Could move %s to dynamic @ %d" %
                       (save_dynamic_locstomove,j))

            #LSEsc_schedule_print(db,unrolledSched[j:i+1])

            for k in save_dynamic_locstomove:
                spk = unrolledSched[k]
                # dummy out stuff that moved
                unrolledSched[k] = (-4,spk[1])

            unrolledSched[:] = (unrolledSched[0:j] +
                                save_dynamic_tlist[0:lt-1] +
                                unrolledSched[j:j+1] +
                                [(save_dynamic_tlist[lt-1][0],0)] +
                                _LSEsc_reduce_after_drop(unrolledSched[j+1:]))
            #LSEsc_schedule_print(db,save_dynamic_tlist)
            #LSEsc_schedule_print(db,unrolledSched[j:i+1])
            #LSEsc_schedule_print(db,unrolledSched)

            # recalculate earliest_driv
            for k in range(i,j-1,-1):
                spk = unrolledSched[k]
                if (spk[0]>=0):
                    cno = db.signalList[spk[0]].cno
                    if (earliest_driv[cno]>=j):
                        earliest_driv[cno]=k

        i = i + 1


############ level-based block coalescing ###########################
# coalesce only if at same level, but this is fairly straight-forward.
# Only question is whether a dynamic section at same level counts the
# same as a block (i.e., should the block join the dynamic section)
# the answer is yes....
# Note that we depend upon the schedule already being in topological
# order or being of a kind of schedule (i.e. LEC/acyclic) which does not
# care about this fact.  This allows us to do a simplified search

def _LSEsc_blevel_block_coalesce(db, unrolledSched,report=1):
    print "Doing blevel-based coalescing"

    i = len(unrolledSched)-1
    sblevs = {}
    parlist = []
    runlevels = {}
    
    while (i>=0):
        spi = unrolledSched[i]
        if (spi[0] == -2): # entering dynamic section
            parlist = [spi]
            dl = 1
            while dl:
                i = i - 1
                spi = unrolledSched[i]
                parlist.insert(0,spi) # so order not changed
                if spi[0] == -2: dl = dl + 1
                elif spi[0] == -1: dl = dl - 1

            # take minimum blevel across
            blev = 0
            for (sno,dummy) in parlist:
                if sno < 0: continue
                sig = db.signalList[sno]
                if len(sig.usedby):
                    ll = map(lambda x:sblevs.get(x,-1),sig.usedby)
                    blev = max(blev,max(ll)+1)
            for (sno,dummy) in parlist:
                if sno < 0: continue
                sblevs[sno] = blev
            runlevels.setdefault(blev,[]).append((-1,parlist))
                    
        elif spi[0] == -4: pass
        
        else:
            sno = spi[0]
            sig = db.signalList[sno]
            cno = sig.cno
            inst = db.getInst(sig.inst)
            
            parlist = [spi[0]]

            if spi[1] == -2: # start parallel section
                while 1:
                    i = i - 1
                    spi = unrolledSched[i]
                    parlist.append(spi[0]) # order irrelevant
                    if spi[1] == -1: break
                    
            # take minimum blevel across
            blev = 0
            for sno in parlist:
                sig = db.signalList[sno]
                if len(sig.usedby):
                    ll = map(lambda x:sblevs.get(x,-1),sig.usedby)
                    blev = max(blev,max(ll)+1)
            ll = []
            for sno in parlist:
                sig = db.signalList[sno]
                sblevs[sno] = blev
                ll.append((sig.cno,sno))
            runlevels.setdefault(blev,[]).extend(ll)
                
        i = i - 1

    rl = runlevels.items()
    rl.sort() # get run levels in blevel order
    rl.reverse() # so we get the blevels backwards
    
    unrolledSched[:] = []
    
    for (level, pieces) in rl:
        if report:
            print "Level %d pieces %s" % (level,pieces)
        pieces.sort() # now sorted by cno and sno....
        dynp = filter(lambda x:x[0]<0,pieces)
        statp = filter(lambda x:x[0]>0,pieces)
        handp = filter(lambda x:x[0]==0,pieces)
        savelen = len(unrolledSched)
        
        unrolledSched.extend(map(lambda x:(x[1],0),handp)) # handlers first

        # now static calls
        if statp:
            lastcno = -1
            firstindex = -1
        thisindex = 0
        for (cno,sno) in statp:
            if cno != lastcno:
                if firstindex >= 0:
                    if thisindex == firstindex + 1:
                        unrolledSched[-1] = (unrolledSched[-1][0],0) 
                    else:
                        ind = -(thisindex - firstindex)
                        unrolledSched[ind] = (unrolledSched[ind][0],-1)
                        unrolledSched[-1] = (unrolledSched[-1][0],-2)
                firstindex = thisindex
                lastcno = cno
            unrolledSched.append((sno,-3))
            thisindex = thisindex + 1
        if statp:
            if thisindex == firstindex + 1:
                unrolledSched[-1] = (unrolledSched[-1][0],0) 
            else:
                ind = -(thisindex - firstindex)
                unrolledSched[ind] = (unrolledSched[ind][0],-1)
                unrolledSched[-1] = (unrolledSched[-1][0],-2) 

        # and finally the dynamic calls put into a single section
        if dynp:
            l = reduce(lambda x,y:x+y[1][1:-1],dynp,[])
            llen = len(l)
            unrolledSched.append((-1,-llen))
            unrolledSched.extend(l)
            unrolledSched.append((-2,-llen))

        #LSEsc_schedule_print(db,unrolledSched[savelen:])
        
    return

def _LSEsc_tlevel_block_coalesce(db, unrolledSched,report=1):
    print "Doing tlevel-based coalescing"

    i = 0
    ls = len(unrolledSched)
    stlevs = {}
    parlist = []
    runlevels = {}
    
    while (i < ls):
        spi = unrolledSched[i]
        if (spi[0] == -1): # entering dynamic section
            parlist = [spi]
            dl = 1
            while dl:
                i = i + 1
                spi = unrolledSched[i]
                parlist.append(spi)
                if spi[0] == -1: dl = dl + 1
                elif spi[0] == -2: dl = dl - 1

            # take maximum tlevel across
            tlev = 0
            for (sno,dummy) in parlist:
                if sno < 0: continue
                sig = db.signalList[sno]
                if len(sig.depson):
                    ll = map(lambda x:stlevs.get(x,-1),sig.depson)
                    tlev = max(tlev,max(ll)+1)
            for (sno,dummy) in parlist:
                if sno < 0: continue
                stlevs[sno] = tlev
            runlevels.setdefault(tlev,[]).append((-1,parlist))
                    
        elif spi[0] == -4: pass
        
        else:
            sno = spi[0]
            sig = db.signalList[sno]
            cno = sig.cno
            inst = db.getInst(sig.inst)
            
            parlist = [spi[0]]

            if spi[1] == -1: # start parallel section
                while 1:
                    i = i + 1
                    spi = unrolledSched[i]
                    parlist.append(spi[0])
                    if spi[1] == -2: break
                    
            # take maximum tlevel across parallel section
            tlev = 0
            for sno in parlist:
                sig = db.signalList[sno]
                if len(sig.depson):
                    ll = map(lambda x:stlevs.get(x,-1),sig.depson)
                    tlev = max(tlev,max(ll)+1)
            ll = []
            for sno in parlist:
                sig = db.signalList[sno]
                stlevs[sno] = tlev
                ll.append((sig.cno,sno))
            runlevels.setdefault(tlev,[]).extend(ll)
                
        i = i + 1

    rl = runlevels.items()
    rl.sort() # get run levels in tlevel order

    unrolledSched[:] = []
    
    for (level, pieces) in rl:
        if report:
            print "Level %d pieces %s" % (level,pieces)
        pieces.sort() # now sorted by cno and sno....
        dynp = filter(lambda x:x[0]<0,pieces)
        statp = filter(lambda x:x[0]>0,pieces)
        handp = filter(lambda x:x[0]==0,pieces)
        savelen = len(unrolledSched)
        
        unrolledSched.extend(map(lambda x:(x[1],0),handp)) # handlers first

        # now static calls
        if statp:
            lastcno = -1
            firstindex = -1
        thisindex = 0
        for (cno,sno) in statp:
            if cno != lastcno:
                if firstindex >= 0:
                    if thisindex == firstindex + 1:
                        unrolledSched[-1] = (unrolledSched[-1][0],0) 
                    else:
                        ind = -(thisindex - firstindex)
                        unrolledSched[ind] = (unrolledSched[ind][0],-1)
                        unrolledSched[-1] = (unrolledSched[-1][0],-2)
                firstindex = thisindex
                lastcno = cno
            unrolledSched.append((sno,-3))
            thisindex = thisindex + 1
        if statp:
            if thisindex == firstindex + 1:
                unrolledSched[-1] = (unrolledSched[-1][0],0) 
            else:
                ind = -(thisindex - firstindex)
                unrolledSched[ind] = (unrolledSched[ind][0],-1)
                unrolledSched[-1] = (unrolledSched[-1][0],-2) 

        # and finally the dynamic calls put into a single section
        if dynp:
            l = reduce(lambda x,y:x+y[1][1:-1],dynp,[])
            llen = len(l)
            unrolledSched.append((-1,-llen))
            unrolledSched.extend(l)
            unrolledSched.append((-2,-llen))

        #LSEsc_schedule_print(db,unrolledSched[savelen:])
        
    return

########## an alternative block coalescing algorithm #######
# This one should be cheaper, as it should be O(N log N)... no
# scanning multiple times.  It should also be better.  EXCEPT:
# IT DOESN'T WORK (YET)... problem is that we respect dependencies
# but when different inputs have different orders, we can't delay
# the earlier ones to match the later ones.
#
#
# Notation: sig[i] = signal at ith location of schedule
#           a -> b = signal b depends upon signal a
#           ord[i] = order of ith location of schedule
#
# The idea:
#
# - Start with an unrolled schedule.  This schedule has the property
#    that if a -> b, a will appear before b in the schedule at least once OR
#    a and b are computed in the same dynamic section.
# - Assign orders to the schedule such that:
#    if i < j and sig[i] -> sig[j], ord[i] < ord[j]
#    - dynamic sections are treated as one location in the schedule, with
#      all the dependencies of signals in the section merged.
# - produce a partial ordering of the schedule by sorting based upon the
#   key (ord[i], driver[i])
# - merge adjacent identical keys into parallel sections unless driver is
#   a handler
#
# This can be done fairly cheaply by maintaining a list of the last order
# at which each signal is seen.  The new order for a schedule entry (and
# its associated signals) is the maximum of the order for the signal upon
# which it depends, plus 1.
#
def _LSEsc_block_coalesce2(db,sched):
    i = 0
    ls = len(sched)
    signum = [-1] * len(db.signalList)
    newsched = []

    # discover order statistic for each scheduling item
    
    while (i<ls):
        sig = sched[i][0]
        if sig >=0: # normal signal
            driver = db.signalList[sig].cno
            newval = -1
            # take max of things depended upon...
            for stuff in db.signalList[sig].depson:
                if stuff>=0 and signum[stuff] > newval: newval = signum[stuff]
            newval = newval + 1
            signum[sig] = newval
            newsched.append((newval,driver,sig))
        elif (sig == -1): # dynamic/repeated section
            i = i + 1
            sig = sched[i][0]
            dlist = [] 
            newval = -1
            
            # iterate over dynamic section
            while sig != -2:
                driver = db.signalList[sig].cno
                dlist.append(sig)
                for stuff in db.signalList[sig].depson:
                    if stuff>=0 and signum[stuff] > newval:
                        newval = signum[stuff]
                i = i + 1
                sig = sched[i][0]
                
            newval = newval + 1
            newsched.append((newval+1,-1,dlist))
            for d in dlist: signum[d] = newval

        i = i+1

    # use partial ordering to advantage

    print "\nBefore sort\n%s\n" % newsched
    newsched.sort()
    print "\nAfter sort\n%s\n" % newsched

    # now turn back into schedule
    
    realnewsched = []
    lval = (-1,0)
    clist = []
    newsched.append((-1,0,0)) # sentinel value
    for spi in newsched:
        # for now, do not allow parallel, since we're just using as
        # a pre-processor
        if 0 and spi[0:2]==lval and lval[1]>0: # can coalesce!
            clist.append(spi)
        else:
            if len(clist)==1:
                sig = clist[0][2]
                if type(sig) == types.ListType:
                    realnewsched.append((-1,-len(sig)))
                    realnewsched.extend(map(lambda x:(x,0),sig))
                    realnewsched.append((-2,-len(sig)))
                else:
                    realnewsched.append((sig,0))
            elif len(clist)>0: # make parallel section
                realnewsched.append((clist[0][2],-1))
                realnewsched.extend(map(lambda x:(x[2],-3),clist[1:-1]))
                realnewsched.append((clist[-1][2],-2))
            clist = [spi]
            lval = spi[0:2]

    # for now we're not creating parallel pieces
    if 0: _LSEsc_sort_parallel_pieces(db,realnewsched)
    return realnewsched
    
#
# make the parallel pieces look pretty....
#
def _LSEsc_sort_parallel_pieces(db,sched):
    i = 0
    while (i<len(sched)):
        spi = sched[i]
        if spi[0]>=0 and spi[1] == -1:
            left = i
            right = i+1
            while (sched[right][1] != -2): right = right+1
            alist = map(lambda x,db=db: (db.signalList[x[0]].cno,x),
                        sched[left:right+1])
            alist.sort()
            sched[left:right+1] = (
                [(alist[0][1][0],-1)] +
                map(lambda x:(x[1][0],-3),alist[1:right-left]) +
                [(alist[right-left][1][0],-2)])
            i = right+1
        elif spi[0]==-1:
            left = i+1
            right = i+1
            dc = 1
            while (dc):
                if sched[right][0] == -2: dc = dc - 1
                elif sched[right][0] == -1: dc = dc + 1
                right=right+1
            # sorting the dynamic section is a bad idea when it is nested
            #alist = sched[left:right-1]
            #alist.sort()
            #sched[left:right-1] = alist
            i=right
        else: i = i + 1

##########################################################################
#  Other routines
##########################################################################

#
# Print out a schedule
#
def LSEsc_schedule_print(db,sched):
    for si in sched:
        (itemval,flag) = si
        if itemval== -1: # down 
            if flag >= 0: print "(",
            else: print "{",
        elif itemval == -2: # up
            if flag >= 0: print (")^%d" % flag),
            else: print "}",
        elif itemval == -3: # dot
            print ".",
        elif itemval == -4: # skipped
            if (flag == -1): print "[",
            print "-",
            if (flag == -2): print "]",
        else:
            if (flag == -1): print "[",
            print ("%d/%d" % (itemval,db.signalList[itemval].cno)), 
            if (flag == -2): print "]",
    print ""

