/* 
 * Copyright (c) 2002 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Static scheduler code for the simulator
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This code determines the static schedule for the simulator.  It
 * uses an algorithm derived from Stephen A. Edwards's Berkeley Ph.D. 
 * thesis, "The Specification and Execution of Heterogeneous Synchronous
 * Reactive Systems".
 *
 * The depth-first search and strongly-connected component algorithms
 * are from Corman, Leiserson, and Rivest
 *
 */
/******* Debug printout options ****/
/* #define SHOWSETS */
/* #define SHOWGRAPHS */
/* #define DEBUG_OPTIMIZER */
/* #define DEBUG_UNROLLING */

#include "scheduler.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#define sched_min(x,y) (((x)<(y))?(x):(y))
#define sched_abs(x) (((x)<0)?(-x):(x))
static int num_dfs=0;
static int num_set=0;
#define LARGE_PARTITION_SIZE 16

/*************************************************************************/
/****************** graph ops ********************************************/
/*************************************************************************/

sched_graph *sched_graph_create(int size)
{
  sched_graph *t;
  t = (sched_graph *)calloc(1,sizeof(sched_graph)+5*size*sizeof(int));
  t->num_nodes = size;
  t->num_masked = 0;
  t->max_inedges = 8;
  t->max_outedges = 8;
  t->inedges = (int *)calloc(size*t->max_inedges+1,sizeof(int));
  t->outedges = (int *)calloc(size*t->max_outedges+1,sizeof(int));
  t->num_nodes = size;
  return t;
}

sched_graph *sched_graph_copy(sched_graph *s)
{
  sched_graph *t;
  int size=s->num_nodes;
  t = (sched_graph *)malloc(sizeof(sched_graph)+5*size*sizeof(int));
  memcpy(t,s,sizeof(sched_graph)+3*size*sizeof(int));
  t->inedges = (int *)malloc((size*s->max_inedges+1)*sizeof(int));
  memcpy(t->inedges,s->inedges,(size*s->max_inedges+1)*sizeof(int));
  t->outedges = (int *)malloc((size*s->max_outedges+1)*sizeof(int));
  memcpy(t->outedges,s->outedges,(size*s->max_outedges+1)*sizeof(int));
  return t;
}

void sched_graph_destroy(sched_graph *G)
{
  free(G->outedges);
  free(G->inedges);
  /*  free(G->edges);*/
  free(G);
}

void sched_graph_print(sched_graph *G) {
  int k,i;
  for (k=0;k<G->num_nodes;k++) {
    printf("%d:",k);
    for (i=0;i<G->n[k].outedgecnt;i++) {
      printf(" %d",sched_graph_get_outedge(G,k,i));
    }
    printf("\n");
  }
}

static void sched_graph_dfs_visit(sched_graph *G, sched_set *S,
				  sched_dfs *dfs,
				  int u, int transpose)
{
  int v,i,cnt;
  
  dfs->v[u].marker=(++dfs->time);
  /*fprintf(stderr,"Entering %d %d\n",u,dfs->v[u].marker);*/
  dfs->v[dfs->num_heads].headsize++;
  dfs->v[dfs->v[dfs->num_heads].lastnodeforhead].nextnode=u;
  dfs->v[dfs->num_heads].lastnodeforhead=u;
  dfs->v[u].compno=dfs->num_heads;
  if (!transpose) {
    cnt = G->n[u].outedgecnt;
    for (i=0;i<cnt;i++) {
      v = sched_graph_get_outedge(G,u,i);
      if (v >= 0 && !dfs->v[v].marker && S->elements[v])
	sched_graph_dfs_visit(G,S,dfs,v,transpose);
    }
  } else {
    cnt = G->n[u].inedgecnt;
    for (i=0;i<cnt;i++) {
      v = sched_graph_get_inedge(G,u,i);
      if (v >= 0 && !dfs->v[v].marker && S->elements[v])
	sched_graph_dfs_visit(G,S,dfs,v,transpose);
    }
  }
  dfs->v[u].f= (++(dfs->time));
  dfs->v[dfs->num_on-(++dfs->fcnt)].forder=u;
  /*fprintf(stderr,"Leaving %d, %d/%d\n",u,dfs->v[u].marker,dfs->v[u].f);*/
}

static void inline do_it(sched_graph *G, sched_set *S, 
			 sched_dfs *dfs, sched_dfs *order,
			 int u, int transpose) {
  dfs->v[dfs->num_heads].headsize=0;
  dfs->v[dfs->num_heads].lastnodeforhead=u;
  sched_graph_dfs_visit(G,S,dfs,u,transpose);
  dfs->v[dfs->v[dfs->num_heads].lastnodeforhead].nextnode= -1;
  dfs->v[dfs->num_heads++].heads=u;
}

/* Do a depth-first search of the graph.  Order can be dealt with,
 * as well as transposition of the edges.  Note that the order of
 * the nodes in forder is a topological sort.
 */
static sched_dfs *
sched_graph_dfs(sched_graph *G,sched_set *S, sched_dfs *order, int transpose)
{
  sched_dfs *dfs;
  int i,u;

  assert(S==NULL || G->num_nodes == S->num_nodes);
  assert(order==NULL || S==NULL || S->num_on==order->num_on);

  /* intentionally oversized to make my life easier */
  dfs = (sched_dfs *)malloc(sizeof(sched_dfs)*(G->num_nodes+1));
  num_dfs++;

  if (S==NULL) {
    for (i=0;i<G->num_nodes;i++) dfs->v[i].marker=0;
    dfs->num_on = G->num_nodes;
  }
  else {
    for (i=0;i<S->num_on;i++) {
      u=S->listelem[i];
      dfs->v[u].marker = 0;
    }
    dfs->num_on = S->num_on;
  }
  dfs->time=dfs->fcnt=dfs->num_heads=0;

  if (order==NULL) {
    if (S==NULL) {
      for (u=0;u<G->num_nodes;u++) {
	if (!dfs->v[u].marker) do_it(G,S,dfs,order,u,transpose);
      }
    } else {
      for (i=0;i<S->num_on;i++) {
	u = S->listelem[i];
	if (!dfs->v[u].marker) do_it(G,S,dfs,order,u,transpose);
      }
    }
  } else {
    for (i=0;i<dfs->num_on;i++) {
      u = order->v[i].forder;
      if (!dfs->v[u].marker) do_it(G,S,dfs,order,u,transpose);
    }
  }
  return dfs;
}

static void sched_dfs_destroy(sched_dfs *dfs) 
{
  num_dfs--;
  free(dfs);
}

/* Really fascinating; the sort that is returned classifies the
 * nodes into SCCs (member_of) where the SCCs themselves are
 * topologically sorted by dependencies between them.  Very, very, very
 * cool and convenient.... too bad S.Edwards didn't point this out in
 * the algorithm for COST; it is important.
 */
static sched_dfs *
sched_graph_find_sccs(sched_graph *G, sched_set *S)
{
  sched_dfs *dfsg, *dfsgt;
  dfsg=sched_graph_dfs(G,S,NULL,0);
  dfsgt=sched_graph_dfs(G,S,dfsg,1);
  sched_dfs_destroy(dfsg);
  return dfsgt;
}

/*************************************************************************/
/****************** set ops **********************************************/
/*************************************************************************/

sched_set *sched_set_create(int size)
{
  sched_set *t;
  t = (sched_set *)calloc(1,sizeof(sched_set)+size);
  t->listelem = (int *)malloc(sizeof(int)*size);
  num_set++;
  t->num_nodes = size;
  t->num_on = 0;
  return t;
}

sched_set *sched_set_copy(sched_set *s)
{
  sched_set *t;
  t = (sched_set *)malloc(sizeof(sched_set)+s->num_nodes);
  num_set++;
  memcpy(t,s,sizeof(sched_set)+s->num_nodes);
  t->listelem = (int *)malloc(sizeof(int)*s->num_nodes);
  memcpy(t->listelem,s->listelem,sizeof(int)*s->num_nodes);
  return t;
}

void sched_set_destroy(sched_set *S)
{
  free(S->listelem);
  free(S);
  num_set--;
}

void sched_set_include_all(sched_set *S)
{
  int i;
  memset(&S->elements[0],1,sizeof(char)*S->num_nodes);
  for (i=0;i<S->num_nodes;i++)
    S->listelem[i] = i;
  S->num_on = S->num_nodes;
}

void sched_set_include_none(sched_set *S)
{
  int i;
  if (S->num_on > S->num_nodes/2) {
    memset(&S->elements[0],0,sizeof(char)*S->num_nodes);
  } else {
    for (i=0;i<S->num_on;i++)
      S->elements[S->listelem[i]]=0;
  }
  S->num_on = 0;
}

void sched_set_print(sched_set *S)
{
  int i;
  for (i=0;i<S->num_on;i++) 
    printf("%d ",S->listelem[i]);
}

void sched_piece_add(sched_schedule *R, int i, int flag) {
  int offset = R->numparts;
  R->i[offset].item = i;
  R->i[offset].flag = flag;
  R->numparts++;
}

void sched_set_mark(sched_set *S, sched_schedule *R, int offset, 
		    int flag)
{
  int i,j;
  for (j=0;j<S->num_on;j++) {
    i=S->listelem[j];
    R->i[offset].item = i;
    R->i[offset].flag = flag!= -3 ? flag : j==0 ? -1 : 
      j==S->num_on-1 ? -2 : -3;
    offset++;
  }
}

/* used for lecsim stuff 
 */
void sched_set_mark_funny(sched_set *S, sched_schedule *R, 
			  int offset, int flag)
{
  int i,j;

  i=S->listelem[0];
  R->i[offset].item = i;
  R->i[offset].flag = flag!= -3 ? flag : -1;
  offset++;

  for (j=S->num_on-1;j>0;j--) {
    i=S->listelem[j];
    R->i[offset].item = i;
    R->i[offset].flag = flag!= -3 ? flag : j==1 ? -2 : -3;
    offset++;
  }
}


/*************************************************************************/
/****************** schedule ops *****************************************/
/*************************************************************************/

sched_schedule *sched_schedule_create(int size) 
{
  sched_schedule *t;
  /* needs the x2 + 2 due to punctuation */
  t=(sched_schedule *)malloc(sizeof(sched_schedule)+
			     sizeof(sched_schedule_part)*size*2+2);
  t->numparts=0;
  return t;
}

void sched_schedule_destroy(sched_schedule *R) 
{
  free(R);
}

void
sched_dump(sched_graph *F, sched_schedule *sched) 
{
  int i,j;
  for (i=0;i<sched->numparts;i+=8) {
    printf("I%7d:",i);
    for (j=0; j<8 && i+j < sched->numparts;j++)
      printf("%7d",sched->i[i+j].item);
    printf("\n");
    printf("F%7d:",i);
    for (j=0; j<8 && i+j < sched->numparts;j++)
      printf("%7d",sched->i[i+j].flag);
    printf("\n");
  }
  printf("\n\n");
}


/*************************************************************************/
/****************** HSR scheduling ***************************************/
/*************************************************************************/

/****** This routine is the heart of the algorithm... */

static cost_t
find_static_sched_int(sched_graph *F, sched_set *S, sched_schedule *R,
		      cost_t B, int force_sep, long verylargesize, 
		      long smallsize)
{ 
  int i,ii,j,k,v;
  sched_dfs *dfs;
  sched_set *H,*T,*Sk;
  int newnumparts=0,driver,must_do;
  cost_t r,b,blimit,rb_piece,a,tmp,t;
  sched_schedule *R2,*myR;
  sched_graph *sccsG, *reachG;

  if (S->num_on == 1) {
    sched_set_mark(S,R,R->numparts,0); /* always O(V) */
    R->numparts++;
    return 1;
  }
  
  /* Decompose */

  assert(F->num_nodes == S->num_nodes);
  
  dfs=sched_graph_find_sccs(F,S); /* total O(V^2-3) */
  /* if we couldn't partition this set and the calling partition
   * was non-separable, then the calling partition was clearly non-optimal
   * and should not be pursued further....
   */
  if (dfs->num_heads == 1 && force_sep) {
    sched_dfs_destroy(dfs);
    return SCHED_INFINITE(F->num_nodes);
  }
  Sk = sched_set_create(S->num_nodes);  
  R2 = sched_schedule_create(S->num_nodes);
  myR = sched_schedule_create(S->num_nodes);
  myR->numparts = 0;

  /* Calculate B */
  tmp=0;
  for (k=0;k<dfs->num_heads;k++)
    tmp += (cost_t)dfs->v[k].headsize * (cost_t)dfs->v[k].headsize 
      - dfs->v[k].headsize + 1;
  rb_piece = dfs->num_on;
  
  B=sched_min(B,tmp);             /* bound to be met */
  r=B;                            /* remaining cost */

  /* create component graph */
  sccsG = sched_graph_create(dfs->num_heads);
  /* create reachability graph */
  reachG = sched_graph_create(dfs->num_heads);

  /* iterate across strongly connected components */
  for (k=0;k<dfs->num_heads;k++) {

    sched_set_include_none(Sk);

    dfs->v[k].pos = myR->numparts; /* place where scc starts in schedule */

    for (j=dfs->v[k].heads;j>=0;j=dfs->v[j].nextnode) {
      Sk->elements[j]=1;
      Sk->listelem[Sk->num_on++]=j;
    }

#ifdef SHOWSETS
    printf("%d=",k);
    sched_set_print(Sk);
    printf("\n");
#endif

    rb_piece -= (cost_t)dfs->v[k].headsize;
    b = r - rb_piece;

    if (b < Sk->num_on && force_sep) {
      /* if we are not in a forcing situation, we do not complain
       * about how much it costs....
       */
      sched_dfs_destroy(dfs);
      sched_set_destroy(Sk);  
      sched_schedule_destroy(R2);
      sched_schedule_destroy(myR);
      sched_graph_destroy(reachG);
      sched_graph_destroy(sccsG);
      return SCHED_INFINITE(F->num_nodes);
    }

    if (Sk->num_on == 1) {
      sched_set_mark(Sk,myR,myR->numparts,0);
      myR->numparts++;
      r=r-1;
    } else {
      
      a=SCHED_INFINITE(F->num_nodes);
      must_do = 0;

      /* For really large partitions, do not even bother.... */
      if (Sk->num_on >= verylargesize) goto after_partition;
      
      /* If there is a signal which is due to queries across modules,
       * we do not know how many signals that actually represents.  As
       * a result, we cannot schedule it, so we should give up...
       */
      if (!force_sep) { /* do this only at the top-level... */
	for (ii=0;ii<Sk->num_on;ii++) {
	  i = Sk->listelem[ii];
	  if (F->n[i].badnode) goto after_partition;
	}
      }

      H = sched_set_create(Sk->num_nodes);
      T = sched_set_copy(Sk);
      sched_set_include_none(H);

      while (1) { /* iterate over heads */

	/* create head and tail */

	if (Sk->num_on >= smallsize) {
	  /* if large, try to partition using a simple heuristic:
	   * pick a module and find its border.  This guarantees
	   * that the tail will be separable, but the number of
	   * iterations of this partition may be huge.  If it is
	   * too huge, we'll dynamically schedule it later.  It
	   * may also be possible to reduce the number of iterations
	   * by calculating number of crossing signals between head
	   * and tail and scheduling the head.
	   *
	   */
	  if (must_do) break; /* already tried */
	  driver=F->n[Sk->listelem[0]].driver;
	  for (ii=0;ii<Sk->num_on;ii++) {
	    i = Sk->listelem[ii];
	    if (F->n[i].driver == driver) {
	      for (j=0;j<F->n[i].outedgecnt;j++) {
		v=sched_graph_get_outedge(F,i,j);
		if (Sk->elements[v] && F->n[v].driver != driver &&
		    !H->elements[v]) {
		  H->elements[v]=1;
		  T->elements[v]=0;
		}
	      }
	    }
	  }
	  H->num_on = T->num_on = 0;
	  for (ii=0;ii<Sk->num_on;ii++) {
	    i = Sk->listelem[ii];
	    if (H->elements[i]) H->listelem[H->num_on++]=i;
	    else T->listelem[T->num_on++]=i;
	  }

	  /* limit the number of iterations by limiting the head size... */
	  // if (H->num_on > LARGE_PARTITION_SIZE-1) break;

#ifdef SHOWDIV
	  printf("Making division\nH= ");
	  sched_set_print(H);
	  printf("\nT= ");
	  sched_set_print(T);
	  printf("\n");
#endif
	  /* if no division made, break */
	  must_do = 1;
	  if (H->num_on == 0 || T->num_on == 0) break; 
	} else {
	  /* search if small */
	  j = 0;
	  H->num_on=0;
	  T->num_on=0;
	  for (j=0;j<Sk->num_on;j++) {
	    i=Sk->listelem[j];
	    if (!H->elements[i]) {
	      /* flip from out to in */
	      H->elements[i]=1;
	      T->elements[i]=0;
	      break;
	    } else {
	      /* flip from out to in */
	      T->elements[i]=1;
	      H->elements[i]=0;
	    }
	  }
	  H->num_on=0;
	  T->num_on=0;
	  for (j=0;j<Sk->num_on;j++) {
	    i = Sk->listelem[j];
	    if (H->elements[i]) H->listelem[H->num_on++]=i;
	    else T->listelem[T->num_on++]=i;
	  }
	  if (H->num_on==Sk->num_on) break; /* all in head--done */
	}

	/* calculate division using optimal tail cost.  However, when
	 * I am going to accept a non-optimal, I need to make it possible
	 * to get less optimal solutions, so the bound must be relaxed.
	 */
	blimit = must_do ? (cost_t)T->num_on * (cost_t)T->num_on 
	  : (b-(cost_t)H->num_on*(cost_t)H->num_on)/(H->num_on+1);
	R2->numparts=0;
	t=find_static_sched_int(F,T,R2,blimit,1,verylargesize,smallsize);
	tmp = (cost_t)H->num_on*(cost_t)H->num_on + (H->num_on + 1)*t;
	/*
	printf(" %d %d %d %d %d %d\n",Sk->num_on,H->num_on,b,blimit,t,tmp);
	*/
	if (tmp<=b) { /* if solution const is better than what we're looking 
		       * for after best previous
		       */
	  /* head is parallel */
	  myR->i[myR->numparts].item = -1; /* down */
	  myR->i[myR->numparts].flag = H->num_on;
	  myR->i[myR->numparts+H->num_on+1].item = -3; /* dot */
	  myR->i[myR->numparts+H->num_on+1].flag = H->num_on;
	  myR->i[myR->numparts+H->num_on+2+R2->numparts].item = -2; /* up */
	  myR->i[myR->numparts+H->num_on+2+R2->numparts].flag = H->num_on;
	  sched_set_mark(H,myR,myR->numparts+1,H->num_on>1 ? -3 : 0);
	  memcpy(myR->i+myR->numparts+H->num_on+2,
		 R2->i,
		 sizeof(sched_schedule_part)*R2->numparts);
	  newnumparts = myR->numparts+H->num_on+3+R2->numparts;
	  a = sched_min(a,tmp); /* new best cost is this... */
	  b = a-1; /* and new outer limit of what to accept */
	}
	/*	if (Sk->num_on > PARTITION_SIZE) break; */
      } /* foreach head of Sk */

      sched_set_destroy(H);
      sched_set_destroy(T);

    after_partition:

      if (a==SCHED_INFINITE(F->num_nodes)) {
	myR->i[myR->numparts].item = -1; /* down */
	myR->i[myR->numparts].flag = -Sk->num_on;
	myR->i[myR->numparts+Sk->num_on+1].item = -3; /* dot */
	myR->i[myR->numparts+Sk->num_on+1].flag = -Sk->num_on;
	myR->i[myR->numparts+Sk->num_on+2].item = -2; /* up */
	myR->i[myR->numparts+Sk->num_on+2].flag = -Sk->num_on;
	sched_set_mark(Sk,myR,myR->numparts+1,0); /* dynamic/repeated */
	myR->numparts += Sk->num_on+3;
	/* We only get here if the schedule was not able to be formed.
	 * In such a case, we might want to say "don't schedule if you
	 * cannot", but in fact, we got here because the previous was
	 * too big, so we want to schedule no matter what.  As a result,
	 * we claim minimum cost so that later partitions are able to
	 * continue to be generated...
	 */
	if (must_do || !force_sep) a=sched_min(a,Sk->num_on);
	else a=sched_min(a,(cost_t)Sk->num_on*(cost_t)Sk->num_on);
      } else {
	myR->numparts = newnumparts;
	/* 
	 * do not know real cost, but do not want to make later partitions
	 * fall apart when we did not partition optimally
	 */
	if (must_do) a=sched_min(a,Sk->num_on);
      }
      r = r - a;
    } /* else Sk->num_on == 1 */
  } /* decomposition pieces */

  /* insert this piece of schedule if we actually got a result. */
  if (r>=0) {

    /* place where last scc ends + 1 */
    dfs->v[dfs->num_heads].pos = myR->numparts; 

    memcpy(R->i+R->numparts,
	   myR->i+dfs->v[0].pos,
	   sizeof(sched_schedule_part)*dfs->v[dfs->num_heads].pos);
    R->numparts += dfs->v[dfs->num_heads].pos;

    sched_schedule_destroy(myR);
    sched_schedule_destroy(R2);
    sched_dfs_destroy(dfs);
    sched_set_destroy(Sk);
    sched_graph_destroy(sccsG);
    sched_graph_destroy(reachG);

    return B-r;
  }
  else {
    sched_schedule_destroy(myR);
    sched_schedule_destroy(R2);
    sched_dfs_destroy(dfs);
    sched_set_destroy(Sk);
    sched_graph_destroy(sccsG);
    sched_graph_destroy(reachG);
    return SCHED_INFINITE(F->num_nodes);
  }
} /* find_static_sched_int */

/**** Note: will destroy F ***/
cost_t
find_static_schedule(sched_graph *F, sched_set *S, sched_schedule *R,
		     cost_t B, int force_sep,
		     long verylargesize, long smallsize)
{ 
  return(find_static_sched_int(F,S,R,B,force_sep,verylargesize,smallsize));
}


/*************************************************************************/
/****************** Edwards optimization *********************************/
/*************************************************************************/

void
sched_edwards_optimize(sched_graph *F, sched_schedule *R)
{
  int signalno, flag, startpar=0;
  int i,j,earliest,earliestnonpar,driver,level,tailloc;

  /* SE's thesis indicates that this can be done on a per-signal basis.
   * Unfortunately, this does not yield the best schedules.  The reason
   * is that a signal with a low index may be stopped from moving to
   * the left in the schedule by a signal which later moves further to
   * the left, essentially "getting out of the way".
   * 
   * The solution is fairly simple: instead of going by signalno, go by
   * location in schedule.  Thus the left-most part of the schedule always
   * has things in "best" order so far.  This still has a difficulty for
   * heads: they may get blocked in the tails, so we need to do things in
   * a funny order: tails, then heads.
   *
   * Note that it would be possible to do the optimization as the
   * schedule is built, which might turn out to be better....
   *
   * And it's even better to do the optimization after unrolling with
   * some resorting allowed (but not much better!)
   *
   * Total time for this is O(V^2) with an adjacency matrix.  With an
   * adjacency list, it is O(V^2 E).
   */
  for (i=0;i<R->numparts;i++) {
    signalno = R->i[i].item;
    if (signalno == -1) { /* skip head */
      ++i;
      level=0;
      while(1) {
	signalno = R->i[i].item;
	if (signalno == -1) level++;
	else if (signalno == -2) level--;
	else if (signalno == -3 && !level) break; 
	i++;
      }
      continue; /* found matching . */
    } else if (signalno == -2) { /* at end of tail, move to head */
      level=0;
      i--;
      while(1) {
	signalno = R->i[i].item;
	if (signalno == -2) level++;
	else if (signalno == -1) {
	  if (!level) break;
	  else level--;
        }
	i--;
      }
      continue; /* found matching left paren */
    } else if (signalno == -3) { /* at end of head, skip tail */
      ++i;
      level=0;
      while(1) {
	signalno = R->i[i].item;
	if (signalno == -1) level++;
	else if (signalno == -2) {
	  if (!level) break;
	  else level--;
	}
	i++;
      }
      continue; /* found matching right paren */	
    }
    flag = R->i[i].flag;
    driver = F->n[signalno].driver;
    if (flag == -1) startpar = i; /* keep track of closest // section */

    /* perform scan */
    earliest=earliestnonpar=R->numparts;
    if (flag<0) { /* if in a parallel section, start before it */
      j = startpar-1;
    } else {
      j = i-1;
    }
    level =0;
    while (j>=0) {
      switch (R->i[j].item) {
      case -1: 
	if (level==0) { /* we were in the head; 
			   change j to the end of tail */
	  j=i+1;
	  while (1) {
	    switch (R->i[j].item) {
	    case -1: level++; break;
	    case -2: 
	      if (level==0) {
		tailloc = j;
		goto foundtail;
	      }
	      level--;
	      break;
	    }
	    j++;
	  }
	} else {
	  level--;
	}
      foundtail:
	break;
      case -3:  /* reached beginning of tail */
	if (level == 0) goto done;
	break;
      case -2:  /* up */
	level++;
	  break;
      default:
	if (sched_graph_has_edge(F,R->i[j].item,signalno)) goto done;
	if (F->n[R->i[j].item].driver == driver) {
	  earliest=j;
	  if (R->i[j].flag>=-1) earliestnonpar = j;
	}
      }
      j--;
    } /* while (j>=0) */
  done:
    
#ifdef DEBUG_OPTIMIZER
    printf("s=%d i=%d j=%d e=%d enp=%d\n",signalno,i,j,
	   earliest,earliestnonpar);
#endif
    /* Invariants: i is at left of leftmost point available.
     * earliest is at the closest signal from the same module to be movable...
     */
    /* check whether stuck inside parallel/dyn. */
    if (earliest < R->numparts && R->i[earliest].flag <= -2) {
      earliest = earliestnonpar;
    }
    if (earliest < R->numparts) { /* we have something to do... */
      if (i-earliest == 1) { /* already at same place.. */
	/* must be left-most already; we don't move into the middle */
	R->i[earliest].flag = -1;
	/* make certain we don't mess up things inside... */
	if (R->i[i].flag != -3) R->i[i].flag = -2;
      }
      else if (earliest<i) { /* need to move forward */
	/* clear up flags at end */
	if (R->i[i].flag == -1) {
	  R->i[i+1].flag = R->i[i+1].flag== -2 ? 0 : -1;
	} else if (R->i[i].flag == -2) {
	  R->i[i-1].flag = R->i[i-1].flag== -1 ? 0 : -2;
	}
	for (j=i;j>=earliest;j--) 
	  R->i[j] = R->i[j-1];
	R->i[earliest].item = signalno;
	R->i[earliest].flag = -1;
	R->i[earliest+1].flag = R->i[earliest+1].flag == -1 ? -3 : -2;
      } else { /* moving from head to tail */
	/* clear up flags at end */
	if (R->i[i].flag == -1) {
	  R->i[i+1].flag = R->i[i+1].flag== -2 ? 0 : -1;
	} else if (R->i[i].flag == -2) {
	  R->i[i-1].flag = R->i[i-1].flag== -1 ? 0 : -2;
	}
	for (j=i;j<earliest-1;j++) 
	  R->i[j] = R->i[j+1];
	R->i[earliest-1].item = signalno;
	R->i[earliest-1].flag = -1;
	R->i[earliest].flag = R->i[earliest].flag == -1 ? -3 : -2;
	i--; /* since we moved forward */
	}
#ifdef DEBUG_OPTIMIZER
      sched_dump(F,R);
      sched_print_sched(F,R);
#endif
    } /* we have something to do */
  } /* for i */

  /* sort parallel sections */
#ifdef DEBUG_OPTIMIZER
      sched_dump(F,R);
      sched_print_sched(F,R);
#endif

  for (i=0;i<R->numparts;i++) {
    signalno = R->i[i].item;
    if (signalno < 0) continue;
    flag = R->i[i].flag;
    driver = F->n[signalno].driver;
    if (flag == -1) startpar = i; /* keep track of closest // section */
    if (flag < 0) { /* sort parallel sections */
      for (earliest = i-1; 
	   earliest>=startpar && (driver < F->n[R->i[earliest].item].driver ||
			   (driver == F->n[R->i[earliest].item].driver &&
			   signalno < R->i[earliest].item));
	   earliest--);
      for (j=i;j>earliest+1;j--) 
	R->i[j].item  = R->i[j-1].item;
      R->i[j].item = signalno;
    }
  } /* for i */
  return;
}

/*************************************************************************/
/******************* Schedule unrolling **********************************/
/*************************************************************************/

/*************************************************************************
 *  Unroll the schedule.
 *
 *  The biggest concern here is whether the unrolled schedule will be too
 *  big; with complete static scheduling, the worst-case could be V^2. 
 *  Our drop into dynamic scheduling does not bound the number of times a
 *  node can be repeated (at least not that I can tell)
 *
 *  So, the unrolling step must turn large-cost repetitions into dynamic
 *  sections; the easiest way of doing this is to establish a size limit for
 *  things inside repeated sections.  However, this can cause large sections
 *  with a repetition count of 1 or 2 to turn into dynamic sections, which is
 *  probably not good.  But, just checking the repetition count doesn't work
 *  either, as that doesn't catch nesting of repeated sections which adds up
 *  to V^2.  So, we have to switch to dynamic when:
 *     larger than limit and (rep count > 2 or recursion level > 2).  This
 *     limits the total times a current signal can appear to 2*2 * limit...
 *
 */

static int
sched_unroll_size(sched_schedule *R, int start, int depth, 
		  long unrolledsize, int *numelementsp) {
  int i, numiter;
  int headsize, tailsize;
  int numelements = 0;

  for (i=start;i<R->numparts;i++) {
    if (R->i[i].item == -1) { /* down */
      numiter = R->i[i].flag;
      headsize = tailsize = 0;
      if (numiter < 0) { /* dynamic section */
	i += 1; /* skip over down */
	i = sched_unroll_size(R,i,depth+1,unrolledsize,&headsize);

	i++; /* skip dot */
	i = sched_unroll_size(R,i,depth+1,unrolledsize,&tailsize);

#ifdef DEBUG_UNROLLING
	fprintf(stderr,"Dynamic is %d\n", headsize + tailsize);
#endif
	numelements += 2 + headsize + tailsize; /* no dots in unrolled */
      } else {
	i++; /* skip down */
	i = sched_unroll_size(R,i,depth+1,unrolledsize,&headsize);
#ifdef DEBUG_UNROLLING
	fprintf(stderr,"Head is %d\n", headsize);
#endif

	i++; /* skip dot */
	i = sched_unroll_size(R,i,depth+1,unrolledsize,&tailsize);
#ifdef DEBUG_UNROLLING
	fprintf(stderr,"Tail is %d\n", tailsize);
#endif

	if ( ((tailsize * (numiter+1) + headsize * numiter) > unrolledsize) &&
	     (numiter > 2 /*|| depth > 2*/)) {
#ifdef DEBUG_UNROLLING
	  fprintf(stderr,"Turning into dynamic section %d\n",
		  headsize+tailsize);
#endif
	  /* turn into a dynamic section.... */
	  numelements += 2 + headsize + tailsize; /* no dots in unrolled */
	} else numelements += tailsize * (numiter+1) + headsize * numiter;
      }
    } else if (R->i[i].item < 0) break;
    else numelements++;
  }
  *numelementsp = numelements;
  return i;
}

static int
do_unroll(sched_schedule *R, 
	  sched_schedule *U,
	  int i, int depth, long unrolledsize) {
  sched_schedule_part sitem, sitem2;

  while (i < R->numparts) {
    sitem = R->i[i++];
    switch (sitem.item) {
    case -1 : /* down */
      if (sitem.flag < 0) { /* dynamic */
	U->i[U->numparts++] = sitem;     /* copy down */
	i = do_unroll(R,U,i,depth+1, unrolledsize); /* dummy head */
	if (R->i[i-1].item == -3)
	  i = do_unroll(R,U,i,depth+1, unrolledsize); /* dummy tail */
	U->i[U->numparts++] = R->i[i-1]; /* copy up */
      } else {
	int dotloc=i, saveparts, tlen, hlen,k, endofit;

	saveparts = U->numparts;
	while (R->i[dotloc].item != -3) dotloc++; /* find dot */

	/* put in tail first, then head */
	endofit = do_unroll(R,U,dotloc+1,depth+1, unrolledsize);
	tlen = U->numparts - saveparts;
	do_unroll(R,U,i,depth+1,unrolledsize)/* - (i+1) DAP: what was this about? */;
	i = endofit;
	hlen = U->numparts - saveparts - tlen;
#ifdef DEBUG_UNROLLING
	fprintf(stderr,"hlen=%d tlen=%d\n", hlen,tlen);
#endif

	if (((tlen * (sitem.flag + 1) + hlen * sitem.flag) > unrolledsize) &&
	    (sitem.flag > 2 /*|| depth > 2*/)) {
	  /* turn into a dynamic section */
	  
	  int cnt=0;
	  
	  /* unroll and flatten.... */
	  /* we use the sitem2 stuff because we might need to move the things
	   * later instead of earlier...
	   */
	  sitem = U->i[saveparts];
	  
	  for (k=1;k<tlen+hlen;k++) {
	    sitem2 = U->i[saveparts+k];
	    if (sitem.item >= 0) {
	      U->i[saveparts+(++cnt)] = sitem;
#ifdef DEBUG_UNROLLING
	      fprintf(stderr,"Copying %d to %d %d/%d\n", 
		      saveparts+k-1,saveparts+cnt,sitem.item,sitem.flag);
#endif
	    }
	    sitem = sitem2;
	  }
	  if (sitem.item >= 0) {
	    U->i[saveparts+(++cnt)] = sitem;
#ifdef DEBUG_UNROLLING
	    fprintf(stderr,"Copying %d to %d %d/%d\n", saveparts+k-1,
		    saveparts+cnt,sitem.item,sitem.flag);
#endif
	  }
	  U->i[saveparts].item = -1;
	  U->i[saveparts].flag = -cnt;
	  U->i[saveparts+cnt+1].item = -2;
	  U->i[saveparts+cnt+1].flag = -cnt;
	  U->numparts = saveparts+cnt+2;
#ifdef DEBUG_UNROLLING
	  fprintf(stderr,"cnt=%d\n", cnt);
#endif
	} else { /* make additional copies... */
	  int j, k;

	  for (j = 0; j<sitem.flag;j++) {
	    if (j>0) { /* head copy */
	      for (k=0;k<hlen;k++) {
		U->i[U->numparts++] = U->i[saveparts+k+tlen];
	      }
	    }
	    for (k=0;k<tlen;k++) { /* tail copy */
	      U->i[U->numparts++] = U->i[saveparts+k];
	    }
	  }
	}
#ifdef DEBUG_UNROLLING
	fprintf(stderr,"i after=%d\n", i);
#endif
      } /* dynamic vs. repeated */
      break;
    case -2 : /* up */
    case -3 : /* dot */
      return i;
    default:
#ifdef DEBUG_UNROLLING
      fprintf(stderr,"Setting %d to %d/%d\n", U->numparts,
	      sitem.item,sitem.flag);
#endif
      U->i[U->numparts++] = sitem;
    } /* switch sitem.item */
  } /* while i */
  return i;
}

sched_schedule *
sched_unroll(sched_schedule *R, sched_graph *G, long unrolledsize) {
  int sz;
  sched_schedule *U;

  sched_unroll_size(R,0,1,unrolledsize,&sz);
#ifdef DEBUG_UNROLLING
  fprintf(stderr,"Unroll size is %d\n",sz);
#endif

  U = sched_schedule_create(sz);
  
  do_unroll(R,U,0,1,unrolledsize);

#ifdef DEBUG_UNROLLING
  printf("After unrolling\n");
  sched_dump(G,U);
  sched_print_sched(G,U);
#endif

  /* now drop unneeded signals, making certain not to mess up
   * parallel sections.... 
   */

  {
    int needsstart = 0;
    int lastgood = 0;
    int count = 0;
    int offset = 0;
    int i;
    int dropflag;
    sched_schedule_part spi;

    for (i=0;i<U->numparts;i++) {
      spi = U->i[i];
      U->i[i-offset] = spi;
      if (spi.item < 0) continue; /* copy in dynamic markers */

      dropflag = G->n[spi.item].dropped;
      if (dropflag) offset ++; /* skip if dropped */

      if (spi.flag >= 0) continue; /* only do following logic if parallel */

      if (!dropflag) {  /* count what's in the parallel section */
	count ++;
	lastgood = i-offset;
      }
	
      if (spi.flag == -1)        /* if a start, mark whether we dropped it */
	needsstart = dropflag;
      else if (spi.flag == -2) {   /* if an end... */
	if (count > 1) 
	  U->i[lastgood].flag = -2;  /* make last good one the end */
	else if (count == 1)
	  U->i[lastgood].flag = 0;   /* drop the whole parallel thing */
	needsstart = 0;
	count = 0;
      }
      else 
	if (needsstart && !dropflag) {  /* otherwise, a middle; if need to 
					   start and not dropping, start */
	  U->i[i-offset].flag = -1;
	  needsstart = 0;
	}
    } /* for */
    
    U->numparts = i - offset;
  }

#ifdef DEBUG_UNROLLING
  printf("After dropping\n");
  sched_dump(G,U);
  sched_print_sched(G,U);
#endif

  return U;
}

/*************************************************************************/
/****************** LECSIM scheduling ************************************/
/*************************************************************************/
/* 
 * This code schedules the signal graph for LECSIM in the sense that it
 * puts the signals in a topological order with some back edges removed
 * and makes some SCCs.  The actual levelization occurs later.  So the
 * real point of this is just to remove edges until all the SCCs are 
 * small and then report the SCCs.  How to remove the edges is not
 * made clear in the LECSIM paper.  I am doing it by removing the "longest"
 * back edge where longest is the greatest difference in finish time.  
 * However, since for independent SCCs I can do this independently, the
 * easiest way to handle it will be to start with the last node finished
 * (i.e. first node started) in each large SCC I see and remove their
 * longest edges (which will be the longest in the SCCs).
 * 
 */
/**** Note: will destroy F ***/
cost_t
find_lecsim_schedule_int(sched_graph *F, sched_set *S, sched_schedule *R,
			 int canfree, int dosubs)
{ 
  int k,j;
  sched_dfs *dfs, *dfsg;
  int cost;
  int effective_size = dosubs ? LARGE_PARTITION_SIZE : 2;

  
  if (S->num_on == 1) {
    sched_set_mark(S,R,R->numparts,0); /* always 0(V) */
    R->numparts++;
    if (canfree) sched_set_destroy(S);
    return 1;
  }
  
  /* double-check */

  assert(F->num_nodes == S->num_nodes);

  /* Do the initial depth-first search */
  dfsg = sched_graph_dfs(F,S,NULL,0);

  /* remove back edges until no SCC is "large" */

  do {
    int didsomething = 0;

    // Find the SCCs
    dfs = sched_graph_dfs(F,S,dfsg,1);
    
    /* are any large? */
    for (k=0;k<dfs->num_heads;k++) {
      if (dfs->v[k].headsize >= effective_size) {
	/* destroy a back edge from this SCC and move on */
	int u, num = -1, diff =0, ncost;

	/* the first node visited in an SCC in the 2nd DFS is the last node
	 * finished in the 1st DFS.  
	 */
	u = dfs->v[k].heads;
	ncost = dfsg->v[u].f;

	/* find largest cost across valid inedges */
	for (j=0 ; j < F->n[u].inedgecnt ; j++) {
	  int ndiff, v;
	  v = sched_graph_get_inedge(F,u,j);

	  if (v >= 0 && S->elements[v]) {
	    ndiff = ncost - dfsg->v[v].f;
	    if (ndiff > diff) {
	      num = j;
	      diff = ndiff;
	    }
	  }
	}

	/* only inedges need to be squashed because they are the only ones
	 * which are used in the 2nd DFS */
	if (num >= 0) {
	   sched_graph_squash_inedge(F,u,num);
	   didsomething = 1;
	}

      } /* if large partition */
    } /* for k in dfs heads */

    /* no large SCCs, we can move on to scheduling */
    if (!didsomething) break;

    /* and remove the old SCC before trying again */
    sched_dfs_destroy(dfs);

  } while (1);

  if (canfree) sched_set_destroy(S);

  /* iterate across strongly connected components */
  cost = 0;

  for (k=0;k<dfs->num_heads;k++) {

    /* remember where head started in schedule */
    dfs->v[k].pos = R->numparts;


    if (dfs->v[k].headsize == 1) {

      /* handle single node SCCs */
      sched_piece_add(R,dfs->v[k].heads,0);
      cost += 1;

    } else {
      int i, maxf, minf, loc=0;

      /* create an embedded SCC */

      sched_piece_add(R,-1,-dfs->v[k].headsize); /* down */

      /* put out in the reverse finish order for the original DFS; this is 
       * a good topo order when back edges are removed.  Later on we'll
       * put into tlevel order...
       */
      maxf = dfsg->v[dfs->v[k].heads].f + 1;
      for (i=0;i<dfs->v[k].headsize;i++) {
	minf = 0;
	for (j=dfs->v[k].heads;j>=0;j=dfs->v[j].nextnode) {
	  if (dfsg->v[j].f > minf && dfsg->v[j].f < maxf) {
	    minf = dfsg->v[j].f;
	    loc = j;
	  }
	}
	maxf = minf;
	sched_piece_add(R, loc, 0);
      } /* for i */

      sched_piece_add(R,-3,-dfs->v[k].headsize); /* dot */
      sched_piece_add(R,-2,-dfs->v[k].headsize); /* up */
      
      cost += dfs->v[k].headsize;
    }

  } /* for sccs in graph */

  sched_dfs_destroy(dfsg);
  sched_dfs_destroy(dfs);

  return cost;

}

cost_t
find_lecsim_schedule(sched_graph *F, sched_set *S, sched_schedule *R, int
		     dosubs)
{ 
  return find_lecsim_schedule_int(F,S,R,0,dosubs);
}

/*************************************************************************/
/****************** Acyclic scheduling ***********************************/
/*************************************************************************/
/* 
 * This code determines the schedule for Acyclic scheduling.  The code
 * tries to follow the methods used in FastSysC as closely as possible; 
 * this implies that the use of graph analysis techniques is not particularly
 * sophisticated.  Comments indicate where this is so.  However, I have
 * improved cycle breaking where necessary.
 * 
 */
/**** Note: could remove loads of edges from F ***/

static void break_cycles_visit(sched_graph *G, sched_set *S, int *markers, 
			       int u) 
{
  int i,cnt,v;
  markers[u] = 1;
  cnt = G->n[u].outedgecnt;
  for (i=0; i < cnt ; i++) {
    v = sched_graph_get_outedge(G,u,i);
    if (v < 0 || !S->elements[v]) continue;
    if (!markers[v]) break_cycles_visit(G,S,markers,v);
    else sched_graph_squash_outedge(G,u,i);
  }
}

static void break_cycles_visit2(sched_graph *G, sched_set *S, int *markers, 
				int u) 
{
  int i,cnt,v;
  markers[u] = 1;
  cnt = G->n[u].outedgecnt;
  for (i=0; i < cnt ; i++) {
    v = sched_graph_get_outedge(G,u,i);
    if (v < 0 || !S->elements[v]) continue;
    if (!markers[v]) break_cycles_visit2(G,S,markers,v);
    else if (markers[v]==1) sched_graph_squash_outedge(G,u,i);
  }
  markers[u] = 2;
}

cost_t
find_acyclic_schedule(sched_graph *F, sched_set *S, sched_schedule *R, 
		      int do_improved, int dodebug)
{ 
  int i, rank, do_rank, j, k, cnt, v;
  sched_dfs *dfs, *dfsg;
  int *markers;

  if (S->num_on == 1) {
    sched_set_mark(S,R,R->numparts,0);
    R->numparts++;
    return 1;
  }

  /* Step 1: check for cycles by doing SCC.  This is not really an efficient
   * way of doing so if all you want is a yes/no answer.  (A simple dfs 
   * will do; you just look at whether a target node of an outedge has been
   * started and finished yet.  If started and not finished, you've got a 
   * cycle; if both started and finished, you're looking at a previously seen
   * dfs tree which could be a sub-tree of the current one.
   */

  dfsg = sched_graph_dfs(F, S, NULL, 0);
  dfs = sched_graph_dfs(F, S, dfsg, 1);

  /* Step 2: break up cycles.  This is done in a depth-first fashion,
   * but does not distinguish between true back edges and dfs
   * sub-trees.  As a result, it removes far more arcs than is
   * necessary.  Actually, this step is totally unnecessary if the
   * topological were coded to ignore back edges or the SCC graph was just
   * used directly!
   */
  if (dfs->num_heads != S->num_on) { /* cycle present (pigeonhole principle) */
    if (dodebug) {
      printf("Breaking cycles\n");
    }

    if (do_improved) {

      /* improved version that only squashes back edges */
 #ifdef OLD_BREAK
      markers = calloc(sizeof(int),F->num_nodes);

      for (k=0; k < dfs->num_heads; k++) {
	
	for (j=dfs->v[k].heads; j >= 0; j=dfs->v[j].nextnode) {
	  
	  if (!markers[j] && S->elements[j]) {
	    break_cycles_visit2(F,S,markers,j);
	  }
	}
      }

      free(markers);
#else

      /* squash all back edges */

      for (i=0 ; i < F->num_nodes; i++) {
	int fcost, v;
	if (!S->elements[i]) continue;
	
	fcost = dfsg->v[i].f;

	for (j=0 ; j < F->n[i].outedgecnt ; j++) {
	  v = sched_graph_get_outedge(F, i, j);
	  if (v >= 0 && S->elements[v] &&
	      dfsg->v[v].f > fcost) {
	    sched_graph_squash_outedge(F, i, j);
	  }
	}
      }

#endif

    } else {

      /* original version that squashes too many edges */

      markers = calloc(sizeof(int),F->num_nodes);
      
      for (i=0 ; i < F->num_nodes; i++) {
	if (!markers[i] && S->elements[i]) {
	  break_cycles_visit(F,S,markers,i);
	}
      }
      
      free(markers);
    }
  }

  sched_dfs_destroy(dfsg); /* do not need the SCC graph any more */
  sched_dfs_destroy(dfs); /* do not need the SCC graph any more */

  /* graph is now acyclic */

  /* Step 3: Put signals in topological order 
   * This could have been gotten straight from the SCC graph, but FastSysC
   * actually does its own stuff here...
   */

  markers = calloc(sizeof(int),F->num_nodes); /* FastSysC number array */
  rank = -1;
  do {
    do_rank = 0;
    rank++;
    for (i=0 ; i < F->num_nodes; i++) {
      if (!S->elements[i]) continue;
      if (markers[i] == rank) {
	cnt = F->n[i].outedgecnt;
	for (j=0; j < cnt ; j++) {
	  v = sched_graph_get_outedge(F,i,j);
	  if (v < 0) continue;
	  markers[v] = rank+1;
	  do_rank = 1;
	}
      }
    }
  } while (do_rank);

  /* markers now contains the rank of each node */

  /* if we do a stable sort by rank we'll get the topological order, but
   * it does not need to be realized quite yet...
   */

  /* Step 4: form the topological order and do some coalescing.
   */
  for (k = 0; k < rank; k++) {
    int rankstart = R->numparts;
    int rankend;

    /* get the nodes for this rank into the list */
    for (i=0 ; i < F->num_nodes; i++) {
      if (S->elements[i] && markers[i] == k) {
	R->i[R->numparts].item = i;
	R->i[R->numparts++].flag = 0;
      }
    }

    rankend = R->numparts;

    /* now try to coalesce them using a selection sort; very inefficient */
    i = rankstart;
    while (i < rankend) {
      int driver = F->n[R->i[i].item].driver;
      int lastfound = i;
      for (j = i+1; j < rankend; j++) {
	if (F->n[R->i[j].item].driver == driver) {
	  if (j > lastfound + 1) {
	    int t = R->i[j].item;
	    memmove(&R->i[lastfound+2],&R->i[lastfound+1],
		    sizeof(R->i[0])*(j-lastfound-1));
	    R->i[++lastfound].item = t;
	  } else {
	    lastfound++;
	  }
	}
      } /* for j = i+1 to end of rank */

      /* make parallel section, but not for cno = 0 */

      if (lastfound != i && driver) {
	for (j = i; j <= lastfound; j++) {
	  R->i[j].flag = -3;
	}
	R->i[i].flag = -1;
	R->i[lastfound].flag = -2;
      }
      i = lastfound + 1;
    } /* while i < rankend */
  } /* for k in rank */

  free(markers);

  /* and R is now complete */

  return R->numparts;

}

