/* 
 * Copyright (c) 2000-2007 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Template for domain-dependent types
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file is processed to become SIM_domain_types.h.  It contains all the
 * constants and types from domains in their fully-uniquified form
 *
 * TODO: create embedded type name things... (like function pointers)
 *
 */
m4_include(SIM_quotes.m4)
LSEm4_push_diversion(-1)
m4_include(SIM_database.m4)
LSEm4_pop_diversion()
#ifndef _LSE_DOMAIN_TYPES_H_
#define _LSE_DOMAIN_TYPES_H_

/* Must already have included SIM_types.h (which gives stdlib, SIM_config.h),
 * SIM_refcount.h, SIM_time.h
 */

/******* Define domain class/instance headers, constants and types ****/

m4_pythonfile(
#
# set up the code generation environment and tokenizer for translation
# 
myenv = LSEcg_Environment()
myenv.db = LSE_db
myenv.inst = None
myenv.codeloc = LSEcg_in_domaindef
myenv.reporter = LSEcg_report_error
myenv.name = None
#
# and ask for the types to be generated...
#
LSEcg_domain_generate_ids(LSE_db,[LSEap_APIs],myenv,0)
)m4_dnl

/************** dynids and resolutions ***************/

m4_pythonfile(

# Here we need to translate as we go along.  Rather than do them 
# one-by-one at the top level here, we use the "file interface" abilities
# of the tokenizer to translate as we go along....
myenv.codeloc = LSEcg_in_def
tok = LSEcg_get_domain_tokenizer(LSE_db,[LSEap_APIs],myenv)
domainres = tok.startInput([],sys.stdout) # restart the tokenizer

for estruc in ["dynid_t", "resolution_t"]:
    print "typedef struct LSEfw_%s_attr_s {" % estruc
    foundone = 0

    for domc in LSE_db.domainOrder:
        dom = domc[0]
	sp = dom.classAttributes.get("LSE_" + estruc)
	if sp:
            LSEcg_domain_set_rc(LSE_db,dom.searchPath)
	    loc = "in attributes for '%s' '%s'" % (dom.className,estruc) 
            myenv.name = dom
            tok.pythonEnv["LSE_domain_class"] = dom
            tok.pythonEnv["LSE_domain_inst"] = None
	    tok.processInput("%s LSE_domain_class_name;\n" % sp,loc,[],sys.stdout)
 	    foundone = 1

    for dom in LSE_db.domainInstanceOrder:
        sp = dom.instAttributes.get("LSE_" + estruc)
        if sp:
            LSEcg_domain_set_rc(LSE_db,dom.searchPath)
	    loc = "in attributes for '%s' '%s'" % (dom.instName,estruc) 
            myenv.name = dom
	    tok.processInput("%s %s;\n" % (sp,dom.instName),loc,[],sys.stdout)
	    foundone = 1

    if not foundone: print "    void *LSEfw_domain_dummy;"
    print "} LSEfw_%s_attr;\n" % estruc

)m4_dnl

/***************** and we are done ***********************/
m4_pythonfile(LSEcg_check_errors())m4_dnl

#endif /* _LSE_DOMAIN_TYPES_H_ */




