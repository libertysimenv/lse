/* 
 * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * This file is processed to become LSE_control.c
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file contains several things:
 * 1) Definitions of global signal and schedule control types, variables, and
 *    macros.  Basically, these are the "core" data structures that generated 
 *     module code and framework code manipulates.
 *
 * 2) Utility functions that do not have anything to do with startup or finish
 *
 * 3) Anything other code that did not seem to have a home....
 *
 * TODO: Make fields in dynids and resolutions release structures inside them
 *
 */
m4_include(SIM_quotes.m4)
LSEm4_push_diversion(-1)
m4_include(SIM_database.m4)
m4_pythonfile(
LSEpy_threaded = int(LSE_db.getParmVal("LSE_mp_multithreaded"))
if LSEpy_threaded:
    LSEpy_align="\n\tLSEfw_ALIGN"
else: LSEpy_align = "" # no need to align if not multi-threaded
)
LSEm4_pop_diversion()

extern "C" {
#include <setjmp.h>
}
#define LSEfw_IN_CONTROL_C
/* LSE_all_types gives stdio */
#include <SIM_all_types.h>
#include <SIM_control.h>
#include <SIM_threading.h>

/****************** Prototypes for code blocks referenced *********/

m4_pythonfile(
codeBlocksSeen={}
for ct in LSE_db.cblocksList:
    pts = LSEcg_codeblock_name(LSE_db,ct,0,1)
    if not codeBlocksSeen.get(pts):
       codeBlocksSeen[pts]=1
       print "extern " + LSEcg_codeblock_prototype(LSE_db,ct,1) + ";"
del codeBlocksSeen
)

/***************** Instance pointers ********************************/

m4_pythonfile(
for i in LSE_db.instanceOrder:
   print "extern class LSEfw_module %s;" % LSEcg_instdata_name(i.name)
)

/**************** Prototypes for port events referenced **********/

m4_pythonfile(
pinfoSeen = {}
for inst in LSE_db.instanceOrder:
    inamet = LSEcg_iname_transform(LSE_db,inst.name)
    for p in inst.portOrder:
        ev = inst.events.get(("%s.resolved" % p.name),None)
        if ev and ev.filled:
            key = (inamet,p.name,0)
            if not pinfoSeen.has_key(key):
                pinfoSeen[key] = None
                print "extern %s;" % \
                      LSEcg_port_event_prototype(inamet, p.name,
                                                 "resolved", p.type, 1)
        ev = inst.events.get(("%s.localresolved" % p.name),None)
        if ev and ev.filled:
            key = (inamet,p.name,1)
            if not pinfoSeen.has_key(key):
                pinfoSeen[key] = None
                print "extern %s;" % \
                      LSEcg_port_event_prototype(inamet, p.name,
                                                 "localresolved", p.type, 1)
del pinfoSeen
)

/******************** Define the structures needed *******************/

/* Standard error file pointer */
FILE *LSE_stderr;

/* Simulator return status */
int LSE_sim_exit_status;

/* Flag to terminate simulation */
int LSE_sim_terminate_now;

/* Counter to terminate simulation */
int LSE_sim_terminate_count;

/* measured wall clock time during actual execution in microseconds */
uint64_t LSE_sim_wall_time_elapsed;

/* main scheduling variables which change between cycles */
LSEfw_mainloop_info_t LSEfw_info LSEfw_ALIGN;

/* number of threads */
int LSEfw_num_threads;

/* global time... unfortunately gcc is weird about this...
   and sometimes puts it *before* LSEfw_info, which is bad!
 */
LSE_time_t LSE_time_now LSEfw_ALIGN;

/* mask for alternating unknowns in signal status */
unsigned int LSEfw_signal_time_mask = 0;

/* In phase_start flag */
boolean LSEfw_schedule_in_phase_start LSEfw_ALIGN;

/* Schedule flags -- indicates what has already been scheduled */
int LSEfw_schedule_flags[m4_python(len(LSE_db.cblocksList))]={1};

/* Common dummy variables and routines */
LSE_signal_t LSEfw_null_port_status = 
   (LSE_signal_nothing|LSE_signal_disabled|LSE_signal_ack);
m4_python(print LSEcg_codeblock_prototype(LSE_db,None,1)) { };
void *LSEfw_none_ptr;

/* calling code block */
LSEfw_tprivate unsigned int LSEfw_schedule_calling_cblock;

/* Scheduling list */
LSEfw_mutex_t LSEfw_schedule_mutex;
LSEfw_schedule_t LSEfw_schedule_timestep LSEfw_ALIGN = {0,0,0,NULL};

#if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
int LSEfw_schedule_LEClevel;
boolean LSEfw_schedule_LECrunagain;

LSEfw_schedule_LEC_levels_t
    LSEfw_schedule_LEC_levels[m4_python(print len(LSE_db.LEClevelSizes))];

m4_python(
size=reduce(lambda x,y:x+y+1, LSE_db.LEClevelSizes.values(), 0)
)m4_dnl
LSEfw_schedule_LEC_t LSEfw_schedule_LEC[m4_python(print 2*size)];

boolean LSEfw_schedule_LEC_flags[m4_python(print size)];
#endif

#if (LSEfw_PARM(LSE_DAP_static_schedule_type)==1||\
     LSEfw_PARM(LSE_DAP_static_schedule_type)==2)
boolean LSEfw_schedule_in_acyclic_finish;
#endif

/* Debugging histogram */
int LSEfw_cblock_histogram[m4_python(len(LSE_db.cblocksList))];

/* Code Blocks */
LSEfw_schedule_cblock_t LSEfw_schedule_cblock[] = {
m4_pythonfile(
for ct in LSE_db.cblocksList:
  funcname = LSEcg_codeblock_name(LSE_db,ct,1,1)
  inst = LSE_db.getInst(ct[1],1)
  if ct[0] < LSEdb_CblockDynamic:
     print "{(LSEfw_firing_t)%s, (LSEfw_iptr_t)&%s, %d}," % \
	   (funcname, LSEcg_instdata_name(inst.name), ct[3])
  else:
     print "{(LSEfw_firing_t)%s, NULL, %d}," % (funcname, ct[3])
)
};

/**************** port status at beginning of each cycle ************/

LSEfw_PSTAT_structure_t LSEfw_PORT_status LSEfw_ALIGN;

m4_pythonfile(
for inst in LSE_db.instanceOrder:
    for p in inst.portOrder:
        if not p.width: continue
        if 0:
            if p.controlEmpty:
                print "LSEfw_PORT_empty_t " + \
                      LSEcg_portstatus_name(inst.name,p.name,1)+\
                      ("[%d]={};" % p.width)
            else:
                print "LSEfw_PORT_nodef_t " + \
                      LSEcg_portstatus_name(inst.name,p.name,1)+\
                      ("[%d]={};" % p.width)
        else:
            if p.controlEmpty:
                print "LSEfw_PORT_empty_t *%s = &%s[0];" \
                      % (LSEcg_portstatus_name(inst.name,p.name,1),
                         LSEcg_portstatus_name(inst.name,p.name,0))
            else:
                print "LSEfw_PORT_nodef_t *%s = &%s[0];" \
                      % (LSEcg_portstatus_name(inst.name,p.name,1),
                         LSEcg_portstatus_name(inst.name,p.name,0))
)m4_dnl
              
LSEfw_PSTAT_structure_t LSEfw_PORT_initial_status LSEfw_ALIGN = {

m4_pythonfile(
enames=["0","LSE_signal_disabled|0400","LSE_signal_disabled|0400","LSE_signal_enabled|0400"]
anames=["0","LSE_signal_nack|040","LSE_signal_nack|040","LSE_signal_ack|040"]
dnames=["0","LSE_signal_nothing|04","LSE_signal_nothing|04","0"]

def getval(x,y,ci):
    globalv = "(%s|%s|%s)" % (dnames[x[0]],enames[x[1]],anames[x[2]])
    localv = "(%s|%s|%s)" % (dnames[y[0]],enames[y[1]],anames[y[2]])
    return "{%s|((%s)<<12),%s|((%s)<<12)}" % (globalv,globalv,localv,localv)

def getvalempty(x,ci):
    globalv = "(%s|%s|%s)" % (dnames[x[0]],enames[x[1]],anames[x[2]])
    return ("{%s|((%s)<<12)}" % (globalv,globalv))

if not LSE_db.pstatList:
  print "0,"
else:
        for pd in LSE_db.pstatList:
            if pd[3]:
                p = LSE_db.getPort(pd[0],pd[1])
                print ("{" + string.join(map(getvalempty, 
                                             p.globalConstants,p.connInfoList),
                                         ",\n")
                       + "},")
            else:
                p = LSE_db.getPort(pd[0],pd[1])
                print ("{" + string.join(map(getval,
                                             p.globalConstants,
                                             p.localConstants,p.connInfoList),
                                         ",\n")
                       + "},")
)
};

/**************** Port status pieces to ignore *************************/

LSEfw_PSTAT_structure_t LSEfw_PORT_ignore_status LSEfw_ALIGN = {

m4_pythonfile(if 1:

  def was_squashed(sno,db=LSE_db):
	sinfo = db.signalList[sno]
	return sinfo.dropreason == LSEdb_sigdropped_notused
	#return ((sinfo.usedby==[] and sinfo.type != LSEdb_sigtype_ofextra and
        #         sinfo.type != LSEdb_sigtype_ifextra and sinfo.type !=
        #         LSEdb_sigtype_pextra) #or (sinfo.constant) or 
	#	#(sinfo.realdepson==[] and sinfo.type<=LSEdb_sigtype_ack)
	#       )

  def do_global(i,x,p,db=LSE_db):
      # calculate global
      dv = 0
      ev = 0
      av = 0
      if (p.dir == "output"):
  	 if (p.controlEmpty): 
	   if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_data):
	     dv = 0004
	   if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_en):
	     ev = 0400
	 else:
	   if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_ofdata):
	     dv = 0004
	   if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_ofen):
	     ev = 0400
      else:
  	 if (p.controlEmpty): 
	   if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_ack):
	     av = 0040
         else:
	   if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_ifack):
	     av = 0040

      if not x:
         if (p.dir == "output"):
            dv = 0004
            ev = 0400
	 else:
            av = 0040
      else:
         q=x[0]
         if (p.dir == "output"):
	    if (q.controlEmpty):
	      if was_squashed(q.basesignalno + x[1]+q.width*LSEdb_sigoff_ack):
	        av = 0040
	    else:
	      if was_squashed(q.basesignalno + 
                              x[1]+q.width*LSEdb_sigoff_ifack):
	        av = 0040
	 else:
	    if (q.controlEmpty):
	      if was_squashed(q.basesignalno + x[1]+q.width*LSEdb_sigoff_data):
		dv = 0004
	      if was_squashed(q.basesignalno + x[1]+q.width*LSEdb_sigoff_en):
		ev = 0400
	    else:
	      if was_squashed(q.basesignalno +
                              x[1]+q.width*LSEdb_sigoff_ofdata):
		dv = 0004
	      if was_squashed(q.basesignalno + x[1]+q.width*LSEdb_sigoff_ofen):
		ev = 0400

      cil = p.connInfoList[i]
      if cil[LSEdb_cil_InstName]:
          cilmask = cil[LSEdb_cil_IgnoreMask]
          ignoremask = ((dv & 0004)>>2) + ((ev & 0400)>>7) + ((av & 0040)>>3)
          if cilmask != ignoremask:
              sys.stderr.write("Mismatch for %s:%s[%d] ig=%d cil=%d\n" \
                               % (p.inst.name,p.name,i,ignoremask,cilmask))
              raise SystemExit(1)
      return "(LSE_signal_t)0%o" % (dv+ev+av)

  def do_local(i,x,p,db=LSE_db):
      # calculate local
      dv = 0
      ev = 0
      av = 0
      if (p.dir == "output"):
	 if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_data):
	   dv = 0004
	 if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_en):
	   ev = 0400
	 if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_ofack):
	   av = 0040
      else:
	 if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_ifdata):
	   dv = 0004
	 if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_ifen):
	   ev = 0400
	 if was_squashed(p.basesignalno + i + p.width*LSEdb_sigoff_ack):
	   av = 0040
      return "(LSE_signal_t)0%o" % (dv + av + ev)
      
  def getval(i,x,p):
        globalv = do_global(i,x,p)
	localv = do_local(i,x,p)
	return "{%s,%s}" % (globalv,localv)
  def getvalempty(i,x,p):
	globalv = do_global(i,x,p)
	return ("{%s}" % globalv)

if not LSE_db.pstatList:
  print "0,"
else:
  for pd in LSE_db.pstatList:
    if pd[3]:
      p = LSE_db.getPort(pd[0],pd[1])
      print ("{" + string.join(map(getvalempty, range(0,p.width),
                                             p.connections,
                                             [p] * p.width
                                             ),",\n")
                       + "},")
    else:
      p = LSE_db.getPort(pd[0],pd[1])
      print ("{" + string.join(map(getval, range(0,p.width),
                                             p.connections,
                                             [p] * p.width),",\n")
                       + "},")
)
};

/**************** Port communication structures *************************
 * These are placed here so that module instances do not need to be     *
 * rebuilt when structure definitions change                            *
 ************************************************************************/

struct LSEfw_portinst_info_s<LSE_type_none> LSEfw_null_portinst_info = {
  { { {-1},{-1},{-1},{-1},{-1},{-1},{-1},{-1} } },
  0, 0,
  };

m4_pythonfile(
qlist = ("global.data", "global.enable",
         "global.ack", "global.any")

# tables in per-instance order
for inst in LSE_db.instanceOrder:
    if not LSE_db.getParmVal("LSE_specialize_codeblock_numbers"):
        print "int " + LSEcg_codeblock_base(inst.name) + \
              "= %d;" % inst.firstCblock

# local tables in per-instance order

for inst in LSE_db.instanceOrder:
  for p in inst.portOrder:
    if not p.width: continue
  
    doalign = LSEpy_align

    print "struct LSEfw_portinst_info_s<%s> %s[%d] %s = { " % \
          (p.type, LSEcg_portinst_info_name(inst.name,p.name),
           p.width, doalign)

    for pcin in range(0,len(p.connInfoList)):
        print ("""{ { { {-1},{-1},{-1},{-1},{-1},{-1},{-1},{-1}}},
                        %d, %d,
                        },""" %
                   (p.localResolveCodeList[pcin],  # localresolvecode
                    p.globalResolveCodeList[pcin], # globalresolvecode
                    ))

    print "};"

# global tables in per-instance order
for inst in LSE_db.instanceOrder:
  for p in inst.portOrder:
    if not p.width: continue
 
    #### define the global information table ####

    doalign = LSEpy_align

    print "\nstruct LSEfw_portinst_ginfo_s<%s> %s[%d] = {" % \
          (p.type, LSEcg_portinst_ginfo_name(inst.name,p.name), p.width)

    dynamic_schedule_type = LSE_db.getParmVal("LSE_DAP_dynamic_schedule_type")
    
    for pcin in range(len(p.connInfoList)):
        pci = p.connInfoList[pcin]
        sigs = p.getConnectedSignals(pcin)[1]
        
        ct = pci[LSEdb_cil_CBlock]
        iname = pci[LSEdb_cil_InstName]
        if iname: 
            psn = "%s[%d].global" % \
               (LSEcg_portstatus_name(pci[LSEdb_cil_InstName],
                                      pci[LSEdb_cil_PortName],0),
                pci[LSEdb_cil_PortInstno])
            ep2 = "(LSEfw_iptr_t)&" + LSEcg_instdata_name(iname)
            pii = "&%s" % \
                LSEcg_portinst_info_ref(pci[LSEdb_cil_InstName], 
                                        pci[LSEdb_cil_PortName], 
                                        pci[LSEdb_cil_PortInstno])
            inamet = LSEcg_iname_transform(LSE_db,iname)
        else: 
            ep2 = "NULL"
            psn = "LSEfw_null_port_status"
            pii = "(struct LSEfw_portinst_info_s<%s> *)&%s" % \
                    (p.type, "LSEfw_null_portinst_info")

        if pci[LSEdb_cil_EventUses][0]:
            presolve = "(LSEfw_portevent_t)" + \
                       LSEcg_event_name_str(inamet,
                                            pci[LSEdb_cil_PortName]+\
                                            ".resolved",
                                            "record",1)
        else: presolve = "NULL";
        if pci[LSEdb_cil_EventUses][1]:
            plresolve = "(LSEfw_portevent_t)" + \
                       LSEcg_event_name_str(inamet,
                                            pci[LSEdb_cil_PortName]+\
                                            ".localresolved",
                                            "record",1)
        else: plresolve = "NULL";

        if dynamic_schedule_type != 0:
            LECstuff = "\n        {{%d,%d,%d},{%d,%d,%d}}, " \
                       "{{%d,%d,%d},{%d,%d,%d}}, {{%d,%d,%d},{%d,%d,%d}}, " % \
                       pci[LSEdb_cil_LECinfo]
        else:
            LECstuff = ""

        print ("""{ &%s,
                    %d, %d, { %d, %d, %d },%s
                    (LSEfw_firing_t)%s,
                    %s, 
                    %s, %s, 
                    %d, %d, 0%o, 
                    %s,
                    },""" %
               (psn, # ostatus
                pci[LSEdb_cil_PortInstno], # instno
                LSE_db.cblocksBacklist[pci[LSEdb_cil_CBlock]],#cno
                sigs[0],sigs[1],sigs[2], # sigs
                LECstuff,  # LEC stuff
                LSEcg_codeblock_name(LSE_db,pci[LSEdb_cil_CBlock],
                                     0,1), #firing
                ep2,                         # pointer
                presolve,                    # port resolution
                plresolve,                   # port local resolution
                pci[LSEdb_cil_SchedCode],    # schedcode
                p.firingCodeList[pcin],      # fschedcode
                pci[LSEdb_cil_IgnoreMask] | pci[LSEdb_cil_ConstantMask],
                pii,                         # other info
                ))
                   
    print "};"

)

/****************** internal global APIs **********************************/

void 
LSEfw_report_err(const char *inst, const char *fmt, ...) {
  va_list ap;
  va_start(ap,fmt);
  fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
  fprintf(LSE_stderr," : %s : ", inst);
  vfprintf(LSE_stderr, fmt, ap);
  fprintf(LSE_stderr,"\n");
  LSE_sim_terminate_now = -99;
  va_end(ap);
}

/**************** Functions for managing port queries ******************/

void LSEfw_port_query_sleep(LSEfw_query_instance_t *instance,
			    unsigned int LSEfw_calling_id, 
			    int LSEfw_instno) {

  LSEfw_query_port_entry_t *entry;
  int i;

  /* Find the query entry */
  
  if (!instance->entry) {
    /* generate a new entry */
    instance->entry= entry = 
	(LSEfw_query_port_entry_t *)malloc(sizeof(LSEfw_query_port_entry_t));
    /* entry->currwaiters = 0; */ /* redundant */
    entry->allocwaiters = 4;
  }
  else {
     entry = (LSEfw_query_port_entry_t *)instance->entry;
  }

  /* If not a valid entry, set the currwaiters to 0; this prevents
   * us from having to clear all the currwaiters at the end of the time
   * step.
   */
  if (instance->index == -1) entry->currwaiters = 0;
  instance->index = LSEfw_instno;

  /* Insert a waiting code block */

  for (i=0;i<entry->currwaiters;i++) 
    if (entry->waiters[i] == LSEfw_calling_id) return;
  
  if (entry->currwaiters >= entry->allocwaiters) {
    entry->allocwaiters = entry->allocwaiters*2;
    instance->entry = entry = (LSEfw_query_port_entry_t *)realloc(entry,
			     sizeof(LSEfw_query_port_entry_t)+
			     sizeof(int) * entry->allocwaiters-4);
  }

  entry->waiters[entry->currwaiters++] = LSEfw_calling_id;

}

void LSEfw_port_query_wakeup(LSEfw_query_instance_t *instance) {

  LSEfw_query_port_entry_t *entry;
  int j;
  
  /* Find the query entry */

  if (instance->index != -1) {

    entry = (LSEfw_query_port_entry_t *) instance->entry;
    for (j=0;j<entry->currwaiters;j++) {
      LSEfw_schedule_append(&LSEfw_schedule_timestep,entry->waiters[j]);
    }
    entry->currwaiters = 0;
 #if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
    LSEfw_schedule_LECrunagain = TRUE;
 #endif
  }

}

/**************** standard debug status routine **************/

void LSEfw_debug_status_standard(const char *fullname, int instno,
                                 LSE_signal_t status) {
  if (!LSEfw_PARM(LSE_show_port_statuses_changes)) return;
  if((LSE_time_ge(LSE_time_now,
	LSE_time_construct(LSEfw_PARM(LSE_show_port_statuses_start_cycle),
                           LSEfw_PARM(LSE_show_port_statuses_start_phase))) &&
      LSE_time_le(LSE_time_now,
	LSE_time_construct(LSEfw_PARM(LSE_show_port_statuses_end_cycle),
                           LSEfw_PARM(LSE_show_port_statuses_end_phase)))) ||
     LSE_time_eq(LSE_time_now,LSE_time_construct(-1,-1))) {
    fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
    fprintf(LSE_stderr,":%s(%d): %04o:",fullname,instno,status);
    LSE_signal_print(LSE_stderr,status);
    fprintf(LSE_stderr,"\n");
  }
}

/*********** Dynid and resolution dump and reclamation functions *********/


void LSE_dynid_dump() {
  int t;
  int count = 0;
  LSE_dynid_t p;

  for (t=0 ; t < LSEfw_num_threads; t++) {
#if (LSEfw_parallelize_dynids)
    p = *LSEfw_shthreads[t].LSEdy_dynid_master_listP;
#else
    p = LSEdy_dynid_master_list;
#endif
    while (p != NULL) {

      fprintf(LSE_stderr,"Dyn %" PRIu64 ":%d ", p->idno, p->super.count);
m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(dynid_dump)(p);"
LSEcg_domain_hook_loop("dynid_dump", doit, doit, LSE_db)
)
      fprintf(LSE_stderr,"\n");
       p=(LSE_dynid_t)p->super.next;
       count ++;
    } /* while p */
  } /* for t */
  fprintf(LSE_stderr,"Total dynids:%d\n",count);
}

void LSE_resolution_dump() {
  LSE_resolution_t p= LSEre_resolution_master_list;
  int count = 0;
  while (p != NULL) {

    fprintf(LSE_stderr,"Res %d %d",
	    p->super.count,
	    p->num_ids);
m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(dynid_dump)(p->resolved_inst);"
LSEcg_domain_hook_loop("dynid_dump", doit, doit, LSE_db)
)
    fprintf(LSE_stderr,"\n");
    p=(LSE_resolution_t)p->super.next;
    count ++;
  }
  fprintf(LSE_stderr,"Total resolutions:%d\n",count);
}

#if (LSEfw_parallelize_dynids)
extern LSEfw_oneshot_barrier_t LSEfw_mainloop_barrier;
extern LSEfw_oneshot_barrier_t LSEfw_phase_barrier;
extern LSEfw_oneshot_barrier_t LSEfw_status_barrier;
#endif
struct LSEfw_sharedthread_info_t *LSEfw_shthreads;

void LSEdy_dynid_reclaim_hooks(LSE_dynid_t dp) {
m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(dynid_reclaim)(dp);"
LSEcg_domain_hook_loop("dynid_reclaim", doit, doit, LSE_db)
)
}

void LSEdy_dynid_allocate_hooks(LSE_dynid_t dp) {
m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(dynid_allocate)(dp);"
LSEcg_domain_hook_loop("dynid_allocate", doit, doit, LSE_db)
)
}

void LSE_memory_reclaim_master() {
  LSE_dynid_t dp, *ldp;
  int dcount;
  LSE_resolution_t rp, *lrp;
  int rcount;
  int i;
  boolean did_something;
  boolean had_neg_dynid = FALSE;
  boolean had_neg_res = FALSE;

 try_again:
  did_something = FALSE;
  dcount = 0;
  rcount = 0;

	/*********** resolutions ***********/

  rp = LSEre_resolution_master_list;
  lrp = &LSEre_resolution_master_list;

  while (rp != NULL) {

    if (!rp->super.count) {
      LSE_resolution_t q;

m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(resolution_reclaim)(rp);"
LSEcg_domain_hook_loop("resolution_reclaim", doit, doit, LSE_db)
)m4_dnl

      LSEdy_dynid_internal_cancel(rp->resolved_inst,
			          "resolution reclaim ", __FILE__,__LINE__);
      /* cancel all IDs in the list */
      for (i=0;i<rp->num_ids;i++) {
        LSEdy_dynid_internal_cancel(rp->ids[i],
			            "resolution reclaim ", __FILE__,__LINE__);
      }

      /* TODO: free dynids/resolutions in fields */

      /* splice out */
      *lrp = q = (LSE_resolution_t)rp->super.next;

      /* add to free list */
      rp->super.next = (LSE_refcount_t *)LSEre_resolution_free_list;
      LSEre_resolution_free_list = rp;

      rp=q;
      did_something = TRUE;
    }
    else {
      if (LSEfw_PARM(LSE_debug_resolution_limit)>0) {
	rcount++;
	if (rp->super.count < 0) /* negative are not good! */
	  had_neg_res = TRUE;
      }
      lrp = (LSE_resolution_t *)&rp->super.next;
      rp=(LSE_resolution_t)rp->super.next;
    }
  }

#if (LSEfw_parallelize_dynids)
  LSEfw_info.thread_command = LSEfw_thread_command_MemReclaim;
  LSEfw_status_barrier.reset(); /* not yet reset */
  LSEfw_mainloop_barrier.nowait(); /* wait for others to begin */
#endif

	/*********** dynids ***********/

  dp = LSEdy_dynid_master_list;
  ldp = &LSEdy_dynid_master_list;

  while (dp != NULL) {

    if (!dp->super.count) {
      LSE_dynid_t q;

m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(dynid_reclaim)(dp);"
LSEcg_domain_hook_loop("dynid_reclaim", doit, doit, LSE_db)
)

      /* TODO: free dynids/resolutions in fields */

      /* splice out */
      *ldp = q = (LSE_dynid_t)dp->super.next;

      /* add to free list */
#if (LSEfw_PARM(LSE_debug_dynid_refs)==TRUE)
      fprintf(LSE_stderr,"F%" PRIu64 " %d\n",dp->idno,dp->super.count);
#endif
      dp->super.next = (LSE_refcount_t *)LSEdy_dynid_free_list;
      LSEdy_dynid_free_list = dp;

      dp=q;
    }
    else {
      if (LSEfw_PARM(LSE_debug_dynid_limit)>0) {
	dcount++;
	if (dp->super.count < 0) /* negative are not good! */
	  had_neg_dynid = TRUE;
      }
      ldp = (LSE_dynid_t *)&dp->super.next;
      dp=(LSE_dynid_t)dp->super.next;
    }
  }

#if (LSEfw_parallelize_dynids)
  LSEfw_status_barrier.wait();  
  LSEfw_mainloop_barrier.reset();
  LSEfw_phase_barrier.wait();  
  LSEfw_info.thread_command = LSEfw_thread_command_None; /* put back */
  /* no need to clear status_barrier because the next timestep will */
#endif

#if (LSEfw_parallelize_dynids)
  /* perform reduction of reports across threads */
  for (i = 0; i < LSEfw_num_threads -1 ; i++) {
    dcount += LSEfw_shthreads[i+1].reclaim_reports.dcount;
    had_neg_dynid |= LSEfw_shthreads[i+1].reclaim_reports.had_neg_dynid;
    did_something |= LSEfw_shthreads[i+1].reclaim_reports.did_something;
  }
#endif

  if (LSEfw_PARM(LSE_debug_resolution_limit)>0) {
    if (rcount > LSEfw_PARM(LSE_debug_resolution_limit)) {
      /* do not complain until we have freed all that can be freed */
      if (did_something) goto try_again;
      LSE_resolution_dump();
      fprintf(LSE_stderr,
	      "Resolution limit of %d exceeded; a memory leak is likely.\n",
	      LSEfw_PARM(LSE_debug_resolution_limit));
      LSE_sim_terminate_now = -2;
    } else if (had_neg_res) {
      LSE_resolution_dump();
      fprintf(LSE_stderr,"A resolution had a negative reference count.\n");
      LSE_sim_terminate_now = -2;
    }
  }

  if (LSEfw_PARM(LSE_debug_dynid_limit)>0) {
    if (dcount > LSEfw_PARM(LSE_debug_dynid_limit)) {
      /* do not complain until we have freed all that can be freed */
      if (did_something) goto try_again;
      LSE_dynid_dump();
      fprintf(LSE_stderr,
	      "Dynid limit of %d exceeded; a memory leak is likely.\n",
	      LSEfw_PARM(LSE_debug_dynid_limit));
      LSE_sim_terminate_now = -2;
    } else if (had_neg_dynid) {
      LSE_dynid_dump();
      fprintf(LSE_stderr,"A dynid had a negative reference count.\n");
      LSE_sim_terminate_now = -2;
    }
  }
  
}

#if (LSEfw_parallelize_dynids)

void LSE_memory_reclaim_slave(int tno) {
  LSE_dynid_t dp, *ldp;
  int dcount;
  boolean did_something;
  boolean had_neg_dynid = FALSE;

  did_something = FALSE;
  dcount = 0;

	/*********** dynids ***********/

  dp = LSEdy_dynid_master_list;
  ldp = &LSEdy_dynid_master_list;

  while (dp != NULL) {

    if (!dp->super.count) {
      LSE_dynid_t q;

m4_pythonfile(
def doit(dinst,dummy): print "LSE_domain_hook_call(dynid_reclaim)(dp);"
LSEcg_domain_hook_loop("dynid_reclaim", doit, doit, LSE_db)
)

      /* TODO: free dynids/resolutions in fields */

      /* splice out */
      *ldp = q = (LSE_dynid_t)dp->super.next;

      /* add to free list */
#if (LSEfw_PARM(LSE_debug_dynid_refs)==TRUE)
      fprintf(LSE_stderr,"F%" PRIu64 " %d\n",dp->idno,dp->super.count);
#endif
      dp->super.next = (LSE_refcount_t *)LSEdy_dynid_free_list;
      LSEdy_dynid_free_list = dp;

      dp=q;
    }
    else {
      if (LSEfw_PARM(LSE_debug_dynid_limit)>0) {
	dcount++;
	if (dp->super.count < 0) /* negative are not good! */
	  had_neg_dynid = TRUE;
      }
      ldp = (LSE_dynid_t *)&dp->super.next;
      dp=(LSE_dynid_t)dp->super.next;
    }
  }

  LSEfw_shthreads[tno].reclaim_reports.dcount = dcount;
  LSEfw_shthreads[tno].reclaim_reports.had_neg_dynid = had_neg_dynid;
  LSEfw_shthreads[tno].reclaim_reports.did_something = did_something;
}
#endif /* LSEfw_parallelize_dynids */

LSE_dynid_t LSE_dynid_default;   // dummy dynamic ID

/******************* Offsets into structures *****************/

size_t LSEdy_dynid_size = sizeof(LSEdy_dynid_info_t);
size_t LSEdy_dynid_field_size = sizeof(struct LSEfw_dynid_t_extension);
size_t LSEdy_dynid_field_offset = offsetof(LSEdy_dynid_info_t,fields);

size_t LSEre_resolution_size = sizeof(LSEre_resolution_info_t);
size_t LSEre_resolution_field_size = 
	sizeof(struct LSEfw_resolution_t_extension);
size_t LSEre_resolution_field_offset =offsetof(LSEre_resolution_info_t,fields);

m4_pythonfile(
itname = {
    "LSE_dynid_t" : "LSEdy_dynid_info_t",
    "LSE_resolution_t" : "LSEre_resolution_info_t"
  }
if not LSE_db.getParmVal("LSE_use_direct_field_access"):
    for s in ["LSE_dynid_t","LSE_resolution_t"]:
        il = LSE_db.structAdds[s].keys(); il.sort()
        for i in il:
	    print "const size_t " + LSEcg_structadd_offset_name(i,s) + \
		  " = offsetof(" + itname[s] + ", fields." + \
		  LSEcg_flatten(i)+");"
)

/**** miscellaneous stuff defined in headers other than SIM_control.h *****/

LSE_dynid_num_t LSEdy_dynid_global_num;      /* last allocated idno */
#if (!LSEfw_parallelize_dynids)
LSE_dynid_t LSEdy_dynid_free_list=NULL;      /* free dynids */
LSE_dynid_t LSEdy_dynid_master_list=NULL;    /* unfree dynids */
#else
LSEfw_tprivate LSE_dynid_t LSEdy_dynid_free_list;
LSEfw_tprivate LSE_dynid_t LSEdy_dynid_master_list;
#endif
LSE_resolution_t LSEre_resolution_free_list=NULL;    /* free resolutions */
LSE_resolution_t LSEre_resolution_master_list=NULL;  /* unfree resolutions */

LSEfw_tprivate int unknown_signal_count;
LSEfw_tprivate int *sigs_resolved;

/**************** port alias structures ****************************/

m4_pythonfile(
pinfoSeen = []
paSeen = {}
for i in LSE_db.instanceOrder:
    aliasl = i.aliasSeen.keys()
    aliasl.sort()
    for key in aliasl:
        if paSeen.has_key(key): continue
        paSeen[key] = 1
        pa = LSE_db.getPortAlias(key[0],key[1])
        if not len(pa[1]): continue # empty port alias

	# pull in the things we need to see
        qdtype = "LSE_type_none"
        for c in pa[1]:
            if c:
                p = LSE_db.getPort(c[0],c[1])
                LSEcg_do_portquery_externs(p,1,pinfoSeen)
                qdtype = p.type
	
        print "struct LSEfw_port_alias_s<%s> %s[] = {" % \
              (qdtype,LSEcg_port_alias_name(key[0],key[1]))
        for c in pa[1]:
           if c:
               pir = LSEcg_portinst_info_ref(c[0],c[1],c[2])
               # note: query of something with zero width will fail
               print "{ &%s[%d].global,\n&%s }," % \
                     (LSEcg_portstatus_name(c[0],c[1],0),c[2], pir)
           else:
               print "{ &LSEfw_null_port_status,\n" \
                     "  &LSEfw_null_portinst_info.wakeups,\n" \
                     "  (struct LSEfw_portinst_info_s<%s> *) " \
                     "&LSEfw_null_portinst_info }," % qdtype
        print "};"
	     
)

/************* Variables from LSS **********/

m4_pythonfile(
def find_tlist(t):
    if not t[3]: return None
    return (t[2],t[3][0])

tgv = map(find_tlist,LSE_db.globalVars)
tgv.sort()
for t in tgv:
    if t: print "%s %s;" % (t[0],t[1]);

rtps = LSE_db.runtimeParms.items()
rtps.sort()
for rtp in rtps:
    print "%s %s;" % (rtp[1][2], rtp[1][0])
)m4_dnl

/****************** Hook for debugger **********************/

void LSEfw_no_more_time(void) {
}

/****************** Extra domain code stuff **********************/

m4_pythonfile(
#
# set up the tokenizer and environment
#
myenv = LSEcg_Environment()
myenv.db = LSE_db
myenv.inst = None
myenv.codeloc = LSEcg_in_domaincode
myenv.reporter = LSEcg_report_error
myenv.name = None
#
# and ask for ids to be generated
#
LSEcg_domain_generate_ids(LSE_db,[LSEap_APIs],myenv,2)
)
