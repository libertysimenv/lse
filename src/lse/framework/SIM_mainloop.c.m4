/* 
 * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * This file is processed to become LSE_mainloop.c
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * The main loop of the simulator
 *
 * TODO:  
 *
 */
m4_include(SIM_quotes.m4)
LSEm4_push_diversion(-1)
m4_include(SIM_database.m4)
m4_pythonfile(
LSEpy_instance = LSE_db.getInst("LSEfw_top")
LSEpy_has_dynamic = (len(LSE_db.dynamicExtras)>0)
LSEpy_timing_needed = LSE_db.getParmVal("LSE_DAP_record_invocation_timing")
)
LSEm4_pop_diversion()

/* LSE_all_types includes stdio, LSE_config */
#include <SIM_all_types.h>
#include <SIM_control.h>
#include <SIM_clp_interface.h>
#include <algorithm>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#if ((LSEfw_PARM(LSE_check_ports_incrementally)||\
      !LSEfw_PARM_constant(LSE_check_ports_incrementally))&&\
     LSEfw_PARM(LSE_DAP_static_schedule_type)!=1 &&\
     LSEfw_PARM(LSE_DAP_static_schedule_type)!=2)
#define LSEfw_check_ports 1
#endif
#include <sys/time.h>
#if (LSEfw_PARM(LSE_mp_multithreaded)) 
#include <MPscheduler/LSEtasks.h>
#endif

/******************** thread information *******************************/

#include <SIM_threading.h>

/****************** Prototypes for code blocks referenced *********/

m4_pythonfile(
codeBlocksSeen={}
for ct in LSE_db.cblocksList:
    pts = LSEcg_codeblock_name(LSE_db,ct,0,1)
    if not codeBlocksSeen.get(pts):
       codeBlocksSeen[pts]=1
       print "extern " + LSEcg_codeblock_prototype(LSE_db,ct,1) + ";"
del codeBlocksSeen
)

/***************** Prototypes for top-level events *******************/

m4_pythonfile(
for e in LSEpy_instance.eventOrder:
  if e.isFilled:
      print "extern " + LSEcg_event_prototype(e,"record",1) + ";"
)

/***************** Instance pointers ********************************/

m4_pythonfile(
for i in LSE_db.instanceOrder:
    print "extern class LSEfw_module %s;" % LSEcg_instdata_name(i.name)
)

/***************** Sync functions ****************************/

LSEfw_oneshot_barrier_t LSEfw_mainloop_barrier LSEfw_ALIGN;
LSEfw_oneshot_barrier_t LSEfw_status_barrier LSEfw_ALIGN;
LSEfw_oneshot_barrier_t LSEfw_phase_barrier LSEfw_ALIGN;
LSEfw_oneshot_barrier_t LSEfw_phase_end_barrier LSEfw_ALIGN;
LSEfw_mutex_t           LSEfw_sigcheck_mutex LSEfw_ALIGN;

LSEfw_syncsig_t         *LSEfw_sync_signals;
LSEfw_mutex_t           *LSEfw_cblock_mutexes;
int                     LSEfw_num_syncs;
int                     LSEfw_num_locks;

void LSEfw_sync_send(LSEfw_stf_def int sno) {
  LSEfw_syncsig_signal(&LSEfw_sync_signals[sno]);
}

void LSEfw_sync_recv(LSEfw_stf_def int sno) {
  LSEfw_syncsig_wait(&LSEfw_sync_signals[sno]);
}

void LSEfw_sync_lock(LSEfw_stf_def int sno) {
  LSEfw_cblock_mutexes[sno].lock();
}

void LSEfw_sync_unlock(LSEfw_stf_def int sno) {
  LSEfw_cblock_mutexes[sno].unlock();
}

void LSEfw_syncend_send(LSEfw_stf_def int sno) {
  LSEfw_syncsig_signal(&LSEfw_sync_signals[sno]);
}

void LSEfw_syncend_recv(LSEfw_stf_def int sno) {
  LSEfw_syncsig_wait(&LSEfw_sync_signals[sno]);
}

void LSEfw_syncend_lock(LSEfw_stf_def int sno) {
  LSEfw_cblock_mutexes[sno].lock();
}

void LSEfw_syncend_unlock(LSEfw_stf_def int sno) {
  LSEfw_cblock_mutexes[sno].unlock();
}

namespace {

#if (defined(__x86_64__) && defined(__GNUC__))
inline uint64_t read_timer(void) {
  unsigned int p1, p2;
  asm volatile(
  "rdtsc\n"
  "mov %%eax, %0\n"
  "mov %%edx, %1\n"
  : "=r" (p1), "=r" (p2) : : "eax", "edx");
  return (((uint64_t)p2)<<32)|p1;
}
#else
inline uint64_t read_timer(void) {
  return 0;
}
#endif

}

/***************** BEGIN dynamic sections *****************/

inline void LSEfw_run_dynamic(void) {
  LSEfw_iptr_t LSEfw_iptr;

  int ptr=LSEfw_schedule_pointer(LSEfw_schedule_timestep);
    while (ptr < LSEfw_schedule_counter(LSEfw_schedule_timestep)) {
      int tp=LSEfw_schedule_piece(LSEfw_schedule_timestep,ptr);
      LSEfw_schedule_flags[tp] = 0;
      LSEfw_schedule_calling_cblock = tp;

      if (LSEfw_PARM(LSE_debug_codeblock_calls))
        fprintf(LSE_stderr,"Dynamically calling code block %d from dyn\n",tp);
      if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram)) 
	 LSEfw_cblock_histogram[tp]++;
      LSEfw_iptr = LSEfw_schedule_cblock[tp].ptr;
      (*(LSEfw_schedule_cblock[tp].cont))
                         (LSEfw_stf_call0 LSEfw_schedule_cblock[tp].instno);
      ptr++;
    }
  LSEfw_schedule_pointer(LSEfw_schedule_timestep)=ptr;
}

inline void LSEfw_clear_flags() {
    int ptr = LSEfw_schedule_pointer(LSEfw_schedule_timestep);
    int ctr = LSEfw_schedule_counter(LSEfw_schedule_timestep);
    while (ptr < ctr) {
      int tp=LSEfw_schedule_piece(LSEfw_schedule_timestep,ptr);
      LSEfw_schedule_flags[tp] = 0;
      ptr++;
    }
}

m4_python(print LSEcg_codeblock_prototype(LSE_db,
					  (LSEdb_CblockDynamic,"DYN",None,0),
					  1)) {
    /* this is a dummy function */
}

m4_pythonfile(
dsects = LSE_db.dynamicExtras.items()
dsects.sort()
if (len(dsects)):
  print "struct LSEfw_LEC_DYN_s { int cno, level, fno; };\n"

for ds in dsects:
    bg = LSE_db.block_sched_graph[ds[0]]

    if bg.dyntype==1:
        print "LSEfw_LEC_DYN_s LSEfw_LEC_DYN_%d[] = {" % (ds[0])
        mustadd = []
        for i in ds[1]:
            for l in LSE_db.cblocksLECtlevels.get(i,[]):
                if l < bg.firsttlevel: continue
                elif l > bg.lasttlevel:
                    level=0
                    break
                else:
                    level=l
                    break
            else:
                level=0
            if level:
                print "{ %d, %d, %d }," % (i, level,
                                           LSE_db.cnolev2flag[(i,level)])
            else: mustadd.append(i)
        print "{ -1, -1, -1},"
        print "};"
        
    print LSEcg_codeblock_prototype(LSE_db,
	   LSE_db.cblocksList[LSE_db.block_sched_graph[ds[0]].cno],
				    1) + \
          "{ "
    
    if bg.dyntype==1:
        for cno in mustadd:
            print "LSEfw_schedule_append(&LSEfw_schedule_timestep,%d);" % cno
        print """{
        int lev, tp, i, fno;
        LSEfw_schedule_LEC_t *head,*tail;
        
        for (i=0;i<sizeof(LSEfw_LEC_DYN_%d)/sizeof(LSEfw_LEC_DYN_%d[0])-1;i++)
        {
          fno = LSEfw_LEC_DYN_%d[i].fno;
          if (!LSEfw_schedule_LEC_flags[fno]) {
             LSEfw_schedule_LEC_flags[fno]=TRUE;
             lev = LSEfw_LEC_DYN_%d[i].level;
             head = LSEfw_schedule_LEC_levels[lev].head;
             head->cno = LSEfw_LEC_DYN_%d[i].cno;
             head->fno = fno;
             if (LSEfw_PARM(LSE_debug_codeblock_calls))
                fprintf(LSE_stderr,"Scheduling %%d@%%d from dynsect %d\\n",
                        head->cno, lev);
             LSEfw_schedule_LEC_levels[lev].head = head->next;
          }
        }
        do {
          LSEfw_schedule_LEClevel = %d;
          LSEfw_run_dynamic();
          LSEfw_schedule_LECrunagain = FALSE;
          for (lev = %d ; lev <= %d ; lev++) {
            LSEfw_schedule_LEClevel = lev;
            tail = LSEfw_schedule_LEC_levels[lev].tail;
            head = LSEfw_schedule_LEC_levels[lev].head;
            while (tail != head) {
              /* run along...*/
              tp = tail->cno;
              LSEfw_schedule_LEC_flags[tail->fno] = FALSE;

              LSEfw_schedule_calling_cblock = tp;

              if (LSEfw_PARM(LSE_debug_codeblock_calls))
                fprintf(LSE_stderr,
		"Dynamically calling code block %%d from level %%d\\n",tp,lev);
              if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram)) 
	        LSEfw_cblock_histogram[tp]++;
              LSEfw_iptr = LSEfw_schedule_cblock[tp].ptr;
              (*(LSEfw_schedule_cblock[tp].cont))
                         (LSEfw_stf_call0 LSEfw_schedule_cblock[tp].instno);
              tail = tail->next;
            }
            LSEfw_schedule_LEC_levels[lev].tail = tail;
          }
        } while (LSEfw_schedule_LECrunagain);
        LSEfw_schedule_LEClevel=%d;
      }""" % (ds[0],ds[0],ds[0],ds[0],ds[0],ds[0],
              bg.firsttlevel-1, bg.firsttlevel, bg.lasttlevel, 
	      bg.lasttlevel + 1)

    elif bg.dyntype==2:

        masks = (0003,0300,0030,0003,0300,0030,0,0003,0300,0030,0,0,0,0)
        bgn = LSE_db.block_sched_graph[ds[0]]

        # determine the signals
        siglist = map(lambda x:LSE_db.signalList[x],bgn.sublist)
        
        siglist2 = []
        for sig in siglist:
            if sig.type >= LSEdb_sigtype_ifextra or \
               sig.type == LSEdb_sigtype_ofextra: continue
            if sig.isGlobal: globalness = "global"
            else: globalness = "local"
            n = "(%s[%d].%s)" % (LSEcg_portstatus_name(sig.inst,sig.port),
                                 sig.instno,globalness)
            siglist2.append((sig.num,n,masks[sig.type]))

        # define signals
        print "  boolean check;" 

        # figure out the pieces
        dorun = []
        for sig in siglist:
            for h in sig.generators:
                if bgn.subcnos.has_key(h):
                    if h not in dorun: dorun.append(h)

        # execute the pieces
        print "  do { "
        for (signum,sigstr,sigmask) in siglist2:
            print "  LSE_signal_t LSEsig_%d = %s;" % (signum,sigstr)
        for h in dorun:
            print """
              LSEfw_schedule_calling_cblock = %d;

              if (LSEfw_PARM(LSE_debug_codeblock_calls))
                fprintf(LSE_stderr,
		"Sub-block calling code block %d from sub-block %d\\n");
              if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram)) 
	        LSEfw_cblock_histogram[%d]++;""" % (h,h,h,ds[0],h)

            ct = LSE_db.cblocksList[h]

            print "  LSEfw_iptr = (LSEfw_iptr_t)&%s;" % \
              LSEcg_instdata_name(LSE_db.getInst(ct[1],1).name)
              
            if ct[0]==LSEdb_CblockPhase:
                print "%s(LSEfw_stf_call0_void); /* cno=%d */" % \
                      (LSEcg_codeblock_name(LSE_db,ct,1,1),h)
            else:
                print "%s(LSEfw_stf_call0 %d); /* cno=%d */" % \
                      (LSEcg_codeblock_name(LSE_db,ct,1,1),ct[3],h)
                
        # check code
        print "check = FALSE;"
        for (signum,sigstr,sigmask) in siglist2:
            print "check = check || ((LSEsig_%d ^ %s) & 0%o);" % \
                  (signum,sigstr,sigmask)
            
        print """  } while (check);"""

    elif bg.dyntype == 3: # acyclic section

        thisvars = {
            "lastcno" : LSE_db.totalCblocks,
            }
        
        print """int cno;
        
        LSEfw_schedule_in_acyclic_finish = TRUE;
        
        for (cno=1;cno<%(lastcno)d;cno++) {
          if (LSEfw_schedule_flags[cno]<0) {

             LSEfw_schedule_flags[cno] = 0;
             LSEfw_schedule_calling_cblock = cno;

      if (LSEfw_PARM(LSE_debug_codeblock_calls))
        fprintf(LSE_stderr,
                "Pseudo-dynamically calling code block %%d from acyclic\\n",
                cno);
      if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram)) 
	 LSEfw_cblock_histogram[cno]++;
      LSEfw_iptr = LSEfw_schedule_cblock[cno].ptr;
      (*(LSEfw_schedule_cblock[cno].cont))
                         (LSEfw_stf_call0 LSEfw_schedule_cblock[cno].instno);
           }
        }
        
        LSEfw_run_dynamic();""" % thisvars

    else:
        for cno in ds[1]:
            print "LSEfw_schedule_append(&LSEfw_schedule_timestep,%d);" % cno
        print "LSEfw_run_dynamic();"
        
    print "}"
)m4_dnl

/**************** BEGIN static schedule ************************/


namespace {

void LSEfw_DUMMY_END(LSEfw_stf_def int sno) { }

// Master single-threaded schedule
m4_pythonfile(
fr = not LSE_db.getParmVal("LSE_DAP_schedule_oblivious")
if fr: forcenullstring = ", TRUE"
else: forcenullstring = ""

gen = 0
extras = send = recv = lock = 0
lastlocks = []
print "LSEfw_static_schedule_t LSEfw_thread_schedule[] = {"
for sp in LSE_db.block_sched_list:
        if fr:
            if sp.forceRun:
                forcestring = ", TRUE"
            else:
                forcestring = ", FALSE"
        else: forcestring = ""
        msg = reduce(lambda x,y:x+("%d " % y),sp.generates,"")
        if (sp.id > 1):
          gen += len(sp.generates)
        if msg: print """  /* generates %s*/""" % msg

        ct = LSE_db.cblocksList[sp.cno]
        inst = LSE_db.getInst(ct[1],1) 
        if ct[0] < LSEdb_CblockDynamic:
            print ("  {(LSEfw_firing_t)%s,%d,%d, " \
                   "(LSEfw_iptr_t)&%s%s /* %d */}," %
                   (LSEcg_codeblock_name(LSE_db,ct,1,1),ct[3],sp.cno,
                    LSEcg_instdata_name(inst.name), forcestring,
                    sp.id))
        else:
            print ("  {(LSEfw_firing_t)%s,%d,%d, NULL%s /* %d */}," %
                   (LSEcg_codeblock_name(LSE_db,ct,1,1),ct[3],sp.cno,
                    forcestring, sp.id))

tl = len(LSE_db.block_sched_list)
print "/* Total static length=%d generated signals=%d */" % (tl,gen)

# ensure we do not create an empty array.... DO NOT DELETE, as the code
# explicitly ignores the last element of the array
print ("  {(LSEfw_firing_t)LSEfw_DYNSECT__DYN___0,0,0, NULL}," )

print "};"

print "LSEfw_static_end_t LSEfw_thread_end_schedule[] = {" 
for sp in LSE_db.end_bsl:
            ct = LSE_db.cblocksList[sp.cno]
            inst = LSE_db.getInst(ct[1],1)
            print ("  {(LSEfw_end_func_t)%s, 0, (LSEfw_iptr_t)&%s /* %d */}," %
                   (LSEcg_codeblock_name(LSE_db,ct,1,1),
		    LSEcg_instdata_name(inst.name), sp.id))

tl = len(LSE_db.end_bsl)
print "/* Total static end length=%d */" % tl

# ensure we do not create an empty array.... DO NOT DELETE, as the code
# explicitly ignores the last element of the array
print ("  {LSEfw_DUMMY_END, 0, NULL },")

print "};"
)

/********************* scheduler node information **********************/

struct LSEfw_sched_info_t {
  int id;
  int inameNo;
  int cno;
  int ctype;
  const char *iname;
  const char *pname;
  int instno;
} bsl_info[] = {
  m4_pythonfile(
inst2id = {}
for sp in LSE_db.block_sched_list:
   cb = LSE_db.cblocksList[sp.cno]
   if cb[0] == LSEdb_CblockDynamic: iid = -1
   else:
     inst2id.setdefault(cb[1],len(inst2id))
     iid = inst2id[cb[1]]
   print """{ %d, %d, %d, %d, "%s", "%s", %d },""" % \
              (sp.id, iid, sp.cno, cb[0], cb[1], cb[2], cb[3])
)
};

LSEfw_sched_info_t end_bsl_info[] = {
  m4_pythonfile(
for sp in LSE_db.end_bsl:
   cb = LSE_db.cblocksList[sp.cno]
   if cb[0] == LSEdb_CblockDynamic: iid = -1
   else:
     inst2id.setdefault(cb[1],len(inst2id))
     iid = inst2id[cb[1]]
   print """{ %d, %d, %d, %d, "%s", "%s", %d },""" % \
              (sp.id, iid, sp.cno, cb[0], cb[1], cb[2], cb[3])
)
};

/************************* Invocation timing records ******************/

m4_pythonfile(
bss = max(1,len(LSE_db.block_sched_list))
ebss = max(1,len(LSE_db.end_bsl))
print "LSEfw_timing_record_t LSEfw_phase_timings[%d] = {};" % bss
print "LSEfw_timing_record_t LSEfw_end_timings[%d] = {};" % ebss
)

} // anonymous namespace

/**************** master signal checker table *************************/


namespace {

#ifdef LSEfw_check_ports
m4_pythonfile(
names=["local","global"]
values=[0001,0100,0010,0001,0100,0010,0,0001,0100,0010,0,0,0,0]
if 1:
    i = 0
    checkno = 0
    realcheckno = 0
    print "LSEfw_signal_checker_t LSEfw_signal_checker[] = {"
    for sp in LSE_db.block_sched_list:
        # TODO: make understand multi-threading
        for sno in sp.final:
          sig = LSE_db.signalList[sno]
          val = values[sig.type]
          checkno += 1
          if val and sig.dropreason != LSEdb_sigdropped_notused:
              realcheckno += 1
              print "{ %d, %d, %d, &%s[%d].%s, \"%s\", 0%o }," % \
                  (i,sno,i,LSEcg_portstatus_name(sig.inst,sig.port,0),
                   sig.instno,names[sig.isGlobal], LSE_db.sigName(sno),
                   val)
        i += 1
    print "/* Total checked signals: %d real / %d marked */" % \
            (realcheckno,checkno)
    print "{ -1, 0, 0, NULL, \"\", 0 } };"
)
#else
LSEfw_signal_checker_t LSEfw_signal_checker[] = { {-1, 0, 0, NULL, "", 0 } };
#endif

} // anonymous namespace



/**************** BEGIN end functions ************************/

  /***** Port status printing *****/

typedef struct LSEfw_show_port_status_s {
  int rtype; /* 0 = instances, 1 = port, 2 = global, 3 = local, 4 = value,
	      * 5 = newline */
  void *item1, *item2;
} LSEfw_show_port_status_t;

namespace {

LSEfw_show_port_status_t LSEfw_show_port[] = {

m4_pythonfile(if 1:
    slist = LSE_db.instanceOrder * 1
    slist.sort(lambda x,y:cmp(x.name,y.name))
    for inst in slist:
        print """{ 0, (void *)"%s", NULL },""" % inst.name
        fname = LSEcg_flatten(inst.name)
        for p in inst.portOrder:
            if not p.width: continue
            print """{ 1, (void *)"%s", NULL }, """ % p.name
            if (not p.controlEmpty):
                print "{ 3, NULL, NULL }, "
                for i in range(p.width):
                    print "{ 4, (void *)&%s[%d].local, " \
                          "(void *)&LSEfw_PORT_ignore_status.%s[%d].local "\
                          "}," \
                          % (LSEcg_portstatus_name(inst.name,p.name,0),i,
                             LSEcg_portstatus_field_name(inst.name,p.name),i)
            print "{ 2, NULL, NULL }, "
            for i in range(p.width):
                print "{ 4, (void *)&%s[%d].global, " \
                      "(void *)&LSEfw_PORT_ignore_status.%s[%d].global }," \
                      % (LSEcg_portstatus_name(inst.name,p.name,0),i,
                         LSEcg_portstatus_field_name(inst.name,p.name),i)
)
  { 5, NULL, NULL },
  { 0, NULL, NULL },
};
} // anonymous namespace

void LSEfw_show_port_status(void)
{
  unsigned int i;
  LSE_signal_t st;
  for (i=0 ; 
       i < sizeof(LSEfw_show_port) / sizeof(LSEfw_show_port_status_t) - 1;
       i++) {
    switch (LSEfw_show_port[i].rtype) {
    case 0: /* instance name */
      fprintf(LSE_stderr,"\nInstance %s:", (char *)(LSEfw_show_port[i].item1));
      break;
    case 1: /* port name */
      fprintf(LSE_stderr,"\n  Port %s:", (char *)(LSEfw_show_port[i].item1));
      break;
    case 2: /* global */
      fprintf(LSE_stderr,"\n\t%-8s:","global");
      break;
    case 3: /* local */
      fprintf(LSE_stderr,"\n\t%-8s:","local");
      break;
    case 4: /* value */
      st = ( LSEfw_readstatus(*((LSE_signal_t *)(LSEfw_show_port[i].item1))) |
	     (*((LSE_signal_t *)(LSEfw_show_port[i].item2))) );
      LSE_signal_print(LSE_stderr, st);
      fprintf(LSE_stderr,",");
      break;
    default:
      fprintf(LSE_stderr,"\n");
      break;
    }
  }
}

  /**** end_of_timestep/phase_end calls ****/

#define LSEfw_num_signals_to_resolve m4_pythonfile(
# how many signals must be resolved
scount = 0
for i in LSE_db.instanceOrder:
    for p in i.portOrder:
	if not p.width: continue
	if p.dir=="output":
            for pi in range(p.width):
                cil = p.connInfoList[pi]
                conval = p.globalConstants[pi]
		if cil[LSEdb_cil_InstName]:
	    	    if (not cil[LSEdb_cil_IgnoreMask]&1) and \
		       (not cil[LSEdb_cil_ConstantMask]&1): 
			scount = scount + 1
	    	    if (not cil[LSEdb_cil_IgnoreMask]&2) and \
		       (not cil[LSEdb_cil_ConstantMask]&2): 
			scount = scount + 1
                    if (not conval[0]) != (not (cil[LSEdb_cil_ConstantMask]&1)):
                        sys.stderr.write("Mismatch! %s %s %d data %d %d\n" % \
                                         (i.name,p.name,pi,conval[0],
                                          cil[LSEdb_cil_ConstantMask]))
                        SystemExit(1)
                    if (not conval[1]) != (not (cil[LSEdb_cil_ConstantMask]&2)):
                        sys.stderr.write("Mismatch! %s %s %d en %d %d\n" % \
                                         (i.name,p.name,pi,conval[1],
                                          cil[LSEdb_cil_ConstantMask]))
                        SystemExit(1)
	else:
            for pi in range(p.width):
                cil = p.connInfoList[pi]
                conval = p.globalConstants[pi]
		if cil[LSEdb_cil_InstName]: 
	    	    if (not cil[LSEdb_cil_IgnoreMask]&4) and \
		       (not cil[LSEdb_cil_ConstantMask]&4): 
			scount = scount + 1
                    if (not conval[2]) != (not (cil[LSEdb_cil_ConstantMask]&4)):
                        sys.stderr.write("Mismatch! %s %s %d ack %d %d\n" % \
                                         (i.name,p.name,pi,conval[2],
                                          cil[LSEdb_cil_ConstantMask]))
                        SystemExit(1)
print scount
)

inline boolean LSEfw_run_unknown_checks(void) {
  int i, uc;
    
  /* check for unknown ports */

  if (LSEfw_PARM(LSE_check_ports_for_unknown)) {
   /* perform reduction across unknown signals; as each thread will have
    * counted down from 0, we simply add them together and hope we 
    * get 0 in the end... */
   uc = LSEfw_num_signals_to_resolve;
   for (i = 0 ; i < LSEfw_num_threads ; i++) {
     uc += *LSEfw_shthreads[i].unknown_signal_countP;
   }
   if (LSEfw_PARM(LSE_check_ports_trace_resolution) && uc != 0) {
	int j;
	fprintf(LSE_stderr,"UC " LSE_time_print_args(LSE_time_now));
	fprintf(LSE_stderr," %d\n", uc);
	for (i = 0 ; i < LSEfw_num_threads ; i++) {
	   fprintf(LSE_stderr,"SC " LSE_time_print_args(LSE_time_now));
	   fprintf(LSE_stderr, " %d %d\n", i, 
		   -*LSEfw_shthreads[i].unknown_signal_countP);
	   for (j = 0; j < -*LSEfw_shthreads[i].unknown_signal_countP; j ++) {
	     if (LSEfw_shthreads[i].sigs_resolved[j] >= 0)
	       fprintf(LSE_stderr,"SR %d %d\n", i, 
	               LSEfw_shthreads[i].sigs_resolved[j]);
	   }
	}
   }
   return (uc != 0);
  }
  return FALSE;
}

inline void LSEfw_do_port_check(LSEfw_signal_checker_t *signal_checker,
				int idno,
				unsigned int sp, unsigned int &scp) {
#ifdef LSEfw_check_ports
  if (LSEfw_PARM(LSE_check_ports_incrementally)) {
    /* do checking for correct results */
    while (signal_checker[scp].goodat == (int)sp) { 
      /* will not run off because of sentinel value */
      if (!((LSEfw_readstatus(*(signal_checker[scp].p))) & 
	    (signal_checker[scp].mask))) {
	LSEfw_sigcheck_mutex.lock();
	fprintf(LSE_stderr,
		"Signal %s(%d) not resolved at step %d(%d) "
		"of the schedule in thread %d\n",
		signal_checker[scp].s,
		signal_checker[scp].sno,
		signal_checker[scp].goodat,
		signal_checker[scp].sloc, idno);
	LSE_sim_terminate_now = 1;
	LSEfw_sigcheck_mutex.unlock();
      }
      scp++;
    } /* while */
  } /* if */
#endif
} // LSEfw_do_port_check

/************** thread functions ********************************/

#if (LSEfw_PARM(LSE_mp_multithreaded))

void *LSEfw_thread_func(void *arg) {
  struct LSEfw_sharedthread_info_t *threadp
                            =(struct LSEfw_sharedthread_info_t *)(arg);
  unsigned int sp, spe;
  unsigned int scp=0;
  int tp;
  LSEfw_iptr_t LSEfw_iptr;

  LSEfw_schedule_calling_cblock = 0;
  LSEdy_dynid_free_list = 0;
  LSEdy_dynid_master_list = 0;
  threadp->LSEdy_dynid_master_listP = &LSEdy_dynid_master_list;
  threadp->unknown_signal_countP = &unknown_signal_count;
 reload:			

  sigs_resolved = threadp->sigs_resolved;
  unsigned int schedule_length = threadp->schedule_length;
  LSEfw_static_schedule_t *thread_schedule = threadp->thread_schedule;
  unsigned int end_schedule_length = threadp->end_schedule_length;
  LSEfw_static_end_t *thread_end_schedule = threadp->thread_end_schedule;
  LSEfw_signal_checker_t *signal_checker = threadp->signal_checker;

  /* do the phase stuff */

  while(1) {
    /* barrier at entry to phase; also place where we kill stuff... */
    LSEfw_mainloop_barrier.wait();
 
    /* look at commands */
    if (LSEfw_info.thread_command == LSEfw_thread_command_Finish) return NULL;

    else if (LSEfw_info.thread_command == LSEfw_thread_command_MemReclaim) {
       LSE_memory_reclaim_slave(threadp->idno);
       LSEfw_status_barrier.wait(); /* reuse this barrier */
	/* need two barriers so mainloop barrier can be reset in between */
       LSEfw_phase_barrier.wait(); /* reuse this barrier */
       continue;
    } else if (LSEfw_info.thread_command == LSEfw_thread_command_Reschedule) {
      LSEfw_status_barrier.wait(); /* reuse this barrier */
      /* need two barriers so mainloop barrier can be reset in between */
      LSEfw_phase_barrier.wait(); /* reuse this barrier */
      goto reload;
    }

#if (LSEfw_PARM_constant(LSE_check_ports_for_unknown) && \
     LSEfw_PARM(LSE_check_ports_for_unknown))
    unknown_signal_count = 0;
#endif

    /* reset signals */
    for (sp = threadp->firstsig, spe=threadp->lastsig; 
         sp < spe; ++sp) {
      LSEfw_syncsig_reset(&LSEfw_sync_signals[sp]);
    }

    LSEfw_status_barrier.wait();

    for (sp = 0; /* should be 0 for all threads but first */
         sp < schedule_length; ++sp) {

      tp = thread_schedule[sp].cblockno;

#if (!LSEfw_PARM(LSE_DAP_schedule_oblivious))
      if (!thread_schedule[sp].forcerun &&
          !LSEfw_schedule_flags[tp]) goto aftercall;
      LSEfw_schedule_flags[tp] = 0;
#elif (LSEfw_PARM(LSE_DAP_static_schedule_type)==1 ||\
       LSEfw_PARM(LSE_DAP_static_schedule_type)==2)
      LSEfw_schedule_flags[tp] = 0;
#endif
      
      LSEfw_schedule_calling_cblock = tp;

      if (LSEfw_PARM(LSE_debug_codeblock_calls))
        fprintf(LSE_stderr,"Statically calling code block %%d @ %(tn)d/%%d\\n",
		tp,sp);
      if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram))
        LSEfw_cblock_histogram[tp]++;
      LSEfw_iptr = thread_schedule[sp].ptr;
      (*(thread_schedule[sp].cont))(LSEfw_stf_call 
					  thread_schedule[sp].instno);

#if (!LSEfw_PARM(LSE_DAP_schedule_oblivious))
    aftercall: ;
#endif
    
      LSEfw_do_port_check(signal_checker, threadp->idno, sp, scp);

    } /* phase schedule */

    /* in case there is a scheduled query in phase end....  */
    LSEfw_schedule_calling_cblock = 0;

    LSEfw_phase_barrier.wait();

    /* do phase end stuff */
    
    for (sp = 0; sp < end_schedule_length ; ++sp) {
       LSEfw_iptr = thread_end_schedule[sp].ptr;
       (*(thread_end_schedule[sp].cont))(LSEfw_stf_call
					 thread_end_schedule[sp].instno);
    }

    /* and report that we finished our work... */
    LSEfw_phase_end_barrier.nowait();

  }  // while(1)
}

#endif

/************** And now for the main loop ***********************/

void LSEfw_do_scheduling();
void LSEfw_undo_scheduling();
bool LSEfw_get_training_data(int);

namespace {

#if (LSEfw_PARM(LSE_DAP_record_invocation_timing))
  std::fstream LSEfw_timing_file;
#endif

  // IMPORTANT: for good performance, always call actual_timestep with
  // a constant parameter and inline it.
  // And never do mp and timing together
inline void actual_timestep(bool dotiming, bool domp) {

  struct LSEfw_sharedthread_info_t *threadp = &LSEfw_shthreads[0];

  LSEfw_iptr_t LSEfw_iptr;
  boolean had_unknown;
  
  unsigned int schedule_length;
  LSEfw_static_schedule_t *thread_schedule;
  unsigned int end_schedule_length;
  LSEfw_static_end_t *thread_end_schedule;
  LSEfw_signal_checker_t *signal_checker;

  if (LSEfw_PARM(LSE_mp_multithreaded) && domp) {
    schedule_length = threadp->schedule_length;
    thread_schedule = threadp->thread_schedule;
    end_schedule_length = threadp->end_schedule_length;
    thread_end_schedule = threadp->thread_end_schedule;
    signal_checker = threadp->signal_checker;
  } else {
    schedule_length = (sizeof(LSEfw_thread_schedule)/
		       sizeof(LSEfw_static_schedule_t)-1);
    thread_schedule = LSEfw_thread_schedule;
    end_schedule_length = (sizeof(LSEfw_thread_end_schedule) /
			   sizeof(LSEfw_static_end_t) - 1);
    thread_end_schedule = LSEfw_thread_end_schedule;
    signal_checker = LSEfw_signal_checker;
  }

m4_pythonfile(
if LSE_db.getParmVal("LSE_debug_gen_codeblock_histogram") or \
   LSEpy_timing_needed:

  if LSE_db.getParmVal("LSE_mp_multithreaded"):
      print "LSEfw_info.timesteps++;"
  else:
      print """
      if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram) || %s) 
        LSEfw_info.timesteps++;""" % LSEpy_timing_needed
elif LSE_db.getParmVal("LSE_mp_multithreaded"):
  print "LSEfw_info.timesteps ^= 1;"
)

  /* Set up the things that always run. */
#if (m4_python(print "%d" % LSEpy_has_dynamic))
  LSEfw_schedule_counter(LSEfw_schedule_timestep) = 0;
  LSEfw_schedule_pointer(LSEfw_schedule_timestep) = 0;
#endif
  
#if (LSEfw_PARM_constant(LSE_check_ports_for_unknown) && \
     LSEfw_PARM(LSE_check_ports_for_unknown))
  unknown_signal_count = 0;
#endif

#if (LSEfw_PARM(LSE_schedule_style_firing) && \
     !LSEfw_PARM(LSE_mp_multithreaded))
  LSEfw_schedule_in_phase_start = TRUE;
#endif

  /* reset barriers */
  if (domp) {
    LSEfw_status_barrier.reset();
    LSEfw_phase_end_barrier.reset();
  }

  m4_pythonfile(
def doit(dinst): print "LSE_domain_hook(start_of_timestep)();"
LSEcg_domain_hook_loop("start_of_timestep", doit, doit, LSE_db)
)

#if (LSEfw_PARM(LSE_DAP_dynamic_schedule_type)!=0)
  LSEfw_schedule_LEClevel = 0;
#endif
#if (LSEfw_PARM(LSE_DAP_static_schedule_type)==1||\
     LSEfw_PARM(LSE_DAP_static_schedule_type)==2)
  LSEfw_schedule_in_acyclic_finish = FALSE;
#endif

  /* let everyone go to work */

  if (LSEfw_PARM(LSE_debug_codeblock_calls)) {
    fprintf(LSE_stderr,"time=" LSE_time_print_args(LSE_time_now));
    fprintf(LSE_stderr,"\n");
  }
  if (domp) {
    LSEfw_mainloop_barrier.nowait();
    LSEfw_phase_barrier.reset();

#if (LSEfw_PARM(LSE_mp_multithreaded))
    /* reset signals */
    for (unsigned int i = threadp->firstsig, ie = threadp->lastsig; 
	 i != ie ; ++i) {
      LSEfw_syncsig_reset(&LSEfw_sync_signals[i]);
    }
#endif
  }

  m4_pythonfile(if 1:
    # run the top-level event; it should NOT be requiring any
    # port queries as start_of_timestep is not a legal time to do such
    # evil things, thus it can be done in parallel with status copies
    # in the other threads and save us a condition variable
    ev = LSEpy_instance.events.get("start_of_timestep")
    if ev and ev.isFilled:
	print "LSEfw_iptr = &%s;" % LSEcg_instdata_name(LSEpy_instance.name)
	print LSEcg_event_name(ev,"record",1) + "(LSEfw_stf_call0_void);"
)

  if (domp) {
    LSEfw_status_barrier.wait();
    LSEfw_mainloop_barrier.reset();
  }

#if (LSEfw_PARM(LSE_schedule_style_firing) && \
    !LSEfw_PARM(LSE_mp_multithreaded))
  LSEfw_schedule_in_phase_start = FALSE;
#endif

  unsigned int scp=0; /* signal check pointer */

  /* now run our schedule */

  for (unsigned int sp = 1, spe = schedule_length ; sp != spe ; ++sp) {

    int tp;
    tp = thread_schedule[sp].cblockno;

#if (!LSEfw_PARM(LSE_DAP_schedule_oblivious))
      if (!thread_schedule[sp].forcerun &&
          !LSEfw_schedule_flags[tp]) goto aftercall;
      LSEfw_schedule_flags[tp] = 0;
#elif (LSEfw_PARM(LSE_DAP_static_schedule_type)==1 ||\
       LSEfw_PARM(LSE_DAP_static_schedule_type)==2)
      LSEfw_schedule_flags[tp] = 0;
#endif

    LSEfw_schedule_calling_cblock = tp;

    if (LSEfw_PARM(LSE_debug_codeblock_calls))
      fprintf(LSE_stderr,"Statically calling code block %d @ 0/%d\n",tp,sp);
    if (LSEfw_PARM(LSE_debug_gen_codeblock_histogram))
	LSEfw_cblock_histogram[tp]++;
    LSEfw_iptr = thread_schedule[sp].ptr;

    if (dotiming) {
      uint64_t ccount, sccount, span;
      sccount = read_timer();
      (*(thread_schedule[sp].cont))(LSEfw_stf_call0 
				    thread_schedule[sp].instno);
      ccount = read_timer();
      span = ccount - sccount;
      LSEfw_phase_timings[sp].clocks += span;
      LSEfw_phase_timings[sp].clocks2 += span*span;
      if (span > LSEfw_phase_timings[sp].maxclocks) {
	LSEfw_phase_timings[sp].maxclocks = span;
	LSEfw_phase_timings[sp].maxclocks2 = span*span;
      }	
    } else
      (*(thread_schedule[sp].cont))(LSEfw_stf_call0 
				    thread_schedule[sp].instno);
    
#if (!LSEfw_PARM(LSE_DAP_schedule_oblivious))
  aftercall: ;
#endif

    LSEfw_do_port_check(signal_checker, 0, sp, scp);

  } // main schedule loop
  
  /* in case there is a scheduled query in phase end....  */
  LSEfw_schedule_calling_cblock = 0;

  /* wait for every thread to finish phase and let them go right on
   * into phase_end */
  if (domp) {
    LSEfw_phase_barrier.wait();
  }

    /* clear any scheduling flags left over */
#if (m4_python(print "%d" % LSEpy_has_dynamic))
  LSEfw_clear_flags();
#endif

  /* fprintf(LSE_stderr,"===>Reached %d\n",LSEfw_schedule_timestep_counter); */

  /* we run the unknown checks here rather than between cycles to catch
   * cases where the user mistakenly sets signals in phase_end.  Actually,
   * there is a race condition on the check, as the other threads will go
   * on into phase_end, but we do not care too much about that.
   */
  had_unknown = LSEfw_run_unknown_checks();

  // Do phase end
  for (int sp = 0, spe = end_schedule_length; sp != spe ; ++sp) {
    LSEfw_iptr = thread_end_schedule[sp].ptr;

    if (dotiming) {
      uint64_t ccount, sccount, span;
      sccount = read_timer();
       (*(thread_end_schedule[sp].cont))(LSEfw_stf_call0 
					  thread_end_schedule[sp].instno);
       ccount = read_timer();
       span = ccount - sccount;
       LSEfw_end_timings[sp].clocks += span;
       LSEfw_end_timings[sp].clocks2 += span * span;
       if (span > LSEfw_end_timings[sp].maxclocks) {
	 LSEfw_end_timings[sp].maxclocks = span;
	 LSEfw_end_timings[sp].maxclocks2 = span*span;
       }
    } else 
      (*(thread_end_schedule[sp].cont))(LSEfw_stf_call0 
					thread_end_schedule[sp].instno);
  }

  if (domp) {
    LSEfw_phase_end_barrier.wait();
  }

  /* and do stuff only thread0 can do.... */

  m4_pythonfile(if 1:
    # run the top-level event
    ev = LSEpy_instance.events.get("end_of_timestep")
    if ev and ev.isFilled:
	print "LSEfw_iptr = &%s;" % LSEcg_instdata_name(LSEpy_instance.name)
	print LSEcg_event_name(ev,"record",1) + "(LSEfw_stf_call0_void);"
)
  
  if (LSEfw_PARM(LSE_show_port_statuses))
    if((LSE_time_ge(LSE_time_now,
	LSE_time_construct(LSEfw_PARM(LSE_show_port_statuses_start_cycle),
                           LSEfw_PARM(LSE_show_port_statuses_start_phase))) &&
      LSE_time_le(LSE_time_now,
	LSE_time_construct(LSEfw_PARM(LSE_show_port_statuses_end_cycle),
                           LSEfw_PARM(LSE_show_port_statuses_end_phase)))) ||
     LSE_time_eq(LSE_time_now,LSE_time_construct(-1,-1))) {
      fprintf(LSE_stderr,"==== Port status for time ");
      fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));	
      fprintf(LSE_stderr," ====\n");
      LSEfw_show_port_status();
    }

  /* use the results of the unknown checks */
  if (LSEfw_PARM(LSE_check_ports_for_unknown)) {
    if (had_unknown) {
      LSE_sim_terminate_now = -3;
      fprintf(LSE_stderr,"Unknown port status at time ");
      fprintf(LSE_stderr,LSE_time_print_args(LSE_time_now));
      fprintf(LSE_stderr,"\n");
      fprintf(LSE_stderr,"==== Dumping port status ====\n");
      LSEfw_show_port_status();	
    }
  }

  m4_pythonfile(
def doit(dinst): print "LSE_domain_hook(end_of_timestep)();"
LSEcg_domain_hook_loop("end_of_timestep", doit, doit, LSE_db)
)

  /* garbage-collect the dynids and resolutions */ 
  if (!(LSE_time_ticks(LSE_time_now) % 
      LSEfw_PARM(LSE_garbage_collection_interval))) {
    LSE_memory_reclaim_master();
  }

  LSE_time_now = LSE_time_add(LSE_time_now, LSE_time_construct(0,1));
  LSEfw_signal_time_mask ^= 12;

#if (LSEfw_PARM(LSE_DAP_record_invocation_timing))
  if (!(LSE_time_now % 1024)) {
    LSEfw_timing_file.write((char *)&LSEfw_phase_timings[0], 
			    sizeof(LSEfw_phase_timings));
    LSEfw_timing_file.write((char *)&LSEfw_end_timings[0], 
			    sizeof(LSEfw_end_timings));
  }
#endif
} // actual timestep

} // anonymous namespace

extern "C" int LSE_sim_engine(void) {
  int i;

#if (LSEfw_PARM(LSE_DAP_record_invocation_timing))
  LSEfw_timing_file.open("timing.txt", 
			 std::ios_base::out | std::ios_base::trunc);
  for (i=1 ; i < m4_python(len(LSE_db.block_sched_list)) ; ++i) {
    LSEfw_timing_file << "M" << bsl_info[i].id 
		      << "," << bsl_info[i].cno 
		      << "," << bsl_info[i].ctype 
		      << "," << bsl_info[i].iname 
		      << "," << bsl_info[i].pname 
		      << "," << bsl_info[i].instno << "\n"; 
  }
  for (i=0 ; i < m4_python(len(LSE_db.end_bsl)) ; ++i) {
    LSEfw_timing_file << "E" << end_bsl_info[i].id 
		      << "," << end_bsl_info[i].cno 
		      << "," << end_bsl_info[i].ctype 
		      << "," << end_bsl_info[i].iname 
		      << "," << end_bsl_info[i].pname 
		      << "," << end_bsl_info[i].instno << "\n"; 
  }
  LSEfw_timing_file.close();

  LSEfw_timing_file.open("timing.dat", 
			 std::ios_base::out | std::ios_base::trunc | 
			 std::ios_base::binary);
  int t1 = sizeof(LSEfw_phase_timings)/4/sizeof(uint64_t);
  LSEfw_timing_file.write((char *)&t1, sizeof(t1));
  t1 = sizeof(LSEfw_end_timings)/4/sizeof(uint64_t);
  LSEfw_timing_file.write((char *)&t1, sizeof(t1));
#endif
 
  // measure time
  struct timeval start, end;
  gettimeofday(&start, 0);

  // Now the execution state machine.  There are four states:
  // training and final     - measure timing, stay in state always
  // not training and final - do not measure timing, stay in state always 
  // training and not final - measure timing, leave when exit condition met
  // not training and final - do not measure timing, leave at next training

#if (LSEfw_PARM(LSE_mp_multithreaded))
  if (LSEfw_threadingParms.trainingLength || 
      LSEfw_threadingParms.trainingConvergence != 0)
    goto initialnottrainingnotfinal;
  else goto nottrainingfinal;
#endif

#if (m4_python(print LSEpy_timing_needed) && !LSEfw_PARM(LSE_mp_multithreaded))
  // trainingfinal:
  while (!LSE_sim_terminate_now && LSE_sim_terminate_count) {
    actual_timestep(true, false);
  }
  goto done;
#else

 nottrainingfinal:
  while (!LSE_sim_terminate_now && LSE_sim_terminate_count) {
    actual_timestep(false, true);
  }
  goto done;

#endif

#if (LSEfw_PARM(LSE_mp_multithreaded)) 
 trainingnotfinal:
  { 
    // TODO: handle adaptive training
    int tcount = LSEfw_threadingParms.trainingLength;
    while (!LSE_sim_terminate_now && LSE_sim_terminate_count && tcount) {
      actual_timestep(true, false);
      tcount--;
    }
    if (LSE_sim_terminate_now || !LSE_sim_terminate_count) goto done;

    gettimeofday(&end, 0);
    LSE_sim_wall_time_elapsed += ((uint64_t)end.tv_sec*1000000 + end.tv_usec - 
				  (uint64_t)start.tv_sec*1000000 - 
				  start.tv_usec);

    std::cerr << "Ending training at " << LSE_time_now << "\n";
   
    if (LSEfw_get_training_data(LSEfw_threadingParms.trainingLength)) {

      LSEfw_undo_scheduling();
      LSEfw_do_scheduling();

      LSEfw_info.thread_command = LSEfw_thread_command_Reschedule;
      LSEfw_status_barrier.reset(); /* not yet reset */
      LSEfw_mainloop_barrier.nowait(); 
      LSEfw_status_barrier.wait();  
      LSEfw_mainloop_barrier.reset();
      LSEfw_phase_barrier.wait();  
      LSEfw_info.thread_command = LSEfw_thread_command_None;
    }
    gettimeofday(&start, 0);
    // TODO: handle multiple training periods
    goto nottrainingfinal;
  }

 initialnottrainingnotfinal:
  {
    int tcount = 10; // temporary
    while (!LSE_sim_terminate_now && LSE_sim_terminate_count && tcount) {
      actual_timestep(false, false);
      --tcount;
    }
    if (LSE_sim_terminate_now || !LSE_sim_terminate_count) goto done;
    goto trainingnotfinal;
  }

 nottrainingnotfinal:
  {
    while (!LSE_sim_terminate_now && LSE_sim_terminate_count) {
      actual_timestep(false, true);
    }
    if (LSE_sim_terminate_now || !LSE_sim_terminate_count) goto done;
    goto trainingnotfinal; // temporary
  }
#endif

 done:
  gettimeofday(&end, 0);
  
  LSE_sim_wall_time_elapsed += ((uint64_t)end.tv_sec*1000000 + end.tv_usec - 
				(uint64_t)start.tv_sec*1000000 - start.tv_usec);

m4_pythonfile(
if LSE_db.getParmVal("LSE_DAP_record_invocation_timing"):
    print r"""
    fprintf(LSE_stderr,"Invocation Stats\n");
    fprintf(LSE_stderr,"--------------------------------------------\n");"""
    print """for (i=1;i<%d;i++) {""" % len(LSE_db.block_sched_list)
    print r"""
      double av = (double)LSEfw_phase_timings[i].clocks / LSEfw_info.timesteps;
      fprintf(LSE_stderr,
              "M%d,%d,%d,%s,%s,%d,%8.0f\n",
              bsl_info[i].id,
              bsl_info[i].cno,
              bsl_info[i].ctype,
              bsl_info[i].iname,
              bsl_info[i].pname,
              bsl_info[i].instno,
	      av);
    }
    """
    print """for (i=0;i<%d;i++) {""" % len(LSE_db.end_bsl)
    print r"""
      double av = (double)LSEfw_end_timings[i].clocks / LSEfw_info.timesteps;
      fprintf(LSE_stderr,
              "E%d,%d,%d,%s,%s,%d,%8.0f\n",
              end_bsl_info[i].id,
              end_bsl_info[i].cno,
              end_bsl_info[i].ctype,
              end_bsl_info[i].iname,
              end_bsl_info[i].pname,
              end_bsl_info[i].instno,
	      av);
    }
    fprintf(LSE_stderr,"--------------------------------------------\n");"""
)

#if (LSEfw_PARM(LSE_DAP_record_invocation_timing))
  LSEfw_timing_file.close();
#endif

  return LSE_sim_terminate_now < 0 ? LSE_sim_terminate_now : 1;
}

extern void LSEfw_no_more_time(void);

extern "C" int LSE_sim_do_timestep(void) {
  if (!LSE_sim_terminate_now && LSE_sim_terminate_count) {
    actual_timestep(false, false);
    return 0;
  } else 
    return LSE_sim_terminate_now < 0 ? LSE_sim_terminate_now : 1;
}

/********************** MP scheduling ****************************/

#if (LSEfw_PARM(LSE_mp_multithreaded)) 

namespace {

  int db_bsg_edges[] = {
m4_pythonfile(
vals=(1,0,0,1,0,0,0,1,0,0,0,0,0,0,0)
areacks=(0,0,1,0,0,1,0,0,0,1,0,0,0,0)
for sp in LSE_db.block_sched_list:
    saw = {}
    for d in sp.deps: 
        if saw.has_key(d): continue
        saw[d] = 1
	sys.stdout.write("%d," % LSE_db.block_sched_graph[d].singleOrder)
	cnt = 0
        types=["4"]
	good = (LSE_db.cblocksList[LSE_db.block_sched_graph[d].cno][0]!=3)
	for s in LSE_db.block_sched_graph[d].generates:
	   if s in LSE_db.cblocksSensInputSignals[sp.cno]: 
	      cnt += 1
	      sig = LSE_db.signalList[s]
	      sval=vals[sig.type]
	      if vals[sig.type]: 
	        types.append("sizeof(%s)" % 
			     LSE_db.instances[sig.inst].ports[sig.port].type)
	      else: types.append("4")
        sys.stdout.write("%d," % cnt)
	sys.stdout.write(reduce(lambda x,y: x + "+" + y, types)+",")
	sys.stdout.write("%d," % good)
    print "-1, /* %d */ " % sp.singleOrder
)
  };

  int db_end_bsg_edges[] = {
m4_pythonfile(
for sp in LSE_db.end_bsl:
    saw = {}
    for d in sp.deps: 
        if saw.has_key(d): continue
        saw[d] = 1
        sys.stdout.write("%d," % LSE_db.end_bsg[d].singleOrder)
	cnt = 0
        types=["4"]
	for s in LSE_db.block_sched_graph[d].generates:
	   if s in LSE_db.cblocksSensInputSignals[sp.cno]: 
	      cnt += 1
	      sig = LSE_db.signalList[s]
	      sval=vals[sig.type]
	      if vals[sig.type]: 
	        types.append("sizeof(%s)" % 
			     LSE_db.instances[sig.inst].ports[sig.port].type)
	      else: types.append("4")
        sys.stdout.write("%d," % cnt)
	sys.stdout.write(reduce(lambda x,y: x + "+" + y, types) + ",")
    print "-1, /* %d */ " % sp.singleOrder
)
  };

  struct pconflict_t { int from, to; } pconflicts[] = {
m4_pythonfile(
for pc in LSE_db.potentialConflicts.items():
    if not pc[1]: continue
    print "\t{ %d, %d }," % (pc[0][0].singleOrder, pc[0][1].singleOrder)
)
  };

  struct pconflict_t end_pconflicts[] = {
m4_pythonfile(
for pc in LSE_db.end_potentialConflicts.items():
    if not pc[1]: continue
    print "\t{ %d, %d }," % (pc[0][0].singleOrder, pc[0][1].singleOrder)
)
  };

  struct mappair {
    int thread, offset;
  };

  typedef std::map<int, int> semmap_t;
  semmap_t semmap, end_semmap;

  void addlist(LSEfw_static_schedule_t *&sp,
	       const std::set<int> &s, LSEfw_firing_t func, bool remap=false) {
    for (std::set<int>::const_iterator k = s.begin(), ke = s.end() ; 
	 k != ke ; ++k) {
      sp->cont = func;
      sp->instno = remap ? semmap[*k] : *k;
      sp->cblockno = 0;
      sp->ptr = 0;
#if (!LSEfw_PARM(LSE_DAP_schedule_oblivious))
      sp->forcerun = 1;
#endif
      ++sp;
    }
  }

  void addlist(LSEfw_static_schedule_t *&sp,
	       const std::vector<int> &s, 
	       LSEfw_firing_t func, bool remap=false) {
    for (std::vector<int>::const_iterator k = s.begin(), ke = s.end() ; 
	 k != ke ; ++k) {
      sp->cont = func;
      sp->instno = remap ? semmap[*k] : *k;
      sp->cblockno = 0;
      sp->ptr = 0;
#if (!LSEfw_PARM(LSE_DAP_schedule_oblivious))
      sp->forcerun = 1;
#endif
      ++sp;
    }
  }

  void addlist(LSEfw_static_end_t *&sp,
	       const std::set<int> &s, LSEfw_end_func_t func, 
	       bool remap=false) {
    for (std::set<int>::const_iterator k = s.begin(), ke = s.end() ; 
	 k != ke ; ++k) {
      sp->cont = func;
      sp->instno = remap ? end_semmap[*k] : *k;
      sp->ptr = 0;
      ++sp;
    }
  }

  void addlist(LSEfw_static_end_t *&sp,
	       const std::vector<int> &s, LSEfw_end_func_t func, 
	       bool remap=false) {
    for (std::vector<int>::const_iterator k = s.begin(), ke = s.end() ; 
	 k != ke ; ++k) {
      sp->cont = func;
      sp->instno = remap ? end_semmap[*k] : *k;
      sp->ptr = 0;
      ++sp;
    }
  }

  inline bool compare_checkers(const LSEfw_signal_checker_t i, 
			       const LSEfw_signal_checker_t j)
  {
    return (i.goodat < j.goodat); 
  }

  using LSE_scheduler::TaskGraph;
  using LSE_scheduler::schednode_t;

  struct order_schedule {
    bool operator()(const schednode_t *x, const schednode_t *y) const {
      return x->getStartTime() < y->getStartTime();
    }
  };

  TaskGraph block_sched_graph;
  TaskGraph end_bsg;

} // anonymous namespace

#endif

void LSEfw_prepare_scheduling() {
#if (!LSEfw_PARM(LSE_mp_multithreaded)) 

  LSEfw_num_threads = 1;
  LSEfw_shthreads = new LSEfw_sharedthread_info_t[1];
  LSEfw_shthreads[0].thread_schedule = LSEfw_thread_schedule;
  LSEfw_shthreads[0].schedule_length = m4_python(len(LSE_db.block_sched_list));
  LSEfw_shthreads[0].thread_end_schedule = LSEfw_thread_end_schedule;
  LSEfw_shthreads[0].end_schedule_length = m4_python(len(LSE_db.end_bsl));
  LSEfw_shthreads[0].signal_checker = LSEfw_signal_checker;
  LSEfw_shthreads[0].firstsig = 0;
  LSEfw_shthreads[0].lastsig = 0;
  LSEfw_num_syncs = 0;
  LSEfw_num_locks = 0;
#else // multithreaded

  /*************** create the task graphs ********************/

  for (int i = 0; i < m4_python(len(LSE_db.block_sched_list)) ; ++i) {
    std::ostringstream os;
    os << bsl_info[i].iname << ":" << bsl_info[i].pname << ":"
       << bsl_info[i].instno << ":" << bsl_info[i].ctype;
    block_sched_graph.addTask(new schednode_t(bsl_info[i].id, i,
					      bsl_info[i].inameNo, os.str()));
  }
  for (int i=0,cnt=0; i < sizeof(db_bsg_edges)/sizeof(int); ++i) {
    if (db_bsg_edges[i] < 0) { cnt++; continue; }
    block_sched_graph.addEdge(db_bsg_edges[i],cnt,db_bsg_edges[i+2],
			      db_bsg_edges[i+3]);
    i+=3;
  }

  for (int i=0,cnt=0; i < sizeof(pconflicts)/sizeof(pconflict_t); ++i) {
    schednode_t &f
      = *static_cast<schednode_t *>(block_sched_graph.getTask(pconflicts[i].
							      from));
    schednode_t &t
      = *static_cast<schednode_t *>(block_sched_graph.getTask(pconflicts[i].
							      to));
    f.addLocalitySibling(&t);
    t.addLocalitySibling(&f);
  }

  for (int i = 0; i < m4_python(len(LSE_db.end_bsl)) ; ++i) {
    std::ostringstream os;
    os << end_bsl_info[i].iname << ":EOT";
    end_bsg.addTask(new schednode_t(end_bsl_info[i].id, i,
				    end_bsl_info[i].inameNo, os.str()));
  }
  for (int i=0,cnt=0; i < sizeof(db_end_bsg_edges)/sizeof(int); ++i) {

    if (db_end_bsg_edges[i] < 0) { cnt++; continue; }
    end_bsg.addEdge(db_end_bsg_edges[i],cnt,db_end_bsg_edges[i+2],0);
    i += 2;
  }

  for (int i=0,cnt=0; i < sizeof(end_pconflicts)/sizeof(pconflict_t); ++i) {
    schednode_t &f
      = *static_cast<schednode_t *>(end_bsg.getTask(end_pconflicts[i].from));
    schednode_t &t
      = *static_cast<schednode_t *>(end_bsg.getTask(end_pconflicts[i].to));
    f.addLocalitySibling(&t);
    t.addLocalitySibling(&f);
  }

  LSEfw_num_threads = LSEfw_threadingParms.numThreads;

  // allocate shared thread data
  LSEfw_shthreads = new LSEfw_sharedthread_info_t[LSEfw_num_threads];

  for (int i = 0; i < LSEfw_num_threads; ++i) {
    LSEfw_shthreads[i].thread_schedule = 0;
    LSEfw_shthreads[i].thread_end_schedule = 0;
    LSEfw_shthreads[i].signal_checker = 0;
  }
  LSEfw_cblock_mutexes = 0;
  LSEfw_sync_signals = 0;
  
#endif /* multithreaded */
}

/* Returns true if the training data has changed enough to reschedule */
bool LSEfw_get_training_data(int interval) {
#if (LSEfw_PARM(LSE_mp_multithreaded)) 

  // The +1 is incredibly important because the cross-communication calculations
  // go nuts if there is ever a zero-cost node.

  for (TaskGraph::iterator i=block_sched_graph.begin(),
	 ie = block_sched_graph.end(); i != ie ; ++i) {
    schednode_t &n= *(schednode_t *)*i;
    n.setCost( 1+ (LSEfw_phase_timings[n.singleOrder].clocks -
		LSEfw_phase_timings[n.singleOrder].maxclocks) / (interval - 1));
    LSEfw_phase_timings[n.singleOrder].clocks = 
      LSEfw_phase_timings[n.singleOrder].maxclocks = 
      LSEfw_phase_timings[n.singleOrder].clocks2 = 
      LSEfw_phase_timings[n.singleOrder].maxclocks2 = 0;
  }

  for (TaskGraph::iterator i=end_bsg.begin(),
	 ie = end_bsg.end(); i != ie ; ++i) {
    schednode_t &n= *(schednode_t *)*i;
    n.setCost( 1 + (LSEfw_end_timings[n.singleOrder].clocks -
		LSEfw_end_timings[n.singleOrder].maxclocks) / (interval - 1));
    LSEfw_end_timings[n.singleOrder].clocks = 
      LSEfw_end_timings[n.singleOrder].maxclocks = 
      LSEfw_end_timings[n.singleOrder].clocks2 = 
      LSEfw_end_timings[n.singleOrder].maxclocks2 = 0;
  }
#endif

  return true;
}

void LSEfw_do_scheduling() {
#if (LSEfw_PARM(LSE_mp_multithreaded)) 

  LSEfw_perform_scheduling(block_sched_graph, end_bsg, LSEfw_threadingParms);

  /*************** data layout for semaphores ************************/

  int *scnts = new int[LSEfw_num_threads];

  for (int i = 0; i < LSEfw_num_threads; ++i) scnts[i] = 0;

  for (TaskGraph::iterator i=block_sched_graph.begin(),
	 ie = block_sched_graph.end(); i != ie ; ++i) {
    schednode_t &n= *(schednode_t *)*i;
    scnts[n.getThread()] += n.getSendSemaphores().size();
  }
  for (TaskGraph::iterator i=end_bsg.begin(),
	 ie = end_bsg.end(); i != ie ; ++i) {
    schednode_t &n= *(schednode_t *)*i;
    for (std::vector<int>::const_iterator j = n.getSendSemaphores().begin(), 
	   je=n.getSendSemaphores().end() ;
	 j != je ; ++j) {
      scnts[n.getThread()]++;
    }
  }

  int totalsem = 0;
  for (int i = 0; i < LSEfw_num_threads; ++i) {
    LSEfw_shthreads[i].firstsig = totalsem;
    LSEfw_shthreads[i].lastsig = totalsem = totalsem + scnts[i];
    scnts[i] = 0;
  }

  for (TaskGraph::iterator i=block_sched_graph.begin(),
	 ie = block_sched_graph.end(); i != ie ; ++i) {
    schednode_t &n= *(schednode_t *)*i;
    for (std::vector<int>::const_iterator j = n.getSendSemaphores().begin(), 
	   je=n.getSendSemaphores().end() ;
	 j != je ; ++j) {
      semmap[*j] = LSEfw_shthreads[n.getThread()].firstsig + 
	(scnts[n.getThread()]++);
    }
  }
  for (TaskGraph::iterator i=end_bsg.begin(),
	 ie = end_bsg.end(); i != ie ; ++i) {
    schednode_t &n= *(schednode_t *)*i;
    for (std::vector<int>::const_iterator j = n.getSendSemaphores().begin(), 
	   je=n.getSendSemaphores().end() ;
	 j != je ; ++j) {
      end_semmap[*j] = LSEfw_shthreads[n.getThread()].firstsig + 
	(scnts[n.getThread()]++);
    }
  }

  delete[] scnts;

  LSEfw_sync_signals = new LSEfw_syncsig_t[std::max(1,totalsem)];
  LSEfw_num_syncs = totalsem;
  for (int j = 0 ; j < LSEfw_num_syncs ; j++) {
    LSEfw_syncsig_init(&LSEfw_sync_signals[j]);
  }

  LSEfw_num_locks = std::max(block_sched_graph.getLockCnt(),
			     end_bsg.getLockCnt());
  LSEfw_cblock_mutexes = new LSEfw_mutex_t[std::max(1,LSEfw_num_locks)];

  for (int j=0 ; j < LSEfw_num_locks ; j++) {
    LSEfw_cblock_mutexes[j].init();
  }

  // ******************** generate phase schedule *****************

  // generate ordering.  Inserting into a set is the easy way

  typedef std::set<schednode_t *, order_schedule> nodelist_t;
  nodelist_t *torder = new nodelist_t[LSEfw_num_threads];

  for (TaskGraph::iterator i=block_sched_graph.begin(),
	 ie = block_sched_graph.end(); i != ie ; ++i) {
    schednode_t &n= *(schednode_t *)*i;
    torder[n.getThread()].insert(&n);
  }

  std::map<int, mappair> maps;

  for (int i = 0; i < LSEfw_num_threads; ++i) {
    std::set<int> lastlocks;
    int scount = torder[i].size();
    for (nodelist_t::iterator j = torder[i].begin(), je = torder[i].end();
	 j != je ; ++j) {
      if ((*j)->getReceiveSemaphores().size() || 
	  (*j)->getLocks() != lastlocks) {
	scount += lastlocks.size();
	scount += (*j)->getReceiveSemaphores().size();
	scount += (*j)->getLocks().size();
      }
      scount += (*j)->getSendSemaphores().size();
      lastlocks = (*j)->getLocks();
    }
    scount += lastlocks.size();

    LSEfw_shthreads[i].schedule_length = scount;
    LSEfw_static_schedule_t *sp =
      LSEfw_shthreads[i].thread_schedule = new LSEfw_static_schedule_t[scount];
    lastlocks.clear();

    for (nodelist_t::iterator j = torder[i].begin(), je = torder[i].end();
	 j != je ; ++j) {

      if ((*j)->getReceiveSemaphores().size() || 
	  (*j)->getLocks() != lastlocks) {
	addlist(sp, lastlocks, (LSEfw_firing_t)LSEfw_sync_unlock);
	addlist(sp, (*j)->getReceiveSemaphores(), 
		(LSEfw_firing_t)LSEfw_sync_recv, true);
	addlist(sp, (*j)->getLocks(), (LSEfw_firing_t)LSEfw_sync_lock);
      }

      *sp = LSEfw_thread_schedule[(*j)->singleOrder];
      mappair r = { i, sp - LSEfw_shthreads[i].thread_schedule };
      maps[(*j)->singleOrder] = r;
      ++sp;

      lastlocks = (*j)->getLocks();
      addlist(sp, (*j)->getSendSemaphores(), 
	      (LSEfw_firing_t)LSEfw_sync_send, true);
    }

    addlist(sp, lastlocks, (LSEfw_firing_t)LSEfw_sync_unlock);
  } // for i in threads


  // ******************** generate end schedule *****************

  delete[] torder;  torder = new nodelist_t[LSEfw_num_threads];

  for (TaskGraph::iterator i=end_bsg.begin(),
	 ie = end_bsg.end(); i != ie ; ++i) {
    schednode_t &n= *(schednode_t *)*i;
    torder[n.getThread()].insert(&n);
  }

  for (int i = 0; i < LSEfw_num_threads; ++i) {
    std::set<int> lastlocks;
    int scount = torder[i].size();
    for (nodelist_t::iterator j = torder[i].begin(), je = torder[i].end();
	 j != je ; ++j) {
      if ((*j)->getReceiveSemaphores().size() || 
	  (*j)->getLocks() != lastlocks) {
	scount += lastlocks.size();
	scount += (*j)->getReceiveSemaphores().size();
	scount += (*j)->getLocks().size();
      }
      scount += (*j)->getSendSemaphores().size();
      lastlocks = (*j)->getLocks();
      scount += (*j)->getLocks().size();
    }
    scount += lastlocks.size();

    LSEfw_shthreads[i].end_schedule_length = scount;
    LSEfw_static_end_t *sp =
      LSEfw_shthreads[i].thread_end_schedule = new LSEfw_static_end_t[scount];

    lastlocks.clear();

    for (nodelist_t::iterator j = torder[i].begin(), je = torder[i].end();
	 j != je ; ++j) {

      if ((*j)->getReceiveSemaphores().size() || 
	  (*j)->getLocks() != lastlocks) {
	addlist(sp, lastlocks, (LSEfw_end_func_t)LSEfw_sync_unlock);
	addlist(sp, (*j)->getReceiveSemaphores(), 
		(LSEfw_end_func_t)LSEfw_sync_recv, true);
	addlist(sp, (*j)->getLocks(), (LSEfw_end_func_t)LSEfw_sync_lock);
      }

      *sp = LSEfw_thread_end_schedule[(*j)->singleOrder];
      ++sp;

      lastlocks = (*j)->getLocks();
      addlist(sp, (*j)->getSendSemaphores(), 
	      (LSEfw_end_func_t)LSEfw_sync_send, true);
    }

    addlist(sp, lastlocks, (LSEfw_end_func_t)LSEfw_sync_unlock);

  } // for i in threads

  delete[] torder;

  // ********************** signal checker calculation ****************

#ifdef LSEfw_check_ports
  unsigned int *cnts = new unsigned int[LSEfw_num_threads];

  for (int i = 0; i < LSEfw_num_threads; ++i) cnts[i] = 0;
  for (int i = 0, 
	 ie = sizeof(LSEfw_signal_checker) / sizeof(LSEfw_signal_checker_t);
       i != ie ; ++i) {
    cnts[maps[LSEfw_signal_checker[i].goodat].thread]++;
  }

  for (int i = 0; i < LSEfw_num_threads; ++i) {
    LSEfw_shthreads[i].signal_checker = new LSEfw_signal_checker_t[cnts[i]+1];
    LSEfw_signal_checker_t *sp= &LSEfw_shthreads[i].signal_checker[cnts[i]];
    sp->goodat = -1;
    sp->sno = sp->sloc = 0;
    sp->p = 0;
    sp->s = "";
    sp->mask = 0;
    cnts[i] = 0;
  }
  for (int i = 0, 
	 ie = sizeof(LSEfw_signal_checker) / sizeof(LSEfw_signal_checker_t);
       i != ie ; ++i) {
    int tno = maps[LSEfw_signal_checker[i].goodat].thread;
    int offset = maps[LSEfw_signal_checker[i].goodat].offset;
    LSEfw_signal_checker_t *sp= &LSEfw_shthreads[tno].signal_checker[cnts[tno]];
    *sp = LSEfw_signal_checker[i];
    sp->goodat = sp->sloc = offset;
    cnts[tno]++;
  }
  // sort the checks, as they do not stay sorted in threads
  for (int i = 0; i < LSEfw_num_threads; ++i) {
    std::stable_sort(&LSEfw_shthreads[i].signal_checker[0],
		     &LSEfw_shthreads[i].signal_checker[cnts[i]],
		     compare_checkers);
  }

  delete[] cnts;
#else
  for (int i = 0; i < LSEfw_num_threads; ++i) {
    LSEfw_shthreads[i].signal_checker = new LSEfw_signal_checker_t[1];
    LSEfw_signal_checker_t *sp= &LSEfw_shthreads[i].signal_checker[0];
    sp->goodat = -1;
    sp->sno = sp->sloc = 0;
    sp->p = 0;
    sp->s = "";
    sp->mask = 0;
  }
#endif

  /******************** and timing records ********************/

#endif
}

void LSEfw_undo_scheduling() {
#if (LSEfw_PARM(LSE_mp_multithreaded))
  for (int i = 0; i < LSEfw_num_threads; ++i) {
    delete[] LSEfw_shthreads[i].thread_schedule;
    delete[] LSEfw_shthreads[i].thread_end_schedule;
    delete[] LSEfw_shthreads[i].signal_checker;
  }
  delete[] LSEfw_cblock_mutexes;
  delete[] LSEfw_sync_signals;
#endif
}

void LSEfw_remove_scheduling() {
  delete[] LSEfw_shthreads;
}
