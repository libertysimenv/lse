/* 
 * Copyright (c) 2003 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * A file to fool the linker into pulling in the framework library
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * All this file does is create references to various functions in the
 * framework library so that all the framework components get pulled in.
 * This avoids having to create a linker command-line which is non-portable.
 *
 */

#include <setjmp.h>
#include <stdio.h>
#include <unistd.h>

#include <SIM_clp_interface.h>

void LSE_do_not_ever_call_me(void) {

  /* Pull in everything a CLP can talk to... */  
  LSE_sim_parse_arg(0,NULL,NULL);
  LSE_sim_parse_leftovers(0,NULL,NULL);
  LSE_domain_parse_arg(NULL,0,NULL,NULL);
  LSE_sim_print_usage();

  LSE_sim_initialize();
  LSE_sim_start();
  LSE_sim_do_timestep();
  LSE_sim_finish(TRUE);
  LSE_sim_finalize();

  LSE_sim_engine();

  /* And hit variables */
  LSE_sim_terminate_now = 0;
  LSE_sim_terminate_count = 0;
  LSE_sim_exit_status = 0;
  LSE_stderr = NULL;
  
  /* need to fool to bring in condor_compile */
  main();
}
