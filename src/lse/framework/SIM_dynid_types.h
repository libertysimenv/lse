/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Definition of the dynamic instruction instance type
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines the monster dynamic instruction instance type
 *
 */
#ifndef _LSE_DYNID_TYPES_H_
#define _LSE_DYNID_TYPES_H_

#include <SIM_clp_interface.h>

/* Must have already included LSE_types (giving stdio,string,LSE_config),
 * LSE_refcount, LSE_time
 */

typedef struct LSEdy_dynid_info_s {
  LSE_refcount_t super;
  LSE_dynid_num_t idno;

  LSEfw_dynid_t_attr attrs;               /* domain-specific pieces */
  struct LSEfw_dynid_t_extension fields;  /* config-specific pieces */

} LSEdy_dynid_info_t;

extern FILE *LSE_stderr;

extern void LSE_dynid_dump(void);

extern size_t LSEdy_dynid_size;
extern size_t LSEdy_dynid_field_size;
extern size_t LSEdy_dynid_field_offset;

#endif /* _LSE_DYNID_TYPES_H_ */


