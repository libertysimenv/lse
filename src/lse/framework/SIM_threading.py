# /* 
#  * Copyright (c) 2005-2011 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python database multi-threading analysis code
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * This code performs analysis of the database for multi-threading of the
#  * simulator.  It is called from SIM_database_build and SIM_create_files
#  *
#  * constraint types are:
#  * 0 = assignment
#  * 1 = same thread
#  * 2 = conflict (implies need for locking)
#  * 3 = conflict group
#  *
#  */
import sys, weakref

from SIM_codegen import * # imports tokenizer and database
from SIM_schedule import *

import string, bisect

############## set up error reporting ##################

def _LSEth_report_error(v):
    _LSEth_env.db.reportErr("Error: " + str(v) + "\n")

_LSEth_env = LSEcg_Environment()
_LSEth_env.reporter = _LSEth_report_error

class LSEth_Exception(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return self.value

######### constraint file functions ##################

def readPartition(fname):

    clist = []
    fp = open(fname,"r")
    lcount = 0
    for line in fp:
        lcount = lcount + 1
        #sys.stderr.write(line)
        string.strip(line)
        
        l = string.split(line)
        
        if (len(l) == 0): continue
        elif (l[0][0] == '#'): continue
        elif (l[0] == 'sameThread'):
            if (len(l) < 3 or (len (l) > 3 and l[3][0] != '#')):
                _LSEth_report_error("Syntax error on line %d of %s: %s\n" %\
                                    (lcount, line, fname))
                continue
            clist.append((1, lcount, l[1], l[2]))
        elif (l[0] == 'assign'):
            if (len(l) < 3 or (len (l) > 3 and l[3][0] != '#')):
                _LSEth_report_error("Syntax error on line %d of %s: %s\n" % \
                                    (lcount, line, fname))
                continue
            try:
                a= int(l[2])
            except ValueError:
                _LSEth_report_error("Syntax error on line %d of %s: %s\n" % \
                                    (lcount, line, fname))
                continue 
            clist.append((0, lcount, l[1], a))
        elif (l[0] == 'conflict'):
            if (len(l) < 3 or (len (l) > 3 and l[3][0] != '#')):
                _LSEth_report_error("Syntax error on line %d of %s: %s\n" %\
                                    (lcount, line, fname))
                continue
            clist.append((2, lcount, l[1], l[2]))
        elif (l[0] == 'conflictgroup'):
            if (len(l) < 3 or (len (l) > 3 and l[3][0] != '#')):
                _LSEth_report_error("Syntax error on line %d of %s: %s\n" %\
                                    (lcount, line, fname))
                continue
            clist.append((3, lcount, l[1], l[2]))
        elif (l[0] == 'include'):
            if (len(l) < 2 or (len(l) > 2 and l[2][0] != '#')):
                _LSEth_report_error("Syntax error on line %d of %s: %s\n" %\
                                    (lcount, line, fname))
                continue
            newlist = readPartition(os.path.join(os.path.dirname(fname),l[1]))
            clist.extend(newlist)
        else:
            _LSEth_report_error("Syntax error on line %d of %s: %s\n" % \
                                (lcount, line, fname))
        
    fp.close()
    return clist

def prefixMatch(np, s, wasadd):
    #sys.stderr.write("filtering %s %s %s\n" % (np,s,wasadd))
    if not len(s): return 0
    sp = string.split(s,'.')
    if wasadd and sp == np[:-1] : return 1 # special case for +**

    if len(sp) < len(np): return 0
    for e in map(lambda x,y:(x,y),np,sp):
        if (e[0] == None): return 0
        if (e[0] == '**'): return 1
        if (e[0] != e[1]):
            if e[0] == '*': continue
            s = string.replace(e[0],'**','.*')
            mo = re.match(s,e[1])
            if mo and mo.start()==0 and mo.end()==len(e[1]): continue
            return 0
    return 1

def blockMatch(np, s):
    return np == s

def findMatchingElements(db, mstr):
    parts=string.split(mstr,':')
    nparts = string.split(parts[0],'.')
    np2 = string.split(nparts[-1],'+',1)
    nparts = nparts[:-1] + np2
    wasadd = len(np2)>1 and np2[-1] == '**'

    # find all codeblocks of instance (includes start,end)
    pm = filter(lambda x,np=nparts,wa=wasadd:\
                x[0] < LSEdb_CblockDynamic and prefixMatch(np,x[1],wa),
                db.cblocksList)
    
    if len(parts)==2 and parts[1] != '*': # filter out non-matching elements
        nparts = string.split(parts[1],'.')
        if nparts[0] == 'phase':
            nparts = ['phase',LSEdb_CblockPhase]
        elif nparts[0] == 'phase_start':
            nparts = ['',LSEdb_CblockPhaseStart]
        elif nparts[0] == 'phase_end':
            nparts = ['',LSEdb_CblockPhaseEnd]
        elif (len(nparts)>1):
            if (nparts[1] == 'handler'): nparts[1] = LSEdb_CblockHandler
            elif (nparts[1] == 'control'): nparts[1] = LSEdb_CblockFiring
	nparts = tuple(nparts)
        pm = filter(lambda x:(x[2],x[0])==nparts,pm)

    return (filter(lambda x:x[0]!=LSEdb_CblockPhaseEnd,pm),
            filter(lambda x:x[0]==LSEdb_CblockPhaseEnd,pm))

def expandCons(db, cons):
    nc = []
    ncBegin = []
    ncEnd = []
    cgroups = {}
    for c in cons:
        if c[0]==0:    # assign
            (l,e) = findMatchingElements(db,c[2])
            nc.extend(map(lambda x,y=c[3],z=c[1]:(0,z,x,y),l))
            ncEnd.extend(map(lambda x,y=c[3],z=c[1]:(0,z,x,y),e))
        elif c[0]==1:  # same thread
            (l,en) = findMatchingElements(db,c[2])
            (l2,en2) = findMatchingElements(db,c[3])
            #sys.stderr.write("%s\n" % l)
            #sys.stderr.write("%s\n" % l2)
            for e in l:
                nc.extend(filter(lambda x:x[2] != x[3],
                                 map(lambda x,y=e,z=c[1],w=e:(1,z,y,x), l2)))
            for e in en:
                ncEnd.extend(filter(lambda x:x[2] != x[3],
                                    map(lambda x,y=e,z=c[1],w=e:(1,z,y,x), en2
                                        )))
        elif c[0]==2: # conflict
            (l,en) = findMatchingElements(db,c[2])
            (l2,en2) = findMatchingElements(db,c[3])
            #sys.stderr.write("%s\n" % l)
            #sys.stderr.write("%s\n" % l2)
            for e in l:
                nc.extend(filter(lambda x:x[2] != x[3],
                                 map(lambda x,y=e,z=c[1],w=e:(2,z,y,x), l2)))
            for e in en:
                ncEnd.extend(filter(lambda x:x[2] != x[3],
                                 map(lambda x,y=e,z=c[1],w=e:(2,z,y,x), en2)))
        elif c[0]==3: # conflict group
            (l,en) = findMatchingElements(db,c[3])
            g = cgroups.setdefault(c[2],([],[],[]))
            g[0].extend(l)
            g[2].extend(en)
        else:
            raise Exception("Bad constraint %s" % c)

    db.cgroups = cgroups
    gl = cgroups.values()
    gl.sort()
    for g in gl:
        for e in g[0]:
            nc.extend(filter(lambda x:x[2] != x[3],
                             map(lambda x,y=e:(2,0,y,x), g[0])))
        for e in g[2]:
            ncEnd.extend(filter(lambda x:x[2] != x[3],
                                map(lambda x,y=e:(2,0,y,x), g[2])))
   
    nc.sort() 
    ncEnd.sort()        
    return (nc,ncBegin,ncEnd)

############################################################################
# LSEth_analyze_threading                                                  #
############################################################################
# Analyze the threading properties of the system and generate thread
# schedules.  May also change port connection scheduling information for
# ports which had direct calls across thread boundaries.
#
# fname must be full path name of the constraint file appropriately formed
# for OS, but we still check the LSE_mp_constraint_file parameter so that
# we know whether fname matters

def LSEth_analyze_threading(db, fname):

    _LSEth_env.db = db
    
    cf = db.getParmVal('LSE_mp_constraint_file')

    if (cf):
        (pcons,bcons,econs) = expandCons(db,readPartition(fname))
    else:
        (pcons,bcons,econs) = ([],[],[])

    db.cons = (pcons,bcons,econs)
    #sys.stderr.write('%s\n' % pcons)

    ######################################################################
    ####################### PHASE SCHEDULE ###############################
    ######################################################################

    _LSEth_phase_scheduling(db,pcons)
    
    ######################################################################
    ####################### PHASE END SCHEDULE ###########################
    ######################################################################

    _LSEth_phase_end_scheduling(db,econs)

    
######################################################################
####################### PHASE SCHEDULE ###############################
######################################################################
#  
# This routine is responsible for scheduling code during the phase
# portion of the timestep.  It may become responsible later on for
# scheduling all merged pieces of schedule as well.  We'll see....
#
# Inputs:
#
#  db.block_sched_list:  A list of scheduling items in schedule order
#  db.block_sched_graph: The same scheduling items, but in dictionary form
#  pcons: a constraint structure
#
# Outputs:
#
#  db.thread_schedule
#  and probably a whole bunch of side effects, like changes to the code
#  blocks list to hold the locking stuff....
#
# There are (or will be) lots of options to this.  The highest-level
# options control the overall flow:
#
# - What is the base cost model for block execution?
# - What cost model is used for signaling/synchronization?; this
#   subsumes the question of whether pre-assignments should be used
#   when calculating the critical path
#
# Fun stuff:
# - keeping original schedule can be done by setting critical path
#   distance and all blocks to ready so the list schedule maintains
#   the original order

def _LSEth_phase_scheduling(db, pcons):
    
    ##################### generate conflicts #######################
    #
    # scheduling items during phase_start/phase conflict if:
    # 1) both from same instance
    # 2) one writes a signal received or written by the other.  This
    #    must use potential output signals instead of planned because
    #    signals can be set earlier, however once a signal is final
    #    it is no longer a potential output.  This includes port queries!
    # 3) writing to the same signal variable (because they are merged)
    #    again, potential must be used
    # 4) there is a conflict constraint between them
    # 5) if item A can call a function in instance of item B (transitively)
    #    NOT YET IMPLEMENTED AND NOT A PROBLEM IN MODELS SO FAR
    # 6) if item A and B can both touch the same shared data
    #    (runtime var or dynid field)
    #    NOT YET IMPLEMENTED AND NOT A PROBLEM IN MODELS SO FAR
    #
    # Note that these conflicts are filtered through data dependencies
    # in the single-thread schedule so that the conflict database is reduced
    # and so that non-conflicts do not pollute later scheduling

    bsg = db.block_sched_graph # speed things up a tiny bit
    bsl = db.block_sched_list
    prepcovered(bsg)
    potentialConflicts = {}
    
    # case 1: both from same instance
    mblocks2={}
    for item in bsl:
        ctx = db.cblocksList[item.cno]
        if ctx[0] == LSEdb_CblockDynamic: continue
        key = ctx[1]
        if not mblocks2.has_key(key):
            mblocks2[key] = [ item ]
        else:
            # check for conflicts...
            for og in mblocks2[key]:
                if potentialConflicts.has_key((og,item)): continue
                if not stordcovered(og,item,bsg):
                    potentialConflicts[(og,item)]=1
                    item.conflicts[og.id] = None
                    og.conflicts[item.id] = None
                else:
                    potentialConflicts[(og,item)]=0
            mblocks2[key].append(item)
    del mblocks2
    
    # case 2: look for writers vs. readers/writers
    writers = {}
    readers = {}
    for item in bsl:
        for sno in item.couldGenerate:
            writers.setdefault(sno,[]).append(item)
            for og in readers.get(sno,[]):
                if potentialConflicts.has_key((og,item)): continue
                if not stordcovered(og, item, bsg):
                    potentialConflicts[(og,item)]=1
                    item.conflicts[og.id] = None
                    og.conflicts[item.id] = None
                else:
                    potentialConflicts[(og,item)]=0
        for sno in db.cblocksPotentialInputSignals[item.cno]:
            readers.setdefault(sno,[]).append(item)
            for og in writers.get(sno,[]):
                if potentialConflicts.has_key((og,item)): continue
                if not stordcovered(og, item, bsg):
                    potentialConflicts[(og,item)]=1
                    item.conflicts[og.id] = None
                    og.conflicts[item.id] = None
                else:
                    potentialConflicts[(og,item)]=0
    del writers # free memory
    del readers # free memory

    # case 3: look for writers of the same input signal
    writers = {}
    for item in bsl:
        for sno in item.couldGenerate:
            s = db.signalList[sno]

            # one side
            key = (s.inst,s.port,s.instno)
            writers.setdefault(key,[]).append(item)
            for og in writers[key]:
                if potentialConflicts.has_key((og,item)): continue
                if not stordcovered(og, item, bsg):
                    potentialConflicts[(og,item)]=1
                    item.conflicts[og.id] = None
                    og.conflicts[item.id] = None
                else:
                    potentialConflicts[(og,item)]=0

            # other side
            try:
                conn = db.instances[s.inst].ports[s.port].connections[s.instno]
            except: # not a connected signal
                continue
            if not conn: continue
            key = (conn[0].inst.name,conn[0].name,conn[1])
            writers.setdefault(key,[]).append(item)
            for og in writers[key]:
                if potentialConflicts.has_key((og,item)): continue
                if not stordcovered(og, item, bsg):
                    potentialConflicts[(og,item)]=1
                    item.conflicts[og.id] = None
                    og.conflicts[item.id] = None
                else:
                    potentialConflicts[(og,item)]=0
    del writers
    
    # case 4: look for conflict constraints
    for c in pcons:
        if (c[0] == 2): # conflict
            cno = db.cblocksBacklist[c[2]]
            cno2 = db.cblocksBacklist[c[3]]
            if cno<=0 or cno2<=0: continue
            
            #sys.stderr.write("%d %d %s\n" % (cno,cno2,c))
            sb  = map(lambda x,db=db:db.block_sched_graph[x],
                      db.cblocksCalls[cno] + db.cblocksDynSects[cno])
            sb2 = map(lambda x,db=db:db.block_sched_graph[x],
                      db.cblocksCalls[cno2] + db.cblocksDynSects[cno2])

            for item in sb:
                for og in sb2:
                    if og.singleOrder < item.singleOrder:
                        if potentialConflicts.has_key((og,item)): continue
                        if not stordcovered(og, item, bsg):
                            potentialConflicts[(og,item)]=1
                            item.conflicts[og.id] = None
                            og.conflicts[item.id] = None
                        else:
                            potentialConflicts[(og,item)]=0
                    else:
                        if potentialConflicts.has_key((item,og)): continue
                        if not stordcovered(item, og, bsg):
                            potentialConflicts[(item,og)]=1
                            item.conflicts[og.id] = None
                            og.conflicts[item.id] = None
                        else:
                            potentialConflicts[(item,og)]=0

    # case 5: look for method calls getting in the way
    
    # case 6: look for shared data
    
    ############## Report single-thread depth ###################

    print "Single-thread depth is %d calls" % len(db.block_sched_list)

    ############## Fix up generators ###########################
    # need to ensure that generators of a signal have the same final
    # generator.  This makes final checks possible and also makes the
    # assumption that final will run last of the generators hold
    # (which is used in locking).  The easiest way to do this is to
    # insert a dependence between the scheduling pieces
    
    for s in db.signalList:
	l = db.signalsCalls[s.num] + db.signalsDynSects[s.num]
	l.sort()
	if len(l) < 2: continue
        thisone = l[-1]
        for lastone in l[:-1]:
            if lastone not in db.block_sched_graph[thisone].deps:
		db.block_sched_graph[thisone].deps.append(lastone)
        db.block_sched_graph[thisone].deps.sort()

    db.potentialConflicts = potentialConflicts
    

######################################################################
####################### PHASE END SCHEDULE ###########################
######################################################################
#  
# This routine is responsible for scheduling code during the phase end
# portion of the timestep.
#
# Inputs:
#
#  db.block_sched_graph - results of scheduling phase so we can do
#                         thread affinity assignments
#  econs: a constraint structure
#
# Outputs:
#

def _LSEth_phase_end_scheduling(db, econs):

    print "############## Phase end scheduling ###############"
    
    ########## determine who has phase end work to do #########
    
    # eot comes before phase_end
    l1 = filter(lambda x:x[0]==LSEdb_CblockPhaseEnd and x[3]!=0,db.cblocksList)
    l2 = filter(lambda x:x[0]==LSEdb_CblockPhaseEnd and x[3]==0,db.cblocksList)
    
    end_bsl = []
    end_bsg = {}
    inst2id = {}
    i = 0
    for cblock in l1:
        cno = db.cblocksBacklist[cblock]
        item = LSEsc_schedNode(i,cno,[],1)
        end_bsl.append(item)
        end_bsg[i] = item
        item.thread = -1
        item.couldGenerate=[]
        item.singleOrder = i
        inst2id.setdefault(cblock[1],[]).append(i)
        db.cblocksCalls[cno] = [ i ]
        i += 1
    leot = i
    for cblock in l2:
        cno = db.cblocksBacklist[cblock]
        item = LSEsc_schedNode(i,cno,[],1)
        end_bsl.append(item)
        end_bsg[i] = item
        item.thread = -1
        item.couldGenerate=[]
        item.singleOrder = i
        inst2id.setdefault(cblock[1],[]).append(i)
        db.cblocksCalls[cno] = [ i ]
        i += 1
        
    ################### generate DAG ###############################
    # parents run before children at present
    # all eot run before all phase ends

    for i in range(len(end_bsl)):
        s2 = db.cblocksList[end_bsl[i].cno][1]
        s = s2 + "."
        # everyone after leot depends upon it
        if i >= leot and leot > 0:
            end_bsl[i].deps.append(leot-1)
        for j in range(i+1,len(end_bsl)):
            sj = db.cblocksList[end_bsl[j].cno][1]
            if string.find(sj,s) == 0 or sj == s2:
                end_bsl[j].deps.append(end_bsl[i].id)

    # leot depends upon everyone before
    if leot > 0:
        item = end_bsl[leot-1]
        for ugh in range(leot-1):
            if ugh not in item.deps: item.deps.append(ugh)

    for item in end_bsl:
        item.deps.sort()

    ################## generate conflicts #######################
    #
    # scheduling items during phase_end conflict if:
    # 1) both from same instance
    # 2) there is a conflict constraint between them
    # 3) item A can call a function in instance of item B (transitively)
    #    NOT YET IMPLEMENTED AND NOT A PROBLEM IN MODELS SO FAR
    # 4) if item A and B can both touch the same shared data
    #    (runtime var or dynid field)
    #    NOT YET IMPLEMENTED AND NOT A PROBLEM IN MODELS SO FAR
    #
    #
    # Note that these conflicts are filtered through data dependencies
    # in the single-thread schedule so that the conflict database is reduced
    # and so that non-conflicts do not pollute later scheduling

    bsg = end_bsg # speed things up a tiny bit
    bsl = end_bsl
    prepcovered(bsg)
    potentialConflicts = {}
    
    # case 1: both from same instance
    mblocks2={}
    for item in bsl:
        ctx = db.cblocksList[item.cno]
        if ctx[0] == LSEdb_CblockDynamic: continue
        key = ctx[1]
        if not mblocks2.has_key(key):
            mblocks2[key] = [ item ]
        else:
            # check for conflicts...
            for og in mblocks2[key]:
                if potentialConflicts.has_key((og,item)): continue
                if not stordcovered(og,item,bsg):
                    potentialConflicts[(og,item)]=1
                    item.conflicts[og.id] = None
                    og.conflicts[item.id] = None
                else:
                    potentialConflicts[(og,item)]=0
            mblocks2[key].append(item)
    del mblocks2

    # case 2: look for conflict constraints
    for c in econs:
        if (c[0] != 2): continue
        cno = db.cblocksBacklist[c[2]]
        cno2 = db.cblocksBacklist[c[3]]
        if cno <= 0 or cno2 <= 0: continue

        #sys.stderr.write("%d %d %s\n" % (cno,cno2,c))
        sb  = map(lambda x:end_bsg[x], db.cblocksCalls[cno])
        sb2 = map(lambda x:end_bsg[x], db.cblocksCalls[cno2])

        for item in sb:
            for og in sb2:
                if og.singleOrder < item.singleOrder:
                    if potentialConflicts.has_key((og,item)): continue
                    if not stordcovered(og, item, bsg):
                        potentialConflicts[(og,item)]=1
                        item.conflicts[og.id] = None
                        og.conflicts[item.id] = None
                    else:
                        potentialConflicts[(og,item)]=0
                else:
                    if potentialConflicts.has_key((item,og)): continue
                    if not stordcovered(item, og, bsg):
                        potentialConflicts[(item,og)]=1
                        item.conflicts[og.id] = None
                        og.conflicts[item.id] = None
                    else:
                        potentialConflicts[(item,og)]=0
    
    # case 3: look for method calls getting in the way
    
    # case 4: look for shared data
    
    ############## Report single-thread depth ###################

    print "Phase End Single-thread depth is %d calls" % len(end_bsl)
 
    db.end_bsg = end_bsg
    db.end_bsl = end_bsl
    db.end_potentialConflicts = potentialConflicts
    
    return

############## cost model calculator ... put here for sharing #########

def calc_cost(item, coeffs, db, bcm):
    if bcm==2:
        cno = item.cno
        cf = coeffs[db.cblocksList[cno][0]]
        rc = int(cf[0] +
                 cf[1] * len(db.cblocksSensInputSignals[cno]) +
                 cf[2] * len(db.cblocksUsefulInputSignals[cno]) +
                 cf[3] * len(db.cblocksPotentialInputSignals[cno]) +
                 cf[4] * len(db.cblocksPotentialOutputSignals[cno]) +
                 cf[5] * len(db.cblocksWantedOutputSignals[cno]) +
                 cf[6] * len(item.generates) +
                 cf[7] * len(item.couldGenerate) +
                 cf[8] * len(item.final) +
                 cf[9] * len(item.deps) +
                 (cf[10] * len(db.cblocksUsefulInputSignals[cno]) *
                  len(item.generates))
                 )

        return max(rc,1)
    elif bcm==1:
        # no point in assigning a cost to dynamic sections, as they
        # are going to synchronize all the threads together
        if db.cblocksList[item.cno][0] == LSEdb_CblockDynamic:
            return 1

        # cost model is 1 + summation over generated signals of the
        # (number of inputs used by each signal + 1)
        # the plus 1 are to avoid nasty situations where cost becomes 0
        return reduce(lambda x,y:x + 1 + len(db.signalList[y].realdepson),
                      item.generates,1)
    else: # unit cost
        return 1
    
############## coverage functions ######################

def prepcovered(bsg):
    for i in bsg.values():
        i.coverMarker = 0
        
# it would be nice to memoize or pre-compute this stuff, but it
# would take megabytes of space for large graphs!
idno = 0
def stcovered(fromi, toi, bsg):

    if fromi is toi: return 1 # quick exit
    
    global idno
    newidno = idno + 1
    if newidno < idno: # must clean up on roll-oveer
        prepcovered(bsg)
        newidno = 1

    # it is ok to use order because it is comparable iff they are covered
    if toi.singleOrder < fromi.singleOrder:
        rval = stordcoveredint(toi, fromi, bsg, newidno)
    else:
        rval = stordcoveredint(fromi, toi, bsg, newidno)
    return rval

def stordcovered(fromi, toi, bsg):

    if fromi is toi: return 1 # quick exit

    global idno
    newidno = idno + 1
    if newidno < idno: # must clean up on roll-oveer
        prepcovered(bsg)
        newidno = 1
    
    rval = stordcoveredint(fromi, toi, bsg, newidno)
    return rval

def stordcoveredint(fromi, toi, bsg, idno):
    # do this as a breadth-first search so we do not overflow the
    # Python stack on large configurations
    
    if fromi.id in toi.deps: return 1 # quick exit
    q = toi.deps * 1 # initialize q

    while len(q):
        toi = bsg[q[0]]
        q.pop(0)
        if fromi.id in toi.deps: return 1
        for i in toi.deps:
            if bsg[i].coverMarker != idno:
                q.append(i)
                bsg[i].coverMarker = idno
    return 0
    
