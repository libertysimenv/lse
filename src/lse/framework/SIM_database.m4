m4_dnl /* 
m4_dnl  * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
m4_dnl  * Group. (http://www.liberty-research.org)
m4_dnl  *
m4_dnl  * All rights reserved.
m4_dnl  * 
m4_dnl  * Permission is hereby granted, without written agreement and without
m4_dnl  * license or royalty fees, to use, copy, modify, and distribute this
m4_dnl  * software and its documentation for any purpose, provided that the
m4_dnl  * above copyright notice and the following two paragraphs appear in
m4_dnl  * all copies of this software.
m4_dnl  * 
m4_dnl  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
m4_dnl  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
m4_dnl  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
m4_dnl  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
m4_dnl  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
m4_dnl  * 
m4_dnl  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
m4_dnl  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
m4_dnl  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
m4_dnl  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
m4_dnl  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
m4_dnl  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
m4_dnl  * OR MODIFICATIONS.
m4_dnl  *
m4_dnl  */
m4_dnl /*%
m4_dnl  * 
m4_dnl  * m4 macros to read in the design database
m4_dnl  *
m4_dnl  * Authors: David A. Penry <dpenry@cs.princeton.edu>
m4_dnl  *
m4_dnl  * This file reads the Python database
m4_dnl  *
m4_dnl  */
m4_dnl get set up - must only include the database once!  Also, if we're
m4_dnl parsing through a tokenizer that has pre-loaded the database, we do
m4_dnl not need to reload it
m4_ifelse(LSEm4_included_database,,,

m4_define(LSEm4_included_database,)

m4_dnl
m4_dnl Read in the database (after scheduling)
m4_dnl
m4_pythonfile(
import sys, os, cPickle, time,string, os.path
sys.path.append("""m4_include(lseenv)""" + "/share/domains")
sys.path.append("""m4_include(lseenv)""" + "/share/lse/framework")
sys.path.append("""m4_include(lseenv)""" + "/lib")

import LSE_domain
from SIM_apidefs import * # imports codegen, database, schedule, tokenizer

realtopdir = os.path.normpath(string.strip("""m4_include(includedir)""") + "/..")

try:
  mine = LSEpy_preloaded_database
except: # not in a preload situation...
  starttime=time.time()
  LSE_db=LSEdb_Design(realtopdir + "/database/SIM_schedule.dat")
  endtime=time.time()
  sys.stderr.write("Elapsed database load time: %f\n" % (endtime-starttime))

# change the topdir to the real one; allows us to move simulators around!
LSE_db.topDir = realtopdir
LSE_db.loadDomains(globals())
LSEcg_init_errors()
) m4_dnl

m4_dnl
m4_dnl  LSEfw_PARM
m4_dnl     Name a framework parameter
m4_define(LSEfw_PARM,
m4_python(print str(LSE_db.getParmVal(string.strip(r"""$1"""),0)))) 
) m4_dnl if already included


