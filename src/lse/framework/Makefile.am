# /* 
#  * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Automakefile for the Liberty simulator framework directory 
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Tar's up the files into two tarballs: one for the framework and
#  * one for the includes and sets up to put them in the simulator's
#  * install area.
#  *
#  */
SUBDIRS=scheduler MPscheduler

frameworkdir=${pkgdatadir}/framework
framework_code_TARSTUFF=\
		SIM_control.c.m4 \
		SIM_framework_code.mk \
		SIM_initfinish.c.m4 \
		SIM_mainloop.c.m4 \
		MPscheduler/Schedulable.h \
		MPscheduler/SchedulableDAG.h \
		MPscheduler/LSEtasks.h

framework_inc_TARSTUFF=\
		Make_module.mk \
		SIM_all_types.h \
		SIM_control.h.m4 \
		SIM_database.m4 \
		SIM_domain_types.h.m4 \
		SIM_dynid.h \
		SIM_dynid_types.h \
		SIM_framework_inc.mk \
		SIM_prefix.m4 \
		SIM_quotes.m4 \
		SIM_refcount.h \
		SIM_refcount_types.h \
		SIM_resolution.h \
		SIM_resolution_types.h \
	        SIM_threading.h.m4 \
		SIM_user_types.h.m4 

# note that SIM_quotes.m4 is put in both the tarball and the install
# area.  It is needed by both m4 processing of files and database build.
# Database build happens before the include directory is created, and
# it is annoying to add a search path to m4 invocations for a single
# file, so we do this.
nontardata = \
	LSE_parmdecl.lss \
	LSE_startup.lss \
	Make_include.mk \
	SIM_analysis.py \
	SIM_apidefs.py \
	SIM_codegen.py \
	SIM_database.py \
	SIM_database_build.py \
	SIM_debug_database.py \
	SIM_rebuild.py \
	SIM_threading.py \
	SIM_quotes.m4 \
	LSE_indent.pl 

framework_DATA=\
	framework_code.tar \
	framework_inc.tar \
	$(nontardata)

pkgdata_DATA = LSEfw_adapter.clm

pythondir=${pkgdatadir}/framework/LSEpy

mbd = $(PWD)

EXTRA_DIST=${framework_inc_TARSTUFF} ${framework_code_TARSTUFF} \
	   $(nontardata) LSEfw_adapter.clm

CLEANFILES=framework_code.tar framework_inc.tar

framework_code.tar: $(framework_code_TARSTUFF)
	list='$(framework_code_TARSTUFF)'; \
	list2=`echo $$list | $(SED) 's+/[^ ]*/++g'` ; \
	(cd $(srcdir); $(TAR) -cvf - $$list2) > framework_code.tar

framework_inc.tar: $(framework_inc_TARSTUFF)
	list='$(framework_inc_TARSTUFF)'; \
	list2=`echo $$list | $(SED) 's+/[^ ]*/++g'` ; \
	(cd $(srcdir); $(TAR) -cvf - $$list2) > framework_inc.tar

INCLUDES=-I@srcdir@/../include
pkglib_LIBRARIES = liblinkdummy.a
liblinkdummy_a_SOURCES = SIM_foollinker.c

install-data-local:
	$(INSTALL_DATA) SIM_foollinker.o $(DESTDIR)$(pkglibdir)/SIM_foollinker.o

uninstall-local:
	rm -f $(DESTDIR)$(pkglibdir)/SIM_foollinker.o

