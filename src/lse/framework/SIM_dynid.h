/* 
 * Copyright (c) 2000-2011 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Definition of the dynamic instruction instance type
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines the monster dynamic instruction instance type and a few APIs
 *
 * The APIs defined here are:
 *   LSE_dynid_create
 *   LSE_dynid_recreate
 *   LSE_dynid_eq
 *
 * APIs defined elsewhere are:
 *
 *  in SIM_common.m4 and SIM_codegen.py:
 *   LSE_dynid_cancel   : m4 macro allows line number to be printed for debug
 *   LSE_dynid_register : m4 macro allows line number to be printed for debug
 *   LSE_dynid_get      : m4/py macros do parsing, checking, expansion
 *   LSE_dynid_set      : m4/py macros do parsing, checking, expansion
 *
 *  in SIM_control.c.m4:
 *   LSE_dynid_dump
 *
 */
#ifndef _LSE_DYNID_H_
#define _LSE_DYNID_H_

#define LSE_dynid_eq(i1,i2) ((i1)==(i2))

#ifndef LSEfw_IN_CONTROL_C
extern 
#endif
LSEfw_mutex_t LSEdy_dynid_mutex;

extern LSE_dynid_t LSEdy_dynid_initial_create();
extern void LSEdy_dynid_initial_cancel(LSE_dynid_t);
extern void LSEdy_dynid_reclaim_hooks(LSE_dynid_t);
extern void LSEdy_dynid_allocate_hooks(LSE_dynid_t);

#ifdef LSEfw_IN_CONTROL_C
LSE_dynid_t LSEdy_dynid_initial_create() {
  LSE_dynid_t t = (LSE_dynid_t)malloc(LSEdy_dynid_size);
  t->super.next = 0;
  LSEdy_dynid_allocate_hooks(t);
  t->super.count = 1;
  t->idno = LSEdy_dynid_global_num++;
  memset((((char *)t)+LSEdy_dynid_field_offset),0,LSEdy_dynid_field_size);
  if (LSEfw_PARM(LSE_debug_dynid_refs)) {
    fprintf(LSE_stderr,"C%" PRIu64 " INITIAL\n", t->idno);
  }
  return(t);
}

void LSEdy_dynid_initial_cancel(LSE_dynid_t d) {
  if (LSEfw_PARM(LSE_debug_dynid_refs))
    fprintf(LSE_stderr,"X%" PRIu64 " %d INITIAL\n", d->idno, d->super.count);
  free(d);
}
#endif

inline LSE_dynid_t 
LSEdy_dynid_internal_create(LSEfw_tf_def const char *iname, 
			    const char *fname, int n) {
  LSE_dynid_t t;

#if (!LSEfw_parallelize_dynids)
  LSEdy_dynid_mutex.lock();
#endif
  if (LSEdy_dynid_free_list==NULL) {
    t = (LSE_dynid_t)malloc(LSEdy_dynid_size);
    t->super.next = (LSE_refcount_t *)(LSEdy_dynid_master_list);
    LSEdy_dynid_master_list = t;
    LSEdy_dynid_allocate_hooks(t);
  } else {
    t = LSEdy_dynid_free_list;
    LSEdy_dynid_free_list=(LSE_dynid_t)t->super.next;
    t->super.next = (LSE_refcount_t *)(LSEdy_dynid_master_list);
    LSEdy_dynid_master_list = t;
  }
  t->super.count=1;
#if (!LSEfw_parallelize_dynids)
  t->idno = LSEdy_dynid_global_num++;
  LSEdy_dynid_mutex.unlock();
#else
  t->idno = LSEfw_atomic_inc_u64(&LSEdy_dynid_global_num,&LSEdy_dynid_mutex);
#endif
  memset((((char *)t)+LSEdy_dynid_field_offset),0,LSEdy_dynid_field_size);
  if (LSEfw_PARM(LSE_debug_dynid_refs)) {
    fprintf(LSE_stderr,"C%" PRIu64 " %s %s:%d\n",t->idno,iname,fname,n);
  }
  return(t);
}


inline void 
LSEdy_dynid_internal_recreate(LSE_dynid_t t, 
  const char *iname, const char *fname, int n) {

  LSEdy_dynid_reclaim_hooks(t);


#if (LSEfw_parallelize_dynids)
  t->idno = LSEfw_atomic_inc_u64(&LSEdy_dynid_global_num,&LSEdy_dynid_mutex);
#else
  LSEdy_dynid_mutex.lock();
  t->idno = LSEdy_dynid_global_num++;
  LSEdy_dynid_mutex.unlock();
#endif

  memset((((char *)t)+LSEdy_dynid_field_offset),0,LSEdy_dynid_field_size);
  if (LSEfw_PARM(LSE_debug_dynid_refs)) {
    fprintf(LSE_stderr,"c%" PRIu64 " %s %s:%d\n",t->idno,iname,fname,n);
  }
  return;
}

inline LSE_dynid_t 
LSEdy_dynid_internal_clone(LSEfw_tf_def LSE_dynid_t ot, const char *iname, 
			   const char *fname, int n) {
  LSE_dynid_t t;

#if (!LSEfw_parallelize_dynids)
  LSEdy_dynid_mutex.lock();
#endif
  if (LSEdy_dynid_free_list==NULL) {
    t = (LSE_dynid_t)malloc(LSEdy_dynid_size);
    LSEdy_dynid_allocate_hooks(t);
  } else {
    t = LSEdy_dynid_free_list;
    LSEdy_dynid_free_list = (LSE_dynid_t)t->super.next;
  }
  memcpy(t,ot,LSEdy_dynid_size);
  t->super.next = (LSE_refcount_t *)LSEdy_dynid_master_list;
  LSEdy_dynid_master_list = t;
  t->super.count=1;

#if (LSEfw_parallelize_dynids)
  t->idno = LSEfw_atomic_inc_u64(&LSEdy_dynid_global_num,&LSEdy_dynid_mutex);
#else
  t->idno = LSEdy_dynid_global_num++;
  LSEdy_dynid_mutex.unlock();
#endif

  if (LSEfw_PARM(LSE_debug_dynid_refs))
    fprintf(LSE_stderr,"L%" PRIu64 " %s %s:%d\n",t->idno,iname,fname,n);
  return(t);
}

inline void 
LSEdy_dynid_internal_register(LSE_dynid_t d, const char *iname, 
			      const char *fname, int n) {
  if (LSEfw_PARM(LSE_debug_dynid_refs))
    fprintf(LSE_stderr,"R%" PRIu64 " %d %s %s:%d\n",
	    (d)->idno,(d)->super.count,iname,fname,n); 

  LSEfw_atomic_inc(&d->super.count,&LSEdy_dynid_mutex);
}

inline void 
LSEdy_dynid_internal_cancel(LSE_dynid_t d, const char *iname, 
			    const char *fname, int n) {
  if (LSEfw_PARM(LSE_debug_dynid_refs))
    fprintf(LSE_stderr,"X%" PRIu64 " %d %s %s:%d\n",
	    d->idno,d->super.count,iname,fname,n);

  LSEfw_atomic_dec(&d->super.count,&LSEdy_dynid_mutex);

  /* Negative counts are probably not good */
  if (LSEfw_PARM(LSE_debug_dynid_refs) &&
      ((d)->super.count) < 0) {
    fprintf(LSE_stderr,"************X%" PRIu64 " %d<0 %s %s:%d\n",
	    d->idno,d->super.count,iname,fname,n);
  }
}

#endif /* _LSE_DYNID_H_ */


