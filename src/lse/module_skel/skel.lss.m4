/*%
 * 
 * This is a one line description of the file.  
 *
 * Authors: Author Name <email-addr@yourdomain.edu>
 *
 * This is a long description of the file that is usually one or two
 * paragraphs in length.  After reading it, one should be able to
 * understand the purpose of the code in this file without looking at
 * anything else.
 *
 */

module MODULE_NAME {
  tar_file = "MODULE_NAME.clm";

  /* Update the next few parameter values to indicate whether or not your
     module uses a phase_start,phase, or phase_end function */
  phase_start = FALSE;
  phase = FALSE;
  phase_end = FALSE;

  /* Update the next parameter to indicate whether or not your module
     reacts only to its inputs */
  reactive = TRUE;

  /* Define parameters (basic and algorithmic) here.
   *
   * The syntax is:
   *   parameter PARMNAME = DEFAULTVALUE : TYPE;
   * Note the default value is optional.
   *
   * Examples:
   *   parameter unblock_at_receive = TRUE : bool;
   *   parameter allow_to_pass_func : userpoint(
   *                "int instno, LSE_dynid_t id, LSE_port_type(src) *data" => 
   *                "int");
   */



  /* Define ports here.
   *
   * The syntax is:
   *   inport PORTNAME : TYPE;
   *   outport PORTNAME : TYPE;
   *
   * Examples:
   *   inport src:*;
   *   inport decode_known:[none|int];
   *   inport decision_made:none;
   */



  /* Define events here
   *
   * The syntax is:
   *  emits event EVENTNAME {
   *    DATANAME1 : <<<TYPE1>>>;
   *    DATANAME2 : <<<TYPE2>>>;
   *    ...
   *  };
   *
   * Example:
   *  emits event EMULATE {
   *    id : <<<LSE_dynid_t>>>;
   *    speculative : <<<bool>>>;
   *    ...
   *  };
   */



  /* Define methods here.
   *
   * The syntax is:
   *   method METHODNAME : (<<<PARAMS>>> => <<<RETURN-TYPE>>>);
   *
   * Examples:
   *   inport src:*;
   *   inport decode_known:[none|int];
   *   inport decision_made:none;
   */



  /* Define data field extensions via structadd here 
   *
   * The syntax is:
   *   structadd(this, "STRUCTURE", "TYPE", "NAME");
   *
   * Example:
   *   structadd(this, "LSE_dynid_t", "gboolean", "speculated");
   */
   
};
