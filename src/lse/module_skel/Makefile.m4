INSTALLFILES=MODULE_NAME`'.clm MODULE_NAME`'.lss

install: $(INSTALLFILES)
	if test ! -d MODULE_DIR; then \
	  mkdir MODULE_DIR; \
	fi; \
	list='$(INSTALLFILES)'; for p in $$list; do \
	  install -c -m 644 $$p MODULE_DIR/$$p; \
	done

uninstall: $(INSTALLFILES)
	list='$(INSTALLFILES)'; for p in $$list; do \
	  rm -f MODULE_DIR/$$p; \
	done
