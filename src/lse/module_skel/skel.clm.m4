/*% -*-c-*-
 * 
 * This is a one line description of the file.
 *
 * Authors: Author Name <email-addr@yourdomain.edu>
 *
 * This is a long description of the file that is usually one or two
 * paragraphs in length.  After reading it, one should be able to
 * understand the purpose of the code in this file without looking at
 * anything else.
 *
 */

class MODULE_NAME : public LSE_module {
/* Place Your Module global variables here.... */

public:
  boolean
  FUNC(init, void)
  {
    /* Put module initialization code here */
  }

  void
  FUNC(finish,void)
  {
    /* Put module cleanup code here */
  }

  void
  FUNC(phase_start, void)
  {
    /* Put cycle initialization code here */
    /* Make sure you handle unconnected ports */
  }

  void
  FUNC(phase, void)
  {
    /* Put code to handle ports, not handled by HANDLERS, here. */
  }


  /* 
   *
   * Modify and duplicate the following function to define the necessary
   * port handlers. 
   *
   */
  void
  HANDLER(put_your_port_name_here, int porti)
  {
    /*
     * Put code to handle this ports changes here. 
     * porti - the port instance number
    */
  }

  void 
  FUNC(phase_end, void)
  {
    /* Put state update code here and other end-of-cycle code here. */
  }

}; // class MODULE_NAME
