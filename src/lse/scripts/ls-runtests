#! /bin/sh
#! perl
eval '(exit $?0)' && eval 'exec perl -S -x $0 ${1+"$@"}'
                    & eval 'exec perl -S -x $0 $argv:q'
                            if $running_under_some_shell;
# /* 
#  * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Script to run module tests
#  *
#  * Authors: Manish Vachharajani <manishv@ee.princeton.edu>
#  *
#  */

use Getopt::Long;

$| = 1;

#### Check environment

$LSE = $0;
$LSE =~ s@/[^/]+/[^/]+$@@;
do $LSE . "/share/scripts/perlsubs.pl";

$LSE_DATA = $LSE . "/share";
$LSE_LIB = $LSE . "/lib";
$LSE_BIN = $LSE . "/bin";

$LIBERTY_SIM_USER_PATH = $ENV{"LIBERTY_SIM_USER_PATH"};

$LSE_SDATA  =  $LSE_DATA . "/lse";
$LSE_MDATA  =  $LSE_DATA . "/modlib";

##### Check arguments

Getopt::Long::Configure(('bundling','permute','no_getopt_compat'));

$help_wanted = '';
$skiplss=0;
$cleanbuild=0;
$reportrebuild=0;
@MADDPATH = ();
@MADDEPATH = ();
@EMULIST = ();

%options = (
    'help|h' => \$help_wanted,
    'mpathbeg=s' =>  \@MADDPATH,
    'mpathend=s' => \@MADDEPATH,
);

$ret = Getopt::Long::GetOptions(%options);
if(!$ret || $help_wanted) {
    &usage();
}

$MADDPATH=join(":", @MADDPATH);
if($MADDPATH ne "") {
    $MADDPATH .= ":";
}
$MADDEPATH=join(":", @MADDEPATH);
if($MADDEPATH ne "") {
    $MADDEPATH = ":" . $MADDEPATH;
}
$USERPATH = join(':',split(/:/,$LIBERTY_SIM_USER_PATH));
if ($USERPATH ne "" && 0) {
    $USERPATH .= "/modulelib" . ":";
}

$WORKDIR= `pwd`; chop($WORKDIR);

# Get LSS executable name

# Parse the configuration and put the module code into the appropriate
# directories

$MODPATH = $MADDPATH . 
    $USERPATH . "$LSE_SDATA:" . "$LSE_MDATA" . $MADDEPATH;

@MODDIRS=split(/:/,$MODPATH);
if($#ARGV != -1) {
    foreach $j (0..$#ARGV) {
	if($ARGV[$j]=~/^\//) {
	    &runtests($ARGV[$j]);
	} else {
            &search_and_runtests($ARGV[$j]);
	}
    }
} else {
    &search_and_runtests("");
}
####################### Subroutines #########################
sub search_and_runtests() {
    my($FD);
    my($ARG)=$_[0];
    foreach $i (0..$#MODDIRS) {
	$PKGFULLNAME=$ARG;
	$MP=$MODDIRS[$i];
        $FD="$MP/$PKGFULLNAME";
        next if(!(-e $FD));
	$TESTDIRS=`find $FD -name tests -print 2>/dev/null`;
	@DIRS=split(/\n/,$TESTDIRS);
	
	foreach $k (0..$#DIRS) {
	    &runtests($DIRS[$k]);
	}
    }
}

sub runtests() {
    my($k);
    my($TESTDIR)=$_[0];

    print "Running tests in $TESTDIR\n";
    &dosysfail("$TESTDIR/runtests $WORKDIR");
}

sub usage() {
    $prog = $0; $prog=~s+.*/++;
    die 
    ((($_[0] ne "") ? "$prog ERROR: $_[0]\n" : "" ) .
"
Usage: $prog [options] [packagename [more packagenames]]

 Builds a simulator from a machine configuration file.

 Arguments:
    packagename               - the name of directory on the search path 
                                containing tests subdirectories in the 
                                directory tree with a script called runtests

 Options:
    -h|-help                  - show this message
    --mpathbeg <path>         - add a module search path to the beginning of 
                                the path (can be repeated)
    --mpathend <path>         - add a module search path to the end of the path
                                (can be repeated)

 Environment variables:
	LIBERTY_SIM_USER_PATH - Additional places to search for .lss files

 Output locations:
   The tests are built and run in the current directory.

 Example:
   ls-runtests corelib

");
}
