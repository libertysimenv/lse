package Liberty.visualizer.document;

import Liberty.visualizer.view.*;

import java.awt.Component;
import java.io.*;

/** the purpose of the document class is to manage the
 *  file and all of the views associated with a given document */

public interface Document{

    public void buildSchematicView();

    public void buildDocument();

    public void cleanUp();

    public File getFile();

    public Component getMainComponent();

    public String getName();

    public String getPath();

    public SchematicView getSchematicView();

    public View getSourceView();

    public boolean isSourceViewVisible();

    public boolean isSchematicViewVisible();

    public void save();

    public void saveAs();

    public void saveProperties();

    public void removeProperty(String key);

    public void setProperty(String key, String value);
}
