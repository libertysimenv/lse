package Liberty.visualizer.document;

import Liberty.visualizer.*;
import Liberty.visualizer.lse.*;
import Liberty.visualizer.library.Library;
import Liberty.visualizer.lse.CompilationManager;
import Liberty.visualizer.UI.frame.*;
import Liberty.visualizer.UI.panel.LibraryPanel;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.view.*;
import Liberty.visualizer.view.lse.*;

import Liberty.LSS.IMR.*;
import Liberty.LSS.*;
import Liberty.LSS.AST.*;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * A class for manipulating an LSSDocument.
 *
 * @author Jason Blome
 * @version 0.1
 */

public class LSSDocument implements Document{
    /** The Compilation Manager for this document */
    private CompilationManager compilationManager;
    /** The Execution Manager for this document */
    private ExecutionManager executionManager;
    /** the file that this document is associated with */
    private File file;
    /** has this file been modified? */
    /** FIXME: this isn't currently used, but should be */
    private boolean isModified;
    /** the component displayed by the main panel when this doc is focussed */
    private LibraryPanel libraryPanel;
    /** The module library that is availabe */
    private Library moduleLibrary;
    /** The path to user-defined modules, set by the
     *  environment variable LIBERTY_SIM_USER_PATH  
     *  concatted with moduleLibraryPath */
    private String moduleSearchPath;
    /** The file containing SchematicView properties */
    private File propertyFile;
    /** The set of properties loaded from propertyFile */
    private LinkedHashMap properties;
    /** the schematic view of this compiled code */
    private SchematicView schematicView;
    /** the source view of this compiled document */
    private SourceView sourceView;
    /** a temporary string used to name this file until it is saved */
    private String tmpFileName;	
    
    public LSSDocument(File file)
    {
	this.file = file;
	this.tmpFileName = null;
	this.isModified = false;

	String moduleSearchPath = 
	    System.getProperty("Liberty.visualizer.module_search_path");

	System.out.println("SEARCH PATH: "+moduleSearchPath);

	String parentFile = file.getParent();
	if(parentFile != null)
	    moduleSearchPath += (":"+parentFile);

	this.moduleLibrary = new Library(moduleSearchPath);
	this.libraryPanel = new LibraryPanel(moduleLibrary);
	this.compilationManager = new CompilationManager(this);
	this.executionManager = new ExecutionManager(this);

	// look for the property file in the standard location
	loadProperties();
    }

    public LSSDocument(String fileName)
    {
	this.file = null;
	this.tmpFileName = fileName;
	this.isModified = false;

	String moduleSearchPath = 
	    System.getProperty("Liberty.visualizer.module_search_path");
	this.moduleLibrary = new Library(moduleSearchPath);
	this.libraryPanel = new LibraryPanel(moduleLibrary);
	this.propertyFile = null;
    }

    public void cleanUp()
    {
	if(schematicView != null){
	    schematicView.setVisible(false);
	    schematicView = null;
	}
	if(sourceView != null){
	    sourceView.setVisible(false);
	    sourceView = null;
	}
    }

    public boolean isSourceViewVisible()
    {
	return sourceView.isVisible();
    }

    public boolean isSchematicViewVisible()
    {
	if(schematicView == null)
	    return false;
	return schematicView.isVisible();
    }


    public void loadProperties()
    {
	String path = file.getAbsolutePath()+".properties";
	this.propertyFile = new File(path);
	this.properties = new LinkedHashMap();

	if(propertyFile.exists()){
	    try{
		BufferedReader reader = 
		    new BufferedReader(new FileReader(propertyFile));
		String propertyString;
		while((propertyString = reader.readLine()) != null){
		    int index = propertyString.indexOf("=");
		    if(index != -1){
			String key = propertyString.substring(0, index);
			String value = 
			    propertyString.substring(index + 1, 
						     propertyString.length());

			properties.put(key, value);
		    }
		}
		//properties.load(new FileInputStream(propertyFile));
	    }
	    catch(IOException ioe){
		//FIXME: should be a JOptionPane
		System.out.println(ioe.getMessage());
	    }
	}
    }

    public CompilationManager getCompilationManager()
    {
	return compilationManager;
    }

    public ExecutionManager getExecutionManager()
    {
	return executionManager;
    }

    public File getFile()
    {
	return file;
    }

    public Component getMainComponent()
    {
	return libraryPanel;
    }

    public String getModuleSearchPath()
    {
	return moduleLibrary.getModuleSearchPath();
    }

    public String getName()
    {
	if(file == null)
	    return tmpFileName;
	return file.getName();
    }

    public String getPath()
    {
	if(file == null)
	    return tmpFileName;
	return file.getAbsolutePath();
    }

    public java.util.List getPropertiesForViewElement(String elementName)
    {
	Iterator keyIterator = properties.keySet().iterator();
	ArrayList propertyList = new ArrayList();

	while(keyIterator.hasNext()){
	    String key = (String)keyIterator.next();
	    int index = key.lastIndexOf(".");

	    if(index > 0){
		String propertyHolderName = key.substring(0, index);

		if(propertyHolderName.equals(elementName)){
		    String propertyString = (String)properties.get(key);
		    PropertyInfo info = 
			PropertyInfo.parseProperty(key, propertyString);
		    propertyList.add(info);
		}
	    }
	}
	return propertyList;
    }

    public void buildDocument()
    {
	Runnable compilationRunnable = new Runnable(){
		public void run(){
		    LSSDocument.this.compilationManager.
			beginSimBuild();
		}
	    };
	new Thread(compilationRunnable).start();
    }

    public void buildSchematicView()
    {
 	this.schematicView = 
 	    new LSESchematicView(this, getName());
 	ViewBuilder.buildSchematicView(this, (LSESchematicView)schematicView);
    }
	
    public SchematicView getSchematicView()
    {
	if(schematicView == null){
	    buildSchematicView();
	}
	return schematicView;
    }

    public View getSourceView()
    {
	if(sourceView == null)
	    this.sourceView = new SourceView(this);
	return sourceView;
    }

    public void save()
    {
	if(file != null){
	    try{
		FileWriter writer = new FileWriter(file);
		String text = sourceView.getText();
		writer.write(text, 0, text.length());
		writer.flush();
	    }
	    catch(Exception e){
		JOptionPane.showMessageDialog(null, "Error saving document: "
					      + file.getAbsolutePath()+"\n"+
					      e.getMessage(), 
					      "alert", 
					      JOptionPane.ERROR_MESSAGE);
	    }
	}
	else{
	    saveAs();
	}
    }

    public void saveAs()
    {
	JFileChooser chooser = 
	    new JFileChooser(System.getProperty("user.dir"));
	int ret = chooser.showSaveDialog(null);
	if(ret != JFileChooser.APPROVE_OPTION)
	    return;
	else{
	    String oldPath;

	    if(this.file != null)
		oldPath = file.getAbsolutePath();
	    else
		oldPath = tmpFileName;

	    this.file = chooser.getSelectedFile();
	    String path = this.file.getAbsolutePath();
	    if(sourceView != null)
		sourceView.setTitle(path);
	    if(schematicView != null)
		schematicView.setTitle(path);

	    this.file = new File(path);
	    VisualizerActions.renameDocument(oldPath, path);
	    save();
	}
    }

    public void saveProperties()
    {
	if(schematicView != null){
	    if(propertyFile.exists()){
		try{
		    File tmpFile = 
			new File(propertyFile.getAbsolutePath()+"~");

		    if(tmpFile.exists())
			tmpFile.delete();

		    tmpFile.createNewFile();
		    
		    FileReader in = new FileReader(propertyFile);
		    FileWriter out = new FileWriter(tmpFile);
		    int c;
		    
		    while ((c = in.read()) != -1)
			out.write(c);
		    
		    out.flush();
		    in.close();
		    out.close();
		    
		    propertyFile.delete();
		    propertyFile.createNewFile();
		}
		catch(IOException ioe){
		    System.out.println(ioe.getMessage());
		}
	    }

	    schematicView.storeProperties();
	    // now dump the properties into propertyFile
	    FileOutputStream fos;

	    try{
		PrintWriter out = 
		    new PrintWriter(new 
		    BufferedWriter(new FileWriter(propertyFile)));
		Iterator keyIterator = properties.keySet().iterator();
		while(keyIterator.hasNext()){
		    String key = (String)keyIterator.next();
		    String value = (String)properties.get(key);
		    out.println(key+"="+value);
		}
		out.flush();
	    }
	    catch(IOException ioe){
		System.out.println(ioe.getMessage());
	    }
	}
    }

    public void removeProperty(String key)
    {
	properties.remove(key);
    }

    public void setProperty(String key, String value)
    {
	properties.put(key, value);
    }

    public void updateModuleSearchPath(String searchPath)
    {
	moduleLibrary.updateModuleSearchPath(searchPath);
	libraryPanel.update();
    }
}

