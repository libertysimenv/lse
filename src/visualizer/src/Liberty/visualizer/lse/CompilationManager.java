package Liberty.visualizer.lse;

import Liberty.util.*;
import Liberty.LSS.types.*;
import Liberty.LSS.AST.*;
import Liberty.LSS.IMR.*;
import Liberty.LSS.*;

import Liberty.visualizer.document.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.prefs.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;

public class CompilationManager{

    private LSSDocument document;
    private String executableFileName;
    private IMRCompilationDialog icd;
    private IMRCompiler compiler;

    public CompilationManager(LSSDocument document)
    {
	this.document = document;

	Preferences prefs =  
	    Preferences.userNodeForPackage(getClass());

	this.executableFileName = prefs.get(document.getName()+
					    ".executableFileName", "");
    }

    public String getExecutableFileName()
    {
	return executableFileName;
    }

    public HashMap buildIMR()
    {
	try{
	    IMRCompilationDialog icd = 
		new IMRCompilationDialog(document.getName());
	    
	    Thread icdThread = new Thread(icd);
	    icdThread.start();
	    
	    IMRCompiler compiler = new IMRCompiler(document, icd);
	    
	    Thread compilerThread = new Thread(compiler);
	    
	    compilerThread.start();
	    compilerThread.join();
	    icd.enableOkButton();

	    return compiler.getInstances();
	}
	catch(InterruptedException ie){
	    JOptionPane.showMessageDialog(null,
					  "Thread Exception occured: '" + 
					  ie.getMessage()+
					  "'\nCould not build file: "+
					  document.getName(), 
					  "alert", 
					  JOptionPane.ERROR_MESSAGE);
	}

	return null;
    }

    public void beginSimBuild()
    {
	if(document.getFile() == null){
	    JOptionPane.showMessageDialog(null, "Could not build file: '"+
					  document.getPath()+
					  "' try saving the file first", 
					  "alert", JOptionPane.ERROR_MESSAGE);
	    return;
	}
	
	final JDialog buildOptionsDialog = new JDialog();
	buildOptionsDialog.setTitle("Buid Options");
	
	File file = document.getFile();
	String documentName = document.getName();
	int index = documentName.lastIndexOf(".");
	
	if(index > 0){
	    documentName = documentName.substring(0, index);
	}
	String outputDir = file.getParentFile().getAbsolutePath()+
	    "/machines/"+documentName;

	final Preferences prefs =  
	    Preferences.userNodeForPackage(getClass());

	String backupDir = prefs.get(document.getName()+".backupDir", "");
	String mpathbeg = prefs.get(document.getName()+".mpathbeg", "");
	String mpathend = prefs.get(document.getName()+".mpathend", "");
	String cflags = prefs.get(document.getName()+".cflags", "");
	outputDir = prefs.get(document.getName()+".outputDir", outputDir);
	boolean skiplss = 
	    prefs.getBoolean(document.getName()+".skiplss", false);
	boolean cleanBuild = 
	    prefs.getBoolean(document.getName()+".cleanBuild", false);
	boolean linkOnly = 
	    prefs.getBoolean(document.getName()+".linkOnly", false);
	boolean linkVisualizer = 
	    prefs.getBoolean(document.getName()+".linkVisualizer", true);


	final JTextField outputDirField = 
	    new JTextField(outputDir, 30);
	outputDirField.setCaretPosition(0);
	final JTextField backupDirField = new JTextField(backupDir);
	final JTextField mpathBegField = new JTextField(mpathbeg);
	final JTextField mpathEndField = new JTextField(mpathend);
	final JTextField cflagsField = new JTextField(cflags);
	final JCheckBox skiplssBox = new JCheckBox("", skiplss);
	final JCheckBox cleanBox = new JCheckBox("", cleanBuild);
	final JCheckBox linkBox = new JCheckBox("", linkOnly);
	final JCheckBox linkVisBox = new JCheckBox("", linkVisualizer);

	JPanel optionsPanel = new JPanel();
	JPanel buttonPanel = new JPanel();
	GridBagLayout gridLayout = new GridBagLayout();
	GridBagConstraints constraints = new GridBagConstraints();
	JButton okButton = new JButton("Ok");
	JButton cancelButton = new JButton("Cancel");
	JLabel fieldLabel;

	constraints.insets = new Insets(4, 4, 4, 4);
	constraints.anchor = GridBagConstraints.WEST;
	constraints.weightx = 0.0;
	constraints.gridx = 0;
	constraints.gridy = 0;

	optionsPanel.setLayout(gridLayout);
	optionsPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

	fieldLabel = new JLabel("Output Directory: ");
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.gridx = 1;
	gridLayout.setConstraints(outputDirField, constraints);
	optionsPanel.add(outputDirField);

 	fieldLabel = new JLabel("mpathbeg: ");
	constraints.weightx = 0.0;
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	constraints.gridx = 0;
	constraints.gridy = 1;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.gridx = 1;
	gridLayout.setConstraints(mpathBegField, constraints);
	optionsPanel.add(mpathBegField);

	fieldLabel = new JLabel("mpathend: ");
	constraints.weightx = 0.0;
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	constraints.gridx = 0;
	constraints.gridy = 2;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.gridx = 1;
	gridLayout.setConstraints(mpathEndField, constraints);
	optionsPanel.add(mpathEndField);

	fieldLabel = new JLabel("cflags: ");
	constraints.weightx = 0.0;
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	constraints.gridx = 0;
	constraints.gridy = 3;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.gridx = 1;
	gridLayout.setConstraints(cflagsField, constraints);
	optionsPanel.add(cflagsField);

	fieldLabel = new JLabel("skip lss: ");
	constraints.weightx = 0.0;
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	constraints.gridx = 0;
	constraints.gridy = 4;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.gridx = 1;
	gridLayout.setConstraints(skiplssBox, constraints);
	optionsPanel.add(skiplssBox);

	fieldLabel = new JLabel("clean build: ");
	constraints.weightx = 0.0;
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	constraints.gridx = 0;
	constraints.gridy = 5;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.gridx = 1;
	gridLayout.setConstraints(cleanBox, constraints);
	optionsPanel.add(cleanBox);

	fieldLabel = new JLabel("link only: ");
	constraints.weightx = 0.0;
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	constraints.gridx = 0;
	constraints.gridy = 6;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.gridx = 1;
	gridLayout.setConstraints(linkBox, constraints);
	optionsPanel.add(linkBox);

	fieldLabel = new JLabel("link to visualizer: ");
	constraints.weightx = 0.0;
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	constraints.gridx = 0;
	constraints.gridy = 7;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.gridx = 1;
	gridLayout.setConstraints(linkVisBox, constraints);
	optionsPanel.add(linkVisBox);

	JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
	constraints.gridx = 0;
	constraints.gridy = 8;
	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	gridLayout.setConstraints(separator, constraints);
	optionsPanel.add(separator);

	okButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    final String mpathbeg = mpathBegField.getText();
		    final String mpathend = mpathEndField.getText();
		    final String cflags = cflagsField.getText();
		    final String outputDir = outputDirField.getText();
		    final boolean skiplss = skiplssBox.isSelected();
		    final boolean cleanBuild = cleanBox.isSelected();
		    final boolean linkOnly = linkBox.isSelected();
		    final boolean linkVisualizer = linkVisBox.isSelected();
		    final LSSDocument document = 
			CompilationManager.this.document;

		    prefs.put(document.getName()+".mpathbeg", mpathbeg); 
		    prefs.put(document.getName()+".mpathend", mpathend);
		    prefs.put(document.getName()+".cflags", cflags);
		    prefs.put(document.getName()+".outputDir", outputDir);
		    prefs.putBoolean(document.getName()+".skiplss", skiplss);
		    prefs.putBoolean(document.getName()+".cleanBuild", 
				     cleanBuild);
		    prefs.putBoolean(document.getName()+".linkOnly", linkOnly);
		    prefs.putBoolean(document.getName()+".linkVisualizer", 
				     linkVisualizer);

		    // FIXME: do error checking!

		    buildOptionsDialog.dispose();
		    Runnable runnable = new Runnable(){
			    public void run(){
				String docmpath = 
				    document.getModuleSearchPath();
				buildSim(docmpath+":"+mpathbeg, 
					 mpathend, cflags, 
					 outputDir, skiplss, cleanBuild,
					 linkOnly, linkVisualizer);
			    }
			};
		    new Thread(runnable).start();
		}
	    });

	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    buildOptionsDialog.dispose();
		}
	    });

	buttonPanel.add(okButton);
	buttonPanel.add(cancelButton);

	Container contentPane = buildOptionsDialog.getContentPane();
	contentPane.add(optionsPanel, BorderLayout.CENTER);
	contentPane.add(buttonPanel, BorderLayout.SOUTH);

	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	Dimension size = buildOptionsDialog.getPreferredSize();

	buildOptionsDialog.setBounds((screen.width-size.width)/2, 
				     (screen.height-size.height)/2, 
				     size.width, size.height+50);
	buildOptionsDialog.show();
    }

    private void buildSim(String mpathbeg, String mpathend, String cflags, 
			  String outputDir, boolean skiplss, 
			  boolean cleanBuild, boolean linkOnly, 
			  boolean linkVisualizer)
    {
 	try{
	    SimCompilationDialog scd = 
		new SimCompilationDialog(document.getName());
 	    scd.start();

	    SimCompiler compiler = new SimCompiler(document.getPath(),
						   scd,
						   mpathbeg,
						   mpathend,
						   cflags,
						   outputDir,
						   skiplss,
						   cleanBuild,
						   linkOnly,
						   linkVisualizer);

 	    compiler.start();
 	    compiler.join();

	    scd.enableOkButton();
	    
	    if(compiler.getBuildResult() == 0 && 
	       compiler.getLinkResult() == 0){
		Preferences prefs =  
		    Preferences.userNodeForPackage(getClass());

		executableFileName = compiler.getExecutableName();
		prefs.put(document.getName()+".executableFileName", "");
		document.getExecutionManager().
		    setExecutableFileName(executableFileName);
	    }
	}
	catch(InterruptedException ie){
	    JOptionPane.showMessageDialog(null,
					  "Thread Exception occured: '" + 
					  ie.getMessage()+
					  "'\nCould not build file: "+
					  document.getName(), 
					  "alert", 
					  JOptionPane.ERROR_MESSAGE);
	}
    }
}

class IMRCompilationDialog extends Thread{
    private JEditorPane editor;
    private JButton okButton;
    private String documentName;
    
    public IMRCompilationDialog(String documentName)
    {
	this.documentName = documentName;
	this.editor = new JEditorPane("text/rtf", "");
    }

    public void enableOkButton()
    {
	okButton.setEnabled(true);
    }

    public void appendText(String text, boolean bold, boolean red){
	javax.swing.text.Document document = editor.getDocument();
	SimpleAttributeSet attrs = new SimpleAttributeSet();

	if(bold){
	    StyleConstants.setBold(attrs, true);
	}
	if(red){
	    StyleConstants.setForeground(attrs, Color.red);
	}

	try{
	    int length = document.getLength();
	    document.insertString(length, text, attrs);
	    editor.setCaretPosition(document.getLength());
	}
	catch(BadLocationException ble){}		
    }

    public void run(){
	final javax.swing.text.Document 
	    document = editor.getDocument();

	editor.setEditable(false);
				
	JScrollPane scroll = new JScrollPane(editor);
	scroll.setPreferredSize(new Dimension(450, 300));
	scroll.setBorder(new CompoundBorder(
			     new EmptyBorder(5, 5, 5, 5),
			     new BevelBorder(BevelBorder.LOWERED)));

	JPanel mainPanel = new JPanel();
		
	final JDialog dialog = new JDialog();

	dialog.setTitle("Build Results");
	dialog.setModal(false);
		
	JPanel buttonPanel = new JPanel();
		
	okButton = new JButton("Ok");
	okButton.setEnabled(false);
	okButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    dialog.dispose();
		}
	    });
		
	buttonPanel.add(okButton);
		
	Container contentPane = dialog.getContentPane();
	JPanel panel = new JPanel();
	panel.setBorder(new EmptyBorder(2, 2, 2, 2));
	panel.add(scroll, BorderLayout.CENTER);
	contentPane.add(panel, BorderLayout.CENTER);
	contentPane.add(buttonPanel, BorderLayout.SOUTH);
		
	Dimension screen = 
	    Toolkit.getDefaultToolkit().getScreenSize();
		
	Dimension size = dialog.getPreferredSize();
		
	double w = size.width;
	double h = size.height+20;
	double x = (screen.width - w)/2; 
	double y = (screen.height - h)/2; 
		
	dialog.setBounds((int)x, (int)y, (int)w, (int)h);
	dialog.setVisible(true);
    }
}

class SimCompilationDialog extends Thread{
    private final JEditorPane buildEditor;
    private final JEditorPane linkEditor;
    private String documentName;
    private JButton okButton;

    public SimCompilationDialog(String documentName)
    {
	this.documentName = documentName;

	this.buildEditor = new JEditorPane("text/rtf", "");
	this.linkEditor = new JEditorPane("text/rtf", "");
    }

    public void enableOkButton()
    {
	okButton.setEnabled(true);
    }    

    public void appendBuildText(String text, boolean bold, boolean red){
	javax.swing.text.Document document = buildEditor.getDocument();
	SimpleAttributeSet attrs = new SimpleAttributeSet();

	if(bold){
	    StyleConstants.setBold(attrs, true);
	}
	if(red){
	    StyleConstants.setForeground(attrs, Color.red);
	}

	try{
	    int length = document.getLength();
	    document.insertString(length, text, attrs);
	    buildEditor.setCaretPosition(document.getLength());
     	}
	catch(BadLocationException ble){
	    System.out.println("ble: "+ble.getMessage());
	}
    }

    public void appendLinkText(String text, boolean bold, boolean red){
	javax.swing.text.Document document = linkEditor.getDocument();
	SimpleAttributeSet attrs = new SimpleAttributeSet();

	if(bold){
	    StyleConstants.setBold(attrs, true);
	}
	if(red){
	    StyleConstants.setForeground(attrs, Color.red);
	}

	try{
	    int length = document.getLength();
	    document.insertString(length, text, attrs);
	    linkEditor.setCaretPosition(document.getLength());
	}
	catch(BadLocationException ble){}		
    }

    public void run(){
	final javax.swing.text.Document 
	    buildDocument = buildEditor.getDocument();

	buildEditor.setEditable(false);

	final javax.swing.text.Document 
	    linkDocument = linkEditor.getDocument();

	linkEditor.setEditable(false);

	JScrollPane buildScroll = new JScrollPane(buildEditor);
	buildScroll.setPreferredSize(new Dimension(450, 300));
	buildScroll.setBorder(new CompoundBorder(
			     new EmptyBorder(5, 5, 5, 5),
			     new BevelBorder(BevelBorder.LOWERED)));


	JScrollPane linkScroll = new JScrollPane(linkEditor);
	linkScroll.setPreferredSize(new Dimension(450, 300));
	linkScroll.setBorder(new CompoundBorder(
			     new EmptyBorder(5, 5, 5, 5),
			     new BevelBorder(BevelBorder.LOWERED)));

	JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
					      buildScroll,
					      linkScroll);

	JPanel mainPanel = new JPanel();
		
	final JDialog dialog = new JDialog();
		
	dialog.setModal(false);

	dialog.setTitle("Build and Link Results: "+documentName);
		
	JPanel buttonPanel = new JPanel();
		
	okButton = new JButton("Ok");
	okButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    dialog.dispose();
		}
	    });
	okButton.setEnabled(false);	
	buttonPanel.add(okButton);
		
	Container contentPane = dialog.getContentPane();
	JPanel panel = new JPanel();
	panel.setBorder(new EmptyBorder(2, 2, 2, 2));
	panel.add(splitPane, BorderLayout.CENTER);
	contentPane.add(panel, BorderLayout.CENTER);
	contentPane.add(buttonPanel, BorderLayout.SOUTH);
		
	Dimension screen = 
	    Toolkit.getDefaultToolkit().getScreenSize();
		
	Dimension size = dialog.getPreferredSize();
		
	double w = size.width;
	double h = size.height+20;
	double x = (screen.width - w)/2; 
	double y = (screen.height - h)/2; 
		
	dialog.setBounds((int)x, (int)y, (int)w, (int)h);
	dialog.setVisible(true);

	SimpleAttributeSet boldAttrs = new SimpleAttributeSet();
	StyleConstants.setBold(boldAttrs, true);
    }
}

class IMRCompiler extends Thread{

    private LSSDocument document;
    private HashMap instances;
    private IMRCompilationDialog icd;

    public IMRCompiler(LSSDocument document, IMRCompilationDialog icd)
    {
	this.document = document;
	this.icd = icd;
    }

    public HashMap getInstances()
    {
	return instances;
    }

    public void run()
    {
	final PipedInputStream stdoutInputPipe;
	final PipedOutputStream stdoutOutputPipe;
	
	final PipedInputStream stderrInputPipe;
	final PipedOutputStream stderrOutputPipe;

	try{
	    stdoutInputPipe = new PipedInputStream();
	    stdoutOutputPipe = new PipedOutputStream(stdoutInputPipe);
	    
	    stderrInputPipe = new PipedInputStream();
	    stderrOutputPipe = new PipedOutputStream(stderrInputPipe);
	}
        catch(IOException ioe){
            JOptionPane.showMessageDialog(null,
                                          "IO Exception occured: '" +
                                          ioe.getMessage()+
                                          "'\nCould not build file: "+
                                          document.getName(),
                                          "alert",
                                          JOptionPane.ERROR_MESSAGE);
	    return;
        }

	PrintStream oldErr = System.err;
	PrintStream oldOut = System.out;

 	PrintStream newErr = new PrintStream(stdoutOutputPipe);
 	PrintStream newOut = new PrintStream(stderrOutputPipe);
		
	// redirect stdout and stderr, then we have to 
	// check them often
	System.setErr(newErr);
	System.setOut(newOut);

	icd.appendText("Building: '"+document.getName()+
		       "'\n\n", true, false);

	// start the output reader threads
	Runnable stdoutReader = new Runnable(){
		public void run(){
		    BufferedReader reader = 
		    new BufferedReader(new InputStreamReader(stdoutInputPipe));
		    String line;
		    try{
			while((line = reader.readLine()) != null)
			    icd.appendText(line+"\n", false, false);
		    }
		    catch(Exception e){}
		}
	    };
	Thread stdoutThread = new Thread(stdoutReader);
	stdoutThread.start();

	Runnable stderrReader = new Runnable(){
		public void run(){
		    BufferedReader reader = 
		    new BufferedReader(new InputStreamReader(stderrInputPipe));
		    String line;
		    try{
			while((line = reader.readLine()) != null)
			    icd.appendText(line+"\n", false, true);
		    }
		    catch(Exception e){}
		}
	    };
	Thread stderrThread = new Thread(stderrReader);
	stderrThread.start();


	// do the build
	boolean error;
	String errorString;
	    
	String filename = document.getPath();
	String outputDir = 
	    System.getProperty("Liberty.visualizer.output_dir");
	String modulePath = document.getModuleSearchPath();
	
	StatementList sl = null;
	StatementList parmdeclSL = null;
	StatementList builtinsSL = null;
	StatementList startupSL = null;
	Collection topLevelParms;
		
	ExecutionContext c = Main.createContext(modulePath);
	InstanceEnvironment env = 
	    c.instanceSpace.getCurrentEnvironment();
		
	error = false;
	errorString = "";

	try {
	    /* Read and parse input */
	    Reader in;
	    String parmdeclFilename;
	    String builtinsFilename;
	    String startupFilename;
	    
	    builtinsFilename = c.getFileName("LSS_builtins.lss");
	    in = new FileReader(builtinsFilename);
	    builtinsSL = 
		IncludeStatement.parseFile(in, builtinsFilename);
	    
	    stdoutOutputPipe.flush();
	    stderrOutputPipe.flush();
	    
	    parmdeclFilename = 
		c.getFileName("framework" + File.separator + 
			      "LSE_parmdecl.lss");
	    in = new FileReader(parmdeclFilename);
	    parmdeclSL = 
		IncludeStatement.parseFile(in, parmdeclFilename);
	    
	    startupFilename = 
		c.getFileName("framework" + File.separator + 
			      "LSE_startup.lss");
	    in = new FileReader(startupFilename);
	    startupSL = 
		IncludeStatement.parseFile(in, startupFilename);
	    
	    stdoutOutputPipe.flush();
	    stderrOutputPipe.flush();
	    
	    if(filename == null) {
		in = new InputStreamReader(System.in);
		filename = "";
	    } else {
		in = new FileReader(filename);
	    }
	    
	    sl = IncludeStatement.parseFile(in, filename);
	    
	} catch(IOException e) {
	    errorString += e.getMessage();
	    error = true;
	} catch(Exception e) { /* CUP produces this */ 
	    errorString += e.getMessage();
	    error = true;
	}
	
	instances = null;
	LinkedHashMap globalTypes;
	Collection runtimeVars;

	if(!error){
	    try {

		Main.bootstrapExecutionContext(c, 
					       parmdeclSL, 
					       builtinsSL,
					       startupSL);
		
		stdoutOutputPipe.flush();
		stderrOutputPipe.flush();
		
		instances = Main.processStatementList(sl,c);
		
		topLevelParms = Main.processTopLevelParms(c);
		
		Module.copyCollectors(instances, c);
		
		
		/* Check to see if there is at least 1 instance */
		if(instances.size() == 0) {
		    throw new TypeException("Error: The "+
					    "configuration "+
					    "has no instances");
		}
		
		stdoutOutputPipe.flush();
		stderrOutputPipe.flush();
		
		/* Perform type inference */
		runtimeVars = c.instanceSpace.getRuntimeVars();
		ConnectionManagement.
		    connectionsOk(instances.values());
		
		stdoutOutputPipe.flush();
		stderrOutputPipe.flush();
		
		ConnectionManagement.
		    inferTypes(instances, 
			       c.instanceSpace,
			       topLevelParms,
			       runtimeVars,
			       env.getConstraints());
		


		globalTypes = c.instanceSpace.getGlobalTypes();
		globalTypes = 
		    InstanceSpace.filterGlobalTypes(globalTypes);
		
		stdoutOutputPipe.flush();
		stderrOutputPipe.flush();
		
		stdoutOutputPipe.close();
		stderrOutputPipe.close();
		
	    } catch(LSSException e) {
		errorString += e.getMessage();
		error = true;
		try{
		    stdoutOutputPipe.flush();
		    stderrOutputPipe.flush();
		    
		    stdoutOutputPipe.close();
		    stderrOutputPipe.close();
		}
		catch(IOException ioe){
		}
		
	    } catch(Exception e) { /* This is bad if it happens */
		errorString += e.getMessage();
		error = true;
		try{
		    stdoutOutputPipe.flush();
		    stderrOutputPipe.flush();
		    
		    stdoutOutputPipe.close();
		    stderrOutputPipe.close();
		}
		catch(IOException ioe){}
	    }
	}

	try{
	    stdoutThread.join();
	    stderrThread.join();
	}
	catch(InterruptedException ie){
	    JOptionPane.showMessageDialog(null,
					  "Thread Exception occured: '" + 
					  ie.getMessage()+
					  "'\nCould not build file: "+
					  document.getName(), 
					  "alert", 
					  JOptionPane.ERROR_MESSAGE);
	}
		    		
	if(!error)
	    icd.appendText("\nBuild Succeeded\n", true, false);
	else
	    icd.appendText("\nBuild Error: " + 
			   errorString+"\n", true, true);

	System.setErr(oldErr);
	System.setOut(oldOut);
	System.out.println("Build Complete.");
	return;
    }
}

class SimCompiler extends Thread{

    private String path;
    private SimCompilationDialog dialog;
    private String buildCommand;
    private String[] buildOptions;
    private String[] linkOptions;
    private String linkCommand;
    private int buildResult;
    private int linkResult;
    private String executableName;
    private boolean linkOnly;

    public SimCompiler(String path,
		       SimCompilationDialog dialog,
		       String mpathBeg,
		       String mpathEnd,
		       String cflags,
		       String outputDir,
		       boolean skiplss,
		       boolean cleanBuild,
		       boolean linkOnly,
		       boolean linkVis)
    {
	buildOptions = new String[1];
	linkOptions = new String[1];
	ArrayList buildOptionsList = new ArrayList();

	this.dialog = dialog;
	this.buildResult = this.linkResult = -1;
	this.path = path;
	this.buildCommand = "ls-build";
	buildOptionsList.add(new String("ls-build"));

	if(!mpathBeg.equals("")){
	    buildOptionsList.add(new String("--mpathbeg"));
	    buildOptionsList.add(new String(mpathBeg));
	}
	if(!mpathEnd.equals("")){
	    buildOptionsList.add(new String(" --mpathend"));
	    buildOptionsList.add(new String(mpathEnd));
	}
	if(!cflags.equals("")){
	    buildOptionsList.add(new String("--cflags"));
	    buildOptionsList.add(new String(cflags));
	}
	if(cleanBuild){
	    buildOptionsList.add(new String("--clean"));
	}
	if(skiplss)
	    buildOptionsList.add(new String("--skiplss"));
	if(outputDir.equals("")){
	    buildOptionsList.add(new String(path));
	}
	else{
	    buildOptionsList.add(new String("--output_dir"));
	    buildOptionsList.add(new String(outputDir));
	    buildOptionsList.add(new String(path));
	}
	this.buildOptions = (String[])buildOptionsList.toArray(buildOptions);
	
	String libraryPath = 
	    System.getProperties().getProperty("liberty")+"/lib/";
	
	this.linkCommand = "ls-link";
	ArrayList linkOptionsList = new ArrayList();

	linkOptionsList.add(new String("ls-link"));

	if(linkVis){

	    //linkOptionsList.add(new String("--noclp"));
	    // all the libraries are implied by the visualizer domain and
	    // the link order ensures that the clp library is not included
	}

	if(outputDir.equals("")){
	    linkOptionsList.add(path);
	    int index = path.lastIndexOf("/");
	    this.executableName = path.substring(0, index)+"/Xsim";
	}
	else{
	    linkOptionsList.add(new String("--machine_dir"));
	    linkOptionsList.add(new String(outputDir));
	    linkOptionsList.add(new String("--output_dir"));
	    linkOptionsList.add(new String(outputDir+"/"));
	    linkOptionsList.add(new String(path));
	    int index = path.lastIndexOf("/");
	    this.executableName = outputDir+"/Xsim";

	}
	this.linkOptions = (String[])linkOptionsList.toArray(linkOptions);
	this.linkOnly = linkOnly;
    }

    public String getExecutableName()
    {
	return executableName;
    }

    public int getBuildResult()
    {
	return buildResult;
    }

    public int getLinkResult()
    {
	return linkResult;
    }

    public void run(){
	Runtime runtime = Runtime.getRuntime();

	try{
	    String optionsString = " ";
	    Process proc;

	    if(linkOnly){
		this.buildResult = 0;
		dialog.appendBuildText("Simulator build not requested, "+
				       "linking only.", true, false);
	    }
	    else{
		for(int i = 0; i < buildOptions.length; i++)
		    optionsString += (buildOptions[i]+" ");
		dialog.appendBuildText("Building Document: "+path+"\n"+
				       "Executing Command: '"+optionsString+
				       "'\n\n", true, false);

		proc = runtime.exec(buildOptions);

		InputStream buildStdout = proc.getInputStream();

		InputStreamReader buildStdoutReader = 
		    new InputStreamReader(buildStdout);
		final BufferedReader br1 = 
		    new BufferedReader(buildStdoutReader);
		Thread reader1 = new Thread(){
			public void run(){
			    String line;
			    try{
				while((line = br1.readLine())!=null){
				    SimCompiler.this.dialog.
					appendBuildText(line+"\n", 
							false, false);
				}
			    }
			    catch(Exception e){
				System.out.println("Error: "+e.getMessage());
			    }
			}
		    };
		reader1.start();

		InputStream buildStderr = proc.getErrorStream();
		
		InputStreamReader buildStderrReader = 
		    new InputStreamReader(buildStderr);
		final BufferedReader br2 = 
		    new BufferedReader(buildStderrReader);

		Thread reader2 = new Thread(){
			public void run(){
			    String line;
			    try{
				while((line = br2.readLine())!=null)
				    SimCompiler.this.dialog.
					appendBuildText(line+"\n", 
							false, true);
			    }
			    catch(Exception e){
				System.out.println("Error: "+e.getMessage());
			    }
			}
		    };
		reader2.start();
		    	    
		this.buildResult = proc.waitFor();
		dialog.appendBuildText("Build completed"+
				       "\n\nBuild exit value: " + buildResult 
				       +"\n\n",
				       true, false);
	    }

	    optionsString = " ";
	    for(int i = 0; i < linkOptions.length; i++)
		optionsString += (linkOptions[i]+" ");
	    dialog.appendLinkText("Linking Document: "+path+"\n"+
				  "Executing Command: '"+optionsString+
				  "'\n\n", 
				  true, false);
	    
	    proc = runtime.exec(linkOptions);
	    
	    InputStream linkStdout = proc.getInputStream();
	    InputStreamReader linkStdoutReader = 
		new InputStreamReader(linkStdout);
	    final BufferedReader br3 = new BufferedReader(linkStdoutReader);
	    Thread reader3 = new Thread(){
		    public void run(){
			String line;
			try{
                            while((line = br3.readLine())!=null)
				SimCompiler.this.dialog.
				    appendLinkText(line+"\n", false, false);
                        }
                        catch(Exception e){
			    System.out.println("Error: "+e.getMessage());
			}
		    }
		};
	    reader3.start();

	    InputStream linkStderr = proc.getErrorStream();
	    InputStreamReader linkStderrReader = 
		new InputStreamReader(linkStderr);
	    final BufferedReader br4 = new BufferedReader(linkStderrReader);
	    Thread reader4 = new Thread(){
		    public void run(){
			String line;
			try{
                            while((line = br4.readLine())!=null)
				SimCompiler.this.dialog.
				    appendLinkText(line+"\n", false, true);
                        }
                        catch(Exception e){
			    System.out.println("Error: "+e.getMessage());
			}
		    }
		};
	    reader4.start();
	    
 	    this.linkResult = proc.waitFor();
	    dialog.appendLinkText("Link completed"+
				  "\n\nLink exit value: " + linkResult +"\n\n",
                                  true, false);
	}
	catch(Exception e){
	    System.out.println("Error: "+e.getMessage());
	}
	catch (Throwable t){
	    // FIXME: process not found...
	    //JOptionPane
	    System.err.println(t.getMessage());
	}
    }
}

