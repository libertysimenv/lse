package Liberty.visualizer.lse;

import Liberty.visualizer.document.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.view.*;
import Liberty.visualizer.util.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.prefs.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;

/**
 * The ExecutionManager is used to begin and monitor the execution of a LSE
 * binary simulator.
 *
 */

public class ExecutionManager{

    /** the LSS file that has been compiled and is being executed */
    private LSSDocument document;
    /** the process that is running the simulation and rpc server */
    private Process simulationServer;
    /** the label used to display the current simulation cycle */
    private JLabel cycleLabel;
    /** the JButton used to single step through simulation */
    private JButton stepButton;
    /** the JButton used to finish simulation and kill the simulationServer */
    private JButton finishButton;
    private JButton runUntilButton;
    private JButton stepIncrementButton;
    //private JButton restartButton;
    private String executableFileName;
    private String executableOptions;
    private int stepIncrement;
    private int currentCycle;
    private boolean visServerActiveLock;
    private boolean simServerActiveLock;
    private boolean isRunning;

    /** jni functions for interaction with the simulation server via RPC */
    private native int nativeStartSimulation();
    private native int nativeFinishSimulation();
    private native int nativeDoTimeStep();
    private native int nativeStartRpcServer(SchematicView view);


    public ExecutionManager(LSSDocument document)
    {
	this.isRunning = false;
	this.currentCycle = 0;
	this.document = document;
	this.stepIncrement = 1;
	Preferences prefs =
	    Preferences.userNodeForPackage(getClass());

	this.executableFileName = prefs.get(document.getName()+
					    ".executableFileName", "");
	this.executableOptions = prefs.get(document.getName()+
					   ".executableOptions", "");
    }

    public void setExecutableFileName(String executableFileName)
    {
	Preferences prefs = Preferences.userNodeForPackage(getClass());

	this.executableFileName = executableFileName;
	prefs.put(document.getName()+".executableFileName", 
		  executableFileName);
    }

    public synchronized void beginExecution()
    {
	if(isRunning){
	    JOptionPane.showMessageDialog(null, "You must first "+
					  "terminate the current execution "+
					  "before executing another "+
					  "simulation.",
					  "Alert",
					  JOptionPane.
					  ERROR_MESSAGE);
	    return;
	}

	isRunning = true;

	String libraryPath = 
	    System.getProperty("liberty")+"/lib/";
	if(!libraryPath.equals("")){
	    try{
		System.loadLibrary("simclient");
		System.loadLibrary("visserver");
	    }
	    catch(SecurityException se){
		JOptionPane.showMessageDialog(null, "Security Exception: "
					      +" could not load the "+
					      "native RPC libraries.  "+
					      "Please be sure ${LIBERTY}"+
					      "/lib is in your "+
					      "LD_LIBRARY_PATH and that "+
					      "you have appropriate "+
					      "permissions.",
                                              "Alert",
                                              JOptionPane.
                                              ERROR_MESSAGE);
		isRunning = false;
		return;
	    }
	    catch(UnsatisfiedLinkError ule){
		JOptionPane.showMessageDialog(null, "Could not load the "+
					      "native RPC libraries.  "+
					      "Please be sure ${LIBERTY}"+
					      "/lib is in your "+
					      "LD_LIBRARY_PATH",
                                              "Alert",
                                              JOptionPane.
                                              ERROR_MESSAGE);
		ule.printStackTrace();
		isRunning = false;
		return;
	    }
	}

	Thread optionsDialogThread = new Thread(){
		public void run(){
		    JDialog optionsDialog = buildOptionsDialog();
		    optionsDialog.show();
		}
	    };
	optionsDialogThread.start();
    }

    private void execute(String path, String options)
    {

	final Runtime runtime = Runtime.getRuntime();
	final String command = path+" "+options;
	final ExecutionOutputDialog executionOutputDialog = 
	    new ExecutionOutputDialog();

	executionOutputDialog.show();

	final Thread simulationServerThread = new Thread(){
		public void run(){
		    try{
			simulationServer = runtime.exec(command);
			
			InputStream stdout = 
			    simulationServer.getInputStream();
			InputStreamReader stdoutISR = 
			    new InputStreamReader(stdout);
			BufferedReader stdoutBR = 
			    new BufferedReader(stdoutISR);
			
			InputStream stderr = simulationServer.getErrorStream();
			InputStreamReader stderrISR = 
			    new InputStreamReader(stderr);
			final BufferedReader stderrBR = 
			    new BufferedReader(stderrISR);
			
			executionOutputDialog.setStdoutReader(stdoutBR);
			executionOutputDialog.setStderrReader(stderrBR);
		    }
		    catch(IOException ioe){
			JOptionPane.
			    showMessageDialog(null, 
					      "IO Exception: "+
					      ioe.getMessage(), 
					      "alert", 
					      JOptionPane.ERROR_MESSAGE);
			isRunning = false;
			setActive(false);
			releaseSimServerActiveLock();
			releaseVisServerActiveLock();
			return;
		    }
		    catch(Exception e){
			JOptionPane.showMessageDialog(null, 
						      "Runtime Exception: "+
						      e.getMessage(), 
						      "alert", 
						    JOptionPane.ERROR_MESSAGE);
			isRunning = false;
			setActive(false);
			releaseSimServerActiveLock();
			releaseVisServerActiveLock();
			return;
		    }
		}
	    };
	

	final Thread simulationThread = new Thread(){
		public void run(){
		    while(!getSimServerActiveLock())
			yield();

		    int ret = nativeStartSimulation();
		    if(ret < 0){
			JOptionPane.showMessageDialog(null, 
				    "RPC Error: Could not connect "+
				    "to simulation server.", 
				    "alert", JOptionPane.ERROR_MESSAGE);
			if(simulationServer != null)
			    simulationServer.destroy();
			isRunning = false;
			setActive(false);
			releaseSimServerActiveLock();
			releaseVisServerActiveLock();
		    }
		    releaseVisServerActiveLock();
		    // this lock will be released by a callback from 
		    // the rpc server

		}
	    };

	final Thread rpcServerThread = new Thread(){
		public void run(){
		    while(getVisServerActiveLock())
			yield();

		    try{
			nativeStartRpcServer(ExecutionManager.this.
					     document.
					     getSchematicView());
		    }
		    catch(Exception e){
			System.out.println("Exception: "+e.getMessage());
			JOptionPane.showMessageDialog(null, 
					  "RPC Error: Could not start "+
					  "the visualizer RPC server.", 
					  "alert", JOptionPane.ERROR_MESSAGE);
			simulationServer.destroy();
			simulationThread.destroy();
			isRunning = false;
			setActive(false);
			releaseSimServerActiveLock();
			releaseVisServerActiveLock();
			// this lock will be released by a callback from 
			// the rpc server
		    }
		    releaseVisServerActiveLock();
		}
	    };

	Thread executionThread = new Thread(){
		public void run(){
		    // gain the lock before doing anything.  this allows the
		    // simulationServerThread to be the first thread to run,
		    // since it does not require the lock, but will attempt
		    // to release it when the simulation server starts up
		    while(!getSimServerActiveLock())
			yield();
		    while(!getVisServerActiveLock())
			yield();

		    simulationServerThread.start();
		    simulationThread.start();
		    rpcServerThread.start();
		    try{
			// once the visServerActiveLock has been released
			// we can be sure that the simulationServer has
			// been started and hence wait for it.
			while(!getVisServerActiveLock())
			    yield();
			simulationServer.waitFor();
			isRunning = false;
			releaseSimServerActiveLock();
			releaseVisServerActiveLock();
			setActive(false);
		    }
		    catch(InterruptedException ie){
			releaseSimServerActiveLock();
			releaseVisServerActiveLock();
			isRunning = false;
			setActive(false);
		    }
		}
	    };
	executionThread.start();

    }

    public synchronized void setCurrentCycle(int cycle)
    {
	this.currentCycle = cycle;
	cycleLabel.setText("Current Cycle: "+cycle+"  ");	
    }

    private synchronized boolean getVisServerActiveLock()
    {
	if(!visServerActiveLock){
	    visServerActiveLock = true;
	    return true;
	}
	return false;
    }

    private synchronized boolean getSimServerActiveLock()
    {
	if(!simServerActiveLock){
	    simServerActiveLock = true;
	    return true;
	}
	return false;
    }

    private synchronized void releaseVisServerActiveLock()
    {
	visServerActiveLock = false;
    }

    private synchronized void releaseSimServerActiveLock()
    {
	simServerActiveLock = false;
    }

    private synchronized void setActive(boolean active)
    {
	stepButton.setEnabled(active);
	finishButton.setEnabled(active);
	runUntilButton.setEnabled(active);
	stepIncrementButton.setEnabled(active);
	//restartButton.setEnabled(active);
    }

    private JDialog buildOptionsDialog()
    {
	final Preferences prefs = Preferences.userNodeForPackage(getClass());
	final JDialog execOptionsDialog = new JDialog();

	execOptionsDialog.setModal(false);
	execOptionsDialog.setTitle("execution options");

	final JTextField pathField = new JTextField(executableFileName, 30);
	pathField.setCaretPosition(0);
	final JTextField execOptionsField = 
	    new JTextField(executableOptions, 30);
	final JButton fileChooserButton = 
	    new JButton(IconStore.getIcon("openSmallIcon"));

	fileChooserButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			JFileChooser chooser = 
			    new JFileChooser(System.getProperty("user.dir"));
			int ret = chooser.showOpenDialog(null);
			if(ret == JFileChooser.APPROVE_OPTION){
			    pathField.setText(chooser.getSelectedFile().
					      getAbsolutePath());
			}
		}
	    });

	JButton okButton = new JButton("Ok");
	JButton cancelButton = new JButton("Cancel");
	JLabel fieldLabel;
	GridBagLayout gridLayout = new GridBagLayout();
	GridBagConstraints constraints = new GridBagConstraints();

	JPanel optionsPanel = new JPanel();
	optionsPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
	optionsPanel.setLayout(gridLayout);

	JPanel buttonPanel = new JPanel();

	constraints.insets = new Insets(4, 4, 4, 4);
	constraints.weightx = 0.0;
 	constraints.fill = GridBagConstraints.NONE;

	fieldLabel = new JLabel("Path to Simulator Executable: ");
 	gridLayout.setConstraints(fieldLabel, constraints);
 	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	gridLayout.setConstraints(pathField, constraints);
	optionsPanel.add(pathField);

 	constraints.weightx = 0.0;
 	constraints.gridwidth = GridBagConstraints.REMAINDER;
 	constraints.fill = GridBagConstraints.NONE;
 	gridLayout.setConstraints(fileChooserButton, constraints);
 	optionsPanel.add(fileChooserButton);

	fieldLabel = new JLabel("Executable Options: ");
	constraints.gridy = 1;
	constraints.anchor = GridBagConstraints.WEST;
	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.NONE;
	gridLayout.setConstraints(fieldLabel, constraints);
	optionsPanel.add(fieldLabel);

	constraints.weightx = 1.0;
	constraints.gridx = 1;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	gridLayout.setConstraints(execOptionsField, constraints);
	optionsPanel.add(execOptionsField);

	JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
	constraints.weightx = 1.0;
	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	gridLayout.setConstraints(separator, constraints);
	optionsPanel.add(separator);
 
	okButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    final String execPath = pathField.getText();
		    final String options = execOptionsField.getText();

		    if(execPath.equals(""))
			JOptionPane.showMessageDialog(null, "You must "+
						      "specify "+
						      "a path to the "+
						      "simulator executable", 
						      "Error",
						   JOptionPane.ERROR_MESSAGE);
		    else{
			File executableFile = new File(execPath);
			if(!executableFile.exists()){
			    JOptionPane.showMessageDialog(null, 
						      "The specified "+
						      "simulator "+
						      "executable does "+
						      "not exist.", 
						      "Error",
						   JOptionPane.ERROR_MESSAGE);

			    return;
			}

			Thread executionThread = new Thread(){
				public void run(){
				    ExecutionManager.this.executableFileName = 
					execPath;
				    ExecutionManager.this.executableOptions = 
					options;

				    prefs.put(document.getName()+
					      ".executableFileName", 
					      execPath);
				    prefs.put(document.getName()+
					      ".executableOptions", 
					      options);

				    execute(execPath, options);
				    execOptionsDialog.dispose();
				}
			    };
			executionThread.start();
		    }
		}
	    });

	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    isRunning = false;
		    execOptionsDialog.dispose();
		}
	    });

	buttonPanel.add(okButton);
	buttonPanel.add(cancelButton);

	Container contentPane = execOptionsDialog.getContentPane();
	contentPane.add(optionsPanel, BorderLayout.CENTER);
	contentPane.add(buttonPanel, BorderLayout.SOUTH);

	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	Dimension size = execOptionsDialog.getPreferredSize();

	execOptionsDialog.setBounds((screen.width-size.width)/2, 
				    (screen.height-size.height + 50)/2, 
				    size.width, size.height + 50);

	return execOptionsDialog;
    }

    class ExecutionOutputDialog extends JDialog{

	private JEditorPane editor;
	private javax.swing.text.Document document;
	private JDialog outputDialog;

	public ExecutionOutputDialog()
	{
	    editor = new JEditorPane("text/rtf", "");;
	    document = editor.getDocument();

	    outputDialog = new JDialog();
	    outputDialog.setModal(false);
	    outputDialog.setTitle("Execution Results");

	    editor.setEditable(false);

	    JScrollPane scroll = new JScrollPane(editor);
	    scroll.setPreferredSize(new Dimension(450, 300));
	    scroll.setBorder(new CompoundBorder(
			     new EmptyBorder(5, 5, 5, 5),
			     new BevelBorder(BevelBorder.LOWERED)));

	    JToolBar toolbar = new JToolBar();
	    ExecutionManager.this.cycleLabel = new JLabel("Current Cycle:");
	    toolbar.add(cycleLabel);

	    JPanel buttonPanel = new JPanel();

	    stepButton = new JButton("Do Timestep");
	    stepButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			ExecutionManager.this.setActive(false);
			Thread stepThread = new Thread(){
				public void run(){
				    ExecutionManager.this.setActive(false);
				    for(int i = 0; 
					i < ExecutionManager.this.
					    stepIncrement; 
					i++){

					while(!getSimServerActiveLock())
					    yield();

					ExecutionManager.this.
					    nativeDoTimeStep();
				    }
				    ExecutionManager.this.setActive(true);
				}
			    };
			stepThread.start();
		    }
		});
	    buttonPanel.add(stepButton);

	    stepIncrementButton = new JButton("Set Timestep Increment");
	    stepIncrementButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			int stepIncrement = 
			    ExecutionManager.this.stepIncrement;
			String stepString = 
			    JOptionPane.showInputDialog(null, 
					    "Set timestep increment",
					    new Integer(stepIncrement));

			if(stepString == null)
			    return;

			try{
			    stepIncrement = Integer.parseInt(stepString);
			    ExecutionManager.this.stepIncrement = 
				stepIncrement;
			}
			catch(NumberFormatException nfe){
			    JOptionPane.showMessageDialog(null, 
							  "Error: The "+
							  "timestep increment"+
							  " value must be an "+
							  "integer value", 
							  "Error", 
							  JOptionPane.
							  ERROR_MESSAGE);
			}
		    }
		});
	    buttonPanel.add(stepIncrementButton);

	    runUntilButton = new JButton("Run to Cycle");
	    runUntilButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			int currentCycle = ExecutionManager.this.currentCycle;
			String cycleString = 
			    JOptionPane.showInputDialog(null, 
							"Run until cyle");

			if(cycleString == null)
			    return;

			try{
			    final int finalCycle = 
				Integer.parseInt(cycleString);

			    if(finalCycle < currentCycle){
				// FIXME: handle this, restart, etc
			    }

			    Thread stepThread = new Thread(){
				    public void run(){
					ExecutionManager.this.setActive(false);

					while(ExecutionManager.this.
					      currentCycle <
					      finalCycle){

					    while(!getSimServerActiveLock())
						yield();

					    ExecutionManager.this.
						nativeDoTimeStep();
					}
				    ExecutionManager.this.setActive(true);
				    }
				};
			    stepThread.start();

			}
			catch(NumberFormatException nfe){
			    JOptionPane.showMessageDialog(null, "Error: The "+
							  "final cycle "+
							  "value must be an "+
							  "integer value", 
							  "Error", 
							  JOptionPane.
							  ERROR_MESSAGE);
			}
		    }
		});
	    buttonPanel.add(runUntilButton);

	    // 	restartButton = new JButton("Restart");
	    // 	restartButton.addActionListener(new ActionListener(){
	    // 		public void actionPerformed(ActionEvent e){
	    // 		    Thread restartThread = new Thread(){
	    // 			    public void run(){
	    // 				while(!getSimServerActiveLock())
	    // 				    yield();
				
	    // 				nativeFinishSimulation();

	    // 				int ret = nativeStartSimulation();
	    // 				if(ret < 0){
	    // 				    JOptionPane.
	    // 					showMessageDialog(null, 
	    // 					     "RPC Error: "
	    //                                       "Could not connect "+
	    // 					     "to simulation server.", 
	    // 					      "alert", 
	    // 					      JOptionPane.
	    //                                        ERROR_MESSAGE);
	    // 				    simulationServer.destroy();
	    // 				}
	    // 				releaseSimServerActiveLock();
	    // 			    }
	    // 			};
	    // 		    restartThread.start();
	    // 		}
	    // 	    });
	    // 	buttonPanel.add(restartButton);

	    finishButton = new JButton("Finish Simulation");
	    finishButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){

			Thread finishThread = new Thread(){
				public void run(){
				    while(!getSimServerActiveLock())
					yield();
				
				    if(simulationServer != null){
					nativeFinishSimulation();
					simulationServer.destroy();
					simulationServer = null;
				    }
				    releaseSimServerActiveLock();
				    releaseVisServerActiveLock();
				    setActive(false);
				    isRunning = false;
				}
			    };
			finishThread.start();
		    }
		});
	    buttonPanel.add(finishButton);

	    Container contentPane = getContentPane();
	    contentPane.add(scroll, BorderLayout.CENTER);
	    contentPane.add(buttonPanel, BorderLayout.SOUTH);
	    contentPane.add(toolbar, BorderLayout.NORTH);

	    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	    setBounds((screen.width-700)/2, 
		      (screen.height-450)/2, 
		      700, 450);

	    try{
		SimpleAttributeSet boldAttrs = new SimpleAttributeSet();
		StyleConstants.setBold(boldAttrs, true);
		document.insertString(document.getLength(), 
				      "Starting Simulation\n", null);
		editor.setCaretPosition(document.getLength());
	    }
	    catch(BadLocationException ble){}

	    addWindowListener(new WindowAdapter(){
		    public void windowClosing(WindowEvent we){
			Thread finishThread = new Thread(){
				public void run(){
				    while(!getSimServerActiveLock())
					yield();
				
				    if(simulationServer != null){
					nativeFinishSimulation();
					simulationServer.destroy();
					simulationServer = null;
				    }
				    releaseSimServerActiveLock();
				    releaseVisServerActiveLock();
				    setActive(false);
				    isRunning = false;
				}
			    };
			finishThread.start();
		    }
		});

	}

	public void setStdoutReader(final BufferedReader stdoutReader)
	{
	    Thread stdoutThread = new Thread(){
		    public void run(){
			String simOut;
			try{
			    while((simOut = stdoutReader.readLine()) != null){
				document.insertString(document.getLength(), 
						      simOut+"\n", null);
				editor.setCaretPosition(document.getLength());
			    }
			}
			catch(IOException ioe){
			    System.out.println(ioe.getMessage());
			}
			catch(BadLocationException ble){
			    System.out.println(ble.getMessage());
			}
		    }
		};
	    stdoutThread.start();
	}

	public void setStderrReader(final BufferedReader stderrReader)
	{
	    Thread stderrThread = new Thread(){
		    public void run(){
			String simOut;
			try{
			    while((simOut = stderrReader.readLine()) != null){
				document.insertString(document.getLength(), 
						      simOut+"\n", null);
				editor.setCaretPosition(document.getLength());
			    }
			}
			catch(IOException ioe){
			    System.out.println(ioe.getMessage());
			}
			catch(BadLocationException ble){
			    System.out.println(ble.getMessage());
			}
		    }
		};
	    stderrThread.start();
	}
    }
}

