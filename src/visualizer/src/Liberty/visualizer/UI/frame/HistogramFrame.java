package Liberty.visualizer.UI.frame;

import ptolemy.plot.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

public class HistogramFrame extends JFrame{
    private String name;
    private Histogram histogramPanel;

    public HistogramFrame(String name)
    {
	super();
	this.name = name;

	histogramPanel = new Histogram();
	//hist.samplePlot();

	getContentPane().add(histogramPanel, BorderLayout.CENTER);
    }

    public void addPoint(int gn, String xLabel, double x, 
			 String yLabel, double y)
    {
	System.out.println("adding point to histogram "+xLabel+" "+x+" "+yLabel+" "+y);
 	histogramPanel.addPoint(gn, x, y, true);
 	histogramPanel.setXLabel(xLabel);
 	histogramPanel.setYLabel(yLabel);
 	histogramPanel.fillPlot();
    }

    public void fillPlot()
    {
 	histogramPanel.fillPlot();
    }

    public String getName()
    {
	return name;
    }

    public void setTitle(String title)
    {
 	histogramPanel.setTitle(title);
    }

    public void setXLabel(String XLabel)
    {
 	histogramPanel.setXLabel(XLabel);
    }

    public void setYLabel(String YLabel)
    {
 	histogramPanel.setYLabel(YLabel);
    }
}
