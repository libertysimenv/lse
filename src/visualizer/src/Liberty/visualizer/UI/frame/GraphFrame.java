package Liberty.visualizer.UI.frame;

import ptolemy.plot.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

public class GraphFrame extends JFrame{
    private Plot plotPanel;
    private String name;

    public GraphFrame(String name)
    {
	super();
	this.name = name;

	plotPanel = new Plot();
	plotPanel.setTitle(name);
	plotPanel.setXLabel("Current Cycle");
	plotPanel.setYLabel("Data");
	plotPanel.setMarksStyle("dots");
        plotPanel.setPointsPersistence(20);

	getContentPane().add(plotPanel, BorderLayout.CENTER);
    }

    public void addPoint(int gn, String xLabel, double x, 
			 String yLabel, double y)
    {
	if(y != 0){
	    plotPanel.addPoint(gn, x, y, true);
	    plotPanel.setXLabel(xLabel);
	    plotPanel.setYLabel(yLabel);
	    plotPanel.fillPlot();
	}
    }

    public void fillPlot()
    {
	plotPanel.fillPlot();
    }

    public String getName()
    {
	return name;
    }

    public void setTitle(String title)
    {
	plotPanel.setTitle(title);
    }

    public void setXLabel(String XLabel)
    {
	plotPanel.setXLabel(XLabel);
    }

    public void setYLabel(String YLabel)
    {
	plotPanel.setYLabel(YLabel);
    }
}
