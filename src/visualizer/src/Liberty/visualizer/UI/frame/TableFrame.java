package Liberty.visualizer.UI.frame;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.EmptyBorder;

public class TableFrame extends JFrame{
    private VisTableModel tableModel;
    private JTable table;
    private String name;

    public TableFrame(String name)
    {
	super();
	this.name = name;

	JPanel panel = new JPanel();
	panel.setBorder(new EmptyBorder(10, 10, 10, 10));
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        tableModel = new VisTableModel();
        table = new JTable(tableModel);
	table.getTableHeader().setReorderingAllowed(true);
	table.setGridColor(Color.black);
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
	try{
	table.setDefaultRenderer(Class.forName("java.lang.String"), 
				 new VisTableRenderer());
	}
	catch(ClassNotFoundException cnfe){}

        JScrollPane scrollPane = new JScrollPane(table);
        getContentPane().add(panel, BorderLayout.CENTER);
	panel.add(new JLabel(name));
	panel.add(scrollPane);

    }

    public String getName()
    {
	return name;
    }
    
    public void setValueForColumn(String columnName, int row, String data)
    {
	int col;

	if((col = tableModel.findColumn(columnName)) == -1){
	    col = tableModel.addColumn(columnName);
	}

	tableModel.setValueAt(data, row, col);
    }

    class VisTableModel extends AbstractTableModel {
	final Vector columnNames = new Vector();

        final Vector rowVector = new Vector();

	public int findColumn(String columnName){
	    int index = 0;
	    Iterator nameIter = columnNames.iterator();
	    while(nameIter.hasNext()){
		String name = (String)nameIter.next();
		
		if(name.equals(columnName)){
		    return index;
		}
		index++;
	    }
	    return -1;
	}
	
	public int addColumn(String name){
	    columnNames.add(name);
	    fireTableStructureChanged();
	    return columnNames.indexOf(name);
	}

	public VisTableModel(){
	    super();
	}

        public int getColumnCount() {
            return columnNames.size();
        }
        
        public int getRowCount() {
	    return rowVector.size();
        }

        public String getColumnName(int col) {
            return (String)columnNames.elementAt(col);
        }

        public Object getValueAt(int row, int col) {
	    if(row < rowVector.size() && 
	       col < ((Vector)rowVector.elementAt(row)).size())
		return ((Vector)rowVector.elementAt(row)).elementAt(col);
            return null;
        }

        public Class getColumnClass(int c) {
	    try{
		return Class.forName("java.lang.String");
	    } catch(ClassNotFoundException e){
		return (new String()).getClass();
	    }
        }

        public boolean isCellEditable(int row, int col) {
	    return false;
        }

        public void setValueAt(Object value, int row, int col) {
	    
	    if(rowVector.size()-1 < row){
		Vector dataVector = new Vector(col+1);
		for(int i=0; i<=col; i++)
		    dataVector.add(null);
		dataVector.set(col, value);
		rowVector.add(dataVector);
		fireTableRowsInserted(0, row); 
	    }
	    else{
		Vector dataVector = (Vector)rowVector.elementAt(row);
		if(dataVector == null){
		    dataVector = new Vector(col+1);
		    dataVector.add(col, value);
		}
		else{
		    if(dataVector.size()-1 < col){
			dataVector.setSize(col);
			dataVector.add(col, value);
		    }
		    else{
			Object current = dataVector.elementAt(col);
			/*if(current != null)
			    current = (String)current + (" / "+(String)value);
			    else*/
			current = value; 
			dataVector.set(col, current);
		    }
		}
	    }
	    fireTableCellUpdated(row, col);
        }
    }

    class VisTableRenderer extends JLabel implements TableCellRenderer{
	private Color[] colors = {Color.green.darker().darker(), 
				  Color.magenta, Color.blue,
				  Color.orange.darker().darker(), 
				  Color.red, Color.black};


	public Component getTableCellRendererComponent(JTable table,
						       Object value,
						       boolean isSelected, 
						       boolean hasFocus,
						       int row, 
						       int column) {

	    this.setHorizontalAlignment(SwingConstants.CENTER);
	    if(value != null){
		String text = (String)value;
		int beginIndex, endIndex, colorIndex;

		beginIndex = text.indexOf("(");
		endIndex = text.indexOf(")");
		if(beginIndex != -1 && endIndex != -1){
		    colorIndex = (new Integer(text.substring(beginIndex+1, 
					      endIndex))).intValue() % 5;
		}
		else
		    colorIndex = 5;
		this.setText(text);
		Font font = getFont().deriveFont(Font.BOLD, 20);
		setFont(font);
		setForeground(colors[colorIndex]);
	    }
	    else
		this.setText("");
	    return this;
	}
    }
}
