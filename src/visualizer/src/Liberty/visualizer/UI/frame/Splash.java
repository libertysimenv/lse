package Liberty.visualizer.UI.frame;

import java.awt.event.*;
import java.awt.geom.*;
import java.awt.font.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

// FIXME: code cleanup
public class Splash extends JWindow
    implements KeyListener, MouseListener, ActionListener {

    SplashPanel panel;

    public Splash(String iconPath, int timeout){ 
	super();
	ImageIcon image = new ImageIcon(iconPath+"/liberty_splash.gif");
	if(image != null){
	    int w = image.getIconWidth(); 
	    int h = image.getIconHeight();
	    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(); 
	    int x = (screen.width - w) / 2; 
	    int y = (screen.height - h) / 2; 
	    setBounds(x, y, w, h);
	    getContentPane().setLayout(new BorderLayout()); 
	    JLabel picture = new JLabel(image);
	    panel = new SplashPanel(image.getImage(), 
				    new String("Starting the Visualizer..."));
	    getContentPane().add("Center", panel);
	    
	    // Listen for key strokes 
	    addKeyListener(this); 
	    // Listen for mouse events from here and parent 
	    addMouseListener(this); 
	    // Timeout after a while 
	    Timer timer = new Timer(0, this); 
	    timer.setRepeats(false); 
	    timer.setInitialDelay(timeout); 
	    timer.start(); 
	}
    }

    public void setText(String text){
	panel.setText(text);
    }
	
    // Dismiss the window on a key press, ignore rest.
    public void keyTyped(KeyEvent event) {} 
    public void keyReleased(KeyEvent event) {} 
    public void keyPressed(KeyEvent event) { 
	setVisible(false); 
	dispose(); 
    } 
	
    // Dismiss the window on a mouse click, ignore rest.
    public void mousePressed(MouseEvent event) {} 
    public void mouseReleased(MouseEvent event) {} 
    public void mouseEntered(MouseEvent event) {} 
    public void mouseExited(MouseEvent event) {} 
    public void mouseClicked(MouseEvent event) { 
	setVisible(false); 
	dispose(); 
    }
	
    // Dismiss the window on a timeout 
    public void actionPerformed(ActionEvent event) { 
	setVisible(false); 
	dispose(); 
    } 
} 

class SplashPanel extends JPanel{
    Image img;
    String text;

    public SplashPanel(Image img, String text){
	this.img = img;
	this.text = text;
    }

    public void paintComponent(Graphics g) {
	super.paintComponent(g);
        setBackground(Color.white);
        int w = getSize().width;
        int h = getSize().height;
        Graphics2D g2;
        g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setRenderingHint(RenderingHints.KEY_RENDERING,
                            RenderingHints.VALUE_RENDER_QUALITY);
        Rectangle r = getBounds();
        g2.drawImage(img, r.x, r.y, r.width, r.height, this);
        FontRenderContext frc = g2.getFontRenderContext();
	Font f = new Font("Helvetica", Font.BOLD, 12);
	g2.setFont(f);
	g2.drawString(text, r.x+210, r.y+200);
    }

    public void setText(String text){
	this.text = text;
	repaint();
    }
}
