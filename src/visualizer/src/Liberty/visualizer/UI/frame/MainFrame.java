package Liberty.visualizer.UI.frame;

import Liberty.visualizer.UI.panel.LibraryPanel;
import Liberty.visualizer.util.IconStore;
import Liberty.visualizer.VisualizerActions;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.util.prefs.*;

public class MainFrame extends JFrame{
    /* menu's that change based on program state */
    private JMenu viewMenu, documentMenu;
    /* menu items to keep track of visible windows */
    private JCheckBoxMenuItem sourceItem, schematicItem;
    /* menu item for the currently focussed document */
    private JMenuItem selectedDocumentItem;
    /* a label for displaying status information */
    private JLabel status;
    /* the toolbar */
    private JToolBar toolbar;

    public MainFrame()
    {
	ImageIcon logo = IconStore.getIcon("libertyFlameSmallIcon");

// 	if(logo != null)
// 	    setIconImage(logo.getImage());
	setTitle("LSE Visualizer");

	Container contentPane = getContentPane();
	initializeMenubar();
	initializeToolbar();
	contentPane.add(new JPanel(), BorderLayout.CENTER);
	status = new JLabel(" ");
	status.setBorder(new BevelBorder(BevelBorder.LOWERED));
	contentPane.add(status, BorderLayout.SOUTH);

	Preferences prefs = 
	    Preferences.userNodeForPackage(getClass());

	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	int xCoord = Math.round(0.10f*screen.width);
	int yCoord = 0;
	int width = Math.round(0.25f*screen.width);
	int height = Math.round(0.45f*screen.height);

	try{
	    xCoord = prefs.getInt("xCoord", xCoord);
	    yCoord = prefs.getInt("yCoord", yCoord);
	    width = prefs.getInt("width", width);
	    height = prefs.getInt("height", height);
	}
	catch(NullPointerException npe){
	    // do nothing, defaults have already been set, just suppress the
	    // message on stderr
	}

	setBounds(xCoord, yCoord, width, height);

	addWindowListener(new WindowAdapter(){
		public void windowClosing(WindowEvent we){
		    MainFrame.this.storePreferences();
		}
	    });
    }

    public void storePreferences()
    {
	try{
	    Rectangle bounds = getBounds();
	    
	    Preferences prefs = 
		Preferences.userNodeForPackage(getClass());
	    
	    prefs.putInt("xCoord", bounds.x);
	    prefs.putInt("yCoord", bounds.y);
	    prefs.putInt("width", bounds.width);
	    prefs.putInt("height", bounds.height);

	    prefs.flush();
	}
	catch(BackingStoreException bse){
	    // do nothing, if the preferences can't be stored it's
	    // not a big deal
	}
	catch(Exception e){}
    }

    public void addFileToDocumentMenu(String fileName)
    {
	JMenuItem item = 
	    new JMenuItem(VisualizerActions.getFocusDocumentAction());
	item.setText(fileName);
	documentMenu.add(item);
   	documentMenu.setEnabled(true);
	selectDocumentMenuItem(item);
    }

    public void initializeMenubar()
    {
	JMenuBar menuBar = new JMenuBar();
	setJMenuBar(menuBar);

	JMenu fileMenu = new JMenu("File");
	menuBar.add(fileMenu);
	
	JMenuItem fileNewSourceItem = 
	    new JMenuItem(VisualizerActions.getOpenNewAction());
	fileNewSourceItem.setIcon(IconStore.getIcon("newSmallIcon"));
	fileMenu.add(fileNewSourceItem);

	JMenuItem fileOpenItem = 
	    new JMenuItem(VisualizerActions.getOpenAction());
	fileOpenItem.setIcon(IconStore.getIcon("openSmallIcon"));
	fileMenu.add(fileOpenItem);

	JMenuItem fileSaveItem = 
	    new JMenuItem(VisualizerActions.getSaveAction());
	fileSaveItem.setIcon(IconStore.getIcon("saveSmallIcon"));
	fileMenu.add(fileSaveItem);

	JMenuItem fileSaveAsItem = 
	    new JMenuItem(VisualizerActions.getSaveAsAction());
	fileSaveAsItem.setIcon(IconStore.getIcon("saveSmallIcon"));
	fileMenu.add(fileSaveAsItem);

	JMenuItem fileCloseItem = 
	    new JMenuItem(VisualizerActions.getCloseAction());
	fileCloseItem.setIcon(IconStore.getIcon("closeSmallIcon"));
	fileMenu.add(fileCloseItem);

// 	JMenuItem filePrintItem = 
// 	    new JMenuItem(VisualizerActions.getPrintAction());
// 	filePrintItem.setIcon(IconStore.getIcon("printSmallIcon"));
// 	fileMenu.add(filePrintItem);

	fileMenu.addSeparator();

	JMenuItem fileExitItem = 
	    new JMenuItem(VisualizerActions.getQuitAction());
	fileExitItem.setIcon(IconStore.getIcon("exitSmallIcon"));
	fileMenu.add(fileExitItem);

	/*JMenu optionsMenu = new JMenu("Options");
	menuBar.add(optionsMenu);

	JMenuItem modulePathItem = new JMenuItem("Add Directory to the "+
				       "module search path");
				       optionsMenu.add(modulePathItem);*/

	viewMenu = new JMenu("View");
	AbstractAction toggleSourceAction = 
	    VisualizerActions.getToggleSourceAction();
	sourceItem = 
	    new JCheckBoxMenuItem(toggleSourceAction);
	sourceItem.setSelected(true);
	toggleSourceAction.putValue("sourceComponent", sourceItem);

	viewMenu.add(sourceItem);

	AbstractAction toggleSchematicAction = 
	    VisualizerActions.getToggleSchematicAction();
	schematicItem = 
	    new JCheckBoxMenuItem(toggleSchematicAction);
	toggleSchematicAction.putValue("sourceComponent", schematicItem);

	viewMenu.add(schematicItem);
	menuBar.add(viewMenu);

	documentMenu = new JMenu("Documents");
	documentMenu.setEnabled(false);
	menuBar.add(documentMenu);

	/*JMenu helpMenu = new JMenu("Help");
	menuBar.add(helpMenu);

	JMenuItem helpAboutItem = 
	    new JMenuItem(VisualizerActions.getAboutAction());
	    helpMenu.add(helpAboutItem);*/

	menuBar.setVisible(true);
    }

    public void initializeToolbar()
    {
    	toolbar = new JToolBar();
	getContentPane().add(toolbar, BorderLayout.NORTH);

	JButton newButton = new JButton(VisualizerActions.getOpenNewAction());
	newButton.setText("");
	newButton.setToolTipText("New");
	newButton.setRolloverIcon(IconStore.getIcon("newOnIcon"));
	newButton.setRolloverEnabled(true);
	newButton.setBorderPainted(false);
	toolbar.add(newButton);

	JButton openButton = new JButton(VisualizerActions.getOpenAction());
	openButton.setText("");
	openButton.setToolTipText("open an existing description file");
	openButton.setRolloverIcon(IconStore.getIcon("openOnIcon"));
	openButton.setRolloverEnabled(true);
	openButton.setBorderPainted(false);
	toolbar.add(openButton);

	JButton saveButton = new JButton(VisualizerActions.getSaveAction());
	saveButton.setText("");
	saveButton.setToolTipText("save this description");
	saveButton.setRolloverIcon(IconStore.getIcon("saveOnIcon"));
	saveButton.setRolloverEnabled(true);
	saveButton.setBorderPainted(false);
	toolbar.add(saveButton);

	JButton refreshButton = 
	    new JButton(VisualizerActions.getRefreshSchematicViewAction());
	refreshButton.setText("");
	refreshButton.setToolTipText("Recompile and Refresh View");
	refreshButton.setRolloverIcon(IconStore.getIcon("refreshOnIcon"));
	refreshButton.setRolloverEnabled(true);
	refreshButton.setBorderPainted(false);
	toolbar.add(refreshButton);

	JButton buildButton = 
	    new JButton(VisualizerActions.getLSSBuildAction());
	buildButton.setText("");
	buildButton.setToolTipText("build this description");
	buildButton.setRolloverIcon(IconStore.getIcon("compileOnIcon"));
	buildButton.setRolloverEnabled(true);
	buildButton.setBorderPainted(false);
	toolbar.add(buildButton);

	JButton executeButton = 
	    new JButton(VisualizerActions.getLSEExecuteAction());
	executeButton.setText("");
	executeButton.setToolTipText("run simulation");
	executeButton.setRolloverIcon(IconStore.getIcon("executeOnIcon"));
	executeButton.setRolloverEnabled(true);
	executeButton.setBorderPainted(false);
	toolbar.add(executeButton);

// 	JButton printButton = new JButton("", IconStore.getIcon("printIcon"));
// 	printButton.setText("");
// 	printButton.setToolTipText("print the selected window contents");
// 	printButton.setRolloverIcon(IconStore.getIcon("printOnIcon"));
// 	printButton.setRolloverEnabled(true);
// 	printButton.setBorderPainted(false);
// 	toolbar.add(printButton);

	toolbar.setVisible(true);
    }

    public void removeDocumentsMenuItem(String fileName)
    {
	int numItems = documentMenu.getItemCount();

	for(int i = 0; i < numItems; i++){
	    JMenuItem item = documentMenu.getItem(i);
	    if(item.getText().equals(fileName))
		documentMenu.remove(item);
	}
    }

    public void renameDocumentsMenuItem(String oldTitle, 
					String newTitle)
    {
	int numItems = documentMenu.getItemCount();

	for(int i = 0; i < numItems; i++){
	    JMenuItem item = documentMenu.getItem(i);
	    if(item.getText().equals(oldTitle))
		item.setText(newTitle);
	}
    }

    public void selectDocumentMenuItem(JMenuItem item)
    {
	if(selectedDocumentItem != null)
	    selectedDocumentItem.setIcon(IconStore.getIcon("blankIcon"));

	item.setIcon(IconStore.getIcon("dotIcon"));
	selectedDocumentItem = item;
    }

    public void selectDocumentMenuItem(String title)
    {
	int numItems = documentMenu.getItemCount();

	for(int i = 0; i < numItems; i++){
	    JMenuItem item = documentMenu.getItem(i);
	    if(item.getText().equals(title))
		selectDocumentMenuItem(item);
	}
    }

    public void setMainComponent(Component component)
    {
	Container contentPane = getContentPane();
	contentPane.removeAll();
	contentPane.add(toolbar, BorderLayout.NORTH);
	contentPane.add(component, BorderLayout.CENTER);
	contentPane.add(status, BorderLayout.SOUTH);
	contentPane.validate();
    }
    
    public void setStatus(String statusText)
    {
	status.setText(statusText);
	Timer timer = new Timer(8000, new ActionListener(){
		public void actionPerformed(ActionEvent evt){
		    setStatus(" ");
		}
	    }); 
	timer.setRepeats(false); 
	timer.start(); 
    }

    public void toggleViewMenu(boolean isSourceVisible,
			       boolean isSchematicVisible)
    {
	sourceItem.setSelected(isSourceVisible);
	schematicItem.setSelected(isSchematicVisible);
    }
}
