package Liberty.visualizer.UI.panel;

import Liberty.visualizer.library.*;
import Liberty.visualizer.view.*;
import Liberty.visualizer.util.IconStore;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.*;

public class LibraryPanel extends JPanel{
    private JTree tree;
    private DefaultMutableTreeNode root;
    private Library library;

    public LibraryPanel(Library library)
    {
	super();
	this.library = library;

	setLayout(new java.awt.BorderLayout());
	root = new DefaultMutableTreeNode(new String("Module Library"));

	tree = new JTree(root);
	tree.putClientProperty("JTree.lineStyle", "Angled");
	tree.setCellRenderer(new LibraryTreeCellRenderer(tree));
	tree.setShowsRootHandles(true);

	MouseListener ml = new MouseAdapter() {
		public void mousePressed(MouseEvent evt) {
		    int selRow = tree.getRowForLocation(evt.getX(), 
							evt.getY());
		    if(selRow != -1) {
			TreePath selPath = 
			    tree.getPathForRow(selRow);
			DefaultMutableTreeNode node = 
			    (DefaultMutableTreeNode)selPath.
			    getLastPathComponent();
			final Object userObject = node.getUserObject();

			if(userObject instanceof LSSFile &&
			   evt.getButton() == MouseEvent.BUTTON3){
			    JPopupMenu popup = new JPopupMenu();
			    
			    JMenuItem viewSourceItem = 
				    new JMenuItem("View Source");
			    
			    viewSourceItem.
				addActionListener(new ActionListener(){
					public void 
					    actionPerformed(ActionEvent e){
					    LSSFile file = (LSSFile)userObject;
					    SourceView sourceView = 
						new SourceView(new 
						    File(file.getPath()), 0);
					    
					    Dimension screen = 
						Toolkit.getDefaultToolkit().
						getScreenSize();
					    double w = 0.45*screen.width;
					    double h = 0.45*screen.height;
					    double x = 0.5*screen.width + 5; 
					    double y = 0.02*screen.height + 5;
					    sourceView.setBounds((int)x, 
								 (int)y, 
								 (int)w, 
								 (int)h);
					    sourceView.setVisible(true);
					}
				    });
			
			    popup.add(viewSourceItem);
			    popup.show(tree, evt.getX(), evt.getY());
			}
			
		    }
		}
	    };
	tree.addMouseListener(ml);


	JScrollPane scrollPane = new JScrollPane(tree);
	add(scrollPane, BorderLayout.CENTER);

	buildTreeFromLibrary();
    }

    public void buildTreeFromLibrary()
    {
	Iterator directoryIterator = 
	    library.getVirtualDirectories().iterator();
	
	while(directoryIterator.hasNext()){
	    VirtualDirectory dir = (VirtualDirectory)directoryIterator.next();
	    
	    DefaultMutableTreeNode child;
	    child = buildTreeFromVirtualDirectory(dir);
	    if(child != null){
		root.add(child);
	    }
	}

	tree.expandPath(new TreePath(root.getPath()));
    }

    public DefaultMutableTreeNode 
	   buildTreeFromVirtualDirectory(VirtualDirectory dir)
    {
	DefaultMutableTreeNode ret = new DefaultMutableTreeNode(dir);
	Enumeration files = dir.getFiles();
	while(files.hasMoreElements()){
	    LSSFile f = 
		(LSSFile)files.nextElement();
		ret.add(new DefaultMutableTreeNode(f));
	}
	Enumeration directories = dir.getSubDirectories();
	while(directories.hasMoreElements()){
	    VirtualDirectory d = 
		(VirtualDirectory)directories.nextElement();
	    ret.add(new DefaultMutableTreeNode(d));
	}
	return ret;
    }

    public void update()
    {
	root.removeAllChildren();
	buildTreeFromLibrary();
	((DefaultTreeModel)tree.getModel()).reload();
    }

    class LibraryTreeCellRenderer extends JLabel 
	                          implements TreeCellRenderer{
	private JTree owner;
	private ImageIcon moduleIcon;
	private ImageIcon openedFolderIcon;
	private ImageIcon closedFolderIcon;
	
	public LibraryTreeCellRenderer(JTree owner){
	    super();

	    this.owner = owner;
	    this.moduleIcon = IconStore.getIcon("moduleIcon");
	    this.openedFolderIcon = IconStore.getIcon("openSmallIcon");
	    this.closedFolderIcon = IconStore.getIcon("closedFolderIcon");

	    setOpaque(true);
	}
	
	public Component getTreeCellRendererComponent(JTree tree, 
						      Object value, 
						      boolean selected, 
						      boolean expanded, 
						      boolean leaf, 
						      int row, 
						      boolean hasFocus){
	    
	    if(value instanceof DefaultMutableTreeNode){
		Object data = ((DefaultMutableTreeNode)value).getUserObject();
		if(data != null){
		    if(data instanceof VirtualDirectory){
			if(expanded)
			    setIcon(openedFolderIcon);
			else{
			    setIcon(closedFolderIcon);
			}
		    }
		    else if(data instanceof LSSFile){
			setIcon(moduleIcon);
		    }
		    else{
			if(expanded)
			    setIcon(openedFolderIcon);
			else{
			    setIcon(closedFolderIcon);
			}
		    }
		}
	    }
	    String s = value.toString();	
	    setText(s);
	    Color background = (selected ? 
				Color.lightGray : Color.white);
	    setBackground(background);
	    setFont(tree.getFont());
	    return this;
	}
    }
}
