package Liberty.visualizer.UI.dialog;

import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.*;

public class LSSInfoDialog{

    //FIXME:
    public static void showDialog(Frame frame, Object info)
    {
	final JDialog dialog = new JDialog(frame);
	JPanel mainPanel = new JPanel();
	JPanel buttonPanel = new JPanel();
	GridBagLayout gridLayout = new GridBagLayout();
	GridBagConstraints constraints = new GridBagConstraints();
	Container contentPane = dialog.getContentPane();
	JScrollPane scrollPane = new JScrollPane(mainPanel);
	scrollPane.setBorder(new CompoundBorder(
			     new EmptyBorder(5, 5, 5, 5),
			     new BevelBorder(BevelBorder.LOWERED)));

	mainPanel.setLayout(gridLayout);
	mainPanel.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10),
					       new LineBorder(Color.black)));
	
	if(info instanceof Liberty.LSS.IMR.Event){
	    Liberty.LSS.IMR.Event event = (Liberty.LSS.IMR.Event)info;
	    constraints.insets = new Insets(4, 4, 4, 4);
	    constraints.anchor = GridBagConstraints.WEST;
	    constraints.weightx = 0.0;
	    constraints.gridx = 0;
	    constraints.gridy = 0;
	    
	    JLabel nameLabel = new JLabel("Name: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    gridLayout.setConstraints(nameLabel, constraints);
	    mainPanel.add(nameLabel);

	    JTextArea nameArea = new JTextArea(event.getName());
	    nameArea.setBorder(new LineBorder(Color.darkGray));
	    nameArea.setEditable(false);
	    nameArea.setBorder(new LineBorder(Color.darkGray));
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(nameArea, constraints);
	    mainPanel.add(nameArea);

	    JLabel typeLabel = 
		new JLabel("Data Item Types: ");
	    constraints.weightx = 0.0;
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 1;
	    gridLayout.setConstraints(typeLabel, constraints);
	    mainPanel.add(typeLabel);

	    JTextArea typesArea = new JTextArea(event.getDataItemTypes());
	    typesArea.setBorder(new LineBorder(Color.darkGray));
	    typesArea.setEditable(false);
	    typesArea.setLineWrap(true);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(typesArea, constraints);
	    mainPanel.add(typesArea);
	    JLabel namesLabel = 
		new JLabel("Data Item Names: ");
	    constraints.weightx = 0.0;
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 2;
	    gridLayout.setConstraints(namesLabel, constraints);
	    mainPanel.add(namesLabel);

	    JTextArea namesArea = new JTextArea(event.getDataItemNames());
	    namesArea.setBorder(new LineBorder(Color.darkGray));
	    namesArea.setEditable(false);
	    namesArea.setLineWrap(true);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(namesArea, constraints);
	    mainPanel.add(namesArea);
	    
	    JButton okButton = new JButton("OK");
	    okButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			dialog.dispose();
		    }
		});
	    
	    buttonPanel.add(okButton, BorderLayout.CENTER);
	    contentPane.add(scrollPane, BorderLayout.CENTER);
	    contentPane.add(buttonPanel, BorderLayout.SOUTH);
	}
	else if(info instanceof Parameter){
	    Parameter parm = (Parameter)info;
	    constraints.insets = new Insets(4, 4, 4, 4);
	    constraints.anchor = GridBagConstraints.WEST;
	    constraints.weightx = 0.0;
	    constraints.gridx = 0;
	    constraints.gridy = 0;

	    JLabel nameLabel = new JLabel("Name: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    gridLayout.setConstraints(nameLabel, constraints);
	    mainPanel.add(nameLabel);

	    JTextArea nameArea = new JTextArea(parm.getName());
	    nameArea.setBorder(new LineBorder(Color.darkGray));
	    nameArea.setLineWrap(true);
	    nameArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(nameArea, constraints);
	    mainPanel.add(nameArea);

	    JLabel typeLabel = new JLabel("Type: ");
	    constraints.weightx = 0.0;
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 1;
	    gridLayout.setConstraints(typeLabel, constraints);
	    mainPanel.add(typeLabel);

	    JTextArea typeArea = new JTextArea(""+parm.getType());
	    typeArea.setBorder(new LineBorder(Color.darkGray));
	    typeArea.setEditable(false);
	    typeArea.setLineWrap(true);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(typeArea, constraints);
	    mainPanel.add(typeArea);

	    JLabel valueLabel = new JLabel("Value:");
	    constraints.weightx = 0.0;
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 2;
	    gridLayout.setConstraints(valueLabel, constraints);
	    mainPanel.add(valueLabel);

	    JTextArea valueArea = new JTextArea(""+parm.getValue());
	    valueArea.setBorder(new LineBorder(Color.darkGray));
	    valueArea.setEditable(false);
	    valueArea.setLineWrap(true);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(valueArea, constraints);
	    mainPanel.add(valueArea);

	    JButton okButton = new JButton("OK");
	    okButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			dialog.dispose();
		    }
		});

	    buttonPanel.add(okButton, BorderLayout.CENTER);

	    contentPane.add(scrollPane, BorderLayout.CENTER);
	    contentPane.add(buttonPanel, BorderLayout.SOUTH);
	}
	else if(info instanceof StructAdd){
	    StructAdd sa = (StructAdd)info;
	    constraints.insets = new Insets(4, 4, 4, 4);
	    constraints.anchor = GridBagConstraints.WEST;
	    constraints.weightx = 0.0;
	    constraints.gridx = 0;
	    constraints.gridy = 0;

	    JLabel nameLabel = new JLabel("Structure Name: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    gridLayout.setConstraints(nameLabel, constraints);
	    mainPanel.add(nameLabel);

	    JTextArea nameArea = 
		new JTextArea(sa.getDataStructure().toString());
	    nameArea.setBorder(new LineBorder(Color.darkGray));
	    nameArea.setLineWrap(true);
	    nameArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(nameArea, constraints);
	    mainPanel.add(nameArea);

	    JLabel typeLabel = new JLabel("Field Type: ");
	    constraints.weightx = 0.0;
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 1;
	    gridLayout.setConstraints(typeLabel, constraints);
	    mainPanel.add(typeLabel);

	    JTextArea typeArea = 
		new JTextArea(sa.getFieldType().toString());
	    typeArea.setBorder(new LineBorder(Color.darkGray));
	    typeArea.setLineWrap(true);
	    typeArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(typeArea, constraints);
	    mainPanel.add(typeArea);

	    JLabel areaLabel = new JLabel("Field Name:");
	    constraints.weightx = 0.0;
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 2;
	    gridLayout.setConstraints(areaLabel, constraints);
	    mainPanel.add(areaLabel);
	    
	    JTextArea fieldArea = 
		new JTextArea(sa.getFieldName().toString());
	    fieldArea.setBorder(new LineBorder(Color.darkGray));
	    fieldArea.setLineWrap(true);
	    fieldArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(fieldArea, constraints);
	    mainPanel.add(fieldArea);

	    JButton okButton = new JButton("OK");
	    okButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			dialog.dispose();
		    }
		});

	    buttonPanel.add(okButton, BorderLayout.CENTER);

	    contentPane.add(scrollPane, BorderLayout.CENTER);
	    contentPane.add(buttonPanel, BorderLayout.SOUTH);
	}
	else if(info instanceof Query){
	    Query query = (Query)info;
	    constraints.insets = new Insets(4, 4, 4, 4);
	    constraints.anchor = GridBagConstraints.WEST;
	    constraints.weightx = 0.0;
	    constraints.gridx = 0;
	    constraints.gridy = 0;

	    JLabel nameLabel = new JLabel("Name: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    gridLayout.setConstraints(nameLabel, constraints);
	    mainPanel.add(nameLabel);

	    JTextArea nameArea = 
		new JTextArea(query.getName());
	    nameArea.setBorder(new LineBorder(Color.darkGray));
	    nameArea.setLineWrap(true);
	    nameArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(nameArea, constraints);
	    mainPanel.add(nameArea);

	    JLabel typeLabel = new JLabel("Return Type: ");
	    constraints.weightx = 0.0;
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 1;
	    gridLayout.setConstraints(typeLabel, constraints);
	    mainPanel.add(typeLabel);

	    JTextArea typeArea = new JTextArea(query.getReturnType().
					       toString());
	    typeArea.setBorder(new LineBorder(Color.darkGray));
	    typeArea.setLineWrap(true);
	    typeArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(typeArea, constraints);
	    mainPanel.add(typeArea);

	    JLabel argsLabel = new JLabel("Arguments: ");
	    constraints.weightx = 0.0;
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 2;
	    gridLayout.setConstraints(argsLabel, constraints);
	    mainPanel.add(argsLabel);

	    JTextArea argsArea = new JTextArea(query.getArguments().
					       toString());
	    argsArea.setBorder(new LineBorder(Color.darkGray));
	    argsArea.setEditable(false);
	    argsArea.setLineWrap(true);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(argsArea, constraints);
	    mainPanel.add(argsArea);

	    JButton okButton = new JButton("OK");
	    okButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			dialog.dispose();
		    }
		});

	    buttonPanel.add(okButton, BorderLayout.CENTER);

	    contentPane.add(scrollPane, BorderLayout.CENTER);
	    contentPane.add(buttonPanel, BorderLayout.SOUTH);
	}
	else if(info instanceof Instance){
	    Instance instance = (Instance)info;
	    constraints.insets = new Insets(4, 4, 4, 4);
	    constraints.anchor = GridBagConstraints.WEST;
	    constraints.weightx = 0.0;
	    constraints.gridx = 0;
	    constraints.gridy = 0;

	    JLabel nameLabel = new JLabel("Name: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    gridLayout.setConstraints(nameLabel, constraints);
	    mainPanel.add(nameLabel);

	    JTextArea nameArea = 
		new JTextArea(instance.getName());
	    nameArea.setBorder(new LineBorder(Color.darkGray));
	    nameArea.setLineWrap(true);
	    nameArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(nameArea, constraints);
	    mainPanel.add(nameArea);

	    JLabel moduleLabel = new JLabel("Module Name: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 1;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(moduleLabel, constraints);
	    mainPanel.add(moduleLabel);

	    JTextArea moduleArea = 
		new JTextArea(instance.getModuleName());
	    moduleArea.setBorder(new LineBorder(Color.darkGray));
	    moduleArea.setLineWrap(true);
	    moduleArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(moduleArea, constraints);
	    mainPanel.add(moduleArea);

	    JLabel parentLabel = new JLabel("Parent: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 2;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(parentLabel, constraints);
	    mainPanel.add(parentLabel);
	    String parentName = instance.getParent() == null ? "null" :
		instance.getParent().getFullyQualifiedName();
	    JTextArea parentArea = 
		new JTextArea(parentName);
	    parentArea.setBorder(new LineBorder(Color.darkGray));
	    parentArea.setLineWrap(true);
	    parentArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(parentArea, constraints);
	    mainPanel.add(parentArea);

	    JLabel tarLabel = new JLabel("Tarball: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 3;
	    gridLayout.setConstraints(tarLabel, constraints);
	    mainPanel.add(tarLabel);

	    JTextArea tarArea = 
		new JTextArea(instance.getTarball());
	    tarArea.setBorder(new LineBorder(Color.darkGray));
	    tarArea.setLineWrap(true);
	    tarArea.setLineWrap(true);
	    tarArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(tarArea, constraints);
	    mainPanel.add(tarArea);

	    JLabel phaseStartLabel = new JLabel("Phase Start: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 4;
	    gridLayout.setConstraints(phaseStartLabel, constraints);
	    mainPanel.add(phaseStartLabel);

	    JTextArea phaseStartArea = 
		new JTextArea(""+instance.getPhaseStart());
	    phaseStartArea.setBorder(new LineBorder(Color.darkGray));
	    phaseStartArea.setLineWrap(true);
	    phaseStartArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(phaseStartArea, constraints);
	    mainPanel.add(phaseStartArea);

	    JLabel phaseLabel = new JLabel("Phase: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 5;
	    gridLayout.setConstraints(phaseLabel, constraints);
	    mainPanel.add(phaseLabel);

	    JTextArea phaseArea = 
		new JTextArea(""+instance.getPhase());
	    phaseArea.setBorder(new LineBorder(Color.darkGray));
	    phaseArea.setLineWrap(true);
	    phaseArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(phaseArea, constraints);
	    mainPanel.add(phaseArea);

	    JLabel phaseEndLabel = new JLabel("Phase End: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 6;
	    gridLayout.setConstraints(phaseEndLabel, constraints);
	    mainPanel.add(phaseEndLabel);

	    JTextArea phaseEndArea = 
		new JTextArea(""+instance.getPhaseEnd());
	    phaseEndArea.setBorder(new LineBorder(Color.darkGray));
	    phaseEndArea.setLineWrap(true);
	    phaseEndArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(phaseEndArea, constraints);
	    mainPanel.add(phaseEndArea);

	    JLabel strictLabel = new JLabel("Strict: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridx = 0;
	    constraints.gridy = 7;
	    gridLayout.setConstraints(strictLabel, constraints);
	    mainPanel.add(strictLabel);

	    JTextArea strictArea = 
		new JTextArea(""+instance.getStrict());
	    strictArea.setBorder(new LineBorder(Color.darkGray));
	    strictArea.setLineWrap(true);
	    strictArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(strictArea, constraints);
	    mainPanel.add(strictArea);

	    JButton okButton = new JButton("OK");
	    okButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			dialog.dispose();
		    }
		});

	    buttonPanel.add(okButton, BorderLayout.CENTER);

	    contentPane.add(scrollPane, BorderLayout.CENTER);
	    contentPane.add(buttonPanel, BorderLayout.SOUTH);

	}
	else if(info instanceof Connection){
	    Connection conn = (Connection)info;
	    constraints.insets = new Insets(4, 4, 4, 4);
	    constraints.anchor = GridBagConstraints.WEST;
	    constraints.weightx = 0.0;
	    constraints.gridx = 0;
	    constraints.gridy = 0;

	    JLabel sourceLabel = new JLabel("Source: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    gridLayout.setConstraints(sourceLabel, constraints);
	    mainPanel.add(sourceLabel);
	    Port sourcePort = conn.getSourcePort();

	    JTextArea sourceArea = 
		new JTextArea(sourcePort.getInstance().getName()+"."+
			      sourcePort.getName()+".["+
			      conn.getSourceInstance()+"]");
	    sourceArea.setBorder(new LineBorder(Color.darkGray));
	    sourceArea.setLineWrap(true);
	    sourceArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(sourceArea, constraints);
	    mainPanel.add(sourceArea);

	    JLabel destLabel = new JLabel("Destination: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 1;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(destLabel, constraints);
	    mainPanel.add(destLabel);
	    Port destPort = conn.getDestPort();

	    JTextArea destArea = 
		new JTextArea(destPort.getInstance().getName()+"."+
			      destPort.getName()+".["+
			      conn.getDestInstance()+"]");
	    destArea.setBorder(new LineBorder(Color.darkGray));
	    destArea.setLineWrap(true);
	    destArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(destArea, constraints);
	    mainPanel.add(destArea);

	    JLabel typeLabel = new JLabel("Type: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 2;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(typeLabel, constraints);
	    mainPanel.add(typeLabel);

	    String connType = ""+conn.getConstraint();

	    JTextArea typeArea = 
		new JTextArea(""+conn.getConstraint());
	    typeArea.setBorder(new LineBorder(Color.darkGray));
	    typeArea.setLineWrap(true);
	    typeArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(typeArea, constraints);
	    mainPanel.add(typeArea);

	    JButton okButton = new JButton("OK");
	    okButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			dialog.dispose();
		    }
		});

	    buttonPanel.add(okButton, BorderLayout.CENTER);

	    contentPane.add(scrollPane, BorderLayout.CENTER);
	    contentPane.add(buttonPanel, BorderLayout.SOUTH);

	}
	else if(info instanceof Liberty.LSS.IMR.Port){
	    Port port = (Port)info;
	    constraints.insets = new Insets(4, 4, 4, 4);
	    constraints.anchor = GridBagConstraints.WEST;
	    constraints.weightx = 0.0;
	    constraints.gridx = 0;
	    constraints.gridy = 0;

	    JLabel nameLabel = new JLabel("Name: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    gridLayout.setConstraints(nameLabel, constraints);
	    mainPanel.add(nameLabel);

	    JTextArea nameArea = 
		new JTextArea(port.getName());
	    nameArea.setBorder(new LineBorder(Color.darkGray));
	    nameArea.setLineWrap(true);
	    nameArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(nameArea, constraints);
	    mainPanel.add(nameArea);

	    JLabel directionLabel = new JLabel("Direction: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 1;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(directionLabel, constraints);
	    mainPanel.add(directionLabel);

	    JTextArea directionArea = 
		new JTextArea(port.getDirection() == Port.OUTPUT ? "OUTPUT" :
			      "INPUT");
	    directionArea.setBorder(new LineBorder(Color.darkGray));
	    directionArea.setLineWrap(true);
	    directionArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(directionArea, constraints);
	    mainPanel.add(directionArea);

	    JLabel typeLabel = new JLabel("Type: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 2;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(typeLabel, constraints);
	    mainPanel.add(typeLabel);

	    JTextArea typeArea = 
		new JTextArea(""+port.getType());
	    typeArea.setBorder(new LineBorder(Color.darkGray));
	    typeArea.setLineWrap(true);
	    typeArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(typeArea, constraints);
	    mainPanel.add(typeArea);

	    JLabel independentLabel = new JLabel("Independent: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 3;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(independentLabel, constraints);
	    mainPanel.add(independentLabel);

	    JTextArea independentArea = 
		new JTextArea(""+port.getIndependent());
	    independentArea.setBorder(new LineBorder(Color.darkGray));
	    independentArea.setLineWrap(true);
	    independentArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(independentArea, constraints);
	    mainPanel.add(independentArea);

	    JLabel handlerLabel = new JLabel("Handler: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 4;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(handlerLabel, constraints);
	    mainPanel.add(handlerLabel);

	    JTextArea handlerArea = 
		new JTextArea(""+port.getHandler());
	    handlerArea.setBorder(new LineBorder(Color.darkGray));
	    handlerArea.setLineWrap(true);
	    handlerArea.setEditable(false);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(handlerArea, constraints);
	    mainPanel.add(handlerArea);

	    JLabel controlLabel = new JLabel("Control: ");
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.NONE;
	    constraints.gridy = 5;
	    constraints.gridx = 0;
	    gridLayout.setConstraints(controlLabel, constraints);
	    mainPanel.add(controlLabel);
	    String control = ""+port.getControl().getValue();

	    JTextArea controlArea = 
		new JTextArea(control.trim());
	    controlArea.setBorder(new LineBorder(Color.darkGray));
	    controlArea.setEditable(false);
	    controlArea.setLineWrap(true);
	    constraints.weightx = 1.0;
	    constraints.gridwidth = GridBagConstraints.REMAINDER;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = 1;
	    gridLayout.setConstraints(controlArea, constraints);
	    mainPanel.add(controlArea);


	    JButton okButton = new JButton("OK");
	    okButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			dialog.dispose();
		    }
		});

	    buttonPanel.add(okButton, BorderLayout.CENTER);
	    contentPane.add(scrollPane, BorderLayout.CENTER);
	    contentPane.add(buttonPanel, BorderLayout.SOUTH);
	}
	else
	    return;
	
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	dialog.setBounds(screen.width/3, 
			 screen.height/3, 
			 screen.width/3, 
			 screen.height/3);
    
	dialog.setVisible(true);

// 	mainPanel.validate();
	
// 	JViewport viewPort = scrollPane.getViewport();
// 	viewPort.setViewPosition(new Point(0, 0)); 
    }
}
