package Liberty.visualizer.UI.widget;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.lse.*;
import Liberty.visualizer.util.IconStore;
import Liberty.visualizer.view.*;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.*;

public class SchematicViewTree extends JTree implements DropTargetListener,
							DragGestureListener, 
							DragSourceListener
{
    //FIXME: clean me
    /* the root of the layertree */
    protected DefaultMutableTreeNode root;
    /* the canvas that this panel describes */
    protected SchematicView view;
    private DragSource dragSource = null;
    private DragSourceContext dragSourceContext = null;

    public SchematicViewTree(SchematicView view, String rootString)
    {
	super();

	SchematicViewElement rootElement = 
	    new SchematicViewElement(rootString,
				     rootString,
				     null,
				     null,
				     rootString);

	view.addViewElement(rootElement);
				    
	root = rootElement.getTreeNode();

	this.view = view;
	setModel(new DefaultTreeModel(root));
        invalidate();
	expandPath(new TreePath(root.getPath()));

	setCellRenderer(new SchematicViewTreeCellRenderer(this));

	MouseListener ml = new MouseAdapter() {
		public void mousePressed(MouseEvent evt) {
		    int selRow = getRowForLocation(evt.getX(), 
						   evt.getY());
		    if(selRow != -1) {
			TreePath selPath = 
			    getPathForRow(selRow);
			DefaultMutableTreeNode node = 
			    (DefaultMutableTreeNode)selPath.
			    getLastPathComponent();

			SchematicViewElement element = 
			    (SchematicViewElement)node.getUserObject();

			if(evt.getClickCount() == 2){
			    element.doDialog();
 			}
 			if(evt.getButton() == MouseEvent.BUTTON3){
			    element.doPopup(SchematicViewTree.this,
					    evt.getX(), evt.getY());
			}
			else if(evt.isShiftDown()){
 			    SchematicViewTree.this.view.
				addElementToSelection(element.getName());
 			}
 			else{
 			    SchematicViewTree.this.view.
 				setSelectedElement(element.getName());
			    SchematicViewTree.this.
				view.scrollToElement(element);
			}
		    }
		}
	    };
	addMouseListener(ml);

	setDragEnabled(false);
	    
	getSelectionModel().setSelectionMode(TreeSelectionModel.
					     DISCONTIGUOUS_TREE_SELECTION);

    }

    public DefaultMutableTreeNode getRoot()
    {
	return root;
    }

    public String getRootName()
    {
	return ((SchematicViewElement)root.getUserObject()).getName();
    }

    public void add(SchematicViewElement child)
    {
	root.add(child.getTreeNode());

	setModel(new DefaultTreeModel(root));
	invalidate();
    }

    public void reset()
    {
	root.removeAllChildren();
	invalidate();
    }

    public DefaultMutableTreeNode getSelectedNode()
    {
	int selRow = getMinSelectionRow();
	if(selRow != -1) {
	    TreePath selPath = 
		getPathForRow(selRow);
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode)selPath.
		getLastPathComponent();
	    return node;
	}
	return null;
    }

    class SchematicViewTreeCellRenderer implements TreeCellRenderer{
	private JTree owner;
	// FIXME: these icons don't actually exist yet
	private ImageIcon nodeIcon;
	private ImageIcon edgeIcon;
	private ImageIcon openedFolderIcon;
	private ImageIcon closedFolderIcon;
	private ImageIcon dotIcon;

	public SchematicViewTreeCellRenderer(JTree owner){
	    super();
	    
	    this.owner = owner;
	    this.openedFolderIcon = IconStore.getIcon("openSmallIcon");
	    this.closedFolderIcon = IconStore.getIcon("closedFolderIcon");
	    this.dotIcon = IconStore.getIcon("dotIcon");
	    this.nodeIcon = IconStore.getIcon("nodeIcon");
	    this.edgeIcon = IconStore.getIcon("edgeIcon");
	}
	
	public Component getTreeCellRendererComponent(JTree tree, 
						      Object value, 
						      boolean selected, 
						      boolean expanded, 
						      boolean leaf, 
						      int row, 
						      boolean hasFocus){

	    Object userObject = 
		((DefaultMutableTreeNode)value).getUserObject();
	    ImageIcon icon;

	    SchematicViewElement element = 
		(SchematicViewElement)userObject;
	    Object semanticValue = element.getSemanticValue();
	    JLabel label;
	    
	    if(element != null){
		icon = element.getIcon();
		
		if(icon == null){
		    if(semanticValue instanceof SchematicNodeFigure)
			icon = nodeIcon;
		    else if(semanticValue instanceof SchematicEdgeFigure)
			icon = edgeIcon;
		    else{
			if(expanded)
			    icon = openedFolderIcon;
			else
			    icon = closedFolderIcon;
		    }
		}
		
		label = new JLabel(element.getLabel(), 
				   icon, 
				   SwingConstants.RIGHT);
		
		label.setForeground(selected ? 
				    Color.red : Color.black);
		label.setBackground(selected ? 
				    Color.lightGray : Color.white);
		return label;
	    }
	    return null;

	}
    }

    /** DragGestureListener interface method */
    public void dragGestureRecognized(DragGestureEvent e) {
	System.out.println("dragGestureRecognized");
	//Get the selected node
	DefaultMutableTreeNode dragNode = getSelectedNode();
	if (dragNode != null) {
	    
	    //Get the Transferable Object
	    Transferable transferable = 
		(Transferable) dragNode.getUserObject();

	    //Select the appropriate cursor;
	    Cursor cursor = DragSource.DefaultCopyNoDrop;
	    int action = e.getDragAction();
	    if (action == DnDConstants.ACTION_MOVE) 
		cursor = DragSource.DefaultMoveNoDrop;
	    
	    //begin the drag
	    dragSource.startDrag(e, cursor, transferable, this);
	}
    }
    
    /** DragSourceListener interface method */
    public void dragDropEnd(DragSourceDropEvent dsde) {
	System.out.println("dragDropEnd");
    }
    
    /** DragSourceListener interface method */
    public void dragEnter(DragSourceDragEvent dsde) {
	System.out.println("dragEnter");
    }
    
    /** DragSourceListener interface method */
    public void dragOver(DragSourceDragEvent dsde) {
	System.out.println("dragOver");
    }
    
    /** DragSourceListener interface method */
    public void dropActionChanged(DragSourceDragEvent dsde) {
	System.out.println("dragActionChanged");
    }
    
    /** DragSourceListener interface method */
    public void dragExit(DragSourceEvent dsde) {
	System.out.println("dragExit");
    }
    
  
  
  
    /** DropTargetListener interface method - 
	What we do when drag is released */
    public void drop(DropTargetDropEvent e) {
	System.out.println("drop");
	try {
	    Transferable tr = e.getTransferable();
	    
	    //flavor not supported, reject drop
	    if (!tr.isDataFlavorSupported(ViewElementInfo.VIEW_FLAVOR)) 
		e.rejectDrop();
	    
	    //cast into appropriate data type
	    ViewElementInfo childInfo = 
		(ViewElementInfo) tr.getTransferData(ViewElementInfo.VIEW_FLAVOR);
	    
	    //get new parent node
	    Point loc = e.getLocation();
	    TreePath destinationPath = getPathForLocation(loc.x, loc.y);
	    

	    int selRow = getMinSelectionRow();
	    TreePath selPath = 
		getPathForRow(selRow);

	    final String msg = 
		testDropTarget(destinationPath, selPath);
	    if (msg != null) {
		e.rejectDrop();
		

		return;
	    }

	    
	    DefaultMutableTreeNode newParent =
		(DefaultMutableTreeNode)destinationPath.getLastPathComponent();
	    
	    //get old parent node
	    DefaultMutableTreeNode oldParent = 
		(DefaultMutableTreeNode)(getSelectedNode().getParent());
	    
	    int action = e.getDropAction();
	    boolean copyAction = (action == DnDConstants.ACTION_COPY);
	    
	    //make new child node
	    DefaultMutableTreeNode newChild = 
		new DefaultMutableTreeNode(childInfo);
	    
	    try { 
		if (!copyAction) oldParent.remove(getSelectedNode());
		newParent.add(newChild);
		
		if (copyAction) e.acceptDrop (DnDConstants.ACTION_COPY);
		else e.acceptDrop (DnDConstants.ACTION_MOVE);
	    }
	    catch (java.lang.IllegalStateException ils) {
		e.rejectDrop();
	    }
	    
	    e.getDropTargetContext().dropComplete(true);
	    
	    //expand nodes appropriately - this probably isnt the best way...
	    DefaultTreeModel model = (DefaultTreeModel) getModel();
	    model.reload(oldParent);
	    model.reload(newParent);
	    TreePath parentPath = new TreePath(newParent.getPath());
	    expandPath(parentPath);
	}
	catch (IOException io) { e.rejectDrop(); }
	catch (UnsupportedFlavorException ufe) {e.rejectDrop();}
    } //end of method
    
    
    /** DropTaregetListener interface method */
    public void dragEnter(DropTargetDragEvent e) {
	System.out.println("dragEnter");
    }
    
    /** DropTaregetListener interface method */
    public void dragExit(DropTargetEvent e) { 
	System.out.println("dragExit");
    }
    
    /** DropTaregetListener interface method */
    public void dragOver(DropTargetDragEvent e) {
	System.out.println("dragOver");
	//set cursor location. Needed in setCursor method
	Point cursorLocationBis = e.getLocation();
        TreePath destinationPath = 
	    getPathForLocation(cursorLocationBis.x, cursorLocationBis.y);
	

	int selRow = getMinSelectionRow();
	TreePath selPath = 
	    getPathForRow(selRow);


	// if destination path is okay accept drop...
	if (testDropTarget(destinationPath, selPath) == null){
	    e.acceptDrag(DnDConstants.ACTION_COPY_OR_MOVE ) ;
	}
	// ...otherwise reject drop
	else {
	    e.rejectDrag() ;
	}
    }
    
    /** DropTaregetListener interface method */
    public void dropActionChanged(DropTargetDragEvent e) {
	System.out.println("dropActionChanged");
    }

    private String testDropTarget(TreePath destination, TreePath dropper) {
	//Typical Tests for dropping
	
	//Test 1.
	boolean destinationPathIsNull = destination == null;
	if (destinationPathIsNull) 
	    return "Invalid drop location.";
	
	//Test 2.
	DefaultMutableTreeNode node = (DefaultMutableTreeNode) destination.getLastPathComponent();
	if (!node.getAllowsChildren())
	    return "This node does not allow children";
	
	if (destination.equals(dropper))
	    return "Destination cannot be same as source";
	
	//Test 3.
	if ( dropper.isDescendant(destination)) 
	    return "Destination node cannot be a descendant.";
	
	//Test 4.
	if ( dropper.getParentPath().equals(destination)) 
	    return "Destination node cannot be a parent.";
	
	return null;
    }

}


class ViewElementInfo implements Transferable, Serializable {

    final public static DataFlavor VIEW_FLAVOR =
	new DataFlavor(ViewElementInfo.class, "View Element Info");
    
    static DataFlavor flavors[] = {VIEW_FLAVOR };


    public boolean isDataFlavorSupported(DataFlavor df) {
	return df.equals(VIEW_FLAVOR);
    }
    
    /** implements Transferable interface */
    public Object getTransferData(DataFlavor df)
	throws UnsupportedFlavorException, IOException {
	if (df.equals(VIEW_FLAVOR)) {
	    return this;
	}
	else throw new UnsupportedFlavorException(df);
    }
    
    /** implements Transferable interface */
    public DataFlavor[] getTransferDataFlavors() {
	return flavors;
    }
    
    private void writeObject(java.io.ObjectOutputStream out) 
	throws IOException {
	out.defaultWriteObject();
    }
    
    private void readObject(java.io.ObjectInputStream in)
	throws IOException, ClassNotFoundException {
	in.defaultReadObject();
    }
}
