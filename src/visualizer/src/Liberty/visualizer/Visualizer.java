package Liberty.visualizer;

import Liberty.visualizer.document.*;
import Liberty.visualizer.library.Library;
import Liberty.visualizer.UI.frame.MainFrame;
import Liberty.visualizer.UI.frame.Splash;
import Liberty.visualizer.UI.panel.LibraryPanel;
import Liberty.visualizer.util.*;
import Liberty.visualizer.view.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import java.util.prefs.*;
import javax.swing.*;


/**
 * This is the main class used to run the Visualizer application.
 *
 * @author Jason Blome
 * @version 0.1
 */

public class Visualizer{
    /** The main JFrame for the visualizer application */
    private MainFrame mainFrame;
    /** A map of the documents mapped based on the file's URL string */
    private HashMap documents;
    /** the Component that the mainFrame will display */
    private Component mainPanel;
    /** the document that currently has focus */
    private Document focussedDocument;
    /** a counter to keep track of new files (for temporary naming purposes) */
    private int newFileCount;
    /** The path to installed visualizer libraries and icons
     * as set by the environment variable $LIBERTY_VISUALIZER_DATA */
    private String visualizerDataPath;

    public static void main(String args[])
    {
	Visualizer visualizer = new Visualizer();
	try{
	    UIManager.setLookAndFeel(
		      UIManager.getSystemLookAndFeelClassName());
	}
	catch(Exception e) {
	    System.out.println("Error loading native look and feel.");
	}
	for(int i = 0; i < args.length; i++){
	    visualizer.openFile(args[i]);
	}
    }

    public Visualizer()
    {
	this.documents = new HashMap();
	this.newFileCount = 1;


	String visualizerDataPath = System.
	    getProperty("Liberty.visualizer.visualizer_data_path")+
	    File.separator;

	VisualizerActions.setVisualizer(this);	
	    	    
	Splash splash = new Splash(visualizerDataPath+"/icons", 5000);
	splash.show();
	splash.setText("loading icons");

	IconStore.initializeIconStore(visualizerDataPath+"icons/");
	splash.setText("starting the visualizer");

	mainFrame = new MainFrame();

	mainFrame.addWindowListener(new WindowAdapter(){
		public void windowClosing(WindowEvent e){
		    /*FIXME: popup a dialog to check save, etc. */
		    Iterator iterator = documents.values().iterator();
		    while(iterator.hasNext()){
			Document document = (Document)iterator.next();
			document.getSourceView().storePreferences();
			View schematicView = document.getSchematicView();
			if(schematicView != null)
			    schematicView.storePreferences();
		    }

		    System.exit(0);
		}
	    });
	mainFrame.setVisible(true);
    }

    public void closeFile(Document document)
    {
	documents.remove(document);
	mainFrame.removeDocumentsMenuItem(document.getPath());
	document.cleanUp();
	    
	document = null;
    }

    public Document getFocussedDocument()
    {
	return focussedDocument;
    }

    public void openFile(File file)
    {
	// get the file extension, if it is ".lss" build an lss document
	String path = file.getAbsolutePath();
	int dotIndex = path.lastIndexOf(".");
	String extension = dotIndex == -1 ? "" : 
	    path.substring(dotIndex, path.length());

	if(extension.equals(".lss")){
	    LSSDocument newDoc = new LSSDocument(file);
	    mainFrame.addFileToDocumentMenu(file.getAbsolutePath());
	    mainFrame.setMainComponent(newDoc.getMainComponent());
	    documents.put(file.getAbsolutePath(), newDoc);
	    	    
	    JFrame sourceViewFrame = newDoc.getSourceView();
	    	    
	    sourceViewFrame.setVisible(true);
	    sourceViewFrame.setTitle(file.getAbsolutePath());
	    
	    sourceViewFrame.setVisible(true);

	    setFocussedDocument(newDoc);
	}
    }

    public void openFile(String fileName)
    {
	File file = new File(fileName);

	if(!file.exists()){
	    JOptionPane.showMessageDialog(null, "Could not open file: "+
					  fileName+"\nFile does not exist.", 
					  "alert", JOptionPane.ERROR_MESSAGE);
	    return;
	}

	openFile(file.getAbsoluteFile());
    }

    public void openNewFile()
    {
	String fileName = "new description "+newFileCount++;
	LSSDocument newDoc = new LSSDocument(fileName);

	mainFrame.addFileToDocumentMenu(fileName);
	mainFrame.setMainComponent(newDoc.getMainComponent());
	documents.put(fileName, newDoc);
	setFocussedDocument(newDoc);

	JFrame sourceViewFrame = newDoc.getSourceView();

	sourceViewFrame.setVisible(true);
	sourceViewFrame.setTitle(fileName);

	sourceViewFrame.setVisible(true);
    }

    public void renameDocument(String oldName, 
			       String newName)
    {
	Document doc = (Document)documents.remove(oldName);
	if(doc != null)
	    documents.put(newName, doc);

	mainFrame.renameDocumentsMenuItem(oldName, newName);
    }

    public void setFocussedDocument(String docName)
    {
	Document doc = (Document)documents.get(docName);
	setFocussedDocument(doc);
    }

    public void setFocussedDocument(Document doc)
    {
	if(doc != getFocussedDocument()){
	    focussedDocument = doc;
	    if(focussedDocument != null){
		mainFrame.selectDocumentMenuItem(doc.getPath());

		mainFrame.toggleViewMenu(doc.isSourceViewVisible(), 
					 doc.isSchematicViewVisible());

		if(doc.isSourceViewVisible()){
		    doc.getSourceView().toFront();
		}
		else{
		    if(doc.isSchematicViewVisible()){
			doc.getSchematicView().toFront();
		    }
		}
	    }
	}
    }
    
    public void setStatus(String status)
    {
	mainFrame.setStatus(status);
    }

    public void showSchematicView()
    {
	Thread showViewThread = new Thread(){
		public void run(){
		    if(focussedDocument != null){
			SchematicView schematicView = 
			    focussedDocument.getSchematicView();
			
			schematicView.setVisible(true);
			mainFrame.toggleViewMenu(focussedDocument.
						 isSourceViewVisible(),
						 true);
		    }
		}
	    };
	showViewThread.start();
    }
}
