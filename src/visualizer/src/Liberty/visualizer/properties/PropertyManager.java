package Liberty.visualizer.properties;

import Liberty.visualizer.document.*;
import Liberty.visualizer.util.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.prefs.*;

public class PropertyManager{
    /** the owner of this PropertyManager */
    private PropertyHolder owner;
    /** a hashmap of the PropertyInfo elements */
    private LinkedHashMap propertyInfos;
    /** the document that is responsible for the property file*/
    private Document document;
    /** a hashmap where the keys are the names of the different property
        groups and the values are HashSets of the PropertyInfos for each
        group */
    private LinkedHashMap propertyGroups;


    public PropertyManager(PropertyHolder owner, Document document)
    {
	this.owner = owner;
	this.document = document;
	this.propertyInfos = new LinkedHashMap();
	this.propertyGroups = new LinkedHashMap();
    }

    public void addProperty(PropertyInfo info)
    {
	propertyInfos.put(info.getPropertyName(), info);

	String groupName = info.getPropertyGroupName();
	ArrayList propertyGroup = 
	    (ArrayList)propertyGroups.get(groupName);

	if(propertyGroup == null){
	    propertyGroup = new ArrayList();
	    propertyGroups.put(groupName, propertyGroup);
	}
	propertyGroup.add(info);	
    }

    public void applyPropertyChanges()
    {
	Iterator iterator = propertyInfos.values().iterator();
	while(iterator.hasNext()){
	    ((PropertyInfo)iterator.next()).setValueFromComponent();
	}

	// just to be safe we have to update all of the components to make
	// sure that they have the right value (changes in a property value
	// may affect other property values)
	iterator = propertyInfos.values().iterator();
	while(iterator.hasNext()){
	    ((PropertyInfo)iterator.next()).updateComponentValue();
	}
    }

    public JPanel buildPropertiesPanel()
    {
	JPanel toplevelPanel = new JPanel();
	JPanel propertiesPanel = new JPanel();
	TitledBorder border = 
	    new TitledBorder(new LineBorder(Color.black), 
			     "Figure Properties: "+owner.getName());

	// build the properties panel
	border.setTitleColor(Color.black);
	propertiesPanel.setBorder(border);
	propertiesPanel.setLayout(new BoxLayout(propertiesPanel, 
						BoxLayout.Y_AXIS));
	//propertiesPanel.setBackground(Color.white);
	int y = 0;

	Iterator keyIterator = propertyGroups.keySet().iterator();
	while(keyIterator.hasNext()){
	    String key = (String)keyIterator.next();
	    ArrayList propertyGroup = (ArrayList)propertyGroups.get(key);
	    JPanel panel = new JPanel();
	    GridBagLayout gridbag = new GridBagLayout();
	    GridBagConstraints constraints = new GridBagConstraints();

	    border = new TitledBorder(new LineBorder(Color.black), key);
	    border.setTitleColor(Color.black);
	    panel.setBorder(border);
	    panel.setLayout(gridbag);
	    constraints.insets = new Insets(2, 2, 2, 2);

	    Iterator propertyIterator = propertyGroup.iterator();
	    while(propertyIterator.hasNext()){
		PropertyInfo info = (PropertyInfo)propertyIterator.next();
		if(info.isVisible()){
		    Component component = info.getVisibleComponent();
		    JLabel label = new JLabel(info.getPropertyName());
		    
		    //label.setBackground(Color.white);
		    //component.setBackground(Color.white);
		    
		    constraints.gridwidth = GridBagConstraints.RELATIVE;
		    constraints.fill = GridBagConstraints.HORIZONTAL;
		    constraints.anchor = GridBagConstraints.WEST;
		    constraints.weightx = 1.0;
		    constraints.gridx = 0;
		    constraints.gridy = y;
		    
		    gridbag.setConstraints(label, constraints);
		    panel.add(label);
		    
		    constraints.gridwidth = GridBagConstraints.REMAINDER;
		    constraints.fill = GridBagConstraints.BOTH;
		    constraints.anchor = GridBagConstraints.CENTER;
		    constraints.weightx = 1.0;
		    constraints.gridx = 1;
		    constraints.gridy = y++;
		    
		    gridbag.setConstraints(component, constraints);
		    panel.add(component);
		    propertiesPanel.add(panel);
		}
	    }
	}

	toplevelPanel.setLayout(new BoxLayout(toplevelPanel, 
					      BoxLayout.Y_AXIS));
	toplevelPanel.add(propertiesPanel);

	return toplevelPanel;
    }

    public void clear()
    {
	propertyInfos.clear();
    }
    
    public PropertyInfo getProperty(String name)
    {
	return (PropertyInfo)propertyInfos.get(name);
    }
    
    public PropertyHolder getOwner()
    {
	return owner;
    }

    public boolean isEmpty()
    {
	return propertyInfos.isEmpty();
    }

    public void removeProperty(String name)
    {
	propertyInfos.remove(name);
    }

    public void showPropertiesDialog()
    {
	final JDialog propertyDialog = new JDialog();
	JPanel buttonPanel = new JPanel();
	JPanel panel = buildPropertiesPanel();
	JScrollPane scroll = new JScrollPane(panel);
	final Preferences prefs = 
	    Preferences.userNodeForPackage(getClass());
	propertyDialog.setTitle("Property Editor: "+owner.getName());

	// build the button panel
	JButton applyButton = new JButton("Apply");
	applyButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    PropertyManager.this.applyPropertyChanges();
		}
	    });


	JButton okButton = new JButton("Ok");
	okButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    PropertyManager.this.applyPropertyChanges();

		    Rectangle bounds = propertyDialog.getBounds();

		    prefs.putInt("xCoord", bounds.x);
		    prefs.putInt("yCoord", bounds.y);
		    prefs.putInt("width", bounds.width);
		    prefs.putInt("height", bounds.height);

		    try{
			prefs.flush();
		    }
		    catch(BackingStoreException bse){
			// do nothing, if the preferences can't be stored it's
			// not a big deal
		    }

		    propertyDialog.dispose();
		}
	    });

	JButton cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    Rectangle bounds = propertyDialog.getBounds();

		    prefs.putInt("xCoord", bounds.x);
		    prefs.putInt("yCoord", bounds.y);
		    prefs.putInt("width", bounds.width);
		    prefs.putInt("height", bounds.height);

		    try{
			prefs.flush();
		    }
		    catch(BackingStoreException bse){
			// do nothing, if the preferences can't be stored it's
			// not a big deal
		    }

		    propertyDialog.dispose();
		}
	    });

	buttonPanel.add(applyButton);
	buttonPanel.add(okButton);
	buttonPanel.add(cancelButton);

	scroll.setBorder(new CompoundBorder(
			     new EmptyBorder(5, 5, 5, 5),
			     new BevelBorder(BevelBorder.LOWERED)));

	propertyDialog.getContentPane().add(scroll, BorderLayout.CENTER);
	propertyDialog.getContentPane().add(buttonPanel, BorderLayout.SOUTH);



	Dimension screen = 
	    Toolkit.getDefaultToolkit().getScreenSize();

	int xCoord = Math.round((screen.width-450)/2);
	int yCoord = Math.round((screen.height-225)/2);
	int width = 450;
	int height = 325;

	try{
	    xCoord = prefs.getInt("xCoord", xCoord);
	    yCoord = prefs.getInt("yCoord", yCoord);
	    width = prefs.getInt("width", width);
	    height = prefs.getInt("height", height);
	}
	catch(NullPointerException npe){
	    // do nothing, defaults have already been set, just suppress the
	    // message on stderr
	}

	propertyDialog.setBounds(xCoord, yCoord, width, height);

	propertyDialog.show();

	propertyDialog.addWindowListener(new WindowAdapter(){
		public void windowClosing(WindowEvent we){
		    Rectangle bounds = propertyDialog.getBounds();

		    try{
			prefs.putInt("xCoord", bounds.x);
			prefs.putInt("yCoord", bounds.y);
			prefs.putInt("width", bounds.width);
			prefs.putInt("height", bounds.height);

			prefs.flush();
		    }
		    catch(BackingStoreException bse){
			// do nothing, if the preferences can't be stored it's
			// not a big deal
		    }
		    catch(Exception e){}
		}
	    });

    }

    public void storeProperties()
    {
	Iterator iterator = propertyInfos.values().iterator();	
	while(iterator.hasNext()){
	    PropertyInfo info = (PropertyInfo)iterator.next();
	    
	    String key = info.getPropertyHolderName()+"."+
		info.getPropertyName();
	    	    
	    String value = info.getPropertyValueString();
	    document.setProperty(key, value);
	}
    }
}
