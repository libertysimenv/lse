package Liberty.visualizer.properties;

import java.io.File;
import java.util.*;

public interface PropertyHolder{

    public String getFullyQualifiedName();

    public String getName();

    public PropertyInfo getProperty(String name);

    public PropertyManager getPropertyManager();

    public void handleStoredProperties(List propertyList);

    public void propertyChanged(PropertyInfo info);

    public void storeProperties();

    public void updateCachedProperties();

}
