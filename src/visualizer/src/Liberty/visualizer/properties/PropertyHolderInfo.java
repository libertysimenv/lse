package Liberty.visualizer.properties;

import java.util.*;

public class PropertyHolderInfo{
    String propertyHolderName;
    PropertyHolder propertyHolder;
    HashMap properties;
    
    public PropertyHolderInfo(String name)
    {
	this.propertyHolderName = name;
	this.properties = new HashMap();
	this.propertyHolder = null;
    }
    
    public void addProperty(PropertyInfo info)
    {
	properties.put(info.getPropertyName(), info);
    }

    public Collection getProperties()
    {
	return properties.values();
    }

    public PropertyInfo getProperty(String propertyName)
    {
	Object ret = properties.get(propertyName);

	if(ret != null)
	    return (PropertyInfo)ret;
	return null;
    }

    public PropertyHolder getPropertyHolder()
    {
	return propertyHolder;
    }

    public String getPropertyHolderName()
    {
	return propertyHolderName;
    }
    
    public void setPropertyHolder(PropertyHolder holder)
    {
	this.propertyHolder = holder;
    }

}
