package Liberty.visualizer.properties;

import Liberty.visualizer.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PropertyInfo{
    private String propertyHolderName;
    private PropertyHolder propertyHolder;
    private String propertyName;
    private String propertyGroupName;
    private Object propertyValue;
    private Component visibleComponent;
    private int datatype;
    private int displayType;
    private Object[] possibleValues;
    private boolean isVisible;

    public static final int INT_DATATYPE = 0;
    public static final int FLOAT_DATATYPE = 1;
    public static final int COLOR_DATATYPE = 2;
    public static final int STRING_DATATYPE = 3;
    public static final int BOOL_DATATYPE = 4;
    public static final int COMMAND_DATATYPE = 5;

    public static final int LIST_DISPLAY_TYPE = 0;
    public static final int COLOR_DISPLAY_TYPE = 1;
    public static final int TEXT_DISPLAY_TYPE = 2;
    public static final int CHECKBOX_DISPLAY_TYPE = 3;

    public PropertyInfo(PropertyHolder propertyHolder,
			String propertyName,
			String propertyGroup,
			Object propertyValue,
			int datatype,
			Object[] possibleValues,
			boolean isVisible)
    {
	this.propertyName = propertyName;
	this.propertyGroupName = propertyGroup;
	this.propertyValue = propertyValue;
	this.datatype = datatype;
	this.possibleValues = possibleValues;
	this.propertyHolder = propertyHolder;
	this.propertyHolderName = propertyHolder.getFullyQualifiedName();
	this.isVisible = isVisible;

	if(possibleValues != null){
	    JComboBox box = new JComboBox(possibleValues);

	    this.visibleComponent = box;
	    this.displayType = LIST_DISPLAY_TYPE;
	}
	else{
	    switch(datatype){
	    case COLOR_DATATYPE:
		ColorChooserButton colorButton = 
		    new ColorChooserButton(Color.black);

		this.visibleComponent = colorButton;
		this.displayType = COLOR_DISPLAY_TYPE;
		break;
	    case BOOL_DATATYPE:
		JCheckBox checkBox = new JCheckBox();

		this.visibleComponent = checkBox;
		this.displayType = CHECKBOX_DISPLAY_TYPE;
		break;
	    case INT_DATATYPE: 
	    case FLOAT_DATATYPE:
	    case STRING_DATATYPE:
	    default:
		JTextField defaultField = new JTextField(15);

		this.visibleComponent = defaultField;
		this.displayType = TEXT_DISPLAY_TYPE;
		break;
	    }
	}
    }

    public PropertyInfo(String propertyHolderName,
			String propertyName,
			Object propertyValue,
			int datatype,
			Object[] possibleValues)
    {
	this.propertyName = propertyName;
	this.propertyValue = propertyValue;
	this.datatype = datatype;
	this.possibleValues = possibleValues;
	this.propertyHolder = null;
	this.propertyHolderName = propertyHolderName;

	if(possibleValues != null){
	    JComboBox box = new JComboBox(possibleValues);
	    this.visibleComponent = box;
	    this.displayType = LIST_DISPLAY_TYPE;
	}
	else{
	    switch(datatype){
	    case COLOR_DATATYPE:
		ColorChooserButton colorButton = 
		    new ColorChooserButton(Color.black);
		this.visibleComponent = colorButton;
		this.displayType = COLOR_DISPLAY_TYPE;
		break;
	    case BOOL_DATATYPE:
		JCheckBox checkBox = new JCheckBox();
		this.visibleComponent = checkBox;
		this.displayType = CHECKBOX_DISPLAY_TYPE;
		break;
	    case INT_DATATYPE: 
	    case FLOAT_DATATYPE:
	    case STRING_DATATYPE:
	    default:
		JTextField defaultField = new JTextField(15);
		this.visibleComponent = defaultField;
		this.displayType = TEXT_DISPLAY_TYPE;
		break;
	    }
	}
    }

    public int getDataType()
    {
	return datatype;
    }

    public int getDisplayType()
    {
	return displayType;
    }

    public String getPropertyGroupName()
    {
	return propertyGroupName;
    }

    public String getPropertyHolderName()
    {
	return propertyHolderName;
    }

    public String getPropertyName()
    {
	return propertyName;
    }

    public String getPropertyValueString()
    {
	String valueString = "";

	switch(datatype){
	case INT_DATATYPE:
	    valueString += "int "+((Integer)propertyValue).toString();
	    break;
	case FLOAT_DATATYPE:
	    valueString += "float "+((Float)propertyValue).toString();
	    break;
	case COLOR_DATATYPE:
	    valueString += "color "+(Integer)propertyValue;
	    break;
	case STRING_DATATYPE:
	    valueString += "string "+"\""+((String)propertyValue)+"\"";
	    break;
	case BOOL_DATATYPE:
	    valueString += "bool "+((Boolean)propertyValue).toString();
	}
	return valueString;
    }

    public boolean isVisible()
    {
	return isVisible;
    }

    public Object getValue()
    {
	return propertyValue;
    }

    public Component getVisibleComponent()
    {
	// try to set the component's value to the current value of this info
	switch(displayType){
	case LIST_DISPLAY_TYPE:
	    for(int i = 0; i < possibleValues.length; i++){
		if(propertyValue.equals(possibleValues[i])){
		    ((JComboBox)visibleComponent).
			setSelectedItem(possibleValues[i]);
		    break;
		}
	    }
	    break;
	case CHECKBOX_DISPLAY_TYPE:
	    ((JCheckBox)visibleComponent).
		setSelected(((Boolean)propertyValue).booleanValue());
	    break;
	case COLOR_DISPLAY_TYPE:
	    ((ColorChooserButton)visibleComponent).
		setColor(new Color(((Integer)propertyValue).intValue()));
	    break;
	case TEXT_DISPLAY_TYPE:
	    switch(datatype){
	    case INT_DATATYPE:
		((JTextField)visibleComponent).
		    setText(propertyValue.toString());
		break;
	    case FLOAT_DATATYPE:
		((JTextField)visibleComponent).
		    setText(propertyValue.toString());
		break;
	    case STRING_DATATYPE:
		((JTextField)visibleComponent).
		    setText(propertyValue.toString());
		break;
	    }
	    break;
	}	
	return visibleComponent;
    }

    public static PropertyInfo parseProperty(String id, 
					     String value)
    {
	int index = id.lastIndexOf(".");
	String propertyHolderName = id.substring(0, index);
	String propertyName = id.substring(index+1, id.length());

	if(propertyName.length() >= 7 && 
	   propertyName.substring(0, 7).equals("command")){
	    return new PropertyInfo(propertyHolderName,
				    propertyName,
				    value,
				    COMMAND_DATATYPE,
				    null);
	}
	else{
	    // strip whitespace off of the valueString
	    String valueString = value.trim();
	    String typeString;
	    int datatype = -1;
	    // get the first token, it is the type
	    index = valueString.indexOf(" ");
	    if(index > 0){
		typeString = valueString.substring(0, index);
		if(typeString.equals("int"))
		    datatype = INT_DATATYPE;
		else if(typeString.equals("float"))
		    datatype = FLOAT_DATATYPE;
		else if(typeString.equals("color"))
		    datatype = COLOR_DATATYPE;
		else if(typeString.equals("string"))
		    datatype = STRING_DATATYPE;
		else if(typeString.equals("bool"))
		    datatype = BOOL_DATATYPE;
		else if(typeString.equals("cmd"))
		    datatype = COMMAND_DATATYPE;
	    }
	    // now, depending on the type we parse the value
	    valueString = valueString.substring(index, valueString.length());
	    valueString = valueString.trim();
	    Object propertyValue = null;
	    switch(datatype){
	    case INT_DATATYPE:
		propertyValue = new Integer(valueString);
		break;
	    case FLOAT_DATATYPE:
		propertyValue = new Float(valueString);
		break;
	    case COLOR_DATATYPE:
		propertyValue = new Integer(valueString);
		break;
	    case STRING_DATATYPE:
		propertyValue = 
		    valueString.substring(1, valueString.length() - 1);
		break;
	    case BOOL_DATATYPE:
		propertyValue = new Boolean(valueString);
		break;
	    case COMMAND_DATATYPE:
		// FIXME
		propertyValue = null;
		break;
	    }
	    
	    return new PropertyInfo(propertyHolderName,
				    propertyName,
				    propertyValue,
				    datatype,
				    null);
	}
    }

    public void setValue(Object value)
    {
	this.propertyValue = value;
    }

    public void setValueFromComponent()
    {
	if(isVisible){
	    switch(displayType){
	    case LIST_DISPLAY_TYPE:
		String comboValue = (String)((JComboBox)visibleComponent).
		    getSelectedItem();
		
		if(!propertyValue.equals(comboValue)){
		    propertyValue = comboValue;
		    propertyHolder.propertyChanged(this);
		}
		break;
	    case COLOR_DISPLAY_TYPE:
		Color color = 
		    ((ColorChooserButton)visibleComponent).getColor();
		Integer newValue = new Integer(color.getRGB());
		
		if(!newValue.equals(propertyValue)){
		    propertyValue = new Integer(color.getRGB());
		    propertyHolder.propertyChanged(this);
		}
		break;
	    case CHECKBOX_DISPLAY_TYPE:
		Boolean bool = 
		    new Boolean(((JCheckBox)visibleComponent).isSelected());
		
		if(!bool.equals(propertyValue)){
		    propertyValue = bool;
		    propertyHolder.propertyChanged(this);
		}
		break;
	    case TEXT_DISPLAY_TYPE:
		String valueString = ((JTextField)visibleComponent).getText();
		switch(datatype){
		case INT_DATATYPE:
		    Integer intValue = new Integer(valueString);
		    if(!intValue.equals(propertyValue)){
			propertyValue = intValue;
			propertyHolder.propertyChanged(this);
		    }
		    break;
		case FLOAT_DATATYPE:
		    Float floatValue = new Float(valueString);
		    if(!floatValue.equals(propertyValue)){
			propertyValue = floatValue;
			propertyHolder.propertyChanged(this);
		    }
		    break;
		case STRING_DATATYPE:
		    if(!propertyValue.equals(valueString)){
			propertyValue = valueString;
			propertyHolder.propertyChanged(this);
		    }
		    break;
		}
		break;
	    }
	}
    }

    public void updateComponentValue()
    {
	// try to set the component's value to the current value of this info
	switch(displayType){
	case LIST_DISPLAY_TYPE:
	    for(int i = 0; i < possibleValues.length; i++){
		if(propertyValue.equals(possibleValues[i])){
		    ((JComboBox)visibleComponent).
			setSelectedItem(possibleValues[i]);
		    break;
		}
	    }
	    break;
	case CHECKBOX_DISPLAY_TYPE:
	    ((JCheckBox)visibleComponent).
		setSelected(((Boolean)propertyValue).booleanValue());
	    break;
	case COLOR_DISPLAY_TYPE:
	    ((ColorChooserButton)visibleComponent).
		setColor(new Color(((Integer)propertyValue).intValue()));
	    break;
	case TEXT_DISPLAY_TYPE:
	    switch(datatype){
	    case INT_DATATYPE:
		((JTextField)visibleComponent).
		    setText(propertyValue.toString());
		break;
	    case FLOAT_DATATYPE:
		((JTextField)visibleComponent).
		    setText(propertyValue.toString());
		break;
	    case STRING_DATATYPE:
		((JTextField)visibleComponent).
		    setText(propertyValue.toString());
		break;
	    }
	    break;
	}	
    }
}
