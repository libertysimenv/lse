package Liberty.visualizer.properties;

import Liberty.visualizer.util.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class PropertyDialog{

    public static void doDialog(final PropertyHolder holder)
    {
	final JDialog propertyDialog = new JDialog();
	Container container = propertyDialog.getContentPane();
	JPanel uberPanel = new JPanel();
	JPanel contentPane = new JPanel();
	JPanel buttonPanel = new JPanel();
	JScrollPane scrollPane = new JScrollPane(contentPane);
	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints constraints = new GridBagConstraints();
	int x = 0, y = 0;
	
	propertyDialog.setTitle(holder.getName());
	
	container.add(uberPanel, BorderLayout.CENTER);
	uberPanel.setLayout(new BoxLayout(uberPanel, BoxLayout.Y_AXIS));
	uberPanel.add(scrollPane);
	uberPanel.add(buttonPanel);
	buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
	contentPane.setLayout(gridbag);
	
	JButton applyButton = new JButton("Apply");
	applyButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e)
		{
		    /*Iterator iterator = properties.iterator();
		    while(iterator.hasNext()){
			PropertyInfo info = (PropertyInfo)iterator.next();
			info.setValueFromComponent();
			}*/
		}
	    });
	JButton okButton = new JButton("Ok");
	okButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e)
		{
		    /*Iterator iterator = properties.iterator();	
		    while(iterator.hasNext()){
			PropertyInfo info = (PropertyInfo)iterator.next();
			info.setValueFromComponent();
			}*/
		    propertyDialog.dispose();
		}
	    });
	JButton cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e)
		{
		    propertyDialog.dispose();
		}
	    });
	
	buttonPanel.add(applyButton);
	buttonPanel.add(okButton);
	buttonPanel.add(cancelButton);

	/*Iterator iterator = properties.iterator();
	
	while(iterator.hasNext()){
	    PropertyInfo info = (PropertyInfo)iterator.next();
	    
	    constraints.insets = new Insets(2, 2, 2, 2);
	    constraints.gridwidth = GridBagConstraints.RELATIVE;
	    constraints.fill = GridBagConstraints.HORIZONTAL;
	    constraints.gridx = x++;
	    constraints.gridy = y;
	    JLabel label = new JLabel(info.getPropertyName());
	    gridbag.setConstraints(label, constraints);
	    contentPane.add(label);
	    
	    switch(info.getDisplayType()){
	    case PropertyInfo.LIST_DISPLAY_TYPE:
		JComboBox box = (JComboBox)info.getVisibleComponent();
		
		constraints.gridx = x++;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		gridbag.setConstraints(box, constraints);
		contentPane.add(box);
		break;
	    case PropertyInfo.COLOR_DISPLAY_TYPE:
		final ColorChooserButton colorButton = 
		    (ColorChooserButton)info.getVisibleComponent();
		
		constraints.gridx = x++;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		gridbag.setConstraints(colorButton, constraints);
		contentPane.add(colorButton);
		
		colorButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
			    Color color = 
				JColorChooser.
				showDialog(null, "Color Chooser", 
					   colorButton.getColor());
			    if(color != null)
				colorButton.setColor(color);
			}
		    });
		break;
	    case PropertyInfo.TEXT_DISPLAY_TYPE:
		JTextField textField = 
		    (JTextField)info.getVisibleComponent();
		
		constraints.gridx = x++;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		gridbag.setConstraints(textField, constraints);
		contentPane.add(textField);
		break;
	    case PropertyInfo.CHECKBOX_DISPLAY_TYPE:
		JCheckBox checkBox = 
		    (JCheckBox)info.getVisibleComponent();
		
		constraints.gridx = x++;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		gridbag.setConstraints(checkBox, constraints);
		contentPane.add(checkBox);
		break;
	    }
	    x = 0;
	    y++;
	    }*/
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	propertyDialog.setBounds((screen.width-450)/2, 
				 (screen.height-225)/2, 
				 450, 225);
	
	propertyDialog.show();
    }
}
