package Liberty.visualizer;

//import Liberty.visualizer.canvas.LVLWriter;
import Liberty.visualizer.document.*;
import Liberty.visualizer.UI.frame.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.lse.*;
import Liberty.visualizer.view.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;

public class VisualizerActions{

    private static Visualizer visualizer=null;
    private static AbstractAction aboutAction=null;
    private static AbstractAction buildAction=null;
    private static AbstractAction closeAction=null;
    private static AbstractAction copyAction=null;
    private static AbstractAction cutAction=null;
    private static AbstractAction executeAction=null;
    private static AbstractAction focusDocumentAction=null;
    private static AbstractAction graphAction=null;
    private static AbstractAction histogramAction=null;
    private static AbstractAction openAction=null;
    private static AbstractAction openNewAction=null;
    private static AbstractAction pasteAction=null;
    private static AbstractAction pauseExecAction=null;
    private static AbstractAction printAction=null;
    private static AbstractAction viewSchematicAction=null;
    private static AbstractAction quitAction=null;
    private static AbstractAction refreshSchematicViewAction=null;
    private static AbstractAction replaceAction=null;
    private static AbstractAction resumeExecAction=null;
    private static AbstractAction saveAction=null;
    private static AbstractAction saveAsAction=null;
    private static AbstractAction saveSchematicAction=null;
    private static AbstractAction toggleSourceAction=null;
    private static AbstractAction toggleSchematicAction=null;
    private static AbstractAction showSchematicAction=null;
    private static AbstractAction searchAction=null;
    private static AbstractAction tableAction=null;


    public static Visualizer getVisualizer()
    {
	return visualizer;
    }

    public static AbstractAction getAboutAction()
    {
	if(aboutAction == null)
	    aboutAction = new AbstractAction("About"){
		    public void actionPerformed(ActionEvent e){
			System.out.println("About");
		    }
		};
	return aboutAction;
    }

    public static AbstractAction getLSSBuildAction()
    {
	if(buildAction == null)
	    buildAction = new AbstractAction("Build", IconStore.
					     getIcon("buildIcon")){
		    public void actionPerformed(ActionEvent e){
			Document focussedDocment = 
			    visualizer.getFocussedDocument();
			focussedDocment.buildDocument();
			visualizer.setStatus("Beginning build "+
					     "process");
		    }
		};
	return buildAction;
    }

    public static AbstractAction getCloseAction()
    {
	if(closeAction == null)
	    closeAction = new AbstractAction("Close", IconStore.
					     getIcon("closeSmallIcon")){
		    public void actionPerformed(ActionEvent e){
			System.out.println("Close");
			visualizer.closeFile(visualizer.getFocussedDocument());
		    }
		};
	return closeAction;
    }

    public static AbstractAction getCopyAction()
    {
	if(copyAction == null)
	    copyAction = new AbstractAction("Copy", IconStore.
					    getIcon("copyIcon")){
		    public void actionPerformed(ActionEvent e){
			visualizer.setStatus("Copy");      
		    } 
		};
	return copyAction;
    }

    public static AbstractAction getCutAction()
    {
	if(cutAction == null)
	    cutAction = new AbstractAction("Cut", IconStore.
					     getIcon("cutIcon")){
		    public void actionPerformed(ActionEvent e){
			visualizer.setStatus("Cut");      
		    }
		};
	return cutAction;
    }

    public static AbstractAction getLSEExecuteAction()
    {
	if(executeAction == null)
	    executeAction = new AbstractAction("Close", IconStore.
					     getIcon("executeIcon")){
		    public void actionPerformed(ActionEvent e){
 			LSSDocument doc = 
			    (LSSDocument)visualizer.getFocussedDocument();

			if(doc.getSchematicView() == null){
			    doc.buildSchematicView();
			    doc.getSchematicView().setVisible(true);
			}

			ExecutionManager manager = doc.getExecutionManager();
			manager.beginExecution();

 			visualizer.setStatus("Beginning Execution");
		    }
		};
	return executeAction;
    }

    public static AbstractAction getFocusDocumentAction()
    {
	if(focusDocumentAction == null)
	    focusDocumentAction = 
	    new AbstractAction("", IconStore.getIcon("dotIcon")){
		public void actionPerformed(ActionEvent e){
		    Object o = e.getSource();
		    if(o instanceof JMenuItem){
			String fileName = ((JMenuItem)o).getText();
 			visualizer.setFocussedDocument(fileName);
		    }
		}
	    };
	return focusDocumentAction;
    }

    public static AbstractAction getGraphAction()
    {
	if(graphAction == null)
	    graphAction = new AbstractAction("Show Graph of Simulator Output",
 					     IconStore.
					     getIcon("graphIcon")){
		    public void actionPerformed(ActionEvent e){
// 			System.out.println("Graph");
// 			JPopupMenu popup = new JPopupMenu();
// 			Iterator graphIter = visualizer.getFocussedDocument().
// 			    getGraphFrames().iterator();

// 			while(graphIter.hasNext()){
// 			    final GraphFrame gf = 
// 				(GraphFrame)graphIter.next();
// 			    JMenuItem menuItem = new JMenuItem(gf.getName());
// 			    menuItem.addActionListener(new ActionListener(){
// 				   public void actionPerformed(ActionEvent e){
// 				       visualizer.setStatus("Displaying Graph"
// 							   +" "+gf.getName());
// 					Dimension screen = 
// 					    Toolkit.getDefaultToolkit().
// 					    getScreenSize();

// 					gf.setBounds((screen.width-600)/2, 
// 						     (screen.height-425)/2, 
// 						     600, 425);
// 					gf.show();
// 				    }
// 				});
// 			    popup.add(menuItem);
// 			}
// 			popup.show((Component)e.getSource(), 0, 0);
		    }
		};
	return graphAction;
    }

    public static AbstractAction getHistogramAction()
    {
	if(histogramAction == null)
	    histogramAction = new AbstractAction("Show Histogram of"+
						 " Simulator Output",
						 IconStore.
						 getIcon("histogramIcon")){
		    public void actionPerformed(ActionEvent e){
// 			System.out.println("Histogram");
// 			JPopupMenu popup = new JPopupMenu();
// 			Iterator histogramIter = visualizer.
// 			    getFocussedDocument().
// 			    getHistogramFrames().iterator();

// 			while(histogramIter.hasNext()){
// 			    final HistogramFrame hf = 
// 				(HistogramFrame)histogramIter.next();
// 			    JMenuItem menuItem = new JMenuItem(hf.getName());
// 			    menuItem.addActionListener(new ActionListener(){
// 				   public void actionPerformed(ActionEvent e){
// 				       visualizer.setStatus("Displaying "+
// 							    "Histogram"
// 							   +" "+hf.getName());
// 					Dimension screen = 
// 					    Toolkit.getDefaultToolkit().
// 					    getScreenSize();

// 					hf.setBounds((screen.width-450)/2, 
// 						     (screen.height-225)/2, 
// 						     450, 225);
// 					hf.show();
// 				    }
// 				});
// 			    popup.add(menuItem);
// 			}
// 			popup.show((Component)e.getSource(), 0, 0);
		    }
		};
	return histogramAction;
    }

    public static AbstractAction getOpenAction()
    {
	if(openAction == null)
	    openAction = new AbstractAction("Open", IconStore.
					    getIcon("openOnIcon")){
		    public void actionPerformed(ActionEvent e){
			JFileChooser chooser = 
			    new JFileChooser(System.getProperty("user.dir"));
			javax.swing.filechooser.FileFilter filter = 
			    new javax.swing.filechooser.FileFilter(){
				public boolean accept(File file){
				    if(file.isDirectory())
					return true;
				    else{
					String path = file.getAbsolutePath();
					if(path.substring(path.length()-4).
					   equals(".lss"))
					    return true;
				    }
				    return false;
				}
				public String getDescription(){
				    return "LSS configuration files";
				}
			    };
			chooser.setFileFilter(filter);
			int ret = chooser.showOpenDialog(null);
			if(ret == JFileChooser.APPROVE_OPTION){
			    visualizer.setStatus("Opening File: ");
			    visualizer.openFile(chooser.getSelectedFile());
			}
		    }
		};
	return openAction;
    }

    public static AbstractAction getOpenNewAction()
    {
	if(openNewAction == null)
	    openNewAction = new AbstractAction("New", IconStore.
					       getIcon("newIcon")){
		    public void actionPerformed(ActionEvent e){
			visualizer.openNewFile();
		    }
		};
	return openNewAction;
    }

    public static AbstractAction getPasteAction()
    {
	if(pasteAction == null)
	    pasteAction = new AbstractAction("Paste", IconStore.
					     getIcon("pasteIcon")){
		    public void actionPerformed(ActionEvent e){
			visualizer.setStatus("Paste");			
		    }
		};
	return pasteAction;
    }

    public static AbstractAction getPauseExecAction()
    {
	if(pauseExecAction == null)
	    pauseExecAction = 
		new AbstractAction("Pause", IconStore.
					     getIcon("stopIcon")){
		    public void actionPerformed(ActionEvent e){
			Document doc = visualizer.getFocussedDocument();

			//doc.getExecutionManager().setPaused(true);
		    }
		};
	return pauseExecAction;
    }


    public static AbstractAction getPrintAction()
    {
	if(printAction == null)
	    printAction = new AbstractAction("Print", IconStore.
					     getIcon("printIcon")){
		    public void actionPerformed(ActionEvent e){
			visualizer.setStatus("Print");
		    }
		};
	return printAction;
    }

    public static AbstractAction getQuitAction()
    {
	if(quitAction == null)
	    //FIXME: where's exitIcon?
	    quitAction = new AbstractAction("Quit", IconStore.
					    getIcon("exitSmallIcon")){
		    public void actionPerformed(ActionEvent e){
			//visualizer.setStatus("Quit");
			System.exit(0);
		    }
		};
	return quitAction;
    }

    public static AbstractAction getResumeExecAction()
    {
	if(resumeExecAction == null)
	    resumeExecAction = 
		new AbstractAction("Resume", IconStore.
					     getIcon("resumeIcon")){
		    public void actionPerformed(ActionEvent e){
// 			LSSDocument doc = visualizer.getFocussedDocument();

// 			doc.getExecutionManager().setPaused(false);
		    }
		};
	return resumeExecAction;
    }

    public static AbstractAction getSaveAction()
    {
	if(saveAction == null)
	    saveAction = new AbstractAction("Save", IconStore.
					    getIcon("saveIcon")){
		    public void actionPerformed(ActionEvent e){
			Document doc = visualizer.getFocussedDocument();
			visualizer.setStatus("Saving file: "+doc.getPath());
			doc.save();
		    }
		};
	return saveAction;
    }

    public static AbstractAction getSaveAsAction()
    {
	if(saveAsAction == null)
	    saveAsAction = new AbstractAction("SaveAs", IconStore.
					      getIcon("saveSmallIcon")){
		    public void actionPerformed(ActionEvent e){
			Document doc = visualizer.getFocussedDocument();
			visualizer.setStatus("Saving file: "+doc.getPath());
			doc.saveAs();
		    }
		};
	
	return saveAsAction;
    }

    public static AbstractAction getSaveSchematicAction()
    {
	if(saveSchematicAction == null)
	    saveSchematicAction = 
		new AbstractAction("SaveSchematic", IconStore.
				   getIcon("saveSchematicIcon")){
		    public void actionPerformed(ActionEvent e){
			Document document = 
			    visualizer.getFocussedDocument();
 			visualizer.setStatus("Storing Canvas Information: "+
					     document.getName());
			document.saveProperties();
		    }
		};
	
	return saveSchematicAction;
    }

    public static AbstractAction getSearchAction()
    {
	if(searchAction == null)
	    searchAction = new AbstractAction("Search", IconStore.
					      getIcon("searchIcon")){
		    public void actionPerformed(ActionEvent e){
			visualizer.setStatus("Search");
		    }
		};
	return searchAction;
    }

    public static AbstractAction getToggleSourceAction()
    {
	if(toggleSourceAction == null){
	    toggleSourceAction = new AbstractAction("Source View",
						  IconStore.
						  getIcon("sourceFileIcon")){
		    public void actionPerformed(ActionEvent e){
			Document document = visualizer.getFocussedDocument();
			View view = document.getSourceView();
			JCheckBoxMenuItem item = 
			    (JCheckBoxMenuItem)getValue("sourceComponent");
			boolean state = item.isSelected();
			view.setVisible(state);
			view.toFront();
		    }
		};
	}
	return toggleSourceAction;
    }

    public static AbstractAction getToggleSchematicAction()
    {
	if(toggleSchematicAction == null)
	    toggleSchematicAction = new AbstractAction("Schematic View",
						    IconStore.
						    getIcon("schematicIcon")){
		    public void actionPerformed(ActionEvent e){
			JCheckBoxMenuItem item = 
			    (JCheckBoxMenuItem)getValue("sourceComponent");
			boolean state = item.isSelected();

			if(state)
			    visualizer.showSchematicView();
			else{
			    Document document = 
				visualizer.getFocussedDocument();
			    View view = document.getSchematicView();
			    view.setVisible(false);
			    view.setVisible(state);
			}
		    }
		};
	return toggleSchematicAction;
    }

    public static AbstractAction getShowSchematicAction()
    {
	if(showSchematicAction == null)
	    showSchematicAction = new AbstractAction("Schematic View",
						    IconStore.
						    getIcon("schematicIcon")){
		    public void actionPerformed(ActionEvent e){
			visualizer.showSchematicView();
		    }
		};
	return showSchematicAction;
    }


    public static AbstractAction getRefreshSchematicViewAction()
    {
	if(refreshSchematicViewAction == null)
	    refreshSchematicViewAction = new AbstractAction("Refresh", 
					     IconStore.
					     getIcon("refreshIcon")){
		    public void actionPerformed(ActionEvent e){
			Document document = visualizer.getFocussedDocument();
			SchematicView view = document.getSchematicView();
			if(view == null){
			    document.buildSchematicView();
			    view = document.getSchematicView();
			}
			view.refresh();
			visualizer.setStatus("Refresh Schematic");
		    }
		};
	return refreshSchematicViewAction;
    }

    public static AbstractAction getReplaceAction()
    {
	if(replaceAction == null)
	    replaceAction = new AbstractAction("Replace", 
					       IconStore.
					       getIcon("replaceIcon")){
		    public void actionPerformed(ActionEvent e){
			visualizer.setStatus("Replace");
		    }
		};
	return replaceAction;
    }

    public static AbstractAction getTableAction()
    {
	if(tableAction == null)
	    tableAction = new AbstractAction("Show Table of Simulator Output",
					     IconStore.
					     getIcon("tableIcon")){
		    public void actionPerformed(ActionEvent e){
// 			JPopupMenu popup = new JPopupMenu();
// 			Iterator tableIter = visualizer.getFocussedDocument().
// 			    getTableFrames().iterator();
// 			while(tableIter.hasNext()){
// 			    final TableFrame tf=(TableFrame)tableIter.next();
// 			    JMenuItem menuItem = new JMenuItem(tf.getName());
// 			    menuItem.addActionListener(new ActionListener(){
// 				   public void actionPerformed(ActionEvent e){
// 				       visualizer.setStatus("Displaying "+
// 							    " Table "+
// 							    tf.getName());
// 					Dimension screen = 
// 					    Toolkit.getDefaultToolkit().
// 					    getScreenSize();

// 					tf.setBounds((screen.width-700)/2, 
// 						     (screen.height-500)/2, 
// 						     700, 500);
// 					tf.show();
// 				    }
// 				});
// 			    popup.add(menuItem);
// 			}
// 			popup.show((Component)e.getSource(), 0, 0);
		    }
		};
	return tableAction;
    }    

    public static void renameDocument(String oldName, 
				      String newName)
    {
	visualizer.renameDocument(oldName, newName);
    }

    public static void setVisualizer(Visualizer vis)
    {
	visualizer = vis;
    }
}
