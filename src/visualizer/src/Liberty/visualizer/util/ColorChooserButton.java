package Liberty.visualizer.util;

import Liberty.visualizer.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ColorChooserButton extends JButton{
    Color color;
    String label = "Color";
    
    public ColorChooserButton(Color color)
    {
	super();
	this.color = color;

	addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e)
		{
		    Color color = 
			JColorChooser.
			showDialog(null, "Color Chooser", 
				   ColorChooserButton.this.getColor());
		    if(color != null)
			ColorChooserButton.this.setColor(color);
		}
	    });
    }

    public Color getColor()
    {
	return color;
    }

    public void setColor(Color color)
    {
	this.color = color;
    }

    public void paintComponent(Graphics g)
    {
	// FIXME: if we make a JLabel for the text, then the default size
	// will come out a bit better
	super.paintComponent(g);
	Graphics2D g2d = (Graphics2D)g;
	FontMetrics metrics = g2d.getFontMetrics();
	Rectangle labelBounds = 
	    metrics.getStringBounds(label, g2d).getBounds();
	int width = getWidth();
	int height = getHeight();
	int x = 0, y = 0;

	if(width == -1 || height == -1)
	    return;


	x = (int)((width - (12 + labelBounds.width))/2);
	y = (int)((height - labelBounds.height)/2) - 2;

	Rectangle rect = new Rectangle(x, ((height- 6)/2), 8, 8);

	g2d.setPaint(color);
	g2d.fill(rect);
	g2d.setPaint(Color.black);
	g2d.draw(rect);

	g2d.drawString(label, x + 12, y + labelBounds.height);
    }
}
