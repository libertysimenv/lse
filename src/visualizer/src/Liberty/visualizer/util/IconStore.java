package Liberty.visualizer.util;

import java.util.Hashtable;
import javax.swing.ImageIcon;

public class IconStore{
    private static Hashtable icons;

    public static void initializeIconStore(String iconPath){
	icons = new Hashtable();

	icons.put("gridIcon", 
		  new ImageIcon(iconPath+"grid.png"));
	icons.put("tableIcon", 
		  new ImageIcon(iconPath+"table.png"));
	icons.put("graphIcon", 
		  new ImageIcon(iconPath+"graph.png"));
	icons.put("histogramIcon", 
		  new ImageIcon(iconPath+"histogram.png"));
	icons.put("newSmallIcon", 
		  new ImageIcon(iconPath+"newSmall.png"));
	icons.put("newIcon", 
		  new ImageIcon(iconPath+"new.png"));
	icons.put("newOnIcon", 
		  new ImageIcon(iconPath+"newOn.png"));
	icons.put("openSmallIcon", 
		  new ImageIcon(iconPath+"openSmall.png"));
	icons.put("openIcon", 
		  new ImageIcon(iconPath+"open.png"));
	icons.put("openOnIcon", 
		  new ImageIcon(iconPath+"openOn.png"));
	icons.put("saveSmallIcon", 
		  new ImageIcon(iconPath+"saveSmall.png"));
	icons.put("saveIcon",  
		  new ImageIcon(iconPath+"save.png"));
	icons.put("saveOnIcon", 
		  new ImageIcon(iconPath+"saveOn.png"));
	icons.put("closeSmallIcon", 
		  new ImageIcon(iconPath+"closeSmall.png"));
	icons.put("closeIcon", 
		  new ImageIcon(iconPath+"close.png"));
	icons.put("closeOnIcon", 
		  new ImageIcon(iconPath+"closeOn.png"));
	icons.put("stopIcon", 
		  new ImageIcon(iconPath+"stop.png"));
	icons.put("stopOnIcon", 
		  new ImageIcon(iconPath+"stopOn.png"));
	icons.put("stepIcon", 
		  new ImageIcon(iconPath+"step.png"));
	icons.put("stepOnIcon", 
		  new ImageIcon(iconPath+"stepOn.png"));
	icons.put("printSmallIcon", 
		  new ImageIcon(iconPath+"printSmall.png"));
	icons.put("printIcon",  
		  new ImageIcon(iconPath+"print.png"));
	icons.put("printOnIcon", 
		  new ImageIcon(iconPath+"printOn.png"));
	icons.put("exitSmallIcon", 
		  new ImageIcon(iconPath+"exitSmall.png"));
	icons.put("exitIcon", 
		  new ImageIcon(iconPath+"exit.png"));
	icons.put("exitOnIcon", 
		  new ImageIcon(iconPath+"exitOn.png"));
        icons.put("searchSmallIcon",
		  new ImageIcon(iconPath+"searchSmall.png"));
	icons.put("searchIcon",
		  new ImageIcon(iconPath+"search.png"));
	icons.put("searchOnIcon",
		  new ImageIcon(iconPath+"searchOn.png"));
        icons.put("replaceSmallIcon",
		  new ImageIcon(iconPath+
				"replaceSmall.png"));
	icons.put("replaceIcon",
		  new ImageIcon(iconPath+
				"replace.png"));
	icons.put("replaceOnIcon",
		  new ImageIcon(iconPath+
				"replaceOn.png"));
	icons.put("compileSmallIcon",
		  new ImageIcon(iconPath+"stock_convert_16.png"));
	icons.put("buildIcon",
		  new ImageIcon(iconPath+"stock_convert_24.png"));
	icons.put("buildOnIcon",
		  new ImageIcon(iconPath+"stock_convert_24_on.png"));
	icons.put("executeSmallIcon",
		  new ImageIcon(iconPath+"stock_exec_16.png"));
	icons.put("executeIcon",
		  new ImageIcon(iconPath+"stock_exec_24.png"));
	icons.put("executeOnIcon",
		  new ImageIcon(iconPath+"stock_exec_24_on.png"));
	icons.put("copySmallIcon",
		  new ImageIcon(iconPath+"stock_copy_16.png"));
	icons.put("copyIcon",
		  new ImageIcon(iconPath+"stock_copy_24.png"));
	icons.put("copyOnIcon",
		  new ImageIcon(iconPath+"stock_copy_24_on.png"));
	icons.put("cutSmallIcon",
		  new ImageIcon(iconPath+"stock_cut_16.png"));
	icons.put("resumeIcon",
		  new ImageIcon(iconPath+"stock_redo_24.png"));
	icons.put("cutIcon",
		  new ImageIcon(iconPath+"stock_cut_24.png"));
	icons.put("cutOnIcon",
		  new ImageIcon(iconPath+"stock_cut_24_on.png"));
	icons.put("pasteSmallIcon",
		  new ImageIcon(iconPath+"stock_paste_16.png"));
	icons.put("pasteIcon",
		  new ImageIcon(iconPath+"stock_paste_24.png"));
	icons.put("pasteOnIcon",
		  new ImageIcon(iconPath+"stock_paste_24_on.png"));
	icons.put("refreshIcon",
		  new ImageIcon(iconPath+"stock_refresh_24.png"));
	icons.put("refreshOnIcon",
		  new ImageIcon(iconPath+"stock_refresh_24_on.png"));
	icons.put("sourceFileIcon", 
		  new ImageIcon(iconPath+"source_description.gif"));
	icons.put("schematicFileIcon", 
		  new ImageIcon(iconPath+"module_description.gif"));
	icons.put("flowIcon", 
		  new ImageIcon(iconPath+"stock_flow_24.png"));
	icons.put("flowOnIcon", 
		  new ImageIcon(iconPath+"stock_flow_24.png"));
	icons.put("schematicIcon", 
		  new ImageIcon(iconPath+"stock_schematic_24.png"));
	icons.put("saveSchematicIcon", 
		  new ImageIcon(iconPath+"stock_save_schematic_24.png"));
	icons.put("schematicOnIcon", 
		  new ImageIcon(iconPath+"stock_schematic_24.png"));
	icons.put("libertyLogoLargeIcon", 
		  new ImageIcon(iconPath+"liberty.gif"));
	icons.put("libertyFlameSmallIcon", 
		  new ImageIcon(iconPath+"liberty_icon.gif"));
	icons.put("closedFolderIcon", 
		  new ImageIcon(iconPath+"closed_folder.gif"));
	icons.put("closedFolderFilledIcon", 
		 new ImageIcon(iconPath+"closed_folder_filled.gif"));
	icons.put("moduleIcon", 
		  new ImageIcon(iconPath+"module.gif"));
	icons.put("noModuleIcon", 
		  new ImageIcon(iconPath+"noModule.gif"));
	icons.put("connectionIcon", 
		  new ImageIcon(iconPath+"connection.gif"));
	icons.put("noConnectionIcon", 
		  new ImageIcon(iconPath+"noConnection.gif"));
	icons.put("addModuleIcon", 
		  new ImageIcon(iconPath+"add_module.gif"));
	icons.put("addModuleOnIcon", 
		  new ImageIcon(iconPath+"add_module_on.gif"));
	icons.put("editSourceIcon", 
		  new ImageIcon(iconPath+"edit_source.gif"));
	icons.put("editSourceOnIcon", 
		  new ImageIcon(iconPath+"edit_source_on.gif"));
	icons.put("inputPortIcon", 
		  new ImageIcon(iconPath+"input_port.gif"));
	icons.put("outputPortIcon", 
		  new ImageIcon(iconPath+"output_port.gif"));
	icons.put("dotIcon", 
		  new ImageIcon(iconPath+"dot.png"));
	icons.put("blankIcon", 
		  new ImageIcon(iconPath+"blank.png"));
	icons.put("portStatusAckIcon", 
		  new ImageIcon(iconPath+"port_status_ack.png"));
	icons.put("portStatusDataIcon", 
		  new ImageIcon(iconPath+"port_status_data.png"));
	icons.put("portStatusEnableIcon", 
		  new ImageIcon(iconPath+"port_status_enable.png"));
    }

    public static ImageIcon getIcon(String name)
    {
	return (ImageIcon)icons.get(name);
    }   
}
