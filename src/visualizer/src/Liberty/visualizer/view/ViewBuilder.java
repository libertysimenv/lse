package Liberty.visualizer.view;

import Liberty.LSS.IMR.*;
import Liberty.LSS.*;

import Liberty.visualizer.lse.*;
import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.lse.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.widget.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.view.lse.*;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.*;

public class ViewBuilder{

    public static void buildSchematicView(LSESchematicView schematicView,
					  Collection elements)
    {
	SchematicViewTree tree = schematicView.getTree();
	SchematicCanvas canvas = schematicView.getCanvas();

	// iterate over the elements and build SchematicViewElements
	if(elements != null && schematicView != null){
	    // first we make a vector of the elements, sorted alphabetically
	    Vector sortedInstances = new Vector();
	    Iterator instanceIterator = elements.iterator();
	    while(instanceIterator.hasNext()){
		Instance currentInstance = (Instance)instanceIterator.next();

		if(sortedInstances.size() == 0)
		    sortedInstances.add(currentInstance);
		else{
		    boolean placed = false;

		    for(int i = 0; i < sortedInstances.size(); i++){
			Instance instance = 
			    (Instance)sortedInstances.elementAt(i);

			if(currentInstance.getName().
			   compareTo(instance.getName()) <= 0){
			    sortedInstances.add(i, currentInstance);
			    placed = true;
			    break;
			}
		    }
		    if(!placed)
			sortedInstances.add(currentInstance);
		}
	    }
	    elements = null;

// 	    System.out.println("a "+schematicView.getBounds());

	    instanceIterator = sortedInstances.iterator();
	    while(instanceIterator.hasNext()){
		Instance instance = (Instance)instanceIterator.next();
		String instanceName = instance.getFullyQualifiedName();

		java.util.List instancePropertyList = 
		    schematicView.getPropertiesForViewElement(instanceName);
		// let's check to see if the instance has set the lvlString
		// parameter, and if so, add these properties to the 
		// propertySet
		Parameter parameter = (Parameter)instance.
		    getParameter("lvlString");
		if(parameter != null){
		    String lvlString = parameter.getValue().toString();
		    if(lvlString.length() > 0 && !lvlString.equals("\"\"")){
			lvlString = lvlString.substring(1, 
							lvlString.length()-1);
			BufferedReader reader = 
			    new BufferedReader(new StringReader(lvlString));
			String propertyString = null;
			try{
			    while((propertyString = reader.readLine())!=null){
				int length = propertyString.length();
				int index = propertyString.indexOf("=");
				if(index > 0){
				    String key = 
					propertyString.substring(0, index);
				    String value = 
					propertyString.substring(index+1, 
								 length);
				    key = key.trim();
				    value = value.trim();
				    PropertyInfo info = 
					PropertyInfo.parseProperty(key, value);

				    // prepend lvl properties so that they may
				    // be overridden
				    instancePropertyList.add(0, info);
				}
			    }
			}
			catch(java.io.IOException ioe){
			    System.out.println("Exception: "+ioe.getMessage());
			}
		    }
		}
// 		System.out.println("a.1 "+schematicView.getBounds());

		// FIXME:
		// let's check to see if this instance has it's 'Layer Name'
		// parameter set and attempt to get that layer, otherwise
		// put it on the default layer named 'Instances:'

		// also, while we're at it, we'll build a set of all
		// PropertyInfo's with instanceName as a substring
		SchematicCanvasLayer layer = null;
		SchematicViewElement layerElement = null;
		String className = null;

		ArrayList instanceProperties = new ArrayList();
		ArrayList subElementProperties = new ArrayList();
		int length = instanceName.length();

		if(instancePropertyList != null){
		    Iterator propertyIterator = 
			instancePropertyList.iterator();
		    while(propertyIterator.hasNext()){
// 			System.out.println("a.2 "+schematicView.getBounds());

			PropertyInfo info = 
			    (PropertyInfo)propertyIterator.next();
			String propertyHolderName = 
			    info.getPropertyHolderName();

			if(propertyHolderName.length() >= length){
			    String substring = 
				propertyHolderName.substring(0, length);
			    
			    if(substring.equals(instanceName)){
				instanceProperties.add(info);
			    }
			    else
				subElementProperties.add(info);
			}

			if(propertyHolderName.equals(instanceName) && 
			   info.getPropertyName().equals("Layer Name")){
			    String layerName = (String)info.getValue();

			    layer = canvas.getLayer(layerName);

			    if(layer == null){
				// create a new canvas layer
				layer = new SchematicCanvasLayer(canvas, 
								 layerName);
				canvas.addLayer(layer);

				layerElement = 
				    new LayerViewElement(layer.getName(),
							 layer.getName(),
							 schematicView,
							 schematicView.
							 getViewElement
							 (tree.getRootName()),
							 layer);

				schematicView.addViewElement(layerElement);
			    }
			    else{
				layerElement = 
				    schematicView.getViewElement(layerName);
			    }
			}
		    }
		}
// 		System.out.println("a.3 "+schematicView.getBounds());

		// we didn't find a layer name property
		if(layer == null){
		    String layerName = "Instances:";
		    layer = canvas.getLayer(layerName);
		    if(layer == null){
			layer = new SchematicCanvasLayer(canvas, 
							 layerName);
			canvas.addLayer(layer);

			// build a SchematicViewElement for the layer
			layerElement = 
			    new LayerViewElement(layer.getName(),
						 layer.getName(),
						 schematicView,
						 schematicView.
						 getViewElement
						 (tree.getRootName()),
						 layer);

			schematicView.addViewElement(layerElement);
		    }
		    else
			layerElement = schematicView.getViewElement(layerName);
		}
// 		System.out.println("a.4 "+schematicView.getBounds());


		// now build the SchematicViewElement for this instance
		SchematicViewElement instanceElement = 
		    new InstanceViewElement(schematicView,
					    layerElement,
					    instanceProperties,
					    instance);
// 		System.out.println("a.4.1 "+schematicView.getBounds());
// 		System.out.println("a.4.1.1 "+instanceElement);
// 		System.out.println("a.4.1.2 "+instance.getName());

		schematicView.addViewElement(instanceElement);
// 		System.out.println("a.4.2 "+schematicView.getBounds());


		String portsString = instance.getFullyQualifiedName()+
		    "."+"Ports:";
// 		System.out.println("a.4.3 "+schematicView.getBounds());

		SchematicViewElement portsElement = 
		    schematicView.getViewElement(portsString);

// 		System.out.println("a.5 "+schematicView.getBounds());

		Iterator portIterator = instance.getPorts().iterator();
		while(portIterator.hasNext()){
		    Port port = (Port)portIterator.next();

		    /* the figure for the port has already been built by
		       the instance figure */
		    PortFigure portFigure = (PortFigure)
			instanceElement.getSchematicFigure().
			getFigureByUserObject(port);
		    String fullPortName = instance.getFullyQualifiedName()+
			"."+port.getName();

		    java.util.List portPropertySet = schematicView.
			getPropertiesForViewElement(fullPortName);

		    Iterator iterator = instanceProperties.iterator();
		    while(iterator.hasNext()){
			PropertyInfo info = (PropertyInfo)iterator.next();
			if(info.getPropertyHolderName().equals(fullPortName)){
			    portPropertySet.add(0, info);
			}
		    }

		    portFigure.handleStoredProperties(portPropertySet);

		    SchematicViewElement portElement = 
			new PortViewElement(schematicView,
					    portsElement,
					    portFigure,
					    null,
					    port);

		    schematicView.addViewElement(portElement);
 		}
	    }

// 	    System.out.println("b "+schematicView.getBounds());

	    /* now that all the figures for the instances have been built
	       we can build the connection figures */
	    instanceIterator = sortedInstances.iterator();

	    String layerName = "Connections:";
	    SchematicCanvasLayer layer = null;
	    SchematicViewElement layerElement = null;

	    layer = canvas.getLayer(layerName);
	    if(layer == null){
		layer = new SchematicCanvasLayer(canvas, 
						 layerName);
		canvas.addLayer(layer);
		
		// build a SchematicViewElement for the layer
		layerElement = new LayerViewElement(layer.getName(),
						    layer.getName(),
						    schematicView,
						    schematicView.
						    getViewElement
						    (tree.getRootName()),
						    layer);

		schematicView.addViewElement(layerElement);
	    }
	    else
		layerElement = schematicView.getViewElement(layerName);

	    while(instanceIterator.hasNext()){
		Instance instance = (Instance)instanceIterator.next();
		Iterator portIterator = instance.getPorts().iterator();
		while(portIterator.hasNext()){
		    Port port = (Port)portIterator.next();
		    /* build view elements for connections */
		    /* make sure we only build each connection once... */
		    Iterator connectionIterator = 
			port.getConnections().iterator();
		       
		    while(connectionIterator.hasNext()){
			Connection connection = 
			    (Connection)connectionIterator.next();
			   
			if(schematicView.getViewElement(connection.toString()) 
			   == null){
			    
			    java.util.List propertySet = schematicView.
				getPropertiesForViewElement(connection.
							    toString());

			    SchematicViewElement connectionElement =
				new ConnectionViewElement(schematicView,
							  layerElement,
							  propertySet,
							  connection);
			    
			    schematicView.addViewElement(connectionElement);
			    
			    SchematicFigure connectionFigure = 
				connectionElement.getSchematicFigure();
			    
			    // here we do a little bit of a hack to
			    // add a connection element to the port nodes
			    // in the tree widget
			    
			    Liberty.LSS.IMR.Port srcPort = 
				connection.getSourcePort();

			    String srcPortName = srcPort.getInstance().
				getFullyQualifiedName()+"."+srcPort.getName();
			    
			    SchematicViewElement srcPortElement = 
				schematicView.getViewElement(srcPortName);
			    
			    SchematicViewElement srcConnElement = 
				new InfoViewElement(srcPortName+"."+
						    connection.toString(),
						    connection.toString(),
						    schematicView,
						    srcPortElement,
						    IconStore.
						    getIcon("connectionIcon"),
						    connection);
			    
			    srcConnElement.
				setSchematicFigure(connectionFigure);
			    
			    schematicView.addViewElement(srcConnElement);
			    
			    Liberty.LSS.IMR.Port destPort = 
				connection.getDestPort();
			    String destPortName = destPort.getInstance().
				getFullyQualifiedName()+"."+destPort.getName();
			    
			    SchematicViewElement destPortElement = 
				schematicView.getViewElement(destPortName);
			    
			    SchematicViewElement destConnElement = 
				new InfoViewElement(destPortName+"."+
						    connection.toString(),
						    connection.toString(),
						    schematicView,
						    destPortElement,
						    IconStore.
						    getIcon("connectionIcon"),
						    connection);
			    
			    destConnElement.
				setSchematicFigure(connectionFigure);
			    
			    schematicView.addViewElement(destConnElement);
			    
		       }
		    }
		}
	    }
// 	    System.out.println("c "+schematicView.getBounds());

	    SchematicFigureLayout.layoutFigures(schematicView.getCanvas());
// 	    System.out.println("d "+schematicView.getBounds());

	    tree.expandPath(new TreePath(((DefaultMutableTreeNode)tree.
					  getModel().getRoot()).getPath()));

// 	    System.out.println("e "+schematicView.getBounds());

	    // 	    System.out.println(tree.getRootName()+" "+
	    //tree.getRoot().getChildCount());
	}
    }

    public static LSESchematicView buildSchematicView(LSSDocument document,
						      LSESchematicView 
						      schematicView)
    {
	// builds and populates both the SchematicCanvas and
	// the SchematicCanvasLayerTree

	// first let's build the window
	Point location = schematicView.getLocation();	

	Dimension screen = 
	    Toolkit.getDefaultToolkit().getScreenSize();

	// make the view visible so that it gets a graphics context,
	// then hide it again
	//schematicView.setLocation(screen.width * 2, screen.height * 2);
	schematicView.setVisible(true);
	//schematicView.setVisible(false);
	//schematicView.setLocation(location.x, location.y);


	CompilationManager compilationManager = document.
	    getCompilationManager();

	HashMap IMR = compilationManager.buildIMR();
	if(IMR == null){
	    System.out.println("Error: Compilation manager returned null IMR");
	    return schematicView;
	}

	Collection elements = IMR.values();

	buildSchematicView(schematicView, elements);

	SchematicCanvas canvas = schematicView.getCanvas();
	canvas.setPaintable(true);
	canvas.repaint();

	return schematicView;
    }

    public static LSESchematicView buildSchematicView(LSSDocument document,
						      Instance parentInstance,
						      LSESchematicView
						      schematicView)
    {
	Collection elements = parentInstance.getSubInstances();

	Point location = schematicView.getLocation();	

	Dimension screen = 
	    Toolkit.getDefaultToolkit().getScreenSize();

	// make the view visible so that it gets a graphics context,
	schematicView.setVisible(true);

	buildSchematicView(schematicView, elements);

	SchematicCanvas canvas = schematicView.getCanvas();
	canvas.setPaintable(true);
	canvas.repaint(canvas.getVisibleRect());

	return schematicView;
    }

    public static void refreshView(LSSDocument document, 
				   LSESchematicView schematicView)
    {
	SchematicViewTree tree = schematicView.getTree();
	DefaultMutableTreeNode root = tree.getRoot();

	document.loadProperties();

	tree.invalidate();
	schematicView.clear();

	CompilationManager compilationManager = 
	    document.getCompilationManager();

	HashMap IMR = compilationManager.buildIMR();

	if(IMR == null)
	    return;

	Collection elements = IMR.values();

	schematicView.getCanvas().setPaintable(false);

	buildSchematicView(schematicView, elements);

	schematicView.getCanvas().setPaintable(true);
	//schematicView.getCanvas().validate();
    }
}
