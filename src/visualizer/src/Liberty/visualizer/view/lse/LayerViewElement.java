package Liberty.visualizer.view.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.UI.dialog.*;
import Liberty.visualizer.view.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

public class LayerViewElement extends SchematicViewElement{

    public LayerViewElement(String name,
			    String label,
			    SchematicView view,
			    SchematicViewElement parent,
			    Object semanticValue)
    {
	super(name, label, view, parent, semanticValue);
    }

    private String label()
    {
	return label;
    }

    public ImageIcon getIcon()
    {
	return null;
    }

    public String getName()
    {
	return name;
    }

    public String getLabel()
    {
	return label;
    }

    public SchematicFigure getSchematicFigure()
    {
	return schematicFigure;
    }

    public Object getSemanticValue()
    {
	return semanticValue;
    }

    public DefaultMutableTreeNode getTreeNode()
    {
	return treeNode;
    }

    public void setSelected()
    {
	
    }

    public void doDialog()
    {
	LSSInfoDialog.showDialog(null, semanticValue);
    }

    public void doPopup(Component component, int x, int y)
    {
	final SchematicCanvasLayer layer = (SchematicCanvasLayer)semanticValue;
	JPopupMenu popup = new JPopupMenu();

	final JCheckBoxMenuItem layerItem = 
	    new JCheckBoxMenuItem("Set Visible", layer.isVisible());

	layerItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    layer.setVisible(layerItem.getState());
		    LayerViewElement.this.schematicView.getCanvas().repaint();
		}
	    });

	popup.add(layerItem);
	popup.show(component, x, y);
    }

    public void setSchematicFigure(SchematicFigure figure)
    {
	this.schematicFigure = figure;
    }

    public String toString()
    {
	return label;
    }
}
