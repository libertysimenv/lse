package Liberty.visualizer.view.lse;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.view.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

public class PortViewElement extends SchematicViewElement{

    public PortViewElement(SchematicView view,
			   SchematicViewElement parent,
			   SchematicFigure portFigure,
			   Collection lvlStatements,
			   Port semanticValue)
    {
	super(semanticValue.getInstance().getFullyQualifiedName()+"."+
	      semanticValue.getName(), 
	      semanticValue.getName(), 
	      view, parent, semanticValue);
	this.schematicFigure = portFigure;
    }

    public ImageIcon getIcon()
    {
	if(((Port)semanticValue).getDirection() == Port.OUTPUT)
	    return IconStore.getIcon("outputPortIcon");
	else
	    return IconStore.getIcon("inputPortIcon");
    }

    public void doPopup(Component component, int x, int y)
    {
	if(schematicFigure != null){
	    JPopupMenu popup = schematicFigure.getPopupMenu(component, x, y);

	    popup.show(component, x, y);
	}
    }

    public void showInfoDialog()
    {

    }
}
