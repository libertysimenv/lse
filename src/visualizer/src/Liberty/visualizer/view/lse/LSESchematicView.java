package Liberty.visualizer.view.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.lse.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.widget.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.view.*;
import Liberty.visualizer.*;

import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import java.util.*;
import java.util.prefs.*;

public class LSESchematicView extends SchematicView implements PropertyHolder{

    protected HashMap childViews;
    protected JLabel scaleLabel;
    protected JSlider scaleSlider;

    public LSESchematicView(Document document, String name)
    {
	super(document, name);

	childViews = new HashMap();

	Preferences prefs = 
	    Preferences.userNodeForPackage(getClass());

	Dimension screen = 
	    Toolkit.getDefaultToolkit().getScreenSize();

	int xCoord = Math.round(0.10f*screen.width);
	int yCoord = Math.round(0.50f*screen.height);
	int width = Math.round(0.85f*screen.width);
	int height = Math.round(0.45f*screen.height);
	int dividerLocation = splitPane.getDividerLocation();

	try{
	    xCoord = prefs.getInt(document.getName()+".xCoord", xCoord);
	    yCoord = prefs.getInt(document.getName()+".yCoord", yCoord);
	    width = prefs.getInt(document.getName()+".width", width);
	    height = prefs.getInt(document.getName()+".height", height);
	    dividerLocation = prefs.getInt(document.getName()+
					   ".dividerLocation", 
					   dividerLocation);
	}
	catch(NullPointerException npe){
	    // do nothing, defaults have already been set, just suppress the
	    // message on stderr
	}

	if(width > screen.width){
	    width = screen.width/2;
	    prefs.putInt(document.getName()+".width", width);
	}
	if(height > screen.height){
	    height = screen.height/2;
	    prefs.putInt(document.getName()+".height", height);
	}

	setBounds(xCoord, yCoord, width, height);
	splitPane.setDividerLocation(dividerLocation);

// 	handleStoredProperties(((LSSDocument)document).
// 	      getPropertiesForViewElement(getFullyQualifiedName()));

	addWindowListener(new WindowAdapter(){
		public void windowClosing(WindowEvent we){
		    try{
			LSESchematicView.this.storePreferences();
		    }
		    catch(Exception e){}
		}
	    });
    }

    public void storePreferences()
    {
	String docName = document.getName();
	Rectangle bounds = getBounds();
	int dividerLocation = splitPane.getDividerLocation();

	try{
	    Preferences prefs = 
		Preferences.userNodeForPackage(getClass());
	
	    prefs.putInt(docName+".xCoord", bounds.x);
	    prefs.putInt(docName+".yCoord", bounds.y);
	    prefs.putInt(docName+".width", bounds.width);
	    prefs.putInt(docName+".height", bounds.height);
	    prefs.putInt(docName+".dividerLocation", 
			 dividerLocation);
	    prefs.flush();
	}
	catch(BackingStoreException bse){
			// do nothing, if the preferences can't be stored it's
			// not a big deal
	}
	catch(Exception e){}
    }


    public void addElementToSelection(String name)
    {
	SchematicViewElement element = 
	    (SchematicViewElement)viewElements.get(name);

	if(element != null && !selectedElements.contains(element)){
	    SchematicFigure figure = element.getSchematicFigure();
	    
	    if(figure != null){
		figure.setSelected(true);
		figure.repaint();
		
		if(tree != null){
		    DefaultMutableTreeNode treeNode = element.getTreeNode();
		    TreePath path = new TreePath(treeNode.getPath());
		    tree.addSelectionPath(path);
		    tree.scrollPathToVisible(path);
		}
	    }
	    selectedElements.add(element);
	}
    }

    public void addViewElement(SchematicViewElement element)
    {
	viewElements.put(element.getName(), element);
    }

    public void deselectElements()
    {
	for(int i = 0; i < selectedElements.size(); i++){
	    SchematicViewElement element = 
		(SchematicViewElement)selectedElements.get(i);
	    
	    SchematicFigure figure = element.getSchematicFigure();

	    if(figure != null){
		figure.setSelected(false);
		figure.repaint();
	    }
	    
	    if(tree != null){
		DefaultMutableTreeNode treeNode = element.getTreeNode();
		TreePath path = new TreePath(treeNode.getPath());
		tree.removeSelectionPath(path);
	    }   
	}
    }

    public void clear()
    {
	canvas.clear();
	viewElements.clear();
	tree.reset();
	propertyManager.clear();
    }

    public void doTranslation(int dx, 
			      int dy, 
			      Point eventPoint)
    {
	Rectangle repaintRegion = null;

	for(int i = 0; i < selectedElements.size(); i++){
	    SchematicViewElement element = 
		(SchematicViewElement)selectedElements.get(i);

	    SchematicFigure figure = element.getSchematicFigure();

	    // get the paint bounds before the drag
	    if(repaintRegion == null)
		repaintRegion = figure.getPaintBounds();
	    else
		repaintRegion = repaintRegion.
		    createUnion(figure.getPaintBounds()).getBounds();

	    figure.drag(dx, dy, eventPoint);
	    
	    // get the union of the bounds before and after the drag
	    repaintRegion = repaintRegion.
		createUnion(figure.getPaintBounds()).getBounds();
	}
	if(repaintRegion != null){
	    canvas.repaint(repaintRegion);
	}
    }

    public SchematicCanvas getCanvas()
    {
	return canvas;
    }

    public Document getDocument()
    {
	return document;
    }

    public SchematicFigure getFigureByUserObject(Object o)
    {
	return canvas.getFigureByUserObject(o);
    }

    public String getFullyQualifiedName()
    {
	return document.getPath();
    }

    public String getName()
    {
	return document.getPath();
    }

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	JPopupMenu popup = new JPopupMenu();

	PropertyInfo info = propertyManager.getProperty("Snap to Grid");
	if(info != null){
	    boolean snapToGrid = ((Boolean)info.getValue()).booleanValue();
	    final JCheckBoxMenuItem snapToGridItem = 
		new JCheckBoxMenuItem("Snap to Grid", snapToGrid);

	    snapToGridItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			PropertyInfo info = 
			    LSESchematicView.this.
			    propertyManager.getProperty("Snap to Grid");
			
			info.setValue(new Boolean(snapToGridItem.
						  getState()));
			LSESchematicView.this.propertyChanged(info);
		    }
		});
	    popup.add(snapToGridItem);
	}

	info = propertyManager.getProperty("Auto-Route Connections");
	if(info != null){
	    boolean autoRoute = ((Boolean)info.getValue()).booleanValue();
	    final JCheckBoxMenuItem autoRouteItem = 
		new JCheckBoxMenuItem("Auto-Route Connections", autoRoute);

	    autoRouteItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			PropertyInfo info = 
			    LSESchematicView.this.
			    propertyManager.
			    getProperty("Auto-Route Connections");
			
			info.setValue(new Boolean(autoRouteItem.
						  getState()));
			LSESchematicView.this.propertyChanged(info);
		    }
		});
	    popup.add(autoRouteItem);
	}

	info = propertyManager.getProperty("Show Jog Points on Connections");
	if(info != null){
	    boolean jogPoints = ((Boolean)info.getValue()).booleanValue();
	    final JCheckBoxMenuItem jogPointsItem = 
		new JCheckBoxMenuItem("Show Jog Points on Connections", 
				      jogPoints);

	    jogPointsItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			PropertyInfo info = 
			    LSESchematicView.this.
			    propertyManager.
			    getProperty("Show Jog Points on Connections");
			
			info.setValue(new Boolean(jogPointsItem.
						  getState()));
			LSESchematicView.this.propertyChanged(info);
		    }
		});
	    popup.add(jogPointsItem);
	}


	JMenuItem showPropertiesItem = new JMenuItem("Show Visual Properties");
	showPropertiesItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    LSESchematicView.this.
			updateCachedProperties();
		    LSESchematicView.this.getPropertyManager().
			showPropertiesDialog();
		}
	    });
	popup.add(showPropertiesItem);
	return popup;
    }

    public java.util.List getPropertiesForViewElement(String elementName)
    {
	return ((LSSDocument)document).
	    getPropertiesForViewElement(elementName);
    }

    public PropertyInfo getProperty(String name)
    {
	return propertyManager.getProperty(name);
    }

    public PropertyManager getPropertyManager()
    {
	return propertyManager;
    }

    public SchematicViewTree getTree()
    {
	return tree;
    }

    public SchematicViewElement getViewElement(String name)
    {
	return (SchematicViewElement)viewElements.get(name);
    }

    public void initializeProperties()
    {	
	PropertyInfo info = new PropertyInfo(this, 
					     "Auto-Route Connections",
					     "General",
					     new Boolean(true),
					     PropertyInfo.BOOL_DATATYPE,
					     null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Show Jog Points on Connections",
				"General",
				new Boolean(false),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Show Direction on Ports",
				"General",
				new Boolean(true),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Snap to Grid",
				"General",
				new Boolean(false),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Canvas Width",
				"General",
				new Integer(canvas.getBounds().width),
				PropertyInfo.INT_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Canvas Height",
				"General",
				new Integer(canvas.getBounds().height),
				PropertyInfo.INT_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Canvas Scale Factor",
				"General",
				new Integer(100),
				PropertyInfo.INT_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

    }

    protected void initializeToolbar()
    {
	toolbar.removeAll();

	final JButton refreshButton = 
	    new JButton(VisualizerActions.getRefreshSchematicViewAction());
	refreshButton.setText("");
	refreshButton.setToolTipText("Recompile and Refresh View");
	refreshButton.setRolloverIcon(IconStore.getIcon("refreshOnIcon"));
	refreshButton.setRolloverEnabled(true);
	refreshButton.setBorderPainted(false);
	
	final JButton saveLayoutButton = 
	    new JButton(VisualizerActions.getSaveSchematicAction());
	saveLayoutButton.setText("");
	saveLayoutButton.setToolTipText("Save Layout");
	saveLayoutButton.setRolloverIcon(IconStore.
					 getIcon("saveSchematicOnIcon"));
	saveLayoutButton.setRolloverEnabled(true);
	saveLayoutButton.setBorderPainted(false);

	JButton buildButton = 
	    new JButton(VisualizerActions.getLSSBuildAction());
	buildButton.setText("");
	buildButton.setToolTipText("build this description");
	buildButton.setRolloverIcon(IconStore.getIcon("compileOnIcon"));
	buildButton.setRolloverEnabled(true);
	buildButton.setBorderPainted(false);

	JButton executeButton = 
	    new JButton(VisualizerActions.getLSEExecuteAction());
	executeButton.setText("");
	executeButton.setToolTipText("run simulation");
	executeButton.setRolloverIcon(IconStore.getIcon("executeOnIcon"));
	executeButton.setRolloverEnabled(true);
	executeButton.setBorderPainted(false);

	scaleLabel = new JLabel("Canvas Scaling: 1.00 ");
	scaleSlider = 
	    new JSlider(SwingConstants.HORIZONTAL, 5, 500, 100);

	scaleSlider.setPreferredSize(new Dimension(150, 40));
	scaleSlider.setMaximumSize(new Dimension(150, 40));
	scaleSlider.setToolTipText("Canvas Scaling Factor");
	scaleSlider.addChangeListener(new ChangeListener(){
		public void stateChanged(ChangeEvent e){
		    int scaleValue = scaleSlider.getValue();
		    float scaleFactor = (float)scaleValue/100;

		    canvas.setScaleFactor(scaleFactor);
		    canvas.repaint();

		    String scaleString = scaleFactor+"";
		    if(scaleString.length() < 4)
			scaleString = scaleString + "0";
		    scaleLabel.setText("Canvas Scaling: "+scaleString+" ");

		    PropertyInfo pInfo = getProperty("Canvas Scale Factor");
		    if(scaleValue != ((Integer)pInfo.getValue()).intValue()){
			pInfo.setValue(new Integer(scaleSlider.getValue()));
		    }
		}
	    });
	//scaleSlider.setPaintLabels(false);
	scaleSlider.setPaintTicks(true);
	scaleSlider.setMajorTickSpacing(50);
	scaleSlider.setPaintLabels(false);

 	toolbar.add(refreshButton);
	toolbar.add(saveLayoutButton);
	toolbar.add(buildButton);
	toolbar.add(executeButton);
	
	toolbar.addSeparator(new Dimension(25, 10));
	
	toolbar.add(scaleLabel);
	toolbar.add(scaleSlider);
	    
    }

    public void handleCommand(final String elementID, final String command)
    {

	Runnable updater = new Runnable(){
		public void run(){
		    SchematicViewElement element = 
			LSESchematicView.this.getViewElement(elementID);
		    if(element != null){
			SchematicFigure figure = element.getSchematicFigure();
			if(figure != null){
			    figure.handleCommand(command);
			}
			else{
			    Iterator viewIterator = 
				LSESchematicView.this.
				childViews.values().iterator();
			    while(viewIterator.hasNext()){
				SchematicView view = 
				    (SchematicView)viewIterator.next();
				element = view.getViewElement(elementID);
				if(element != null){
				    figure = element.getSchematicFigure();
				    if(figure != null){
					figure.handleCommand(command);
				    }
				}
			    }
			}
		    
			if(figure == null){
			    System.out.println("Visualizer: Element '"+
					       elementID+
					       "' not found, could not "+
					       "handle "+
					       "command: '"+command+"'");
			}		    
		    }
		}
	    };
	new Thread(updater).run();
		
    }

    /**
     * Set this figures properties to reflect the properties defined in the
     * propertySet, if they are applicable.
     *
     * @param propertySet a set of PropertyInfo objects
     */
    public void handleStoredProperties(java.util.List propertyList)
    {
	Document document = canvas.getDocument();

	Iterator propertyIterator = propertyList.iterator();
	while(propertyIterator.hasNext()){
	    PropertyInfo info = (PropertyInfo)propertyIterator.next();
	    Object value = info.getValue();
	    String propertyName = info.getPropertyName();
	    String propertyHolderName = info.getPropertyHolderName();
	    int datatype = info.getDataType();

	    if(propertyHolderName.
	       equals(getFullyQualifiedName())){

		if(datatype != PropertyInfo.COMMAND_DATATYPE){
		    PropertyInfo localInfo = 
			propertyManager.getProperty(info.getPropertyName());
		    Object localValue;
		    
		    if(localInfo != null){
			localValue = localInfo.getValue();
			if(!localValue.equals(value)){
			    localInfo.setValue(value);
			    propertyChanged(localInfo);
			}
		    }
		}
	    }
	}
    }


    public boolean isSelectionEmpty()
    {
	return selectedElements.isEmpty();
    }

    public void propertyChanged(PropertyInfo info)
    {
	String propertyName = info.getPropertyName();

	if(propertyName.equals("Show Jog Points on Connections")){
	    Object value = info.getValue();

	    Iterator elementIterator = viewElements.values().iterator();
	    while(elementIterator.hasNext()){
		SchematicViewElement element = 
		    (SchematicViewElement)elementIterator.next();
		SchematicFigure figure = element.getSchematicFigure();

		if(figure != null && figure instanceof 
		   PluggableConnectionFigure){
		    PropertyInfo pInfo = ((PropertyHolder)figure).
			getProperty("Show Jog Points");
		    if(pInfo != null && (!pInfo.getValue().equals(value))){
			pInfo.setValue(value);
			((PropertyHolder)figure).propertyChanged(pInfo);
		    }
		}
	    }
	}
	else if(propertyName.equals("Auto-Route Connections")){
	    Object value = info.getValue();

	    Iterator elementIterator = viewElements.values().iterator();
	    while(elementIterator.hasNext()){
		SchematicViewElement element = 
		    (SchematicViewElement)elementIterator.next();
		SchematicFigure figure = element.getSchematicFigure();

		if(figure != null && figure instanceof 
		   PluggableConnectionFigure){
		    PropertyInfo pInfo = ((PropertyHolder)figure).
			getProperty("Auto-Route Connection");
		    if(pInfo != null && (!pInfo.getValue().equals(value))){
			pInfo.setValue(value);
			((PropertyHolder)figure).propertyChanged(pInfo);
		    }
		}
	    }
	}
	else if(propertyName.equals("Show Direction on Ports")){
	    Object value = info.getValue();

	    Iterator elementIterator = viewElements.values().iterator();
	    while(elementIterator.hasNext()){
		SchematicViewElement element = 
		    (SchematicViewElement)elementIterator.next();
		SchematicFigure figure = element.getSchematicFigure();

		if(figure != null && figure instanceof PortFigure){
		    PropertyInfo pInfo = ((PropertyHolder)figure).
			getProperty("Show Direction");
		    if(pInfo != null && (!pInfo.getValue().equals(value))){
			pInfo.setValue(value);
			((PropertyHolder)figure).propertyChanged(pInfo);
		    }
		}
	    }
	}
	else if(propertyName.equals("Snap to Grid")){
	    boolean value = ((Boolean)info.getValue()).booleanValue();
	    if(value != canvas.isSnapToGrid()){
		canvas.setSnapToGrid(value);

		Iterator elementIterator = viewElements.values().iterator();
		while(elementIterator.hasNext()){
		    SchematicViewElement element = 
			(SchematicViewElement)elementIterator.next();

		    if(element instanceof InstanceViewElement){
			SchematicFigure figure = element.getSchematicFigure();

			if(figure != null)
			    figure.snapToGrid();
		    }
		}
		canvas.repaint();
	    }
	}
	else if(propertyName.equals("Canvas Width")){
	    int width = ((Integer)info.getValue()).intValue();
	    canvas.setWidth(width);
	}
	else if(propertyName.equals("Canvas Height")){
	    int height = ((Integer)info.getValue()).intValue();
	    canvas.setHeight(height);
	}
	else if(propertyName.equals("Canvas Scale Factor")){
	    System.out.println("Setting Scale Factor: "+
			       ((Integer)info.getValue()).intValue());
	    scaleSlider.setValue(((Integer)info.getValue()).intValue());
	}
    }

    public void selectBoundedElements(Rectangle bounds)
    {
	Iterator iterator = viewElements.values().iterator();

	while(iterator.hasNext()){
	    SchematicViewElement element = 
		(SchematicViewElement)iterator.next(); 
	    SchematicFigure figure = element.getSchematicFigure();

	    if(figure != null){
		if(bounds.contains(figure.getBounds())){
		    if(figure instanceof SchematicNodeFigure){
			figure.setSelected(true);
			figure.repaint();
		    
			if(tree != null){
			    DefaultMutableTreeNode treeNode = 
				element.getTreeNode();
			    TreePath path = new TreePath(treeNode.getPath());
			    tree.addSelectionPath(path);
			    tree.scrollPathToVisible(path);
			}
			selectedElements.add(element);
		    }
		}
	    }
	}
    }

    public void setSelectedElement(String name)
    {
	if(name != null){
	    SchematicViewElement element = 
		(SchematicViewElement)viewElements.get(name);

	    if(element == null){
		deselectElements();
		selectedElements.clear();
		return;
	    }
	    
	    SchematicFigure figure = element.getSchematicFigure();

	    if(figure != null){
		deselectElements();
		selectedElements.clear();
		selectedElements.add(element);

		figure.setSelected(true);
		figure.repaint();
	    }
	    else{
		deselectElements();
		selectedElements.clear();
		selectedElements.add(element);
	    }
	    
	    if(tree != null){
		DefaultMutableTreeNode treeNode = element.getTreeNode();
		TreePath path = new TreePath(treeNode.getPath());
		tree.setSelectionPath(path);
		tree.scrollPathToVisible(path);
	    }
	}
	else{
	    deselectElements();
	    selectedElements.clear();
	}
    }

    public void showChildView(String elementName)
    {
	View view = (View)childViews.get(elementName);
	if(view != null){
	    view.setVisible(true);
	}
	else{
	    SchematicViewElement element = 
		(SchematicViewElement)viewElements.get(elementName);
	    Instance instance = (Instance)element.getSemanticValue();

	    view = new LSESchematicView((LSSDocument)document,
					instance.
					getFullyQualifiedName());

	    ViewBuilder.buildSchematicView((LSSDocument)document, 
					   instance,
					   (LSESchematicView)view);

	    Rectangle bounds = view.getBounds();

	    view.setBounds(bounds.x + 15*(childViews.size()+1), 
			   bounds.y + 15*(childViews.size()+1),
			   bounds.width,
			   bounds.height);

	    view.setVisible(true);
	    childViews.put(elementName, view);
	}
    }

    public void refresh()
    {
	clear();
	ViewBuilder.refreshView((LSSDocument)document, this);
	canvas.repaint();
    }

    public void scrollToElement(SchematicViewElement element)
    {
	SchematicFigure figure = element.getSchematicFigure();

	if(figure != null){
	    JViewport viewPort = scrollPane.getViewport();
	    Rectangle viewBounds = viewPort.getViewRect();
	    float scaleFactor = canvas.getScaleFactor();
	    if(scaleFactor != 1.0f){
		viewBounds.setRect(Math.round(viewBounds.x*(1/scaleFactor)),
				   Math.round(viewBounds.y*(1/scaleFactor)),
				   Math.round(viewBounds.width*
					      (1/scaleFactor)),
				   Math.round(viewBounds.height*
					      (1/scaleFactor)));
	    }
	    Rectangle figureBounds = figure.getBounds();
	    if(!viewBounds.intersects(figureBounds)){
		// we'll try to center the figure on the screen
		int width = viewBounds.width;
		int height = viewBounds.height;
		int xCoord = figureBounds.x - (width/2) + 
		    (figureBounds.width/2);
		int yCoord = figureBounds.y - (width/2) + 
		    (figureBounds.height/2);
		
		
		if(xCoord < 0){
		    xCoord = (int)(figureBounds.x/2);
		}
		if(yCoord < 0){
		    yCoord = (int)(figureBounds.y/2);
		}

		if(scaleFactor != 1.0f){
		    xCoord = (int)(xCoord*scaleFactor);
		    yCoord = (int)(yCoord*scaleFactor);
		}
		viewPort.setViewPosition(new Point(xCoord, yCoord)); 
	    }
	}
    }

    public void update()
    {

    }

    public void storeProperties()
    {
	super.storeProperties();

	Iterator childViewIterator = childViews.values().iterator();
	while(childViewIterator.hasNext()){
	    SchematicView childView = (SchematicView)childViewIterator.next();

	    childView.storeProperties();
	}
    }

    public void updateCachedProperties()
    {
    }
}
