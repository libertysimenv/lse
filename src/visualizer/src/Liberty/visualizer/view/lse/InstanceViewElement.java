package Liberty.visualizer.view.lse;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.lse.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.view.*;
import Liberty.visualizer.UI.dialog.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.*;

public class InstanceViewElement extends SchematicViewElement{

    /** for hierarchical instances */
    public InstanceViewElement(LSESchematicView view,
			       SchematicViewElement parent,
			       Instance semanticValue)
    {
	super(semanticValue.getFullyQualifiedName(),
	      semanticValue.getName() + " : " + semanticValue.getModuleName(), 
	      view, parent, semanticValue);

    }

    public InstanceViewElement(LSESchematicView view,
			       SchematicViewElement parent,
			       java.util.List propertyList,
			       Instance semanticValue)
    {
	super(semanticValue.getFullyQualifiedName(),
	      semanticValue.getName() + " : " + semanticValue.getModuleName(), 
	      view, parent, semanticValue);

	this.schematicFigure = SchematicFigureBuilder.
	    buildInstanceFigure(view, parent, semanticValue, propertyList);

	Object obj = parent.getSemanticValue();

	if(obj != null && obj instanceof SchematicCanvasContainer){
	    ((SchematicCanvasContainer)obj).addFigure(schematicFigure);
	}

	buildTreeNode(view);
    }

    private void buildTreeNode(LSESchematicView view)
    {
	Instance instance = (Instance)semanticValue;
	Iterator iterator;

	if(!instance.isLeaf()){
	    String instancesString = "Instances:";
	    SchematicViewElement instancesElement =
		new SchematicViewElement(instance.getFullyQualifiedName()+"."+
					 instancesString,
					 instancesString,
					 view,
					 this,
					 instancesString);
	    view.addViewElement(instancesElement);

	    iterator = instance.getSubInstances().iterator();
	    while(iterator.hasNext()){
		Instance subInstance = (Instance)iterator.next();

		SchematicViewElement subElement = 
		    new InstanceViewElement((LSESchematicView)schematicView,
					    instancesElement,
					    subInstance);
		
		view.addViewElement(subElement);
	    }
	}

	String portsString = "Ports:";
	SchematicViewElement portsElement =
	    new SchematicViewElement(instance.getFullyQualifiedName()+"."+
				     portsString,
				     portsString,
				     view,
				     this,
				     portsString);

	view.addViewElement(portsElement);


	String parametersString = "Parameters:";
	SchematicViewElement parametersElement =
	    new SchematicViewElement(instance.getFullyQualifiedName()+"."+
				     parametersString,
				     parametersString,
				     view,
				     this,
				     parametersString);

	view.addViewElement(parametersElement);

	iterator = instance.getParameters().iterator();
	while(iterator.hasNext()){
	    Parameter parm = (Parameter)iterator.next();
	    String nodeLabel = parm.getName() + " = " + parm.getValue() + 
		" : " + parm.getType();

	    SchematicViewElement parmElement = 
		new InfoViewElement(instance.getFullyQualifiedName()+"."+
				    parm.getName(),
				    nodeLabel,
				    view,
				    parametersElement,
				    IconStore.getIcon("dotIcon"),
				    parm);

	    view.addViewElement(parmElement);

	}

	String codePointsString = "Code Points:";
	SchematicViewElement codePointsElement =
	    new SchematicViewElement(instance.getFullyQualifiedName()+"."+
				     codePointsString,
				     codePointsString,
				     view,
				     this,
				     codePointsString);

	view.addViewElement(codePointsElement);

	iterator = instance.getCodePoints().iterator();
	while(iterator.hasNext()){
	    CodePoint cp = (CodePoint)iterator.next();

	    SchematicViewElement cpElement = 
		new InfoViewElement(instance.getFullyQualifiedName()+"."+
				    cp.getName(),
				    cp.getName(),
				    view,
				    codePointsElement,
				    IconStore.getIcon("dotIcon"),
				    cp);

	    view.addViewElement(cpElement);

	}

	String eventsString = "Events:";
	SchematicViewElement eventsElement =
	    new SchematicViewElement(instance.getFullyQualifiedName()+"."+
				     eventsString,
				     eventsString,
				     view,
				     this,
				     eventsString);

	view.addViewElement(eventsElement);

	iterator = instance.getEvents().iterator();
	while(iterator.hasNext()){
	    Liberty.LSS.IMR.Event event = 
		(Liberty.LSS.IMR.Event)iterator.next();

	    SchematicViewElement eventElement = 
		new InfoViewElement(instance.getFullyQualifiedName()+"."+
				    event.getName(),
				    event.getName(),
				    view,
				    eventsElement,
				    IconStore.getIcon("dotIcon"),
				    event);

	    view.addViewElement(eventElement);

	}

	String queriesString = "Queries:";
	SchematicViewElement queriesElement =
	    new SchematicViewElement(instance.getFullyQualifiedName()+"."+
				     queriesString,
				     queriesString,
				     view,
				     this,
				     queriesString);

	view.addViewElement(queriesElement);

	iterator = instance.getQueries().iterator();
	while(iterator.hasNext()){
	    Query query = (Query)iterator.next();

	    SchematicViewElement queryElement = 
		new InfoViewElement(instance.getFullyQualifiedName()+"."+
				    query.getName(),
				    query.getName(),
				    view,
				    queriesElement,
				    IconStore.getIcon("dotIcon"),
				    query);

	    view.addViewElement(queryElement);
	}

	String structAddsString = "Struct Adds:";
	SchematicViewElement structAddsElement =
	    new SchematicViewElement(instance.getFullyQualifiedName()+"."+
				     structAddsString,
				     structAddsString,
				     view,
				     this,
				     structAddsString);

	view.addViewElement(structAddsElement);

	HashMap structAdds = instance.getStructAdds();
	iterator = structAdds.keySet().iterator();
	while(iterator.hasNext()){
	    Object key = iterator.next();
	    String dataStructureName = key.toString();
	    HashMap hm = (HashMap)structAdds.get(key);

	    SchematicViewElement dataStructureElement = 
		new SchematicViewElement(instance.getFullyQualifiedName()+
					 "."+dataStructureName,
					 dataStructureName,
					 view,
					 structAddsElement,
					 dataStructureName);
	    
	    view.addViewElement(dataStructureElement);
	    
	    Iterator keyIterator = hm.keySet().iterator();
	    while(keyIterator.hasNext()){
		Object saKey = keyIterator.next();

		StructAdd structAdd = (StructAdd)hm.get(saKey);

		SchematicViewElement structAddElement = 
		    new InfoViewElement(instance.
					getFullyQualifiedName()+"."+
					structAdd,
					structAdd.getFieldName().
					getValue().toString(),
					view,
					dataStructureElement,
					IconStore.getIcon("dotIcon"),
					structAdd);
		
	    }
	}
    }

    public ImageIcon getIcon()
    {
	return IconStore.getIcon("moduleIcon");
    }

    public void doPopup(Component component, int x, int y)
    {
	final Instance instance = (Instance)semanticValue;
	final SchematicFigure figure = schematicFigure;

	if(figure != null){
	    JPopupMenu popup = figure.getPopupMenu(component, x, y);
	    popup.show(component, x, y);
	}
    }

    public void showInfoDialog()
    {

    }

}
