package Liberty.visualizer.view.lse;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import Liberty.visualizer.UI.widget.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.view.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

public class InfoViewElement extends SchematicViewElement{
    private ImageIcon icon;

    public InfoViewElement(String name,
			   String label,
			   SchematicView view,
			   SchematicViewElement parent,
			   ImageIcon icon,
			   Object semanticValue)
    {
	super(name, label, view, parent, semanticValue);

	this.icon = icon;
    }

    public ImageIcon getIcon()
    {
	return icon;
    }

    public void doPopup(Component component, int x, int y)
    {
    }
}
