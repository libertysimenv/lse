package Liberty.visualizer.view.lse;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.canvas.figure.lse.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.view.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

public class ConnectionViewElement extends SchematicViewElement{

    public ConnectionViewElement(SchematicView view,
				 SchematicViewElement parent,
				 java.util.List propertyList,
				 Connection semanticValue)
    {
	super(semanticValue.toString(), semanticValue.toString(),
	      view, parent, semanticValue);

	Port srcPort = semanticValue.getSourcePort();
	Port destPort = semanticValue.getDestPort();
	Instance srcInstance = srcPort.getInstance();
	Instance destInstance = destPort.getInstance();

	PluggableInstanceFigure srcInstanceFigure = 
	    (PluggableInstanceFigure)view.
	    getFigureByUserObject(srcInstance);
	PluggableInstanceFigure destInstanceFigure = 
	    (PluggableInstanceFigure)view.
	    getFigureByUserObject(destInstance);

	if(srcInstanceFigure != null && destInstanceFigure != null){

	    this.schematicFigure = SchematicFigureBuilder.
		buildConnectionFigure(view.getCanvas(),
				      semanticValue.toString(), 
				      semanticValue, 
				      srcInstanceFigure, 
				      destInstanceFigure,
				      propertyList);

	    srcInstanceFigure.addOutEdge((SchematicEdgeFigure)schematicFigure);
	    destInstanceFigure.addInEdge((SchematicEdgeFigure)schematicFigure);
	}
	else{
	    if(srcInstanceFigure != null){
		this.schematicFigure = SchematicFigureBuilder.
		    buildStubFigure(view.getCanvas(),
				    semanticValue.toString(), 
				    semanticValue, 
				    srcInstanceFigure, 
				    destInstanceFigure,
				    propertyList);

		srcInstanceFigure.
		    addOutEdge((SchematicEdgeFigure)schematicFigure);
	    }
	    else if(destInstanceFigure != null){
		this.schematicFigure = SchematicFigureBuilder.
		    buildStubFigure(view.getCanvas(),
				    semanticValue.toString(), 
				    semanticValue, 
				    srcInstanceFigure, 
				    destInstanceFigure,
				    propertyList);

		destInstanceFigure.
		    addInEdge((SchematicEdgeFigure)schematicFigure);
	    }
	}
	Object obj = parent.getSemanticValue();
	
	if(obj != null && obj instanceof SchematicCanvasContainer){
	    ((SchematicCanvasContainer)obj).
		addFigure(this.schematicFigure);
	}
    }

    public ImageIcon getIcon()
    {
	return IconStore.getIcon("connectionIcon");
    }

    public void doPopup(Component component, int x, int y)
    {
	if(schematicFigure != null){
	    JPopupMenu popup = schematicFigure.getPopupMenu(component, x, y);

	    popup.show(component, x, y);
	}
    }

    public void showInfoDialog()
    {

    }
}
