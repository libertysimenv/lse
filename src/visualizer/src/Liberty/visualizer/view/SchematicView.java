package Liberty.visualizer.view;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.widget.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.*;

import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import java.util.*;

//FIXME: this should be an abstract class...  remove LSS specific methods

public class SchematicView extends View implements PropertyHolder{
    /** the document that this frame provides a view of */
    protected Document document;
    /** the canvas that the schematc layout is drawn on */
    protected SchematicCanvas canvas;
    /** the scroll panel that holds the canvas */
    protected JScrollPane scrollPane;
    /** the panel that displays layer info */
    protected SchematicViewTree tree;
    /** the JSplitPane, the left component is tree, the right is canvas */
    protected JSplitPane splitPane;
    /** the toolbar for this JFrame */
    protected JToolBar toolbar;
    /** */
    protected Vector selectedElements;
    /** */
    protected HashMap viewElements;
    /** */
    protected PropertyManager propertyManager;

    public SchematicView(Document doc, String name)
    {
	super();

	int index;
	if((index = name.indexOf(".")) > 0){
	    name = name.substring(0, index);
	}

	this.document = doc;
	this.toolbar = new JToolBar();
	this.selectedElements = new Vector();
	this.viewElements = new HashMap();
	this.propertyManager = new PropertyManager(this, doc);
	this.canvas = new SchematicCanvas(doc, this);

	initializeToolbar();

	Container contentPane = getContentPane();
	contentPane.add(toolbar, BorderLayout.NORTH);

	this.scrollPane = new JScrollPane(canvas);
	this.tree = new SchematicViewTree(this, name);
	this.splitPane = 
		new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true);

	splitPane.setRightComponent(scrollPane);
	splitPane.setLeftComponent(new JScrollPane(tree));
	splitPane.setDividerLocation(200);
	splitPane.setOneTouchExpandable(true);
	contentPane.add(splitPane, BorderLayout.CENTER);

	setTitle(name + "    ("+doc.getPath()+")");
	ImageIcon logo = IconStore.getIcon("libertyFlameSmallIcon");

// 	if(logo != null)
// 	    setIconImage(logo.getImage());

	initializeProperties();
    }

    public void storePreferences()
    {

    }

    public void addElementToSelection(String name)
    {
	SchematicViewElement element = 
	    (SchematicViewElement)viewElements.get(name);

	if(element != null && !selectedElements.contains(element)){
	    SchematicFigure figure = element.getSchematicFigure();
	    
	    if(figure != null){
		figure.setSelected(true);
		figure.repaint();
		
		if(tree != null){
		    DefaultMutableTreeNode treeNode = element.getTreeNode();
		    TreePath path = new TreePath(treeNode.getPath());
		    tree.addSelectionPath(path);
		    tree.scrollPathToVisible(path);
		}
	    }
	    selectedElements.add(element);
	}
    }

    public void addViewElement(SchematicViewElement element)
    {
	viewElements.put(element.getName(), element);
    }

    public void deselectElements()
    {
	for(int i = 0; i < selectedElements.size(); i++){
	    SchematicViewElement element = 
		(SchematicViewElement)selectedElements.get(i);
	    
	    SchematicFigure figure = element.getSchematicFigure();

	    if(figure != null){
		figure.setSelected(false);
		figure.repaint();
	    }
	    
	    if(tree != null){
		DefaultMutableTreeNode treeNode = element.getTreeNode();
		TreePath path = new TreePath(treeNode.getPath());
		tree.removeSelectionPath(path);
	    }   
	}
    }

    public void clear()
    {
	canvas.clear();
	viewElements.clear();
	tree.reset();
	propertyManager.clear();
    }

    public void doTranslation(int dx, 
			      int dy, 
			      Point eventPoint)
    {
	Rectangle repaintRegion = null;

	for(int i = 0; i < selectedElements.size(); i++){
	    SchematicViewElement element = 
		(SchematicViewElement)selectedElements.get(i);

	    SchematicFigure figure = element.getSchematicFigure();

	    // get the paint bounds before the drag
	    if(repaintRegion == null)
		repaintRegion = figure.getPaintBounds();
	    else
		repaintRegion = repaintRegion.
		    createUnion(figure.getPaintBounds()).getBounds();

	    figure.drag(dx, dy, eventPoint);
	    
	    // get the union of the bounds before and after the drag
	    repaintRegion = repaintRegion.
		createUnion(figure.getPaintBounds()).getBounds();
	}
	if(repaintRegion != null){
	    canvas.repaint(repaintRegion);
	}
    }

    public SchematicCanvas getCanvas()
    {
	return canvas;
    }

    public Document getDocument()
    {
	return document;
    }

    public SchematicFigure getFigureByUserObject(Object o)
    {
	return canvas.getFigureByUserObject(o);
    }

    public String getFullyQualifiedName()
    {
	return document.getPath();
    }

    public String getName()
    {
	return document.getPath();
    }

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	return new JPopupMenu();
    }

    public PropertyInfo getProperty(String name)
    {
	return propertyManager.getProperty(name);
    }

    public PropertyManager getPropertyManager()
    {
	return propertyManager;
    }

    public SchematicViewTree getTree()
    {
	return tree;
    }

    public SchematicViewElement getViewElement(String name)
    {
	return (SchematicViewElement)viewElements.get(name);
    }

    public void initializeProperties()
    {
    }

    protected void initializeToolbar()
    {
	toolbar.removeAll();

	final JButton refreshButton = 
	    new JButton(VisualizerActions.getRefreshSchematicViewAction());
	refreshButton.setText("");
	refreshButton.setToolTipText("Recompile and Refresh View");
	refreshButton.setRolloverIcon(IconStore.getIcon("refreshOnIcon"));
	refreshButton.setRolloverEnabled(true);
	refreshButton.setBorderPainted(false);
	
	final JButton saveLayoutButton = 
	    new JButton(VisualizerActions.getSaveSchematicAction());
	saveLayoutButton.setText("");
	saveLayoutButton.setToolTipText("Save Layout");
	saveLayoutButton.setRolloverIcon(IconStore.
					 getIcon("saveSchematicOnIcon"));
	saveLayoutButton.setRolloverEnabled(true);
	saveLayoutButton.setBorderPainted(false);

	final JSlider scaleSlider = 
	    new JSlider(SwingConstants.HORIZONTAL, 5, 100, 500);
	
	scaleSlider.setPreferredSize(new Dimension(150, 40));
	scaleSlider.setMaximumSize(new Dimension(150, 40));
	scaleSlider.setToolTipText("Canvas Scaling Factor");
	scaleSlider.addChangeListener(new ChangeListener(){
		public void stateChanged(ChangeEvent e){
		    canvas.setScaleFactor((float)scaleSlider.getValue()/100);
		    canvas.repaint();
		}
	    });
	scaleSlider.setPaintLabels(false);
	scaleSlider.setPaintTicks(true);
	scaleSlider.setMajorTickSpacing(50);
	//scaleSlider.setPaintLabels(true);

 	toolbar.add(refreshButton);
	toolbar.add(saveLayoutButton);
	
	toolbar.addSeparator(new Dimension(25, 10));
	
	toolbar.add(new JLabel("Canvas Scaling"));
	toolbar.add(scaleSlider);
	    
    }

    public void handleCommand(String elementID, String command)
    {
	SchematicViewElement element = getViewElement(elementID);
	if(element != null){
	    element.getSchematicFigure().handleCommand(command);
	}
    }

    public void handleStoredProperties(java.util.List propertyList)
    {
    }

    public boolean isSelectionEmpty()
    {
	return selectedElements.isEmpty();
    }

    public void propertyChanged(PropertyInfo info)
    {
    }

    public void selectBoundedElements(Rectangle bounds)
    {
	Iterator iterator = viewElements.values().iterator();

	while(iterator.hasNext()){
	    SchematicViewElement element = 
		(SchematicViewElement)iterator.next(); 
	    SchematicFigure figure = element.getSchematicFigure();

	    if(figure != null){
		if(bounds.contains(figure.getBounds())){
		    if(figure instanceof SchematicNodeFigure){
			figure.setSelected(true);
			figure.repaint();
		    
			if(tree != null){
			    DefaultMutableTreeNode treeNode = 
				element.getTreeNode();
			    TreePath path = new TreePath(treeNode.getPath());
			    tree.addSelectionPath(path);
			    tree.scrollPathToVisible(path);
			}
			selectedElements.add(element);
		    }
		}
	    }
	}
    }

    public void setSelectedElement(String name)
    {
	if(name != null){
	    SchematicViewElement element = 
		(SchematicViewElement)viewElements.get(name);

	    if(element == null){
		deselectElements();
		selectedElements.clear();
		return;
	    }
	    
	    SchematicFigure figure = element.getSchematicFigure();

	    if(figure != null){
		deselectElements();
		selectedElements.clear();
		selectedElements.add(element);

		figure.setSelected(true);
		figure.repaint();
	    }
	    else{
		deselectElements();
		selectedElements.clear();
		selectedElements.add(element);
	    }
	    
	    if(tree != null){
		DefaultMutableTreeNode treeNode = element.getTreeNode();
		TreePath path = new TreePath(treeNode.getPath());
		tree.setSelectionPath(path);
		tree.scrollPathToVisible(path);
	    }
	}
	else{
	    deselectElements();
	    selectedElements.clear();
	}
    }

    public void refresh()
    {
    }

    public void scrollToElement(SchematicViewElement element)
    {
	SchematicFigure figure = element.getSchematicFigure();

	if(figure != null){
	    JViewport viewPort = scrollPane.getViewport();
	    Rectangle viewBounds = viewPort.getViewRect();
	    Rectangle figureBounds = figure.getBounds();
	    
	    if(!viewBounds.intersects(figureBounds)){
		// we'll try to center the figure on the screen
		int width = viewBounds.width;
		int height = viewBounds.height;
		int xCoord = figureBounds.x - (width/2) + 
		    (figureBounds.width/2);
		int yCoord = figureBounds.y - (width/2) + 
		    (figureBounds.height/2);
		
		
		if(xCoord < 0){
		    xCoord = (int)(figureBounds.x/2);
		}
		if(yCoord < 0){
		    yCoord = (int)(figureBounds.y/2);
		}

		float scaleFactor = canvas.getScaleFactor();
		if(scaleFactor != 1.0f){
		    xCoord = (int)(xCoord*scaleFactor);
		    yCoord = (int)(yCoord*scaleFactor);
		}
		viewPort.setViewPosition(new Point(xCoord, yCoord)); 
	    }
	}
    }

    public void update()
    {

    }

    public void snapDraggedSelections()
    {
	Rectangle repaintRegion = null;

	for(int i = 0; i < selectedElements.size(); i++){
	    SchematicViewElement element = 
		(SchematicViewElement)selectedElements.get(i);
	    
	    SchematicFigure figure = element.getSchematicFigure();

	    if(repaintRegion == null)
		repaintRegion = figure.getPaintBounds();
	    else
		repaintRegion = 
		    repaintRegion.createUnion(figure.getPaintBounds()).
		    getBounds();

	    figure.snapToGrid();
	    repaintRegion = 
		repaintRegion.createUnion(figure.getPaintBounds()).
		getBounds();
	    canvas.repaint(repaintRegion);
	}
    }

    public void storeProperties()
    {
	propertyManager.storeProperties();

	Iterator iterator = viewElements.values().iterator();
	while(iterator.hasNext()){
	    SchematicViewElement element = 
		(SchematicViewElement)iterator.next();
	    SchematicFigure figure = element.getSchematicFigure();

	    if(figure != null && figure instanceof PropertyHolder)
		((PropertyHolder)figure).storeProperties();
	}
    }

    public void updateCachedProperties()
    {
    }
}
