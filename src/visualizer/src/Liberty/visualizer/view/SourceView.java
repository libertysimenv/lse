package Liberty.visualizer.view;

import Liberty.visualizer.document.*;
import Liberty.visualizer.editor.*;
import Liberty.visualizer.library.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.util.prefs.*;

public class SourceView extends View{
    /* the document that this frame provides a view of */
    private Document document;
    /* the EditorPanel */
    private EditorPanel editorPanel;

    public SourceView(Document doc)
    {
	this.document = doc;

	if(doc.getFile() == null)
	    editorPanel = new EditorPanel();
	else
	    editorPanel = new EditorPanel(doc.getFile());

	initializeToolbar();
	getContentPane().add(editorPanel, BorderLayout.CENTER);

	ImageIcon logo = IconStore.getIcon("libertyFlameSmallIcon");

// 	if(logo != null)
// 	    setIconImage(logo.getImage());

	Preferences prefs = 
	    Preferences.userNodeForPackage(getClass());

	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	int xCoord = Math.round(0.5f*screen.width);
	int yCoord = 0;
	int width = Math.round(0.45f*screen.width);
	int height = Math.round(0.45f*screen.height);

	try{
	    xCoord = prefs.getInt(doc.getName()+".xCoord", xCoord);
	    yCoord = prefs.getInt(doc.getName()+".yCoord", yCoord);
	    width = prefs.getInt(doc.getName()+".width", width);
	    height = prefs.getInt(doc.getName()+".height", height);
	}
	catch(NullPointerException npe){
	    // do nothing, defaults have already been set, just suppress the
	    // message on stderr
	}

	addWindowListener(new WindowAdapter(){
		public void windowClosing(WindowEvent we){
		    try{
			SourceView.this.storePreferences();
		    }
		    catch(Exception e){}
		}
	    });

	setBounds(xCoord, yCoord, width, height);

    }

    public void storePreferences()
    {
	String docName = document.getName();
	Rectangle bounds = getBounds();

	try{

	    Preferences prefs = 
		Preferences.userNodeForPackage(getClass());
	
	    prefs.putInt(docName+".xCoord", bounds.x);
	    prefs.putInt(docName+".yCoord", bounds.y);
	    prefs.putInt(docName+".width", bounds.width);
	    prefs.putInt(docName+".height", bounds.height);
	    
	    prefs.flush();
	}
	catch(BackingStoreException bse){
	    // do nothing, if the preferences can't be stored it's
	    // not a big deal
	}
	catch(Exception e){}
    }

    public SourceView(String source)
    {
	this.document = null;

	editorPanel = new EditorPanel(source);
	getContentPane().add(editorPanel, BorderLayout.CENTER);

	ImageIcon logo = IconStore.getIcon("libertyFlameSmallIcon");

	if(logo != null)
	    setIconImage(logo.getImage());

    }

    public SourceView(File file, int lineNumber)
    {
	setTitle(file.getName());

	if(file.exists()){
	    editorPanel = new EditorPanel(file, lineNumber);
	}
	else
	    editorPanel = new EditorPanel();

	getContentPane().add(editorPanel, BorderLayout.CENTER);

	ImageIcon logo = IconStore.getIcon("libertyFlameSmallIcon");

	if(logo != null)
	    setIconImage(logo.getImage());
    }

    public Document getDocument()
    {
	return document;
    }

    public String getText()
    {
	return editorPanel.getText();
    }

    private void initializeToolbar()
    {
	JToolBar toolbar = new JToolBar();
	getContentPane().add(toolbar, BorderLayout.NORTH);

	JButton saveButton = new JButton(VisualizerActions.getSaveAction());
	saveButton.setText("");
	saveButton.setToolTipText("save this description");
	saveButton.setRolloverIcon(IconStore.getIcon("saveOnIcon"));
	saveButton.setRolloverEnabled(true);
	saveButton.setBorderPainted(false);
	toolbar.add(saveButton);

	JButton schematicButton = 
	    new JButton(VisualizerActions.getShowSchematicAction());
	schematicButton.setText("");
	schematicButton.setToolTipText("view schematic");
	schematicButton.setRolloverIcon(IconStore.getIcon("schematicOnIcon"));
	schematicButton.setRolloverEnabled(true);
	schematicButton.setBorderPainted(false);
	toolbar.add(schematicButton);

	JButton buildButton = new JButton(VisualizerActions.getLSSBuildAction());
	buildButton.setText("");
	buildButton.setToolTipText("build this description");
	buildButton.setRolloverIcon(IconStore.getIcon("compileOnIcon"));
	buildButton.setRolloverEnabled(true);
	buildButton.setBorderPainted(false);
	toolbar.add(buildButton);

	JButton executeButton = 
	    new JButton(VisualizerActions.getLSEExecuteAction());
	executeButton.setText("");
	executeButton.setToolTipText("run simulation");
	executeButton.setRolloverIcon(IconStore.getIcon("executeOnIcon"));
	executeButton.setRolloverEnabled(true);
	executeButton.setBorderPainted(false);
	toolbar.add(executeButton);

// 	toolbar.addSeparator(new Dimension(16, 16));

// 	JButton cutButton = new JButton(VisualizerActions.getCutAction());
// 	cutButton.setText("");
// 	cutButton.setToolTipText("cut");
// 	cutButton.setRolloverIcon(IconStore.getIcon("cutOnIcon"));
// 	cutButton.setRolloverEnabled(true);
// 	cutButton.setBorderPainted(false);
// 	toolbar.add(cutButton);

// 	JButton copyButton = new JButton(VisualizerActions.getCopyAction());
// 	copyButton.setText("");
// 	copyButton.setToolTipText("copy");
// 	copyButton.setRolloverIcon(IconStore.getIcon("copyOnIcon"));
// 	copyButton.setRolloverEnabled(true);
// 	copyButton.setBorderPainted(false);
// 	toolbar.add(copyButton);

// 	JButton pasteButton = new JButton(VisualizerActions.getPasteAction());
// 	pasteButton.setText("");
// 	pasteButton.setToolTipText("paste");
// 	pasteButton.setRolloverIcon(IconStore.getIcon("pasteOnIcon"));
// 	pasteButton.setRolloverEnabled(true);
// 	pasteButton.setBorderPainted(false);
// 	toolbar.add(pasteButton);

// 	JButton searchButton = 
// 	    new JButton(VisualizerActions.getSearchAction());
// 	searchButton.setText("");
// 	searchButton.setToolTipText("search");
// 	searchButton.setRolloverIcon(IconStore.getIcon("searchOnIcon"));
// 	searchButton.setRolloverEnabled(true);		
// 	searchButton.setBorderPainted(false);
// 	toolbar.add(searchButton);

// 	JButton replaceButton = 
// 	    new JButton(VisualizerActions.getReplaceAction());
// 	replaceButton.setText("");
// 	replaceButton.setToolTipText("search and replace");
// 	replaceButton.setRolloverIcon(IconStore.getIcon("replaceOnIcon"));
// 	replaceButton.setRolloverEnabled(true);
// 	replaceButton.setBorderPainted(false);
// 	toolbar.add(replaceButton);
    }


    public void update()
    {

    }

}
