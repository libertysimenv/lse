package Liberty.visualizer.view;

import Liberty.visualizer.document.*;

import javax.swing.JFrame;

public abstract class View extends JFrame{

    public abstract Document getDocument();

    public abstract void update();

    public abstract void storePreferences();
}
