package Liberty.visualizer.view;

import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.util.*;
import Liberty.visualizer.UI.dialog.*;

import java.awt.*;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

public class SchematicViewElement{
    /** */
    protected SchematicViewElement parent;
    /** the name used to refer to this element; must be unique */
    protected String name;
    /** the label given to this element (this will appear in tooltips,
	on tree nodes, etc) */
    protected String label;
    /** the figure that this view's canvas will render; may be null*/
    protected SchematicFigure schematicFigure;
    /** the node that will be displayed in this view's tree */
    protected DefaultMutableTreeNode treeNode;
    /** the semantic value of this view element*/
    protected Object semanticValue;
    /** */
    protected SchematicView schematicView;

    public SchematicViewElement(String name,
				String label,
				SchematicView view,
				SchematicViewElement parent,
				Object semanticValue)
    {
	this.name = name;
// 	if(semanticValue instanceof Liberty.LSS.IMR.Instance)
// 	    System.out.println("4.1.1.Y "+name+" "+label+" "+view+" "+parent+" ")
		;
	this.label = label;
	this.treeNode = new DefaultMutableTreeNode(this);
// 	if(semanticValue instanceof Liberty.LSS.IMR.Instance)
// 	    System.out.println("4.1.1.Y "+view.getBounds());
	if(parent != null){
	    DefaultMutableTreeNode parentNode = parent.getTreeNode();
	    if(parentNode != null)
		parentNode.add(treeNode);

	}
// 	if(semanticValue instanceof Liberty.LSS.IMR.Instance)
// 	    System.out.println("4.1.1.Y "+view.getBounds());
	this.parent = parent;
	this.semanticValue = semanticValue;
	this.schematicView = view;
// 	if(semanticValue instanceof Liberty.LSS.IMR.Instance)
// 	    System.out.println("4.1.1.Y "+view.getBounds());
    }

    public ImageIcon getIcon()
    {
	return null;
    }

    public String getName()
    {
	return name;
    }

    public String getLabel()
    {
	return label;
    }

    public SchematicFigure getSchematicFigure()
    {
	return schematicFigure;
    }

    public Object getSemanticValue()
    {
	return semanticValue;
    }

    public DefaultMutableTreeNode getTreeNode()
    {
	return treeNode;
    }

    public void setSelected()
    {
	
    }

    public void doDialog()
    {
	LSSInfoDialog.showDialog(null, semanticValue);
    }

    public void doPopup(Component component, int x, int y)
    {

    }

    public void setSchematicFigure(SchematicFigure figure)
    {
	this.schematicFigure = figure;
    }

    public String toString()
    {
	return label;
    }
}
