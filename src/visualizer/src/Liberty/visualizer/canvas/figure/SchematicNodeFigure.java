package Liberty.visualizer.canvas.figure;

import java.awt.Point;
import java.util.Collection;

public interface SchematicNodeFigure extends SchematicFigure{

    public void addInEdge(SchematicEdgeFigure edge);

    public void addOutEdge(SchematicEdgeFigure edge);

    public Collection getEdges();

    public Collection getInEdges();

    public Point getDestConnectLocation(SchematicEdgeFigure edge);

    public Point getSourceConnectLocation(SchematicEdgeFigure edge);

    public int getDestConnectSite(SchematicEdgeFigure edge);

    public int getSourceConnectSite(SchematicEdgeFigure edge);

    public Collection getOutEdges();

}
