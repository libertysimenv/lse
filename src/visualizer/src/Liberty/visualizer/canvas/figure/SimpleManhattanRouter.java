package Liberty.visualizer.canvas.figure;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.lse.*;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.SwingConstants;
import java.util.*;

public class SimpleManhattanRouter implements javax.swing.SwingConstants{

    public static void buildConnectionFigure(PluggableConnectionFigure conn,
					     SchematicCanvas canvas)
    {
	SchematicNodeFigure srcFigure = conn.getSrcFigure();
	SchematicNodeFigure destFigure = conn.getDestFigure();
	Rectangle srcFigureBounds = srcFigure.getBounds();
	Rectangle destFigureBounds = destFigure.getBounds();
	Point srcConnectPoint = srcFigure.getSourceConnectLocation(conn);
	Point destConnectPoint = destFigure.getDestConnectLocation(conn);
	int srcConnectSite = srcFigure.getSourceConnectSite(conn);
	int destConnectSite = destFigure.getDestConnectSite(conn);

	SchematicFigure srcJogPoint, destJogPoint;
	SegmentFigure segmentFigure;

	int xCoord, yCoord, offset;

	int dx = destConnectPoint.x - srcConnectPoint.x;
	int dy = destConnectPoint.y - srcConnectPoint.y;

	if(srcConnectSite == CENTER){
	    if(Math.abs(dy) > Math.abs(dx)){
		if(dy > 0)
		    srcConnectSite = SOUTH;
		else
		    srcConnectSite = NORTH;
	    }
	    else{
		if(dx > 0)
		    srcConnectSite = EAST;
		else
		    srcConnectSite = WEST;
	    }
	}
	if(destConnectSite == CENTER){
	    if(Math.abs(dy) > Math.abs(dx)){
		if(dy > 0)
		    destConnectSite = NORTH;
		else
		    srcConnectSite = SOUTH;
	    }
	    else{
		if(dx > 0)
		    destConnectSite = WEST;
		else
		    destConnectSite = EAST;
	    }
	}

	SegmentFigure[] segments = conn.getSegementArray();

	switch(srcConnectSite){
	case NORTH:
	    offset = (int)((srcConnectPoint.x - srcFigureBounds.x)/2);
	    yCoord = srcConnectPoint.y - offset;
	    switch(destConnectSite){
	    case NORTH:
		for(int i = 0; i < 3; i++){
		    segments[i].setVisible(true);
		}
		segments[3].setVisible(false);
		segments[4].setVisible(false);

		yCoord = srcConnectPoint.y < destConnectPoint.y ?
		    srcConnectPoint.y - offset : destConnectPoint.y - offset;

		// head upwards
		segments[0].getSrcFigure().
		    setLocation(srcConnectPoint.x,
				srcConnectPoint.y);

		segments[0].getDestFigure().
		    setLocation(srcConnectPoint.x,
				yCoord);


		// head left or right
		segments[1].getDestFigure().
		    setLocation(destConnectPoint.x,
				yCoord);
		
		// finish up
		segments[2].getDestFigure().
		    setLocation(destConnectPoint.x,
				destConnectPoint.y);
		break;
	    case SOUTH:
		if(srcFigure == destFigure){
		    for(int i = 0; i < 5; i++){
			segments[i].setVisible(true);
		    }
		    
		    yCoord = srcFigureBounds.y +
			srcFigureBounds.height + 20 + offset;
		    xCoord = srcConnectPoint.x - 20 - offset;
		    
		    segments[0].getDestFigure().
			setLocation(srcConnectPoint.x, yCoord);

		    xCoord = destConnectPoint.x - 20 - offset;
		    
		    // head left
		    segments[1].getDestFigure().
			setLocation(xCoord, yCoord);
		    
		    yCoord = srcFigureBounds.y - 20 - offset;

		    // head up
		    segments[2].getDestFigure().
			setLocation(xCoord, yCoord);
		    
		    segments[3].getDestFigure().
			setLocation(destConnectPoint.x, yCoord);
		    
		    segments[4].getDestFigure().
			setLocation(destConnectPoint.x, destConnectPoint.y);
	    }
	    else if(srcConnectPoint.x == destConnectPoint.x &&
		   srcConnectPoint.y > destConnectPoint.y){
		    // we only need a single segment
		    for(int i = 1; i < 5; i++){
			segments[i].setVisible(false);
		    }
		    segments[0].setVisible(true);

		    segments[0].getSrcFigure().
			setLocation(srcConnectPoint.x,
				    srcConnectPoint.y);

		    segments[0].getDestFigure().
			setLocation(destConnectPoint.x,
				    destConnectPoint.y);
		}
		else{
		    if(dx > 0){
			if(dy > 0){
			    for(int i = 0; i < 5; i++){
				segments[i].setVisible(true);
			    }

			    // head upwards
			    segments[0].getSrcFigure().
				setLocation(srcConnectPoint.x,
					    srcConnectPoint.y);
			    
			    segments[0].getDestFigure().
				setLocation(srcConnectPoint.x,
					    yCoord);


			    // move right			    
			    xCoord = srcFigureBounds.x + 
				srcFigureBounds.width + 20 + offset;

			    segments[1].getDestFigure().
				setLocation(xCoord, yCoord);

			    // move down
			    yCoord = destConnectPoint.y + offset;

			    segments[2].getDestFigure().
				setLocation(xCoord, yCoord);

			    // move right
			    segments[3].getDestFigure().
				setLocation(destConnectPoint.x, yCoord);

			    // finish up
			    segments[4].getDestFigure().
				setLocation(destConnectPoint.x, 
					    destConnectPoint.y);

			}
			else{
			    // head upwards
			    for(int i = 0; i < 3; i++){
				segments[i].setVisible(true);
			    }
			    segments[3].setVisible(false);
			    segments[4].setVisible(false);

			    // head upwards
			    segments[0].getSrcFigure().
				setLocation(srcConnectPoint.x,
					    srcConnectPoint.y);
			    
			    segments[0].getDestFigure().
				setLocation(srcConnectPoint.x,
					    yCoord);

			    // move right
			    segments[1].getDestFigure().
				setLocation(destConnectPoint.x,
					    yCoord);

			    
			    // finish up
			    segments[2].getDestFigure().
				setLocation(destConnectPoint.x,
					    destConnectPoint.y);
			}
		    }
		    else{
			if(dy > 0){
			    for(int i = 0; i < 5; i++){
				segments[i].setVisible(true);
			    }

			    // head upwards
			    segments[0].getSrcFigure().
				setLocation(srcConnectPoint.x,
					    srcConnectPoint.y);
			    
			    segments[0].getDestFigure().
				setLocation(srcConnectPoint.x,
					    yCoord);

			    // move right
			    xCoord = srcFigureBounds.x - 20 - offset;

			    segments[1].getDestFigure().setLocation(xCoord,
								    yCoord);
			    
			    // move down
			    yCoord = destConnectPoint.y + offset;

			    segments[2].getDestFigure().setLocation(xCoord,
								    yCoord);
			    
			    // move right
			    segments[3].getDestFigure().
				setLocation(destConnectPoint.x,
					    yCoord);

			    // finish up
			    segments[4].getDestFigure().
				setLocation(destConnectPoint.x,
					    destConnectPoint.y);	
			}
			else{
			    for(int i = 0; i < 3; i++){
				segments[i].setVisible(true);
			    }
			    segments[3].setVisible(false);
			    segments[4].setVisible(false);

			    // head upwards
			    segments[0].getSrcFigure().
				setLocation(srcConnectPoint.x,
					    srcConnectPoint.y);
			    
			    segments[0].getDestFigure().
				setLocation(srcConnectPoint.x,
					    yCoord);

			    // move right
			    segments[1].getDestFigure().
				setLocation(destConnectPoint.x,
					    yCoord);
			    
			    // finish up
			    segments[2].getDestFigure().
				setLocation(destConnectPoint.x,
					    destConnectPoint.y);
			}
		    }
		}
		break;
	    case EAST:
		if(dx > 0){
		    if(dy > 0){
			yCoord = srcConnectPoint.y - offset;

			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			// head upwards
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);
			
			// head right
			xCoord = destConnectPoint.x + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head down
			segments[2].getDestFigure().
			    setLocation(xCoord, destConnectPoint.y);

			// finish up
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		    else{
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			// head upwards
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);

			// head right
			xCoord = destConnectPoint.x + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head up
			segments[2].getDestFigure().
			    setLocation(xCoord, destConnectPoint.y);

			// head left
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x, 
					destConnectPoint.y);
		    }		    
		}
		else{
		    if(dy > 0){
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

      			// head up
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);
			
			// head left
			xCoord = destConnectPoint.x + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord,
					yCoord);

			// head down
			segments[2].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);

			// head left
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		    else{
			for(int i = 2; i < 4; i++){
			    segments[i].setVisible(false);
			}
			segments[0].setVisible(true);
			segments[1].setVisible(true);

			// head up
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					destConnectPoint.y);

			// head left
			segments[1].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		}
		break;
	    case WEST:
		if(dx > 0){
		    if(dy > 0){
			yCoord = srcConnectPoint.y - offset;

			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			// head upwards
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);
			
			// head right
			xCoord = srcFigureBounds.x + srcFigureBounds.width +
			    offset;

			segments[1].getDestFigure().
			    setLocation(xCoord,
					yCoord);


			// head down
			segments[2].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);

			// finish up
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);

		    }
		    else{
			yCoord = srcConnectPoint.y - offset;

			for(int i = 2; i < 5; i++){
			    segments[i].setVisible(false);
			}
			segments[0].setVisible(true);
			segments[1].setVisible(true);

			// head upwards
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					destConnectPoint.y);

			// head right
			segments[1].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		    
		}
		else{
		    if(dy > 0){
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			// head upwards
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);

			// head left
			xCoord = destConnectPoint.x - offset;

			segments[1].getDestFigure().
			    setLocation(xCoord,
					yCoord);

			// head down
			xCoord = destConnectPoint.x - offset;

			segments[2].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);

			// head right
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x, 
					destConnectPoint.y);

		    }
		    else{
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			// head up
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);

			// head left
			xCoord = destConnectPoint.x - offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head up
			xCoord = destConnectPoint.x - offset;

			segments[2].getDestFigure().
			    setLocation(xCoord, destConnectPoint.y);

			// head right
			xCoord = destConnectPoint.x - offset;

			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x, 
					destConnectPoint.y);			
		    }
		}
		break;
	    }
	    break;
	case SOUTH:
	    offset = (int)((srcConnectPoint.x - srcFigureBounds.x)/2);
	    yCoord = srcConnectPoint.y + offset;
	    switch(destConnectSite){
	    case NORTH:
		if(srcConnectPoint.x == destConnectPoint.x &&
		   destConnectPoint.y > srcConnectPoint.y){
		    // we only need a single segment
		    for(int i = 1; i < 5; i++){
			segments[i].setVisible(false);
		    }
		    segments[0].setVisible(true);

		    segments[0].getSrcFigure().
			setLocation(srcConnectPoint.x,
				    srcConnectPoint.y);
		    
		    segments[0].getDestFigure().
			setLocation(destConnectPoint.x,
				    destConnectPoint.y);
		    
		}
		else{
		    if(dx > 0){
			if(dy > 0){
			    for(int i = 0; i < 3; i++){
				segments[i].setVisible(true);
			    }
			    segments[3].setVisible(false);
			    segments[4].setVisible(false);

			    // head down
			    segments[0].getSrcFigure().
				setLocation(srcConnectPoint.x,
					    srcConnectPoint.y);
			    
			    segments[0].getDestFigure().
				setLocation(srcConnectPoint.x,
					    yCoord);

			    // move right
			    segments[1].getDestFigure().
				setLocation(destConnectPoint.x,
					    yCoord);

			    // move down
			    segments[2].getDestFigure().
				setLocation(destConnectPoint.x,
					    destConnectPoint.y);
			}
			else{
			    for(int i = 0; i < 5; i++){
				segments[i].setVisible(true);
			    }

			    // head down
			    segments[0].getSrcFigure().
				setLocation(srcConnectPoint.x,
					    srcConnectPoint.y);
			    
			    segments[0].getDestFigure().
				setLocation(srcConnectPoint.x,
					    yCoord);

			    // move right
			    xCoord = srcFigureBounds.x + 
				srcFigureBounds.width + offset;

			    segments[1].getDestFigure().
				setLocation(xCoord, yCoord);
			    
			    // move up
			    yCoord = destFigureBounds.y - offset;

			    segments[2].getDestFigure().
				setLocation(xCoord, yCoord);

			    // move right
			    segments[3].getDestFigure().
				setLocation(destConnectPoint.x, yCoord);

			    // move down
			    segments[4].getDestFigure().
				setLocation(destConnectPoint.x, 
					    destConnectPoint.y);
			}
		    }
		    else{
			if(dy > 0){
			    for(int i = 0; i < 3; i++){
				segments[i].setVisible(true);
			    }
			    segments[3].setVisible(false);
			    segments[4].setVisible(false);

			    // head down
			    segments[0].getSrcFigure().
				setLocation(srcConnectPoint.x,
					    srcConnectPoint.y);
			    
			    segments[0].getDestFigure().
				setLocation(srcConnectPoint.x,
					    yCoord);

			    // move left
			    segments[1].getDestFigure().
				setLocation(destConnectPoint.x,
					    yCoord);

			    // move down
			    segments[2].getDestFigure().
				setLocation(destConnectPoint.x,
					    destConnectPoint.y);

			}
			else{
			    for(int i = 0; i < 5; i++){
				segments[i].setVisible(true);
			    }

			    // head down
			    segments[0].getSrcFigure().
				setLocation(srcConnectPoint.x,
					    srcConnectPoint.y);
			    
			    segments[0].getDestFigure().
				setLocation(srcConnectPoint.x,
					    yCoord);

			    // move left
			    xCoord = srcFigureBounds.x - offset;

			    segments[1].getDestFigure().
				setLocation(xCoord,
					    yCoord);


			    // go up
			    yCoord = destConnectPoint.y - offset;

			    segments[2].getDestFigure().
				setLocation(xCoord, yCoord);

			    // go left
			    segments[3].getDestFigure().
				setLocation(destConnectPoint.x,
					    yCoord);

			    // go down
			    segments[4].getDestFigure().
				setLocation(destConnectPoint.x,
					    destConnectPoint.y);
			}
		    }
		}
	    break;
	    case SOUTH:
		if(dy > 0){
		    yCoord = destConnectPoint.y + offset;

		    for(int i = 0; i < 3; i++){
			segments[i].setVisible(true);
		    }
		    segments[3].setVisible(false);
		    segments[4].setVisible(false);
		    
		    // head down
		    segments[0].getSrcFigure().
			setLocation(srcConnectPoint.x,
				    srcConnectPoint.y);
		    
		    segments[0].getDestFigure().
			setLocation(srcConnectPoint.x,
				    yCoord);

		    // head left or right
		    segments[1].getDestFigure().
			setLocation(destConnectPoint.x,
				    yCoord);

		    // head up
		    segments[2].getDestFigure().
			setLocation(destConnectPoint.x,
				    destConnectPoint.y);

		}
		else{
		    for(int i = 0; i < 3; i++){
			segments[i].setVisible(true);
		    }
		    segments[3].setVisible(false);
		    segments[4].setVisible(false);
		    
		    // head down
		    segments[0].getSrcFigure().
			setLocation(srcConnectPoint.x,
				    srcConnectPoint.y);
		    
		    segments[0].getDestFigure().
			setLocation(srcConnectPoint.x,
				    yCoord);

		    // head left or right
		    segments[1].getDestFigure().
			setLocation(destConnectPoint.x,
				    yCoord);

		    // finish up
		    segments[2].getDestFigure().
			setLocation(destConnectPoint.x,
				    destConnectPoint.y);
		}
		break;
   	    case EAST:
		if(dx > 0){
		    if(dy > 0){
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);
			
			// head down
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);

			// head right
			xCoord = destConnectPoint.x + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord,
					yCoord);

			// head down
			segments[2].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);

			// head left
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		    else{
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);
		    
			// head down
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);

			// head right
			xCoord = destConnectPoint.x + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord,
					yCoord);

			// head up
			segments[2].getDestFigure().
				setLocation(xCoord,
					    destConnectPoint.y);

			// head left
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		}
		else{
		    if(dy < 0){
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);
			
			// head down
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);

			// go left
			xCoord = srcFigureBounds.x - offset;
			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// go up
			segments[2].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);

			// go left
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);

		    }
		    else{
			for(int i = 2; i < 5; i++){
			    segments[i].setVisible(false);
			}
			segments[0].setVisible(true);
			segments[1].setVisible(true);
			
			// head down
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);

			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					destConnectPoint.y);


			// go left
			segments[1].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		}
		break;
	    case WEST:
		if(dx > 0){
		    if(dy > 0){
			for(int i = 2; i < 5; i++){
			    segments[i].setVisible(false);
			}
			segments[0].setVisible(true);
			segments[1].setVisible(true);
		    
			// head down
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
		    
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					destConnectPoint.y);

			// head right
			segments[1].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		    else{
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);
		    
			// head down
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);


			// head right
			xCoord = srcFigureBounds.x + 
			    srcFigureBounds.width + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head up
			segments[2].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);
			
			// head right
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		}
		else{
		    if(dy > 0){
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);
			
			// head down
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);
			
			// head left
			xCoord = destFigureBounds.x - offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head down
			segments[2].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);

			// head right
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);

		    }
		    else{
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);
			
			// head down
			segments[0].getSrcFigure().
			    setLocation(srcConnectPoint.x,
					srcConnectPoint.y);
			
			segments[0].getDestFigure().
			    setLocation(srcConnectPoint.x,
					yCoord);

			// head left
			xCoord = destFigureBounds.x - offset;
			
			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head up
			segments[2].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);

			// head right
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		}
		break;
	    }

	    break;
	case WEST:
	    offset = srcConnectPoint.y - srcFigureBounds.y;

	    xCoord = (dx > 0) ? srcConnectPoint.x - offset : 
		destFigureBounds.x - offset;

	    // head west
	    segments[0].getSrcFigure().
		setLocation(srcConnectPoint.x,
			    srcConnectPoint.y);

	    segments[0].getDestFigure().
		setLocation(xCoord, srcConnectPoint.y);


	    switch(destConnectSite){
	    case NORTH:
		if(dx > 0){
		    for(int i = 0; i < 4; i++){
			segments[i].setVisible(true);
		    }
		    segments[4].setVisible(false);

		    // head north or south
		    yCoord = destConnectPoint.y - 20 - offset;

		    segments[1].getDestFigure().
			setLocation(xCoord, yCoord);

		    // head east
		    segments[2].getDestFigure().
			setLocation(destConnectPoint.x, yCoord);

		    // head south
		    segments[3].getDestFigure().
			setLocation(destConnectPoint.x, destConnectPoint.y);
		    
		}
		else{
		    if(dy > 0){
			for(int i = 2; i < 4; i++){
			    segments[i].setVisible(false);
			}
			segments[0].setVisible(true);
			segments[1].setVisible(true);
			
			// head west
			segments[0].getDestFigure().
			    setLocation(destConnectPoint.x,
					srcConnectPoint.y);

			// head south
			segments[1].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);

		    }
		    else{
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			yCoord = destConnectPoint.y - offset;

			// head north
			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head west
			segments[2].getDestFigure().
			    setLocation(destConnectPoint.x, yCoord);

			// head south
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		}

		break;
	    case SOUTH:
		if(dx > 0){
		    for(int i = 0; i < 4; i++){
			segments[i].setVisible(true);
		    }
		    segments[4].setVisible(false);

		    // head north or south
		    yCoord = destConnectPoint.y + offset;

		    segments[1].getDestFigure().
			setLocation(xCoord, yCoord);
		    
		    // head east
		    segments[2].getDestFigure().
			setLocation(destConnectPoint.x, yCoord);

		    // head south
		    segments[3].getDestFigure().
			setLocation(destConnectPoint.x,
				    destConnectPoint.y);

		}
		else{
		    if(dy < 0){
			for(int i = 2; i < 4; i++){
			    segments[i].setVisible(false);
			}
			segments[0].setVisible(true);
			segments[1].setVisible(true);


			// head west
			segments[0].getDestFigure().
			    setLocation(destConnectPoint.x,
					srcConnectPoint.y);

			// head north
			segments[1].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		    else{
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);
			
			// head south
			yCoord = destConnectPoint.y + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head west
			segments[2].getDestFigure().
			    setLocation(destConnectPoint.x, yCoord);
			
			// head north
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x, 
					destConnectPoint.y);

		    }
		}

		break;
	    case EAST:
		if(srcFigure == destFigure){
		    for(int i = 0; i < 5; i++){
			segments[i].setVisible(true);
		    }
		    
		    yCoord = srcFigureBounds.y - 20 - offset;
		    xCoord = srcConnectPoint.x + 20 + offset;

		    segments[0].getDestFigure().
			setLocation(xCoord, srcConnectPoint.y);

		    // head up
		    segments[1].getDestFigure().
			setLocation(xCoord, yCoord);
		    
		    xCoord = destConnectPoint.x - 20 - offset;

		    // last down
		    segments[2].getDestFigure().
			setLocation(xCoord, yCoord);

		    segments[3].getDestFigure().
			setLocation(xCoord, destConnectPoint.y);

		    segments[4].getDestFigure().
			setLocation(destConnectPoint.x, destConnectPoint.y);
		}
		else if(srcConnectPoint.y == destConnectPoint.y &&
		   srcConnectPoint.x > destConnectPoint.x){
		    // we only need a single segment
		    for(int i = 1; i < 4; i++){
			segments[i].setVisible(false);
		    }
		    segments[0].setVisible(true);

		    segments[0].getDestFigure().
			setLocation(destConnectPoint.x, destConnectPoint.y);
		}
		else{
		    if(dx > 0){
			for(int i = 0; i < 5; i++){
			    segments[i].setVisible(true);
			}

			yCoord = (dy > 0 ) ? srcFigureBounds.y + 
			    srcFigureBounds.height + offset : 
			    srcFigureBounds.y - offset;

			// head up or down
			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head east
			xCoord = destConnectPoint.x + offset;

			segments[2].getDestFigure().
			    setLocation(xCoord, yCoord);
			
			// head north or south
			segments[3].getDestFigure().
			    setLocation(xCoord, destConnectPoint.y);

			// head west
			segments[4].getDestFigure().
			    setLocation(destConnectPoint.x, 
					destConnectPoint.y);

		    }
		    else{
			for(int i = 0; i < 3; i++){
			    segments[i].setVisible(true);
			}
			segments[3].setVisible(false);
			segments[4].setVisible(false);

			xCoord = srcConnectPoint.x - offset;

			segments[0].getDestFigure().
			    setLocation(xCoord, srcConnectPoint.y);


			// head up or down
			segments[1].getDestFigure().
			    setLocation(xCoord, destConnectPoint.y);
			
			// head west
			segments[2].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
			
		    }
		}
		break;
	    case WEST:
		for(int i = 0; i < 3; i++){
		    segments[i].setVisible(true);
		}
		segments[3].setVisible(false);
		segments[4].setVisible(false);

		// head north or south
		segments[1].getDestFigure().
		    setLocation(xCoord, destConnectPoint.y);

		// head east
		segments[2].getDestFigure().
		    setLocation(destConnectPoint.x, destConnectPoint.y);

		break;
	    }
	    break;
	default: // EAST
	    offset = srcConnectPoint.y - srcFigureBounds.y;
	    xCoord = srcConnectPoint.x + 20 + offset;

	    // go east
	    segments[0].getSrcFigure().
		setLocation(srcConnectPoint.x,
			    srcConnectPoint.y);

	    segments[0].getDestFigure().
		setLocation(xCoord, srcConnectPoint.y);


	    switch(destConnectSite){
	    case EAST:
		for(int i = 0; i < 3; i++){
		    segments[i].setVisible(true);
		}
		segments[3].setVisible(false);
		segments[4].setVisible(false);

		xCoord = destConnectPoint.x > srcConnectPoint.x ?
		    destConnectPoint.x + offset : srcConnectPoint.x + offset;

		segments[0].getDestFigure().
		    setLocation(xCoord, 
				srcConnectPoint.y);
		    
		// go down
		segments[1].getDestFigure().
		    setLocation(xCoord,
				destConnectPoint.y);
		
		segments[2].getDestFigure().
		    setLocation(destConnectPoint.x, 
				destConnectPoint.y);

		break;
	    case WEST:
		if(srcConnectPoint.y == destConnectPoint.y &&
		   destConnectPoint.x > srcConnectPoint.x){
		    // we only need a single segment
		    segments[0].setVisible(true);
		    for(int i = 1; i < 5; i++){
			segments[i].setVisible(false);
		    }

		    segments[0].getSrcFigure().
			setLocation(srcConnectPoint.x,
				    srcConnectPoint.y);

		    segments[0].getDestFigure().
			setLocation(destConnectPoint.x,
				    destConnectPoint.y);

		    conn.validateBounds();
		}
		else{
		    if(dx - 20 > 0){ // the connection heads to the right
			for(int i = 0; i < 3; i++){
			    segments[i].setVisible(true);
			}
			segments[3].setVisible(false);
			segments[4].setVisible(false);
			
			// head up or down
			segments[1].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);
			
			// last leg
			segments[2].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);

		    }
		    else{ // we need to double back
			for(int i = 0; i < 5; i++){
			    segments[i].setVisible(true);
			}

			if(dy > 0){ // head south
			    yCoord = srcFigureBounds.y + 
				srcFigureBounds.height + 20 + offset;
			}
			else{ // head north
			    yCoord = srcFigureBounds.y - 20 - offset;
			}
			
			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);
			
			// head left
			xCoord = destConnectPoint.x - 20 - offset;
			segments[2].getDestFigure().
			    setLocation(xCoord, yCoord);
			
			// destConnectSite
			segments[3].getDestFigure().
			    setLocation(xCoord,
					destConnectPoint.y);
			
			// finish up
			segments[4].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		}
		break;
	    case SOUTH:
		if(dx > 0){
		    if(dy > 0){
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			// head down
			yCoord = destConnectPoint.y + 20 + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			// head right
			segments[2].getDestFigure().
			    setLocation(destConnectPoint.x, yCoord);

			// head north
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x, 
					destConnectPoint.y);
		    
		    }
		    else{
			for(int i = 2; i < 5; i++){
			    segments[i].setVisible(false);
			}
			segments[0].setVisible(true);
			segments[1].setVisible(true);

			segments[0].getDestFigure().
			    setLocation(destConnectPoint.x, 
					srcConnectPoint.y);
		    
			// head up
			segments[1].getDestFigure().
			    setLocation(destConnectPoint.x, 
					destConnectPoint.y);
		    }
		}
		else{
		    if(dy > 0){
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			yCoord = destConnectPoint.y + 20 + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			segments[2].getDestFigure().
			    setLocation(destConnectPoint.x, yCoord);

			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    
		    }
		    else{
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			// head up
			yCoord = destConnectPoint.y + 20 + offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);
		    
			// head left
			segments[2].getDestFigure().
			    setLocation(destConnectPoint.x, yCoord);

			// finish up
			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    }
		}
		break;
	    case NORTH:
		if(dx > 0){
		    if(dy < 0){
			for(int i = 0; i < 4; i++){
			    segments[i].setVisible(true);
			}
			segments[4].setVisible(false);

			// head up
			yCoord = destConnectPoint.y - 20 - offset;

			segments[1].getDestFigure().
			    setLocation(xCoord, yCoord);

			segments[2].getDestFigure().
			    setLocation(destConnectPoint.x, yCoord);

			segments[3].getDestFigure().
			    setLocation(destConnectPoint.x,
					destConnectPoint.y);
		    
		    }
		    else{
			for(int i = 0; i < 2; i++){
			    segments[i].setVisible(true);
			}
			segments[2].setVisible(false);
			segments[3].setVisible(false);
			segments[4].setVisible(false);

			segments[0].getDestFigure().
			    setLocation(destConnectPoint.x, 
					srcConnectPoint.y);
		    
			segments[1].getDestFigure().
			    setLocation(destConnectPoint.x, 
					destConnectPoint.y);
		    }
		}
		else{
		    for(int i = 0; i < 4; i++){
			segments[i].setVisible(true);
		    }
		    segments[4].setVisible(false);
		    
		    // head up
		    yCoord = destConnectPoint.y - 20 - offset;
		    
		    segments[1].getDestFigure().
			setLocation(xCoord, yCoord);
		    
		    segments[2].getDestFigure().
			setLocation(destConnectPoint.x, yCoord);
		    
		    
		    segments[3].getDestFigure().
			setLocation(destConnectPoint.x, 
				    destConnectPoint.y);
		}
		break;
	    }
	}
	conn.validateBounds();
    }
}
