package Liberty.visualizer.canvas.figure.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.dialog.*;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;

import javax.swing.*;

/**
 * The DefaultConnectionFigure is the default figure used to represent 
 * an LSE IMR Connection object visually on a SchematicCanvas.
 *
 * @see SchematicFigure
 * @see PropertyHolder
 * @see PluggableConnectionFigure
 *
 * @author Jason Blome
 * @version 0.1
 */

public class DefaultConnectionFigure extends PluggableConnectionFigure{

    /** should this connection be auto-routed */
    private boolean autoRoute;
    /** a list of this figure's Drawable objects */
    private LinkedHashMap drawables;
    /** a list of this figure's SchematicFigure objects */
    private LinkedHashMap figures;
    /** is this figure selected */
    private boolean isSelected;
    /** is this figure visible */
    private boolean isVisible;
    /** the name used to refer to this figure */
    private String name;
    /** the bounding rectangle for this connection */
    private Rectangle paintBounds;
    /** an array of segments for use by the manhattan router */
    private SegmentFigure[] segmentArray;
    /** the color used to draw this connection */
    private Color fillColor;
    /** the collection of line segments used to draw this figure if the 
	figure is not auto-routed */
    private ArrayList segments;
    /** the color used to draw this connection when it is selected */
    private Color selectedFillColor;    
    /** the currently selected segment if it exists */
    private SegmentFigure selectedSegment;
    /** the stroke used to draw the connection */
    private BasicStroke stroke;
    /** previous figure locations so that we know if we need to rebuild
	the connection when auto-routing */
    private Point srcConnectLocation, destConnectLocation;

    /**
     * Constructor for the DefaultConnectionFigure object used to represent 
     * IMR Connections objects on a SchematicCanvas.  Requires that all five
     * parameters be non-null.
     *
     * @param canvas the canvas that this GenericInstanceFigure
     *               will be rendered upon
     * @param parent the parent of this 
     *               GenericInstanceFigure, which may be any class
     *               that implements SchematicCanvasContainer
     * @param connection the IMR Connection object obtained from the LSE 
     *               compiler
     * @param srcFigure the srcFigure representing the source IMR Instance 
     *                  object
     * @param destFigure the srcFigure representing the destination IMR 
     *                  Instance object
     */
    public DefaultConnectionFigure(SchematicCanvas canvas,
				   SchematicCanvasContainer parent,
				   Connection connection,
				   SchematicNodeFigure srcFigure, 
				   SchematicNodeFigure destFigure)
    {
	super(canvas, parent, connection, srcFigure, destFigure);

	this.name = connection.toString();

	this.drawables = null;
	this.figures = null;
	this.isVisible = true;
	this.isSelected = false;
	this.stroke = new BasicStroke(1.0f);
	this.autoRoute = true;
	this.segments = new ArrayList();
	this.paintBounds = null;
	this.segmentArray = new SegmentFigure[5];
	this.fillColor = Color.black;
	this.selectedFillColor = new Color(0xe51d27);

	// initialize the segments array, assuming that this connection
	// will be auto-routed
	srcConnectLocation = srcFigure.getSourceConnectLocation(this);
	destConnectLocation = destFigure.getDestConnectLocation(this);

	JogPointFigure jogPointFigure1;
	JogPointFigure jogPointFigure2 = new JogPointFigure(canvas, this);

	for(int i = 0; i < 5; i++){
	    segmentArray[i] = new SegmentFigure(this, canvas);

	    jogPointFigure1 = jogPointFigure2;
	    jogPointFigure2 = new JogPointFigure(canvas, this);
	    segmentArray[i].setSourceFigure(jogPointFigure1);
	    segmentArray[i].setDestFigure(jogPointFigure2);
	}

	SimpleManhattanRouter.buildConnectionFigure(this, canvas);

	initializeProperties();
    }

    /**
     * Add the specified Drawable to the set of Drawable objects that this
     * figure is responsible for painting.
     *
     * @param drawable the Drawable to be added to this figure
     */
    public void addDrawable(Drawable drawable)
    {
	if(drawables == null){
	    drawables = new LinkedHashMap();
	}
	drawables.put(drawable.getName(), drawable);

	Rectangle bounds = drawable.getBounds();

	if(paintBounds == null)
	    paintBounds = bounds;
	else
	    paintBounds = paintBounds.createUnion(bounds).getBounds();
    }

    /**
     * Add the specified SchematicFigure to the set of SchematicFigure 
     * objects that this figure is responsible for painting.
     *
     * @param figure the SchematicFigure to be added to this figure
     */
    public void addFigure(SchematicFigure figure)
    {
	if(figure instanceof SegmentFigure){
	    segments.add(figure);
	}
	else{
	    if(figures == null)
		figures = new LinkedHashMap();
	    figures.put(figure.getFullyQualifiedName(), figure);
	}

	Rectangle bounds = figure.getPaintBounds();

	if(paintBounds == null)
	    paintBounds = bounds;
	else
	    paintBounds = paintBounds.createUnion(bounds).getBounds();
    }

    /**
     * Build a new JogPointFigure and position it at the specified coordinates.
     * Also build a new SegmentFigure and twiddle semgent endpoints 
     * accordingly.
     *
     * @param xCoord the x coordinate for the desired JogPointFigure
     * @param yCoord the y coordinate for the desired JogPointFigure
     */
    public void addJogPoint(int xCoord, int yCoord)
    {
	if(segments != null && !autoRoute){
	    SegmentFigure lastSegment = 
		(SegmentFigure)segments.get(segments.size()-1);
	    
	    addJogPoint(lastSegment, xCoord, yCoord);
	}
    }

    /**
     * Build a new JogPointFigure and positions it at the specified 
     * coordinates.  This function takes the segment parameter and
     * uses its destination figure as the new segment's source and
     * uses the newly created jog point as its destination.
     *
     * @param segment the segment that the JogPointFigure should lie on
     * @param xCoord the x coordinate for the desired JogPointFigure
     * @param yCoord the y coordinate for the desired JogPointFigure
     */    
    public void addJogPoint(SegmentFigure segment, int xCoord, int yCoord)
    {
	if(segments != null && !autoRoute){
	    SegmentFigure newSegment = new SegmentFigure(this, canvas);
	    JogPointFigure jogPoint = new JogPointFigure(canvas, this);

	    newSegment.setDestFigure(segment.getDestFigure());
	    newSegment.setSourceFigure(jogPoint);
	    newSegment.setFillColor(this.fillColor);
	    newSegment.setSelectedFillColor(this.selectedFillColor);
	    segment.setDestFigure(jogPoint);
	    jogPoint.setLocation(xCoord, yCoord);
	    int index = segments.indexOf(segment) + 1;

	    segments.add(index, newSegment);
	    paintBounds = 
	      paintBounds.createUnion(newSegment.getPaintBounds()).getBounds();
	}
    }

    /**
     * Make all segments in this connection perpendicular.
     *
     */
    public void cleanUpSegments()
    {
	if(!autoRoute){
	    Rectangle repaintRegion = getPaintBounds();
	    for(int i = 0; i < segments.size(); i++){
		SegmentFigure segment = (SegmentFigure)segments.get(i);
		
		SchematicFigure srcFigure = segment.getSrcFigure();
		SchematicFigure destFigure = segment.getDestFigure();
		if(!(destFigure instanceof PortInstanceFigure)){
		    Point srcLocation = srcFigure.getLocation();
		    Point destLocation = destFigure.getLocation();
		    int destX = destLocation.x;
		    int destY = destLocation.y;
		    
		    if(Math.abs(srcLocation.x - destLocation.x) <= 10)
			destX = srcLocation.x;
		    if(Math.abs(srcLocation.y - destLocation.y) <= 10)
			destY = srcLocation.y;
		    
		    destFigure.setLocation(destX, destY);
		}
		repaintRegion = repaintRegion.createUnion(getPaintBounds()).
		    getBounds();
		canvas.repaint(repaintRegion);
	    }
	}
    }

    /**
     * Remove all Drawable and SchematicFigure objects from this figure.
     *
     */
    public void clear()
    {
	if(drawables != null)
	    drawables.clear();
	if(figures != null)
	    figures.clear();
	segments.clear();
	this.paintBounds = null;
    }

    /**
     * Remove all JogPointFigure and the appropriate SegmentFigure objects 
     * from this figure.
     *
     */
    public void clearJogPoints()
    {
	if(!autoRoute){
	    SegmentFigure firstSegment = 
		(SegmentFigure)segments.get(0);
	    SegmentFigure lastSegment = 
		(SegmentFigure)segments.get(segments.size()-1);

	    segments.clear();
	    SegmentFigure newSegment = new SegmentFigure(this, canvas);
	    newSegment.setFillColor(this.fillColor);
	    newSegment.setSelectedFillColor(this.selectedFillColor);
	    newSegment.setSourceFigure(firstSegment.getSrcFigure());
	    newSegment.setDestFigure(lastSegment.getDestFigure());

	    segments.add(newSegment);
	}
    }

    /**
     * Determine if the supplied location is contained in this figure.
     *
     * @param x the x coordinate in question
     * @param y the y coordinate in question
     */
    public boolean contains(int x, int y)
    {
	if(autoRoute){
	    for(int i = 0; i < 5; i++){
		if(segmentArray[i].isVisible() && 
		   segmentArray[i].contains(x, y)){
		    return true;
		}
	    }
	}
	else{
	    for(int i = 0; i < segments.size(); i++){
		SegmentFigure segment = (SegmentFigure)segments.get(i);
		if(segment.contains(x, y))
		    return true;
	    }
	}
	return false;
    }

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	JPopupMenu popup = new JPopupMenu();

	PropertyInfo info = propertyManager.getProperty("Set Visible");
	if(info != null){
	    boolean isVisible = ((Boolean)info.getValue()).booleanValue();
	    final JCheckBoxMenuItem isVisibleItem = 
		new JCheckBoxMenuItem("Set Visible", isVisible);

	    isVisibleItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			PropertyInfo info = 
			    DefaultConnectionFigure.this.
			    propertyManager.getProperty("Set Visible");
			
			info.setValue(new Boolean(isVisibleItem.
						  getState()));
			DefaultConnectionFigure.this.propertyChanged(info);
		    }
		});
	    popup.add(isVisibleItem);
	}

	info = propertyManager.getProperty("Show Jog Points");
	if(info != null){
	    boolean showJogPoints = ((Boolean)info.getValue()).booleanValue();
	    final JCheckBoxMenuItem showJogPointsItem = 
		new JCheckBoxMenuItem("Show Jog Points", showJogPoints);

	    showJogPointsItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			PropertyInfo info = 
			    DefaultConnectionFigure.this.
			    propertyManager.getProperty("Show Jog Points");
			
			info.setValue(new Boolean(showJogPointsItem.
						  getState()));
			DefaultConnectionFigure.this.propertyChanged(info);
		    }
		});
	    popup.add(showJogPointsItem);
	}

	info = propertyManager.getProperty("Auto-Route Connection");
	if(info != null){
	    boolean autoRouteConn = ((Boolean)info.getValue()).booleanValue();
	    final JCheckBoxMenuItem autoRouteItem = 
		new JCheckBoxMenuItem("Auto-Route Connection", autoRouteConn);

	    autoRouteItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			PropertyInfo info = 
			    DefaultConnectionFigure.this.
			    propertyManager.
			    getProperty("Auto-Route Connection");
			
			info.setValue(new Boolean(autoRouteItem.
						  getState()));
			DefaultConnectionFigure.this.propertyChanged(info);
		    }
		});
	    popup.add(autoRouteItem);
	}

	JMenuItem showDataItem = new JMenuItem("View Connection Data");
	showDataItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    LSSInfoDialog.showDialog(null, DefaultConnectionFigure.
					     this.connection);
		}
	    });
	popup.add(showDataItem);

	JMenuItem showPropertiesItem = new JMenuItem("View Visual Properties");
	showPropertiesItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    DefaultConnectionFigure.this.propertyManager.
			showPropertiesDialog();
		}
	    });
	popup.add(showPropertiesItem);

	SegmentFigure tmp = null;

	for(int i = 0; i < segments.size(); i++){
	    
	    if(((SegmentFigure)segments.get(i)).contains(x, y)){
		tmp = (SegmentFigure)segments.get(i);
		break;
	    }
	}
	if(tmp != null){

	    JMenuItem perpItem = 
		new JMenuItem("Make Segments Perpendicular");
	    perpItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			DefaultConnectionFigure.this.cleanUpSegments();
		    }
		});
	    popup.add(perpItem);



	    final SegmentFigure segment = tmp;
	    final SchematicFigure segmentSrcFigure = tmp.getSrcFigure();
	    final SchematicFigure segmentDestFigure = tmp.getDestFigure();

	    if(segmentSrcFigure.contains(x, y) && 
	       segmentSrcFigure instanceof JogPointFigure){
		JMenuItem removeJogPointItem = 
		    new JMenuItem("Remove Jog Point");
		removeJogPointItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
			    DefaultConnectionFigure.this.
				removeJogPoint(segmentSrcFigure);
			}
		    });
		popup.add(removeJogPointItem);
	    }
	    else if(segmentDestFigure.contains(x, y) && 
	       segmentDestFigure instanceof JogPointFigure){
		JMenuItem removeJogPointItem = 
		    new JMenuItem("Remove Jog Point");
		removeJogPointItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
			    DefaultConnectionFigure.this.
				removeJogPoint(segmentDestFigure);
			}
		    });
		popup.add(removeJogPointItem);
	    }
	    else{
		JMenuItem addJogPointItem = new JMenuItem("Add Jog Point");
		addJogPointItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
			    DefaultConnectionFigure.this.
				addJogPoint(segment, x, y);
			    DefaultConnectionFigure.this.validateBounds();
			    DefaultConnectionFigure.this.repaint();
			}
		    });
		popup.add(addJogPointItem);
	    }
	}
	return popup;
    }

    /**
     * The SchematicCanvas that owns this SchematicFigure will call this
     * method whenever a drag event involving this figure occurs.
     *
     * @param dx the horizontal distance traversed by the drag event
     * @param dy the vertical distance traversed by the drag even
     * @param eventPoint the initial point at which the mouse down event
     *           occurred
     */
    public void drag(int dx, int dy, Point eventPoint)
    {
	if(!autoRoute){
	    // figure out if we have a jog point or a segment and take
	    // the appropriate action
	    int originatingX = eventPoint.x - dx;
	    int originatingY = eventPoint.y - dy;
	    boolean dragged = false;

	    for(int i = 0; i < segments.size(); i++){
		SegmentFigure segment = (SegmentFigure)segments.get(i);

		if(segment.contains(originatingX, originatingY)){
		    segment.drag(dx, dy, eventPoint);
		    selectedSegment = segment;
		    dragged = true;
		    // we don't want drag to be called twice at the point where
		    // two segments intersect
		    break;
		}
	    }
	    // here's the problem, everynow and then the eventPoint is not
	    // contained in a segment, but the drag event would not be called
	    // if this connection was not currently selected and being dragged
	    // hence, we just assume correctness and drag the last segment
	    // that was dragged.
	    if(!dragged && selectedSegment != null)
		selectedSegment.drag(dx, dy, eventPoint);
	    validateBounds();
	}
    }

    /**
     * Calculate the bounding region of this figure.  
     * This value is used by the SchematicCanvas to determine the
     * clickable region of a figure.
     *
     * @return the bounding rectangle of this figure 
     */
    public Rectangle getBounds()
    {
	return getPaintBounds();
    }

    /**
     * Return a collection of the drawables that this figure owns.
     *
     * @return a collection of the drawables owned by this figure
     */
    public Collection getDrawables()
    {
	return drawables.values();
    }

    /**
     * Return a SchematicFigure owned by this figure which contains the
     * specified location.  This function may return this figure, a
     * sub-figure, or null.
     *
     * @param x the x coordinate in question
     * @param y the y coordinate in question
     * @return a figure which contains the specified point or null
     */
    public SchematicFigure getFigureAt(int x, int y)
    {
	// we don't actually want to pass up the segments of the
	// jogpoints, so if either element lies within the
	// connection's bounds, the connection will handle it
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		
		if(figure.contains(x, y)){
		    return figure;
		}
	    }
	}
	return null;
    }

    /**
     * Return a SchematicFigure owned by this figure, which has the 
     * specified name or null.
     *
     * @param name the name of the figure sought
     * @return a figure bearing the specified name or null
     */
    public SchematicFigure getFigureByName(String name)
    {
	return (SchematicFigure)figures.get(name);
    }

    /**
     * Return a SchematicFigure which has the specified Object as its
     * user object.
     *
     * @param o the user object belonging to the figure sought
     * @return the figure which has the specified Object as its user object
     *         or null
     */
    public SchematicFigure getFigureByUserObject(Object o)
    {
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		if(figure.getUserObject() == o)
		    return figure;
	    }
	}
	return null;
    }

    /**
     * Return a collection of the figures that this figure owns.
     *
     * @return a collection of the figures owned by this figure
     */
    public Collection getSubFigures()
    {
	return figures.values();
    }

    /**
     * Returns a rectangle that bounds the region that needs to rendered 
     * when this figure is painted.
     *
     * @return a rectangle that bounds the region that needs to be rendered
     *         when this figure is painted
     */
    public Rectangle getPaintBounds()
    {
	if(paintBounds == null){
	    if(autoRoute)
		SimpleManhattanRouter.buildConnectionFigure(this, canvas);
	    validateBounds();
	}

	Rectangle ret = new Rectangle(paintBounds);

	return ret;
    }

    /**
     * Get the array of segments used by the SimpleManhattanRouter.
     *
     * @return an array of segments to be used by the SimpleManhattanRouter
     */
    public SegmentFigure[] getSegementArray()
    {
	return segmentArray;
    }

    /**
     * Parse the text and perform the action specified by the text parameter 
     * if it is defined by this figure, otherwise ignore the text.
     *
     * @param text the string which represents an action to be carried out
     *             by this figure
     */
    public void handleCommand(String text)
    {
	int parameterBeginIndex = text.indexOf("(");
	int parameterEndIndex = text.indexOf(")");

	if(parameterBeginIndex < 0 || parameterEndIndex < 0){
	    // FIXME:report an error
	    return;
	}
       
	String command = text.substring(0, parameterBeginIndex);
	String parameters = text.substring(parameterBeginIndex+1, 
					   parameterEndIndex);
	StringTokenizer tokenizer = new StringTokenizer(parameters, ", ");
	String parameterList[] = new String[tokenizer.countTokens()];
	int i = 0;

	while(tokenizer.hasMoreTokens()){
	    String parameter = tokenizer.nextToken();

	    parameterList[i++] = parameter;
	}

	if(command.equals("clearJogPoints")){
	    clearJogPoints();
	}
	else if(command.equals("addJogPoint")){
	    if(parameterList.length != 2){
		// FIXME:report an error
		return;
	    }
	    int xCoord = new Integer(parameterList[0]).intValue();
	    int yCoord = new Integer(parameterList[1]).intValue();
		
	    addJogPoint(xCoord, yCoord);
	}
    }

    /**
     * Set this figures properties to reflect the properties defined in the
     * propertySet, if they are applicable.
     *
     * @param propertySet a set of PropertyInfo objects
     */
    public void handleStoredProperties(java.util.List propertyList)
    {
	Document document = canvas.getDocument();
	Vector commands = new Vector();

	Iterator infoIterator = propertyList.iterator();
	while(infoIterator.hasNext()){
	    PropertyInfo info = (PropertyInfo)infoIterator.next();
	    String propertyName = info.getPropertyName();
	    Object value = info.getValue();
	    int datatype = info.getDataType();

	    if(datatype == PropertyInfo.COMMAND_DATATYPE){
		boolean placed = false;

		for(int i = 0; i < commands.size(); i++){
		    if(propertyName.compareTo(((PropertyInfo)commands.
		       get(i)).getPropertyName()) < 0){
			commands.add(i, info);
			placed = true;
			break;
		    }
		}

		if(placed == false)
		    commands.add(info);

		document.removeProperty(connection.toString()+"."+
					propertyName);
	    }
	    else{
		PropertyInfo localInfo = propertyManager.
		    getProperty(propertyName);
		if(!localInfo.getValue().equals(value)){   
		    localInfo.setValue(value);
		    propertyChanged(localInfo);
		}
	    }
	}

	Iterator commandIterator = commands.iterator();
	while(commandIterator.hasNext()){
	    handleCommand((String)((PropertyInfo)commandIterator.next()).
			  getValue());
	}
    }

    /**
     * Builds PropertyInfo objects for all of the properties that will be
     * stored for this figure.
     *
     */
    public void initializeProperties()
    {
	PropertyInfo info = new PropertyInfo(this, 
					     "Set Visible",
					     "General",
					     new Boolean(true),
					     PropertyInfo.BOOL_DATATYPE,
					     null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Show Jog Points",
				"General",
				new Boolean(showJogPoints),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Auto-Route Connection",
				"General",
				new Boolean(autoRoute),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Line Weight",
				"General",
				new Float(1.0f),
				PropertyInfo.FLOAT_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this,
				"Fill Color",
				"General",
				new Integer(fillColor.
					    getRGB()),
				PropertyInfo.COLOR_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this,
				"Selected Fill Color",
				"General",
				new Integer(selectedFillColor.
					    getRGB()),
				PropertyInfo.COLOR_DATATYPE,
				null, true);
	propertyManager.addProperty(info);
    }

    /**
     * Return whether this connection is auto-routed or not.
     *
     * @return whether this connection is auto-routed or not
     */
    public boolean isAutoRouted()
    {
	return autoRoute;
    }

    /**
     * Return whether or not this figure is currently selected.
     *
     * @return whether this figure is currently selected
     */
    public boolean isSelected()
    {
	return isSelected;
    }
    
    /**
     * Return whether or not this figure is currently visible.
     *
     * @return whether this figure is currently visible
     */
    public boolean isVisible()
    {
	return isVisible;
    }

    /**
     * Paint this figure using the specified Graphics2D object.
     *
     * @param g2d the graphics object to use when painting this figure
     */
    public void paint(Graphics2D g2d)
    {
	if(isVisible){
	    Stroke oldStroke = g2d.getStroke();
	    g2d.setStroke(stroke);
	    g2d.setPaint(Color.black);

	    // don't rebuild the connection if it's not necessary
	    if(autoRoute){
		Point newSrcLocation = 
		    srcFigure.getSourceConnectLocation(this);
		Point newDestLocation = 
		    destFigure.getDestConnectLocation(this);

		if(newSrcLocation.x != srcConnectLocation.x || 
		   newSrcLocation.y != srcConnectLocation.y ||
		   newDestLocation.x != destConnectLocation.x ||
		   newDestLocation.y != destConnectLocation.y){

		    SimpleManhattanRouter.buildConnectionFigure(this, canvas);
		    srcConnectLocation = newSrcLocation;
		    destConnectLocation = newDestLocation;
		}

		for(int i = 0; i < 5; i++){
		    segmentArray[i].paint(g2d);
		}
		validateBounds();
	    }
	    else{
		for(int i = 0; i < segments.size(); i++){
		    SegmentFigure segmentFigure = 
			(SegmentFigure)segments.get(i);

		    segmentFigure.paint(g2d);
		}
	    }
	    g2d.setStroke(oldStroke);
	}
    }    

    /**
     * Signal to this figure that a property change event has occurred and
     * that it should update its state accordingly.
     *
     * @param info the PropertyInfo object that caused this event
     */
    public void propertyChanged(PropertyInfo info)
    {
	String propertyName = info.getPropertyName();

	if(propertyName.equals("Set Visible")){
	    this.isVisible = ((Boolean)info.getValue()).booleanValue();
	    repaint();
	}
	if(propertyName.equals("Line Weight")){
 	    float lineWeight = ((Float)info.getValue()).floatValue();
	    this.stroke = new BasicStroke(lineWeight);
	    repaint();
	}
	else if(propertyName.equals("Fill Color")){
	    int newFillColor = ((Integer)info.
				getValue()).intValue();
	    if(fillColor.getRGB() != newFillColor)
		setFillColor(new Color(newFillColor));
	    repaint();
	}
	else if(propertyName.equals("Selected Fill Color")){
	    int newSelectedFillColor = ((Integer)info.
					getValue()).intValue();		
	    if(selectedFillColor.getRGB() != newSelectedFillColor)
		setSelectedFillColor(new Color(newSelectedFillColor));
	    repaint();
	}
	else if(propertyName.equals("Show Jog Points")){
	    this.showJogPoints = ((Boolean)info.getValue()).booleanValue();
	    repaint();
	}
	else if(propertyName.equals("Auto-Route Connection")){
	    boolean newValue = ((Boolean)info.getValue()).booleanValue();
	    if(this.autoRoute != newValue){
		Rectangle repaintRegion = getPaintBounds();
		setAutoRoute(newValue);
		repaintRegion = 
		    repaintRegion.createUnion(getPaintBounds()).getBounds();
		canvas.repaint(repaintRegion);
	    }
	}
    }

    /**
     * Remove the specified JogPointFigure and the appropriate SegmentFigure
     * from this connection.
     *
     */
    public void removeJogPoint(SchematicFigure jogPoint)
    {
	SegmentFigure segment1 = null;
	SegmentFigure segment2 = null;

	for(int i = 0; i < segments.size(); i++){
	    SegmentFigure tmp = (SegmentFigure)segments.get(i);
	    
	    if(tmp.getDestFigure() == jogPoint)
		segment1 = tmp;
	    else if(tmp.getSrcFigure() == jogPoint)
		segment2 = tmp;
	}

	if(segment1 != null && segment2 != null){
	    segment1.setDestFigure(segment2.getDestFigure());
	    segments.remove(segment2);
	    segment2 = null;
	    repaint();
	}
    }

    /**
     * Repaint this figure.
     *
     */
    public void repaint()
    {
	canvas.repaint(getPaintBounds());
    }

    /**
     * Set the status for whether this connection is auto-routed or not.
     *
     * @param b should this connection be auto-routed or not
     */
    public void setAutoRoute(boolean b)
    {
	autoRoute = b;

	if(this.autoRoute){
	    JogPointFigure jogPointFigure1;
	    JogPointFigure jogPointFigure2 = new JogPointFigure(canvas, this);
	    
	    for(int i = 0; i < 5; i++){
		segmentArray[i] = new SegmentFigure(this, canvas);
		
		jogPointFigure1 = jogPointFigure2;
		jogPointFigure2 = new JogPointFigure(canvas, this);
		segmentArray[i].setSourceFigure(jogPointFigure1);
		segmentArray[i].setDestFigure(jogPointFigure2);
		segmentArray[i].setFillColor(this.fillColor);
		segmentArray[i].setSelectedFillColor(this.selectedFillColor);
	    }
	    
	    SimpleManhattanRouter.buildConnectionFigure(this, canvas);
	}
	else{
	    segments.clear();
	    for(int i = 0; i < 5; i++){
		if(segmentArray[i].isVisible()){
		    segments.add(segmentArray[i]);
		    segmentArray[i] = null;
		}
	    }
	    SegmentFigure firstSegment = 
		(SegmentFigure)segments.get(0);
	    
	    Port srcPort = connection.getSourcePort();
	    String srcInstanceName = 
		srcPort.getInstance().getFullyQualifiedName();
	    PortFigure portFigure = (PortFigure)
			srcFigure.getFigureByName(srcInstanceName+"."+
						  srcPort.getName());
	    
	    firstSegment.setSourceFigure(portFigure.
					 getFigureByName(""+connection.
							 getSourceInstance()));
	    
	    SegmentFigure lastSegment = 
		(SegmentFigure)segments.get(segments.size()-1);
	    Port destPort = connection.getDestPort();
	    String destInstanceName = 
		destPort.getInstance().getFullyQualifiedName();
	    
	    portFigure = (PortFigure)
		destFigure.getFigureByName(destInstanceName+"."+
					   destPort.getName());
	    lastSegment.setDestFigure(portFigure.
				      getFigureByName(""+connection.
						      getDestInstance()));
	    
	    validateBounds();
	}
    }

    /**
     * Set the destination SchematicNodeFigure of this 
     * DefaultConnectionFigure to be the figure specified by destFigure.
     *
     * @param destFigure the new destination for this DefaultConnectionFigure
     */
    public void setDestFigure(SchematicNodeFigure destFigure)
    {
	this.destFigure = destFigure;

	// invalidate cached data
	srcConnectLocation = destConnectLocation = new Point(0, 0);
	paintBounds = null;

	// patch up the segment figures to point to the new destFigure
	if(autoRoute){
	}
	else{
	}
    }

    /**
     * Set the fill color for this connection figure.
     *
     * @param c the color to be used for a fill color in painting this figure
     */
    public void setFillColor(Color c)
    {
	this.fillColor = c;
	if(autoRoute){
	    for(int i = 0; i < 5; i++){
		segmentArray[i].setFillColor(c);
	    }
	}
	else{
	    for(int i = 0; i < segments.size(); i++){
		((SegmentFigure)segments.get(i)).setFillColor(c);
	    }
	}
    }

    /**
     * Set the selected status of this figure.
     *
     * @param b the boolean value specifying whether this figure is selected
     *          or not
     */
    public void setSelected(boolean b)
    {
	isSelected = b;
	repaint();
    }

    /**
     * Set the fill color for this connection figure when the figure 
     * is selected
     *
     * @param c the color to be used for a fill color in painting this figure
     *          when the figure is selected
     */
    public void setSelectedFillColor(Color c)
    {
	this.selectedFillColor = c;
	if(autoRoute){
	    for(int i = 0; i < 5; i++){
		segmentArray[i].setSelectedFillColor(c);
	    }
	}
	else{
	    for(int i = 0; i < segments.size(); i++){
		((SegmentFigure)segments.get(i)).setSelectedFillColor(c);
	    }
	}
    }

    /**
     * Set the source SchematicNodeFigure of this DefaultConnectionFigure to
     * be the figure specified by srcFigure.
     *
     * @param srcFigure the new destination for this DefaultConnectionFigure
     */
    public void setSrcFigure(SchematicNodeFigure srcFigure)
    {
	this.srcFigure = srcFigure;

	// invalidate cached data
	srcConnectLocation = destConnectLocation = new Point(0, 0);
	paintBounds = null;


	if(autoRoute){

	}
	else{

	}
    }

    /**
     * Set the visible status of this figure.
     *
     * @param b the boolean value specifying whether this figure is visible
     *          or not
     */
    public void setVisible(boolean b)
    {
	isVisible = b;
    }

    /**
     * This function is used to set the location of this figure to x and y
     * coordinates which are divisible by the integer value 10.
     *
     */
    public void snapToGrid()
    {
	if(!autoRoute){
	    for(int i = 0; i < segments.size(); i++){
		SegmentFigure segmentFigure = (SegmentFigure)segments.get(i);
		
		segmentFigure.snapToGrid();
	    }
	}
    }

    /**
     * Report this figure's current property state to the LSSDocument which
     * will eventually write them to a file.
     *
     */
    public void storeProperties()
    {
	updateCachedProperties();
	propertyManager.storeProperties();

	LSSDocument document = (LSSDocument)canvas.getDocument();

	if(!autoRoute){
	    document.setProperty(connection.toString()+".command0", 
				 "clearJogPoints()");

	    // write a command to add a JogPointFigure for each JogPointFigure
	    // in the segments vector
	    for(int i = 0; i < segments.size(); i++){
		SegmentFigure segment = (SegmentFigure)segments.get(i);
		SchematicFigure destFigure = segment.getDestFigure();

		if(destFigure instanceof JogPointFigure){
		    Point jogLocation = destFigure.getLocation();

		    document.setProperty(connection.toString()+
					 ".command"+(i+1),
					 "addJogPoint("+jogLocation.x+", "+
					 jogLocation.y+")");
		}				     
	    } 
	}
    }
    
    /**
     * Return a string representation of this figure.
     *
     * @return a string representation of this figure
     */
    public String toString()
    {
	return name;
    }

   /**
     * Do nothing.
     *
     * @param dx the horizontal distance to translate this figure
     * @param dy the vertical distance to translate this figure
     */
    public void translate(int dx, int dy)
    {
	// do nothing
    }

    /**
     * Recalculate the bounding regions for this figure if they are cached.
     *
     */
    public void validateBounds()
    {
	if(autoRoute){
	    paintBounds = segmentArray[0].getPaintBounds();
	    for(int i = 1; i < 5; i++){
		if(segmentArray[i].isVisible())
		    paintBounds = 
			paintBounds.createUnion(segmentArray[i].
						getPaintBounds()).getBounds();
	    }
	}
	else{
	    paintBounds = srcFigure.getPaintBounds().
		createUnion(destFigure.getPaintBounds()).getBounds();
	    for(int i = 0; i < segments.size(); i ++){
		SegmentFigure segment = (SegmentFigure)segments.get(i);
		paintBounds = paintBounds.
		    createUnion(segment.getPaintBounds()).getBounds();
	    }
	    paintBounds.setRect(paintBounds.x - 2, paintBounds.y -2,
				paintBounds.width + 4, paintBounds.height + 4);
	}
    }

    /**
     * Check the location of a figure owned by this figure and if it is in an
     * invalid location, set its location accordingly.
     *
     * @param child the child figure whose location is being validated
     * @param mouseLocation a hint in determining where the location of the
     *                      child figure should be
     */
    public void validateChildLocation(SchematicFigure child, Point eventPoint)
    {}
}
