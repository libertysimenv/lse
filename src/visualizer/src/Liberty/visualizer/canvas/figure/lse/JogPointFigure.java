package Liberty.visualizer.canvas.figure.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;

import java.awt.*;
import java.io.File;
import java.util.*;
import javax.swing.*;

public class JogPointFigure implements SchematicFigure{
    /** the bounding rectangle for this figure */
    private Rectangle bounds;
    /* this figure's parent */
    private PluggableConnectionFigure parent;
    /* is this figure selected */
    private boolean isSelected;
    /* the location of this figure */
    private Point location;
    /** the drawable used to draw this port instance */
    private ShapeDrawable shapeDrawable;
    /** */
    private SchematicCanvas canvas;

    public JogPointFigure(SchematicCanvas canvas, 
			  PluggableConnectionFigure parent)
    {
	this.location = new Point();
	this.parent = parent;

	this.isSelected = false;
	Shape rect = new Rectangle(0, 0, 3, 3);
	shapeDrawable = new ShapeDrawable(canvas, this, "Shape", rect,
					  new Color(0xC0C0C0),
					  new Color(0xe51d27)/*,
							       null*/);
	bounds = shapeDrawable.getBounds();
    }

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     * @param oldBounds the old bounding rectangle for the figure
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {
    }

    public boolean contains(int x, int y)
    {
	if(getPaintBounds().contains(x, y))
	    return true;
	return false;
    }

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	return null;
    }

    public void drag(int dx, int dy, Point eventPoint)
    {
	translate(dx, dy);
	if(parent != null)
	    parent.validateChildLocation(this, eventPoint);
    }

    public Rectangle getBounds()
    {
	return shapeDrawable.getBounds();
    }

    public String getFullyQualifiedName()
    {
	return parent.getFullyQualifiedName();
    }

    public Collection getSubFigures()
    {
	return null;
    }

    public Collection getDrawables()
    {
	return null;
    }

    public SchematicFigure getFigureAt(int x, int y)
    {
	return null;
    }

    public SchematicFigure getFigureByName(String name)
    {
	return null;
    }

    public SchematicFigure getFigureByUserObject(Object o)
    {
	return null;
    }

    public Point getLocation()
    {
	return location;
    }

    public String getName()
    {
	return "Jog Point: "+parent.getName();
    }
    
    public Rectangle getPaintBounds()
    {
	Rectangle bounds = shapeDrawable.getBounds();
	return new Rectangle(bounds.x - 1, bounds.y - 1,
			     bounds.width + 2, bounds.height + 2);
    }

    public SchematicCanvasContainer getParent()
    {
	return parent;
    }

    /**
     * This function returns a String to be displayed in a tooltip when
     * the mouse hovers over this figure.
     *
     * @return the tooltip text to be displayed
     */
    public String getTooltipText()
    {
	return parent.getTooltipText();
    }


    public Object getUserObject()
    {
	return null;
    }

    public void handleCommand(String command)
    {

    }

    public void handleDoubleClick(int x, int y)
    {
    }

    public boolean isSelected()
    {
	return isSelected;
    }
    
    public boolean isVisible()
    {
	return parent.isVisible();
    }
    
    public void paint(Graphics2D g2d)
    {
	if(parent.showJogPoints()){
	    shapeDrawable.paint(g2d);
	}
    }

    public void repaint()
    {
	canvas.repaint(bounds);
    }

    public void setLocation(int x, int y)
    {
	int dx, dy;

	dx = x - location.x;
	dy = y - location.y;

	this.location.setLocation(x, y);
	shapeDrawable.translate(dx, dy);
	bounds.translate(dx, dy);
    }

    /**
     * Set the parent of this figure to be the SchematicCanvasContainer defined
     * by the parameter parent.
     *
     * @param parent the new SchematicCanvasContainer that owns this figure
     */
    public void setParent(SchematicCanvasContainer parent)
    {
	if(parent instanceof PluggableConnectionFigure)
	    this.parent = (PluggableConnectionFigure)parent;
    }

    public void setSelected(boolean b)
    {
	parent.setSelected(b);
	isSelected = b;
	repaint();
    }

    public void setVisible(boolean b)
    {
    }
    
    public String toString()
    {
	return parent.getName();
    }

    public void translate(int dx, int dy)
    {
	int newX = location.x + dx;
	int newY = location.y + dy;

	if(newX < 0)
	    newX = 0;
	if(newY < 0)
	    newY = 0;

	location.setLocation(location.x + dx, location.y + dy);
	shapeDrawable.translate(dx, dy);
	bounds.translate(dx, dy);
    }

    public void validateBounds()
    {
    }

    public void validateChildLocation(SchematicFigure child, Point eventPoint)
    {
    }


    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     */
    public void clear(){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param drawable the drawable to be added to this figure
     */
    public void addDrawable(Drawable drawable){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param figure the figure to be added to this figure
     */
    public void addFigure(SchematicFigure figure){}

    /**
     * This function is used to set the location of this figure to x and y
     * coordinates which are divisible by the integer value 10.
     *
     */
    public void snapToGrid()
    {
	int xCoord = location.x;
	int yCoord = location.y;

	if(xCoord % 5 != 0){
	    int base = Math.round(xCoord/5);
	    xCoord = base * 5;
	}
	if(yCoord % 5 != 0){
	    int base = Math.round(yCoord/5);
	    yCoord = base * 5;
	}
	setLocation(xCoord, yCoord);
    }

}
