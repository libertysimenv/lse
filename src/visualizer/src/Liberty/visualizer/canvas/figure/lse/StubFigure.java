package Liberty.visualizer.canvas.figure.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.geom.*;
import java.io.File;
import java.util.*;

/**
 * The StubFigure is designed to be used to depict connections to ports which
 * connect to a higher level in the configuration hierarchy, meaning that the
 * PortFigure for one element in the connection will be null.
 *
 * @see PluggableConnectionFigure
 * @see DefaultConnectionFigure
 *
 * @author Jason Blome
 * @version 0.1
 */


public class StubFigure extends PluggableConnectionFigure{
    
    /** is this figure selected */
    private boolean isSelected;
    /** is this figure visible */
    private boolean isVisible;
    /** a segment figure used to draw the stub for this figure */
    private SegmentFigure segmentFigure;
    /** a figure used for an endpoint in this connection representation */
    private JogPointFigure jogPointFigure;
    /** the label for this figure */
    private LabelDrawable label;
    /** the stroke used to draw the connection */
    private BasicStroke stroke;

    /**
     * Constructor for the StubFigure object used to represent 
     * hierarchical IMR Connections objects on a SchematicCanvas.
     *
     * @param canvas the canvas that this GenericInstanceFigure
     *               will be rendered upon
     * @param parent the parent of this 
     *               StubFigure, which may be any class
     *               that implements SchematicCanvasContainer
     * @param connection the IMR Connection object obtained from the LSE 
     *               compiler
     * @param srcFigure the srcFigure representing the source IMR Instance 
     *                  object
     * @param destFigure the srcFigure representing the destination IMR 
     *                  Instance object
     */
    public StubFigure(SchematicCanvas canvas,
		      SchematicCanvasContainer parent,
		      Connection connection,
		      SchematicNodeFigure srcFigure,
		      SchematicNodeFigure destFigure)
    {
	super(canvas, parent, connection, srcFigure, destFigure);
	this.isSelected = false;
	this.isVisible = true;
	this.stroke = new BasicStroke(1.0f);

	this.segmentFigure = new SegmentFigure(this, canvas);
	this.jogPointFigure = new JogPointFigure(canvas, this);

	int connectSite;
	Point connectLocation;
        String labelText;
	Port srcPort = connection.getSourcePort();
	Port destPort = connection.getDestPort();
	String srcInstanceName = 
	    srcPort.getInstance().getFullyQualifiedName();
	String destInstanceName = 
	    destPort.getInstance().getFullyQualifiedName();

	if(destFigure == null){
	    PortFigure portFigure = (PortFigure)
		srcFigure.getFigureByName(srcInstanceName+"."+
					  srcPort.getName());  
	    segmentFigure.
		setSourceFigure(portFigure.getFigureByName(""+connection.
				getSourceInstance()));

	    labelText = destInstanceName+"."+destPort.getName()+
		"["+connection.getDestInstance()+"]";
	}
	else{
	    PortFigure portFigure = (PortFigure)
		destFigure.getFigureByName(destInstanceName+"."+
					   destPort.getName());

	    segmentFigure.
		setSourceFigure(portFigure.getFigureByName(""+connection.
							   getDestInstance()));

	    labelText = srcInstanceName+"."+srcPort.getName()+"["+
		connection.getSourceInstance()+"]";
	}

	segmentFigure.setDestFigure(jogPointFigure);

	label = new LabelDrawable(canvas, this, "Label", labelText);
	label.setFontSize(9);
    }

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     * @param oldBounds the old bounding rectangle for the figure
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {
    }

    /**
     * Determine if the supplied location is contained in this figure.
     *
     * @param x the x coordinate in question
     * @param y the y coordinate in question
     */
    public boolean contains(int x, int y)
    {
	return getBounds().contains(x, y);
    }

    /**
     * The SchematicCanvas that owns this SchematicFigure will call this
     * method whenever a drag event involving this figure occurs.
     *
     * @param dx the horizontal distance traversed by the drag event
     * @param dy the vertical distance traversed by the drag even
     * @param eventPoint the initial point at which the mouse down event
     *           occurred
     */
    public void drag(int dx, int dy, Point eventPoint)
    {
// 	if(label.getBounds().contains(eventPoint)){
// 	    label.translate(dx, dy);
// 	}	    
    }

    /**
     * Calculate the bounding region of this figure.  
     * This value is used by the SchematicCanvas to determine the
     * clickable region of a figure.
     *
     * @return the bounding rectangle of this figure 
     */
    public Rectangle getBounds()
    {
	Rectangle ret = segmentFigure.getPaintBounds().
	    createUnion(label.getBounds()).getBounds();

	return ret;
    }

    /**
     * Return a SchematicFigure owned by this figure which contains the
     * specified location.  This function may return this figure, a
     * sub-figure, or null.
     *
     * @param x the x coordinate in question
     * @param y the y coordinate in question
     * @return a figure which contains the specified point or null
     */
    public SchematicFigure getFigureAt(int x, int y)
    {
	return null;
    }

    /**
     * Return a SchematicFigure owned by this figure, which has the 
     * specified name or null.
     *
     * @param name the name of the figure sought
     * @return a figure bearing the specified name or null
     */
    public SchematicFigure getFigureByName(String name)
    {
	return null;
    }

    /**
     *
     * Return a SchematicFigure which has the specified Object as its
     * user object.
     *
     * @param o the user object belonging to the figure sought
     * @return the figure which has the specified Object as its user object
     *         or null
     */
    public SchematicFigure getFigureByUserObject(Object o)
    {
	return null;
    }

    /**
     * Returns a rectangle that bounds the region that needs to rendered 
     * when this figure is painted.
     *
     * @return a rectangle that bounds the region that needs to be rendered
     *         when this figure is painted
     */
    public Rectangle getPaintBounds()
    {
	Rectangle ret = segmentFigure.getPaintBounds().
	    createUnion(label.getBounds()).getBounds();
 
	ret.setRect(ret.x - 2, ret.y - 2, ret.width + 4, ret.height + 4);

	return ret;
    }

    /**
     * Get the array of segments used by the SimpleManhattanRouter.  In the
     * case of the StubFigure, this will always return null.
     *
     * @return an array of segments to be used by the SimpleManhattanRouter
     */
    public SegmentFigure[] getSegementArray()
    {
	return null;
    }


    /**
     * Return whether or not this figure is currently selected.
     *
     * @return whether this figure is currently selected
     */
    public boolean isSelected()
    {
	return isSelected;
    }

    /**
     * Return whether or not this figure is currently visible.
     *
     * @return whether this figure is currently visible
     */
    public boolean isVisible()
    {
	return isVisible;
    }

    /**
     * Paint this figure using the specified Graphics2D object.
     *
     * @param g2d the graphics object to use when painting this figure
     */
    public void paint(Graphics2D g2d)
    {
	Point srcPoint;
	Point destPoint;

	int y_offset = Math.round(label.getBounds().height/2);

	int connectSite = srcFigure == null ? 
	    destFigure.getDestConnectSite(this) : 
	    srcFigure.getSourceConnectSite(this);

	switch(connectSite){
	case WEST:
	    srcPoint = segmentFigure.getSrcFigure().getLocation();
	    destPoint = new Point(srcPoint.x - 20, srcPoint.y);
	    jogPointFigure.setLocation(destPoint.x, destPoint.y);
	    label.setJustification(LabelDrawable.RIGHT);
	    label.setLocation(destPoint.x - 3, destPoint.y - y_offset);
	    break;
	case EAST:
	    srcPoint = segmentFigure.getSrcFigure().getLocation();
	    destPoint = new Point(srcPoint.x + 20, srcPoint.y);
	    jogPointFigure.setLocation(destPoint.x, destPoint.y);
	    label.setJustification(LabelDrawable.LEFT);
	    label.setLocation(destPoint.x + 3, destPoint.y - y_offset);
	    break;
	case NORTH:
	    srcPoint = segmentFigure.getSrcFigure().getLocation();
	    destPoint = new Point(srcPoint.x, srcPoint.y - 20);
	    jogPointFigure.setLocation(destPoint.x, destPoint.y);
	    label.setJustification(LabelDrawable.CENTER);
	    label.setLocation(destPoint.x, destPoint.y - 3);
	    break;
	case SOUTH:
	    srcPoint = segmentFigure.getSrcFigure().getLocation();
	    destPoint = new Point(srcPoint.x, srcPoint.y + 20);
	    jogPointFigure.setLocation(destPoint.x, destPoint.y);
	    label.setJustification(LabelDrawable.RIGHT);
	    label.setLocation(destPoint.x, destPoint.y + 3);
	    break;
	}

	Stroke oldStroke = g2d.getStroke();
	g2d.setStroke(stroke);
	segmentFigure.paint(g2d);
	g2d.setStroke(oldStroke);
	jogPointFigure.paint(g2d);
	label.paint(g2d);
    }

    /**
     * Repaint this figure.
     *
     */
    public void repaint()
    {
	canvas.repaint(getPaintBounds());
    }
    
    /**
     * Set the destination SchematicNodeFigure of this 
     * DefaultConnectionFigure to be the figure specified by destFigure.
     *
     * @param destFigure the new destination for this DefaultConnectionFigure
     */
    public void setDestFigure(SchematicNodeFigure destFigure)
    {
	this.destFigure = destFigure;
    }

    /**
     * Set the selected status of this figure.
     *
     * @param b the boolean value specifying whether this figure is selected
     *          or not
     */
    public void setSelected(boolean b)
    {
	this.isSelected = b;
    }

    /**
     * Set the source SchematicNodeFigure of this DefaultConnectionFigure to
     * be the figure specified by srcFigure.
     *
     * @param srcFigure the new destination for this DefaultConnectionFigure
     */
    public void setSrcFigure(SchematicNodeFigure srcFigure)
    {
	this.srcFigure = srcFigure;
    }

    /**
     * Set the visible status of this figure.
     *
     * @param b the boolean value specifying whether this figure is visible
     *          or not
     */
    public void setVisible(boolean b)
    {
	this.isVisible = b;
    }

   /**
     * Do nothing.
     *
     * @param dx the horizontal distance to translate this figure
     * @param dy the vertical distance to translate this figure
     */
    public void translate(int dx, int dy)
    {
    }

    /**
     * Recalculate the bounding regions for this figure if they are cached.
     *
     */
    public void validateBounds(){}

    /**
     * Return a string representation of this figure.
     *
     * @return a string representation of this figure
     */
    public String toString()
    {
	return connection.toString();
    }

}
