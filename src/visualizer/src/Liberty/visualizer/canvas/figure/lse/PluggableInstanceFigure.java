package Liberty.visualizer.canvas.figure.lse;

import Liberty.LSS.IMR.*;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.dialog.*;
import Liberty.visualizer.view.*;
import Liberty.visualizer.view.lse.*;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import javax.swing.*;

/**
 * The PluggableInstanceFigure is an abstract class that defines some of the
 * general functionality that will be needed for all SchematicFigures which 
 * will be used to represent an LSE IMR instance on the canvas.  All
 * SchematicFigures which will be used to represent an IMR instance should
 * be a descendent of this class.
 *
 * @see GenericInstanceFigure
 * @see DefaultInstanceFigure
 *
 * @author Jason Blome
 * @version 0.1
 */
public abstract class PluggableInstanceFigure implements SchematicNodeFigure, 
							 PropertyHolder
{
    /** a map of the edges containing this node */
    protected HashMap edges;
    /** the canvas that this figure will be drawn on */
    protected SchematicCanvas canvas;
    /** the parent container of this figure */
    protected SchematicCanvasContainer parent;
    /** the property manager for this figure */
    protected PropertyManager propertyManager;
    /** the IMR instance that this figure represents */
    protected Instance instance;

    /**
     * The sole constructor for the PluggableInstanceFigure.  Note that any 
     * class that extends PluggableInstanceFigure must call super with the
     * three parameters specified below as no empty constructor is specified 
     * for the abstract PluggableInstanceFigure class.
     *
     * @param canvas the canvas that this GenericInstanceFigure
     *               will be rendered upon
     * @param parent the parent of this 
     *               GenericInstanceFigure, which may be any class
     *               that implements SchematicCanvasContainer
     * @param instance the IMR instance obtained from the LSE 
     *               compiler
     */
    public PluggableInstanceFigure(SchematicCanvas canvas,
				   SchematicCanvasContainer parent,
				   Instance instance)
    {
	this.edges = new HashMap();
	this.canvas = canvas;
	this.parent = parent;
	this.propertyManager = new PropertyManager(this, canvas.getDocument());
	this.instance = instance;
    }

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param drawable the drawable to be added to this figure
     */
    public void addDrawable(Drawable drawable){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param figure the figure to be added to this figure
     */
    public void addFigure(SchematicFigure figure){}


    /**
     * Add the specified edge figure to the list of edges that this node
     * stores.
     *
     * @param edge a SchematicEdgeFigure which terminates at this node
     */
    public void addInEdge(SchematicEdgeFigure edge)
    {
	Connection connection = (Connection)edge.getUserObject();
	Port port = connection.getDestPort();
	Instance instance = port.getInstance();

	if(port.getInstance() != instance)
	    return;
    
	edges.put(edge.toString(), edge);

	PortFigure portFigure = 
	    (PortFigure)getFigureByName(instance.getFullyQualifiedName()+
					"."+port.getName());
	portFigure.addConnection((PluggableConnectionFigure)edge);
    }

    /**
     * Add the specified edge figure to the list of edges that this node
     * stores.
     *
     * @param edge a SchematicEdgeFigure which begins at this node
     */
    public void addOutEdge(SchematicEdgeFigure edge)
    {
	Connection connection = (Connection)edge.getUserObject();
	Port port = connection.getSourcePort();
	Instance instance = port.getInstance();

	if(instance != this.instance)
	    return;
    
	edges.put(edge.toString(), edge);

	PortFigure portFigure = 
	    (PortFigure)getFigureByName(instance.getFullyQualifiedName()+
					"."+port.getName());
	portFigure.addConnection((PluggableConnectionFigure)edge);
    }

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     * @param oldBounds the old bounding rectangle for the figure
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {
    }

    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     */
    public void clear(){}

    /**
     * Determine if the supplied location is contained in this figure.  This
     * is calculated by determining wheter or not the given location is
     * contained in the union of the bounding regions of all of the Drawable
     * objects that this figure owns.
     *
     * @param x the x coordinate in question
     * @param y the y coordinate in question
     */
    public boolean contains(int x, int y)
    {
	if(getBounds().contains(x, y)){
	    return true;
	}
	return false;
    }

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	JPopupMenu popup = new JPopupMenu();

	JMenuItem showPropertiesItem = new JMenuItem("View Visual Properties");
	showPropertiesItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    PluggableInstanceFigure.this.updateCachedProperties();
		    PluggableInstanceFigure.this.propertyManager.
			showPropertiesDialog();
		}
	    });

	popup.add(showPropertiesItem);

	if(!instance.isLeaf()){
	    JMenuItem viewInternalsItem = 
		new JMenuItem("View Hierarchy");
	    viewInternalsItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			LSESchematicView view = (LSESchematicView)
			    PluggableInstanceFigure.this.canvas.getView();
			view.showChildView(getFullyQualifiedName());
		    }
		});
	    popup.add(viewInternalsItem);
	}

	JMenuItem viewModuleCodeItem = new JMenuItem("View Module Code");
	viewModuleCodeItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    CodeObtainable code = instance.getModuleCode();
		    SourceView sourceView = 
			new SourceView(code.getOriginalCode());

		    sourceView.setVisible(true);
		}
	    });
	popup.add(viewModuleCodeItem);

	JMenuItem viewSourceItem = new JMenuItem("View Module Source File");
	viewSourceItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    CodeObtainable code = instance.getModuleCode();
		    SourceView sourceView = 
			new SourceView(new File(code.getFileName()), 
				       code.getLine());

		    sourceView.setVisible(true);
		}
	    });
	popup.add(viewSourceItem);

	JMenuItem viewInstanceItem = new JMenuItem("View Instance Data");
	viewInstanceItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    LSSInfoDialog.showDialog(null, PluggableInstanceFigure.
					     this.instance);
		}
	    });
	popup.add(viewInstanceItem);
        return popup;
    }

    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     *
     * @return null
     */
    public Collection getDrawables(){return null;}

    /**
     * Returns the Collection of edges that leave from or terminate at this 
     * node.
     *
     * @return a Collection of the edges that leave from or terminate at this
     *         node
     */
    public Collection getEdges()
    {
	return edges.values();
    }

    /**
     * Returns the Collection of edges that terminate at this node.
     *
     * @return a Collection of edges that terminate at this node
     */
    public Collection getInEdges()
    {
	Iterator iterator = edges.values().iterator();
	HashSet inEdges = new HashSet();

	while(iterator.hasNext()){
	    SchematicEdgeFigure edge = (SchematicEdgeFigure)iterator.next();

	    if(edge.getDestFigure() == this)
		inEdges.add(edge);
	}
	return inEdges;
    }

    /**
     * Return the fully qualified name of this figure as given by the IMR
     * instance's fully qualified name.
     *
     * @return the fully qualified name of this figure as given by the IMR
     *         instance's fully qualified name
     */
    public String getFullyQualifiedName()
    {
	return instance.getFullyQualifiedName();
    }

    /**
     * Return the short name of this figure as given by the IMR instance's name
     *
     * @return the short name of this figure
     */
    public String getName()
    {
	return instance.getName();
    }

    /**
     * Returns the Collection of edges that begin at this node.
     *
     * @return a Collection of edges that begin at this node
     */
    public Collection getOutEdges()
    {
	Iterator iterator = edges.values().iterator();
	HashSet outEdges = new HashSet();

	while(iterator.hasNext()){
	    SchematicEdgeFigure edge = (SchematicEdgeFigure)iterator.next();

	    if(edge.getSrcFigure() == this)
		outEdges.add(edge);
	}
	return outEdges;
    }

    /**
     * Return's this figure's parent SchematicCanvasContainer object.
     *
     * @return the parent SchematicCavnasContainer object for this figure
     */
    public SchematicCanvasContainer getParent()
    {
	return parent;
    }

    /**
     * Returns the PropertyInfo object representing the named property or null
     * if this figure does not define the specified property.
     *
     * @param name the name of the sought property
     * @return the PropertyInfo object for the named property or null
     */
    public PropertyInfo getProperty(String name)
    {
	return propertyManager.getProperty(name);
    }

    /**
     * Return the PropertyManager object responsible for managing this 
     * figure's properties.
     *
     * @return the PropertyManager object for this figure
     */
    public PropertyManager getPropertyManager()
    {
	return propertyManager;
    }

    /**
     * This is a stub implementation for figures that may not needd to 
     * implement this function.
     *
     * @return null
     */
    public Collection getSubFigures(){return null;}

    /**
     * This function returns a String to be displayed in a tooltip when
     * the mouse hovers over this figure.
     *
     * @return the tooltip text to be displayed
     */
    public String getTooltipText()
    {
	return instance.getName()+" : "+instance.getModuleName();
    }

    /**
     * Returns the user object for this PluggableInstanceFigure, this value
     * will always be the IMR instance that this figure represents.
     *
     * @return the user object for this figure
     */
    public Object getUserObject()
    {
	return instance;
    }

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param text the string which represents an action to be carried out
     *             by this figure
     */
    public void handleCommand(String command){}

    /**
     * This method is called by the SchematicCanvas that owns this figure
     * when a double click event occurs on this figure.
     *
     * @param x the x coordinate of the double click event
     * @param y the y coordinate of the double click event
     */
    public void handleDoubleClick(int x, int y)
    {
	LSSInfoDialog.showDialog(null, instance);
    }

    /**
     * Lays out the ports on this figure provided that they have not been
     * already been positioned by their stored properties.  This implementation
     * is empty, but subclasses of the PluggableInstanceFigure should 
     * implement this.
     *
     *
     */
    public void layoutPorts(){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param info the PropertyInfo object that caused this event
     */
    public void propertyChanged(PropertyInfo info){}
    
    /**
     * Repaint this figure.
     *
     */
    public void repaint()
    {
	canvas.repaint(getPaintBounds());
    }

    /**
     * Set the parent of this figure to be the SchematicCanvasContainer defined
     * by the parameter parent.
     *
     * @param parent the new SchematicCanvasContainer that owns this figure
     */
    public void setParent(SchematicCanvasContainer parent)
    {
	this.parent = parent;
    }

    /**
     * Report this figure's current property state to the LSSDocument which
     * will eventually write them to a file.
     *
     */
    public void storeProperties()
    {
	updateCachedProperties();

	propertyManager.storeProperties();
    }

    /**
     * Return a string representation of this figure.
     *
     * @return a string representation of this figure
     */
    public String toString()
    {
	return instance.getName();
    }

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     */
    public void updateCachedProperties(){}
}
