package Liberty.visualizer.canvas.figure.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.widget.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.widget.*;
import Liberty.visualizer.view.*;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.File;
import java.util.*;

import javax.swing.*;

/**
 * GenericInstanceFigure is the base figure used to draw IMR instances on 
 * SchematicCanvas.  It will determine the bounding regions for selecting
 * canvas elements and rendering them.  The bounding region for clicking on
 * a GenericInstanceFigure is defined by the Drawable objects that it owns.
 * The bounding region for repainting a figure is defined as the union of
 * the bounding regions of the Drawable objects, and the SchematicFigures(both
 * SchematicEdgeFigures and SchematicNodeFigures) that it owns.
 *
 * @see GenericInstanceFigure
 * @see DefaultInstanceFigure
 *
 * @author Jason Blome
 * @version 0.1
 *
 */

public class GenericInstanceFigure extends PluggableInstanceFigure{

    /** the bounding region of this figure defined by the Drawable objects
     * that it owns */
    protected Rectangle bounds;
    /** an ordered hashset of the Drawable objects that this figure owns */
    protected LinkedHashMap drawables;
    /** an ordered hashset of the SchematicFigures that this figure owns */
    protected LinkedHashMap figures;
    /** is this figure currently selected */
    protected boolean isSelected;
    /** is this figure currently visible */
    protected boolean isVisible;
    /** the absolute location of this figure on the canvas */
    protected Point location;
    /** the bounding region used when repainting this SchematicFigure */
    protected Rectangle paintBounds;

    /**
     * The sole constructor for the GenericInstanceFigure.  Note that any 
     * class that extends PluggableInstanceFigure must call super with the
     * three parameters specified below as no empty constructor is specified 
     * for the abstract PluggableInstanceFigure class.
     *
     * @param canvas the canvas that this GenericInstanceFigure
     *               will be rendered upon
     * @param parent the parent of this 
     *               GenericInstanceFigure, which may be any class
     *               that implements SchematicCanvasContainer
     * @param instance the IMR instance obtained from the LSE 
     *               compiler
     */
    public GenericInstanceFigure(SchematicCanvas canvas,
				 SchematicCanvasContainer parent,
				 Instance instance)
    {
	super(canvas, parent, instance);

	this.drawables = null;
	this.figures = null;
	this.isSelected = false;
	this.isVisible = true;
	this.location = new Point(0, 0);
	this.bounds = new Rectangle(location);
	this.paintBounds = new Rectangle(location);
    }

    /**
     * Add the specified Drawable to the set of Drawable objects that this
     * figure is responsible for painting.
     *
     * @param drawable the Drawable to be added to this figure
     */
    public void addDrawable(Drawable drawable)
    {
	if(drawables == null)
	    drawables = new LinkedHashMap();

	drawables.put(drawable.getName(), drawable);

	bounds = bounds.createUnion(drawable.getBounds()).getBounds();
	paintBounds = paintBounds.
	    createUnion(drawable.getBounds()).getBounds();
    }

    /**
     * Add the specified SchematicFigure to the set of SchematicFigure 
     * objects that this figure is responsible for painting.
     *
     * @param figure the SchematicFigure to be added to this figure
     */
    public void addFigure(SchematicFigure figure)
    {
	if(figures == null)
	    figures = new LinkedHashMap();

	figures.put(figure.getFullyQualifiedName(), figure);

	paintBounds = paintBounds.
	    createUnion(figure.getPaintBounds()).getBounds();
    }    

    /**
     * Remove all figures and drawables from this figure.
     *
     */
    public void clear()
    {
	drawables.clear();
	figures.clear();
    }

    /**
     * The SchematicCanvas that owns this SchematicFigure will call this
     * method whenever a drag event involving this figure occurs.
     *
     * @param dx the horizontal distance traversed by the drag event
     * @param dy the vertical distance traversed by the drag even
     * @param eventPoint the initial point at which the mouse down event
     *           occurred
     */
    public void drag(int dx, int dy, Point eventPoint)
    {
	translate(dx, dy);

	if(parent != null)
	    parent.validateChildLocation(this, eventPoint);
    }

    /**
     * Calculate the bounding region of this figure.  This is carried out by
     * calculating the union of all Drawable objects that this SchematicFigure
     * owns.  This value is used by the SchematicCanvas to determine the
     * clickable region of a figure.
     *
     * @return the bounding rectangle of this figure 
     */
    public Rectangle getBounds()
    {
	Rectangle ret = new Rectangle(bounds);

	return ret;
    }

    /**
     * This calculates the location at which a SchematicEdgeFigure connected
     * to this SchematicNodeFigure will terminate.  The
     * GenericInstanceFigure will simply return the center of its bounding
     * region.
     *
     * @param edge the edge that is connected to this ScchematicNodeFigure
     * @return the Point object where the specified edge will terminate
     */
    public Point getDestConnectLocation(SchematicEdgeFigure edge)
    {
	Rectangle bounds = getBounds();
	return new Point(bounds.x + Math.round(bounds.width/2), 
			 bounds.y + Math.round(bounds.height/2));
    }

    /**
     * This calculates the location at which a SchematicEdgeFigure connected
     * to this SchematicNodeFigure will terminate.  The
     * GenericInstanceFigure will simply return the center of its bounding
     * region.
     *
     * @param edge the edge that is connected to this ScchematicNodeFigure
     * @return the Point object where the specified edge will terminate
     */
    public Point getSourceConnectLocation(SchematicEdgeFigure edge)
    {
	Rectangle bounds = getBounds();
	return new Point(bounds.x + Math.round(bounds.width/2), 
			 bounds.y + Math.round(bounds.height/2));
    }

    /**
     * Returns the face of this SchematicFigure that the specified edge is
     * connected to, where face belongs to the set {EAST, WEST, NORTH, SOUTH,
     * CENTER}
     *
     * @param edge the edge that is connected to this ScchematicNodeFigure
     * @return the face on this figure that the specified edge is connected to
     */
    public int getDestConnectSite(SchematicEdgeFigure edge)
    {
	return CENTER;
    }

    /**
     * Returns the face of this SchematicFigure that the specified edge is
     * connected to, where face belongs to the set {EAST, WEST, NORTH, SOUTH,
     * CENTER}
     *
     * @param edge the edge that is connected to this ScchematicNodeFigure
     * @return the face on this figure that the specified edge is connected to
     */
    public int getSourceConnectSite(SchematicEdgeFigure edge)
    {
	return CENTER;
    }

    /**
     * Return a collection of the drawables that this figure owns.
     *
     * @return a collection of the drawables owned by this figure
     */
    public Collection getDrawables()
    {
	return drawables.values();
    }

    /**
     * Return a collection of the figures that this figure owns.
     *
     * @return a collection of the figures owned by this figure
     */
    public Collection getSubFigures()
    {
	return figures.values();
    }

    /**
     * Returns the absolute location of this SchematicFigure on the 
     * SchematicCanvas that owns it.
     *
     * @return a Point representing the absolute location of this figure
     *         on the canvas
     */
    public Point getLocation()
    {
	return new Point(location);
    }

    /**
     * Return a SchematicFigure owned by this figure which contains the
     * specified location.  This function may return this figure, a
     * sub-figure, or null.
     *
     * @param x the x coordinate in question
     * @param y the y coordinate in question
     * @return a figure which contains the specified point or null
     */
    public SchematicFigure getFigureAt(int x, int y)
    {
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		if(figure.contains(x, y))
		    return figure;
	    }
	}
	return null;
    }

    /**
     * Return a SchematicFigure owned by this figure, which has the 
     * specified name or null.
     *
     * @param name the name of the figure sought
     * @return a figure bearing the specified name or null
     */
    public SchematicFigure getFigureByName(String name)
    {
	return (SchematicFigure)figures.get(name);
    }

    /**
     *
     * Return a SchematicFigure which has the specified Object as its
     * user object.
     *
     * @param o the user object belonging to the figure sought
     * @return the figure which has the specified Object as its user object
     *         or null
     */
    public SchematicFigure getFigureByUserObject(Object o)
    {
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		if(figure.getUserObject() == o)
		    return figure;
	    }
	}
	return null;
    }

    /**
     * Returns a rectangle that bounds the region that needs to rendered 
     * when this figure is painted.
     *
     * @return a rectangle that bounds the region that needs to be rendered
     *         when this figure is painted
     */
    public Rectangle getPaintBounds()
    {
	Rectangle ret = new Rectangle(paintBounds);

	// Now we add the boundaries for the edge figures.
	// The repaint regions for edges will change frequently, so we
	// always recompute their bounds.
	Iterator iterator = edges.values().iterator();
	while(iterator.hasNext()){
	    SchematicFigure edgeFigure = (SchematicFigure)iterator.next();
		ret = ret.createUnion(edgeFigure.getPaintBounds()).getBounds();
	}
	return ret;
    }
    
    /**
     * Parse the text and perform the action specified by the text parameter 
     * if it is defined by this figure, otherwise ignore the text.
     *
     * @param text the string which represents an action to be carried out
     *             by this figure
     */
    public void handleCommand(String text)
    {
	//FIXME:
    }

    /**
     * Set this figures properties to reflect the properties defined in the
     * propertySet, if they are applicable.
     *
     * @param propertySet a set of PropertyInfo objects
     */
    public void handleStoredProperties(java.util.List propertyList)
    {
	//FIXME:
    }

    /**
     * Initialize the properties that this figure manages.
     *
     */
    private void initializeProperties()
    {
	//FIXME:
    }

    /**
     * Return whether or not this figure is currently selected.
     *
     * @return whether this figure is currently selected
     */
    public boolean isSelected()
    {
	return isSelected;
    }

    /**
     * Return whether or not this figure is currently visible.
     *
     * @return whether this figure is currently visible
     */
    public boolean isVisible()
    {
	return isVisible;
    }

    /**
     * Paint this figure using the specified Graphics2D object.
     *
     * @param g2d the graphics object to use when painting this figure
     */
    public void paint(Graphics2D g2d)
    {
	if(isVisible){	    
	    if(drawables != null){
		Iterator drawableIterator = drawables.values().iterator();
		while(drawableIterator.hasNext()){
		    Drawable drawable = (Drawable)drawableIterator.next();
		    
		    drawable.paint(g2d);
		}
	    }

	    if(figures != null){
		Iterator figureIterator = figures.values().iterator();
		while(figureIterator.hasNext()){
		    SchematicFigure figure = 
			(SchematicFigure)figureIterator.next();
		    
		    figure.paint(g2d);
		}
	    }
	}
    }

    /**
     * Signal to this figure that a property change event has occurred and
     * that it should update its state accordingly.
     *
     * @param info the PropertyInfo object that caused this event
     */
    public void propertyChanged(PropertyInfo info)
    {
	//FIXME:
    }

    /**
     * Set the location of this figure on the canvas.
     *
     * @param x the new x coordinate for this figure
     * @param y the new y coordinate for this figure
     */
    public void setLocation(int x, int y)
    {
	int dx, dy;
	dx = x - location.x;
	dy = y - location.y;
	this.location.setLocation(x, y);

	// translate all of the figures
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		
		figure.translate(dx, dy);
	    }
	}

	// translate all of the drawables
	if(drawables != null){
	    Iterator drawableIterator = drawables.values().iterator();
	    while(drawableIterator.hasNext()){
		Drawable drawable = (Drawable)drawableIterator.next();
		
		drawable.translate(dx, dy);
	    }
	}

	bounds.translate(dx, dy);
	paintBounds.translate(dx, dy);
    }

    /**
     * Set the selected status of this figure.
     *
     * @param b the boolean value specifying whether this figure is selected
     *          or not
     */
    public void setSelected(boolean b)
    {
	isSelected = b;
    }

    /**
     * Set the visible status of this figure.
     *
     * @param b the boolean value specifying whether this figure is visible
     *          or not
     */
    public void setVisible(boolean b)
    {
	isVisible = b;
    }

    /**
     * This function is used to set the location of this figure to x and y
     * coordinates which are divisible by the integer value 10.
     *
     */
    public void snapToGrid()
    {
	int xCoord = location.x;
	int yCoord = location.y;

	if(xCoord % 10 != 0){
	    int base = Math.round(xCoord/10);
	    xCoord = base * 10;
	}
	if(yCoord % 10 != 0){
	    int base = Math.round(yCoord/10);
	    yCoord = base * 10;
	}
	setLocation(xCoord, yCoord);

	Iterator figureIterator = figures.values().iterator();
	while(figureIterator.hasNext()){
	    SchematicFigure figure = (SchematicFigure)figureIterator.next();
	    if(figure instanceof PortFigure)
		figure.snapToGrid();
	}
    }

    /**
     * Translate this figure the specified horizontal and vertical distances.
     *
     * @param dx the horizontal distance to translate this figure
     * @param dy the vertical distance to translate this figure
     */
    public void translate(int dx, int dy)
    {
	int x = location.x + dx;
	int y = location.y + dy;

	this.location.translate(dx, dy);

	// translate all of the figures
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();

		figure.translate(dx, dy);
	    }
	}

	// translate all of the drawables
	if(drawables != null){
	    Iterator drawableIterator = drawables.values().iterator();
	    while(drawableIterator.hasNext()){
		Drawable drawable = (Drawable)drawableIterator.next();
		
		drawable.translate(dx, dy);
	    }
	}

	bounds.translate(dx, dy);
	paintBounds.translate(dx, dy);
    }

    /**
     * Update any PropertyInfo objects for values which may be cached in local
     * variables
     *
     */
    public void updateCachedProperties()
    {
	PropertyInfo pInfo = propertyManager.getProperty("X Coordinate");
	pInfo.setValue(new Integer(location.x));

	pInfo = propertyManager.getProperty("Y Coordinate");
	pInfo.setValue(new Integer(location.y));
    }


    /**
     * Recalculate the bounding regions for this figure if they are cached.
     *
     */
    public void validateBounds()
    {
 	this.bounds = new Rectangle(location);

	// get the bounds for all of the drawables	
	if(drawables != null){
	    Iterator drawableIterator = drawables.values().iterator();
	    while(drawableIterator.hasNext()){
		Drawable drawable = (Drawable)drawableIterator.next();
		
		bounds = bounds.createUnion(drawable.getBounds()).getBounds();
	    }
	}

	this.paintBounds = new Rectangle(bounds);

	// get the paintBounds all of the figures
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		
		paintBounds = paintBounds.
		    createUnion(figure.getPaintBounds()).getBounds();
	    }
	}

	// just to make sure...
	paintBounds.setRect(paintBounds.x - 1, paintBounds.y - 1,
			    paintBounds.width + 2, paintBounds.height + 2);
    }

    /**
     * Check the location of a figure owned by this figure and if it is in an
     * invalid location, set its location accordingly.
     *
     * @param child the child figure whose location is being validated
     * @param mouseLocation a hint in determining where the location of the
     *                      child figure should be
     */
    public void validateChildLocation(SchematicFigure child, 
				      Point mouseLocation)
    {
	//FIXME:
    }
}
