package Liberty.visualizer.canvas.figure.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;

import java.awt.*;
import java.awt.geom.*;
import java.io.File;
import java.util.*;
import javax.swing.*;

/**
 * The SegmentFigure is used to draw a line between two figures.  Specifically
 * it is used by PluggableConnectionFigures to draw the line segments which 
 * represent a connection between two PluggableInstanceFigures.
 *
 * @see PluggableConnectionFigure
 * @see PluggableInstanceFigure
 *
 * @author Jason Blome
 * @version 0.1
 */

public class SegmentFigure implements SchematicFigure{

    /** whether or not this figure may be dragged by the user */
    private boolean isDraggable;
    /** whether or not this figure is visible*/
    private boolean isVisible;
    /** the source figure of this segment */
    private SchematicFigure srcFigure;
    /** the destination figure of this segment */
    private SchematicFigure destFigure;
    /** the color used to draw this connection */
    private Color fillColor;
    /** this figure's parent */
    private PluggableConnectionFigure parent;
    /** the color used to draw this connection when it is selected */
    private Color selectedFillColor;    
    /** the points used to draw the line segment */
    private Point srcPoint, destPoint;
    /** the canvas that this figure will be drawn on */
    private SchematicCanvas canvas;
    
    /**
     * Constructor for the SegmentFigure, requires only a parent 
     * PluggableConnectionFigure.
     *
     * @param parent this figure's parent
     */
    public SegmentFigure(PluggableConnectionFigure parent,
			 SchematicCanvas canvas)
    {
	this.canvas = canvas;
	this.parent = parent;
	this.isVisible = true;
	this.isDraggable = true;
	this.fillColor = Color.black;
	this.selectedFillColor = new Color(0xe51d27);
    }

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param drawable the drawable to be added to this figure
     */
    public void addDrawable(Drawable drawable){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param figure the figure to be added to this figure
     */
    public void addFigure(SchematicFigure figure){}

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     * @param oldBounds the old bounding rectangle for the figure
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {}


    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     */
    public void clear(){}

    /**
     * Determine if the supplied location is contained in this figure.
     *
     * @param x the x coordinate in question
     * @param y the y coordinate in question
     */
    public boolean contains(int x, int y)
    {
	if(srcPoint != null && destPoint != null){
	    Line2D.Float line = new Line2D.Float(srcPoint, destPoint);
	    return (line.ptSegDist(x, y) <= 2);
	}
	return false;
    }

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	return null;
    }

    /**
     * The SchematicCanvas that owns this SchematicFigure will call this
     * method whenever a drag event involving this figure occurs.
     *
     * @param dx the horizontal distance traversed by the drag event
     * @param dy the vertical distance traversed by the drag even
     * @param eventPoint the initial point at which the mouse down event
     *           occurred
     */
    public void drag(int dx, int dy, Point eventPoint)
    {
	int originatingX = eventPoint.x - dx;
	int originatingY = eventPoint.y - dy;

	if(srcFigure.contains(originatingX, originatingY)){
	    if(srcFigure instanceof JogPointFigure)
		srcFigure.translate(dx, dy);
	}
	else if(destFigure.contains(originatingX, originatingY)){
	    if(destFigure instanceof JogPointFigure)
		destFigure.translate(dx, dy);
	}
	else{
	    if(isDraggable){
		srcFigure.translate(dx, dy);
		destFigure.translate(dx, dy);
	    }
	}
    }

    /**
     * Calculate the bounding region of this figure.  
     * This value is used by the SchematicCanvas to determine the
     * clickable region of a figure.
     *
     * @return the bounding rectangle of this figure 
     */
    public Rectangle getBounds()
    {
	Rectangle ret = srcFigure.getBounds().
	    createUnion(destFigure.getBounds()).getBounds();

	return ret;
    }

    /**
     * Returns the SchematicFigure that is the destination of this line segment
     *
     * @return the figure that is the destination of this line segment
     */
    public SchematicFigure getDestFigure()
    {
	return destFigure;
    }

    /**
     * Returns the terminating point for this line segment.
     *
     * @return the destination point of this line segment
     */
    public Point getDestPoint()
    {
	return destPoint;
    }

    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     *
     * @return null
     */
    public Collection getDrawables()
    {
	return null;
    }

    /**
     * Return a SchematicFigure owned by this figure which contains the
     * specified location.  This function may return this figure, a
     * sub-figure, or null.
     *
     * @param x the x coordinate in question
     * @param y the y coordinate in question
     * @return a figure which contains the specified point or null
     */
    public SchematicFigure getFigureAt(int x, int y)
    {
	if(srcFigure.contains(x, y))
	    return parent;
	if(destFigure.contains(x, y))
	    return parent;

	return null;
    }

    /**
     * Return a SchematicFigure owned by this figure, which has the 
     * specified name or null.
     *
     * @param name the name of the figure sought
     * @return a figure bearing the specified name or null
     */
    public SchematicFigure getFigureByName(String name)
    {
	return null;
    }

    /**
     * Return a SchematicFigure which has the specified Object as its
     * user object.
     *
     * @param o the user object belonging to the figure sought
     * @return the figure which has the specified Object as its user object
     *         or null
     */
    public SchematicFigure getFigureByUserObject(Object o)
    {
	return null;
    }

    /**
     * Return the fully qualified name of this figure as given by the IMR
     * connection's fully qualified name.
     *
     * @return the fully qualified name of this figure as given by the IMR
     *         connection's fully qualified name
     */
    public String getFullyQualifiedName()
    {
	return parent.getFullyQualifiedName();
    }

    /**
     * Returns the SchematicFigure that is the source of this line segment
     *
     * @return the figure that is the source of this line segment
     */
    public SchematicFigure getSrcFigure()
    {
	return srcFigure;
    }

    /**
     * This is a stub implementation for figures that may not needd to 
     * implement this function.
     *
     * @return null
     */
    public Collection getSubFigures()
    {
	return null;
    }

    /**
     * The SegmentFigure does not define a location, 
     * hence this function will always return null.
     *
     * @return null
     */
    public Point getLocation()
    {
	return null;
    }

    /**
     * Return the short name of this figure as given by the IMR connection's 
     * name
     *
     * @return the short name of this figure
     */
    public String getName()
    {
	return parent.getName();
    }

    /**
     * Returns a rectangle that bounds the region that needs to rendered 
     * when this figure is painted.
     *
     * @return a rectangle that bounds the region that needs to be rendered
     *         when this figure is painted
     */
    public Rectangle getPaintBounds()
    {
	Rectangle ret = srcFigure.getBounds().
	    createUnion(destFigure.getBounds()).getBounds();

	ret.setRect(ret.x - 2, 
		    ret.y - 2, 
		    ret.width + 4, 
		    ret.height + 4);

	return ret;
    }

    /**
     * Return's this figure's parent SchematicCanvasContainer object.
     *
     * @return the parent SchematicCavnasContainer object for this figure
     */
    public SchematicCanvasContainer getParent()
    {
	return parent;
    }

    /**
     * This function returns a String to be displayed in a tooltip when
     * the mouse hovers over this figure.
     *
     * @return the tooltip text to be displayed
     */
    public String getTooltipText()
    {
	return parent.getTooltipText();
    }

    /**
     * Returns the user object for this SegmentFigure this will be 
     *.the user object of the parent connection figure
     *
     * @return null
     */
    public Object getUserObject()
    {
	return parent.getUserObject();
    }

    /**
     * Parse the text and perform the action specified by the text parameter 
     * if it is defined by this figure, otherwise ignore the text.
     *
     * @param text the string which represents an action to be carried out
     *             by this figure
     */
    public void handleCommand(String command)
    {

    }

    /**
     * This method is called by the SchematicCanvas that owns this figure
     * when a double click event occurs on this figure.
     *
     * @param x the x coordinate of the double click event
     * @param y the y coordinate of the double click event
     */
    public void handleDoubleClick(int x, int y){}

    /**
     * Return whether or not this figure is currently selected.
     *
     * @return whether this figure is currently selected
     */
    public boolean isSelected()
    {
	return parent.isSelected();
    }

    /**
     * Returns the source point for this line segment.
     *
     * @return the source point of this line segment
     */
    public Point getSourcePoint()
    {
	return srcPoint;
    }

    /**
     * Return whether or not this figure is currently visible.
     *
     * @return whether this figure is currently visible
     */
    public boolean isVisible()
    {
	return isVisible;
    }

    /**
     * Paint this figure using the specified Graphics2D object.
     *
     * @param g2d the graphics object to use when painting this figure
     */
    public void paint(Graphics2D g2d)
    {
	if(isVisible){
	    if(parent.isVisible()){
		Rectangle srcFigureBounds = srcFigure.getBounds();
		Rectangle destFigureBounds = destFigure.getBounds();
		srcPoint = new Point();
		destPoint = new Point();
		Line2D.Float lineSegment;
		
		srcPoint.setLocation(srcFigureBounds.getX() +
				     (int)(srcFigureBounds.getWidth()/2),
				     srcFigureBounds.getY() +
				     (int)(srcFigureBounds.getHeight()/2));
		
		destPoint.setLocation(destFigureBounds.getX() +
				      (int)(destFigureBounds.getWidth()/2),
				      destFigureBounds.getY()+
				      (int)(destFigureBounds.getHeight()/2));
		
		lineSegment = new Line2D.Float(srcPoint, destPoint);
		g2d.setPaint(isSelected() ? selectedFillColor : fillColor);
		g2d.draw(lineSegment);
		g2d.fill(lineSegment);
		srcFigure.paint(g2d);
		destFigure.paint(g2d);
	    }
	}
    }

    /**
     * Repaint this figure.
     *
     */
    public void repaint(){}

    /**
     * Set the destination SchematicFigure for this SegmentFigure.
     *
     * @param destFigure the destination figure for this line segment
     */
    public void setDestFigure(SchematicFigure destFigure)
    {
	this.destFigure = destFigure;
	if(srcFigure != null){
	    if(srcFigure instanceof JogPointFigure && 
	       destFigure instanceof JogPointFigure)
		isDraggable = true;
	    else
		isDraggable = false;
	}
	else{
	    if(destFigure instanceof PortInstanceFigure)
		isDraggable = false;
	    else
		isDraggable = true;
	}

    }

    /**
     * Set the fill color for this SegmentFigure.
     *
     * @param c the color to be used for a fill color in painting this figure
     */
    public void setFillColor(Color c)
    {
	this.fillColor = c;
    }

    /**
     * Do nothing.
     *
     * @param x the new x coordinate for this figure
     * @param y the new y coordinate for this figure
     */
    public void setLocation(int x, int y){}

    /**
     * Set the parent of this figure to be the SchematicCanvasContainer defined
     * by the parameter parent.
     *
     * @param parent the new SchematicCanvasContainer that owns this figure
     */
    public void setParent(SchematicCanvasContainer parent)
    {
	if(parent instanceof PluggableConnectionFigure)
	    this.parent = (PluggableConnectionFigure)parent;
    }

    /**
     * Set the selected status of this figure.
     *
     * @param b the boolean value specifying whether this figure is selected
     *          or not
     */
    public void setSelected(boolean b)
    {
	parent.setSelected(b);
    }

    /**
     * Set the fill color for this SegmentFigure when the figure 
     * is selected
     *
     * @param c the color to be used for a fill color in painting this figure
     *          when the figure is selected
     */
    public void setSelectedFillColor(Color c)
    {
	this.selectedFillColor = c;
    }

    /**
     * Set the source SchematicFigure for this SegmentFigure.
     *
     * @param srcFigure the source figure for this line segment
     */
    public void setSourceFigure(SchematicFigure srcFigure)
    {
	this.srcFigure = srcFigure;
	if(destFigure != null){
	    if(srcFigure instanceof JogPointFigure && 
	       destFigure instanceof JogPointFigure)
		isDraggable = true;
	    else
		isDraggable = false;
	}
	else{
	    if(srcFigure instanceof PortInstanceFigure)
		isDraggable = false;
	    else
		isDraggable = true;
	}
    }

    /**
     * Set the visible status of this figure.
     *
     * @param b the boolean value specifying whether this figure is visible
     *          or not
     */
    public void setVisible(boolean b){
	isVisible = b;
    }

    /**
     * This function is used to set the location of this figure to x and y
     * coordinates which are divisible by the integer value 10.
     *
     */
    public void snapToGrid()
    {
	srcFigure.snapToGrid();
	destFigure.snapToGrid();
    }

    /**
     * Return a string representation of this figure.
     *
     * @return a string representation of this figure
     */
    public String toString()
    {
	return parent.getName();
    }

   /**
     * Do nothing.
     *
     * @param dx the horizontal distance to translate this figure
     * @param dy the vertical distance to translate this figure
     */
    public void translate(int dx, int dy){}

    /**
     * Recalculate the bounding regions for this figure if they are cached.
     *
     */
    public void validateBounds(){}

    /**
     * Check the location of a figure owned by this figure and if it is in an
     * invalid location, set its location accordingly.
     *
     * @param child the child figure whose location is being validated
     * @param mouseLocation a hint in determining where the location of the
     *                      child figure should be
     */
    public void validateChildLocation(SchematicFigure child, Point eventPoint)
    {
    }
}
