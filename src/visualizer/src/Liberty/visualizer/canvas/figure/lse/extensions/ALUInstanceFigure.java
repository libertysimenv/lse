package Liberty.visualizer.canvas.figure.lse.extensions;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.lse.*;
import Liberty.visualizer.canvas.figure.widget.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.widget.*;
import Liberty.visualizer.view.*;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.File;
import java.util.*;

import javax.swing.*;

/**
 * The ALUInstanceFigure can be used to represent functional units in an LSE 
 * configuration, it is designed to be rendered like a typical ALU in
 * a block diagram.
 *
 * @see GenericInstanceFigure
 * @see PluggableInstanceFigure
 *
 * @author Jason Blome
 * @version 0.1
 */

public class ALUInstanceFigure extends GenericInstanceFigure{

    /** the label for this figure */
    protected LabelDrawable labelDrawable;
    /** the position of the label for this figure */
    protected int labelPlacement;
    /** the shape for this figure */
    protected ShapeDrawable shapeDrawable;
    /** a cached map of PortInstanceFigures, where the ConnectionFigure
     * that is connected to the port instance is used as the lookup key */
    protected HashMap srcPortInstances;
    /** a cached map of PortInstanceFigures, where the ConnectionFigure
     * that is connected to the port instance is used as the lookup key */
    protected HashMap destPortInstances;

    protected int outputFace;

    protected Polygon westPolygon;
    protected Polygon eastPolygon;
    protected Polygon northPolygon;
    protected Polygon southPolygon;

    protected String operand_1;
    protected String operand_2;
    protected String operation;
    protected String result;

    /** constant for label placement */
    public static final int ABOVE = 0;
    /** constant for label placement */
    public static final int BELOW = 1;
    /** constant for label placement */
    public static final int HEADER = 2;
    /** constant for label placement */
    public static final int FOOTER = 3;
    /** constant for label placement */
    public static final int CENTER = 4;

    /**
     * The sole constructor for the ALUInstanceFigure.  Note that any 
     * class that extends ALUInstanceFigure must call super with the
     * three parameters specified below as no empty constructor is specified 
     * for the abstract PluggableInstanceFigure class which this is a 
     * decendent of.
     *
     * @param canvas the canvas that this GenericInstanceFigure
     *               will be rendered upon
     * @param parent the parent of this 
     *               GenericInstanceFigure, which may be any class
     *               that implements SchematicCanvasContainer
     * @param instance the IMR instance obtained from the LSE 
     *               compiler
     */
    public ALUInstanceFigure(SchematicCanvas canvas,
			     SchematicCanvasContainer parent,
			     Instance instance)
    {
	super(canvas, parent, instance);

	// set the location to (-1, -1) to alert the layout class that
	// this figure has not been positioned
	this.location = new Point(-1, -1);
	this.labelPlacement = HEADER;
	this.srcPortInstances = new HashMap();
	this.destPortInstances = new HashMap();
	this.outputFace = EAST;

	// create the label for this figure
	this.labelDrawable = new LabelDrawable(canvas,
					       this,
					       "Label",
					       instance.getName());

	labelDrawable.setJustification(LabelDrawable.CENTER);
	labelDrawable.setFontSize(14);

	// position the label
	Rectangle labelBounds = labelDrawable.getBounds();
	int width = labelBounds.width > 100 ? labelBounds.width + 20 : 100;

	// build the shapes used to draw this figure
	initializePolygons(width);

	labelDrawable.translate((int)(width/2), 60-labelBounds.height);

	// create the shape for this figure
	shapeDrawable = new ShapeDrawable(canvas,
					  this, 
					  "Shape", 
					  eastPolygon, 
					  new Color(137, 183, 229, 250),
					  new Color(78, 134, 224, 250));


	initializeProperties();

	// calculate the bounds for this figure
	bounds = shapeDrawable.getBounds().
	    createUnion(labelDrawable.getBounds()).getBounds();

	paintBounds = new Rectangle(bounds);

	paintBounds.setRect(paintBounds.x - 1, 
			    paintBounds.y - 1,
			    paintBounds.width + 2,
			    paintBounds.height + 2);
    }

    /**
     * This calculates the location at which a SchematicEdgeFigure connected
     * to this SchematicNodeFigure will terminate.
     *
     * @param edge the edge that is connected to this ScchematicNodeFigure
     * @return the Point object where the specified edge will terminate
     */
    public Point getDestConnectLocation(SchematicEdgeFigure edge)
    {
	int xCoord=0, yCoord=0;

	PortInstanceFigure portInstanceFigure = 
	    (PortInstanceFigure)destPortInstances.get(edge);
	
	if(portInstanceFigure == null){
	    Connection connection = (Connection)edge.getUserObject();
	    
	    // the PortInstanceFigure is not cached, look it up
	    PortFigure portFigure = (PortFigure)
		getFigureByName(getFullyQualifiedName()+"."+
				connection.getDestPort().getName());
	    
	    portInstanceFigure = (PortInstanceFigure)portFigure.
		getFigureByName(""+connection.getDestInstance());
	    
	    destPortInstances.put(edge, portInstanceFigure);
	}
	
	Rectangle portInstBounds = portInstanceFigure.getBounds();
	
	switch(portInstanceFigure.getConnectSite()){
	case NORTH:
	    xCoord = portInstBounds.x;
		yCoord = portInstBounds.y;
	case SOUTH:
	    xCoord = portInstBounds.x;
	    yCoord = portInstBounds.y;
	case EAST:
	    xCoord = portInstBounds.x;
	    yCoord = portInstBounds.y;
	case WEST:
	    xCoord = portInstBounds.x;
	    yCoord = portInstBounds.y;
	}
  
	return new Point(xCoord, yCoord);
    }

    /**
     * This calculates the location at which a SchematicEdgeFigure connected
     * to this SchematicNodeFigure will terminate.
     *
     * @param edge the edge that is connected to this ScchematicNodeFigure
     * @return the Point object where the specified edge will terminate
     */
    public Point getSourceConnectLocation(SchematicEdgeFigure edge)
    {
	int xCoord=0, yCoord=0;

	PortInstanceFigure portInstanceFigure = 
	    (PortInstanceFigure)srcPortInstances.get(edge);
	
	if(portInstanceFigure == null){
	    Connection connection = (Connection)edge.getUserObject();
	    
	    // the PortInstanceFigure is not cached, look it up
	    PortFigure portFigure = (PortFigure)
		getFigureByName(getFullyQualifiedName()+"."+
				connection.getSourcePort().getName());
	    
	    portInstanceFigure = (PortInstanceFigure)portFigure.
		getFigureByName(""+connection.getSourceInstance());
	    srcPortInstances.put(edge, portInstanceFigure);
	}
	
	Rectangle portInstBounds = portInstanceFigure.getBounds();
	
	switch(portInstanceFigure.getConnectSite()){
	case NORTH:
	    xCoord = portInstBounds.x;
	    yCoord = portInstBounds.y;
	case SOUTH:
	    xCoord = portInstBounds.x;
	    yCoord = portInstBounds.y;
	case EAST:
	    xCoord = portInstBounds.x;
	    yCoord = portInstBounds.y;
	case WEST:
	    xCoord = portInstBounds.x;
	    yCoord = portInstBounds.y;
	}
	return new Point(xCoord, yCoord);
    }

    /**
     * Returns the face of this SchematicFigure that the specified edge is
     * connected to, where face belongs to the set {EAST, WEST, NORTH, SOUTH,
     * CENTER}
     *
     * @param edge the edge that is connected to this ScchematicNodeFigure
     * @return the face on this figure that the specified edge is connected to
     */
    public int getDestConnectSite(SchematicEdgeFigure edge)
    {

	PortInstanceFigure portInstanceFigure = 
	    (PortInstanceFigure)destPortInstances.get(edge);

	if(portInstanceFigure == null){
	    // the PortInstanceFigure is not cached
	    Connection connection = (Connection)edge.getUserObject();
		
	    PortFigure portFigure = (PortFigure)
		getFigureByName(instance.getFullyQualifiedName()+"."+
				connection.getDestPort().getName());
	    
	    portInstanceFigure = (PortInstanceFigure)portFigure.
		getFigureByName(""+connection.getDestInstance());
	    
	    destPortInstances.put(edge, portInstanceFigure);
	}
	return portInstanceFigure.getConnectSite();
    }

    /**
     * Returns the face of this SchematicFigure that the specified edge is
     * connected to, where face belongs to the set {EAST, WEST, NORTH, SOUTH,
     * CENTER}
     *
     * @param edge the edge that is connected to this ScchematicNodeFigure
     * @return the face on this figure that the specified edge is connected to
     */
    public int getSourceConnectSite(SchematicEdgeFigure edge)
    {
	PortInstanceFigure portInstanceFigure = 
	    (PortInstanceFigure)srcPortInstances.get(edge);

	if(portInstanceFigure == null){
	    // the PortInstanceFigure is not cached
	    Connection connection = (Connection)edge.getUserObject();
		
	    PortFigure portFigure = (PortFigure)
		getFigureByName(instance.getFullyQualifiedName()+"."+
				connection.getSourcePort().getName());
	    
	    portInstanceFigure = (PortInstanceFigure)portFigure.
		getFigureByName(""+connection.getSourceInstance());
	    
	    srcPortInstances.put(edge, portInstanceFigure);
	}
	return portInstanceFigure.getConnectSite();
    }

    /**
     * Parse the text and perform the action specified by the text parameter 
     * if it is defined by this figure, otherwise ignore the text.
     *
     * @param text the string which represents an action to be carried out
     *             by this figure
     */
    public void handleCommand(String text)
    {
	//FIXME: this needs to resize, etc for the show table command
	// also, the parser should be more intelligent
	int parameterBeginIndex = text.indexOf("(");
	int parameterEndIndex = text.indexOf(")");

	if(parameterBeginIndex < 0 || parameterEndIndex < 0){
	    // FIXME:report an error
	    return;
	}
       
	String command = text.substring(0, parameterBeginIndex);
	String parameters = text.substring(parameterBeginIndex+1, 
					   parameterEndIndex);
	StringTokenizer tokenizer = new StringTokenizer(parameters, ", ");
	String parameterList[] = new String[tokenizer.countTokens()];
	int i = 0;

	while(tokenizer.hasMoreTokens()){
	    String parameter = tokenizer.nextToken();

	    parameterList[i++] = parameter;
	}

	// handle ALUInstanceFigure commands here
    }

    /**
     * Set this figures properties to reflect the properties defined in the
     * propertySet, if they are applicable.
     *
     * @param propertySet a set of PropertyInfo objects
     */
    public void handleStoredProperties(java.util.List propertyList)
    {
	Vector commands = new Vector();
	Document document = canvas.getDocument();

	Iterator propertyIterator = propertyList.iterator();
	while(propertyIterator.hasNext()){
	    PropertyInfo info = (PropertyInfo)propertyIterator.next();
	    Object value = info.getValue();
	    String propertyName = info.getPropertyName();
	    String propertyHolderName = info.getPropertyHolderName();
	    int datatype = info.getDataType();

	    if(propertyHolderName.
	       equals(instance.getFullyQualifiedName())){
		if(datatype == PropertyInfo.COMMAND_DATATYPE){
		    // FIXME:
		    boolean placed = false;
		    for(int i = 0; i < commands.size(); i++){
			if(propertyName.compareTo(((PropertyInfo)commands.
						   elementAt(i)).
						  getPropertyName()) < 0){
			    commands.add(i, info);
			    placed = true;
			    break;
			}
		    }

		    if(placed == false)
			commands.add(info);

		    document.removeProperty(getFullyQualifiedName()+"."+
					    propertyName);
		}
		else{
		    PropertyInfo localInfo = 
			propertyManager.getProperty(info.getPropertyName());
		    Object localValue;
		    
		    if(localInfo != null){
			localValue = localInfo.getValue();
			if(!localValue.equals(value)){
			    localInfo.setValue(value);
			    propertyChanged(localInfo);
			}
		    }
		}
	    }
	}

	Iterator commandIterator = commands.iterator();
	while(commandIterator.hasNext()){
	    System.out.println(commandIterator.next());
 	    handleCommand((String)((PropertyInfo)commandIterator.next()).
 			  getValue());
	}
    }

    /**
     * Builds the polygons used to draw this figure, one for each direction
     * that it may be facing.
     *
     */
    private void initializePolygons(int width)
    {
	eastPolygon = new Polygon();
	eastPolygon.addPoint(0, 0);
	eastPolygon.addPoint(0, 40);
	eastPolygon.addPoint(10, 50);
	eastPolygon.addPoint(0, 60);
	eastPolygon.addPoint(0, 100);
	eastPolygon.addPoint(width, 75);
	eastPolygon.addPoint(width, 25);

	westPolygon = new Polygon();
	westPolygon.addPoint(0, 25);
	westPolygon.addPoint(0, 75);
	westPolygon.addPoint(width, 100);
	westPolygon.addPoint(width, 60);
	westPolygon.addPoint(width - 10, 50);
	westPolygon.addPoint(width, 40);
	westPolygon.addPoint(width, 0);

	northPolygon = new Polygon();
	northPolygon.addPoint(25, 0);
	northPolygon.addPoint(0, width);
	northPolygon.addPoint(40, width);
	northPolygon.addPoint(50, width - 10);
	northPolygon.addPoint(60, width);
	northPolygon.addPoint(100, width);
	northPolygon.addPoint(75, 0);

	southPolygon = new Polygon();
	southPolygon.addPoint(0, 0);
	southPolygon.addPoint(25, width);
	southPolygon.addPoint(75, width);
	southPolygon.addPoint(100, 0);
	southPolygon.addPoint(60, 0);
	southPolygon.addPoint(50, 10);
	southPolygon.addPoint(40, 0);
    }

    /**
     * Builds PropertyInfo objects for all of the properties that will be
     * stored for this figure.
     *
     */
    private void initializeProperties()
    { 
	PropertyInfo info = new PropertyInfo(this, 
					     "X Coordinate",
					     "General",
					     new Integer(location.x),
					     PropertyInfo.INT_DATATYPE,
					     null, false);
	propertyManager.addProperty(info);
	
	info = new PropertyInfo(this, 
				"Y Coordinate",
				"General",
				new Integer(location.y),
				PropertyInfo.INT_DATATYPE,
				null, false);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this,
				"Fill Color",
				"General",
				new Integer(shapeDrawable.getFillColor().
					    getRGB()),
				PropertyInfo.COLOR_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this,
				"Selected Fill Color",
				"General",
				new Integer(shapeDrawable.
					    getSelectedFillColor().
					    getRGB()),
				PropertyInfo.COLOR_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this,
				"Label",
				"Label",
				labelDrawable.getText(),
				PropertyInfo.STRING_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this,
				"Font Size",
				"Label",
				new Integer(labelDrawable.getFontSize()),
				PropertyInfo.INT_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this,
				"Label Fill Color",
				"Label",
				new Integer(labelDrawable.
					    getFillColor().getRGB()),
				PropertyInfo.COLOR_DATATYPE,
				null, true);

	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Show Port Direction", 
				"General",
				new Boolean(true),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Set Visible",
				"General",
				new Boolean(true),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	String[] faces = {// "North", 
// 			  "South",
			  "East",
			  "West"
	};
	
	info = new PropertyInfo(this, 
				"Output Direction",
				"General",
				new String("East"),
				PropertyInfo.STRING_DATATYPE,
				faces, true);
	propertyManager.addProperty(info);	  

	info = new PropertyInfo(this, 
				"Display Class",
				"General",
				this.getClass().getName(),
				PropertyInfo.STRING_DATATYPE,
				null, true);
	propertyManager.addProperty(info);
    }

    /**
     * Lays out the ports on this figure provided that they have not been
     * already been positioned by their stored properties.
     *
     */
    public void layoutPorts()
    {
	Rectangle shapeBounds = shapeDrawable.getBounds();
	Rectangle labelBounds = labelDrawable.getBounds();

	// we want to begin laying out the ports at the y coordinate which is 
	// the first multiple of 10, that is below the label boundary
	int yCoord = shapeBounds.y + labelBounds.height;
	int base = Math.round(yCoord/10);
	yCoord = (base + 1) * 10;

 	int currentInY = yCoord;
	int currentOutY = currentInY;

	int outputX = shapeBounds.x + shapeBounds.width + 1;

	// reset the paintBounds to the bounds of just the label
	paintBounds = labelBounds;

	// output ports go on the east face and input ports go on the west
	Iterator figureIterator = figures.values().iterator();
	while(figureIterator.hasNext()){
	    // FIXME: some of these calculations should be up front instead
	    // of repetitive
	    SchematicFigure figure = (SchematicFigure)figureIterator.next();

	    if(figure instanceof PortFigure){
		Rectangle portBounds = figure.getBounds();

		if(((PortFigure)figure).getDirection() == Port.OUTPUT){
		    figure.setLocation(outputX, currentOutY);
		    currentOutY += portBounds.height + 10;
		}
		else{
		    figure.setLocation(shapeBounds.x - portBounds.width + 1, 
				       currentInY);
		    currentInY += portBounds.height + 10;
		}

		// add the port bounds to the paintBounds
		paintBounds = paintBounds.
		    createUnion(figure.getPaintBounds()).getBounds();
	    }
	}
    }

    /**
     * Paint this figure using the specified Graphics2D object.
     *
     * @param g2d the graphics object to use when painting this figure
     */
    public void paint(Graphics2D g2d)
    {
	if(isVisible()){
// 	    AffineTransform transform = null;
// 	    if(scaleFactorX != 1.0 || scaleFactorY != 1.0){
// 		transform = AffineTransform.getScaleInstance(scaleFactorX, 
// 							     scaleFactorY);
// 		g2d.transform(transform);
// 	    }
	    
	    if(drawables != null){
		Iterator drawableIterator = drawables.values().iterator();
		while(drawableIterator.hasNext()){
		    Drawable drawable = (Drawable)drawableIterator.next();
		    
		    drawable.paint(g2d);
		}
	    }

	    shapeDrawable.paint(g2d);
	    labelDrawable.paint(g2d);

	    if(figures != null){
		Iterator figureIterator = figures.values().iterator();

		while(figureIterator.hasNext()){
		    SchematicFigure figure = 
			(SchematicFigure)figureIterator.next();
		    
		    figure.paint(g2d);
		}
	    }

// 	    if(transform != null){
// 		AffineTransform inverse = null;
// 		try{
// 		    inverse = transform.createInverse();
// 		    g2d.transform(inverse);
// 		}
// 		catch(NoninvertibleTransformException e){
// 		    //FIXME:
// 		}
// 	    }
	}
    }

    /**
     * Signal to this figure that a property change event has occurred and
     * that it should update its state accordingly.
     *
     * @param info the PropertyInfo object that caused this event
     */
    public void propertyChanged(PropertyInfo info)
    {
	String propertyName = info.getPropertyName();

	if(propertyName.equals("Width")){
	    // FIXME:
	}
	else if(propertyName.equals("Height")){
	    //FIXME:
	}
	else if(propertyName.equals("Output Direction")){
	    String newFace = (String)info.getValue();
	    if(newFace.equals("North"))
		setOutputFace(NORTH);
	    else if(newFace.equals("East"))
		setOutputFace(EAST);
	    else if(newFace.equals("West"))
		setOutputFace(WEST);
	    else if(newFace.equals("South"))
		setOutputFace(SOUTH);
	}
	else if(propertyName.equals("Fill Color")){
	    shapeDrawable.setFillColor(new Color(((Integer)info.
				       getValue()).intValue()));
	    repaint();
	}
	else if(propertyName.equals("Selected Fill Color")){
	    shapeDrawable.setSelectedFillColor(new Color(((Integer)info.
					       getValue()).intValue()));
	    repaint();
	}
	else if(propertyName.equals("Label")){
	    labelDrawable.setText((String)info.getValue());
	    // reset the label position
	    repaint();
	}
	else if(propertyName.equals("Font Size")){
	    labelDrawable.setFontSize(((Integer)info.
				       getValue()).intValue());
	    validateBounds();
	    repaint();
	}
	else if(propertyName.equals("Label Fill Color")){
	    labelDrawable.setFillColor(new Color(((Integer)info.
				       getValue()).intValue()));

	    repaint();
	}
	else if(propertyName.equals("X Coordinate")){
	    Rectangle repaintRegion = getPaintBounds();

	    setLocation(((Integer)info.getValue()).intValue(), location.y);

	    repaintRegion = 
		repaintRegion.createUnion(getPaintBounds()).getBounds();
	    canvas.repaint(repaintRegion);
	}
	else if(info.getPropertyName().equals("Y Coordinate")){
	    Rectangle repaintRegion = getPaintBounds();

	    setLocation(location.x, ((Integer)info.getValue()).intValue());
	    repaintRegion = 
		repaintRegion.createUnion(getPaintBounds()).getBounds();
	    canvas.repaint(repaintRegion);
	}
	else if(info.getPropertyName().equals("Show Port Direction")){
	    boolean didUpdate = false;
	    Iterator figureIterator = figures.values().iterator();
	    Boolean value = (Boolean)info.getValue();

	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		if(figure instanceof PortFigure){
		    PortFigure portFigure = (PortFigure)figure;

		    PropertyInfo portInfo = 
			portFigure.getProperty("Show Direction");

		    if(portInfo != null){
			Boolean showDirection = (Boolean)portInfo.getValue();

			if(!showDirection.equals(value)){
			    portInfo.setValue(value);
			    portFigure.propertyChanged(portInfo);
			    didUpdate = true;
			}
		    }
		}
	    }

	}
	else if(info.getPropertyName().equals("Set Visible")){
	    //this.isVisible = ((Boolean)info.getValue()).booleanValue();
	    repaint();
	}
	else if(info.getPropertyName().equals("Display Class")){
	    String className = (String)info.getValue();

	    if(!className.equals(this.getClass().getName())){
		Class dynClass = null;
		
		try{
		    dynClass = Class.forName(className);
		}
		catch(ClassNotFoundException cnfe){
		    info.setValue(this.getClass().getName());
		    JOptionPane.showMessageDialog(null, "The specified display"
						  +" class: '"+
						  className+
						  "' could not be loaded."+
						  "Please check the class "+
						  "name. ", 
						  "Notice", 
						  JOptionPane.
						  ERROR_MESSAGE);
		    return;
		}
		
		// now we determine if the class provided is a decendent of
		// the PluggableInstanceFigure class.
		Class superClass = dynClass;
		Class pluggableClass = this.getClass().getSuperclass().
		    getSuperclass();
		boolean isDescendent = false;
		
		while((superClass = superClass.getSuperclass()) != null){
		    if(superClass.equals(pluggableClass)){
			isDescendent = true;
			break;
		    }
		}
		if(isDescendent){
		    storeProperties();
		    canvas.createAndSwapFigure(this, className);
		}
		else
		    info.setValue(this.getClass().getName());
		    JOptionPane.showMessageDialog(null, "Error: "+
						  "The specified class "+
						  "is not a descendent of "+
						  "PluggableInstanceFigure.",
						  "ERROR", 
						  JOptionPane.
						  ERROR_MESSAGE);
		
	    }
	}
    }

    /**
     * Set the direction of this ALUFigure, where the value of face comes
     * from javax.SwingConstants.[NORTH | SOUTH | EAST | WEST]
     *
     * @param newFace the new direction that the output face should be pointing
     */
    public void setOutputFace(int newFace)
    {
	if(newFace == outputFace)
	    return;

	outputFace = newFace;
	Rectangle repaintRegion = getPaintBounds();
	Point location = shapeDrawable.getLocation();
	int dx = 0;
	int dy = 0;

	switch(newFace){
	case NORTH:
	    dx = location.x - northPolygon.xpoints[0] + 25;
	    dy = location.y - northPolygon.ypoints[0];
	    northPolygon.translate(dx, dy);
	    shapeDrawable.setShape(northPolygon);
	    break;
	case SOUTH:
	    dx = location.x - southPolygon.xpoints[0];
	    dy = location.y - southPolygon.ypoints[0];
	    southPolygon.translate(dx, dy);
	    shapeDrawable.setShape(southPolygon);
	    break;
	case EAST:
	    dx = location.x - eastPolygon.xpoints[0];
	    dy = location.y - eastPolygon.ypoints[0];
	    eastPolygon.translate(dx, dy);
	    shapeDrawable.setShape(eastPolygon);
	    break;
	case WEST:
	    dx = location.x - westPolygon.xpoints[0];
	    dy = location.y - westPolygon.ypoints[0] + 25;
	    westPolygon.translate(dx, dy);
	    shapeDrawable.setShape(westPolygon);
	    break;
	}

	repaintRegion = 
	    repaintRegion.createUnion(getPaintBounds()).getBounds();
	canvas.repaint(repaintRegion);
    }

    /**
     * Set the location of this figure on the canvas.
     *
     * @param x the new x coordinate for this figure
     * @param y the new y coordinate for this figure
     */
    public void setLocation(int x, int y)
    {
// 	if(scaleFactorX != 1.0f)
// 	    x = (int)(x * (1/scaleFactorX));
// 	if(scaleFactorY != 1.0f)
// 	    y = (int)(y * (1/scaleFactorY));

	int dx, dy;
	dx = x - location.x;
	dy = y - location.y;
	this.location.setLocation(x, y);

	// translate all of the figures
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		
		figure.translate(dx, dy);
	    }
	}

	// translate all of the drawables
	if(drawables != null){
	    Iterator drawableIterator = drawables.values().iterator();
	    while(drawableIterator.hasNext()){
		Drawable drawable = (Drawable)drawableIterator.next();
		
		drawable.translate(dx, dy);
	    }
	}

	shapeDrawable.translate(dx, dy);
	labelDrawable.translate(dx, dy);

	bounds.translate(dx, dy);
	paintBounds.translate(dx, dy);
    }

    /**
     * Translate this figure the specified horizontal and vertical distances.
     *
     * @param dx the horizontal distance to translate this figure
     * @param dy the vertical distance to translate this figure
     */
    public void translate(int dx, int dy)
    {
// 	if(scaleFactorX != 1.0f)
// 	    dx = (int)(dx * (1/scaleFactorX));
// 	if(scaleFactorY != 1.0f)
// 	    dy = (int)(dy * (1/scaleFactorY));

	int x = location.x + dx;
	int y = location.y + dy;
	this.location.translate(dx, dy);

	// translate all of the figures
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		
		figure.translate(dx, dy);
	    }
	}

	// translate all of the drawables
	if(drawables != null){
	    Iterator drawableIterator = drawables.values().iterator();
	    while(drawableIterator.hasNext()){
		Drawable drawable = (Drawable)drawableIterator.next();
		
		drawable.translate(dx, dy);
	    }
	}

	shapeDrawable.translate(dx, dy);
	labelDrawable.translate(dx, dy);

	bounds.translate(dx, dy);
	paintBounds.translate(dx, dy);
    }

    /**
     * Update any PropertyInfo objects for values which may be cached in local
     * variables
     *
     */
    public void updateCachedProperties()
    {
	PropertyInfo pInfo = propertyManager.getProperty("X Coordinate");
	pInfo.setValue(new Integer(location.x));

	pInfo = propertyManager.getProperty("Y Coordinate");
	pInfo.setValue(new Integer(location.y));
    }

    /**
     * Recalculate the bounding regions for this figure if they are cached.
     *
     */
    public void validateBounds()
    {
 	this.bounds = shapeDrawable.getBounds().
	    createUnion(labelDrawable.getBounds()).getBounds();

	// get the bounds for all of the drawables	
	if(drawables != null){
	    Iterator drawableIterator = drawables.values().iterator();
	    while(drawableIterator.hasNext()){
		Drawable drawable = (Drawable)drawableIterator.next();
		
		bounds = bounds.createUnion(drawable.getBounds()).getBounds();
	    }
	}

	this.paintBounds = new Rectangle(bounds);

	// get the paintBounds all of the figures
	if(figures != null){
	    Iterator figureIterator = figures.values().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();
		
		paintBounds = paintBounds.
		    createUnion(figure.getPaintBounds()).getBounds();
	    }
	}

	// just to make sure...
	paintBounds.setRect(paintBounds.x - 1, paintBounds.y - 1,
			    paintBounds.width + 2, paintBounds.height + 2);
    }

    /**
     * Check the location of a figure owned by this figure and if it is in an
     * invalid location, set its location accordingly.
     *
     * @param child the child figure whose location is being validated
     * @param mouseLocation a hint in determining where the location of the
     *                      child figure should be
     */
    public void validateChildLocation(SchematicFigure child, 
				      Point mouseLocation)
    {
	if(child instanceof PortFigure){
	    PortFigure port = (PortFigure)child;

	    // the face of the instance that this port belongs on
	    int face = NORTH;
	    Polygon polygon = (Polygon)shapeDrawable.getShape();
	    int nPoints = polygon.npoints;
	    Line2D.Float[] segments = new Line2D.Float[nPoints];
	    Point point2 = new Point(polygon.xpoints[0], polygon.ypoints[0]);
	    Point point1;

	    for(int i = 0; i < nPoints - 1; i++){
		point1 = point2;
		point2 = new Point(polygon.xpoints[i + 1], 
				   polygon.ypoints[i + 1]);
		segments[i] = new Line2D.Float(point1, point2);
	    }
	    point1 = point2;
	    point2 = new Point(polygon.xpoints[0], 
			       polygon.ypoints[0]);
	    segments[nPoints - 1] = new Line2D.Float(point1, point2);

	    Line2D.Float eastBoundary = new Line2D.Float();
	    Line2D.Float westBoundary = new Line2D.Float();
	    Line2D.Float northBoundary = new Line2D.Float();
	    Line2D.Float southBoundary = new Line2D.Float();

	    switch(outputFace){
	    case NORTH:
		westBoundary = segments[0];
		
		eastBoundary = segments[5];

		northBoundary = segments[6];

		southBoundary = new Line2D.Float(segments[1].getP1(),
						 segments[4].getP2());
		break;
	    case SOUTH:
		westBoundary = segments[0];
		
		eastBoundary = segments[5];

		northBoundary =  new Line2D.Float(segments[6].getP2(),
						  segments[3].getP1());

		southBoundary = segments[1];

		break;
	    case EAST:
		westBoundary = new Line2D.Float(segments[0].getP1(),
						segments[3].getP2());
		
		eastBoundary = segments[5];

		northBoundary = segments[6];

		southBoundary = segments[4];
		break;
	    case WEST:
		westBoundary = segments[0];
		
		eastBoundary = new Line2D.Float(segments[2].getP1(),
						segments[5].getP2());

		northBoundary = segments[6];
		southBoundary = segments[1];
		break;
	    }

	    double westDistance = westBoundary.ptSegDist(mouseLocation.x,
							 mouseLocation.y);
	    double eastDistance = eastBoundary.ptSegDist(mouseLocation.x,
							 mouseLocation.y);
	    double northDistance = northBoundary.ptSegDist(mouseLocation.x,
							   mouseLocation.y);
	    double southDistance = southBoundary.ptSegDist(mouseLocation.x,
							   mouseLocation.y);

	    if(westDistance < eastDistance){
		if(westDistance < northDistance){
		    if(westDistance < southDistance){
			face = WEST;
		    }
		    else{
			face = SOUTH;
		    }
		}
		else{
		    if(northDistance < southDistance){
			face = NORTH;
		    }
		    else{
			face = SOUTH;
		    }
		}
	    }
	    else{
		if(eastDistance < northDistance){
		    if(eastDistance < southDistance){
			face = EAST;
		    }
		    else{
			face = SOUTH;
		    }
		}
		else{
		    if(northDistance < southDistance){
			face = NORTH;
		    }
		    else{
			face = SOUTH;
		    }
		}
	    }

	    Rectangle portBounds = port.getBounds();
	    int xCoord = portBounds.x;
	    int yCoord = portBounds.y;

	    port.setConnectSite(face);

	    switch(face){
	    case NORTH:
		{
		if(portBounds.x < westBoundary.x1)
		    xCoord = Math.round(westBoundary.x1);
		else if(portBounds.x + portBounds.width > 
			eastBoundary.x1){
		    xCoord -= ((portBounds.x + portBounds.width) - 
			       (eastBoundary.x1));
		}

		// The northern boundary corresponds to segments[6]
		Point2D srcPoint = northBoundary.getP2();
		Point2D destPoint = northBoundary.getP1();

		double slope = (destPoint.getY() - srcPoint.getY()) / 
		    (destPoint.getX() - srcPoint.getX());
		double yIntercept = (srcPoint.getY() - 
				     (srcPoint.getX()*slope));
		yCoord = (int)Math.round((slope*xCoord) + yIntercept) -
		    Math.round(port.getBounds().height/2);
		}
		break;
	    case SOUTH:
		{
		if(portBounds.x < westBoundary.x1)
		    xCoord = Math.round(westBoundary.x1);
		else if(portBounds.x + portBounds.width > 
			eastBoundary.x1){
		    xCoord -= ((portBounds.x + portBounds.width) - 
			       (Math.round(eastBoundary.x1)));
		}

		// The southern boundary corresponds to segments[4]
		Point2D srcPoint = southBoundary.getP1();
		Point2D destPoint = southBoundary.getP2();

		double slope = (destPoint.getY() - srcPoint.getY()) / 
		    (destPoint.getX() - srcPoint.getX());
		double yIntercept = (srcPoint.getY() - 
				     (srcPoint.getX()*slope));

		yCoord = (int)Math.round((slope*xCoord) + yIntercept) - 
		    Math.round(port.getBounds().height/2);
		}
		break;
	    case WEST:
		if(portBounds.y < westBoundary.y1)
		    yCoord = Math.round(westBoundary.y1);
		else if(portBounds.y + portBounds.height > 
			westBoundary.y2){
		    yCoord -= ((portBounds.y + portBounds.height) - 
			       (Math.round(westBoundary.y2)));
		}

		xCoord = Math.round(westBoundary.x1) - portBounds.width;

		break;
	    case EAST:
		if(portBounds.y < eastBoundary.y2)
		    yCoord = Math.round(eastBoundary.y2);
		else if(portBounds.y + portBounds.height > 
			eastBoundary.y1){
		    yCoord -= ((portBounds.y + portBounds.height) - 
			       (Math.round(eastBoundary.y1)));
		}

		xCoord = Math.round(eastBoundary.x1);

		break;
	    }
	    port.setLocation(xCoord, yCoord);
	    repaint();

	    Iterator edgeIterator = edges.values().iterator();
	    while(edgeIterator.hasNext()){
		PluggableConnectionFigure connectionFigure = 
		    (PluggableConnectionFigure)edgeIterator.next();
		connectionFigure.validateBounds();
	    }
	    repaint();
	}
    }
 }
