package Liberty.visualizer.canvas.figure.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.dialog.*;

import Liberty.LSS.*;
import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.File;
import java.util.*;

import javax.swing.*;

/**
 * The PluggableConnectionFigure is the abstract base class that should be 
 * extended and used for any figure that will represent an LSE IMR Connection
 *
 * @see SchematicEdgeFigure
 * @see PropertyHolder
 *
 * @author Jason Blome
 * @version 0.1
 */

public abstract class PluggableConnectionFigure implements SchematicEdgeFigure,
							   PropertyHolder{

    /** the canvas that this will be drawn on */
    protected SchematicCanvas canvas;
    /** the semantic object that this figure represents */
    protected Connection connection;
    /** the destination figure for this connection */
    protected SchematicNodeFigure destFigure;
    /** a list of this figure's Drawable objects */
    protected LinkedHashMap drawables;
    /** a list of this figure's SchematicFigure objects */
    protected LinkedHashMap figures;
    /** this container responsible for this figure */
    protected SchematicCanvasContainer parent;
    /** the PropertyManager for this SchematicFigure */
    protected PropertyManager propertyManager;
    /** the source figure for this connection */
    protected SchematicNodeFigure srcFigure;
    /** are the jog points visible */
    protected boolean showJogPoints;

    /**
     * Constructor for the PluggableConnectionFigure object used to represent 
     * IMR Connections objects on a SchematicCanvas.
     *
     * @param canvas the canvas that this GenericInstanceFigure
     *               will be rendered upon
     * @param parent the parent of this 
     *               GenericInstanceFigure, which may be any class
     *               that implements SchematicCanvasContainer
     * @param connection the IMR Connection object obtained from the LSE 
     *               compiler
     * @param srcFigure the srcFigure representing the source IMR Instance 
     *                  object
     * @param destFigure the srcFigure representing the destination IMR 
     *                  Instance object
     */
    public PluggableConnectionFigure(SchematicCanvas canvas,
				     SchematicCanvasContainer parent,
				     Connection connection,
				     SchematicNodeFigure srcFigure, 
				     SchematicNodeFigure destFigure)
    {
	this.canvas = canvas;
	this.parent = parent;
	this.connection = connection;
	this.srcFigure = srcFigure;
	this.destFigure = destFigure;
	this.propertyManager = new PropertyManager(this, canvas.getDocument());
	this.showJogPoints = false;
    }

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param drawable the drawable to be added to this figure
     */
    public void addDrawable(Drawable drawable){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param figure the figure to be added to this figure
     */
    public void addFigure(SchematicFigure figure){}

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     * @param oldBounds the old bounding rectangle for the figure
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {
    }

    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     */
    public void clear(){}

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	return null;
    }

    /**
     * Get the SchematicNodeFigure that represents the destination instance
     * for the connection object.
     *
     * @return the SchematicNodeFigure at which this connection terminates
     */
    public SchematicNodeFigure getDestFigure()
    {
	return destFigure;
    }

    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     *
     * @return null
     */
    public Collection getDrawables(){return null;}

    /**
     * Return the fully qualified name of this figure as given by the IMR
     * connection's fully qualified name.
     *
     * @return the fully qualified name of this figure as given by the IMR
     *         connection's fully qualified name
     */
    public String getFullyQualifiedName()
    {
	return connection.toString();
    }

    /**
     * The DefaultConnectionFigure does not define a location, 
     * hence this function will always return null.
     *
     * @return null
     */
    public Point getLocation()
    {
	return null;
    }

    /**
     * Return the short name of this figure as given by the IMR connection's 
     * name
     *
     * @return the short name of this figure
     */
    public String getName()
    {
	return connection.toString();
    }

    /**
     * Return's this figure's parent SchematicCanvasContainer object.
     *
     * @return the parent SchematicCavnasContainer object for this figure
     */
    public SchematicCanvasContainer getParent()
    {
	return parent;
    }

    /**
     * Returns the PropertyInfo object representing the named property or null
     * if this figure does not define the specified property.
     *
     * @param name the name of the sought property
     * @return the PropertyInfo object for the named property or null
     */
    public PropertyInfo getProperty(String name)
    {
	return propertyManager.getProperty(name);
    }

    /**
     * Return the PropertyManager object responsible for managing this 
     * figure's properties.
     *
     * @return the PropertyManager object for this figure
     */
    public PropertyManager getPropertyManager()
    {
	return propertyManager;
    }

    /**
     * Get the array of segments used by the SimpleManhattanRouter.
     *
     * @return an array of segments to be used by the SimpleManhattanRouter
     */
    public abstract SegmentFigure[] getSegementArray();

    /**
     * Get the SchematicNodeFigure that represents the source instance
     * for the connection object.
     *
     * @return the SchematicNodeFigure at which this connection begins
     */
    public SchematicNodeFigure getSrcFigure()
    {
	return srcFigure;
    }

    /**
     * This is a stub implementation for figures that may not needd to 
     * implement this function.
     *
     * @return null
     */
    public Collection getSubFigures(){return null;}

    /**
     * This function returns a String to be displayed in a tooltip when
     * the mouse hovers over this figure.
     *
     * @return the tooltip text to be displayed
     */
    public String getTooltipText()
    {
	return connection.toString();
    }

    /**
     * Returns the user object for this PluggableConnectionFigure, this value
     * will always be the IMR connection that this figure represents.
     *
     * @return the user object for this figure
     */
    public Object getUserObject()
    {
	return connection;
    }

    /**
     * Parse the text and perform the action specified by the text parameter 
     * if it is defined by this figure, otherwise ignore the text.
     *
     * @param text the string which represents an action to be carried out
     *             by this figure
     */
    public void handleCommand(String command){}

    /**
     * This method is called by the SchematicCanvas that owns this figure
     * when a double click event occurs on this figure.
     *
     * @param x the x coordinate of the double click event
     * @param y the y coordinate of the double click event
     */
    public void handleDoubleClick(int x, int y)
    {
	LSSInfoDialog.showDialog(null, connection);
    }

    /**
     * Set this figures properties to reflect the properties defined in the
     * propertySet, if they are applicable.  This class provides an empty 
     * implementation for convenience.
     *
     * @param propertySet a set of PropertyInfo objects
     */
    public void handleStoredProperties(java.util.List propertyList)
    {
    }

    /**
     * Signal to this figure that a property change event has occurred and
     * that it should update its state accordingly.  This class provides an 
     * empty implementation for convenience.
     *
     * @param info the PropertyInfo object that caused this event
     */
    public void propertyChanged(PropertyInfo info)
    {
    }

    /**
     * Do nothing.
     *
     * @param x the new x coordinate for this figure
     * @param y the new y coordinate for this figure
     */
    public void setLocation(int x, int y)
    {
	// do nothing
    }

    /**
     * Set the parent of this figure to be the SchematicCanvasContainer defined
     * by the parameter parent.
     *
     * @param parent the new SchematicCanvasContainer that owns this figure
     */
    public void setParent(SchematicCanvasContainer parent)
    {
	this.parent = parent;
    }

    /**
     * Set the status for whether this connection's JogPointFigure objects are
     * visible or not.
     *
     * @param b should this connection be auto-routed or not
     */
    public void setShowJogPoints(boolean b)
    {
	showJogPoints = b;
    }

    /**
     * Return whether or not the JogPointFigures are visible or not.
     *
     * @return whether the JogPointFigure objects are visible or not
     */
    public boolean showJogPoints()
    {
	return showJogPoints;
    }

    /**
     * This function is used to set the location of this figure to x and y
     * coordinates which are divisible by the integer value 10.
     *
     */
    public void snapToGrid(){}

    /**
     * Report this figure's current property state to the LSSDocument which
     * will eventually write them to a file.  This class provides an 
     * empty implementation for convenience.
     *
     */
    public void storeProperties()
    {
    }

    /**
     * Update any PropertyInfo objects for values which may be cached in local
     * variables
     *
     */
    public void updateCachedProperties()
    {
    }

    /**
     * Check the location of a figure owned by this figure and if it is in an
     * invalid location, set its location accordingly.
     *
     * @param child the child figure whose location is being validated
     * @param mouseLocation a hint in determining where the location of the
     *                      child figure should be
     */
    public void validateChildLocation(SchematicFigure child, Point eventPoint)
    {}
}
