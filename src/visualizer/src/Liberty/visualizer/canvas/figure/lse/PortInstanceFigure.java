package Liberty.visualizer.canvas.figure.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;

import java.awt.*;
import java.awt.geom.*;
import java.io.File;
import java.util.*;
import javax.swing.*;

public class PortInstanceFigure implements SchematicFigure{
    /* the name used to refer to this figure */
    private String name;
    /** the bounding rectangle for this figure */
    private Rectangle bounds;
    /* is this figure visible */
    private boolean isVisible;
    /* is this figure selected */
    private boolean isSelected;
    /* the location of this figure */
    private Point location;
    /* this port instance's instance number */
    private Integer instanceNumber;
    /* this figure's parent */
    private PortFigure parent;
    /* the connect site, one of NORTH, SOUTH, EAST, WEST from SwingConstants */
    private int connectSite;
    /** the drawable used to draw this port instance */
    private ShapeDrawable shapeDrawable;
//     private boolean showStub;
//     private StubFigure stubFigure;
    private SchematicCanvas canvas;

    public PortInstanceFigure(SchematicCanvas canvas, 
			      PortFigure parent, 
			      String name, 
			      int connectSite)
    {
	this.name = name;
	this.isSelected = false;
	this.isVisible = true;
	this.location = new Point(0, 0);
	this.instanceNumber = new Integer(name);
	this.connectSite = connectSite;
	this.parent = parent;
// 	this.showStub = false;
	this.canvas = canvas;

	Shape rect = new Rectangle(0, 0, 3, 3);
	shapeDrawable = new ShapeDrawable(canvas, this, "Shape", rect,
					  new Color(0xC0C0C0),
					  new Color(0xe51d27));
	bounds = shapeDrawable.getBounds();
    }

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     * @param oldBounds the old bounding rectangle for the figure
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {
    }

    public boolean contains(int x, int y)
    {
	if(bounds.contains(x, y))
	    return true;
	return false;
    }

    /**
     * This is a stub implementation for figures that may not needd to 
     * implement this function.
     *
     * @return null
     */
    public Collection getSubFigures(){return null;}


    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     *
     * @return null
     */
    public Collection getDrawables(){return null;}


    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     */
    public void clear(){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param drawable the drawable to be added to this figure
     */
    public void addDrawable(Drawable drawable){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param figure the figure to be added to this figure
     */
    public void addFigure(SchematicFigure figure){}


    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	return null;
    }

    public void drag(int dx, int dy, Point eventPoint)
    {
    }

    public Rectangle getBounds()
    {
	return new Rectangle(bounds);
    }

    public SchematicFigure getFigureAt(int x, int y)
    {
	return null;
    }

    public SchematicFigure getFigureByName(String name)
    {
	return null;
    }

    public int getConnectSite()
    {
	return parent.getConnectSite();
    }

    public SchematicFigure getFigureByUserObject(Object o)
    {
	return null;
    }

    public String getFullyQualifiedName()
    {
	return parent.getFullyQualifiedName() + "." + name;
    }

    public Integer getInstanceNumber()
    {
	return instanceNumber;
    }

    public Point getLocation()
    {
	return location;
    }

    public String getName()
    {
	return name;
    }

    public Rectangle getPaintBounds()
    {
	return new Rectangle(bounds.x - 1, bounds.y - 1,
			     bounds.width + 2, bounds.height + 2);
    }

    public SchematicCanvasContainer getParent()
    {
	return parent;
    }

    /**
     * This function returns a String to be displayed in a tooltip when
     * the mouse hovers over this figure.
     *
     * @return the tooltip text to be displayed
     */
    public String getTooltipText()
    {
	return parent.getName()+"["+instanceNumber+"]";
    }

    public Object getUserObject()
    {
	return null;
    }

    public void handleCommand(String command)
    {
    }

    public void handleDoubleClick(int x, int y)
    {
    }
    
    public boolean isSelected()
    {
	return false;
    }
    
    public boolean isVisible()
    {
	return isVisible;
    }
    
    public void paint(Graphics2D g2d)
    {	
	shapeDrawable.paint(g2d);
    }

    public void repaint(){}

    public void setConnectSite(int site)
    {
	this.connectSite = site;
    }

//     public void showStub(boolean isVisible, String label)
//     {
// 	showStub = isVisible;
// 	stubFigure = new StubFigure(
// 	stubLabel = new LabelDrawable(canvas, this, "stubLabel", label);
// 	switch(getConnectSite()){
// 	case NORTH:
// 	    stubLabel.setJustification(LabelDrawable.CENTER);
// 	    stubLabel.setLocation(location.x, location.y - 20);
// 	    break;
// 	case SOUTH:
// 	    stubLabel.setJustification(LabelDrawable.CENTER);
// 	    stubLabel.setLocation(location.x, location.y - 20);
// 	    break;
// 	case EAST:
// 	    stubLabel.setJustification(LabelDrawable.LEFT);
// 	    stubLabel.setLocation(location.x + bounds.width + 20, location.y + 
// 				  (int)(stubLabel.getBounds().getHeight()/2));
// 	    break;
// 	case WEST:
// 	    stubLabel.setJustification(LabelDrawable.RIGHT);
// 	    stubLabel.setLocation(location.x - 20, location.y +
// 				  (int)(stubLabel.getBounds().getHeight()/2));
// 	    break;
// 	}
// 	System.out.println("Validating parent bounds: "+((SchematicFigure)parent.getParent()).getName()+"\n\t"+
// 			   ((SchematicFigure)parent.getParent()).getPaintBounds());
// 	((SchematicFigure)parent.getParent()).validateBounds();
// 	System.out.println("\t"+
// 			   ((SchematicFigure)parent.getParent()).getPaintBounds());
// 	bounds = bounds.createUnion(stubLabel.getBounds()).getBounds();
//     }

    public void setLocation(int x, int y)
    {
	int dx, dy;

	dx = x - location.x;
	dy = y - location.y;

	this.location.setLocation(x, y);
	shapeDrawable.translate(dx, dy);
// 	if(stubLabel != null)
// 	    stubLabel.translate(dx, dy);
	bounds.translate(dx, dy);
    }

    /**
     * Set the parent of this figure to be the SchematicCanvasContainer defined
     * by the parameter parent.
     *
     * @param parent the new SchematicCanvasContainer that owns this figure
     */
    public void setParent(SchematicCanvasContainer parent)
    {
	if(parent instanceof PortFigure)
	    this.parent = (PortFigure)parent;
    }

    public void setSelected(boolean b)
    {
	isSelected = b;
    }

    public void setVisible(boolean b)
    {
	isVisible = b;
    }
    
    public String toString()
    {
	return parent.getName()+"["+name+"]";
    }

    public void translate(int dx, int dy)
    {
	location.translate(dx, dy);
	shapeDrawable.translate(dx, dy);
// 	if(stubLabel != null)
// 	    stubLabel.translate(dx, dy);
	bounds.translate(dx, dy);
    }

    public void validateBounds()
    {
    }

    public void validateChildLocation(SchematicFigure child, Point eventPoint)
    {
    }

    /**
     * This function does nothing as the PortInstanceFigure's parent PortFigure
     * will handle snapping to grid points.
     *
     */
    public void snapToGrid()
    {
	// Do nothing
    }

}
