package Liberty.visualizer.canvas.figure.lse;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.dialog.*;

import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.File;
import java.util.*;

import javax.swing.*;

public class PortFigure implements SchematicFigure, PropertyHolder{
    /** the rectangle that defines the clickable region for this figure */
    private Rectangle bounds;
    /** the canvas that this will be drawn on */
    private SchematicCanvas canvas;
    /** the face of the port on which connections are to be made */
    private int connectSite;
    /** the direction of this port */
    private int direction;
    /** the fully qualified name of this port */
    private String fullyQualifiedName;
    /** variable to determine if this figure is selected */
    private boolean isSelected;
    /** variable to determine if this figure is visible */
    private boolean isVisible;
    /** the LabelDrawable for this port */
    private LabelDrawable labelDrawable; 
    /** the location of this figure */
    private Point location;
    /** the name used to refer to this figure */
    private String name;
    /** the bounding rectangle for all elements that this figure owns */
    protected Rectangle paintBounds;
    /** this figure's parent */
    private PluggableInstanceFigure parent;
    /** the semantic object that this figure represents */
    private Port port;
    /** a map of PortInstanceFigures */
    private LinkedHashMap portInstances;
    /** the PropertyManager for this SchematicFigure */
    private PropertyManager propertyManager;
    /** the shape used to draw this port */
    private ShapeDrawable shapeDrawable;
    /** is the arrow showing */
    private boolean showDirection;
    /**  the arrow that is used when showing the direction of this port */
    private ShapeDrawable arrowDrawable;
    /** a set of all the connections made to this port */
    private HashSet connections;

    private final static String[] directionNames = {"North", 
						    "South", 
						    "East",
						    "West"};


    public PortFigure(SchematicCanvas canvas,
		      PluggableInstanceFigure parent,
		      Port port)
    {
	this.canvas = canvas;
	this.parent = parent;
	this.port = port;

	this.direction = port.getDirection();
	this.connectSite = direction == Port.OUTPUT ? EAST : 
	    SwingConstants.WEST;
	this.name = port.getName();
	this.fullyQualifiedName = port.getInstance().
	    getFullyQualifiedName() + "." + name;
	this.isSelected = false;
	this.isVisible = true;
	this.connections = new HashSet();
	Rectangle parentBounds = parent.getBounds();
	this.labelDrawable = new LabelDrawable(canvas, this, "Label", name);
	while((parentBounds.width/2)-10 < labelDrawable.getBounds().width){
	    String text = labelDrawable.getText();
	    if(text.length() <= 3){
		break;
	    }
	    labelDrawable.setText(text.substring(0, text.length() - 1));
	}
	this.location = new Point(-1, -1);
	this.portInstances = null;
	this.propertyManager = new PropertyManager(this, canvas.getDocument());
	// give the drawables bogus shapes, the correct shapes will be
	// assigned when the function layoutInstances is called
	this.shapeDrawable = new ShapeDrawable(canvas, 
					       this, 
					       "Shape", 
					       new Rectangle(),
					       Color.lightGray, 
					       Color.red);

	this.arrowDrawable = new ShapeDrawable(canvas, 
					       this, 
					       "Arrow",
					       new Polygon(), 
					       Color.black, 
					       Color.black);

	this.showDirection = true;

	this.bounds = shapeDrawable.getBounds();
	this.paintBounds = bounds.
	    createUnion(labelDrawable.getBounds()).getBounds();
	paintBounds.setRect(paintBounds.x - 3, paintBounds.y - 3,
			    paintBounds.width + 6, paintBounds.height + 6);

	initializeProperties();
    }

    /**
     * This is a stub implementation for figures that may not needd to 
     * implement this function.
     *
     * @return null
     */
    public Collection getSubFigures(){return null;}

    public void addConnection(PluggableConnectionFigure connection)
    {
	connections.add(connection);
    }

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     * @param oldBounds the old bounding rectangle for the figure
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {
    }

    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     *
     * @return null
     */
    public Collection getDrawables(){return null;}


    /**
     * This is a stub implementation for figures that may not need to implement
     * this function.
     */
    public void clear(){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param drawable the drawable to be added to this figure
     */
    public void addDrawable(Drawable drawable){}

    /**
     * This is a stub implementation for figures that do not wish to implement
     * this function.
     *
     * @param figure the figure to be added to this figure
     */
    public void addFigure(SchematicFigure figure){}


    public void addPortInstance(PortInstanceFigure piFigure)
    {
	if(portInstances == null)
	    portInstances = new LinkedHashMap();
	portInstances.put(piFigure.getInstanceNumber(), piFigure);
    }

    public boolean contains(int x, int y)
    {
	if(bounds.contains(x, y))
	    return true;
	return false;
    }

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {
	JPopupMenu popup = new JPopupMenu();

	PropertyInfo info = propertyManager.getProperty("Set Visible");
	if(info != null){
	    boolean isVisible = ((Boolean)info.getValue()).booleanValue();
	    final JCheckBoxMenuItem isVisibleItem = 
		new JCheckBoxMenuItem("Set Visible", isVisible);

	    isVisibleItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			PropertyInfo info = 
			    PortFigure.this.
			    propertyManager.getProperty("Set Visible");
			
			info.setValue(new Boolean(isVisibleItem.
						  getState()));
			PortFigure.this.propertyChanged(info);
		    }
		});
	    popup.add(isVisibleItem);
	}

	info = propertyManager.getProperty("Show Direction");
	if(info != null){
	    boolean showDirection = ((Boolean)info.getValue()).booleanValue();
	    final JCheckBoxMenuItem showDirectionItem = 
		new JCheckBoxMenuItem("Show Direction", showDirection);

	    showDirectionItem.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			PropertyInfo info = 
			    PortFigure.this.
			    propertyManager.getProperty("Show Direction");
			
			info.setValue(new Boolean(showDirectionItem.
						  getState()));
			PortFigure.this.propertyChanged(info);
		    }
		});
	    popup.add(showDirectionItem);
	}

	JMenuItem showDataItem = new JMenuItem("View Port Data");
	showDataItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    LSSInfoDialog.showDialog(null, PortFigure.
					     this.port);
		}
	    });
	popup.add(showDataItem);

	JMenuItem showPropertiesItem = new JMenuItem("View Visual Properties");
	showPropertiesItem.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    PortFigure.this.updateCachedProperties();
		    PortFigure.this.propertyManager.
			showPropertiesDialog();
		}
	    });

	popup.add(showPropertiesItem);
	return popup;
    }

    public void drag(int dx, int dy, Point eventPoint)
    {
	translate(dx, dy);

	if(parent != null){
	    parent.validateChildLocation(this, eventPoint);
	    parent.validateBounds();
	    parent.repaint();
	}
    }

    public Rectangle getBounds()
    {
	return new Rectangle(bounds);
    }

    public int getConnectSite()
    {
	return connectSite;
    }

    public int getDirection()
    {
	return direction;
    }

    public SchematicFigure getFigureAt(int x, int y)
    {
	return null;
    }

    public SchematicFigure getFigureByName(String name)
    {
	if(portInstances == null)
	    return null;

	try{
	    Integer instanceNumber = new Integer(name);
	    return (PortInstanceFigure)portInstances.get(instanceNumber);
	}
	catch(NumberFormatException nfe){
	    return null;
	}
    }

    public SchematicFigure getFigureByUserObject(Object o)
    {
	return null;
    }

    public String getFullyQualifiedName()
    {
	return fullyQualifiedName;
    }

    public Point getLocation()
    {
	return location;
    }

    public String getName()
    {
	return name;
    }

    public Rectangle getPaintBounds()
    {
	return new Rectangle(paintBounds);
    }

    public SchematicCanvasContainer getParent()
    {
	return parent;
    }

    public PropertyInfo getProperty(String name)
    {
	return propertyManager.getProperty(name);
    }

    public PropertyManager getPropertyManager()
    {
	return propertyManager;
    }

    /**
     * This function returns a String to be displayed in a tooltip when
     * the mouse hovers over this figure.
     *
     * @return the tooltip text to be displayed
     */
    public String getTooltipText()
    {
	return port.getName();
    }

    public Object getUserObject()
    {
	return port;
    }

    public void handleCommand(String command)
    {

    }


    public void handleDoubleClick(int x, int y)
    {
	LSSInfoDialog.showDialog(null, port);
    }

    public void handleStoredProperties(java.util.List propertyList)
    {
	// we're going to cache the "X Coordinate" and "Y Coordinate"
	// properties and update them together if possible.
	PropertyInfo xCoordProperty = null;
	PropertyInfo yCoordProperty = null;

	Iterator infoIterator = propertyList.iterator();
	while(infoIterator.hasNext()){
	    PropertyInfo info = (PropertyInfo)infoIterator.next();
	    String propertyHolderName = info.getPropertyHolderName();
	    String propertyName = info.getPropertyName();
	    Object value = info.getValue();

	    if(propertyHolderName.equals(getFullyQualifiedName())){
		PropertyInfo localInfo = 
		    propertyManager.getProperty(info.getPropertyName());
		Object localValue;
		
		if(localInfo != null){
		    localValue = localInfo.getValue();
		    if(!localValue.equals(value)){
			localInfo.setValue(value);

			if(propertyName.equals("X Coordinate")){
			    xCoordProperty = localInfo;
			}
			else if(propertyName.equals("Y Coordinate")){
			    yCoordProperty = localInfo;
			}
			else
			    propertyChanged(localInfo);
		    }
		}
	    }
	}
	if(xCoordProperty != null)
	    propertyChanged(xCoordProperty);
	if(yCoordProperty != null)
	    propertyChanged(yCoordProperty);
    }

    public void initializeProperties()
    {
	PropertyInfo info = new PropertyInfo(this, 
					     "X Coordinate",
					     "General",
					     new Integer(location.x),
					     PropertyInfo.INT_DATATYPE,
					     null, false);
	propertyManager.addProperty(info);
	
	info = new PropertyInfo(this, 
				"Y Coordinate",
				"General",
				new Integer(location.y),
				PropertyInfo.INT_DATATYPE,
				null, false);
	propertyManager.addProperty(info);
	
	info = new PropertyInfo(this, 
				"Show Direction",
				"General",
				new Boolean(showDirection),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this, 
				"Set Visible",
				"General",
				new Boolean(true),
				PropertyInfo.BOOL_DATATYPE,
				null, true);
	propertyManager.addProperty(info);

	info = new PropertyInfo(this,
				"Label",
				"Label",
				labelDrawable.getText(),
				PropertyInfo.STRING_DATATYPE,
				null, true);
	propertyManager.addProperty(info);
    }
    
    public boolean isSelected()
    {
	return isSelected;
    }
    
    public boolean isVisible()
    {
	return isVisible;
    }

    public void layoutInstances()
    {
	int xCoord, yCoord;
	int numInstances = portInstances == null ? 0 : portInstances.size();
	int spacing;
	Iterator iterator;
	Rectangle rect = 
	    (Rectangle)shapeDrawable.getShape();
	Polygon triangle = (Polygon)arrowDrawable.getShape();
	triangle.reset();
	Rectangle labelBounds = labelDrawable.getBounds();
	if(numInstances == 0){
	    switch(connectSite){
	    case NORTH:
		rect.setRect(location.x, location.y, 8, 6);
		labelDrawable.setJustification(LabelDrawable.CENTER);
		labelDrawable.setLocation(location.x + (rect.width/2),
					  location.y + rect.height);

		if(direction == Port.INPUT){
		    triangle.addPoint(location.x + 1, location.y);
		    triangle.addPoint(location.x + 4, location.y + 6);
		    triangle.addPoint(location.x + 7, location.y);
		}
		else{
		    triangle.addPoint(location.x + 1, location.y + 6);
		    triangle.addPoint(location.x + 4, location.y);
		    triangle.addPoint(location.x + 7, location.y + 6);
		}
		break;
	    case SOUTH:
		rect.setRect(location.x, location.y, 8, 6);
		labelDrawable.setJustification(LabelDrawable.CENTER);
		labelDrawable.setLocation(location.x + (rect.width/2),
					  location.y - labelBounds.height);

		if(direction == Port.INPUT){
		    triangle.addPoint(location.x + 1, location.y + 6);
		    triangle.addPoint(location.x + 4, location.y);
		    triangle.addPoint(location.x + 7, location.y + 6);
		}
		else{
		    triangle.addPoint(location.x + 1, location.y);
		    triangle.addPoint(location.x + 4, location.y + 6);
		    triangle.addPoint(location.x + 7, location.y);
		}
		break;
	    case EAST:
		rect.setRect(location.x, location.y, 6, 8);
		labelDrawable.setJustification(LabelDrawable.RIGHT);
		labelDrawable.setLocation(rect.x - 4, rect.y);
		labelDrawable.setLocation(rect.x - 4, (int)((rect.y + 4) - 
					  labelBounds.height/2));

		if(direction == Port.INPUT){
		    triangle.addPoint(location.x + 6, location.y + 1);
		    triangle.addPoint(location.x, location.y + 4);
		    triangle.addPoint(location.x + 6, location.y + 7);
		}
		else{
		    triangle.addPoint(location.x, location.y + 1);
		    triangle.addPoint(location.x + 6, location.y + 4);
		    triangle.addPoint(location.x, location.y + 7);
		}

		break;
	    case WEST:
		rect.setRect(location.x, location.y, 6, 8);
		labelDrawable.setJustification(LabelDrawable.LEFT);
		labelDrawable.setLocation(rect.x + rect.width + 4,
					  (int)((rect.y + 4) - 
					  labelBounds.height/2));

		if(direction == Port.INPUT){
		    triangle.addPoint(location.x, location.y + 1);
		    triangle.addPoint(location.x + 6, location.y + 4);
		    triangle.addPoint(location.x, location.y + 7);
		}
		else{
		    triangle.addPoint(location.x + 6, location.y + 1);
		    triangle.addPoint(location.x, location.y + 4);
		    triangle.addPoint(location.x + 6, location.y + 7);
		}

		break;
	    }
	}
	else{
	    switch(connectSite){
	    case NORTH:
		rect.setRect(location.x, location.y, 
			     (numInstances*5)+8, 6);
		
		xCoord = rect.x + 5;
		yCoord = rect.y - 2;

		iterator = portInstances.values().iterator();
		while(iterator.hasNext()){
		    SchematicFigure figure = 
			(SchematicFigure)iterator.next();
		    figure.setLocation(xCoord, yCoord);
		    xCoord += 5;
		}

		labelDrawable.setJustification(LabelDrawable.CENTER);
		labelDrawable.setLocation(location.x + (rect.width/2),
					  location.y + rect.height);

		if(direction == Port.INPUT){
		    triangle.addPoint(rect.x + 1, rect.y);
		    triangle.addPoint(rect.x + (rect.width/2),
				      rect.y + rect.height);
		    triangle.addPoint(rect.x + rect.width - 1, rect.y);
		}
		else{
		    triangle.addPoint(rect.x + 1, rect.y + rect.height);
		    triangle.addPoint(rect.x + (rect.width/2), rect.y);
		    triangle.addPoint(rect.x + rect.width - 1, 
				      rect.y + rect.height);
		}

		break;
	    case SOUTH:
		rect.setRect(location.x, location.y, 
			     (numInstances*5)+8, 6);
		
		xCoord = rect.x + 5;
		yCoord = rect.y + rect.height;

		iterator = portInstances.values().iterator();
		while(iterator.hasNext()){
		    SchematicFigure figure = 
			(SchematicFigure)iterator.next();
		    figure.setLocation(xCoord, yCoord);
		    xCoord += 5;
		}

		labelDrawable.setJustification(LabelDrawable.CENTER);
		labelDrawable.setLocation(location.x + (rect.width/2),
					  location.y - labelBounds.height);

		if(direction == Port.INPUT){
		    triangle.addPoint(rect.x + 1, rect.y + rect.height);
		    triangle.addPoint(rect.x + (rect.width/2), rect.y);
		    triangle.addPoint(rect.x + rect.width - 1, 
				      rect.y + rect.height);
		}
		else{
		    triangle.addPoint(rect.x + 1, rect.y);
		    triangle.addPoint(rect.x + (rect.width/2),
				      rect.y + rect.height);
		    triangle.addPoint(rect.x + rect.width - 1, rect.y);
		}

		break;
	    case EAST:
		rect.setRect(location.x, location.y, 
			     6, (numInstances*5)+8);

		xCoord = rect.x + rect.width;
		yCoord = rect.y + 5;

		iterator = portInstances.values().iterator();
		while(iterator.hasNext()){
		    SchematicFigure figure = 
			(SchematicFigure)iterator.next();
		    figure.setLocation(xCoord, yCoord);
		    yCoord += 5;
		}

		labelDrawable.setJustification(LabelDrawable.RIGHT);
		labelDrawable.setLocation(rect.x - 4, 
					  (int)((rect.y+(rect.height/2)) - 
						labelBounds.height/2));

		if(direction == Port.INPUT){
		    triangle.addPoint(rect.x + rect.width, rect.y + 1);
		    triangle.addPoint(rect.x, rect.y + (rect.height/2));
		    triangle.addPoint(rect.x + rect.width, 
				      rect.y + rect.height - 1);
		}
		else{
		    triangle.addPoint(rect.x, rect.y + 1);
		    triangle.addPoint(rect.x + rect.width, 
				      rect.y + (rect.height/2));
		    triangle.addPoint(rect.x, rect.y + rect.height - 1);
		}

		break;
	    default: //WEST
		rect.setRect(location.x, location.y, 
			     6, (numInstances*5)+8);

		xCoord = rect.x - 2;
		yCoord = rect.y + 5;

		iterator = portInstances.values().iterator();
		while(iterator.hasNext()){
		    SchematicFigure figure = 
			(SchematicFigure)iterator.next();
		    figure.setLocation(xCoord, yCoord);
		    yCoord += 5;
		}
		labelDrawable.setJustification(LabelDrawable.LEFT);
		labelDrawable.setLocation(rect.x + rect.width + 4,
					  (int)((rect.y+(rect.height/2)) - 
						labelBounds.height/2));

		if(direction == Port.INPUT){
		    triangle.addPoint(rect.x, rect.y + 1);
		    triangle.addPoint(rect.x + rect.width, 
				      rect.y + (rect.height/2));
		    triangle.addPoint(rect.x, rect.y + rect.height - 1);
		}
		else{
		    triangle.addPoint(rect.x + rect.width, rect.y + 1);
		    triangle.addPoint(rect.x, rect.y + (rect.height/2));
		    triangle.addPoint(rect.x + rect.width, 
				      rect.y + rect.height - 1);
		}

		break;
	    }
	}

	this.bounds = shapeDrawable.getBounds();
	this.paintBounds = bounds.
	    createUnion(labelDrawable.getBounds()).getBounds();

	paintBounds.setRect(paintBounds.x - 3, paintBounds.y - 3,
			    paintBounds.width + 6, paintBounds.height + 6);
    }
    
    public void paint(Graphics2D g2d)
    {
	if(isVisible){
	    shapeDrawable.paint(g2d);
	    labelDrawable.paint(g2d);

 	    if(showDirection){
		arrowDrawable.paint(g2d);
	    }

	    if(portInstances != null){
		Iterator iterator = portInstances.values().iterator();
		while(iterator.hasNext()){
		    PortInstanceFigure piFigure = 
			(PortInstanceFigure)iterator.next();
		    piFigure.paint(g2d);
		}
	    }
	}
    }

    public void propertyChanged(PropertyInfo info)
    {
	String propertyName = info.getPropertyName();

	if(propertyName.equals("Show Direction")){
	    this.showDirection = ((Boolean)info.getValue()).booleanValue();
	    layoutInstances();
	    repaint();
	}
	else if(propertyName.equals("X Coordinate")){
	    int relativeXcoord = ((Integer)info.getValue()).intValue();
	    PropertyInfo yCoordProperty = 
		propertyManager.getProperty("Y Coordinate");
	    int relativeYcoord = ((Integer)yCoordProperty.getValue()).
		intValue();
	    Point parentLocation = parent.getLocation();
	    int xCoord = relativeXcoord + parentLocation.x;
	    int yCoord = relativeYcoord + parentLocation.y;

	    setLocation(xCoord, yCoord);
	    Point evtPoint = new Point(location.x + (bounds.width/2),
				       location.y + (bounds.height/2));
	    if(parent != null)
		parent.validateChildLocation(this, evtPoint);
	}
	
	else if(propertyName.equals("Y Coordinate")){
	    int relativeYcoord = ((Integer)info.getValue()).intValue();
	    PropertyInfo xCoordProperty = 
		propertyManager.getProperty("X Coordinate");
	    int relativeXcoord = ((Integer)xCoordProperty.getValue()).
		intValue();
	    Point parentLocation = parent.getLocation();
	    int xCoord = relativeXcoord + parentLocation.x;
	    int yCoord = relativeYcoord + parentLocation.y;

	    setLocation(xCoord, yCoord);
	    Point evtPoint = new Point(location.x + (bounds.width/2),
				       location.y + (bounds.height/2));
	    if(parent != null)
		parent.validateChildLocation(this, location);
	}

	else if(propertyName.equals("Set Visible")){
	    this.isVisible = ((Boolean)info.getValue()).booleanValue();
	    repaint();
	}
	else if(propertyName.equals("Label")){
	    labelDrawable.setText((String)info.getValue());
	    repaint();
	}
    }

    public void repaint()
    {
	canvas.repaint(getPaintBounds());
    }

    public void setConnectSite(int site)
    {
	this.connectSite = site;

	layoutInstances();
    }

    public void setLocation(int x, int y)
    {
	int dx, dy;

	dx = x - location.x;
	dy = y - location.y;

	this.location.setLocation(x, y);

	shapeDrawable.translate(dx, dy);
	labelDrawable.translate(dx, dy);
	arrowDrawable.translate(dx, dy);

	if(portInstances != null){
	    Iterator iterator = portInstances.values().iterator();
	    while(iterator.hasNext()){
		SchematicFigure figure = (SchematicFigure)iterator.next();
		figure.translate(dx, dy);
	    }
	}

	bounds.translate(dx, dy);
	paintBounds.translate(dx, dy);

	if(parent != null){
	    parent.validateBounds();
	}
    }

    /**
     * Set the parent of this figure to be the SchematicCanvasContainer defined
     * by the parameter parent.
     *
     * @param parent the new SchematicCanvasContainer that owns this figure
     */
    public void setParent(SchematicCanvasContainer parent)
    {
	if(parent instanceof PluggableInstanceFigure)
	    this.parent = (PluggableInstanceFigure)parent;
    }

    public void setSelected(boolean b)
    {
	isSelected = b;
    }

    public void setVisible(boolean b)
    {
	isVisible = b;
    }
    
    public String toString()
    {
	return name;
    }

    public void translate(int dx, int dy)
    {
	shapeDrawable.translate(dx, dy);
	labelDrawable.translate(dx, dy);
	arrowDrawable.translate(dx, dy);

	if(portInstances != null){
	    Iterator iterator = portInstances.values().iterator();
	    while(iterator.hasNext()){
		SchematicFigure figure = (SchematicFigure)iterator.next();
		figure.translate(dx, dy);
	    }
	}

	location.translate(dx, dy);
	bounds.translate(dx, dy);
	paintBounds.translate(dx, dy);
    }

    public void validateBounds()
    {
	this.bounds = shapeDrawable.getBounds();
	this.paintBounds = bounds.
	    createUnion(labelDrawable.getBounds()).getBounds();

	if(portInstances != null){
	    Iterator portInstanceIterator = portInstances.values().iterator();
	    while(portInstanceIterator.hasNext()){
		PortInstanceFigure piFigure = 
		    (PortInstanceFigure)portInstanceIterator.next();
		
		paintBounds = 
		    paintBounds.createUnion(piFigure.getPaintBounds()).
		    getBounds();
	    }
	}
   
	paintBounds.setRect(paintBounds.x - 3, paintBounds.y - 3,
			    paintBounds.width + 6, paintBounds.height + 6);
    }

    public void validateChildLocation(SchematicFigure child, 
				      Point eventPoint)
    {
    }

    public void storeProperties()
    {
	updateCachedProperties();

	propertyManager.storeProperties();
    }

    public void updateCachedProperties()
    {
	Point parentLocation = parent.getLocation();
	PropertyInfo pInfo = propertyManager.getProperty("X Coordinate");
	pInfo.setValue(new Integer(location.x - parentLocation.x));

	pInfo = propertyManager.getProperty("Y Coordinate");
	pInfo.setValue(new Integer(location.y - parentLocation.y));

// 	pInfo = propertyManager.getProperty("Connect Site");
// 	pInfo.setValue(getDirectionString(connectSite));
    }

    private static String getDirectionString(int dir)
    {
	switch(dir){
	case EAST:
	    return directionNames[2];
	case WEST:
	    return directionNames[3];
	case NORTH:
	    return directionNames[0];
	case SOUTH:
	    return directionNames[1];
	}
	return "";
    }

    private static int getDirectionInt(String dir)
    {
	if(dir.equals(directionNames[0]))
	    return NORTH;
	else if(dir.equals(directionNames[1]))
	    return SOUTH;
	else if(dir.equals(directionNames[2]))
	    return EAST;
	else
	    return WEST;
    }

    /**
     * This function is used to set the location of this figure to x and y
     * coordinates which are divisible by the integer value 10.
     *
     */
    public void snapToGrid()
    {
	int xCoord = location.x;
	int yCoord = location.y;

	if(connectSite == NORTH || connectSite == SOUTH){
	    if(xCoord % 10 != 0){
		int base = Math.round(xCoord/10);
		xCoord = base * 10;
	    }
	}
	else if(connectSite == EAST || connectSite == WEST){
	    if(yCoord % 10 != 0){
		int base = Math.round(yCoord/10);
		yCoord = base * 10;
	    }
	}

	setLocation(xCoord, yCoord);

	if(parent != null){
	    parent.validateBounds();
	    parent.repaint();
	}
    }

}
