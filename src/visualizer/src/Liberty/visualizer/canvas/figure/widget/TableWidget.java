package Liberty.visualizer.canvas.figure.widget;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.lse.*;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

public class TableWidget implements SchematicFigure{

    private SchematicFigure parent;
    private int numColumns;
    private int numRows;
    private boolean isVisible;
    private boolean showHeaders;
    private String name;
    private String fullyQualifiedName;
    private Point location;
    private TableValue[][] tableValues;
    private Font font;
    private int fontSize;
    private SchematicCanvas canvas;
    private int orientation;
    private Rectangle table;
    private int rowHeight;
    private int columnWidth;

    public TableWidget(SchematicCanvas canvas,
		       SchematicFigure parent,
		       String name,
		       int numRows,
		       int numColumns)
    {
	Graphics2D g2d = (Graphics2D)canvas.getGraphics();

	this.canvas = canvas;
	this.name = name;
	this.fullyQualifiedName = parent.getFullyQualifiedName()+"."+name;
	this.parent = parent;
	this.numColumns = numColumns;
	// row number 0 is reserved for table headers
	this.numRows = numRows + 1;
	this.fontSize = 10;
	this.font = g2d.getFont().
	    deriveFont(Font.BOLD, fontSize);
	this.showHeaders = false;
	this.isVisible = true;
	this.location = new Point();
	this.tableValues = new TableValue[this.numRows][this.numColumns];
	this.orientation = VERTICAL;

	g2d.setFont(font);
	FontMetrics metrics = g2d.getFontMetrics();

	this.rowHeight = metrics.getHeight() + 5;
	this.columnWidth = 35;
	validateTableSize();
    }

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {
    }

    public int getFontSize()
    {
	return fontSize;
    }

    public void setFontSize(int size)
    {
       	this.fontSize = size;
	this.font = canvas.getGraphics().getFont().
	    deriveFont(Font.BOLD, fontSize);

	validateTableSize();
    }

    public void setOrientation(int orientation)
    {
	this.orientation = orientation;
	parent.repaint();
    }

    public int getNumRows()
    {
	return numRows - 1;
    }

    public void setNumRows(int numRows)
    {
	if((numRows+1) == this.numRows)
	    return;

	int oldCapacity = this.numRows;

	this.numRows = numRows + 1;
	TableValue newValueArray[][] = 
	    new TableValue[this.numRows][this.numColumns];

	if(oldCapacity < numRows){
	    for(int i = 0; i < oldCapacity; i++){
		for(int j = 0; j < numColumns; j++){
		    newValueArray[i][j] = tableValues[i][j];
		}
	    }
	}
	else{
	    for(int i = 0; i < numRows; i++){
		for(int j = 0; j < numColumns; j++){
		    newValueArray[i][j] = tableValues[i][j];
		}
	    }
	}
	this.tableValues = newValueArray;
	validateTableSize();
	parent.validateBounds();
    }

    public int getNumColumns()
    {
	return numColumns;
    }

    public void setNumColumns(int numColumns)
    {
	if(numColumns == this.numColumns)
	    return;

	int oldCapacity = this.numColumns;

	this.numColumns = numColumns;
	TableValue newValueArray[][] = 
	    new TableValue[this.numRows][this.numColumns];

	if(oldCapacity < numColumns){
	    for(int i = 0; i < numRows; i++){
		for(int j = 0; j < oldCapacity; j++){
		    newValueArray[i][j] = tableValues[i][j];
		}
	    }
	}
	else{
	    for(int i = 0; i < numRows; i++){
		for(int j = 0; j < numColumns; j++){
		    newValueArray[i][j] = tableValues[i][j];
		}
	    }
	}
	this.tableValues = newValueArray;
	validateTableSize();
	parent.validateBounds();
    }

    public boolean contains(int x, int y)
    {
	return table.contains(x, y);
    }

    public Rectangle getBounds()
    {
	if(table == null)
	    return null;
	return new Rectangle(table);
    }


    public Rectangle getPaintBounds()
    {
	return new Rectangle(table.x - 1,
			     table.y - 1,
			     table.width + 2,
			     table.height + 2);
    }

    public int getColumnWidth()
    {
	return columnWidth;
    }

    public void setColumnWidth(int width)
    {
	this.columnWidth = width;

	Rectangle oldBounds = getBounds();

	validateTableSize();

    }

    public int getRowHeight()
    {
	return rowHeight;
    }

    public void setRowHeight(int height)
    {
	this.rowHeight = height;

	Rectangle oldBounds = getBounds();

	validateTableSize();
    }

    public void handleDoubleClick(int x, int y)
    {
	// figure out which cell was clicked
	int cellHeight = Math.round(table.height/numRows);
	int cellWidth = Math.round(table.width/numColumns);
	int x_offset = x - table.x;
	int y_offset = y - table.y;
	final int rowNumber = Math.round(y_offset/cellHeight);
	final int columnNumber = Math.round(x_offset/cellWidth);
		
	// allow the user to enter a value for the cell

	// set column width

	// set row height

	// want to manage font sizes
	
	final JDialog propertyDialog = new JDialog();
	JPanel buttonPanel = new JPanel();
	JPanel panel = new JPanel();
	JScrollPane scroll = new JScrollPane(panel);

	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints constraints = new GridBagConstraints();
	Border border = new LineBorder(Color.black);

	panel.setBorder(border);
	panel.setLayout(gridbag);
	constraints.insets = new Insets(2, 2, 2, 2);

	JLabel label = new JLabel("Cell Value:");

	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.anchor = GridBagConstraints.WEST;
	constraints.weightx = 1.0;
	constraints.gridx = 0;
	constraints.gridy = 0;
		
	gridbag.setConstraints(label, constraints);
	panel.add(label);

	final JTextField cellTextField = new JTextField(15);
	if(tableValues[rowNumber][columnNumber] != null)
	    cellTextField.setText(tableValues[rowNumber][columnNumber].
				  getValue());

	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.BOTH;
	constraints.anchor = GridBagConstraints.CENTER;
	constraints.weightx = 1.0;
	constraints.gridx = 1;
	constraints.gridy = 0;

	gridbag.setConstraints(cellTextField, constraints);
	panel.add(cellTextField);

	label = new JLabel("Column Width:");

	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.anchor = GridBagConstraints.WEST;
	constraints.weightx = 1.0;
	constraints.gridx = 0;
	constraints.gridy = 1;
		
	gridbag.setConstraints(label, constraints);
	panel.add(label);

	final JTextField columnTextField = new JTextField(15);
	columnTextField.setText(""+columnWidth);

	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.BOTH;
	constraints.anchor = GridBagConstraints.CENTER;
	constraints.weightx = 1.0;
	constraints.gridx = 1;
	constraints.gridy = 1;

	gridbag.setConstraints(columnTextField, constraints);
	panel.add(columnTextField);

	label = new JLabel("Row Height:");

	constraints.gridwidth = GridBagConstraints.RELATIVE;
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.anchor = GridBagConstraints.WEST;
	constraints.weightx = 1.0;
	constraints.gridx = 0;
	constraints.gridy = 2;
		
	gridbag.setConstraints(label, constraints);
	panel.add(label);

	final JTextField rowTextField = new JTextField(15);
	rowTextField.setText(""+rowHeight);

	constraints.gridwidth = GridBagConstraints.REMAINDER;
	constraints.fill = GridBagConstraints.BOTH;
	constraints.anchor = GridBagConstraints.CENTER;
	constraints.weightx = 1.0;
	constraints.gridx = 1;
	constraints.gridy = 2;

	gridbag.setConstraints(rowTextField, constraints);
	panel.add(rowTextField);


	JButton applyButton = new JButton("Apply");
	applyButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		        TableWidget.this.setValueAt(rowNumber, 
						    columnNumber, 
						    cellTextField.getText());

			try{
			    Integer columnWidth = 
				new Integer(columnTextField.getText());

			    TableWidget.this.
				setColumnWidth(columnWidth.intValue());
			}
			catch(NumberFormatException nfe){
			    JOptionPane.showMessageDialog(null,
							  "The column width "+
							  "must be an integer "
							  +"value", 
							  "alert", 
							  JOptionPane.
							  ERROR_MESSAGE);
			}
			try{
			    Integer rowHeight = 
				new Integer(rowTextField.getText());

			    TableWidget.this.
				setRowHeight(rowHeight.intValue());
			}
			catch(NumberFormatException nfe){
			    JOptionPane.showMessageDialog(null,
							  "The row height "+
							  "must be an integer "
							  +"value", 
							  "alert", 
							  JOptionPane.
							  ERROR_MESSAGE);
			}

			TableWidget.this.repaint();
			//PropertyManager.this.applyPropertyChanges();
		}
	    });


	JButton okButton = new JButton("Ok");
	okButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    TableWidget.this.setValueAt(rowNumber, 
						columnNumber, 
						cellTextField.getText());
		    try{
			Integer columnWidth = 
			    new Integer(columnTextField.getText());
			
			TableWidget.this.
			    setColumnWidth(columnWidth.intValue());
		    }
		    catch(NumberFormatException nfe){
			JOptionPane.showMessageDialog(null,
						      "The column width "+
						      "must be an integer "
						      +"value", 
						      "alert", 
						      JOptionPane.
						      ERROR_MESSAGE);
		    }
		    try{
			Integer rowHeight = 
			    new Integer(rowTextField.getText());
			
			TableWidget.this.
			    setRowHeight(rowHeight.intValue());
		    }
		    catch(NumberFormatException nfe){
			JOptionPane.showMessageDialog(null,
						      "The row height "+
						      "must be an integer "
						      +"value", 
						      "alert", 
						      JOptionPane.
						      ERROR_MESSAGE);
		    }
		    TableWidget.this.repaint();

		    //PropertyManager.this.applyPropertyChanges();
		    propertyDialog.dispose();
		}
	    });

	JButton cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    propertyDialog.dispose();
		}
	    });

	buttonPanel.add(applyButton);
	buttonPanel.add(okButton);
	buttonPanel.add(cancelButton);

	propertyDialog.getContentPane().add(scroll, BorderLayout.CENTER);
	propertyDialog.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
//	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
// 	propertyDialog.setBounds((screen.width-450)/2, 
// 				    (screen.height-225)/2, 
// 				    450, 225);
	propertyDialog.show();


    }

    public void setValueAt(int column, int row, String value)
    {
	setValueAt(column, row, value, Color.black);
    }

    public void setValueAt(int column, int row, String value, Color fillColor)
    {
	if(column >= numColumns || row >= numRows)
 	    return;

	if(tableValues[row][column] != null){
	    tableValues[row][column].setValue(value);
	    tableValues[row][column].setFillColor(fillColor);
	}
	else
	    tableValues[row][column] = new TableValue(value, fillColor);
	repaint();
    }

    private void validateTableSize()
    {
	Rectangle oldBounds = getBounds();
	Rectangle paintBounds = null;
	if(table == null)
	    paintBounds = new Rectangle(location);
	else
	    paintBounds = getPaintBounds();

	Graphics2D g2d = (Graphics2D)canvas.getGraphics();
	g2d.setFont(font);
	FontMetrics metrics = g2d.getFontMetrics();
	if(rowHeight < metrics.getHeight())
	    rowHeight = metrics.getHeight() + 5;

	if(showHeaders)
	    this.table = new Rectangle(location.x, 
				       location.y, 
				       numColumns * columnWidth, 
				       numRows * rowHeight);
	else
	    this.table = new Rectangle(location.x, 
				       location.y, 
				       numColumns * columnWidth, 
				       (numRows-1) * rowHeight);
	paintBounds = paintBounds.createUnion(getPaintBounds()).
	    getBounds();
	canvas.repaint(paintBounds);

	if(oldBounds != null)
	    parent.childBoundsChanged(this, oldBounds);
    }

    public boolean isShowHeaders()
    {
	return showHeaders;
    }

    public void showHeaders(boolean showHeaders)
    {
	this.showHeaders = showHeaders;

	validateTableSize();
	parent.validateBounds();
    }

    public void paint(Graphics2D g2d)
    {
	if(isVisible){
// 	    AffineTransform transform = null;
// 	    Rectangle rect = new Rectangle(bounds);
	    
// 	    if(orientation == HORIZONTAL){
// 		transform = AffineTransform.
// 		    getRotateInstance(Math.toRadians(90));
// 		rect = transform.createTransformedShape(rect).getBounds();
// 	    }

	    
	    int horizontalSpacing = table.width/numColumns;
	    int verticalSpacing = table.height/(showHeaders ? 
						numRows : numRows - 1);

	    g2d.setFont(font);
	    
	    g2d.setPaint(Color.white);
	    g2d.fill(table);
	    
	    if(showHeaders){
		Rectangle headerBounds = 
		    new Rectangle(table.x, 
				  table.y,
				  table.width,
				  verticalSpacing);

		g2d.setPaint(Color.lightGray);
		g2d.fill(headerBounds);
	    }

	    g2d.setPaint(Color.black);
	    g2d.draw(table);
	    
	    int currentX = table.x + horizontalSpacing;
	    
	    for(int i = 0; i < numColumns - 1; i++){
		g2d.drawLine(currentX, 
			     table.y, 
			     currentX, 
			     table.y + table.height);
		currentX += horizontalSpacing;
	    }
	    
	    int currentY = table.y + verticalSpacing;
	    
	    for(int i = 0; i < (showHeaders ? numRows : numRows - 1) - 1; i++){
		g2d.drawLine(table.x, 
			     currentY, 
			     table.x + table.width, currentY);
		currentY += verticalSpacing;
	    }
	    
	    FontMetrics metrics = g2d.getFontMetrics();	    
	    
	    // draw the labels if they exist
	    for(int i = (showHeaders ? 0 : 1); i < numRows; i++){
		for(int j = 0; j < numColumns; j++){

		    if(tableValues[i][j] != null){
			g2d.setPaint(tableValues[i][j].getFillColor());
			String label = tableValues[i][j].getValue();
			Rectangle stringBounds = 
			    metrics.getStringBounds(label, g2d).getBounds();
			
			int yOffset = (showHeaders ? i+1 : i)*verticalSpacing;

			Rectangle cellBounds = 
			    new Rectangle(table.x + (j * horizontalSpacing), 
					  table.y + yOffset,
					  horizontalSpacing,
					  verticalSpacing);
			
			while(stringBounds.width > horizontalSpacing){
			    if(label.length() < 2)
				break;
			    label = label.substring(0, label.length() - 1);
			    stringBounds = 
				metrics.getStringBounds(label, g2d).
				getBounds();
			}
			
			int xCoord = cellBounds.x + 
			    Math.round((cellBounds.width - 
					stringBounds.width)/2) + 1;
			
			int yCoord = cellBounds.y - 
			    ((cellBounds.height - 
			      stringBounds.height)/2) - 1;
			
			g2d.drawString(label,
				       xCoord, 
				       yCoord);
		    }
		}
	    }
	}
    }

    public void addDrawable(Drawable drawable)
    {
    }

    public void addFigure(SchematicFigure figure)
    {
    }

    public void clear()
    {
    }


    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y)
    {return null;}

    public void drag(int dx, int dy, Point mouseLocation)
    {
    }


    public Collection getDrawables()
    {
	return null;
    }

    public SchematicFigure getFigureAt(int x, int y)
    {
	return null;
    }

    public SchematicFigure getFigureByName(String name)
    {
	return null;
    }

    public SchematicFigure getFigureByUserObject(Object o)
    {
	return null;
    }

    public String getFullyQualifiedName()
    {
	return "";
    }

    public Point getLocation()
    {
	return location;
    }

    public String getName()
    {
	return "";
    }


    public SchematicCanvasContainer getParent()
    {
	return parent;
    }

    public Collection getSubFigures()
    {
	return null;
    }

    /**
     * This function returns a String to be displayed in a tooltip when
     * the mouse hovers over this figure.
     *
     * @return the tooltip text to be displayed
     */
    public String getTooltipText()
    {
	return "";
    }


    public Object getUserObject()
    {
	return null;
    }

    public void handleCommand(String command)
    {

    }


    public boolean isSelected()
    {
	return false;
    }

    public boolean isVisible()
    {
	return true;
    }

    public void repaint()
    {
	canvas.repaint(table);
    }

    public void setLocation(int x, int y)
    {
	int dx = x - location.x;
	int dy = y - location.y;

	this.location = new Point(x, y);
	table.translate(dx, dy);
    }

    /**
     * Set the parent of this figure to be the SchematicCanvasContainer defined
     * by the parameter parent.
     *
     * @param parent the new SchematicCanvasContainer that owns this figure
     */
    public void setParent(SchematicCanvasContainer parent)
    {
	if(parent instanceof SchematicFigure)
	    this.parent = (SchematicFigure)parent;
	//FIXME
    }

    public void setSelected(boolean b)
    {
    }

    public void setVisible(boolean b)
    {
	isVisible = b;
    }

    public void translate(int dx, int dy)
    {
	location.translate(dx, dy);
	table.translate(dx, dy);
    }

    public void validateBounds()
    {
    }

    public void validateChildLocation(SchematicFigure figure, Point point)
    {
    }

    /**
     * This function is used to set the location of this figure to x and y
     * coordinates which are divisible by the integer value 10.
     *
     */
    public void snapToGrid()
    {
    }
}

class TableValue{
    private String value;
    private Color fillColor;

    public TableValue(String value, Color fillColor)
    {
	this.value = value;
	this.fillColor = fillColor;
    }

    public String getValue()
    {
	return new String(value);
    }

    public Color getFillColor()
    {
	return fillColor;
    }

    public void setFillColor(Color c)
    {
	this.fillColor = c;
    }

    public void setValue(String v)
    {
	this.value = v;
    }
}
