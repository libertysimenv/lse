package Liberty.visualizer.canvas.figure;

import Liberty.visualizer.document.*;
import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.lse.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.view.*;

import Liberty.LSS.IMR.*;
import Liberty.LSS.types.StringValue;

import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.swing.*;

/**
 * The SchematicFigureBuilder class is used to build figures for the 
 * SchematicCanvas.
 *
 */

public class SchematicFigureBuilder{

    /**
     * Function to build a PluggableInstanceFigure for the given instance.
     *
     * @param view the SchematicView that will own the figure to be built
     * @param parent the parent view element of this figure
     * @param instance the LSE IMR instance that this figure will represent
     * @param propertyList a list of properties associated with this figure
     */
    public static PluggableInstanceFigure 
	buildInstanceFigure(SchematicView view,
			    SchematicViewElement parent,
			    Instance instance,
			    java.util.List propertyList)
    {
	SchematicCanvas canvas = view.getCanvas();
	SchematicCanvasContainer parentContainer = parent.getSchematicFigure();

	// if the parentContainer is a LayerViewElement, it does have a
	// SchematicFigure, but its semanticValue is a SchematicCanvasLayer,
	// so use that.
	if(parentContainer == null){
	    Object obj = parent.getSemanticValue();
	    if(obj instanceof SchematicCanvasContainer)
		parentContainer = (SchematicCanvasContainer)obj;
	}

	// let's look for a property that declares the class file to use
	// for this figure and also see if there is a statement that declares
	// another property file to use.
	String className = null;
	Iterator propertyIterator = propertyList.iterator();
	while(propertyIterator.hasNext()){
	    PropertyInfo info = (PropertyInfo)propertyIterator.next();

	    if(info.getPropertyName().equals("Display Class")){
		className = (String)info.getValue();
		// remove this property so that when the class is loaded
		// it does not fire a propertyChanged event
		propertyIterator.remove();
	    }
	}
	propertyIterator = null;

	PluggableInstanceFigure instanceFigure = null;

	if(className != null){
	    // use the reflection API to build the figure from the className
	    try{
		Class classObj = Class.forName(className);
		Constructor ct = classObj.getConstructors()[0];
		Object arglist[] = new Object[3];
		arglist[0] = canvas;
		arglist[1] = parentContainer;
		arglist[2] = instance;

		instanceFigure = 
		    (PluggableInstanceFigure)ct.newInstance(arglist);
	    }
	    catch(ClassNotFoundException cnfe){
		instanceFigure = new DefaultInstanceFigure(canvas, 
							   parentContainer,
							   instance);
		System.out.println("Class Not Found Exception: "+
				   cnfe.getMessage());
		JOptionPane.showMessageDialog(null, "The specified display"
					      +" class: '"+
					      className+
					      "' for instance '"+
					      instance.getName()+
					      "' could not be found.", 
					      "Notice", 
					      JOptionPane.
					      ERROR_MESSAGE);
	    }
	    catch(InstantiationException ie){
		instanceFigure = new DefaultInstanceFigure(canvas, 
							   parentContainer,
							   instance);

		System.out.println("Instantiation Exception: "+
				   ie.getMessage());
		JOptionPane.showMessageDialog(null, "The specified display"
					      +" class: '"+
					      className+
					      "' for instance '"+
					      instance.getName()+
					      "' could not be instantiated.", 
					      "Notice", 
					      JOptionPane.
					      ERROR_MESSAGE);
	    }
	    catch(IllegalAccessException iae){
		instanceFigure = new DefaultInstanceFigure(canvas, 
							   parentContainer,
							   instance);

		System.out.println("Illegal Access Exception: "+
				   iae.getMessage());
		JOptionPane.showMessageDialog(null, "The specified display"
					      +" class: '"+
					      className+
					      "' for instance '"+
					      instance.getName()+
					      "' could not be loaded.", 
					      "Notice", 
					      JOptionPane.
					      ERROR_MESSAGE);
	    }
	    catch(InvocationTargetException ite){
		instanceFigure = new DefaultInstanceFigure(canvas, 
							   parentContainer,
							   instance);
		System.out.println("Invocation Target Exception: "+
				   ite.getMessage());
		//ite.printStackTrace();
		JOptionPane.showMessageDialog(null, "The specified display"
					      +" class: '"+
					      className+
					      "' for instance '"+
					      instance.getName()+
					      "' could not be loaded.", 
					      "Notice", 
					      JOptionPane.
					      ERROR_MESSAGE);
	    }
	}
	else{
	    // no class file is specified so let's build the default 
	    // instance figure
	    instanceFigure = new DefaultInstanceFigure(canvas, 
						       parentContainer,
						       instance);
	}

	// build the port figures
	Iterator portIterator = instance.getPorts().iterator();
	Vector inputPortFigures = new Vector();
	Vector outputPortFigures = new Vector();
	
	int inputPortsHeight = 0, outputPortsHeight = 0;
	
	while(portIterator.hasNext()){
	    Port port = (Port)portIterator.next();
	    int numConnects = port.getConnections().size();
	    
	    PortFigure portFigure = buildPortFigure(view,
						    port, 
						    instanceFigure);
	    
	    if(port.getDirection() == Port.INPUT){
		inputPortFigures.add(portFigure);
		inputPortsHeight += portFigure.getBounds().height;
	    }
	    else{
		outputPortFigures.add(portFigure);
		outputPortsHeight += portFigure.getBounds().height;
	    }
	    
	    instanceFigure.addFigure(portFigure);
	}

	instanceFigure.layoutPorts();

	instanceFigure.handleStoredProperties(propertyList);

	return instanceFigure;
    }


    /**
     * Function to build a PluggableInstanceFigure for the given instance
     * using the class specified by the string className.  This function is
     * ONLY to be used when instance display classes are being changed on
     * the fly.
     *
     * @param view the SchematicView that will own the figure to be built
     * @param parent the parent view element of this figure
     * @param instance the LSE IMR instance that this figure will represent
     * @param propertyList a list of properties associated with this figure
     * @param className the name of the PluggableInstanceFigure descendent 
     *                  class that should be used to build this figure
     */
    public static PluggableInstanceFigure 
	buildInstanceFigure(SchematicView view,
			    SchematicViewElement parent,
			    Instance instance,
			    java.util.List propertyList,
			    String className)
    {
	SchematicCanvas canvas = view.getCanvas();
	SchematicCanvasContainer parentContainer = parent.getSchematicFigure();

	// if the parentContainer is a LayerViewElement, it does have a
	// SchematicFigure, but its semanticValue is a SchematicCanvasLayer,
	// so use that.
	if(parentContainer == null){
	    Object obj = parent.getSemanticValue();
	    if(obj instanceof SchematicCanvasContainer)
		parentContainer = (SchematicCanvasContainer)obj;
	}

	PluggableInstanceFigure instanceFigure = null;

	try{
	    Class classObj = Class.forName(className);
	    Constructor ct = classObj.getConstructors()[0];
	    Object arglist[] = new Object[3];
	    arglist[0] = view.getCanvas();
	    arglist[1] = parentContainer;
	    arglist[2] = instance;

	    instanceFigure = (PluggableInstanceFigure)ct.newInstance(arglist);
	    
	}
	catch(ClassNotFoundException cnfe){
	    instanceFigure = new DefaultInstanceFigure(canvas, 
						       parentContainer,
						       instance);
	    System.out.println("Class Not Found Exception: "+
			       cnfe.getMessage());
	    JOptionPane.showMessageDialog(null, "The specified display"
					  +" class: '"+
					  className+
					  "' for instance '"+
					  instance.getName()+
					  "' could not be found.", 
					  "Notice", 
					  JOptionPane.
					  ERROR_MESSAGE);
	}
	catch(InstantiationException ie){
	    instanceFigure = new DefaultInstanceFigure(canvas, 
						       parentContainer,
						       instance);
	    
	    System.out.println("Instantiation Exception: "+ie.getMessage());
	    JOptionPane.showMessageDialog(null, "The specified display"
					  +" class: '"+
					  className+
					  "' for instance '"+
					  instance.getName()+
					  "' could not be instantiated.", 
					  "Notice", 
					  JOptionPane.
					  ERROR_MESSAGE);
	    
	}
	catch(IllegalAccessException iae){
	    instanceFigure = new DefaultInstanceFigure(canvas, 
						       parentContainer,
						       instance);
	    
	    System.out.println("Illegal Access Exception: "+iae.getMessage());
	    JOptionPane.showMessageDialog(null, "The specified display"
					  +" class: '"+
					  className+
					  "' for instance '"+
					  instance.getName()+
					  "' could not be loaded.", 
					  "Notice", 
					  JOptionPane.
					  ERROR_MESSAGE);
	    
	}
	catch(InvocationTargetException ite){
	    instanceFigure = new DefaultInstanceFigure(canvas, 
						       parentContainer,
						       instance);
	    System.out.println("Invocation Target Exception: "+
			       ite.getMessage());
	    //ite.printStackTrace();
	    JOptionPane.showMessageDialog(null, "The specified display"
					  +" class: '"+
					  className+
					  "' for instance '"+
					  instance.getName()+
					  "' could not be loaded.", 
					  "Notice", 
					  JOptionPane.
					  ERROR_MESSAGE);   
	}
	
	// build the port figures
	Iterator portIterator = instance.getPorts().iterator();
	Vector inputPortFigures = new Vector();
	Vector outputPortFigures = new Vector();
	
	int inputPortsHeight = 0, outputPortsHeight = 0;
	
	while(portIterator.hasNext()){
	    Port port = (Port)portIterator.next();
	    int numConnects = port.getConnections().size();
	    
	    PortFigure portFigure = buildPortFigure(view,
						    port, 
						    instanceFigure);

	    // we need to get the ViewElement for this port figure and
	    // set its SchematicFigure to be portFigure
	    SchematicViewElement viewElement = 
		view.getViewElement(instance.getFullyQualifiedName()+"."+
				    port.getName());
	    if(viewElement != null){
		viewElement.setSchematicFigure(portFigure);
	    }
	    
	    if(port.getDirection() == Port.INPUT){
		inputPortFigures.add(portFigure);
		inputPortsHeight += portFigure.getBounds().height;
	    }
	    else{
		outputPortFigures.add(portFigure);
		outputPortsHeight += portFigure.getBounds().height;
	    }
	    
	    instanceFigure.addFigure(portFigure);
	}

	instanceFigure.layoutPorts();

	instanceFigure.handleStoredProperties(propertyList);

	return instanceFigure;
    }
			    

    public static PortFigure buildPortFigure(SchematicView view,
					     Port port,
					     PluggableInstanceFigure parent)
    {
	SchematicCanvas canvas = view.getCanvas();
	PortFigure portFig = new PortFigure(canvas,
					    parent,
					    port);

	// used to sort the port instance figures before adding them to
	// the portfigure
	Vector portInstanceFigures = new Vector();

	Iterator connIter = port.getConnections().iterator();
	while(connIter.hasNext()){
	    String instString;
	    Connection conn = (Connection)connIter.next();
	    int instNumber = (port.getDirection() == Port.OUTPUT) ?
		conn.getSourceInstance() : conn.getDestInstance();

	    instString = new String(""+instNumber);

	    PortInstanceFigure piFig = 
		buildPortInstanceFigure(view,
					instString,
					portFig.getConnectSite(),
					portFig);

	    for(int i = 0; i < portInstanceFigures.size(); i++){
		PortInstanceFigure tmpFig = (PortInstanceFigure)
		    portInstanceFigures.elementAt(i);
		if(instNumber < tmpFig.getInstanceNumber().intValue()){
		    portInstanceFigures.add(i, piFig);
		    break;
		}
	    }
	    if(!portInstanceFigures.contains(piFig))
		portInstanceFigures.add(piFig);
	}
	Iterator pifIterator = portInstanceFigures.iterator();
	while(pifIterator.hasNext()){
	    portFig.addPortInstance((PortInstanceFigure)pifIterator.next());
	}
	portFig.layoutInstances();
	return portFig;
    }

    public static PortInstanceFigure 
	buildPortInstanceFigure(SchematicView view,
				String name,
				int connectSite,
				PortFigure parent)
    {
	SchematicCanvas canvas = view.getCanvas();
	PortInstanceFigure piFig = new PortInstanceFigure(canvas,
							  parent, 
							  name, 
							  connectSite);
	return piFig;
    }

    public static SchematicFigure 
	buildConnectionFigure(SchematicCanvas canvas,
			      String name,
			      Connection conn,
			      SchematicNodeFigure src,
			      SchematicNodeFigure dest, 
			      java.util.List propertyList)
    {
	SchematicCanvasLayer layer = canvas.getLayer("Connections:");

	DefaultConnectionFigure connFig = new DefaultConnectionFigure(canvas,
								      layer,
								      conn, 
								      src,
								      dest);

	connFig.handleStoredProperties(propertyList);
	return connFig;
    }

    public static SchematicFigure 
	buildStubFigure(SchematicCanvas canvas,
			String name,
			Connection conn,
			SchematicNodeFigure src,
			SchematicNodeFigure dest, 
			java.util.List propertyList)
    {
	SchematicCanvasLayer layer = canvas.getLayer("Connections:");

	StubFigure stubFig = new StubFigure(canvas,
					    layer,
					    conn, 
					    src,
					    dest);

	stubFig.handleStoredProperties(propertyList);
	return stubFig;
    }

}
