package Liberty.visualizer.canvas.figure;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.lse.*;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

public class SchematicFigureLayout{

    public static void layoutFigures(SchematicCanvas canvas){
	Iterator layerIter = canvas.getLayers().iterator();

	// lay out the figures on each layer
	while(layerIter.hasNext()){
	    SchematicCanvasLayer layer = 
		(SchematicCanvasLayer)layerIter.next();
	    SchematicNodeFigure root = null;
	    HashSet unseenNodes = new HashSet();
	    HashSet currentNodes = new HashSet();
	    HashSet adjacentNodes = new HashSet();
	    int leastInConnects = Integer.MAX_VALUE;

	    // find the node with the least number of in edges
	    Iterator figureIterator = layer.getFigures().iterator();
	    while(figureIterator.hasNext()){
		SchematicFigure figure = 
		    (SchematicFigure)figureIterator.next();

		if(figure instanceof SchematicNodeFigure){
		    SchematicNodeFigure nodeFigure = 
			(SchematicNodeFigure)figure;
		    Point nodeLocation = nodeFigure.getLocation();

		    // if nodeLocation == (-1, -1) then it doesn't need to
		    // be positioned
		    if(nodeLocation.x == -1){
			int numInNodes = nodeFigure.getInEdges().size();
			int numOutNodes = nodeFigure.getOutEdges().size();

			unseenNodes.add(nodeFigure);

			if(numInNodes < leastInConnects && numOutNodes > 0)
			    root = nodeFigure;
		    }
		}
	    }

	    if(root != null)
		currentNodes.add(root);

	    final int dx = 200;
	    final int dy = 80;
	    int currentXcoord = 25;
	    int currentYcoord = 30;

	    while(!unseenNodes.isEmpty()){
		int nodeWidth = 0;
		// if there are no current nodes, add the first remaining 
		// in element unseenNodes.  This case should be very rare...
		if(currentNodes.isEmpty()){
		    SchematicFigure figure = 
			(SchematicFigure)unseenNodes.iterator().next();
		    currentNodes.add(figure);
		}

		HashSet additionalCurrentNodes = new HashSet();

		// build a list of adjacent nodes and add the source nodes
		// of input edges on adjacent nodes to the current list
		Iterator currentNodeIterator = currentNodes.iterator();
		while(currentNodeIterator.hasNext()){
		    SchematicNodeFigure currentNode = 
			(SchematicNodeFigure)currentNodeIterator.next();
		    Iterator outEdgeIterator = 
			currentNode.getOutEdges().iterator();

		    while(outEdgeIterator.hasNext()){
			SchematicEdgeFigure outEdge = 
			    (SchematicEdgeFigure)outEdgeIterator.next();
			SchematicNodeFigure destNode = 
			    outEdge.getDestFigure();

			if(destNode != null){

			    if(unseenNodes.contains(destNode)){
				adjacentNodes.add(destNode);
			    }

			    Iterator inEdgeIterator = 
				destNode.getInEdges().iterator();
			    while(inEdgeIterator.hasNext()){
				SchematicEdgeFigure inEdge = 
				    (SchematicEdgeFigure)inEdgeIterator.next();
				SchematicFigure sourceFigure = 
				    inEdge.getSrcFigure();

				if(sourceFigure != null){
				    if(unseenNodes.contains(sourceFigure)){
					additionalCurrentNodes.
					    add(sourceFigure);
				    }
				}
			    }
			}
		    }
		}

		currentNodes.addAll(additionalCurrentNodes);
		additionalCurrentNodes = null;

		// position all of the nodes in currentNodes
		currentNodeIterator = currentNodes.iterator();
		while(currentNodeIterator.hasNext()){
		    SchematicNodeFigure currentNode = 
			(SchematicNodeFigure)currentNodeIterator.next();
		    Rectangle bounds = currentNode.getBounds();

		    unseenNodes.remove(currentNode);
		    currentNode.setLocation(currentXcoord, currentYcoord);
		    currentYcoord += (dy + bounds.height);
		    if(bounds.width > nodeWidth)
			nodeWidth = bounds.width;
		}
		currentXcoord += (dx + nodeWidth);
		currentNodes.removeAll(currentNodes);
		currentNodes.addAll(adjacentNodes);
		adjacentNodes.removeAll(adjacentNodes);
		currentYcoord = 30;
	    }
	}
    }
}
