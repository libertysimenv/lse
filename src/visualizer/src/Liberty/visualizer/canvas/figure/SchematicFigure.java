package Liberty.visualizer.canvas.figure;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.properties.*;

import java.awt.*;
import java.util.*;
import javax.swing.*;

public interface SchematicFigure extends SchematicCanvasContainer, 
					 javax.swing.SwingConstants{
    
    public boolean contains(int x, int y);

    /**
     * The SchematicCanvas which owns this figure will call this function
     * when a popup-triggering event occurs on this figure.
     *
     * @param component the component which triggered this event 
     *                  (usually the canvas)
     * @param x the x coordinate of the event
     * @param y the y coordinate of the event
     * @return the popup menu for this figure
     */
    public JPopupMenu getPopupMenu(Component component, 
				   final int x, final int y);

    public void drag(int dx, int dy, Point mouseLocation);

    public Point getLocation();

    public String getFullyQualifiedName();

    public String getName();

    public Object getUserObject();

    public String getTooltipText();

    public void handleCommand(String command);

    public void handleDoubleClick(int x, int y);

    public boolean isSelected();

    public boolean isVisible();
    
    public void setLocation(int x, int y);

    public void setSelected(boolean b);

    public void setVisible(boolean b);

    public void snapToGrid();

    public void translate(int dx, int dy);

    public void validateBounds();
}
