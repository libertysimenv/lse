package Liberty.visualizer.canvas.figure;

import java.util.*;

public interface SchematicEdgeFigure extends SchematicFigure{

    public SchematicNodeFigure getDestFigure();

    public SchematicNodeFigure getSrcFigure();

    public void setDestFigure(SchematicNodeFigure destFigure);

    public void setSrcFigure(SchematicNodeFigure srcFigure);
}
