package Liberty.visualizer.canvas;

import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;

import java.awt.*;
import java.util.Collection;

public interface SchematicCanvasContainer{

    public void addDrawable(Drawable drawable);

    public void addFigure(SchematicFigure figure);

    public void childBoundsChanged(SchematicFigure figure, 
				   Rectangle oldBounds);

    public void clear();

    public Rectangle getBounds();

    public Collection getDrawables();

    public SchematicFigure getFigureAt(int x, int y);

    public SchematicFigure getFigureByName(String name);

    public SchematicFigure getFigureByUserObject(Object o);

    public Rectangle getPaintBounds();

    public SchematicCanvasContainer getParent();

//     public float getScaleFactorX();

//     public float getScaleFactorY();

    public Collection getSubFigures();

//     public boolean isScaled();

    public void paint(Graphics2D g2d);

    public void repaint();

    public void setParent(SchematicCanvasContainer parent);

    public void validateChildLocation(SchematicFigure child, Point eventPoint);

}
