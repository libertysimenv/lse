package Liberty.visualizer.canvas;

import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;
import Liberty.visualizer.canvas.figure.lse.*;
import Liberty.visualizer.document.*;
import Liberty.visualizer.properties.*;
import Liberty.visualizer.UI.frame.*;
import Liberty.visualizer.UI.dialog.PropertyEditorDialog;
import Liberty.visualizer.view.*;
import Liberty.visualizer.view.lse.*;

import Liberty.LSS.IMR.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

public class SchematicCanvas extends JPanel{
    // FIXME: there should be an extension: LSECanvas
    // FIXME: this class should implement canvas container
    /** Is this canvas locally scaled */
    private boolean isScaled;
    /** A vector of the layers that this canvas owns */
    private Vector layers;
    /** the current canvas scale factor */
    private float scaleFactor;
    /** the color used to paint the selection rectangle */
    private Color selectionColor;
    /** the point where the mouse was last pressed */
    private Point mouseDownPoint;
    /** the point where the last mouse event occurred */
    private Point mouseLocation;
    /** the document associated with this canvas */
    private Document document;
    /** the SchematicView that owns this canvas */
    private SchematicView view;
    /** the boundary of the drag region when selecting multiple elements */
    private Rectangle selectionBounds;
    /** is the canvas in snap-to-grid mode */
    private boolean isSnapToGrid;
    /** is an element being dragged on the canvas*/
    private boolean isDragging;
    /** the current width of the canvas */
    private int canvasWidth = 5500;
    /** the current height of the canvas */
    private int canvasHeight = 3500;
    /** is the canvas currently paintable (used during initialization) */
    private boolean isPaintable;

    public SchematicCanvas(Document document, SchematicView view)
    {
	super();
	this.scaleFactor = 1.0f;
	this.layers = new Vector();
	this.mouseDownPoint = new Point();
	this.mouseLocation = new Point();
	this.document = document;
	this.view = view;
	this.selectionBounds = null;
	this.selectionColor = new Color(59, 140, 239, 100);
	this.isScaled = false;
	this.isSnapToGrid = false;
	this.isDragging = false;
	this.isPaintable = false;

	setBackground(Color.white);
	setBounds(0, 0, canvasWidth, canvasHeight);
	setPreferredSize(new Dimension(canvasWidth, canvasHeight));

	installMouseListeners();
    }

    public void addLayer(SchematicCanvasLayer layer)
    {
	layers.add(layer);
    }

    public void clear()
    {
	layers.clear();
    }

    public boolean contains(int x, int y)
    {
	// undo any scaling on the coordinate
	Point eventPoint = 
	    new Point((int)(x*(1/scaleFactor)),
		      (int)(y*(1/scaleFactor)));
	SchematicFigure figure = getFigureAt(eventPoint.x, 
					     eventPoint.y);

	if(figure != null){
	    setToolTipText(figure.getTooltipText());
	}
	else
	    setToolTipText(null);
	return true;
    }

    public void doPopup(Component component, int x, int y)
    {
	JPopupMenu popup = view.getPopupMenu(component, x, y);

	popup.show(component, x, y);
    }

    public Document getDocument()
    {
	return document;
    }

    public SchematicFigure getFigureAt(int x, int y)
    {
	Iterator layerIter = layers.iterator();

	while(layerIter.hasNext()){
	    SchematicFigure ret;
	    SchematicCanvasLayer layer = 
		(SchematicCanvasLayer)layerIter.next();

	    if((ret = layer.getFigureAt(x, y)) != null)
		return ret;
	}
	return null;
    }

    public SchematicFigure getFigureByUserObject(Object o)
    {
	Iterator layerIter = layers.iterator();
	while(layerIter.hasNext()){
	    SchematicFigure ret;
	    SchematicCanvasLayer layer = 
		(SchematicCanvasLayer)layerIter.next();
	    if((ret = layer.getFigureByUserObject(o)) != null)
		return ret;
	}
	return null;
    }

    public SchematicCanvasLayer getLayer(String name)
    {
	Iterator layerIter = layers.iterator();
	while(layerIter.hasNext()){
	    SchematicCanvasLayer layer = 
		(SchematicCanvasLayer)layerIter.next();

	    if(layer.getName().equals(name)){
		return layer;
	    }
	}

	return null;
    }

    public Vector getLayers()
    {
	return layers;
    }

    public float getScaleFactor()
    {
	return scaleFactor;
    }

    public SchematicView getView()
    {
	return view;
    }

    private void installMouseListeners()
    {
	addMouseListener(new MouseAdapter(){
		public void mouseReleased(MouseEvent e)
		{
		    if(isDragging && isSnapToGrid){
			view.snapDraggedSelections();
		    }
		    if(selectionBounds != null){
			Rectangle paintBounds = new Rectangle(selectionBounds);
			view.selectBoundedElements(selectionBounds);
			selectionBounds = null;
			paintBounds.setRect(paintBounds.x - 2, 
					    paintBounds.y - 2,
					    paintBounds.width + 4, 
					    paintBounds.height + 4);

			repaint(paintBounds);
		    }
		}
		public void mousePressed(MouseEvent e)
		{
		    Point eventPoint = 
			new Point((int)(e.getX()*(1/scaleFactor)),
				  (int)(e.getY()*(1/scaleFactor)));

		    SchematicFigure figure = getFigureAt(eventPoint.x, 
							 eventPoint.y);
		    mouseDownPoint.setLocation(eventPoint);

		    if((e.getButton() == MouseEvent.BUTTON3)||
		      ((e.getButton() == MouseEvent.BUTTON1) && (e.isControlDown()))){
			//e.isPopupTrigger()){
			if(figure != null){
			    view.setSelectedElement(figure.
						    getFullyQualifiedName());
			    JPopupMenu popup = 
				figure.getPopupMenu(SchematicCanvas.this, 
						    eventPoint.x,
						    eventPoint.y);
			    if(popup != null)
				popup.show(SchematicCanvas.this, 
					   e.getX(), 
					   e.getY());
			}
			else{
			    doPopup(e.getComponent(),
				    (int)e.getX(),
				    (int)e.getY());
			}
		    }
		    else{
			mouseLocation.setLocation(eventPoint);
			if(figure == null)
			    view.setSelectedElement(null);
			else{
			    if(e.getClickCount() == 2){
				figure.handleDoubleClick(eventPoint.x,
							 eventPoint.y);
			    }
			    else{
				if(e.isShiftDown())
				    view.addElementToSelection(figure.
					 getFullyQualifiedName());
				else{
				    if(view.isSelectionEmpty() || 
				       !figure.isSelected()){
					view.setSelectedElement(figure.
					     getFullyQualifiedName());
				    }
				    
				}
			    }
			}
		    }
		}
	    });
	addMouseMotionListener(new MouseMotionListener(){
		public void mouseDragged(MouseEvent e)
		{
		    Point eventPoint = 
			new Point((int)(e.getX()*(1/scaleFactor)),
				  (int)(e.getY()*(1/scaleFactor)));

		    if(!view.isSelectionEmpty()){
			isDragging = true;
			view.doTranslation(eventPoint.x - mouseLocation.x,
					   eventPoint.y - mouseLocation.y,
					   eventPoint);
		    }
		    else{
			Rectangle paintBounds;

			if(selectionBounds == null){
			    selectionBounds = new Rectangle(mouseDownPoint);
			    selectionBounds.add(eventPoint);
			    paintBounds = new Rectangle(selectionBounds);
			}
			else{
			    paintBounds = new Rectangle(selectionBounds);
			    selectionBounds = new Rectangle(mouseDownPoint);
			    selectionBounds.add(eventPoint);
			    paintBounds = paintBounds.
				createUnion(selectionBounds).getBounds();
			}
			paintBounds.setRect(paintBounds.x - 2, 
					    paintBounds.y - 2,
					    paintBounds.width + 4, 
					    paintBounds.height + 4);

			repaint(paintBounds);
		    }
		    mouseLocation.setLocation(eventPoint);

		}
		
		public void mouseMoved(MouseEvent e){

		}
	    });
    }

    public void paintComponent(Graphics g)
    {
 	super.paintComponent(g);
	if(isPaintable){
	    Rectangle clip = g.getClipBounds();
	    Graphics2D g2d = (Graphics2D)g;
	    
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				 RenderingHints.VALUE_ANTIALIAS_ON);
	    
	    g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				 RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	    
	    //FIXME
// 	    if(isSnapToGrid){
// 		g2d.setPaint(Color.lightGray.brighter());
// 		for(int i=50; i <= CANVAS_SIZE; i+=50){
// 		    Line2D.Float line = new Line2D.Float(i, 0, i, CANVAS_SIZE);
// 		    g2d.draw(line);
// 		    line = new Line2D.Float(0, i, CANVAS_SIZE, i);
// 		    g2d.draw(line);
// 		}
// 	    }
	    
	    
	    AffineTransform at = null;
	    if(scaleFactor != 1.0){
		at = AffineTransform.getScaleInstance(scaleFactor, 
						      scaleFactor);
		g2d.transform(at);
		clip.setRect(Math.round((1/scaleFactor) * clip.x),
			     Math.round((1/scaleFactor) * clip.y),
			     Math.round((1/scaleFactor) * clip.width),
			     Math.round((1/scaleFactor) * clip.height));
		
		AffineTransform atInv = 
		    AffineTransform.getScaleInstance(1.0f/scaleFactor, 
						     1.0f/scaleFactor);
	    }
	    
	    for(int index = layers.size()-1; index >=0; index--){
		((SchematicCanvasLayer)layers.elementAt(index)).
		    paint(g2d, clip);
	    }
	    
	    if(selectionBounds != null){
		g2d.setPaint(selectionColor.darker());
		g2d.draw(selectionBounds);
		g2d.setPaint(selectionColor);
		g2d.fill(selectionBounds);
	    }
	}
    }

    public void repaint(Rectangle repaintRegion){
	super.repaint(new Rectangle(Math.round(scaleFactor*repaintRegion.x),
				    Math.round(scaleFactor*repaintRegion.y),
				    Math.round(scaleFactor*
					       repaintRegion.width),
				    Math.round(scaleFactor*
					       repaintRegion.height)));
    }

    public void setScaleFactor(float scaleFactor)
    {
	if(this.scaleFactor != scaleFactor){
	    this.scaleFactor = scaleFactor;
	    int scaledWidth = Math.round(canvasWidth * scaleFactor);
	    int scaledHeight = Math.round(canvasHeight * scaleFactor);

	    setBounds(0, 0, scaledWidth, scaledHeight);
	    setPreferredSize(new Dimension(scaledWidth, scaledHeight));
	    if(scaleFactor != 1.0f)
		isScaled = true;
	    repaint();
	}
    }

    public void setWidth(int width)
    {
	canvasWidth = width;

	int scaledWidth = Math.round(canvasWidth * scaleFactor);
	int scaledHeight = Math.round(canvasHeight * scaleFactor);
	setBounds(0, 0, scaledWidth, scaledHeight);
	setPreferredSize(new Dimension(scaledWidth, scaledHeight));
    }

    public void setHeight(int height)
    {
	canvasHeight = height;

	int scaledWidth = Math.round(canvasWidth * scaleFactor);
	int scaledHeight = Math.round(canvasHeight * scaleFactor);
	setBounds(0, 0, scaledWidth, scaledHeight);
	setPreferredSize(new Dimension(scaledWidth, scaledHeight));
    }

    public void setSnapToGrid(boolean b)
    {
	isSnapToGrid = b;
    }

    public void createAndSwapFigure(PluggableInstanceFigure figure, 
				    String className)
    {
	// FIXME: this is lse specific -- there should be an LSE specific
	// subclass af the canvas...
	// FIXME: there should be a param for swapping the ports
	//System.out.println("swapping "+figure+" with "+className);
	SchematicCanvasLayer layer = (SchematicCanvasLayer)figure.getParent();
	SchematicViewElement layerElement = 
	    view.getViewElement(layer.getName());
	Instance instance = (Instance)figure.getUserObject();


	PluggableInstanceFigure newFigure = SchematicFigureBuilder.
	    buildInstanceFigure(view, 
				layerElement, 
				instance, 
				((LSSDocument)document).
				getPropertiesForViewElement(instance.
					     getFullyQualifiedName()),
				className);

	layer.removeFigure(figure);
	Point location = figure.getLocation();
	newFigure.setLocation(location.x, location.y);
	layer.addFigure(newFigure);

	InstanceViewElement ive = (InstanceViewElement)view.
	    getViewElement(instance.getFullyQualifiedName());
	    
	ive.setSchematicFigure(newFigure);

	// add the edges to the new instance figure
	Collection outEdges = figure.getOutEdges();
	Collection inEdges = figure.getInEdges();
	
	Iterator edgeIterator = outEdges.iterator();
	while(edgeIterator.hasNext()){
	    SchematicEdgeFigure edgeFigure =
		(SchematicEdgeFigure)edgeIterator.next();

	    newFigure.addOutEdge(edgeFigure);
	    edgeFigure.setSrcFigure(newFigure);
	}
	
	edgeIterator = inEdges.iterator();
	while(edgeIterator.hasNext()){
	    SchematicEdgeFigure edgeFigure =
		(SchematicEdgeFigure)edgeIterator.next();

	    newFigure.addInEdge(edgeFigure);
	    edgeFigure.setDestFigure(newFigure);
	}
    }

    public boolean isSnapToGrid()
    {
	return isSnapToGrid;
    }

    public void setPaintable(boolean b)
    {
	this.isPaintable = b;
    }
}
