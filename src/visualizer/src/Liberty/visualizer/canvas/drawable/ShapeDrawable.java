package Liberty.visualizer.canvas.drawable;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.SchematicFigure;

import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;

public class ShapeDrawable implements Drawable{

    /** the canvas that this will be drawn on */
    private SchematicCanvas canvas;
    /** the fill color for this shape */
    private Color fillColor;
    /** the location of this drawable */
    private Point location;
    /** the name used to reference this drawable */
    private String name;
    /** the parent of this drawable */
    private SchematicFigure parent;
    /** the fill color for this shape when it is selected */
    private Color selectedFillColor;
    /** the shape that will be drawn */
    private Shape shape;

    public ShapeDrawable(SchematicCanvas canvas,
			 SchematicFigure parent,
			 String name,
			 Shape shape, 
			 Color fillColor, 
			 Color selectedFillColor)
    {
	this.canvas = canvas;
	this.parent = parent;
	this.name = name;
	this.shape = shape;
	this.fillColor = fillColor;
	this.selectedFillColor = selectedFillColor;

	this.location = new Point(0, 0);
    }

    public Rectangle getBounds()
    {
	return shape.getBounds();
    }

    public Color getFillColor()
    {
	return fillColor;
    }

    public String getFullyQualifiedName()
    {
	return parent.getFullyQualifiedName()+"."+name;
    }

    public Point getLocation()
    {
	return location;
    }

    public String getName()
    {
	return name;
    }

    public Shape getShape()
    {
	return shape;
    }

    public Color getSelectedFillColor()
    {
	return selectedFillColor;
    }

    public void paint(Graphics2D g2d)
    {
	g2d.setPaint(parent.isSelected() ? 
		     selectedFillColor : fillColor);
	g2d.fill(shape);
	g2d.setPaint(Color.black);
	g2d.draw(shape);
    }

    public void setLocation(int x, int y)
    {
	int dx, dy;
	dx = x - location.x;
	dy = y - location.y;

	translate(dx, dy);
    }

    public void setFillColor(Color color)
    {
	this.fillColor = color;
    }

    public void setSelectedFillColor(Color color)
    {
	this.selectedFillColor = color;
    }

    public void setShape(Shape shape)
    {
	this.shape = shape;
    }

    public void setHeight(int height)
    {
	Rectangle bounds = shape.getBounds();

	((RectangularShape)shape).setFrame(location.x, 
					   location.y,
					   bounds.width,
					   height);

    }

    public void setWidth(int width)
    {
	Rectangle bounds = shape.getBounds();

	((RectangularShape)shape).setFrame(location.x, 
					   location.y,
					   width,
					   bounds.height);

    }

    public void translate(int dx, int dy)
    {
	if(shape instanceof Rectangle){
	    ((Rectangle)shape).translate(dx, dy);
	}
	else if(shape instanceof Polygon){
	    ((Polygon)shape).translate(dx, dy);
	}
	else if(shape instanceof RectangularShape){
	    RectangularShape rs = (RectangularShape)shape;
	    Rectangle2D rect = rs.getFrame();

	    rs.setFrame(rect.getX()+dx, rect.getY()+dy, rect.getWidth(), 
			rect.getHeight());
	}

	location.translate(dx, dy);
    }
}
