package Liberty.visualizer.canvas.drawable;

import Liberty.visualizer.properties.*;

import java.awt.*;

public interface Drawable{

    public Rectangle getBounds();

    public String getName();

    public void paint(Graphics2D g2d);

    public void setFillColor(Color color);

    public void setSelectedFillColor(Color color);

    public void translate(int dx, int dy);
}
