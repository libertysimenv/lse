package Liberty.visualizer.canvas.drawable;

import Liberty.visualizer.canvas.*;
import Liberty.visualizer.canvas.figure.*;

import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;

public class LabelDrawable implements Drawable{

    /** the bounds of this label */
    private Rectangle bounds;
    /** the canvas that this will be drawn on */
    private SchematicCanvas canvas;
    /** the fill color for this text */
    protected Color fillColor;
    /** the font used to draw this label */
    private Font font;
    /** the font size to be used */
    private int fontSize;
    /** the justification of the label text around it's location */
    private int justification;
    /** the string that this label will draw */
    private String label;
    /** the location of the center of this string */
    private Point location;
    /** the name used to reference this drawable */
    private String name;
    /** the parent figure of this drawable */
    private SchematicFigure parent;
    /** the fill color for this text when it is selected */
    protected Color selectedFillColor;
    /** the maximum number of characters to be drawn */
    private int truncation;

    /** Constant used to specify center justification */
    public static final int CENTER=0;
    /** Constant used to specify left justification  */
    public static final int LEFT=1;
    /** Constant used to specify right justification  */
    public static final int RIGHT=2;

    public LabelDrawable(SchematicCanvas canvas,
			 SchematicFigure parent,
			 String name,
			 String label)
    {
	this.canvas = canvas;
	this.name = name;
	this.parent = parent;
	this.label = label;

	// Default values
	this.location = new Point(0, 0);
	this.fontSize = 10;
	this.font = null;
	this.truncation = label.length();
	this.justification = LEFT;
	this.fillColor = Color.black;
	this.selectedFillColor = Color.red;
	this.font = canvas.getGraphics().getFont().
	    deriveFont(Font.BOLD, fontSize);
	calculateBounds();
    }

    private void calculateBounds()
    {
	// Caclulate the bounding region for this label
	Graphics2D g2d = (Graphics2D)canvas.getGraphics();
	g2d.setFont(font);

	FontMetrics metrics = g2d.getFontMetrics();
	float x, y;

	bounds = metrics.getStringBounds(label, g2d).getBounds();

	switch(justification){
	case LEFT:
	    x = (float)location.getX();
	    y = (float)location.getY();
	    bounds.setRect(x, y, bounds.width, bounds.height);
	    break;
	case RIGHT:
	    x = (float)(location.getX() - bounds.getWidth());
	    y = (float)location.getY();
	    bounds.setRect(x, y, bounds.width, bounds.height);
	    break;
	case CENTER:
	    x = (float)(location.getX() - (bounds.getWidth()/2));
	    y = (float)(location.getY());
	    bounds.setRect(x, y, bounds.width, bounds.height);
	    break;
	default:
	    x = (float)(location.getX() - (bounds.getWidth()/2));
	    y = (float)(location.getY());
	    bounds.setRect(x, y, bounds.width, bounds.height);
	}
	bounds.setRect(bounds.x, bounds.y, 
		       bounds.width, bounds.height);
    }

    public Rectangle getBounds()
    {
	return bounds;
    }

    public Color getFillColor()
    {
	return fillColor;
    }

    public int getFontSize()
    {
	return fontSize;
    }

    public String getFullyQualifiedName()
    {
	return parent.getFullyQualifiedName()+"."+name;
    }

    public Point getLocation()
    {
	return location;
    }

    public String getName()
    {
	return name;
    }

    public Color getSelectedFillColor()
    {
	return selectedFillColor;
    }

    public String getText()
    {
	return label;
    }


    public void paint(Graphics2D g2d){
	g2d.setFont(font);
	g2d.setPaint(parent.isSelected() ? 
		     selectedFillColor : fillColor);

	FontMetrics metrics = g2d.getFontMetrics();
	float x, y;

	switch(justification){
	case LEFT:
	    x = (float)location.getX();
	    y = (float)location.getY();
	    break;
	case RIGHT:
	    x = (float)(location.getX() - bounds.getWidth());
	    y = (float)location.getY();
	    break;
	case CENTER:
	    x = (float)(location.getX() - (bounds.getWidth()/2));
	    y = (float)(location.getY());
	    break;
	default:
	    x = (float)(location.getX() - (bounds.getWidth()/2));
	    y = (float)(location.getY());
	}
	g2d.drawString(label, x, y + metrics.getAscent());
    }


    public void setFillColor(Color color)
    {
	this.fillColor = color;
    }

    public void setSelectedFillColor(Color color)
    {
	this.fillColor = color;
    }

    public void setFontSize(int fontSize)
    {
	this.fontSize = fontSize;
	font = canvas.getGraphics().getFont().deriveFont(Font.BOLD, fontSize);

	calculateBounds();
    }

    public void setJustification(int justification)
    {
	this.justification = justification;
	calculateBounds();
    }

    public void setLocation(int x, int y)
    {
	bounds.translate(x - location.x, y - location.y);
	this.location.setLocation(x, y);
    }

    public void setText(String text)
    {
	label = text;
	calculateBounds();
    }

    public void translate(int dx, int dy)
    {
	location.setLocation(location.x+dx, location.y+dy);
	bounds.translate(dx, dy);
    }
}
