package Liberty.visualizer.canvas;

import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;

import java.awt.*;
import java.util.Collection;

public interface MutableSchematicCanvasContainer 
    extends SchematicCanvasContainer{

    public void addDrawable(Drawable drawable);

    public void addFigure(SchematicFigure figure);

    public void clear();

    public Collection getSubFigures();

    public Collection getDrawables();

}
