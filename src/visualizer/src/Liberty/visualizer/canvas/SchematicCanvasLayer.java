package Liberty.visualizer.canvas;

import Liberty.visualizer.canvas.drawable.*;
import Liberty.visualizer.canvas.figure.*;

import java.awt.*;
import java.util.*;

public class SchematicCanvasLayer implements SchematicCanvasContainer{
    /* the id of this layer */
    private String layerName;
    /* a z-list of the figures contained in this layer */
    private Vector figures;
    /* the drawables contained in this layer */
    private HashMap drawables;
    /* is this layer visible on the canvas */
    private boolean isVisible;
    /* this layer's parent container */
    private SchematicCanvas parent;

    public SchematicCanvasLayer(SchematicCanvas parent, 
				String layerName)
    {
	this.parent = parent;
	this.layerName = layerName;
	this.figures = new Vector();
	this.drawables = new HashMap();
	this.isVisible = true;
    }

    public void addDrawable(Drawable drawable)
    {
	drawables.put(drawable.getName(), drawable);
    }

    public void addFigure(SchematicFigure figure)
    {
	figures.add(figure);
    }

    /**
     * Function used by a child of this figure to signal that it's bounds have
     * changed and for this figure to take appropriate action.
     *
     * @param figure the child figure whose bounds have changed
     */
    public void childBoundsChanged(SchematicFigure figure, Rectangle oldBounds)
    {
    }

    public void clear()
    {
	drawables.clear();
	figures.clear();
    }

    public Rectangle getBounds()
    {
	Rectangle bounds = parent.getBounds();
	float scaleFactor = 1/parent.getScaleFactor();

	if(scaleFactor != 1.0f){
	    bounds.setRect(bounds.x * scaleFactor,
			   bounds.y * scaleFactor,
			   bounds.width * scaleFactor,
			   bounds.height * scaleFactor);
	}

	return bounds;
    }

    public Collection getSubFigures()
    {
	return figures;
    }

    public Collection getDrawables()
    {
	return drawables.values();
    }

    public SchematicFigure getFigureAt(int x, int y)
    {
	for(int index = figures.size()-1; index >= 0; index--){
	    SchematicFigure figure = (SchematicFigure)figures.
		elementAt(index);
	    SchematicFigure subFigure;
	    if((subFigure = figure.getFigureAt(x, y)) != null){
		return subFigure;
	    }
	    else if(figure.contains(x, y) && figure.isVisible()){
		return figure;
	    }
	}
	return null;
    }

    public SchematicFigure getFigureByName(String name)
    {
	Iterator figureIter = figures.iterator();

	while(figureIter.hasNext()){
	    SchematicFigure figure = (SchematicFigure)figureIter.next();
	    SchematicFigure subFigure;

	    if(figure.getFullyQualifiedName().equals(name))
		return figure;
	    else if((subFigure = figure.getFigureByName(name)) != null)
		return subFigure;
	}
	return null;
    }

    public SchematicFigure getFigureByUserObject(Object o)
    {
	Iterator figureIter = figures.iterator();

	while(figureIter.hasNext()){
	    SchematicFigure figure = (SchematicFigure)figureIter.next();
	    SchematicFigure subFigure;

	    if(figure.getUserObject() == o)
		return figure;
	    else if((subFigure = figure.getFigureByUserObject(o)) != null)
		return subFigure;
	}
	return null;
    }

    public Vector getFigures()
    {
	return figures;
    }

    public String getName()
    {
	return layerName;
    }

    public Rectangle getPaintBounds()
    {
	Rectangle bounds = null;
	Iterator drawableIter = drawables.values().iterator();
	Iterator figureIter = figures.iterator();

	while(drawableIter.hasNext()){
	    Drawable drawable = (Drawable)drawableIter.next();
	    Rectangle dbounds = drawable.getBounds();

	    if(dbounds != null){
		if(bounds == null)
		    bounds = dbounds;
		else
		    bounds = bounds.createUnion(dbounds).getBounds();
	    }
	}
	while(figureIter.hasNext()){
	    SchematicFigure figure = (SchematicFigure)figureIter.next();
	    Rectangle fBounds = figure.getPaintBounds();
	    
	    if(fBounds != null){
		if(bounds == null)
		    bounds = fBounds;
		else
		    bounds = bounds.createUnion(fBounds).getBounds();
	    }
	}
	bounds.setRect(bounds.x - 4, bounds.y -4, 
		       bounds.width +8, bounds.height + 8);
	return bounds;
    }


    public SchematicCanvasContainer getParent()
    {
	return null;
    }

    /**
     * Set the parent of this figure to be the SchematicCanvasContainer defined
     * by the parameter parent.
     *
     * @param parent the new SchematicCanvasContainer that owns this figure
     */
    public void setParent(SchematicCanvasContainer parent)
    {
	// FIXME:
	//this.parent = parent;
    }

    public boolean isVisible()
    {
	return isVisible;
    }

    public void paint(Graphics2D g2d)
    {
	if(isVisible){
	    Iterator drawableIter = drawables.values().iterator();
	    while(drawableIter.hasNext()){
		((Drawable)drawableIter.next()).paint(g2d);
	    }

	    Iterator figureIter = figures.iterator();
	    while(figureIter.hasNext()){
		SchematicFigure figure = (SchematicFigure)figureIter.next();
		figure.paint(g2d);
	    }
	}
    }

    public void paint(Graphics2D g2d, Rectangle clip)
    {
	if(isVisible){
	    Iterator drawableIter = drawables.values().iterator();
	    while(drawableIter.hasNext()){
		((Drawable)drawableIter.next()).paint(g2d);
	    }

	    Iterator figureIter = figures.iterator();
	    while(figureIter.hasNext()){
		SchematicFigure figure = (SchematicFigure)figureIter.next();
		Rectangle repaintRegion = figure.getPaintBounds();

		if(clip.intersects(figure.getPaintBounds())){
		    figure.paint(g2d);
		}
	    }
	}
    }

    public void removeFigure(SchematicFigure figure)
    {
	System.out.println("Removing: "+figure+" "+figures.remove(figure));
    }

    public void repaint()
    {
	parent.repaint(getPaintBounds());
    }

    public void setVisible(boolean b)
    {
	isVisible = b;
    }

    public void validateChildLocation(SchematicFigure child, Point eventPoint)
    {
	Rectangle bounds = parent.getBounds();
	float scaleFactor = 1/parent.getScaleFactor();

	if(scaleFactor != 1.0f){
	    bounds.setRect(bounds.x * scaleFactor,
			   bounds.y * scaleFactor,
			   bounds.width * scaleFactor,
			   bounds.height * scaleFactor);
	}


	// validate that this isn't being placed off of the canvas
	Rectangle childBounds = child.getBounds();
	int x = childBounds.x, y = childBounds.y;

	if(x < bounds.x)
	    x = bounds.x;
	else if(x > bounds.width - childBounds.width)
	    x = bounds.width - childBounds.width;
	if(y < bounds.y)
	    y = bounds.y;
	else if(y > bounds.height - childBounds.height)
	    y = bounds.height - childBounds.height;

	if(x != childBounds.x || y != childBounds.y)
	    child.setLocation(x, y);
    }
}
