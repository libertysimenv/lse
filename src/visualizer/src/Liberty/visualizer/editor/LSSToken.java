package Liberty.visualizer.editor;


public class LSSToken extends Token{ 
    private int ID;
    private String contents;
    private int lineNumber;
    private int charBegin;
    private int charEnd;

    /* misc tokens that shouldn't be colored */
    public static final int INT_LITERAL = 0x000;
    public static final int FLOAT_LITERAL = 0x001;

    /* literals */
    public static final int STRING_LITERAL = 0x102;
    public static final int CHAR_LITERAL = 0x103;
    public static final int CODE_LITERAL = 0x104;

    /* reserve words */
    public static final int IF = 0x200;
    public static final int ELSE = 0x201;
    public static final int BREAK = 0x202;
    public static final int FOR = 0x203;
    public static final int RETURN = 0x204;
    public static final int ENUM = 0x205;
    public static final int STRUCT = 0x206;
    public static final int UNION = 0x207;
    public static final int CONST = 0x208;
    public static final int IMPORT = 0x209;
    public static final int VAR = 0x20a;
    public static final int TYPEDEF = 0x20b;
    public static final int FUN = 0x20c;
    public static final int INCLUDE = 0x20d;
    public static final int TRUE = 0x20e;
    public static final int FALSE = 0x20f;
    public static final int PACKAGE = 0x210;
    public static final int SUBPACKAGE = 0x2111;
    public static final int USING = 0x212;
    public static final int REF = 0x213;
    public static final int TYPE = 0x214;
    public static final int RUNTIMEABLE = 0x215;
    public static final int RUNTIME_PARM = 0x216;
    public static final int NEW = 0x217;
    public static final int LOCAL = 0x218;
    public static final int INTERNAL = 0x219;
    public static final int EXPORT = 0x21a;
    public static final int AS = 0x21b;
    public static final int ON = 0x21c;
    public static final int INITIALIZED = 0x21d;
    public static final int EMITS = 0x21e;
    public static final int LOCKED = 0x21f;

    /* data types */
    public static final int INT = 0x300;
    public static final int CHAR = 0x301;
    public static final int FLOAT = 0x302;
    public static final int DOUBLE = 0x303;
    public static final int LITERAL = 0x304;
    public static final int STRING = 0x305;
    public static final int BOOL = 0x306;
    public static final int VOID = 0x307;
    public static final int NONE = 0x308;
    public static final int TYPEVAR = 0x312;

    /* lss data types */
    public static final int DECODEPOINT = 0x409;
    public static final int USERPOINT = 0x40a;
    public static final int MODULE = 0x40b;
    public static final int INSTANCE = 0x40c;
    public static final int PARAMETER = 0x40d;
    public static final int INPORT = 0x40e;
    public static final int OUTPORT = 0x40f;
    public static final int EVENT = 0x410;
    public static final int QUERY = 0x411;
    public static final int METHOD = 0x412;
    public static final int DOMAIN = 0x413;
    public static final int RUNTIME_VAR = 0x414;
    public static final int COLLECTOR = 0x415;

    /* operators */
    public static final int RETURNS = 0x500;
    public static final int CONNECT = 0x501;
    public static final int ASSIGN = 0x502;
    public static final int PLUS = 0x503;
    public static final int MINUS = 0x504;
    public static final int TIMES = 0x505;
    public static final int DIVIDE = 0x506;
    public static final int MOD = 0x507;
    public static final int PLUS_ASSIGN = 0x508;
    public static final int MINUS_ASSIGN = 0x509;
    public static final int TIMES_ASSIGN = 0x50a;
    public static final int DIVIDE_ASSIGN = 0x50b;
    public static final int MOD_ASSIGN = 0x50c;
    public static final int INCREMENT = 0x50d;
    public static final int DECREMENT = 0x50e;
    public static final int EXCLAMATION = 0x50f;
    public static final int LOGICAL_AND = 0x510;
    public static final int LOGICAL_OR = 0x511;
    public static final int LESS_THAN = 0x512;
    public static final int GREATER_THAN = 0x513;
    public static final int LESS_THAN_EQUAL = 0x514;
    public static final int GREATER_THAN_EQUAL = 0x515;
    public static final int EQUAL = 0x516;
    public static final int NOT_EQUAL = 0x517;
    public static final int SEMICOLON = 0x518;
    public static final int COMMA = 0x519;
    public static final int QUESTION = 0x51a;
    public static final int COLON = 0x51b;
    public static final int DOT = 0x51c;
    public static final int PIPE = 0x51d;
    public static final int LBRACE = 0x51e;
    public static final int RBRACE = 0x51f;
    public static final int LPAREN = 0x520;
    public static final int RPAREN = 0x521;
    public static final int LBRACK = 0x522;
    public static final int RBRACK = 0x523;
    public static final int QUOTE = 0x524;
    public static final int TICK = 0x525;

    /* other */
    public static final int IDENT = 0x600;

    /* error */
    public final static int ERROR_IDENTIFIER = 0x700;
    public final static int ERROR_UNCLOSED_STRING = 0x701;
    public final static int ERROR_MALFORMED_STRING = 0x702;
    public final static int ERROR_MALFORMED_UNCLOSED_STRING = 0x703;
    public final static int ERROR_UNCLOSED_CHARACTER = 0x704;
    public final static int ERROR_MALFORMED_CHARACTER = 0x705;
    public final static int ERROR_MALFORMED_UNCLOSED_CHARACTER = 0x706;
    public final static int ERROR_INTEGER_DECIMIAL_SIZE = 0x707;
    public final static int ERROR_INTEGER_OCTAL_SIZE = 0x708;
    public final static int ERROR_INTEGER_HEXIDECIMAL_SIZE = 0x709;
    public final static int ERROR_LONG_DECIMIAL_SIZE = 0x70a;
    public final static int ERROR_LONG_OCTAL_SIZE = 0x70b;
    public final static int ERROR_LONG_HEXIDECIMAL_SIZE = 0x70c;
    public final static int ERROR_FLOAT_SIZE = 0x70d;
    public final static int ERROR_DOUBLE_SIZE = 0x70e;
    public final static int ERROR_FLOAT = 0x70f;
    public final static int ERROR_UNCLOSED_COMMENT = 0x710;

    /* comment */
    public static final int COMMENT = 0x800;

    public LSSToken(int ID, String contents, int lineNumber, 
		    int charBegin, int charEnd){
        this.ID = ID;
        this.contents = new String(contents);
        this.lineNumber = lineNumber;
        this.charBegin = charBegin;
        this.charEnd = charEnd;
    }

    public int getID(){
  	return ID;
    }

    public String getContents(){
  	return (new String(contents));
    }

    public int getLineNumber(){
  	return lineNumber;
    }

    public int getCharBegin(){
  	return charBegin;
    }
    
    public int getCharEnd(){
 	return charEnd;
    }

    public boolean isLiteral(){
  	return((ID >> 8) == 0x1);
    }

    public boolean isReservedWord(){
  	return((ID >> 8) == 0x2);
    }

    public boolean isDatatype(){
	return((ID >> 8) == 0x3 || (ID >> 8) == 0x4);
    }
    
    public boolean isOperator(){
  	return((ID >> 8) == 0x5);
    }
    
    public boolean isIdentifier(){
  	return((ID >> 8) == 0x6);
    }
    
    public boolean isError(){
  	return((ID >> 8) == 0x7);
    }

    public boolean isSeparator(){
  	return((ID >> 8) == 0x8);
    }

    public boolean isPreProcessor(){
  	return false;
    }

    public boolean isComment(){
  	return((ID >> 8) == 0x8);
    }

    public boolean isWhiteSpace(){
  	return((ID >> 8) == 0x8);
    }

    public String getDescription(){
	if (isReservedWord()){
	    return("reservedWord");
	} else if (isIdentifier()){
	    return("identifier");
	} else if (isLiteral()){
	    return("literal");
	} else if (isSeparator()){
	    return("separator");
	} else if (isOperator()){
	    return("operator");
	} else if (isComment()){
	    return("comment");
	} else if (isPreProcessor()){
	    return("preprocessor");
	} else if (isWhiteSpace()){
	    return("whitespace");
	} else if (isError()){
	    return("error");
	} else if (isDatatype()){
	    return("datatype");
	} else {
	    return("unknown");
	}
    }

    public String errorString(){
  	String s;
  	if (isError()){
	    s = "Error on line " + lineNumber + ": ";
	    switch (ID){
	    case ERROR_IDENTIFIER:
		s += "Unrecognized Identifier: " + contents;
  		break;
	    case ERROR_UNCLOSED_STRING:
		s += "'\"' expected after " + contents;
  		break;
	    case ERROR_MALFORMED_STRING:
	    case ERROR_MALFORMED_UNCLOSED_STRING:
		s += "Illegal character in " + contents;
  		break;
	    case ERROR_UNCLOSED_CHARACTER:
		s += "\"'\" expected after " + contents;
  		break;
	    case ERROR_MALFORMED_CHARACTER:
	    case ERROR_MALFORMED_UNCLOSED_CHARACTER:
		s += "Illegal character in " + contents;
  		break;
	    case ERROR_INTEGER_DECIMIAL_SIZE:
	    case ERROR_INTEGER_OCTAL_SIZE:
	    case ERROR_FLOAT:
		s += "Illegal character in " + contents;
  		break;
	    case ERROR_INTEGER_HEXIDECIMAL_SIZE:
	    case ERROR_LONG_DECIMIAL_SIZE:
	    case ERROR_LONG_OCTAL_SIZE:
	    case ERROR_LONG_HEXIDECIMAL_SIZE:
	    case ERROR_FLOAT_SIZE:
	    case ERROR_DOUBLE_SIZE:
		s += "Literal out of bounds: " + contents;
  		break;
	    case ERROR_UNCLOSED_COMMENT:
		s += "*/ expected after " + contents;
  		break;
	    }
	    
  	} else {
	    s = null;
  	}
  	return (s);
    }

    public String toString() {
	return ("Token #" + Integer.toHexString(ID) + ": " + 
		getDescription() + " Line " +
		lineNumber + " from " +charBegin + " to " + 
		charEnd + " : " + contents);
    }

    public int getState() {
	return INITIAL_STATE;
    }
}
