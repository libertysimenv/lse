package Liberty.visualizer.editor;

import java.awt.event.*;
import java.awt.Rectangle;
import javax.swing.text.*;
import javax.swing.*;

public class ShortcutManager{

    private int mode;
    private int SM_WINDOWS_MODE = 0;
    private int SM_EMACS_MODE = 1;
    private int current_state;
    private JTextPane textPane;
    private EditorKit editorKit;
    private StyledDocument styledDocument;

    public static final int Ctrl_A = 0;  // emacs ? beginning of line : select all
    public static final int Ctrl_B = 1;  // emacs ? back one char
    public static final int Ctrl_C = 2;  // emacs ? ctrl_x_pressed -> exit : cut 
    public static final int Ctrl_D = 3;  // emacs ? delete current char
    public static final int Ctrl_E = 4;  // emacs ? end of line
    public static final int Ctrl_F = 5;  // emacs ? forward one char
    public static final int Ctrl_G = 6;  // emacs ? cancel
    public static final int Ctrl_H = 7;  // help
    public static final int Ctrl_N = 8;  // emacs ? down one line
    public static final int Ctrl_O = 9;  // emacs ? open a new line : open a file
    public static final int Ctrl_P = 10; // emacs ? goto the previous line
    public static final int Ctrl_R = 11;
    public static final int Ctrl_S = 12;
    public static final int Ctrl_V = 13;
    public static final int Ctrl_Y = 14;
    public static final int Ctrl_W = 15;
    public static final int Ctrl__ = 16;
    public static final int Esc    = 17;
    public static final int V_Key  = 18;

    public ShortcutManager(JTextPane textPane){
	this.textPane = textPane;
	this.editorKit = textPane.getEditorKit();
	this.styledDocument = textPane.getStyledDocument();
    }

    public void executeKeyEvent(int event){
	switch(event){
	case Ctrl_A:
	    int pos = textPane.getCaretPosition();
	    int line = 1;
	    int col = 1;

	    try{
		Rectangle current = textPane.modelToView(pos);
		if(current == null)
		    return;
		Rectangle one = textPane.modelToView(0);
		Rectangle two = textPane.modelToView(1);
		int width = two.x - one.x;
		if(width == 0)
		    return;
		line = ((current.y - one.y)/current.height)+1;
		col = ((current.x - one.x)/width)+1;
		System.out.println("row: "+line+" col: "+col);
		textPane.setCaretPosition(textPane.getCaretPosition()-(col-1));
	    }
	    catch(BadLocationException e){
		return;
	    }     
	    break;
	case Ctrl_W:
	    textPane.cut();
	    break;
	case Ctrl_Y:
	    textPane.paste();
	    break;
	}
    }
}
