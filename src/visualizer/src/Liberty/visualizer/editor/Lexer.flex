package Liberty.visualizer.editor;
import java.io.*;

%%


%{

    private int lastToken;
    private int nextState = YYINITIAL;

    
    /* method to reset the lexer */
    public void reset(java.io.Reader reader, int yyline, 
		      int yychar, int yycolumn) throws IOException
    {
	yyreset(reader);
	this.yyline = yyline;
	this.yychar = yychar;
	this.yycolumn = yycolumn;
    }

%}

%line
%column
%char

%state COMMENT
%state STRING_LITERAL

%public
%class Lexer
%type Token
%function getNextToken

CDECINT         =    0 | [1-9][0-9]*
CBININT         =    0[bB][01]+
COCTINT         =    0[0-7]+
CHEXINT         =    0[xX][0-9a-fA-F]+
CINT            =    {CDECINT}|{COCTINT}|{CHEXINT}
CFLOAT          =    (([0-9]+("."[0-9]*)?)|("."[0-9]+))([eE][+-]?[0-9]+)?
CSTRING         =    \"[^\"]*\"
CCHAR           =    '[!-~]' | '\\' | '\n' | '\t' | '\r'
IDENTIFIER      =    [A-Za-z][_A-Za-z0-9]*



%%

<YYINITIAL> {

/* Constants */

{CBININT}     {  String text = yytext();
                 return new LSSToken(LSSToken.INT_LITERAL, text, yyline, 
				    yychar, yychar+text.length());}
{CINT}        {  String text = yytext();
                 return new LSSToken(LSSToken.INT_LITERAL, text, yyline, 
				    yychar, yychar+text.length());}
{CFLOAT}      {  String text = yytext();
                 return new LSSToken(LSSToken.FLOAT_LITERAL, text, yyline, 
				    yychar, yychar+text.length());}
{CSTRING}     {  String text = yytext();
                 return new LSSToken(LSSToken.STRING_LITERAL, text, yyline, 
				    yychar, yychar+text.length());}
{CCHAR}       {  String text = yytext();
                 return new LSSToken(LSSToken.CHAR_LITERAL, text, yyline, 
				    yychar, yychar+text.length());}
"<<<" ~">>>"  {  String text = yytext();
                 return new LSSToken(LSSToken.CODE_LITERAL, text, yyline, 
				    yychar, yychar+text.length());}

"package"          { String text = yytext();
                return new LSSToken(LSSToken.PACKAGE, text, yyline, 
				    yychar, yychar+text.length());}
"subpackage"          { String text = yytext();
                return new LSSToken(LSSToken.SUBPACKAGE, text, yyline, 
				    yychar, yychar+text.length());}
"using"          { String text = yytext();
                return new LSSToken(LSSToken.USING, text, yyline, 
				    yychar, yychar+text.length());}

  /* reserve words */
"if"          { String text = yytext();
                return new LSSToken(LSSToken.IF, text, yyline, 
				    yychar, yychar+text.length());}
"else"        { String text = yytext();
                return new LSSToken(LSSToken.ELSE, text, yyline, 
				    yychar, yychar+text.length());}
"break"       { String text = yytext();
                return new LSSToken(LSSToken.BREAK, text, yyline, 
				    yychar, yychar+text.length());}
"include"     { String text = yytext();
                return new LSSToken(LSSToken.INCLUDE, text, yyline, 
				    yychar, yychar+text.length());}
"for"	      { String text = yytext();
                return new LSSToken(LSSToken.FOR, text, yyline, 
				    yychar, yychar+text.length());}
"return"      { String text = yytext();
                return new LSSToken(LSSToken.RETURN, text, yyline, 
				    yychar, yychar+text.length());}
"const"       { String text = yytext();
                return new LSSToken(LSSToken.CONST, text, yyline, 
				    yychar, yychar+text.length());}
"struct"      { String text = yytext();
                return new LSSToken(LSSToken.STRUCT, text, yyline, 
				    yychar, yychar+text.length());}
"union"       { String text = yytext();
                return new LSSToken(LSSToken.UNION, text, yyline, 
				    yychar, yychar+text.length());}
"enum"        { String text = yytext();
                return new LSSToken(LSSToken.ENUM, text, yyline, 
				    yychar, yychar+text.length());}
"import"      { String text = yytext();
                return new LSSToken(LSSToken.IMPORT, text, yyline, 
				    yychar, yychar+text.length());}
"var"         { String text = yytext();
                return new LSSToken(LSSToken.VAR, text, yyline, 
				    yychar, yychar+text.length());}
"typedef"     { String text = yytext();
                return new LSSToken(LSSToken.TYPEDEF, text, yyline, 
				    yychar, yychar+text.length());}
"fun"         { String text = yytext();
                return new LSSToken(LSSToken.FUN, text, yyline, 
				    yychar, yychar+text.length());}
"false"       { String text = yytext();
                return new LSSToken(LSSToken.FALSE, text, yyline, 
				    yychar, yychar+text.length());}
"true"        { String text = yytext();
                return new LSSToken(LSSToken.TRUE, text, yyline, 
				    yychar, yychar+text.length());}

"ref"        { String text = yytext();
                return new LSSToken(LSSToken.REF, text, yyline, 
				    yychar, yychar+text.length());}

"type"        { String text = yytext();
                return new LSSToken(LSSToken.TYPE, text, yyline, 
				    yychar, yychar+text.length());}

"runtimeable"        { String text = yytext();
                return new LSSToken(LSSToken.RUNTIMEABLE, text, yyline, 
				    yychar, yychar+text.length());}

"runtime_parm"        { String text = yytext();
                return new LSSToken(LSSToken.RUNTIME_PARM, text, yyline, 
				    yychar, yychar+text.length());}

"new"        { String text = yytext();
                return new LSSToken(LSSToken.NEW, text, yyline, 
				    yychar, yychar+text.length());}

"local"        { String text = yytext();
                return new LSSToken(LSSToken.LOCAL, text, yyline, 
				    yychar, yychar+text.length());}

"internal"        { String text = yytext();
                return new LSSToken(LSSToken.INTERNAL, text, yyline, 
				    yychar, yychar+text.length());}

"export"        { String text = yytext();
                return new LSSToken(LSSToken.EXPORT, text, yyline, 
				    yychar, yychar+text.length());}

"as"        { String text = yytext();
                return new LSSToken(LSSToken.AS, text, yyline, 
				    yychar, yychar+text.length());}

"on"        { String text = yytext();
                return new LSSToken(LSSToken.ON, text, yyline, 
				    yychar, yychar+text.length());}

"initialized"        { String text = yytext();
                return new LSSToken(LSSToken.INITIALIZED, text, yyline, 
				    yychar, yychar+text.length());}

"emits"        { String text = yytext();
                return new LSSToken(LSSToken.EMITS, text, yyline, 
				    yychar, yychar+text.length());}

"locked"        { String text = yytext();
                return new LSSToken(LSSToken.LOCKED, text, yyline, 
				    yychar, yychar+text.length());}


/* Data Types */
"int"	      { String text = yytext();
                return new LSSToken(LSSToken.INT, text, yyline, 
				    yychar, yychar+text.length());}
"char"        { String text = yytext();
                return new LSSToken(LSSToken.CHAR, text, yyline, 
				    yychar, yychar+text.length());}
"float"       { String text = yytext();
                return new LSSToken(LSSToken.FLOAT, text, yyline, 
				    yychar, yychar+text.length());}
"double"      { String text = yytext();
                return new LSSToken(LSSToken.DOUBLE, text, yyline, 
				    yychar, yychar+text.length());}
"literal"     { String text = yytext();
                return new LSSToken(LSSToken.LITERAL, text, yyline, 
				    yychar, yychar+text.length());}
"string"      { String text = yytext();
                return new LSSToken(LSSToken.STRING, text, yyline, 
				    yychar, yychar+text.length());}
"bool"        { String text = yytext();
                return new LSSToken(LSSToken.BOOL, text, yyline, 
				    yychar, yychar+text.length());}
"void"        { String text = yytext();
                return new LSSToken(LSSToken.VOID, text, yyline, 
				    yychar, yychar+text.length());}
"none"        { String text = yytext();
                return new LSSToken(LSSToken.NONE, text, yyline, 
				    yychar, yychar+text.length());}
"decodepoint" { String text = yytext();
                return new LSSToken(LSSToken.DECODEPOINT, text, yyline, 
				    yychar, yychar+text.length());}
"userpoint"   { String text = yytext();
                return new LSSToken(LSSToken.USERPOINT, text, yyline, 
				    yychar, yychar+text.length());}


/* Other */

"module"      { String text = yytext();
                return new LSSToken(LSSToken.MODULE, text, yyline, 
				    yychar, yychar+text.length());}
"instance"    { String text = yytext();
                return new LSSToken(LSSToken.INSTANCE, text, yyline, 
				    yychar, yychar+text.length());}
"parameter"   { String text = yytext();
                return new LSSToken(LSSToken.PARAMETER, text, yyline, 
				    yychar, yychar+text.length());}
"inport"      { String text = yytext();
                return new LSSToken(LSSToken.INPORT, text, yyline, 
				    yychar, yychar+text.length());}
"outport"     { String text = yytext();
                return new LSSToken(LSSToken.OUTPORT, text, yyline, 
				    yychar, yychar+text.length());}
"event"       { String text = yytext();
                return new LSSToken(LSSToken.EVENT, text, yyline, 
				    yychar, yychar+text.length());}
"query"       { String text = yytext();
                return new LSSToken(LSSToken.QUERY, text, yyline, 
				    yychar, yychar+text.length());}
"method"        { String text = yytext();
                return new LSSToken(LSSToken.METHOD, text, yyline, 
				    yychar, yychar+text.length());}

"domain"        { String text = yytext();
                return new LSSToken(LSSToken.DOMAIN, text, yyline, 
				    yychar, yychar+text.length());}

"runtime_var"        { String text = yytext();
                return new LSSToken(LSSToken.RUNTIME_VAR, text, yyline, 
				    yychar, yychar+text.length());}

"collector"        { String text = yytext();
                return new LSSToken(LSSToken.COLLECTOR, text, yyline, 
				    yychar, yychar+text.length());}

/* Operators */
"=>"          { String text = yytext();
                return new LSSToken(LSSToken.RETURNS, text, yyline, 
				    yychar, yychar+text.length());}
"->"          { String text = yytext();
                return new LSSToken(LSSToken.CONNECT, text, yyline, 
				    yychar, yychar+text.length());}
"+"           { String text = yytext();
                return new LSSToken(LSSToken.PLUS, text, yyline, 
				    yychar, yychar+text.length());}
"-"           { String text = yytext();
                return new LSSToken(LSSToken.MINUS, text, yyline, 
				    yychar, yychar+text.length());}
"*"           { String text = yytext();
                return new LSSToken(LSSToken.TIMES, text, yyline, 
				    yychar, yychar+text.length());}
"/"           { String text = yytext();
                return new LSSToken(LSSToken.DIVIDE, text, yyline, 
				    yychar, yychar+text.length());}
"%"           { String text = yytext();
                return new LSSToken(LSSToken.MOD, text, yyline, 
				    yychar, yychar+text.length());}
"="           { String text = yytext();
                return new LSSToken(LSSToken.ASSIGN, text, yyline, 
				    yychar, yychar+text.length());}
"+="          { String text = yytext();
                return new LSSToken(LSSToken.PLUS_ASSIGN, text, yyline, 
				    yychar, yychar+text.length());}
"-="          { String text = yytext();
                return new LSSToken(LSSToken.MINUS_ASSIGN, text, yyline, 
				    yychar, yychar+text.length());}
"*="          { String text = yytext();
                return new LSSToken(LSSToken.TIMES_ASSIGN, text, yyline, 
				    yychar, yychar+text.length());}
"/="          { String text = yytext();
                return new LSSToken(LSSToken.DIVIDE_ASSIGN, text, yyline, 
				    yychar, yychar+text.length());}
"%="          { String text = yytext();
                return new LSSToken(LSSToken.MOD_ASSIGN, text, yyline, 
				    yychar, yychar+text.length());}
"++"          { String text = yytext();
                return new LSSToken(LSSToken.INCREMENT, text, yyline, 
				    yychar, yychar+text.length());}
"--"          { String text = yytext();
                return new LSSToken(LSSToken.DECREMENT, text, yyline, 
				    yychar, yychar+text.length());}
"!"           { String text = yytext();
                return new LSSToken(LSSToken.EXCLAMATION, text, yyline, 
				    yychar, yychar+text.length());}
"&&"          { String text = yytext();
                return new LSSToken(LSSToken.LOGICAL_AND, text, yyline, 
				    yychar, yychar+text.length());}
"||"          { String text = yytext();
                return new LSSToken(LSSToken.LOGICAL_OR, text, yyline, 
				    yychar, yychar+text.length());}
"<"           { String text = yytext();
                return new LSSToken(LSSToken.LESS_THAN, text, yyline, 
				    yychar, yychar+text.length());}
">"           { String text = yytext();
                return new LSSToken(LSSToken.GREATER_THAN, text, yyline, 
				    yychar, yychar+text.length());}
"<="          { String text = yytext();
                return new LSSToken(LSSToken.LESS_THAN_EQUAL, text, yyline, 
				    yychar, yychar+text.length());}
">="          { String text = yytext();
                return new LSSToken(LSSToken.GREATER_THAN_EQUAL, text, yyline, 
				    yychar, yychar+text.length());}
"=="          { String text = yytext();
                return new LSSToken(LSSToken.EQUAL, text, yyline, 
				    yychar, yychar+text.length());}
"!="          { String text = yytext();
                return new LSSToken(LSSToken.NOT_EQUAL, text, yyline, 
				    yychar, yychar+text.length());}
";"           { String text = yytext();
                return new LSSToken(LSSToken.SEMICOLON, text, yyline, 
				    yychar, yychar+text.length());}
","           { String text = yytext();
                return new LSSToken(LSSToken.COMMA, text, yyline, 
				    yychar, yychar+text.length());}
"?"           { String text = yytext();
                return new LSSToken(LSSToken.QUESTION, text, yyline, 
				    yychar, yychar+text.length());}
":"           { String text = yytext();
                return new LSSToken(LSSToken.COLON, text, yyline, 
				    yychar, yychar+text.length());}
"."           { String text = yytext();
                return new LSSToken(LSSToken.DOT, text, yyline, 
				    yychar, yychar+text.length());}
"|"           { String text = yytext();
                return new LSSToken(LSSToken.PIPE, text, yyline, 
				    yychar, yychar+text.length());}
"{"           { String text = yytext();
                return new LSSToken(LSSToken.LBRACE, text, yyline, 
				    yychar, yychar+text.length());}
"}"           { String text = yytext();
                return new LSSToken(LSSToken.RBRACE, text, yyline, 
				    yychar, yychar+text.length());}
"("           { String text = yytext();
                return new LSSToken(LSSToken.LPAREN, text, yyline, 
				    yychar, yychar+text.length());}
")"           { String text = yytext();
                return new LSSToken(LSSToken.RPAREN, text, yyline, 
				    yychar, yychar+text.length());}
"["           { String text = yytext();
                return new LSSToken(LSSToken.LBRACK, text, yyline, 
				    yychar, yychar+text.length());}
"]"           { String text = yytext();
                return new LSSToken(LSSToken.RBRACK, text, yyline, 
				    yychar, yychar+text.length());}
"\""           { String text = yytext();
                 yybegin(STRING_LITERAL);
                 return new LSSToken(LSSToken.STRING_LITERAL, text, yyline, 
				    yychar, yychar+text.length());
                 }
"\'"           { String text = yytext();
                 return new LSSToken(LSSToken.CHAR_LITERAL, text, yyline, 
				    yychar, yychar+text.length());
                 }


{IDENTIFIER}  { String text = yytext();
                return new LSSToken(LSSToken.IDENT, text, yyline, 
				    yychar, yychar+text.length());}

  /* Whitespace & comments */  
  [ \t\r\n]    { /*pws.append(yytext());*/ }
  "/*" ~"*/"   { String text = yytext();
                 return new LSSToken(LSSToken.COMMENT, text, yyline, 
				    yychar, yychar+text.length());}
}  

<STRING_LITERAL> {
  "\""         { String text = yytext();  
                 yybegin(YYINITIAL);
                 return new LSSToken(LSSToken.STRING_LITERAL, text, yyline, 
				    yychar, yychar+text.length());
                 }

  [^\n\t]*     { String text = yytext();
                 return new LSSToken(LSSToken.STRING_LITERAL, text, yyline, 
				    yychar, yychar+text.length());
                 }
}
