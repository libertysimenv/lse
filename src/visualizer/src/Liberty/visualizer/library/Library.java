package Liberty.visualizer.library;

import java.util.*;
import java.net.URL;
import java.io.*;

/**
 * A class for managing the LSE module library
 *
 * @author Jason Blome
 * @version 0.1
 */

public class Library{
    /* A vector containing the directories loaded into the library */
    private Vector virtualDirectories;
    /* A string of colon separated paths to directories containing LSS files */
    private String moduleSearchPath;

    public Library(String searchPath)
    {
	this.moduleSearchPath = searchPath;
	
	this.virtualDirectories = new Vector();
	loadModuleSearchPath(searchPath);
    }

    public void clearLibrary()
    {
	virtualDirectories.clear();
    }

    public String getModuleSearchPath()
    {
	String moduleSearchPath = "";
	Iterator dirIter = virtualDirectories.iterator();
	while(dirIter.hasNext()){
	    VirtualDirectory dir = (VirtualDirectory)dirIter.next();
	    moduleSearchPath += (dir.getURL().getPath()+":");
	}

	return moduleSearchPath;
    }

    public Collection getVirtualDirectories()
    {
	return virtualDirectories;
    }

    public void loadURL(URL u)
    {
	if(u.getProtocol().equals("file")){
	    File f = new File(u.getPath());
	    if(f.exists() && f.isDirectory()){
		VirtualDirectory toplevel = 
		    new VirtualDirectory(u);

		// sort the virtual directories..
		if(virtualDirectories.size() == 0)
		    virtualDirectories.add(toplevel);
		else{
		    boolean placed = false;

		    for(int i = 0; i < virtualDirectories.size(); i++){
			VirtualDirectory currentDir = 
			    (VirtualDirectory)virtualDirectories.elementAt(i);

			if(currentDir.toString().
			   compareTo(toplevel.toString()) >= 0){
			    virtualDirectories.add(i, toplevel);
			    placed = true;
			    break;
			}
		    }
		    if(!placed)
			virtualDirectories.add(toplevel);
		}

		traverseDirectoryTree(toplevel, f);
	    }
	}
    }

    private void loadModuleSearchPath(String searchPath){
	// scan searchPath for .lss files

	int beginIndex = 0, endIndex = 0;

    	endIndex = searchPath.indexOf(":", beginIndex);
	while(endIndex != -1){
	    try{
		loadURL(new URL("file:"+
				searchPath.substring(beginIndex, endIndex)));
	    }
	    catch(java.net.MalformedURLException e){}
	    beginIndex = endIndex+1;
	    endIndex = searchPath.indexOf(":", beginIndex);
	}
	try{
	    loadURL(new URL("file:"+
			    searchPath.substring(beginIndex, 
						 searchPath.length())));
	}
	catch(java.net.MalformedURLException e){}
    }

    public void traverseDirectoryTree(VirtualDirectory vdir, File file)
    {
	File[] descriptions = file.listFiles(new FileFilter(){
		public boolean accept(File file){
		    String s = file.getName();
		    String ext = null;
		    int i = s.lastIndexOf('.');
		    
		    if (i > 0 &&  i < s.length() - 1) {
			ext = s.substring(i+1).toLowerCase();
		    }
		    if(ext != null)
			if(ext.equals("lss"))
			    return true;
		    return false;
		}
	    });

	if(descriptions.length > 0){
	    for(int i=0; i < descriptions.length; i++){
		String s = descriptions[i].getName();
		int begin=0, end=0;
		end = s.lastIndexOf('.');
		if(end < 0)
		    end = s.length();
		begin = s.lastIndexOf(File.pathSeparatorChar);
		if(begin < 0)
		    begin = 0;

		try{
		    LSSFile f = 
			new LSSFile(descriptions[i].toURL(), 
			vdir);
		    vdir.addFile(f);
		}
		catch(java.net.MalformedURLException e){}
	    }
	}
    }

    public void updateModuleSearchPath(String searchPath)
    {
	this.moduleSearchPath = searchPath;
	clearLibrary();
	loadModuleSearchPath(searchPath);
    }

}
