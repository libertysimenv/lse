package Liberty.visualizer.library;

import java.util.*;
import java.net.URL;

public class LSSFile{
    private URL url;
    private String fileName;
    private String typeName;
    private VirtualDirectory parentDir;

    // FIXME: typeName needs to be found another way, the file name
    // may not always be correct
    public LSSFile(URL url, VirtualDirectory parentDir)
    {
	this.url = url;
	this.parentDir = parentDir;

	String tmp = url.getFile();
	int i = tmp.lastIndexOf(java.io.File.separatorChar);
	int j = tmp.lastIndexOf('.');

	if(i > -1)
	    this.fileName = tmp.substring(i+1);
	else
	    this.fileName = tmp;
	if(j > -1)
	    this.typeName = tmp.substring(i+1, j);
	else
	    this.typeName = tmp.substring(i+1);
    }

    public String toString()
    {
	return fileName;
    }

    public String getPath()
    {
	return url.getPath();
    }

    public String getTypeName()
    {
	return typeName;
    }
}
