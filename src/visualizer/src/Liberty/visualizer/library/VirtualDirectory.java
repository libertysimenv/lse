package Liberty.visualizer.library;

import java.util.Vector;
import java.net.URL;

public class VirtualDirectory{
    /* the url where this is loaded from*/
    private URL url;
    /* module description files */
    private Vector files;
    /* a list of sub directories */
    private Vector virtualDirectories;

    public VirtualDirectory(URL url)
    {
	this.url = url;
	files = new Vector();
	virtualDirectories = new Vector();
    }

    public void addFile(LSSFile f)
    {
	files.add(f);
    }

    public URL getURL()
    {
	return url;
    }

    public String toString()
    {
	return url.toString();
    }

    public java.util.Enumeration getFiles()
    {
	return files.elements();
    }

    public java.util.Enumeration getSubDirectories()
    {
	return virtualDirectories.elements();
    }
}
