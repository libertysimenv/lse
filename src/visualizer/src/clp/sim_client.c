#include "sim.h"
#include "jni_stubs.h"


int
sim_server_start_simulation(char *sim_start_arg)
{
  CLIENT *clnt=NULL;
  int result;
  int i;

  fprintf(stderr, "calling sim server to start simulation\n");

  
  for (i=0 ; i < 1000000 && !clnt ; ++i)
    clnt = clnt_create ("sim_socket", SIMSERVER, SIM_SERV_VERSION, "unix");
  if (clnt == NULL) {
    clnt_pcreateerror ("sim_socket");
    fprintf(stderr, "from function: sim_server_start_simulation\n");
    return -1;
  }

  result = sim_start_1((void*)&sim_start_arg, clnt);
  if (result == -1) {
    clnt_perror (clnt, "call failed");
    fprintf(stderr, "from function: sim_server_start_simulation\n");
    result = -1;
  }
  clnt_destroy (clnt);

  return result;
}

int
sim_server_do_timestep(char *sim_do_timestep_arg)
{
  CLIENT *clnt=NULL;
  int result, i;

  for (i=0 ; i < 1000000 && !clnt ; ++i)
    clnt = clnt_create ("sim_socket", SIMSERVER, SIM_SERV_VERSION, "unix");
  if (clnt == NULL) {
    clnt_pcreateerror ("sim_socket");
    fprintf(stderr, "from function: sim_server_do_timestep\n");
    return -1;
  }

  result = sim_do_timestep_1((void*)&sim_do_timestep_arg, clnt);
  if (result == -1) {
    clnt_perror (clnt, "call failed");
    fprintf(stderr, "from function: sim_server_do_timestep\n");
    result = -1;
  }

  clnt_destroy (clnt);

  return result;
}

int
sim_server_finish_simulation(char *sim_finish_arg)
{
  CLIENT *clnt=NULL;
  int result,i ;

  for (i=0 ; i < 1000000 && !clnt ; ++i)
    clnt = clnt_create ("sim_socket", SIMSERVER, SIM_SERV_VERSION, "unix");
  if (clnt == NULL) {
    clnt_pcreateerror ("sim_socket");
    fprintf(stderr, "from function: sim_server_finish_simulation\n");
    return -1;
  }

  result = sim_finish_1((void*)&sim_finish_arg, clnt);
  if (result == -1) {
    clnt_perror (clnt, "call failed");
    fprintf(stderr, "from function: sim_server_finish_simulation\n");
    result = -1;
  }

  clnt_destroy (clnt);

  return result;
}

int
sim_server_get_current_simulation_cycle(char *sim_get_current_cycle_arg)
{
  CLIENT *clnt=NULL;
  int result, i;

  for (i=0 ; i < 1000000 && !clnt ; ++i)
    clnt = clnt_create ("sim_socket", SIMSERVER, SIM_SERV_VERSION, "unix");
  if (clnt == NULL) {
    clnt_pcreateerror ("sim_socket");
    fprintf(stderr, "from function: sim_server_get_current_simulation_cycle\n");
    return -1;
  }

  result = sim_get_current_cycle_1((void*)&sim_get_current_cycle_arg, clnt);
  if (result == -1) {
    clnt_perror (clnt, "call failed");
    fprintf(stderr, "from function: sim_server_get_current_simulation_cycle\n");
    result = -1;
  }

  clnt_destroy (clnt);

  return result;
}

JNIEXPORT jint JNICALL
Java_Liberty_visualizer_lse_ExecutionManager_nativeStartSimulation(
                                                    JNIEnv *env,
                                                    jobject obj){              
  int result;
  result = sim_server_start_simulation(HOST);
  return result;
}

JNIEXPORT jint JNICALL 
Java_Liberty_visualizer_lse_ExecutionManager_nativeDoTimeStep(JNIEnv *env, 
							      jobject obj){
  int ret = sim_server_do_timestep(HOST);
  return ret;
}

JNIEXPORT jint JNICALL 
Java_Liberty_visualizer_lse_ExecutionManager_nativeFinishSimulation(
						   JNIEnv *env, 
						   jobject obj){

  int ret = sim_server_finish_simulation(HOST);
  return ret;
}
