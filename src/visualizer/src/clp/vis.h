#ifndef _VIS_H_RPCGEN
#define _VIS_H_RPCGEN

#include <rpc/rpc.h>

struct vis_handle_command_1_argument {
	char *arg1;
	char *arg2;
};
typedef struct vis_handle_command_1_argument vis_handle_command_1_argument;

#define VISSERVER 0x20000001
#define VIS_SERV_VERSION 1
#define HOST "localhost"
#if defined(__STDC__) || defined(__cplusplus)
#define vis_handle_command 1
extern  int vis_handle_command_1(char *, char *, CLIENT *);
extern  int * vis_handle_command_1_svc(char *, char *, struct svc_req *);
#define vis_update_current_cycle 2
extern  int vis_update_current_cycle_1(int , CLIENT *);
extern  int * vis_update_current_cycle_1_svc(int , struct svc_req *);
#define vis_sim_is_ready 3
extern  int vis_sim_is_ready_1(CLIENT *);
extern  int * vis_sim_is_ready_1_svc(struct svc_req *);
extern int visserver_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define vis_handle_command 1
extern  int vis_handle_command_1();
extern  int * vis_handle_command_1_svc();
#define vis_update_current_cycle 2
extern  int vis_update_current_cycle_1();
extern  int * vis_update_current_cycle_1_svc();
#define vis_sim_is_ready 3
extern  int vis_sim_is_ready_1();
extern  int * vis_sim_is_ready_1_svc();
extern int visserver_1_freeresult ();
#endif /* K&R C */

/* the xdr functions */


static 
bool_t
xdr_vis_handle_command_1_argument (XDR *xdrs,
                                   vis_handle_command_1_argument *objp)
{
         if (!xdr_string (xdrs, &objp->arg1, ~0))
                 return FALSE;
         if (!xdr_string (xdrs, &objp->arg2, ~0))
                 return FALSE;
        return TRUE;
}

  extern int vis_server_set_ready();

#endif /* !_VIS_H_RPCGEN */
