// this is broken
#define LSEfw_PARM(x) LSEfw_ ## x
#define LSEfw_LSE_phases 1

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <errno.h>
#include <SIM_time.h>
#include <SIM_clp_interface.h>
#include "sim.h"

#include <SIM_clp_interface.h>

namespace LSE_vis {

  extern void vis_server_set_ready();

int *
sim_start_1_svc(void *argp, struct svc_req *rqstp)
{
  static int result;

  if (result = LSE_sim_start()) {
    fprintf(LSE_stderr,
	    "CLP: Error %d returned from LSE_sim_start\n",result);
    result = -1;
  }
  fprintf(stderr, "simulation started\n");

  vis_server_set_ready();

  return &result;
}

int *
sim_do_timestep_1_svc(void *argp, struct svc_req *rqstp)
{
  static int result;

  result = LSE_sim_do_timestep();
  if(result < 0)
    fprintf(LSE_stderr,
	    "CLP: Error %d returned from LSE_sim_do_timestep\n", result);

  vis_server_set_ready();

  return &result;
}

int *
sim_finish_1_svc(void *argp, struct svc_req *rqstp)
{

  static int result;
	
  /* finish simulation */
  if (result = LSE_sim_finish(TRUE)) {
    fprintf(LSE_stderr,
	    "CLP: Error %d returned from LSE_sim_finish_svc\n",result);
  }
	
  /* finalize simulator */
	
  if (result = LSE_sim_finalize()) {
    fprintf(LSE_stderr,
	    "CLP: Error %d returned from LSE_sim_finalize_svc\n",result);
  }

  result = LSE_sim_exit_status;

  vis_server_set_ready();

  return &result;
}

int *
sim_get_current_cycle_1_svc(void *argp, struct svc_req *rqstp)
{
  static int  result;

  result = LSE_time_get_cycle(LSE_time_now);

  return &result;
}


#ifndef SIG_PF
#define SIG_PF void(*)(int)
#endif

static char *pname;

static void
simserver_1(struct svc_req *rqstp, register SVCXPRT *transp)
{
  union {
    int fill;
  } argument;
  char *result;
  xdrproc_t _xdr_argument, _xdr_result;
  char *(*local)(char *, struct svc_req *);

  switch (rqstp->rq_proc) {
  case NULLPROC:
    (void) svc_sendreply (transp, (xdrproc_t) xdr_void, (char *)NULL);
    return;

  case sim_start:
    _xdr_argument = (xdrproc_t) xdr_void;
    _xdr_result = (xdrproc_t) xdr_int;
    local = (char *(*)(char *, struct svc_req *)) sim_start_1_svc;
    break;

  case sim_do_timestep:
    _xdr_argument = (xdrproc_t) xdr_void;
    _xdr_result = (xdrproc_t) xdr_int;
    local = (char *(*)(char *, struct svc_req *)) sim_do_timestep_1_svc;
    break;

  case sim_finish:
    _xdr_argument = (xdrproc_t) xdr_void;
    _xdr_result = (xdrproc_t) xdr_int;
    local = (char *(*)(char *, struct svc_req *)) sim_finish_1_svc;
    break;

  case sim_get_current_cycle:
    _xdr_argument = (xdrproc_t) xdr_void;
    _xdr_result = (xdrproc_t) xdr_int;
    local = (char *(*)(char *, struct svc_req *)) sim_get_current_cycle_1_svc;
    break;

  default:
    svcerr_noproc (transp);
    return;
  }
  memset ((char *)&argument, 0, sizeof (argument));
  if (!svc_getargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
    svcerr_decode (transp);
    return;
  }
  result = (*local)((char *)&argument, rqstp);
  if (result != NULL && !svc_sendreply(transp, 
			 (xdrproc_t) _xdr_result, result)) {
    svcerr_systemerr (transp);
  }
  if (!svc_freeargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
    fprintf (stderr, "%s", "unable to free arguments");
    exit (1);
  }
  return;
}
}
using namespace LSE_vis;

int
main (int argc, char **argv, char *envp[])
{
  int i,numused,j;
  int scriptfile = -1;   /* scriptfile position in argument list */
  int rstat;             /* return status */
  register SVCXPRT *transp;
  
  unlink("sim_socket");
  transp = svcunix_create(RPC_ANYSOCK, 0, 0, (char *)"sim_socket");
  if (transp == NULL) {
    fprintf (stderr, "%s", "cannot create service.");
    exit(1);
  }
  if (!svc_register(transp, SIMSERVER, SIM_SERV_VERSION, 
		    simserver_1, 0)) {
    fprintf (stderr, "%s", 
	     "unable to register (SIMSERVER, SIM_SERV_VERSION, unix).");
    exit(1);
  }

  /* do initialization of the LSE simulator */
  pname = argv[0];

  /* initialize the exit status */
  LSE_sim_exit_status = 0;

  /* initialize the error file to point to stderr */
  LSE_stderr = stderr;

  /* initialize the simulator(and domains) */
  if (rstat = LSE_sim_initialize()) { /* exit on error... */
    fprintf(LSE_stderr,
	    "CLP: Error %d on call to LSE_sim_initialize(), exiting\n",
	    rstat);
    vis_server_set_ready();
    exit(1);
  }

  for (i=1;i<argc;) {
    if (!strcmp(argv[i],"--")) {
      i++;
      break;
    }

    if (!strncmp(argv[i],"--sim:",6)) {
      numused = LSE_sim_parse_arg(argc-i,argv[i]+6,argv+i+1);
      if (numused <= 0) {
	fprintf(LSE_stderr,"LSE: Error: unrecognized argument\n");
	vis_server_set_ready();
	exit(0);
      }
      i+= numused;
    }
    else if (!strncmp(argv[i],"--dom:",6)) {
      int j,indno;
      char *ind;
      ind = strchr(argv[i]+6,':');
      if (ind==NULL) {
	fprintf(LSE_stderr,
		"Missing domain instance or class name in argument %s\n",
		argv[i]);
	vis_server_set_ready();
	exit(0);
      }
      *ind = '\0';  /* end the domain instance name */
      numused = LSE_domain_parse_arg(argv[i]+6,argc-i,ind+1,argv+i+1);
      if (numused <= 0) {
	fprintf(LSE_stderr,"LSE: Error: unrecognized domain argument\n");
	vis_server_set_ready();
	exit(0);
      }
      i+= numused;
    }
    else {
      if (!strncmp(argv[i],"--script:",9)) {
	scriptfile=i;
      }
      else if (argv[i][0]!='-') break;
      else {
	fprintf(LSE_stderr,"LSE: Error: unrecognized argument\n");
	vis_server_set_ready();
	exit(0);
      }
      i++;
    }

  } /* for arguments */
  /* Exit condition: i = start of program options or potentially benchmark */

  /* Exit condition: i = start of program options or potentially benchmark */
  if (rstat = LSE_sim_parse_leftovers(argc-i,argv+i, envp)) {
    if (rstat<0){
	fprintf(stderr, 
		"ERROR: improper arguments passed to the LSE simulator");
    }
    vis_server_set_ready();
    exit(1);
  }

  fprintf(stderr, "simulation rpc server started\n");

  vis_server_set_ready();

  svc_run ();
  fprintf (stderr, "%s", "svc_run returned");
  exit (1);
  /* NOTREACHED */
}
