
program SIMSERVER {
  version SIM_SERV_VERSION {

    int sim_start() = 1;
    int sim_do_timestep() = 2;
    int sim_finish() = 3;
    int sim_get_current_cycle() = 4;

  } = 1;
} = 0x20000002;
