#include "vis.h"
#include "jni_stubs.h"

JNIEnv **global_env_pointer = NULL;
jobject *global_view_pointer = NULL;
jobject *execution_manager_pointer = NULL;

JNIEXPORT jint JNICALL 
Java_Liberty_visualizer_lse_ExecutionManager_nativeStartRpcServer(JNIEnv* env,
							       jobject caller,
							       jobject view)
{
  global_view_pointer = &view;
  global_env_pointer = &env;
  execution_manager_pointer = &caller;
  vis_server_start();
}

void
jni_vis_handle_command(char *eid, char *com)
{
  jstring elementID;
  jstring command;
  jthrowable exc;

  jclass cls = 
    (**global_env_pointer)->GetObjectClass(*global_env_pointer, 
					   *global_view_pointer);
  jmethodID mid = 
    (**global_env_pointer)->GetMethodID(*global_env_pointer, cls, 
					"handleCommand", 
					"(Ljava/lang/String;"
					"Ljava/lang/String;)V");

  elementID = (**global_env_pointer)->NewStringUTF(*global_env_pointer, eid);
  command = (**global_env_pointer)->NewStringUTF(*global_env_pointer, com);


  if (mid == 0) {
    return;
  }
  (**global_env_pointer)->CallVoidMethod(*global_env_pointer, 
					 *global_view_pointer, 
					 mid,
					 elementID,
					 command);
  exc = (**global_env_pointer)->ExceptionOccurred(*global_env_pointer);
  if (exc) {
    jclass newExcCls;

    (**global_env_pointer)->ExceptionDescribe(*global_env_pointer);
    (**global_env_pointer)->ExceptionClear(*global_env_pointer);

    newExcCls = (**global_env_pointer)->FindClass(*global_env_pointer, 
				        "java/lang/Exception");
    if (newExcCls == 0) { 
      /* Unable to find the new exception class, give up. */
      return;
    }
    (**global_env_pointer)->ThrowNew(*global_env_pointer, newExcCls, 
		     "Exception vis_server.c: vis_handle_command");
  }
}

void
jni_set_cycle(jint cycle)
{
  jthrowable exc;
  jclass cls = 
    (**global_env_pointer)->GetObjectClass(*global_env_pointer, 
					   *execution_manager_pointer);
  jmethodID mid = 
    (**global_env_pointer)->GetMethodID(*global_env_pointer, cls, 
					"setCurrentCycle",
					"(I)V");
  if (mid == 0) {
    fprintf(stderr, "JNI Error: could not find method: setCurrentCycle\n");
    return;
  }
  (**global_env_pointer)->CallVoidMethod(*global_env_pointer, 
					 *execution_manager_pointer, 
					 mid,
					 cycle);
  exc = (**global_env_pointer)->ExceptionOccurred(*global_env_pointer);
  if (exc) {
    jclass newExcCls;

    (**global_env_pointer)->ExceptionDescribe(*global_env_pointer);
    (**global_env_pointer)->ExceptionClear(*global_env_pointer);

    newExcCls = (**global_env_pointer)->FindClass(*global_env_pointer, 
				        "java/lang/Exception");
    if (newExcCls == 0) { 
      /* Unable to find the new exception class, give up. */
      return;
    }
    (**global_env_pointer)->ThrowNew(*global_env_pointer, newExcCls, 
		     "Exception vis_server.c: vis_handle_command");
  }
}

void
jni_set_vis_ready()
{
  jthrowable exc;
  jclass cls =
    (**global_env_pointer)->GetObjectClass(*global_env_pointer,
					   *execution_manager_pointer);
  jmethodID mid =
    (**global_env_pointer)->GetMethodID(*global_env_pointer, cls,
					"releaseSimServerActiveLock",
					"()V");
  if (mid == 0) {
    fprintf(stderr, 
	    "JNI Error: could not find method: releaseSimServerActiveLock\n");
    return;
  }

  (**global_env_pointer)->CallVoidMethod(*global_env_pointer,
					 *execution_manager_pointer,
					 mid,
					 JNI_TRUE);
  exc = (**global_env_pointer)->ExceptionOccurred(*global_env_pointer);
  if (exc) {
    jclass newExcCls;

    (**global_env_pointer)->ExceptionDescribe(*global_env_pointer);
    (**global_env_pointer)->ExceptionClear(*global_env_pointer);

    newExcCls = (**global_env_pointer)->FindClass(*global_env_pointer, 
				        "java/lang/Exception");
    if (newExcCls == 0) { 
      /* Unable to find the new exception class, give up. */
      return;
    }
    (**global_env_pointer)->ThrowNew(*global_env_pointer, newExcCls, 
		     "Exception vis_server.c: vis_handle_command");
  }
}

void
jni_set_rpc_server_ready()
{
  jthrowable exc;
  jclass cls =
    (**global_env_pointer)->GetObjectClass(*global_env_pointer,
					   *execution_manager_pointer);
  jmethodID mid =
    (**global_env_pointer)->GetMethodID(*global_env_pointer, cls,
					"releaseVisServerActiveLock",
					"()V");
  if (mid == 0) {
    fprintf(stderr, 
	    "JNI Error: could not find method: releaseVisServerActiveLock\n");
    return;
  }

  (**global_env_pointer)->CallVoidMethod(*global_env_pointer,
					 *execution_manager_pointer,
					 mid,
					 JNI_TRUE);
  exc = (**global_env_pointer)->ExceptionOccurred(*global_env_pointer);
  if (exc) {
    jclass newExcCls;

    (**global_env_pointer)->ExceptionDescribe(*global_env_pointer);
    (**global_env_pointer)->ExceptionClear(*global_env_pointer);

    newExcCls = (**global_env_pointer)->FindClass(*global_env_pointer, 
				        "java/lang/Exception");
    if (newExcCls == 0) { 
      /* Unable to find the new exception class, give up. */
      return;
    }
    (**global_env_pointer)->ThrowNew(*global_env_pointer, newExcCls, 
		     "Exception vis_server.c: vis_handle_command");
  }
}

int *
vis_handle_command_1_svc(char *arg1, char *arg2,  struct svc_req *rqstp)
{
  static int result = 0;
  jni_vis_handle_command(arg1, arg2);
  return &result;
}

int *
vis_update_current_cycle_1_svc(int arg1,  struct svc_req *rqstp)
{
  static int result = 0;
  jni_set_cycle(arg1);
  return &result;
}

int *
vis_sim_is_ready_1_svc(struct svc_req *rqstp)
{
  static int result = 0;

  jni_set_vis_ready();

  return &result;
}
