#include <memory.h> /* for memset */
#include "sim.h"

/* Default timeout can be changed using clnt_control() */
static struct timeval TIMEOUT = { 25, 0 };

int
sim_start_1(void *argp, CLIENT *clnt)
{
  static int clnt_res;

  memset((char *)&clnt_res, 0, sizeof(clnt_res));
  if (clnt_call (clnt, sim_start,
		 (xdrproc_t) xdr_void, (caddr_t) argp,
		 (xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		 TIMEOUT) != RPC_SUCCESS) {

    return -1;
  }

  return clnt_res;
}

int
sim_do_timestep_1(void *argp, CLIENT *clnt)
{
  static int clnt_res;

  memset((char *)&clnt_res, 0, sizeof(clnt_res));
  if (clnt_call (clnt, sim_do_timestep,
		 (xdrproc_t) xdr_void, (caddr_t) argp,
		 (xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		 TIMEOUT) != RPC_SUCCESS) {
    return -1;
  }
  return clnt_res;
}

int
sim_finish_1(void *argp, CLIENT *clnt)
{
  static int clnt_res;

  memset((char *)&clnt_res, 0, sizeof(clnt_res));
  if (clnt_call (clnt, sim_finish,
		 (xdrproc_t) xdr_void, (caddr_t) argp,
		 (xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		 TIMEOUT) != RPC_SUCCESS) {
    return -1;
  }
  return clnt_res;
}

int
sim_get_current_cycle_1(void *argp, CLIENT *clnt)
{
  static int clnt_res;

  memset((char *)&clnt_res, 0, sizeof(clnt_res));
  if (clnt_call (clnt, sim_get_current_cycle,
		 (xdrproc_t) xdr_void, (caddr_t) argp,
		 (xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		 TIMEOUT) != RPC_SUCCESS) {
    return -1;
  }
  return clnt_res;
}
