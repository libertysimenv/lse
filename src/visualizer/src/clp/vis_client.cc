#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <memory.h> /* for memset */

namespace LSE_vis {

#include "vis.h"

/* Default timeout can be changed using clnt_control() */
static struct timeval TIMEOUT = { 25, 0 };

int
vis_handle_command_1(char *arg1, char *arg2,  CLIENT *clnt)
{
  vis_handle_command_1_argument arg;
  static int clnt_res;
  
  memset((char *)&clnt_res, 0, sizeof(clnt_res));
  arg.arg1 = arg1;
  arg.arg2 = arg2;
  if (clnt_call (clnt, vis_handle_command, 
		 (xdrproc_t) xdr_vis_handle_command_1_argument, 
		 (caddr_t) &arg,
		 (xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		 TIMEOUT) != RPC_SUCCESS) {
    return -1;
  }
  return clnt_res;
}

int
vis_update_current_cycle_1(int arg1,  CLIENT *clnt)
{
  static int clnt_res;
  
  memset((char *)&clnt_res, 0, sizeof(clnt_res));
  if (clnt_call (clnt, vis_update_current_cycle,
		 (xdrproc_t) xdr_int, (caddr_t) &arg1,
		 (xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		 TIMEOUT) != RPC_SUCCESS) {
    return -1;
  }
  return clnt_res;
}

int
vis_sim_is_ready_1(CLIENT *clnt)
{
  static int clnt_res;
  
  memset((char *)&clnt_res, 0, sizeof(clnt_res));
  if (clnt_call (clnt, vis_sim_is_ready, 
		 (xdrproc_t) xdr_void, (caddr_t) NULL,
		 (xdrproc_t) xdr_int, (caddr_t) &clnt_res,
		 TIMEOUT) != RPC_SUCCESS) {
    return -1;
  }
  return clnt_res;
}

int handle_command(char *arg1, char *arg2)
{
  CLIENT *clnt=0;
  int result, i;

  int arg1_len = strlen(arg1);
  int arg2_len = strlen(arg2);

  char *elementID = (char *)malloc(sizeof(char) * (arg1_len+1));
  char *command = (char *)malloc(sizeof(char) * (arg2_len+1));
  
  strcpy(elementID, arg1);
  strcpy(command, arg2);

  for (i=0 ; i < 1000000 && !clnt ; ++i)
    clnt = clnt_create ("vis_socket", VISSERVER, VIS_SERV_VERSION, "unix");
  if (clnt == NULL) {
    clnt_pcreateerror("vis_socket");
    fprintf(stderr, "from function: vis_server_handle_command\n");
    return -1;
  }
  else{
    result = vis_handle_command_1(elementID, 
				  command, clnt);
    if (result == -1) {
      clnt_perror(clnt, "call failed");
      fprintf(stderr, "from function: vis_server_handle_command\n");
      result = -1;
    }

    clnt_destroy (clnt);
  }

  free(elementID);
  free(command);
  return result;
}

int update_current_cycle(int current_cycle)
{
  CLIENT *clnt=0;
  int result, i;

  for (i=0 ; i < 1000000 && !clnt ; ++i)
    clnt = clnt_create ("vis_socket", VISSERVER, VIS_SERV_VERSION, "unix");
  if (clnt == NULL) {
    clnt_pcreateerror ("vis_socket");
    fprintf(stderr, "from function: vis_server_update_current_cycle\n");
    return -1;
  }

  result = vis_update_current_cycle_1(current_cycle, clnt);
  if (result == -1) {
    clnt_perror (clnt, "call failed");
    fprintf(stderr, "from function: vis_server_update_current_cycle\n");
    result = -1;
  }
  clnt_destroy(clnt);
  return result;
}

int
vis_server_set_ready()
{
  CLIENT *clnt=NULL;
  int result,i;

  for (i=0 ; i < 1000000 && !clnt ; ++i)
    clnt = clnt_create("vis_socket", VISSERVER, VIS_SERV_VERSION, "unix");
  if (clnt == NULL) {
    clnt_pcreateerror ("vis_socket");
    fprintf(stderr, "from function: vis_server_set_ready\n");
    return -1;
  }

  result = vis_sim_is_ready_1(clnt);
  if (result == -1) {
    clnt_perror (clnt, "call failed");
    fprintf(stderr, "from function: vis_server_set_ready\n");
    result = -1;
  }

  clnt_destroy(clnt);

  return result;
}

} // namespace LSE_vis
