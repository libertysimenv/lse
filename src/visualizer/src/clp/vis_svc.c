#include "vis.h"
#include <stdio.h>
#include <stdlib.h>
#include <rpc/pmap_clnt.h>
#include <string.h>
#include <memory.h>
#include <sys/socket.h>
#include <netinet/in.h>

#ifndef SIG_PF
#define SIG_PF void(*)(int)
#endif

static int *
_vis_handle_command_1 (vis_handle_command_1_argument *argp, 
		       struct svc_req *rqstp)
{
  return (vis_handle_command_1_svc(argp->arg1, argp->arg2, rqstp));
}

static int *
_vis_update_current_cycle_1 (int  *argp, struct svc_req *rqstp)
{
  return (vis_update_current_cycle_1_svc(*argp, rqstp));
}

static int *
_vis_sim_is_ready_1 (void  *argp, struct svc_req *rqstp)
{
  return (vis_sim_is_ready_1_svc(rqstp));
}

static void
visserver_1(struct svc_req *rqstp, register SVCXPRT *transp)
{
  union {
    vis_handle_command_1_argument vis_handle_command_1_arg;
    int vis_update_current_cycle_1_arg;
  } argument;
  char *result;
  xdrproc_t _xdr_argument, _xdr_result;
  char *(*local)(char *, struct svc_req *);

  switch (rqstp->rq_proc) {
  case NULLPROC:
    (void) svc_sendreply (transp, (xdrproc_t) xdr_void, (char *)NULL);
    return;

  case vis_handle_command:
    _xdr_argument = (xdrproc_t) xdr_vis_handle_command_1_argument;
    _xdr_result = (xdrproc_t) xdr_int;
    local = (char *(*)(char *, struct svc_req *)) _vis_handle_command_1;
    break;

  case vis_update_current_cycle:
    _xdr_argument = (xdrproc_t) xdr_int;
    _xdr_result = (xdrproc_t) xdr_int;
    local = (char *(*)(char *, struct svc_req *)) _vis_update_current_cycle_1;
    break;

  case vis_sim_is_ready:
    _xdr_argument = (xdrproc_t) xdr_void;
    _xdr_result = (xdrproc_t) xdr_int;
    local = (char *(*)(char *, struct svc_req *)) _vis_sim_is_ready_1;
    break;

  default:
    svcerr_noproc (transp);
    return;
  }
  memset ((char *)&argument, 0, sizeof (argument));
  if (!svc_getargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
    svcerr_decode (transp);
    return;
  }
  result = (*local)((char *)&argument, rqstp);
  if (result != NULL && !svc_sendreply(transp, 
				       (xdrproc_t) _xdr_result, result)) {
    svcerr_systemerr (transp);
  }
  if (!svc_freeargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
    fprintf (stderr, "%s", "unable to free arguments");
    exit (1);
  }
  return;
}


int
vis_server_start()
{
  SVCXPRT *transp=NULL;
  unlink("vis_socket");
  transp = svcunix_create(RPC_ANYSOCK, 0, 0, "vis_socket");
  if (transp == NULL) {
    fprintf (stderr, "%s", "cannot create service.");
    exit(1);
  }
  if (!svc_register(transp, VISSERVER, VIS_SERV_VERSION, 
		    visserver_1, 0)) {
    fprintf (stderr, "%s", 
	     "unable to register (VISSERVER, VIS_SERV_VERSION, unix).");
    exit(1);
  }

  svc_run ();
  fprintf (stderr, "%s", "svc_run returned");
  exit (1);
  /* NOTREACHED */
}
