#include <jni.h>
/* Header for class Liberty.visualizer.lse.ExecutionManager */

#ifndef _Included_Liberty_visualizer_lse_ExecutionManager
#define _Included_Liberty_visualizer_lse_ExecutionManager
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     Liberty_visualizer_lse_ExecutionManager
 * Method:    nativeStartSimulation
 * Signature: ()I
 */
JNIEXPORT jint JNICALL 
Java_Liberty_visualizer_lse_ExecutionManager_nativeStartSimulation
  (JNIEnv *, jobject);

/*
 * Class:     Liberty_visualizer_lse_ExecutionManager
 * Method:    nativeFinishSimulation
 * Signature: ()I
 */
JNIEXPORT jint JNICALL 
Java_Liberty_visualizer_lse_ExecutionManager_nativeFinishSimulation
  (JNIEnv *, jobject);

/*
 * Class:     Liberty_visualizer_lse_ExecutionManager
 * Method:    nativeDoTimeStep
 * Signature: ()I
 */
JNIEXPORT jint JNICALL 
Java_Liberty_visualizer_lse_ExecutionManager_nativeDoTimeStep
  (JNIEnv *, jobject);

/*
 * Class:     Liberty_visualizer_lse_ExecutionManager
 * Method:    nativeStartRpcServer
 * Signature: (LLiberty/visualizer/view/SchematicView)I
 */
JNIEXPORT jint JNICALL
Java_Liberty_visualizer_lse_ExecutionManager_nativeStartRpcServer
  (JNIEnv *, jobject, jobject);



#ifdef __cplusplus
}
#endif
#endif
