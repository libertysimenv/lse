
program VISSERVER {
  version VIS_SERV_VERSION {

    int vis_handle_command(string, string) = 1;
    int vis_update_current_cycle(int) = 2;
    int vis_sim_is_ready() = 3;

  } = 1;
} = 0x20000001;
