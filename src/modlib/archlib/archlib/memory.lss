/* -*-c-*- */
/*
 * Copyright (c) 2000-2007 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * Memory library LSS file.
 *
 * Author: Ram Rangan <ram@cs.princeton.edu>
 *
 * Pulls in all modules in the memory library.  Also defines a type generator 
 * for memory accesses.  These modules depend upon the cache domain, but the
 * domain is *not* imported at the top level so that the user of the archlib
 * doesn't have an unnecessary domain instance running around.  Instead, they
 * are only imported if you actually use the modules or type generator.
 *
 */

package archlib.memory;

// memoryAccess_t is a type generator which describes memory accesses.  Note
// that *any* lss type providing the same fields could be used.  Data is not 
// normally carried in LSE memory hierarchy accesses, as the emulator is 
// usually used for this.  If you wish to carry data, put it in the extra 
// field.

fun memoryAccess_t(addr_t : type) => type {
  import LSE_cache;

  return struct {
    // Type of access message
    pmsg : LSE_cache::LSE_cache_msg_t(LSE_cache::defaultEngine);

    // message ID to reply to
    replyID : uint64;

    // addressing.  Typically addr is the start of the access and len
    // is the access length.  To model critical-word-first, you should
    // adjust the timing of when "data available" messages are sent; additional
    // information needed for that should be put in the extra fields.
    addr : addr_t; // address of the access, generally aligned
    len : addr_t;  // length of the access.
  };
};

fun memoryAccess_t(addr_t : type, extra_t : type) => type {
  import LSE_cache; 

  return struct {
    // Type of access message
    pmsg : LSE_cache::LSE_cache_msg_t(LSE_cache::defaultEngine);

    // message ID to reply to
    replyID : uint64;

    // addressing.  Typically addr is the start of the access and len
    // is the access length.  To model critical-word-first, you should
    // adjust the timing of when "data available" messages are sent; additional
    // information needed for that should be put in the extra fields.
    addr : addr_t; // address of the access, generally aligned
    len : addr_t;  // length of the access.

    // and the extra fields
    extra : extra_t;
  };
};

include "archlib/memory/cache.lss";
include "archlib/memory/mshr.lss";
include "archlib/memory/cache_config1.lss";
