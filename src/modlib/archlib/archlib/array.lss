/* -*-c-*- */

/*
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 *
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 *
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 *
 * An array  module definition file
 *
 * Author: Ram Rangan <ram@cs.princeton.edu>
 *          
 * The array module is a memory element that supports set-associative lookups
 * with parametric types and lookup functions.  It supports checkpointing,
 * though, unfortunately, when the data type is set to "none", it wastes
 * a lot of space.
 *
 */



subpackage array {

  fun element_struct(data_t : type, tag_t : type) => type {
    return struct { /* list alphabetically so it matches LSS */
      data : data_t;
      tag : tag_t;
    };
  };

  fun row_struct(data_t : type, tag_t : type, assoc : int) => type {
    var element_t = archlib.array :: element_struct(data_t, tag_t) : type;
    return struct {
      way : element_t[assoc];
    };
  };
}

module array {

  import LSE_chkpt;
  add_to_domain_searchpath(LSE_chkpt::checkpointer);
 
  tar_file = "archlib/array.clm";
  phase_start = TRUE;
  phase_end = TRUE;
  phase = FALSE;
  reactive = FALSE;

  runtimeable parameter num_rows = 1: int;

  parameter assoc = 1: int;

  /* Simply determine whether enable is checked before calling the write
   * user points.  Do *NOT* use enable as a "write vs. read" port flag; you
   * are guaranteed to screw things up royally.
   */
  parameter write_on_enable = TRUE : boolean;
  
  internal parameter data_t = int : type; 
  export data_t as data_t;
  internal parameter tag_t = int : type;
  export tag_t as tag_t;

  typedef element_t : archlib.array :: element_struct(data_t, tag_t);
  export element_t as element_t;

  typedef row_t : archlib.array :: row_struct(data_t, tag_t, assoc);
  export row_t as row_t;

  /* ports */
  inport request : *;
  request.handler = TRUE;
  outport response : *;
  response.handler = TRUE;
  outport hit : *; /* hit/miss info */
  hit.handler = TRUE;
  inport bulkcontrol : *;
  bulkcontrol.independent = TRUE;

  // for default functions
  this.funcheader = <<<
#include <string.h>
	       >>>;

  if(response.width != request.width) {
    punt(<<<response port width (${response.width}) must match >>> +
         <<<request port width (${request.width})>>>);
  }
  if(hit.width > 0 && hit.width != request.width) {
    punt(<<<hit port width (${hit.width}) must match >>> +
         <<<request port width (${request.width})>>>);
  }

  var hit_up : literal;
  if (hit.width > 0) {
    hit_up = <<<
      ('USERPOINT(get_hitinfo)','response.data','1'),
      ('USERPOINT(get_hitinfo)','hit.data','1'),
    >>>;
  } else { hit_up = ""; }

  /* I really hate the fact that data affects enable! */
  port_dataflow = <<<[
		   ('USERPOINT(*)','*','0'),
	           ('*','*',  '0'),
		   ('request.data', 'response.data', 'isporti==osporti'),
		   ('request.en', 'response.en', 'isporti==osporti'),
		   ('request.data', 'hit.data', 'isporti==osporti'),
		   ('request.en', 'hit.en', 'isporti==osporti'),
		   ('hit.ack','request.ack','isporti==osporti'),
		   ('response.ack','request.ack','isporti==osporti'),
		   ('USERPOINT(is_read)','response.data','1'),
		   ('USERPOINT(is_read)','hit.data','1'),
		   ('USERPOINT(get_index)','response.data','1'),
		   ('USERPOINT(get_index)','hit.data','1'),
		   ('USERPOINT(get_tag)','response.data','1'),
		   ('USERPOINT(get_tag)','hit.data','1'),
		   ('USERPOINT(tag_match)','response.data','1'),
		   ('USERPOINT(tag_match)','hit.data','1'),
		   ('USERPOINT(read)','response.data','1'),
		   ('USERPOINT(read)','hit.data','1'),
		   ${hit_up}
		   ]>>>;
 
  /* userpoints */
  parameter array_init_func = <<< >>>:
    userpoint(<<<${row_t} *rows>>> => <<< void >>>);

  parameter is_write: 
    userpoint(<<<int porti, LSE_dynid_t id, LSE_port_type(request) *request>>>
               => <<< boolean >>>);
			      
  parameter is_read: 
    userpoint(<<<int porti, LSE_dynid_t id, LSE_port_type(request) *request>>>
               => <<< boolean >>>);

  parameter get_index:
    userpoint(<<<int porti, LSE_dynid_t id, LSE_port_type(request) *request>>>
              => <<< int >>>);

  parameter get_tag:
    userpoint(<<<int porti, LSE_dynid_t id, LSE_port_type(request) *request>>>
              => <<< ${tag_t} >>>);
  
  /* return TRUE if tags match, FALSE otherwise */
  parameter tag_match:
    userpoint(<<<int porti, int wayi, int index,
              ${tag_t} tag1, LSE_dynid_t id1, LSE_port_type(request) *data1,
	      ${tag_t} tag2, ${element_t} *element2>>> => <<< boolean >>>);
  tag_match = <<<{
    return !memcmp((void*)&tag1, (void*)&tag2, sizeof(${tag_t}));
  }>>>;
  
  parameter get_write_way:
    userpoint(<<<int porti, int index, LSE_dynid_t id, 
                 LSE_port_type(request) *request, ${row_t} *row>>>
	      => <<< int >>>);
  if(assoc == 1) {
    get_tag = <<< {tag_t} tag; return tag; >>>;
    tag_match = <<< return TRUE; >>>;
    get_write_way = <<< return 0; >>>;
  }
  

  /* hitway is -1 if associative lookup is a miss */
  parameter read:
    userpoint(<<<int porti, LSE_signal_t status, LSE_dynid_t id, 
	      LSE_port_type(request) *request, ${row_t} *row, 
	      LSE_port_type(response) *response, int index, 
	      int hitway>>> => <<< LSE_signal_t >>>);

  parameter write:
    userpoint(<<<int porti, LSE_signal_t status, LSE_dynid_t id, 
	      LSE_port_type(request) *request, ${row_t} *row, int index, 
	      int writeway>>>
	      => <<< void >>>);

  parameter bulkcontrol_func = <<< >>>:
    userpoint(<<<int porti, LSE_signal_t status, LSE_dynid_t id, 
	      LSE_port_type(bulkcontrol) *control, ${row_t} *array >>> => 
	      <<< void >>>);

  if(hit.width > 0) {
    parameter get_hitinfo = <<< >>>:
      userpoint(<<<int porti, LSE_signal_t status, LSE_dynid_t id, 
		LSE_port_type(request) *request, ${row_t} *row, 
		LSE_port_type(hit) *hitdata, int index, int hitway>>> 
		=> <<< LSE_signal_t >>>);
  }

  /* perform a lookup operation, as if something had happened on the
   * request port that would be a lookup (read)
   *
   * Basically, this method does all the calculations which occur on
   * a request for which is_read is TRUE.
   */
  if (hit.width > 0) {
    method lookup : (<<<int porti, LSE_dynid_t id,
                     LSE_port_type(request) *idata,
                     LSE_port_type(response) *odata,
                     LSE_signal_t *ostat,
                     LSE_port_type(hit) *hdata,
                     LSE_signal_t *hstat>>> => <<< void >>>);
  } else {
    method lookup : (<<<int porti, LSE_dynid_t id,
                     LSE_port_type(request) *idata,
                     LSE_port_type(response) *odata,
                     LSE_signal_t *ostat>>> => <<< void >>>);
  }

  /* perform an update operation, as if something happened on the request
   * port that would be an update (write)
   *
   * Basically, this method does all the stuff which occurs on
   * a request for which is_write is TRUE.
   */
  method update : (<<<int porti, LSE_signal_t status, LSE_dynid_t id,
                      LSE_port_type(request) *idata>>>
                   => <<< void >>>);

  /* perform a bulk update operation, as if something happened on the 
   * bulkcontrol port.
   *
   */
  method bulkupdate : (<<<int porti, LSE_dynid_t id,
                       LSE_port_type(bulkcontrol) *idata>>> => <<< void >>>);

  method get_array_pointer : (<<< void >>> => <<< ${row_t}* >>>);

  method set_array : (<<< ${row_t} *newarr >>> => <<< void >>>);

  /************************************************************/
  /************** Methods/userpoints for checkpointing ********/
  /************************************************************/
  /* 
   * polymorphism reigns, but makes life difficult for us, principally
   * in the encoding of data....  
   */

  method chkpt_add_toc:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  method chkpt_check_toc:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  method chkpt_read_data:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  method chkpt_skip_data:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  method chkpt_write_data:
     (<<<LSE_chkpt::file_t *cpFile, const char *name, boolean newSeg>>>
                         => <<<LSE_chkpt::error_t>>>);

  parameter translate_data_t:
    userpoint(<<<${data_t} *dp, unsigned char *cp, 
                 boolean isBigEndian, boolean wasBigEndian, 
                 size_t old_size>>> => <<< void >>>);
  
  parameter translate_tag_t:
    userpoint(<<<${tag_t} *dp, unsigned char *cp, 
                 boolean isBigEndian, boolean wasBigEndian, 
                 size_t old_size>>> => <<< void >>>);
  
  translate_data_t = <<<{
    LSE_report_err("translate_data_t has not been overridden\n");
  }>>>;
  translate_tag_t = <<<{
    LSE_report_err("translate_tag_t has not been overridden\n");
  }>>>;

};

