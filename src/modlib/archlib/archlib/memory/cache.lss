/* -*-c-*- */

subpackage cache {

  typedef cache_access_mode_t : enum {
    read, 
    write,
    invalidate,
    special /* can be used for multiprocessor simulation, 
	       writes to status bits and such
	    */
  };


  typedef write_policy_t : enum {
    write_back,
    write_through
  };
  
  typedef write_allocate_policy_t : enum {
    allocate_on_write_miss,
    no_allocate_on_write_miss
  };

  typedef invalidate_policy_t : enum {
    invalidate_on_miss,
    invalidate_on_fill
  };

  typedef replacement_policy_t : enum {
    random,
    LRU,
    custom
  };

  /* cache access type constructor */
  fun cache_access_struct(special_mode_t : type,  data_t : type) => type {
    return struct {
      addr : LSE_emu::LSE_emu_addr_t;
      data : data_t;
      size : uint32;
      mode : struct { basic : cache_access_mode_t; special : special_mode_t; };
      req_id : uint32;
    };
  };

};
