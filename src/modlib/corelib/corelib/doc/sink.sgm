<!--<!DOCTYPE chapter PUBLIC "-//Liberty ARG//DTD DocBook V4.1-Based Extension V1.0//EN">-->
<sect1><title>The <modulename>sink</> Module</>

<bridgehead renderas="sect2">Short Description</>
<para>
Instances of the sink module simply ack any data on the in port and
throw it away.
</para>

<bridgehead renderas="sect2">Detailed Description</>
<para>
Instances of the sink module simply ack any data on any
<portname>in</> port instance and then throw it away.  The instance
calls the <modparam>sink_func</> on every port instance at the end of
the timestep with the <portname>in</> port status, <portname>in</>
port data, dynamic identifier and port instance number.
</para>

<bridgehead renderas="sect2">Types</>
<para>none</para>

<bridgehead renderas="sect2">Parameters</>
<parmlist>
  <parmlistentry>
    <funcsynopsis>
      <funcprototype>
        <funcdef>void <function>sink_func</function></funcdef>
        <paramdef>LSE_signal_t <parameter>status</></paramdef>
        <paramdef>LSE_dynid_t <parameter>id</></paramdef>
        <paramdef>LSE_port_type(in) *<parameter>data</></paramdef>
        <paramdef>int <parameter>porti</></paramdef>
      </funcprototype>
    </funcsynopsis>
    <listitem>
    <para>
      Called on every <portname>in</> port instance at the end of
      timestep.  The default value is:
      <programlisting>
      &lt;&lt;&lt; ; &gt;&gt;&gt;
      </programlisting>
    </para>
    </listitem>
  </parmlistentry>
</parmlist>
        
<bridgehead renderas="sect2">Events</>
<variablelist>
  <varlistentry><term><varname>SUNK_DATA</></term>
    <listitem>
    <para>
    This event passes the following data to a collector's record code
    at the end of timestep when an item of data is sunk:
    <programlisting>
porti : &lt;&lt;&lt;int&gt;&gt;&gt;
status : &lt;&lt;&lt;LSE_signal_t&gt;&gt;&gt;
id : &lt;&lt;&lt;int&gt;&gt;&gt;
datap : &lt;&lt;&lt;LSE_port_type(<portname>in</>) *&gt;&gt;&gt;
    </programlisting>
    <literal>porti</> is the port instance on which the stored data
    was received, <literal>status</> is the port status, 
    <literal>id</> is the dynamic id stored, and
    <literal>datap</> is a pointer to the data stored.  It is illegal
    to modify the data pointed to by <literal>datap</>.
    </para>
    </listitem>
  </varlistentry>
</variablelist>

<bridgehead renderas="sect2">Ports</>
<variablelist>
  <varlistentry><term><portname>in:'a</></term>
  <listitem><para>The input port.  All data recieved on this port is 
                  acknowledged and thrown out.</para></listitem>
  </varlistentry>
</variablelist>

<bridgehead renderas="sect2">Functions</>
<para>none</para>

<bridgehead renderas="sect2">Methods and queries</>
<para>none</para>

</sect1>


