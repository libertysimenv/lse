<!--<!DOCTYPE chapter PUBLIC "-//Liberty ARG//DTD DocBook V4.1-Based Extension V1.0//EN">-->
<sect1><title>The <modulename>tee</> Module</>

<bridgehead renderas="sect2">Short Description</>
<para>
Instances of the tee module fan out an input signal to multiple output
signals.
</para>


<bridgehead renderas="sect2">Detailed Description</>
<para>
An instance of the tee module fans out input on its <portname>in</>
port instances to multiple output port instances on the
<portname>out</> port.  If there are m instances on the
<portname>in</> port, and n instances of the <portname>out</> port,
the input data and enable signals on port <portname>in[i]</> are
forwarded to <portname>out[(n/m)*i+j]</> for j=0..(n/m-1).  (n must be
evenly divisible by m).
</para>
<para>
Instances also propagate ack signals on the <portname>out</> port back
to the <portname>in</> ports.  If the <modparam>control_flow_style</>
parameter is set to <constant>tee::and_acks</>, the ack signals from
port instances <portname>out[(n/m)*i+j]</>, j=0..(n/m-1), are ANDed
together and the result is forwarded to input port <portname>in[i]</>
(with <constant>LSE_signal_ack</> being 1 and
<constant>LSE_signal_nack</> being 0).  For any ack signal on
<portname>out[(n/m)*i+j]</>, if there is an unknown ack value but a
known ack signal is <constant>LSE_signal_nack</>, then the
<portname>in[i]</>'s ack signal is set to
<constant>LSE_signal_nack</>.  If the <modparam>control_flow_style</>
parameter is set to <constant>tee::or_acks</>, the ack signal on input
port <portname>in[i]</> is computed by ORing the ack signals on
<portname>out[(n/m)*i+j]</> in a similar fashion, with a known ack
signal with value <constant>LSE_signal_ack</> forcing
<portname>in[i]</>'s ack signal to be <portname>LSE_signal_ack</>.
The default setting is to AND the ack signals.
</para>

<bridgehead renderas="sect2">Types</>
<variablelist>
  <varlistentry>
    <term><varname>tee::control_flow_style_t</>:<type>enum {and_acks,
    or_acks}</></term>
    <listitem>
      <para>Type for the <modparam>control_flow_style</> parameter</para>
    </listitem>
  </varlistentry>
</variablelist>

<bridgehead renderas="sect2">Parameters</>
<parmlist>
  <parmlistentry>
    <term><modparam>control_flow_style</>:<type>control_flow_style_t</></term>
    <listitem>
      <para>
        Determines whether acks are ANDed or ORed to generate ack for
        the <portname>in</> port instances.  The default value is
        <constant>tee::and_acks</>
      </para>
    </listitem>
  </parmlistentry>
</parmlist>

<bridgehead renderas="sect2">Events</>
<para>none</para>

<bridgehead renderas="sect2">Ports</>
<variablelist>
  <varlistentry><term><portname>in:'a</></term>
  <listitem><para>The input port.</para></listitem>
  </varlistentry>

  <varlistentry><term><portname>out:'a</></term>
  <listitem><para>The output port.</para></listitem>
  </varlistentry>
</variablelist>

<bridgehead renderas="sect2">Methods and queries</>
<para>none</para>

<bridgehead renderas="sect2">Functions</>
<variablelist>
  <varlistentry>
    <term><varname>tee::connect_bus_from_tee_output</>
                              (from : <type>port ref</>,
                               to : <type>port ref</>,
                               width : <type>int</>, 
                               ratio : <type>int</>,
                               tee_out_num : <type>int</>) => <type>void</>
    </term>
    <listitem>
      <para>
        This function will connect port instances
        from[ratio*i+tee_out_num] to port to[i]; This is useful when
        fanning out a bus of signals.  For example to expand the
        following connection pattern:
        <programlisting>
        using corelib;

        instance bar:source;
        instance hole0:sink;
        instance hole1:sink;
        instance hole2:sink;
        instance fo:tee;
 
        bar.out ->int fo.in;
        fo.out[0] -> hole0.in;
        fo.out[1] -> hole1.in;
        fo.out[2] -> hole2.in;
        </programlisting>

        to a five wide bus one would write:
        <programlisting>
        using corelib;

        instance bar:source;
        instance hole0:sink;
        instance hole1:sink;
        instance hole2:sink;
        instance fo:tee;
 
        LSS_connect_bus(bar.out,fo.in,5,int);
        tee::connect_bus_from_tee_output(fo.out,hole0.in,5,3,0);
        tee::connect_bus_from_tee_output(fo.out,hole1.in,5,3,1);
        tee::connect_bus_from_tee_output(fo.out,hole2.in,5,3,2);
        </programlisting>
        ratio is the input bus width versus the output of
        output busses, tee_out_num is which output set to connect
        to. (this number should be between 0 and ratio-1).
      </para>
    </listitem>
  </varlistentry>
</variablelist>

</sect1>

         
