<!--<!DOCTYPE chapter PUBLIC "-//Liberty ARG//DTD DocBook V4.1-Based Extension V1.0//EN">-->
<sect1><title>The <modulename>reducer</modulename> Module</>

<bridgehead renderas="sect2">Short Description</>
  <para>
   "Reduces" several input port instances into a single output port instance
   based upon a user point.
  </para>


<bridgehead renderas="sect2">Detailed Description</>
  <para>
  This module computes the value of an output port instance based upon
  several instances of the input port.  This allows "reduction" across
  port instances.
  </para>

  <para>
  Behavior of this module is sliced with a "fan-in" effect; that is,
  if the ratio of input port instances to output port instances is n,
  the input port instances numbered i*n to i*n-1 can only affect the
  output port instance numbered i, and vice-versa.  The ratio must be
  an integer.
  </para>

  <para>
  The input and output port types need not be the same; thus this
  module can be used to perform type conversions.
  </para>

<bridgehead renderas="sect2">Types</>
  <para>none</para>

<bridgehead renderas="sect2">Parameters</>
  <parmlist>
  <parmlistentry>
  <funcsynopsis>
    <funcprototype>
      <funcdef>void <function>reduce</function></funcdef>
      <paramdef>LSE_signal_t <parameter>in_statusp</>[]</paramdef>
      <paramdef>LSE_dynid_t <parameter>in_idp</>[]</paramdef>
      <paramdef>LSE_port_type(in) <parameter>in_datap</>[]</paramdef>
      <paramdef>LSE_signal_t *<parameter>out_statusp</></paramdef>
      <paramdef>LSE_dynid_t *<parameter>out_idp</></paramdef>
      <paramdef>LSE_port_type(out) *<parameter>out_datap</></paramdef>
      <paramdef>int <parameter>porti</></paramdef>
    </funcprototype>
  </funcsynopsis>  
  <listitem>
  <para>
  This user point determines how to compute the outputs from the
  inputs.  It is called during the timestep.
  </para>
  <para>
  The user function is passed the input port status (data and ack, but
  enable only if <modparam>reduce_calculates_enable</> is
  <constant>TRUE</>), ids, and data for the input port instances in
  the slice.  It is also passed the current output port status (data
  and enable, but not ack) in *out_statusp.  For the output port, the
  new signal values should be placed in *out_statusp, the new dynamic
  id in *out_idp, and the new data in *out_datap.</para>

  <para>There is no default value for this parameter.</para>

  </listitem>
  </parmlistentry>

  <parmlistentry>
    <term><modparam>wait_for_data</>:<type>bool</></term> 
    <listitem>
    <para>
     Determines if all input data signals for a slice must be
     something other than <constant>LSE_signal_unknown</> before the
     <modparam>reduce</> user point is called.
     </para>
     
     <para>
     When <constant>TRUE</>, the <modparam>reduce</> user point is only
     called when all the data input signals for slice porti of the
     output port have data signal values that are known, otherwise the
     <modparam>reduce</> user point is called as individual signals
     become available.
     </para>
     
     <para>
     The default value for this parameter is <constant>TRUE</>.
     </para>
     </listitem>
  </parmlistentry>

    <parmlistentry>
      <term><modparam>propagate_nothing</>:<type>bool</></term>
      <listitem>
      <para>
      When <constant>TRUE</>, send nothing on an output slice if all
      the inputs to that slice have nothing.
      </para>
      <para>The default value is <constant>TRUE</>.</para>
      </listitem>
    </parmlistentry>

    <parmlistentry>
      <term><modparam>reduce_calculates_enable</>:<type>bool</type></term>
      <listitem>
      <para>
        When <constant>FALSE</>, the output enables for output port instances
        numbered porti are computing by anding or oring the enable
        signals of input port instances in the slice.  The enable signals
        from the input port instances are not passed to the reduce function.
      </para>
      <para>
        When <constant>TRUE</>, the <modparam>reduce</> function MUST
        explicitly set the enable signal if it is called.  The enable
        signals from the input port instances are passed to the
        <modparam>reduce</> function.
      </para>
      <para>The default value is <constant>FALSE</>.</para>
      </listitem>
    </parmlistentry>

    <parmlistentry>
      <term><modparam>reduce_enable_with_OR</>:<type>bool</type></term>
      <listitem>
      <para>
        When <constant>FALSE</> (and
        <modparam>reduce_calculates_enable</> is <constant>FALSE</>),
        the output enables for output port instances numbered porti
        are computing by and'ing the enable signals of input port
        instances in the slice.
      </para>
      <para>
        When <constant>TRUE</> (and
        <modparam>reduce_calculates_enable</> is <constant>FALSE</>),
        the output enables for output port instances numbered porti
        are computing by or'ing the enable signals of input port
        instances in the slice.
      </para>
      <para>The default value is <constant>FALSE</>.  This parameter
      is runtimeable.</para>
      </listitem>
    </parmlistentry>

  </parmlist>

<bridgehead renderas="sect2">Events</>
<para>none</para>

<bridgehead renderas="sect2">Ports</>
<variablelist>
  <varlistentry><term><portname>in:'a</></term>
  <listitem><para>The input port.</para></listitem>
  </varlistentry>

  <varlistentry><term><portname>out:'b</></term>
  <listitem><para>The output port.</para></listitem>
  </varlistentry>
</variablelist>

<bridgehead renderas="sect2">Modules and queries</>
<para>none</para>

<bridgehead renderas="sect2">Functions</>
<para>none</para>

</sect1>
   

   




