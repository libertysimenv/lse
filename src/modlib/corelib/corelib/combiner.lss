/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * A combiner unit module definition file  
 *
 * Authors: Neil A. Vachharajani <nvachhar@cs.princeton.edu>
 *
 * The combiner module provides a mechanism to time arrivals of
 * multiple signals and generate a single output based on the input.
 * The ACKs will flow backwards and the data and enable will be passed
 * to the user to combined.
 *
 * TODO: Add port dependence annotations
 */

module combiner {
  tar_file = "corelib/combiner.clm";
  phase_start = FALSE;
  phase = TRUE;
  phase_end = FALSE;
  reactive = TRUE;

  internal parameter inputs : string[];
  internal parameter outputs : string[];
  local parameter width : int;

  parameter wait_for_data = TRUE : boolean;
  runtimeable parameter require_all_data_present = FALSE : boolean;
  parameter propagate_nothing = TRUE : boolean;
  parameter NACK_nothing = FALSE : boolean;
  parameter combine_calculates_enable = FALSE : boolean;
  parameter combine_uses_acks = FALSE : boolean;
  parameter combine_uses_enable = TRUE : boolean;

  outputs[0] = "out";

  var linputs, loutputs : literal[];
  linputs=inputs;
  loutputs=outputs;

  if(inputs.size == 0) {
    punt("You must have at least one input to the combiner");
  }

  if(outputs.size == 0) {
    punt("You must have at least one output from the combiner");
  }

  if(propagate_nothing == TRUE &&
     wait_for_data == FALSE) {
    punt("If propagate_nothing is TRUE then wait_for_data must be TRUE");
  }

  var i : int;
  var in : port ref[inputs.size];
  var check_width : int;
  var bad_width = FALSE : boolean;

  /* Create input ports */
  for(i = 0; i < inputs.size; i++) {
    in[i] = new inport(inputs[i], *);
    in[i].handler = FALSE;
    in[i].independent = FALSE;
    if(i == 0) {
      check_width = in[0].width;
    } elsif(in[i].width != check_width) {
	bad_width = TRUE;
    }
  }

  /* Create the output port */
  var out : port ref[outputs.size];
  for(i = 0; i < outputs.size; i++) {
    out[i] = new outport(outputs[i], *);
    out[i].handler = FALSE;
    out[i].independent = FALSE;
    if(out[i].width != check_width) {
      bad_width = TRUE;
    }
  }

  if (bad_width) {
    var s = "" : string;
    for (i=0; i < inputs.size; i++) {
      s = s + "\t" + inputs[i] + ".width=" + <<<${in[i].width}>>> + "\n";
    }
    for (i=0; i < outputs.size; i++) {
      s = s + "\t" + outputs[i] + ".width=" + <<<${out[i].width}>>> + "\n";
    }
    punt("All port widths must match better\n" + s);
  }

  width = check_width;

  /******************************************************/
  /* Calculate the argument list for the user functions */
  /******************************************************/
  var arglist = "int porti" : string;
  var arg : string;

  for(i = 0; i < outputs.size; i++) {
    arg = <<< LSE_signal_t *${loutputs[i]}_status, 
  	      LSE_dynid_t *${loutputs[i]}_id,
	      LSE_port_type(${loutputs[i]}) *${loutputs[i]}_data >>>;
    arglist += ", " + arg;
  }
  for(i = 0; i < inputs.size; i++) {
    arg = <<< LSE_signal_t ${linputs[i]}_status, 
  	      LSE_dynid_t ${linputs[i]}_id,
	      LSE_port_type(${linputs[i]}) *${linputs[i]}_data >>>;
    arglist += ", " + arg;
  }

  /*****************************/
  /* Define the user functions */
  /*****************************/
  parameter combine : userpoint(arglist => <<<void>>>);

  /****************************************************/
  /* Generate the string which is the tar file script */
  /****************************************************/
  var script_cmd = <<<${inputs.size}>>> : string;
  for(i = 0; i < inputs.size; i++) {
    script_cmd += " " + inputs[i];
  }
  script_cmd += <<< ${outputs.size}>>>;
  for(i = 0; i < outputs.size; i++) {
    script_cmd += " " + outputs[i];
  }

  local parameter script_string = script_cmd : literal;

  /****************************************************/
  /* calculate the port dataflow                      */
  /****************************************************/

  var j: int;
  var pdf : string;

  pdf = "[('*','*','0'),('USERPOINT(combine)','*','0'),('USERPOINT(combine)','*.data','1'),";
  for (i=0 ;i < inputs.size; i++) {

    if (NACK_nothing) {
      for (j=0 ; j < inputs.size; j++) {
	/* this one is rather strange... actually all input signals to a 
         * slice  affect acks when NACK_nothing is TRUE
	 * because the ack depends upon the *output* data...
         */
	pdf += ( "('" + inputs[j] + ".data','" + inputs[i] + 
		".ack', 'isporti==osporti')," );
	if (combine_uses_enable) {
		pdf += ( "('" + inputs[j] + ".en','" + inputs[i] + 
			".ack', 'isporti==osporti')," );
	}
	pdf += ( "('USERPOINT(combine)','*.ack','1')," );
      }
    }

    for (j=0 ; j < outputs.size; j++) {
	pdf += ( "('" + outputs[j] + ".ack','" + inputs[i] + 
		".ack', 'isporti==osporti')," );

	pdf += ( "('" + inputs[i] + ".data','" + outputs[j] + 
		".data', 'isporti==osporti')," );
	if (combine_uses_enable) {
		pdf += ( "('" + inputs[i] + ".en','" + outputs[j] + 
			".data', 'isporti==osporti')," );
	}

	pdf += ( "('" + inputs[i] + ".en','" + outputs[j] + 
		".en', 'isporti==osporti')," );

	if (combine_calculates_enable || propagate_nothing) {
	  pdf += ( "('" + inputs[i] + ".data','" + outputs[j] + 
		".en', 'isporti==osporti')," );
	  pdf += ( "('USERPOINT(combine)','*.en','1')," );
	}
    }
  }
  if (combine_uses_acks) {
    for (j=0; j < outputs.size; j++) {
	pdf += ( "('" + outputs[j] + ".ack','" + outputs[j] + 
		".data', 'isporti==osporti')," );
        if (combine_calculates_enable) {
	  pdf += ( "('" + outputs[j] + ".ack','" + outputs[j] + 
		".en', 'isporti==osporti')," );
        }
    }
  }

  pdf += "]";

  port_dataflow = pdf;

};
