/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * demux channel model definition.
 *
 * Authors: Neil Vachharajani <nvachhar@cs.princeton.edu>
 *
 * This simple module calls a user function to determine to which
 * output to propagate data/enable forward.  ACK gets propagated back 
 * to the corresponding input.
 *
 * The user function is called choose_logic; it must return an integer
 * corresponding to the correct output destination or -1 if it doesn't know
 * yet.
 *
 */

subpackage demux {
  typedef ack_style_t : enum {nack, ack, and_acks, or_acks};
}

module demux {
  using corelib.demux;

    phase_start=TRUE;
    phase=FALSE;
    phase_end=FALSE;
    reactive=TRUE;
    port_dataflow = <<<[
                   ('*','*', '0'),
                   # where data and enable can fan out to
	           ('in.data','out.data',
		           'isporti==(osporti/(osport.width/isport.width))'),
	           ('in.en'  ,'out.en', 
		           'isporti==(osporti/(osport.width/isport.width))'),
                   # data controls fanout of enable
	           ('in.data','out.en',
		           'isporti==(osporti/(osport.width/isport.width))'),
                   # data also controls ack
	           ('in.data','in.ack','isporti==osporti'),
                   # fan in for acks
	           ('out.ack','in.ack',
		           'osporti==(isporti/(isport.width/osport.width))'),
                  ]>>>;

    tar_file="corelib/demux.clm";

    export corelib.demux::ack_style_t as ack_style_t;

    parameter choose_logic : userpoint(<<<int porti, LSE_dynid_t id, 
                                          LSE_port_type(in) *data>>> => 
                                       <<<int>>>);

    parameter ack_for_nothing_style = corelib.demux::nack 
      : ack_style_t;

    inport in:'datatype;
    in.handler=TRUE;

    outport out:'datatype;
    out.handler=TRUE;

  if(in.width == 0 && out.width != 0) {
    punt("in port must be connected");
  }

  if(in.width != 0 && (out.width % in.width) != 0) {
    punt("The out to in port width ratio of a tee must be an integer" +
         <<< out.width=${out.width}, in.width=${in.width}>>>);
  }

};
