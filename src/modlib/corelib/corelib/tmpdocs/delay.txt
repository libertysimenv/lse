Short Description:

This module delays its input by at least one cycle.

Detailed Description:

Instance of this module takes enabled data on an input port instance,
in[i], and store the data, outputting it each cycle thereafter on port
instance out[i], until an ack is received on out[i].  The enable
signal on port out[i] is equal to the ack signal in that cycle.

If the pass_acks_when_full parameter is TRUE (the default), then in[i]
will get the value of the ack signal on out[i].  Otherwise, in[i] is
acked when the delay element is not holding any data to be output on
port out[i].

The delay element starts of storing no data unless there is an initial_state
function.  See below for details.

The input and output ports must have the same type.  The input and
output ports must have the same width.

Types:

none

Parameters:

pass_acks_when_full:  Determines when space is freed in the delay element.

  pass_acks_when_full:bool, default value = TRUE

initial_state: Allows setting of the initial state of the delay element.

  initial_state :  userpoint(<<<int porti, LSE_dynid_t *init_id, 
                                LSE_port_type(out) *init_value>>> => 
                             <<<boolean>>>), 
  default value = <<< return FALSE; >>>

  This function should create a dynamic identifier and output data for
  the initial value of the delay element (for output on port instance
  out[porti]) and return a pointer to the id in init_id and store the
  initial value in the location pointed to by init_value.  If the
  function returns FALSE, the delay element slice porti starts out
  empty.  If it returns TRUE, the values in init_value and init_id are
  stored in the delay element and output on port out[porti].

Functions:

delay::init(value:string) => string

  This function returns a user point that will set the initial value
  of the delay element for all port instances to the value in the
  value argument.  The string in the value argument is treated as a
  literal of the type of the input and output ports.



  



