Short Description:

Instance of the tee module fan out an input signal to multiple output
signals

Detailed Description:

An instance of the tee module fans out input on its in port to
multiple output port instances on the out port.  If there are m
connections on the input port, and n output connections, the input
data and enable signals on port in[i] are forwarded to out[(n/m)*i+j]
for j=1..n/m.  (n must be evenly divisible by m).

Instances also propagate ack signals on the out port back to the in
ports.  If the control_flow_style parameter is set to tee::and_acks,
the ack signals from port instances out[(n/m)*i+j], j=1..n/m, are
anded together and the result is forwarded to input port in[i] (with
LSE_signal_ack being 1 and LSE_signal_nack being 0).  For any ack
signal on out[(n/m)*i+j], if there is an unknown ack value but a known
ack signal is LSE_signal_nack, then the in[i]'s ack signal is set to
LSE_signal_nack.  If the control_flow_style parameter is set to
tee::or_acks, the ack signal on input port in[i] is computed by oring
the ack signals on out[(n/m)*i+j] in a similar fashion, with a known
ack signal with value LSE_signal_ack forcing in[i]'s ack signal to be
LSE_signal_ack.  The default setting is to and ack signals.

Types:

tee::control_flow_style_t:enum {and_acks, or_acks};

Parameters:

control_flow_style: Determines whether acks are anded or ored to
generate input ack

   control_flow_style:control_flow_style_t, default =
   tee::and_acks

Support Functions:

tee::connect_bus_from_tee_output(from : port ref,
                                to : port ref,
                                width : port ref, 
                                ratio : int,
                                tee_out_num : int) => void

        This function will connect port instances
        from[ratio*i+tee_out_num to port to[i]; This is useful when
        fanning out a bus of signals.  For example to expand the
        following connection pattern:

        using corelib;

        instance bar:source;
        instance hole0:sink;
        instance hole1:sink;
        instance hole2:sink;
        instance fo:tee;
 
        bar.out ->:int fo.in;
        fo.out[0] -> hole0.in;
        fo.out[1] -> hole1.in;
        fo.out[2] -> hole2.in;
        

        to a five wide bus one would write:

        using corelib;

        instance bar:source;
        instance hole0:sink;
        instance hole1:sink;
        instance hole2:sink;
        instance fo:tee;
 
        LSS_connect_bus(bar.out,fo.in,5,int);
        tee::connect_bus_from_tee_output(fo.out,hole0.in,5,3,0);
        tee::connect_bus_from_tee_output(fo.out,hole1.in,5,3,1);
        tee::connect_bus_from_tee_output(fo.out,hole2.in,5,3,2);
        
        ratio is the number of input busses versus the number of
        output busses, tee_out_num is which output set to connect
        to. (this number should be between 0 and ratio-1).

         
