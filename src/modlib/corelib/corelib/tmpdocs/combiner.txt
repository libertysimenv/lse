Short Description:

Flexible module whose instances can be parameterized to combine
multiple inputs into multiple outputs based on a number of user points
and parameters.

Detailed Description:

Instances of this module have input ports with the names given in
elements of the inputs parameter.  Outputs are determined by the
outputs parameter.  By default, the combine user point is called for
every port instance, i, to determine how to set the outputs.  The time
when the user point is called, as well as the default behavior when it
is not called is subject to the setting of several parameters.  See
the parameters section below.

Behavior of this module is purely sliced, that is, input port
instances number i can only affect output port instances numbered i.
Thus, the input port widths must match the output port widths.

This is a very general module and can be used to synchronize inputs,
behave as an ALU, etc.  Note, however that the input and output ports
have no type constraints.  Please wrap this module in another module
that enforces the type constraints you desire.

Types:

none

Parameters:

combine: User point that determines how to compute the outputs from
         the inputs.

  The user function has a set of arguments, portname_data,
  portname_status, and portname_id for every input and output port,
  and an argument, porti that determines the slice being worked
  on. For input ports, the portname_status variable gives the input
  status of portname[porti], portname_id is the dynamic identifier
  received, and portname_data is a pointer to the data.

  For output ports, portname_status is a pointer to the output status
  which contains the current output ack status and must be set with
  the proper data and enable status (i.e. LSE_signal_something,
  LSE_signal_enabled, etc.).  portname_id is a pointer to a
  LSE_dynid_t which should be set to the output dynamic identifier.
  portname_data is a pointer to a cell in which the output data should
  be stored.

wait_for_data: Determines if all data signals on input port instances
   numbered porti must be something other than LSE_signal_unknown
   before the combine user point is called.

   wait_for_data:bool, default value = TRUE

   When TRUE, the combine function is only called when all the inputs
   in slice porti have data signal values that are known, otherwise
   all outputs on slice porti get LSE_signal_nothing,
   LSE_signal_disabled.

require_all_data_present: Determines if all data signals on input port
   instances numbered porti must be LSE_signal_something before
   the combine user point is called.

   require_all_data_present:bool, default value = TRUE

   When TRUE, the combine function is only called when all the inputs
   in slice porti have data signal values that are
   LSE_signal_something, otherwise all outputs on slice porti get
   LSE_signal_nothing, LSE_signal_disabled.

propagate_nothing: Send nothing on the output if any input has nothing.
   
   propagate_nothing:bool, default value = TRUE

   Sends nothing on all outputs as soon as any input has a nothing value.

NACK_nothing: determines whether nothings on the inputs are sent
              LSE_signal_ack or LSE_signal_nack.

  NACK_nothing:bool, default value = TRUE

combine_calculates_enable: Determines how enable is computed

  combine_calculates_enable:bool, default value = FALSE

  When FALSE, the output enables for output port instances numbered
  porti are computing by anding the enable signals of input port
  instances numbered porti.

  When TRUE, the combine function MUST explicitly set the enable
  signal if it is called.

Functions:

none

   

   




