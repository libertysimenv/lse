/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Module definition for a sink
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This module accepts inputs and throws them away; to make this even
 * faster, the default control function says to not send them on and
 * the input is marked as independent, so the phase function is never
 * even called (and doesn't exist).
 *
 */

module sink {
  tar_file = "corelib/sink.clm";
  phase_start=TRUE;
  reactive=FALSE;
  phase=FALSE;
  phase_end=TRUE;
  port_dataflow = <<<[('*','*',  '0'),
		      ('USERPOINT(sink_func)','*','0') ]>>>;

  inport in:'datatype;
  in.independent=TRUE;

  parameter sink_func = <<< ; >>>
    : userpoint(<<<LSE_signal_t status, LSE_dynid_t id, 
                   LSE_port_type(in)* data, int porti>>> => <<<void>>>);

  emits event SUNK_DATA {
    porti : <<<int>>>;
    status : <<<LSE_signal_t>>>;
    id : <<<LSE_dynid_t>>>;
    datap : <<<${'datatype} *>>>;
  };

};


