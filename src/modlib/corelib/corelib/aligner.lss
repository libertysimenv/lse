/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * aligner channel model definition.
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * The aligner is a special case of the arbiter that has been optimized for
 * simply "squashing" out data signals of value nothing.  The normal use
 * for it is between a filter and an mqueue when you want the mqueue to
 * hold things up after it becomes full, but not hold things off if the
 * filter has gotten rid of some earlier ones so that the mqueue will not
 * really become full.  Enables are not set until after data routing is
 * known; likewise acks are not propagated backwards until after data 
 * routing is known, but the enable and acks can be developed in a 
 * sequential fashion (unlike the full arbiter).
 *
 * However, from a dependency point of view, we still have to say that
 * output port instances are dependent upon all input port instances.  The
 * problem is that even though output port 1 can never get input port 0's
 * value, it does need to know that it was not nothing to determine whether
 * it is going to get the value from input 1, 2, 3, etc.
 *
 */

module aligner {
  tar_file = "corelib/aligner.clm";
  phase = TRUE;
  phase_end = FALSE;
  reactive = TRUE;
  
  // the dataflow is a bit tricky; an output port can get data or enable
  // only from the input port instances with higher or same numbers, but
  // still depends upon all input data instances because the lower ones 
  // affect the routing of the upper ones.
  port_dataflow = <<<[
	           ('*','*',  '0'),
	           ('in.data','out.data',  '1'), # because of routing
	           ('in.data','out.en',  '1'), # because of routing
	           ('in.en','out.en',  'isporti>=osporti'),
	           ('in.data','in.ack',  'isporti<osporti'),
	           ('out.ack','in.ack',  'isporti<=osporti') ]>>>;

  inport in:'datatype;
  outport out:'datatype;

  phase_start = (out.width > in.width);

  in.independent = FALSE;
  in.handler = FALSE;
  out.independent = FALSE;
  out.handler = FALSE;

  if (out.width == 0) {
    punt("out width must be non-zero");
  }
};
