/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * A serializer unit module definition file  
 *
 * Authors: Neil A. Vachharajani <nvachhar@cs.princeton.edu>
 *
 * The serializer module provides a mechanism to make sure 
 * inputs arrive in order or don't arrive at all.  This can 
 * generally be implemented by a control function, but since
 * the behavior is common enough, this widget has been written.
 *
 * Data, enable, and ACK can be serialized independently.
 */
module serializer {
    tar_file = "corelib/serializer.clm";
    phase_start=FALSE;
    phase=FALSE;
    phase_end=FALSE;
    reactive=TRUE;

    parameter serialize_data=TRUE:boolean;
    parameter serialize_enable=TRUE:boolean;
    parameter serialize_ack=TRUE:boolean;

    var datacmp = serialize_data ? "<=" : "==" : literal;
    var encmp = serialize_enable ? "<=" : "==" : literal;
    var ackcmp = serialize_ack ? "<=" : "==" : literal;

    port_dataflow = <<<[
                   ('*', '*', '0'),
                   ('in.data', 'out.data', 'isporti ${datacmp} osporti'),
                   ('in.en', 'out.en', 'isporti ${encmp} osporti'),
                   ('out.ack', 'in.ack', 'isporti ${ackcmp} osporti'),
                   ]>>>;

    inport in:'datatype;
    in.handler=TRUE;
    in.independent=FALSE;

    outport out:'datatype;
    out.handler=TRUE;
    out.independent=FALSE;


    if(in.width == 0) {
      punt("in width must be non-zero");
    }
    if(in.width != out.width) {
      punt("in and out port widths are not the same");
    }
};
