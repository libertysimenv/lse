/*  -*-c-*-
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * This module routes inputs to outputs
 *
 * Authors: Ram Rangan <ram@cs.princeton.edu>
 *          Manish Vachharajani <manishv@ee.princeton.edu>
 *          David A. Penry <dpenry@cs.princeton.edu>
 *
 * See router.lss for a detailed description
 *
 */

class router : public LSE_module {

#define INVALID_VAL -1
#define NOTHING_VAL -2

/* Additional checks we couldn't do before... */
#LSE if LSE_nameequal(LSE_port_type(route_info),int)

#if (LSE_port_width(route_info)!=LSE_port_width(out))
#  error "Route_info-Out port width mismatch"
# endif

#LSE else

#if (LSE_port_width(route_info)>1)
#  error "Route_info too large"
# endif

#LSE endif

private:

/* The routing table is a dest-to-src map */
GLOBDEF(int, routing_table[LSE_port_width(out)+1]);

/* how far we know so far... */
GLOBDEF(int, route_info_known);


public:

boolean
FUNC(init, void)
{
  return TRUE;
}

void
FUNC(finish,void)
{
  /* Put module cleanup code here */
}

void
FUNC(phase_start, void)
{
  int i;

  GLOB(route_info_known) = 0;

  /* call user function to initialize the routing table with the default
     routing function
  */
  LSE_userpoint_invoke(default_routing_function, LSE_port_width(in),
		  LSE_port_width(out), GLOB(routing_table),
		  INVALID_VAL,NOTHING_VAL);
}

private:
inline void
FUNC(compute_and_set_ack, void)
{
  int i;
  LSE_signal_t inacks[LSE_port_width(in)+1];
  boolean saw[LSE_port_width(in)+1];
  LSE_signal_t sig;

  /* initialize status */

  for (i=0;i<LSE_port_width(in);i++) {
    inacks[i] = LSE_signal_ack;
    saw[i] = FALSE;
  }

  /* check each output port */
  for (i=0;i<LSE_port_width(out);i++) {
    int route = GLOB(routing_table)[i];
    if (route >= 0 && route < LSE_port_width(in)) {
      saw[route] = 1;
      sig = LSE_port_get(out[i].ack);
      inacks[route] = LSE_signal_and(inacks[route],sig);
    }
  }

  /* send acks to the input ports */

  for (i=0;i<LSE_port_width(in);i++) {
    if (saw[i]) 
      LSE_port_set(in[i].ack,inacks[i]);
    else
      LSE_port_set(in[i].ack,LSE_signal_nack);
  }
}

/* must do all because input could fanout to anywhere and we don't keep
 *  a forward routing table
 */

inline void 
FUNC(do_forward, void) {
  int porti;

  for(porti=0;porti<LSE_port_width(out);porti++) {
    int srci = GLOB(routing_table)[porti];
    
    if (srci >= 0 && srci < LSE_port_width(in)) {
      LSE_dynid_t sid;
      LSE_port_type(in) *sdata;
      LSE_signal_t sstatus = LSE_port_get(in[srci],&sid,&sdata);
      LSE_port_set(out[porti],sstatus,sid,sdata);
    } else if (srci == NOTHING_VAL) {
      LSE_port_set(out[porti],LSE_signal_nothing|LSE_signal_enabled,
		   NULL,NULL);
    } else if (srci != INVALID_VAL) {
      LSE_report_warn("Attempting to route from non-existent in "
		      "port [%d] to out [%d]",srci, porti);
      LSE_port_set(out[porti],LSE_signal_nothing|LSE_signal_disabled,
		   NULL,NULL);
    } else {
      LSE_report_warn("Illegal route info value for out port [%d], value=%d",
		      porti, srci);
      LSE_port_set(out[porti],LSE_signal_nothing|LSE_signal_enabled,
		   NULL,NULL);
    }
  }
}

public:
void
FUNC(phase, void) 
{
  int porti;
  LSE_signal_t status,astatus;
  LSE_port_type(route_info) *datap;

#LSE if LSE_nameequal(LSE_port_type(route_info),int)

  for (porti=GLOB(route_info_known);porti<LSE_port_width(route_info);porti++) {
    status = LSE_port_get(route_info[porti],NULL,&datap);
    if (!LSE_signal_data_known(status)) break;
    GLOB(route_info_known) = porti+1;
    if (LSE_signal_data_present(status))
      GLOB(routing_table)[porti] = *datap;
  }

  if (GLOB(route_info_known)==LSE_port_width(route_info)) {
    FUNC(do_forward);
    FUNC(compute_and_set_ack);
  }
  
  for (porti=0;porti<LSE_port_width(out);porti++) {
    astatus = LSE_port_get(out[porti].ack);
    LSE_port_set(route_info[porti].ack,astatus);
  }

#LSE else

  status = LSE_port_get(route_info[0],NULL,&datap);
  if (LSE_signal_data_present(status) && !GLOB(route_info_known))
    memcpy(&GLOB(routing_table),datap->elements,
	   sizeof(int)*LSE_port_width(out));
  
  if (LSE_signal_data_known(status)) {
    FUNC(do_forward);
    FUNC(compute_and_set_ack);    
    GLOB(route_info_known) = 1;
  }
  
  astatus = LSE_signal_ack;
  for (porti=0;porti<LSE_port_width(out);porti++) {
    status = LSE_port_get(out[porti].ack);
    astatus = LSE_signal_and(astatus, status);
  }
  LSE_port_set(route_info[0].ack,astatus);

#LSE endif

}

}; /* class router */

