/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * selector channel model definition.
 *
 * Authors: Neil Vachharajani <nvachhar@cs.princeton.edu>
 *          David A. Penry <dpenry@cs.princeton.edu>
 *
 *
 * The selector is a special case of a arbiter/router pair.  It basically 
 * takes its input and performs the aligning behavior on it and then 
 * routes the result and routes them to outputs that have ACKed.  
 *
 */
module selector {
   tar_file="corelib/selector.clm";
   phase_start=FALSE;
   phase=TRUE;
   phase_end=FALSE;
   reactive=TRUE;
   port_dataflow = <<<[
                   ('*','*', '0'),
                   ('in.data','out.data', '1'),
                   ('out.ack','out.data', 'isporti<=osporti'),
                   ('in.data','out.en', '1'),
                   ('in.en','out.en', '1'),
                   ('out.ack','out.en', 'isporti<=osporti'),
                   ('in.data','in.ack', 'isporti<=osporti'),
                   ('out.ack','in.ack', '1')
                  ]>>>;

   inport in:'datatype;
   in.handler = FALSE;
   in.independent = FALSE;

   outport out:'datatype;
   out.handler = FALSE;
   out.independent = FALSE;
};
