/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Implementation of checkpoint file access functions
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file implements the checkpoint library... it's fun!
 *
 */

#include <LSE_chkpt.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <sys/stat.h>
#include <stdlib.h>

#include "asn1lib.h"

namespace LSE_chkpt {

  static inline void
  encode_fixed_len(unsigned char *buf, unsigned int ls, unsigned int len) {
    unsigned int i;
    for (i=ls;i>0;) {
      *(buf++) = (unsigned char)(len >> ((--i)*8));
    }
    return;
  }

  static inline unsigned int
  decode_fixed_len(unsigned char *buf, unsigned int ls) {
    unsigned int i,r=0;
    for (i=0;i<ls;i++) {
      r = (r<<8) | buf[i];
    }
    return r;
  }

  static unsigned char terminator[] = { 0, 0 };

  /************** file handling *****************************/

  file_t::file_t() {
    fp = 0;
    isRead = compressInited = false;
    compBuf = uncompBuf = 0;
  }

  file_t::file_t(const char *filename, const char *mode) {
    fp = 0;
    isRead = compressInited = false;
    compBuf = uncompBuf = 0;
    this->open(filename, mode);
  }

  file_t::~file_t() {
    this->close();
  }

  error_t 
  file_t::open(const char *filename, const char *mode) {

    if (fp) this->close();

    if (!strcmp(mode,"w")) isRead = false;
    else if (!strcmp(mode,"a")) isRead = false;
    else if (!strcmp(mode,"r")) isRead = true;
    else return error_BadArgument; /* bad mode */
  
    fp = fopen(filename,mode);
    if (!fp) return error_NotFound;

    compressInfo.zalloc = Z_NULL;
    compressInfo.zfree = Z_NULL;
    compressInfo.opaque = 0;
    compressInited = false;
    isRead = isRead;
    uncompBuf = compBuf = 0;

    /* set up data structures as needed */
    if (!isRead) { /* initialize for writing */

      d.write.header = 0;
      d.write.headerState = 0;
      d.write.cpState = 0;

      d.write.compressBytes = build_octetstring(0,0,0,false);
      if (!d.write.compressBytes) {
	this->close();
	return error_OutOfMemory;
      }

    } else { /* initialize for reading */
      struct stat sbuf;
      int rval;
      d.read.header = 0;
      d.read.currCPno = 0;
      d.read.currCPpos = 0;
      d.read.beganCP = false;
      d.read.state = 0;
      d.read.sought = 0;
      d.read.highestSought = -1;

      rval = stat(filename,&sbuf);
      if (rval) {
	this->close();
	return error_IO;
      }
      d.read.length = sbuf.st_size;
    }

    return error_None;
  }

  void file_t::close() {

    if (fp) fclose(fp);
    fp = 0;
    if (isRead) {
      if (d.read.header) delete (d.read.header);
      d.read.header = 0;
    } else {
      if (d.write.header) delete (d.write.header);
      delete (d.write.compressBytes);
      d.write.header = 0;
      d.write.cpState = 0;
      d.write.compressBytes = 0;
    }

    if (compressInited) {
      if (isRead) {
	inflateEnd(&compressInfo);
      } else {
	deflateEnd(&compressInfo);
      }
    }

    if (compBuf) free(compBuf);
    if (uncompBuf) free(uncompBuf);
    compBuf = uncompBuf = 0;
  }

  inline long file_t::skip_forward(long offset) {
    long cnt = 0;
    long real = d.read.currCPno;
    
    error_t rval;
    while (offset) { /* do the skipping */
      offset--;
      if (d.read.highestSought == real-1) {
	d.read.sought = (long *)realloc(d.read.sought,
					sizeof(long)*(real+1));
	if (!d.read.sought) {
	  d.read.highestSought = -1;
	}
	else {
	  d.read.sought[real] = ftell(fp);
	  d.read.highestSought = real;
	}
      }
      rval = ::LSE_chkpt::read_data(fp, skipper, 0, 0, 0);
      if (rval) return cnt;
      cnt++;
      real++;
    }
    return cnt;
  }

  error_t file_t::seek(long offset, int whence) {
    long targetloc, currloc, lret;
    int ret;
  
    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;

    currloc = d.read.currCPno;

    switch (whence) {
    case SEEK_SET:
      targetloc = offset;
      break;
    case SEEK_CUR:
      targetloc = currloc + offset;
      break;

    case SEEK_END:
      /* need to count and figure out where we are */
      rewind(fp);
      d.read.currCPno = 0;
      currloc = skip_forward(LONG_MAX);
      targetloc = currloc + offset;
      rewind(fp);
      currloc = 0;
      d.read.currCPpos = 0;
      d.read.currCPno = 0;
      d.read.beganCP = false;
      break;

    default:
      return error_BadArgument;
    }

    if (targetloc < 0) goto had_error;

    if (targetloc <= d.read.highestSought) {
      ret = fseek(fp,d.read.sought[targetloc],SEEK_SET);
      if (ret) goto had_error;
      d.read.currCPno = targetloc;
      d.read.currCPpos = d.read.sought[targetloc];
      d.read.beganCP = false;
    } else if (targetloc == currloc) { /* start of current checkpoint */
      ret = fseek(fp,d.read.currCPpos,SEEK_SET);
      if (ret) goto had_error;
      d.read.beganCP = false;
    } else if (targetloc < currloc) { /* move backwards -- expensive */
      if (d.read.highestSought > 0) {
	ret = fseek(fp,
		    d.read.sought[d.read.highestSought],
		    SEEK_SET);
	if (ret) goto had_error;
	d.read.currCPno = d.read.highestSought;
      } else {
	rewind(fp);
	d.read.currCPno = 0;
      }
      lret = skip_forward(targetloc);
      if (lret != targetloc) goto had_error;
      d.read.currCPno = targetloc;
      d.read.currCPpos = ftell(fp);
      d.read.beganCP = false;
    } else if (currloc < 0) { /* error condition */
      if (d.read.highestSought > 0) {
	ret = fseek(fp,
		    d.read.sought[d.read.highestSought],
		    SEEK_SET);
	if (ret) goto had_error;
	d.read.currCPno = d.read.highestSought;
      } else {
	rewind(fp);
	d.read.currCPno = 0;
      }
      lret = skip_forward(targetloc);
      if (lret != targetloc) goto had_error;
      d.read.currCPno = targetloc;
      d.read.currCPpos = ftell(fp);
      d.read.beganCP = false;
    } else { /* move forward */
      if (d.read.beganCP) {
	ret = fseek(fp,d.read.nextCPpos,SEEK_SET);
	if (ret) goto had_error;
	currloc++;
	d.read.currCPno = currloc;
      } else {
	ret = fseek(fp,d.read.currCPpos,SEEK_SET);
	if (ret) goto had_error;
      }
      lret = skip_forward(targetloc-currloc);
      if (lret != (targetloc - currloc)) goto had_error;
      d.read.currCPno = targetloc;
      d.read.currCPpos = ftell(fp);
      d.read.beganCP = false;
    }
    return error_None;  

  had_error:
    d.read.currCPno = -1;
    d.read.beganCP = false;
    return error_IO;
  }

  bool file_t::more_checkpoints() {
    /* Unfortunately we cannot just use feof because fseek to the end of file
     * does not set feof!
     */
    long currpos;
    if (!fp) return false;
    if (!isRead) return false;

    if (d.read.beganCP) {
      if (d.read.nextCPpos == d.read.length) return false;
    } else {
      currpos = ftell(fp);
      if (currpos == d.read.length) return false;
    }
    return true;
  }

  /***************** header write functions ****************/

  error_t file_t::begin_header_write(const char *name) {
    data_t *tmp;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;

    if (d.write.header) delete (d.write.header);

    d.write.headerState = 0;

    /* start ChkptFileHeader */
    d.write.header = build_sequence(0);
    d.write.header->change_tag(TAG_CHKPTFILEHEADER,
			       CLASS_PRIVATE);

    if (!d.write.header) return error_OutOfMemory;

    /* id */
    tmp = build_string(d.write.header,(char *)name,true);

    /* start simParms */
    d.write.buildPtr = build_sequence(d.write.header);
    if (!tmp || !d.write.buildPtr) {
      delete (d.write.header);
      d.write.header = 0;
      return error_OutOfMemory;
    }

    /* start TOC */
    tmp = build_sequence(d.write.header);
    if (!tmp) {
      delete (d.write.header);
      d.write.header = 0;
      return error_OutOfMemory;
    }

    return error_None;
  }

  error_t 
  file_t::add_globalparm(const char *parmVal) {
    data_t *tmp;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (!d.write.header) return error_IllegalFunction;

    tmp = build_string(d.write.header->oldestChild->sibling,
		       (char *)parmVal,true);
    if (!tmp) return error_OutOfMemory;
    return error_None;
  }

  error_t file_t::add_toc(const char *entryName) {
    data_t *dp, *tmp;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (!d.write.header) return error_IllegalFunction;

    dp = d.write.header->oldestChild->sibling->sibling; /* TOC sequence */

    dp = build_sequence(dp); /* toc record */
    if (!dp) goto free_header;
    tmp = build_string(dp,(char *)entryName,true); /* segmentID */
    if (!tmp) goto free_header;
    dp = build_sequence(dp); /* parms */
    if (!dp) goto free_header;

    d.write.buildPtr = dp;
    d.write.headerState = 1;
    return error_None;

  free_header:
    delete (d.write.header);
    d.write.header = 0;
    return error_OutOfMemory;
  }

  error_t file_t::add_tocparm(const char *parmVal) {
    data_t *tmp;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (!d.write.header) return error_IllegalFunction;

    if (d.write.headerState == 1) {
      tmp = build_string(d.write.buildPtr,(char *)parmVal,
			 true);
      if (!tmp) return error_OutOfMemory;
    } else return error_IllegalFunction;
    return error_None;
  }

  error_t file_t::end_header_write() {
    error_t rval;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (!d.write.header) return error_IllegalFunction;
  
    d.write.header->update_size();
    rewind(fp);

    /* write the header */
    rval = ::LSE_chkpt::write_data(fp, d.write.header); 

    /* and free it */
    delete (d.write.header);
    d.write.headerState = 0;
    d.write.header = 0;
    return rval;
  }

  /********************* header read functions *******************/

  error_t
  file_t::begin_header_read(char **name) {
    error_t rval;
    data_t *header, *t;
    unsigned char buf[2];
    size_t rlen, used;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;

    rewind(fp);
    /* look to see that it is a header (PRIVATE 0) with a reasonable length */
    rlen = fread(buf,1,2,fp);
    if (rlen != 2 || 
	buf[0] != (CLASS_PRIVATE | CONSTRUCTED |
		   TAG_CHKPTFILEHEADER) ||
	(buf[1] == 0x80) || (buf[1] > 0x84))
      return error_FileFormat;
    rewind(fp);
    rval = ::LSE_chkpt::read_data(fp, 0, &header, 0, &used);
    if (rval) return rval;

    d.read.currCPno = 1;
    d.read.beganCP = false;
    d.read.currCPpos = ftell(fp);
    d.read.state = 0;

    header->update_size();

    if (d.read.header) delete (d.read.header);
    d.read.header = 0; /* in case of error */

    /* now double check that we can get the name */

    if (header->oldestChild && 
	header->oldestChild->formatTag == TAG_UTF8STRING &&
	header->oldestChild->tagClass == CLASS_UNIVERSAL) {
      if (name) *name = header->oldestChild->content.stringVal;
    } else goto free_and_leave;

    /* start the global parms iterator */
    t = header->oldestChild->sibling;
    if (!t || 
	t->tagClass != (CLASS_UNIVERSAL | CONSTRUCTED)
	|| t->actualTag != TAG_SEQUENCE) {
    }
    d.read.gpIter = t->oldestChild;

    /* start the toc iterator */
    t = t->sibling;
    if (!t ||
	t->tagClass != (CLASS_UNIVERSAL | CONSTRUCTED)
	|| t->actualTag != TAG_SEQUENCE) goto free_and_leave;
    d.read.tocIter = t->oldestChild;
    d.read.tocIterNo = 0;

    d.read.header = header;
    d.read.segParmIter = 0;

    return error_None;

  free_and_leave:
    delete (header);
    return error_FileFormat;

  }

  error_t
  file_t::get_globalparm(char **parmP, bool restart) {
    data_t *t;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (!d.read.header) return error_IllegalFunction;

    if (restart) {
      t = d.read.header;
      if (!t->oldestChild || !t->oldestChild->sibling) 
	return error_FileFormat;
      d.read.gpIter = t->oldestChild->sibling->oldestChild;
    }
  
    if (!d.read.gpIter) {
      if (parmP) *parmP = 0;
    } else { 
      t = d.read.gpIter;
      if (t->tagClass == CLASS_UNIVERSAL && 
	  t->actualTag == TAG_UTF8STRING) {
	if (parmP) *parmP = t->content.stringVal;
      } else return error_FileFormat;
      d.read.gpIter = t->sibling;
    }

    return error_None;
  }

  error_t
  file_t::find_toc(const char *segmentName, unsigned int *positionP,
		   data_t **plistP) {
    data_t *t,*toc;
    unsigned int tocNo;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (!d.read.header) return error_IllegalFunction;
  
    t=d.read.header;
    if (!t->oldestChild || !t->oldestChild->sibling || 
	!t->oldestChild->sibling->sibling) return error_FileFormat;

    toc=t->oldestChild->sibling->sibling; /* table of contents SEQUENCE OF */
    if (toc->tagClass != (CLASS_UNIVERSAL | CONSTRUCTED)
	|| toc->actualTag != TAG_SEQUENCE) 
      return error_FileFormat;

    toc = toc->oldestChild; /* first entry (a SEQUENCE) */
    tocNo = 0;

    while (toc) {
      if (toc->tagClass != 
	  (CLASS_UNIVERSAL | CONSTRUCTED)
	  || toc->actualTag != TAG_SEQUENCE) 
	return error_FileFormat;

      t = toc->oldestChild; /* supposedly the segment ID */
      if (!t || t->tagClass != CLASS_UNIVERSAL || 
	  t->actualTag != TAG_UTF8STRING) 
	return error_FileFormat;

      if (!strcmp(segmentName,t->content.stringVal)) {
	/* set up the iterator... */
	d.read.tocIter = toc;
	d.read.tocIterNo = tocNo;
	t = t->sibling;
	if (!t || t->tagClass != 
	    (CLASS_UNIVERSAL | CONSTRUCTED)
	    || t->actualTag != TAG_SEQUENCE) 
	  return error_FileFormat;
	d.read.segParmIter = d.read.segParmReset = 
	  t->oldestChild;
	if (positionP) *positionP = tocNo;
	if (plistP) *plistP = t;
	return error_None;
      }
      toc = toc->sibling; /* next entry */
      tocNo++;
    }

    return error_NotFound;
  }

  error_t
  file_t::get_toc(char **segmentNameP, unsigned int *positionP, bool restart) {
    data_t *t, *toc;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (!d.read.header) return error_IllegalFunction;

    if (restart) {
      t=d.read.header;
      if (!t->oldestChild || !t->oldestChild->sibling || 
	  !t->oldestChild->sibling->sibling) return error_FileFormat;

      toc=t->oldestChild->sibling->sibling; /* table of contents SEQUENCE OF */
      if (toc->tagClass != (CLASS_UNIVERSAL | 
			    CONSTRUCTED)
	  || toc->actualTag != TAG_SEQUENCE) 
	return error_FileFormat;

      d.read.tocIter = toc = toc->oldestChild;
      d.read.tocIterNo = 0;
    } else toc = d.read.tocIter;
  
    if (!toc) {
      if (segmentNameP) *segmentNameP = 0;
    } else { 

      if (toc->tagClass != (CLASS_UNIVERSAL | 
			    CONSTRUCTED)
	  || toc->actualTag != TAG_SEQUENCE) 
	return error_FileFormat;

      t = toc->oldestChild; /* supposedly the segment ID */
      if (!t || t->tagClass != CLASS_UNIVERSAL || 
	  t->actualTag != TAG_UTF8STRING) 
	return error_FileFormat;

      if (segmentNameP) *segmentNameP = t->content.stringVal;
      if (positionP) *positionP = d.read.tocIterNo;

      /* parameter list iterator */
      t = t->sibling;
      if (!t || t->tagClass != 
	  (CLASS_UNIVERSAL | CONSTRUCTED)
	  || t->actualTag != TAG_SEQUENCE) 
	return error_FileFormat;
      d.read.segParmIter = d.read.segParmReset = t->oldestChild;

      d.read.tocIter = toc->sibling;
      d.read.tocIterNo++;
    }

    return error_None;
  }

  error_t
  file_t::get_tocparm(char **parmP, bool restart) {
    data_t *t;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (!d.read.header) return error_IllegalFunction;

    if (restart) {
      t=d.read.segParmIter = d.read.segParmReset;
    } else t = d.read.segParmIter;

    if (!t) {
      if (parmP) *parmP = 0;
    } else {
      if (!t || t->tagClass != CLASS_UNIVERSAL || 
	  t->actualTag != TAG_UTF8STRING) 
	return error_FileFormat;

      if (parmP) *parmP = t->content.stringVal;
      d.read.segParmIter = t->sibling;
    }

    return error_None;
  }

  /********** checkpoint read functions ****************/

#define TBUFSIZE (1024 * 64)

  size_t
  compress_writer(void *cpFileDummy, const unsigned char *buf, size_t olen) {
    file_t *cpFile = (file_t *)cpFileDummy;
    size_t len = olen, togo = TBUFSIZE - cpFile->d.write.cpSize;
    int zval;
    error_t rval;

    while (len > togo) {
      if (togo) {
	/* fill up remainder of buffer */
	memcpy(cpFile->uncompBuf+cpFile->d.write.cpSize,buf,togo);
	len -= togo;
	buf += togo;
	cpFile->d.write.cpSize += togo;
      }
      /* put it through the grinder... */
      zval = Z_STREAM_END;
      cpFile->compressInfo.next_in = cpFile->uncompBuf;
      cpFile->compressInfo.avail_in = cpFile->d.write.cpSize;
      /* make sure we completely compress this input... */
      do {
	zval = deflate(&cpFile->compressInfo, 0);
	if (zval != Z_OK) {
	  cpFile->d.write.lastError = error_Compression;
	  return 0; /* some kind of error... */
	}
	if (!cpFile->compressInfo.avail_out) { 
	  cpFile->d.write.compressBytes->size = TBUFSIZE;
	  cpFile->d.write.compressBytes->content.ustringVal = cpFile->compBuf;
	  rval = ::LSE_chkpt::write_data(cpFile->fp,
					 cpFile->d.write.compressBytes);
	  if (rval) {
	    cpFile->d.write.lastError = error_Compression;
	    return 0;
	  }
	  cpFile->compressInfo.next_out = cpFile->compBuf;
	  cpFile->compressInfo.avail_out = TBUFSIZE;
	}
	else break;
      } while (1);
      cpFile->d.write.cpSize = 0;
      togo = TBUFSIZE;
    } 
    memcpy(cpFile->uncompBuf+cpFile->d.write.cpSize,buf,len);
    cpFile->d.write.cpSize += len;
    return olen;
  }

  error_t
  file_t::begin_checkpoint_write(uint64_t idNo, bool compressed) {
    static unsigned char cpHeader[] = { 0xe2, 0x84, 00, 00, 00, 00};
    data_t *dp;
    error_t rval;
    size_t fret;
    int zval;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (d.write.headerState) return error_IllegalFunction;
    if (d.write.cpState) return error_IllegalFunction;

    // clean up pointers
    ptrID = 0;
    ptrToID.clear();
    idToPtr.clear();

    if (!compBuf || !uncompBuf) {
      compBuf = (unsigned char *)malloc(TBUFSIZE);
      uncompBuf = (unsigned char *)malloc(TBUFSIZE);
      if (!compBuf || !uncompBuf) 
	return error_OutOfMemory;
    }
    d.write.cpSize = 0;
    d.write.cpLoc = ftell(fp);

    /* want to put out a sequence header with an over-sized length field
     * so that we can go back and fix it later...
     * hex: e2 84 00 00 00 00 // integer // 30 80 ....... contents ..... 00 00
     */
    fret=fwrite(cpHeader,1,6,fp);
    if (fret != 6) return error_IO;

    d.write.cpState = 1;

    /* idNo */
  
    dp=build_unsigned(0,idNo);
    if (!dp) return error_OutOfMemory;
    rval = ::LSE_chkpt::write_data(fp,dp);
    delete (dp);
    if (rval) return rval;

    d.write.compressCP = compressed;
    if (compressed) {
      static unsigned char segmentedOctetHeader[] = {0x24, 0x80};
      /* put out a header for a segmented octet string.  It is segmented
       * because primitive octet strings cannot have indefinite length and I
       * do not want to go back and fix it....
       */
      fret = fwrite(segmentedOctetHeader,1,2,fp);
      if (fret != 2) return error_IO;
      d.write.cpSize = 0;
      if (compressInited) {
	zval = deflateEnd(&compressInfo);
	compressInited = false;
	if (zval != Z_OK) return error_Compression;
      }
      zval = deflateInit(&compressInfo,9);
      compressInfo.next_out = compBuf;
      compressInfo.avail_out = TBUFSIZE;
      compressInited = true;
      if (zval == Z_MEM_ERROR) return error_OutOfMemory;
      else if (zval != Z_OK) return error_Compression;
    } else {
      static unsigned char sequenceHeader[] = {0x30, 0x80};
      /* indefinite length sequence for checkpoint data */
      fret = fwrite(sequenceHeader,1,2,fp);
      if (fret != 2) return error_IO;
    }
    return error_None;
  }

  error_t
  file_t::begin_segment_write(const char *segmentName) {
    data_t *np;
    size_t rlen;
    static unsigned char sequence[]={ 0x30, 0x80 };
    error_t rval;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (d.write.cpState != 1) return error_IllegalFunction;

    np = build_string(0,(char *)segmentName,true);
    if (!np) return error_OutOfMemory;

    if (d.write.compressCP) {
      rlen=compress_writer((void *)this, sequence, 2);
      if (rlen != 2) {
	delete (np);
	return d.write.lastError;
      }
      rlen = encode_data_internal(compress_writer, (void *)this, np);
      if (!rlen) {
	delete (np);
	return d.write.lastError;
      }
    } else {
      /* indefinite-length sequence; theory is that we're building a structure. 
       */
      rlen = fwrite(sequence,1,2,fp);
      if (rlen != 2) {
	delete (np);
	return error_IO;
      }
      rval = ::LSE_chkpt::write_data(fp,np);
      if (rval) {
	delete (np);
	return rval;
      }
    }
    d.write.cpState = 2;
    delete (np);
    return error_None;
  }

  error_t 
  file_t::write_to_segment(bool freeData, data_t *cpData) {
    size_t rlen;
    error_t rval;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (d.write.cpState != 2) return error_IllegalFunction;
    if (!cpData) return error_None;

    if (d.write.compressCP) {
      rlen = encode_data_internal(compress_writer, (void *)this, 
				  cpData);
      if (!rlen) return d.write.lastError;
      rval = error_None;
    } else {
      rval = ::LSE_chkpt::write_data(fp,cpData);
    }
    if (freeData) delete (cpData);
    return rval;
  }

  error_t file_t::end_segment_write() {
    size_t rlen;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (d.write.cpState != 2) return error_IllegalFunction;

    d.write.cpState = 1;
    if (d.write.compressCP) {
      rlen = compress_writer((void *)this, terminator, 2);
      if (rlen != 2) return d.write.lastError;
    } else {
      rlen = fwrite(terminator,1,2,fp);
      if (rlen !=2) return error_IO;
    }
    return error_None;
  }

  error_t file_t::end_checkpoint_write() {
    long currloc;
    unsigned char lbuf[4];
    error_t rval;
    int rvalint;
    size_t rlen;

    if (!fp) return error_FileClosed;
    if (isRead) return error_IllegalFunction;
    if (d.write.cpState!=1) return error_IllegalFunction;

    d.write.cpState = 0;

    if (d.write.compressCP) {
      int zval;
      /* finish up compression */
      compressInfo.next_in = uncompBuf;
      compressInfo.avail_in = d.write.cpSize;
      d.write.compressBytes->content.ustringVal = compBuf;
      do {
	zval = deflate(&compressInfo, Z_FINISH);
	if (zval == Z_OK) {
	  d.write.compressBytes->size = TBUFSIZE;
	  rval = ::LSE_chkpt::write_data(fp,d.write.compressBytes);
	  if (rval) return rval;
	  compressInfo.next_out = compBuf;
	  compressInfo.avail_out = TBUFSIZE;
	} else if (zval == Z_STREAM_END) {
	  d.write.compressBytes->size = (TBUFSIZE - 
					 compressInfo.avail_out);
	  rval = ::LSE_chkpt::write_data(fp,d.write.compressBytes);
	  if (rval) return rval;
	  break;
	} else { /* some sort of error */
	  return error_Compression;
	}
      } while (true);
      d.write.cpSize = 0;
      zval = deflateEnd(&compressInfo);
      compressInited = false;
      if (zval != Z_OK) return error_Compression;
    }

    rlen = fwrite(terminator,1,2,fp);
    if (rlen != 2) return error_IO;

    /* fix up length of checkpoint */
    currloc = ftell(fp);
    if (currloc<0) return error_IO;
    rvalint = fseek(fp,d.write.cpLoc+2,SEEK_SET);
    if (rvalint) return error_IO;
    encode_fixed_len(lbuf,4,currloc - d.write.cpLoc - 2 - 4);
    rlen = fwrite(lbuf,1,4,fp);
    if (rlen != 4) return error_IO;

    /* and return to end of file */
    rvalint = fseek(fp,0L,SEEK_END);
    if (rvalint) return error_IO;
    return error_None;
  }

  /*********************** checkpoint reading ************************/

  size_t
  compress_reader(void *cpFileDummy, unsigned char *buf, size_t olen,
		  error_t *errSeen) {
    file_t *cpFile = (file_t *)cpFileDummy;
    size_t len = olen, togo = cpFile->d.read.uSize - cpFile->d.read.uCurr,
      rlen;
    int zval;
    error_t rval;
    data_t *t;
  
    /* len is bytes we are to read, togo is bytes left in the output buffer */
    while (len > togo) {
      if (togo) {
	/* drain out remainder of buffer */
	memcpy(buf, cpFile->uncompBuf+cpFile->d.read.uCurr,togo);
	len -= togo;
	buf += togo;
	togo = 0;
      }
      /* at this point nothing is left in uncompressed buffer */
      /* was there anything left in the compressed buffer? */
      if (cpFile->compressInfo.avail_in) {

	cpFile->compressInfo.next_out = cpFile->uncompBuf;
	cpFile->compressInfo.avail_out = TBUFSIZE;
	cpFile->d.read.uCurr = 0;

	zval = inflate(&cpFile->compressInfo, Z_SYNC_FLUSH);
	if (zval != Z_OK && zval != Z_STREAM_END) {
	  *errSeen = error_Compression;
	  return 0;
	}
	cpFile->d.read.uSize = togo = 
	  TBUFSIZE - cpFile->compressInfo.avail_out;
      
      } else { /* read in more compressed data */
	rval = ::LSE_chkpt::read_data(cpFile->fp,0,&t,
				      cpFile->d.read.CPspaceRemaining,&rlen);
	if (rval) {
	  *errSeen = rval;
	  return 0;
	}
	if (t->tagClass != CLASS_UNIVERSAL ||
	    t->actualTag != TAG_OCTETSTRING) {
	  *errSeen = error_FileFormat;
	  delete (t);
	  return 0;
	}
	if (cpFile->d.read.compressBytes) {
	  delete (cpFile->d.read.compressBytes);
	}
	cpFile->d.read.CPspaceRemaining -= rlen;
	cpFile->d.read.compressBytes = t;
	cpFile->compressInfo.avail_in = t->size;
	cpFile->compressInfo.next_in = t->content.ustringVal;
	/* and we'll let it pick us up on the next iteration  */
      }
    } /* while len > togo */
    if (len) {
      memcpy(buf,cpFile->uncompBuf+cpFile->d.read.uCurr,len);
      cpFile->d.read.uCurr += len;
    }
    return olen;
  }

  static size_t
  file_reader(void *fpdummy, unsigned char *buf, size_t len, 
	      error_t *errSeen) {
    size_t alen;
    if (buf) { 
      alen = fread(buf,1,len,(FILE *)fpdummy); 
    }
    else {
      long currpos,newpos;
      currpos = ftell((FILE *)fpdummy);
      fseek((FILE *)fpdummy,len,SEEK_CUR);
      newpos = ftell((FILE *)fpdummy);
      alen = newpos - currpos;
    }
    if (alen != len) {
      *errSeen = error_IO;
      return 0;
    } else return len;
  }

  error_t
  file_t::begin_checkpoint_read(uint64_t *idNoP, bool *compressedP) {
    unsigned char buf[6];
    size_t rlen, used;
    long currpos;
    int rint, zval, dlen;
    error_t rval;
    data_t *t;
    bool compress;

    // clean up pointers
    ptrID = 0;
    ptrToID.clear();
    idToPtr.clear();

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (d.read.beganCP) {
      rint = fseek(fp,d.read.nextCPpos,SEEK_SET);
      if (rint) return error_IO;
      d.read.currCPno++;
    }
    currpos = ftell(fp);
    d.read.currCPpos = currpos;

    if (!uncompBuf) {
      uncompBuf = (unsigned char *)malloc(TBUFSIZE);
      if (!uncompBuf) return error_OutOfMemory;
    }

    rlen = fread(buf,1,6,fp);
    if (rlen != 6) return error_IO;
    if (buf[0] != 0xe2 || buf[1] != 0x84) return error_FileFormat;

    d.read.beganCP = true;
    dlen = decode_fixed_len(buf+2,4);
    d.read.nextCPpos = currpos + 6 + dlen;

    rval = ::LSE_chkpt::read_data(fp, 0, &t, dlen, &used);
    if (rval) return rval;

    if (!t) return error_FileFormat;
    if (t->formatTag != TAG_INTEGER ||
	t->tagClass != CLASS_UNIVERSAL) {
      delete (t);
      return error_FileFormat;
    }
    if (idNoP) *idNoP = t->content.uint64Val;
    delete (t);

    /* need to pull in next thing... should be indefinite segment list or
     * octet string
     */
    if (dlen - used < 2) return error_FileFormat;
    rlen = fread(buf,1,2,fp);
    if (rlen != 2) return error_IO;
    if (buf[1] != 0x80) return error_FileFormat;
    if (buf[0] == (TAG_SEQUENCE | CONSTRUCTED)) 
      compress = false;
    else if (buf[0] == (TAG_OCTETSTRING | CONSTRUCTED)) 
      compress=true;
    else return error_FileFormat;

    if (compress) {
      /* initialize the compressor */
      if (compressInited) {
	zval = inflateEnd(&compressInfo);
	compressInited = false;
	if (zval != Z_OK) return error_Compression;
      }
      compressInfo.next_in = 0;
      zval = inflateInit(&compressInfo);
      if (zval == Z_MEM_ERROR) return error_OutOfMemory;
      else if (zval != Z_OK) return error_Compression;
      compressInited = true;
      compressInfo.next_out = uncompBuf;
      compressInfo.avail_out = TBUFSIZE;
      compressInfo.avail_in = 0;
    }

    if (compressedP) *compressedP = compress;
    d.read.compressed = compress;
    d.read.state = 1;
    d.read.uSize = 0;
    d.read.uCurr = 0;
    d.read.compressBytes = 0;

    /* at this point I've read the tag/length for the SegList (SEQUENCE OF) 
     * or the segmented octet string 
     * and the total space taken after computing data length was used + 2
     */
    d.read.CPspaceRemaining = dlen - (used+2);
    return error_None;
  }

  error_t file_t::begin_segment_read(char **segmentName) {
    /* The segment should be a SEQUENCE { string, anything, ... } */
    size_t rlen, used;
    unsigned char buf[2];
    error_t rval = error_None;
    data_t *t;
    bool compressed;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (d.read.state != 1) return error_IllegalFunction;

    compressed = d.read.compressed;

    /* get in the sequence header for the segment */
    if (compressed) {
      rlen = compress_reader((void *)this,buf,2,&rval);
      if (rlen != 2) return rval;
    } else {
      rlen = fread(buf,1,2,fp);
      if (rlen != 2) return error_IO;
    }

    if (buf[1] != 0x80 || 
	buf[0] != (TAG_SEQUENCE | CONSTRUCTED))
      return error_FileFormat;

    /* now pull in the segment name */

    if (compressed) {
      decode_data_internal(compress_reader,(void *)this,
			   0, &t, 0 /* self-limiting */,
			   &rval);
    } else {
      rval = ::LSE_chkpt::read_data(fp, 0, &t, 
				    d.read.CPspaceRemaining-2, &used);
    }
    if (rval) return rval;

    if (!t) return error_FileFormat;
    if (t->formatTag != TAG_UTF8STRING ||
	t->tagClass != CLASS_UNIVERSAL) {
      delete (t);
      return error_FileFormat;
    }

    if (!compressed) d.read.CPspaceRemaining -= 2 + used;
    d.read.state = 2;
    if (segmentName) {
      *segmentName = (char *)malloc(t->size+1);
      if (!*segmentName) {
	delete (t);
	return error_OutOfMemory;
      }
      memcpy(*segmentName, t->content.stringVal,t->size+1);
    }
    delete (t);
    return error_None;
  }

  inline error_t file_t::find_indefinite_end() {
    error_t rval=error_None;
    data_t *t;
    size_t rlen;
    bool compressed = d.read.compressed;
  
    while (1) {
      if (compressed) {
	rlen = decode_data_internal(compress_reader,(void *)this,
				    skipper, &t, 
				    0 /* self-limiting */,
				    &rval);
      } else {
	rval = ::LSE_chkpt::read_data(fp, skipper, &t,
				      d.read.CPspaceRemaining, &rlen);
	d.read.CPspaceRemaining -= rlen;
      }
      if (!t) break;
      if (rlen) delete (t);
      if (rval) return rval;
      if (!compressed && !d.read.CPspaceRemaining) 
	return error_FileFormat;
    }
    return error_None;
  }

  error_t
  file_t::end_segment_read(bool ateEnd) {

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (d.read.state != 2) return error_IllegalFunction;

    /* segment has an indefinite end and might even be compressed! */
    if (!ateEnd) find_indefinite_end();

    d.read.state = 1;
    return error_None;
  }

  error_t 
  file_t::read_from_segment(acceptor_t acceptor, data_t **dp) {

    size_t tookSize;
    error_t rval=error_None;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (d.read.state != 2) return error_IllegalFunction;

    if (d.read.compressed) {
      decode_data_internal(compress_reader,(void *)this,
			   acceptor, dp, 0 /* self-limiting */,
			   &rval);
    } else {
      rval = ::LSE_chkpt::read_data(fp, acceptor, dp, 
				    d.read.CPspaceRemaining, &tookSize);
      if (!rval) d.read.CPspaceRemaining -= tookSize;
    }
    return rval;
  }

  error_t
  file_t::read_taglen_from_segment(acceptor_t acceptor, data_t **dp) {

    size_t tookSize;
    error_t rval=error_None;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (d.read.state != 2) return error_IllegalFunction;

    if (d.read.compressed) {
      decode_taglen_internal(compress_reader,(void *)this,
			     acceptor, dp, 0 /* self-limiting */,
			     &rval);
    } else {
      tookSize = decode_taglen_internal(file_reader,(void *)fp,
					acceptor, dp, 
					d.read.CPspaceRemaining,
					&rval);
      if (!rval) d.read.CPspaceRemaining -= tookSize;
    }
    return rval;
  }

  error_t
  file_t::read_body_from_segment(acceptor_t acceptor, data_t *dp) {

    size_t tookSize;
    error_t rval=error_None;

    if (!fp) return error_FileClosed;
    if (!isRead) return error_IllegalFunction;
    if (d.read.state != 2) return error_IllegalFunction;
    if (!dp) return error_None;

    if (d.read.compressed) {
      decode_body_internal(compress_reader,(void *)this,
			   acceptor, dp, 0 /* self-limiting */,
			   &rval);
    } else {
      tookSize = decode_body_internal(file_reader,(void *)fp,
				      acceptor, dp, d.read.CPspaceRemaining,
				      &rval);
      if (!rval) d.read.CPspaceRemaining -= tookSize - 1;
    }
    return rval;
  }

  error_t
  file_t::read_data(acceptor_t acceptor,
		    data_t **cpDataP, size_t maxToRead,
		    size_t *actualP) {
    return ::LSE_chkpt::read_data(fp, acceptor, cpDataP, maxToRead, actualP);
  }

  error_t
  file_t::write_data(data_t *dp) {
    return ::LSE_chkpt::write_data(fp, dp);
  }

} // namespace LSE_chkpt
