/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Checkpoint header file
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Header file for the checkpoint library.  Besides being useful, this is
 * also a demonstration of straight wrapping of a library
 *
 */

#ifndef _LSE_CHKPT_H_
#define _LSE_CHKPT_H_

#include <LSE_inttypes.h>
#include <stdio.h>
#include <zlib.h>
#include <map>

namespace LSE_chkpt {

  typedef enum error_t {
    error_None = 0,
    error_OutOfMemory = 1,
    error_BadArgument = 2,
    error_FileClosed = 3,
    error_IllegalFunction = 4,
    error_Compression = 5,
    error_IO = 6,
    error_BufferOverflow = 7,
    error_FileFormat = 8,
    error_NotFound = 9,
    error_NotYetImplemented = 10,
    error_Application = 11
  } error_t;

  inline bool IS_CONSTRUCTED(int x) {
    return ((x) & 0x20);
  }

  inline bool TAG_CLASS(int x) {
    return ((x) & 0xc0);
  }

  typedef void (*acceptor_t)(int, int, int *);

  class data_t {
  public: // Everyone is supposed to be able to see these to read at least.
    struct data_t *parent, *sibling, 
      *oldestChild, *youngestChild;
    int formatTag; /* tag to use for encoding... must be a universal tag */
    int actualTag; /* actual tag to report */
    int tagClass;  /* classification of tag + primitive/constructed */
    bool allocated;
    int size;
    union { /* always put allocated thing first... */
      void *genericAllocated;
      struct {
	struct data_t *identification;
	bool idAllocated;
      } pdv;
      unsigned char *ustringVal;
      char *stringVal;
      struct {
	unsigned int *buffer;
	int length;
      } oid;
      uint64_t uint64Val;
      int64_t int64Val;
      bool booleanVal;
    } content;

    ~data_t();

    void adopt_child(data_t *child);
    void adopt_parent(data_t *parent);
    
    error_t change_tag(int newtag, int newclass);
    unsigned int update_size();
    data_t * copy_data();

  };

  class file_t {
  public:
    typedef std::map<void *,uint64_t> ptrToID_t;
    typedef std::map<uint64_t, void *> idToPtr_t;
    uint64_t ptrID;
    ptrToID_t ptrToID;
    idToPtr_t idToPtr;

  private: 
    FILE *fp;
    bool isRead;
    bool compressInited;
    z_stream compressInfo;
    unsigned char *compBuf;
    unsigned char *uncompBuf;
    union {
      struct {
	long cpLoc;
	long cpSize; /* fullness of the uncompressed buffer */
	data_t *header;
	data_t *buildPtr, *compressBytes;
	error_t lastError; /* for internal use only */
	int headerState; /* 0=accept GP, 1=accepting TOC parms*/
	int cpState; /* 0= doing nothing, 1 = doing checkpoint,
		      * 2 = doing segment */
	bool compressCP;
      } write;
      struct {
	long uSize; /* last good data in uncompressed buffer */
	long uCurr; /* next to read data in uncompressed buffer */
	data_t *compressBytes;
	data_t *header;
	data_t *gpIter, *tocIter, *segParmReset, *segParmIter;
	unsigned int tocIterNo;
      
	/* checkpoint positions */
	long currCPpos, nextCPpos, currCPno;
	bool beganCP; /* implies that nextCPpos good and we are in middle */
	long length;
	int  state; /* 0 = not processing, 1 = in CP, 2 = in segment */
	bool compressed;
	size_t CPspaceRemaining;
	long *sought;
	long highestSought;
      } read;
    } d;

  public:
    ~file_t();
    file_t();
    file_t(const char *filename, const char *mode);
    error_t open(const char *filename, const char *mode);
    void close();

    long getReadLength() { return d.read.length; }
    FILE *getFP() { return fp; }

    error_t seek(long offset, int whence);
    bool more_checkpoints();
    
    error_t begin_header_write(const char *name);
    error_t add_globalparm(const char *parmVal);
    error_t add_toc(const char *entryName);
    error_t add_tocparm(const char *parmVal);
    error_t end_header_write();

    error_t begin_checkpoint_write(uint64_t idNo, bool compressed);
    error_t end_checkpoint_write();

    error_t begin_segment_write(const char *segmentName);
    error_t write_to_segment(bool freeData, data_t *cpData);
    error_t end_segment_write();
    
    error_t begin_header_read(char **name);
    error_t find_toc(const char *segmentName, unsigned int *positionP,
			       data_t **plistP);

    error_t get_globalparm(char **parmP, bool restart);
    error_t get_toc(char **segmentNameP, unsigned int *positionP,
			      bool restart);
    error_t get_tocparm(char **parmP, bool restart);

    error_t begin_checkpoint_read(uint64_t *idNoP, bool *compressedP);

    error_t begin_segment_read(char **segmentName);
    error_t read_from_segment(acceptor_t acceptor,
					data_t **cpDataP);
    error_t read_taglen_from_segment(acceptor_t acceptor,
					       data_t **cpDataP);
    error_t read_body_from_segment(acceptor_t acceptor,
				   data_t *cpDataP);
    error_t end_segment_read(bool ateEnd);

    error_t write_data(data_t *dp);
    error_t read_data(acceptor_t acceptor,
				data_t **cpDataP,
				size_t maxToRead,
				size_t *actualP);
    
  private:
    inline long skip_forward(long offset);
    inline error_t find_indefinite_end();

    friend size_t compress_writer(void *, const unsigned char *, size_t);
    friend size_t compress_reader(void *, unsigned char *, size_t,
				  error_t *);

  };

  static const int  CONSTRUCTED = 0x20;
  static const int  CLASS_UNIVERSAL  = 0x0;
  static const int  CLASS_APPLICATION  = 0x40;
  static const int  CLASS_CONTEXTSPECIFIC  = 0x80;
  static const int  CLASS_PRIVATE  = 0xc0;
  static const int  TAG_BOOLEAN = 1;
  static const int  TAG_INTEGER = 2;
  static const int  TAG_BITSTRING = 3;
  static const int  TAG_OCTETSTRING = 4;
  static const int  TAG_NULL = 5;
  static const int  TAG_OBJECTID = 6;
  static const int  TAG_OBJECTDESC = 7;
  static const int  TAG_EXTERNAL = 8;
  static const int  TAG_REAL = 9;
  static const int  TAG_ENUMERATED = 10;
  static const int  TAG_EMBEDDEDPDV = 11;
  static const int  TAG_UTF8STRING = 12;
  static const int  TAG_RELATIVEOID = 13;
  static const int  TAG_SEQUENCE = 16;
  static const int  TAG_SET = 17;
  static const int  TAG_NUMERICSTRING = 18;
  static const int  TAG_PRINTABLESTRING = 19;
  static const int  TAG_TELETEXSTRING = 20;
  static const int  TAG_VIDEOTEXSTRING = 21;
  static const int  TAG_IA5STRING = 22;
  static const int  TAG_UNIVERSALTIME = 23;
  static const int  TAG_GENERALIZEDTIME = 24;
  static const int  TAG_GRAPHICSTRING = 25;
  static const int  TAG_VISIBLESTRING = 26;
  static const int  TAG_GENERALSTRING = 27;
  static const int  TAG_UNIVERSALSTRING = 28;
  static const int  TAG_UNRESTRICTEDSTRING = 29;
  static const int  TAG_BMPSTRING = 30;
  static const int  TAG_ENCODED = 129;

  data_t *build_boolean(data_t *parent, bool val);
  data_t *build_unsigned(data_t *parent, uint64_t val);
  data_t *build_signed(data_t *parent, int64_t val);
  data_t *build_enumerated(data_t *parent, unsigned int val);
  data_t *build_null(data_t *parent);
  data_t *build_sequence(data_t *parent);
  data_t *build_set(data_t *parent);
  data_t *build_objectid(data_t *parent, unsigned int *dv, int dlen);
  data_t *build_relativeoid(data_t*parent, unsigned int *dv, int dlen);
  data_t *build_restrictedstring(data_t *parent, int sclass, 
				 const unsigned char *dv, int slen, bool copy);
  data_t *build_octetstring(data_t *parent, const unsigned char *str, 
			    int slen, bool copy);
  data_t *build_objectdescriptor(data_t *parent, const unsigned char *str,
				 bool copy);
  data_t *build_string(data_t *parent, const char *str, bool copy);
  data_t *build_stringN(data_t *parent, const char *str, int slen, bool copy);
  data_t *build_unrestrictedstring(data_t *parent, data_t *idval, 
				   const unsigned char *str, int slen, 
				   bool copy);
  data_t *build_embeddedpdv(data_t *parent,  data_t *idval,
			    const unsigned char *str, int slen, bool copy);
  data_t *build_explicit_tag(data_t *parent, int actualTag, int tagClass);
  data_t *build_header(data_t *parent, int actualTag, int tagClass, int size);
  data_t *build_indefinite_end(data_t *parent);
  data_t *build_encoded(data_t *parent, const unsigned char *str, 
			int slen, bool copy);

  error_t write_data(FILE *fp, data_t *dp);

  error_t read_data(FILE *fp, acceptor_t acceptor,
		    data_t **cpDataP, size_t maxToRead, size_t *actualP);

  error_t encode_data(unsigned char *buf, size_t maxlen, data_t *cpData,
		      size_t *actualSize);


  error_t decode_data(unsigned char *buf, size_t len,
		      acceptor_t acceptor, data_t **cpDataP, size_t *actualP);

} // namespace LSE_chkpt

#endif /* _LSE_CHKPT_H_ */
