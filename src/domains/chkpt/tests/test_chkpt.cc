/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Tests for the checkpoint domain
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file tests the checkpoint library and also provides a 
 * utility for viewing checkpoint files.
 *
 */
#include "LSE_chkpt.h"
#include "asn1lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace LSE_chkpt;

static void 
report_cerr(LSE_chkpt::error_t cerr, const char *msg, int lineno) {
  const char *emsg;
  switch (cerr) {
  case LSE_chkpt::error_None : emsg = "No error"; break;
  case LSE_chkpt::error_OutOfMemory : emsg = "Out of memory"; break;
  case LSE_chkpt::error_BadArgument : emsg = "Bad argument"; break;
  case LSE_chkpt::error_FileClosed : emsg = "File closed"; break;
  case LSE_chkpt::error_IllegalFunction : emsg = "Illegal function"; break;
  case LSE_chkpt::error_Compression : emsg = "Compression"; break;
  case LSE_chkpt::error_IO : emsg = "I/O"; break;
  case LSE_chkpt::error_BufferOverflow : emsg = "Buffer overflow"; break;
  case LSE_chkpt::error_FileFormat : emsg = "File format"; break;
  default: emsg = "Unknown"; break;
  }
  fprintf(stderr,"Error: %s error returned from %s at %d\n",emsg,msg,lineno);
}

#define check_error(c,s) if (c) { report_cerr(c,s,__LINE__); exit(1); }

static void print_data(const LSE_chkpt::data_t *t, int depth, int dodeep) {
  int i;
  static const char *classnames[] = { "UNIVERSAL", "APPLICATION",
				      "CONTEXT-SPECIFIC", "PRIVATE" };
  /* indent */
  for (i=0;i<depth*2;i++) {
    putc(' ',stdout);
  }
  printf("[ %s %d ] %s ", classnames[t->tagClass>>6], t->actualTag,
	 LSE_chkpt::IS_CONSTRUCTED(t->tagClass)?"CONSTRUCTED":"PRIMITIVE");
  if (t->size < 0) printf("INDEFINITE ");
  else printf("length %d ", t->size);

  if (LSE_chkpt::IS_CONSTRUCTED(t->tagClass)) {
    LSE_chkpt::data_t *p = t->oldestChild;
    printf("\n");

    if ( (t->formatTag == LSE_chkpt::TAG_UNRESTRICTEDSTRING ||
	  t->formatTag == LSE_chkpt::TAG_EMBEDDEDPDV) && 
	 t->content.pdv.identification) {
      print_data(t->content.pdv.identification,depth+1,dodeep);
    }

    while (p) {
      print_data(p, depth+1,dodeep);
      p=p->sibling;
    }
  } else {
    switch (t->formatTag) {
    case LSE_chkpt::TAG_BOOLEAN:
      printf("value = %s", t->content.booleanVal?"TRUE":"FALSE");
      break;

    case LSE_chkpt::TAG_INTEGER:
    case LSE_chkpt::TAG_ENUMERATED:
      /* note... there's no way to keep track of whether they are to be treated
       * as negative or not anyway without looking at the sizing...
       */
      printf("value = %" PRIx64 " %" PRId64, t->content.uint64Val,
	     t->content.int64Val);
      break;

    case LSE_chkpt::TAG_OCTETSTRING:
      if (dodeep) {
	int j;
	for (j=0;j<t->size;j++) {
	  if ((j%16) == 0) {
	    printf("\n");
	    for (i=0;i<depth*2+5;i++) {
	      putc(' ',stdout);
	    }
	    printf("%06x:", j);
	  }
	  printf(" %02x", t->content.ustringVal[j]);
	}
      }
      break;

    case LSE_chkpt::TAG_UTF8STRING:
    case LSE_chkpt::TAG_NUMERICSTRING:
    case LSE_chkpt::TAG_PRINTABLESTRING:
    case LSE_chkpt::TAG_TELETEXSTRING:
    case LSE_chkpt::TAG_VIDEOTEXSTRING:
    case LSE_chkpt::TAG_IA5STRING:
    case LSE_chkpt::TAG_GRAPHICSTRING:
    case LSE_chkpt::TAG_VISIBLESTRING:
    case LSE_chkpt::TAG_GENERALSTRING:
    case LSE_chkpt::TAG_UNIVERSALSTRING:
    case LSE_chkpt::TAG_BMPSTRING:
    case LSE_chkpt::TAG_OBJECTDESC:
      /* this is really wrong for multi-byte characters */
      printf("value = %s",t->content.stringVal);
      break;

    case LSE_chkpt::TAG_OBJECTID:
    case LSE_chkpt::TAG_RELATIVEOID:
      printf("value = ");
      for (i=0;i<t->content.oid.length;i++) {
	if (i!=0) putc('.',stdout);
	printf("%u",t->content.oid.buffer[i]);
      }
      break;

    case LSE_chkpt::TAG_BITSTRING: /* TODO: what is in-memory representation? */
    case LSE_chkpt::TAG_EXTERNAL:
    case LSE_chkpt::TAG_REAL:
    case LSE_chkpt::TAG_UNIVERSALTIME:
    case LSE_chkpt::TAG_GENERALIZEDTIME:
    default:
      break;
    }
    /* end line */
    printf("\n");
  }
}

static void dump_file(char *fname, int dodeep) {
  LSE_chkpt::file_t *cpf;
  LSE_chkpt::error_t cerr;
  LSE_chkpt::data_t *t;
  size_t rlen;

  cpf = new LSE_chkpt::file_t(fname,"r");
  if (!cpf) {
    fprintf(stderr,"Error: unable to open file %s for reading\n",fname);
    exit(1);
  }

  while (ftell(cpf->getFP()) != cpf->getReadLength()) {
    cerr = cpf->read_data(0, &t, 0, &rlen);
    check_error(cerr,"LSE_chkpt::read_data");
    print_data(t,0,dodeep);
    delete t;
  }
}

static void do_segment_pieces(LSE_chkpt::file_t *cpf) {
  LSE_chkpt::data_t *t;
  LSE_chkpt::error_t cerr;
  do {
    cerr = cpf->read_from_segment(0,&t);
    check_error(cerr,"LSE_chkpt::read_from_segment");
    if (t!=0) print_data(t,2,0);
  } while (t);
}

static void test_read(char *fname) {
  LSE_chkpt::file_t *cpf;
  char *str;
  LSE_chkpt::error_t cerr;
  uint64_t idNo;
  bool wasCompressed;
  unsigned int position;

  /* open and start reading header */
  cpf = new LSE_chkpt::file_t(fname,"r");
  if (!cpf) {
    fprintf(stderr,"Error: unable to open file %s for reading\n",fname);
    exit(1);
  }

  cerr=cpf->begin_header_read(&str);
  check_error(cerr,"LSE_chkpt::begin_header_read");
  printf("File ID: %s\n",str);

  /* global parameters */
  cerr = cpf->get_globalparm(&str, false);
  check_error(cerr,"LSE_chkpt::get_globalparm");
  while (str != 0) {
    printf("  Global parm: %s\n",str);
    cerr = cpf->get_globalparm(&str, false);      
    check_error(cerr,"LSE_chkpt::get_globalparm");
  }

  /* table of contents */
  cerr = cpf->get_toc(&str,&position,false);
  check_error(cerr,"LSE_chkpt::get_toc");
  while (str != 0) {
    printf("  TOC entry #%d: %s\n", position, str);

    cerr = cpf->get_tocparm(&str,position % 2);
    check_error(cerr,"LSE_chkpt::get_tocparm");
    while (str != 0) {
      printf("    %s\n",str);
      cerr = cpf->get_tocparm(&str,false);
      check_error(cerr,"LSE_chkpt::get_tocparm");
    }

    cerr = cpf->get_toc(&str,&position,false);
    check_error(cerr,"LSE_chkpt::get_toc");
  }

  /* check toc lookup */
  cerr = cpf->find_toc("L1D",&position,0);
  check_error(cerr,"LSE_chkpt::find_toc");
  printf("  Found TOC entry for L1D at %d\n",position);

  cerr = cpf->get_tocparm(&str,true);
  check_error(cerr,"LSE_chkpt::get_tocparm");
  while (str != 0) {
    printf("    %s\n",str);
    cerr = cpf->get_tocparm(&str,false);
    check_error(cerr,"LSE_chkpt::get_tocparm");
  }
  
  /* begin checkpoint read -- gets arguments so we can compare */
  cerr = cpf->seek(1,SEEK_SET); /* first checkpoint */
  check_error(cerr,"LSE_chkpt::seek");

  while (cpf->more_checkpoints()) {

    cerr = cpf->begin_checkpoint_read(&idNo, &wasCompressed);
    check_error(cerr,"LSE_chkpt::begin_checkpoint_read");
    printf("Checkpoint %" PRIu64 " compressed=%c\n", idNo,
	   wasCompressed ? 'Y' : 'N');

    /* begin segment read */
    cerr = cpf->begin_segment_read(&str);
    check_error(cerr,"LSE_chkpt::begin_segment_read");
    printf("  Segment %s\n", str);
    free(str);

    do_segment_pieces(cpf);

    /* end segment read */
    cerr = cpf->end_segment_read(true);
    check_error(cerr,"LSE_chkpt::end_segment_read");

    /* begin segment read */
    cerr = cpf->begin_segment_read(&str);
    check_error(cerr,"LSE_chkpt::begin_segment_read");
    printf("  Segment %s\n", str);
    free(str);

    do_segment_pieces(cpf);

    /* end segment read */
    cerr = cpf->end_segment_read(true);
    check_error(cerr,"LSE_chkpt::end_segment_read");

  }

  /* also a begin checkpoint with search */

  cpf->close();
  delete cpf;
}

static void test_write(bool docompress) {
  LSE_chkpt::file_t *cpf;
  LSE_chkpt::data_t *dp;
  LSE_chkpt::error_t cerr;
  static unsigned int testoid[] = { 2, 100, 3 };
  static unsigned int testreloid[] = { 8571, 3, 2 };

  /* try writing the file */
  cpf=new LSE_chkpt::file_t("test.cpf","w");
  if (!cpf) {
    fprintf(stderr,"Unable to open test.cpf for writing\n");
    exit(1);
  }
  
  cerr=cpf->begin_header_write("testFile");
  check_error(cerr,"LSE_chkpt::begin_header_write");
  cerr=cpf->add_globalparm("U=1000000");
  check_error(cerr,"LSE_chkpt::add_globalparm");
  cerr=cpf->add_globalparm("V=1000");
  check_error(cerr,"LSE_chkpt::add_globalparm");
  cerr=cpf->add_globalparm("W=4000");
  check_error(cerr,"LSE_chkpt::add_globalparm");
  cerr=cpf->add_globalparm(			    "long="
			    "12345678901234567890123456789012"
			    "12345678901234567890123456789012"
			    "12345678901234567890123456789012"
			    "12345678901234567890123456789012"
			    "12345678901234567890123456789012"
			    );
  check_error(cerr,"LSE_chkpt::add_globalparm");

  cerr=cpf->add_toc("emulator");
  check_error(cerr,"LSE_chkpt::add_toc");
  cerr=cpf->add_tocparm("physreg=256");
  check_error(cerr,"LSE_chkpt::add_tocparm");
  cerr=cpf->add_tocparm("doOS=TRUE");
  check_error(cerr,"LSE_chkpt::add_tocparm");
  cerr=cpf->add_toc("L1D");
  check_error(cerr,"LSE_chkpt::add_toc");
  cerr=cpf->add_tocparm("assoc=4");
  check_error(cerr,"LSE_chkpt::add_tocparm");
  cerr=cpf->add_tocparm("numsets=256");
  check_error(cerr,"LSE_chkpt::add_tocparm");
  cerr=cpf->add_tocparm("blocksize=32");
  check_error(cerr,"LSE_chkpt::add_tocparm");
  cerr=cpf->end_header_write();
  check_error(cerr,"LSE_chkpt::end_header_write");

  /* checkpoint 0 */

  cerr=cpf->begin_checkpoint_write(0, docompress);
  check_error(cerr,"LSE_chkpt::begin_checkpoint_write");

  cerr=cpf->begin_segment_write("emulator");
  check_error(cerr,"LSE_chkpt::begin_segment_write");
  cerr=cpf->write_to_segment(true, 
			      LSE_chkpt::build_unsigned(0,UINT64_C(1)<<63));
  check_error(cerr,"LSE_chkpt::write_to_segment");
  cerr=cpf->write_to_segment(true, 
			      LSE_chkpt::build_unsigned(0,UINT64_C(1)<<62));
  check_error(cerr,"LSE_chkpt::write_to_segment");
  cerr=cpf->write_to_segment(true, 
			      LSE_chkpt::build_unsigned(0,UINT64_C(1)));
  check_error(cerr,"LSE_chkpt::write_to_segment");
  cerr=cpf->write_to_segment(true, 
			      LSE_chkpt::build_unsigned(0,UINT64_C(64)));
  check_error(cerr,"LSE_chkpt::write_to_segment");
  cerr=cpf->write_to_segment(true, 
			      LSE_chkpt::build_unsigned(0,UINT64_C(128)));
  check_error(cerr,"LSE_chkpt::write_to_segment");
  cerr=cpf->write_to_segment(true, 
			      LSE_chkpt::build_unsigned(0,UINT64_C(256)));
  check_error(cerr,"LSE_chkpt::write_to_segment");
  cerr=cpf->end_segment_write();
  check_error(cerr,"LSE_chkpt::end_segment_write");

  cerr=cpf->begin_segment_write("L1D");
  check_error(cerr,"LSE_chkpt::begin_segment_write");
  dp = LSE_chkpt::build_sequence(0);
  LSE_chkpt::build_signed(dp,INT64_C(-1));
  LSE_chkpt::build_signed(dp,INT64_C(-64));
  LSE_chkpt::build_signed(dp,INT64_C(-128));
  LSE_chkpt::build_signed(dp,INT64_C(-256));
  cerr=cpf->write_to_segment(true, dp);
  check_error(cerr,"LSE_chkpt::write_to_segment");
  cerr=cpf->end_segment_write();
  check_error(cerr,"LSE_chkpt::end_segment_write");

  cerr=cpf->end_checkpoint_write();
  check_error(cerr,"LSE_chkpt::end_checkpoint_write");

  /* checkpoint 1 */

  cerr=cpf->begin_checkpoint_write(1, docompress);
  check_error(cerr,"LSE_chkpt::begin_checkpoint_write");

  cerr=cpf->begin_segment_write("emulator");
  check_error(cerr,"LSE_chkpt::begin_segment_write");
  dp = LSE_chkpt::build_sequence(0);
  LSE_chkpt::build_boolean(dp,true);
  LSE_chkpt::build_boolean(dp,false);
  LSE_chkpt::build_objectdescriptor(dp,(unsigned char *)"myobject",true);
  LSE_chkpt::build_objectid(dp,testoid,sizeof(testoid)/sizeof(int));
  LSE_chkpt::build_relativeoid(dp,testreloid,sizeof(testreloid)/sizeof(int));
  cerr=cpf->write_to_segment(true, dp);
  check_error(cerr,"LSE_chkpt::write_to_segment");
  cerr=cpf->end_segment_write();
  check_error(cerr,"LSE_chkpt::end_segment_write");

  cerr=cpf->begin_segment_write("L1D");
  check_error(cerr,"LSE_chkpt::begin_segment_write");
  cerr=cpf->end_segment_write();
  check_error(cerr,"LSE_chkpt::end_segment_write");

  cerr=cpf->end_checkpoint_write();
  check_error(cerr,"LSE_chkpt::end_checkpoint_write");

  cpf->close();
  delete cpf;
  /* unlink("test.cpf"); */
}

static 
LSE_chkpt::error_t 
copy_header(LSE_chkpt::file_t *fromF, LSE_chkpt::file_t *toF, int *segcountP) {
  LSE_chkpt::error_t cerr;
  char *str;
  int segmentcount = 0;

  /* file header beginning */
  cerr=fromF->begin_header_read(&str);
  check_error(cerr,"LSE_chkpt::begin_header_read");

  cerr = toF->begin_header_write(str);
  check_error(cerr,"LSE_chkpt::begin_header_write");

  /* global parameters */
  cerr = fromF->get_globalparm(&str, false);
  check_error(cerr,"LSE_chkpt::get_globalparm");
  while (str != 0) {
    cerr = toF->add_globalparm(str);
    check_error(cerr,"LSE_chkpt::add_globalparm");

    cerr = fromF->get_globalparm(&str, false);      
    check_error(cerr,"LSE_chkpt::get_globalparm");
  }

  /* table of contents */
  cerr = fromF->get_toc(&str,0,false);
  check_error(cerr,"LSE_chkpt::get_toc");
  while (str != 0) {

    cerr = toF->add_toc(str);
    check_error(cerr,"LSE_chkpt::add_toc");

    segmentcount++;

    cerr = fromF->get_tocparm(&str,false);
    check_error(cerr,"LSE_chkpt::get_tocparm");
    while (str != 0) {      
      cerr = toF->add_tocparm(str);
      check_error(cerr,"LSE_chkpt::add_tocparm");

      cerr = fromF->get_tocparm(&str,false);
      check_error(cerr,"LSE_chkpt::get_tocparm");
    }

    cerr = fromF->get_toc(&str,0,false);
    check_error(cerr,"LSE_chkpt::get_toc");
  }

  /* and finish off the header */
  cerr = toF->end_header_write();
  check_error(cerr,"LSE_chkpt::end_header_write");

  *segcountP = segmentcount;
  return LSE_chkpt::error_None;
}

static LSE_chkpt::error_t
copy_checkpoint(LSE_chkpt::file_t *fromF, LSE_chkpt::file_t *toF,
		bool useFileCompress, bool compress,
		int segmentcount) {
  LSE_chkpt::error_t cerr;
  char *str;
  LSE_chkpt::data_t *t;
  uint64_t idNo;
  int i;

  cerr = fromF->begin_checkpoint_read(&idNo, 
				     useFileCompress ? &compress : 0);
  check_error(cerr,"LSE_chkpt::begin_checkpoint_read");

  cerr = toF->begin_checkpoint_write(idNo, compress);
  check_error(cerr,"LSE_chkpt::begin_checkpoint_write");

  for (i=0;i<segmentcount;i++) {

    /* begin segment read */
    cerr = fromF->begin_segment_read(&str);
    check_error(cerr,"LSE_chkpt::begin_segment_read");
    
    cerr = toF->begin_segment_write(str);
    check_error(cerr,"LSE_chkpt::begin_segment_write");
    
    free(str);
    
    /* and copy over the segments */
    do {
      cerr = fromF->read_from_segment(0,&t);
      check_error(cerr,"LSE_chkpt::read_from_segment");
      
      if (t!=0) {
	cerr = toF->write_to_segment(true,t);
	check_error(cerr,"LSE_chkpt::write_to_segment");
      }
    } while (t);
    
    /* end segment read */
    cerr = fromF->end_segment_read(true);
    check_error(cerr,"LSE_chkpt::end_segment_read");
    
    cerr = toF->end_segment_write();
    check_error(cerr,"LSE_chkpt::end_segment_write");
    
  } /* for segments */

  cerr = toF->end_checkpoint_write();
  check_error(cerr,"LSE_chkpt::end_checkpoint_write");

  return LSE_chkpt::error_None;
}

void copy_file(char *fromName, char *toName, bool compress) {
  LSE_chkpt::file_t *fromF, *toF;
  LSE_chkpt::error_t cerr;

  int segmentcount = 0;

  /* open files */
  fromF = new LSE_chkpt::file_t(fromName,"r");
  if (!fromF) {
    fprintf(stderr,"Error: unable to open file %s for reading\n",fromName);
    exit(1);
  }
  toF = new LSE_chkpt::file_t(toName,"w");
  if (!toF) {
    fprintf(stderr,"Error: unable to open file %s for writing\n",toName);
    exit(1);
  }

  cerr = copy_header(fromF, toF, &segmentcount);

  /* checkpoints */

  cerr = fromF->seek(1,SEEK_SET); /* first checkpoint */
  check_error(cerr,"LSE_chkpt::seek");

  while (fromF->more_checkpoints()) {
    copy_checkpoint(fromF, toF, false, compress, segmentcount);
  } /* while more checkpoints */

  fromF->close();
  toF->close();
  delete toF;
  delete fromF;
}

void extract_file(char *fromName, char *toName, int first, int num, 
		  int skip) {
  LSE_chkpt::file_t *fromF, *toF;
  LSE_chkpt::error_t cerr;
  int cnt;
  int segmentcount = 0;
  FILE *fromFP;

  /* open files */
  fromF = new LSE_chkpt::file_t(fromName,"r");
  if (!fromF) {
    fprintf(stderr,"Error: unable to open file %s for reading\n",fromName);
    exit(1);
  }
  fromFP = fopen(fromName,"r");
  if (!fromFP) {
    fprintf(stderr,"Error: unable to open file %s for reading\n",fromName);
    exit(1);
  }

  toF = new LSE_chkpt::file_t(toName,"w");
  if (!toF) {
    fprintf(stderr,"Error: unable to open file %s for writing\n",toName);
    exit(1);
  }

  cerr = copy_header(fromF, toF, &segmentcount);

  /* checkpoints */

  cerr = fromF->seek(first+1,SEEK_SET); /* first checkpoint */
  check_error(cerr,"LSE_chkpt::seek");

  cnt = 0;
  while (fromF->more_checkpoints() && num > cnt) {
    long currloc, len;
    cnt++;

    currloc = ftell(fromF->getFP());
    fseek(fromFP,currloc,SEEK_SET);

    fromF->read_data(LSE_chkpt::skipper, 0, 0, 0);
    len = ftell(fromF->getFP()) - currloc;

    while (len) {
      char buf[2048];
      size_t namt, amt;
      amt = len > 2048 ? 2048 : len;

      namt = fread(&buf[0],1,amt,fromFP);
      fwrite(&buf[0],1,namt,toF->getFP());
      len -= namt;
    }

    if (skip > 1) {
      int i;
      for (i=1;i<skip;i++) {
	fromF->read_data(LSE_chkpt::skipper, 0, 0, 0);
      }
    }
  } /* while more checkpoints */

  fclose(fromFP);
  fromF->close();
  toF->close();
  delete toF;
  delete fromF;
}

void randomize_file(char *fromName, char *toName, int seed, 
		    bool cliplast) {
  LSE_chkpt::file_t *fromF, *toF;
  LSE_chkpt::error_t cerr;
  int numCP, savenumCP;
  int segmentcount = 0;
  int *marks;
  int ret;
  int i;
  FILE *saveFP, *newFP, *fromFP;
  char nbuf[5000];

  srand(seed);

  /* open files */
  fromF = new LSE_chkpt::file_t(fromName,"r");
  if (!fromF) {
    fprintf(stderr,"Error: unable to open file %s for reading\n",fromName);
    exit(1);
  }
  fromFP = fopen(fromName,"r");
  if (!fromFP) {
    fprintf(stderr,"Error: unable to open file %s for reading\n",fromName);
    exit(1);
  }

  toF = new LSE_chkpt::file_t(toName,"w");
  if (!toF) {
    fprintf(stderr,"Error: unable to open file %s for writing\n",toName);
    exit(1);
  }

  cerr = copy_header(fromF, toF, &segmentcount);
  check_error(cerr,"copy_header");

  /* now, create a file per checkpoint */

  sprintf(nbuf,"tempRand.%d",seed);
  ret = mkdir(nbuf,0777);

  numCP = 0;
  saveFP = toF->getFP();

  while (fromF->more_checkpoints()) {
    long currloc, len;
    numCP++;

    currloc = ftell(fromF->getFP());
    fseek(fromFP,currloc,SEEK_SET);

    fromF->read_data(LSE_chkpt::skipper, 0, 0, 0);
    len = ftell(fromF->getFP()) - currloc;

    sprintf(nbuf,"tempRand.%d/%d",seed,numCP);
    newFP = fopen(nbuf,"w");

    while (len) {
      char buf[2048];
      size_t namt, amt;
      amt = len > 2048 ? 2048 : len;

      namt = fread(&buf[0],1,amt,fromFP);
      fwrite(&buf[0],1,namt,newFP);
      len -= namt;
    }

    fclose(newFP);
  } /* while more checkpoints */
  fclose(fromFP);

  //toF->getFP() = saveFP;

  printf("numCP=%d\n",numCP);

  savenumCP = numCP;

  if (cliplast && numCP) numCP--;

  /* form the set of checkpoint numbers */
  marks = (int *)malloc(sizeof(int)*(numCP+1));
  if (!marks) {
    fprintf(stderr,"Error: out of memory\n");
    exit(1);
  }
  for (i=0;i<numCP;i++) {
    marks[i] = i+1;
  }

  /* select from the set until none left */

  while (numCP) {
    int ind,ind2;

    /* select an element from the set (which is of size numCP) */
    ind = 0+(int)(((float)numCP) * rand()/(RAND_MAX+1.0));

    ind2 = marks[ind];

    /* write out that checkpoint */
    
    sprintf(nbuf,"tempRand.%d/%d",seed,ind2);

    newFP = fopen(nbuf,"r");
    while (!feof(newFP)) {
      size_t l;
      char buf[2048];
      l = fread(&buf[0],1,2048,newFP);
      if (l) fwrite(&buf[0],1,l,saveFP);
    }
    fclose(newFP);
    unlink(nbuf);

    /* remove the checkpoint number from the set */
    marks[ind] = marks[--numCP]; 
  }

  if (cliplast && savenumCP) {
    sprintf(nbuf,"tempRand.%d/%d",seed,savenumCP);
    unlink(nbuf);
  }
  sprintf(nbuf,"tempRand.%d",seed);
  rmdir(nbuf);

  /* and we're done */

  fromF->close();
  toF->close();
  delete toF;
  delete fromF;
}

void usage(const char *msg, int status) {
  FILE *fp = status ? stderr : stdout;

  if (msg) fprintf(fp,"Error: %s\n",msg);

  fprintf(fp,"ls-chkpt command [args...]\n\n");
  fprintf(fp, "Normal Commands:");
  fprintf(fp,"  help                           - this message\n");
  fprintf(fp,"  dump file1                     - print contents of file1\n");
  fprintf(fp,"  fdump file1                    - print " 
	  "full contents of file1\n");
  fprintf(fp,"  extract f1 f2 start cnt [skip] - extract checkpoints\n");
  fprintf(fp,"  randomize f1 f2 seed [clip]    - randomize f1 to f2\n");
  fprintf(fp,"  compress file1 file2           - compress file1 to file2\n");
  fprintf(fp,"  uncompress file1 file2         - uncompress file1 to file2\n");

  fprintf(fp,"\nTesting commands:\n");
  fprintf(fp,"    read file1                        - read a test chkpt\n");
  fprintf(fp,"    write                             - write a test chkpt\n");
  fprintf(fp,"    writec                            - write a test chkpt\n\n");

  exit(status);
}


int main(int argc, char *argv[], char **envp) {

  if (argc < 2) usage("Missing command",1);

  if (!strcmp(argv[1],"help")) usage(0,0);
  else if (!strcmp(argv[1],"write")) test_write(false);
  else if (!strcmp(argv[1],"writec")) test_write(true);

  else if (!strcmp(argv[1],"read")) {
    if (argc < 3) usage("Missing file name",1);
    test_read(argv[2]);
  }

  else if (!strcmp(argv[1],"compress")) {
    if (argc < 4) usage("Missing file names",1);
    copy_file(argv[2], argv[3], true);
  }  

  else if (!strcmp(argv[1],"uncompress")) {
    if (argc < 4) usage("Missing file names",1);
    copy_file(argv[2], argv[3], false);
  }  

  else if (!strcmp(argv[1],"dump")) {
    if (argc < 3) usage("Missing file name",1);
    dump_file(argv[2],0);
  }
  
  else if (!strcmp(argv[1],"fdump")) {
    if (argc < 3) usage("Missing file name",1);
    dump_file(argv[2],1);
  }
  
  else if (!strcmp(argv[1],"extract")) {
    if (argc < 6) usage("Missing argument",1);
    {
      int extract, num, skip=1;
      sscanf(argv[4],"%d",&extract);
      sscanf(argv[5],"%d",&num);
      if (argc > 6)
	sscanf(argv[6],"%d",&skip);
      extract_file(argv[2],argv[3],extract, num,skip);
    }
  }
  
  else if (!strcmp(argv[1],"randomize")) {
    if (argc < 5) usage("Missing argument",1);
    {
      int seed;
      sscanf(argv[4],"%d",&seed);
      randomize_file(argv[2],argv[3],seed,argc>5);
    }
  }
  
  else return 1;

  return 0;
}
