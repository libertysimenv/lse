/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * header for implementation of internal data structure/ASN.1 BER mappings
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file is loads of fun!
 *
 * TODO:
 * - asn encoding functions...
 *   BITSTRING, EXTERNAL, REAL, 
 *   UNIVERSALTIME, GENERALIZEDTIME
 *   FILE
 */

#ifndef _ASN1LIB_H_
#define _ASN1LIB_H_

#include "LSE_chkpt.h"
#include <string.h> /* guaranteed to get size_t */

namespace LSE_chkpt {

  typedef size_t (*writer_t)(void *dummy, const unsigned char *src,
			     size_t len);
  typedef size_t (*reader_t)(void *dummy, unsigned char *dest,
			     size_t len, error_t *errSeen);

  size_t encode_data_internal(writer_t writer, void *wparam, data_t *dp);
  
  size_t decode_data_internal(reader_t reader, void *rparam,
			      acceptor_t acceptor, data_t **dp, 
			      size_t amtToRead, error_t *errSeen);

  size_t decode_taglen_internal(reader_t reader, void *rparam,
				acceptor_t acceptor, data_t **dp, 
				size_t amtToRead, error_t *errSeen);

  size_t decode_body_internal(reader_t reader, void *rparam,
			      acceptor_t acceptor, data_t *dp, 
			      size_t amtToRead, error_t *errSeen);

  void skipper(int cclass, int tag, int *formatTag);

  static const int TAG_CHKPTFILEHEADER = 1;
  static const int TAG_CHECKPOINT = 2;

} 
#endif
