/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Implementation of internal data structure/ASN.1 BER mappings
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file is loads of fun!
 *
 * Note: build functions should adopt the child to the parent as the last
 *       operation so that if build fails the parent is not messed up.
 *
 * TODO:
 * - asn encoding/decoding functions...
 *   BITSTRING, EXTERNAL, REAL, 
 *   UNIVERSALTIME, GENERALIZEDTIME
 *   FILE
 */

#include "asn1lib.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

namespace LSE_chkpt {

  /***************** encoding details *********************/

  static inline unsigned int lenSize(unsigned int size) {
    if (size < (0x80)) {
      return 1;
    } else if (size < (0x100)) {
      return 2;
    } else if (size < 0x10000) {
      return 3;
    } else if (size < 0x1000000) {
      return 4;
    } else {
      return 5;
    }
  }

  static inline unsigned int
  encodelen(unsigned char *buf, unsigned int len) {
    unsigned int ls,i;
    if (len<128) {
      *(buf++) = len;
      return 1;
    } else {
      ls = lenSize(len)-1;
      *(buf++) = (unsigned char)(ls|0x80);
      for (i=ls;i>0;) {
	*(buf++) = (unsigned char)(len >> ((--i)*8));
      }
      return ls+1;
    }
  }

  static inline void
  encode_fixed_len(unsigned char *buf, unsigned int ls, unsigned int len) {
    unsigned int i;
    for (i=ls;i>0;) {
      *(buf++) = (unsigned char)(len >> ((--i)*8));
    }
    return;
  }

  static inline unsigned int
  uintSize(uint64_t val) {
    unsigned int i;
    for (i=9;i>1;) {
      if ((val>>(--i*8-1))&0x1ff) return i+1;
    }
    return 1;
  }

  static inline unsigned int
  sintSize(int64_t val) {
    unsigned int i;
    if (val < 0) {
      for (i=9;i>1;) {
	if ( ((val>>(--i*8-1))&0x1ff) != 0x1ff) return i+1;
      }
      return 1;
    } else {
      for (i=9;i>1;) {
	if ((val>>(--i*8-1))&0x1ff) return i+1;
      }
      return 1;
    }
  }

  static inline int
  encodeoid_component(unsigned char *buf, unsigned int val)
  {
    unsigned char c;
    int i, cnt = 0;

    for (i=5;i>0;) {
      c = (val >> (7 * --i)) & 0x7f;
      if (cnt || c || i==0) {
	buf[cnt] = c | (i>0 ? 0x80 : 0);
	cnt++;
      }
    }
    return cnt;
  }

  static inline int
  decodeoid_component(const unsigned char *buf, unsigned int *val)
  {
    int nval=0, cnt = 0;
    do {
      nval = (nval << 7) | (buf[cnt++] & 0x7f);
    } while (buf[cnt-1] & 0x80);
    *val = nval;
    return cnt;
  }

  static inline int
  encodetag_component(unsigned char *buf, unsigned int val)
  {
    unsigned char c;
    int i, cnt = 0;

    for (i=5;i>0;) {
      c = (val >> (7 * --i)) & 0x7f;
      if (cnt || c || i==0) {
	buf[cnt] = c | (i>0 ? 0x80 : 0);
	cnt++;
      }
    }
    return cnt;
  }

  /************** structure handling *********************/

  void data_t::adopt_child(data_t *child) {
    child->parent = this;
    if (!this) return;
    if (!oldestChild) oldestChild = child;
    if (youngestChild) youngestChild->sibling = child;
    youngestChild = child;
    return;
  }

  void data_t::adopt_parent(data_t *p) {
    parent = p;
    if (!p) return;
    if (!p->oldestChild) p->oldestChild = this;
    if (p->youngestChild) p->youngestChild->sibling = this;
    p->youngestChild = this;
    return;
  }

  data_t *
  build_boolean(data_t *parent, bool val) {
    data_t *np;

    np = new data_t();
    np->allocated = false;
    np->actualTag = np->formatTag = TAG_BOOLEAN;
    np->tagClass = 0;
    np->sibling = np->youngestChild = np->oldestChild = NULL;

    np->size = 1;
    np->content.booleanVal = val;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_unsigned(data_t *parent, uint64_t val) {
    data_t *np;

    np = new data_t();
    np->allocated = false;
    np->actualTag = np->formatTag = TAG_INTEGER;
    np->tagClass = 0;
    np->sibling = np->youngestChild = np->oldestChild = NULL;

    np->size = uintSize(val);
    np->content.uint64Val = val;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_signed(data_t *parent, int64_t val) {
    data_t *np;

    np = new data_t();
    np->allocated = false;
    np->actualTag =  np->formatTag = TAG_INTEGER;
    np->tagClass = 0;
    np->sibling = np->youngestChild = np->oldestChild = NULL;

    np->size = sintSize(val);
    np->content.int64Val = val;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_enumerated(data_t *parent, unsigned int val) {
    data_t *np;

    np = new data_t();
    np->allocated = false;
    np->actualTag = np->formatTag = TAG_ENUMERATED;
    np->tagClass = 0;
    np->sibling = np->youngestChild = np->oldestChild = NULL;

    np->size = uintSize(val);
    np->content.uint64Val = val;

    np->adopt_parent(parent);
    return np;
  }


  data_t *
  build_null(data_t *parent) {
    data_t *np;

    np = new data_t();
    np->allocated = false;
    np->actualTag = np->formatTag = TAG_NULL;
    np->tagClass = 0;
    np->size = 0;
    np->sibling = np->youngestChild = np->oldestChild = NULL;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_sequence(data_t *parent) {
    data_t *np;

    np = new data_t();
    np->allocated = false;
    np->actualTag = np->formatTag = TAG_SEQUENCE;
    np->tagClass = CONSTRUCTED;
    np->size = -1;
    np->sibling = np->youngestChild = np->oldestChild = NULL;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_set(data_t *parent) {
    data_t *np;

    np = new data_t();
    np->allocated = false;
    np->actualTag = np->formatTag = TAG_SET;
    np->tagClass = CONSTRUCTED;
    np->size = -1;
    np->sibling = np->youngestChild = np->oldestChild = NULL;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_objectid(data_t *parent, unsigned int *dv, int dlen) {
    data_t *np;
    int i,scount;
    unsigned char tbuf[6];

    np = new data_t();
    np->allocated = true;
    np->actualTag = np->formatTag = TAG_OBJECTID;
    np->tagClass = 0;
    np->content.oid.buffer 
      = (unsigned int *)malloc(dlen*sizeof(unsigned int)+1);
    np->content.oid.length = dlen;
    if (!np->content.oid.buffer) {
      delete np;
      return NULL;
    }

    /* copy over the object ID */
    memcpy(np->content.oid.buffer,dv,dlen * sizeof(unsigned int));

    /* just figure out how long it is */
    scount = 0;
    scount += encodeoid_component(tbuf,dv[0]*40+dv[1]);
    for (i=2;i<dlen;i++) {
      scount += encodeoid_component(tbuf,dv[i]);
    }
    np->size = scount;
    np->sibling = NULL;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_relativeoid(data_t *parent, unsigned int *dv, int dlen) {
    data_t *np;
    int i,scount;
    unsigned char tbuf[6];

    np = new data_t();
    np->allocated = true;
    np->actualTag = np->formatTag = TAG_RELATIVEOID;
    np->tagClass = 0;
    np->content.oid.buffer = (unsigned int *)malloc(sizeof(int) * dlen+1);
    np->content.oid.length = dlen;
    if (!np->content.oid.buffer) {
      delete np;
      return NULL;
    }
    memcpy(np->content.oid.buffer,dv,dlen * sizeof(unsigned int));
  
    scount = 0;
    for (i=0;i<dlen;i++) {
      scount += encodeoid_component(tbuf,dv[i]);
    }
    np->size = scount;
    np->sibling = NULL;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_restrictedstring(data_t *parent, int sclass,
			 const unsigned char *dv, int slen, bool copy) {
    data_t *np;

    np = new data_t();
    np->allocated = copy && dv;
    np->actualTag = np->formatTag = sclass;
    np->tagClass = 0;

    if (!dv) {
      np->sibling = np->youngestChild = np->oldestChild = NULL;
      np->size = -1;
    } else {
      np->size = slen;
      np->sibling = NULL;
      if (copy) {
	if (slen) {
	  np->content.stringVal = (char *)malloc(slen);
	} else np->content.stringVal = (char *)malloc(1);
	if (!np->content.stringVal) {
	  delete np;
	  return NULL;
	}
	memcpy(np->content.stringVal,dv,slen);
      } else {
	np->content.stringVal = (char *)dv;
      }
    }

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_octetstring(data_t *parent, 
		    const unsigned char *str, int slen, bool copy) {
    return build_restrictedstring(parent, TAG_OCTETSTRING, 
				  str, slen, copy);
  }

  data_t *
  build_objectdescriptor(data_t *parent, 
			 const unsigned char *str, bool copy) {
    return build_restrictedstring(parent, TAG_OBJECTDESC, 
				  str, strlen((char *)str), copy);
  }

  data_t *
  build_string(data_t *parent, const char *str, bool copy) {
    return build_restrictedstring(parent, TAG_UTF8STRING, 
				  (unsigned char *)str, strlen(str), copy);
  }

  data_t *
  build_stringN(data_t *parent, const char *str, int slen, bool copy) {
    return build_restrictedstring(parent, TAG_UTF8STRING, 
				  (unsigned char *)str, slen, copy);
  }

  data_t *
  build_unrestricted_string(data_t *parent, data_t *idval,
			    const unsigned char *str, int slen, bool dcopy) {
    data_t *np, *np2;

    np = new data_t();
    np->allocated = false;
    np->actualTag = np->formatTag = TAG_UNRESTRICTEDSTRING;
    np->tagClass = CONSTRUCTED;
    np->size = -1;
    np->sibling = np->youngestChild = np->oldestChild = NULL;
    np->content.pdv.identification = idval;
    np->content.pdv.idAllocated = false;
    np2 = build_restrictedstring(np, TAG_OCTETSTRING, 
				 str, slen, dcopy);
    if (!np2) {
      delete np;
      return NULL;
    }
    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_embeddedpdv(data_t *parent, data_t *idval,
		    const unsigned char *str, int slen, bool dcopy) {
    data_t *np, *np2;

    np = new data_t();
    np->allocated = false;
    np->actualTag = np->formatTag = TAG_EMBEDDEDPDV;
    np->tagClass = CONSTRUCTED;
    np->size = -1;
    np->sibling = np->youngestChild = np->oldestChild = NULL;
    np->content.pdv.identification = idval;
    np->content.pdv.idAllocated = false;
    np2 = build_restrictedstring(np, TAG_OCTETSTRING, 
				 str, slen, dcopy);
    if (!np2) {
      delete np;
      return NULL;
    }
    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_explicit_tag(data_t *parent, int actualTag, int tagClass) {
    data_t *np;

    np = new data_t();
    np->allocated = false;
    np->actualTag = actualTag;
    np->formatTag = TAG_CLASS(tagClass) == CLASS_UNIVERSAL ?
      actualTag : TAG_SEQUENCE; /* nice and safe... */
    np->tagClass = CONSTRUCTED | tagClass;
    np->size = -1;
    np->sibling = np->youngestChild = np->oldestChild = NULL;

    np->adopt_parent(parent);
    return np;
  }

  data_t *
  build_encoded(data_t *parent, const unsigned char *str, int slen, bool copy) {
    return build_restrictedstring(parent, TAG_ENCODED, 
				  str, slen, copy);
  }

  data_t *
  build_header(data_t *parent, int actualTag, int tagClass, int size) {
    unsigned char buf[32];
    size_t tlen, llen;

    /* encode tag and length */
    if (actualTag < 31) {
      buf[0] = actualTag | tagClass;
      tlen = 1;
    } else {
      buf[0] = tagClass | 0x1f;
      tlen = 1 + encodetag_component(buf+1,actualTag);
    }
    if (!IS_CONSTRUCTED(tagClass) || size >= 0) {
      llen = encodelen(buf+tlen,size);
    } else { 
      llen = 1;
      buf[tlen] = 0x80;
    }
    return build_encoded(parent,buf,(int)(llen+tlen),true);
  }

  static unsigned char nullbuf[2] = { 0,0 };

  data_t *
  build_indefinite_end(data_t *parent) {
    return build_encoded(parent,nullbuf,2,true);
  }

  error_t
  data_t::change_tag(int newtag, int newclass) {
    actualTag = newtag;
    tagClass &= CONSTRUCTED;
    tagClass |= (newclass & ~CONSTRUCTED);
    return error_None;
  }

  data_t::~data_t() {
    if (allocated) free(content.genericAllocated);
    if ((formatTag == TAG_UNRESTRICTEDSTRING ||
	 formatTag == TAG_EMBEDDEDPDV) &&
	content.pdv.idAllocated) 
      free(content.pdv.identification);
    if (IS_CONSTRUCTED(tagClass)) {
      data_t *q,*p=oldestChild;
      while (p) {
	q = p->sibling;
	delete p;
	p = q;
      }
    }
  }

  data_t *data_t::copy_data() {
    data_t *np, *t, *nt;

    if (!this) return NULL; /* allow NULLs */
  
    np = new data_t();
    np->actualTag = actualTag;
    np->formatTag = formatTag;
    np->tagClass = tagClass;
    np->size = size;

    np->parent = np->sibling = np->oldestChild = np->youngestChild = NULL;

    if (IS_CONSTRUCTED(tagClass)) {

      t = oldestChild;
      while (t) {
	nt = t->copy_data();
	if (!nt) {
	  delete np;
	  return NULL;
	}
	nt->adopt_parent(np);
	t=t->sibling;
      }

      if (formatTag == TAG_UNRESTRICTEDSTRING ||
	  formatTag == TAG_EMBEDDEDPDV) {
	np->content.pdv.idAllocated = false;
	if (content.pdv.idAllocated) {
	  nt = content.pdv.identification->copy_data();
	  if (!nt) {
	    delete np;
	    return NULL;
	  }
	  np->content.pdv.identification = nt;
	  np->content.pdv.idAllocated=true;
	}
      } /* embedded PDV */

    } else { /* primitive */
    
      switch (formatTag) {
      case TAG_BOOLEAN:
	np->content.booleanVal = content.booleanVal;
	break;
      case TAG_INTEGER:
      case TAG_ENUMERATED:
	np->content.uint64Val = content.uint64Val;
	break;
      
      case TAG_BITSTRING: /* TODO: what is in-memory representation? */
	break;

      case TAG_OCTETSTRING:
      case TAG_UTF8STRING:
      case TAG_NUMERICSTRING:
      case TAG_PRINTABLESTRING:
      case TAG_TELETEXSTRING:
      case TAG_VIDEOTEXSTRING:
      case TAG_IA5STRING:
      case TAG_GRAPHICSTRING:
      case TAG_VISIBLESTRING:
      case TAG_GENERALSTRING:
      case TAG_UNIVERSALSTRING:
      case TAG_BMPSTRING:
      case TAG_OBJECTDESC:

	np->content.stringVal = (char *)malloc(size);
	if (!np->content.stringVal) {
	  delete np;
	  return NULL;
	}
	memcpy(np->content.stringVal, content.stringVal,size);
	break;

      case TAG_OBJECTID:
      case TAG_RELATIVEOID:

	np->content.oid.length = content.oid.length;
	np->content.oid.buffer = 
	  (unsigned int *)malloc(content.oid.length * sizeof(unsigned int)+1);

	if (!np->content.oid.buffer) {
	  delete np;
	  return NULL;
	}
	memcpy(np->content.oid.buffer, content.oid.buffer,
	       content.oid.length * sizeof(unsigned int));
	break;

      case TAG_EXTERNAL:
      case TAG_REAL:
      case TAG_UNIVERSALTIME:
      case TAG_GENERALIZEDTIME:
      default:
	break;
      }
    } /* primitive */
    return np;
  }

  /****************************** encoding stuff ****************************/

  /* calculate lengths so we can use definite sizing.... */
  unsigned int data_t::update_size() {
    if (!IS_CONSTRUCTED(tagClass)) {
      return size + 1 + lenSize(size);
    } else {
      unsigned int size = 0;
      data_t *q,*p=oldestChild;
      while (p) {
	q = p->sibling;
	size += p->update_size(); /* NOTE: guaranteed to not return 0 */
	p = q;
      }
      this->size = size;
      return size + 1 + lenSize(size);
    }
  }

  size_t
  encode_data_internal(writer_t writer, void *wparam, data_t *dp) {
    unsigned char terminator[] = { 0, 0 };
    unsigned char garbage[] = { 0xff };
    size_t llen, tlen, amt, camt;
    unsigned int i,j;
    unsigned char buf[32];

    if (!dp) return 0;

    if (dp->actualTag == TAG_ENCODED && dp->tagClass==0) {
      return (*writer)(wparam,(unsigned char *)dp->content.genericAllocated,
		       dp->size);
    }

    /* write tag and length */
    if (dp->actualTag < 31) {
      buf[0] = dp->actualTag | dp->tagClass;
      tlen = 1;
    } else {
      buf[0] = dp->tagClass | 0x1f;
      tlen = 1 + encodetag_component(buf+1,dp->actualTag);
    }
    if (!IS_CONSTRUCTED(dp->tagClass) || dp->size >= 0) {
      llen = encodelen(buf+tlen,dp->size);
    } else { 
      llen = 1;
      buf[tlen] = 0x80;
    }
    amt = llen + tlen;
    camt = (*writer)(wparam,buf,llen+tlen);
    if (amt != camt) return 0;

    if (IS_CONSTRUCTED(dp->tagClass)) {
      data_t *q,*p=dp->oldestChild;

      switch (dp->formatTag) {
      case TAG_UNRESTRICTEDSTRING:
      case TAG_EMBEDDEDPDV:
	camt = encode_data_internal(writer, wparam,
				    dp->content.pdv.identification);
	if (!camt) return 0;
	amt += camt;
	break;
      default:
	break;
      }
    
      while (p) {
	q = p->sibling;
	camt = encode_data_internal(writer, wparam, p);
	if (!camt) return 0;
	amt += camt;
	p = q;
      }

      /* covers...
       * TAG_SET, TAG_SEQUENCE
       * various strings done as constructed instead of primitive types...
       */

    } else { /* primitive... size must be set */

      switch (dp->formatTag) {
      case TAG_BOOLEAN:
	buf[0] = dp->content.booleanVal?0xff:0;
	camt = (*writer)(wparam, buf, 1);
	if (!camt) return 0;
	amt += camt;
	break;
      case TAG_INTEGER:
      case TAG_ENUMERATED:
	/* should be able to use signed or unsigned for this, since we do not 
	 * care about the sign extension after having figured out the proper
	 * length...
	 */
	for (i=dp->size,j=0;i>0;j++) {
	  if (i>=9) {
	    buf[j] = 0;
	    --i;
	  } else {
	    buf[j] = (dp->content.uint64Val>>(--i*8)) & 0xff;
	  } 
	}
	camt = (*writer)(wparam, buf, j);
	if (!camt) return 0;
	amt += camt;
	break;

      case TAG_BITSTRING: /* TODO: what is in-memory representation? */
	break;

      case TAG_OCTETSTRING:
      case TAG_UTF8STRING:
      case TAG_NUMERICSTRING:
      case TAG_PRINTABLESTRING:
      case TAG_TELETEXSTRING:
      case TAG_VIDEOTEXSTRING:
      case TAG_IA5STRING:
      case TAG_GRAPHICSTRING:
      case TAG_VISIBLESTRING:
      case TAG_GENERALSTRING:
      case TAG_UNIVERSALSTRING:
      case TAG_BMPSTRING:
      case TAG_OBJECTDESC:
	if (dp->size) {
	  camt = (*writer)(wparam, 
			   (unsigned char *)dp->content.stringVal, dp->size);
	  if (!camt) return 0;
	  amt += camt;
	}
	break;

      case TAG_OBJECTID:
	{
	  unsigned char *tbuf;
	  int i,scount;
	  unsigned int *dv = dp->content.oid.buffer;

	  tbuf = (unsigned char *)malloc(5 * dp->content.oid.length + 1);
	  if (!tbuf) return 0;

	  scount = 0;
	  scount += encodeoid_component(tbuf,dv[0]*40+dv[1]);
	  for (i=2;i<dp->content.oid.length;i++) {
	    scount += encodeoid_component(tbuf+scount,dv[i]);
	  }
	  camt = (*writer)(wparam, tbuf, scount);
	  free(tbuf);
	  if (!camt) return 0;
	  amt += camt;
	}
	break;

      case TAG_RELATIVEOID:
	{
	  unsigned char *tbuf;
	  int i,scount;
	  unsigned int *dv = dp->content.oid.buffer;

	  tbuf = (unsigned char *)malloc(5 * dp->content.oid.length + 1);
	  if (!tbuf) return 0;

	  scount = 0;
	  for (i=0;i<dp->content.oid.length;i++) {
	    scount += encodeoid_component(tbuf+scount,dv[i]);
	  }
	  camt = (*writer)(wparam, tbuf, scount);
	  free(tbuf);
	  if (!camt && scount) return 0;
	  amt += camt;
	}
	break;

      case TAG_EXTERNAL:
      case TAG_REAL:
      case TAG_UNIVERSALTIME:
      case TAG_GENERALIZEDTIME:
      default:
	/* fill with nonsense equal to the size */
	for (i=0;i<dp->size;i++) {
	  camt = (*writer)(wparam, garbage, 1);
	  if (!camt) return 0;
	}
	amt += dp->size;
	break;

      } /* formatTag */

    } /* !constructed */

    if (dp->size < 0) {
      camt = (*writer)(wparam, terminator, 2);
      if (!camt) return 0;
      amt += camt;
    }
    return amt;
  }

  static size_t
  file_writer(void *fpdummy, const unsigned char *buf, size_t len) {
    size_t alen;
    alen = fwrite(buf,1,len,(FILE *)fpdummy);
    return (alen != len) ? 0 : len;
  }

  typedef struct {
    unsigned char *buf;
    size_t len;
  } mem_writer_t;

  static size_t
  mem_writer(void *mbufdummy, const unsigned char *buf, size_t len) {
    mem_writer_t *mbuf = (mem_writer_t *)mbufdummy;
    if (len > mbuf->len) return 0;
    memcpy(mbuf->buf, buf, len);
    mbuf->buf += len;
    mbuf->len -= len;
    return len;
  }

  error_t
  write_data(FILE *fp, data_t *dp) {
    size_t amt;
    amt = encode_data_internal(file_writer,(void *)fp, dp);
    if (!amt && dp!=NULL) return error_IO;
    else return error_None;
  }

  error_t
  encode_data(unsigned char *buf, size_t maxlen, data_t *dp,
	      size_t *actualSize) {
    mem_writer_t mbuf;
    mbuf.buf = buf;
    mbuf.len = maxlen;
    *actualSize = encode_data_internal(mem_writer,(void *)&mbuf,dp);
    if (!*actualSize && dp) return error_BufferOverflow;
    else return error_None;
  }


  /***************************** decoding functions **********************/

  static inline size_t
  decode_tag_and_len(reader_t reader, void *rparam, int *tag, int *tclass, 
		     int *clen, size_t maxlen, error_t *errSeen) {
    unsigned int flen, ftag;
    unsigned char buf[5]; /* should be plenty big; we prevent overflow... */
    size_t rlen, tlen;


    /* get the tag information.  Read two bytes as an optimization for the
     * common case 
     */
    if (maxlen && maxlen < 2) {
      *errSeen = error_FileFormat;
      return 0;
    }
    rlen = (*reader)(rparam, buf, 2, errSeen);
    if (rlen != 2) return 0;
    *tclass = buf[0] & 0xe0;

    ftag = buf[0] & 0x1f;
    if (ftag != 31) {
      *tag = ftag;
      tlen = 1;
    } else { /* extended tag, so next part is part... */
      ftag = buf[1] & 0x7f;
      tlen=2;
      while (buf[1] & 0x80) {
	if (maxlen == tlen) {
	  *errSeen = error_FileFormat;
	  return 0; /* ran out of space */
	}
	rlen = (*reader)(rparam,buf+1,1,errSeen);
	if (!rlen) return 0;
	ftag = (ftag << 7) + (buf[1] & 0x7f);
	tlen++;
      }
      *tag = ftag;
      /* and get the first byte of length into the buffer */
      if (maxlen == tlen) {
	*errSeen = error_FileFormat;
	return 0; /* ran out of space */
      }
      rlen = (*reader)(rparam,buf+1,1,errSeen); 
      if (!rlen) return 0;
    }

    /* first byte of length information will be in buf[1] */
  
    if (buf[1] == 0x80) {
      *clen = -1;
      return tlen + 1;
    } else if (!(buf[1] & 0x80)) {
      *clen = buf[1] & 0x7f;
      return tlen + 1;
    } else {
      int sz = buf[1] & 0x7f;
      int i;
      if (maxlen && maxlen < tlen+1+sz) {
	*errSeen = error_FileFormat;
	return 0; /* ran out of space */
      }
      flen = 0;
      if (sz > 5) {
	for (i=0;i<sz;i++) {
	  rlen = (*reader)(rparam,buf,1,errSeen);
	  if (!rlen) return 0;
	  flen = (flen << 8) + buf[0];
	}
      }
      else {
	rlen = (*reader)(rparam, buf, sz, errSeen);
	if (!rlen) return 0;
	for (i=0;i<sz;i++) {
	  flen = (flen<<8) + buf[i];
	}
      }
      *clen = flen;
      return tlen + sz + 1;
    }
  }

  void skipper(int cclass, int tag, int *formatTag) {
    *formatTag = 0;
  }

  size_t
  decode_taglen_internal(reader_t reader, void *rparam, acceptor_t acceptor,
			 data_t **dp, size_t maxToRead, error_t *errSeen) {
    data_t *np;
    size_t hlen;
    int tag, cclass, len, formatTag;

    hlen=decode_tag_and_len(reader, rparam, &tag, &cclass, &len, maxToRead,
			    errSeen);
    if (!hlen) return 0;
    if (maxToRead && len >= 0 && (hlen + len > maxToRead)) {
      *errSeen = error_FileFormat;
      return 0;
    }

    if (!IS_CONSTRUCTED(cclass) && len < 0) {
      *errSeen = error_FileFormat;
      return 0;
    }

    /* end of indefinite length constructed type */
    if (TAG_CLASS(cclass) == CLASS_UNIVERSAL && tag == 0) {
      if (dp) *dp = NULL;
      return hlen;
    }

    formatTag = (TAG_CLASS(cclass) == CLASS_UNIVERSAL ? tag :
		 IS_CONSTRUCTED(cclass) ? TAG_SEQUENCE :
		 TAG_OCTETSTRING);
    if (acceptor) (*acceptor)(cclass, tag, &formatTag);

    np = new data_t();
    np->actualTag = tag;
    np->formatTag = formatTag;
    np->tagClass = cclass;
    np->size = len;
    np->sibling = np->youngestChild = np->oldestChild = np->parent = NULL;
    np->allocated = false;

    if (dp) *dp = np;
    else delete np;
    return hlen;
  }

  size_t
  decode_body_internal(reader_t reader, void *rparam, acceptor_t acceptor,
		       data_t *np, size_t maxToRead, error_t *errSeen) {
    size_t clen, totlen;
    int cclass, len, formatTag;

    len = np->size;
    totlen = 0;
    cclass = np->tagClass;
    formatTag = np->formatTag;

    /* now need to figure out content... */
    if (IS_CONSTRUCTED(cclass)) { /* constructed are all the same */
      data_t *child;

      /* if we do not know how long to go... */

      if (len < 0) {
	bool checkit = maxToRead!=0;

	do {
	  if (checkit && !maxToRead) {
	    *errSeen = error_FileFormat;
	    goto cleanup;
	  }
	  /* find a child */
	  clen = decode_data_internal(reader, rparam, 
				      formatTag ? acceptor : 
				      skipper, 
				      &child, maxToRead, errSeen);
	  if (!clen) goto cleanup;
	  if (checkit) maxToRead -= clen;
	  totlen += clen;
	  if (child && formatTag) child->adopt_parent(np);
	} while (child);

      } else {
	if (formatTag) {
	  int lremaining = len;
	  while (lremaining > 0) {
	    /* find a child */
	    clen = decode_data_internal(reader, rparam, 
					acceptor, &child,
					lremaining, errSeen);
	    if (!clen) goto cleanup;
	    lremaining -= clen;
	    if (child) child->adopt_parent(np);
	    else break;
	  }
	  if (lremaining) { /* should not be any leftovers */
	    *errSeen = error_FileFormat;
	    goto cleanup;
	  }
	} else {
	  /* pop ahead by length */
	  clen = (*reader)(rparam, NULL, len, errSeen);
	  if (!clen) goto cleanup;
	}
	totlen += len;
      }

      /* and the weird ones... */
      if (formatTag == TAG_UNRESTRICTEDSTRING ||
	  formatTag == TAG_EMBEDDEDPDV) {
	if (!np->oldestChild) {
	  *errSeen = error_FileFormat;
	  goto cleanup;
	}
	np->content.pdv.identification = np->oldestChild;
	np->content.pdv.idAllocated = true;
	np->oldestChild = np->oldestChild->sibling;
      }

    } else { /* primitive  */

      switch (formatTag) {

      case 0:
	/* pop ahead by length */
	clen = (*reader)(rparam, NULL, len, errSeen);
	if (!clen) goto cleanup;
	break;

      case TAG_BOOLEAN:
	{
	  unsigned char c;
	  if (len > 1) {
	    *errSeen = error_FileFormat;
	    goto cleanup;
	  }
	  clen = (*reader)(rparam, &c, 1, errSeen);
	  if (!clen) goto cleanup;
	  np->content.booleanVal = (c != 0);
	}
	break;

      case TAG_INTEGER:
      case TAG_ENUMERATED:
	{
	  uint64_t nval;
	  unsigned char ibuf[10];
	  int i;
	
	  if (len < 10) {
	    clen=(*reader)(rparam,ibuf,len,errSeen);
	    if (!clen) return 0;
	    if (ibuf[0] & 0x80) nval = ~UINT64_C(0);
	    else nval = 0;
	    for (i=0;i<len;i++) {
	      nval = (nval<<8)|ibuf[i];
	    }
	  } else {
	    int togo = len - 1;
	    clen=(*reader)(rparam,ibuf,1,errSeen);
	    if (!clen) return 0;
	    if (ibuf[0] & 0x80) nval = ~UINT64_C(0);
	    else nval = 0;
	    nval = (nval<<8)|ibuf[0];
	    while (togo > 0) {
	      clen=(*reader)(rparam,ibuf,togo>10?10:togo,errSeen);
	      if (!clen) return 0;
	      for (i=0;i<len;i++) {
		nval = (nval<<8)|ibuf[i];
	      }
	      togo -= clen;
	    }
	  }
	  np->content.uint64Val = nval;
	}
	break;

      case TAG_OCTETSTRING:
      case TAG_UTF8STRING:
      case TAG_NUMERICSTRING:
      case TAG_PRINTABLESTRING:
      case TAG_TELETEXSTRING:
      case TAG_VIDEOTEXSTRING:
      case TAG_IA5STRING:
      case TAG_GRAPHICSTRING:
      case TAG_VISIBLESTRING:
      case TAG_GENERALSTRING:
      case TAG_UNIVERSALSTRING:
      case TAG_BMPSTRING:
      case TAG_OBJECTDESC:
	np->content.stringVal = (char *)malloc(len+1);
	if (!np->content.stringVal) {
	  *errSeen = error_OutOfMemory;
	  goto cleanup;
	}
	np->allocated = true;
	if (len > 0) {
	  clen = (*reader)(rparam, (unsigned char *)np->content.stringVal, 
			   len, errSeen);
	  if (!clen) {
	    free(np->content.stringVal);
	    goto cleanup;
	  }
	}
	np->content.stringVal[len] = 0;
	break;

      case TAG_OBJECTID:
      case TAG_RELATIVEOID:
	{
	  unsigned char *tbuf;
	  int dlen = 0, scount;

	  tbuf = (unsigned char *)malloc(len+1);
	  if (!tbuf) {
	    *errSeen = error_OutOfMemory;
	    goto cleanup;
	  }

	  /* oversize the buffer for safety's sake */
	  np->content.oid.buffer = 
	    (unsigned int *)malloc(sizeof(unsigned int)*(len+1));
	  if (!np->content.oid.buffer) {
	    free(tbuf);
	    *errSeen = error_OutOfMemory;
	    goto cleanup;
	  }
	  np->allocated = true;
	  if (len > 0) {
	    clen = (*reader)(rparam, tbuf, len, errSeen);
	    if (!clen) {
	      free(tbuf);
	      free(np->content.oid.buffer);
	      goto cleanup;
	    }
	  }

	  scount = 0;

	  if (formatTag == TAG_OBJECTID) {
	    unsigned int tmp;
	    scount += decodeoid_component(tbuf,&tmp);
	    dlen = 2;
	    /* very weird decoding because 0 and 1 root nodes are strange */
	    if (tmp < 40) {
	      np->content.oid.buffer[0] = 0;
	      np->content.oid.buffer[1] = 1;
	    } else if (tmp < 80) {
	      np->content.oid.buffer[0] = 1;
	      np->content.oid.buffer[1] = tmp-40;
	    } else {
	      np->content.oid.buffer[0] = 2;
	      np->content.oid.buffer[1] = tmp-80;
	    }
	  }

	  while (scount < len) {
	    scount += decodeoid_component(tbuf+scount,
					  np->content.oid.buffer+dlen);
	    dlen++;
	  }

	  np->content.oid.length = dlen;
	  free(tbuf);
	}
	break;

      case TAG_BITSTRING: /* TODO: what is in-memory representation? */
      case TAG_EXTERNAL:
      case TAG_REAL:
      case TAG_UNIVERSALTIME:
      case TAG_GENERALIZEDTIME:
      default:

	/* leave it for some poor chump to deal with; i.e. treat it as an
	 * octet string.  Note that it cannot be a primitive type, so we do
	 * know how long it should be...
	 */
	/* give it an extra 0 for strings */
	np->content.genericAllocated = (unsigned char *)malloc(len+1);
	if (!np->content.genericAllocated) {
	  *errSeen = error_OutOfMemory;
	  goto cleanup;
	}
	np->allocated = true;
	if (len > 0) {
	  clen = (*reader)(rparam, (unsigned char *)
			   np->content.genericAllocated, len, errSeen);
	  if (!clen) {
	    free(np->content.genericAllocated);
	    np->allocated = false;
	    goto cleanup;
	  }
	}
	((unsigned char *)np->content.genericAllocated)[len] = 0;
	break;
      } /* switch primitive tags */

      totlen += len;

    } /* else primitive */

    return totlen+1;
  cleanup:
    return 0;
  }

  size_t
  decode_data_internal(reader_t reader, void *rparam, acceptor_t acceptor,
		       data_t **dp, size_t maxToRead, error_t *errSeen) {
    data_t *np;
    size_t clen, totlen;

    totlen = decode_taglen_internal(reader, rparam, acceptor,
				    &np, maxToRead, errSeen);
    if (!totlen) return 0; /* error will be in errSeen */
    if (!np) { 
      if (dp) *dp = NULL;
      return totlen;
    }

    clen = decode_body_internal(reader, rparam, acceptor,
				np, maxToRead ? maxToRead - totlen : 0,
				errSeen);
    if (!clen) goto cleanup;

    totlen += clen-1; /* reported one extra... */
    if (dp) *dp = np;
    else delete np;
    return totlen;
  cleanup:
    delete np;
    return 0;
  }

  static size_t
  file_reader(void *fpdummy, unsigned char *buf, size_t len, error_t *errSeen) {
    size_t alen;
    if (buf) { 
      alen = fread(buf,1,len,(FILE *)fpdummy); 
    }
    else {
      long currpos,newpos;
      currpos = ftell((FILE *)fpdummy);
      fseek((FILE *)fpdummy,len,SEEK_CUR);
      newpos = ftell((FILE *)fpdummy);
      alen = newpos - currpos;
    }
    if (alen != len) {
      *errSeen = error_IO;
      return 0;
    } else return len;
  }

  static size_t
  mem_reader(void *mbufdummy, unsigned char *buf, size_t len, 
	     error_t *errSeen) {
    mem_writer_t *mbuf = (mem_writer_t *)mbufdummy;
    if (len > mbuf->len) return 0;
    if (buf) memcpy(buf,mbuf->buf,len);
    mbuf->buf += len;
    mbuf->len -= len;
    return len;
  }

  error_t
  read_data(FILE *fp, acceptor_t acceptor,
	    data_t **cpDataP, size_t maxToRead, size_t *actualP) {
    size_t amt;
    error_t rval = error_None;
    amt = decode_data_internal(file_reader,(void *)fp, acceptor,
			       cpDataP, maxToRead,&rval);
    if (!amt) return rval;
    if (actualP) *actualP = amt;
    return error_None;
  }

  error_t decode_data(unsigned char *buf, size_t len, acceptor_t acceptor,
		      data_t **cpDataP, size_t *actualP) {
    error_t rval = error_None;
    mem_writer_t mbuf;
    size_t amt;

    mbuf.buf = buf;
    mbuf.len = len;

    amt = decode_data_internal(mem_reader,(void *)&mbuf, acceptor,
			       cpDataP, len,&rval);
    if (!amt) return rval;
    if (actualP) *actualP = amt;
    return error_None;
  }

} // namespace LSE_chkpt
