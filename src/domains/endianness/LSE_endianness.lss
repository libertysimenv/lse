/* -*-c-*-
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Endianness domain description
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This LSS file is imported to allow use of the LSE endianness domain.
 * This domain is rather interesting in that it ignores both the implementation
 * and the instance; attempts to create new domain instances result in 
 * returning the same instance every time, and if the domain is ever imported,
 * there will be an instance.
 *
 */
package LSE_endianness;

var class_name = "LSE_endianness" : const string;
var inner_create = new domain(class_name) : const LSE_domain_constructor;

var checkpointer : domain ref;

checkpointer = inner_create("LSE_endianness_inst","","");

fun create() => domain ref {
  return checkpointer;
};

// create with dummy arguments so it looks like a "standard" creation...
fun create(a : string, b : string, c : string) => domain ref {
  return checkpointer;
};
