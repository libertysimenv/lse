/* -*-c++-*-
 * 
 * Copyright (c) 2008-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header file for dealing with clock
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Definitions of clock domain stuff.
 *
 */

#include <LSE_clock.h>
#include <iostream>

namespace LSE_clock {

  clock_t::clock_t(const std::string n) 
    : name(n), run_status(true), count(0), num(1), denom(1) {
  }

  clock_t::clock_t(const char *n) 
    : name(n), run_status(true), count(0), num(1), denom(1) {
  }

  void clock_t::add_todo(uint64_t c, LSE_time_method_t m, void *d, 
			 bool nosame) {
    if (c > count || !nosame && c == count) {
      todo.push(c, m, d);
    }// else std::cerr << "Dropping event " << c << " " << count << "\n";
  }

  void clock_t::run_todos_int() {
    LSE_timeact_t a = todo.top();
    
    while (a.t <= count) {
      
      (*a.method)(a.data);
      
      todo.pop();
      if (todo.empty()) return;
      a = todo.top();
    }
  }
  
} // namespace LSE_clock


