/* -*-c++-*-
 * 
 * Copyright (c) 2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header file for dealing with clock
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Definitions of clock domain stuff.
 *
 */

#ifndef _LSE_CLOCK_H_
#define _LSE_CLOCK_H_

#include <LSE_inttypes.h>
#include <LSE_time.h>
#include <string>

namespace LSE_clock {

  extern uint64_t timescale; // number of ticks per 

  class clock_t {
    std::string name;
    bool run_status;
    uint64_t count;
    uint64_t num, denom;
    LSE_time_queue todo;

    void run_todos_int();

  public:
    clock_t(const std::string n);
    clock_t(const char *);

    void set_freq(uint64_t n, uint64_t d) {
      num = n;
      denom = d;
    }

    void stop();
    void start();
    bool is_running() { return run_status; }

    void add_todo(uint64_t c, LSE_time_method_t m, void *d, bool nosame=false);

    uint64_t get_cycle() { return count; }
    void incr(uint64_t amt) { count += amt; }

    void run_todos() { 
      if (todo.empty()) return;
      if (todo.top().t > count) return;
      run_todos_int();
    }

    bool has_todo() { return !todo.empty(); }
    void skip_to_next() { if (!todo.empty()) count = todo.top().t; }
 
  }; // class clock_t

} // namespace LSE_clock

// Because this is a type that will be so common, I'm giving it a special
// name in the global space.

typedef LSE_clock::clock_t *LSE_clock_t;

#endif /* _LSE_CLOCK_H_ */

