# /* 
#  * Copyright (c) 2000-2008 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * LSE_clock domain class Python module
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Declares the LSE_clock domain class to LSE.
#  *
#  */

import LSE_domain, string, getopt

class LSE_DomainObject(LSE_domain.LSE_BaseDomainObject):

    # domain class name
    className = "LSE_clock"

    classHeaders = [ "LSE_clock.h" ]

    classLibraries = "-lLSEclock"
    
    classNamespaces = [ "LSE_clock" ]

    createIfRequired = 1
    
    classIdentifiers = []
    
    classHooks = []

    changesTerminateCount = 0
    
    # init method
    def __init__(self, instname, buildArgs, runArgs, bp):
        LSE_domain.LSE_BaseDomainObject.__init__(self,instname,buildArgs,
                                                 runArgs, bp)

        self.buildArgs = buildArgs
        self.runArgs = runArgs

        # parse the build arguments...
        buildArgsList = string.split(buildArgs)
        try:
            opts, args = getopt.getopt(buildArgsList, "",
                                      ["freq=",
                                       ])
        except getopt.GetoptError, (msg,opt):
                s = "Error %s in domain build arguments for instance '%s'" % \
                    (s,msg)
                raise LSE_domain.LSE_DomainException(s)

        self.noInitialContexts = 0
        for opt, arg in opts:
            if opt == "--freq":
                pass

        self.instHeaderText += r'''
extern ::LSE_clock::clock_t *LSE_clk;
'''

        self.instCodeText = r'''
::LSE_clock::clock_t LSE_internal_clk("LSE_domain_inst_name");
::LSE_clock::clock_t *LSE_clk = &LSE_internal_clk;
'''

    ########### determine whether we can use this as a required domain ######
    def approveRequirement(self, buildArgs):
        return 1

    # Create the merged constants, types, and APIs
def createMergedInfo(self, objlist):
    #sys.stderr.write("Merging clock information\n")
    counter = 1
    for o in objlist:
        o.instIdentifiers.append(("LSE_clock_instid",
                                  LSE_domain.LSE_domainID_const|
                                  LSE_domain.LSE_domainID_inst,
                                  counter))
        o.instIdentifiers.append(("LSE_clk",
                                  LSE_domain.LSE_domainID_var,None))
        o.instId = counter
        counter = counter+1

    self.mergedIdentifiers = []
        

    
