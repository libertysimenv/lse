# /* 
#  * Copyright (c) 2000-2008 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * LSE_chkpt domain class Python module
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Declares the LSE_chkpt domain class to LSE.
#  *
#  */

import LSE_domain

class LSE_DomainObject(LSE_domain.LSE_BaseDomainObject):

    # domain class name
    className = "LSE_ieee754"

    classHeaders = [ "LSE_ieee754.h" ]
   
    classNamespaces = [ "LSE_ieee754" ]

    createIfRequired = 1
    
    classIdentifiers = [ ]

    # init method
    def __init__(self, instname, buildArgs, runArgs, bp):
        LSE_domain.LSE_BaseDomainObject.__init__(self,instname,buildArgs,
                                                 runArgs, bp)

    ########### determine whether we can use this as a required domain ######
    def approveRequirement(self, buildArgs):
        return 1

    
