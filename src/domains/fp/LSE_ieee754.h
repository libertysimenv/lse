/* 
 * Copyright (c) 2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LSE IEEE 754 floating point support
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file defines functions which support IEEE 754 floating point
 * operations.  Lots of fun!  The idea is to define all of the operations
 * here, but different implementations might do different things.  Templates
 * are used to resolve implementation dependences and to prevent copying stuff
 * over and over
 *
 * NaN handling: generates quiet nans by default.  Can be overridden by the
 * user by either manipulating the significand field or 
 * calling form_snan_signficand.  QNaNs are propagated from the first argument
 * always.  Any additional formatting of the NaNs needs to be done by the
 * user.
 *
 */
#ifndef _LSE_IEEE754_H_
#define _LSE_IEEE754_H_

#include <LSE_inttypes.h>
#include <cmath>
#include <iostream>

namespace LSE_ieee754 {

  template<class T> inline T bits(int n) { return (T(1)<<n) - T(1); }
  template<class T> inline T bit(int n) { return (T(1)<<n); }

  // NOTE: encoding of signaling vs. quiet NaNs is not specified in IEEE 754.
  // However, we will assume quiet sets the highest bit of the significand
  // for now.

  enum fp_valtypes { value=0, zero, nan, infinity };
  enum fp_rmodes { to_nearest=0, to_zero, to_pos_inf, to_neg_inf};
  const int exc_inexact = 1;
  const int exc_divbyzero = 2;
  const int exc_underflow = 4;
  const int exc_overflow = 8;
  const int exc_invalid = 16;
  const int zerobyzero = 64;
  const int signan = 128;
  const int roundedup = 256;

  /******************** Master FP type template *************************/

  // This template is used to create floating point types.  It specifies
  // a type to hold the significand, the size of the exponent field, the
  // size of the fraction field, and the size of the significand, which must
  // be at least 5 bits larger than the of the fraction (to hold the
  // implicit bit, guard, round, sticky, and one for carry out)
  //
  // The significand type must provide arithmetic operations and bit shifting
  // and masking operations.  It must also provide a type conversion of its 
  // lower bits to uint64_t.
  //
  // The binary point is two bits in from the left.
  //
  // The idea is to support up to 32 bit exponents and precision up to 
  // the limits of the significand.  The sticky bit is carried in the last
  // bit of the significand.

  template<typename ST, int es, int fs, int ss> 
  class ieee754_fp {

  public:
    static const int esize = es; // exponent size;
    static const int fsize = fs; // fraction size;
    static const int ssize = ss; // total significand size
    static const int maxexp = (1<<esize)-1;
    static const int smallexp = -maxexp/2+1;
    static const int bigexp = maxexp/2;
    static const int bias = maxexp/2;
    static const int expwrap = 3*(1<<(es-2));
    static const bool check_underflow_before_rounding = true;

    typedef ST sig_t;

    bool sign;
    int exponent;
    sig_t significand;
    fp_valtypes valtype;
    int status;

    ieee754_fp() : sign(false), exponent(0), significand(0),
      valtype(zero), status(0) {}

    inline void normalize(void) {
      // NOTE: assumes not a NaN or infinity!
      if (!bool(significand)) exponent=0; // pseudo-zeros become true zeros
      else 
	while (bool(significand) && !(significand & bit<sig_t>(ss-2))) {
	  exponent--;
	  significand <<= 1;
	}
    }

    inline bool is_denorm() const {
      return (valtype == value && exponent < smallexp);
    }

    inline bool nan_is_qnan() const {
      return bool(significand & bit<sig_t>(ssize-3));
    }

  };

  template<class ST, int es, int fs, int ss>
  inline std::ostream& operator<<(std::ostream &os, 
				  const ieee754_fp<ST,es,fs,ss>& v) {
    os << "(" << v.sign << "/" << v.exponent << "/"
       << std::hex << v.significand << std::dec;
    if (v.valtype == ::LSE_ieee754::zero) os << " zero";
    if (v.valtype == ::LSE_ieee754::nan) os << " nan";
    if (v.valtype == ::LSE_ieee754::infinity) os << " infinity";
    if (v.status & ::LSE_ieee754::exc_inexact) os << " inexact";
    if (v.status & ::LSE_ieee754::exc_divbyzero) os << " div0";
    if (v.status & ::LSE_ieee754::exc_underflow) os << " under";
    if (v.status & ::LSE_ieee754::exc_overflow) os << " over";
    if (v.status & ::LSE_ieee754::exc_invalid) os << " inv";
    if (v.status & ::LSE_ieee754::roundedup) os << " rup";
    os << ")";
    return os;
  }

  /******************** pack/unpack functions ********************/

  template<class FP, class RT, int rs> void unpack(FP &nv, const RT &v1) {

    nv.sign = bool((v1 >> (FP::esize+FP::fsize)) & typename FP::sig_t(1));
    nv.exponent = uint32_t((v1 >> FP::fsize) & bits<RT>(FP::esize));
    nv.significand = static_cast<typename FP::sig_t>(v1 & bits<RT>(FP::fsize))
		       << (FP::ssize-2-FP::fsize);

    if (nv.exponent == FP::maxexp) {
      /* Infinity/NaN */
      if (nv.significand) nv.valtype = nan;
      else nv.valtype = infinity;
    } else if (nv.exponent == 0) /* denorm/zero */ {
	if (nv.significand) {
	  nv.exponent = FP::smallexp;
	  nv.normalize();
	  nv.valtype = value;
	} else nv.valtype = zero;
    } else { /* normal value */
      nv.valtype = value;
      nv.significand |= bit<typename FP::sig_t>(FP::ssize-2); /* hidden 1 */
      nv.exponent -= FP::bias; 
    }
    nv.status = 0;
  }

  template<class FP, class RT, int rs> RT pack(const FP &v1) {
    // Assumes a normalized, in-range representation
    RT res;
    res = RT(v1.sign) << (FP::esize+FP::fsize);

    if (v1.valtype == zero) { }
    else if (v1.valtype == infinity) res |= RT(FP::maxexp)<<FP::fsize;
    else if (v1.valtype == nan) 
      res |= (RT(FP::maxexp) << FP::fsize) | 
	((v1.significand>>(rs-2-FP::fsize)) & bits<RT>(FP::fsize));
    else if (v1.exponent < FP::smallexp) { // denorm
      typename FP::sig_t sv = v1.significand >> (FP::smallexp - v1.exponent);
      if (bool(sv))
	res |= RT(sv>>(rs-2-FP::fsize)) & bits<RT>(FP::fsize);
    } else {
      res |= RT(v1.exponent + FP::bias) << FP::fsize;
      res |= (v1.significand>>(rs-2-FP::fsize)) & bits<RT>(FP::fsize);
    }
    return res;
  } // pack

  /*************************** Rounding *********************************/

  template<class FP> inline bool core_round(FP &nv, fp_rmodes rmode,
					    int precision = FP::fsize+1) { 

    // This function does the core rounding of bits, which makes life
    // a little bit nicer when denorms cause a second rounding.

    static bool roundtable[8][8] = {
      /*        to_nearest:
		0.00 - no   (2.00 -> 2)
		0.01 - no   (2.25 -> 2)
		0.10 - no   (2.50 -> 2)
		0.11 - yes  (2.75 -> 3)
		1.00 - no   (3.00 -> 3)
		1.01 - no   (3.25 -> 3)
		1.10 - yes  (3.50 -> 4)
		1.11 - yes  (3.75 -> 4)
      */
      { 0, 0, 0, 1, 0, 0, 1, 1 },
      { 0, 0, 0, 1, 0, 0, 1, 1 },
      /*        to_zero */
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      /*        to_pos_inf:
		0.00 - no   (2.00 -> 2)
		0.01 - yes  (2.25 -> 3)
		0.10 - yes  (2.50 -> 3)
		0.11 - yes  (2.75 -> 3)
		1.00 - no   (3.00 -> 3)
		1.01 - yes  (3.25 -> 4)
		1.10 - yes  (3.50 -> 4)
		1.11 - yes  (3.75 -> 4)
      */
      { 0, 1, 1, 1, 0, 1, 1, 1 },
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      /*        to_neg_inf:
		0.00 - no   (-2.00 -> 2)
		0.01 - yes  (-2.25 -> -3)
		0.10 - yes  (-2.50 -> -3)
		0.11 - yes  (-2.75 -> -3)
		1.00 - no   (-3.00 -> 3)
		1.01 - yes  (-3.25 -> -4)
		1.10 - yes  (-3.50 -> -4)
		1.11 - yes  (-3.75 -> -4)
      */
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      { 0, 1, 1, 1, 0, 1, 1, 1 }
    };

    //const int fsize = FP::fsize;
    const int ssize = FP::ssize;
    typedef typename FP::sig_t sig_t;

    int offset = ssize - precision - 1;

    unsigned int rbits; // add, round, sticky

    // Note: we are taking the last bit and the two bits beyond.  But that's
    // not actually the guard bit.  Perhaps it is right because we are 
    // guaranteed to be normalized?

    //std::cout << offset << " " << precision << std::endl;

    if (offset >= ssize) { // will shift completely off!
      nv.exponent = -1; // will be adjusted later
      rbits = 1;
      offset = ssize-2;
      precision = 0; // will result in empty mask and setting to 1 later
    } else if (offset > 2) 
      rbits = ( (nv.significand >> (offset-2) & sig_t(6)) |
		sig_t(!!(nv.significand & bits<sig_t>(offset-1))) );
    else if (offset == 2) 
      rbits = nv.significand & sig_t(7);
    else if (offset == 1)
      rbits = (nv.significand << 1) & sig_t(6);
    else 
      return false;
      
    sig_t sigmask = bits<sig_t>(precision) << offset;

    //std::cout << offset << " " << bits<sig_t>(precision) << std::endl;
    //std::cout << rmode << " " << rbits << " " << std::hex << sigmask << std::dec << "\n";

    nv.significand &= sigmask; // clip off the significand
    
    bool roundup = roundtable[ (rmode<<1) | nv.sign ][rbits & 7];
    if (roundup) {
      nv.status |= roundedup;
      if (nv.significand == sigmask) {
	nv.exponent++;
	nv.significand = bit<sig_t>(ssize-2);
      } else nv.significand += bit<sig_t>(offset);
    }

    if (nv.significand == sig_t(0)) {
      nv.exponent = 0;
      nv.valtype = zero;
      nv.status |= exc_inexact;
    }
    //std::cout << rbits << " " << roundup << " " << nv << std::endl;

    if (rbits & 3) nv.status |= exc_inexact; // had to round in some way

    return roundup;
  }
 
  template<class FP, class DFP> FP& fp_round(FP &nv, fp_rmodes rmode,
					     bool trap_overflow=false,
					     bool trap_underflow=false) { 

    // WARNING: assumes that the number has been normalized

    if (nv.valtype) return nv; // if a special value, don't do anything to it!

    if (DFP::fsize > FP::fsize) return nv; // going bigger doesn't change it

    //std::cout << "Entering fp_round " << nv << std::endl;

    typedef typename FP::sig_t sig_t;

    FP saveit = nv;

    bool roundup;

    if (DFP::check_underflow_before_rounding && 
	nv.exponent < DFP::smallexp) { 

      if (trap_underflow) {
	nv.exponent += DFP::expwrap;
	nv.status |= exc_underflow;
      } else {

	int offset = DFP::smallexp - nv.exponent;

	// denormalize the number, keeping stickiness of bits
	if (offset >= DFP::ssize-1) {
	  nv.significand = sig_t(1);
	  nv.exponent = DFP::smallexp;
	} else {
	  offset = DFP::smallexp - saveit.exponent;
	  nv = saveit;
	  bool sticky = ( bool(nv.significand & bits<sig_t>(offset)) || 
			  (nv.status & exc_inexact) );
	  nv.significand >>= offset;
	  nv.significand |= sig_t(sticky);
	  nv.exponent = DFP::smallexp;
	}
      
	roundup = core_round(nv, rmode, DFP::fsize+1);
      
	if (!nv.significand) { // rounded to zero
	  nv.significand = sig_t(0);
	  nv.exponent = 0;
	  nv.valtype = zero;
	  nv.status |= exc_underflow | exc_inexact;
	} else {
	  nv.normalize(); 
	  if (nv.status & exc_inexact) nv.status |= exc_underflow;
	}
      }
    } 
    else roundup = core_round(nv, rmode, DFP::fsize+1);

    //std::cout << "After 1st round " << nv << std::endl;

    // 3 * (1<<esize-2) is the bias for reporting if we were to do it

    if (nv.exponent > DFP::bigexp) { // overflow.  pick inf or largest

      nv.status |= (exc_inexact | exc_overflow);
      if (trap_overflow) {
	nv.exponent -= DFP::expwrap;
      } else {
	if (rmode == to_nearest || rmode == to_neg_inf && nv.sign || 
	    rmode == to_pos_inf && !nv.sign) {
	  nv.significand = sig_t(0);
	  nv.exponent = DFP::maxexp; // makes fp_cmp work more easily
	  nv.valtype = infinity;
	  nv.status |= roundedup;
	} else { // largest finite
	  nv.exponent = DFP::bigexp;
	  nv.significand = bits<sig_t>(FP::fsize+1)<<(FP::ssize-FP::fsize-2);
	}
      }
    } 

    if (!DFP::check_underflow_before_rounding && 
	nv.exponent < DFP::smallexp) { // denorm results

      if (trap_underflow) {
	nv.exponent += DFP::expwrap;
	nv.status |= exc_underflow;
      } else {

	int offset = DFP::smallexp - nv.exponent;

	// denormalize the number, keeping stickiness of bits
	if (offset >= DFP::ssize-1) {
	  nv.significand = sig_t(1);
	  nv.exponent = DFP::smallexp;
	} else {
	  offset = DFP::smallexp - saveit.exponent;
	  nv = saveit;
	  bool sticky = ( bool(nv.significand & bits<sig_t>(offset)) || 
			  (nv.status & exc_inexact) );
	  nv.significand >>= offset;
	  nv.significand |= sig_t(sticky);
	  nv.exponent = DFP::smallexp;
	}

	// round again
	nv.status &= ~exc_inexact & ~roundedup;

	roundup = core_round(nv, rmode);

	//std::cout << "After 2nd round " << nv << std::endl;
      
	if (!nv.significand) { // rounded to zero
	  nv.significand = sig_t(0);
	  nv.exponent = 0;
	  nv.valtype = zero;
	  nv.status |= exc_underflow | exc_inexact;
	} else {
	  nv.normalize(); 
	  if (nv.status & exc_inexact) nv.status |= exc_underflow;
	}
      }
    }

    return nv;
  }

#ifdef NOMORE

  template <class TT>
  inline void form_qnan_significand(TT& r, const TT &a, const TT &b) {
    if (a.valtype != nan && b.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan() ||
	b.valtype == nan && !b.nan_is_qnan())
      r.significand = bit<typename TT::sig_t>(TT::ssize-3);
    else if (a.valtype == nan) 
      r.significand = a.significand;
    else r.significand = b.significand;
  }

  template <class TT, class FT>
  inline void form_qnan_significand(TT& r, const FT &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      r.significand = bit<typename TT::sig_t>(TT::ssize-3);
    else r.significand 
	   = typename TT::sig_t(a.significand) << (TT::ssize - FT::ssize);
  }

  template <class TT, class FT>
  inline void form_qnan_significand_lesser(TT& r, const FT &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      r.significand = bit<typename TT::sig_t>(TT::ssize-3);
    else r.significand = typename TT::sig_t(a.significand >>
					    (FT::ssize - TT::ssize));
  }
#endif

  /****************** BASE operators ****************************/

  // NOTE: in all of these operators FP had better be a subclass of
  // ieee754_fp.

  template <class FP> FP& fp_addsub1(const FP &a, const FP &b, FP &r, 
				     fp_rmodes rmode, bool issub=false) {

    typedef typename FP::sig_t sig_t;

    // depends upon order: normal, zero, nan, infinity

    static int newres[] = 
      { value, value, nan, infinity, /* value + x */
	value, value, nan, 4|infinity, /* value + -x */
        value, zero, nan, infinity,   /* zero + x */
        value, zero, nan, 4|infinity,   /* zero + -x */
        nan, nan, nan, nan,            /* nan + x */
        nan, nan, nan, nan,            /* nan + -x */
	infinity, infinity, nan, infinity, /* infinity + x */
	infinity, infinity, nan, nan, /* infinity + -x */
        value, value, nan, infinity, /* -value + x */
	value, value, nan, 4|infinity, /* -value + -x */
        value, zero, nan, infinity,   /* -zero + x */
        value, 4|zero, nan, 4|infinity,   /* -zero + -x */
        nan, nan, nan, nan,            /* -nan + x */
        nan, nan, nan, nan,            /* -nan + -x */
	4|infinity, 4|infinity, nan, nan, /* -infinity + x */
	4|infinity, 4|infinity, nan, 4|infinity, /* -infinity + -x */

	value, value, nan, 4|infinity, /* value - x */
	value, value, nan, infinity, /* value - -x */
        value, zero, nan, 4|infinity,   /* zero - x */
        value, zero, nan, infinity,   /* zero - -x */
        nan, nan, nan, nan,            /* nan - x */
        nan, nan, nan, nan,            /* nan - -x */
	infinity, infinity, nan, nan, /* infinity - x */
	infinity, infinity, nan, infinity, /* infinity - -x */
        value, value, nan, 4|infinity, /* -value - x */
	value, value, nan, infinity, /* -value - -x */
        value, 4|zero, nan, 4|infinity,   /* -zero - x */
        value, zero, nan, infinity,   /* -zero - -x */
        nan, nan, nan, nan,            /* -nan - x */
        nan, nan, nan, nan,            /* -nan - -x */
	4|infinity, 4|infinity, nan, 4|infinity, /* -infinity - x */
	4|infinity, 4|infinity, nan, nan, /* -infinity - -x */
      };

    if (a.valtype || b.valtype) {
      unsigned int specials = newres[(issub?64:0) + a.valtype*8 + 
				     a.sign*32 + b.valtype + b.sign*4];
      fp_valtypes nf = static_cast<fp_valtypes>(specials & 3);

      switch (nf) {
      case zero: // 0 +- 0 has special case to deal with
	r = a;
	if (!issub) {
	  if (a.sign^b.sign) r.sign = (rmode==to_neg_inf);
	} else {
	  if (a.sign^b.sign) r.sign = a.sign && !b.sign;
	  else r.sign = (rmode==to_neg_inf);
	}
	return r;

      case nan:
	r = FP();
	r.valtype = nan;
	r.form_qnan_significand(a, b);

	if (a.valtype != nan && b.valtype != nan) 
	  r.status |= exc_invalid;
	else if (a.valtype == nan && !a.nan_is_qnan() ||
	         b.valtype == nan && !b.nan_is_qnan()) 
	  r.status |= exc_invalid | signan;

	return r;

      case infinity:
	r = FP();
	r.valtype = infinity;
	r.sign = specials & 4;
	return r;

      default:
	// only one of the operands must have been zero
	if (b.valtype == zero) return ((r = a));
	else if (a.valtype == zero) {
	  if (!issub) return ((r = b));
	  else {
	    r = b;
	    r.sign ^= true;
	    return r;
	  }
	}
      } // switch nf
    } // specials

    bool dosub = a.sign ^ b.sign ^ issub;
    int ediff = a.exponent - b.exponent;
    bool sticky = false;

    if (ediff > 0 || ediff == 0 && a.significand >= b.significand) { 

      if (ediff < FP::ssize) {
	sticky = bool(b.significand & bits<sig_t>(ediff));
	if (dosub) r.significand = a.significand - (b.significand >> ediff);
	else r.significand = a.significand + (b.significand >> ediff);

      } else {
	r.significand = a.significand;
	sticky = bool(b.significand);
      }
      r.exponent = a.exponent;
      r.sign = a.sign;

    } else { // a smaller

      if (-ediff < FP::ssize) {
	sticky = a.significand & bits<sig_t>(-ediff);
	if (dosub) r.significand = b.significand - (a.significand >> -ediff);
	else r.significand = b.significand + (a.significand >> -ediff);
      } else {
	r.significand = b.significand;
	sticky = a.significand;
      }
      r.exponent = b.exponent;
      r.sign = a.sign ^ dosub;
    }

    if (r.significand & bit<sig_t>(FP::ssize-1)) { // carry out
      r.exponent++;
      sticky |= bool(r.significand & sig_t(1));
      r.significand = (r.significand >> 1);
    }
	
    if (sticky) { // we lost precision
      r.status |= exc_inexact;
      if (dosub) r.significand -= sig_t(1); // may do bad things?
      r.significand |= sig_t(1); // keep the sticky bit handy
    }

    // deal with normalization
    if (!bool(r.significand)) { // turned into zero
      r.exponent = 0;
      r.sign = (rmode == to_neg_inf);
      r.valtype = zero;
    } else {
      r.normalize();
      r.valtype = value;
    }

    // NOTE: we do *not* check for underflow/overflow here; that is something 
    // that is checked when we round.

    return r;
  }

  template <class T, int size> 
  void mul_x2(const T &a, const T &b, T &hi, T &lo) {

    /* wild multiplication
     * how to do it:  for 64-bits, 
     * result(128 bits) = (A*2^32 + B) * (C*2^32 + D)
     *         A    B
     *   x     C    D
     * ----------------
     *         BDh  BDl
     *     ADh ADl
     *     BCh BCl
     * ACh ACl
     */
    
    T A, B, C, D, BC, AD, middle;

    const T halfmask = bits<T>(size/2);
    const int halfshift = size/2;

    A = a >> halfshift;
    B = a & halfmask;
    C = b >> halfshift;
    D = b & halfmask;

    BC = B * C;
    AD = A * D;

    lo = a * b; // loses the high bits
    middle = ( (B*D)>>halfshift ) + (AD & halfmask) + (BC & halfmask);
    hi = A*C + (BC>>halfshift) + (AD>>halfshift) + (middle>>halfshift);    
  }

  template <class FP> FP& fp_mul1(const FP &a, const FP &b, FP &r, 
				  fp_rmodes rmode) {

    typedef typename FP::sig_t sig_t;

    // depends upon order: normal, zero, nan, infinity

    static int newres[] = 
      { value, zero, nan, infinity, /* value * x */
	4|value, 4|zero, nan, 4|infinity, /* value * -x */
        zero, zero, nan, nan,   /* zero * x */
        4|zero, 4|zero, nan, nan,   /* zero * -x */
        nan, nan, nan, nan,            /* nan * x */
        nan, nan, nan, nan,            /* nan * -x */
	infinity, nan, nan, infinity, /* infinity * x */
	4|infinity, nan, nan, 4|infinity, /* infinity * -x */
        4|value, 4|zero, nan, 4|infinity, /* -value * x */
	value, zero, nan, infinity, /* -value * -x */
        4|zero, 4|zero, nan, nan,   /* -zero * x */
        zero, zero, nan, nan,   /* -zero * -x */
        nan, nan, nan, nan,            /* -nan * x */
        nan, nan, nan, nan,            /* -nan * -x */
	4|infinity, nan, nan, 4|infinity, /* -infinity * x */
	infinity, nan, nan, infinity, /* -infinity * -x */
      };

    if (a.valtype || b.valtype) {
      unsigned int specials = newres[a.valtype*8 + a.sign*32 + 
				     b.valtype + b.sign*4];
      fp_valtypes nf = static_cast<fp_valtypes>(specials & 3);

      switch (nf) {
      case zero: 
	r = FP();
	r.sign = specials & 4;
	return r;

      case nan:
	r = FP();
	r.valtype = nan;
	r.form_qnan_significand(a, b);

	if (a.valtype != nan && b.valtype != nan) 
	  r.status |= exc_invalid;
        else if (a.valtype == nan && !a.nan_is_qnan() ||
	         b.valtype == nan && !b.nan_is_qnan()) 
	  r.status |= exc_invalid | signan;

	return r;

      case infinity:
	r = FP();
	r.valtype = infinity;
	r.sign = specials & 4;
	return r;

      default:
	break; // shouldn't get here!
      } // switch nf
    } // specials

    r.sign = a.sign ^ b.sign;
    r.exponent = a.exponent + b.exponent + 2;

    /* wild multiplication
     * how to do it:  for 64-bits, 
     * result(128 bits) = (A*2^32 + B) * (C*2^32 + D)
     *         A    B
     *   x     C    D
     * ----------------
     *         BDh  BDl
     *     ADh ADl
     *     BCh BCl
     * ACh ACl
     */
    
    sig_t lo, hi;

    mul_x2<sig_t, FP::ssize>(a.significand, b.significand, hi, lo);

    if (hi==sig_t(0) && lo==sig_t(0)) {
      std::cout << std::hex << a.significand << " " << b.significand << std::endl;}

    //std::cout << std::hex << hi << "/" << lo << std::dec << "\n";

    // NOTE: they can't be zero; we already handled zeros!
    // Now we normalize the beast...
    const sig_t topmask = bit<sig_t>(FP::ssize-2);

    // normalize across both values
    while (!(hi & topmask)) {
      hi <<= 1;
      hi |= lo >> (FP::ssize-1);
      lo <<= 1;
      r.exponent--;
    }

    r.significand = hi | sig_t(bool(lo)); // add in sticky bit.
    r.valtype = value;

    //std::cout << std::hex << hi << "/" << lo << "/" 
    // << r.significand << std::dec << "\n";

    // NOTE: we do *not* check for underflow/overflow here; that is something 
    // that is checked when we round.

    return r;
  }

  template <class FP> FP& fp_div1(const FP &a, const FP &b, FP &r, 
				  fp_rmodes rmode, bool isrem=false) {

    typedef typename FP::sig_t sig_t;

    // depends upon order: normal, zero, nan, infinity

    static int newres[] = 
      { value, infinity, nan, zero, /* value / x */
	4|value, 4|infinity, nan, 4|zero, /* value / -x */
        zero, nan, nan, zero,   /* zero / x */
        4|zero, nan, nan, 4|zero,   /* zero / -x */
        nan, nan, nan, nan,            /* nan / x */
        nan, nan, nan, nan,            /* nan / -x */
	infinity, infinity, nan, nan, /* infinity / x */
	4|infinity, 4|infinity, nan, nan, /* infinity / -x */
        4|value, 4|infinity, nan, 4|zero, /* -value / x */
	value, infinity, nan, zero, /* -value / -x */
        4|zero, nan, nan, 4|zero,   /* -zero / x */
        zero, nan, nan, zero,   /* -zero / -x */
        nan, nan, nan, nan,            /* -nan / x */
        nan, nan, nan, nan,            /* -nan / -x */
	4|infinity, 4|infinity, nan, nan, /* -infinity / x */
	infinity, infinity, nan, nan, /* -infinity / -x */

	value, nan, nan, value, /* value % x */
	4|value, nan, nan, value, /* value % -x */
        zero, nan, nan, zero,   /* zero % x */
        zero, nan, nan, zero,   /* zero % -x */
        nan, nan, nan, nan,            /* nan % x */
        nan, nan, nan, nan,            /* nan % -x */
	nan, nan, nan, nan, /* infinity % x */
	nan, nan, nan, nan, /* infinity % -x */
        4|value, nan, nan, 4|value, /* -value % x */
	value, nan, nan, 4|value, /* -value % -x */
        4|zero, nan, nan, 4|zero,   /* -zero % x */
        4|zero, nan, nan, 4|zero,   /* -zero % -x */
        nan, nan, nan, nan,            /* -nan % x */
        nan, nan, nan, nan,            /* -nan % -x */
	nan, nan, nan, nan, /* -infinity % x */
	nan, nan, nan, nan, /* -infinity % -x */
      };

    if (a.valtype || b.valtype) {
      unsigned int specials = newres[a.valtype*8 + a.sign*32 + 
				     b.valtype + b.sign*4 + isrem*64];
      fp_valtypes nf = static_cast<fp_valtypes>(specials & 3);

      switch (nf) {
      case zero:
	r = FP();
	r.sign = specials & 4;
	return r;

      case nan:
	r = FP();
	r.valtype = nan;
	r.form_qnan_significand(a, b);

	if (a.valtype != nan && b.valtype != nan)
	  r.status |= exc_invalid;
        else if (a.valtype == nan && !a.nan_is_qnan() ||
	         b.valtype == nan && !b.nan_is_qnan())
	  r.status |= exc_invalid | signan;

	return r;

      case infinity:
	r = FP();
	r.valtype = infinity;
	r.sign = specials & 4;
	if (b.valtype == zero && a.valtype != infinity) 
	  r.status |= exc_divbyzero;
	return r;

      default:
	if (isrem && b.valtype == infinity) {
	  r = a;
	  r.sign = specials & 4;
	  return r;
	}
	break; // shouldn't get here!
      } // switch nf
    } // specials

    r.sign = a.sign ^ b.sign;
    r.exponent = a.exponent - b.exponent;

    sig_t quo(0), dividend(a.significand);

    int ii;
    for (ii = 0 ; ii < FP::ssize-1 ; ii++) {
      //std::cout << std::hex << dividend << std::dec << std::endl;
      quo <<= 1;
      if (b.significand <= dividend) {
	dividend -= b.significand;
	quo |= sig_t(1);
	if (dividend == sig_t(0)) {
	  quo <<= (FP::ssize-ii-2);
	  break;
	}
      }
      dividend <<= 1;
    }

    if (ii == FP::ssize-1) {
      quo |= sig_t(1); // sticky bit
      r.status |= exc_inexact;
    }

    //std::cout << std::hex << a.significand << " " << b.significand 
    //	      << " " << quo << " " << std::dec << std::endl;

    r.significand = quo;
    r.valtype = value;
    r.normalize();

    if (isrem) {
      if (r.exponent < -1) return ((r=a));
      else {
	FP prod;
	core_round(r, to_nearest, r.exponent+1); // round to integer
	r.status = 0;
	fp_mul1<FP>(r, b, prod, rmode);
	r = FP();
	fp_addsub1<FP>(a, prod, r, rmode, true);
	if (r.valtype == zero) r.sign = a.sign;
	return r;
      }
    }

    // NOTE: we do *not* check for underflow/overflow here; that is something 
    // that is checked when we round.

    return r;
  }

  template <class FP> FP& fp_sqrt1(const FP &a, FP &r, 
				   fp_rmodes rmode) {

    typedef typename FP::sig_t sig_t;

    // depends upon order: normal, zero, nan, infinity

    static int newres[] = 
      { value, zero, nan, infinity, /* value / x */
	nan, 4|zero, nan, nan, /* value / -x */
      };

    if (a.valtype || a.sign) {
      unsigned int specials = newres[a.valtype + a.sign*4];
      fp_valtypes nf = static_cast<fp_valtypes>(specials & 3);

      switch (nf) {
      case zero:
	r = FP();
	r.sign = specials & 4;
	return r;

      case nan:
	r = FP();
	r.valtype = nan;
	r.form_qnan_significand(a);

	if (a.valtype != nan) 
	  r.status |= exc_invalid;
	else if (a.valtype == nan && !a.nan_is_qnan())
	  r.status |= exc_invalid | signan;

	return r;

      case infinity:
	r = FP();
	r.valtype = infinity;
	r.sign = specials & 4;
	return r;

      default:
	break; // shouldn't get here!
      } // switch nf
    } // specials

    r.sign = false;

    if (a.exponent % 2) {
      r.significand = a.significand << 1;
      r.exponent = (a.exponent-1)/2;
    } else {
      r.significand = a.significand;
      r.exponent = a.exponent/2;
    }
    // significand now in range [1,4)
    r.status = 0;

    // digit-by-digit algorithm (I'm ashamed to say it's from Wikipedia)

    sig_t res = sig_t(0);
    sig_t rem = sig_t(0);

    int ii;
    //std::cout << std::hex << r.significand << std::dec << std::endl;

    for (ii=0 ; ii<FP::ssize*2; ii+=2) {
      sig_t c;
      int shiftamt = FP::ssize-ii-2;

      if (shiftamt < 0) c = rem<<2;
      else c = (rem<<2) + ((r.significand >> shiftamt) & sig_t(3));

      sig_t y = (res<<2) + sig_t(1);

      if (0) std::cout << std::hex << "B " << c << " " << y << " " 
		       << res << " " << rem
		       << std::dec << std::endl;

      res <<= 1;

      if (y <= c) {
	rem = c - y;
	res |= sig_t(1);

	if (0) std::cout << std::hex << "A " << c << " " << y << " " 
			 << res << " " << rem
			 << std::dec << std::endl;
	
      } else rem = c;
    }

    r.significand = res >> 1;
    r.valtype = value;
    if (rem || (res & sig_t(1))) {
      r.significand |= sig_t(1); // sticky bit
      r.status |= exc_inexact;
    }

    //std::cout << r << std::endl;

    r.normalize();

    //std::cout << r << std::endl;

    // NOTE: we do *not* check for underflow/overflow here; that is something 
    // that is checked when we round.

    return r;
  }

  /*************************  Operators ********************************/

  // These do not have the most beautiful of signatures, but they do allow 
  // very efficient inlining without multiple structure copies.

  // add

  template<class T> inline T& fp_add(const T &a, const T &b, 
				     T &r, fp_rmodes mode,
				     bool trap_overflow=false,
				     bool trap_underflow=false) {
    fp_addsub1(a,b,r,mode,false);
    return fp_round<T,T>(r, mode, trap_overflow, trap_underflow);
  }

  // sub

  template<class T> inline T& fp_sub(const T &a, const T &b, 
				     T &r, fp_rmodes mode,
				     bool trap_overflow=false,
				     bool trap_underflow=false) {
    fp_addsub1(a,b,r,mode,true);
    return fp_round<T,T>(r, mode, trap_overflow, trap_underflow);
  }

  // mul
  template<class T> inline T& fp_mul(const T &a, const T &b, 
				     T &r, fp_rmodes mode,
				     bool trap_overflow=false,
				     bool trap_underflow=false) {
    fp_mul1(a,b,r,mode);
    return fp_round<T,T>(r, mode, trap_overflow, trap_underflow);
  }

  // div
  template<class T> inline T& fp_div(const T &a, const T &b, 
				     T &r, fp_rmodes mode,
				     bool trap_overflow=false,
				     bool trap_underflow=false) {
    fp_div1(a,b,r,mode,false);
    return fp_round<T,T>(r, mode, trap_overflow, trap_underflow);
  }

  // rem
  template<class T> inline T& fp_rem(const T &a, const T &b, 
				     T &r, fp_rmodes mode,
				     bool trap_overflow=false,
				     bool trap_underflow=false) {
    fp_div1(a,b,r,mode,true);
    return fp_round<T,T>(r, mode, trap_overflow, trap_underflow);
  }

  // mul
  template<class T> inline T& fp_sqrt(const T &a, 
				      T &r, fp_rmodes mode,
				     bool trap_overflow=false,
				     bool trap_underflow=false) {
    fp_sqrt1(a,r,mode);
    return fp_round<T,T>(r, mode, trap_overflow, trap_underflow);
  }

  // compare; uses the SPARC encoding for the return value:
  // 0 = equal, 1 = lt , 2 = gt, 3 = unordered, add 4 for a signaling NaN

  template<class T> inline int fp_cmp(const T&a, const T&b) {
    if (a.valtype == nan || b.valtype == nan) {
      if (a.valtype == nan && !a.nan_is_qnan()) return 7;
      if (b.valtype == nan && !b.nan_is_qnan()) return 7;
      return 3;
    }
    // NOTE: we need to special-case zero/infinity because the exponents
    // are no longer encoded in a bias format.  We could have done some
    // work to give the exponent field very small or very large values, but
    // that is hard to remember to do consistently and not very happy when
    // we have to convert between different FP types

    // zeros don't compare sign and have messed up exponents by now
    if (a.valtype == zero && b.valtype == zero) return 0; 
    if (a.valtype == zero) return b.sign ? 2 : 1;
    if (b.valtype == zero) return a.sign ? 1 : 2;

    if (a.valtype == infinity && b.valtype == infinity) 
      return a.sign == b.sign ? 0 : a.sign ? 1 : 2;
    if (a.valtype == infinity) return a.sign ? 1 : 2;
    if (b.valtype == infinity) return b.sign ? 2 : 1;

    // and now it must be a value or infinity or zero
    if (a.exponent == b.exponent && a.significand == b.significand) {
      return a.sign == b.sign ? 0 : a.sign ? 1 : 2;
    }
    if (a.sign & !b.sign) return 1;
    if (!a.sign & b.sign) return 2;
    if (a.exponent < b.exponent || 
	a.exponent == b.exponent && a.significand < b.significand) 
      return a.sign ? 2 : 1;
    else return a.sign ? 1 : 2;
  }

  // conversion between formats

  template<class TT, class FT> 
  inline TT& fp_convert_smaller(TT& nv, const FT &v, fp_rmodes rmode,
				bool trap_overflow=false,
				bool trap_underflow=false) {

    if (v.valtype == nan) {
      nv = TT();
      nv.valtype = nan;
      nv.form_qnan_significand(v);

      if (!v.nan_is_qnan()) nv.status |= exc_invalid | signan;

      return nv;
    }

    FT v1(v);
    fp_round<FT,TT>(v1, rmode, trap_overflow, trap_underflow);
    nv.sign = v1.sign;
    nv.exponent = v1.exponent;
    nv.significand = typename TT::sig_t(v1.significand >> 
					(FT::ssize - TT::ssize));
    nv.valtype = v1.valtype;
    nv.status = v1.status;
    return nv;
  }

  template<class TT, class FT> 
  inline TT& fp_convert_larger(TT& nv, const FT &v, fp_rmodes rmode) {
    if (v.valtype == nan) {
      nv = TT();
      nv.valtype = nan;
      nv.form_qnan_significand(v);

      if (!v.nan_is_qnan()) nv.status |= exc_invalid | signan;
      return nv;
    }
    nv.sign = v.sign;
    nv.exponent = v.exponent;
    nv.significand = typename TT::sig_t(v.significand) 
      << (TT::ssize - FT::ssize);
    nv.valtype = v.valtype;
    nv.status = v.status;
    return nv;
  }

  // rounding to integral value

  template<class T>
  inline T& fp_round_to_integral(T& v, fp_rmodes rmode) {
    if (v.valtype == nan) {
      T nv = T();
      nv.valtype = nan;
      nv.form_qnan_significand(v); 

      if (!v.nan_is_qnan()) nv.status |= exc_invalid | signan;
      return v = nv;
    }
    if (!v.valtype) core_round(v, rmode, v.exponent + 1);
    return v;
  }

  // take to integer format. This function differs from all the others
  // in that its status is returned through the return value of the
  // function.  Note that precision must still be specified.

  template<typename TT, class T>
  inline int fp_to_integer(TT& nv, T v, fp_rmodes rmode, bool issigned, 
			   int precision=T::ssize) {
    switch (v.valtype) {
    case nan:
      nv = bit<TT>(precision - 1);
      if (v.nan_is_qnan()) return exc_invalid;
      else return exc_invalid | signan;
    case infinity:
      nv = bit<TT>(precision - 1);
      return exc_invalid;
    case zero: 
      nv = TT(0);
      return 0;
    default:

      core_round(v, rmode, v.exponent + 1); // take to integer

      if (v.exponent > precision - 1 || 
	  issigned && v.exponent == precision-1 && 
	  (!v.sign || v.significand != bit<typename T::sig_t>(T::ssize-2))) {

	nv = bit<TT>(precision - 1);
	return exc_invalid;

      } else {

	int offset = T::ssize - 2 - v.exponent;

	if (offset >= 0) {
	  v.significand >>= offset;
	  nv = TT(v.significand);
	  if (v.sign && issigned) nv = ~nv + bit<TT>(0);
	} else {
	  nv = TT(v.significand);
	  nv <<= -offset;
	  if (v.sign && issigned) nv = ~nv + bit<TT>(0);
	}

	return v.status;
      }
    }
    return 0; // never gets here
  }

  // Take from integer in significand format.  NOTE: assumes that you
  // have set it up at the bottom of the significand.

  template<class TT, typename FT>
  inline TT& fp_from_integer(TT& nv, FT v, 
			     fp_rmodes rmode, bool issigned, 
			     int precision=TT::ssize) {
    // will assume the valtype is normal when doing this.
    nv.status = 0;
    if (v == FT(0)) {
      nv.valtype = zero;
      nv.exponent = 0;
      nv.significand = typename TT::sig_t(0);
      return nv;
    }

    nv.valtype = value;

    // correct the sign
    nv.sign = (issigned && (v & bit<FT>(precision-1)));
    if (nv.sign) v = ~v + bit<FT>(0);

    if (precision == TT::ssize) {
      nv.significand = typename TT::sig_t(v);
      nv.exponent = TT::ssize - 2;
    } else if (precision < TT::ssize) {
      nv.significand = typename TT::sig_t(v);
      // deal with bad sign extension  
      nv.significand &= bits<typename TT::sig_t>(precision);
      nv.exponent = TT::ssize - 2;
    } else { // might be a number that won't fit in the new type
      FT mask = ~(bits<FT>(TT::ssize));
      nv.exponent = TT::ssize-2;;
      while (v & mask) {
	if (v & bit<FT>(0)) nv.status |= exc_inexact;
	v = (v >> 1) & bits<FT>(precision-1);
	nv.exponent++;
      }
      nv.significand = typename TT::sig_t(v);
      if (nv.status & exc_inexact) 
	nv.significand |= bit<typename TT::sig_t>(0);
    }

    //std::cout << std::hex << v << " " << nv.significand << "\n";

    if (nv.significand & bit<typename TT::sig_t>(TT::ssize-1)) {
      if (nv.significand & bit<typename TT::sig_t>(0)) {
	nv.status |= exc_inexact; // lost the lower bit 
	nv.significand = nv.significand >> 1 | bit<typename TT::sig_t>(0);
      } else nv.significand >>= 1;
      nv.exponent++;
    } else nv.normalize();

    if (nv.significand & bits<typename TT::sig_t>(TT::ssize-2-TT::fsize)) {
      nv.status |= exc_inexact;
      core_round(nv, rmode, TT::fsize+1); // take to integer
    }

    return nv;
  }

  /************************ standard precisions *****************************/

  // single-precision and double-precision classes, with magic 
  // type conversion operators

  class fp_single;
  class fp_double;
  class fp_quad;

  class fp_single : public ieee754_fp<uint32_t, 8, 23, 32> {
  public:
    fp_single() {}
    //fp_single(uint64_t v) { unpack<fp_single,uint64_t,64>(*this,v); }
    explicit fp_single(uint32_t v) { unpack<fp_single,uint32_t,32>(*this,v); }

    operator float() {
      // NOTE: will not do special cases properly!
      return (this->sign ? -ldexp(float(this->significand), 
				  this->exponent-30) :
	      ldexp(float(this->significand), this->exponent-30));
    }

    operator uint32_t() { return pack<fp_single,uint32_t,32>(*this); }

    inline void form_qnan_significand(const fp_single &a);
    inline void form_qnan_significand(const fp_double &a);
    inline void form_qnan_significand(const fp_quad &a);
    inline void form_qnan_significand(const fp_single &a, const fp_single &b);

  };


  class fp_double : public ieee754_fp<uint64_t, 11, 52, 64> {
  public:
    fp_double() : ieee754_fp<uint64_t, 11, 52, 64>() {}
    explicit fp_double(uint64_t v) { unpack<fp_double,uint64_t,64>(*this,v); }

    operator double() {
      // NOTE: will not do special cases properly!
      return (this->sign ? -ldexp(double(this->significand), 
				  this->exponent-62) :
	      ldexp(double(this->significand), this->exponent-62));
    }

    operator uint64_t() { return pack<fp_double,uint64_t,64>(*this); }
    
    inline void form_qnan_significand(const fp_single &a);
    inline void form_qnan_significand(const fp_double &a);
    inline void form_qnan_significand(const fp_quad &a);
    inline void form_qnan_significand(const fp_double &a, const fp_double &b);
  };

  // 128-bit integer class.  It is *not* complete but is good enough to
  // do the FP support for SPARC.  Note that the name has the LSE_ prefix
  // just in case uint128_t becomes defined in some later version of C++.
  // This is unfortunate, but I don't want the maintenance hassle later.

  class LSE_uint128_t {
  public:
    uint64_t hi, lo;
    LSE_uint128_t() : hi(0), lo(0) {}
    explicit LSE_uint128_t(const uint64_t h, const uint64_t l) : hi(h), lo(l) {}
    explicit LSE_uint128_t(const uint64_t l) : hi(0), lo(l) {}

    // type conversions

    //inline operator uint64_t() const { return lo; }
    //inline operator uint32_t() const { return lo & 0xffffffffU; }
    //inline operator int64_t() const { return lo; }
    //inline operator int32_t() const { return lo & 0xffffffffU; }
    inline operator bool() const { return lo || hi; }

    // and the operators

    inline LSE_uint128_t& operator+=(const LSE_uint128_t b) {
      uint64_t tmp = lo;
      lo += b.lo;
      hi += b.hi;
      if (lo <tmp) hi++;
      return *this;
    }
    inline LSE_uint128_t& operator-=(const LSE_uint128_t b) {
      uint64_t tmp = lo;
      lo -= b.lo;
      hi += ~b.hi;
      if (lo <= tmp) hi++;
      return *this;
    }
    inline LSE_uint128_t& operator*=(const LSE_uint128_t b) {

      /* wild multiplication
       * how to do it:  for 64-bits, 
       * result(128 bits) = (A*2^32 + B) * (C*2^32 + D)
       *         A    B
       *   x     C    D
       * ----------------
       *         BDh  BDl
       *     ADh ADl
       *     BCh BCl
       * ACh ACl
       */
    
      uint64_t A, B, C, D, BC, AD, middle, low, high;

      const uint64_t halfmask = (uint64_t(1)<<32)-1;
      const int halfshift = 32;

      A = lo >> halfshift;
      B = lo & halfmask;
      C = b.lo >> halfshift;
      D = b.lo & halfmask;

      BC = B * C;
      AD = A * D;

      low = lo * b.lo; // loses the high bits
      middle = ( (B*D)>>halfshift ) + (AD & halfmask) + (BC & halfmask);
      high = A*C + (BC>>halfshift) + (AD>>halfshift) + (middle>>halfshift);
      
      hi = high + hi * b.lo + lo * b.hi;
      lo = low;
      return *this;
    }
#ifdef NOTYET
    inline LSE_uint128_t& operator/=(const LSE_uint128_t b) {
      return *this;
    }
    inline LSE_uint128_t& operator%=(const LSE_uint128_t b) {
      return *this;
    }
#endif
    inline LSE_uint128_t& operator&=(const LSE_uint128_t b) {
      lo &= b.lo;
      hi &= b.hi;
      return *this;
    }
    inline LSE_uint128_t& operator|=(const LSE_uint128_t b) {
      lo |= b.lo;
      hi |= b.hi;
      return *this;
    }
    inline LSE_uint128_t& operator^=(const LSE_uint128_t b) {
      lo ^= b.lo;
      hi ^= b.hi;
      return *this;
    }
    inline LSE_uint128_t operator~() {
      return LSE_uint128_t(~hi, ~lo);
    }
    inline bool operator==(const LSE_uint128_t b) const {
      return lo == b.lo && hi == b.hi;
    }
    inline bool operator<(const LSE_uint128_t b) const {
      return hi < b.hi || hi == b.hi && lo < b.lo;
    }

    inline LSE_uint128_t& operator<<=(const int b) {
      if (b <= 0) {}
      else if (b >= 128) { hi = lo = 0; }
      else if (b == 64) { hi = lo; lo = 0; }
      else if (b > 64) { hi = lo << (b-64); lo = 0; }
      else {
	hi = (hi << b) | (lo >> (64-b)) & bits<uint64_t>(b);
	lo = lo << b;
      }
      return *this;
    }

    inline LSE_uint128_t& operator>>=(const int b) {
      if (b <= 0) {}
      else if (b >= 128) { hi = lo = 0; }
      else if (b == 64) { lo = hi; hi = 0; }
      else if (b > 64) { lo = hi >> (b-64); hi = 0; }
      else {
	lo = (lo >> b) | ((hi & bits<uint64_t>(b)) << (64-b));
	hi = hi >> b;
      }
      return *this;
    }

    friend std::ostream& operator<<(std::ostream &os, const LSE_uint128_t &v);

  };

  inline bool operator>(const LSE_uint128_t a, const LSE_uint128_t b) {
    return b < a;
  }
  inline bool operator>=(const LSE_uint128_t a, const LSE_uint128_t b) {
    return !(a < b);
  }
  inline bool operator<=(const LSE_uint128_t a, const LSE_uint128_t b) {
    return !(a > b);
  }
  inline bool operator!=(const LSE_uint128_t a, const LSE_uint128_t b) {
    return !(a == b);
  }

  inline LSE_uint128_t operator+(const LSE_uint128_t a, const LSE_uint128_t b) {
    LSE_uint128_t r = a;
    return r += b;
  }
  inline LSE_uint128_t operator-(const LSE_uint128_t a, const LSE_uint128_t b) {
    LSE_uint128_t r = a;
    return r -= b;
  }
  inline LSE_uint128_t operator*(const LSE_uint128_t a, const LSE_uint128_t b) {
    LSE_uint128_t r = a;
    return r *= b;
  }
#ifdef NOTYET
  inline LSE_uint128_t operator/(const LSE_uint128_t a, const LSE_uint128_t b) {
    LSE_uint128_t r = a;
    return r /= b;
  }
  inline LSE_uint128_t operator%(const LSE_uint128_t a, const LSE_uint128_t b) {
    LSE_uint128_t r = a;
    return r %= b;
  }
#endif
  inline LSE_uint128_t operator&(const LSE_uint128_t a, const LSE_uint128_t b) {
    LSE_uint128_t r = a;
    return r &= b;
  }
  inline LSE_uint128_t operator|(const LSE_uint128_t a, const LSE_uint128_t b) {
    LSE_uint128_t r = a;
    return r |= b;
  }
  inline LSE_uint128_t operator^(const LSE_uint128_t a, const LSE_uint128_t b) {
    LSE_uint128_t r = a;
    return r ^= b;
  }
  inline LSE_uint128_t operator<<(const LSE_uint128_t a, const int b) {
    LSE_uint128_t r = a;
    return r <<= b;
  }
  inline LSE_uint128_t operator>>(const LSE_uint128_t a, const int b) {
    LSE_uint128_t r = a;
    return r >>= b;
  }

  inline std::ostream& operator<<(std::ostream &os, const LSE_uint128_t &v) {
    os << "(" << std::hex << v.hi << "/" << v.lo << std::dec << ")";
    return os;
  }

  class fp_quad : public ieee754_fp<LSE_uint128_t, 15, 112, 128> {
  public:
    fp_quad() : ieee754_fp<LSE_uint128_t, 15, 112, 128>() {}
    fp_quad(const LSE_uint128_t v) { 
      unpack<fp_quad,LSE_uint128_t,128>(*this,v); 
    }
    fp_quad(const uint64_t hi, const uint64_t lo) { 
      LSE_uint128_t v(hi,lo);
      unpack<fp_quad,LSE_uint128_t,128>(*this,v); 
    }

    /*
    operator long double() {
      // NOTE: will not do special cases properly!
      return (sign ? -ldexp(long double(significand), exponent-62) :
	      ldexp(long double(significand), exponent-62));
    }
    */

    operator LSE_uint128_t() { return pack<fp_quad,LSE_uint128_t,128>(*this); }
    inline void form_qnan_significand(const fp_single &a);
    inline void form_qnan_significand(const fp_double &a);
    inline void form_qnan_significand(const fp_quad &a);
    inline void form_qnan_significand(const fp_quad &a, const fp_quad &b);
  };


  // And all of the qnan translation routines

  void fp_single::form_qnan_significand(const fp_single &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = a.significand;
  }

  void fp_single::form_qnan_significand(const fp_double &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = sig_t(a.significand >> (fp_double::ssize - ssize));
  }

  void fp_single::form_qnan_significand(const fp_quad &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = sig_t(a.significand >> (fp_quad::ssize - ssize));
  }

  void fp_single::form_qnan_significand(const fp_single &a, 
					const fp_single &b) {
    if (a.valtype != nan && b.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan() ||
	b.valtype == nan && !b.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else if (a.valtype == nan) 
      significand = a.significand;
    else significand = b.significand;
  }

  void fp_double::form_qnan_significand(const fp_single &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = sig_t(a.significand) << (ssize - fp_single::ssize);
  }

  void fp_double::form_qnan_significand(const fp_double &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = a.significand;
  }

  void fp_double::form_qnan_significand(const fp_quad &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = sig_t(a.significand >> (fp_quad::ssize - ssize));
  }

  void fp_double::form_qnan_significand(const fp_double &a, 
					const fp_double &b) {
    if (a.valtype != nan && b.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan() ||
	b.valtype == nan && !b.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else if (a.valtype == nan) 
      significand = a.significand;
    else significand = b.significand;
  }

  void fp_quad::form_qnan_significand(const fp_single &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = sig_t(a.significand) << (ssize - fp_single::ssize);
  }

  void fp_quad::form_qnan_significand(const fp_double &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = sig_t(a.significand) << (ssize - fp_double::ssize);
  }

  void fp_quad::form_qnan_significand(const fp_quad &a) {
    if (a.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else significand = a.significand;
  }

  void fp_quad::form_qnan_significand(const fp_quad &a, 
				      const fp_quad &b) {
    if (a.valtype != nan && b.valtype != nan ||
	a.valtype == nan && !a.nan_is_qnan() ||
	b.valtype == nan && !b.nan_is_qnan())
      significand = bit<sig_t>(ssize-3);
    else if (a.valtype == nan) 
      significand = a.significand;
    else significand = b.significand;
  }



}

#endif /* _LSE_IEEE754_H_ */

