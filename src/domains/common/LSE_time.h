/* 
 * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * This file is the header file for simulation time
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Defines the time variable and the operations.  There are three different
 * ways of representing time implemented; generally the straight 64-bit
 * integer is going to be the way to go. 
 *
 * All time APIs (i.e. LSE_time_*) are defined in this file
 *
 */

#ifndef _LSE_TIME_H
#define _LSE_TIME_H

#ifdef __cplusplus
#include <queue>
#endif

/* Must already have included SIM_types.h, which has stdint, SIM_config.h */

/* Note: a tick is marked every time we do an event or clock cycle.  These
 * ticks are in general of variable length.
 */

typedef uint64_t LSE_time_t;

#define LSE_time_zero (UINT64_C(0))

extern LSE_time_t LSE_time_now;

#define LSE_time_eq(ta, tb) ((ta) == (tb))
#define LSE_time_ne(ta, tb) ((ta) != (tb))
#define LSE_time_ge(ta, tb) ((ta) >= (tb))
#define LSE_time_gt(ta, tb) ((ta) > (tb))
#define LSE_time_le(ta, tb) ((ta) <= (tb))
#define LSE_time_lt(ta, tb) ((ta) < (tb))

#define LSE_time_add(ta, tb) ((ta) + (tb))
#define LSE_time_sub(ta, tb) ((ta) - (tb))

/* #define LSE_time_print_args(ta) "%" PRIu64,(ta) */

#ifdef __cplusplus

typedef void (*LSE_time_method_t)(void *);

struct LSE_timeact_t {
  LSE_time_t t;
  LSE_time_method_t method;
  void *data;
  bool operator<(const LSE_timeact_t& x) const { return t > x.t; }
};

class LSE_time_queue {

 public:

  bool empty() const { return myqueue.empty(); }

  void push(LSE_time_t t, LSE_time_method_t m, void *d) {
    LSE_timeact_t a = { t, m, d };
    myqueue.push(a);
  }

  LSE_timeact_t top() const { return myqueue.top(); }

  void pop() { myqueue.pop(); }

 private:
  
  typedef std::priority_queue<LSE_timeact_t> tqueue;
  tqueue myqueue;

}; // LSE_time_queue

#endif /* __cplusplus */

#endif /* _LSE_TIME_H */
