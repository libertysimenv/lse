# /* 
#  * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Python domain module
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Generic domain support
#  * 
#  *
#  */
import os, os.path, sys

# assume that we are importing this to do full code generation
# if not, the importer can set this to 0.  What this is for
# is to allow domain python files to know whether or not to
# import the code generation stuff from LSE
inCodeGen = 1

###### identifier types #####

LSE_domainID_const=1
LSE_domainID_type=2
LSE_domainID_var=4
LSE_domainID_func=8
LSE_domainID_m4macro=16
LSE_domainID_cmacro=32
LSE_domainID_tokmacro=64
LSE_domainID_inlinefunc=128
LSE_domainID_hook=256

LSE_domainID_class=1024
LSE_domainID_inst=2048
LSE_domainID_impl=4096

LSE_domainID_all = (LSE_domainID_const | LSE_domainID_type |
                    LSE_domainID_var | LSE_domainID_func |
                    LSE_domainID_tokmacro | LSE_domainID_inlinefunc |
                    LSE_domainID_cmacro | LSE_domainID_m4macro)

LSE_domainIDtype_R=0
LSE_domainIDtype_C=1
LSE_domainIDtype_CC=2
LSE_domainIDtype_CNN=3

############ An exception for domain classes to throw #############

class LSE_DomainException(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return self.value
  
########### LSE_BaseDomainObject ################
#
#  This is the base object type for all domain classes
#
class LSE_BaseDomainObject:

    used = 0
    
    className = None

    classAttributes = {}
    classCompileFlags = ""
    classHeaders = []
    classHooks = []
    classIdentifiers = []
    classLibraries = ""
    classLibPath = []
    classNamespaces = []
    classRequiresDomains = [] # a list of names
    classUseHeaders = []

    classMacroText = ""
    classHeaderText = ""
    classCodeText = ""

    mergedIdentifiers = []
    changesTerminateCount = 0
    createIfRequired = 0
  
    # this is so that there is something there for code which treats
    # instances and domains alike.
    instName = None

    changed = 0                   # for self-reporting of rebuilds

    def approveRequirement(self, buildArgs):
        return 0  # in general, we do not approve unless the domain does
  
    def __init__(self, instname, buildArgs, runArgs, buildPath):
        # instance variables
        self.instName = instname
        self.implName = self.className
        self.buildArgs = buildArgs
        self.runArgs = runArgs
        self.buildPath = buildPath

        self.implCompileFlags = ""
        self.implFrontRename = []
        self.implHeaders = []
        self.implIdentifiers = []
        self.implNamespaces = []
        self.implLibraries = ""
        self.implLibPath = []
        self.implRename = 0
        self.implRenameHeaders = []
        self.implRenameNamespaces = []
        self.implRequiresDomains = [] # a list of names
        self.implSkipRename = []
        self.implUseHeaders = []

        self.implMacroText = ""
        self.implHeaderText = ""
        self.implCodeText = ""

        self.instAttributes = {}
        self.instHeaders = []
        self.instHooks = []
        self.instIdentifiers = []
        self.instLibPath = []
        self.instLibraries = ""
        self.instRequiresDomains = [] # a list of tuples (name,buildparms)

        self.instMacroText = ""
        self.instHeaderText = ""
        self.instCodeText = ""
        
        self.compiled = 0 # so we do not special-case emulators...
        
        self.changed = 0           # for self-reporting of rebuilds
        self.generated = None      # code for the domain inst is generated
        self.suppressed = 0
       
        # is the domain actually used to pull out an identifier or is in
        # a search path or a parm?
        self.used = 0


    def checkAttribute(self,struct,attrname):
        return None

def createMergedInfo(dclass, objlist):
    pass

def checkAttribute(self,struct,attrname):
    return None

def dropimpl(x):
    if (x[1] == LSE_domainID_var or
        x[1] == LSE_domainID_type or
        x[1] == LSE_domainID_const or
        x[1] == LSE_domainID_func or
        x[1] == LSE_domainID_inlinefunc): return None
    else:
        return x[2]

def dropDefs(l):
    return map(lambda x:(x[0],x[1],dropimpl(x)), l)

def findFile(f,p,default=0):
    for x in p:
        if os.path.exists(os.path.join(x,f)):
            return os.path.abspath(os.path.join(x,f))
    else:
        if default: return f
        else: return None

#################### end of base object #######################









