# /* 
#  * Copyright (c) 2010 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * Hashlib wrapper
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Wrap the hashlib stuff so the code will work in Python 2.4 or 2.6
#  * 
#  *
#  */

import sys
cur_version = sys.version_info

newlib = cur_version[1] > 5

if newlib:
  import hashlib
  def hashit(fbytes):
	return hashlib.sha1(fbytes).hexdigest()
  def gethasher():
	return hashlib.sha1()
else:
  import sha
  def hashit(fbytes):
	return sha.new(fbytes).hexdigest()
  def gethasher():
	return sha.new()

