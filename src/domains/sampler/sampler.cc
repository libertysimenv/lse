/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Implementation of sampler domain functions
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file implements the sampler library
 *
 * Parameter values are as such:
 *
 * Period    Length    Warmup    First       Meaning
 * --------  --------  --------  --------    ----------------------------
 *   0         x          x        0         always in collect
 *   0         x          x       F > 0      forward F, then collect
 * non-zero    L          W        0         collect L, then cycle
 * non-zero    L          W       F <= W     warmup (F), then cycle
 * non-zero    L          W       F > W      forward (F-W), then cycle
 */

#include <LSE_inttypes.h>
#include "LSE_sampler.h"
#include <math.h>
#include <stdlib.h>

namespace LSE_sampler {
/*
 * Create a sampler instance
 */

  sampler_t::sampler_t(int64_t p, int64_t l, int64_t w, int64_t f)
  {
    state = state_forward;

    /* massage parameters so that all are positive and  (L+W) < P */
    period = p <= 0 ? 0 : p;
    length = l <= 0 ? 0 : l;
    warmup = w <= 0 ? 0 : w;
    first = f <= 0 ? 0 : f;

    if (length > period) {
      length = period;
      warmup = 0;
    } else if (warmup > period - length) {
      warmup = period - length;
    }

    /* Now figure out first state... */

    if (period) {

      if (first) { /* skipping to be done... */
	if (first > warmup) {
	  state = state_forward;
	  eventsToGo = first - warmup;
	} else {
	  state = state_warmup;
	  eventsToGo = first;
	}
      } else { /* start right off into collection if it is on... */
	if (length) {
	  state = state_collect;
	  eventsToGo = length;
	} else {
	  state = state_forward;
	  eventsToGo = (period - warmup);
	}
      }

    } else { /* (!period) -- no sampling set... */

      if (first) {
	state = state_forward;
	eventsToGo = first;
	period = length = INT64_MAX;
      } else {
	state = state_collect;
	eventsToGo = length = period = INT64_MAX;
      }
    } /* (!period) */

    sampleCount = 0;
  }

  /*
   * Notify sampler of events and do state transitions as needed
   */
  bool sampler_t::notify(int64_t nev)
  {
    if (nev <= 0) nev = 0;
    eventsToGo -= nev;

    if (eventsToGo <= 0) {
      switch (state) {
      case state_forward:
	state = state_warmup;
	eventsToGo += warmup;
	break;
      case state_warmup:
	state = state_collect;
	eventsToGo += length;
	break;
      case state_collect:
	sampleCount++;
	state = state_recover;
	break;
      case state_recover:
	return false; /* do not move on... */
	break;
      default:
	state = state_forward;
	eventsToGo += (period - length - warmup);
	break;
      }
      return true;
    } /* eventsToGo <= 0 */
    return false;
  }

  /*
   * Advance the sampler state machine, updating events as needed
   */
  void sampler_t::advance()
  {
    switch (state) {
    case state_forward:
      state = state_warmup;
      eventsToGo += warmup;
      break;
    case state_warmup:
      state = state_collect;
      eventsToGo += length;
      break;
    case state_collect:
      sampleCount++;
      state = state_recover;
      break;
    case state_recover:
    default:
      state = state_forward;
      eventsToGo += (period - length - warmup);
      break;
    }
  }

} // namespace LSE_sampler
