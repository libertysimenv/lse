/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Test file for the sampler domain
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file tests the sampler domain functions
 *
 */
#include <LSE_inttypes.h>
#include "LSE_sampler.h"
#include <stdio.h>
#include <stdlib.h>

const char *states[] = {"forward","warmup","collect","recover"};

static void check_state(const char *loc, LSE_sampler::sampler_t *p, 
			bool res, bool expect,
			LSE_sampler::state_t state,
			int64_t period, int64_t length, int64_t warmup,
			int64_t togo) {
  bool wasbad = false;

  if (!p) {
    printf("%s: did not produce a sampler\n",loc);
    exit(1);
  }
  if (res != expect) {
    printf("%s: mismatch in return value: was %d want %d\n",
	   loc, res, expect);
    wasbad = true;
  }
  if (p->state != state || p->period != period || p->length != length ||
      p->warmup != warmup || p->eventsToGo != togo || res != expect) {
    printf("%s: mismatch in internal state\n",loc);
    wasbad = true;
  }

  if (wasbad) {
    printf("\tstate:      %20s\t%20s\n", states[state], states[p->state]);
    printf("\tperiod:     %20" PRId64 "\t%20" PRId64 "\n", 
	   period, p->period);
    printf("\tlength:     %20" PRId64 "\t%20" PRId64 "\n", 
	   length, p->length);
    printf("\twarmup:     %20" PRId64 "\t%20" PRId64 "\n", 
	   warmup, p->warmup);
    printf("\teventsToGo: %20" PRId64 "\t%20" PRId64 "\n", 
	     togo, p->eventsToGo);
    exit(1);
  } else {
    printf("%s ok\n", loc);
  }

}

static void test_fsm() {
  bool res;

  {  /***** No sampling at all -- should always be in collect ****/

    LSE_sampler::sampler_t p(0,0,0,0);
    check_state("(0,0,0,0) : create", &p, true, true,
		LSE_sampler::state_collect,INT64_MAX,INT64_MAX,0,INT64_MAX);

    res = p.notify(30);
    check_state(" (0,0,0,0): notify 30", &p, res, false,
		LSE_sampler::state_collect,INT64_MAX,INT64_MAX,0,INT64_MAX-30);

  }

  {   /***** Initial ffwd, then 0-length warmup, then collect *****/

    LSE_sampler::sampler_t p(0,0,0,10);
    check_state("(0,0,0,10) : create", &p, true, true,
		LSE_sampler::state_forward,INT64_MAX,INT64_MAX,0,10);

    res = p.notify(30);
    check_state(" (0,0,0,10): notify 30", &p, res, true,
		LSE_sampler::state_warmup,INT64_MAX,INT64_MAX,0,-20);
    res = p.notify(0);
    check_state(" (0,0,0,10): notify 0", &p, res, true,
		LSE_sampler::state_collect,INT64_MAX,INT64_MAX,0,INT64_MAX-20);
    res = p.notify(0);
    check_state(" (0,0,0,10): notify 0", &p, res, false,
		LSE_sampler::state_collect,INT64_MAX,INT64_MAX,0,INT64_MAX-20);

  }

  { 
    /* Normal cycle, ffwd > warmup */

    LSE_sampler::sampler_t p(10,2,4,6); 
    check_state("(10,2,4,6) : create", &p, true, true,
		LSE_sampler::state_forward,10,2,4,2);

    res = p.notify(10);
    check_state(" (10,2,4,6): notify 10", &p, res, true, 
		LSE_sampler::state_warmup,10,2,4,-4);
    res = p.notify(0);
    check_state(" (10,2,4,6): notify 0", &p, res, true,
		LSE_sampler::state_collect,10,2,4,-2);
    res = p.notify(0);
    check_state(" (10,2,4,6): notify 0", &p, res, true,
		LSE_sampler::state_recover,10,2,4,-2);
    res = p.notify(0);
    check_state(" (10,2,4,6): notify 0", &p, res, false,
		LSE_sampler::state_recover,10,2,4,-2);
    p.advance();
    check_state(" (10,2,4,6): advance", &p, true, true,
		LSE_sampler::state_forward,10,2,4,2);
    res = p.notify(0);
    check_state(" (10,2,4,6): notify 0", &p, res, false,
		LSE_sampler::state_forward,10,2,4,2);

  }

  { 
    /* Normal cycle, ffwd < warmup */

    LSE_sampler::sampler_t p(10,2,4,3); 
    check_state("(10,2,4,3) : create", &p,true,true,
		LSE_sampler::state_warmup,10,2,4,3);

    res = p.notify(10);
    check_state(" (10,2,4,3): notify 10", &p, res, true, 
		LSE_sampler::state_collect,10,2,4,-5);
    res = p.notify(0);
    check_state(" (10,2,4,3): notify 0", &p, res, true,
		LSE_sampler::state_recover,10,2,4,-5);
    res = p.notify(0);
    check_state(" (10,2,4,3): notify 0", &p, res, false,
		LSE_sampler::state_recover,10,2,4,-5);
    p.advance();
    check_state(" (10,2,4,3): advance", &p, true, true,
		LSE_sampler::state_forward,10,2,4,-1);
    res = p.notify(0);
    check_state(" (10,2,4,3): notify 0", &p, res, true,
		LSE_sampler::state_warmup,10,2,4,3);
    res = p.notify(0);
    check_state(" (10,2,4,3): notify 0", &p, res, false,
		LSE_sampler::state_warmup,10,2,4,3);

  }

 
  { 
    /* Normal cycle, ffwd == warmup */

    LSE_sampler::sampler_t p(10,2,4,4); 
    check_state("(10,2,4,4) : create", &p,true,true,
		LSE_sampler::state_warmup,10,2,4,4);

    res = p.notify(10);
    check_state(" (10,2,4,4): notify 10", &p, res, true, 
		LSE_sampler::state_collect,10,2,4,-4);

    res = p.notify(0);
    check_state(" (10,2,4,4): notify 0", &p, res, true, 
		LSE_sampler::state_recover,10,2,4,-4);
    res = p.notify(0);
    check_state(" (10,2,4,4): notify 0", &p, res, false, 
		LSE_sampler::state_recover,10,2,4,-4);
    p.advance();
    check_state(" (10,2,4,4): advance", &p, true, true, 
		LSE_sampler::state_forward,10,2,4,0);
    res = p.notify(0);
    check_state(" (10,2,4,4): notify 0", &p, res, true, 
		LSE_sampler::state_warmup,10,2,4,4);
    res = p.notify(0);
    check_state(" (10,2,4,4): notify 0", &p, res, false, 
		LSE_sampler::state_warmup,10,2,4,4);

  }

  { 
    /* 0-length cycle */
    
    LSE_sampler::sampler_t p(10,0,0,0); 
    check_state("(10,0,0,0) : create",&p,true,true,
		LSE_sampler::state_forward,10,0,0,10);

    res = p.notify(10);
    check_state(" (10,0,0,0): notify 10", &p, res, true, 
		LSE_sampler::state_warmup,10,0,0,0);
    res = p.notify(0);
    check_state(" (10,0,0,0): notify 0", &p, res, true, 
		LSE_sampler::state_collect,10,0,0,0);
    res = p.notify(0);
    check_state(" (10,0,0,0): notify 0", &p, res, true, 
		LSE_sampler::state_recover,10,0,0,0);
    res = p.notify(0);
    check_state(" (10,0,0,0): notify 0", &p, res, false, 
		LSE_sampler::state_recover,10,0,0,0);
    p.advance();
    check_state(" (10,0,0,0): advance", &p, true, true, 
		LSE_sampler::state_forward,10,0,0,10);
    res = p.notify(0);
    check_state(" (10,0,0,0): notify 0", &p, res, false, 
		LSE_sampler::state_forward,10,0,0,10);
  }

  { 
    /* no warmup, no ffwd */
    
    LSE_sampler::sampler_t p(10,4,0,0); 
    check_state("(10,4,0,0) : create",&p,true,true,
		LSE_sampler::state_collect,10,4,0,4);

    res = p.notify(10);
    check_state(" (10,4,0,0): notify 10", &p, res, true, 
		LSE_sampler::state_recover,10,4,0,-6);
    res = p.notify(0);
    check_state(" (10,4,0,0): notify 0", &p, res, false, 
		LSE_sampler::state_recover,10,4,0,-6);
    p.advance();
    check_state(" (10,4,0,0): advance", &p, true, true, 
		LSE_sampler::state_forward,10,4,0,0);
    res = p.notify(0);
    check_state(" (10,4,0,0): notify 0", &p, res, true, 
		LSE_sampler::state_warmup,10,4,0,0);
    res = p.notify(0);
    check_state(" (10,4,0,0): notify 0", &p, res, true, 
		LSE_sampler::state_collect,10,4,0,4);
    res = p.notify(0);
    check_state(" (10,4,0,0): notify 0", &p, res, false, 
		LSE_sampler::state_collect,10,4,0,4);

  }


}

int main(int argc, char *argv[], char **envp) {

  test_fsm();

  return 0;
}
