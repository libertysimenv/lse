/* Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header file for sampler support
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 */

#ifndef _LSE_SAMPLER_H_
#define _LSE_SAMPLER_H_

namespace LSE_sampler {

  typedef enum state_t {
    state_forward = 0,
    state_warmup = 1,
    state_collect = 2,
    state_recover = 3
  } state_t;

  class sampler_t {
  public:
    /* parameters */
    int64_t period;
    int64_t warmup;
    int64_t length;
    int64_t first;

    state_t state;
    
    int64_t sampleCount; /* number of collections made */
    int64_t eventsToGo;

    sampler_t(int64_t period, int64_t length, int64_t warmup, int64_t first);
    void advance();
    bool notify(int64_t nev);
  };
  
} // namespace LSE_sampler

#endif /* _LSE_SAMPLER_H_ */
