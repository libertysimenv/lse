# /* 
#  * Copyright (c) 2000-2007 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * LSE_sampler domain class Python module
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Declares the LSE_sampler domain class to LSE.
#  *
#  */

import LSE_domain

if LSE_domain.inCodeGen:
    from SIM_apidefs import *  # routines useful for parsing arguments
    from SIM_codegen import *  # imports all we need to do code generation

def cname(tinfo,args,name,dinfo,extraargs,env):
    (db,currinst,subinst,codeloc,rexc) = env.getStandard()
    newname = "SAMPLER" + name[11:]
    if (len(args) != extraargs+1):
        LSEap_report_badargs(name,extraargs,tinfo[0])

    # single-instance declared in namespace, therefore, simply prepend
    uname =  "::" + LSEcg_domain_namespace_mangler("LSE_sampler",1)+ "::" + name 

    if (len(args)>1):
        parmtok = LSEtk_TokenPList(
            reduce(lambda x,y:x+[LSEtk_TokenOther(","),y],
                   args[2:],[args[1]]))
        return [ LSEtk_TokenIdentifier(uname),
                 parmtok ]
    else:
        return [ LSEtk_TokenIdentifier(uname) ]

class LSE_DomainObject(LSE_domain.LSE_BaseDomainObject):

    # domain class name
    className = "LSE_sampler"

    classHeaders = [ "LSE_sampler.h" ]
   
    classLibraries = "-lLSEsampler -lm"

    classNamespaces = [ "LSE_sampler" ]

    createIfRequired = 1
    
    classIdentifiers = []

    # init method
    def __init__(self, instname, buildArgs, runArgs, bp):        
        LSE_domain.LSE_BaseDomainObject.__init__(self,instname,buildArgs,
                                                 runArgs, bp)

    ########### determine whether we can use this as a required domain ######
    def approveRequirement(self, buildArgs):
        return 1

    
