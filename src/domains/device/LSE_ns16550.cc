/* 
 * Copyright (c) 2000-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LSE ROM device model
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Defines a device model for ROMs.  The basic idea is that write does nothing
 * while read returns the proper value.  Note that while the type is a 
 * subclass of LSE_device, its declaration never escapes this file.
 *
 */
#include <LSE_device.h>
#include <queue>
#include <stdio.h>
#include <sys/select.h>

namespace LSE_device {

  class ns16550 : public device_t {
  private:
    unsigned char RBR, IER, IID, IIR, FCR, LCR, MCR, SCR, DLL, 
      DLM;
    std::deque<unsigned char> xmit, rcvr;
    fd_set rfds;

  public:

    ns16550() 
      : RBR(0),IER(0),IID(0),IIR(0),FCR(0),LCR(0),MCR(0),SCR(0),DLL(0),DLM(0)
    {  }

    ~ns16550() { }

    devaddr_t translate(devaddr_t addr, device_t **devpp, devtptr_t *datapp);
    devaddr_t writeSub(devaddr_t addr, const devdata_t *buffer, 
		       devaddr_t len, int writeFlags=0, 
		       devdata_t *backup=0, resolver_t **reslist=0);
    devaddr_t readSub(devaddr_t addr, devdata_t *buffer, devaddr_t len,
		      resolver_t **reslist);
  };

  devaddr_t ns16550::translate(devaddr_t addr, 
			       device_t **devpp, devtptr_t *datapp) {
    *devpp = this;
    datapp->a = addr;
    return ~devaddr_t(0);
  }

  devaddr_t ns16550::readSub(devaddr_t addr, devdata_t *buffer, devaddr_t len,
			     resolver_t **reslist) {

    if (reslist) { // speculative read
      add_to_reslist(reslist, new readRedoCommit());
      ::memset(buffer, 0, static_cast<size_t>(len));
      return len;
    }

    using ::memset; // in case FD_ZERO uses memset w/o qualification
    for (int i=0; i < (int)len; i++) {
      unsigned int raddr = (addr + i) % 8;

      switch (raddr) {
      case 0: // receive buffer or divisor latch
	if (LCR & 128) buffer[i] = DLL;
	else {
	  FD_ZERO(&rfds); FD_SET(0, &rfds);
	  struct timespec tv = { 0, 100 };
	  int rv = ::pselect(1, &rfds, 0, 0, &tv, 0);
	  if (rv > 0) ::read(0, buffer+i, 1);
	  else buffer[i] = 0;
	}
	break;
      case 1: // interrupt enable or divisor latch
	if (LCR & 128) buffer[i] = DLM;
	else buffer[i] = IER;
	break;
      case 2: // interrupt identification
	buffer[i] = IIR | ((FCR & 1) ? 0xc0 : 0);
	break;
      case 3: // line control
	buffer[i] = LCR;
	break;
      case 4: // modem control
	buffer[i] = MCR;
	break;
      case 5: // line status
	buffer[i] = 0x60; // no overruns; always can transmit; TODO: data ready
	{
	  FD_ZERO(&rfds); FD_SET(0, &rfds);
	  struct timespec tv = { 0, 100 };
	  int rv = ::pselect(1, &rfds, 0, 0, &tv, 0);
	  if (rv > 0) buffer[i] |= 1;
	}
	//LSR &= ~0x1e; // stuff cleared on read
	break;
      case 6: // modem status
	// ignore real status (call 0)
	buffer[i] = (MCR & 0x10) ? ( ((MCR & 1) << 5) |
				     ((MCR & 2) << 3) |
				     ((MCR & 4) << 4) |
				     ((MCR & 8) << 4) ) : 0;
	break;
      case 7: // scratch
	buffer[i] = SCR;
	break;
      }
    }
    return len;
  } // ns16550::read
  
  devaddr_t ns16550::writeSub(devaddr_t addr, const devdata_t *buffer, 
			      devaddr_t len, int writeFlags, 
			      devdata_t *backup, resolver_t **reslist) {

    if (writeFlags & writeOpRollback) { return len; } // no rollbacks
    if (backup) {
      if (reslist)
	add_to_reslist(reslist, new writeCommit(this,addr, buffer, len));
      return len;
    }

    for (int i=0; i < (int)len; i++) {
      unsigned int raddr = (addr + i) % 8;

      switch (raddr) {
      case 0: // transmitter holding register or divisor latch
	if (LCR & 128) DLL = buffer[i];
	else {
	  putchar(buffer[i]); // TODO: do this more carefully
	  fflush(stdout);
	}
	break;
      case 1: // interrupt enable or divisor latch
	if (LCR & 128) DLM = buffer[i];
	else IER = buffer[i] & 0xf;
	break;
      case 2: // fifo control
	if (!(buffer[i] & 1) || (buffer[i] & 1) && !(FCR & 1)) {
	  xmit.clear();
	  rcvr.clear();
	}
	if ((buffer[i] & 3)==3) rcvr.clear();
	if ((buffer[i] & 5)==5) xmit.clear();
	FCR = buffer[i] & ~0x66;
	break;
      case 3: // line control
	LCR = buffer[i];
	break;
      case 4: // modem control
	MCR = buffer[i] & 0x1f;
	break;
      case 5: // line status
	break;
      case 6: // modem status
	break;
      case 7: // scratch
	SCR = buffer[i];
	break;
      }
    }
    return len;
  } // ns16550::read
  
  device_t *create_ns16550_inst() {
    return new ns16550();
  }

  device_t *create_ns16550_inst(device_t *parent, const args_t &argv) {
    devaddr_t len, base;
    if (argv.size() != 2) {
      std::cerr << "ns16550 instantiation: bad arguments\n";
      return 0;
    } 
    if (get_addr_arg(argv[0], &base)) {
      std::cerr << "ns16550 instantation: bad base\n";
      return 0;
    }
    if (get_addr_arg(argv[1], &len)) {
      std::cerr << "ns16550 instantation: bad length\n";
      return 0;
    }
    device_t *nd = new ns16550();
    parent->addchild(base, len, nd);
    return nd;
  }

  registrar ns16550_registered("ns16550", create_ns16550_inst);

} // namespace LSE_device
