# /* 
#  * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * LSE_device domain class Python module
#  *
#  * Authors: David A. Penry <dpenry@ee.byu.edu>
#  *
#  * Declares the LSE_device domain class to LSE.
#  *
#  * This domain class has both renamed symbols and non-renamed symbols.
#  * Non-renamed symbols are in namespace LSE_device.  Renamed symbols
#  * start with LSE_device.   The device base class and the service functions
#  * which are common to all domain instances (such as the basic device
#  * registration functions) are not renamed, and must go into a "support"
#  * library.  Actual devices are in libraries which can be renamed when linked
#  * statically, but need not be renamed when loaded dynamically (with dlopen)
#  *
#  */

import LSE_domain

class LSE_DomainObject(LSE_domain.LSE_BaseDomainObject):

    # domain class name
    className = "LSE_device"

    classHeaders = [ "LSE_device.h" ]
   
    classLibraries = "-lLSEdevice"

    classNamespaces = [ "LSE_device" ]

    classRequiresDomains = [ "LSE_chkpt" ]
    
    createIfRequired = 1
    
    def __init__(self, instname, buildArgs, runArgs, bp):
        LSE_domain.LSE_BaseDomainObject.__init__(self,instname,buildArgs,
                                                 runArgs, bp)
        # TODO: idea is to parse the build args to figure out what kind
        # of device we're talking about.  This will allow us to pull the
        # device's library and headers into LSE space.  Of course, the
        # built-in devices will have to be recognized specially.  I think
        # we'll still let the control file define the actual instances; this
        # is just a way to pull the additional device models into the
        # simulator's space.
        
    ########### determine whether we can use this as a required domain ######
    def approveRequirement(self, buildArgs):
        return 1

    
