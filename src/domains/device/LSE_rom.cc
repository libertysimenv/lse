/* 
 * Copyright (c) 2000-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LSE ROM device model
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Defines a device model for ROMs.  The basic idea is that write does nothing
 * while read returns the proper value.  Note that while the type is a 
 * subclass of LSE_device, its declaration never escapes this file.
 *
 */
#include <LSE_device.h>

namespace LSE_device {

  class rom : public device_t {
  private:
    devdata_t *data;
    devaddr_t rlen;
    bool locked;

  public:
    rom() : data(0), rlen(0) {}
    rom(devaddr_t l) : data(0), rlen(0) { setlen(l); }
    ~rom() { delete[] data; }

    devaddr_t translate(devaddr_t addr, device_t **devpp, devtptr_t *datapp);
    devaddr_t writeSub(devaddr_t addr, const devdata_t *buffer, 
		       devaddr_t len, int writeFlags=0, 
		       devdata_t *backup=0, resolver_t **reslist=0);
    devaddr_t readSub(devaddr_t addr, devdata_t *buffer, devaddr_t len,
		      resolver_t **reslist=0);

    void setlen(devaddr_t len);
  };

  devaddr_t rom::translate(devaddr_t addr, 
			   device_t **devpp, devtptr_t *datapp) {
    *devpp = this;
    datapp->a = addr;// % rlen; // rolls over
    return rlen - datapp->a;
  }

  devaddr_t rom::readSub(devaddr_t addr, devdata_t *buffer, devaddr_t len,
			 resolver_t **reslist) {
    ::memcpy(buffer, data+addr, len);
    return len;
  }
  
  devaddr_t rom::writeSub(devaddr_t addr, const devdata_t *buffer, 
				devaddr_t len, int writeFlags, 
				devdata_t *backup, resolver_t **reslist) {
    // both forces and rollbacks write
    if (backup) ::memcpy(backup, data+addr, len);
    if (writeFlags & (writeOpRollback|writeOpForced)) 
      ::memcpy(data+addr, buffer, len);
    return len;
  }
  
  void rom::setlen(devaddr_t l) {
    if (rlen) delete[] data;
    if (l) data = new devdata_t[l];
    else data = 0;
    rlen = l;
  }

  device_t *create_rom_inst(devaddr_t len) {
    return new rom(len);
  }

  device_t *create_rom_inst(device_t *parent, const args_t &argv) {
    devaddr_t len, base;
    if (argv.size() != 2) {
      std::cerr << "rom instantation: bad arguments\n";
      return 0;
    } 
    if (get_addr_arg(argv[0], &base)) {
      std::cerr << "rom instantation: bad base\n";
      return 0;
    }
    if (get_addr_arg(argv[1], &len)) {
      std::cerr << "rom instantation: bad length\n";
      return 0;
    }
    device_t *nd = new rom(len);
    parent->addchild(base, len, nd);
    return nd;
  }

  registrar rom_registered("rom", create_rom_inst);

} // namespace LSE_device
