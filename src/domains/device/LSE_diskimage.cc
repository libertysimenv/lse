/* 
 * Copyright (c) 2000-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LSE disk image device model
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Defines a device model for disk images.  Disk images are host system
 * files which are accessed like memory.  
 *
 */
#include <LSE_device.h>
#include <fstream>

namespace LSE_device {

  class diskimage : public device_t {
  private:
    static const unsigned int bufferSize = 8192;
    std::fstream fs;
    devaddr_t bufferAddr;
    devdata_t buffer[bufferSize];
    bool bufferValid;

  public:
    //diskimage() : fd(-1) {}
    //diskimage(std::fstream &f) : fs(f) { }
    diskimage(const char *fname) { 
      fs.open(fname, std::ios::binary | std::ios::in | std::ios::out);
      bufferValid = false;
    }
    ~diskimage() { 
      if (fs.is_open()) fs.close();
    }

    devaddr_t translate(devaddr_t addr, device_t **devpp, devtptr_t *datapp);
    devaddr_t writeSub(devaddr_t addr, const devdata_t *buffer, 
		       devaddr_t len, int writeFlags=0, 
		       devdata_t *backup=0, resolver_t **reslist=0);
    devaddr_t readSub(devaddr_t addr, devdata_t *buffer, devaddr_t len,
		      resolver_t **reslist=0);
    
  };

  devaddr_t diskimage::translate(devaddr_t addr, device_t **devpp,
				 devtptr_t *datapp) {
    *devpp = this;
    datapp->a = addr;
    return ~devaddr_t(0);
  }

  // Read from the device
  devaddr_t diskimage::readSub(devaddr_t addr, devdata_t *buf, devaddr_t len,
			       resolver_t **reslist) {
    fs.clear();

    if (len > bufferSize || addr + len < addr) {
      fs.seekg(addr);
      fs.read(reinterpret_cast<char *>(buffer), 
	      static_cast<std::fstream::pos_type>(len));
    } else if (bufferValid && addr >= bufferAddr && 
	       addr + len <= bufferAddr + bufferSize) {
      memcpy(buf, buffer + addr - bufferAddr, len);
    } else {
      fs.seekg(addr);
      fs.read(reinterpret_cast<char *>(buffer), 
	      static_cast<std::fstream::pos_type>(bufferSize));
      bufferAddr = addr;
      memcpy(buf, buffer, len);
      bufferValid = true;
    }
    return len;
  } // diskimage::read
  
  // Write to the device
  devaddr_t diskimage::writeSub(devaddr_t addr, const devdata_t *buffer, 
				devaddr_t len, int writeFlags, 
				devdata_t *backup, resolver_t **reslist) {

    if (writeFlags & writeOpRollback) { return len; } // no rollbacks
    if (backup) {
      if (reslist)
	add_to_reslist(reslist, new writeCommit(this, addr, buffer, len));
      return len;
    }

    fs.clear();
    fs.seekp(addr);
    fs.write(reinterpret_cast<const char *>(buffer), 
	     static_cast<std::fstream::pos_type>(len));
    fs.sync(); // because cntl-c in simulator would mess it up badly!
    if (bufferValid && 
	( (addr >= bufferAddr && addr < bufferAddr + bufferSize) ||
	  (addr + len >= bufferAddr && addr + len < bufferAddr + bufferSize) ))
      bufferValid = false; // wrote to the read buffer location
    return len;
  } // diskimage::write
  
  device_t *create_diskimage_inst(const char *fname) {
    return new diskimage(fname);
  }

  device_t *create_diskimage_inst(device_t *parent, const args_t &argv) {
    devaddr_t len, base;
    if (argv.size() != 3) {
      std::cerr << "diskimage instantiation: bad arguments\n";
      return 0;
    } 
    if (get_addr_arg(argv[0], &base)) {
      std::cerr << "diskimage instantation: bad base\n";
      return 0;
    }
    if (get_addr_arg(argv[1], &len)) {
      std::cerr << "diskimage instantation: bad length\n";
      return 0;
    }
    device_t *nd = new diskimage(argv[2].c_str());
    parent->addchild(base, len, nd);
    return nd;
  }

  registrar diskimage_registered("diskimage", create_diskimage_inst);

} // namespace LSE_device
