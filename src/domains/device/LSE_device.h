/* -*-c++-*-
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Device domain header file
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Defines a base class for devices.  This header should be used when
 * writing devices or when writing emulators which use devices.  However,
 * it does not contain the definitions of symbols which are to be renamed
 * when making multiple copies of a library
 *
 * Also defines some useful helper classes.
 */

#ifndef _LSE_DEVICE_H_
#define _LSE_DEVICE_H_

#include <LSE_inttypes.h>
#include <errno.h>
#include <stdio.h>
#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <vector>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <LSE_chkpt.h>

namespace LSE_device {

  enum deverror_code_t {
    deverror_none=0,
    deverror_unmapped=1,
    deverror_illegalop=2,
    deverror_fileerror=3
  };

  const int tag_DEVICE = 1000;
  const int tag_DEVICETRACE = 1001;
  const int tag_DEVICERECS = 1002;
  const char *errstring(const deverror_code_t errnum);

  class deverror_t {
  public:
    deverror_code_t err;
    deverror_t(deverror_code_t e=deverror_none) : err(e){}
    const char *errstring() const { return ::LSE_device::errstring(err); }
  };

  class fileerror_t : public deverror_t {
  public:
    int fileerr;
    ssize_t amt;
    fileerror_t(int e, ssize_t a) 
      : deverror_t(deverror_fileerror), fileerr(e), amt(a) {}
  };
    
  typedef uint64_t devaddr_t;
  typedef unsigned char devdata_t;

  typedef union devptr_u {
    devdata_t *hp;
    devaddr_t a;
  } devtptr_t;

  const int resolveRollback = 1;
  const int resolveRedo = 2;
  const int resolveCommit = 4;
  const int writeOpRollback = 2;
  const int writeOpForced = 1;

  class resolver_t {
  public:
    int resolveFlag;
    bool mustRedo;
    class resolver_t *next;
    resolver_t(int m) : resolveFlag(m), next(0) {}
    virtual void commit();
    virtual void rollback();
    virtual ~resolver_t();
  };

  class device_t {
  protected:
    int refcount;
    uint64_t oid;
  public:
    typedef LSE_device::devaddr_t devaddr_t;
    typedef LSE_device::devdata_t devdata_t;
    typedef LSE_device::devtptr_t devtptr_t;
    typedef LSE_device::resolver_t resolver_t;

    std::string deviceType, name;
    class device_t *parent;
    LSE_chkpt::data_t *incrRecords;
    class device_t *proxy;

    // refcounting
    int decr(bool dodel = true) { 
      int c = --refcount; 
      if (dodel && !refcount) delete this; 
      return c; 
    }
    int incr() { return ++refcount; }
    
    uint64_t getOID() const { return oid; }

    // Functions a device must implement
    virtual devaddr_t translate(devaddr_t addr, 
				device_t **devpp, devtptr_t *datapp) = 0;

    // shortcut for OS emulation
    virtual devaddr_t translate(devaddr_t addr, devdata_t **datapp);

    // Functions it need not implement if it doesn't need them.  Default is
    // to do nothing.

    virtual void addchild(devaddr_t addr, devaddr_t len, device_t *child);
    virtual void delchild(devaddr_t addr, devaddr_t len);
    virtual void delchild(device_t *child);
    virtual devaddr_t writeSub(devaddr_t addr, const devdata_t *buffer, 
			       devaddr_t len, int writeFlags=0, 
			       devdata_t *backup=0, resolver_t **reslist=0);
    virtual devaddr_t readSub(devaddr_t addr, devdata_t *buffer, 
			      devaddr_t len, resolver_t **reslist=0);

    // Checkpointing functions.  Invariants are that:
    // 1) for write, must write out a tag_DEVICE; it will be indefinite
    // 2) for read, tag_DEVICE, oid, and type will have just been read
    //
    // Format for any device is: tag_DEVICE := oid kind body
    LSE_chkpt::error_t writeChkpt(LSE_chkpt::file_t *cptFile, bool isIncr);
    virtual LSE_chkpt::error_t writeChkptGuts(LSE_chkpt::file_t *cptFile,
					      bool isIncr);
    virtual LSE_chkpt::error_t readChkptGuts(LSE_chkpt::file_t *cptFile,
					     bool isIncr);
    virtual LSE_chkpt::error_t incrReadChkpt(LSE_chkpt::file_t *cptFile);

    // Way to handle pretty much anything else we want the device to do.
    // It's defined this way so we don't have to include separate header files
    // for each device, as that would be really annoying.  Returns false if
    // the command could not be handled.
    virtual bool handleCommand(const char *cmd, ...);

    // top-level methods to access data.  Note that the idea behind doing it
    // this way is to allow the user to have a non-virtual call right off and
    // only need to do virtual calls for translation.
    //
    // writeFlags: bit 0 = force write to read-only dev.
    //             bit 1 = doing a rollback

    devaddr_t write(devaddr_t addr, const devdata_t *buffer, 
		    devaddr_t len, int writeFlags=0, 
		    devdata_t *backup = 0,
		    resolver_t **reslist=0);

    devaddr_t read(devaddr_t addr, devdata_t *buffer, devaddr_t len,
		   resolver_t **reslist=0);

    // NOTE: no need for an explicit resolve method because:
    // 1) write rollbacks can be handled as a write with a particular flag bit
    //    as we never need a return value from them (since all later are 
    //    assumed to have rolled back).
    // 2) read and commit rollbacks handled through the reslist and called
    //    by the user of the device tree

    // These methods do not support speculation
    devaddr_t memset(devaddr_t addr, const int value, devaddr_t len);
    int       memcmp(devaddr_t addr, const devdata_t *buffer, devaddr_t len);
    devaddr_t read_cstr(devaddr_t addr, devdata_t *buffer, devaddr_t len);

     ////////////////////////
     // File handling methods: do not support speculation!
     ////////////////////////

    template<typename FileReader>
      devaddr_t read_from_file(FileReader f, devaddr_t addr, 
			       devaddr_t len, bool forceWrite=false) {

       devtptr_t realbufp;
       devaddr_t madelen;
       device_t *devp;
       devaddr_t olen = len;
       
       while (len) {
	 madelen = translate(addr, &devp, &realbufp);
	 madelen = std::min(madelen, len);
	 if (devp) {
	   devdata_t rbuf[8192];
	   madelen = std::min(madelen,static_cast<devaddr_t>(8192));
	   f(rbuf, madelen);
	   devp->writeSub(realbufp.a, rbuf, madelen, forceWrite);
	 } else f(realbufp.hp, madelen);
	 addr += madelen;
	 len -= madelen;
       }
       return olen - len;
    } // read_from_file

    template<typename FileWriter>
      devaddr_t write_to_file(FileWriter f, devaddr_t addr, 
			      devaddr_t len) {

       devtptr_t realbufp;
       devaddr_t madelen;
       device_t *devp;
       devaddr_t olen = len;

       while (len) {
	 madelen = translate(addr, &devp, &realbufp);
	 madelen = std::min(madelen, len);
	 if (devp) {
	   devdata_t rbuf[8192];
	   madelen = std::min(madelen, static_cast<devaddr_t>(8192));
	   devp->read(realbufp.a, rbuf, madelen);
	   f(rbuf, madelen);
	 } else f(realbufp.hp, madelen);
	 addr += madelen;
	 len -= madelen;
       }
       return olen - len;
    } // write_to_file

    device_t() : refcount(1), oid(0), incrRecords(0), proxy(this) { }
    device_t(uint64_t id) : refcount(0), oid(id), incrRecords(0), 
			    proxy(this) { }
    virtual ~device_t() {
      delete incrRecords;
    }
  };


  ////////////////////////////////////////////////////////////////////////
  // File reader and writer helper classes
  ////////////////////////////////////////////////////////////////////////

  class fp_reader {

    FILE *fp;

  public:
    fp_reader(FILE *f) : fp(f) {}

    devaddr_t operator()(devdata_t *const realbuf, const devaddr_t len) {
      ssize_t count = fread(realbuf, sizeof(devdata_t), len, fp);
      if (len != static_cast<devaddr_t>(count))
	if (errno) throw fileerror_t(errno, count);
      return count;
    }

  }; // class fp_reader
     
  class fp_writer {

    FILE *fp;

  public:
    fp_writer(FILE *f) : fp(f) {}

    devaddr_t operator()(const devdata_t *realbuf, const devaddr_t len) {
      ssize_t count = fwrite(realbuf, sizeof(devdata_t), len, fp);
      if (len != static_cast<devaddr_t>(count)) {
	if (errno) throw fileerror_t(errno, count);
      }
      return count;
    }

  }; // class fp_writer
     
  class fd_reader {

    int fd;

  public:
    fd_reader(int f) : fd(f) {}

    devaddr_t operator()(devdata_t *const realbuf, const devaddr_t len) {
      ssize_t count = read(fd, realbuf, len*sizeof(devdata_t));
      if (count < 0) throw fileerror_t(errno, 0);
      return count/sizeof(devdata_t);
    }

  }; // class fd_reader
  
  class fd_writer {

    int fd;

  public:
    fd_writer(int f) : fd(f) {}

    devaddr_t operator()(devdata_t *const realbuf, const devaddr_t len) {
      ssize_t count = write(fd, realbuf, len*sizeof(devdata_t));
      if (count < 0) throw fileerror_t(errno, 0);
      return count/sizeof(devdata_t);
    }

  }; // class fd_writer
  
  class stream_reader {

    std::istream &ist;

  public:
    stream_reader(std::istream &i) : ist(i) {}

    devaddr_t operator()(devdata_t *realbuf, const devaddr_t len) {
      ist.read(reinterpret_cast<char *>(realbuf), static_cast<ssize_t>(len));
      int count = ist.gcount();
      if (ist.fail()) throw fileerror_t(errno, 0);
      return count;
    }

  }; // class ist_reader


  class stream_writer {

    std::ostream &ost;

  public:
    stream_writer(std::ostream &i) : ost(i) {}

    devaddr_t operator()(devdata_t *realbuf, const devaddr_t len) {
      ost.write(reinterpret_cast<char *>(realbuf),len);
      if (ost.fail()) throw fileerror_t(errno, 0);
      return len;
    }

  }; // class ost_writer

  ////////////////////////////////////////////////////////////////////////
  // a wrapper class for pointers to devices to let them be checkpointed 
  ////////////////////////////////////////////////////////////////////////

  class devwrapptr_t  {
  public:
    device_t *dptr;
    devwrapptr_t() : dptr(0) {}
    devwrapptr_t(device_t *d) : dptr(d) {}
    devwrapptr_t(const devwrapptr_t &s) : dptr(s.dptr) {}

    void drop() { 
      if (dptr) dptr->decr();
    }

    LSE_chkpt::error_t writeChkpt(LSE_chkpt::file_t *cptFile, bool isIncr) {
      LSE_chkpt::data_t *dp;
      LSE_chkpt::error_t cerr;
      if (dptr) { 
	// TODO: figure out how to deal with sub-devices
      } else {
	dp = LSE_chkpt::build_unsigned(0, 0);
	if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
      }
      return LSE_chkpt::error_None;
    }

    LSE_chkpt::error_t readChkpt(LSE_chkpt::file_t *cptFile, bool isIncr) {
      LSE_chkpt::error_t cerr;
      LSE_chkpt::data_t *dp;

      // TODO: handle subdevice...
      if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
      delete dp;
      
      return LSE_chkpt::error_None;
    }

  };

  ////////////////////////////////////////////////////////////////////////
  // Address range lookup helper class
  ////////////////////////////////////////////////////////////////////////

  template<class T> class addr_range {
  public:
    class range_t {
    public:
      devaddr_t first;
      devaddr_t last;
      T data;
    };

  private:
    typedef std::map<devaddr_t, range_t> ranges_t;
    ranges_t ranges;

  public:
    typedef typename ranges_t::iterator iterator;

    iterator begin() { return ranges.begin(); }
    iterator end() { return ranges.end(); }

    void add_range(const devaddr_t addr, const devaddr_t len, T &val) {
      // remove overlapping portion
      del_range(addr, len);

      // and add the new range
      range_t nr = { addr, addr + len - 1, val };
      ranges[addr] = nr;
    }

    void del_range(devaddr_t addr, devaddr_t len) {
       devaddr_t laddr = addr + len - 1;

       while (ranges.size()) {
	 typename ranges_t::iterator fnd = ranges.lower_bound(addr);
	 
	 // worry about
	 //       XXXXXXXXXXXX           fnd
	 //  OOOOOOOOOO                  new
	 
	 if (fnd != ranges.end()) { // started after start of last one
	   // invariant: addr <= fnd->first
	   if (laddr >= fnd->second.last) {
	     // totally overlapped
	     ranges.erase(fnd);
	     continue;
	   } else if (laddr >= fnd->first) { // partial overlap
	     range_t nr = fnd->second;
	     nr.first = laddr + 1;
	     ranges.erase(fnd);
	     ranges[nr.first] = nr;
	     continue;
	   }
	 }
	 // worry about
	 // XXXXXXXXXXX            XXXXXXXXXXX
	 //     OOOOOOOOOOOOOOOO
	 // Note: the gap before the second X is already proven, but
	 if (fnd == ranges.begin()) return; // already taken care of
	 fnd--;
	 // invariant: addr > fnd->first

	 if (fnd->second.last < addr) return; // we're done
	 if (fnd->second.last > laddr) { // XXXXOOOOXXXX
	   range_t nr = fnd->second;
	   fnd->second.last = addr - 1;
	   nr.first = laddr + 1;
	   ranges[nr.first] = nr;
	   return; // I believe in this no other could overlap!
	 } else {
	   fnd->second.last = addr - 1;
	   continue;
	 }
       } // while (ranges.size())
    }

    range_t *find_range(devaddr_t addr) {

       typename ranges_t::iterator fnd = ranges.lower_bound(addr);

       if (!ranges.size()) throw deverror_t(deverror_unmapped);
       if (fnd == ranges.end()) fnd--;
       else if (fnd->first == addr) {}
       else if (fnd == ranges.begin()) throw deverror_t(deverror_unmapped);
       else fnd--;

       if (addr < fnd->first || addr > fnd->second.last) 
	 throw deverror_t(deverror_unmapped);
       else return &fnd->second;
    } // find_range


    ////// Checkpoint the ranges

    LSE_chkpt::error_t writeChkpt(LSE_chkpt::file_t *cptFile, bool isIncr) {
      LSE_chkpt::data_t *dp;
      LSE_chkpt::error_t cerr;
      
      dp = LSE_chkpt::build_header(0, LSE_chkpt::TAG_SEQUENCE, 
				   LSE_chkpt::CONSTRUCTED, -1);
      if ((cerr = cptFile->write_to_segment(true, dp))) return cerr; 
      
      for (typename ranges_t::iterator i = ranges.begin(), ie = ranges.end() ; 
	   i != ie ; ++i) {
	
	dp = LSE_chkpt::build_unsigned(0, i->second.first);
	if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
	
	// smaller numbers take less space in the checkpoint and so are faster
	dp = LSE_chkpt::build_unsigned(0, i->second.last - i->second.first);
	if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
	
	if ((cerr = i->second.data.writeChkpt(cptFile, isIncr)))
	  return cerr;
	
      } // for ranges

      dp = LSE_chkpt::build_indefinite_end(0);
      return cptFile->write_to_segment(true,dp);
    } // writeChkpt

    LSE_chkpt::error_t readChkpt(LSE_chkpt::file_t *cptFile, bool isIncr) {
      LSE_chkpt::error_t cerr;
      LSE_chkpt::data_t *dp;
      
       if ((cerr = cptFile->read_taglen_from_segment(0, &dp))) return cerr;
       delete dp;

       // Tricky code: copy the old ranges, but then drop elements from
       // it which match in the checkpoint as they are added. Finally,
       // drop the old range elements.
       ranges_t orng = ranges;

       do {  
	 range_t nv;

	 if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
	 if (!dp) break;
	 nv.first = dp->content.uint64Val;
	 delete dp;

	 if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
	 nv.last = dp->content.uint64Val + nv.first;
	 delete dp;

	 iterator ri = ranges.find(nv.first);
	 if (ri != ranges.end()) {
	   ri->second.last = nv.last;
	   if ((cerr = ri->second.data.readChkpt(cptFile,isIncr))) return cerr;
	   orng.erase(nv.first);
	 } else {
	   if ((cerr = nv.data.readChkpt(cptFile, isIncr))) return cerr;
	   ranges[nv.first] = nv;
	 }

       } while(1);

       for (typename ranges_t::iterator i = orng.begin(), ie = orng.end() ; 
	    i != ie ; ++i) {
	 i->second.data.drop();
	 ranges.erase(i->first);
       }
       return LSE_chkpt::error_None;
    } // readChkpt

  }; // class addr_range

  /////////////
  // helper classes for aligned data access
  ////////////
  template <typename T> class aligned_access {
    device_t *dev;
  public:
    inline aligned_access(device_t *d) : dev(d) {}

    inline T read(devaddr_t addr, resolver_t **reslist=0) {
       devtptr_t realbufp;
       device_t *devp;

       // NOTE: we assume a single read won't cross devices!
       dev->translate(addr, &devp, &realbufp);
       if (devp) {
	 T nv;
	 devp->readSub(realbufp.a, reinterpret_cast<devdata_t *>(&nv), 
		       sizeof(T), reslist);
	 return nv;
       } else {
	 switch (sizeof(T)) {
	 case 1:
	 case 2:
	 case 4:
	 case 8:
	   // NOTE: this should work as long as realbuf is 8-byte aligned,
	   // which I enforce in the buffer alloc routine
	   return *(reinterpret_cast<T *>(realbufp.hp));
	 default: break;
	 }
       
	 T nv;
	 ::memcpy(&nv, realbufp.hp, sizeof(T));
	 return nv;
       }
     } // read

    inline void write(devaddr_t addr, const T& data, 
		      int writeFlags=0, devdata_t *backup = 0,
		      resolver_t **reslist = 0) {
      devtptr_t realbufp;
      device_t *devp;

      // NOTE: we assume a single read won't cross devices!
      dev->translate(addr, &devp, &realbufp);
      if (devp) {
	devp->writeSub(realbufp.a, reinterpret_cast<const devdata_t *>(&data), 
		       sizeof(T), writeFlags, backup, reslist);
       } else {
	 switch (sizeof(T)) {
	 case 1:
	 case 2:
	 case 4:
	 case 8:
	   // NOTE: this should work as long as realbuf is 16-byte aligned,
	   // which I enforce in the buffer alloc routine
	   if (backup) 
	     *(reinterpret_cast<T *>(backup)) = 
	       *(reinterpret_cast<T *>(realbufp.hp));
	   *(reinterpret_cast<T *>(realbufp.hp)) = data;
	   return;
	 default: 
	   if (backup) ::memcpy(backup, realbufp.hp, sizeof(T));
	   ::memcpy(realbufp.hp, &data, sizeof(T));
	   break;
	 } // switch
       } // else (devp)
    } // write

  }; // template aligned_access<T>


  extern void add_to_reslist(resolver_t **reslist, resolver_t *p);
  extern int playback_reslist(resolver_t *reslist, int oper);

  class writeCommit : public resolver_t {
    device_t *dev;
    devaddr_t addr;
    devdata_t *buf;
    devaddr_t len;
  public:
    writeCommit(device_t *d, devaddr_t a, const devdata_t *b, devaddr_t l);
    void commit();
    ~writeCommit();
  };

  class readRedoCommit : public resolver_t {
  public:
    readRedoCommit() : resolver_t(resolveRedo) {}
    void commit();
  };

  /************** Device tree functions *********************/

  typedef std::vector<std::string> args_t;
  
  extern device_t *lookup_device(const std::string &);

  typedef int (*cparser_t)(const args_t &);
  extern int parse_description(std::istream &, cparser_t=0);
  extern int parse_description(const std::string &, cparser_t=0);
  extern int parse_description_line(const std::string &, cparser_t=0);
  extern int get_addr_arg(const std::string &str, devaddr_t *nv);
  extern void form_args(const std::string &iline, args_t &argv);

  typedef device_t *(*instantiator_t)(device_t *,const args_t &);

  class registrar {
  public:
    registrar(std::string name, instantiator_t inst);
    ~registrar();
  };

  extern LSE_chkpt::error_t incrWriteChkpt(LSE_chkpt::file_t *cptFile);
  extern LSE_chkpt::error_t incrReadChkpt(LSE_chkpt::file_t *cptFile);

} // namespace LSE_device

// A debugging helper function to return a translation, forcing
// allocation while it does so.  It has a "C" interface so that it
// will be easy to find in a debugger.
  
extern "C" LSE_device::devtptr_t 
LSE_device_translate(LSE_device::device_t *dev, 
		     LSE_device::devaddr_t addr);
        
#endif /* _LSE_DEVICE_H_ */
