/* 
 * Copyright (c) 2000-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LSE access drop device model
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Defines a device model which drops all of its accesses
 *
 */
#include <LSE_device.h>
#include <string.h>

namespace LSE_device {

  class drop : public device_t {

  public:
    //    drop() : {}
    // ~drop() { }

    devaddr_t translate(devaddr_t addr, device_t **devpp, devtptr_t *datapp);
    // default writeSub/readSub already drop everything :-)
  };

  devaddr_t drop::translate(devaddr_t addr, 
			    device_t **devpp, devtptr_t *datapp) {
    *devpp = this;
    datapp->a = addr;
    return ~devaddr_t(0);
  }

  device_t *create_drop_inst(void) {
    return new drop();
  }

  device_t *create_drop_inst(device_t *parent, const args_t &argv) {
    devaddr_t len, base;
    if (argv.size() != 2) {
      std::cerr << "drop instantiation: bad arguments\n";
      return 0;
    } 
    if (get_addr_arg(argv[0], &base)) {
      std::cerr << "drop instantation: bad base\n";
      return 0;
    }
    if (get_addr_arg(argv[1], &len)) {
      std::cerr << "drop instantation: bad length\n";
      return 0;
    }
    device_t *nd = new drop();
    parent->addchild(base, len, nd);
    return nd;
  }

  registrar drop_registered("drop", create_drop_inst);

} // namespace LSE_device
