/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Device domain functions
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Define base device class methods as well as device management functions.
 * Note that it is possible to get into an infinite loop if you call a device
 * method (read/write/initialize) which has not been overridden, unless the 
 * translate method changes the device.  Thus these are really device methods 
 * for bridges.
 *
 */

#include <algorithm>
#include <list>
#include <string.h>
#include "LSE_device.h"
#include "LSE_memory.h"

namespace LSE_device {

  //////////////////////////////////////////////////////////////////////
  // Resolver stuff
  //////////////////////////////////////////////////////////////////////

  void resolver_t::commit() { }
  void resolver_t::rollback() { }
  resolver_t::~resolver_t() {}

  void add_to_reslist(resolver_t **reslist, resolver_t *p) {
    if (!reslist) {
      delete p;
      return;
    }
    while (*reslist) reslist = &(*reslist)->next;
    *reslist = p;
  }

  int playback_reslist(resolver_t *reslist, int oper) {
    int rval = 0;
    while (reslist) {
      resolver_t *t = reslist->next;
      if (!oper) {
	reslist->commit();
	delete reslist;
      } else if (oper == 1) {
	reslist->rollback();
	delete reslist;
      } else if (oper == 2) rval |= reslist->resolveFlag;
      else if (oper == 3) delete reslist;
      reslist = t;
    }
    return rval;
  }

  writeCommit::writeCommit(device_t *d, devaddr_t a, const devdata_t *b, 
			   devaddr_t l) :
    resolver_t(resolveCommit), dev(d), addr(a), len(l) {
    buf = new devdata_t[l];
    ::memcpy(buf, b, l * sizeof(devdata_t));
  }
  void writeCommit::commit() {
    dev->writeSub(addr, buf, len, writeOpForced, 0, 0);
  }
  writeCommit::~writeCommit() { delete[] buf; }
 
  void readRedoCommit::commit() { }
 
  //////////////////////////////////////////////////////////////////////
  // Base class
  //////////////////////////////////////////////////////////////////////

  void device_t::addchild(devaddr_t addr, devaddr_t len, 
			  class device_t *child) {
    throw deverror_t(deverror_illegalop);
  }

  void device_t::delchild(devaddr_t addr, devaddr_t len) {
    throw deverror_t(deverror_illegalop);
  }

  void device_t::delchild(device_t *child) {
    throw deverror_t(deverror_illegalop);
  }

  devaddr_t device_t::translate(devaddr_t addr, devdata_t **datapp) {
    throw deverror_t(deverror_illegalop);
  }

  devaddr_t device_t::writeSub(devaddr_t addr, const devdata_t *buffer, 
			       devaddr_t len, int writeFlags, 
			       devdata_t *backup, resolver_t **reslist) {
    return len;
  }

  devaddr_t device_t::readSub(devaddr_t addr, devdata_t *buffer, devaddr_t len,
			      resolver_t **reslist) {
    ::memset(buffer, 0, len); // return something consistent
    return len;
  }

  LSE_chkpt::error_t device_t::writeChkpt(LSE_chkpt::file_t *cptFile, 
					  bool isIncr) {

    LSE_chkpt::error_t cerr;
    LSE_chkpt::data_t *dp;

    // If we have already been written, nothing to do
    if (cptFile->ptrToID.find((void *)(this)) != cptFile->ptrToID.end())
      return LSE_chkpt::error_None;

    // assign ID.
    cptFile->ptrToID[(void *)this] = oid = ++cptFile->ptrID;
    
    dp = LSE_chkpt::build_header(0, tag_DEVICE,
				 LSE_chkpt::CLASS_CONTEXTSPECIFIC | 
				 LSE_chkpt::CONSTRUCTED, -1);
    if ((cerr = cptFile->write_to_segment(true, dp))) return cerr; 
    
    dp = LSE_chkpt::build_unsigned(0,oid);
    if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
    
    dp = LSE_chkpt::build_stringN(0, deviceType.c_str(), deviceType.size(),
				  true);
    if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
    
    dp = LSE_chkpt::build_stringN(0, name.c_str(), name.size(), true);
    if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
    
    if ((cerr = writeChkptGuts(cptFile, isIncr))) return cerr;
    
    proxy = this; // make sure proxy is reset.
    if (isIncr) {
      int ttype = LSE_chkpt::CLASS_CONTEXTSPECIFIC | LSE_chkpt::CONSTRUCTED;
      incrRecords = LSE_chkpt::build_explicit_tag(0, tag_DEVICETRACE, ttype);
      LSE_chkpt::build_unsigned(incrRecords, oid);
    } else incrRecords = 0;
    
    dp = LSE_chkpt::build_indefinite_end(0);
    return cptFile->write_to_segment(true,dp);
  }

  LSE_chkpt::error_t device_t::writeChkptGuts(LSE_chkpt::file_t *cptFile, 
					      bool isIncr) {

    return LSE_chkpt::error_None;
  }

  LSE_chkpt::error_t device_t::readChkptGuts(LSE_chkpt::file_t *cptFile, 
					     bool isIncr) {
    LSE_chkpt::error_t cerr;
    LSE_chkpt::data_t *dp;

    do {  // just suck up everything in the indefinite section
      if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
      if (!dp) break;
      delete dp;
    } while(1);
    return LSE_chkpt::error_None;
  }

  LSE_chkpt::error_t device_t::incrReadChkpt(LSE_chkpt::file_t *cptFile) {
    LSE_chkpt::error_t cerr;
    LSE_chkpt::data_t *dp;

    do {  // just suck up everything in the indefinite section
      if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
      if (!dp) break;
      delete dp;
    } while(1);
    return LSE_chkpt::error_None;
  }

  bool device_t::handleCommand(const char *cmd, ...) { return false; }

  // Write to a device

  devaddr_t device_t::write(devaddr_t addr, const devdata_t *buffer, 
			    devaddr_t len, int writeFlags, 
			    devdata_t *backup, resolver_t **reslist) 
  {
    devtptr_t realbufp;
    devaddr_t madelen;
    device_t *devp;
    devaddr_t olen = len;

    while (len) {
      madelen = translate(addr, &devp, &realbufp);
      madelen = std::min(madelen, len);
      if (devp) devp->writeSub(realbufp.a, buffer, madelen, writeFlags, 
			       backup, reslist);
      else { 
	if (backup) 
	  ::memcpy(backup, realbufp.hp, static_cast<size_t>(madelen));
	::memcpy(realbufp.hp, buffer, static_cast<size_t>(madelen));
      }
      addr += madelen;
      buffer += madelen;
      if (backup) backup += madelen;
      len -= madelen;
    }
    return olen;
  }

  // Read from a device
  devaddr_t device_t::read(devaddr_t addr, devdata_t *buffer,
			   devaddr_t len, resolver_t **reslist) {
    devtptr_t realbufp;
    devaddr_t madelen;
    device_t *devp;
    devaddr_t olen = len;

    while (len) {
      madelen = translate(addr, &devp, &realbufp);
      madelen = std::min(madelen, len);
      if (devp) devp->readSub(realbufp.a, buffer, madelen, reslist);
      else ::memcpy(buffer, realbufp.hp, static_cast<size_t>(madelen));
      addr += madelen;
      buffer += madelen;
      len -= madelen;
    }
    return olen;
  } // device_t::read

  // Set a range of addresses to a value

  devaddr_t device_t::memset(devaddr_t addr, const int value,
			     devaddr_t len) {
    devtptr_t realbufp;
    devaddr_t madelen;
    device_t *devp;

    while (len) {
      madelen = translate(addr, &devp, &realbufp);
      madelen = std::min(madelen, len);
      if (devp) {
	unsigned char v = static_cast<unsigned char>(value);
	for (devaddr_t i = 0; i < madelen; i++)
	  devp->write(realbufp.a++, &v, 1);
      } else {
	::memset(realbufp.hp, value, static_cast<size_t>(madelen));
	addr += madelen;
      }
      len -= madelen;
    }
    return addr;
  } // device_t::memset

  //////////////
  // compare memory with buffer.
  /////////////
  int device_t::memcmp(devaddr_t addr, const devdata_t *buffer,  
		       devaddr_t len) {

    devtptr_t realbufp;
    devaddr_t madelen;
    device_t *devp;

    while (len) {
      int cres;
      madelen = translate(addr, &devp, &realbufp);
      madelen = std::min(madelen, len);
      if (devp) {
	devdata_t rbuf[madelen];
	devp->read(realbufp.a, rbuf, madelen);
	cres = ::memcmp(buffer, rbuf, static_cast<size_t>(madelen));
      }
      else cres = ::memcmp(buffer, realbufp.hp, static_cast<size_t>(madelen));
      if (cres) return cres;
      addr += madelen;
      buffer += madelen;
      len -= madelen;
    }
    return 0;
  } // memcmp

  //////////////
  // read out until you find a zero.  Returns the actual length
  // read.  Won't go past len.  Includes the zero.  Note.  Only
  // good if data_t can be compared with zero!
  /////////////
  devaddr_t device_t::read_cstr(devaddr_t addr, devdata_t *buffer, 
				devaddr_t len) 
  {
    devtptr_t realbufp;
    devaddr_t madelen;
    device_t *devp;
    devaddr_t olen = len;

    while (len) {
      madelen = translate(addr, &devp, &realbufp);
      madelen = std::min(madelen, len);
      devdata_t *db = buffer;
      for (devaddr_t i = 0; i < madelen; i++) {
	devdata_t nv;
	if (!devp) nv = realbufp.hp[i];
	else devp->read(realbufp.a+i, &nv, 1);
	*(db++) = nv;
	if (!nv) {
	  /* we're done */
	  return olen - len + i + 1;
	}
      }
      addr += madelen;
      buffer += madelen;
      len -= madelen;
    }
    return olen;
  }

  LSE_chkpt::error_t incrWriteChkpt(LSE_chkpt::file_t *cptFile) {
    LSE_chkpt::error_t cerr;
    bool done = false;
    LSE_chkpt::data_t *dp;

    dp = LSE_chkpt::build_header(0, tag_DEVICERECS,
				 (LSE_chkpt::CLASS_CONTEXTSPECIFIC |
				  LSE_chkpt::CONSTRUCTED), -1 );
    if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;

    // write out all the incremental stuff available.
    for (LSE_chkpt::file_t::ptrToID_t::iterator 
	   i = cptFile->ptrToID.begin(), ie = cptFile->ptrToID.end();
	 i != ie ; ++i) {

      device_t *p = static_cast<device_t *>(i->first);
      if (p->incrRecords) {
	if ((cerr = cptFile->write_to_segment(true, p->incrRecords)))
	  return cerr;
	p->incrRecords = 0;
      }
    }

    dp = LSE_chkpt::build_indefinite_end(0);
    if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;

    return LSE_chkpt::error_None;
  }

  LSE_chkpt::error_t incrReadChkpt(LSE_chkpt::file_t *cptFile) {
    LSE_chkpt::error_t cerr;
    bool done = false;
    LSE_chkpt::data_t *dp;

    // read all the incremental stuff available.

    /* indefinite header for the sequence */
    if ((cerr = cptFile->read_taglen_from_segment(0,&dp))) return cerr;
    delete dp;
    
    do {
      // DEVICETRACE header
      if ((cerr = cptFile->read_taglen_from_segment(0,&dp))) return cerr;
      if (!dp) break; // done with traces
      delete dp;

      // read OID
      if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
      uint64_t oid = dp->content.uint64Val;
      delete dp;

      device_t *d = (device_t *)(cptFile->idToPtr[oid]);
      if (!d) return LSE_chkpt::error_FileFormat;
      if ((cerr = d->incrReadChkpt(cptFile))) return cerr;
    } while (1);

    return LSE_chkpt::error_None;
  }

} // namespace LSE_device

LSE_device::devtptr_t LSE_device_translate(LSE_device::device_t *dev, 
					   LSE_device::devaddr_t addr) {
  LSE_device::devtptr_t temp;
  LSE_device::device_t *devp;
  dev->translate(addr, &devp, &temp);
  return temp;
}


