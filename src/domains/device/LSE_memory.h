/* -*-c++-*- 
 * Copyright (c) 2000-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Template file for memories
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * The class defined by this template represents both an address space
 * as well as "normal" memory within that space.  Normal memory is not
 * implemented as a device as a performance optimization, saving a
 * virtual method call in the common case of access to normal memory.
 *
 * The main data structure is a mapping from address ranges to devices; it
 * is implemented using a helper class from LSE_device.h.  A null device
 * pointer implies that the address range is implemented by "normal" memory.
 *  
 * Normal memory is allocated in increments of (1<<allocbits) (modified to
 * be allocated on a boundary of (1<<alignbits)).  A mapping of target
 * address to host address is used to track allocated memory.  Memory is
 * allocated as it is accessed.
 * 
 */

#ifndef _LSE_MEMORY_H_
#define _LSE_MEMORY_H_

#include <LSE_device.h>
#include <map>
#include <set>
#include <cstring>

namespace LSE_device {

  template<unsigned int allocbits, unsigned int alignbits, 
	   unsigned int cpSize = 4096>
    class devmemory : public device_t {

  public:
    typedef devaddr_t addr_t; 
    typedef LSE_device::devdata_t devdata_t;

  protected:

    typedef devmemory<allocbits,alignbits,cpSize> thismemory_t;

     struct allocs_t {
       devdata_t *realp;
       devdata_t *alignp;
     };

     typedef std::map<devaddr_t, allocs_t> allocations_t;
     allocations_t allocations;
     typedef addr_range<devwrapptr_t> ranges_t;
     ranges_t ranges;

     typedef std::set<devaddr_t> checked_t;
     checked_t checked;

     //////////
     // Constructor
     //////////

    public:
    explicit devmemory() {
      char crazy[40];
      snprintf(crazy, 39, "devmemory<%d,%d,%d>", allocbits,alignbits,cpSize);
      deviceType = crazy;
    } // LSE_memory()

    explicit devmemory(uint64_t oid) : device_t(oid) {
      char crazy[40];
      snprintf(crazy, 39, "devmemory<%d,%d,%d>", allocbits,alignbits,cpSize);
      deviceType = crazy;
    } // LSE_memory()

     //////////
     // Destructor
     //////////

     ~devmemory() {
       for (typename ranges_t::iterator i = ranges.begin();
	    i != ranges.end(); ++i) {
	 delete i->second.data.dptr;
       }
       for (typename allocations_t::iterator i = allocations.begin();
	    i != allocations.end(); ++i) {
	 delete[] i->second.realp;
       }
     } // nothing special here


     //////////
     // Add a mapping
     //////////

     void addchild(const devaddr_t addr, const devaddr_t len, device_t *child) {
       devwrapptr_t p(child);
       ranges.add_range(addr, len, p);
       if (child) child->parent = this;
     }

     //////////// 
     // Delete a mapping
     /////////////

     void delchild(devaddr_t addr, devaddr_t len) {
       ranges.del_range(addr, len);
     } // delchild

     
     ///////////////
     // Translation
     ///////////////

  private:
     inline allocs_t *allocate(devaddr_t amask) {
       allocs_t na;

       na.realp = new devdata_t[(devaddr_t(1)<<allocbits) + 
				(devaddr_t(1)<<alignbits) - 1];
       ::memset(na.realp, 0, sizeof(devdata_t) * ((devaddr_t(1)<<allocbits) +
                                (devaddr_t(1)<<alignbits) - 1));
       na.alignp = (devdata_t *)(((long)na.realp + (long(1)<<alignbits)-1)
				 & ~((long(1)<<alignbits)-1));
       allocations[amask] = na;
       return &allocations[amask];
     }

  public:
     inline devaddr_t translate(devaddr_t addr, device_t **devpp,
				devtptr_t *datapp) {
  
       typename ranges_t::range_t *fnd = ranges.find_range(addr);

       if (fnd->data.dptr) { // report a subdevice

	 return std::min(fnd->last - addr + 1, 
			 fnd->data.dptr->translate(addr - fnd->first, 
						   devpp, datapp));
	 
       } else { // report memory

	 *devpp = 0;
	 devaddr_t amask = addr & ~((devaddr_t(1)<<allocbits)-1);
	 allocs_t *al;

	 typename allocations_t::iterator afnd = allocations.find(amask);
	 
	 if (afnd == allocations.end()) { // need to allocate this memory
	   al = allocate(amask);
	 } else al = &afnd->second;

	 datapp->hp = al->alignp + (addr - amask);

	 if (proxy->incrRecords) {
	   // to record all, must limit the size.
	   devaddr_t rmask = addr & ~(devaddr_t(cpSize-1));
	   checked_t &ck = static_cast<thismemory_t *>(proxy)->checked;

	   if (proxy->incrRecords && ck.find(rmask) == ck.end()) {
	     LSE_chkpt::build_unsigned(proxy->incrRecords, rmask);
	     LSE_chkpt::build_octetstring(proxy->incrRecords, 
					  al->alignp + (rmask - amask),
					  cpSize, true);
	     ck.insert(rmask);
	   }

	   return std::min(std::min(fnd->last - addr + 1, 
				    rmask + cpSize - 1 - addr + 1),
			   amask + (devaddr_t(1)<<allocbits) - 1 - addr + 1);
	 } else 
	   return std::min(fnd->last - addr + 1, 
			   amask + (devaddr_t(1)<<allocbits) - 1 - addr + 1);
       }
     } // translate
	 
     inline devaddr_t translate(devaddr_t addr, devdata_t **datapp) {
       device_t *devp;
       devtptr_t tp;
       devaddr_t ra = translate(addr, &devp, &tp);
       if (devp) throw deverror_t(deverror_illegalop);
       *datapp = tp.hp;
       return ra;
     } // translate

     inline void copyValsFrom(thismemory_t *src) {
       // delete old allocations
       for (typename allocations_t::iterator i = allocations.begin();
	    i != allocations.end(); ++i) {
	 delete[] i->second.realp;
       }
       allocations.clear();
       allocations = src->allocations; // shallow copy

       for (typename allocations_t::iterator i = allocations.begin();
	    i != allocations.end(); ++i) {
	 allocs_t na;
	 size_t ns = (devaddr_t(1)<<allocbits) + (devaddr_t(1)<<alignbits) - 1;
	 na.realp = new devdata_t[ns];
	 na.alignp = (devdata_t *)(((long)na.realp + (long(1)<<alignbits)-1)
				   & ~((long(1)<<alignbits)-1));
	 std::memcpy(na.realp, i->second.realp, ns);
	 i->second = na;
       }

       // Now copy the ranges
       // NOTE: not good for subdevices!  But we don't fork devices ;-)
       ranges = src->ranges; 

       // the theory here is that the destination must add incremental
       // records to the source so that the destination will have the
       // right stuff to copy.  It would be much easier if we could
       // link up the new device, but that is really up to the OS.
       proxy = src->proxy != static_cast<device_t *>(src) ? src->proxy : src;
      }

     /************************* checkpointing **************************/
     // Format for memory body is:
     // membody := SEQUENCE OF { start len ( device | 0 ) }
     //            SEQUENCE OF { start data }

     LSE_chkpt::error_t writeChkptGuts(LSE_chkpt::file_t *cptFile, 
				       bool isIncr) {
       LSE_chkpt::data_t *dp;
       LSE_chkpt::error_t cerr;

       if ((cerr = ranges.writeChkpt(cptFile, isIncr))) return cerr;

       if (isIncr) {
	 checked.clear();
	 return LSE_chkpt::error_None;
       }

       // write out memory contents
       
       dp = LSE_chkpt::build_header(0, LSE_chkpt::TAG_SEQUENCE, 
				    LSE_chkpt::CONSTRUCTED, -1);
       if ((cerr = cptFile->write_to_segment(true, dp))) return cerr; 
       
       for (typename allocations_t::iterator i = allocations.begin(), 
	      ie = allocations.end(); i != ie ; ++i) {
	 
	 dp = LSE_chkpt::build_unsigned(0, i->first);
	 if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
	 
	 dp = LSE_chkpt::build_octetstring(0, i->second.alignp,
					   (devaddr_t(1)<<allocbits), false);
	 if ((cerr = cptFile->write_to_segment(true, dp))) return cerr;
       }
       
       dp = LSE_chkpt::build_indefinite_end(0);
       return cptFile->write_to_segment(true,dp);
     }

     LSE_chkpt::error_t readChkptGuts(LSE_chkpt::file_t *cptFile, 
				      bool isIncr) {
       LSE_chkpt::error_t cerr;
       LSE_chkpt::data_t *dp;
       
       // should now be at the start of the range sequence.
       if ((cerr = ranges.readChkpt(cptFile, isIncr))) return cerr;

       // should now be at the memory contents

       if (isIncr) {
	 // end of tag_DEVICE
	 if ((cerr = cptFile->read_taglen_from_segment(0, &dp))) return cerr;
	 if (dp) { 
	   delete dp;
	   return LSE_chkpt::error_FileFormat;
	 }
	 return LSE_chkpt::error_None;
       }

       // SEQUENCE header
       if ((cerr = cptFile->read_taglen_from_segment(0, &dp))) return cerr;
       delete dp;
       
       // Funny code: if the allocation has already been made, reuse it, else
       // allocate it.  Need to know which were reused.

       allocations_t savealloc = allocations;

       do {  // just suck up everything in the indefinite section for now
	 if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
	 if (!dp) break;

	 devaddr_t addr = dp->content.uint64Val;
	 delete dp;

	 if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;

	 typename allocations_t::iterator ai = allocations.find(addr);
	 allocs_t *al;

	 if (ai == allocations.end()) al = allocate(addr);
	 else { savealloc.erase(addr); al = &ai->second; }

	 std::memcpy(al->alignp, dp->content.stringVal, 
		     (devaddr_t(1)<<allocbits));
	 delete dp;

       } while(1);

       for (typename allocations_t::iterator i = savealloc.begin(), 
	      ie = savealloc.end() ; i != ie ; ++i) {
	 delete[] i->second.realp;
	 allocations.erase(i->first);
       }
       
       // and eat the end of tag_DEVICE
       if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
       delete dp;

       return LSE_chkpt::error_None;
     } // readChkptGuts

     LSE_chkpt::error_t incrReadChkpt(LSE_chkpt::file_t *cptFile) {
       LSE_chkpt::error_t cerr;
       LSE_chkpt::data_t *dp;
       
       do {  // just suck up everything in the indefinite section
	 if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;
	 if (!dp) break;
	 uint64_t addr = dp->content.uint64Val;
	 delete dp;

	 if ((cerr = cptFile->read_from_segment(0,&dp))) return cerr;

	 devaddr_t amask = addr & ~((devaddr_t(1)<<allocbits)-1);
	 allocs_t *al;
	 
	 typename allocations_t::iterator afnd = allocations.find(amask);
	 if (afnd == allocations.end()) al = allocate(amask);
	 else al = &afnd->second;
	 
	 devdata_t *hp = al->alignp + (addr - amask);
	 std::memcpy(hp, dp->content.stringVal, cpSize);
	 delete dp;

       } while(1);
       return LSE_chkpt::error_None;
     }

  }; // template LSE_memory

}; // namespace LSE_device

#endif /* _LSE_MEMORY_H */

