/* 
 * Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LSE simple TOD device model
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Defines a device model for a TOD device which simply returns time since
 * the Epoch (12 am, Jan 1, 1970)
 *
 */
#include <LSE_device.h>
#include <LSE_clock.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>

namespace LSE_device {

  class simpletod : public device_t {
  protected:
    uint64_t true_initial_time;
    uint64_t time_offset;
    enum smethod { host, lse, clock } timemethod;
    union smeth {
      LSE_clock::clock_t *clock;
    } timemethodinfo;
    double scale_factor;

    friend device_t *create_simpletod_inst(device_t *parent, 
					   const args_t &argv);

  public:

    simpletod() : timemethod(host), scale_factor(1) { 
      true_initial_time = time_offset = time(0); 
    }
    simpletod(uint64_t nt) : timemethod(host), scale_factor(1) { 
      true_initial_time = time(0); 
      time_offset = nt;
    }
    ~simpletod() { }

    devaddr_t translate(devaddr_t addr, device_t **devpp, devtptr_t *datapp);
    // default writeSub already drops the write
    devaddr_t readSub(devaddr_t addr, devdata_t *buffer, devaddr_t len,
		      resolver_t **reslist=0);

    bool handleCommand(const char *cmd, ...);

  };

  devaddr_t simpletod::translate(devaddr_t addr, 
				 device_t **devpp, devtptr_t *datapp) {
    *devpp = this;
    datapp->a = addr;
    return ~devaddr_t(0);
  }

  devaddr_t simpletod::readSub(devaddr_t addr, devdata_t *buffer, 
			       devaddr_t len, resolver_t **reslist) {
    if (reslist) { // speculative read
      add_to_reslist(reslist, new readRedoCommit());
      ::memset(buffer, 0, static_cast<size_t>(len));
      return len;
    }
    uint64_t nowtime;

    switch (timemethod) {
    case clock:
      // Not the right thing in the end, but will do for now.
      nowtime = uint64_t(timemethodinfo.clock->get_cycle()*scale_factor) 
	+ time_offset;
      break;
    case lse:
      nowtime = uint64_t(LSE_time_now*scale_factor) + time_offset;
      break;
    default: // host
      nowtime = (uint64_t)((time(0) - true_initial_time)*scale_factor) 
	+ time_offset;
      break;
    }

    //std::cerr << "Reading time " << nowtime << std::endl;

    for (int i=0; i < (int)len; i++) {
      unsigned int raddr = (addr + i) % 8;

      // big-endian device, apparently
      buffer[i] = (nowtime >> ((7-raddr)*8)) & 0xff;
    }
    return len;
  } // simpletod::read
  
  bool simpletod::handleCommand(const char *cmd, ...) {
    args_t argv;
    form_args(cmd, argv);

    va_list ap;
    va_start(ap, cmd);

    if (!argv.size()) goto nothandled;

    if (argv[0] == "setmethod") {
      if (argv.size() < 2) {
	std::cerr << "simpletod command: missing method name";
	goto nothandled;
      }
      if (argv[1] == "lse") {
	timemethod = lse;
	scale_factor = va_arg(ap, double);
	goto handled;
      } else if (argv[1] == "host") {
	timemethod = host;
	scale_factor = va_arg(ap, double);
	goto handled;
      } else if (argv[1] == "clock") {
	timemethod = clock;
	scale_factor = va_arg(ap, double);
	timemethodinfo.clock = va_arg(ap, LSE_clock::clock_t *);
	goto handled;
      } else {
	std::cerr << "simpletod command: unknown method name\n";
	goto nothandled;
      }
    } else std::cerr << "simpletod command: unknown command\n";

  nothandled:
    va_end(ap);
    return false;
  handled:
    va_end(ap);
    return true;
  }


  device_t *create_simpletod_inst() {
    return new simpletod();
  }

  device_t *create_simpletod_inst(device_t *parent, const args_t &argv) {
    devaddr_t len, base;
    simpletod *nd;

    if (argv.size() < 2 || argv.size() > 5) {
      std::cerr << "simpletod instantiation: bad arguments\n";
      return 0;
    } 
    if (get_addr_arg(argv[0], &base)) {
      std::cerr << "simpletod instantation: bad base\n";
      return 0;
    }
    if (get_addr_arg(argv[1], &len)) {
      std::cerr << "simpletod instantation: bad length\n";
      return 0;
    }
    if (argv.size() > 2) { // initial time value
      errno = 0; // for some reason, this ought to be cleared here...
      uint64_t time_offset = strtoll(argv[2].c_str(), 0, 0);
      if (errno == ERANGE) {
	std::cerr << "simpletod instantiation: bad initial time argument '" 
		  << argv[2].c_str() << "'\n";
	return 0;
      }
      nd = new simpletod(time_offset);

      if (argv.size() > 3) { // time method
	if (argv[2] == "lse") {
	  if (argv.size() < 4) {
	    std::cerr << "simpletod instantation: missing time method args\n";
	    return 0;
	  }
	  nd->timemethod = simpletod::lse;
	  nd->scale_factor = strtod(argv[3].c_str(), 0);

	} else if (argv[2] == "host") {
	  if (argv.size() < 4) {
	    std::cerr << "simpletod instantation: missing time method args\n";
	    return 0;
	  }
	  nd->timemethod = simpletod::host;
	  nd->scale_factor = strtod(argv[3].c_str(), 0);

	} else if (argv[2] == "clock") {
	  if (argv.size() < 4) {
	    std::cerr << "simpletod instantation: missing time method args\n";
	    return 0;
	  }
	  nd->scale_factor = strtod(argv[3].c_str(), 0);
	  // TODO: look up the clock somehow.
	} else {
	  std::cerr << "simpletod instantiation: unknown method name\n";
	  return 0;
	}
      }
    } else nd = new simpletod();
    parent->addchild(base, len, nd);
    return nd;
  }

  registrar simpletod_registered("simpletod", create_simpletod_inst);

} // namespace LSE_device
