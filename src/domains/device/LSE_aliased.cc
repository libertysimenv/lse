/* 
 * Copyright (c) 2000-2008 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * LSE aliased device model
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Defines a device model which simply aliases the addresses.  The purpose
 * is to allow devices and memory locations to appear at multiple addresses
 * in the physical memory, as is often the case.
 *
 */
#include <LSE_device.h>
#include <queue>
#include <stdio.h>
#include <sys/select.h>

namespace LSE_device {

  class aliased : public device_t {
  private:
    devaddr_t len, destbase, destlen;

  public:

    aliased(devaddr_t al, devaddr_t b, devaddr_t l) : 
      len(al), destbase(b), destlen(l)
    {  }

    ~aliased() { }

    devaddr_t translate(devaddr_t addr, device_t **devpp, devtptr_t *datapp);
  };

  devaddr_t aliased::translate(devaddr_t addr, 
			       device_t **devpp, devtptr_t *datapp) {
    devaddr_t na = parent->translate(addr % destlen + destbase, devpp, datapp);
    return std::min(len - addr, na);
  }
  
  device_t *create_aliased_inst(devaddr_t al, devaddr_t b, devaddr_t l) {
    return new aliased(al, b, l);
  }

  device_t *create_aliased_inst(device_t *parent, const args_t &argv) {
    devaddr_t len, base, destbase, destlen;
    if (argv.size() != 4) {
      std::cerr << "aliased instantiation: bad arguments\n";
      return 0;
    } 
    if (get_addr_arg(argv[0], &base)) {
      std::cerr << "aliased instantation: bad base\n";
      return 0;
    }
    if (get_addr_arg(argv[1], &len)) {
      std::cerr << "aliased instantation: bad length\n";
      return 0;
    }
    if (get_addr_arg(argv[2], &destbase)) {
      std::cerr << "aliased instantation: bad dest base\n";
      return 0;
    }
    if (get_addr_arg(argv[3], &destlen)) {
      std::cerr << "aliased instantation: bad dest len\n";
      return 0;
    }
    device_t *nd = new aliased(len, destbase, destlen);
    parent->addchild(base, len, nd);
    return nd;
  }

  registrar aliased_registered("aliased", create_aliased_inst);

} // namespace LSE_device
