/* 
 * Copyright (c) 2000-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Device domain functions
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Define base device class methods as well as device management functions.
 * Note that it is possible to get into an infinite loop if you call a device
 * method (read/write/initialize) which has not been overridden, unless the 
 * translate method changes the device.  Thus these are really device methods 
 * for bridges.
 *
 */

#include <algorithm>
#include <fstream>
#include <list>
#include <stdlib.h>
#include <string>
#include "LSE_device.h"
#include "LSE_memory.h"

namespace LSE_device {

  const char *errstring(const deverror_code_t errnum) {
    switch (errnum) {
    case deverror_none: return "";
    case deverror_illegalop : return "Illegal operation";
    case deverror_unmapped : return "Access to unmapped address";
    case deverror_fileerror : return "File Error";
    default :
      return "Unknown device error";
    }
  } // errstring
  
  /****************** data structures for device trees *************/

  typedef LSE_device::devmemory<21,3> system_t;
  extern device_t *create_drop_inst();

  typedef std::map<std::string, device_t *> devices_t;
  devices_t LSE_devices;
  std::list<device_t *> unnamed_devices;
  device_t *dropper;

  /******************* device class registrar **********************/

  // NOTE: registrations is *not* just declared as a map but goes through
  // a construct-on-first-use idiom so that valgrind does not complain
  // about the registration being leaked.  Oddly enough, I could not
  // get ride of the valgrind complaint even with an atexit routine to
  // clean it up.
  //
  // NOTE: do not look at registrations during any destructor of a 
  // static / global variable, since we cannot guarantee the order in
  // which they will be destroyed!

  typedef std::map<std::string, instantiator_t> registrations_t;

  static registrations_t *registrations() {
    static registrations_t regs;
    return &regs;
  }

  registrar::registrar(std::string name, instantiator_t inst) {
    (*registrations())[name] = inst;
  }  

  registrar::~registrar() {}

  extern registrar rom_registered, drop_registered, diskimage_registered,
    simpletod_registered, ns16550_registered, aliased_registered;
  registrar *standard_devs[] = { // exists only to pull in all the devices!
    &rom_registered, &drop_registered, &diskimage_registered, 
    &simpletod_registered, &ns16550_registered, &aliased_registered,
  };

  /**************** Device tree construction ************************/

  extern device_t *lookup_device(const std::string &n) {
    devices_t::const_iterator di = LSE_devices.find(n);
    return di == LSE_devices.end() ? 0 : di->second;
  }

  int get_addr_arg(const std::string &str, devaddr_t *nv) {
    const char *s = str.c_str();
    char *ep;
    *nv = strtoll(s, &ep, 0);
    switch (*ep) {
    case 'k':
    case 'K' : *nv *= 1024; ep++; break;
    case 'm':
    case 'M' : *nv *= 1024*1024; ep++; break;
    case 'g':
    case 'G' : *nv *= 1024*1024*1024; ep++; break;
    case 's':
    case 'S' : { // s for shift (b for bits conflicts with hex digits!)
      int bits = strtol(ep+1, &ep, 0);
      *nv <<= bits;
    }
    default: break;
    }
    return (!!*ep);
  } // get_addr_arg

  void form_args(const std::string &iline, args_t &argv) {
    std::string::size_type line=0, last=iline.size();

#ifdef DEBUG
    std::cout << iline << std::endl;
#endif

    // First, chop after "//"
    std::string::size_type ll = iline.find("//");
    if (ll != std::string::npos) last = ll;

    while (last != line) {
      std::string::size_type l;
      line = iline.find_first_not_of(" \t",line); // remove whitespace
      if (line == std::string::npos || line == last) break;
      if (iline[line] == '"') {
	l = iline.find('"',line+1);
	if (l == std::string::npos || l >= last) {
	  argv.push_back(iline.substr(line+1,last-line-1));
	  break; // end of line
	} else {
	  argv.push_back(iline.substr(line+1,l-line-1));
	  line = l + 1;
	}
      } else {
	l = iline.find_first_of(" \t",line);
	if (l == std::string::npos || l >= last) {
	  argv.push_back(iline.substr(line,last-line));
	  break; // end of line
	} else {
	  argv.push_back(iline.substr(line,l-line));
	  line = l;
	}
      }
    }
  }

  int parse_description_line(const std::string &iline, cparser_t parser) {

    std::string::size_type line=0, last=iline.size();
    std::vector<std::string> argv;

#ifdef DEBUG
    std::cout << iline << std::endl;
#endif

    form_args(iline, argv);

    int error = 0;

    if (!argv.size()) return 0;

#ifdef DEBUG
    for (std::vector<std::string>::const_iterator i=argv.begin();
	 i != argv.end(); ++i) {
      std::cout << '<' << *i << '>';
    }
    std::cout << std::endl;
#endif

    if (argv[0] == "system") {       // define a system

      if (argv.size() != 2) goto bad_arguments;

      system_t *newsys;
      LSE_devices[argv[1]] = newsys = new system_t();
      newsys->name = argv[1];

    } else if (argv[0] == "memory") { // define a memory range

      if (argv.size() != 4 && argv.size() != 5) goto bad_arguments;

      devices_t::iterator di = LSE_devices.find(argv[1]);
      if (di == LSE_devices.end()) goto unknown_parent;

      devaddr_t base, len;
      if (get_addr_arg(argv[2], &base)) goto bad_number;
      if (get_addr_arg(argv[3], &len)) goto bad_number;

      if (argv.size() == 4) 
	di->second->addchild(base, len, 0);

      else if (argv[4] == "drop") {

	if (!dropper) {
	  dropper = create_drop_inst();
	  unnamed_devices.push_back(dropper);
	}
	di->second->addchild(base, len, dropper);

      } else if (argv[4] == "rom") {

	args_t nargs(argv.begin()+2, argv.begin()+4);
	device_t *rom = (*(registrations()->find("rom")->second))(di->second,
								nargs);
	unnamed_devices.push_back(rom);

      } else goto bad_arguments;

    } else if (argv[0] == "device") {

      if (argv.size() < 4) goto bad_arguments;

      devices_t::iterator di = LSE_devices.find(argv[1]);
      if (di == LSE_devices.end()) goto unknown_parent;

      std::string uname = argv[1] + "." + argv[2];

      if (!registrations()->count(argv[3])) goto unknown_device;

      std::vector<std::string> nargs(argv.begin()+4, argv.end());
      device_t *ndev = (*(registrations()->find(argv[3])->second))(di->second,
								 nargs);

      LSE_devices[uname] = ndev;
      ndev->name = uname;

    } else if (argv[0] == "load") {

      if (argv.size() < 2) goto bad_arguments;
      if (argv[1] == "bin") {
	if (argv.size() < 5) goto bad_arguments;
	
	devices_t::iterator di = LSE_devices.find(argv[2]);
	if (di == LSE_devices.end()) goto unknown_parent;
	
	devaddr_t saddr;
	if (get_addr_arg(argv[3], &saddr)) goto bad_number;
	
	std::ifstream ifs(argv[4].c_str());
	if (ifs.fail()) goto bad_file;
	
	char ch;
	while (ifs.get(ch)) 
	  LSE_device::aligned_access<char>(di->second).write(saddr++, ch, 
							     true);

      } else if (argv[1] == "image") {
	if (argv.size() < 4) goto bad_arguments;

	devices_t::iterator di = LSE_devices.find(argv[2]);
	if (di == LSE_devices.end()) goto unknown_parent;
	
	std::ifstream ifs(argv[3].c_str());
	if (ifs.fail()) goto bad_file;
	
	devaddr_t counter = 0;

	while (!ifs.eof()) {
	  char linebuf[300];
	  unsigned char valbuf[150];
	  ifs.getline(linebuf, 300);
	  //if (!isprint(linebuf[0])) goto bad_file_format;

	  char *comment = strstr(linebuf,"//");
	  if (comment) *comment = 0;

	  char *tok;
	  char *silly;
	  tok = strtok_r(linebuf, " \t", &silly);

	  while (tok) {

	    if (tok[0] == '@') { // new address
	      
	      char *dummy;
	      counter = static_cast<devaddr_t>(strtoll(tok+1, &dummy, 16));

	    } else { // must be number; take two digits at a time
	      int sl = strlen(tok);
	      for (int i = 0; i < sl; i+=2) {
		valbuf[i/2] = ( (tok[i]-'0' - (tok[i]>'9' ? 'a'-'0'-10 : 0)) 
				* 16 +
				(tok[i+1]-'0' - (tok[i+1]>'9' ? 'a'-'0'-10 : 0)
				 ) );
	      }
	      // map in if not otherwise there.
	      try {
		di->second->write(counter, valbuf, sl/2);
	      } catch (LSE_device::deverror_t e) {
		// TODO: do something with this
		std::cerr << "Bad write to " << std::hex << counter << "\n";
	      }
	      //std::cerr << "Writing " << sl << " to " << std::hex << counter << std::dec << "\n";
	      counter += sl/2;
	    }
	    tok = strtok_r(NULL, " \t", &silly);
	  }
	}
	
      } else goto bad_arguments;

    } else if (parser) {

      error = (*parser)(argv);
      if (error) goto report_error;

    } else {

      std::cerr << "LSE_device: unknown configuration file command\n";
      error = 4;
      goto report_error;

    }

    return 0;

  unknown_device:
    std::cerr << "LSE_device: Unknown device type\n";
    error = 5;
    goto report_error;

  bad_number:
    std::cerr << "LSE_device: invalid number format\n";
    error = 3;
    goto report_error;

  bad_file:
    std::cerr << "LSE_device: unable to open file for reading\n";
    error = 6;
    goto report_error;

  bad_file_format:
    std::cerr << "LSE_device: memory image file has bad format\n";
    error = 7;
    goto report_error;

  bad_arguments:
    std::cerr << "LSE_device: incorrect arguments to command\n";
    error = 1;
    goto report_error;

  unknown_parent:
    std::cerr << "LSE_device: Unknown parent system or device name\n";
    error = 2;
    goto report_error;

  report_error:
    std::cerr << "\tConfiguration line was: '" << iline << "'" << std::endl;
    return error;
  } // parse_description_line

  int parse_description(std::istream &is, cparser_t parser) {
    int err, rerr=0;
    while (!is.eof()) {
      std::string s;
      getline(is, s);
      err = parse_description_line(s.c_str(), parser);
      if (err && !rerr) rerr = err;
    }
    return rerr;
  } // parse_description

  int parse_description(const std::string &s, cparser_t parser) {
    std::istringstream is(s);
    return parse_description(is);
  }

} // namespace LSE_device
