# networks
N toMem 1
N n1-21  0
N nP1 	0
# memories (must go before caches so we get server numbers right)
M Mem toMem
# caches (must go before processors so we get server numbers right!)
C L2.1	1 1 32 n1-21	toMem	NoneWB	WrEarly WrAlloc 
I L2.1  1 Snoop	2 noCOWI noSIWAI
C L1.1	1 1 32 nP1	n1-21	MSI	WrEarly
I L1.1  1 Snoop	2 noCOWI noSIWAI
I L1.1  0 Snoop	2 noCOWI noSIWAI

# processors
P P1 	nP1 

# Time to run

R P1 0 4	# get into caches
Q

O ======================================================================
O ====================== Test hits =====================================
O ======================================================================

W P1 0 4 noCoh noCache noWT Alloc
Q
W P1 0 4 noCoh noCache noWT noAlloc
Q
W P1 0 4 noCoh noCache WT Alloc
Q
W P1 0 4 noCoh noCache WT noAlloc
Q

W P1 0 4 Coh noCache noWT Alloc
Q
W P1 0 4 Coh noCache noWT noAlloc
Q
W P1 0 4 Coh noCache WT Alloc
Q
W P1 0 4 Coh noCache WT noAlloc
Q

W P1 0 4 noCoh Cache noWT Alloc
Q
W P1 0 4 noCoh Cache noWT noAlloc
Q
W P1 0 4 noCoh Cache WT Alloc
Q
W P1 0 4 noCoh Cache WT noAlloc
Q

W P1 0 4 Coh Cache noWT Alloc
Q
W P1 0 4 Coh Cache noWT noAlloc
Q
W P1 0 4 Coh Cache WT Alloc
Q
W P1 0 4 Coh Cache WT noAlloc
Q

O ======================================================================
O ====================== Test misses ===================================
O ======================================================================

W P1 20 4 noCoh noCache noWT Alloc
Q
W P1 40 4 noCoh noCache noWT noAlloc
Q
W P1 60 4 noCoh noCache WT Alloc
Q
W P1 80 4 noCoh noCache WT noAlloc
Q

W P1 a0 4 Coh noCache noWT Alloc
Q
W P1 c0 4 Coh noCache noWT noAlloc
Q
W P1 e0 4 Coh noCache WT Alloc
Q
W P1 100 4 Coh noCache WT noAlloc
Q

W P1 120 4 noCoh Cache noWT Alloc
Q
W P1 140 4 noCoh Cache noWT noAlloc
Q
W P1 160 4 noCoh Cache WT Alloc
Q
W P1 180 4 noCoh Cache WT noAlloc
Q

W P1 1a0 4 Coh Cache noWT Alloc
Q
W P1 1c0 4 Coh Cache noWT noAlloc
Q
W P1 1e0 4 Coh Cache WT Alloc
Q
W P1 200 4 Coh Cache WT noAlloc
Q


E
