/* 
 * Copyright (c) 2005-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Test the LSE cache support domain - processors
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This is a processor model.  It does require that you have defined a whole
 * bunch of other types before you include it; the modularization here is
 * simply to make it easy to copy the file and modify it separately (i.e., to
 * run real programs)
 *
 */

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <list> 
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

#ifdef DATA_WIDTH
  unsigned char dataBuf[DATA_WIDTH];
#endif

class Proc : public Unit {
public:
  addr_t reservationAddr;
  bool reservationValid;

  Proc(const char *name, Network *lower) 
    : Unit(name,NULL,lower), reservationAddr(0), 
      reservationValid(false) { }

  Proc(istringstream &ist, const string &name, 
       map<string,Network *>& networks,
       const string command) {

    string net;
      
    ist >> net;

    Unit::setup(name.c_str(),NULL,networks[net]);

    reservationValid = false;
    reservationAddr = 0;
  }

  bool is_overlapped(addr_t addr, int len) {
    addr_t taddr = addr & ~(addr_t(7));
    int tsize = ((addr + len + 7) & ~(addr_t(7))) - taddr;
    return (reservationValid &&
	    (!((taddr + tsize <= reservationAddr) ||
		 (reservationAddr + 8 <= taddr))));
  }

  // handle a message received on the lower interface
  void handleMsg(const msg_t &m, int interNo) {
    transaction_t *t;
    msg_t m2;
    bool snooped = m.dest != this;

    if (!quiet && !snooped) cout << idStr << " received " <<  m << "\n";

#ifdef DATA_WIDTH
    if (doData) {
      int i;
      if (!snooped && !quiet &&
	  (m.pmsg.type == LSE_cache_msgtype_WrReq ||
	   m.pmsg.type == LSE_cache_msgtype_RdAck ||
	   (m.pmsg.type == LSE_cache_msgtype_InvalAck &&
	    (m.pmsg.flags & LSE_cache_msgflagbit_WithData))
	   )) {
	cout << "\tdata: " << hex << setfill('0');
	for (i=0;i<m.len;i++) 
	  cout << hex << setw(2) << (((unsigned int)m.data[i])&0xff);
	cout << setfill(' ') << dec << '\n';
      }
    }
#endif

    switch (m.pmsg.type) {
    case RdAck :
#ifdef DATA_WIDTH
      if (doData) memcpy(&dataBuf[0],&m.data[0],m.len);
#endif      
    case WrAck :
    case RdWrAck :
      if (snooped) break;
      t = find(m.replyID);
      if (t) {
	forget(t);
	delete t;
      }
      break;
    case WrReq : // snoop writes
      if ( is_overlapped(m.addr, m.len) ) {
	reservationValid = false;
	if (!quiet) cout << idStr << " clearing reservation\n";
      } 
      break;
    case InvalReq :
      if ( is_overlapped(m.addr, m.len) ) {
	reservationValid = false;
	if (!quiet) cout << idStr << " clearing reservation\n";
      } 
      m2 = m;
      m2.replyID = m.replyID;
      m2.destID = m.sendID;
      m2.pmsg.reply = LSE_cache_msgreply_OK;
      m2.pmsg.type = InvalAck;
      m2.pmsg.flags = 0;
      sendMsg(msgUnits(m2,1),1);
      break;
    default: break;
    }
  }

  void read(addr_t addr, addr_t len, bool coherent, bool cacheable,
	    bool toown, bool noalloc, bool withdata, bool locked) {
    msg_t m;

    m.pmsg.type = RdReq;
    m.pmsg.flags = ((cacheable ? LSE_cache_msgflagbit_Cacheable : 0) |
		    (coherent  ? LSE_cache_msgflagbit_Coherent : 0) |
		    (noalloc  ? LSE_cache_msgflagbit_NoAlloc : 0) |
		    (false  ? LSE_cache_msgflagbit_Prefetch : 0) |
		    (toown  ? LSE_cache_msgflagbit_ToOwn : 0) |
		    (withdata  ? LSE_cache_msgflagbit_WithData : 0));
    m.addr = m.firstAddr = addr;
    m.len = len;
    m.destID = 0;
    
    if (locked) {
      reservationValid = true;
      reservationAddr = m.addr & ~(addr_t(7));
    }

    transaction_t *t = new transaction_t(m,1);

    remember(t);
    t->m.replyID = t->id;
    sendMsg(msgUnits(t->m,1),1);
  }

  void write(addr_t addr, addr_t len, bool coherent,
	     bool cacheable, bool wt, bool noalloc, bool conditional) {
    msg_t m;

    if (conditional && !reservationValid) {
      if (!quiet) cout << "\tReservation not valid\n";
      return;
    }

    m.pmsg.type = WrReq;
    m.pmsg.flags = ((cacheable ? LSE_cache_msgflagbit_Cacheable : 0) |
		    (coherent  ? LSE_cache_msgflagbit_Coherent : 0) |
		    (noalloc  ? LSE_cache_msgflagbit_NoAlloc : 0) |
		    (wt  ? LSE_cache_msgflagbit_WriteThrough : 0) |
		    (conditional  ? LSE_cache_msgflagbit_Conditional : 0));
    m.addr = m.firstAddr = addr;
    m.len = len;
    m.destID = 0;

    transaction_t *t = new transaction_t(m,1);

    remember(t);
    t->m.replyID = t->id;

#ifdef DATA_WIDTH
    if (doData) memcpy(&m.data[0],&dataBuf[0],m.len);
#endif

    sendMsg(msgUnits(t->m,1),1);
  }

};


