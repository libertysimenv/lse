/* 
 * Copyright (c) 2005-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Test the LSE cache support domain - system
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This class represents an entire system and provides nice interfaces
 * so that we can build test systems, including ones that run real programs.
 *
 */

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <list> 
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

#include <Common.h>
#include <Proc.h>
#include <Mem.h>
#include <Cache.h>

class System {
 public:
  bool quiet;
  typedef class priority_queue<class msg_t> MsgList_t;
  MsgList_t simmessages;

  void enqueue(msg_t &m, int interNo) {
    // cout << "Enqueing " << m << "\n";
    m.rtime = timeCount + (userandom ? (int)((double(rand())/RAND_MAX)*8) : 1);
    simmessages.push(m);
  }

  void run_step(void) {
    if (!quiet) cout << "==== Time " << timeCount << " ====\n";
    while (!simmessages.empty()) {
      msg_t m = simmessages.top();
      if (m.rtime > timeCount) break;
      
      // handle the message
      // cout << "Dequeueing " << m << "\n";
      simmessages.pop();
      m.sender->interfaces[m.interNo]->send(m);
      
    }
    timeCount++;
  }

  void run_till_quiet(void) {
    do {
      run_step();
    } while (!simmessages.empty());
  }

 private:
  map<string,Network *> networks;
  map<string,Proc *> processors;
  map<string,Cache *> caches;

 public:
  vector<Proc *> processorvec;
  map<string,Mem *> memories;
  vector<Cache *> cachevec;

 public:
  bool untimed, userandom;

  System(istream &cfile, bool ut, bool ur, bool q) 
    : untimed(ut), userandom(ur) {

    quiet = q;
    if (userandom) untimed = false;

    string command;

    /* read configuration file */
    while (!cfile.eof()) {

      getline(cfile,command,'\n');
    
      if (command[0] == '#' || command=="") continue; // comment

      else if (command[0] == 'N') { /* networks */

	istringstream ist(command);
	string name, dummy;
	bool snoop;
      
	if (!quiet) cout << "--> " << command << '\n';
	ist >> dummy >> name >> snoop;
	networks[name] = new Network(this, name.c_str(),snoop);
	networks[name]->quiet = quiet;

      } else if (command[0] == 'P') { /* processors */

	istringstream ist(command);
	string name, dummy, net;
      
	if (!quiet) cout << "--> " << command << '\n';
	ist >> dummy >> name;
	processors[name] = new Proc(ist, name, networks, command);
	processorvec.push_back(processors[name]);
	processors[name]->quiet = quiet;

      } else if (command[0] == 'C') { /* caches */

	istringstream ist(command);
	string name, dummy;
      
	if (!quiet) cout << "--> " << command << '\n';
	ist >> dummy >> name;
	caches[name] = new Cache(ist, name, networks, command);
	caches[name]->quiet = quiet;
	cachevec.push_back(caches[name]);

      } else if (command[0] == 'M') { /* memories */

	istringstream ist(command);
	string name, dummy, net;
      
	if (!quiet) cout << "--> " << command << '\n';
	ist >> dummy >> name >> net;
	memories[name] = new Mem(name.c_str(),networks[net]);
      	memories[name]->quiet = quiet;

      } else if (command[0] == 'I') { /* interfaces */

	istringstream ist(command);
	string name, dummy;
      
	if (!quiet) cout << "--> " << command << '\n';
	ist >> dummy >> name;
	caches[name]->setupInterface(ist,command);
	
      } else if (command[0] == 'E') break; // Done!
    
      else if (command[0] == 'O') {
	if (!quiet) cout << "--> " << command << '\n';
      } else if (command[0] == 'Q')  { // Run until quiescent
	if (!quiet) cout << "--> " << command << "\n\n";
	run_till_quiet();
      }
    
      else if (command[0] == 'S') { // Run one time step
	if (!quiet) cout << "--> " << command << "\n\n";
	run_step();
      }
      
      else if (command[0] == 'W') { // do a write
	istringstream ist(command);
	string proc, dummy;
	int addr, len;
	bool Coherent = true;
	bool Cacheable = true;
	bool WriteThrough = false;
	bool NoAllocate = false;
	bool Conditional = false;

	if (!quiet) cout << "\n--> " << command << "\n";
	ist >> dummy >> proc >> hex >> addr >> dec >> len;
      
	while (!ist.eof()) {
	  string word;
	  ist >> word;
	  if (word == "" || word[0] == '#') break;
	  else if (word == "noCoh") Coherent = false;
	  else if (word == "Coh") Coherent = true;
	  else if (word == "noCache") Cacheable = false;
	  else if (word == "Cache") Cacheable = true;
	  else if (word == "noWT") WriteThrough = false;
	  else if (word == "WT") WriteThrough = true;
	  else if (word == "noAlloc") NoAllocate = true;
	  else if (word == "Alloc") NoAllocate = false;
	  else if (word == "Cond") Conditional = true;
	  else if (word == "noCond") Conditional = false;
	  else {
	    cerr << "Unrecognized flag " << word << " in " << command << '\n';
	    exit(2);
	  }
	}
	processors[proc]->write(addr,len,Coherent,Cacheable,WriteThrough,
				NoAllocate, Conditional);
      }
    
      else if (command[0] == 'R') { // do a read
	istringstream ist(command);
	string proc, dummy;
	int addr, len;
	bool Coherent = true;
	bool Cacheable = true;
	bool ToOwn = false;
	bool NoAllocate = false;
	bool WithData = true;
	bool Locked = false;
	if (!quiet) cout << "\n--> " << command << "\n";
	ist >> dummy >> proc >> hex >> addr >> dec >> len;

	while (!ist.eof()) {
	  string word;
	  ist >> word;
	  if (word == "" || word[0] == '#') break;
	  else if (word == "noCoh") Coherent = false;
	  else if (word == "Coh") Coherent = true;
	  else if (word == "noCache") Cacheable = false;
	  else if (word == "Cache") Cacheable = true;
	  else if (word == "noOwn") ToOwn = false;
	  else if (word == "Own") ToOwn = true;
	  else if (word == "noAlloc") NoAllocate = true;
	  else if (word == "Alloc") NoAllocate = false;
	  else if (word == "noData") WithData = false;
	  else if (word == "Data") WithData = true;
	  else if (word == "Locked") Locked = true;
	  else if (word == "noLocked") Locked = false;
	  else {
	    cerr << "Unrecognized flag " << word << " in " << command << '\n';
	    exit(2);
	  }
	}
      
	processors[proc]->read(addr,len,Coherent,Cacheable,ToOwn,NoAllocate,
			       WithData, Locked);
      
      } else {
	cerr << "Unknown command " << command << '\n';
	exit(2);
      }
    
    } /* while !cfile.eof */

  } /* System */

  void report(ostream &os) {
    for (map<string,Cache *>::const_iterator i = caches.begin();
	 i != caches.end();i++) {
      i->second->report(os);
    }
    for (map<string,Mem *>::const_iterator i = memories.begin();
	 i != memories.end();i++) {
      i->second->report(os);
    }
  }

  void setQuiet(bool nq) {
    quiet = nq;

    for (map<string,Network *>::const_iterator i=networks.begin();
	 i != networks.end(); i++)
      i->second->quiet = nq;

    for (map<string,Cache *>::const_iterator i=caches.begin();
	 i != caches.end(); i++)
      i->second->quiet = nq;

    for (map<string,Proc *>::const_iterator i=processors.begin();
	 i != processors.end(); i++)
      i->second->quiet = nq;

    for (map<string,Mem *>::const_iterator i=memories.begin();
	 i != memories.end(); i++)
      i->second->quiet = nq;
  }

  ~System() {

    for (map<string,Network *>::const_iterator i=networks.begin();
	 i != networks.end(); i++) delete i->second;

    for (map<string,Cache *>::const_iterator i=caches.begin();
	 i != caches.end(); i++) delete i->second;

    for (map<string,Proc *>::const_iterator i=processors.begin();
	 i != processors.end(); i++) delete i->second;

    for (map<string,Mem *>::const_iterator i=memories.begin();
	 i != memories.end(); i++) delete i->second;
  }

};


inline void Network::enqueue(msg_t &m, int interNo) {
  if (mySys->untimed) send(m);
  else {
    // cout << "Enqueing " << m << "\n";
    m.rtime = timeCount + (mySys->userandom ? 
			   (int)((double(rand())/RAND_MAX)*8) : 1);
    mySys->simmessages.push(m);
  }
}


