# networks
N toMem 1
N n1-21  0
N nP1 	0
# memories (must go before caches so we get server numbers right)
M Mem toMem
# caches (must go before processors so we get server numbers right!)
C L2.1	1 1 32 n1-21	toMem	NoneWT	WrEarly WrAlloc 
I L2.1  1 CentralDirectory	2 
C L1.1	1 1 32 nP1	n1-21	MSI	WrEarly
I L1.1  1 CentralDirectory	2 
I L1.1  0 CentralDirectory	2 

# processors
P P1 	nP1 

# Time to run

R P1 0 4	# get into caches
Q

O ======================================================================
O ====================== Test hits =====================================
O ======================================================================

R P1 0 4 noOwn noCoh noCache Alloc
Q
R P1 0 4 noOwn noCoh noCache noAlloc
Q
R P1 0 4 noOwn noCoh Cache Alloc
Q
R P1 0 4 noOwn noCoh Cache noAlloc
Q
R P1 0 4 noOwn Coh noCache Alloc
Q
R P1 0 4 noOwn Coh noCache noAlloc
Q
R P1 0 4 noOwn Coh Cache Alloc
Q
R P1 0 4 noOwn Coh Cache noAlloc
Q

R P1 0 4 Own noCoh noCache Alloc
Q
R P1 0 4 Own noCoh noCache noAlloc
Q
R P1 0 4 Own noCoh Cache Alloc
Q
R P1 0 4 Own noCoh Cache noAlloc
Q
R P1 0 4 Own Coh noCache Alloc
Q
R P1 0 4 Own Coh noCache noAlloc
Q
R P1 0 4 Own Coh Cache Alloc
Q
R P1 0 4 Own Coh Cache noAlloc
Q

O ======================================================================
O ====================== Test misses ===================================
O ======================================================================

R P1 20 4 noOwn noCoh noCache Alloc
Q
R P1 40 4 noOwn noCoh noCache noAlloc
Q
R P1 60 4 noOwn noCoh Cache Alloc
Q
R P1 80 4 noOwn noCoh Cache noAlloc
Q
R P1 a0 4 noOwn Coh noCache Alloc
Q
R P1 c0 4 noOwn Coh noCache noAlloc
Q
R P1 e0 4 noOwn Coh Cache Alloc
Q
R P1 100 4 noOwn Coh Cache noAlloc
Q

R P1 120 4 Own noCoh noCache Alloc
Q
R P1 140 4 Own noCoh noCache noAlloc
Q
R P1 160 4 Own noCoh Cache Alloc
Q
R P1 180 4 Own noCoh Cache noAlloc
Q
R P1 1a0 4 Own Coh noCache Alloc
Q
R P1 1c0 4 Own Coh noCache noAlloc
Q
R P1 1e0 4 Own Coh Cache Alloc
Q
R P1 200 4 Own Coh Cache noAlloc
Q

E
