/* 
 * Copyright (c) 2005-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Test the LSE cache support domain - cache model
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This is a cache model.  It does require that you have defined a whole
 * bunch of other types before you include it; the modularization here is
 * simply to make it easy to copy the file and modify it separately from
 * other files
 *
 */
#ifndef _CACHE_H_
#define _CACHE_H_
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <list> 
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <sstream>
#include <set>

using namespace std;

class Cache : public Unit {

  // for purpose of this test, we will use a direct-mapped 1-entry cache
private:

  struct ta {
    addr_t addr;
    cacheState_t state;
    int LRUbits;
  } *tagarray;

  typedef struct {
    LSE_cache_direntry_t e;
    uint32_t dbits[4];
  } direntry_t;

  direntry_t *uDir;
  direntry_t *lDir;
#ifdef DATA_WIDTH
  unsigned char *dataArray;
#endif

  int ways;
  int sets;
  int linesize;

  int lInvalReqCnt;

  transactionID_t numReadAccesses;
  transactionID_t numWriteAccesses;
  transactionID_t numReadHits;
  transactionID_t numWriteHits;

  class map<addr_t, int> pmTransients;

  int find_transient_second(addr_t addr) {
    map<addr_t, int>::const_iterator iter;
    if ((iter = pmTransients.find(addr)) != pmTransients.end()) 
	return  iter->second;
    else return 0;
  }

  void showActions(transaction_t *t) {
    if (!quiet) 
      cout << "[" << t->id << "]   " << idStr << " " << t->estate << "\n";
  }

  void doActions(transaction_t *t) {
    int ntd, tmp;
    LSE_cache_event_t ev;

    if (t->processing) return;
    t->processing = true; /* guard against re-entrance */

    showActions(t);
    ntd = t->estate.numToDo;

    for (int tdi = t->estate.todoPtr ; tdi < ntd; tdi++) {
      if (t->estate.todo[tdi].progress) continue;
      if (t->estate.todoPtr == tdi) t->estate.todoPtr++;
      t->estate.todo[tdi].progress = 1;

      switch (t->estate.todo[tdi].action) {

      case LSE_cache_actiontype_Send:
	{
	  msg_t m2 = t->m;
	  int dir;

	  m2.pmsg = t->estate.todo[tdi].work.Send.newMsg;
	  dir = t->estate.todo[tdi].work.Send.dir;
	  
	  switch (t->estate.todo[tdi].work.Send.replyIDtouse) {
	  case 0: m2.replyID = t->id; break;
	  default: m2.replyID = t->m.replyID; break;
	  }
	  
	  m2.destID = t->estate.todo[tdi].work.Send.destID;

	  if (t->estate.todo[tdi].work.Send.wholeBlock) {
	    m2.len = linesize;
	    m2.addr = m2.addr & ~((addr_t)(linesize-1));
	  }

#ifdef DATA_WIDTH
	  addr_t eaddr = m2.addr & ~((addr_t)(linesize-1));
	  int offset = m2.addr - eaddr;
	  if (doData) {
	    switch (m2.pmsg.type) {
	    case LSE_cache_msgtype_WrReq:
	      memcpy(&m2.data[0],&t->m.data[0],m2.len);
	      break;
	    case LSE_cache_msgtype_RdAck:
	      memcpy(&m2.data[0],&t->data[offset],m2.len);
	      break;
	    case LSE_cache_msgtype_InvalAck:
	    case LSE_cache_msgtype_CastOutReq:
	      if (m2.pmsg.flags & LSE_cache_msgflagbit_WithData)
		memcpy(&m2.data[0],&t->data[0],m2.len);
	      break;
	    default:
	      break;
	    }
	  }
#endif
	  
	  sendMsg(msgUnits(m2,dir),dir);
	  if (ntd != t->estate.numToDo) showActions(t);
	  ntd = t->estate.numToDo;
	}
	break;

      case LSE_cache_actiontype_ReadState:

	{
	  int i, state, setno, hitway;
	  addr_t eaddr = t->m.addr & ~((addr_t)(linesize-1));

	  if (t->estate.todo[tdi].work.ReadState.fields & 
	      LSE_cache_stateaccess_Tags) { 
	    /* asking to look at tags is asking to look up the cache
	     * line
	     */

	    hitway = -1;
	    state = Invalid;
	    setno = (eaddr / linesize) % sets;
	    for (i = 0; i < ways; i++) {
	      if (eaddr == tagarray[setno*ways+i].addr && 
		  tagarray[setno*ways+i].state != Invalid) {
		
		state = tagarray[setno*ways+i].state;
		
		// Update LRU bits
		int oldone = tagarray[setno*ways+i].LRUbits;
		for (int j = 0; j < ways; j++) 
		  if (tagarray[setno*ways+j].LRUbits > oldone)
		    tagarray[setno*ways+j].LRUbits--;
		tagarray[setno*ways+i].LRUbits = ways;
		
		if (t->m.pmsg.type == RdReq && 
		    (t->m.pmsg.flags & LSE_cache_msgflagbit_Cacheable))
		  numReadHits++;
		if (t->m.pmsg.type == WrReq &&
		    (t->m.pmsg.flags & LSE_cache_msgflagbit_Cacheable))
		  numWriteHits++;
		hitway = i;
		break;
	      }
	    } /* for i in ways */

	    state |= find_transient_second(eaddr);

	    t->estate.lineState = state;
	    if (hitway < 0 || (state & 0xff) == Invalid) {
	      t->way = addr_t(-1);
	      if (!quiet) 
		cout << "[" << t->id << "]   " << idStr 
		     << " read line state @none: 0" 
		     << oct << t->estate.lineState << dec << '\n';
	    
	    } else {
	      t->way = setno * ways + i;
	      if (!quiet) 
		cout << "[" << t->id << "]   " << idStr 
		     << " read line state @" << t->way << ": 0" 
		     << oct << t->estate.lineState << dec << '\n';
	    
	    }
	  } /* tag read; i.e. associative lookup */

	  if (t->estate.todo[tdi].work.ReadState.fields & 
	      LSE_cache_stateaccess_LineState) {

	    /* just look up the line state for the given way */
	    t->estate.lineState = tagarray[t->way].state;

	    /* pull in transients */
	    t->estate.lineState |= find_transient_second(eaddr);

	    if (!quiet) 
	      cout << "[" << t->id << "]   " << idStr 
		   << " read line state @" << t->way << ": 0" 
		   << oct << t->estate.lineState << dec << '\n';
	    
	  }

	  if ((t->estate.todo[tdi].work.ReadState.fields >> 
	       LSE_cache_stateaccess_offset_upper) & 1) {
	    /* look up the upper directory */
	    if (t->way != addr_t(-1)) {
	      t->estate.dState[0].flags = uDir[t->way].e.flags;
	      t->estate.dState[0].d.Central = &t->uBits[0];
	      memcpy(&t->uBits[0],uDir[t->way].e.d.Central,
		     sizeof(t->uBits));
	    }
	    else {
	      t->estate.dState[0].flags = 0;
	      t->estate.dState[0].d.Central = &t->uBits[0];
	      memset(&t->uBits[0],0,sizeof(t->uBits));
	    }
	    if (!quiet) {
	      cout << "[" << t->id << "]   " << idStr 
		   << " read upper directory: @";
	      if (t->way == addr_t(-1)) cout << "none";
	      else cout << t->way;
	      cout << ": 0x" << hex << t->estate.dState[0].flags 
		   << ' ' << setfill('0');
	      for (i=3;i>=0;i--) 
		cout << setw(8) << t->uBits[i];
	      cout << dec << setfill(' ') << '\n';
	    }
	  }

	  if ((t->estate.todo[tdi].work.ReadState.fields >> 
	       LSE_cache_stateaccess_offset_lower) & 1) {
	    /* look up the lower directory */
	    if (t->way != addr_t(-1)) {
	      t->estate.dState[2].flags = lDir[t->way].e.flags;
	      t->estate.dState[2].d.Central = &t->lBits[0];
	      memcpy(&t->lBits[0],lDir[t->way].e.d.Central,
		     sizeof(t->uBits));
	    }
	    else {
	      t->estate.dState[2].flags = 0;
	      t->estate.dState[2].d.Central = &t->lBits[0];
	      memset(&t->lBits[0],0,sizeof(t->uBits));
	    }
	    if (!quiet) {
	      cout << "[" << t->id << "]   " << idStr 
		   << " read lower directory: @";
	      if (t->way == addr_t(-1)) cout << "none";
	      else cout << t->way;
	      cout << ": 0x" << hex << t->estate.dState[2].flags
		   << ' ' << setfill('0');
	      for (i=3;i>=0;i--) 
		cout << setw(8) << t->lBits[i];
	      cout << dec << setfill(' ') << '\n';
	    }
	  }

	} /* block */

	ev.etype = LSE_cache_eventtype_StateRead;
	ev.ino = t->estate.todo[tdi].ino;
	handle_event(engine,&t->estate,&ev);
	showActions(t);
	ntd = t->estate.numToDo;
	break;

      case LSE_cache_actiontype_WriteState:

	// write tags
	if (t->estate.todo[tdi].work.WriteState.fields & 
	    LSE_cache_stateaccess_Tags) { 
	  addr_t eaddr = t->m.addr & ~((addr_t)(linesize-1));
	  tagarray[t->way].addr = eaddr;

	  /* need to remove transient; the value has been kept up to date
	   * after the victim choice was made...
	   */
	  pmTransients.erase(eaddr);
	}

	// write main line state
	if (t->estate.todo[tdi].work.WriteState.fields & 
	    LSE_cache_stateaccess_LineState) {
	  int nt;

	  if (t->way != addr_t(-1)) {
	    addr_t eaddr = t->m.addr & ~((addr_t)(linesize-1));
	    
	    nt = ((tagarray[t->way].state
		   & t->estate.todo[tdi].work.WriteState.clearLineState )
		  | t->estate.todo[tdi].work.WriteState.setLineState);
	    tagarray[t->way].state = nt;
	    
	    /* here, if the tag does not currently match the address, 
	     * there could also be a transient to be updated.  Basically,
	     * while waiting for tag writes, the transient state applies
	     * to both the old and the new line
	     *
	     * If there are no transient bits set, then delete the transient
	     * (will cover cases where the replacement does not occur)
	     *
	     */

	    if (tagarray[t->way].addr != eaddr) {
	      nt |= find_transient_second(eaddr);
	      if (nt & ~0xff) pmTransients[eaddr] = nt;
	      else pmTransients.erase(eaddr);
	    }
	    
	    if (!quiet) 
	      cout << "[" << t->id << "]   " << idStr 
		   << " write line state @" << t->way << ": 0" 
		   << oct << nt << dec << '\n';

	  } else {

	    addr_t eaddr = t->m.addr & ~((addr_t)(linesize-1));
	    int ns = find_transient_second(eaddr);
	    nt = ((ns & t->estate.todo[tdi].work.WriteState.clearLineState )
		  | t->estate.todo[tdi].work.WriteState.setLineState);
	    if (nt) pmTransients[eaddr] = nt;
	    else pmTransients.erase(eaddr);

	    if (!quiet) 
	      cout << "[" << t->id << "]   " << idStr 
		   << " write line state @none: 0" 
		   << oct << nt << dec << '\n';
	  }
	  
	}

	tmp = (t->estate.todo[tdi].work.WriteState.fields >> 
	       LSE_cache_stateaccess_offset_upper);

	if (tmp & LSE_cache_stateaccess_clearall) {
	  for (int i = 0; i < 4; i++) 
	    uDir[t->way].e.d.Central[i] = 0;
	}
	if (tmp & LSE_cache_stateaccess_clear) {
	  int bno = t->estate.todo[tdi].work.WriteState.dbits[0];
	  uDir[t->way].e.d.Central[bno / 32] &= ~(1 << (bno % 32));
	}
	if (tmp & LSE_cache_stateaccess_set) {
	  int bno = t->estate.todo[tdi].work.WriteState.dbits[0];
	  uDir[t->way].e.d.Central[bno / 32] |= (1 << (bno % 32));
	}
	if (tmp & LSE_cache_stateaccess_setflags) {
	  uDir[t->way].e.flags = t->estate.todo[tdi].work.WriteState.dflags[0];
	}
	if (!quiet && 
	    (tmp & (LSE_cache_stateaccess_clearall |
		    LSE_cache_stateaccess_clear |
		    LSE_cache_stateaccess_set |
		    LSE_cache_stateaccess_setflags))) {
	  cout << "[" << t->id << "]   " << idStr 
	       << " write upper directory @";
	  if (t->way == addr_t(-1)) cout << "none";
	  else cout << t->way;
	  cout << ": 0x" 
	       << hex << uDir[t->way].e.flags << ' ' << setfill('0');
	  for (int k=3;k>=0;k--) 
	    cout << setw(8) << uDir[t->way].e.d.Central[k];
	  cout << dec << setfill(' ') << '\n';
	}

	tmp = (t->estate.todo[tdi].work.WriteState.fields >> 
	       LSE_cache_stateaccess_offset_lower);

	if (tmp & LSE_cache_stateaccess_clearall) {
	  for (int i = 0; i < 4; i++) 
	    lDir[t->way].e.d.Central[i] = 0;
	}
	if (tmp & LSE_cache_stateaccess_clear) {
	  int bno = t->estate.todo[tdi].work.WriteState.dbits[2];
	  lDir[t->way].e.d.Central[bno / 32] &= ~(1 << (bno % 32));
	}
	if (tmp & LSE_cache_stateaccess_set) {
	  int bno = t->estate.todo[tdi].work.WriteState.dbits[2];
	  lDir[t->way].e.d.Central[bno / 32] |= (1 << (bno % 32));
	}
	if (tmp & LSE_cache_stateaccess_setflags) {
	  lDir[t->way].e.flags = t->estate.todo[tdi].work.WriteState.dflags[2];
	}
	if (!quiet && 
	    (tmp & (LSE_cache_stateaccess_clearall |
		    LSE_cache_stateaccess_clear |
		    LSE_cache_stateaccess_set |
		    LSE_cache_stateaccess_setflags))) {
	  cout << "[" << t->id << "]   " << idStr 
	       << " write lower directory @"; 
	  if (t->way == addr_t(-1)) cout << "none";
	  else cout << t->way;
	  cout << ": 0x" 
	       << hex << lDir[t->way].e.flags << ' ' << setfill('0');
	  for (int k=3;k>=0;k--) 
	    cout << setw(8) << lDir[t->way].e.d.Central[k];
	  cout << dec << setfill(' ') << '\n';
	}


	break;
	
      case LSE_cache_actiontype_CrossCheck:
	{
	  transaction_t *t2;
	  /* must copy map into a vector because we may end up deleting,
	   * which would change the map!
	   */
	  TransactionVec_t tv(remembered.size());
	  int cnt=0;
	  for (TransactionMap_t::const_iterator i = remembered.begin();
	       i != remembered.end();
	       i++) tv[cnt++] = i->second;

	  addr_t eaddr = t->m.addr & ~((addr_t)(linesize-1));
	  bool didsomething=false;
	  for (int i=0;i<cnt;i++) {

	    t2 = tv[i];

	    if (t2 != t && (eaddr == 
			    (t2->m.addr & ~((addr_t)(linesize-1))))) {
	      if (!quiet) 
		cout << "[" << t->id << "]   " << idStr << " crossing "
		     << msg_name(&t2->m.pmsg) << "[" << t2->id << "]\n";
	      handle_crosscheck(engine, &t->estate, &t2->estate);
	      //t->processing = true;
	      doActions(t2);
	      //t->processing = false;
	      didsomething = true;
	    } 
	  } /* for i */
	  if (didsomething) {
	    showActions(t);
	    ntd = t->estate.numToDo;	
	  }
	}
	break;

      case LSE_cache_actiontype_ChooseVictim:
	{ 
	  int i, least, leastway, setno;
          addr_t eaddr = t->m.addr & ~((addr_t)(linesize-1));

          ev.etype = LSE_cache_eventtype_VictimChosen;
          setno = (eaddr / linesize) % sets;
 	  
	  /* try for non-locked or invalid lines in LRU order */
	  least = ways+1;

	  for (i=0;i<ways;i++) {
	    if (tagarray[setno*ways+i].state == Invalid && least >= 0) {
	      leastway = i;
	      least = -1;
	      break;
	    } else if (!(tagarray[setno*ways+i].state & 
			 LSE_cache_lock_Victim) && 
		       tagarray[setno*ways+i].LRUbits < least) {
	      leastway = i;
	      least = tagarray[setno*ways+i].LRUbits;
	    }
	    tagarray[setno*ways+i].LRUbits--;
	  }
	  if (least < ways+1) { i = leastway; goto found_victim; }
	  else {
	    for (i=0;i<ways;i++) tagarray[setno*ways+i].LRUbits++;
	  }

	  /* all locked */

	  if (!quiet) 
	    cout << "[" << t->id << "]   " << idStr << " cannot find victim\n";

	  ev.d.VictimChosen.t = NULL;
	  ev.d.VictimChosen.gotVictim = false;
	  ev.d.VictimChosen.victimState = Invalid;
	  ev.ino = t->estate.todo[tdi].ino;
	  handle_event(engine,&t->estate,&ev);
	  showActions(t);
	  ntd = t->estate.numToDo;
	  break;

	found_victim:
	  i = setno*ways+i;
	  tagarray[i].LRUbits = ways;
	  if (!quiet) 
	    cout << "[" << t->id << "]   " << idStr << " chose victim @"
		 << i << ": 0x" 
		 << hex << tagarray[i].addr << oct
		 << " (0" << tagarray[i].state << dec << ")\n";
	  
	  ev.d.VictimChosen.victimState = tagarray[i].state;
	  t->way = i;
	  t->victimAddr = tagarray[i].addr;

	  /* set locks indicated */
	  tagarray[i].state |= t->estate.todo[tdi].work.ChooseVictim.locks;

	  /* make victim location and transients agree */
	  int ns = find_transient_second(eaddr);
	  if (ns) tagarray[i].state |= ns;
	  if (tagarray[i].state) pmTransients[eaddr] = tagarray[i].state;

	  ev.d.VictimChosen.gotVictim = true;

	  if (!(ev.d.VictimChosen.victimState & 7)) {
	    ev.d.VictimChosen.t = NULL;
	  } else {

	    transaction_t *t2 = new transaction_t(t->m,t->dir);

	    init_tstate(engine,&t2->estate, &t->m.pmsg, t->dir, 
			      t->estate.sendID, true);
	    t2->estate.origMsg.type = LSE_cache_msgtype_None;
	    t2->m.pmsg.type = LSE_cache_msgtype_None; /* indicates victim */
	    t2->estate.lineState = tagarray[i].state;
	    t2->estate.aliveCnt = 1;
	    t2->estate.iState[t->estate.todo[tdi].ino].stateNo 
	      = t->estate.todo[tdi].work.ChooseVictim.stateNo;
	    t2->estate.iState[t->estate.todo[tdi].ino].atRisk = true;

	    t2->m.addr = t2->m.firstAddr = t->victimAddr;
	    t2->m.len = linesize;

	    t2->lineStateRead = true;
	    t2->way = t->way;
	    t->otherTrans = t2;
	    t->estate.linkedTrans = &t2->estate;
	    t2->otherTrans = t;	  
	    t2->estate.linkedTrans = &t->estate;
	    remember(t2);
	    ev.d.VictimChosen.t = &t2->estate;
	  } 
	  ev.ino = t->estate.todo[tdi].ino;
	  handle_event(engine,&t->estate,&ev);
	  showActions(t);
	  ntd = t->estate.numToDo;
	  break;
	}
	break;

      case LSE_cache_actiontype_WriteData:
#ifdef DATA_WIDTH
	if (doData) {
	  addr_t eaddr = t->m.addr & ~((addr_t)(linesize-1));
	  int offset = t->m.addr - eaddr;
	  if (t->estate.todo[tdi].work.WriteData.source & 2) 
	    memcpy(&dataArray[t->way * linesize],
		   &t->data[0],
		   linesize);
	  if (t->estate.todo[tdi].work.WriteData.source & 1)
	    memcpy(&dataArray[t->way * linesize + offset],
		   &t->m.data[0],
		   t->m.len);
	}
#endif
	break;

      case LSE_cache_actiontype_ReadData:
#ifdef DATA_WIDTH
	if (doData) {
	  memcpy(&t->data[0],
		 &dataArray[t->way * linesize],
		 linesize);
	}
#endif
	ev.etype = LSE_cache_eventtype_DataRead;
	ev.ino = t->estate.todo[tdi].ino;
	handle_event(engine,&t->estate,&ev);
	showActions(t);
	ntd = t->estate.numToDo;
	break;

      case LSE_cache_actiontype_Notify:
	ev.etype = LSE_cache_eventtype_Notify;
	ev.d.Notify.msg = t->estate.todo[tdi].work.Notify.msg;
	ev.ino = t->estate.todo[tdi].ino;
	handle_event(engine,&t->otherTrans->estate,&ev);
	if (t->otherTrans) doActions(t->otherTrans);
	showActions(t);
	ntd = t->estate.numToDo;	
	break;

      case LSE_cache_actiontype_None:
	break;      

      case LSE_cache_actiontype_Error:
	cerr << "Error " << t->estate.todo[tdi].work.Error << " reported\n";
	exit(1);

      default:
	cerr << "Unknown action " << t->estate.todo[tdi].action << "\n";
	exit(1);
      } /* switch action type */

    } /* for i in actions */

    if (!t->estate.aliveCnt && t->estate.todoPtr == t->estate.numToDo) {
      //addr_t eaddr = t->m.addr & ~((addr_t)(linesize-1));

      forget(t);
      finish_tstate(engine,&t->estate);
      if (t->m.pmsg.type == InvalReq && !t->dir) 
	lInvalReqCnt--;
      if (t->otherTrans) t->otherTrans->otherTrans = NULL;

      if (!quiet) 
	cout << idStr << " finished [" << t->id << "]\n";
      delete t;
      return;
    }
    t->processing = false;
  } /* doActions */
  
 public:
  LSE_cache_engine_t *engine;

 private:
  void setup(const char *name, Network *u, Network *l,
	     LSE_cache_engine_parms_t *ep) {

    Unit::setup(name,u,l);

    tagarray = new struct ta[sets*ways];
    uDir = new direntry_t[sets*ways];
    lDir = new direntry_t[sets*ways];

    for (int i = 0; i < ways * sets; i++) {
      tagarray[i].state = Invalid;
      tagarray[i].addr = 0; // make valgrind happy
      tagarray[i].LRUbits = 0;
      uDir[i].e.flags = 0;
      uDir[i].e.d.Central = &uDir[i].dbits[0];
      uDir[i].dbits[0] = uDir[i].dbits[1] = uDir[i].dbits[2] 
	= uDir[i].dbits[3] = 0;
      lDir[i].e.flags = 0;
      lDir[i].e.d.Central = &lDir[i].dbits[0];
      lDir[i].dbits[0] = lDir[i].dbits[1] = lDir[i].dbits[2] 
	= lDir[i].dbits[3] = 0;
    }
    
    engine = create_engine(ep);

    numReadAccesses = numWriteAccesses = numReadHits = numWriteHits = 0;

#ifdef DATA_WIDTH
    dataArray = new unsigned char[linesize * ways * sets];
#endif
  }

 public:
  Cache(const char *name, int s, int w, int ls, Network *u, Network *l,
	LSE_cache_engine_parms_t *ep) 
    : ways(w), sets(s), linesize(ls), lInvalReqCnt(0)
  {
    setup(name,u,l,ep);
  }

  Cache(istringstream &ist, const string &name, 
	map<string,Network *>& networks,
	const string command) : lInvalReqCnt(0) {
    string netu, netl, proto;
    string word;
      
    ist >> sets >> ways >> linesize >> netu >> netl >> proto;

    LSE_cache_engine_parms_t ip;
    ip.P.ReadBuffered = false;
    ip.P.WriteEarly = false;
    ip.P.WriteAlloc = false;
    ip.P.Inclusive = true;

    ip.P.PuntOnRMW = false;
    if (proto == "WT") {
      ip.P.protocol = LSE_cache_protocol_WriteThrough;
    } else if (proto == "NoneWT") { 
      ip.P.protocol = LSE_cache_protocol_NoneWT;
    } else if (proto == "NoneWB") { 
      ip.P.protocol = LSE_cache_protocol_NoneWB;
    } else if (proto == "MSI") { 
      ip.P.protocol = LSE_cache_protocol_MSI;
    } else if (proto == "MESI") { 
      ip.P.protocol = LSE_cache_protocol_MESI;
    } else if (proto == "MOESI") { 
      ip.P.protocol = LSE_cache_protocol_MOESI;
    } else if (proto == "MOSI") { 
      ip.P.protocol = LSE_cache_protocol_MOSI;
    } else if (proto == "Illinois") { 
      ip.P.protocol = LSE_cache_protocol_Illinois;
    } else if (proto == "Synapse") { 
      ip.P.protocol = LSE_cache_protocol_Synapse;
    } else if (proto == "Firefly") { 
      ip.P.protocol = LSE_cache_protocol_Firefly;
    } else if (proto == "Dragon") { 
      ip.P.protocol = LSE_cache_protocol_Dragon;
    } else {
      cerr << "Unrecognized protocol " << proto << " in " << command << '\n';
      exit(2);
    }
    ip.P.VictimHandling = 4;
    
    while (!ist.eof()) {
      ist >> word;
      if (word == "" || word[0] == '#') break;
      else if (word == "noReadBuf") ip.P.ReadBuffered = false;
      else if (word == "ReadBuf") ip.P.ReadBuffered = true;
      else if (word == "noWrEarly") ip.P.WriteEarly = false;
      else if (word == "WrEarly") ip.P.WriteEarly = true;
      else if (word == "noWrAlloc") ip.P.WriteAlloc = false;
      else if (word == "WrAlloc") ip.P.WriteAlloc = true;
      else if (word == "noIncl") ip.P.Inclusive = false;
      else if (word == "Incl") ip.P.Inclusive = true;
      else if (word == "victimH") {
	ist >> word;
	ip.P.VictimHandling = atoi(word.c_str());
      } else {
	cerr << "Unrecognized flag " << word << " in " 
	     << command << '\n';
	exit(2);
      }
    }
    setup(name.c_str(),networks[netu],networks[netl],&ip);
  }

  ~Cache() { 
    if (pmTransients.size()) {
      cerr << "Left over transients: " << pmTransients.size() << '\n';
    }
    delete[] tagarray;
    delete[] uDir;
    delete[] lDir;
#ifdef DATA_WIDTH
    delete[] dataArray;
#endif
    destroy_engine(engine); 
  }

  void setupInterface(istringstream &ist, const string &command) {
    string style;
    bool isUpper;
    int numUnits;
    ist >> isUpper >> style >> numUnits;
      
    LSE_cache_engine_parms_t ip;
    memset(&ip,0,sizeof(ip)); /* clear out all flags, etc... */
      
    ip.I.numUnits = numUnits;
    if (style == "Snoop") {
      ip.I.style = LSE_cache_interfacestyle_Snoop;
      if (isUpper) {
      } else {
	while (!ist.eof()) {
	  string word;
	  ist >> word;
	  if (word == "" || word[0] == '#') break;
	  else if (word == "noCOWI") 
	    ip.Snoop.CombineOutWriteInval = false;
	  else if (word == "COWI") 
	    ip.Snoop.CombineOutWriteInval = true;
	  else if (word == "noSIWAI") 
	    ip.Snoop.SeeInWriteAsInval = false;
	  else if (word == "SIWAI") 
	    ip.Snoop.SeeInWriteAsInval = true;
	  else {
	    cerr << "Unrecognized flag " << word << " in " 
		 << command << '\n';
	    exit(2);
	  }
	}
      }
    } else if (style == "CentralDirectory") {
      ip.I.style = LSE_cache_interfacestyle_CentralDirectory;
      if (isUpper) {
	while (!ist.eof()) {
	  string word;
	  ist >> word;
	  if (word == "" || word[0] == '#') break;
	  else if (word == "noBCT") 
	    ip.CentralDir.BetweenCacheTransfers = false;
	  else if (word == "BCT") 
	    ip.CentralDir.BetweenCacheTransfers = true;
	  else {
	    cerr << "Unrecognized flag " << word << " in " 
		 << command << '\n';
	    exit(2);
	  }
	}
      }
    } else {
      cerr << "Unrecognized interface style " << style << " in " 
	   << command << '\n';
      exit(2);
    }
    
    attach_interface(engine,isUpper,&ip);
  }

  void handleMsg(const msg_t &m, int interNo) {
    transaction_t *t;
    bool snooped;
    int dir = 1-interNo;

    snooped = m.dest != this;

    if (!snooped && !quiet) 
      cout << idStr << " received " << m << " on " 
	   << (interNo ? "lower" : "upper") << " int.\n";

    if (msg_starts_transaction(engine, &m.pmsg, dir, snooped)) {

      if (snooped && !quiet) 
	cout << idStr << " snooped " << m << " on " 
	     << (interNo ? "lower" : "upper") << " int.\n";

      t = new transaction_t(m,dir);
      init_tstate(engine, &t->estate, &m.pmsg, dir, t->m.sendID, true);
      remember(t);
      if (dir == 0 && m.pmsg.type == InvalReq) {
	lInvalReqCnt++;
      }

      if (dir) {
	if (m.pmsg.type == RdReq && 
	    (m.pmsg.flags & LSE_cache_msgflagbit_Cacheable))
	  numReadAccesses++;
	if (m.pmsg.type == WrReq && 
	    (m.pmsg.flags & LSE_cache_msgflagbit_Cacheable))
	  numWriteAccesses++;
      }
    } else if (msg_is_reply(engine, &m.pmsg, dir, snooped)) {
      t = find(m.replyID);
    } else t = NULL;

    if (t) {

#ifdef DATA_WIDTH
      addr_t eaddr = m.addr & ~((addr_t)(linesize-1));
      int offset = m.addr - eaddr;
      int i;

      if (doData) {
	switch (m.pmsg.type) {
	case LSE_cache_msgtype_RdAck:
	  memcpy(&t->data[offset],&m.data[0],m.len);
	  break;
	case LSE_cache_msgtype_InvalAck:
	  if (m.pmsg.flags & LSE_cache_msgflagbit_WithData) {
	    memcpy(&t->data[offset],&m.data[0],m.len);
	  }
	  break;
	default:
	  break;
	}

	if (!snooped && !quiet &&
	    (m.pmsg.type == LSE_cache_msgtype_WrReq ||
	     m.pmsg.type == LSE_cache_msgtype_RdAck ||
	     (m.pmsg.type == LSE_cache_msgtype_InvalAck &&
	      (m.pmsg.flags & LSE_cache_msgflagbit_WithData)) ||
	     (m.pmsg.type == LSE_cache_msgtype_CastOutReq &&
	      (m.pmsg.flags & LSE_cache_msgflagbit_WithData))
	     )) {
	  cout << "\tdata: " << hex << setfill('0');
	  for (i=0;i<m.len;i++) 
	    cout << hex << setw(2) << (((unsigned int)m.data[i])&0xff);
	  cout << setfill(' ') << dec << '\n';
	}
      }
#endif

      LSE_cache_event_t ev;
      ev.etype = LSE_cache_eventtype_Msg;
      ev.d.Msg.m = &m.pmsg;
      ev.d.Msg.dir = dir;
      ev.d.Msg.snooped = snooped;
      ev.d.Msg.sendID = m.sendID;
      ev.d.Msg.destID = m.destID;
      handle_event(engine, &t->estate, &ev);
      doActions(t);
    }

  } /* handleMsg */

  void report(ostream &os) {
    os << idStr << ":"
       << "\tRead Miss Rate = " << (numReadAccesses - numReadHits) 
       << "/" << numReadAccesses << " (" 
       << (double(numReadAccesses - numReadHits))/numReadAccesses * 100
       << "%)";
    os << "\tWrite Miss Rate = " << (numWriteAccesses - numWriteHits) 
       << "/" << numWriteAccesses << " (" 
       << (double(numWriteAccesses - numWriteHits))/numWriteAccesses * 100
       << "%)\n";
  }

  void flushData(void (*writer)(addr_t,unsigned char *, int)) {
#ifdef DATA_WIDTH
    int i;
    unsigned char *dp = &dataArray[0];
    if (doData) {
      for (i=0;i<sets * ways;i++) {
	if (tagarray[i].state & 2) { /* is dirty, so flush it */
	  writer(tagarray[i].addr,dp,linesize);
	}
	dp += linesize; /* avoid the multiply */
      }
    }
#endif
  } /* flushData */

  void renewData(void (*reader)(addr_t, unsigned char *, int)) {
#ifdef DATA_WIDTH
    int i;
    unsigned char *dp = &dataArray[0];
    if (doData) {
      for (i=0;i<sets * ways;i++) {
	if (tagarray[i].state) { /* is dirty, so flush it */
	  reader(tagarray[i].addr,dp,linesize);
	}
	dp += linesize; /* avoid the multiply */
      }
    }
#endif
  } /* renewData */

};
#endif /* _CACHE_H_ */
