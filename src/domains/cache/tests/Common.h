/* 
 * Copyright (c) 2005-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Test the LSE cache support domain - common stuff
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This file provides common type definitions and includes
 *
 */

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <list> 
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <sstream>

using namespace std;
using namespace LSE_cache_default;

#define RdReq LSE_cache_msgtype_RdReq
#define RdAck LSE_cache_msgtype_RdAck
#define WrReq LSE_cache_msgtype_WrReq
#define WrAck LSE_cache_msgtype_WrAck
#define RdWrReq LSE_cache_msgtype_RdWrReq
#define RdWrAck LSE_cache_msgtype_RdWrAck
#define InvalReq LSE_cache_msgtype_InvalReq
#define InvalAck LSE_cache_msgtype_InvalAck
#define CastOutReq LSE_cache_msgtype_CastOutReq
#define CastOutAck LSE_cache_msgtype_CastOutAck
#define msgtype_t LSE_cache_msgtype_t

#define Invalid       LSE_cache_linestate_Invalid
#define SharedClean   LSE_cache_linestate_SharedClean
#define SharedDirty   LSE_cache_linestate_SharedDirty
#define PrivateClean   LSE_cache_linestate_PrivateClean
#define PrivateDirty   LSE_cache_linestate_PrivateDirty

typedef class msg_t {
public:
  LSE_cache_msg_t pmsg;
  simtime_t rtime;

  // simulator routing nonsense
  int interNo;         // sent to interface (needed only for delays..)
  class Unit *sender;
  class Unit *dest;    // intended destination

  // real modeling
  unitID_t sendID; // local id of sender
  unitID_t destID; // local id of destination (0 for down unit, 
                   // -1 for broadcast)

  transactionID_t replyID; // id of the message to reply to

  addr_t addr, len, firstAddr; // location to access

#ifdef DATA_WIDTH
  char data[DATA_WIDTH]; // maximum width of data
#endif

  union {
    struct {
      class Unit *replyTo; // unit you should reply to
      unitID_t replyToID; // local id of unit to reply to
      int count; // count for acknowledgments, etc...
    } copyback;
  } md;

  bool operator<(const msg_t& x) const { return rtime > x.rtime; }

} msg_t;

typedef class Transaction {
public:
  msg_t m;              // original message
  int dir;              // direction
  transactionID_t id;   // id of transaction

  addr_t victimAddr;        // victim addr
  bool lineStateRead;       // line state has been read
  bool processing;          // currently in processing

  addr_t index;   // where in the cache we found it
  addr_t way;
#ifdef DATA_WIDTH
  char data[DATA_WIDTH]; // maximum width of data
#endif

  LSE_cache_direntry_t lDir, uDir; // directory entries
  uint32_t lBits[4]; // for holding directory entries
  uint32_t uBits[4];

  LSE_cache_tstate_t estate;

  class Transaction *otherTrans;

  Transaction(const msg_t &om, int d) 
    : m(om), dir(d), lineStateRead(false), processing(false), 
    way(addr_t(-1)), otherTrans(NULL)
  {}

} transaction_t;
typedef class map<int, class Transaction *> TransactionMap_t;
typedef class vector<class Transaction *> TransactionVec_t;

extern simtime_t timeCount;
#ifdef DATA_WIDTH
extern bool doData;
#endif

ostream& operator<<( ostream& os, const msg_t &m);
ostream& operator<<( ostream& os, const LSE_cache_tstate_t &t);

//////////////////////////////////////////////////////////////////////

/* for now, only two interfaces supported; need to have only one upper
 * attached...
 */

class Network { 
public:
  typedef struct {
    int interNo; // 1 = lower, 0 = upper interface
    class Unit *unit;
  } unitInfo;
private:
  bool attachedUpper;
  class vector<unitInfo> units;
  const char *name;
  class System *mySys;
public:
  bool snooping;
  bool quiet;

  Network(class System *ms, const char *n, bool snoop) 
    : attachedUpper(false), name(n), mySys(ms), snooping(snoop),
    quiet(true) {

    /* create a dummy first record for the "server" */
    unitInfo dummy;
    dummy.interNo = -1;
    dummy.unit = NULL;
    units.push_back(dummy);
  }

  unitID_t attachUnit(Unit *u, int ino) {
    unitInfo ui;

    ui.interNo = ino;
    ui.unit = u;
    //units.push_back(ui);

    if (!ino) {
      if (attachedUpper) {
	cerr << "Attempted to attach two upper interfaces to network " << name
	     << '\n';
	exit(1);
      }
      else {
	attachedUpper = true;
	units[0] = ui;
	return 0;
      }
    } else {
      units.push_back(ui);
      return units.size() - 1;
    }
  }

  int numConnected(void) { return units.size(); }

  Unit *findUnit(unitID_t ino) {
    if (ino < 0) return NULL; 
    return units[ino].unit; 
  }

  void send(msg_t &m);
  void enqueue(msg_t &m, int interNo);

};

class Unit {
public:
  const char *idStr;
  class Network *interfaces[3]; 
  int interfaceIDs[3];

  class Network *upper, *lower; // upper and lower interfaces

  int transactionID;
  TransactionMap_t remembered;
  bool quiet;

protected:
  void setup(const char *name, Network *up, Network *low) {
    idStr = name;
    upper = up;
    lower = low;
    if (low) {
      interfaces[1] = low;
      interfaceIDs[1] = low->attachUnit(this,1);
    }
    if (up) {
      interfaces[0] = up;
      interfaceIDs[0] = up->attachUnit(this,0);
    }
  }

public:
  Unit(void) : transactionID(0), quiet(true) {}
  Unit(const char *name, Network *up, Network *low) : transactionID(0),
    quiet(true)
  {
    setup(name,up,low);
    remembered.end()->second = NULL;
  }

  virtual ~Unit() {
    if (remembered.size()) {
      cout << idStr << " has leftover transactions:\n";
      for (TransactionMap_t::const_iterator i = remembered.begin();
	   i != remembered.end();
	   i++) cout << "\t[" << i->second->id << "]\n";
      for (TransactionMap_t::const_iterator i = remembered.begin();
	   i != remembered.end();
	   i++) delete i->second;
    }
  }

  virtual void handleMsg(const msg_t &m, int interNo) {}

  msg_t &msgUnits(msg_t &m, int interNo) {
    m.interNo = interNo;
    m.dest = interfaces[interNo]->findUnit(m.destID);
    m.sendID = interfaceIDs[interNo];
    m.sender = this;
    return m;
  }

  void sendMsg(msg_t &m, int interNo) {
    interfaces[interNo]->enqueue(m, interNo);
    /*
    if (untimed) interfaces[interNo]->send(m);
    else SystemSupp::enqueue(mySys,m,interNo);
    */
  }

  void remember(transaction_t *t) {
    remembered[t->id = transactionID++] = t;
    //cerr << idStr << " " << remembered.size() << " remembering "
    // << t->id << "\n";
  }
  void forget(transaction_t *t) { 
    remembered.erase(t->id); 
    //cerr << idStr << " " << remembered.size() << " forgot " 
    // << t->id << "\n";
  }
  transaction_t *find(int tid) { return remembered.find(tid)->second; }

};

inline void Network::send(msg_t &m) {
  unitInfo ui;
  msg_t m2 = m; // need copy because m could get deallocated during a handleMsg

  if (m.destID < 0) { /* broadcast */
    int size = units.size();
    for (int i=0;i<size;i++) {
      if (i == m2.sendID) continue;
      ui = units[i];
      m2.destID = i;
      m2.dest = ui.unit;
      ui.unit->handleMsg(m2,ui.interNo);
    }
  } else {
    ui = units[m.destID];

    ui.unit->handleMsg(m,ui.interNo);

    if (snooping) {
      int size = units.size();
      for (int i=0;i<size;i++) {
	if (i == m2.destID || i == m2.sendID) continue;
	ui = units[i];

	ui.unit->handleMsg(m2,ui.interNo);
      }
    }
  }
};

//////////////////////////////////////////////////////////////////////////////

inline ostream&
operator <<( ostream& os, const LSE_cache_tstate_t &t)
{
  bool needscomma;
  int tmp;

  os << ::LSE_cache_default::msg_name(&t.origMsg) << " :";

  os << ' ' << t.iState[0].stateNo << ' ' 
     << t.iState[1].stateNo << ' ' << t.iState[2].stateNo << " :";

  for (int i = t.todoPtr ; i < t.numToDo; i++) {
    if (t.todo[i].progress) continue;

    if (t.todo[i].dep[0] >= 0) {
      if (t.todo[i].dep[1] >= 0) 
	os << " {" << t.todo[i].dep[0] << "," << t.todo[i].dep[1] << "}";
      else os << " {" << t.todo[i].dep[0] << "}";
    } else if (t.todo[i].dep[1] >= 0) {
      os << " {" << t.todo[i].dep[1] << "}";
    } else os << " {}";

    switch (t.todo[i].action) {
    case LSE_cache_actiontype_Send:
      os << "Send:" << msg_name(&t.todo[i].work.Send.newMsg);
      break;
    case LSE_cache_actiontype_ReadState:
      os << "ReadState(f=" << hex << t.todo[i].work.ReadState.fields
	 << dec << ')';
      break;
    case LSE_cache_actiontype_WriteState:
      os << "WriteState(";
      needscomma = false;

      tmp = (t.todo[i].work.WriteState.fields);

      if (tmp & LSE_cache_stateaccess_Tags) {
	os << "Tag";
	needscomma = true;
      }

      if (tmp & LSE_cache_stateaccess_LineState) {
	if (needscomma) os << ',';
	os << "Line((&" << oct 
	   << t.todo[i].work.WriteState.clearLineState
	   << ")|" << t.todo[i].work.WriteState.setLineState << ")" << dec;
	needscomma = true;
      }

      tmp = (t.todo[i].work.WriteState.fields >> 
	     LSE_cache_stateaccess_offset_upper);

      if (tmp & LSE_cache_stateaccess_clearall) {
	if (needscomma) os << ',';
	os << "UpperClearAll";
	needscomma = true;
      }
      if (tmp & LSE_cache_stateaccess_clear) {
	if (needscomma) os << ',';
	os << "UpperClear(" << t.todo[i].work.WriteState.dbits[0] << ')';
	needscomma = true;
      }
      if (tmp & LSE_cache_stateaccess_set) {
	if (needscomma) os << ',';
	os << "UpperSet(" << t.todo[i].work.WriteState.dbits[0] << ')';
	needscomma = true;
      }
      if (tmp & LSE_cache_stateaccess_setflags) {
	if (needscomma) os << ',';
	os << "UpperFlags(" << hex << t.todo[i].work.WriteState.dflags[0] 
	   << ')' << dec;
	needscomma = true;
      }

      tmp = (t.todo[i].work.WriteState.fields >> 
	     LSE_cache_stateaccess_offset_lower);

      if (tmp & LSE_cache_stateaccess_clearall) {
	if (needscomma) os << ',';
	os << "LowerClearAll";
	needscomma = true;
      }
      if (tmp & LSE_cache_stateaccess_clear) {
	if (needscomma) os << ',';
	os << "LowerClear(" << t.todo[i].work.WriteState.dbits[0] << ')';
	needscomma = true;
      }
      if (tmp & LSE_cache_stateaccess_set) {
	if (needscomma) os << ',';
	os << "LowerSet(" << t.todo[i].work.WriteState.dbits[0] << ')';
	needscomma = true;
      }
      if (tmp & LSE_cache_stateaccess_setflags) {
	if (needscomma) os << ',';
	os << "LowerFlags(" << hex << t.todo[i].work.WriteState.dflags[0] 
	   << ')' << dec;
	needscomma = true;
      }
      
      os << ')';
      break;
    case LSE_cache_actiontype_ChooseVictim:
      os << "ChooseVictim";
      break;
    case LSE_cache_actiontype_CrossCheck:
      os << "CrossCheck";
      break;
    case LSE_cache_actiontype_WriteData:
      os << "WriteData-" << t.todo[i].work.WriteData.source;
      break;
    case LSE_cache_actiontype_ReadData:
      os << "ReadData";
      break;
    case LSE_cache_actiontype_Notify:
      os << "Notify(" 
	 << t.todo[i].ino << "," 
	 << t.todo[i].work.Notify.dest << "," 
	 << t.todo[i].work.Notify.msg << ")";
      break;
    case LSE_cache_actiontype_Error:
      os << "Error(" << t.todo[i].work.Error << ")";
      break;
    case LSE_cache_actiontype_None:
      os << "None";
      break;
    default:
      os << "Unknown";
      break;
    }
    os << '{' << i << '/' << t.todo[i].ino << '}';
  } /* for i */
  os << ' ' << t.aliveCnt;
  return os;
}

inline
ostream&
operator <<( ostream& os, const msg_t &m)
{
  os << msg_name(&m.pmsg) << "[" << m.replyID << "]";
  if (m.dest) {
    os << " for " << m.dest->idStr << "(" << m.destID << ")";
  } else {
    os << " for any ";
  }
  os << " from " << m.sender->idStr << "(" << m.sendID << ")";
  os << " with addr " << hex << m.addr << "/" << m.firstAddr 
     << ", len " << dec << m.len;
  return os;
}

