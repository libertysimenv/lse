/* 
 * Copyright (c) 2005 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Test the LSE cache support domain - memory unit
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This is a memory unit
 *
 */

class Mem : public Unit {
  uint64_t numReadAccesses;
  uint64_t numWriteAccesses;
  uint64_t numWB;
#ifdef DATA_WIDTH
  unsigned char dataBuf[DATA_WIDTH];
#endif

public:
#ifdef DATA_WIDTH
  void (*doRead)(addr_t, unsigned char *, int);
  void (*doWrite)(addr_t, unsigned char *, int);
#endif

  Mem(const char *name, Network *upper) : Unit(name,upper,NULL),
    numReadAccesses(0), numWriteAccesses(0), numWB(0) {}

  // handle a message received on the upper interface (which is the only one)
  void handleMsg(const msg_t &m, int interNo) {

    if (!quiet) cout << idStr << " received " << m << "\n";

    msg_t m2 = m;
    m2.replyID = m.replyID;
    m2.destID = m.sendID;
    m2.pmsg.reply = LSE_cache_msgreply_OK;
    m2.pmsg.flags = 0;

    switch (m.pmsg.type) {
    case RdReq:
      m2.pmsg.type = RdAck;
#ifdef DATA_WIDTH
      if (doData) {
	doRead(m.addr,(unsigned char *)&m2.data[0],m.len);
      }
#endif
      numReadAccesses++;
      break;
    case WrReq:
      m2.pmsg.type = WrAck;
#ifdef DATA_WIDTH
      if (doData) {
	if (!quiet) {
	  int i;
	  cout << "\tdata: " << hex << setfill('0');
	  for (i=0;i<m.len;i++) 
	    cout << hex << setw(2) << (((unsigned int)m.data[i])&0xff);
	  cout << setfill(' ') << dec << '\n';
	}
	doWrite(m.addr,(unsigned char *)&m.data[0],m.len);
      }
#endif
      numWriteAccesses++;
      break;
    case InvalReq: /* presumably snooped */
    case InvalAck:
    case CastOutAck:
      return;
    case CastOutReq:
      m2.pmsg.type = CastOutAck;
      if (m.pmsg.flags & LSE_cache_msgflagbit_WithData) {
	numWB++;
#ifdef DATA_WIDTH
	if (doData) {
	  if (!quiet) {
	    int i;
	    cout << "\tdata: " << hex << setfill('0');
	    for (i=0;i<m.len;i++) 
	      cout << hex << setw(2) << (((unsigned int)m.data[i])&0xff);
	    cout << setfill(' ') << dec << '\n';
	  }
	  doWrite(m.addr,(unsigned char *)&m.data[0],m.len);
	}
#endif
      }
      break;
    default:
      cerr << idStr << " unable to handle message " << m << "\n";
      cerr << "\n";
      exit(1);
    }
    sendMsg(msgUnits(m2,0),0);
  }

  void report(ostream &os) {
    os << idStr << ":"
       << "\tReads = " << numReadAccesses
       << "\tWrites = " << numWriteAccesses
       << "\tWritebacks = " << numWB
       << '\n';
  }
};
