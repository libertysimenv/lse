/* 
 * Copyright (c) 2005-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Test the LSE cache support domain
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * This program mimics a simulator using the cache support domain libraries.
 * This has been done here so as to be independent of the simulator itself.
 * Also, one could conceive of cache-only simulators being built by using
 * the emulator and cache support libraries directly.
 *
 * At any rate, this one allows two timing abstractions: a "no time" to 
 * complete anything abstraction, and a single cycle delay to send a 
 * message abstraction.
 *
 * The basic structure is that we create a bunch of processors, caches,
 * and memory interconnected by networks.  Networks handle things like
 * snooping for us.  The caches create protocol engines and talk to them; 
 * they are extremely simplistic in design.
 *
 */

/* Prevent CYGWIN from defining an addr_t */
#ifdef __CYGWIN__
#define __addr_t_defined
#endif

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <list> 
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

// Use the base cache implementation
#include <LSE_cache.h>
#include <LSE_cache_default.h>
using namespace LSE_cache_default;

typedef unsigned long addr_t;
typedef int unitID_t;
typedef int transactionID_t;
typedef int cacheState_t;
typedef uint64_t simtime_t;

#include "System.h"

simtime_t timeCount= 0;

static void usage(const char *s) {
  if (s) cerr << s << "\n";
  cerr << "try_cache [options]\n";
  cerr << "    --help            this message\n";
  cerr << "    --[no]quiet       turn messages off (on by default)\n";
  cerr << "    --[no]timed       turn timing on (off by default)\n";
  cerr << "    --[no]random <#>  use random timing (with a particular seed)\n";
  exit(s ? 1 : 0);
}

int main(int argc, char *argv[]) {
  bool untimed=true;
  bool userandom=false;
  bool quiet=false;
  int rseed = 0;

  cout.sync_with_stdio();
  cerr.sync_with_stdio();

  for (int i=1;i<argc;i++) {
    if (!strcmp(argv[i],"--help")) usage(NULL);
    if (!strcmp(argv[i],"--timed")) untimed=false;
    else if (!strcmp(argv[i],"--notimed")) untimed=true;
    else if (!strcmp(argv[i],"--quiet")) quiet=true;
    else if (!strcmp(argv[i],"--noquiet")) quiet=false;
    else if (!strcmp(argv[i],"--random")) {
      if (i+1 >= argc) usage("Missing random seed");
      userandom=true;
      rseed = atoi(argv[i+1]);
      i++;
    }
    else if (!strcmp(argv[i],"--norandom")) userandom=false;
    else usage("Unrecognized argument");
  }

  if (userandom) untimed=false;
  srand(rseed);

  System s(cin, untimed, userandom, quiet);
  s.report(cout);

  return 0;
}
