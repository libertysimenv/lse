/* Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Include file for common portions of cache interfaces
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Definitions of cache domain stuff.
 *
 */

// NOTE: do *not* keep this from multiple includes, as it will be
// included within a namespace.

extern LSE_cache_engine_t *create_engine(LSE_cache_engine_parms_t *parms);
extern void destroy_engine(LSE_cache_engine_t *e);
extern LSE_cache_error_t attach_interface(LSE_cache_engine_t *e, bool isUpper, LSE_cache_engine_parms_t *p);
extern void destroy_interface(LSE_cache_interface_t *f);
extern void init_tstate(LSE_cache_engine_t *e, LSE_cache_tstate_t *t, const LSE_cache_msg_t *m, int dir, int sendID, bool newToDo);
extern void finish_tstate(LSE_cache_engine_t *e, LSE_cache_tstate_t *t);
extern bool msg_starts_transaction(LSE_cache_engine_t *e, const LSE_cache_msg_t *m, int dir, bool snooped);
extern bool msg_is_reply(LSE_cache_engine_t *e, const LSE_cache_msg_t *m, int dir, bool snooped);
extern const char *msg_name(const LSE_cache_msg_t *m);
extern void handle_event(LSE_cache_engine_t *e, LSE_cache_tstate_t *t, const LSE_cache_event_t *ev);
extern void handle_crosscheck(LSE_cache_engine_t *e, LSE_cache_tstate_t *t, LSE_cache_tstate_t *told);
