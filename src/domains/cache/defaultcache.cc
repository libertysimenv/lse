/* 
 * Copyright (c) 2005-2010 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Implementation of the "default" cache functions
 *
 * Authors: David A. Penry <dpenry@cs.princeton.edu>
 *
 * Implements the default cache functions, including the default cache
 * protocol engine routines.  This will be a blast to get right...
 *
 * TODO: 
 * - deal with serialization
 * - improved performance parameters
 *
 * TODO eventually:
 * - write update protocols
 * - distributed directories
 * - RMW
 * - options: Protocol: ReadBuffered
 *
 * Interesting invariants in the code:
 * - before calling a handler of another interface, must set the state 
 *   number properly so handler re-entrance can take place as needed.
 * - same thing applies before sending a message which will could be
 *   intercepted internally
 * 
 * NOTES:
 * 
 *
 */

#include "LSE_cache.h"
#include "LSE_cache_default.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace LSE_cache_default {

#define upperInterface 0
#define mainEngine     1
#define lowerInterface 2

#define DEFAULT_SPACE 32

#define RdReq        LSE_cache_msgtype_RdReq
#define RdAck        LSE_cache_msgtype_RdAck
#define WrReq        LSE_cache_msgtype_WrReq
#define WrAck        LSE_cache_msgtype_WrAck
#define RdWrReq      LSE_cache_msgtype_RdWrReq
#define RdWrAck      LSE_cache_msgtype_RdWrAck
#define InvalReq     LSE_cache_msgtype_InvalReq
#define InvalAck     LSE_cache_msgtype_InvalAck
#define CastOutReq   LSE_cache_msgtype_CastOutReq
#define CastOutAck   LSE_cache_msgtype_CastOutAck

#define Invalid         LSE_cache_linestate_Invalid
#define ExclusiveOwner  LSE_cache_linestate_ExclusiveOwner
#define SharedOwner     LSE_cache_linestate_SharedOwner
#define DirtyOwner      LSE_cache_linestate_DirtyOwner
#define Shared          LSE_cache_linestate_Shared

#define OK           LSE_cache_msgreply_OK
#define NACK         LSE_cache_msgreply_NACK
#define Retry        LSE_cache_msgreply_Retry

#define lock_Victim    LSE_cache_lock_Victim
#define lock_DRd       LSE_cache_lock_DRd
#define lock_DWr       LSE_cache_lock_DWr
#define lock_URd       LSE_cache_lock_URd
#define lock_UWr       LSE_cache_lock_UWr
#define lock_DCoh      LSE_cache_lock_DCoh
#define lock_UCoh      LSE_cache_lock_UCoh
#define lock_All       LSE_cache_lock_All

#define Cacheable    LSE_cache_msgflagbit_Cacheable
#define Coherent     LSE_cache_msgflagbit_Coherent
#define NoAlloc      LSE_cache_msgflagbit_NoAlloc
#define WriteThrough LSE_cache_msgflagbit_WriteThrough
#define Conditional  LSE_cache_msgflagbit_Conditional
#define WithData     LSE_cache_msgflagbit_WithData
#define ToOwn        LSE_cache_msgflagbit_ToOwn
#define Prefetch     LSE_cache_msgflagbit_Prefetch
#define Shareable    LSE_cache_msgflagbit_Shareable
#define NoCache      LSE_cache_msgflagbit_NoCache

#define isCacheable(x)    ((x).flags & LSE_cache_msgflagbit_Cacheable)
#define isCoherent(x)     ((x).flags & LSE_cache_msgflagbit_Coherent)
#define isNoAlloc(x)      ((x).flags & LSE_cache_msgflagbit_NoAlloc)
#define isWriteThrough(x) ((x).flags & LSE_cache_msgflagbit_WriteThrough)
#define isConditional(x)  ((x).flags & LSE_cache_msgflagbit_Conditional)
#define isWithData(x)     ((x).flags & LSE_cache_msgflagbit_WithData)
#define isToOwn(x)        ((x).flags & LSE_cache_msgflagbit_ToOwn)
#define isPrefetch(x)     ((x).flags & LSE_cache_msgflagbit_Prefetch)
#define isShareable(x)    ((x).flags & LSE_cache_msgflagbit_Shareable)
#define isNoCache(x)      ((x).flags & LSE_cache_msgflagbit_NoCache)

  /********************** standard stuff *********************************/

  static void setup_protocol(LSE_cache_engine_t *e);
  static void setup_lower_interface(LSE_cache_engine_t *e);
  static void setup_upper_interface(LSE_cache_engine_t *e);

  static LSE_cache_event_t nullEv;

  /********
   * Create a protocol engine
   *******/
  LSE_cache_engine_t *
  create_engine(LSE_cache_engine_parms_t *parms) {
    LSE_cache_engine_t *e;
    e = (LSE_cache_engine_t *)malloc(sizeof(LSE_cache_engine_t));
    if (!e) {
      fprintf(stderr,"Out of memory in create_engine\n");
      abort();
    }
    e->interfaces[mainEngine].parms = *parms;
    setup_protocol(e);
    e->interfaces[upperInterface].parms.I.style = LSE_cache_interfacestyle_Null;
    setup_upper_interface(e);
    e->interfaces[lowerInterface].parms.I.style = LSE_cache_interfacestyle_Null;
    setup_lower_interface(e);

    /* make dummies happy */
    nullEv.etype = LSE_cache_eventtype_None;

    return e;
  }

  /********
   * Create an interface
   *******/
  LSE_cache_error_t
  attach_interface(LSE_cache_engine_t *e,
		   bool isUpper,
		   LSE_cache_engine_parms_t *p) {

    if (!e) return LSE_cache_error_BadParameter;

    if (isUpper) {
      e->interfaces[upperInterface].parms = *p;
      setup_upper_interface(e);
    } else {
      e->interfaces[lowerInterface].parms = *p;
      setup_lower_interface(e);
    }
    return LSE_cache_error_None;
  }

  /********
   * Destroy a protocol engine
   *******/
  void destroy_engine(LSE_cache_engine_t *e) {
    if (e) {
      free(e);
    }
  }

  /********
   * Turn message names to strings
   ********/
  const char *msg_name(const LSE_cache_msg_t *m) {
    /* bit 0: NotCacheable, bit 1 = NoAlloc, bit 2 = !Coherent, 
       bit 3 = ToOwn */
    const static char *rdreq[] = { 
      "RdReq", "RdReq.NCa", "RdReq.NA", "RdReq.NA.NCa",
      "RdReq.NCo", "RdReq.NCo.NCa", "RdReq.NCo.NA", "RdReq.NCo.NCa",
      "RdReq.ToOwn", "RdReq.ToOwn.NCa", "RdReq.ToOwn.NA", "RdReq.ToOwn.NA.NCa",
      "RdReq.ToOwn.NCo", "RdReq.ToOwn.NCo.NCa", "RdReq.ToOwn.NCo.NA", 
      "RdReq.ToOwn.NCo.NCa",
    };

    /* bit 0 = NotCacheable, bit 1 = NoAlloc, bit 2 = WriteThrough, 
       bit 3 = NotCoherent, bit 4 = Conditional */
    const static char *wrreq[] = {
      "WrReq", "WrReq.NCa", "WrReq.NA", "WrReq.NA.NCa",
      "WrReq.WT", "WrReq.WT.NCa", "WrReq.WT.NA", "WrReq.NA.WT.NCa",
      "WrReq.NCo", "WrReq.NCo.NCa", "WrReq.NCo.NA", "WrReq.NCo.NA.NCa",
      "WrReq.NCo.WT", "WrReq.NCo.WT.NCa", "WrReq.NCo.WT.NA", 
      "WrReq.NCo.NA.WT.NCa",
      "WrReq.Cnd", "WrReq.Cnd.NCa", "WrReq.Cnd.NA", "WrReq.Cnd.NA.NCa",
      "WrReq.Cnd.WT", "WrReq.Cnd.WT.NCa", "WrReq.Cnd.WT.NA", 
      "WrReq.Cnd.NA.WT.NCa",
      "WrReq.Cnd.NCo", "WrReq.Cnd.NCo.NCa", "WrReq.Cnd.NCo.NA", 
      "WrReq.Cnd.NCo.NA.NCa",
      "WrReq.Cnd.NCo.WT", "WrReq.Cnd.NCo.WT.NCa", "WrReq.Cnd.NCo.WT.NA", 
      "WrReq.Cnd.NCo.NA.WT.NCa",
    };

    /* bits 0,1 = reply.  bit 2 = NoCache */
    const static char *rdack[] = {
      "RdAck.OK", "RdAck.NACK", "RdAck.Retry", "RdAck.Retry",
      "RdAck.OK.NC", "RdAck.NACK.NC", "RdAck.Retry.NC", "RdAck.Retry.NC",
    };
    /* bits 0,1 = reply.  bit 2 = NoCache */
    const static char *wrack[] = {
      "WrAck.OK", "WrAck.NACK", "WrAck.Retry", "WrAck.Retry",
      "WrAck.OK.NC", "WrAck.NACK.NC", "WrAck.Retry.NC", "WrAck.Retry.NC",
    };
    /* bits 0,1 = reply.  bit 2 = NoCache */
    const static char *rdwrack[] = {
      "RdWrAck.OK", "RdWrAck.NACK", "RdWrAck.Retry", "RdWrAck.Retry",
      "RdWrAck.OK.NC", "RdWrAck.NACK.NC", "RdWrAck.Retry.NC", "RdWrAck.Retry.NC",
    };
    /* bits 0,1 = reply */
    const static char *invalack[] = {
      "InvalAck.OK", "InvalAck.NACK", "InvalAck.Retry", "InvalAck.OK",
      "InvalAck.WD.OK", "InvalAck.WD.NACK", 
      "InvalAck.WD.Retry", "InvalAck.WD.OK",
    };
    /* bits 0,1 = reply */
    const static char *castoutack[] = {
      "CastOutAck.OK", "CastOutAck.NACK", "CastOutAck.Retry", "CastOutAck.OK",
    };

    switch (m->type) {
    case LSE_cache_msgtype_None: return "None";
    case RdReq : 
      return rdreq[(isToOwn(*m) ? 8 : 0) + (isCoherent(*m) ? 0 : 4) +
		   (isNoAlloc(*m) ? 2 : 0) + (isCacheable(*m) ? 0 : 1)];
    case RdAck : 
      return rdack[((int)m->reply) + (isNoCache(*m) ? 4 : 0)];

    case WrReq : 
      return wrreq[(isConditional(*m) ? 16 : 0) + 
		   (isCoherent(*m) ? 0 : 8) + (isWriteThrough(*m) ? 4 : 0) +
		   (isNoAlloc(*m) ? 2 : 0) + (isCacheable(*m) ? 0 : 1)];
    case WrAck : 
      return wrack[((int)m->reply) + (isNoCache(*m) ? 4 : 0)];

    case RdWrReq : return "RdWrReq";
    case RdWrAck : return rdwrack[((int)m->reply) + (isNoCache(*m) ? 4 : 0)];

    case InvalReq : return isShareable(*m) ? "InvalReq.Sh" : "InvalReq";
    case InvalAck : return invalack[((int)m->reply) + (isWithData(*m) ? 4 : 0)];

    case CastOutReq: return isWithData(*m) ? "CastOutReq.WD" : "CastOutReq";
    case CastOutAck: return castoutack[((int)m->reply)];

    default: return "Unknown";
    }
  }

  /* Could this message be a reply to a previous transaction? */
  bool msg_is_reply(LSE_cache_engine_t *e,
		       const LSE_cache_msg_t *m,
		       int dir, bool snooped) {
    return ((!dir && (m->type == RdAck || m->type == WrAck || 
		      m->type == RdWrAck || m->type == InvalAck ||
		      m->type == CastOutAck) && !snooped) ||
	    (dir && m->type == InvalAck && !snooped));
  }

  /*******************************************************************
   * Action trampolines
   *******************************************************************/

#define OwnedBit        0x80000

  /* locks allowing us to do things, so anything which might prevent should
   * not be allowed to go forward
   */

#define StateMask       0x00ff

#define OwnerBit 0x1
#define ModifiedBit 0x2
#define SharedBit 0x4
#define get_state(s) ((s) & 0xff)

  static inline void 
  ensure_todo_space(LSE_cache_tstate_t *t, int size) {
    if (t->todoSize > size + t->numToDo) return; /* already big enough */
    do {
      t->todoSize *= 2;
    } while (t->todoSize < size + t->numToDo);
    t->todo = static_cast<LSE_cache_action_t *>
      (realloc(t->todo, t->todoSize * sizeof(LSE_cache_action_t)));
  }

  static inline void
  demand_generic(LSE_cache_tstate_t *t, int ino, int d1, int d2) {
    t->todo[t->numToDo].progress = 0;
#ifdef CHECK_DEPS
    if (d1 == t->numToDo) printf("Hey d1!\n");
    if (d2 == t->numToDo) printf("Hey d2!\n");
#endif
    t->todo[t->numToDo].dep[0] = d1;
    t->todo[t->numToDo].dep[1] = d2;
    t->todo[t->numToDo].ino = ino;
    t->numToDo++;
  }

  static inline void 
  demand_finish(LSE_cache_tstate_t *t, int ino) {
    t->aliveCnt--;
    t->iState[ino].stateNo = -1;
  }

  static inline void 
  demand_may_finish(LSE_cache_tstate_t *t, int ino) {
    t->aliveCnt--;
  }

  static inline void
  demand_none(LSE_cache_tstate_t *t, int ino, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_None;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_readlinestate(LSE_cache_tstate_t *t, int ino, int kind, 
		       int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_ReadState;
    t->todo[t->numToDo].work.ReadState.fields = kind;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_notify(LSE_cache_tstate_t *t, int ino, int dest, int msg, 
		int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_Notify;
    t->todo[t->numToDo].work.Notify.dest = dest;
    t->todo[t->numToDo].work.Notify.msg = msg;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_writelinestate(LSE_cache_tstate_t *t, int ino, int newState,
			int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields = LSE_cache_stateaccess_LineState;
    t->todo[t->numToDo].work.WriteState.setLineState = newState & 0xff;
    t->todo[t->numToDo].work.WriteState.clearLineState = ~0xff;
    t->lineState = (t->lineState & ~0xff) | (newState & 0xff);
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_writetag(LSE_cache_tstate_t *t, int ino, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields = LSE_cache_stateaccess_Tags;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_lock(LSE_cache_tstate_t *t, int ino, int amt, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields = LSE_cache_stateaccess_LineState;
    amt &= (LSE_cache_lockbits_All << ino);
    t->lineState = t->lineState | (amt & ~0xff);
    t->todo[t->numToDo].work.WriteState.setLineState = amt & ~0xff;
    t->todo[t->numToDo].work.WriteState.clearLineState = ~0;
    demand_generic(t,ino, d1, d2);
  }

  static inline void
  demand_unlock(LSE_cache_tstate_t *t, int ino, int amt, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields = LSE_cache_stateaccess_LineState;
    amt &= (LSE_cache_lockbits_All << ino);
    t->lineState = t->lineState & ~(amt & ~0xff);
    t->todo[t->numToDo].work.WriteState.setLineState = 0;
    t->todo[t->numToDo].work.WriteState.clearLineState = ~amt | 0xff;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_alllock(LSE_cache_tstate_t *t, int ino, int amt, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields = LSE_cache_stateaccess_LineState;
    t->lineState = t->lineState | (amt & ~0xff);
    t->todo[t->numToDo].work.WriteState.setLineState = amt & ~0xff;
    t->todo[t->numToDo].work.WriteState.clearLineState = ~0;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_allunlock(LSE_cache_tstate_t *t, int ino, int amt, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields = LSE_cache_stateaccess_LineState;
    t->lineState = t->lineState & ~(amt & ~0xff);
    t->todo[t->numToDo].work.WriteState.setLineState = 0;
    t->todo[t->numToDo].work.WriteState.clearLineState = ~amt | 0xff;
    demand_generic(t,ino, d1, d2);
  }

  static inline void
  demand_readdirectory(LSE_cache_tstate_t *t, int ino, bool locKnown,
		       int d1, int d2) { 
    t->todo[t->numToDo].action = LSE_cache_actiontype_ReadState;
    t->todo[t->numToDo].work.ReadState.fields 
      = (0x100 << (8*ino)) | (locKnown ? 0 : LSE_cache_stateaccess_Tags);
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_dirwriteflags(LSE_cache_tstate_t *t, int ino, int val, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields 
      = (LSE_cache_stateaccess_setflags << 
	 (ino ? LSE_cache_stateaccess_offset_lower : 
	  LSE_cache_stateaccess_offset_upper));
    t->todo[t->numToDo].work.WriteState.dflags[ino] = val;
    demand_generic(t,ino, d1, d2);
  }

  static inline void
  demand_dirclearall(LSE_cache_tstate_t *t, int ino, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields 
      = (LSE_cache_stateaccess_clearall << 
	 (ino ? LSE_cache_stateaccess_offset_lower : 
	  LSE_cache_stateaccess_offset_upper));
    demand_generic(t,ino, d1, d2);
  }

  static inline void
  demand_dirclearall_flags(LSE_cache_tstate_t *t, int ino, int nf, int d1, 
			   int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields 
      = ((LSE_cache_stateaccess_clearall | LSE_cache_stateaccess_setflags) << 
	 (ino ? LSE_cache_stateaccess_offset_lower : 
	  LSE_cache_stateaccess_offset_upper));
    t->todo[t->numToDo].work.WriteState.dflags[ino] = nf;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_dirsetbit(LSE_cache_tstate_t *t, int ino, int bno, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields 
      = (LSE_cache_stateaccess_set << 
	 (ino ? LSE_cache_stateaccess_offset_lower :
	  LSE_cache_stateaccess_offset_upper));
    t->todo[t->numToDo].work.WriteState.dbits[ino] = bno;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_dirsetbit_flags(LSE_cache_tstate_t *t, int ino, int bno, int nf,
			 int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields 
      = ((LSE_cache_stateaccess_set | LSE_cache_stateaccess_setflags) << 
	 (ino ? LSE_cache_stateaccess_offset_lower :
	  LSE_cache_stateaccess_offset_upper));
    t->todo[t->numToDo].work.WriteState.dbits[ino] = bno;
    t->todo[t->numToDo].work.WriteState.dflags[ino] = nf;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_dirclearbit(LSE_cache_tstate_t *t, int ino, int bno, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields 
      = (LSE_cache_stateaccess_clear << 
	 (ino ? LSE_cache_stateaccess_offset_lower :
	  LSE_cache_stateaccess_offset_upper));
    t->todo[t->numToDo].work.WriteState.dbits[ino] = bno;
    demand_generic(t,ino,d1,d2);
  }


  static inline void
  demand_dirsetonlybit(LSE_cache_tstate_t *t, int ino, int bno, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields 
      = ((LSE_cache_stateaccess_set | LSE_cache_stateaccess_clearall) << 
	 (ino ? LSE_cache_stateaccess_offset_lower :
	  LSE_cache_stateaccess_offset_upper));
    t->todo[t->numToDo].work.WriteState.dbits[ino] = bno;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_dirsetonlybit_flags(LSE_cache_tstate_t *t, int ino, int bno, int nf,
			     int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields 
      = ((LSE_cache_stateaccess_set | LSE_cache_stateaccess_clearall |
	  LSE_cache_stateaccess_setflags) << 
	 (ino ? LSE_cache_stateaccess_offset_lower :
	  LSE_cache_stateaccess_offset_upper));
    t->todo[t->numToDo].work.WriteState.dbits[ino] = bno;
    t->todo[t->numToDo].work.WriteState.dflags[ino] = nf;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_send(LSE_cache_engine_t *e, int ino,
	      LSE_cache_tstate_t *t, const LSE_cache_msg_t *m,
	      int dir, int replyID, int destID, bool wb,
	      int d1, int d2) {

    t->todo[t->numToDo].action = LSE_cache_actiontype_Send;
    t->todo[t->numToDo].work.Send.newMsg = *m;
    t->todo[t->numToDo].work.Send.dir = dir;
    t->todo[t->numToDo].work.Send.replyIDtouse = replyID;
    t->todo[t->numToDo].work.Send.destID = destID;
    t->todo[t->numToDo].work.Send.wholeBlock = wb;
    demand_generic(t,ino,d1,d2);
    if (dir)
      while (ino < lowerInterface && 
	     !e->interfaces[ino+1].interceptMsg(e, t)) ino++;
    else 
      while (ino > 0 &&
	     !e->interfaces[ino-1].interceptMsg(e, t)) ino--;
  }

#define demand_send_uReply(e,i,t,m,d1,d2)		\
  demand_send(e,i,t,m,0,1,t->sendID,false,d1,d2)
#define demand_send_ServReq(e,i,t,m,d1,d2)	\
  demand_send(e,i,t,m,1,0,0,false,d1,d2)
#define demand_send_lReqAll(e,i,t,m,d1,d2)	\
  demand_send(e,i,t,m,1,0,-1,false,d1,d2)
#define demand_send_ServReqWB(e,i,t,m,d1,d2)	\
  demand_send(e,i,t,m,1,0,0,true,d1,d2)
#define demand_send_uReqAll(e,i,t,m,d1,d2)	\
  demand_send(e,i,t,m,0,0,-1,false,d1,d2)
#define demand_send_uReqAllWB(e,i,t,m,d1,d2)	\
  demand_send(e,i,t,m,0,0,-1,true,d1,d2)
#define demand_send_lReply(e,i,t,m,d1,d2)		\
  demand_send(e,i,t,m,1,1,t->sendID,false,d1,d2)

  static inline void
  demand_serialized(LSE_cache_tstate_t *t, int ino, int d1, int d2) {
#if 0
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteState;
    t->todo[t->numToDo].work.WriteState.fields = LSE_cache_stateaccess_LineState;
    t->todo[t->numToDo].work.WriteState.lineState 
      = t->lineState & (0xff|LockedBit);
    demand_generic(t,ino, d1, d2);
#endif
  }

  static inline void
  demand_readdata(LSE_cache_tstate_t *t, int ino, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_ReadData;
    demand_generic(t,ino, d1, d2);
  }

  static inline void
  demand_writedata(LSE_cache_tstate_t *t, int ino, int source,
		   int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_WriteData;
    t->todo[t->numToDo].work.WriteData.source = source;
    demand_generic(t,ino, d1, d2);
  }

  static inline void
  demand_choosevictim(LSE_cache_tstate_t *t, int ino, int locks, int stateNo,
		      int d1, int d2) {
    locks &= (LSE_cache_lockbits_All << ino);
    t->todo[t->numToDo].action = LSE_cache_actiontype_ChooseVictim;
    t->todo[t->numToDo].work.ChooseVictim.locks = locks;
    t->todo[t->numToDo].work.ChooseVictim.stateNo = stateNo;
    demand_generic(t,ino,d1,d2);
  }

  static inline void
  demand_crosscheck(LSE_cache_tstate_t *t, int ino, int d1, int d2) {
    t->todo[t->numToDo].action = LSE_cache_actiontype_CrossCheck;
    demand_generic(t,ino,d1,d2);
  }

  static void /* do not want to force inlining here */
  demand_error(LSE_cache_engine_t *e,
	       LSE_cache_tstate_t *t, int ino, LSE_cache_error_t err) {
    fprintf(stderr,"error %d in interface %d state %d\n",
	    err,ino,t->iState[ino].stateNo);
    ensure_todo_space(t,DEFAULT_SPACE);
    t->todo[t->numToDo].action = LSE_cache_actiontype_Error;
    t->todo[t->numToDo].work.Error = err;
    demand_generic(t,ino,-1,-1);
  }

  /************************************************************************
   * Waiting macros
   ************************************************************************/

  /* The cache protocols are handled in a rather odd
   *  fashion... basically, I attempt to form continuations when
   *  waiting for a state machine.  This allows me to specify the
   *  protocols in a very natural, programmatic.  Unfortunately, it's not
   *  the 100% most efficient solution.  But in this case, I believe that
   *  clarity of expression is going to be far more important than
   *  speed....  
   */

#define WAIT_StateRead(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n :						\
    if (ev->etype != LSE_cache_eventtype_StateRead)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
  }

#define WAIT_DataRead(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n :						\
    if (ev->etype != LSE_cache_eventtype_DataRead)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
  }

#define WAIT_VictimChosen(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n :						\
    if (ev->etype != LSE_cache_eventtype_VictimChosen)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
  }

#define WAIT_lRdAck(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n:							\
    if (ev->etype != LSE_cache_eventtype_Msg)			\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
    if (ev->d.Msg.dir || ev->d.Msg.m->type != RdAck)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentMsg);	\
  }

#define WAIT_lWrAck(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n:							\
    if (ev->etype != LSE_cache_eventtype_Msg)			\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
    if (ev->d.Msg.dir || ev->d.Msg.m->type != WrAck)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentMsg);	\
  }

#define WAIT_lWrAck_lCastOutAck(n) {				\
    ist->stateNo = n;						\
    return;							\
    state ## n:							\
    if (ev->etype != LSE_cache_eventtype_Msg)			\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
    if (ev->d.Msg.dir || (ev->d.Msg.m->type != WrAck &&		\
			  ev->d.Msg.m->type != CastOutAck))	\
      do_error(e,t,ino,LSE_cache_error_InconsistentMsg);	\
  }

#define WAIT_lCastOutAck(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n:							\
    if (ev->etype != LSE_cache_eventtype_Msg)			\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
    if (ev->d.Msg.dir || ev->d.Msg.m->type != CastOutAck)	\
      do_error(e,t,ino,LSE_cache_error_InconsistentMsg);	\
  }

#define WAIT_uInvalAck(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n:							\
    if (ev->etype != LSE_cache_eventtype_Msg)			\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
    if (!ev->d.Msg.dir || ev->d.Msg.m->type != InvalAck)	\
      do_error(e,t,ino,LSE_cache_error_InconsistentMsg);	\
  }

#define WAIT_uInvalAck_lCastOutAck(n) {					\
    ist->stateNo = n;							\
    return;								\
    state ## n:								\
    if (ev->etype != LSE_cache_eventtype_Msg)				\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);		\
    if (!( (ev->d.Msg.dir && ev->d.Msg.m->type == InvalAck) ||		\
	   (!ev->d.Msg.dir && ev->d.Msg.m->type == CastOutAck)))	\
      do_error(e,t,ino,LSE_cache_error_InconsistentMsg);		\
  }


#define WAIT_lInvalAck(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n:							\
    if (ev->etype != LSE_cache_eventtype_Msg)			\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
    if (ev->d.Msg.dir || ev->d.Msg.m->type != InvalAck)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentMsg);	\
  }

#define WAIT_lInvalAck_lWrAck(n) {					\
    ist->stateNo = n;							\
    return;								\
    state ## n:								\
    if (ev->etype != LSE_cache_eventtype_Msg)				\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);		\
    if (ev->d.Msg.dir ||						\
	(ev->d.Msg.m->type != InvalAck && ev->d.Msg.m->type != WrAck))	\
      do_error(e,t,ino,LSE_cache_error_InconsistentMsg);		\
  }

#define WAIT_Notify(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n :						\
    if (ev->etype != LSE_cache_eventtype_Notify)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
  }

#define WAIT_LinkedStart(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n :						\
    if (ev->etype != LSE_cache_eventtype_LinkedStart)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
  }

#define WAIT_Crossed(n) {					\
    ist->stateNo = n;						\
    return;							\
    state ## n :						\
    if (ev->etype != LSE_cache_eventtype_Crossed)		\
      do_error(e,t,ino,LSE_cache_error_InconsistentEvent);	\
  }

#define WAIT(n) {				\
    ist->stateNo = n;				\
    return;					\
    state ## n : ;				\
  }

  /************************************************************************
   * helper macros and functions
   ************************************************************************/

#define is_RdReq(t) ((t)->origMsg.type == RdReq)
#define is_WrReq(t) ((t)->origMsg.type == WrReq)
#define is_RdWrReq(t) ((t)->origMsg.type == RdWrReq)
#define is_InvalReq(t) ((t)->origMsg.type == InvalReq)
#define is_CastOutReq(t) ((t)->origMsg.type == CastOutReq)

  static inline bool 
  upper_requiresRetry(LSE_cache_engine_t *e,
		      LSE_cache_tstate_t *t) {

    return ( ( is_RdReq(t) && (t->lineState & lock_DRd)) ||
	     ( is_WrReq(t) && (t->lineState & lock_DWr)) ||
	     /*( is_CastOutReq(t) && t->origMsg.WithData &&
	       (t->lineState & lock_DWr)) ||*/
	     ( is_RdWrReq(t) && (t->lineState & (lock_DRd | lock_DWr))) ||
	     ( is_InvalReq(t) && (t->lineState & lock_DCoh)) );

#if 0
    return ( (t->lineState & PendingBit) &&
	     (e->interfaces[mainEngine].parms.P.SinglePending || 
	      (t->origMsg.type == RdWrReq ||
	       /* cannot service a read-to-own while still waiting for line */
	       (is_RdReq(t) && t->origMsg.ToOwn) ||
	       /* 
		  (is_RdReq(t) && !good_overlap(msg,validbits) && !PendingClean) ||
	       */
	       (is_RdReq(t) && !e->interfaces[mainEngine].parms.P.ReadBuffered) ||
	       /* cannot service read if writes not done to read by! */
	       (is_RdReq(t) && !e->interfaces[mainEngine].parms.P.WriteEarly)) ));
#endif
  }

  static inline bool 
  lower_requiresRetry(LSE_cache_engine_t *e,
		      LSE_cache_tstate_t *t) {

    return ( ( is_InvalReq(t) && (t->lineState & lock_UCoh)));
  }

  static inline bool
  is_Readable(int s) {
    return (get_state(s) != Invalid);
  }

  static inline bool
  is_Present(int s) {
    return (get_state(s) != Invalid);
  }

  static inline bool
  is_Writeable(int s) { /* can write if we own it and we do not share it */
    return (s & OwnerBit) && !(s & SharedBit);
  }

  static inline bool
  is_Dirty(int s) {
    return (s & ModifiedBit);
  }

  static inline bool
  is_Upgradeable(int s) {
    return (s & SharedBit);
  }

  static inline bool
  do_RTO(LSE_cache_engine_t *e, LSE_cache_tstate_t *t) {
    return false;
    /* make work with WB protocols with write allocate */
    /*return e->pinfo.hasOwner && 
      (is_WrReq(t) || is_CastOutReq(t)
      || is_RdReq(t));*/
  }

  static inline LSE_cache_msg_t *
  form_reply(LSE_cache_tstate_t *t,
	     LSE_cache_msg_t *m,
	     LSE_cache_msgreply_t reply) {
    *m = t->origMsg;
    /* relies upon ack value being 1 more than original value */
    m->type = static_cast<LSE_cache_msgtype_t>(m->type+1);
    m->reply = reply;
    m->flags = 0;
    return m;
  }

  static inline LSE_cache_msg_t *
  form_nc_reply(LSE_cache_tstate_t *t,
		LSE_cache_msg_t *m,
		LSE_cache_msgreply_t reply) {
    *m = t->origMsg;
    /* relies upon ack value being 1 more than original value */
    m->type = static_cast<LSE_cache_msgtype_t>(m->type+1);
    m->reply = reply;
    m->flags = NoCache;
    return m;
  }

  static inline LSE_cache_msgreply_t flag2reply(int flags) {
    return static_cast<LSE_cache_msgreply_t>(flags);
  }

#define do_error(n,t,i,e) { demand_error(n,t,i,e); return; }

  static inline int 
  nextCstate(LSE_cache_engine_t *e,
	     LSE_cache_tstate_t *t, LSE_cache_msg_t *m, int dir)
  {
    int i, sz = e->pinfo.protoTableSize;
    LSE_cache_protoTableEntry_t *tab = e->pinfo.protoTable;
    LSE_cache_msgtype_t msgtype = m->type;
    int mflags = m->flags;

    for (i=0 ; i < sz ; i++) {
      if (tab[i].state == get_state(t->lineState) &&
	  tab[i].mtype == msgtype &&
	  (tab[i].flagsMask & mflags) == tab[i].flagsVal &&
	  tab[i].dir == dir) return tab[i].newState;
    } 
    return t->lineState;
  }

#define eParms(e) e->interfaces[mainEngine].parms

  /************************************************************************
   * Engine protocols
   ************************************************************************/
  /* 
   * In general, only state transitions are listed here so as to 
   * make the engine run faster.
   *
   * To add:
   * MESI, MOSI, MOESI, ILLINOIS, SYNAPSE, BERKELEY,
   * FIREFLY, DRAGON
   *
   * Also will need to put in RMW support
   *
   * Format of flags matches that of messages
   * bit 8 = Shareable
   */

  /* the none protocols are for caches which have no one else on
   * the other side....
   */
  static LSE_cache_protoTableEntry_t noneWB[] = {
    {Invalid,        RdReq,      1, 0, 0, ExclusiveOwner},
    {Invalid,        WrReq,      1, 0, 0, DirtyOwner}, /* if write alloc */
    {ExclusiveOwner, WrReq,      1, 0, 0, DirtyOwner},
    {ExclusiveOwner, CastOutReq, 1, 0, 0, DirtyOwner},
    {ExclusiveOwner, InvalReq,   0, 0, 0, Invalid}, 
    {DirtyOwner,     InvalReq,   0, 0, 0, Invalid}, 
  };

  static LSE_cache_protoTableEntry_t noneWT[] = {
    {Invalid,        RdReq,      1, 0, 0, ExclusiveOwner},
    {Invalid,        WrReq,      1, 0, 0, ExclusiveOwner}, /* if write alloc */
    {ExclusiveOwner, InvalReq,   0, 0, 0, Invalid}, /* full invalidation */
  };

  /* writeThrough protocols have two states: Invalid and Shared */
  static LSE_cache_protoTableEntry_t writeThrough[] = {
    {Invalid, RdReq,      1, 0,   0, Shared},
    {Invalid, WrReq,      1, 0,   0, Shared}, /* if write alloc */
    {Shared,  InvalReq,   0, Shareable, 0, Invalid}, /* full invalidation */
  };

  /* MSI protocol:
   * M = Modified, Owner, ~Shared
   * S = ~Modified, Shared, ~Owner
   * I = ~Modified, ~Owner, ~Shared
   */
  static LSE_cache_protoTableEntry_t MSI[] = {
    {Invalid,    RdReq,      1, 0,   0,   Shared},
    {Shared,     RdReq,      1, 0,   0,   Shared},
    {DirtyOwner, RdReq,      1, 0,   0,   DirtyOwner},
    {Invalid,    WrReq,      1, 0,   0,   DirtyOwner}, /* if write alloc */
    {Shared,     WrReq,      1, 0,   0,   DirtyOwner},
    {DirtyOwner, WrReq,      1, 0,   0,   DirtyOwner},
    {Shared,     CastOutReq, 1, 0,   0,   DirtyOwner},
    {DirtyOwner, CastOutReq, 1, 0,   0,   DirtyOwner},
    {Shared,     InvalReq,   0, Shareable, 0,   Invalid}, 
    {DirtyOwner, InvalReq,   0, Shareable, 0,   Invalid},
    {DirtyOwner, InvalReq,   0, Shareable, Shareable, Shared}, 
  };

  /* 
   * Should this message enter the core?  Yes... all should go into it!
   */
  static bool 
  engine_intercept_msg(LSE_cache_engine_t *e, LSE_cache_tstate_t *t) {
    LSE_cache_event_t ev;
    LSE_cache_msg_t m;
    t->numToDo--;
    m = t->todo[t->numToDo].work.Send.newMsg;

    ev.etype = LSE_cache_eventtype_Msg;
    ev.d.Msg.m = &m;
    ev.d.Msg.dir = t->todo[t->numToDo].work.Send.dir;
    ev.d.Msg.snooped = false;
    e->interfaces[mainEngine].handler(e,t,&ev);
    return true; /* stop extraneous messages cold... */
  }

  /* Should this message start a transaction? We look for:
   *   Upper interface: requests directed at us
   *   Lower interface: requests directed at us, as well as 
   *                    snooped transactions.
   */
  static bool 
  engine_msg_starts_transaction(LSE_cache_engine_t *e,
				const LSE_cache_msg_t *m,
				int dir, bool snooped) {
    return ((dir && !snooped &&
	     (m->type == RdReq || m->type == WrReq || m->type == RdWrReq ||
	      m->type == CastOutReq)) ||
	    (!dir && m->type == InvalReq));
  }

  static void
  engine_handle_event(LSE_cache_engine_t *e, LSE_cache_tstate_t *t,
		      const LSE_cache_event_t *ev) {

    LSE_cache_istate_t *ist = &t->iState[mainEngine];
    int ino = mainEngine; /* needed for wait macros */
    LSE_cache_msg_t nm;
    int newstate, nflags;
    LSE_cache_msgtype_t mtype;
    
    switch (ist->stateNo) {
    case -1: return;
    case 0: goto state0;
    case 1: goto state1;
    case 2: goto state2;
    case 3: goto state3;
    case 4: goto state4;
    case 5: goto state5;
    case 6: goto state6;
    case 7: goto state7;
    case 8: goto state8;
    case 9: goto state9;
    case 10: goto state10;
    case 11: goto state11;
    case 12: goto state12;
    case 13: goto state13;
    case 14: goto state14;
    case 15: goto state15;
    case 16: goto state16;
    case 17: goto state17;
    case 18: goto state18;
    case 19: goto state19;
    case 20: goto state20;
    case 21: goto state21;
    case 22: goto state22;

    case 99: goto state99;
    default:
      if (ist->stateNo < 0) return;
      do_error(e,t,mainEngine,LSE_cache_error_UnknownState);
      break;
    }

    { /* indention for handling downstream transactions */
  
      /********************* Initial state *********************
       * We should only see transactions which we accepted back
       * in msg_starts_transaction (this includes things
       * we are snooping).  Furthermore, the cache line state
       * should already have been read by the interface and any
       * retries needed for pending states should have been
       * taken care of.
       *********************************************************/
      {
      state0:
	if (ev->etype != LSE_cache_eventtype_None) 
	  do_error(e,t,mainEngine,LSE_cache_error_InconsistentEvent);
	if (!t->dir) goto upward_bound;
      }
      t->aliveCnt++;
  
      /* read request come first because they are most frequent kind */
      if (is_RdReq(t)) {

	if (!isCacheable(t->origMsg) || 
	    (!is_Present(t->lineState) && isNoAlloc(t->origMsg))) {

	  /* read requests which bypass or refuse to allocate in the cache 
	   *
	   * Covers:
	   * uRdReq.!Own.!Coh.!Cach.!NA:
	   * uRdReq.!Own.!Coh.!Cach. NA:
	   * uRdReq.!Own. Coh.!Cach.!NA:
	   * uRdReq.!Own. Coh.!Cach. NA:
	   * uRdReq. Own.!Coh.!Cach.!NA:
	   * uRdReq. Own.!Coh.!Cach. NA:
	   * uRdReq. Own. Coh.!Cach.!NA:
	   * uRdReq. Own. Coh.!Cach. NA:
	   *
	   * Covers: (when not present)
	   * uRdReq. Own. Coh. Cach. NA:
	   * uRdReq.!Own.!Coh. Cach. NA:
	   * uRdReq.!Own. Coh. Cach. NA:
	   * uRdReq. Own.!Coh. Cach. NA:
	   *
	   */
	
	  t->wasNotCached = true;
	  ist->stateNo = 6; /* guard against dangerous re-entrance */
	  do {
	    ensure_todo_space(t, DEFAULT_SPACE);
	    demand_send_ServReq(e, mainEngine, t, &t->origMsg, -1, -1);
	    WAIT_lRdAck(6);
	  } while (ev->d.Msg.m->reply == Retry);
	
	  demand_finish(t, mainEngine);
	  demand_send_uReply(e, mainEngine, t, ev->d.Msg.m, -1, -1);
	  return;

	} else if (is_Writeable(t->lineState) ||
		   (is_Present(t->lineState) && 
		    !isToOwn(t->origMsg))) {

	  /* read requests which hit and return immediately 
	   * 
	   * Covers: (when writeable)
	   * uRdReq. Own. Coh. Cach.!NA:
	   * uRdReq. Own. Coh. Cach. NA:
	   * uRdReq. Own.!Coh. Cach.!NA:
	   * uRdReq. Own.!Coh. Cach. NA:
	   *
	   * Covers: (when present)
	   * uRdReq.!Own.!Coh. Cach.!NA:
	   * uRdReq.!Own.!Coh. Cach. NA:
	   * uRdReq.!Own. Coh. Cach.!NA:
	   * uRdReq.!Own. Coh. Cach. NA:
	   *
	   */

	  if (isWithData(t->origMsg)) {
	    demand_readdata(t,mainEngine, -1, -1);
	    WAIT_DataRead(7);
	  }
	  demand_finish(t, mainEngine);
	  demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,OK), -1, -1);
	  return;

	} else if (is_Present(t->lineState)) {

	  /* read requests which need an upgrade.  Note that even if the
	   * protocol does not have an owner state, requests to own
	   * must be passed through so that lower levels of the protocol which
	   * might have an owner state have a chance of working.  However,
	   * it isn't really clear that such a scheme would work.
	   *
	   * Covers: (when not writeable, but present)
	   * uRdReq. Own. Coh. Cach.!NA:
	   * uRdReq. Own. Coh. Cach. NA:
	   *
	   */

	  printf("TODO: upgrades for reads\n");
	  demand_finish(t,mainEngine);
	  demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,NACK), -1, -1);
	  return;

	} else {

	  /* remaining read requests fall through.  They are those which require
	   * us to find a victim and fetch the line.  They are:
	   *
	   * Covers: (when not present)
	   * uRdReq. Own. Coh. Cach.!NA:
	   * 
	   * Covers: (when not present)
	   * uRdReq.!Own.!Coh. Cach.!NA:
	   * uRdReq.!Own. Coh. Cach.!NA:
	   * uRdReq. Own.!Coh. Cach.!NA:
	   */

	  /* end of reads */
	}

      } else if (is_WrReq(t) || (is_CastOutReq(t) && isWithData(t->origMsg))) {

	/* handling writes and castouts with data */

	if (!isCacheable(t->origMsg)) {
	  /* 
	   * When not cacheable, we have not looked at the cache to find the
	   * data and must send the write down regardless of the write-through.
	   * Note that we do not write the data (since it is not in the cache!)
	   *
	   * Covers:
	   * uWrReq.!Coh.!Cach.!WT.!WA:	lWR, w lWA, uWA
	   * uWrReq.!Coh.!Cach.!WT. WA:	lWR, w lWA, uWA
	   * uWrReq.!Coh.!Cach. WT.!WA:	lWR, w lWA, uWA
	   * uWrReq.!Coh.!Cach. WT. WA:	lWR, w lWA, uWA
	   * uWrReq. Coh.!Cach.!WT.!WA:	lWR, w lWA, uWA
	   * uWrReq. Coh.!Cach.!WT. WA:	lWR, w lWA, uWA
	   * uWrReq. Coh.!Cach. WT.!WA:	lWR, w lWA, uWA
	   * uWrReq. Coh.!Cach. WT. WA:   lWR, w lWA, uWA
	   */

	  t->wasNotCached = true;
	  /* send on down */
	  ist->stateNo = 2; /* guard against dangerous re-entrance */
	  do {
	    ensure_todo_space(t, DEFAULT_SPACE);
	    demand_send_ServReq(e, mainEngine, t, &t->origMsg, -1, -1);
	    WAIT_lWrAck(2);
	  } while (ev->d.Msg.m->reply == Retry);
	  demand_finish(t, mainEngine);
	  demand_send_uReply(e, mainEngine, t, ev->d.Msg.m, -1, -1);
	  return;
      
	} else if (is_Writeable(t->lineState)) {

	  /* write request with write access already granted;
	   * note that write-through is encoded in such a way
	   * that this path is not taken for write-through caches
	   * 
	   * Covers: (all when writeable)
	   * uWrReq.!Coh.Cach.!WT.!WA:	wd,mv,uWrAck
	   * uWrReq.!Coh.Cach.!WT. WA:	wd,mv,uWrAck
	   * uWrReq.!Coh.Cach. WT.!WA:	wd,mv,lWrReq,w lWrAck,uWrAck
	   * uWrReq.!Coh.Cach. WT. WA:	wd,mv,lWrReq,w lWrAck,uWrAck
	   * uWrReq. Coh.Cach.!WT.!WA:	wd,mv,uWA
	   * uWrReq. Coh.Cach.!WT. WA:	wd,mv,uWA
	   * uWrReq. Coh.Cach. WT.!WA:	wd,mv,lWR,w lWA,uWA
	   * uWrReq. Coh.Cach. WT. WA:    wd,mv,lWR,w lWA,uWA
	   */

	  /* TODO: think about invalidation crosscheck here */
	  ist->flags = 0;

	now_writeable:
	  ist->flags2 = t->numToDo;
	  demand_writedata(t, mainEngine, 1, -1, -1);
	now_writeable2:
	  demand_writelinestate(t, mainEngine, 
				nextCstate(e, t, &t->origMsg, 1), -1, -1);
	  // demand_serialized(t, mainEngine);
	  if (isWriteThrough(t->origMsg)) {
	    ist->stateNo = 3; /* guard against dangerous re-entrance */
	    do {
	      ensure_todo_space(t, DEFAULT_SPACE);
	      demand_send_ServReq(e, mainEngine, t, &t->origMsg, ist->flags2,
				  ist->flags2 + 1);
	      WAIT_lWrAck(3);
	    } while (ev->d.Msg.m->reply == Retry);
	    if (ist->flags) demand_unlock(t, mainEngine, ist->flags, ist->flags2,
					  ist->flags2+1);
	    demand_finish(t, mainEngine);
	    demand_send_uReply(e, mainEngine, t, ev->d.Msg.m, -1, -1);
	  }
	  else {
	    if (ist->flags) demand_unlock(t, mainEngine, ist->flags, ist->flags2,
					  ist->flags2+1);
	    demand_finish(t, mainEngine);
	    demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,OK), -1, -1);
	  }
	  return;
      
	} else if (e->pinfo.hasOwner && is_Upgradeable(t->lineState)) {

	  /* write request where access needs to be upgraded to an owner state
	   *
	   * Covers: (not writeable, but upgradeable)
	   * uWrReq.!Coh. Cach.!WT.!WA:  
	   * uWrReq.!Coh. Cach.!WT. WA:  
	   * uWrReq.!Coh. Cach. WT.!WA:  
	   * uWrReq.!Coh. Cach. WT. WA:  
	   * uWrReq. Coh. Cach.!WT.!WA:  
	   * uWrReq. Coh. Cach.!WT. WA:  
	   * uWrReq. Coh. Cach. WT.!WA:  
	   * uWrReq. Coh. Cach. WT. WA:  
	   *
	   */

	  /* TODO: make more efficient locks */
	  if (eParms(e).P.WriteEarly) 
	    ist->flags = (lock_Victim | lock_UWr | lock_URd | lock_DRd | 
			  lock_DWr);
	  else 
	    ist->flags = lock_Victim;

	  ist->flags2 = t->numToDo;
	  demand_lock(t, mainEngine, ist->flags, -1, -1);
	  if (eParms(e).P.WriteEarly && !isConditional(t->origMsg)) {
	    demand_writedata(t, mainEngine, 1, ist->flags2, -1);
	    ist->flags2 = t->numToDo-1;
	  }

	  ist->atRisk = true;
	  /* do a read-to-own */
	  do {
	    ensure_todo_space(t, DEFAULT_SPACE);
	    nm.type = RdReq;
	    /* no need to request actual data transfer or to write it back */
	    nm.flags = (ToOwn | Cacheable | isCoherent(t->origMsg) | 
			isNoAlloc(t->origMsg));
	    demand_send_ServReqWB(e, mainEngine, t, &nm, -1, -1);
	    WAIT_lRdAck(22);
	  } while (ev->d.Msg.m->reply == Retry);

	  if (ist->invalidated) {
	    demand_finish(t, mainEngine);
	    demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,Retry),-1,-1);
	  } else if (ev->d.Msg.m->reply == NACK) {
	    demand_unlock(t, mainEngine, ist->flags, -1, -1);
	    demand_finish(t, mainEngine);
	    demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,NACK),-1,-1);
	  } else {
	    ist->atRisk = false;
	    if (eParms(e).P.WriteEarly && !isConditional(t->origMsg)) 
	      goto now_writeable2; /* so I do not duplicate code */
	    else 
	      goto now_writeable; /* so I do not duplicate code */
	  }
	  return;
      
	} else if (is_Present(t->lineState)) {

	  /* write request where write access is not granted and will not be 
	   * requested, forcing us to write-through the value and update the
	   * cache.  In general, this is where write-through caches go on hits, 
	   * but write-back caches do not end up here.
	   *
	   * Covers: (not writeable and not upgradeable, but present)
	   * uWrReq.!Coh. Cach.!WT.!WA:  wd, (mv), lWR, w lWA, wd, mv, uWA
	   * uWrReq.!Coh. Cach.!WT. WA:  ""
	   * uWrReq.!Coh. Cach. WT.!WA:  ""
	   * uWrReq.!Coh. Cach. WT. WA:  ""
	   * uWrReq. Coh. Cach.!WT.!WA:  wd, lWR, w lWA, wd, mv, uWA
	   * uWrReq. Coh. Cach.!WT. WA:  
	   * uWrReq. Coh. Cach. WT.!WA:  
	   * uWrReq. Coh. Cach. WT. WA:  
	   *
	   */

	  /* If we are going to write data early (i.e., keep it in the
	   * array while waiting for confirmation of the write), we must
	   * lock the array against victimization, upstream accesses, and
	   * possibly downstream reads.  If we are not going to write
	   * early, we must still lock against victimization.  
	   *
	   * NOTE: cannot do conditional writes early!
	   *
	   * TODO: incoherent stores may have slightly different locks
	   */

#ifdef TODO /* make more efficient locks */
	  if (eParms(e).P.WriteEarly && !isConditional(t->origMsg)) 
	    ist->flags = (lock_Victim | lock_URd | lock_UWr | 
			  ((!eParms(e).P.ReadBuffered || 
			    !eParms(e).P.WriteEarly) 
			   ? lock_DRd : 0));
	  else 
	    ist->flags = lock_Victim;
#else
	  /* castout requests need to remember previous lock state */
	  /* TODO: I do not think the locking is right for castouts; we may
	   * need to deal with a cross-check situation or a special kind
	   * of locking....
	   */
	  nflags = ist->flags = (lock_Victim | lock_URd | lock_UWr | lock_DRd | 
				 lock_DWr /*| lock_DCoh*/);
	  if (is_CastOutReq(t)) ist->flags &= ~t->lineState;
#endif
	
	  if (isCoherent(t->origMsg)) ist->atRisk = true;
	  ist->flags2 = t->numToDo;
	  demand_lock(t, mainEngine, nflags, -1, -1);

	  /* early write */
	  if (eParms(e).P.WriteEarly && !isConditional(t->origMsg)) {
	    demand_writedata(t, mainEngine, 1, ist->flags2, -1);
	    ist->flags2 = t->numToDo-1;
	  }

	  /* send on down */
	  ist->stateNo = 4; /* guard against dangerous re-entrance */
	  do {
	    ensure_todo_space(t, DEFAULT_SPACE);
	    demand_send_ServReq(e, mainEngine, t, &t->origMsg, ist->flags2, -1);
	    WAIT_lWrAck_lCastOutAck(4);
	  } while (!ist->invalidated && 
		   (ev->d.Msg.m->reply == Retry ||
		    /* if wrote early, must finish unless invalidated */
		    (ev->d.Msg.m->reply == NACK && eParms(e).P.WriteEarly &&
		     !isConditional(t->origMsg))));

	  if (ist->invalidated) {
	    ist->atRisk = false;
	    demand_finish(t, mainEngine);
	    demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,Retry),-1,-1);
	    return;
	  }
	  if (ev->d.Msg.m->reply == OK) {
	    /* late write */
	    if (!eParms(e).P.WriteEarly || isConditional(t->origMsg)) 
	      demand_writedata(t, mainEngine, 1,-1,-1);
	    ist->flags2 = t->numToDo;
	    demand_writelinestate(t, mainEngine, 
				  nextCstate(e, t, &t->origMsg, 1),-1,-1);
	  }
	  if (ist->flags)
	    demand_unlock(t, mainEngine, ist->flags, ist->flags2-1, ist->flags2);
	  demand_may_finish(t, mainEngine);
	  ist->stateNo = -1001;
	  demand_send_uReply(e, mainEngine, t, ev->d.Msg.m, ist->flags2-1, 
			     ist->flags2);
	  return;

	} else if (isConditional(t->origMsg)) {

	  /* conditional writes where the line is not in the cache 
	   * must fail. 
	   */

	  demand_finish(t, mainEngine);
	  demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,NACK), -1, -1);
	  return;

	} else if (!eParms(e).P.WriteAlloc || isNoAlloc(t->origMsg)) {

	  /* write request where access is not granted and will not be requested,
	   * and we are not in the cache.
	   *
	   * Covers: (not writeable and not upgradeable and not present)
	   * uWrReq.!Coh. Cach.!WT.!WA:  lWR, w lWA, uWA
	   * uWrReq.!Coh. Cach. WT.!WA:  ""
	   * uWrReq. Coh. Cach.!WT.!WA:  ""
	   * uWrReq. Coh. Cach. WT.!WA:  ""
	   *
	   * Covers: (not writeable and not upgradeable and not present)
	   *         and WriteAlloc parameter is false
	   * uWrReq.!Coh. Cach.!WT. WA:  lWR, w lWA, uWA
	   * uWrReq.!Coh. Cach. WT. WA:  ""
	   * uWrReq. Coh. Cach.!WT. WA:  ""
	   * uWrReq. Coh. Cach. WT. WA:  ""
	   *
	   */
	
	  t->wasNotCached = true;
	  /* send on down */
	  ist->stateNo = 5; /* guard against dangerous re-entrance */
	  do {
	    ensure_todo_space(t, DEFAULT_SPACE);
	    demand_send_ServReq(e, mainEngine, t, &t->origMsg, -1, -1);
	    WAIT_lWrAck_lCastOutAck(5);
	  } while (ev->d.Msg.m->reply == Retry);
      
	  demand_finish(t, mainEngine);
	  demand_send_uReply(e, mainEngine, t, ev->d.Msg.m, -1, -1);
	  return;

	}
      
	/* remaining write requests fall through.  They are:
	 *
	 * write requests where we are allocating the line 
	 *
	 * Covers: (not writeable and not upgradeable and not present)
	 * uWrReq.!Coh. Cach.!WT. WA:  lWR, w lWA, uWA
	 * uWrReq.!Coh. Cach. WT. WA:  ""
	 * uWrReq. Coh. Cach.!WT. WA:  ""
	 * uWrReq. Coh. Cach. WT. WA:  ""
	 *
	 */

      } else if (is_CastOutReq(t)) {
	demand_finish(t, mainEngine);
	demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,OK),-1,-1);
	return;
      }

      /* requests where we are allocating the line 
       *
       * Covers: (not writeable and not upgradeable and not present)
       * uWrReq.!Coh. Cach.!WT. WA:  lWR, w lWA, uWA
       * uWrReq.!Coh. Cach. WT. WA:  ""
       * uWrReq. Coh. Cach.!WT. WA:  ""
       * uWrReq. Coh. Cach. WT. WA:  ""
       *
       * Covers: (when not writeable, and not upgradeable and not present)
       * uRdReq. Own. Coh. Cach.!NA:
       * 
       * Covers: (when not present)
       * uRdReq.!Own.!Coh. Cach.!NA:
       * uRdReq.!Own. Coh. Cach.!NA:
       * uRdReq. Own.!Coh. Cach.!NA:
       *
       */
#define earlyVictimChoice (eParms(e).P.VictimHandling < 4)
#define earlyVictimStart  (eParms(e).P.VictimHandling < 3)
#define earlyEviction     (eParms(e).P.VictimHandling < 2)
#define earlyVictimFinish (eParms(e).P.VictimHandling < 1)

      if (isCoherent(t->origMsg)) ist->atRisk = true;

      /* lock the line to be put in (i.e., primary miss locking)
       * lock will transfer when victim chosen and be visible on both
       * new line and chosen line
       */
      ist->flags = lock_URd | lock_UWr | lock_DRd | lock_DWr/* | lock_DCoh*/;
      ist->flags2 = t->numToDo;
      demand_lock(t, ino, ist->flags, -1, -1);

      ist->msgCount = 0; /* will always keep number of transactions to go */

      if (earlyVictimChoice) {

	/* lock up victim and start up a transaction for it; the new
	 * transaction must be at risk from invalidations, must have
	 * one interface alive, have the state specified, and have its
	 * state properly set up.  
	 */
	/* ist->flags |= (lock_All); */
	ist->flags |= (lock_URd | lock_UWr | lock_DRd | lock_DWr);
	demand_choosevictim(t,mainEngine,ist->flags,99,ist->flags2,-1);
	WAIT_VictimChosen(8);

	if (!ev->d.VictimChosen.gotVictim) { /* no victim here! */
	  if (!ist->invalidated) { /* invalidate will unlock */
	    demand_unlock(t,ino,ist->flags,-1,-1);
	    demand_may_finish(t, mainEngine);
	    ist->stateNo = -1001;	/* forget everything but messages on Inval */
	  } else {
	    demand_finish(t, mainEngine);
	  }
	  demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,Retry),-1,-1);
	  return;
	}

	/* 
	 * At this point, the victim transaction is waiting for us, and is
	 * at risk from being invalidated itself.
	 */
      
	/* if we got invalidated before we even began, tags must not have
	 * matched, so invalidation has already done our unlock.  We should
	 * tell the victim line to go back to normal and should do nothing
	 * ourselves...
	 */

	if (ist->invalidated) {
	  if (t->linkedTrans) demand_notify(t,mainEngine,1,-1, -1, -1);
	  goto handle_bus;
	}

	if (earlyEviction) demand_writetag(t,mainEngine,-1,-1);

	/* While tag is not yet written, responsibility for removing locks
	 * remains here.
	 */

	if (t->linkedTrans) {

	  if (eParms(e).P.WriteEarly && is_WrReq(t)) {

	    demand_notify(t,mainEngine, 1, 0, -1, -1);
	    /* 
	     * During this wait, if we get an invalidation, we should
	     * not go ahead and write the data.  We will have to do a
	     * write-through later on.  However, we are not going to
	     * tell the victim anything; it will just have to keep
	     * going... We will not cancel the replacement actions on 
	     * invalidation.
	     */
	    WAIT_Notify(9);  /* must wait for data to be read! */
	    ist->msgCount = 1; /* remaining notifications */
	    if (ist->invalidated) goto handle_bus;
	    ist->flags2 = t->numToDo;
	    demand_writedata(t, mainEngine, 1, -1, -1);
	  } 

	  if (earlyVictimFinish) {

	    demand_notify(t,mainEngine, 1, 1, -1, -1);
	    //t->hasVictim = true;
	    if (!ist->msgCount) ist->msgCount = 2; 
	    /* wait for both read and messages */
	    while (ist->msgCount) {
	      /* 
	       * During this wait, if we get an invalidation, we do nothing
	       * in particular, as we are going to just drop out into bus
	       * handling anyway...  We will not cancel the replacement
	       * actions (tag/data) on invalidation.
	       */
	      WAIT_Notify(10);  /* wait for invalidation to finish.... */
	      ist->msgCount--;
	    }

	  } else if (earlyVictimStart) {
	    demand_notify(t,mainEngine, 1, 1, -1, -1); /* start it up... */
	    ist->msgCount = 2; /* remaining notifications */
	  }
	  else ist->msgCount = 2; /* have not done anything yet... */

	} else { /* no victim handling to communicate with */

	  if (eParms(e).P.WriteEarly && is_WrReq(t)) {
	    demand_writedata(t, mainEngine, 1, ist->flags2, -1);
	  }
	}

      } /* if (earlyVictimChoice) */
  
    handle_bus:

      /* now we can go ahead and request the line */
      ist->stateNo = 12; /* guard against dangerous re-entrance */
      do {
	nm = t->origMsg;
	nm.type = RdReq;
	nm.flags &= ~(Prefetch | ToOwn);
	nm.flags |= WithData | (do_RTO(e,t) ? ToOwn : 0);

	ensure_todo_space(t, DEFAULT_SPACE);
	demand_send_ServReqWB(e, mainEngine, t, &nm, ist->flags2, -1);
	/* 
	 * An invalidation during this state does nothing; we still attempt
	 * to do any tag and data writes we scheduled before.
	 */
      in_waithb:
	WAIT(12);
	if (ev->etype == LSE_cache_eventtype_Notify) {
	  ist->msgCount--;
	  goto in_waithb;
	} else if (ev->etype != LSE_cache_eventtype_Msg) {
	  do_error(e,t,mainEngine,LSE_cache_error_InconsistentEvent);
	}
	else if (ev->d.Msg.dir || ev->d.Msg.m->type != RdAck) {
	  do_error(e,t,mainEngine,LSE_cache_error_InconsistentMsg);
	}
      } while (ev->d.Msg.m->reply == Retry);

      /* Handle NACK or non-cacheable reply.  Must end with line
       * not in cache.
       */

      if (ev->d.Msg.m->reply == NACK) {

	/* tell victim transaction we gave up */
	if (t->linkedTrans && ist->msgCount) 
	  demand_notify(t,mainEngine,1,-1,-1,-1);

	/* if an invalidate didn't clear out the transaction state,
	 * clear it out ourselves
	 */
	if (!(ist->invalidated & 2)) {
	  demand_writelinestate(t, mainEngine, Invalid, -1, -1);
	  demand_unlock(t, mainEngine, ist->flags, t->numToDo-1, -1);
	}
	/* if an invalidate occurs now, it shuld just wipe out state writes
	 * if the invalidate hit the line
	 */
	demand_may_finish(t, mainEngine);
	ist->stateNo = -1002;
	demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,NACK), -1, -1);
	return;

      }

      if (isNoCache(*(ev->d.Msg.m))) {

	/* tell victim transaction we gave up */
	if (t->linkedTrans && ist->msgCount) 
	  demand_notify(t,mainEngine,1,-1,-1,-1);

	/* if an invalidate didn't clear out the transaction state,
	 * clear it out ourselves
	 */
	if (!(ist->invalidated & 2)) {
	  demand_writelinestate(t, mainEngine, Invalid, -1, -1);
	  demand_unlock(t, mainEngine, ist->flags, t->numToDo-1, -1);
	}

	/* if an invalidate occurs now, it shuld just wipe out state writes
	 * if the invalidate hit the line
	 */
	if (ist->invalidated) newstate = Invalid;

	if (!is_WrReq(t)) {
	  demand_may_finish(t, mainEngine);
	  ist->stateNo = -1002;
	  demand_send_uReply(e, mainEngine, t, form_nc_reply(t,&nm,NACK), -1,-1);
	} else goto write_through;

	return;
      }
  
      /* NOTE: must re-read ist->invalidated after every wait because of 
       * cross-checking with incoming invalidations
       */

      /* Do we need to choose a victim? */

      if (!ist->invalidated && !earlyVictimChoice) {

	/* lock up victim and start up a transaction for it; the new
	 * transaction must be at risk from invalidations, must have
	 * one interface alive, have the state specified, and have its
	 * state properly set up.  
	 */
	/* ist->flags |= lock_All; */
	ist->flags |= (lock_URd | lock_UWr | lock_DRd | lock_DWr);
	demand_choosevictim(t,mainEngine,ist->flags,99,-1,-1);
	/* 
	 * During this wait, if an invalidate occurs, we do nothing.
	 */
	WAIT_VictimChosen(14);
      
	if (!ev->d.VictimChosen.gotVictim) { /* no victim! */
	  if (!ist->invalidated) { /* invalidate will unlock */
	    demand_unlock(t,ino,ist->flags,-1,-1);
	    demand_may_finish(t, mainEngine);
	    ist->stateNo = -1002; /* remember all but state writes on hits */
	  } else demand_finish(t, mainEngine);
	  if (is_WrReq(t)) {
	    // TODO: could turn into a write-through...
	    demand_send_uReply(e, mainEngine, t, form_reply(t,&nm,Retry),-1,-1);
	  } else 
	    demand_send_uReply(e, mainEngine, t, 
			       eParms(e).P.Inclusive || ist->invalidated 
			       ? form_nc_reply(t,&nm,OK) : form_reply(t,&nm,OK),
			       -1, -1);
	  return;
	}

	/* 
	 * At this point, the victim transaction is waiting for us, and is
	 * at risk from being invalidated itself.  We need two notifications
	 * from it...
	 */
	if (t->linkedTrans) ist->msgCount = 2;
      } 
  
      /* the writes of data must happen after the old data has been read,
       * but tag and linestate can be updated at any time.  Of course
       * lock must wait for all...  However, waiting for line state is best
       * as it will ensure that we see invalidations without having to write
       * twice
       */
    
      if (!ist->invalidated && ist->msgCount == 2) { 
	/* need to do reads before writing line */
	if (!earlyVictimStart) {
	  demand_notify(t,mainEngine,1,0,-1,-1);
	}
	/* During this wait, invalidations do nothing */
	WAIT_Notify(13);

	// The "1" message handles the case where the victim is
	// invalidated while it waits for the upstream interface
	// to acknowledge that invalidate.  In that case, the line
	// waiting for the victim to finish needs to know that it should
	// not wait for the rest of the protocol to be finished.

	if (ev->d.Notify.msg) ist->msgCount = 0;
	else ist->msgCount = 1;
      }

      /* now write the data update to the line */
      if (!ist->invalidated) {
	if (is_WrReq(t)) {
	  if (eParms(e).P.WriteEarly && earlyVictimChoice) {
	    ist->flags2 = t->numToDo;
	    demand_writedata(t, mainEngine, 7, -1, -1);
	  } else {
	    ist->flags2 = t->numToDo;
	    demand_writedata(t, mainEngine, 3, -1, -1);
	  }
	} else if (is_RdReq(t)) {
	  ist->flags2 = t->numToDo;
	  demand_writedata(t, mainEngine, 2, -1, -1);
	}
      }
  
      if (!ist->invalidated && ist->msgCount) {
	demand_notify(t,mainEngine,1,1,-1,-1);
	/* During this wait, invalidations do nothing */
	WAIT_Notify(1);
	ist->msgCount = 0;
      }

      /* tell the victim to die */
      if (ist->invalidated && ist->msgCount) {
	demand_notify(t,mainEngine,1,-1,-1,-1);
      }

      if (!ist->invalidated) {
	if (!earlyEviction) demand_writetag(t, mainEngine,-1,-1);
	newstate = nextCstate(e, t, &t->origMsg, 1);
	demand_writelinestate(t, mainEngine, newstate, -1, -1);
	if (!earlyEviction) { /* 3 things to wait for, so must use none node */
	  demand_none(t, mainEngine, t->numToDo-2, t->numToDo-1);
	}
	demand_unlock(t, mainEngine, ist->flags, ist->flags2, t->numToDo-1);
      } else if (!(ist->invalidated & 2)) {
	demand_writelinestate(t, mainEngine, Invalid, -1, -1);
	newstate = Invalid;
	demand_unlock(t, mainEngine, ist->flags, ist->flags2, t->numToDo-1);
      }
    
    write_through:

      /* Do we need to do a write-through? */
      if (is_WrReq(t) && (!is_Writeable(newstate) || 
			  isWriteThrough(t->origMsg))) {
	/* do write and send ack when over... */
	ist->stateNo = 16; /* guard against dangerous re-entrance */
	do {
	  ensure_todo_space(t, DEFAULT_SPACE);
	  demand_send_ServReq(e, mainEngine, t, &t->origMsg, -1, -1);
	in_wait:
	  /* DO NOT CHANGE THIS WAIT NUMBER, AS IT IS CROSS-CHECKED 
	   * During this wait, if an invalidation arrives, the line is already
	   * synchronized.  However, there might still be state updates out
	   * there which haven't gone yet.  These updates should be deleted
	   * if there is a hit.
	   *
	   * Note that there might be notifications left over which are
	   * to be ignored...
	   *
	   */
	  WAIT(16);
	  if (ev->etype == LSE_cache_eventtype_Notify) {
	    ist->msgCount--;
	    goto in_wait;
	  } else if (ev->etype != LSE_cache_eventtype_Msg) 
	    do_error(e,t,mainEngine,LSE_cache_error_InconsistentEvent);
	  if (ev->d.Msg.dir || ev->d.Msg.m->type != WrAck)
	    do_error(e,t,mainEngine,LSE_cache_error_InconsistentMsg);
	} while (ev->d.Msg.m->reply == Retry);
	nm = *ev->d.Msg.m;
	nm.flags &= ~(NoCache); nm.flags |= ist->invalidated ? NoCache : 0;
      } else {
	/* send ack now */
	form_reply(t,&nm,OK);
	nm.flags |= ist->invalidated ? NoCache : 0;
      }
  
      demand_may_finish(t,mainEngine);
      /* and now, if we get an invalidate, we are to remove any state writes
       * from hits and turn any replies into non-cacheable replies. If the
       * invalidate was a miss, we need to clear the line state
       */
      ist->stateNo = -1003;
      demand_send_uReply(e, mainEngine, t, &nm, -1, -1);
      return;
  
    state99: /* handling of victim lines */

      if (ev->etype != LSE_cache_eventtype_Notify) 
	do_error(e,t,mainEngine,LSE_cache_error_InconsistentEvent);

      if (ev->d.Notify.msg == -1) { /* message is to ignore */
	demand_finish(t,mainEngine);
	return;
      }

      /* We must invalidate upper caches first and then see whether we
       * have dirty data.  The reason for this is that the invalidate to
       * the upper might flush dirty data towards us, which we must
       * accept and merge before sending out our dirty data.  We must
       * also delay the notification that the data was dirty until this
       * occurs.  Ugh!  
       */

      if (is_Present(t->lineState)) {

	if (ist->invalidated) {
	  demand_notify(t,mainEngine,0,0,-1,-1);
	  demand_finish(t,mainEngine);
	  return;
	}

	/* TODO: DO WE NEED TO LOCK THE LINE VS INVALIDATIONS? */
	ist->flags = ev->d.Notify.msg;

	if (eParms(e).P.Inclusive) {
	
	  /* Now send up invalidations.  We must invalidate the upper
	   * caches before we read the data because the invalidate might
	   * flush data towards us which we need to have written into the
	   * array.
	   */
	  ist->stateNo = 11; /* guard against dangerous re-entrance */
	  do {
	    ensure_todo_space(t, DEFAULT_SPACE);
	    nm.type = InvalReq;
	    nm.flags = 0;
	    demand_send_uReqAllWB(e, mainEngine, t, &nm, -1, -1);
	    WAIT_uInvalAck(11);
	  } while (ev->d.Msg.m->reply != OK && !ist->invalidated);
	
	  if (ist->invalidated) {
	    // The "1" messages handles the case where the victim is
	    // invalidated while it waits for the upstream interface
	    // to acknowledge that invalidate.  In that case, the line
	    // waiting for the victim to finish needs to know that it should
	    // not wait for the rest of the protocol to be finished.
	    demand_notify(t,mainEngine,0,1,-1,-1);
	    demand_finish(t,mainEngine);
	    return;
	  }

	  /* invalidation has data; put it away */
	  if (isWithData(*(ev->d.Msg.m))) {
	    t->lineState |= ModifiedBit;
	    demand_writedata(t,mainEngine,2,-1,-1);
	    ist->flags2 = t->numToDo-1;
	  } else ist->flags2 = -1;
	}
      
	/* read the line if we were dirty */	
	if (is_Dirty(t->lineState)) {
	  demand_readdata(t,mainEngine, ist->flags2, -1);
	  ist->flags2 = t->numToDo - 1;
	  WAIT_DataRead(18);

	  if (ist->invalidated) {
	    demand_notify(t,mainEngine,0,0,-1,-1); 
	    demand_finish(t,mainEngine);
	    return;
	  }
	}

	/* say we read the line */
	demand_notify(t,mainEngine,0,0, ist->flags2, -1);
	if (ist->flags == 0) 
	  WAIT_Notify(19);  /* wait to be allowed to do final message */

	ist->msgCount = 1;
      } else { /* is_Present */
	demand_finish(t,mainEngine);
	return;
      }

      while (!ist->invalidated) {
	ist->stateNo = 17; /* guard against dangerous re-entrance */
	nm.type = CastOutReq;
	nm.flags = (Cacheable | Coherent | NoAlloc |
		    (is_Dirty(t->lineState) ? WithData : 0));
	ensure_todo_space(t, DEFAULT_SPACE);
	demand_send_ServReqWB(e, mainEngine, t, &nm, -1, -1);
	WAIT_lCastOutAck(21);
	if (ev->d.Msg.m->reply == OK) break;
      }

      /* say we dealt with the invalidation */
      demand_notify(t,mainEngine,0,0,-1,-1); 
      demand_finish(t,mainEngine);
      return;
  
#undef earlyVictimChoice
#undef earlyVictimDataRead
#undef earlyEviction
#undef earlyVictimMsg
  
    };

    /*************** Upward-bound transactions ***************/
    {
    upward_bound:
      mtype = t->origMsg.type; 

      if (mtype != InvalReq)
	do_error(e,t,mainEngine,LSE_cache_error_InconsistentMsg);

      t->aliveCnt++;

      /* cache line has already been looked up by interface */

      /* if line is present or in a transient state */

      if (t->lineState) {
	ist->flags = true;
	/* overwrite the locks */
	t->lineState &= 0xff;
	ist->flags2 = t->numToDo;
	demand_alllock(t, mainEngine, lock_All, -1, -1);

	ist->stateNo = 15;
	/* cross-check should cause all conflicting ones to forget their locks */
	demand_crosscheck(t, mainEngine, ist->flags2, -1);
	ist->flags2 = t->numToDo-1;
      } else {
	ist->flags2 = -1;
	ist->flags = false;
      }
    state15:

      /* if we've got to send messages up, do it. */
      if (is_Present(t->lineState) || !eParms(e).P.Inclusive) {
	ist->stateNo = 17;
	do {
	  ensure_todo_space(t, DEFAULT_SPACE);
	  demand_send_uReqAll(e, mainEngine, t, &t->origMsg, ist->flags2, -1);
	  WAIT_uInvalAck(17);
	} while (ev->d.Msg.m->reply != OK);

	/* if the Inval.OK has data, use it! */
	if (isWithData(*(ev->d.Msg.m))) {
	  t->lineState |= ModifiedBit;
	  demand_writedata(t, mainEngine, 2, ist->flags2, -1);
	  ist->flags2 = t->numToDo - 1;
	}
      } /* if is_Present */

      /* Now, we need to double-check the state to see if we have to 
       * throw it away
       */

      if (is_Dirty(t->lineState)) {
	/* need to writeback dirty lines.... */
	demand_readdata(t,mainEngine, ist->flags2, -1);
	WAIT_DataRead(20);
      }

      form_reply(t,&nm,OK);
      nm.flags |= is_Dirty(t->lineState) ? WithData : 0;

      if (is_Present(t->lineState)) {
	demand_writelinestate(t,mainEngine,
			      nextCstate(e, t, &t->origMsg, 0), 
			      ist->flags2, -1);
	ist->flags2 = t->numToDo-1;
      }
      if (ist->flags) demand_allunlock(t, mainEngine, lock_All, ist->flags2, -1);

      demand_send_lReply(e, mainEngine, t, &nm, -1, -1);
      demand_finish(t, mainEngine);
      return;
    } /* upward_bound section */

  } /* engine_handle_event */

  static void 
  engine_handle_crosscheck(LSE_cache_engine_t *e,
			   LSE_cache_tstate_t *t,
			   LSE_cache_tstate_t *told) {
    LSE_cache_istate_t *ist;
    int ntd, tdi;
    bool invalhit = (t->lineState & 7);

    ist = &told->iState[mainEngine];
    /* indicate whether invalidating instruction hit */
    ist->invalidated = invalhit ? 3 : 1;

    switch (ist->stateNo) {
    case -1001:
      /* forget everything but messages */
      ntd = told->numToDo;
      for (tdi = told->todoPtr ; tdi <= ntd ; tdi++) {
	if (told->todo[tdi].ino == mainEngine) {
	  if (told->todo[tdi].action == LSE_cache_actiontype_Send) continue;
	  told->todo[tdi].action = LSE_cache_actiontype_None;
	}
      }
      ist->atRisk = false;
      ist->stateNo = -1;
      break;

    case -1002:
      /* forget only state updates and only if the invalidate hit */
      if (invalhit) {
	ntd = told->numToDo;
	for (tdi = told->todoPtr ; tdi <= ntd ; tdi++) {
	  if (told->todo[tdi].ino == mainEngine) {
	    if (told->todo[tdi].action != LSE_cache_actiontype_WriteState) 
	      continue;
	    told->todo[tdi].action = LSE_cache_actiontype_None;
	  }
	}
	ist->atRisk = false;
	ist->stateNo = -1;
      }
      break;

    case -1003:
      /* change the reply to a non-cacheable reply, cancel all hit
       * state writes, and if invalidate was a hit, clear the line state
       */
      ntd = told->numToDo;
      for (tdi = told->todoPtr ; tdi <= ntd ; tdi++) {
	if (told->todo[tdi].ino == mainEngine) {
	  if (told->todo[tdi].action == LSE_cache_actiontype_Send)
	    told->todo[tdi].work.Send.newMsg.flags |= NoCache;
	  else if (told->todo[tdi].action != LSE_cache_actiontype_WriteState) 
	    continue;
	  else if (invalhit)
	    told->todo[tdi].action = LSE_cache_actiontype_None;
	}
      }
      if (!invalhit) {
	ensure_todo_space(told, DEFAULT_SPACE);
	demand_writelinestate(told, told->todo[tdi].ino, Invalid,
			      t->numToDo-1, -1);
	demand_unlock(told, told->todo[tdi].ino, lock_All, t->numToDo-1, -1);
      }
      ist->atRisk = false;
      ist->stateNo = -1;
      break;

      /* special cases during miss processing */
      /* forget state updates, as we do not want them any more, but
       * need to remember other stuff and remain at risk and in state
       */
      ntd = told->numToDo;
      for (tdi = told->todoPtr ; tdi <= ntd ; tdi++) {
	if (told->todo[tdi].ino == mainEngine) {
	  if (told->todo[tdi].action != LSE_cache_actiontype_WriteState) 
	    continue;
	  told->todo[tdi].action = LSE_cache_actiontype_None;
	}
      }
      break;

    case 16:
      /* forget only state updates and only if the invalidate hit */
      if (invalhit) {
	ntd = told->numToDo;
	for (tdi = told->todoPtr ; tdi <= ntd ; tdi++) {
	  if (told->todo[tdi].ino == mainEngine) {
	    if (told->todo[tdi].action != LSE_cache_actiontype_WriteState) 
	      continue;
	    told->todo[tdi].action = LSE_cache_actiontype_None;
	  }
	}
      }
      break;

    default: break; /* do nothing */
    } /* ist->stateNo */
  }

  static void setup_protocol(LSE_cache_engine_t *e) {
    switch (e->interfaces[mainEngine].parms.P.protocol) {
    case LSE_cache_protocol_NoneWB:
      e->pinfo.protoTable = noneWB;
      e->pinfo.protoTableSize = (sizeof(noneWB)/
				 sizeof(LSE_cache_protoTableEntry_t));
      e->pinfo.hasOwner = true;
      break;
    case LSE_cache_protocol_NoneWT:
      e->pinfo.protoTable = noneWT;
      e->pinfo.protoTableSize = (sizeof(noneWT)/
				 sizeof(LSE_cache_protoTableEntry_t));
      e->pinfo.hasOwner = true;
      break;
    case LSE_cache_protocol_WriteThrough:
      e->pinfo.protoTable = writeThrough;
      e->pinfo.protoTableSize = (sizeof(writeThrough)/
				 sizeof(LSE_cache_protoTableEntry_t));
      e->pinfo.hasOwner = false;
      break;
    case LSE_cache_protocol_MSI:
      e->pinfo.protoTable = MSI;
      e->pinfo.protoTableSize = (sizeof(MSI)/
				 sizeof(LSE_cache_protoTableEntry_t));
      e->pinfo.hasOwner = true;
      break;
    case LSE_cache_protocol_MESI:
    case LSE_cache_protocol_MOESI:
    case LSE_cache_protocol_MOSI:
    case LSE_cache_protocol_Illinois:
    case LSE_cache_protocol_Synapse:
    case LSE_cache_protocol_Berkeley:
    case LSE_cache_protocol_Firefly:
    case LSE_cache_protocol_Dragon:
      fprintf(stderr,"Unimplemented cache protocol %d\n", 
	      e->interfaces[mainEngine].parms.P.protocol);
      exit(1);
    default:
      fprintf(stderr,"Unrecognized cache protocol %d\n", 
	      e->interfaces[mainEngine].parms.P.protocol);
      exit(1);
    }
    e->interfaces[mainEngine].handler = engine_handle_event;
    e->interfaces[mainEngine].crosshandler = engine_handle_crosscheck;
    e->interfaces[mainEngine].willStart = engine_msg_starts_transaction;
    e->interfaces[mainEngine].interceptMsg = engine_intercept_msg;
  }

  /*********************************************************************
   * Lower interface protocols
   ********************************************************************/

  /********************** Null *****************************/

  static bool 
  null_lower_msg_starts_transaction(LSE_cache_engine_t *e, 
				    const LSE_cache_msg_t *m,
				    int dir, bool snooped) {
    if (!dir) return e->interfaces[lowerInterface-1].willStart(e,m,dir,snooped);
    else return false;
  }

  static bool 
  null_lower_intercept_msg(LSE_cache_engine_t *e, LSE_cache_tstate_t *t) {
    return false;
  }

  static void
  null_lower_handle_event(LSE_cache_engine_t *e, 
			  LSE_cache_tstate_t *t,
			  const LSE_cache_event_t *ev) {

    LSE_cache_msg_t nm;
    LSE_cache_istate_t *ist= &t->iState[lowerInterface];
    int ino = lowerInterface; /* needed for wait macros */

    /********************* 
     * Invariants: 
     * 1) outgoing messages will only be passed to this routine if they are 
     *    "intercepted"
     * 2) incoming messages will all be passed to this routine...
     * 3) accesses to directories/tags will only be passed to this routine
     *    if they were requested by this interface
     ************************/

    switch (ist->stateNo) {
    case -1: 
      { /* we are already done with this transaction, so we allow
	 * any more upstream messages destined for it to move upstream
	 * On the other hand, we do *not* accept any more upstream or
	 * any other kind...
	 */
	if (ev->etype != LSE_cache_eventtype_Msg) 
	  demand_error(e,t,lowerInterface,LSE_cache_error_InconsistentEvent);
	else if (ev->d.Msg.dir) 
	  demand_error(e,t,lowerInterface,LSE_cache_error_InconsistentMsg);
	else e->interfaces[lowerInterface-1].handler(e,t,ev);
	return;
      };
    case 0: goto state0;
    case 1: goto state1;
    default:
      demand_error(e,t,lowerInterface,LSE_cache_error_UnknownState);
      return;
    }


    {
      LSE_cache_msgtype_t mtype;
      /************ Initial state *******************/
    state0:

      if (ev->etype != LSE_cache_eventtype_Msg) {
	demand_error(e,t,lowerInterface,LSE_cache_error_InconsistentEvent);
	return;
      }

      /* we should not have intercepted anything going downstream */
      if (ev->d.Msg.dir) {
	demand_error(e,t,lowerInterface,LSE_cache_error_InconsistentMsg);
	return; 
      }
      /* replies coming back need to be passed directly through, OR
       * we need to intercept the request on its way up and have our own
       * states.  It is cheaper to pass directly if we have nothing special
       * to be doing...
       */
      mtype = ev->d.Msg.m->type;
      if (mtype != LSE_cache_msgtype_InvalReq) {
	ist->stateNo = -1;
	e->interfaces[lowerInterface-1].handler(e,t,ev);
	return;
      }

      /* start up upstream request by checking the cache */

      if (mtype == LSE_cache_msgtype_InvalReq || isCacheable(*(ev->d.Msg.m))) {
	t->aliveCnt++;
	demand_readlinestate(t, lowerInterface, LSE_cache_stateaccess_Tags, 
			     -1, -1);
	WAIT_StateRead(1);

	if (lower_requiresRetry(e,t)) {
	  demand_send_lReply(e, lowerInterface, t, form_reply(t,&nm,Retry), 
			     -1, -1);
	  demand_finish(t, lowerInterface);
	  return;
	}
	demand_finish(t, lowerInterface);
	e->interfaces[lowerInterface-1].handler(e, t, &nullEv);
      } else {
	t->lineState = Invalid;
	ist->stateNo = -1;
	e->interfaces[lowerInterface-1].handler(e, t, &nullEv);
      }
    }
    return;
  }

  /********************** Snoop *****************************/

  static bool 
  snoop_lower_msg_starts_transaction(LSE_cache_engine_t *e,
				     const LSE_cache_msg_t *m,
				     int dir, bool snooped) {
    if (!dir)
      if (m->type == WrReq && isCoherent(*m) && 
	  e->interfaces[lowerInterface].parms.Snoop.SeeInWriteAsInval) 
	return true;
      else return e->interfaces[lowerInterface-1].willStart(e,m,dir,snooped);
    else return false;
  }

  static bool 
  snoop_lower_intercept_msg(LSE_cache_engine_t *e, LSE_cache_tstate_t *t) {

    LSE_cache_event_t ev;
    LSE_cache_msg_t m;

    /* intercept outgoing write requests if we need to wait for 
     * acknowledgements
     */
    if ((t->todo[t->numToDo-1].work.Send.dir &&
	 t->todo[t->numToDo-1].work.Send.newMsg.type == WrReq &&
	 isCoherent(t->todo[t->numToDo-1].work.Send.newMsg)) ||
	(t->todo[t->numToDo-1].work.Send.dir &&
	 t->todo[t->numToDo-1].work.Send.newMsg.type == RdReq &&
	 isToOwn(t->todo[t->numToDo-1].work.Send.newMsg))
	) {
      t->numToDo--;
      m = t->todo[t->numToDo].work.Send.newMsg;

      ev.etype = LSE_cache_eventtype_Msg;
      ev.d.Msg.m = &m;
      ev.d.Msg.dir = t->todo[t->numToDo].work.Send.dir;
      ev.d.Msg.snooped = false;
      /* ev.d.Msg.replyIDtouse = t->todo[t->numToDo].work.replyIDtouse; */
      e->interfaces[lowerInterface].handler(e,t,&ev);
      return true; /* handled it... */
    }
    return false;
  }

  static void
  snoop_lower_handle_event(LSE_cache_engine_t *e, LSE_cache_tstate_t *t,
			   const LSE_cache_event_t *ev) {

    LSE_cache_istate_t *ist = &t->iState[lowerInterface];
    int ino = lowerInterface; /* so macros will be happy */
    LSE_cache_msg_t nm;
    LSE_cache_msgtype_t mtype;

    /********************* 
     * Invariants: 
     * 1) outgoing messages will only be passed to this routine if they are 
     *    "intercepted"
     * 2) incoming messages will all be passed to this routine...
     * 3) accesses to directories/tags will only be passed to this routine
     *    if they were requested by this interface
     ************************/

    switch (ist->stateNo) {
    case -1: 
      { /* we are already done with this transaction, so we allow
	 * any more upstream messages destined for it to move upwards.
	 * On the other hand, we do *not* accept any more downstream or
	 * any other kind...
	 */
	if (ev->etype != LSE_cache_eventtype_Msg) 
	  demand_error(e,t,ino,LSE_cache_error_InconsistentEvent);
	else if (ev->d.Msg.dir) 
	  demand_error(e,t,ino,LSE_cache_error_InconsistentMsg);
	else e->interfaces[lowerInterface-1].handler(e,t,ev);
	return;
      };
    case 0: goto state0;
    case 1: goto state1;
    case 2: goto state2;
    case 3: goto state3;
    case 4: goto state4;
    case 5: goto state5;
    case 6: goto state6;
    case 7: goto state7;
    default:
      demand_error(e,t,ino,LSE_cache_error_UnknownState);
      return;
    }


    {
      /************ Initial state *******************/
    state0:

      if (ev->etype != LSE_cache_eventtype_Msg) {
	demand_error(e,t,ino,LSE_cache_error_InconsistentEvent);
	return;
      }

      mtype = ev->d.Msg.m->type;

      if (!ev->d.Msg.dir) {
      
	/* write requests coming upstream are turned into invalidate
	 * requests when the parametrs say that invalidates are combined
	 * with writes.  All others are simply forwarded to the engine
	 */
	if (!(mtype == InvalReq ||
	      (mtype == WrReq && isCoherent(*(ev->d.Msg.m)) && 
	       e->interfaces[ino].parms.Snoop.SeeInWriteAsInval))) {
	  ist->stateNo = -1;
	  e->interfaces[lowerInterface-1].handler(e,t,ev);
	  return;
	}

	/* start up upstream request by checking the cache; all requests
	 * which have made it this far will check the cache!
	 */

	t->aliveCnt++;
	t->origMsg.type = InvalReq;
	t->origMsg.flags = 0;

	demand_readlinestate(t, lowerInterface, LSE_cache_stateaccess_Tags, 
			     -1, -1);
	WAIT_StateRead(4);

	demand_finish(t, lowerInterface);
	if (lower_requiresRetry(e,t)) {
	  demand_send_lReply(e, lowerInterface, t, form_reply(t,&nm,Retry),-1,
			     -1);
	} else e->interfaces[lowerInterface-1].handler(e, t, &nullEv);

      } else {

	if (mtype == WrReq) {

	  /* The basic idea here is:
	   * 1) send invalidate request
	   * 2) wait for all invalidate acks
	   * 3) report coherence met (so can release data)
	   * 4) send write request
	   * 5) wait for write request
	   *
	   * Options:
	   * - When CombineOutWriteInval is set, the invalidate and write request
	   *   are sent forward as one request.
	   * - When WaitForInvalidateAck is cleared, no need to wait for 
	   *   any invalidate acks to arrive
	   *
	   */

	  t->aliveCnt++;
	
	  ist->savedMsg = *ev->d.Msg.m; /* save for eventual reply */
	
	  /* form invalidate */
	  nm = *ev->d.Msg.m;
	  if (!e->interfaces[ino].parms.Snoop.CombineOutWriteInval) {
	    nm.type = InvalReq;
	    nm.flags = 0;
	  }

	send_invalidates_for_write:
	  /* send invalidations... */	  
	
	  /* need to hear from others on bus, but not ourselves or the
	   * destination (unless combined...) */
	  if (!e->interfaces[ino].parms.Snoop.CombineOutWriteInval)
	    ist->msgCount = e->interfaces[ino].parms.I.numUnits-2;
	  else
	    ist->msgCount = e->interfaces[ino].parms.I.numUnits-1;

	  if (ist->msgCount) {
	    ensure_todo_space(t, DEFAULT_SPACE);
	    demand_send_ServReq(e, ino, t, &nm, -1, -1);
	    /* wait for invalidations... */
	    do {
	      WAIT_lInvalAck_lWrAck(1);
	      ist->msgCount--;

	      if (ev->d.Msg.m->reply == NACK) { 
		/* Wait for everyone and then nack back... */
		while (ist->msgCount) {	
		  WAIT_lInvalAck_lWrAck(2);
		  ist->msgCount--;
		}
		/* NACK backwards... */
		nm = ist->savedMsg;
		nm.reply = NACK;
		demand_finish(t,ino);
		demand_send_uReply(e, ino, t, &nm, -1, -1);
		return;
	      } else if (ev->d.Msg.m->reply == Retry) {
		/* wait for everyone and try again... */
		while (ist->msgCount) {
		  WAIT_lInvalAck_lWrAck(3);
		  ist->msgCount--;
		}
		nm = ist->savedMsg;
		if (!e->interfaces[ino].parms.Snoop.CombineOutWriteInval) {
		  nm.type = InvalReq;
		  nm.flags = 0;
		}
		goto send_invalidates_for_write;
	      }

	      if (ev->d.Msg.m->type == WrAck) 
		ist->flags = isNoCache(*(ev->d.Msg.m));
	    } while (ist->msgCount > 0);
	  }

	  /* make data visible now that acks are in place... */
	  // demand_serialized(t, mainEngine);

	  /* now actually send the write if not already done, as the *
	   * value can be visible...  the interface is done and any
	   * remaining reply can just pass through */
	
	  demand_finish(t,ino);
	  if (!e->interfaces[ino].parms.Snoop.CombineOutWriteInval)
	    demand_send_ServReq(e, ino, t, &ist->savedMsg, -1, -1);
	  else { /* must set up the reply */
	    nm.type = WrAck;
	    nm.reply = OK;
	    nm.flags = ist->flags ? NoCache : 0;
	    demand_send_uReply(e, ino, t, &nm, -1, -1);
	  }
	
	} /* wrReq */
      
	else if (mtype == RdReq) { /* these will be read-to-own */

	  /* The basic idea here is:
	   * 1) send invalidate request
	   * 2) wait for all invalidate acks
	   * 3) report coherence met (so can release data)
	   * 4) send read request
	   * 5) allow ack to pass-through
	   *
	   * Options:
	   * - When WaitForInvalidateAck is cleared, no need to wait for 
	   *   any invalidate acks to arrive
	   *
	   */

	  t->aliveCnt++;
	
	  ist->savedMsg = *ev->d.Msg.m; /* save for eventual reply */
	
	  /* form invalidate */
	  nm = *ev->d.Msg.m;
	  nm.type = InvalReq;
	  nm.flags = 0;

	send_invalidates_for_read:
	  /* send invalidations... */	  
	
	  /* need to hear from others on bus, but not ourselves or the
	   * destination (unless combined...) */
	  ist->msgCount = e->interfaces[ino].parms.I.numUnits-2;

	  if (ist->msgCount) {
	    ensure_todo_space(t, DEFAULT_SPACE);
	    demand_send_ServReq(e, ino, t, &nm, -1, -1);
	    /* wait for invalidations... */
	    do {
	      WAIT_lInvalAck_lWrAck(5);
	      ist->msgCount--;

	      if (ev->d.Msg.m->reply == NACK) { 
		/* Wait for everyone and then nack back... */
		while (ist->msgCount) {	
		  WAIT_lInvalAck_lWrAck(6);
		  ist->msgCount--;
		}
		/* NACK backwards... */
		nm = ist->savedMsg;
		nm.reply = NACK;
		demand_finish(t,ino);
		demand_send_uReply(e, ino, t, &nm, -1, -1);
		return;
	      } else if (ev->d.Msg.m->reply == Retry) {
		/* wait for everyone and try again... */
		while (ist->msgCount) {
		  WAIT_lInvalAck_lWrAck(7);
		  ist->msgCount--;
		}
		nm = ist->savedMsg;
		nm.type = InvalReq;
		nm.flags = 0;
		goto send_invalidates_for_read;
	      }
	    } while (ist->msgCount > 0);
	  }

	  /* make data visible now that acks are in place... */
	  // demand_serialized(t, mainEngine);

	  /* now actually send the read.  The interface is done and any
	   * remaining reply can just pass through */
	
	  demand_finish(t,ino);
	  demand_send_ServReq(e, ino, t, &ist->savedMsg, -1, -1);
	
	} /* RdReq */
      

      } /* if downstream */

    }

    return;
  }

  /********************* central directory ********************/

  static bool 
  centraldir_lower_msg_starts_transaction(LSE_cache_engine_t *e,
					  const LSE_cache_msg_t *m,
					  int dir, bool snooped) {
    if (!dir) return e->interfaces[lowerInterface-1].willStart(e,m,dir,snooped);
    else return false;
  }

  static bool 
  centraldir_lower_intercept_msg(LSE_cache_engine_t *e,
				 LSE_cache_tstate_t *t) {
    return false;
  }

  static void
  centraldir_lower_handle_event(LSE_cache_engine_t *e,
				LSE_cache_tstate_t *t,
				const LSE_cache_event_t *ev) {

    LSE_cache_msg_t nm;
    LSE_cache_istate_t *ist= &t->iState[lowerInterface];
    int ino = lowerInterface; /* needed for wait macros */

    /********************* 
     * Invariants: 
     * 1) outgoing messages will only be passed to this routine if they are 
     *    "intercepted"
     * 2) incoming messages will all be passed to this routine...
     * 3) accesses to directories/tags will only be passed to this routine
     *    if they were requested by this interface
     ************************/

    switch (ist->stateNo) {
    case -1: 
      { /* we are already done with this transaction, so we allow
	 * any more upstream messages destined for it to move upstream
	 * On the other hand, we do *not* accept any more upstream or
	 * any other kind...
	 */
	if (ev->etype != LSE_cache_eventtype_Msg) 
	  demand_error(e,t,lowerInterface,LSE_cache_error_InconsistentEvent);
	else if (ev->d.Msg.dir) 
	  demand_error(e,t,lowerInterface,LSE_cache_error_InconsistentMsg);
	else e->interfaces[lowerInterface-1].handler(e,t,ev);
	return;
      };
    case 0: goto state0;
    case 1: goto state1;
    default:
      demand_error(e,t,lowerInterface,LSE_cache_error_UnknownState);
      return;
    }


    {
      LSE_cache_msgtype_t mtype;
      /************ Initial state *******************/
    state0:

      if (ev->etype != LSE_cache_eventtype_Msg) {
	demand_error(e,t,lowerInterface,LSE_cache_error_InconsistentEvent);
	return;
      }

      /* we should not have intercepted anything going downstream */
      if (ev->d.Msg.dir) {
	demand_error(e,t,lowerInterface,LSE_cache_error_InconsistentMsg);
	return; 
      }
      /* replies coming back need to be passed directly through, OR
       * we need to intercept the request on its way up and have our own
       * states.  It is cheaper to pass directly if we have nothing special
       * to be doing...
       */
      mtype = ev->d.Msg.m->type;
      if (mtype != LSE_cache_msgtype_InvalReq) {
	ist->stateNo = -1;
	e->interfaces[lowerInterface-1].handler(e,t,ev);
	return;
      }

      /* start up upstream request by checking the cache */

      if (mtype == LSE_cache_msgtype_InvalReq || isCacheable(*(ev->d.Msg.m))) {
	t->aliveCnt++;
	demand_readlinestate(t, lowerInterface, LSE_cache_stateaccess_Tags,
			     -1, -1);
	WAIT_StateRead(1);

	if (lower_requiresRetry(e,t)) {
	  demand_send_lReply(e, lowerInterface, t, form_reply(t,&nm,Retry),
			     -1, -1);
	  demand_finish(t, lowerInterface);
	  return;
	}
	demand_finish(t, lowerInterface);
	e->interfaces[lowerInterface-1].handler(e, t, &nullEv);
      } else {
	t->lineState = Invalid;
	ist->stateNo = -1;
	e->interfaces[lowerInterface-1].handler(e, t, &nullEv);
      }
    }
    return;

  }

  static void setup_lower_interface(LSE_cache_engine_t *e) {
    LSE_cache_interface_t *f 
      = (LSE_cache_interface_t *)&e->interfaces[lowerInterface];
    switch (f->parms.I.style) {
    case LSE_cache_interfacestyle_Null: 
      f->handler = null_lower_handle_event;
      f->willStart = null_lower_msg_starts_transaction;
      f->interceptMsg = null_lower_intercept_msg;
      break;
    case LSE_cache_interfacestyle_Snoop:
      f->handler = snoop_lower_handle_event;
      f->willStart = snoop_lower_msg_starts_transaction;
      f->interceptMsg = snoop_lower_intercept_msg;
      break;
    case LSE_cache_interfacestyle_CentralDirectory:
      f->handler = centraldir_lower_handle_event;
      f->willStart = centraldir_lower_msg_starts_transaction;
      f->interceptMsg = centraldir_lower_intercept_msg;
      break;
    default:
      fprintf(stderr,"Unknown cache interface style\n");
      break;
    }
  }

  /*********************************************************************
   * Upper interface protocols
   ********************************************************************/

  /********************* Null ******************/

  static bool
  null_upper_msg_starts_transaction(LSE_cache_engine_t *e,
				    const LSE_cache_msg_t *m,
				    int dir, bool snooped) {
    if (dir) return e->interfaces[upperInterface+1].willStart(e,m,dir,snooped);
    else return false;
  }

  static bool 
  null_upper_intercept_msg(LSE_cache_engine_t *e,
			   LSE_cache_tstate_t *t) {

    return false;
  }

  static void
  null_upper_handle_event(LSE_cache_engine_t *e, 
			  LSE_cache_tstate_t *t,
			  const LSE_cache_event_t *ev) {

    LSE_cache_msg_t nm;
    LSE_cache_istate_t *ist= &t->iState[upperInterface];
    int ino = upperInterface; /* needed for wait macros */

    /********************* 
     * Invariants: 
     * 1) outgoing messages will only be passed to this routine if they are 
     *    "intercepted"
     * 2) incoming messages will all be passed to this routine...
     * 3) accesses to directories/tags will only be passed to this routine
     *    if they were requested by this interface
     ************************/

    switch (ist->stateNo) {
    case -1: 
      { /* we are already done with this transaction, so we allow
	 * any more downstream messages destined for it to move downwards
	 * On the other hand, we do *not* accept any more upstream or
	 * any other kind...
	 */
	if (ev->etype != LSE_cache_eventtype_Msg) 
	  demand_error(e,t,upperInterface,LSE_cache_error_InconsistentEvent);
	else if (!ev->d.Msg.dir) 
	  demand_error(e,t,upperInterface,LSE_cache_error_InconsistentMsg);
	else e->interfaces[upperInterface+1].handler(e,t,ev);
	return;
      };
    case 0: goto state0;
    case 1: goto state1;
    default:
      demand_error(e,t,upperInterface,LSE_cache_error_UnknownState);
      return;
    }


    {
      LSE_cache_msgtype_t mtype;
      /************ Initial state *******************/
    state0:

      if (ev->etype != LSE_cache_eventtype_Msg) {
	demand_error(e,t,upperInterface,LSE_cache_error_InconsistentEvent);
	return;
      }

      /* we should not have intercepted anything going upstream */
      if (!ev->d.Msg.dir) {
	demand_error(e,t,upperInterface,LSE_cache_error_InconsistentMsg);
	return; 
      }
      /* replies coming back need to be passed directly through, OR
       * we need to intercept the request on its way up and have our own
       * states.  It is cheaper to pass directly if we have nothing special
       * to be doing...
       */
      mtype = ev->d.Msg.m->type;
      if (mtype == LSE_cache_msgtype_InvalAck) {
	ist->stateNo = -1;
	e->interfaces[upperInterface+1].handler(e,t,ev);
	return;
      }

      /* start up downstream request by checking the cache */

      if (mtype == InvalReq || mtype==CastOutReq || 
	  isCacheable(*(ev->d.Msg.m))) {
	t->aliveCnt++;
	demand_readlinestate(t, upperInterface, LSE_cache_stateaccess_Tags,
			     -1, -1);
	WAIT_StateRead(1);

	if (upper_requiresRetry(e,t)) {
	  demand_send_uReply(e, upperInterface, t, form_reply(t,&nm,Retry),
			     -1, -1);
	  demand_finish(t, upperInterface);
	  return;
	}
	demand_finish(t, upperInterface);
	e->interfaces[upperInterface+1].handler(e, t, &nullEv);
      } else {
	t->lineState = Invalid;
	ist->stateNo = -1;
	e->interfaces[upperInterface+1].handler(e, t, &nullEv);
      }
    }
    return;
  }

  /********************* Snoop *****************/

  static bool 
  snoop_upper_msg_starts_transaction(LSE_cache_engine_t *e, 
				     const LSE_cache_msg_t *m,
				     int dir, bool snooped) {
    if (dir) return e->interfaces[upperInterface+1].willStart(e,m,dir,snooped);
    else return false;
  }

  static bool 
  snoop_upper_intercept_msg(LSE_cache_engine_t *e,
			    LSE_cache_tstate_t *t) {

    LSE_cache_event_t ev;
    LSE_cache_msg_t m;

    /* intercept outgoing invalidate requests if we need to wait for 
     * acknowledgements
     */
    if (!t->todo[t->numToDo-1].work.Send.dir &&
	t->todo[t->numToDo-1].work.Send.newMsg.type == InvalReq) {
      t->numToDo--;
      m = t->todo[t->numToDo].work.Send.newMsg;

      ev.etype = LSE_cache_eventtype_Msg;
      ev.d.Msg.m = &m;
      ev.d.Msg.dir = t->todo[t->numToDo].work.Send.dir;
      ev.d.Msg.snooped = false;
      e->interfaces[upperInterface].handler(e,t,&ev);
      return true; /* handled it... */
    }
    return false;
  }

  static void
  snoop_upper_handle_event(LSE_cache_engine_t *e, 
			   LSE_cache_tstate_t *t,
			   const LSE_cache_event_t *ev) {

    LSE_cache_msg_t nm;
    LSE_cache_istate_t *ist = &t->iState[upperInterface];
    int ino = upperInterface; /* needed for wait macros */
    LSE_cache_msgtype_t mtype;

    /********************* 
     * Invariants: 
     * 1) outgoing messages will only be passed to this routine if they are 
     *    "intercepted"
     * 2) incoming messages will all be passed to this routine...
     * 3) accesses to directories/tags will only be passed to this routine
     *    if they were requested by this interface
     ************************/

    switch (ist->stateNo) {
    case -1: 
      { /* we are already done with this transaction, so we allow
	 * any more downstream messages destined for it to move downwards
	 * On the other hand, we do *not* accept any more upstream or
	 * any other kind...
	 */
	if (ev->etype != LSE_cache_eventtype_Msg) 
	  demand_error(e,t,ino,LSE_cache_error_InconsistentEvent);
	else if (!ev->d.Msg.dir) 
	  demand_error(e,t,ino,LSE_cache_error_InconsistentMsg);
	else e->interfaces[upperInterface+1].handler(e,t,ev);
	return;
      };
    case 0: goto state0;
    case 1: goto state1;
    case 2: goto state2;
    case 3: goto state3;
    case 4: goto state4;
    default:
      demand_error(e,t,ino,LSE_cache_error_UnknownState);
      return;
    }


    {
      /************ Initial state *******************/
    state0:

      if (ev->etype != LSE_cache_eventtype_Msg) {
	demand_error(e,t,ino,LSE_cache_error_InconsistentEvent);
	return;
      }

      if (ev->d.Msg.dir) {

	/* start up downstream request by checking the cache */
	mtype = ev->d.Msg.m->type;

	if (mtype == InvalReq || mtype==CastOutReq || 
	    isCacheable(*(ev->d.Msg.m))) {
	  t->aliveCnt++;
	  demand_readlinestate(t, upperInterface, LSE_cache_stateaccess_Tags,
			       -1, -1);
	  WAIT_StateRead(4);

	  if (upper_requiresRetry(e,t)) {
	    demand_send_uReply(e, upperInterface, t, form_reply(t,&nm,Retry),
			       -1, -1);
	    demand_finish(t, upperInterface);
	    return;
	  }
	  demand_finish(t, upperInterface);
	  e->interfaces[upperInterface+1].handler(e, t, &nullEv);
	} else {
	  t->lineState = Invalid;
	  ist->stateNo = -1;
	  e->interfaces[upperInterface+1].handler(e, t, &nullEv);
	}

      }
      else {
	/* only invalidate requests are intercepted in the upstream
	 * direction.
	 */

	if (ev->d.Msg.m->type == InvalReq) {

	  /* The basic idea here is:
	   * 1) send invalidate request
	   * 2) wait for all invalidate acks
	   * 3) report coherence met (so can release data)
	   * 4) send write request
	   * 5) wait for write request
	   *
	   * Options:
	   * - When CombineOutWriteInval is set, the invalidate and write request
	   *   are sent forward as one request.
	   * - When WaitForInvalidateAck is set, 
	   */

	  t->aliveCnt++;
	  ist->flags = 0;
	
	  ist->savedMsg = *ev->d.Msg.m; /* save for eventual reply */
	
	  ist->msgCount = e->interfaces[ino].parms.I.numUnits-1;
	
	send_invalidates_for_inval:
	  ensure_todo_space(t, DEFAULT_SPACE);
	  demand_send(e, ino, t, &ist->savedMsg, 0, 0, -1, false, -1, -1);

	  /* wait for invalidations... */
	  do {
	    WAIT_uInvalAck(1);
	    ist->msgCount--;

	    if (ev->d.Msg.m->reply == NACK) { 
	      /* Wait for everyone and then nack back... */
	      while (ist->msgCount) {	
		WAIT_uInvalAck(2);
		ist->msgCount--;
	      }
	      /* NACK backwards... */
	      nm.type = InvalAck;
	      nm.reply = NACK;
	      demand_finish(t,ino);
	      demand_send(e, ino, t, &nm, 1, 1, t->sendID, false, -1, -1);
	      return;
	    } else if (ev->d.Msg.m->reply == Retry) {
	      /* wait for everyone and try again... */
	      while (ist->msgCount) {
		WAIT_uInvalAck(3);
		ist->msgCount--;
	      }
	      goto send_invalidates_for_inval;
	    } else if (isWithData(*(ev->d.Msg.m))) ist->flags = 1;

	  } while (ist->msgCount > 0);

	  /* now send the invalidate acknowledge back */
	  nm.type = InvalAck;
	  nm.reply = OK;
	  nm.flags = ist->flags ? WithData : 0;
	  demand_finish(t,ino);
	  demand_send(e, ino, t, &nm, 1, 1, t->sendID, false, -1, -1);
	
	} /* invalReq */
      
      } /* if upstream */

    }

    return;
  }

  /*********************** centraldir *********************/

  static bool 
  centraldir_upper_msg_starts_transaction(LSE_cache_engine_t *e, 
					  const LSE_cache_msg_t *m,
					  int dir, bool snooped) {
    if (dir) {
      if (m->type == LSE_cache_msgtype_CastOutReq) return true;
      return e->interfaces[upperInterface+1].willStart(e,m,dir,snooped);
    } else return false;
  }

  static bool 
  centraldir_upper_intercept_msg(LSE_cache_engine_t *e,
				 LSE_cache_tstate_t *t) {

    LSE_cache_msg_t m;
    LSE_cache_event_t ev;
    int currtodo = t->numToDo-1;

    /* do not start any new transactions downstream */
    if (t->todo[currtodo].work.Send.dir) return false;

    /* upstream invalidations start new work; anything which is not in an
     * initial or finished state for the interface is continuing old work.
     * In either case, we need to handle it.
     */

    if (t->todo[currtodo].work.Send.newMsg.type == InvalReq ||
	t->iState[upperInterface].stateNo > 0) {

      t->numToDo--;
      m = t->todo[currtodo].work.Send.newMsg;
    
      ev.etype = LSE_cache_eventtype_Msg;
      ev.d.Msg.m = &m;
      ev.d.Msg.dir = t->todo[currtodo].work.Send.dir;
      ev.d.Msg.snooped = false;
      /* ev.d.Msg.replyIDtouse = t->todo[currtodo].work.replyIDtouse; */
      e->interfaces[upperInterface].handler(e,t,&ev);
      return true; /* handled it... */
    } else return false;
  }

  static void
  centraldir_upper_handle_event(LSE_cache_engine_t *e, 
				LSE_cache_tstate_t *t,
				const LSE_cache_event_t *ev) {
    LSE_cache_msg_t nm;
    LSE_cache_istate_t *ist = &t->iState[upperInterface];
    int ino = upperInterface;
    LSE_cache_msgtype_t mtype;
    LSE_cache_direntry_t *dirEntP = &t->dState[upperInterface];

    /*********************************
     * Invariants:
     * 1) All downward-flowing messages will be passed to this routine
     * 2) All upward-flowing messages will only come here if intercepted.
     * 3) Accesses to directories will only be passed to this routine if they
     * were requested by this interface
     *********************************/

    switch (ist->stateNo) {
    case -1:
      { /* we are already done with this transaction, so we allow
	 * any more downstream messages destined for it to move downwards
	 * On the other hand, upstream messages start us doing something
	 * new for the transaction!
	 */
	if (ev->etype != LSE_cache_eventtype_Msg) 
	  demand_error(e,t,ino,LSE_cache_error_InconsistentEvent);
	else if (!ev->d.Msg.dir) goto state0;
	else e->interfaces[upperInterface+1].handler(e,t,ev);
	return;
      }
    case 0: goto state0;
    case 1: goto state1;
    case 2: goto state2;
    case 3: goto state3;
    case 4: goto state4;
    case 5: goto state5;
    case 6: goto state6;
    case 7: goto state7;
    case 8: goto state8;
    case 9: goto state9;
    case 10: goto state10;
    case 11: goto state11;
    case 12: goto state12;
    case 13: goto state13;
    case 14: goto state14;
    case 15: goto state15;
    default:
      if (ist->stateNo < 0) return;
      demand_error(e,t,ino,LSE_cache_error_UnknownState);
      return;
    }


    /************ Initial state *******************/
  state0:
    if (ev->etype != LSE_cache_eventtype_Msg) {
      demand_error(e,t,ino,LSE_cache_error_InconsistentEvent);
      return;
    }

    if (!ev->d.Msg.dir) goto upstream_msg_state0;

    /************* downstream messages *******************/
  
    /* start up downstream request by checking the cache */
    mtype = ev->d.Msg.m->type;

    if (mtype == InvalReq || mtype==CastOutReq || isCacheable(*(ev->d.Msg.m))) {
      t->aliveCnt++;
      /* line state for free */
      demand_readdirectory(t, upperInterface, false, -1, -1);
      WAIT_StateRead(1);

      if (upper_requiresRetry(e,t)) {
	demand_finish(t, upperInterface);
	demand_send_uReply(e, upperInterface, t, form_reply(t,&nm,Retry),-1,-1);
	return;
      }

    } else {
      t->lineState = Invalid;
    }

    {
    
      if (is_RdReq(t)) {

	if (!isCacheable(t->origMsg)) {

	  /* read requests which bypass the cache.  Just send them on to the
	   * engine.
	   *
	   * Covers:
	   * uRdReq.!Own.!Coh.!Cach.!NA:
	   * uRdReq.!Own.!Coh.!Cach. NA:
	   * uRdReq.!Own. Coh.!Cach.!NA:
	   * uRdReq.!Own. Coh.!Cach. NA:
	   * uRdReq. Own.!Coh.!Cach.!NA:
	   * uRdReq. Own.!Coh.!Cach. NA:
	   * uRdReq. Own. Coh.!Cach.!NA:
	   * uRdReq. Own. Coh.!Cach. NA:
	   *
	   */
	  ist->stateNo = -1;
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	  return;
	}

	if (t->lineState == Invalid) {

	  if (isNoAlloc(t->origMsg)) {
	    demand_finish(t, ino);
	    e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	    return;
	  }

	  /* missed the cache */
	  ist->atRisk = true;
	  ist->stateNo = 2;
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);

	  /* We need to look for both lRdAck and for notification from
	   * an upstream invalidation.  This is because the upstream
	   * path must mark when it is safe to write the directory.
	   */

	  ist->flags2 = t->numToDo;
	  demand_lock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
		      -1, -1);

#define NCBit 16
#define FoundNotifyBit 32
	  do {
	    WAIT(2);
	    if (ev->etype == LSE_cache_eventtype_Notify) {
	      ist->flags = FoundNotifyBit;
	      continue;
	    } else if (ev->etype != LSE_cache_eventtype_Msg) 
	      do_error(e,t,ino,LSE_cache_error_InconsistentEvent)
	      else if (ev->d.Msg.dir || ev->d.Msg.m->type != RdAck) 
		do_error(e,t,ino,LSE_cache_error_InconsistentMsg)
		  break;
	  } while (1);

	  ist->flags |= ev->d.Msg.m->reply | (isNoCache(*(ev->d.Msg.m)) ? NCBit : 0);
	  if (ev->d.Msg.m->reply == OK && !isNoCache(*(ev->d.Msg.m)) && 
	      !ist->invalidated) {

	    if (t->hasVictim && !(ist->flags & FoundNotifyBit)) {
	      /* do we need a notification? */
	      WAIT_Notify(3);
	    }

	    /* NOTE: we do not keep our copy of the directory up to date 
	     * in this process 
	     */
	    if (!t->wasNotCached && !ist->invalidated && !t->wasNotCached) {
	      demand_dirsetonlybit_flags(t, ino, t->sendID,
					 isToOwn(t->origMsg) ? OwnedBit : 0,
					 -1, -1);
	    }

	  }

	  demand_unlock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
			ist->flags2, -1);
	  if (ist->invalidated || (ist->flags & NCBit) || t->wasNotCached) { 
	    /* cannot cache the result */ 
	    demand_send_uReply(e, ino, t,
			       form_nc_reply(t,&nm,flag2reply(ist->flags & 15)),
			       -1, -1);
	  } else {
	    demand_send_uReply(e, ino, t, 
			       form_reply(t,&nm,flag2reply(ist->flags & 15)), 
			       -1, -1);
	  }
	  demand_may_finish(t, ino);
	  ist->stateNo = -1000; /* special state for at-risk finish */
	  return;
#undef NCBit
#undef FoundNotifyBit

	} else if (isToOwn(t->origMsg)) {
	  /* if we need to own the line, we need
	   * to ensure that no one else owns it anymore...
	   */
	  int nu, i;
	  nu = e->interfaces[ino].parms.I.numUnits;
	  ist->msgCount=0;

	  // TODO: lock linking and correct invalidation stuff
	
	  ensure_todo_space(t,nu + DEFAULT_SPACE);
	  demand_lock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
		      -1, -1);
	  ist->flags = dirEntP->d.Central[t->sendID/32] & (1<<(t->sendID % 32));

	  for (i=1;i<nu;i++) 
	    if (i != t->sendID && (dirEntP->d.Central[i/32] & (1<<(i % 32)))) {
	      /* if in directory, invalidate it */
	      nm.type = InvalReq;
	      nm.flags = 0;
	      demand_send(e, ino, t, &nm, 0, 0, i, true, -1, -1);
	      ist->msgCount++;
	    }

	  while (ist->msgCount) {
	    WAIT_uInvalAck(11);
	    if (ev->d.Msg.m->reply == OK) {
	      if (isWithData(*(ev->d.Msg.m))) {
		demand_writedata(t,mainEngine,2,-1,-1);
	      }
	      ist->msgCount--;
	    }
	    else {
	      nm.type = InvalReq;
	      nm.flags = 0;
	      ensure_todo_space(t, DEFAULT_SPACE);
	      demand_send(e, ino, t, &nm, 0, 0, ev->d.Msg.sendID, true,
			  -1, -1);
	    }
	  }
	  ist->stateNo = 12;
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	  WAIT_lRdAck(12);

	  if (ist->invalidated) {
	  } else if (ev->d.Msg.m->reply == OK && !isNoCache(*(ev->d.Msg.m))) {
	    demand_dirsetonlybit_flags(t, ino, t->sendID, OwnedBit, -1, -1);
	  } else {
	    if (ist->flags) {
	      demand_dirsetonlybit_flags(t, ino, t->sendID, 0, -1, -1);
	    } else 
	      demand_dirclearall_flags(t, ino, 0, -1, -1);
	  }
	  if (!ist->invalidated) {
	    demand_unlock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
			  ist->flags2, -1);
	    demand_send_uReply(e, ino, t, ev->d.Msg.m, -1, -1);
	  } else {
	    demand_send_uReply(e, ino, t, 
			       form_nc_reply(t,&nm,ev->d.Msg.m->reply), 
			       -1, -1);
	  }
	  demand_may_finish(t, ino);	
	  ist->stateNo = -1000;

	} else if (dirEntP->flags & OwnedBit) {

	  /* line is owned by someone...; need to convince them to write it
	   * back, but they may be able to keep it...
	   */

	  int nu, i;
	  nu = e->interfaces[ino].parms.I.numUnits;
	  ist->msgCount=0;

	  // TODO: lock linking and correct invalidation stuff
	
	  ensure_todo_space(t,nu + DEFAULT_SPACE);
	  demand_lock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
		      -1, -1);
	  ist->flags = dirEntP->d.Central[t->sendID/32] & (1<<(t->sendID % 32));

	  for (i=1;i<nu;i++) 
	    if (i != t->sendID && (dirEntP->d.Central[i/32] & (1<<(i % 32)))) {
	      /* if in directory, invalidate it */
	      nm.type = InvalReq;
	      nm.flags = Shareable;
	      demand_send(e, ino, t, &nm, 0, 0, i, true, -1, -1);
	      ist->msgCount++;
	    }

	  while (ist->msgCount) {
	    WAIT_uInvalAck(14);
	    if (ev->d.Msg.m->reply == OK) {
	      if (isWithData(*(ev->d.Msg.m))) {
		demand_writedata(t,mainEngine,2,-1,-1);
	      }
	      ist->msgCount--;
	    }
	    else {
	      nm.type = InvalReq;
	      nm.flags = Shareable;
	      ensure_todo_space(t, DEFAULT_SPACE);
	      demand_send(e, ino, t, &nm, 0, 0, ev->d.Msg.sendID, true,
			  -1, -1);
	    }
	  }
	  ist->stateNo = 13;
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	  WAIT_lRdAck(13);

	  if (ist->invalidated) {
	  } else if (ev->d.Msg.m->reply == OK && !isNoCache(*(ev->d.Msg.m))) {
	    demand_dirsetbit_flags(t, ino, t->sendID, 0, -1, -1);
	  } else {
	    demand_dirwriteflags(t, ino, 0, -1, -1);
	  }
	  if (!ist->invalidated) {
	    demand_unlock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
			  ist->flags2, -1);
	    demand_send_uReply(e, ino, t, ev->d.Msg.m, -1, -1);
	  } else {
	    demand_send_uReply(e, ino, t, 
			       form_nc_reply(t,&nm,ev->d.Msg.m->reply), 
			       -1, -1);
	  }
	  demand_may_finish(t, ino);	
	  ist->stateNo = -1000;
	  return;

	} else {

	  /* line is in the cache and not owned by anyone; can simply add to the
	   * list.  Only question is whether to serve from another cache.
	   */

	  int nu, i;

	  // TODO: option to serve the line from another cache...
	  nu = e->interfaces[ino].parms.I.numUnits;

	  if (e->interfaces[ino].parms.CentralDir.BetweenCacheTransfers) {
	    for (i=0;i<nu;i++) 
	      if (dirEntP->d.Central[i/32] & (1<<(i % 32)))
		break;
	  } else i = nu;

	  ist->atRisk = true;

	  if (i < nu) { /* hit; serve from other cache */
	  } else {

	    ist->flags2 = t->numToDo;
	    demand_lock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
			-1, -1);

	    ist->stateNo = 4;
	    e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	    WAIT_lRdAck(4);

	    if (ev->d.Msg.m->reply == OK && !isNoAlloc(t->origMsg) && 
		!isNoCache(*(ev->d.Msg.m)) && !ist->invalidated) {
	      demand_dirsetbit(t, ino, t->sendID, -1, -1);
	    }
	  
	    demand_unlock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
			  ist->flags2, -1);
	    if (ist->invalidated) { /* cannot cache the result */
	      demand_send_uReply(e, ino, t, 
				 form_nc_reply(t,&nm,ev->d.Msg.m->reply), 
				 -1, -1);
	    } else
	      demand_send_uReply(e, ino, t, ev->d.Msg.m, -1, -1);

	    demand_may_finish(t, ino);
	    ist->stateNo = -1000; /* same as state -1000, so just go there */
	  }
	  return;
	}

      } /* read requests */
      else if (is_WrReq(t)) {
      
	if (!isCacheable(t->origMsg) && !isCoherent(t->origMsg)) {

	  /* write requests which bypass the cache and are not coherent. 
	   *
	   * Covers:
	   * uWrReq.!Coh.!Cach.!WT. NA:
	   * uWrReq.!Coh.!Cach.!WT.!NA:
	   * uWrReq.!Coh.!Cach. WT. NA:
	   * uWrReq.!Coh.!Cach. WT.!NA:
	   *
	   */
	  ist->stateNo = -1;
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	  return;
	}

	if (!isCacheable(t->origMsg)) {
	  int i, nu;

	  /* write requests which bypass the cache but try to remain coherent.
	   * In this case, the directory cannot be found, so we have to
	   * explicitly broadcast to all.
	   *
	   * Covers:
	   * uWrReq. Coh.!Cach.!WT. NA:
	   * uWrReq. Coh.!Cach.!WT.!NA:
	   * uWrReq. Coh.!Cach. WT. NA:
	   * uWrReq. Coh.!Cach. WT.!NA:
	   *
	   */

	  t->aliveCnt++;

	  /* We have been asked to invalidate the upper levels */
	  ist->msgCount = 0;

	  nu = e->interfaces[ino].parms.I.numUnits;

	  ensure_todo_space(t,nu + DEFAULT_SPACE);
	  ist->flags2 = t->numToDo;

	  for (i=1;i<nu;i++) 
	    if (i != t->sendID) {
	      /* if in directory, invalidate it... */
	      nm.type = InvalReq;
	      nm.flags = 0;
	      demand_send(e, ino, t, &nm, 0, 0, i, true, -1, -1);
	      ist->msgCount++;
	    }

	  while (ist->msgCount) {
	    WAIT_uInvalAck(5);
	    if (ev->d.Msg.m->reply == OK) ist->msgCount--;
	    else {
	      nm.type = InvalReq;
	      nm.flags = 0;
	      ensure_todo_space(t, DEFAULT_SPACE);
	      demand_send(e, ino, t, &nm, 0, 0, ev->d.Msg.sendID, true,
			  -1, -1);
	    }
	  }
	
	  demand_finish(t, ino);
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	  return;
	} /* not cacheable */

	if (t->lineState == Invalid) {

	  /* write requests which miss
	   *
	   * Covers: (when owner does not exist)
	   * uWrReq.!Coh. Cach.!WT. NA:
	   * uWrReq.!Coh. Cach.!WT.!NA:
	   * uWrReq.!Coh. Cach. WT. NA:
	   * uWrReq.!Coh. Cach. WT.!NA:
	   * uWrReq. Coh. Cach.!WT.!NA:
	   * uWrReq. Coh. Cach. WT. NA:
	   * uWrReq. Coh. Cach. WT.!NA:
	   * uWrReq. Coh. Cach.!WT. NA:
	   *
	   */

	  if (isNoAlloc(t->origMsg)) {
	    demand_finish(t, ino);
	    e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	    return;
	  }

	  /* missed the cache */
	  ist->atRisk = true;
	  ist->stateNo = 7;
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);

	  ist->flags2 = t->numToDo;
	  demand_lock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
		      -1, -1);

#define NCBit 16
#define FoundNotifyBit 32
	  do {
	    WAIT(7);
	    if (ev->etype == LSE_cache_eventtype_Notify) {
	      ist->flags = FoundNotifyBit;
	      continue;
	    } else if (ev->etype != LSE_cache_eventtype_Msg) 
	      do_error(e,t,ino,LSE_cache_error_InconsistentEvent)
	      else if (ev->d.Msg.dir || ev->d.Msg.m->type != WrAck) 
		do_error(e,t,ino,LSE_cache_error_InconsistentMsg)
		  break;
	  } while (1);

	  ist->flags |= ev->d.Msg.m->reply | (isNoCache(*(ev->d.Msg.m)) ? 
					      NCBit : 0);
	  if (ev->d.Msg.m->reply == OK && !isNoCache(*(ev->d.Msg.m)) && 
	      !ist->invalidated) {

	    if (t->hasVictim && !(ist->flags & FoundNotifyBit)) {
	      WAIT_Notify(8);
	    }

	    /* NOTE: we do not keep our copy of the directory up to date 
	     * in this process 
	     */
	    if (!t->wasNotCached && !ist->invalidated && !(ist->flags & NCBit)) {
	      demand_dirclearall_flags(t, ino, 0, -1, -1);
	    }

	  }

	  demand_unlock(t, ino, lock_URd | lock_UWr | lock_DRd | lock_DWr, 
			ist->flags2, -1);

	  if (ist->invalidated || (ist->flags & NCBit) || t->wasNotCached) { 
	    /* cannot cache the result */ 
	    demand_send_uReply(e, ino, t,
			       form_nc_reply(t,&nm,flag2reply(ist->flags & 15)),
			       -1, -1);
	  } else {
	    demand_send_uReply(e, ino, t, 
			       form_reply(t,&nm,flag2reply(ist->flags & 15)),
			       -1, -1);
	  }
	  demand_may_finish(t,ino);
	  ist->stateNo = -1000; /* same as -1000 */
	  return;
#undef NCBit
#undef FoundNotifyBit

	} else { 

	  /* write requests where the line is present
	   *
	   * Covers:
	   * uWrReq.!Coh. Cach.!WT. NA:
	   * uWrReq.!Coh. Cach.!WT.!NA:
	   * uWrReq.!Coh. Cach. WT. NA:
	   * uWrReq.!Coh. Cach. WT.!NA:
	   * uWrReq. Coh. Cach.!WT.!NA:
	   * uWrReq. Coh. Cach. WT. NA:
	   * uWrReq. Coh. Cach. WT.!NA:
	   * uWrReq. Coh. Cach.!WT. NA:
	   *
	   */
	  int i, nu;

	  if (isConditional(t->origMsg)) {
	    /* conditional stores must not only be in the cache, but must also
	     * *still* be known to be in the cache of the requestor.
	     */
	    if (!(dirEntP->d.Central[t->sendID/32] & (1<<(t->sendID % 32)))) {

	      //fprintf(stderr,"Bad, bad things might have happened!\n");

	      demand_send_uReply(e, ino, t, form_reply(t,&nm,NACK), -1, -1);
	      demand_finish(t, ino);
	      return;
	    }
	  }

	  /* Must invalidate all the rest and lock line in the meanwhile */
	  ist->flags2 = t->numToDo;
	  demand_lock(t,ino,lock_All,-1,-1);
	  ist->msgCount=0;
	  nu = e->interfaces[ino].parms.I.numUnits;
	  ensure_todo_space(t,nu + DEFAULT_SPACE);
	  for (i=1;i<nu;i++) 
	    if (i != t->sendID && 
		dirEntP->d.Central[i/32] & (1<<(i%32))) {

	      dirEntP->d.Central[i/32] &= ~(1<<(i%32));
	      nm.type = InvalReq;
	      nm.flags = 0;
	      demand_send(e, ino, t, &nm, 0, 0, i, true, -1, -1);
	      ist->msgCount++;
	    }
	  if (ist->msgCount) {
	    ist->flags = 0;

	    /* NOTE: we do not keep our copy of the directory up to date 
	     * in this process 
	     */
	    if (!t->wasNotCached && !ist->invalidated) {
	      if (dirEntP->d.Central[t->sendID/32] & (1<<(t->sendID%32))) {
		if (dirEntP->flags & OwnedBit) ist->flags = OwnedBit;
		else ist->flags = 1;
	      }
	    }
	  } else { /* remove lock operation */
	    ist->flags = -1;
	    demand_unlock(t,ino,lock_All,-1,-1);
	    t->numToDo -= 2; 
	    ist->flags2--;
	  }
	  ist->atRisk = true;

	  while (ist->msgCount) {
	    WAIT_uInvalAck(9); 
	    if (ev->d.Msg.m->reply == OK) {
	      ist->msgCount--;
	      if (!ist->invalidated && isWithData(*(ev->d.Msg.m))) {
		demand_writedata(t,mainEngine,2,-1,-1);
	      }
	    }
	    else {
	      nm.type = InvalReq;
	      nm.flags = 0;
	      ensure_todo_space(t,DEFAULT_SPACE);
	      demand_send(e, ino, t, &nm, 0, 0, ev->d.Msg.sendID, true,
			  -1, -1);
	    }
	  }

	  if (ist->flags >= 0 && !ist->invalidated) {
	    if (ist->flags) demand_dirsetonlybit_flags(t, ino, t->sendID, 
						       ist->flags&OwnedBit,
						       ist->flags2, 
						       -1);
	    else demand_dirclearall_flags(t, ino, ist->flags2, 0, -1);
	    demand_unlock(t,ino,lock_All, t->numToDo-1, -1);
	  }
	
	  demand_may_finish(t, ino);
	  ist->stateNo = -1001; /* just clear anything you see */
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	
	} /* present in some way */
  
      } /* write requests */
      else if (is_CastOutReq(t)) {

	int i, nu;

	/* a missed castout just goes to next level */
	if (t->lineState == Invalid) {

	  demand_finish(t, ino);
	  e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	  return;
	}

	/* if there was data in the castout remove it and invalidate all
	 * the rest
	 */
	if (t->origMsg.flags & WithData) {
	  /* note: write of data will be taken care of by controller... */

	  ist->flags2 = t->numToDo;
	  demand_lock(t,ino,lock_All,-1,-1);

	  ist->msgCount=0;
	  nu = e->interfaces[ino].parms.I.numUnits;
	  ensure_todo_space(t,nu + DEFAULT_SPACE);
	  for (i=1;i<nu;i++) 
	    if (i != t->sendID && 
		dirEntP->d.Central[i/32] & (1<<(i%32))) {
	    
	      dirEntP->d.Central[i/32] &= ~(1<<(i%32));
	      nm.type = InvalReq;
	      nm.flags = 0;
	      demand_send(e, ino, t, &nm, 0, 0, i, true, -1, -1);
	      ist->msgCount++;
	    }
	  if (ist->msgCount) {
	    ist->flags = 0;

	    /* NOTE: we do not keep our copy of the directory up to date 
	     * in this process 
	     */
	    if (!t->wasNotCached && !ist->invalidated) {
	      if (dirEntP->d.Central[t->sendID/32] & (1<<(t->sendID%32))) {
		ist->flags = 1;
	      }
	    }
	  } else { /* remove lock operation */
	    ist->flags = -1;
	    demand_unlock(t,ino,lock_All,-1,-1);
	    t->numToDo -= 2; 
	    ist->flags2--;
	  }
	  ist->atRisk = true;
	
	  while (ist->msgCount) {
	    WAIT_uInvalAck(15); 
	    if (ev->d.Msg.m->reply == OK) {
	      ist->msgCount--;
	      if (!ist->invalidated && isWithData(*(ev->d.Msg.m))) {
		demand_writedata(t,mainEngine,2,-1,-1);
	      }
	    }
	    else {
	      nm.type = InvalReq;
	      nm.flags = 0;
	      ensure_todo_space(t,DEFAULT_SPACE);
	      demand_send(e, ino, t, &nm, 0, 0, ev->d.Msg.sendID, true,
			  -1, -1);
	    }
	  }
	
	  if (ist->flags >= 0 && !ist->invalidated) {
	    demand_dirclearall_flags(t, ino, 0, 0, -1);
	    demand_unlock(t,ino,lock_All, t->numToDo-1, -1);
	  }
	
	} else {
	  if (dirEntP->d.Central[t->sendID/32] & (1<<(t->sendID%32))) {
	    ist->flags2 = t->numToDo;
	    demand_lock(t,ino,lock_All,-1,-1);
	    demand_dirclearbit(t, ino, t->sendID, ist->flags2, -1);
	    demand_unlock(t,ino,lock_All, ist->flags2+1, -1);
	  }
	}
	demand_may_finish(t, ino);
	ist->stateNo = -1001; /* just clear anything you see */
	e->interfaces[upperInterface+1].handler(e,t,&nullEv);
	return;
      }
      /* TODO: rmw */

      return;
    } /* downstream messages */

    /************* upstream messages *******************/
    {
    upstream_msg_state0: /* new work... */

      if (ev->d.Msg.m->type == InvalReq) { 
	int i, nu;

	t->aliveCnt++;

	/* read the directory to see what is in there, but really need to
	 * read the victim!
	 *
	 * NOTE: engine must have verified that we are not locked against
	 *       this transaction
	 */

	demand_readdirectory(t,ino,true,-1,-1);
	WAIT_StateRead(10);

	if (t->origMsg.type == LSE_cache_msgtype_None) { /* victim expulsion */
	  demand_notify(t,ino,0,0,-1,-1); 
	} 

	/* missed the cache */
	if (t->lineState == Invalid) {
	  /* nothing to invalidate... */
	  nm.type = InvalAck;
	  nm.reply = OK;
	  demand_finish(t, ino);
	  demand_send_lReply(e, ino, t, &nm,-1,-1);
	  return;
	} 

	/* We have been asked to invalidate the upper levels */
	ist->msgCount = 0;
	ist->flags = 0;

	// TODO: deal with dirty lines needing writeback

	/* If we assume that the line has already been marked invalid, then
	 * no new request will see the directory, and we do not need to lock 
	 * it.  There is an assumption here that we've buffered the directory
	 * state.
	 */
      
	nu = e->interfaces[ino].parms.I.numUnits;
	ensure_todo_space(t, nu + DEFAULT_SPACE);
	for (i=1;i<nu;i++) {
	  if (dirEntP->d.Central[i/32] & (1<<(i%32))) {
	    /* if in directory, invalidate it... */
	    nm.type = InvalReq;
	    nm.flags = 0;
	    demand_send(e, ino, t, &nm, 0, 0, i, true,-1,-1);
	    ist->msgCount++;
	  }
	}

	while (ist->msgCount) {
	  WAIT_uInvalAck(6);
	  if (ev->d.Msg.m->reply == OK) {
	    ist->msgCount--;
	    if (isWithData(*(ev->d.Msg.m))) ist->flags = 1;
	  }
	  else {
	    nm.type = InvalReq;
	    nm.flags = 0;
	    ensure_todo_space(t, DEFAULT_SPACE);
	    demand_send(e, ino, t, &nm, 0, 0, ev->d.Msg.sendID, true,-1,-1);
	  }
	}

	/* Note: no need to write directory, as it is now invalid */

	nm.type = InvalAck;
	nm.reply = OK;
	nm.flags = ist->flags ? WithData : 0;
	demand_finish(t, ino);
	demand_send_lReply(e, ino, t, &nm,-1,-1);

      } else demand_error(e,t,ino,LSE_cache_error_InconsistentMsg);
      return;
    }
  }

  static void 
  centraldir_upper_handle_crosscheck(LSE_cache_engine_t *e, 
				     LSE_cache_tstate_t *t,
				     LSE_cache_tstate_t *told) {
    LSE_cache_istate_t *ist;
    int ntd, tdi;

    ist = &told->iState[upperInterface];
    ist->invalidated = true;

    switch (ist->stateNo) {
    case -1000:
      /* change the reply to a non-cacheable reply and cancel anything
       * else that is outstanding....
       */
      ntd = told->numToDo;
      for (tdi = told->todoPtr ; tdi <= ntd ; tdi++) {
	if (told->todo[tdi].ino == upperInterface) {
	  if (told->todo[tdi].action == LSE_cache_actiontype_Send)
	    told->todo[tdi].work.Send.newMsg.flags |= NoCache;
	  else 
	    told->todo[tdi].action = LSE_cache_actiontype_None;
	}
      }
      ist->atRisk = false;
      ist->stateNo = -1;
      break;

    case -1001:
      /* change the reply to a non-cacheable reply and cancel anything
       * else that is outstanding....
       */
      ntd = told->numToDo;
      for (tdi = told->todoPtr ; tdi <= ntd ; tdi++) {
	if (told->todo[tdi].ino == upperInterface) {
	  told->todo[tdi].action = LSE_cache_actiontype_None;
	}
      }
      ist->atRisk = false;
      ist->stateNo = -1;
      break;

    default: /* just let things occur */
      break;
    }
  }

  /*********************** setup routine ***********************/

  static void setup_upper_interface(LSE_cache_engine_t *e) {
    LSE_cache_interface_t *f 
      = (LSE_cache_interface_t *)&e->interfaces[upperInterface];
    switch (f->parms.I.style) {
    case LSE_cache_interfacestyle_Null: 
      f->handler = null_upper_handle_event;
      f->willStart = null_upper_msg_starts_transaction;
      f->interceptMsg = null_upper_intercept_msg;
      break;
    case LSE_cache_interfacestyle_Snoop:
      f->handler = snoop_upper_handle_event;
      f->willStart = snoop_upper_msg_starts_transaction;
      f->interceptMsg = snoop_upper_intercept_msg;
      break;
    case LSE_cache_interfacestyle_CentralDirectory:
      f->handler = centraldir_upper_handle_event;
      f->willStart = centraldir_upper_msg_starts_transaction;
      f->interceptMsg = centraldir_upper_intercept_msg;
      f->crosshandler = centraldir_upper_handle_crosscheck;
      break;
    default:
      fprintf(stderr,"Unknown cache interface style");
      break;
    }
  }

  /*******************************************************************
   * redirection routines
   *******************************************************************/

  /* Initialize a transaction's state */
  void init_tstate(LSE_cache_engine_t *e,
		   LSE_cache_tstate_t *t, 
		   const LSE_cache_msg_t *m, 
		   int dir,
		   int sendID,
		   bool newToDo) {
    t->numToDo = 0;
    t->todoPtr = 0;
    t->aliveCnt = 0;
    if (newToDo) {
      t->todoSize = DEFAULT_SPACE;
      t->todo = static_cast<LSE_cache_action_t *>(malloc(sizeof(LSE_cache_action_t) * DEFAULT_SPACE));
    }
    t->needsDealloc = newToDo;
    memset(&t->iState[0],0,sizeof(t->iState));
    t->hasVictim = false;
    t->wasNotCached = false;
    t->linkedTrans = NULL;
    t->dir = dir;
    t->origMsg = *m;
    t->sendID = sendID;
  }

  /* finish/free transaction state */
  void finish_tstate(LSE_cache_engine_t *e,
		     LSE_cache_tstate_t *t) {
    if (t->needsDealloc) free(t->todo);
  }

  /* Should this message start a transaction? We look for:
   *   Upper interface: requests directed at us
   *   Lower interface: requests directed at us, as well as 
   *                    snooped transactions.
   */
  bool msg_starts_transaction(LSE_cache_engine_t *e,
				 const LSE_cache_msg_t *m,
				 int dir, bool snooped) {
    int ino = dir ? 0 : lowerInterface;
    return e->interfaces[ino].willStart(e,m,dir,snooped);
  }

  void handle_event(LSE_cache_engine_t *e,
		    LSE_cache_tstate_t *t,
		    const LSE_cache_event_t *ev) {

    switch (ev->etype) {
    case LSE_cache_eventtype_Msg:
      if (ev->d.Msg.dir) {
	/* goto outermost upper interface */
	e->interfaces[0].handler(e,t,ev);
	return;
      } else {
	/* go to outermost lower interface */
	e->interfaces[lowerInterface].handler(e,t,ev);
	return;
      }
      break;
    default:
      e->interfaces[ev->ino].handler(e,t,ev);
      break;
    }
  }

  void handle_crosscheck(LSE_cache_engine_t *e,
			 LSE_cache_tstate_t *t,
			 LSE_cache_tstate_t *told) {
    int i;
    for (i=0;i<=lowerInterface;i++)
      if (told->iState[i].atRisk)
	e->interfaces[i].crosshandler(e,t,told);
  }

  /*******************************************************************
   * setup routines
   *******************************************************************/

} // namespace LSE_cache_default
