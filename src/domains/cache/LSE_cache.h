/* Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header file for cache protocol emulation
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Definitions of cache domain stuff.
 *
 */

#ifndef _LSE_CACHE_H_
#define _LSE_CACHE_H_

namespace LSE_cache {

  typedef enum LSE_cache_error_t {
    LSE_cache_error_None = 0,
    LSE_cache_error_NoProcess = 1,
    LSE_cache_error_UnknownState = 2,
    LSE_cache_error_InconsistentMsg = 3,
    LSE_cache_error_InconsistentEvent = 4,
    LSE_cache_error_BadParameter = 5
  } LSE_cache_error_t;
  
  typedef enum LSE_cache_msgtype_t {
    LSE_cache_msgtype_None = 0,
    LSE_cache_msgtype_RdReq = 1,
    LSE_cache_msgtype_RdAck = 2,
    LSE_cache_msgtype_WrReq = 3,
    LSE_cache_msgtype_WrAck = 4,
    LSE_cache_msgtype_RdWrReq = 5,
    LSE_cache_msgtype_RdWrAck = 6,
    LSE_cache_msgtype_InvalReq = 7,
    LSE_cache_msgtype_InvalAck = 8,
    LSE_cache_msgtype_CastOutReq = 9,
    LSE_cache_msgtype_CastOutAck = 10
  } LSE_cache_msgtype_t;

  typedef enum LSE_cache_actiontype_t {
    LSE_cache_actiontype_None = 0,
    LSE_cache_actiontype_Send = 1,
    LSE_cache_actiontype_ReadState = 2,
    LSE_cache_actiontype_WriteState = 3,
    LSE_cache_actiontype_Notify = 4,
    LSE_cache_actiontype_CrossCheck = 5,
    LSE_cache_actiontype_WriteData = 6,
    LSE_cache_actiontype_ReadData = 7,
    LSE_cache_actiontype_Error = 8,
    LSE_cache_actiontype_ChooseVictim = 9
  } LSE_cache_actiontype_t;

  typedef enum LSE_cache_eventtype_t {
    LSE_cache_eventtype_None = 0,
    LSE_cache_eventtype_Msg = 1,
    LSE_cache_eventtype_StateRead = 2,
    LSE_cache_eventtype_DataRead = 3,
    LSE_cache_eventtype_VictimChosen = 4,
    LSE_cache_eventtype_Notify = 5
  } LSE_cache_eventtype_t;

  typedef enum LSE_cache_msgreply_t {
    LSE_cache_msgreply_OK = 0,
    LSE_cache_msgreply_NACK = 1,
    LSE_cache_msgreply_Retry = 2
  } LSE_cache_msgreply_t;

} // namespace LSE_cache

#endif /* _LSE_CACHE_H_ */
