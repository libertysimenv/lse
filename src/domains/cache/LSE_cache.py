# /* 
#  * Copyright (c) 2000-2005 The Liberty Computer Architecture Research
#  * Group. (http://www.liberty-research.org)
#  *
#  * All rights reserved.
#  * 
#  * Permission is hereby granted, without written agreement and without
#  * license or royalty fees, to use, copy, modify, and distribute this
#  * software and its documentation for any purpose, provided that the
#  * above copyright notice and the following two paragraphs appear in
#  * all copies of this software.
#  * 
#  * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
#  * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
#  * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
#  * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
#  * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  * 
#  * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
#  * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
#  * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
#  * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
#  * OR MODIFICATIONS.
#  *
#  */
# /*%
#  * 
#  * LSE_cache domain class Python module
#  *
#  * Authors: David A. Penry <dpenry@cs.princeton.edu>
#  *
#  * Declares the LSE_cache domain class to LSE.
#  * This domain supports cache protocol engines and various cache
#  * support operations.  Note that the implementation choice determines
#  * the types for messages, options, and state machine information as
#  * well as which library will be used.  However, we have unified the
#  * common state machines into a single library, so it is not really
#  * very interesting except for those wishing to roll their own state
#  * machine without disturbing the code.
#  *
#  * When new implementations are created, change the locations saying
#  * IMPLEMENTATION CODE HERE
#  *
#  */

import LSE_domain, string;

class LSE_DomainObject(LSE_domain.LSE_BaseDomainObject):

    # domain class name
    className = "LSE_cache"

    classHeaders = [ "LSE_cache.h" ]
   
    classNamespaces = [ "LSE_cache" ]

    libraries = "libLSEcache.a"

    createIfRequired = 1
    
    # init method
    def __init__(self, instname, buildArgs, runArgs, bp):
        
        LSE_domain.LSE_BaseDomainObject.__init__(self,instname,buildArgs,
                                                 runArgs, bp)

        # assign per-instance attributes... there will be none

        self.implLibraries = LSE_DomainObject.libraries

        # determine types based upon instance...

        # parse the build arguments...
        buildArgsList = string.split(buildArgs)
        
        if (not len(buildArgsList)):
            raise LSE_domain.\
                  LSE_DomainException("Missing cache protocol name "
                                      "for instance '%s'" % instname)

        # IMPLEMENTATION CODE HERE ... just add elif clauses to check
        # for the new protocol; each clause must define the
        # identifiers defined in the default implementation and any
        # others it may have by adding them to tl
        
        if (buildArgsList[0] == "default"):
            self.implHeaders = ["LSE_cache_default.h"]
            self.implNamespaces = [ "LSE_cache_default" ]
        else:
            raise LSE_domain.\
                  LSE_DomainException("Unknown protocol name "
                                      "'%s' for instance '%s'" %
                                      (buildArgsList[0],instname))
        
    ########### determine whether we can use this as a required domain ######
    def approveRequirement(self, buildArgs):
        return 1

    
