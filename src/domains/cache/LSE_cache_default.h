/* Copyright (c) 2000-2009 The Liberty Computer Architecture Research
 * Group. (http://www.liberty-research.org)
 *
 * All rights reserved.
 * 
 * Permission is hereby granted, without written agreement and without
 * license or royalty fees, to use, copy, modify, and distribute this
 * software and its documentation for any purpose, provided that the
 * above copyright notice and the following two paragraphs appear in
 * all copies of this software.
 * 
 * IN NO EVENT SHALL THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP
 * BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF THE LIBERTY COMPUTER ARCHITECTURE
 * RESEARCH GROUP HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP SPECIFICALLY
 * DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 * AND THE LIBERTY COMPUTER ARCHITECTURE RESEARCH GROUP HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 *
 */
/*%
 * 
 * Header file for cache protocol emulation
 *
 * Authors: David A. Penry <dpenry@ee.byu.edu>
 *
 * Definitions of cache domain stuff.
 *
 */

#ifndef _LSE_CACHE_DEFAULT_H_
#define _LSE_CACHE_DEFAULT_H_

#include <LSE_inttypes.h>
namespace LSE_cache_default {
  using namespace LSE_cache;

  typedef enum LSE_cache_protocol_t {
    LSE_cache_protocol_NoneWB = 0,
    LSE_cache_protocol_NoneWT = 1,
    LSE_cache_protocol_WriteThrough = 2,
    LSE_cache_protocol_MSI = 3,
    LSE_cache_protocol_MESI = 4,
    LSE_cache_protocol_MOESI = 5,
    LSE_cache_protocol_MOSI = 6,
    LSE_cache_protocol_Illinois = 7,
    LSE_cache_protocol_Synapse = 8,
    LSE_cache_protocol_Berkeley = 9,
    LSE_cache_protocol_Firefly = 10,
    LSE_cache_protocol_Dragon = 11
  } LSE_cache_protocol_t;

  typedef enum LSE_cache_interfacestyle_t {
    LSE_cache_interfacestyle_Null = 0,
    LSE_cache_interfacestyle_Snoop = 1,
    LSE_cache_interfacestyle_CentralDirectory = 2
  } LSE_cache_interfacestyle_t;

  typedef union LSE_cache_engine_parms_t {
    struct {
      LSE_cache_protocol_t protocol;

      bool WriteEarly;
      bool WriteAlloc;
      bool ReadBuffered;
      bool Inclusive;
      int VictimHandling; /* 0 = all early, 1 = late msg,
			   * 2 = late msg+eviction
			   * 3 = late msg+eviction+data read
			   * 4 = (3) + late choice
			   */
                     
      bool PuntOnRMW;

    } P;
                   
    struct { /* all interfaces must begin with these fields */
      LSE_cache_interfacestyle_t style;
      int numUnits;
    } I;
    struct {
      LSE_cache_interfacestyle_t style;
      int numUnits;
      bool CombineOutWriteInval; /* for sender */
      bool SeeInWriteAsInval;
    } Snoop;
    struct {
      LSE_cache_interfacestyle_t style;
      int numUnits;
      bool BetweenCacheTransfers; /* for sender */
    } CentralDir;
  } LSE_cache_engine_parms_t;

  typedef struct LSE_cache_protoTableEntry_t {
    int state;
    LSE_cache_msgtype_t mtype;
    int dir;
    int flagsMask;
    int flagsVal;
    int newState;
  } LSE_cache_protoTableEntry_t;

  typedef struct LSE_cache_msg_t {
    LSE_cache_msgtype_t type;
    LSE_cache_msgreply_t reply; /* acks */
    int flags;
  } LSE_cache_msg_t;

  const int LSE_cache_msgflagbit_Cacheable = 1;
  const int LSE_cache_msgflagbit_Coherent = 2;
  const int LSE_cache_msgflagbit_NoAlloc = 4;
  const int LSE_cache_msgflagbit_WriteThrough = 8;
  const int LSE_cache_msgflagbit_Conditional = 16;
  const int LSE_cache_msgflagbit_WithData = 32;
  const int LSE_cache_msgflagbit_ToOwn = 64;
  const int LSE_cache_msgflagbit_Prefetch = 128;
  const int LSE_cache_msgflagbit_Shareable = 256;
  const int LSE_cache_msgflagbit_NoCache = 512;

  const int LSE_cache_linestate_Invalid = 0;
  const int LSE_cache_linestate_ExclusiveOwner = 1;
  const int LSE_cache_linestate_SharedOwner = 5;
  const int LSE_cache_linestate_DirtyOwner = 3;
  const int LSE_cache_linestate_Shared = 4;

  const int LSE_cache_stateaccess_Tags = 1;
  const int LSE_cache_stateaccess_LineState = 2;
  const int LSE_cache_stateaccess_offset_upper = 8;
  const int LSE_cache_stateaccess_offset_lower = 24;

  const int LSE_cache_stateaccess_clearall = 1;
  const int LSE_cache_stateaccess_clear = 2;
  const int LSE_cache_stateaccess_set = 4;
  const int LSE_cache_stateaccess_setflags = 8;

  const int LSE_cache_lock_Victim = 3584;
  const int LSE_cache_lock_DRd = 28672;
  const int LSE_cache_lock_DWr = 229376;
  const int LSE_cache_lock_URd = 1835008;
  const int LSE_cache_lock_UWr = 14680064;
  const int LSE_cache_lock_DCoh = 117440512;
  const int LSE_cache_lock_UCoh = 939524096;
  const int LSE_cache_lock_All = 1073741312;
  const int LSE_cache_lockbits_All = 153391616;

  typedef struct LSE_cache_direntry_t {
    int flags; /* owned, pending, etc. */
    union {
      uint32_t *Central;
      /* pointer to bit-wise array of flags */
      struct {
	int head;
	int tail;
      } Distributed;
    } d;
  } LSE_cache_direntry_t;

  typedef struct LSE_cache_action_t {
    LSE_cache_actiontype_t action;
    int progress; /* 0=not done, 1=done, 2+=begun in some way */
    int dep[2]; /* dependencies */
    int flag; /* up to implementation to use this */
    int ino; /* interface requesting action */
    union {
      struct {
	LSE_cache_msg_t newMsg;
	int dir; /* 1 = downstream, 0 = upstream */
	int replyIDtouse; /* 0 = transaction, 1 = copy */
	int destID; /* -1 = broadcast */
	bool wholeBlock;
      } Send;
      struct {
	int fields; /* bit mask for fields (_stateaccess_*) */
	int setLineState; /* bits to set */
	int clearLineState;   /* bits to clear (inverted) */
	int dflags[3]; /* flags */
	int dbits[3];  /* #1 is extraneous */
      } WriteState;
      struct {
	int fields;
      } ReadState;
      struct {
	int dest;
	int msg;
      } Notify;
      LSE_cache_error_t Error;
      struct {
	int source;  /* 1 = msg, 2 = buffer,
			3 = both (buffer first),
			7 = both (but early write) */
      } WriteData;
      struct {
	int locks;
	int stateNo;
      } ChooseVictim;
    } work;
  } LSE_cache_action_t;

  typedef struct LSE_cache_istate_t {
    int stateNo;
    int flags;
    int flags2;
    int msgCount;
    bool atRisk; /* a cross-transaction may matter */
    int invalidated; /* invalidation information */
    LSE_cache_msg_t savedMsg;  /* a reply we need to save */
  } LSE_cache_istate_t;

  typedef struct LSE_cache_tstate_t {
    LSE_cache_msg_t origMsg;  /* original message info */
    int dir;    /* 1 = recvd on upper */
    int sendID; /* local id of sender */
    int aliveCnt; /* how many interfaces are keeping it alive */

    int lineState;
    bool hasVictim; /* is a line victimized? */
    bool wasNotCached; /* was the line cached in the end? */

    LSE_cache_direntry_t dState[3];
    LSE_cache_istate_t iState[3];

    int numToDo;
    int todoSize;
    int todoPtr; /* points to oldest not yet done;
		    may safely be out-of-date */
    struct LSE_cache_tstate_t *linkedTrans;
                 
    bool needsDealloc;
    LSE_cache_action_t *todo;
  } LSE_cache_tstate_t;

  typedef struct LSE_cache_event_t {
    int etype;
    int ino;
    union {
      struct {
	const LSE_cache_msg_t *m;
	int dir;
	bool snooped;
	int replyIDtouse; /* for internal passage */
	int sendID;
	int destID;
      } Msg;
      struct {
	int victimState;
	struct LSE_cache_tstate_t *t; /* NULL if none */
	bool gotVictim;
      } VictimChosen;
      struct {
	int msg;
      } Notify;
    } d;
  } LSE_cache_event_t;

  typedef struct LSE_cache_engine_t {
    struct {
      bool (*willStart)(struct LSE_cache_engine_t *, 
			   const LSE_cache_msg_t *m,
			   int, bool);
      bool (*interceptMsg)(struct LSE_cache_engine_t *, 
			      LSE_cache_tstate_t *);
      void (*handler)(struct LSE_cache_engine_t *, 
		      LSE_cache_tstate_t *,
		      const LSE_cache_event_t *);
      void (*crosshandler)(struct LSE_cache_engine_t *, 
			   LSE_cache_tstate_t *,
			   LSE_cache_tstate_t *);
      LSE_cache_engine_parms_t parms;
    } interfaces[3];
    struct {
      int protoTableSize;
      LSE_cache_protoTableEntry_t *protoTable;
      bool hasOwner;
    } pinfo;
  } LSE_cache_engine_t;

  typedef struct LSE_cache_interface_t {
    bool (*willStart)(struct LSE_cache_engine_t *, 
			 const LSE_cache_msg_t *m,
			 int, bool);
    bool (*interceptMsg)(struct LSE_cache_engine_t *, 
			    LSE_cache_tstate_t *);
    void (*handler)(struct LSE_cache_engine_t *, 
		    LSE_cache_tstate_t *,
		    const LSE_cache_event_t *);
    void (*crosshandler)(struct LSE_cache_engine_t *, 
			 LSE_cache_tstate_t *,
			 LSE_cache_tstate_t *);
    LSE_cache_engine_parms_t parms;
  } LSE_cache_interface_t;

#include <LSE_cache_common.h>

} // namespace LSE_cache_default

#endif /* _LSE_CACHE_DEFAULT_H_ */
