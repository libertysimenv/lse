<!DOCTYPE book PUBLIC "-//Liberty ARG//DTD DocBook V4.1-Based Extension V1.0//EN" [
<!ENTITY typograph            SYSTEM "typograph.sgm">
<!ENTITY cache                SYSTEM "cache.sgm">
<!ENTITY checkpoint           SYSTEM "checkpoint.sgm">
<!ENTITY clp                  SYSTEM "clp.sgm">
<!ENTITY configurations       SYSTEM "configurations.sgm">
<!ENTITY devemu               SYSTEM "devemu.sgm">
<!ENTITY domains              SYSTEM "domains.sgm">
<!ENTITY dynamicvisualization SYSTEM "dynamic_visualization.sgm">
<!ENTITY emuinterface         SYSTEM "emuinterface.sgm">
<!ENTITY lis                  SYSTEM "lis.sgm">
<!ENTITY lssappendix          SYSTEM "lssappendix.sgm">
<!ENTITY morerefinements      SYSTEM "morerefinements.sgm">
<!ENTITY refinements          SYSTEM "refinements.sgm">
<!ENTITY sampling             SYSTEM "sampling.sgm">
<!ENTITY simplemodel          SYSTEM "simplemodel.sgm">
<!ENTITY staticvisualization  SYSTEM "static_visualization.sgm">
<!ENTITY usingemu             SYSTEM "usingemu.sgm">

<!ENTITY multicyclelss        SYSTEM "models/multicycle.lss">
<!ENTITY multicycle2lss       SYSTEM "models/multicycle2.lss">
<!ENTITY multicycleEventslss  SYSTEM "models/multicycleEvents.lss">
<!ENTITY nonuniformlss        SYSTEM "models/nonuniform.lss">
<!ENTITY nonuniform2lss       SYSTEM "models/nonuniform2.lss">
<!ENTITY exPipeslss           SYSTEM "models/exPipes.lss">
<!ENTITY exPipes2lss          SYSTEM "models/exPipes2.lss">
<!ENTITY pipelinedlss         SYSTEM "models/pipelined.lss">
<!ENTITY bypassinglss         SYSTEM "models/bypassing.lss">
<!ENTITY bypassing2lss        SYSTEM "models/bypassing2.lss">
<!ENTITY exPipesWithDroplss   SYSTEM "models/exPipesWithDrop.lss">
<!ENTITY controlspeclss       SYSTEM "models/controlspec.lss">
<!ENTITY controlspec2lss      SYSTEM "models/controlspec2.lss">

<!ENTITY LSS "<command>lss</>">
<!ENTITY REFMAN
"<citetitle pubwork='book'>The Liberty Simulation Environment Reference Manual</citetitle>">
]>

<book>
  <bookinfo>
    <title>Liberty Simulation Environment User Manual</title> 

    <edition>Version 2.0</> 
    <corpauthor>The Liberty Research Group</>
  </bookinfo>

  <preface>
    <title>Preface</title>

    <para>This book describes how to use LSE to develop simulators and
          how to use LSE tools more effectively.  It includes
          information on LSS, debugging, control of simulation
          parameters, and use of the various APIs available to code
          points.  For a complete listing of APIs available to
          configurations, see &REFMAN;</para>

    &typograph;

  </preface>

  <part><title>Developing Simulation Models in LSE</>

    <partintro>

      <para>We assume that you have read <citetitle
      pubwork='book'>Getting Started with the Liberty Simulation
      Environment</citetitle> and have learned how to install and
      invoke LSE and a little bit about writing configurations and
      modules.  Now you want to use LSE to develop a useful simulator.
      This part of the <citetitle pubwork='book'>User Manual</> will
      help you to develop your own simulators.  It provides our
      recommendations for how to proceed with the development task.
      It also provides instructions on how to use the various LSE
      domains (extensions).</para>

      <para>In the course of these chapters we will develop a model of
      a simple in-order microarchitecture for a processor executing
      the PowerPC instruction set.  This simulator will use an LSE
      emulator which is able to emulate Linux system calls.  We
      suggest using the <productname>crosstool</> cross-compilation
      system (available at <ulink
      url="http://www.kegel.com/crosstool"></>) to create a gcc
      cross-compiler to produce PowerPC executables.</para>

<!--
      <note><para>PowerPC executables to run on the simulator should be
      linked using the <command>-static</> flag.</para>
      </note>
-->

    </partintro>

    &simplemodel;
    &refinements;
    &morerefinements;

    &usingemu;
    &devemu;
    &checkpoint;
    &sampling;
<!--    &cache;-->

  </part>


  <part><title>Using the LSE tools more effectively</>

    &configurations;
    &staticvisualization;
    &dynamicvisualization;

  </part>

  <part><title>Extending LSE</>

    <partintro>

      <para>This part of the manual describes how to extend LSE by
      writing new modules, domains, and emulators.</para>

    </partintro>

    &domains;
    &clp;
    &emuinterface;
    &lis;

  </part>

  <part><title>Reference materials</>

    <chapter><title>Useful information I haven't organized yet.</title>

      <literallayout>
- Use the LSE_endianness domain to provide translation from/to big or
  little endian format and the host format.

- Note in docs that CXX, SED, NM, OBJDUMP set at build time

- If inside some code you put something like: //*, weird, weird things will 
  happen, as m4 will see that as starting a C-comment, not a C++ comment.

- Point out that when you design an emulator, exceptions should be detected
  before writeback (so we can properly stop writeback and do exceptions
  in parallel with writeback); any which are not should be considered
  fatal exceptions.  Exception overrides need to occur before writeback, but
  should be treated as side-effecting, since they're likely to call OS's or
  some such.

- call LSEfw_show_port_status from a debugger to show current port status

</literallayout>

      <itemizedlist>
	<listitem>
	  <para>Rules about when dynids/resolutions/etc. are reclaimed</para>
	</listitem>
	<listitem>
	  <para>Do not use assert inside modules</para>
	</listitem>
	<listitem>
	  <para>Do not use state-updating libc calls (like rand!)</para>
	</listitem>
	<listitem>
	  <para>Do not use LSEm4_warn or print statements for
	  debugging inside modules.  Definitely do not create
	  debugging parameters to print things out.  All of this
	  should be done using events and stat libraries.
	  <emphasis>If it's interesting enough to print while
	  debugging, it's interesting enough to be an
	  event.</emphasis></para>
	</listitem>
	<listitem>
	<para>When making a makefile for a module, be sure to include targets
	      clean and all.  .clm files should depend upon a file named
	      remaker (used for forcing rebuild with incremental rebuild)
        </para>
        </listitem>
	<listitem><para>Responsibilities with respect to checkpointing</para>
	</listitem>

	<listitem><para>When you don't include the public before inheriting from LSE_module, you get errors that look like:
<programlisting>
lookup_handler.cc: In constructor `&lt;unnamed&gt;::LSEfw_class_0_lookup_handler::LSEfw_class_0_lookup_handler(char*)':
../../../../../../include/SIM_control.h:226: error: `const char*LSEfw_module::LSE_instance_name' is inaccessible
lookup_handler.cc:1074: error: within this context
</programlisting></para>
	</listitem>

	<listitem><para>When you forget to make methods public, you get errors that look like:
<programlisting>
regalloc_manager.cc: In function `void LSEmi__core1__rntable__spec_alloc_entry_control___LSE_do_init(void*)':
regalloc_manager.cc:938: error: `boolean &lt;unnamed&gt;::regalloc_manager::LSEmi__init()' is private
regalloc_manager.cc:1252: error: within this context
</programlisting></para>
	</listitem>
      </itemizedlist>

      <note>
      <para>Note that if a module wishes to create a "library" of
      functions to be shared among instances of the module, the best
      way to do this will be to create a domain implementation of the
      <domainclass>library</> domain class and install that library in
      the install area.  The source code for this library should
      <emphasis>not</> be placed in the module tarballs and can only
      know about instance data through parameters of calls to the
      library.</para>
      </note>


<literallayout>
UI decisions:
-------------
ls-create-module &lt;name>
        - create module under LIBERTY_SIM_USER_PATH (first item)
</literallayout>


  <sect1><title>Clocks</title> 

    <para>At present, LSE directly supports only a single clock,
    though multi-clock support is expected in the future.  In the
    meanwhile, multiple ratioed clocks can be modeled by considering
    the LSE clock to be a clock fast enough to allow any clock in the
    system to be an integral divisor of that clock.  Then create
    modules which update their state only every N clocks (including
    any early state update).  Note that standard LSE modules with
    state (e.g. the <modulename>delay</> module) do not support such
    behavior.</para>

  </sect1>

  <sect1><title>Organizing a configuration</title> 

  <todo><para>Write.  Bring in idea of libraries.  Hierarchy.
    Granularity.  Divide and conquer.
</para>
    </todo>
  </sect1>

  <sect1><title>Common hardware paradigms</title> <todo><para>Write.
    Thoughts about state machines, including early state update,
    enforcing ordering (within and between cycles), wakeup logic,
    arbitration, selection, routing.</para>
    </todo>
  </sect1>

  </chapter>

  &lssappendix;
</part>

</book>


