import LSE_emu;
var emu = LSE_emu::create("emuinst", <<<LSE_PowerPC --
include PowerPC64.lis
include PPCLinux.lis
include PPCbuild.lis
include PowerPC_compat.lis
show maximal queue;
>>>, "") : domain ref;
add_to_domain_searchpath(emu);

using corelib;
include "exPipes.lss";

instance PC          : corelib::delay;
instance Imem        : corelib::converter;
instance IF_ID_latch : corelib::delay;
instance Decode      : corelib::converter;
instance regRead     : corelib::converter;
instance regWrite    : corelib::sink;
instance ID_EX_latch : corelib::delay;
instance EXtee       : corelib::tee;
instance ALUmem      : exPipes;
instance EX_WB_latch : corelib::delay;
instance newPC_latch : corelib::pipe;
instance newDynid    : corelib::converter;

PC.initial_state = <<<
  *init_id = LSE_dynid_create();
  LSE_emu_init_instr(*init_id, 1, LSE_emu_get_start_addr(1));

  return TRUE; // we set an initial state
>>>;

PC.out   -> [none] Imem.in;

Imem.convert_func = <<<
  LSE_emu_do_instrstep(id, LSE_emu_instrstep_name_ifetch);
  return data;
>>>;

Imem.out        -> [none] IF_ID_latch.in;
IF_ID_latch.out -> Decode.in;

Decode.convert_func = <<<
  LSE_emu_do_instrstep(id, LSE_emu_instrstep_name_decode);
  return data;
>>>;

Decode.out  -> [none] regRead.in;

regRead.convert_func = <<<
  LSE_emu_do_instrstep(id, LSE_emu_instrstep_name_opfetch);
  return data;
>>>;

regWrite.sink_func = <<<
  if (LSE_signal_data_present(status) && LSE_signal_enable_present(status)) {
    LSE_emu_writeback_remaining_operands(id);
    LSE_emu_do_instrstep(id, LSE_emu_instrstep_name_exception);
  }
>>>;

regRead.out     -> [none] ID_EX_latch.in;
ID_EX_latch.out -> EXtee.in;
EXtee.out       -> ALUmem.in;
EXtee.out       -> newPC_latch.in;

ALUmem.out      -> [none] EX_WB_latch.in;

EX_WB_latch.out -> regWrite.in;
newPC_latch.out -> newDynid.in;
newDynid.out    -> [none] PC.in;

newPC_latch.depth = 4;

newPC_latch.delay_for_send = <<<
    if (LSE_emu_dynid_is(id, load) || LSE_emu_dynid_is(id, store))
      return 2;
    else if (LSE_emu_dynid_get(id, queue) == LSE_emu::PPC_FPU_Queue) 
      return 4;
    else return 1;
>>>;

newDynid.convert_func = <<<
  *newidp = LSE_dynid_create();
  LSE_dynid_cancel(*newidp);

  if (LSE_emu_get_context_mapping(1) == LSE_emu_dynid_get(id, swcontexttok))
    LSE_emu_init_instr(*newidp, 1, LSE_emu_dynid_get(id, next_pc));
  else if (LSE_emu_get_context_mapping(1))
    LSE_emu_init_instr(*newidp, 1, LSE_emu_get_start_addr(1));
  else LSE_emu_init_instr(*newidp, 1, LSE_emu_dynid_get(id, addr));

  return data;  
>>>;

