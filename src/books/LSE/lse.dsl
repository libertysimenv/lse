<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY print-ss PUBLIC "-//Norman Walsh//DOCUMENT DocBook Print Stylesheet//EN" CDATA DSSSL>
<!ENTITY html-ss PUBLIC "-//Norman Walsh//DOCUMENT DocBook HTML Stylesheet//EN" CDATA DSSSL>
]>

<style-sheet>
<style-specification id="print" use="print-stylesheet">
<style-specification-body>

(define %footer-margin% 4pi) ; 4pi default
(define %bottom-margin% 6pi) ; 8pi default
(define %body-start-indent% 1pi) ; 4pi default
(define %head-before-factor% 0.5) ; 0.75 default

(define %graphic-extensions% 
  '("pdf" "eps" "epsf" "gif" "tif" "tiff" "jpg" "jpeg" "png"))
(define preferred-mediaobject-notations
  '("PDF" "EPS" "PS" "JPG" "JPEG" "PNG" "linespecific"))
(define preferred-mediaobject-extensions
  '("pdf" "eps" "ps" "jpg" "jpeg" "png"))

(define colr-space (color-space 
		    "ISO/IEC 10179:1996//Color-Space Family::Device RGB"))
(define red (color colr-space 1 0 0))
(define black (color colr-space 0 0 0))

(define ($smallerbold-seq$ #!optional (sosofo (process-children)))
  (make sequence
    font-size: (- (inherited-font-size) 1pt)
    font-weight: 'bold
    sosofo))

(element inlinehead 
  (make sequence
    font-weight: 'bold
    font-family-name: %title-font-family%
    (process-children)))
(element capability ($italic-seq$))
(element modparam ($italic-seq$))
(element portname ($bold-seq$))
(element modulename ($bold-seq$))
(element packagename ($bold-seq$))
(element type ($smallerbold-seq$))
(element emulname ($bold-seq$))
(element domainimpl ($bold-seq$))
(element domainclass ($italic-seq$))
(element domainattr ($smallerbold-seq$))

(element openissue
      (make display-group
        space-before: (* %para-sep% 1.5)
        space-after: %para-sep%
	start-indent: (inherited-start-indent)
	end-indent: (inherited-end-indent)
        font-family-name: %admon-font-family%
        font-size: (- %bf-size% 1pt)
        font-weight: 'medium
        font-posture: 'upright
        line-spacing: (* (- %bf-size% 1pt) %line-spacing-factor%)
        (make box
          display?: #t
          box-type: 'border
          line-thickness: 2pt
	  color: red
          (make paragraph
            space-before: %para-sep%
            space-after: %para-sep%
            font-family-name: %title-font-family%
            font-weight: 'bold
            font-size: (HSIZE 2)
            line-spacing: (* (HSIZE 2) %line-spacing-factor%)
            quadding: 'center
            keep-with-next?: #t
	    color: red
            (literal "Open Issue"))
	  (make display-group
		start-indent: 2pt
		end-indent: 2pt
	        (process-children)))))

(element todo
      (make display-group
        space-before: (* %para-sep% 1.5)
        space-after: %para-sep%
	start-indent: (inherited-start-indent)
	end-indent: (inherited-end-indent)
        font-family-name: %admon-font-family%
        font-size: (- %bf-size% 1pt)
        font-weight: 'medium
        font-posture: 'upright
        line-spacing: (* (- %bf-size% 1pt) %line-spacing-factor%)
        (make box
          display?: #t
          box-type: 'border
          line-thickness: 2pt
	  color: red
          (make paragraph
            space-before: %para-sep%
            space-after: %para-sep%
            font-family-name: %title-font-family%
            font-weight: 'bold
            font-size: (HSIZE 2)
            line-spacing: (* (HSIZE 2) %line-spacing-factor%)
            quadding: 'center
            keep-with-next?: #t
	    color: red
            (literal "TO DO"))
	  (make display-group
		start-indent: 2pt
		end-indent: 2pt
	        (process-children)))))

;; Attempt to remove orderedlist from the auto TOC
;; Returns #t if nd should appear in the auto TOC
(define (appears-in-auto-toc? nd)
  (if (or (equal? (gi nd) (normalize "refsect1"))
          (and (or (member (gi nd) (list-element-list))
                    (has-ancestor-member? nd (list-element-list)))
	       (node-list-empty? (select-elements (children nd) 
						  (normalize "title")))
          )
          (have-ancestor? (normalize "refsect1") nd))
      #f
      #t))

;; API lists

(element apilist
  (make display-group
      start-indent: (if (INBLOCK?)
                        (inherited-start-indent)
                        (+ %block-start-indent% (inherited-start-indent)))
      space-before: %block-sep%
      space-after:  %block-sep%
      (process-children)))

(element apilistentry
  (make paragraph
    space-before: (if (first-sibling?)
		      0pt
		      (* 2 %block-sep%))
    (process-children)))

;; Remove annoying block space after a funcsynopsys which is in an apilist
;; Also, keep the font size the same (normal stylesheets reduced it)
(element (apilistentry funcsynopsis)
  (let* ((fsize (lambda () (* (inherited-font-size) 1.0))))
    (make paragraph
      font-family-name: %mono-font-family%
      font-size: (fsize)
      font-weight: 'medium
      font-posture: 'upright
      line-spacing: (* (fsize) %line-spacing-factor%)
      (process-children))
     ))

(element (apilistentry term)
  (make paragraph
    keep-with-next?: #t
    first-line-start-indent: 0pt
    (process-children)))

(element (apilistentry listitem)
  (let ((vle-indent 2em)) ; this ought to be in dbparam!
    (generic-list-item 
     vle-indent
     (make line-field
       field-width: vle-indent
       (literal "\no-break-space;")))))

;; Parm Lists

(element parmlist
  (make display-group
      start-indent: (if (INBLOCK?)
                        (inherited-start-indent)
                        (+ %block-start-indent% (inherited-start-indent)))
      space-before: %block-sep%
      space-after:  %block-sep%
      (process-children)))

(element parmlistentry
  (make paragraph
    space-before: (if (first-sibling?)
		      0pt
		      %block-sep%)
    (process-children)))

;; Remove annoying block space after a funcsynopsys which is in an parmlist
;; Also, keep the font size the same (normal stylesheets reduced it)
(element (parmlistentry funcsynopsis)
  (let* ((fsize (lambda () (* (inherited-font-size) 1.0))))
    (make paragraph
      font-family-name: %mono-font-family%
      font-size: (fsize)
      font-weight: 'medium
      font-posture: 'upright
      line-spacing: (* (fsize) %line-spacing-factor%)
      (process-children))
     ))

(element (parmlistentry term)
  (make paragraph
    keep-with-next?: #t
    first-line-start-indent: 0pt
    (process-children)))

(element (parmlistentry listitem)
  (let ((vle-indent 2em)) ; this ought to be in dbparam!
    (generic-list-item 
     vle-indent
     (make line-field
       field-width: vle-indent
       (literal "\no-break-space;")))))

;; Error lists

(element errorlist
  (make display-group
      start-indent: (if (INBLOCK?)
                        (inherited-start-indent)
                        (+ %block-start-indent% (inherited-start-indent)))
      space-before: %block-sep%
      space-after:  %block-sep%
      (process-children)))

(element (errorlist varlistentry)
  (make paragraph
    keep-with-next?: #t
    first-line-start-indent: 0pt
    space-before: (* 2 %block-sep%)
    (process-children)))

(element (errorlist varlistentry term)
  (make paragraph
    keep-with-next?: #t
    font-weight: 'bold
    first-line-start-indent: 0pt
    (process-children)))

(define %linenumber-mod%
  1)

;; IDList cross-reference
(element idlistxref
  (let* ((linkend (attribute-string (normalize "linkend")))
	 (target (element-with-id linkend)))
    (if (node-list-empty? target)
        (error (string-append "IDListXRef LinkEnd to missing ID '" linkend "'"))
	(make link
	  destination: (node-list-address target)
	  (make sequence
	    (literal " p. ")
	    (element-page-number-sosofo target))))))

</style-specification-body>
</style-specification>

<style-specification id="html" use="html-stylesheet">
<style-specification-body>

(define %funcsynopsis-decoration% #t)

(element inlinehead ($bold-seq$))

(element capability ($italic-seq$))
(element modparam ($italic-seq$))
(element portname ($bold-seq$))
(element modulename ($bold-seq$))
(element packagename ($bold-seq$))
(element type ($bold-seq$))
(element emulname ($bold-seq$))
(element domainimpl ($bold-seq$))
(element domainclass ($italic-seq$))
(element domainattr ($bold-seq$))

(element openissue
  (let* ((adm-title (literal "Open Issue"))
         (id     (attribute-string (normalize "id"))))
   (make element gi: "DIV"
	attributes: (list (list "CLASS" (gi)))
	;; The DIV isn't strictly necessary, of course, but it
	;; is consistent with the graphical-admonition case.
	(if %spacing-paras%
	    (make element gi: "P" (empty-sosofo))
	    (empty-sosofo))
	(make element gi: "TABLE"
	      attributes: (list
			   (list "COLOR" "#FF0000")
			   (list "CLASS" (gi))
			   (list "BORDER" "1")
			   (list "WIDTH" ($table-width$)))
	      (make element gi: "TR"
		    (make element gi: "TD"
			  attributes: (list
				       (list "ALIGN" "CENTER"))
			  (make element gi: "B"
				(if id
				    (make element gi: "A"
					  attributes: (list (list "NAME" id))
					  (empty-sosofo))
				    (empty-sosofo))
				adm-title)))
	      (make element gi: "TR"
		    (make element gi: "TD"
			  attributes: (list
				       (list "ALIGN" "LEFT"))
			  (process-children)))))))

(element todo
  (let* ((adm-title (literal "TO DO"))
         (id     (attribute-string (normalize "id"))))
   (make element gi: "DIV"
	attributes: (list (list "CLASS" (gi)))
	;; The DIV isn't strictly necessary, of course, but it
	;; is consistent with the graphical-admonition case.
	(if %spacing-paras%
	    (make element gi: "P" (empty-sosofo))
	    (empty-sosofo))
	(make element gi: "TABLE"
	      attributes: (list
			   (list "COLOR" "#FF0000")
			   (list "CLASS" (gi))
			   (list "BORDER" "1")
			   (list "WIDTH" ($table-width$)))
	      (make element gi: "TR"
		    (make element gi: "TD"
			  attributes: (list
				       (list "ALIGN" "CENTER"))
			  (make element gi: "B"
				(if id
				    (make element gi: "A"
					  attributes: (list (list "NAME" id))
					  (empty-sosofo))
				    (empty-sosofo))
				adm-title)))
	      (make element gi: "TR"
		    (make element gi: "TD"
			  attributes: (list
				       (list "ALIGN" "LEFT"))
			  (process-children)))))))

;; Attempt to remove orderedlist from the auto TOC
;; Returns #t if nd should appear in the auto TOC
(define (appears-in-auto-toc? nd)
  (if (or (equal? (gi nd) (normalize "refsect1"))
          (and (or (member (gi nd) (list-element-list))
                    (has-ancestor-member? nd (list-element-list)))
	       (node-list-empty? (select-elements (children nd) 
						  (normalize "title")))
          )
          (have-ancestor? (normalize "refsect1") nd))
      #f
      #t))

;; API lists
(element apilist
  (make sequence
    (make element gi: "DIV"
	  attributes: (list (list "CLASS" (gi)))
	  (make element gi: "DL" (process-children)))
    (para-check 'restart)))
	   
(element apilistentry
  (let* ((terms    (select-elements (children (current-node)) (normalize "term")))
	 (synop (select-elements (children (current-node)) (normalize "funcsynopsis")))
	 (listitem (select-elements (children (current-node)) (normalize "listitem"))))
    (make sequence
      (make element gi: "BR" (empty-sosofo))
      (make element gi: "DT"
            (if (attribute-string (normalize "id"))
                (make element gi: "A"
                      attributes: (list
                                   (list "NAME" (attribute-string (normalize "id"))))
                      (process-node-list synop))
                (process-node-list synop)))
      (process-node-list terms)
      (process-node-list listitem))))

(element (apilistentry term) 
  (make sequence
     (process-children-trim)
     (make element gi: "BR" (empty-sosofo))
  ))

(element (apilistentry listitem)
  (make element gi: "DD" (process-children)))

(element errorlist
  (make sequence
    (make element gi: "DIV"
	  attributes: (list (list "CLASS" (gi)))
	  (make element gi: "DL" (process-children)))
    (para-check 'restart)))

(element (errorlist varlistentry term)
  (make element gi: "B"
     (make sequence
       (process-children-trim)
       (make element gi: "BR" (empty-sosofo))
  )))

(element (errorlist varlistentry listitem)
  (make element gi: "DD" (process-children)))

(define %linenumber-mod%
  1)

;; IDList cross-reference
;; there is no need to do the cross-reference because we're going to be
;; grabbing the cross-reference from the name field.
(element idlistxref 
 (let* ((linkend (attribute-string (normalize "linkend")))
	(target (element-with-id linkend))
	)
   (empty-sosofo)))

</style-specification-body>
</style-specification>

<external-specification id="print-stylesheet" document="print-ss">
<external-specification id="html-stylesheet" document="html-ss">

</style-sheet>
