<!--<!DOCTYPE chapter PUBLIC "-//Liberty ARG//DTD DocBook V4.1-Based Extension V1.0//EN">-->
<chapter>
  <title>Domain Interfaces</title>

  <abstract>

    <para>This chapter describes nasty guts of how domains are
    accessed by the simulator.  It also describes some emulator
    decisions.  interfaces between the command-line processor, LSE,
    and domains.</para>

  </abstract>

  <sect1><title>Interface goals</title>

    <para>The goals of the interfaces are to:</para>

    <itemizedlist>
      
      <listitem><para>Make it possible to embed Liberty into other
       systems (such as vertically-integrated systems like
       MILAN).</para></listitem>
      
      <listitem><para>Make it possible to use domains (such as
       emulators) from a variety of sources with limited modifications
       to their source code or even without source code
       availability.</para></listitem>
      
      <listitem><para>Recognize that some domains may have many
       different implementations for its instances and that different
       experimental conditions may involve different domain
       implementations.  In such situations, recompilation should be
       minimized.  For example, compiled-code emulators will produce a
       different library for each benchmark.  Ideally, nothing would
       need recompiled when a different benchmark is to be run except
       for the emulator itself.  </para></listitem>

      <listitem><para>Allow both domain instances and simulators to
       have run-time arguments to change some
       behaviors.</para></listitem>
      
      <listitem><para>Support domain instance libraries written in both
       C++ and C.</para></listitem>
      
    </itemizedlist>

  </sect1>

  <sect1><title>Design principles and decisions</title>

    <para>There are some fundamental principles and design decisions
    driven by the goals:</para>
    
    <itemizedlist>

      <listitem><para>Simulator code is always generated.  It can be
      specialized as needed to fit the situation.  However, it cannot
      be specialized at link time, so the simulator builder must know
      about the domains to be used.  </para></listitem>

      <listitem><para> Command-line parser code should be a library.
      This allows it to be easily replaced by another command-line
      parser, making it possible to embed Liberty into other
      systems.</para></listitem>

      <listitem><para>Linking should be accomplished through C++ to
      ensure that constructors for emulators written in C++ get called
      and that C++ system libraries are included as needed.
      </para></listitem>

      <listitem><para>All interface routines have "C" linkage unless
      the domain is guaranteed to have a single instance and the
      identifiers in the domain implementation libraries are
      guaranteed to be unique.</para></listitem>

      <listitem><para>We are willing to have some inefficiency on
      entry to domain instances in return for flexibility, but expect
      not to lose much because of code specialization on the simulator
      side.</para></listitem>

      </itemizedlist>

  </sect1>

  <sect1><title>Termination conditions</title>

    <para>How does the simulator or CLP know when to terminate?  There
    are three ways to terminate:</para>
    <orderedlist>
      <listitem><para>An error condition occurs or a module or domain
      class or instance requests termination.  In both cases, the
      variable <varname>LSE_sim_terminate_now</> is set to a
      non-zero value (a negative value in the case of errors).</para>
      </listitem>
      <listitem><para>The simulator has no more scheduled timesteps.</para>
      </listitem>
      <listitem><para>The termination count variable
      (<varname>LSE_sim_termination_count</>) reaches zero.
      This variable is initialized to zero at the beginning of
      <function>LSE_simulation_start</>.  Domain classes and instances
      can increment and decrement it.  For example, an emulator may
      increment the count when there is a new execution context
      created and decrement it when a context finishes.  When the
      counter reaches zero at the beginning of a time step, simulation
      terminates.  Domains which change the termination count indicate
      that they do so in their Python class object.  If there are no
      domains which change the termination count, it is initialized to
      <literal>1</> so that this termination condition does not cause
      immediate termination.</para>
      </listitem>
    </orderedlist>

  </sect1>


  <sect1><title>Implications for the build process</title>

    <sect2><title>Multiple-definition identifiers</title>

      <para>The most significant way in which the domain interfaces
      affect the build process is that many identifiers
      (i.e. constants, types, variables, API calls) do not have one
      fixed, global definition.  Instead, these identifiers (such as
      <type>LSE_emu_addr_t</>) depend upon which instance is being
      referenced.</para>

      <para>Figuring out what the proper definition is for a
      particular reference to an identifier is difficult.  There are
      two ways it can be determined:</para>
      <orderedlist>
	<listitem><para>It can be stated explicitly by using a domain
	instance name as the first parameter of the API call or an
	additional parameter on other identifiers, e.g.:
	<literal>LSE_emu_addr_t[<replaceable>myinst</>]</literal>.  The
	name can also be the name of a domain class for class-defined
	identifiers.</para>
	</listitem>
	<listitem><para>It can be found implicitly.  This is possible
	unambiguously when there is no name conflict between
	identifiers in different domain instances or domain classes.
	When there is a conflict, the ambiguity is resolved using a
	"domain instance search path".</para>
	</listitem>
      </orderedlist>

      <para>The way in which this gets implemented is that each domain
      instance must state all of its identifiers to the backend.  The
      backend creates a macro definition for each identifier.  This
      definition translates to a call to a Python domain name
      resolution function, which checks the current domain search path
      to determine what the implicit domain instance should be for the
      identifier.  This procedure works surprisingly well; domain
      search paths can be easily manipulated while generating include
      and framework files to only allow identifiers that should be in
      scope to be manipulated.  Identifiers which are out of scope are
      ignored and passed through to the output code, which prevents
      problems where a "non-interesting" domain overrides an
      identifier in some module.</para>

    </sect2>

    <sect2><title>Implementing identifiers</title> 

      <para>How are identifiers implemented?  Constants and types are
      simple enough; the domain class/instance contains a list of
      constants and types along with their definitions, which are
      placed in <filename>SIM_domain_types.h</>.  APIs and m4 macros
      are a bit more difficult.</para>

      <para>Clearly, we must have a list of APIs and macros so we can
      do the mapping, just as we had for constants and types.  The
      trick is to have two ways to implement things: they can either
      be placed as identifiers with non-<literal>None</>
      implementation (just as constants and types usually are), or
      they can be defined in an m4 "macro" file.</para>

      <para>The structure of the macro file is defined by keywords
      which indicate whether the following section of code is for
      classes or instances, and whether it is for headers, stand-alone
      (one-generation-only) code, and macros (which appear in headers,
      stand-alon code, and modules).  Within each section, there are
      macro calls for defining per-class and per-inst macros and for
      translating identifiers into per-class or per-inst identifiers.
      During database build, after the Python module is read, the
      macro file (if any) is read and separated into sections.</para>

      <note><para>This split occurs without going through m4 first, so
      the keywords cannot be commented out or qualified in any
      way.</para>
      </note>

    </sect2>
    <sect2><title>Hooks</title>

      <para>Hooks are known identifiers that a class or instance can fill with
      code.</para>

    </sect2>

    <sect2><title>Callbacks</title> 

      <para>There are interesting issues when domain implemenations
      are to call functions or access variables provided by the domain
      or by LSE (e.g. <varname>LSE_stderr</>.  First, how do they name
      them, as functions/variables provided by the domain will have
      LSE-munged names?  Second, how do you get a dynamic library to
      build and link properly with the main program when it uses
      variables that are outside?</para>

      <para>The first issue could be resolved with macros supplied to
      domain implementers.  The second is actually possible with the
      right linker command-line flags and use of libtool.  But either
      one is ugly.  A better way to go is to not attempt to statically
      name or link called-back functions or variables.  Instead, the
      domain class passes a structure with pointers to the functions
      and variables to the domain instance library when it is
      initialized.  This solution is simple, requires no special
      linking tricks and prevents libraries from accessing parts of
      the simulator they should not.  The only downside is that use of
      callbacks requires an extra indirect reference.</para>

    </sect2>

    <sect2><title>Linking</title>

      <para>The linker script must search for domain libraries, which
      may be test-case specific.  There are some tricky things to
      worry about:</para>

      <itemizedlist>
	<listitem><para>There could be filename conflicts between
	test-case specific libraries.</para>
	</listitem>
	<listitem><para>There can be symbol conflicts between domain
         libraries</para>
	</listitem>
      </itemizedlist>

      <para>The build process does at least know about the domain
      instances and the nominal library names.</para>

      <para>The linker script deals with symbol conflicts by renaming
      libraries.  Libraries are copied via <command>objcopy</> to
      rename all global symbols in each library by prefixing them with
      <literal>LSEdi_<replaceable>instance_name</></>.  When code is
      generated in the simulator, the renamed name is used for each
      backend identifier.</para>

      <openissue>

	<para>Need to get LD_LIBRARY_PATH right for
           copies of shared libraries.</para>

      </openissue>

    </sect2>

  </sect1>

  <sect1><title>Details of the <domainclass>emulator</> domain
  class</title>

    <para>Emulators are an important domain class
    and have had much debate and thought invested in them.  The goals
    of the emulator interface were:</para>

    <itemizedlist>

      <listitem><para>Make Liberty configurations as ISA-independent
       as possible.</para></listitem>
      
      <listitem><para>Support multi-processing-element systems with
       heterogeneous ISAs.</para></listitem>
      
      <listitem><para>Support weird and wonderful new ISA ideas and
       paradigms for sharing state between execution
       contexts.</para></listitem>
      
      <listitem><para>Allow switching between "pure" emulation and
        detailed simulation.</para></listitem>

      <listitem><para>Benchmarks should be choosable at link-time or
      run-time (link-time for compiled-code emulators, run-time
      otherwise).</para></listitem>

    </itemizedlist>

    <sect2><title>Management of contexts</title>
      
      <para>LSE is responsible for context management.  The basic idea
      is that the simulator and emulators trade opaque tokens which
      allow them to notify each other of events to the context.  For
      example, emulators call the context management library using the
      simulator token when they create new contexts or contexts
      finish.  The simulator uses the emulator token when calling
      emulator functions.</para>

      <para>There is no need here to repeat the information in the
      Developer's Manual and the User's Manual about the difference
      between hardware and software contexts.  What should be pointed
      out is that LSE does need to check the inputs to context
      management API calls to confirm that the context numbers are
      valid.</para>

      <para>Contexts can be identified both by this global context
      number and by an opaque emulator token
      (<type>LSE_emu_ctoken_t</>).  LSE must maintain a mapping from
      number to token for use by various emulator APIs, but need not
      maintain a backwards mapping.  LSE does not attempt to classify
      relationships (e.g. kill children on parent death) among
      contexts.</para>

      <para>LSE does not provide a scheduler for hardware contexts;
      emulators are responsible for maintaining mappings and informing
      LSE of changes to the mappings.  LSE keeps its own copy of the
      global mappings.  There are API calls (rather nasty ones) for
      trying to inform the emulator of how to manage the
      mappings.</para>

      <openissue>
	<para>Another difficult problem in context management is the
        allocation/freeing of resources.  Either contexts must leak
        memory, or some sort of reference counting scheme must be
        used.  Any dynid in a context constitutes a reference, but
        there are also references involved in potential dynid
        generators.  How to deal with this is an open issue.  (For
        now, we leak.)</para>
      </openissue>

      <note><para>Contexts cannot be moved from one emulator instance
      to another.</para></note>

    </sect2>

    <sect2><title>Emulator instances</title>

      <para>Emulator instances are domain instances and, as such, create
      "copies" of an emulator implementation's code and data.
      Different emulator instances are used mainly because some
      emulators may not be able to support multiple contexts
      internally.  Copying the code and data allows the emulator to be
      used multiple times.  Note, however, that when such copying
      occurs, sharing of data between contexts must pass through LSE;
      all of the instances but one must treat the shared state space
      as external.</para>

    </sect2>

    <sect2><title>The <capability>operandval</> capability.</title>

      <para>This capability was a gut-wrencher.  Originally we copied
      source operands into a source value array, stuck results in a
      destination value array, and then wrote back from the result
      array.  This was awfully slow.  We didn't think that mattered
      too much, until we learned that statistical sampling is a
      desirable methodology and that fast-forward speed matters
      immensely then (and found that the IA64 emulator didn't achieve
      1 MIPS on a 733 MHz Pentium III).</para>

      <para>So &hellip; after a lot of thinking, we decided to use
      pointers.  Yes, it's a bit weird when you first think about it.
      Yes, not all operands are treated identically.  But it makes
      data copy a microarchitectural thing, which is more like
      hardware, and happens only during detailed simulation.
      Furthermore, it makes it easier to handle register renaming;
      just change your destination pointers and don't write back
      normally!  In fact, we had no way other than just a lot of
      slogging through data copying to make register renaming work
      before.</para>

      <para>One final thing: the thought did occur that maybe we
      should just not spec this and let each emulator handle it on its
      own.  After all, we don't seem to write generic modules to
      handle operand values.  However, giving a spec gives emulator
      writers some guidance as to how we think they can best achieve
      both performance and flexibility.</para>

    </sect2>

    <sect2><title>Old stuff to figure out how to say</title>

	<para>Stuff to do on emulator init - Create, load, and map
	initial contexts.  Binaries (if needed) and program arguments
	are supplied for each context at this time.</para>

      <sect3><title>Internal APIs for context manipulation (not
      documented elsewhere)</title>

	<apilist>

	  <apilistentry>
	    <funcsynopsis>
		<funcprototype>
		  <funcdef>LSE_emu_contextno_t
                      <function>LSE_emu_context_alloc</></> 
		  <paramdef>int <parameter>emuinstno</></>
		</funcprototype>
	      </funcsynopsis>

	    <listitem><para>Create a new context in emulator instance
	    number <parameter>emuinstno</>.  The context state is
	    <constant>LSE_contextstate_waiting</>.  Returns the global
	    context number for the newly created context.  This
	    function may fail, returning <literal>-1</> if the
	    emulator instance cannot create more
	    contexts.</para></listitem>

	  </apilistentry>

	</apilist>
      </sect3>

    </sect2>

  </sect1>
</chapter>

