.SUFFIXES: .fig .eps .pdf .sgm .htm .dvi

BUILTSGMFILES=corelibMods.sgm archlibMods.sgm archlibMMods.sgm archlibPMods.sgm

SGMFILES=../common/typograph.sgm ../lse.dsl catalog \
	reference.sgm \
	$(BUILTSGMFILES) \
	archlib.sgm \
	chkptapi.sgm \
	clpemuint.sgm \
	corefunc.sgm \
	corelib.sgm \
	datatypes.sgm \
	domainconstr.sgm \
	emuapi.sgm \
	endiannessapi.sgm \
	errors.sgm \
	visapi.sgm \
	IA64emu.sgm \
	idlist.sgm \
	LSSsiminfo.sgm \
	PPCemu.sgm samplerapi.sgm \
	SPARCemu.sgm \

FIGFILES=
EPSFILES=$(FIGFILES:.fig=.eps)
PDFFILES=$(EPSFILES:.eps=.pdf)

default: dvi

all: htm dvi

.fig.eps:
	fig2dev -L eps $< $@

.eps.pdf:
	epstopdf $<


reference.tex: $(SGMFILES)
reference.htm: $(SGMFILES)

.sgm.tex:
	jade -t tex -d ../lse.dsl#print $<

.sgm.htm:
	jade -t sgml -d ../lse.dsl#html $< 

.tex.dvi:
	jadetex $<
	jadetex $<

htm: reference.htm

dvi: reference.dvi

ps: reference.ps

pdf: reference.pdf

print: reference.ps
	lpr reference.ps

print-liberty: reference.ps
	lpr -Php_libertyd reference.ps

print-2nd-floor: reference.ps
	lpr -Php_233 reference.ps

reference.ps: reference.dvi
	dvips -o reference.ps reference.dvi

reference.pdf:reference.tex $(PDFFILES)
	pdfjadetex reference 
	pdfjadetex reference 
	pdfjadetex reference

reference.dvi:reference.tex $(EPSFILES)
	jadetex reference 
	jadetex reference 
	jadetex reference 

LSS_FILES:=$(shell ls $(MODLIB_SRC)/corelib/corelib/*.lss)
SGM_FILES:=$(patsubst $(MODLIB_SRC)/corelib/corelib/%.lss,$(MODLIB_SRC)/corelib/corelib/doc/%.sgm,$(LSS_FILES))

corelibMods.sgm: $(SGM_FILES)
	rm -f $@; \
	if [ "$(MODLIB_SRC)x" = "x" ] ; then \
	  echo "Environment variable MODLIB_SRC undefined, must point to modlib source directory"; \
	  exit -1;\
	fi;\
	echo "" > $@; \
        CURDIR=`pwd`;\
	(for i in $(SGM_FILES); do\
	  sed '/<\!DOCTYPE/D' $$i >> $@; \
	done);\

# archlib has a nasty directory structure as well as extra stuff in it, 
# so I'm going to just look for the sgm files.
ASGM_FILES:=$(shell find $(MODLIB_SRC)/archlib/archlib/doc -name '*.sgm' | sort)
AMSGM_FILES:=$(shell find $(MODLIB_SRC)/archlib/archlib/memory -name '*.sgm' | sort)
APSGM_FILES:=$(shell find $(MODLIB_SRC)/archlib/archlib/predictors -name '*.sgm' | sort)

archlibMods.sgm: $(ASGM_FILES)
	rm -f $@; \
	if [ "$(MODLIB_SRC)x" = "x" ] ; then \
	  echo "Environment variable MODLIB_SRC undefined, must point to modlib source directory"; \
	  exit -1;\
	fi;\
	echo "" > $@; \
        CURDIR=`pwd`;\
	(for i in $(ASGM_FILES); do\
	  sed '/<\!DOCTYPE/D' $$i >> $@; \
	done);\

archlibMMods.sgm: $(AMSGM_FILES)
	rm -f $@; \
	if [ "$(MODLIB_SRC)x" = "x" ] ; then \
	  echo "Environment variable MODLIB_SRC undefined, must point to modlib source directory"; \
	  exit -1;\
	fi;\
	echo "" > $@; \
        CURDIR=`pwd`;\
	(for i in $(AMSGM_FILES); do\
	  sed '/<\!DOCTYPE/D' $$i >> $@; \
	done);\

archlibPMods.sgm: $(APSGM_FILES)
	rm -f $@; \
	if [ "$(MODLIB_SRC)x" = "x" ] ; then \
	  echo "Environment variable MODLIB_SRC undefined, must point to modlib source directory"; \
	  exit -1;\
	fi;\
	echo "" > $@; \
        CURDIR=`pwd`;\
	(for i in $(APSGM_FILES); do\
	  sed '/<\!DOCTYPE/D' $$i >> $@; \
	done);\

clean:
	rm -f $(PDFFILES) $(EPSFILES) *.dvi *.pdf *.ps *.tex *.htm *.log *.aux *.out $(BUILTSGMFILES)

