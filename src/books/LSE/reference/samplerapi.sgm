<!--<!DOCTYPE chapter PUBLIC "-//Liberty ARG//DTD DocBook V4.1-Based Extension V1.0//EN">-->
<chapter id='samplerapi.chapter'>
  <title>Sampling Interface</title>

  <abstract>

    <para>This chapter describes datatypes, structure fields, and API
    functions used to access sampling functionality in the Liberty
    Simulation Environment.  This interface is made available by
    the <domainclass>LSE_sampler</> domain class.  All identifiers in
    the interface are in the <varname>LSE_sampler</> namespace and
    should be qualified with the namespace name.  There is never more
    than one instance or implementation of this domain class.</para>

  </abstract>

  <sect1 id='samplerapi.typesfieldsconstants'>
    <title>Datatypes, structure fields, and constants</title>

    <para id='LSE.sampler.state.t'>
    <inlinehead>state_t</inlinehead> is an enumerated type
    listing the sampler state machine states.  It is defined
    as:</para>
<programlisting>
typedef enum {
  state_forward = 0,
  state_warmup  = 1,
  state_collect = 2,
  state_recover = 3,
} state_t;
</programlisting>

<para id='LSE.sampler.sampler.t'><inlinehead>sampler_t</inlinehead> is a
  class representing a sampler state machine.  It contains the following
  fields:</para>
<programlisting>
class sampler_t {

   /* parameters */
   int64_t period;             /* state machine period */
   int64_t warmup;             /* events in warmup state */
   int64_t length;             /* events in collect state */
   int64_t first;              /* first event in collect state */

   state_t state;              /* current state */

   int64_t sampleCount;        /* number of times recover has been entered */
   int64_t eventsToGo;         /* number of events remaining before 
                                  a state transition */
} sampler_t;
</programlisting>
<para>The <type>sampler_t</> class has the following methods:</para>

    <apilist>

      <apilistentry id='LSE.sampler.create'>
	<funcsynopsis>
	    <funcprototype>
	      <funcdef><function>sampler_t</></funcdef>
	      <paramdef>int64_t <parameter>period</></paramdef>
	      <paramdef>int64_t <parameter>length</></paramdef>
	      <paramdef>int64_t <parameter>warmup</></paramdef>
	      <paramdef>int64_t <parameter>first</></paramdef>
	    </funcprototype>
	</funcsynopsis>
	<listitem>
	  <para>Constructs a new sampler state machine with the
	  given parameters.  The parameters may be modified by the
	  function in two ways. First, negative values are changed to
	  0.  Second, an invariant is made to be true: <literal>period
	  >= warmup + length</literal>.  <parameter>Warmup</> is
	  reduced first, then <parameter>length</>, until the
	  invariant is satisfied.  The meaning of the resulting
	  parameter values is:</para>
	  <table>
	    <title>Sampler parameters</title>
	    <tgroup cols="5">
	      <colspec colwidth="10*" align="center">
	      <colspec colwidth="10*" align="center">
	      <colspec colwidth="10*" align="center">
	      <colspec colwidth="15*" align="center">
	      <colspec colwidth="55*">
	      <thead>
		<row>
		  <entry><parameter>period</></entry>
		  <entry><parameter>length</></entry>
		  <entry><parameter>warmup</></entry>
		  <entry><parameter>first</></entry>
		  <entry>Behavior</entry>
		</row>
	      </thead>
	      <tbody>
		<row>
		  <entry>0</entry>
		  <entry>&mdash</entry>
		  <entry>&mdash</entry>
		  <entry>0</entry>
		  <entry>Always in "collect"</entry>
		</row>
		<row>
		  <entry>0</entry>
		  <entry>&mdash</entry>
		  <entry>&mdash</entry>
		  <entry>F > 0</entry>
		  <entry>"forward" for F events, "warmup" for 0 events,
	           then always in "collect"</entry>
		</row>
		<row>
		  <entry>P > 0</entry>
		  <entry>L</entry>
		  <entry>W</entry>
		  <entry>0</entry>
		  <entry>repeat: "collect" for L events, "recovery",
	           "forward" for P-L-W events, "warmup" for W
	           events.</entry>
		</row>
		<row>
		  <entry>P > 0</entry>
		  <entry>L</entry>
		  <entry>W</entry>
		  <entry>0 < F <= W</entry>
		  <entry>warmup for F events, then repeat: "collect" for L
	            events, "recovery", "forward" for P-L-W events, "warmup"
	            for W events.</entry>
		</row>
		<row>
		  <entry>P > 0</entry>
		  <entry>L</entry>
		  <entry>W</entry>
		  <entry>F > W</entry>
		  <entry>forward for (F-W events), then repeat: "warmup"
	           for W events, "collect" for L events, "recovery",
	           "forward" for P-L-W events.</entry>
		</row>
	      </tbody>
	    </tgroup>
	  </table>
	</listitem>
      </apilistentry>      

      <apilistentry id='LSE.sampler.advance'>
	<funcsynopsis>
	    <funcprototype>
	      <funcdef>void <function>advance</></funcdef><void>
	    </funcprototype>
	</funcsynopsis>
	<listitem>
	  <para>Advance state machine <parameter>samp</> to the next
	  state, and adjust the <structfield>eventsToGo</> field for
	  the next transition.</para>
	</listitem>
      </apilistentry>


      <apilistentry id='LSE.sampler.notify'>
	<funcsynopsis>
	    <funcprototype>
	      <funcdef>bool <function>notify</></funcdef>
	      <paramdef>int64_t <parameter>nev</></paramdef>
	    </funcprototype>
	</funcsynopsis>
	<listitem>
	  <para>Subtract <parameter>nev</> events from the
	  <structfield>eventsToGo</> field of the state machine and
	  transition to a new state as needed.  If <parameter>nev</>
	  is 0, state transitions are still possible (when
	  the <structfield>eventsToGo</> field was <= 0).  When state
	  transitions occur, the
	  <structfield>eventsToGo</> field is adjusted for the next
	  transition.  Returns <constant>true</> if a state
	  transition occurs, <constant>false</> otherwise.</para>
	</listitem>
      </apilistentry>

    </apilist>
  </sect1>
</chapter>

