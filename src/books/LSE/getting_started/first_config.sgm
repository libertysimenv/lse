<!--<!ENTITY lfsr SYSTEM "3-bit lfsr.doc" NDATA DOC>
<!ENTITY lse SYSTEM "lse.doc" NDATA DOC>
<!ENTITY control SYSTEM "control.doc" NDATA DOC>
-->
<chapter id='fc.chapter'><title>A First Machine Specification</title>

<para>
The Liberty Simulation Environment (LSE) is a suite of tools that
generates a simulator executable automatically from a structural
system specification.  A model is specified by instantiating
components called modules and then connecting these module instances
together.  Each module instance has a set of parameters that can
control the specifics of its behavior at simulation time.
</para>

<para>
The generation process of a simulator uses these different pieces of
user input (module behavior and structural specification) in two
separate phases of compilation.  This two-phase compilation process is
shown in <xref linkend="figure.lseover">.  This chapter walks through
the construction and use of a simple structural specification and
relies on the core module library to provide modules and their
behavioral specification.
</para>
<figure id="figure.lseover">
<title>LSE Compilation Overview</>
<mediaobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/lseover" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/lseover.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>

<sect1 id = section.simple.config>
<title>The Initial Specification</title>
<para>
<xref linkend="config"> shows a small specification that connects an
instance of the <modulename>source</> module named <literal>gen</>
that generates data to an instance of the <modulename>sink</> module name
<literal>hole</> that consumes data and throws it away.
</para>
<example id="config">
<title>A first LSE specification</title>
<programlisting linenumbering="numbered">
using corelib;

instance gen:source;
instance hole:sink;

gen.out -&gt;[int] hole.in;
</programlisting>
</example>

<para>
Line 1 in the example code tells the specification compiler (referred
to as LSS) to import the <packagename>corelib</> package.
<packagename>corelib</> defines a set of very useful basic modules.
(More information on the modules in <packagename>corelib</> can be
found in the Core Module Library Reference.)
</para>
<para>
 The <literal>using</> keyword tells LSS to not only import the
package, but add all the entities in the package (such as module
definitions, type definitions, etc.) to the current namespace (the
semantics are similar to C++'s <literal>using</> statement).
</para>
<para>
If we wanted to not include the names in the local namespace but load
the package for use, we could write instead:
<programlisting>
import corelib;
</programlisting>
In this case, references to things inside corelib would have to have
an absolute name, such as <literal>corelib::source</>, instead of just
<literal>source</>.  More information on packages and naming can be
found in &USERMAN; in the <citetitle pubwork="chapter">LSS
Reference Appendix</>.
</para>

<para>
Lines 3 and 4 instantiate the <modulename>source</> and the
<modulename>sink</> module.  The syntax for instantiating a module is
<programlisting>
instance <replaceable>instance name</>:<replaceable>module name</>;
</programlisting>
This will create an instance named <replaceable>instance name</> in
the system using the module definition named <replaceable>module
name</> as a template for how <replaceable>instance name</> should be
built and how it should behave at simulator run time.
</para>

<para>
In our example, the <modulename>source</> module is a template for an
instance that generates some data, and the <modulename>sink</> module
is a template for an instance that will consume and discard whatever
data is sent to it in a cycle.  Both these modules are
<emphasis>polymorphic</emphasis>.  This means that instances created
from these modules can have any type on their ports, though the type
must be fixed for a given instance.
</para>

<para>
Line 6 in the specification specifies that the <portname>out</> port
of the <literal>gen</> instance should be connected to the
<portname>in</> port of the <literal>hole</> instance.
</para>
<para>
A connection automatically forces the types of the two connected ports
to be identical (recall that the <modulename>source</> and
<modulename>sink</> modules are polymorphic).  The item inside the
brackets is a constraint that indicates that the ports on either side
of the connection must have type <type>int</>. LSS can use this
constraint during type inference to infer the types for the
<portname>in</> and <portname>out</> ports of the instances
(<literal>gen</> and <literal>hole</>).
</para>

<para>
The general syntax for making a connection is 
<programlisting>
<replaceable>instancename.portname</> -&gt;[<replaceable>type constraint</>] <replaceable>instancename.portname</>
</programlisting>
The <literal>[<replaceable>type constraint</>]</> part is optional and
may be omitted.  More powerful connection and type inference features
are available.  See &USERMAN;, LSS Reference Appendix for
more details.
</para>
</sect1>

<sect1>
<title>Visualization</title> 
<para>
The LSE system has a visualizer that can be used to view a
specification (though not edit it visually).  Save the text in <xref
linkend="config"> to a file named <filename>firstspec.lss</>.  To view
the specification in <xref linkend="config">, type
<userinput>visualizer firstspec.lss</> on the command line.  You
should see two windows that are similar to those in <xref
linkend="figure.firstspec-initial">.
</para>
<figure id="figure.firstspec-initial">
<title>LSE Visualizer Initial Windows</title>
<mediaobject>
  <imageobject>
    <imagedata scale="40" scalefit="1" fileref="figures/visualizer_init" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scale="40" scalefit="1" fileref="figures/visualizer_init.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>
<para>
By clicking on the second icon from the left on the tool bar in the
window that shows the source code, you should see a dialog box where
build messages appear.  After clicking ok, a window which shows a
picture of the specification will appear.  The visualization of the
specification is shown in <xref linkend="figure.firstspec-visual">.
</para>
<figure id="figure.firstspec-visual">
<title>LSE Visualizer Visualization of <xref linkend="config"></title>
<mediaobject>
  <imageobject>
    <imagedata scale="40" scalefit="1" fileref="figures/visualizer_firstconfig" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scale="40" scalefit="1" fileref="figures/visualizer_firstconfig.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>
<para>
The blue boxes in the figure are the module instances, the grey boxes
are the ports.  The line connecting the <portname>out</> port to the
<portname>in</> port shows the connection.
</para>
<para>
More information on using the visualizer can be found in the
<citetitle>Static visualization of LSE Configurations</> of &USERMAN; and by typing <userinput>visualizer -h</>.
</para>
</sect1>

<sect1>
<title>Building and Running a Simulator</title>
<para>
To build this specification, at the command prompt type
<userinput>ls-build firstspec.lss</userinput>.
</para>
<para>
The ls-build script should send output similar to the following to the screen:
<screen>
ls-build==> creating directory machines
Machine output directory is .../machines/firstspec
ls-build==> echo $CFLAGS
 
ls-build==> /bin/rm -fr .../machines/firstspec/valid_machine
ls-build==> /bin/rm -fr .../machines/firstspec/database/SIM_domain_info.py
ls-build==> somedir/lss -o .../machines/firstspec -m ...
Processing Instance gen
Processing Instance hole
Performing Type Inference
Type Inference complete
....
ls-build==> touch valid_machine
</screen>
</para>
<para>
To link this specification to form an executable simulator, type
<userinput>ls-link firstspec.lss</>.
The output will be:
<screen>
Machine directory is .../machines/firstspec
Output directory is ...
ls-link==> find .../machines/firstspec/MODULES -name *.o -print
ls-link==> Writing linker file
ls-link==> g++ -g  ...
</screen>
There may be a warning regarding <literal>tmpnam_r</>. The warning may
or may not appear on your system.  If it appears it is ok, if not,
that's fine too.
</para>
<para>
At this point there should be a file in the current directory called
<filename>Xsim</>.  This program is the built simulator binary.  Run
<filename>Xsim</> by typing <userinput>./Xsim</> on the command line.
The simulator should hang.  The simulator hangs because you have not
told the simulator when to stop running.  Press <keycap>control</>-C
to stop it.</para>

<para>
In general, a simulator is built by typing <userinput>ls-build
<replaceable>filename</></>, followed by <userinput>ls-link
<replaceable>filename</></>.  Typing <userinput>ls-build -h</> and
<userinput>ls-link -h</> will give more options and instructions on
using these commands.
</para>
</sect1>

<sect1 id= "section.instrumentation">
<title>Instrumentation</title>
<para>
Right now, the simulator does little that is interesting and we don't
know what data is being sent on the signal.  To see what data is
being sent on the signal, we need to instrument the simulator to see
what data is transmitted on the connection from the port
named <portname>in</> to the port named
<portname>out</>.  This section will describe how to do this.
</para>

<para>
To begin, add the following code to <xref linkend="config">.
<programlisting linenumbering="numbered">
collector out.resolved on "gen" {
    header = &lt;&lt;&lt;
#include &lt;stdio.h&gt;
           &gt;&gt;&gt;;
    record = &lt;&lt;&lt;
      if (LSE_signal_data_known(status) &&
          !LSE_signal_data_known(prevstatus)) {
        printf(LSE_time_print_args(LSE_time_now));
        if(LSE_signal_data_present(status)) {
          printf(": %d\n", *datap);
        } else {
          printf(": Nothing sent\n");
        }
      }
    &gt;&gt;&gt;;
};
</programlisting>
</para>

<para>
Now, rebuild the <filename>firstspec.lss</> file with the added
code and relink it.  Running Xsim should yield the following output:
<screen>
0/0: Nothing sent
1/0: Nothing sent
2/0: Nothing sent
3/0: Nothing sent
4/0: Nothing sent
&hellip;
</screen>
</para>

<para>The simulator runs forever and needs to be stopped
with <keycap>control</>-C.  This result is not suprising since
instrumenting the simulator should not alter its behavior.  However,
there is an additional output line for each clock cycle, looking
like:<literal>0/0: Nothing sent</literal>.
</para>

<para>
An examination of lines 8 and 12 reveals the source of this additional
output.  The printf on line 8 is outputting the <literal>0/0</> and
the printf on line 12 is outputting <literal>: Nothing sent</>.  The
next few sections explain what the code above is doing and why the
output is what it is.
</para>

<sect2>
<title>Collecting Data</title>
<para>
Line 1 starts a declaration for a data collector.  The
<literal>out.resolved</> specifies which information this collector
wishes to monitor.  In this case, the collector is monitoring changes
on the status of signals on the <portname>out</> port on the instance
<literal>gen</>.  The last item before the <literal>{</> is a string
that tells which module instance to monitor.
</para>
<para>
The general syntax for a data collector is
<programlisting>
collector <replaceable>eventname</> on <replaceable>instancename</> {
  <replaceable>collectorbody</>
};
</programlisting>
</para>

<para>
Within the curly braces is the body declaring what the collector does.
Line 2 assigns, to the <literal>header</> field of a collector, a
string that can include whatever C++ header files are needed by the
code in the rest of the collector.  In this case we include
<filename>stdio.h</> for <function>printf</>.  (Actually, this was
unnecessary here as <filename>stdio.h</> is automatically included by
LSE.)  The characters enclosed in <literal>&lt;&lt;&lt;</> and
<literal>&gt;&gt;&gt;</> are also strings (in the LSS language) but
they can span multiple lines and have special properties if they
contain a substring that begins with <literal>${</> and ends with
<literal>}</>.  More information is available in later chapters and in
the LSS Reference Appendix of &USERMAN;.  For now, we can consider
this a simple string.
</para>

<para>
Line 5 assigns, to the <literal>record</> field of the collector, a
string that contains code that will be run each time the status of the
<portname>out</> port changes on instance <literal>gen</>.  This code
is <emphasis>not</> LSS code, but instead, code in LSE's behavioral
specification language (BSL) which is used to specify runtime behavior
of leaf module instances (as opposed to hierarchical module instances
which are composed from other modules) and other entities, such as
data collectors.
</para>

<para>
The BSL is an augmented subset of the C++ language.  Most "normal" C++
code is permissible.  In addition to normal C++, additional data types,
LSE API calls, and macros are available for use.  
</para>

<para>
In the above collector, the record code on line 6 first checks to see
if the data signal<footnote><para> As we will see in a later chapter, each
connection in LSE carries three signals: a primary data signal and an
enable and ack signal used for abstracting and providing default
control semantics.
</para></footnote>
on <portname>out</> is known.  The <literal>status</> variable
contains the status of all signals on the <portname>out</> port for
this call of the record code of the collector.  If the data signal
value is known, line 7 checks to see if the data was known last time
the record code was invoked by inspecting the status in the
<literal>prevstatus</>.  This prevents the body of the if statement,
lines 8-13, from being executed more than once, since the only time
the previous and current status can be different is the first time the
data signal becomes known <footnote><para> It is illegal, according to LSE's
semantics, for a signal value to go from unknown to known and back to
unknown again within the same clock cycle.  Each signal can be assigned at
most one known value per cycle.  The framework will automatically reset the
value of <literal>prevstatus</> to unknown for all signals at the start of each
cycle.  </para></footnote>.
</para>

<para>
The next few lines, lines 8-13, create the output the first time in a
cycle that the data signal is known.  Line 8 prints out the current
simulation cycle.  Since the time data type is opaque in LSE, there is
a special macro,
<literal>LSE_time_print_args(<replaceable>timevar</>)</> that will
generate the correct arguments for a call to printf to print any
variable that stores simulation time.  <literal>LSE_time_now</> is a
variable, globally available to any BSL code, that has the current
simulation time.
</para>
<para>
Line 9 checks to see if any data was sent this cylcle.  In general,
the data signals in LSE can have an unknown value, and two known values,
<constant>LSE_signal_something</> and
<constant>LSE_signal_nothing</><footnote><para> It is also illegal for a
signal to transition from nothing to something or vice-versa in a
given clock cycle. </para></footnote>.  If the <literal>gen</> instance sent
data on its output this cycle, line 10 is invoked and the data is
printed.  Otherwise, line 12 is invoked, producing the output seen
earlier.
</para>
<para>
Based on this discussion, we see that the <literal>gen</> instance is
outputting no data in any cycle.
</para>
<para>
Collectors also have three additional fields called <literal>decl</>,
<literal>init</>, and <literal>report</>.  The code in the
<literal>decl</> field is used to define variables needed by the data
collector.  The code for the <literal>init</> field is called at
simulator startup to initialize and data collection mechanisms.  The
code in the <literal>report</> field is called once the simulation
finishes and can be used to report any statistics collected using the
<literal>report</> field in the collector.
</para>
</sect2>

<sect2>
<title>Events</title>
<para>
Each instance in an LSE specification may emit <emphasis>events</>
that can be used by <emphasis>data collectors</> to compute
statistics.  There is a set of standard events for every module, and a
set of module specific events for an instance that is specified in the
module declaration.  The data collector in the previous section used
<replaceable>portname</>.<literal>resolved</> event.  This is a system
defined event on each instance that has a port named
<replaceable>portname</>.
</para>
<para>
Each event in the system defines a tuple of data that will be sent to
collector's record code each time the event fires.  For the
<literal>resolved</> event, the following data with the listed type is
sent:
<programlisting>
  int porti
  LSE_signal_t status
  LSE_signal_t prevstatus
  <replaceable>porttype</>* datap
</programlisting>
The <literal>porti</> parameter is an integer specifying the
port instance index.  Port instances are discussed in the next
chapter.  In this example <literal>porti</> is 0, since there is
only one connection to the port.  The <literal>status</> variable
contains the status of data arrival on the port instance (as discussed
earlier and discussed in more detail in the next chapter).  The
<literal>prevstatus</> variable contains the status of the port
instance the last time the collector code was called.  The
<literal>datap</> variable is a pointer to the data sent.  It is
<literal>NULL</> if the data signal value for the port instance is
unknown or <constant>LSE_signal_nothing</>.
</para>

<para>
More information on collectors can be found in the next chapter.
</para>
</sect2>
</sect1>

<sect1>
<title>Parameters and Userpoints</title>
<para>
<xref linkend="config"> is not particularly interesting since the
simulation does not do much of anything.  In this section, we will
modify the behavior of the source instance via parameters to generate
more interesting behavior.
</para>

<para>
Modules can declare parameters for their instances.  These parameters
can be set by the user to customize the behavior of a particular
instance.  In particular, LSE supports an unusual type of parameter
called a <emphasis>userpoint</>.  A userpoint parameter declares, on
the instance, a BSL function whose body is the value of the userpoint.
The value of the userpoint is simply a string that is valid BSL code
for a function body.
</para>

<para>
The <modulename>source</> module defines a userpoint parameter called
create_data which contains code to create the data that will be output
each cycle by the module.  We will get the source module to output the
current cycle number on each simulator clock cycle.  The code to do
this is shown below.
<programlisting linenumbering="numbered">
gen.create_data = &lt;&lt;&lt;
  *data = LSE_time_get_cycle(LSE_time_now);
  return LSE_signal_something | LSE_signal_enabled;
&gt;&gt;&gt;;
</programlisting>
</para>

<para>
Building and running the specification with these lines added gives the
following output:
<screen>
0/0: 0
1/0: 1
2/0: 2
3/0: 3
4/0: 4
5/0: 5
6/0: 6
7/0: 7
8/0: 8
9/0: 9
...
</screen>
And again the simulation needs to be terminated with an interrupt
signal (i.e. using <keycap>control</>-C).  The next few paragraphs
explains what is happening.
</para>

<para>
Line 1 assigns a string to the <modparam>create_data</> parameter of
the <literal>gen</> instance.  
</para>

<para>
The function whose body this parameter defines has several arguments.
The <literal>data</> argument is used in the function body defined
above.  It is a pointer to the data value that is to be output.  Line
2 sets this value to the current cycle number (chopping off the
phase).  
</para>
<para>
The return value of this function is used by <literal>gen</> to
determine what the output port status should be.  Here, the status
will always be <constant>LSE_signal_something</> (discussed earlier),
and <constant>LSE_signal_enabled</>.  This second value is a value for
the enable signal on the output port.  This tells all consumers of the
data that it is ok to latch this data at the end of cycle.  More
information about the enable signal will be discussed in the next
chapter.
</para>
<para>
In the original code, the default value for this parameter was used
(since no override was specified).  The default code simply returns
<constant>LSE_signal_nothing</> ORed with
<constant>LSE_signal_disabled</>.  This explains why data was never
sent before.
</para>

<para>
The simulation continues forever because the simulator has never been
told to terminate.  The <varname>LSE_sim_terminate_now</> variable is
used to request termination.  Setting this variable to
<literal>TRUE</> always terminates simulation at the end of the
current timestep.  However, when using emulation, the simulation may
terminate earlier. (This is discussed further in a later chapter).
</para>
</sect1>
<sect1>
<title>Full Specification</>
<para>
The full listing for the final code built up in this chapter is shown
below.  All the calls made in BSL code in the above configuration are
documented fully in &REFMAN;
</para>
<programlisting linenumbering="numbered">
using corelib;
 
instance gen:source;
instance hole:sink;
 
gen.create_data = &lt;&lt;&lt;
  *data = LSE_time_get_cycle(LSE_time_now);
  return LSE_signal_something | LSE_signal_enabled;
&gt;&gt;&gt;;
 
gen.out -&gt;[int] hole.in;
 
collector out.resolved on "gen" {
    header = &lt;&lt;&lt;
#include &lt;stdio.h&gt;
           &gt;&gt;&gt;;
    record = &lt;&lt;&lt;
      if (LSE_signal_data_known(status) &&
          !LSE_signal_data_known(prevstatus)) {
        printf(LSE_time_print_args(LSE_time_now));
        if(LSE_signal_data_present(status)) {
          printf(": %d\n", *datap);
        } else {
          printf(": Nothing sent\n");
        }
      }
    &gt;&gt;&gt;;
};
</programlisting>
</sect1>
</chapter>

