<!--<!ENTITY lfsr SYSTEM "3-bit lfsr.doc" NDATA DOC>
<!ENTITY lse SYSTEM "lse.doc" NDATA DOC>
<!ENTITY control SYSTEM "control.doc" NDATA DOC>
-->
<chapter>
<title>An LFSR Specification</title>
<para>
This chapter presents a walk through of a simple 3-bit linear feedback
shift register (LFSR) to reinforce the concepts above and to introduce
some additional features of LSE.  In the next chapter a simple
microprocessor model is assembled using an emulator for the IA64
instruction set.
</para>

<sect1>
<title>The Specification</title> 

<para><xref linkend="lfsr-config"> shows the specification for a very
simple 3-bit linear feedback shift register (LFSR) that will be
discussed in this chapter.
</para>

<example id="lfsr-config">
<title>A specification for a 3-bit LFSR</title>
<programlisting linenumbering="numbered">
using corelib;
include "xor_gate.lss";

instance bit0 : delay;
instance bit1 : delay;
instance bit2 : delay;
instance xor : xor_gate;
instance bit1_tee : tee;

bit0.initial_state = &lt;&lt;&lt; *init_id = LSE_dynid_create(); 
                         *init_value = 1; 
                         return TRUE; &gt;&gt;&gt;;
bit1.initial_state = &lt;&lt;&lt; *init_id = LSE_dynid_create(); 
                         *init_value = 1; 
                         return TRUE; &gt;&gt;&gt;;
bit2.initial_state = &lt;&lt;&lt; *init_id = LSE_dynid_create(); 
                         *init_value = 1; 
                         return TRUE; &gt;&gt;&gt;;

bit2.out -&gt; bit1.in;
bit1.out -&gt; bit1_tee.in;
bit1_tee.out[0] -&gt; xor.in0;
bit1_tee.out[1] -&gt; bit0.in;
bit0.out -&gt; xor.in1;
xor.out -&gt; bit2.in;

collector STORED_DATA on "bit2" {
  header=&lt;&lt;&lt;
#include &lt;stdio.h&gt;
  &gt;&gt;&gt;;
  
  record=&lt;&lt;&lt;
      printf(LSE_time_print_args(LSE_time_now));
      printf(": bit2=%d\n", *datap);
  &gt;&gt;&gt;;
};

collector STORED_DATA on "bit1" {
  header=&lt;&lt;&lt;
#include &lt;stdio.h&gt;
  &gt;&gt;&gt;;
  
  record=&lt;&lt;&lt;
      printf(LSE_time_print_args(LSE_time_now));
      printf(": bit1=%d\n", *datap);
  &gt;&gt;&gt;;
};

collector STORED_DATA on "bit0" {
  header=&lt;&lt;&lt;
#include &lt;stdio.h&gt;
  &gt;&gt;&gt;;
  
  record=&lt;&lt;&lt;
      printf(LSE_time_print_args(LSE_time_now));
      printf(": bit0=%d\n", *datap);
  &gt;&gt;&gt;;
};


</programlisting>
</example>
<para>
The LFSR in <xref linkend="lfsr-config">, when clocked, moves bits
from the higher order state elements (delay2 being the MSb) to the
lower order state elements.  The outputs of the lower-order two bits
are xor-ed together and the result is fed back into the most
significant bit (see <xref linkend="lfsr-fig">).
</para>

<figure id="lfsr-fig"><title>A 3-bit LFSR</title>
<mediaobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/lfsr" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/lfsr.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>

<para>
A visualization of this configuration is shown if <xref linkend="lfsr-vis">.
</para>
<figure id="lfsr-vis"><title>Visualization of <xref linkend="lfsr-config"></title>
<mediaobject>
  <imageobject>
    <imagedata scale="40" scalefit="1" fileref="figures/visualizer_lfsr" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scale="40" scalefit="1" fileref="figures/visualizer_lfsr.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>
</sect1>

<sect1>
<title>New Modules</>
<para>
The first thing to notice in this configuration is that the it uses a
set of new modules.  The <modulename>delay</> and <modulename>tee</>
modules are from the standard module library (and are fully documented
in the Core Module Library Reference, along with the <modulename>source</>
and <modulename>sink</> modules).  The <modulename>xor_gate</> module
is actually a module composed of modules from <packagename>corelib</>.
</para>

<para>
As mentioned before, the state elements of the LFSR are modeled by the
<modulename>delay</> module instances.  This module takes data coming
in on its <portname>in</> port and stores it.  The data stored in the previous
cycle is output this cycle (the actual behavior of this module is a
bit more complex, but those details will be described shortly).  The
<literal>bit1_tee</> module fans out the output of <literal>bit1</> to
the input of <literal>bit2</> and the xor gate.  The definition of the
<modulename>xor_gate</> module is included by line 2.  A discussion
this module appears later in this chapter.
</para>
</sect1>

<sect1>
<title>Multiports</>
<para>
The next unusual thing to notice is that the <portname>out</> port of
the <literal>bit1_tee</> instance has multiple connections made to it
(this is clearly visible in <xref linkend="lfsr-vis">.  These multiple
connections are made by lines 22 and 23.  Notice that these lines
specify an array like index for the <portname>out</> port of the instance.
</para>
<para>
Each port in LSE is actually an array of port instances called a
multiport.  The size of this array is called the <emphasis>width</> of
the port and is denoted
<portname><replaceable>portname</></>.<literal>width</>.  Each port
instance behaves as described in the previous chapter.  In fact, in
the previous chapter, most of the time the word port was used, it is
more correct to say port instance.  Each reference to the port in
<xref linkend="config"> could have had <literal>[0]</> attached after
the name of the port, and the specification would be equivalent.
</para>

<para>
In this example, The <literal>bit1_tee</> instance of the
<modulename>tee</> module takes any data on the input port instance
and sends it to all the output port instances, in this case
<portname>out[0]</> and <portname>out[1]</>.  The behavior of
<modulename>tee</> module instances is more complex if the width of
the <portname>in</> port is greater than 1.  This behavior is
documented in the Core Module Library Reference.
</para>

<para>
If the index on the port is omitted, LSS will infer an index for the
port automatically.  The indexes will be assigned in increasing order,
based on the order in which expressions containing the port reference
are evaluated.  Details are in the LSS Reference Appendix of
&USERMAN;.  Making more than one connection to a single port instance
is illegal.  Also, mixing implicit and explicit indexing for a given
port is illegal.
</para>
</sect1>

<sect1>
<title>Initial Values and Dynamic Identifiers</> 
<para>
Instances of the <modulename>delay</> module have a parameter called
<modparam>initial_state</> that contains code that can define the
initial state for the instances.  By default, the state elements start
out holding no data, and thus will output
<literal>LSE_signal_nothing</>.  Given that the delay elements are
connected in a sequential loop, this means that no delay element will
ever get data (Given two <constant>LSE_signal_nothing</>s the
<modulename>xor_gate</> module will output
<constant>LSE_signal_nothing</>).
</para>

<sect2>
<title>Initial Value</>
<para>The code on lines 10-12 initializes the value of the bit2 to
TRUE. (The delay elements store booleans since, as we will see, the
<modulename>xor_gate</> instances only accept <type>boolean</> on
its ports.) 
</para>

<para>
The <modparam>initial_state</> user point has two arguments,
<literal>init_id</> and <literal>init_value</>.  The
<literal>init_value</> argument is fairly straight forward.  It is a
pointer where the user specified code should place the initial value.
The <literal>init_id</> argument will be explained in the next
section.  The code should return <constant>TRUE</> if there is an
initial value, <constant>FALSE</> otherwise.</para>
</sect2>

<sect2>
<title>Dynamic Identifiers</title>
<para>
In addition to the the data signal, ack signal, enable signal (recall
that discussion of ack and enable was deferred earlier), and data
value, each message sent on a port instance also has a dynamic
identifier.  This dynamic identifier is created by certain instances
and passed from input to output on most other instances.  The specific
behavior is dependent on the module.  It can be used for a variety of
purposes during simulation.  For example, a common use is to have a
dynamic identifier for each dynamic instruction executing in a
processor.
</para>
<para>
The <literal>*init_id = LSE_dynid_create();</> statement on line 10
creates a new dynamic identifier that will be output with the initial
value.  Further discussion of the dynamic identifier is deferred to a
later chapter that makes use of the construct.  For now, it suffices
to know that it exists and must be present if the data signal is
<constant>LSE_signal_something</>.
</para>
</sect2>
</sect1>

<sect1>
<title>Examining the Output</>
<para>
Lines 27-58 are collectors that output the value of each bit every
time a new data element is stored (even if the new and old value are
the same).  This is done using the <literal>STORED_DATA</> event on
the delay element which occurs every time a new data element is stored.
The record code is passed the port instance in <literal>porti</>, the
stored dynamic identifier in <literal>id</>, and the stored data value
in <literal>datap</> which is a pointer to the data.
</para>
</sect1>

<sect1>
<title>Running the simulator</>
<para>
Following the earlier procedure, this specification can be compiled
into an executable simulator using <literal>ls-build lfsr.lss</> and
<literal>ls-link lfsr.lss</>.  Running the simulator, we get the
following output:
<screen>
Unknown port status at time 0
==== Dumping port status ====
Instance bit0:
  Port in:
        global  : dYeUaU,
  Port out:
        global  : dYeUaU,
Instance bit1:
  Port in:
        global  : dYeUaU,
  Port out:
        global  : dYeUaU,
Instance bit1_tee:
  Port in:
        global  : dYeUaU,
  Port out:
        global  : dYeUaU, dYeUaU,
Instance bit2:
  Port in:
        global  : dYeUaU,
  Port out:
        global  : dYeUaU,
Instance xor.gate:
  Port in0:
        global  : dYeUaU,
  Port in1:
        global  : dYeUaU,
  Port out:
        global  : dYeUaU,
CLP: Error -3 returned from LSE_sim_engine
Finish time: 1/0
</screen>
</para>
</sect1>
<sect1>
<title>Control, Data, Enable, and Ack</>
<para>
The output seen above is an error message from the simulator framework
complaining that at the end of a cycle, certain signal values failed
to resolve (i.e. change from <constant>LSE_signal_unknown</>).  Recall
that, in LSE, each port instance carries three signals, data, enable,
and ack.  All signals can have the value
<constant>LSE_signal_unknown</>.  Each signal has two additional
states; for data these are <constant>LSE_signal_something</> or
<constant>LSE_signal_nothing</>, for enable,
<constant>LSE_signal_enabled</> or <constant>LSE_signal_disabled</>,
and for ack, <constant>LSE_signal_ack</> and
<constant>LSE_signal_nack</>.  If data is
<constant>LSE_signal_something</> then there is an associated dynamic
identifier and possibly a data value.
</para>

<para>In the above output a <literal>dY</> indicates that the data
signal on the port instance was <constant>LSE_signal_something</>,
<literal>dN</> means that the data was
<constant>LSE_signal_nothing</>, and <literal>dU</> means
<constant>LSE_signal_unknown</>.  Similar rules apply for enable and
ack, except with the prefix <literal>e</> and <literal>a</>
respectively. Thus, we see that all the enable and ack values are unknown.
</para>

<para>The data and enable signal travel from output port instance to
input port instance.  However, the ack signal flows from the input
port instance to the output port instance.  These signals are used by
the modules (in their default behavior) to create pipeline like
back pressure.
</para>

<para>
In the configuration above, this means that if <literal>bit0</> has
data and <literal>bit2</> refuses to accept new data, then,
<literal>bit0</> will also refuse new data.  
</para>

<para>
The mechanism through which this is achieved is illustrated for the
general case in <xref linkend="three-wire">.
</para>
<figure id="three-wire"><title>3 wire control semantics in LSE</title>
<mediaobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/3wire" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/3wire.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>

<para>When module instance A transitions data from
<constant>LSE_signal_unknown</> to <constant>LSE_signal_something</>,
module instance B decides whether or not it can accept this data.  If
it can, it sends an <constant>LSE_signal_ack</> to module instance A.
Usually, module instance A then routes ack back to enable.
Alternatively, if module A simply forwarded data from another port
onto its <portname>output</> port, then it may send the ack signal to
that port and route the enable signal from that port to
<portname>output</>.  If enable is <constant>LSE_signal_enable</>
module instance B updates its internal state using the data on
<portname>input</>.
</para>

<para>
By default, the control signal handling of instances of the delay
module is as follows.  If the delay element is empty, it will
unconditionally send <constant>LSE_signal_ack</> on <portname>in[i]</>
(where i is some port instance).  If the delay element is full for
port instance <literal>i</>, then the delay element sends
<constant>LSE_signal_ack</> if its <portname>out[i]</> port instance
had <constant>LSE_signal_ack</> and <constant>LSE_signal_nack</>
otherwise.  The enable signal on port instance <portname>out[i]</> is
the same as the ack signal.
</para>

<para>
The <literal>bit1_tee</> instance will AND its output port instance
ack signals together using <constant>LSE_signal_ack</> as logical
TRUE, and <constant>LSE_signal_nack</> as logical FALSE (any unknown
ack signal causes an unknown output ack signal).  The instance simply
fans out the incoming enable signal.  <literal>xor_gate</> behaves in
a similar fashion, but passes the ack through and ANDs the enables.
</para>


<para>An analysis of the specification then shows that we have a data
dependence loop in the computation of the ack signals.  This means
that the ack signals cannot resolve, and neither can the enables.
<xref linkend="combo-loop"> shows one of the loops on the visualization of
the specification.
</para>
<figure id="combo-loop"><title>Combinational loop</title>
<mediaobject>
  <imageobject>
    <imagedata scale="40" scalefit="1" fileref="figures/combo_loop" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scale="40" scalefit="1" fileref="figures/combo_loop.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>
</sect1>

<sect1>
<title>Control Points</>
<para>
Each module instance has a system defined user point parameter per
port (not port instance) called a control point.  The code for this
user point is free to change the status of any signal before it leaves
or reaches the module (depending on whether the signal is an output or
input signal).  The only restriction is that the data value itself
cannot be changed (though an <constant>LSE_signal_something</> can be
changed to an <constant>LSE_signal_nothing</>).  This section will
describe these control points and how to use them to break the
combinational loop described earlier.
</para>
<para>
Control point parameters are set as if they were parameters of a port.
The following syntax sets a control point:
<programlisting>
<replaceable>instance</>.<replaceable>portname</>.control = <<< ... >>>;
</programlisting>
</para>
<sect2><title>Input Control Points</> 
<para>
Control points on input ports have the following parameters
<variablelist>
  <varlistentry><term><literal>porti</></term>
  <listitem>
  <para>The port instance for which the control point is being
        invoked.  All statuses reported refer to the signals for this
        port instance.
  </para>
  </listitem>
  </varlistentry>

  <varlistentry><term><literal>istatus</></term> 
  <listitem>
  <para>A status variable that contains the status for the incoming
  data signal, incoming enable signal, and outgoing ack signal.
  </para>
  </listitem>
  </varlistentry>

  <varlistentry><term><literal>ostatus</></term> 
  <listitem>
  <para>A status variable that contains the status for the data signal
  and enable signal that will be seen by the instance, and the ack
  signal produced by the instance.
  </para>
  </listitem>
  </varlistentry>
</variablelist>
The return value of the code specifies the status for the data and
enable signal the instance will see and the ack signal that will be
sent out on the particular port instance.  This is illustrated in
<xref linkend="controlfunc-in">.
</para>
<figure id="controlfunc-in"><title>Input Control Point</title>
<mediaobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/controlfunc_in" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/controlfunc_in.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>
</sect2>

<sect2><title>Output Control Points</> 
<para>
Control points on output ports have the following parameters
<variablelist>
  <varlistentry><term><literal>porti</></term>
  <listitem>
  <para>The port instance for which the control point is being
  invoked.  All statuses reported refer to the signals for this port
  instance.
  </para>
  </listitem>
  </varlistentry>

  <varlistentry><term><literal>istatus</></term> 
  <listitem>
  <para>A status variable that contains the status for the data signal
  and enable signal coming from the instance and outgoing ack signal
  coming in on the port.
  </para>
  </listitem>
  </varlistentry>

  <varlistentry><term><literal>ostatus</></term> 
  <listitem>
  <para>A status variable that contains the status for the outgoing
  data signal, outgoing enable signal, and the ack signal that will be
  seen by the instance.
  </para>
  </listitem>
  </varlistentry>
</variablelist>
The return value of the code specifies the status for the ack signal
the instance will see and the data and enable signal that will be sent
out on the particular port instance.  This is illustrated in <xref
linkend="controlfunc-out">.
</para>
<figure id="controlfunc-out"><title>Output Control Point</title>
<mediaobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/controlfunc_out" format="EPS">
  </imageobject>
  <imageobject>
    <imagedata scalefit="1" fileref="figures/controlfunc_out.png" format="PNG">
  </imageobject>
</mediaobject>
</figure>
</sect2>

<sect2>
<title>Breaking the Loop</title>
<para>
Since control points can alter the status of a signal, we can fill in
a control point such that it breaks the circular dependence.  The
following code does exactly that.
<programlisting linenumbering="numbered">
bit2.in.control = &lt;&lt;&lt; return LSE_signal_extract_data(istatus) |
                             LSE_signal_extract_enable(istatus) |
                             LSE_signal_ack; &gt;&gt;&gt;;
</programlisting>
</para>
<para>
Line 1 above defines a control point for the <portname>in</> port on
<literal>bit2</>.  The code on lines 1-3 builds a status value for the
data, enable and ack signals.  The data and enable signal are just
passed through (via the <literal>LSE_signal_extract</> calls), and the
ack status is unconditionally <constant>LSE_signal_ack</>.  In general
port status values are built up by bitwise ORing individual data,
enable, and ack statuses.
</para>
<para>
Since the ack signal for <literal>bit2</>.<portname>in</> no longer
depends on the ack signal on <literal>bit2</>.<portname>out</>, we
have broken the combinational loop.  Always outputting
<constant>LSE_signal_ack</> will yield the behavior desired because of
the overall topology and functionality of the blocks.
</para>
<para>
Building and running this configuration gives the following output.
Once again, simulation must be terminated with an user interrupt.
<screen>
0/0: bit0=1
0/0: bit1=1
0/0: bit2=0
1/0: bit0=1
1/0: bit1=0
1/0: bit2=0
2/0: bit0=0
2/0: bit1=0
2/0: bit2=1
...
</screen>
</para>
</sect2>
<sect2><title>Full Code</>
<para>
Below is the full text of the example code.
<programlisting>
using corelib;
include "xor_gate.lss";

instance bit0 : delay;
instance bit1 : delay;
instance bit2 : delay;
instance xor : xor_gate;
instance bit1_tee : tee;

bit0.initial_state = &lt;&lt;&lt; *init_id = LSE_dynid_create(); 
                         *init_value = TRUE; 
                         return TRUE; &gt;&gt;&gt;;
bit1.initial_state = &lt;&lt;&lt; *init_id = LSE_dynid_create(); 
                         *init_value = TRUE; 
                         return TRUE; &gt;&gt;&gt;;
bit2.initial_state = &lt;&lt;&lt; *init_id = LSE_dynid_create(); 
                         *init_value = TRUE; 
                         return TRUE; &gt;&gt;&gt;;

bit2.out -&gt; bit1.in;
bit1.out -&gt; bit1_tee.in;
bit1_tee.out[0] -&gt; xor.in0;
bit1_tee.out[1] -&gt; bit0.in;
bit0.out -&gt; xor.in1;
xor.out -&gt; bit2.in;

bit2.in.control = &lt;&lt;&lt; return LSE_signal_extract_data(istatus) | 
                             LSE_signal_extract_enable(istatus) |
                             LSE_signal_ack; &gt;&gt;&gt;;

collector STORED_DATA on "bit2" {
  decl=&lt;&lt;&lt;
#include &lt;stdio.h&gt;
  &gt;&gt;&gt;;
  
  record=&lt;&lt;&lt;
      printf(LSE_time_print_args(LSE_time_now));
      printf(": bit2=%d\n", *datap);
  &gt;&gt;&gt;;
};

collector STORED_DATA on "bit1" {
  decl=&lt;&lt;&lt;
#include &lt;stdio.h&gt;
  &gt;&gt;&gt;;
  
  record=&lt;&lt;&lt;
      printf(LSE_time_print_args(LSE_time_now));
      printf(": bit1=%d\n", *datap);
  &gt;&gt;&gt;;
};

collector STORED_DATA on "bit0" {
  decl=&lt;&lt;&lt;
#include &lt;stdio.h&gt;
  &gt;&gt;&gt;;
  
  record=&lt;&lt;&lt;
      printf(LSE_time_print_args(LSE_time_now));
      printf(": bit0=%d\n", *datap);
  &gt;&gt;&gt;;
};
</programlisting>
</para>
</sect2>
</sect1>

<sect1>
<title>The <modulename>xor_gate</> Module</title>
<para>
In <xref linkend="lfsr-config">, recall that line 2 included a file
that defined the <modulename>xor_gate</> module.  This section
describes how to build this module.
</para>
<para>
The following is the text in the xor_gate.lss file.
<programlisting linenumbering="numbered">
module xor_gate {
  using corelib;

  inport in0:boolean;
  inport in1:boolean;
  outport out:boolean;

  instance gate:combiner;

  gate.inputs={"in0","in1"};
  gate.outputs={"out"};

  gate.combine = &lt;&lt;&lt; *out_id=in0_id;
                     *out_data = (*in0_data) ^ (*in1_data);
                     *out_status = LSE_signal_something; &gt;&gt;&gt;;

  if(in0.width != in1.width) {
    punt(&lt;&lt;&lt;in0.width (${in0.width}) must equal in1.width (${in1.width}).&gt;&gt;&gt;);
  }

  if(in0.width != out.width) {
    punt(&lt;&lt;&lt;in0.width must equal out.width.&gt;&gt;&gt;);
  }


  LSS_connect_bus(in0,gate.in0,in0.width);
  LSS_connect_bus(in1,gate.in1,in1.width);
  LSS_connect_bus(gate.out,out,out.width);
};
</programlisting>
</para>

<para>
Line 1 opens a declaration for a new module using the
<literal>module</> keyword.  The syntax for this declaration is:
<programlisting>
module <replaceable>modulename</> {
  <replaceable>modulebody</>
};
</programlisting>
</para>

<para>
Line 2 imports the modules in <packagename>corelib</> into the local
module name space.  (this is so that the module can be used even if
the user of this module does not import <packagename>corelib</>).
</para>

<para>
Lines 3-5 define the ports of the module.  Here, there are two input
ports, <portname>in0</> and <portname>in1</>, and one output port,
<portname>out</>.  These ports require <type>boolean</type> values for
inputs.  More information on port declarations can be found in the LSS
reference appendix in &USERMAN;
</para>

<para>
Line 8 instantiates the <modulename>combiner</> module, which is a very
flexible module that can be used for many functions.  Instantiating a
module within a module definition means that each time the defined
module is instantiated the submodule is also instantiated.  In general
modules that have submodules are called hierarchical modules.
</para>

<para>
Line 10-11 define the ports on the <modulename>combiner</> module.
Most modules have a predetermined set of ports, however, the
<modulename>combiner</> module allows the ports of instances to be
defined based on a parameter.  Lines 13-15 define the I/O function,
which is simply an exclusive-or of the data values.  More information
on the operation of the combiner module can be found in the Core Module
Library Reference.
</para>

<para>
Lines 17-23 checks to make sure that the width of all ports match.
This is because the desired behavior of this module is to act as an
array of xor gates when more than one port instance per port is used.
As a result, non-matching widths will lead to unconnected ports for
the gate, which results in undefined output values.
</para>

<para>
Lines 26, 27, and 28 connect the outer module's ports to the
<literal>gate</> instance's ports.
<literal>LSS_connect_bus(<replaceable>output</>,
<replaceable>input</>, <replaceable>width</>, <replaceable>type</>)</>
is a builtin LSS function that is equivalent to the following LSS
code:
<programlisting>
var i:int;
for(i=0;i&lt;<replaceable>width</>;i++) {
  <replaceable>input</>[i] -&gt;[<replaceable>type</>] <replaceable>output</>[i];
}
</programlisting>
Note that using this function implies explicit indexing on the ports.
Also, the <replaceable>type</> parameter is optional.
</para>
<para>
Hierarchical modules can also have parameters.  More information is
available in the LSS Reference Appendix of &USERMAN;
</para>
</sect1>
</chapter>
